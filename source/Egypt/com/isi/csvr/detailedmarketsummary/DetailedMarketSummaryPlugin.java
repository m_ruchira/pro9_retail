package com.isi.csvr.detailedmarketsummary;

import com.isi.csvr.plugin.Plugin;
import com.isi.csvr.plugin.event.PluginEvent;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.Meta;

/**
 * Created by IntelliJ IDEA.
 * User: Rajeevp
 * Date: Aug 16, 2010
 * Time: 2:05:57 PM
 */
public class DetailedMarketSummaryPlugin implements Plugin {
    public void load(Object... params) {

    }

    public void showUI(Object... params) {
        DetailedMarketSummaryWindow.getSharedInstance().setVisible(true, "CASE");
    }

    public void save(Object... params) {

    }

    public boolean isLoaded() {
        return false;
    }

    public String getCaption() {
        return null;
    }

    public void unload(Object... params) {
    }

    public void pluginEvent(PluginEvent event, Object... data) {

    }

    public boolean isDataNeeded() {
        return true;
    }

    public void setData(int id, Object... params) {
        try {
            if (id == Meta.MESSAGE_TYPE_DETAILDE_MARKET_SUMMARY) {
                DetailedMarketSummary dms = DetailedMarketSummaryStore.getSharedInstance().getDetailedMktSummary(params[1].toString() +
                        DetailedMarketSummary.KEY_SEPARATOR + params[0].toString());
                boolean status = dms.setData(((String) params[2]).split(Meta.FD));
                if (status) {
                    DetailedMarketSummaryWindow.executeDataScript(DetailedMarketSummaryStore.boards.indexOf(params[1].toString()), true);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void applyWorkspace(String id, String value) {
        if (id.equals(getViewSettingID())) {
            DetailedMarketSummaryWindow.getSharedInstance().setWorkspaceString(value);
        }
    }

    public String getWorkspaceString(String id) {
        if (id.equals(getViewSettingID())) {
            return DetailedMarketSummaryWindow.getSharedInstance().getWorkspaceString();
        }
        return null;
    }

    public String getViewSettingID() {
        return "" + ViewSettingsManager.DETAILED_MARKET_SUMMARY;
    }

    public int getWindowTypeID() {
        return -1;
    }

    public int getWindowTypeCategory() {
        return Meta.WINDOW_TYPE_CATEGORY_INVALID;
    }
}
