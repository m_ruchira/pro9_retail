package com.isi.csvr.detailedmarketsummary;

import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: rajeevp
 * Date: Aug 20, 2010
 * Time: 10:38:03 AM
 */
public class DetailedMarketSummary {
    public static String KEY_SEPARATOR = "~";
    private String id;
    private String name;
    private String exchange;
    private long volume = 0;
    private double turnover = 0;
    private double noOfTrades = 0;
    private int symbolsTraded = 0;
    private int noOfUps = 0;
    private int noOfDown = 0;
    private int noOfNoChange = 0;
    private int mktStatus = -1;

    public DetailedMarketSummary(String name, String exchange) {
        this.name = name;
        this.exchange = exchange;
        this.id = name + KEY_SEPARATOR + exchange;
    }

    public boolean setData(String[] data) {
        if (data == null)
            return false;

        char tag;
        try {
            for (String aData : data) {
                tag = aData.charAt(0);
                try {
                    switch (tag) {
                        case 'A':
                            volume = SharedMethods.getLong(aData.substring(1, aData.length()));
                            break;
                        case 'B':
                            turnover = SharedMethods.getDouble(aData.substring(1, aData.length()));
                            break;
                        case 'C':
                            noOfTrades = SharedMethods.getDouble(aData.substring(1, aData.length()));
                            break;
                        case 'D':
                            noOfUps = Integer.parseInt(aData.substring(1, aData.length()));
                            break;
                        case 'E':
                            noOfDown = Integer.parseInt(aData.substring(1, aData.length()));
                            break;
                        case 'F':
                            noOfNoChange = Integer.parseInt(aData.substring(1, aData.length()));
                            break;
                        case 'G':
                            symbolsTraded = Integer.parseInt(aData.substring(1, aData.length()));
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
        } catch (
                Exception ex
                )

        {
            ex.printStackTrace();
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getVolume() {
        if (volume >= 1000)
            return volume / 1000;
        else
            return ((double) volume) / 1000d;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public double getTurnover() {
        if (turnover >= 1000)
            return turnover / 1000;
        else
            return turnover / 1000d;
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
    }

    public double getNoOfTrades() {
        return noOfTrades;
    }

    public void setNoOfTrades(int noOfTrades) {
        this.noOfTrades = noOfTrades;
    }

    public int getSymbolsTraded() {
        return symbolsTraded;
    }

    public void setSymbolsTraded(int symbolsTraded) {
        this.symbolsTraded = symbolsTraded;
    }

    public int getNoOfUps() {
        return noOfUps;
    }

    public void setNoOfUps(int noOfUps) {
        this.noOfUps = noOfUps;
    }

    public int getNoOfDown() {
        return noOfDown;
    }

    public void setNoOfDown(int noOfDown) {
        this.noOfDown = noOfDown;
    }

    public int getNoOfNoChange() {
        return noOfNoChange;
    }

    public void setNoOfNoChange(int noOfNoChange) {
        this.noOfNoChange = noOfNoChange;
    }

    public int getMktStatus() {
        return mktStatus;
    }

    public void setMktStatus(int mktStatus) {
        this.mktStatus = mktStatus;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getId() {
        return id;
    }
}
