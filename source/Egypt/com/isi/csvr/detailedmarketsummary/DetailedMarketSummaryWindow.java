package com.isi.csvr.detailedmarketsummary;

import com.isi.csvr.Client;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: Rajeevp
 * Date: Aug 16, 2010
 * Time: 2:12:36 PM
 */
public class DetailedMarketSummaryWindow extends InternalFrame implements ActionListener {
    private static final int NO_OF_COLS = 6;
    private static final int LISTED_ROWS = 4;
    private static final int NILEX_ROWS = 1;
    private static final int OTC_ROWS = 1;
    private static DetailedMarketSummaryWindow self = null;
    private static HashMap<Integer, Object[]> DMSummaries = new HashMap<Integer, Object[]>(9);
    private static Browser browser = null;
    private final int WINDOW_WIDTH = 607;
    private final int WINDOW_HEIGHT = 295;


    public DetailedMarketSummaryWindow() {
        init();
        createUI();

        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setTitle(Language.getString("DETAILED_MARKET_SUMMARY"));
        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        this.setLayer(GUISettings.DEFAULT_LAYER);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setIconifiable(true);
        this.setResizable(false);
        this.setClosable(true);
        this.hideTitleBarMenu();

        Client.getInstance().getDesktop().add(this);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
    }

    public static DetailedMarketSummaryWindow getSharedInstance() {
        if (self == null)
            self = new DetailedMarketSummaryWindow();

        return self;
    }

    private static String formatDecimal(String val, int decimalCount) {
        if (decimalCount > 0 && Double.parseDouble(val) < 1)
            return SharedMethods.getDecimalFormat(decimalCount).format(Double.parseDouble(val));
        return SharedMethods.getDecimalFormat(-0).format(Double.parseDouble(val));
    }

    public static void executeDataScript(Integer id, boolean setData) {
        if (setData)
            setData(id);
        StringBuilder script = new StringBuilder("");
        switch (id) {
            case 0:  // total Listed
                script.append("valrow1('");
                break;
            case 1:
                script.append("valrow2('");
                break;
            case 2:
                script.append("valrow3('");
                break;
            case 3:
                script.append("valrow4('");
                break;
            case 4:
                script.append("valrow5('");
                break;
            case 5:   // total NILEX
                script.append("valrow6('");
                break;
            case 6:
                script.append("valrow7('");
                break;
            case 7:   // total OTC
                script.append("valrow8('");
                break;
            case 8:
                script.append("valrow9('");
                break;
            case -5:  // reset to zero
                script.append("valrow1('0','0','0','0','0','0');");
                script.append("valrow2('0','0','0','0','0','0');");
                script.append("valrow3('0','0','0','0','0','0');");
                script.append("valrow4('0','0','0','0','0','0');");
                script.append("valrow5('0','0','0','0','0','0');");
                script.append("valrow6('0','0','0','0','0','0');");
                script.append("valrow7('0','0','0','0','0','0');");
                script.append("valrow8('0','0','0','0','0','0');");
                script.append("valrow9('0','0','0','0','0','0');");
                break;
            default:
                System.out.println("Error while parsing Detailed Market Summary data");
        }

        if (id >= 0) {
            for (int i = 0; i < DMSummaries.get(id).length; i++) {
                if (i < 2) // Long values
                    script.append(formatDecimal(DMSummaries.get(id)[i].toString(), 3));
                else
                    script.append(formatDecimal(DMSummaries.get(id)[i].toString(), 0));
//                    script.append(DMSummaries.get(id)[i].toString());
                script.append("','");
            }
            script.append("0')");
        }
        try {
            browser.executeScript(script.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void setData(int id) {
        Object[] data;
        if (id != -1) {
            data = new Object[NO_OF_COLS];
            DetailedMarketSummary dms = DetailedMarketSummaryStore.getSharedInstance().
                    getDetailedMarketSummaryIfAvailable(DetailedMarketSummaryStore.boards.get(id) + DetailedMarketSummary.KEY_SEPARATOR + DetailedMarketSummaryStore.exchange);
            setData(dms, data, id);
        }
//        else {
//            data = new Object[NO_OF_COLS];
//            for (int i = 0; i < NO_OF_COLS; i++)
//                data[i] = "0";
//        }
    }

    private static void setData(DetailedMarketSummary dms, Object[] data, int id) {
        if (dms != null) {
            Integer totIndex = 0;
            int subRows = 0;
            data[0] = dms.getVolume();
            data[1] = dms.getTurnover();
            data[2] = dms.getNoOfTrades();
            data[3] = dms.getNoOfUps();
            data[4] = dms.getNoOfDown();
            data[5] = dms.getNoOfNoChange();


            DMSummaries.put(id, data);

            if (id <= LISTED_ROWS) {
                totIndex = 0;
                subRows = LISTED_ROWS;
            } else if (id <= LISTED_ROWS + NILEX_ROWS + 1) {
                totIndex = LISTED_ROWS + NILEX_ROWS;
                subRows = NILEX_ROWS;
            } else if (id <= LISTED_ROWS + NILEX_ROWS + OTC_ROWS + 2) {
                totIndex = LISTED_ROWS + NILEX_ROWS + OTC_ROWS + 1;
                subRows = OTC_ROWS;
            }

            Object[] total = new Object[NO_OF_COLS];
            for (int i = 0; i < NO_OF_COLS; i++) {
//                if (i < 3) // received as double/long
                total[i] = getTotal(totIndex, i, subRows);
//                else
//                    total[i] = Integer.parseInt((DMSummaries.get(totIndex) != null ? DMSummaries.get(totIndex)[i] : "0").toString()) + Integer.parseInt(data[i].toString());
            }


            DMSummaries.remove(totIndex);
            DMSummaries.put(totIndex, total);
            executeDataScript(totIndex, false);
        }
//        else {
//            for (int j = 0; j < NO_OF_COLS; j++)
//                data[j] = "0";
//        }
    }

    private static Object getTotal(int totIndex, int i, int subRows) {
        double tot = 0;
        try {
            for (Integer j = totIndex + 1; j < totIndex + 1 + subRows; j++)
                tot += Double.parseDouble((DMSummaries.get(j) != null ? DMSummaries.get(j)[i].toString() : "0"));
            return tot;
        } catch (Exception e) {
            return 0;
        }
    }

    public static void clearDMSummaries() {
        DMSummaries.clear();
    }

    public void setVisible(boolean value, String exch) {
        super.setVisible(value);
        DetailedMarketSummaryStore.exchange = exch;
//        addRequest();
    }

    private void addRequest() {
        String request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + Meta.MESSAGE_TYPE_DETAILDE_MARKET_SUMMARY + Meta.FD + DetailedMarketSummaryStore.exchange;
        System.out.println("Detailed Market Summary Request = " + request);
        SendQFactory.addData(Constants.PATH_PRIMARY, request);
    }

    private void createUI() {
        try {
            browser = new Browser();
            browser.getProperties().setAllowContextMenu(false);
//            browser.getProperties().setAllowClientPull(false);
//            browser.getProperties().setAllowNewWindow(false);
            browser.getProperties().setShowScrollBars(false);
            String path;
            if (Language.isLTR())
                path = "./Templates/mkt-summury.html";
            else path = "./Templates/mkt-summury-ar.html";
            File file = new File(path);
            browser.navigate(file.getAbsolutePath());

            executeNameScript();
            executeDataScript(-5, false);  // -5 is initial data (reset to zero) (-1 used to invalid symbol)
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.setLayout(new GridLayout(1, 1));
        if (browser != null) {
            Dimension dim = browser.getDocument().getBody().getSize();
            this.setSize((int) dim.getWidth(), (int) dim.getHeight() + 25);
            this.add(browser);
            this.repaint();
        } else {
            this.add(new JLabel("Error"));
        }
//        this.setLayout(new FlexGridLayout(new String[]{"100", "100"}, new String[]{"100", "100"}));
//        this.add(new JLabel("A"));
//        this.add(new JLabel("B"));
//        this.add(new JLabel("C"));
//        this.add(new JLabel("D"));
    }

    private void executeNameScript() {
        try {
            StringBuilder scriptNames = new StringBuilder("msumlble('");
            scriptNames.append(Language.getString("LISTED")).append("','");
            scriptNames.append(Language.getString("LISTED_STOCKS")).append("','");
            scriptNames.append(Language.getString("LISTED_PDBONDS")).append("','");
            scriptNames.append(Language.getString("LISTED_OTHER_BONDS")).append("','");
            scriptNames.append(Language.getString("LISTED_FUNDS")).append("','");
            scriptNames.append(Language.getString("NILEX")).append("','");
            scriptNames.append(Language.getString("NILEX_STOCKS")).append("','");
            scriptNames.append(Language.getString("OTC")).append("','");
            scriptNames.append(Language.getString("OTC_ORDERS")).append("','");

            scriptNames.append(Language.getString("DMS_VOLUME")).append("','");
            scriptNames.append(Language.getString("DMS_TURNOVER")).append("','");
            scriptNames.append(Language.getString("DMS_TRADES")).append("','");
            scriptNames.append(Language.getString("DMS_UPS")).append("','");
            scriptNames.append(Language.getString("DMS_DOWNS")).append("','");
            scriptNames.append(Language.getString("DMS_NO_OF_CHANGES")).append("','");
            scriptNames.append(Language.getString("DMS_VOL000")).append("','");
            scriptNames.append(Language.getString("DMS_TURN000")).append("'");
            scriptNames.append(")");

            browser.executeScript(scriptNames.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean init() {
        try {
            DetailedMarketSummaryStore.boards.addAll(Arrays.asList("LISTED", "LSTOCK", "LPDBOND", "LOTBOND", "LFUND", "NILEX", "NSTOCK", "OTC", "OORDER"));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String toString() {
        return Language.getString("DETAILED_MARKET_SUMMARY");
    }

    public String getWorkspaceString() {
        return (String.valueOf(this.isVisible()) + ";" + this.getWidth() + ";" + this.getHeight() + ";" +
                this.getLocation().x + ";" + this.getLocation().y);

    }

    public void setWorkspaceString(String value) {
        try {
            String[] results = value.split(";");
            boolean isVisible = Boolean.parseBoolean(results[0]);
            int windowWidth = Integer.parseInt(results[1]);
            int windowHeight = Integer.parseInt(results[2]);
            int x = Integer.parseInt(results[3]);
            int y = Integer.parseInt(results[4]);

            this.setSize(windowWidth, windowHeight);
            this.setVisible(isVisible);
            this.setLocation(x, y);

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}
