package com.isi.csvr.investortypes;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 14, 2009
 * Time: 9:26:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class InvestorTypeWindow extends InternalFrame implements ActionListener, Runnable, TWTabbedPaleListener {

    private static InvestorTypeWindow window = null;
    private final int WINDOW_WIDTH = 780;
    private final int WINDOW_HEIGHT = 480;
    private final long REFRESH_RATE = 1000;
    private final String STRING_N_A = "N/A";
    private final String CURRENT_EXCHANGE = "CASE";
    private final String htmlStringShow = "<html><u>" + Language.getString("INV_SHOW") + "</u></html>";
    private final String htmlStringHide = "<html><u>" + Language.getString("INV_HIDE") + "</u></html>";
    private final FlexGridLayout layoutWithOutChart = new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "79", "25", "79"}, 0, 0);
    private final FlexGridLayout layoutWithChart = new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "79", "250", "104"}, 0, 0);
    private boolean isChartVisible = true;
    private JPanel modeSelectorPanel;
    private TWTabbedPane tabPanel;
    private JPanel topPanel;
    private JPanel bottomPanel;
    private JPanel chartPanel;
    private JPanel periodPanel;
    private JLabel lblPeriod;
    private TWComboBox periodCombo;
    private JSeparator sep = new JSeparator(JSeparator.HORIZONTAL);
    private PrimaryInvestorChart primaryChart;
    private SecondaryInvestorChart secondayChart;
    private Color domColor;
    private Color arabColor;
    private Color forColor;
    private Color indColor;
    private Color insColor;
    private Color topPanelFillColor;
    private Color bottomPanelFillColor;
    private RowInfoPanel rowInfoDom;
    private RowInfoPanel rowInfoArab;
    private RowInfoPanel rowInfoFor;
    private RowInfoPanel rowInfoInd;
    private RowInfoPanel rowInfoIns;
    private JPanel header1 = new HeaderPanel();
    private JPanel header2 = new HeaderPanel();
    private JSeparator sep1;
    private JSeparator sep2;
    private JSeparator sep3;
    private JSeparator sep4;
    private JSeparator sep5;
    private JSeparator sep6;
    private JSeparator sep7;
    private Color tableBoderColor;
    private Color labelFontColor;
    private Font labelFont = new Font("Arial", Font.BOLD, 12);
    private Thread updatorThread;
    private boolean isWIndowActive = false;
    private ResultPercentagePanel resultDom = null;
    private ResultPercentagePanel resultFor = null;
    private ResultPercentagePanel resultArab = null;
    private ResultPercentagePanel resultInd = null;
    private ResultPercentagePanel resultIns = null;
    private JLabel lblShowChart;
    private JLabel lblHideChart;
    private JPanel pnlEmpty = new JPanel();
    private JPanel pnlShowChart;
    private JPanel pnlTemp1 = new JPanel();
    private JPanel pnlTemp2 = new JPanel();
    private JPanel pnlTemp4 = new JPanel();
    private JPanel pnlTemp3 = new JPanel();
    private JPanel pnlTemp5 = new JPanel();
    private TWDecimalFormat pctFormatter = new TWDecimalFormat("#0.00");

    private InvestorTypeWindow() {
        initializeParams();
        createUI();

        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setTitle(Language.getString("INV_TYPES"));
        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        this.setLayer(GUISettings.TOP_LAYER);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setIconifiable(true);
        this.setClosable(true);
        this.hideTitleBarMenu();

        Client.getInstance().getDesktop().add(this);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);

        //updatorThread.start();
    }

    public static InvestorTypeWindow getSharedInstance() {
        if (window == null) {
            window = new InvestorTypeWindow();
        }
        return window;
    }

    private void createUI() {
        createTabPanel();
        topPanel = createTopPanel();
        bottomPanel = createBottomPanel();
        chartPanel = createChartPanel();
        pnlShowChart = createShowChartPanel();

        constructChartArea();
    }

    private void createTabPanel() {
        modeSelectorPanel = new JPanel();
        modeSelectorPanel.setLayout(new FlexGridLayout(new String[]{"25%", "25%", "100%"}, new String[]{"100%"}));

        modeSelectorPanel.setOpaque(true);

        periodPanel = new JPanel();
        periodPanel.setLayout(new FlexGridLayout(new String[]{"45%", "55%"}, new String[]{"100%"}));
        lblPeriod = new JLabel(Language.getString("PERIOD"));
        periodPanel.setOpaque(true);
        periodCombo = new TWComboBox();
        populatePeriodCombo();
        periodPanel.add(lblPeriod);
        periodPanel.add(periodCombo);
        periodCombo.addActionListener(this);
        periodPanel.setVisible(false);

        tabPanel = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "dt");
        tabPanel.addTabPanelListener(this);
        tabPanel.addTab(Language.getString("INTRADAY"), new JLabel());
        tabPanel.addTab(Language.getString("HISTORY"), new JLabel());
        tabPanel.setSelectedIndex(0);


        modeSelectorPanel.add(tabPanel);
        modeSelectorPanel.add(periodPanel);
        try {
            Color bgColor = Theme.getOptionalColor("TAB_" + "DT_" + "BGCOLOR");
            modeSelectorPanel.setBackground(bgColor);
            periodPanel.setBackground(bgColor);
        } catch (Exception e) {
            modeSelectorPanel.setBackground(null);
            periodPanel.setBackground(null);
        }

    }

    private void populatePeriodCombo() {
        periodCombo.addItem(Language.getString("WEEKLY"));
        periodCombo.addItem(Language.getString("MONTHLY"));
        periodCombo.addItem(Language.getString("QUARTERLY"));
        periodCombo.addItem(Language.getString("YEARLY"));
    }

    private void initializeParams() {

        sep1 = new JSeparator(JSeparator.HORIZONTAL);
        sep2 = new JSeparator(JSeparator.HORIZONTAL);
        sep3 = new JSeparator(JSeparator.HORIZONTAL);
        sep4 = new JSeparator(JSeparator.HORIZONTAL);
        sep5 = new JSeparator(JSeparator.HORIZONTAL);
        sep6 = new JSeparator(JSeparator.HORIZONTAL);
        sep7 = new JSeparator(JSeparator.HORIZONTAL);

        tableBoderColor = Theme.getColor("INV_TABLE_BORDER_COLOR");
        setTableBorders(tableBoderColor);

        arabColor = Theme.getColor("INV_ARAB_COLOR");
        domColor = Theme.getColor("INV_DOMESTIC_COLOR");
        forColor = Theme.getColor("INV_FOREIGN_COLOR");
        indColor = Theme.getColor("INV_IND_COLOR");
        insColor = Theme.getColor("INV_INS_COLOR");

        topPanelFillColor = Theme.getColor("INV_TOP_COLOR");
        bottomPanelFillColor = Theme.getColor("INV_BOTTOM_COLOR");
        rowInfoDom = new RowInfoPanel(RowInfoPanel.PANEL_DOMESTIC, bottomPanelFillColor);
        rowInfoArab = new RowInfoPanel(RowInfoPanel.PANEL_ARAB, bottomPanelFillColor);
        rowInfoFor = new RowInfoPanel(RowInfoPanel.PANEL_FOREIGN, bottomPanelFillColor);

        rowInfoInd = new RowInfoPanel(RowInfoPanel.PANEL_INDIVIDUAL, topPanelFillColor);
        rowInfoIns = new RowInfoPanel(RowInfoPanel.PANEL_INSTITUTIONAL, topPanelFillColor);

        resultDom = new ResultPercentagePanel(Language.getString("INV_DOMESTIC"), STRING_N_A, domColor);
        resultFor = new ResultPercentagePanel(Language.getString("INV_FOREIGN"), STRING_N_A, forColor);
        resultArab = new ResultPercentagePanel(Language.getString("INV_ARAB"), STRING_N_A, arabColor);
        resultInd = new ResultPercentagePanel(Language.getString("INV_INDIVIDUALS"), STRING_N_A, indColor);
        resultIns = new ResultPercentagePanel(Language.getString("INV_INSTITUTION"), STRING_N_A, insColor);

        resultDom.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        resultFor.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        resultArab.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        resultInd.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        resultIns.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));

        lblShowChart = new JLabel(htmlStringShow, new ImageIcon("images/Theme" + Theme.getID() + "/charts/inv_chart.gif"), JLabel.CENTER);
        lblHideChart = new JLabel(htmlStringHide, new ImageIcon("images/Theme" + Theme.getID() + "/charts/inv_chart.gif"), JLabel.CENTER);
        lblHideChart.setOpaque(true);
        lblHideChart.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));

        //updatorThread = new Thread(this, "investor type updator");
    }

    private JPanel createChartPanel() {
        JPanel chartPanel = new JPanel(new FlexGridLayout(new String[]{"46%", "17%", "37%"}, new String[]{"100%"}, 8, 10));
        chartPanel.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        primaryChart = new PrimaryInvestorChart(new ArrayList<InvestorBarModel>());
        chartPanel.add(primaryChart);

        JPanel panelResult = new JPanel();
        panelResult.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "25", "25", "25", "25", "25", "25", "25", "90", "50", "25"}, 0, 0));
        panelResult.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));

        pnlTemp1.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        panelResult.add(pnlTemp1);
        panelResult.add(resultDom);
        panelResult.add(resultFor);
        panelResult.add(resultArab);

        pnlTemp2.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        panelResult.add(pnlTemp2);
        panelResult.add(resultInd);
        panelResult.add(resultIns);

        pnlTemp3.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        panelResult.add(pnlTemp3);

        lblHideChart.setFont(new Font("Arial", Font.BOLD, 12));
        lblHideChart.setForeground(Theme.getColor("INV_HIDE_LABEL_FONT_COLOR"));
        lblHideChart.addMouseListener(this);
        //lblHideChart.setPreferredSize(new Dimension(60, 25));
        lblHideChart.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        pnlTemp5 = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"50%"}, 10, 10));
        pnlTemp5.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        pnlTemp5.add(lblHideChart);
        panelResult.add(pnlTemp5);

        pnlTemp4.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        panelResult.add(pnlTemp4);

        chartPanel.add(panelResult);

        secondayChart = new SecondaryInvestorChart(new ArrayList<InvestorBarModel>());
        chartPanel.add(secondayChart);

        return chartPanel;
    }

    private JPanel createTopPanel() {

        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "1", "25", "1", "25", "1"}));

        topPanel.add(header1);
        topPanel.add(sep7);
        topPanel.add(rowInfoInd);
        topPanel.add(sep1);
        topPanel.add(rowInfoIns);
        topPanel.add(sep2);

        topPanel.setBorder(BorderFactory.createLineBorder(tableBoderColor));

        return topPanel;
    }

    private JPanel createBottomPanel() {

        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "1", "25", "1", "25", "1", "25", "1"}));

        topPanel.add(header2);
        topPanel.add(sep);

        topPanel.add(rowInfoDom);
        topPanel.add(sep4);
        topPanel.add(rowInfoArab);
        topPanel.add(sep5);
        topPanel.add(rowInfoFor);
        topPanel.add(sep6);

        topPanel.setBorder(BorderFactory.createLineBorder(tableBoderColor));

        return topPanel;
    }

    private JPanel createShowChartPanel() {

        JPanel pnlShowChart = new JPanel(new FlexGridLayout(new String[]{"100%", "100"}, new String[]{"100%"}, 0, 0));

        lblShowChart.setFont(new Font("Arial", Font.BOLD, 12));
        lblShowChart.setOpaque(true);
        lblShowChart.addMouseListener(this);
        lblShowChart.setBackground(Theme.getColor("INV_HEADER_COLOR"));
        lblShowChart.setForeground(Theme.getColor("INV_SHOW_LABEL_FONT_COLOR"));
        lblShowChart.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        pnlEmpty.setBackground(Theme.getColor("INV_HEADER_COLOR"));
        pnlShowChart.add(pnlEmpty);
        pnlShowChart.add(lblShowChart);

        GUISettings.applyOrientation(lblShowChart);
        return pnlShowChart;
    }

    public void constructChartArea() {
        this.add(modeSelectorPanel);
        //if (tabPanel.getSelectedIndex() == 0) {
        if (!isChartVisible) {
            this.setLayout(layoutWithOutChart);
            this.add(topPanel);
            this.remove(chartPanel);
            bottomPanel.remove(header2);
            bottomPanel.remove(sep);
            bottomPanel.doLayout();
            this.add(pnlShowChart);
            this.add(bottomPanel);
            this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT - 255);
        } else {
            this.setLayout(layoutWithChart);
            if (this.getComponentCount() > 0) {
                this.remove(topPanel);
                this.remove(chartPanel);
                this.remove(bottomPanel);
                this.remove(pnlShowChart);
            }
            if (bottomPanel.getComponent(0) != header2) {
                bottomPanel.add(header2, 0);
                bottomPanel.add(sep, 1);
                bottomPanel.doLayout();
            }
            this.add(topPanel);
            this.add(chartPanel);
            this.add(bottomPanel);
            this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        }
        //}

    }

    private void setTableBorders(Color borderColor) {

        sep1.setForeground(borderColor);
        sep2.setForeground(borderColor);
        sep3.setForeground(borderColor);
        sep4.setForeground(borderColor);
        sep5.setForeground(borderColor);
        sep6.setForeground(borderColor);
        sep7.setForeground(borderColor);
    }

    public Color getDomesticColor() {
        return domColor;
    }

    public Color getArabicColor() {
        return arabColor;
    }

    public Color getForeignColor() {
        return forColor;
    }

    public Color getIndividualColor() {
        return indColor;
    }

    public Color getInstitutionalColor() {
        return insColor;
    }

    public void actionPerformed(ActionEvent e) {

    }


    public void populateData() {

        /*
        TWDecimalFormat format = new TWDecimalFormat("############");
        String result = System.currentTimeMillis() + "-";
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("E:\\development\\data.txt", true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
        ArrayList<InvestorInfo> infoList;
        infoList = InvestorTypeStore.getSharedInstance().getDataForExchange(CURRENT_EXCHANGE);

        if (infoList == null) {
            return;
        }
        //if (getMode() == 0) {
        for (InvestorInfo investorInfo : infoList) {
            //result += investorInfo.getType() + ";" + format.format(investorInfo.getBuyTurnOver()) + ";" + format.format(investorInfo.getSellTurnOver()) + "-";
            if (investorInfo.getType() == Meta.INVESTER_TYPE_DOMESTIC) {
                rowInfoDom.setValues(investorInfo.getBuyTurnOver(), investorInfo.getBuyPercentage(), investorInfo.getSellTurnOver(), investorInfo.getSellPercentage(), investorInfo.getNetValue());
            } else if (investorInfo.getType() == Meta.INVESTER_TYPE_ARAB) {
                rowInfoArab.setValues(investorInfo.getBuyTurnOver(), investorInfo.getBuyPercentage(), investorInfo.getSellTurnOver(), investorInfo.getSellPercentage(), investorInfo.getNetValue());
            } else if (investorInfo.getType() == Meta.INVESTER_TYPE_FOREIGN) {
                rowInfoFor.setValues(investorInfo.getBuyTurnOver(), investorInfo.getBuyPercentage(), investorInfo.getSellTurnOver(), investorInfo.getSellPercentage(), investorInfo.getNetValue());
            } else if (investorInfo.getType() == Meta.INVESTER_TYPE_INDIVIDUAL) {
                rowInfoInd.setValues(investorInfo.getBuyTurnOver(), investorInfo.getBuyPercentage(), investorInfo.getSellTurnOver(), investorInfo.getSellPercentage(), investorInfo.getNetValue());
            } else if (investorInfo.getType() == Meta.INVESTER_TYPE_INSTITUTIONAL) {
                rowInfoIns.setValues(investorInfo.getBuyTurnOver(), investorInfo.getBuyPercentage(), investorInfo.getSellTurnOver(), investorInfo.getSellPercentage(), investorInfo.getNetValue());
            }
        }

        ArrayList<InvestorBarModel> resultPrimary = new ArrayList<InvestorBarModel>();
        ArrayList<InvestorBarModel> resultSecondary = new ArrayList<InvestorBarModel>();

        for (int i = infoList.size() - 1; i >= 0; i--) {
            InvestorInfo investorInfo = infoList.get(i);

            if (investorInfo.getType() == Meta.INVESTER_TYPE_INDIVIDUAL || investorInfo.getType() == Meta.INVESTER_TYPE_INSTITUTIONAL) {
                resultSecondary.add(new InvestorBarModel(investorInfo.getType(), InvestorBarModel.BUY, investorInfo.getBuyPercentage()));
            } else {
                resultPrimary.add(new InvestorBarModel(investorInfo.getType(), InvestorBarModel.BUY, investorInfo.getBuyPercentage()));
            }
        }
        resultPrimary = sortArrayList(resultPrimary);

        ArrayList<InvestorBarModel> tempPrimary = new ArrayList<InvestorBarModel>();
        for (int i = infoList.size() - 1; i >= 0; i--) {
            InvestorInfo investorInfo = infoList.get(i);

            if (investorInfo.getType() == Meta.INVESTER_TYPE_INDIVIDUAL || investorInfo.getType() == Meta.INVESTER_TYPE_INSTITUTIONAL) {
                resultSecondary.add(new InvestorBarModel(investorInfo.getType(), InvestorBarModel.SELL, investorInfo.getSellPercentage()));
            } else {
                tempPrimary.add(new InvestorBarModel(investorInfo.getType(), InvestorBarModel.SELL, investorInfo.getSellPercentage()));
            }
        }

        tempPrimary = sortArrayList(tempPrimary);
        resultPrimary.addAll(tempPrimary);

        primaryChart.setInfo(resultPrimary);
        primaryChart.calculatePixelValues();
        primaryChart.repaint();


        String domVal = pctFormatter.format(getTotalPercentage(infoList, Meta.INVESTER_TYPE_DOMESTIC));
        String arabVal = pctFormatter.format(getTotalPercentage(infoList, Meta.INVESTER_TYPE_ARAB));
        String forVal = pctFormatter.format(getTotalPercentage(infoList, Meta.INVESTER_TYPE_FOREIGN));

        resultDom.setValue(domVal);
        resultArab.setValue(arabVal);
        resultFor.setValue(forVal);

        secondayChart.setInfo(resultSecondary);
        secondayChart.calculatePixelValues();
        secondayChart.repaint();

        String indVal = pctFormatter.format(getTotalPercentage(infoList, Meta.INVESTER_TYPE_INDIVIDUAL));
        String insVal = pctFormatter.format(getTotalPercentage(infoList, Meta.INVESTER_TYPE_INSTITUTIONAL));

        resultInd.setValue(indVal);
        resultIns.setValue(insVal);

        /*
        InvestorInfo inv1 = new InvestorInfo(Meta.INVESTER_TYPE_DOMESTIC, 323496933, 882964219);
        InvestorInfo inv2 = new InvestorInfo(Meta.INVESTER_TYPE_ARAB, 263821331, 298519684);
        InvestorInfo inv3 = new InvestorInfo(Meta.INVESTER_TYPE_FOREIGN, 195393489, 3911227850.0);
        InvestorInfo inv4 = new InvestorInfo("", Meta.INVESTER_TYPE_INDIVIDUAL, 195393489, 3911227850.0, .22, .6);
        InvestorInfo inv5 = new InvestorInfo("", Meta.INVESTER_TYPE_INSTITUTIONAL, 195393489, 3911227850.0, .64, .3);

        ArrayList<InvestorInfo> basic = new ArrayList<InvestorInfo>();
        basic.add(inv1);
        basic.add(inv2);
        basic.add(inv3);

        rowInfoDom.setValues(234234546764.0, 45.65, 34654787945.0, 23.54, 834184347);
        rowInfoArab.setValues(674743657235.0, 77.87, 56325687453.0, 66.76, -346768934);
        rowInfoFor.setValues(674743657235.0, 77.87, 56325687453.0, 66.76, -346768934);
        rowInfoInd.setValues(789493834547.0, 23.65, 77235907567.0, 46.35, 239495600);
        rowInfoIns.setValues(789493834547.0, 23.65, 77235907567.0, 46.35, 239495600);

        ArrayList<InvestorBarModel> barInfoList = DataHandler.getInvestorInfoForChart(basic);

        ArrayList<InvestorBarModel> resultPrimary = new ArrayList<InvestorBarModel>();

        for (int i = basic.size() - 1; i >= 0; i--) {
            InvestorInfo investorInfo = basic.get(i);
            resultPrimary.add(new InvestorBarModel(investorInfo.getType(), InvestorBarModel.BUY, investorInfo.getBuyPercentage() * 100));

        }
        resultPrimary = sortArrayList(resultPrimary);

        ArrayList<InvestorBarModel> tempPrimary = new ArrayList<InvestorBarModel>();
        for (int i = basic.size() - 1; i >= 0; i--) {
            InvestorInfo investorInfo = basic.get(i);
            tempPrimary.add(new InvestorBarModel(investorInfo.getType(), InvestorBarModel.SELL, investorInfo.getSellPercentage() * 100));
        }

        tempPrimary = sortArrayList(tempPrimary);
        resultPrimary.addAll(tempPrimary);

        primaryChart.setInfo(resultPrimary);
        primaryChart.calculatePixelValues();
        primaryChart.repaint();


        basic.clear();
        basic.add(inv4);
        basic.add(inv5);

        ArrayList<InvestorBarModel> resultSecondary = new ArrayList<InvestorBarModel>();

        for (int i = basic.size() - 1; i >= 0; i--) {
            InvestorInfo investorInfo = basic.get(i);
            resultSecondary.add(new InvestorBarModel(investorInfo.getType(), InvestorBarModel.BUY, investorInfo.getBuyPercentage() * 100));

        }
        //resultPrimary = sortArrayList(resultPrimary);

        tempPrimary.clear();
        for (int i = basic.size() - 1; i >= 0; i--) {
            InvestorInfo investorInfo = basic.get(i);
            tempPrimary.add(new InvestorBarModel(investorInfo.getType(), InvestorBarModel.SELL, investorInfo.getSellPercentage() * 100));
        }

        //tempPrimary = sortArrayList(tempPrimary);
        resultSecondary.addAll(tempPrimary);

        resultDom.setValue("34.65");
        resultArab.setValue("89.31");
        resultFor.setValue("87.54");

        resultInd.setValue("21.87");
        resultIns.setValue("64.42");

        secondayChart.setInfo(resultSecondary);
        secondayChart.calculatePixelValues();
        secondayChart.repaint();
        */
    }

    private double getTotalPercentage(ArrayList<InvestorInfo> infoList, byte type) {

        for (int i = 0; i < infoList.size(); i++) {
            InvestorInfo investorInfo = infoList.get(i);
            if (investorInfo.getType() == type) {
                return (investorInfo.getBuyPercentage() + investorInfo.getSellPercentage()) / 2;
            }
        }
        return 0;
    }

    public ArrayList<InvestorBarModel> sortArrayList(ArrayList<InvestorBarModel> result) {

        ArrayList<InvestorBarModel> list = new ArrayList<InvestorBarModel>();
        for (int i = result.size() - 1; i >= 0; i--) {
            InvestorBarModel bar = result.get(i);
            if (bar.getType() == Meta.INVESTER_TYPE_DOMESTIC) {
                list.add(bar);
            }
        }
        for (int i = result.size() - 1; i >= 0; i--) {
            InvestorBarModel bar = result.get(i);
            if (bar.getType() == Meta.INVESTER_TYPE_ARAB) {
                list.add(bar);
            }
        }
        for (int i = result.size() - 1; i >= 0; i--) {
            InvestorBarModel bar = result.get(i);
            if (bar.getType() == Meta.INVESTER_TYPE_FOREIGN) {
                list.add(bar);
            }
        }

        return list;
    }

    public void run() {

        while (isWIndowActive) {

            try {
                if (this.isVisible()) {
                    populateData();
                    Thread.sleep(REFRESH_RATE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void internalFrameActivated(InternalFrameEvent e) {

        if (!isWIndowActive) {
            isWIndowActive = true;
            updatorThread = new Thread(this, "***** updator thread");
            updatorThread.start();
        }
    }

    public void internalFrameOpened(InternalFrameEvent e) {
        if (!isWIndowActive) {
            isWIndowActive = true;
            updatorThread = new Thread(this, "***** updator thread");
            updatorThread.start();
        }

    }

    @Override
    public void internalFrameClosing(InternalFrameEvent e) {
        isWIndowActive = false;
        updatorThread = null;
    }

    public String getWorkspaceString() {

        return (String.valueOf(this.isVisible()) + ";" + this.getWidth() + ";" + this.getHeight() + ";" +
                this.getLocation().x + ";" + this.getLocation().y + ";" + String.valueOf(isChartVisible));

    }

    public void setWorkspaceString(String value) {

        try {
            String[] results = value.split(";");
            boolean isVisible = Boolean.parseBoolean(results[0]);
            int windowWidth = Integer.parseInt(results[1]);
            int windowHeight = Integer.parseInt(results[2]);
            int x = Integer.parseInt(results[3]);
            int y = Integer.parseInt(results[4]);
            boolean chartVisible = Boolean.parseBoolean(results[5]);

            this.setSize(windowWidth, windowHeight);
            this.setVisible(isVisible);
            this.setLocation(x, y);
            isChartVisible = chartVisible;
            constructChartArea();

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);

        if (e.getSource().equals(lblHideChart)) {
            isChartVisible = false;
            constructChartArea();
        } else if (e.getSource().equals(lblShowChart)) {
            isChartVisible = true;
            constructChartArea();
        }
    }

    public void applyTheme() {

        tableBoderColor = Theme.getColor("INV_TABLE_BORDER_COLOR");
        setTableBorders(tableBoderColor);

        labelFontColor = Theme.getColor("INV_TABLE_HEADER_FONT_COLOR");

        arabColor = Theme.getColor("INV_ARAB_COLOR");
        domColor = Theme.getColor("INV_DOMESTIC_COLOR");
        forColor = Theme.getColor("INV_FOREIGN_COLOR");
        indColor = Theme.getColor("INV_IND_COLOR");
        insColor = Theme.getColor("INV_INS_COLOR");

        topPanelFillColor = Theme.getColor("INV_TOP_COLOR");
        bottomPanelFillColor = Theme.getColor("INV_BOTTOM_COLOR");

        rowInfoDom.setBackground(bottomPanelFillColor);
        rowInfoArab.setBackground(bottomPanelFillColor);
        rowInfoFor.setBackground(bottomPanelFillColor);

        rowInfoInd.setBackground(topPanelFillColor);
        rowInfoIns.setBackground(topPanelFillColor);

        resultDom.setColor(domColor);
        resultFor.setColor(forColor);
        resultArab.setColor(arabColor);

        resultInd.setColor(indColor);
        resultIns.setColor(insColor);

        pnlEmpty.setBackground(Theme.getColor("INV_HEADER_COLOR"));

        lblShowChart.setBackground(Theme.getColor("INV_HEADER_COLOR"));
        lblShowChart.setForeground(Theme.getColor("INV_SHOW_LABEL_FONT_COLOR"));

        lblHideChart.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        lblHideChart.setForeground(Theme.getColor("INV_HIDE_LABEL_FONT_COLOR"));

        pnlTemp1.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        pnlTemp2.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        pnlTemp4.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        pnlTemp3.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        pnlTemp5.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));

        chartPanel.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));

        resultDom.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        resultFor.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        resultArab.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        resultInd.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));
        resultIns.setBackground(Theme.getColor("INV_CENTER_PANEL_BG_COLOR"));

        lblShowChart.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/inv_chart.gif"));
        lblHideChart.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/inv_chart.gif"));
    }

    public void tabStareChanged(TWTabEvent event) {
        if (tabPanel.getSelectedIndex() == 0) { //  > intraday mode
            periodPanel.setVisible(false);
        } else {                               //  > history mode
            periodPanel.setVisible(true);

            //chartPanel.setVisible(false);
        }
        populateData();
        /*topPanel.repaint();
        chartPanel.repaint();
        bottomPanel.repaint();*/

        //To change body of implemented methods use File | Settings | File Templates.
    }

    int getMode() {
        return tabPanel.getSelectedIndex();
    }

    public int getDateRange() {
        if (getMode() == 0)
            return 1;
        else {
            if (periodCombo.getSelectedItem().equals(Language.getString("WEEKLY")))
                return 7;
            else if (periodCombo.getSelectedItem().equals(Language.getString("MONTHLY")))
                return 30;
            else if (periodCombo.getSelectedItem().equals(Language.getString("QUARTERLY")))
                return 90;
            else
                return 52;
        }
    }
}