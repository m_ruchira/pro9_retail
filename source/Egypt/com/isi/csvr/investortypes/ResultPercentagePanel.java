package com.isi.csvr.investortypes;

import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Apr 3, 2009
 * Time: 3:50:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResultPercentagePanel extends JPanel {

    private final int SQUARE_WIDTH = 10;
    private String value = "N/A";
    private String str;
    private Color color;

    public ResultPercentagePanel(String str, String value, Color color) {
        if (Language.isLTR()) {
            //this.value = value + " %";
            this.value = value;
            this.str = str;
        } else {
            //this.str = str + " %";
            this.str = str;
            this.value = value;
        }
        this.color = color;

    }

    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(this.getBackground());
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.BLACK);

        int y1 = (this.getHeight() - SQUARE_WIDTH) / 2;
        int y2 = (this.getHeight() + SQUARE_WIDTH) / 2;
        if (Language.isLTR()) {
            g.drawRect(0, y1, SQUARE_WIDTH, SQUARE_WIDTH);
            g.setColor(color);
            g.fillRect(0, y1, SQUARE_WIDTH, SQUARE_WIDTH);
            g.drawString(str, SQUARE_WIDTH + 4, y2);
            int length = g.getFontMetrics().stringWidth(str);
            g.drawString(value, SQUARE_WIDTH + length + 5, y2);
        } else {
            g.drawRect(this.getWidth() - SQUARE_WIDTH, y1, SQUARE_WIDTH, SQUARE_WIDTH);
            g.setColor(color);
            g.fillRect(this.getWidth() - SQUARE_WIDTH, y1, SQUARE_WIDTH, SQUARE_WIDTH);
            String result = str + " " + value;
            //g.drawString(str, SQUARE_WIDTH + 4, y2);
            int length = g.getFontMetrics().stringWidth(result);
            g.drawString(result, this.getWidth() - SQUARE_WIDTH - length - 5, y2);
        }
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        if (Language.isLTR()) {
            this.value = value + " %";
        } else {
            if (!str.contains("%")) {
                this.str = this.str + " %";
            }
            this.value = value;
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
