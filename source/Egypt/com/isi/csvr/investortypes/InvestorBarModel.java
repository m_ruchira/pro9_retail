package com.isi.csvr.investortypes;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 14, 2009
 * Time: 7:27:13 PM
 * To change this template use File | Settings | File Templates.
 */

public class InvestorBarModel {

    protected static final byte BUY = 0;
    protected static final byte SELL = 1;
    private byte type;
    private byte buySell;
    private double value;

    public InvestorBarModel(byte type, byte buySell, double value) {
        this.type = type;
        this.buySell = buySell;
        this.value = value;
    }

    public byte getType() {
        return type;
    }

    public byte getBuySell() {
        return buySell;
    }

    public double getValue() {
        return value;
    }
}
