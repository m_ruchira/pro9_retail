package com.isi.csvr.investortypes;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.downloader.InvestorTypeHistoryDownloadManager;
import com.isi.csvr.downloader.InvestorTypeHistoryDownloader;
import com.isi.csvr.downloader.ZipFileDownloader;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 14, 2009
 * Time: 8:57:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class InvestorInfo {

    public static boolean isDownloadCompleted = false;
    private static String dataHome;
    private static String mktDateHome;
    private static ArrayList<InvestorHistoryInfo> historyInfo_110 = new ArrayList<InvestorHistoryInfo>();
    private static ArrayList<InvestorHistoryInfo> historyInfo_111 = new ArrayList<InvestorHistoryInfo>();
    private static ArrayList<InvestorHistoryInfo> historyInfo_112 = new ArrayList<InvestorHistoryInfo>();
    private static ArrayList<InvestorHistoryInfo> historyInfo_113 = new ArrayList<InvestorHistoryInfo>();
    private static ArrayList<InvestorHistoryInfo> historyInfo_114 = new ArrayList<InvestorHistoryInfo>();
    private static ArrayList[] investorObjList = new ArrayList[]{historyInfo_110, historyInfo_111, historyInfo_112, historyInfo_113, historyInfo_114};
    private static Byte[] invList = new Byte[]{Meta.INVESTER_TYPE_DOMESTIC, Meta.INVESTER_TYPE_FOREIGN, Meta.INVESTER_TYPE_ARAB, Meta.INVESTER_TYPE_INSTITUTIONAL, Meta.INVESTER_TYPE_INDIVIDUAL};
    private static HashMap<Byte, double[][]> processedValues = new HashMap<Byte, double[][]>();
    private final String KEY_SEPERATOR = "~";
    InvestorTypeWindow invInstance = InvestorTypeWindow.getSharedInstance();
    private String exchange;
    private byte investorType = Meta.INVESTER_TYPE_ARAB;
    private boolean active = true;
    private ZipFileDownloader downloader;
    private HashMap calcVal = new HashMap();
    private double buyTurnOver = 0;
    private double sellTurnOver = 0;
    private double netValue = 0;
    private double buyPercentage = 0;
    private double sellPercentage = 0;
    private double buyTurnOver7 = 0;
    private double sellTurnOver7 = 0;
    private double netValue7 = 0;
    private double buyPercentage7 = 0;
    private double sellPercentage7 = 0;
    private double buyTurnOver30 = 0;
    private double sellTurnOver30 = 0;
    private double netValue30 = 0;
    private double buyPercentage30 = 0;
    private double sellPercentage30 = 0;
    private double buyTurnOver90 = 0;
    private double sellTurnOver90 = 0;
    private double netValue90 = 0;
    private double buyPercentage90 = 0;
    private double sellPercentage90 = 0;
    private double buyTurnOver52 = 0;
    private double sellTurnOver52 = 0;
    private double netValue52 = 0;
    private double buyPercentage52 = 0;
    private double sellPercentage52 = 0;
    private int noOfDaysIn7D = 0;
    private int noOfDaysIn30D = 0;
    private int noOfDaysIn90D = 0;
    private int noOfDaysIn52W = 0;
    private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

    /*public InvestorInfo(double buy, double sell) {
        this.buyTurnOver = buy;
        this.sellTurnOver = sell;
        this.netValue = sell - buy;
    }
*/
    /*public InvestorInfo(double buy, double sell, double buyPct, double sellPct) {
        this.buyTurnOver = buy;
        this.sellTurnOver = sell;
        this.netValue = buy - sell;
        this.buyPercentage = buyPct;
        this.sellPercentage = sellPct;
    }*/

    /*public InvestorInfo(String exchange, byte type, double buy, double sell, double buyPct, double sellPct) {
        this.exchange = exchange;
        this.investorType = type;
        this.buyTurnOver = buy;
        this.sellTurnOver = sell;
        this.netValue = buy - sell;
        this.buyPercentage = buyPct;
        this.sellPercentage = sellPct;
    }*/

    /* public InvestorInfo(byte type, double buy, double sell) {

        this.investorType = type;
        this.buyTurnOver = buy;
        this.sellTurnOver = sell;
        this.netValue = buy - sell;
    }*/

    public InvestorInfo(String exchange, byte type) {
        this.exchange = exchange;
        this.investorType = type;
        this.dataHome = Settings.getAbsolutepath() + "DataStore\\investorTypes\\";
        setMktDateHome(exchange);
        clearDataFolderOnStart();

    }

    public InvestorInfo() {
    }

    public static void createFolder(String path) {
        try {
            File folder = new File(path);
            folder.mkdirs();
            folder = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void extractFile(String filepath, File file) {
        synchronized (com.isi.csvr.shared.Constants.FILE_EXTRACT_LOCK) {
            try {
                String path;
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                byte[] bytData = new byte[1000];
                int i;

                while (true) {
                    try {
                        ZipEntry oEntry = zIn.getNextEntry();
                        if (oEntry == null)
                            break;
                        if (oEntry.isDirectory()) {
                            path = filepath + "/" + oEntry.getName();
                            createFolder(path);
                        } else {
                            path = filepath;
                        }
                        File f = new File(oEntry.getName());
                        FileOutputStream oOut = new FileOutputStream(path + "/" + f.getName());
                        while (true) {
                            i = zIn.read(bytData);
                            if (i == -1)
                                break;
                            oOut.write(bytData, 0, i);
                        }
                        zIn.closeEntry();
                        oOut.close();
                        f = null;
                        oEntry = null;
                        oOut = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Thread.sleep(2);
                }
                zIn.close();
                zIn = null;
                bytData = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public static boolean checkIfSummaryFileExsists(String exchange) {
        Exchange exg = ExchangeStore.getSharedInstance().getExchange(exchange);
        if (exg == null) {
            return false;
        }
        //clearFolderOnStart(new File(Settings.getAbsolutepath() + "DataStore\\InvestorTypes\\"));
        File summary = new File(mktDateHome);
        if (summary.exists()) {
            return true;
        } else {
            //clearFolderOnStart(new File(Settings.getAbsolutepath() + "DataStore\\InvestorTypes\\"));
            return false;
        }
    }

    public double getBuyTurnOver() {
        if (invInstance.getMode() == 1)
            return getBuyTurnOverH(this.investorType, invInstance.getDateRange());
        return buyTurnOver;
    }

    public double getSellTurnOver() {
        if (invInstance.getMode() == 1)
            return getSellTurnOverH(this.investorType, invInstance.getDateRange());
        return sellTurnOver;
    }

    public double getNetValue() {
        if (invInstance.getMode() == 1)
            return getNetValueH(this.investorType, invInstance.getDateRange());
        return netValue;
    }

    public double getBuyPercentage() {
        if (invInstance.getMode() == 1)
            return getBuyPercentageH(this.investorType, invInstance.getDateRange());
        return buyPercentage;
    }

    public void setBuyPercentage(double buyPercentage) {
        this.buyPercentage = buyPercentage;
    }

    public double getSellPercentage() {
        if (invInstance.getMode() == 1)
            return getSellPercentageH(this.investorType, invInstance.getDateRange());
        return sellPercentage;
    }

    public void setSellPercentage(double sellPercentage) {
        this.sellPercentage = sellPercentage;
    }

    private double getBuyTurnOverH(Byte invType, int period) {
        if (period == 7)
            return processedValues.get(invType)[0][0];
        else if (period == 30)
            return processedValues.get(invType)[0][1];
        else if (period == 90)
            return processedValues.get(invType)[0][2];
        else
            return processedValues.get(invType)[0][3];
    }

    public double getSellTurnOverH(Byte invType, int period) {
        if (period == 7)
            return processedValues.get(invType)[1][0];
        else if (period == 30)
            return processedValues.get(invType)[1][1];
        else if (period == 90)
            return processedValues.get(invType)[1][2];
        else
            return processedValues.get(invType)[1][3];
    }

    public double getNetValueH(Byte invType, int period) {
        if (period == 7)
            return processedValues.get(invType)[2][0];
        else if (period == 30)
            return processedValues.get(invType)[2][1];
        else if (period == 90)
            return processedValues.get(invType)[2][2];
        else
            return processedValues.get(invType)[2][3];
    }

    public double getBuyPercentageH(Byte invType, int period) {
        if ((invType == Meta.INVESTER_TYPE_DOMESTIC) || (invType == Meta.INVESTER_TYPE_ARAB) || (invType == Meta.INVESTER_TYPE_FOREIGN)) {
            if (period == 7)
                return processedValues.get(invType)[0][0] * 100 / (processedValues.get(Meta.INVESTER_TYPE_DOMESTIC)[0][0] + processedValues.get(Meta.INVESTER_TYPE_ARAB)[0][0] + processedValues.get(Meta.INVESTER_TYPE_FOREIGN)[0][0]);
            else if (period == 30)
                return processedValues.get(invType)[0][1] * 100 / (processedValues.get(Meta.INVESTER_TYPE_DOMESTIC)[0][1] + processedValues.get(Meta.INVESTER_TYPE_ARAB)[0][1] + processedValues.get(Meta.INVESTER_TYPE_FOREIGN)[0][1]);
            else if (period == 90)
                return processedValues.get(invType)[0][2] * 100 / (processedValues.get(Meta.INVESTER_TYPE_DOMESTIC)[0][2] + processedValues.get(Meta.INVESTER_TYPE_ARAB)[0][2] + processedValues.get(Meta.INVESTER_TYPE_FOREIGN)[0][2]);
            else if (period == 52)
                return processedValues.get(invType)[0][3] * 100 / (processedValues.get(Meta.INVESTER_TYPE_DOMESTIC)[0][3] + processedValues.get(Meta.INVESTER_TYPE_ARAB)[0][3] + processedValues.get(Meta.INVESTER_TYPE_FOREIGN)[0][3]);
        } else if ((invType == Meta.INVESTER_TYPE_INDIVIDUAL) || (invType == Meta.INVESTER_TYPE_INSTITUTIONAL)) {
            if (period == 7)
                return processedValues.get(invType)[0][0] * 100 / (processedValues.get(Meta.INVESTER_TYPE_INDIVIDUAL)[0][0] + processedValues.get(Meta.INVESTER_TYPE_INSTITUTIONAL)[0][0]);
            else if (period == 30)
                return processedValues.get(invType)[0][1] * 100 / (processedValues.get(Meta.INVESTER_TYPE_INDIVIDUAL)[0][1] + processedValues.get(Meta.INVESTER_TYPE_INSTITUTIONAL)[0][1]);
            else if (period == 90)
                return processedValues.get(invType)[0][2] * 100 / (processedValues.get(Meta.INVESTER_TYPE_INDIVIDUAL)[0][2] + processedValues.get(Meta.INVESTER_TYPE_INSTITUTIONAL)[0][2]);
            else if (period == 52)
                return processedValues.get(invType)[0][3] * 100 / (processedValues.get(Meta.INVESTER_TYPE_INDIVIDUAL)[0][3] + processedValues.get(Meta.INVESTER_TYPE_INSTITUTIONAL)[0][3]);
        }
        return 0;
    }

    public double getSellPercentageH(Byte invType, int period) {
        if ((invType == Meta.INVESTER_TYPE_DOMESTIC) || (invType == Meta.INVESTER_TYPE_ARAB) || (invType == Meta.INVESTER_TYPE_FOREIGN)) {
            if (period == 7)
                return processedValues.get(invType)[1][0] * 100 / (processedValues.get(Meta.INVESTER_TYPE_DOMESTIC)[1][0] + processedValues.get(Meta.INVESTER_TYPE_ARAB)[1][0] + processedValues.get(Meta.INVESTER_TYPE_FOREIGN)[1][0]);
            else if (period == 30)
                return processedValues.get(invType)[1][1] * 100 / (processedValues.get(Meta.INVESTER_TYPE_DOMESTIC)[1][1] + processedValues.get(Meta.INVESTER_TYPE_ARAB)[1][1] + processedValues.get(Meta.INVESTER_TYPE_FOREIGN)[1][1]);
            else if (period == 90)
                return processedValues.get(invType)[1][2] * 100 / (processedValues.get(Meta.INVESTER_TYPE_DOMESTIC)[1][2] + processedValues.get(Meta.INVESTER_TYPE_ARAB)[1][2] + processedValues.get(Meta.INVESTER_TYPE_FOREIGN)[1][2]);
            else if (period == 52)
                return processedValues.get(invType)[1][3] * 100 / (processedValues.get(Meta.INVESTER_TYPE_DOMESTIC)[1][3] + processedValues.get(Meta.INVESTER_TYPE_ARAB)[1][3] + processedValues.get(Meta.INVESTER_TYPE_FOREIGN)[1][3]);
        } else if ((invType == Meta.INVESTER_TYPE_INDIVIDUAL) || (invType == Meta.INVESTER_TYPE_INSTITUTIONAL)) {
            if (period == 7)
                return processedValues.get(invType)[1][0] * 100 / (processedValues.get(Meta.INVESTER_TYPE_INDIVIDUAL)[1][0] + processedValues.get(Meta.INVESTER_TYPE_INSTITUTIONAL)[1][0]);
            else if (period == 30)
                return processedValues.get(invType)[1][1] * 100 / (processedValues.get(Meta.INVESTER_TYPE_INDIVIDUAL)[1][1] + processedValues.get(Meta.INVESTER_TYPE_INSTITUTIONAL)[1][1]);
            else if (period == 90)
                return processedValues.get(invType)[1][2] * 100 / (processedValues.get(Meta.INVESTER_TYPE_INDIVIDUAL)[1][2] + processedValues.get(Meta.INVESTER_TYPE_INSTITUTIONAL)[1][2]);
            else if (period == 52)
                return processedValues.get(invType)[1][3] * 100 / (processedValues.get(Meta.INVESTER_TYPE_INDIVIDUAL)[1][3] + processedValues.get(Meta.INVESTER_TYPE_INSTITUTIONAL)[1][3]);
        }
        return 0;
    }

    public byte getType() {
        return investorType;
    }

    public String getExchange() {
        return exchange;
    }

    public String getKey() {
        return exchange + KEY_SEPERATOR + investorType;
    }

    public void setValues(String[] data) {
        if (data == null)
            return;

        char tag;

        try {
            for (int i = 0; i < data.length; i++) {
                tag = data[i].charAt(0);
                switch (tag) {
                    case 'A':
                        investorType = Byte.parseByte(data[i].substring(1, data[i].length()));
                        break;
                    case 'B':
                        buyTurnOver = SharedMethods.getDouble(data[i].substring(1, data[i].length()));
                        break;
                    case 'C':
                        sellTurnOver = SharedMethods.getDouble(data[i].substring(1, data[i].length()));
                        break;
                    case 'D':
                        netValue = SharedMethods.getDouble(data[i].substring(1, data[i].length()));
                        break;
                    case 'E':
                        buyPercentage = SharedMethods.getDouble(data[i].substring(1, data[i].length()));
                        break;
                    case 'F':
                        sellPercentage = SharedMethods.getDouble(data[i].substring(1, data[i].length()));
                        break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loadHistoryData() {
        while (InvestorTypeHistoryDownloader.isDownloading) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        int i = 0;
        for (ArrayList list : investorObjList) {
            if (list.size() == 0)
                i++;
        }
        if (i == 0) {
            return;
        }


        for (ArrayList list : investorObjList) {
            list.clear();
        }
        for (Byte invType : invList) {
            String fPath = mktDateHome + invType + ".csv";
            String data = "";
            try {
                BufferedReader br = new BufferedReader(new FileReader(new File(fPath)));
                while (true) {
                    data = br.readLine();
                    if (data == null || data == "" || data.isEmpty()) {
                        break;
                    } else {
                        if (invType == Meta.INVESTER_TYPE_DOMESTIC)
                            historyInfo_110.add(new InvestorHistoryInfo(invType, data));
                        else if (invType == Meta.INVESTER_TYPE_FOREIGN)
                            historyInfo_111.add(new InvestorHistoryInfo(invType, data));
                        else if (invType == Meta.INVESTER_TYPE_ARAB)
                            historyInfo_112.add(new InvestorHistoryInfo(invType, data));
                        else if (invType == Meta.INVESTER_TYPE_INSTITUTIONAL)
                            historyInfo_113.add(new InvestorHistoryInfo(invType, data));
                        else if (invType == Meta.INVESTER_TYPE_INDIVIDUAL)
                            historyInfo_114.add(new InvestorHistoryInfo(invType, data));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        calcHistoryValues();
    }

    private void calcHistoryValues() {
        long margin7D;
        long margin30D;
        long margin90D;
        long margin52W;

        long today;
        InvestorHistoryInfo temp = null;
        double buyTurnoverTemp = 0;
        double sellTurnoverTemp = 0;
        double netValueTemp = 0;
        double buyPercentageTemp = 0;
        double sellPercentageTemp = 0;
        double totalPercentageTemp = 0;
        double[][] calcVal;
        Calendar c = Calendar.getInstance();
        today = Long.parseLong(format.format(c.getTime()));

        c.add(Calendar.DAY_OF_MONTH, -7);
        margin7D = Long.parseLong(format.format(c.getTime()));
        c.add(Calendar.DAY_OF_MONTH, 7 - 30);
        margin30D = Long.parseLong(format.format(c.getTime()));
        c.add(Calendar.DAY_OF_MONTH, 30 - 90);
        margin90D = Long.parseLong(format.format(c.getTime()));
        c.add(Calendar.DAY_OF_MONTH, 90 - 365);
        margin52W = Long.parseLong(format.format(c.getTime()));
        ArrayList curList;
        for (ArrayList list : investorObjList) {
            noOfDaysIn7D = noOfDaysIn30D = noOfDaysIn90D = noOfDaysIn52W = 0;
            curList = list;
            calcVal = new double[6][4];
            for (int i = list.size() - 1; i >= 0; i--) {
                temp = (InvestorHistoryInfo) list.get(i);
                buyTurnoverTemp = temp.getBuyTurnover();
                sellTurnoverTemp = temp.getSellTurnover();
                netValueTemp = temp.getNetValue();
                buyPercentageTemp = temp.getBuyPercentage();
                sellPercentageTemp = temp.getSellPercentage();
                totalPercentageTemp = temp.getTotalPercentage();

                if (temp.getTransactionDate() >= margin52W) {
                    buyTurnOver52 += buyTurnoverTemp;
                    sellTurnOver52 += sellTurnoverTemp;
                    netValue52 += netValueTemp;
                    buyPercentage52 += buyPercentageTemp;
                    sellPercentage52 += sellPercentageTemp;
                    noOfDaysIn52W++;
                    if (temp.getTransactionDate() >= margin90D) {
                        buyTurnOver90 += buyTurnoverTemp;
                        sellTurnOver90 += sellTurnoverTemp;
                        netValue90 += netValueTemp;
                        buyPercentage90 += buyPercentageTemp;
                        sellPercentage90 += sellPercentageTemp;
                        noOfDaysIn90D++;
                        if (temp.getTransactionDate() >= margin30D) {
                            buyTurnOver30 += buyTurnoverTemp;
                            sellTurnOver30 += sellTurnoverTemp;
                            System.out.println("Testing for net Value30 : NetTotal: " + netValue30 + "  NrtCurrent: " + netValueTemp);
                            netValue30 += netValueTemp;
                            buyPercentage30 += buyPercentageTemp;
                            sellPercentage30 += sellPercentageTemp;
                            noOfDaysIn30D++;
                            if (temp.getTransactionDate() >= margin7D) {
                                buyTurnOver7 += buyTurnoverTemp;
                                sellTurnOver7 += sellTurnoverTemp;
                                netValue7 += netValueTemp;
                                buyPercentage7 += buyPercentageTemp;
                                sellPercentage7 += sellPercentageTemp;
                                noOfDaysIn7D++;
                            }
                        }
                    }
                } else {
                    break;
                }

            }

            calcVal[0][0] = buyTurnOver7;
            calcVal[0][1] = buyTurnOver30;
            calcVal[0][2] = buyTurnOver90;
            calcVal[0][3] = buyTurnOver52;

            calcVal[1][0] = sellTurnOver7;
            calcVal[1][1] = sellTurnOver30;
            calcVal[1][2] = sellTurnOver90;
            calcVal[1][3] = sellTurnOver52;

            calcVal[2][0] = netValue7;
            calcVal[2][1] = netValue30;
            calcVal[2][2] = netValue90;
            calcVal[2][3] = netValue52;

            calcVal[3][0] = buyPercentage7;
            calcVal[3][1] = buyPercentage30;
            calcVal[3][2] = buyPercentage90;
            calcVal[3][3] = buyPercentage52;

            calcVal[4][0] = sellPercentage7;
            calcVal[4][1] = sellPercentage30;
            calcVal[4][2] = sellPercentage90;
            calcVal[4][3] = sellPercentage52;

            processedValues.put(temp.getInvestorType(), calcVal);

            /*buyTurnOver = 0;
            sellTurnOver = 0;
            netValue = 0;
            buyPercentage = 0;
            sellPercentage = 0;*/

            buyTurnOver7 = 0;
            sellTurnOver7 = 0;
            netValue7 = 0;
            buyPercentage7 = 0;
            sellPercentage7 = 0;

            buyTurnOver30 = 0;
            sellTurnOver30 = 0;
            netValue30 = 0;
            buyPercentage30 = 0;
            sellPercentage30 = 0;

            buyTurnOver90 = 0;
            sellTurnOver90 = 0;
            netValue90 = 0;
            buyPercentage90 = 0;
            sellPercentage90 = 0;

            buyTurnOver52 = 0;
            sellTurnOver52 = 0;
            netValue52 = 0;
            buyPercentage52 = 0;
            sellPercentage52 = 0;
        }
    }

    public void downloadSummaryFile(String exchange) {
        if (Settings.isConnected()) {
            if (!checkIfSummaryFileExsists(exchange)) {  // to stop infinite DL loop if .csv is incompatible
                InvestorTypeHistoryDownloadManager.getSharedInstance().addInvestorTypeHistoryRequest(exchange);
            }
        }
    }

    public void deactivate() {
        //CashFlowHistory.getSharedInstance().stopSerchIconOnThreadInturrupt();
        try {
            active = false;
            downloader.interrupt();
            downloader = null;
        } catch (Exception e) {

        }
    }

    private void setMktDateHome(String exchange) {
        Exchange exg = ExchangeStore.getSharedInstance().getExchange(exchange);
        long time = exg.getMarketDate();
        String foldername = format.format(new Date(time));
        mktDateHome = Settings.getAbsolutepath() + "DataStore\\InvestorTypes\\" + exchange + "\\" + foldername + "\\";
    }


    private void clearDataFolderOnStart() {
        String path = Settings.getAbsolutepath() + "DataStore\\InvestorTypes\\" + exchange + "\\";
        File file = new File(path);
        File temp;
        if (file.isDirectory()) {
            for (String s : file.list()) {
                if (!mktDateHome.equals(path + s + "\\")) {
                    temp = new File(path + s + "\\");
                    deleteTree(new File(path + s + "\\"));
                }
            }

        }

    }

    void deleteTree(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                deleteTree(new File(dir, children[i]));

            }
        }
        dir.delete();
    }
}
