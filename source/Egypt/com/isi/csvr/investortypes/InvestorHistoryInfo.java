package com.isi.csvr.investortypes;

import com.isi.csvr.shared.Meta;

/**
 * Created by IntelliJ IDEA.
 * User: madhawies
 * Date: 6/29/11
 * Time: 3:38 PM
 * To change this template use File | Settings | File Templates.
 */

public class InvestorHistoryInfo {
    private long transactionDate = 0;
    private double buyTurnOver = 0;
    private double sellTurnOver = 0;
    private double netValue = 0;
    private double buyPercentage = 0;
    private double sellPercentage = 0;
    private double totalPercentage = 0;
    private Byte invType = 0;

    public InvestorHistoryInfo(Byte invType, String data) {
        this.invType = invType;
        this.transactionDate = Long.parseLong(data.split(Meta.FS)[0]);
        this.buyTurnOver = Double.parseDouble(data.split(Meta.FS)[1]);
        this.sellTurnOver = Double.parseDouble(data.split(Meta.FS)[2]);
        this.netValue = Double.parseDouble(data.split(Meta.FS)[3]);
        this.buyPercentage = Double.parseDouble(data.split(Meta.FS)[4]);
        this.sellPercentage = Double.parseDouble(data.split(Meta.FS)[5]);
        this.totalPercentage = Double.parseDouble(data.split(Meta.FS)[6]);
    }

    public Byte getInvestorType() {
        return this.invType;
    }

    public long getTransactionDate() {
        return this.transactionDate;
    }

    public double getBuyTurnover() {
        return this.buyTurnOver;
    }

    public double getSellTurnover() {
        return this.sellTurnOver;
    }

    public double getBuyPercentage() {
        return this.buyPercentage;
    }

    public double getSellPercentage() {
        return this.sellPercentage;
    }

    public double getNetValue() {
        return this.netValue;
    }

    public double getTotalPercentage() {
        return this.totalPercentage;
    }
}