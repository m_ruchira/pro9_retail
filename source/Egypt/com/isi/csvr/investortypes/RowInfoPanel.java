package com.isi.csvr.investortypes;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 14, 2009
 * Time: 10:27:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class RowInfoPanel extends JPanel implements Themeable {

    protected static final byte PANEL_DOMESTIC = 0;
    protected static final byte PANEL_ARAB = 1;
    protected static final byte PANEL_FOREIGN = 2;
    protected static final byte PANEL_INDIVIDUAL = 3;
    protected static final byte PANEL_INSTITUTIONAL = 4;
    private JLabel lblBuy;
    private JLabel lblBuyPct;
    private JLabel lblSell;
    private JLabel lblSellPct;
    private JLabel lblNetVal;
    private TWDecimalFormat qtFormatter;
    private TWDecimalFormat pctFormatter;
    private byte panelID;
    private Color fillColor;

    public RowInfoPanel(byte panelID, Color color) {

        this.panelID = panelID;
        this.fillColor = color;

        lblBuy = new JLabel("-", JLabel.RIGHT);
        lblBuyPct = new JLabel("-", JLabel.RIGHT);
        lblSell = new JLabel("-", JLabel.RIGHT);
        lblSellPct = new JLabel("-", JLabel.RIGHT);
        lblNetVal = new JLabel("-", JLabel.RIGHT);

        /*Font f = new Font("Arial",Font.BOLD, 12);
        lblBuy.setFont(f);
        lblBuyPct.setFont(f);
        lblSell.setFont(f);
        lblSellPct.setFont(f);
        lblNetVal.setFont(f);*/

        setLabelFontColors();
        qtFormatter = new TWDecimalFormat("###,###,###,###");
        pctFormatter = new TWDecimalFormat("#0.00");

        this.setBackground(fillColor);

        this.setLayout(new FlexGridLayout(new String[]{"14%", "22%", "10%", "22%", "10%", "22%"}, new String[]{"100%"}));
        this.add(new CustomPanel(panelID));
        this.add(lblBuy);
        this.add(lblBuyPct);
        this.add(lblSell);
        this.add(lblSellPct);
        this.add(lblNetVal);

        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
    }

    public void setValues(double buy, double buyPct, double sell, double sellPct, double netValue) {
        lblBuy.setText(qtFormatter.format(buy));
        lblSell.setText(qtFormatter.format(sell));
        lblNetVal.setText(qtFormatter.format(netValue));

        if (!Double.isNaN(buyPct)) {
            lblBuyPct.setText(pctFormatter.format(buyPct) + "%");
        } else {
            lblBuyPct.setText("N/A");
        }

        if (!Double.isNaN(sellPct)) {
            lblSellPct.setText(pctFormatter.format(sellPct) + "%");
        } else {
            lblSellPct.setText("N/A");
        }
    }

    public void applyTheme() {
        //TODO: need to complete
        setLabelFontColors();
    }

    private void setLabelFontColors() {

        lblBuy.setForeground(Theme.getColor("INV_TABLE_CELL_FONT_COLOR"));
        lblBuyPct.setForeground(Theme.getColor("INV_TABLE_CELL_FONT_COLOR"));
        lblSell.setForeground(Theme.getColor("INV_TABLE_CELL_FONT_COLOR"));
        lblSellPct.setForeground(Theme.getColor("INV_TABLE_CELL_FONT_COLOR"));
        lblNetVal.setForeground(Theme.getColor("INV_TABLE_CELL_FONT_COLOR"));
    }

    private class CustomPanel extends JPanel implements Themeable {

        private final int SQUARE_HEIGHT = 12;
        private final int SQUARE_WIDTH = 12;
        private final int X_START = 6;
        private final int Y_START = (25 - SQUARE_HEIGHT) / 2;
        private String text = "";
        private byte panelID;

        private Color fillColor;

        public CustomPanel(byte panelid) {
            super();
            this.panelID = panelid;
            Theme.registerComponent(this);

            if (panelID == RowInfoPanel.PANEL_ARAB) {
                fillColor = Theme.getColor("INV_BOTTOM_COLOR");
                text = Language.getString("INV_ARAB");
            } else if (panelID == RowInfoPanel.PANEL_DOMESTIC) {
                fillColor = Theme.getColor("INV_BOTTOM_COLOR");
                text = Language.getString("INV_DOMESTIC");
            } else if (panelID == RowInfoPanel.PANEL_FOREIGN) {
                fillColor = Theme.getColor("INV_BOTTOM_COLOR");
                text = Language.getString("INV_FOREIGN");
            } else if (panelID == RowInfoPanel.PANEL_INDIVIDUAL) {
                fillColor = Theme.getColor("INV_TOP_COLOR");
                text = Language.getString("INV_INDIVIDUALS");
            } else if (panelID == RowInfoPanel.PANEL_INSTITUTIONAL) {
                fillColor = Theme.getColor("INV_TOP_COLOR");
                text = Language.getString("INV_INSTITUTION");
            }
        }

        public void paint(Graphics g) {
            super.paint(g);

            this.setBackground(fillColor);
            g.setColor(Color.BLACK);

            if (Language.isLTR()) {
                g.drawRect(X_START, Y_START, SQUARE_WIDTH, SQUARE_HEIGHT);
            } else {
                g.drawRect(this.getWidth() - X_START - SQUARE_WIDTH, Y_START, SQUARE_WIDTH, SQUARE_HEIGHT);
            }
            if (panelID == RowInfoPanel.PANEL_ARAB) {
                g.setColor(InvestorTypeWindow.getSharedInstance().getArabicColor());
            } else if (panelID == RowInfoPanel.PANEL_DOMESTIC) {
                g.setColor(InvestorTypeWindow.getSharedInstance().getDomesticColor());
            } else if (panelID == RowInfoPanel.PANEL_FOREIGN) {
                g.setColor(InvestorTypeWindow.getSharedInstance().getForeignColor());
            } else if (panelID == RowInfoPanel.PANEL_INDIVIDUAL) {
                g.setColor(InvestorTypeWindow.getSharedInstance().getIndividualColor());
            } else if (panelID == RowInfoPanel.PANEL_INSTITUTIONAL) {
                g.setColor(InvestorTypeWindow.getSharedInstance().getInstitutionalColor());
            }
            g.setFont(new Font("Arial", Font.BOLD, 12));


            if (Language.isLTR()) {
                g.fillRect(X_START, Y_START, SQUARE_WIDTH, SQUARE_HEIGHT);
                g.drawString(text, 2 * X_START + SQUARE_HEIGHT, Y_START + SQUARE_HEIGHT / 2 + 4);
            } else {
                g.fillRect(this.getWidth() - X_START - SQUARE_WIDTH, Y_START, SQUARE_WIDTH, SQUARE_HEIGHT);
                int length = g.getFontMetrics().stringWidth(text);
                g.drawString(text, this.getWidth() - X_START - SQUARE_WIDTH - length - 4, Y_START + SQUARE_HEIGHT / 2 + 4);
            }
        }


        public void applyTheme() {
            if (panelID == RowInfoPanel.PANEL_ARAB || panelID == RowInfoPanel.PANEL_DOMESTIC || panelID == RowInfoPanel.PANEL_FOREIGN) {
                fillColor = Theme.getColor("INV_BOTTOM_COLOR");
            } else if (panelID == RowInfoPanel.PANEL_INDIVIDUAL || panelID == RowInfoPanel.PANEL_INSTITUTIONAL) {
                fillColor = Theme.getColor("INV_TOP_COLOR");
            }
            this.setBackground(fillColor);
        }
    }
}
