package com.isi.csvr.investortypes;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Apr 3, 2009
 * Time: 12:34:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class HeaderPanel extends JPanel implements Themeable {

    private JLabel lblHeader1;
    private JLabel lblHeader2;
    private JLabel lblHeader3;
    private JLabel lblHeader4;
    private JLabel lblHeader5;
    private JLabel lblHeader6;

    //private Color labelFontColor = Theme.getColor("INV_TABLE_HEADER_FONT_COLOR");
    private Color labelFontColor = Color.BLACK;
    private Font labelFont = new Font("Arial", Font.BOLD, 12);

    public HeaderPanel() {
        createUI();
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
    }

    private void createUI() {
        //this.setLayout(new FlexGridLayout(new String[]{"20%", "20%", "10%", "20%", "10%", "20%"}, new String[]{"100%"}));
        this.setLayout(new FlexGridLayout(new String[]{"14%", "22%", "10%", "22%", "10%", "22%"}, new String[]{"100%"}));
        this.setBackground(Theme.getColor("INV_HEADER_COLOR"));
        lblHeader1 = new JLabel(Language.getString("INV_TYPE"), JLabel.CENTER);
        lblHeader2 = new JLabel(Language.getString("INV_BUYING"), JLabel.RIGHT);
        lblHeader3 = new JLabel(Language.getString("INV_BUYING_PCT"), JLabel.RIGHT);
        lblHeader4 = new JLabel(Language.getString("INV_SELLING"), JLabel.RIGHT);
        lblHeader5 = new JLabel(Language.getString("INV_SELLING_PCT"), JLabel.RIGHT);
        lblHeader6 = new JLabel(Language.getString("INV_NET_VALUE"), JLabel.RIGHT);

        labelFontColor = Theme.getColor("INV_TABLE_HEADER_FONT_COLOR");
        setTableCellLabelProperties(lblHeader1, labelFont, labelFontColor);
        setTableCellLabelProperties(lblHeader2, labelFont, labelFontColor);
        setTableCellLabelProperties(lblHeader3, labelFont, labelFontColor);
        setTableCellLabelProperties(lblHeader4, labelFont, labelFontColor);
        setTableCellLabelProperties(lblHeader5, labelFont, labelFontColor);
        setTableCellLabelProperties(lblHeader6, labelFont, labelFontColor);

        this.add(lblHeader1);
        this.add(lblHeader2);
        this.add(lblHeader3);
        this.add(lblHeader4);
        this.add(lblHeader5);
        this.add(lblHeader6);
    }

    public void applyTheme() {

        this.setBackground(Theme.getColor("INV_HEADER_COLOR"));

        labelFontColor = Theme.getColor("INV_TABLE_HEADER_FONT_COLOR");
        setHeaderlLabelProperties(lblHeader1, labelFontColor);
        setHeaderlLabelProperties(lblHeader2, labelFontColor);
        setHeaderlLabelProperties(lblHeader3, labelFontColor);
        setHeaderlLabelProperties(lblHeader4, labelFontColor);
        setHeaderlLabelProperties(lblHeader5, labelFontColor);
        setHeaderlLabelProperties(lblHeader6, labelFontColor);
    }

    private void setHeaderlLabelProperties(JLabel label, Color fontColor) {
        label.setForeground(fontColor);
    }

    private void setTableCellLabelProperties(JLabel label, Font labelFont, Color fontColor) {

        label.setFont(labelFont);
        label.setForeground(fontColor);
    }
}
