package com.isi.csvr.chrriis.dj.swingsuite;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.NumberFormat;


/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Nov 23, 2009
 * Time: 3:08:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class FormattedTextField {

    private JNumberEntryField<Double> createDoubleFieldWithDecimalsAndCustomFormatter() {
        final JNumberEntryField<Double> formattedField = new JNumberEntryField<Double>(1000000000.0d, 14, 2);
        formattedField.setFormatter(new TextEntryFormatter() {
            public String getTextForDisplay(JTextEntryField textEntryField, String validText) {
                return NumberFormat.getNumberInstance().format(((JNumberEntryField<?>) textEntryField).getNumber());
            }
        });
        formattedField.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                try {
                    System.out.println("222VALUE in Number ==" + formattedField.getNumber());
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                try {
                    System.out.println("222VALUE in formatter ==" + NumberFormat.getNumberInstance().format(formattedField.getNumber()));
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return formattedField;
    }
}
