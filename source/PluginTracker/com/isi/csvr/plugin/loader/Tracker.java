package com.isi.csvr.plugin.loader;

import java.util.Hashtable;


/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Mar 20, 2008
 * Time: 4:23:28 PM
 */
public class Tracker {
    private static Hashtable<String, Class> pluginDef;

    static {
        pluginDef = new Hashtable<String, Class>();

		/*
                 * Define your plugins here. following defined classes will be activated once required
				 *
				 * format:
				 *      pluginDef.put("MY_PLUGIN_ID",com.my.package.MyPlugin.class);
				 */
//		pluginDef.put("INVESTOR_TYPES", com.isi.csvr.investortypes.InvestorTypesPlugin.class);
        pluginDef.put("MEDIA_PLAYER", com.mubasher.mediaplayer.MediaPlayerPlugin.class);
        pluginDef.put("OPTION_CALCULATOR", com.mubasher.csvr.optionscalculator.OptionCalculatorPlugin.class);
        pluginDef.put("FAIRVALUE_CALCULATOR", com.mubasher.csvr.fairvaluecalculator.FairValueCalculatorPlugin.class);
        pluginDef.put("DERIVATIVE_PREFERENCES", com.isi.csvr.derivativepreference.DerivativePreferencePlugin.class);
        pluginDef.put("STRATEGY_OPTION_STAT_MAKER", com.isi.csvr.optionstrategymaker.OptionStrategyMakerPlugin.class);
        pluginDef.put("DETAILED_MARKET_SUMMARY", com.isi.csvr.detailedmarketsummary.DetailedMarketSummaryPlugin.class);
//		pluginDef.put("STRATEGY_OPTION_STAT_MAKER", com.isi.csvr.optionstrategymaker.OptionStrategyMakerPlugin.class);
//		pluginDef.put("BHAV_COPY", com.isi.csvr.india.bhavcopy.PluginLoaderBhavCopy.class);
    }

    public static Class get(Object key) {
        return pluginDef.get(key);
    }
}
