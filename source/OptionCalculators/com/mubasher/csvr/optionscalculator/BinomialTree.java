package com.mubasher.csvr.optionscalculator;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 3, 2008
 * Time: 7:04:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class BinomialTree {

    private ArrayList<TreeNode> nodes;

    public BinomialTree() {
        nodes = new ArrayList<TreeNode>();
    }

    public void resetNodes() {
        nodes = null;
        nodes = new ArrayList<TreeNode>();
    }

    public TreeNode CreateNode(int step, int verticalRank, double optionUp, double optionDown, double stockPrice, double deltaT) {
        TreeNode node = new TreeNode(step, verticalRank);
        node.stockPrice_S = stockPrice * Math.pow(optionUp, verticalRank) * Math.pow(optionDown, (step - verticalRank));
        nodes.add(node);
        return node;
    }

    public TreeNode GetNode(int step, int verticalRank) {
        TreeNode reqNode = null;
        TreeNode tempNode = null;
        for (int i = 0; i < nodes.size(); i++) {
            tempNode = nodes.get(i);

            if (tempNode.step == step && tempNode.verticalRank == verticalRank) {
                reqNode = tempNode;
                break;
            }
        }
        return reqNode;
    }
}
