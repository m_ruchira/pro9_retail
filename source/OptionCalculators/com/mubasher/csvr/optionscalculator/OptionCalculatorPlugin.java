/*
 * (C) Copyright 2009-2010 Direct FN Technologies Limited. All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * Direct FN Technologies and constitute a TRADE SECRET of Direct FN Technologies Limited.
 *
 * Direct FN Technologies Limited retains all title to and intellectual property rights
 * in these materials.
 */
package com.mubasher.csvr.optionscalculator;

import com.isi.csvr.plugin.Plugin;
import com.isi.csvr.plugin.event.PluginEvent;
import com.isi.csvr.shared.Meta;

/**
 * com.mubasher.csvr.optionscalculator.OptionCalculatorPlugin
 */
public class OptionCalculatorPlugin implements Plugin {
    public void load(Object... params) {
    }

    public void showUI(Object... params) {
        OptionCalculaterIndiaUI.getSharedInstance().clearAllForCalculation();
        OptionCalculaterIndiaUI.getSharedInstance().setVisible(!OptionCalculaterIndiaUI.getSharedInstance().isVisible());
    }

    public void save(Object... params) {
    }

    public boolean isLoaded() {
        return false;
    }

    public String getCaption() {
        return "Option Calculator";
    }

    public void unload(Object... params) {
    }

    public void pluginEvent(PluginEvent event, Object... data) {
    }

    public boolean isDataNeeded() {
        return false;
    }

    public void setData(int id, Object... params) {
    }

    public void applyWorkspace(String id, String value) {
    }

    public String getWorkspaceString(String id) {
        return null;
    }

    public String getViewSettingID() {
        return null;
    }

    public int getWindowTypeID() {
        return -1;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getWindowTypeCategory() {
        return Meta.WINDOW_TYPE_CATEGORY_INVALID;
    }
}
