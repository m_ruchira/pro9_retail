package com.mubasher.csvr.optionscalculator;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWRadioButton;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: hasika
 * Date: Jan 8, 2009
 * Time: 1:32:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptionCalculaterIndiaUI extends InternalFrame implements ItemListener, ActionListener {
    private static OptionCalculaterIndiaUI self;
    public static String COX_ROSS = "COX_ROSS";
    public static String BLACK_SCHOLES = "BLACK_SCHOLES";

    private JLabel lblOptionType;
    private JLabel lblSpotPrice;
    private JLabel lblStrikePrice;
    private JLabel lblInterestRates;
    private JLabel lblDividendYield;
    private JLabel lblDateUntExpiary;
    private JLabel lblNoOfSteps;
    private JLabel lblCalculationMtd;


    private TWComboBox cmbOptionType;
    private TWComboBox cmbCalculationMtd;
    private TWTextField txtSpotPrice;
    private TWTextField txtStrikePrice;
    private TWTextField txtInterestRates;
    private TWTextField txtDividendYield;
    private TWTextField txtDateUntExpiary;
    private TWTextField txtNoOfSteps;


    private TWTextField txtPremium;
    private TWTextField txtVolatility;

    private TWRadioButton rbPremium;
    private TWRadioButton rbVolatility;

    private ButtonGroup btnGroup;

    private TWButton btnCalulate;


    private JLabel lblDelta;
    private JLabel lblGamma;
    private JLabel lblTheta;
    private JLabel lblVega;
    private JLabel lblRow;
    private JLabel lblDailyVol;
    private JLabel lblVolChange;
    private JLabel lblBreakEvPrice;
    private JLabel lblIntrinsic;
    private JLabel lblTimeVal;

    private TWTextField txtDelta;
    private TWTextField txtGama;
    private TWTextField txtTheta;
    private TWTextField txtVaga;
    private TWTextField txtRow;
    private TWTextField txtDailyVol;
    private TWTextField txtVolChange;
    private TWTextField txtBreakEvPrice;
    private TWTextField txtIntrinsic;
    private TWTextField txtTimeVal;

    private int heigth = 400;
    private int width = 530;
    private TWDecimalFormat formater = new TWDecimalFormat("#0.00000");

    private ArrayList<Double> results;
    private double intrensic = 0.00d;
    private double timeval = 0.00d;
    private double breakeven = 0.00d;
    private BlackAndScholes blackCalc;


    private OptionCalculaterIndiaUI() {
        blackCalc = new BlackAndScholes();
        results = new ArrayList<Double>(6);

        lblOptionType = new JLabel(Language.getString("OPTION_TYPE"));
        lblSpotPrice = new JLabel(Language.getString("SPOT_PRICE"));
        lblStrikePrice = new JLabel(Language.getString("STRIKE_PRICE"));
        lblInterestRates = new JLabel(Language.getString("INTEREST_RATES"));
        lblDividendYield = new JLabel(Language.getString("DIVIDEND_YIELD"));
        lblDateUntExpiary = new JLabel(Language.getString("EXPIRAY_DATE"));
        lblNoOfSteps = new JLabel(Language.getString("NO_OF_STEPS"));
        lblCalculationMtd = new JLabel(Language.getString("CALC_MTD"));

        cmbOptionType = new TWComboBox();
        cmbCalculationMtd = new TWComboBox();
        cmbCalculationMtd.addItemListener(this);
        cmbCalculationMtd.setPreferredSize(new Dimension(150, 25));

        txtSpotPrice = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL, 8, 2), "0", 1);
        txtStrikePrice = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL, 8, 2), "0", 1);

        txtInterestRates = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL, 8, 2), "0", 1);
        txtDividendYield = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL, 8, 2), "0", 1);
        txtDateUntExpiary = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL, 8, 2), "0", 1);
        txtNoOfSteps = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL, 8, 2), "0", 1);

        rbVolatility = new TWRadioButton(Language.getString("VOLATILITY"));
        rbPremium = new TWRadioButton(Language.getString("INSTRUMENT_PREMIUM"));
        btnGroup = new ButtonGroup();
        btnGroup.add(rbVolatility);
        btnGroup.add(rbPremium);
        rbVolatility.addItemListener(this);
        rbPremium.addItemListener(this);

        txtPremium = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL, 8, 5), "0", 1);
        txtVolatility = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL, 8, 5), "0", 1);

        btnCalulate = new TWButton(Language.getString("CALCULATE"));
//        btnCalulate.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                calculate();
//            }
//        });
        btnCalulate.addActionListener(this);
        btnCalulate.setActionCommand("O");

        lblDelta = new JLabel(Language.getString("DELATA"));
        lblGamma = new JLabel(Language.getString("GAMMA"));
        lblTheta = new JLabel(Language.getString("THETA"));
        lblVega = new JLabel(Language.getString("VEGA"));
        lblRow = new JLabel(Language.getString("RHO"));
        lblDailyVol = new JLabel(Language.getString("DAILY_VOL"));
        lblVolChange = new JLabel(Language.getString("VOL_CHANGE"));
        lblBreakEvPrice = new JLabel(Language.getString("BREAKEVEN_PRICE"));
        lblIntrinsic = new JLabel(Language.getString("INTRINSIC"));
        lblTimeVal = new JLabel(Language.getString("TIME_VALUE"));

        txtDelta = new TWTextField();
        txtGama = new TWTextField();
        txtTheta = new TWTextField();
        txtVaga = new TWTextField();
        txtRow = new TWTextField();
        txtDailyVol = new TWTextField();
        txtVolChange = new TWTextField();
        txtBreakEvPrice = new TWTextField();
        txtIntrinsic = new TWTextField();
        txtTimeVal = new TWTextField();

        txtDelta.setEditable(false);
        txtGama.setEditable(false);
        txtTheta.setEditable(false);
        txtVaga.setEditable(false);
        txtRow.setEditable(false);
        txtDailyVol.setEditable(false);
        txtVolChange.setEditable(false);
        txtBreakEvPrice.setEditable(false);
        txtIntrinsic.setEditable(false);
        txtTimeVal.setEditable(false);

        createUI();
    }

    public void calculate() {
        intrensic = 0.00;
        timeval = 0.00;
        breakeven = 0.00;
        if (getSelectedCalculationMethod().equals(BLACK_SCHOLES) && isInputValid()) {
            double spot = Double.parseDouble(txtSpotPrice.getText());
            double strike = Double.parseDouble(txtStrikePrice.getText());
            double volatility = 0.00d;
            try {
                volatility = Double.parseDouble(txtVolatility.getText());
            } catch (NumberFormatException e) {
                volatility = 0.00d;
            }
            double interest = Double.parseDouble(txtInterestRates.getText());
            double div = 0.00d;
            try {
                div = Double.parseDouble(txtDividendYield.getText());
            } catch (NumberFormatException e) {
                div = 0.00d;
                txtDividendYield.setText("0");
            }
            double prem = 0.00d;
            try {
                prem = Double.parseDouble(txtPremium.getText());
            } catch (NumberFormatException e) {
                prem = 0.00d;
            }
            int days = Integer.parseInt(txtDateUntExpiary.getText());
            boolean isImpVolEnabled = !rbVolatility.isSelected();

            blackCalc.calculateTheoritaclPrice(getSelectedOptionType(), spot, ((double) days / 365), strike, interest, volatility, div, isImpVolEnabled, prem);
            blackCalc.fillDataToList(results);
            if (isImpVolEnabled) {

                double impvol = results.get(6);
                //  double premium = results.get(0);
                blackCalc.initCalculator();
                blackCalc.calculateTheoritaclPrice(getSelectedOptionType(), spot, ((double) days / 365), strike, interest, impvol, div, false, prem);
                blackCalc.fillDataToList(results);
                results.set(0, prem);
                results.set(6, impvol);
            }
            if (getSelectedOptionType().equalsIgnoreCase("call")) {
                intrensic = Math.max((spot - strike), 0);

            } else {
                intrensic = Math.max((strike - spot), 0);
            }
            breakeven = spot;
            setValue(results);


        } else if (getSelectedCalculationMethod().equals(COX_ROSS) && isInputValid()) {

            double spot = Double.parseDouble(txtSpotPrice.getText());
            double strike = Double.parseDouble(txtStrikePrice.getText());
            double volatility = Double.parseDouble(txtVolatility.getText());
            double interest = Double.parseDouble(txtInterestRates.getText());
            double div = 0.00d;
            try {
                div = Double.parseDouble(txtDividendYield.getText());
            } catch (NumberFormatException e) {
                div = 0.00d;
                txtDividendYield.setText("0");
            }
            int days = Integer.parseInt(txtDateUntExpiary.getText());
            int steps = Integer.parseInt(txtNoOfSteps.getText());
            if (steps > 175) {
//                    steps = 175;
//                    txtNoOfSteps.setText("175");
                SharedMethods.showMessage(Language.getString("OPTION_CALC_MAX_NO_OF_STEPS"), JOptionPane.ERROR_MESSAGE);
                return;
                // new ShowMessage(Language.getString("OPTION_CALC_MAX_NO_OF_STEPS"), "I");
            }
            BinomialCalculator.EnumCallPut tempEnum = BinomialCalculator.EnumCallPut.Put;
            if (getSelectedOptionType().equalsIgnoreCase(Language.getString("OPT_CALL"))) {
                tempEnum = BinomialCalculator.EnumCallPut.Call;
            }
            BinomialCalculator calculator = new BinomialCalculator(spot, strike, ((double) days / 365), volatility / 100, interest / 100, div / 100, tempEnum, BinomialCalculator.EnumStyle.European, steps);
            calculator.GetOptionValue(results);
            if (getSelectedOptionType().equalsIgnoreCase("call")) {
                intrensic = Math.max((spot - strike), 0);

            } else {
                intrensic = Math.max((strike - spot), 0);
            }
            breakeven = spot;
            setValue(results);


        }
    }

    private void setValue(ArrayList<Double> results) {
        if (rbVolatility.isSelected()) {
            txtPremium.setText(formater.format(results.get(0).doubleValue()));
            timeval = results.get(0) - intrensic;
            breakeven = breakeven + results.get(0);
            //  txtPremium.setText("test");
        } else {
            txtVolatility.setText(formater.format(results.get(6)));
            timeval = Double.parseDouble(txtPremium.getText()) - intrensic;
            breakeven += Double.parseDouble(txtPremium.getText());
        }

        if (results.get(1).compareTo(Double.NaN) != 0) {
            txtDelta.setText(formater.format(results.get(1)));
        } else {
            txtDelta.setText(Language.getString("NA"));
        }
        if (results.get(2).compareTo(Double.NaN) != 0) {
            txtGama.setText(formater.format(results.get(2)));
        } else {
            txtGama.setText(Language.getString("NA"));
        }
        if (results.get(3).compareTo(Double.NaN) != 0) {
            txtTheta.setText(formater.format(results.get(3)));
        } else {
            txtTheta.setText(Language.getString("NA"));
        }
        if (results.get(4).compareTo(Double.NaN) != 0) {
            txtVaga.setText(formater.format(results.get(4)));
        } else {
            txtVaga.setText(Language.getString("NA"));
        }
        if (results.get(5).compareTo(Double.NaN) != 0) {
            txtRow.setText(formater.format(results.get(5)));
        } else {
            txtRow.setText(Language.getString("NA"));
        }
        txtDailyVol.setText(formater.format(results.get(5)));
        txtVolChange.setText(results.get(0).toString());
        txtBreakEvPrice.setText(formater.format(breakeven));
        txtIntrinsic.setText(formater.format(intrensic));
        txtTimeVal.setText(formater.format(timeval));
        //if (getSelectedCalculationMethod().equals(COX_ROSS) || (getSelectedCalculationMethod().equals(BLACK_SCHOLES)&& !rbVolatility.isSelected())) {
        if (getSelectedCalculationMethod().equals(COX_ROSS)) {
            clearGreeks();
        }
    }

    private void clearGreeks() {
        txtDelta.setText("");
        txtGama.setText("");
        txtTheta.setText("");
        txtVaga.setText("");
        txtRow.setText("");
    }

    public static OptionCalculaterIndiaUI getSharedInstance() {
        if (self == null) {
            self = new OptionCalculaterIndiaUI();
        }
        return self;
    }

    private void createUI() {
        JPanel mainPan = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"142", "40", "150"}, 10, 10, true, true));
        mainPan.add(createNorthPan());
        mainPan.add(createCenterPan());
        mainPan.add(createSouthPan());

        populateComboBoxes();

        this.add(mainPan);
        this.setTitle(Language.getString("OPTION_CALCULATOR"));
        this.setSize(new Dimension(width, heigth));
        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        this.setResizable(false);
        this.setClosable(true);
        this.setLayer(GUISettings.TOP_LAYER);
        this.hideTitleBarMenu();
        Client.getInstance().getDesktop().add(this);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        GUISettings.applyOrientation(this);
        this.setFocusTraversalPolicy(new OptionCalculatorForcusTravesalPolicy());
        ((JPanel) (this.getContentPane())).registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);

    }

    private JPanel createSouthPan() {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"25%", "25%", "5", "25%", "25%"}, new String[]{"20", "20", "20", "20", "20"}, 5, 5));
        panel.add(lblDelta);
        panel.add(txtDelta);
        panel.add(new JLabel());
//        panel.add(lblDailyVol);
//        panel.add(txtDailyVol);
        panel.add(lblBreakEvPrice);
        panel.add(txtBreakEvPrice);

        panel.add(lblGamma);
        panel.add(txtGama);
        panel.add(new JLabel());
//        panel.add(lblVolChange);
//        panel.add(txtVolChange);
        panel.add(lblIntrinsic);
        panel.add(txtIntrinsic);

        panel.add(lblTheta);
        panel.add(txtTheta);
        panel.add(new JLabel());
        panel.add(lblTimeVal);
        panel.add(txtTimeVal);


        panel.add(lblVega);
        panel.add(txtVaga);
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());


        panel.add(lblRow);
        panel.add(txtRow);
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());

//		panel.setBorder(new OvalBorder(Color.BLACK, Color.GRAY));
        panel.setBorder(BorderFactory.createLineBorder(Theme.getBlackColor()));
        return panel;
    }

    private JPanel createCenterPan() {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"120", "200", "100%", "100", "10"}, new String[]{"20"}, 5, 5, true, true));
//        panel.setBorder(BorderFactory.createLineBorder(Theme.getBlackColor()));
//        panel.add(new JLabel());
        panel.add(lblCalculationMtd);
        panel.add(cmbCalculationMtd);
        panel.add(new JLabel());
        panel.add(btnCalulate);
        panel.add(new JLabel());
//		panel.setBorder(new OvalBorder(Color.BLACK, Color.GRAY));
        panel.setBorder(BorderFactory.createLineBorder(Theme.getBlackColor()));
        return panel;
    }

    private JPanel createNorthPan() {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"25%", "25%", "5", "25%", "25%"}, new String[]{"20", "20", "20", "20", "20"}, 5, 5));
        panel.add(lblOptionType);
        panel.add(cmbOptionType);
        panel.add(new JLabel());
        panel.add(lblInterestRates);
        panel.add(txtInterestRates);

        panel.add(lblSpotPrice);
        panel.add(txtSpotPrice);
        panel.add(new JLabel());
        panel.add(lblDividendYield);
        panel.add(txtDividendYield);

        panel.add(lblStrikePrice);
        panel.add(txtStrikePrice);
        panel.add(new JLabel());
        panel.add(lblNoOfSteps);
        panel.add(txtNoOfSteps);

        panel.add(lblDateUntExpiary);
        panel.add(txtDateUntExpiary);
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());

        panel.add(rbPremium);
        panel.add(txtPremium);
        panel.add(new JLabel());
        panel.add(rbVolatility);
        panel.add(txtVolatility);

        rbPremium.setSelected(true);
//		panel.setBorder(new LineBorder(Color.BLACK, 1, true));
//		panel.setBorder(new OvalBorder(Color.BLACK, Color.GRAY));
        panel.setBorder(BorderFactory.createLineBorder(Theme.getBlackColor()));
        return panel;
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource().equals(cmbCalculationMtd)) {
            changeCalcMtd();
            TWComboItem item = (TWComboItem) cmbCalculationMtd.getSelectedItem();
            String selected = item.getValue();
            if (selected.equals(Language.getString("COX_ROSS"))) {
                txtNoOfSteps.setEditable(true);
                rbPremium.setEnabled(false);
                txtPremium.setEditable(false);
                rbVolatility.setEnabled(true);
                txtVolatility.setEnabled(true);
                rbVolatility.setSelected(true);

            } else {
                txtNoOfSteps.setEditable(false);
                rbVolatility.setEnabled(true);
                txtVolatility.setEnabled(true);
                rbPremium.setEnabled(true);
                txtPremium.setEditable(true);
                if (rbVolatility.isSelected()) {
                    txtVolatility.setEditable(true);
                    txtPremium.setEditable(false);
                } else {
                    txtVolatility.setEditable(false);
                    txtPremium.setEditable(true);
                }
            }

            this.updateUI();
        } else if (e.getSource().equals(rbVolatility)) {
            txtVolatility.setEditable(true);
            txtPremium.setEditable(false);
            rbPremium.setText(Language.getString("THEORITICAL_PRICE"));
            rbVolatility.setText(Language.getString("VOLATILITY"));
            rbPremium.repaint();
            rbVolatility.repaint();
            txtPremium.setText("");
            txtVolatility.setText("");
            /*if (rbVolatility.isSelected()) {
                   txtVolatility.setEnabled(true);
                   txtPremium.setEnabled(false);
                   rbPremium.setText(Language.getString("THEORITICAL_PRICE"));
                   rbVolatility.setText(Language.getString("VOLATILITY"));
                   rbPremium.repaint();
                   rbVolatility.repaint();
                   txtPremium.setText("");
                   txtVolatility.setText("");
               } else {
                   txtVolatility.setEnabled(false);
                   txtPremium.setEnabled(false);
               }*/


        } else if (e.getSource().equals(rbPremium)) {
            txtPremium.setEditable(true);
            txtVolatility.setEditable(false);
            rbVolatility.setText(Language.getString("OC_IMPLIED_VOLATILITY"));
            rbPremium.setText(Language.getString("INSTRUMENT_PREMIUM"));
            rbVolatility.repaint();
            rbPremium.repaint();
            txtPremium.setText("");
            txtVolatility.setText("");
            /*if (rbPremium.isSelected()) {
                   txtPremium.setEnabled(true);
                   txtVolatility.setEnabled(false);
                   rbVolatility.setText(Language.getString("OC_IMPLIED_VOLATILITY"));
                   rbPremium.setText(Language.getString("INSTRUMENT_PREMIUM"));
                   rbVolatility.repaint();
                   rbPremium.repaint();
                   txtPremium.setText("");
                   txtVolatility.setText("");
               }*/
        }


    }


    private void populateComboBoxes() {

        cmbOptionType.addItem(new TWComboItem(1, Language.getString("OPT_CALL")));
        cmbOptionType.addItem(new TWComboItem(2, Language.getString("OPT_PUT")));


        cmbCalculationMtd.addItem(new TWComboItem(COX_ROSS, Language.getString("COX_ROSS")));
        cmbCalculationMtd.addItem(new TWComboItem(BLACK_SCHOLES, Language.getString("BLACK_SCHOLES")));

        cmbCalculationMtd.setSelectedIndex(1);
        txtNoOfSteps.setEditable(true);
        rbVolatility.setEnabled(true);
        rbPremium.setEnabled(false);
        txtPremium.setEditable(false);
        txtVolatility.setEditable(true);
        rbVolatility.setSelected(true);

    }

    private boolean isInputValid() {

        if (txtSpotPrice.getText().isEmpty()) {
            new ShowMessage(Language.getString("OC_INVALID_SPOTPRICE"), "E");
            return false;
        }
        double spot = Double.parseDouble(txtSpotPrice.getText());
        if (spot <= 0) {
            new ShowMessage(Language.getString("OC_INVALID_SPOTPRICE"), "E");
            return false;
        }

        if (txtStrikePrice.getText().isEmpty()) {
            new ShowMessage(Language.getString("OC_INVALID_STRIKEPRICE"), "E");
            return false;
        }
        double strike = Double.parseDouble(txtStrikePrice.getText());
        if (strike <= 0) {
            new ShowMessage(Language.getString("OC_INVALID_STRIKEPRICE"), "E");
            return false;
        }

        if (getSelectedCalculationMethod().equals(COX_ROSS)) {
            if (txtVolatility.getText().isEmpty()) {
                new ShowMessage(Language.getString("OC_INVALID_VOLATILITY"), "E");
                return false;
            }
            double volatility = Double.parseDouble(txtVolatility.getText());
            if (volatility <= 0) {
                new ShowMessage(Language.getString("OC_INVALID_VOLATILITY"), "E");
                return false;
            }
        }

        if (getSelectedCalculationMethod().equals(BLACK_SCHOLES) && rbVolatility.isSelected()) {
            if (txtVolatility.getText().isEmpty()) {
                new ShowMessage(Language.getString("OC_INVALID_VOLATILITY"), "E");
                return false;
            }
            double volatility = Double.parseDouble(txtVolatility.getText());
            if (volatility <= 0) {
                new ShowMessage(Language.getString("OC_INVALID_VOLATILITY"), "E");
                return false;
            }
        }

        if (txtInterestRates.getText().isEmpty()) {
            new ShowMessage(Language.getString("FVC_INVALID_INTEREST"), "E");
            return false;
        }
        double interest = Double.parseDouble(txtInterestRates.getText());
        if (interest < 0) {
            new ShowMessage(Language.getString("FVC_INVALID_INTEREST"), "E");
            return false;
        }

        // if (getSelectedCalculationMethod().equals(BLACK_SCHOLES) ) {
        if (!txtDividendYield.getText().isEmpty()) {
            double div = 0.00;
            try {
                div = Double.parseDouble(txtDividendYield.getText());
            } catch (NumberFormatException e) {
                new ShowMessage(Language.getString("OC_INVALID_DIVIDENDYIELD"), "E");
                return false;
            }

            if (div < 0) {
                new ShowMessage(Language.getString("OC_INVALID_DIVIDENDYIELD"), "E");
                return false;
            }
        }
//        }
//        if (getSelectedCalculationMethod().equals(COX_ROSS)) {
//            if (txtDividendYield.getText().isEmpty()) {
//                new ShowMessage(Language.getString("OC_INVALID_DIVIDENDYIELD"), "E");
//                return false;
//            }
//            double div = Double.parseDouble(txtDividendYield.getText());
//            if (div < 0) {
//
//                new ShowMessage(Language.getString("OC_INVALID_DIVIDENDYIELD"), "E");
//                return false;
//
//
//            }
//        }


        if ((getSelectedCalculationMethod().equals(BLACK_SCHOLES) && !rbVolatility.isSelected())) {
            if (txtPremium.getText().isEmpty()) {
                new ShowMessage(Language.getString("OC_INVALID_ACTMKTVAL"), "E");
                return false;
            }
            double actMkt = Double.parseDouble(txtPremium.getText());
            if (actMkt <= 0) {
                new ShowMessage(Language.getString("OC_INVALID_ACTMKTVAL"), "E");
                return false;
            }
        }
//        if((getSelectedCalculationMethod().equals(BLACK_SCHOLES) && rbVolatility.isSelected()) ) {
//              if(txtVolatility.getText().isEmpty()){
//                new ShowMessage(Language.getString("OC_INVALID_VOLATILITY"), "E");
//                return false;
//              }
//            double vol = Double.parseDouble(txtDividendYield.getText());
//            if(vol<=0){
//              new ShowMessage(Language.getString("OC_INVALID_VOLATILITY"), "E");
//                return false;
//            }
//        }


        if (txtDateUntExpiary.getText().isEmpty()) {
            new ShowMessage(Language.getString("OC_INVALID_EXPIARY"), "E");
            return false;
        }
        int days = Integer.parseInt(txtDateUntExpiary.getText());
        if (days <= 0) {
            new ShowMessage(Language.getString("OC_INVALID_EXPIARY"), "E");
            return false;
        }
        if (getSelectedCalculationMethod().equals(COX_ROSS)) {
            if (txtNoOfSteps.getText().isEmpty()) {
                new ShowMessage(Language.getString("OC_INVALID_STEPS"), "E");
                return false;
            }
            int steps = Integer.parseInt(txtNoOfSteps.getText());
            if (steps <= 0) {
                new ShowMessage(Language.getString("OC_INVALID_STEPS"), "E");
                return false;
            }

        }

        return true;


    }

    private void validateCheckBox() {
        try {
            String selected = ((TWComboItem) cmbCalculationMtd.getSelectedItem()).getValue();

            if (selected.equals(Language.getString("COX_ROSS"))) {
                txtNoOfSteps.setEditable(true);
                rbPremium.setEnabled(false);
                txtPremium.setEditable(false);
                rbVolatility.setEnabled(true);
                txtVolatility.setEditable(true);
                rbVolatility.setSelected(true);

            } else {
                txtNoOfSteps.setEditable(false);
                rbVolatility.setEnabled(true);
                txtVolatility.setEditable(true);
                rbPremium.setEnabled(true);
                txtPremium.setEditable(true);

                if (rbVolatility.isSelected()) {
                    txtVolatility.setEditable(true);
                    txtPremium.setEditable(false);
                    rbPremium.setText(Language.getString("THEORITICAL_PRICE"));
                    rbVolatility.setText(Language.getString("VOLATILITY"));
                } else {
                    txtVolatility.setEditable(false);
                    txtPremium.setEditable(true);
                    rbVolatility.setText(Language.getString("OC_IMPLIED_VOLATILITY"));
                    rbPremium.setText(Language.getString("INSTRUMENT_PREMIUM"));
                }
/*
                if (rbVolatility.isSelected()) {
                    txtVolatility.setEnabled(true);
                    txtPremium.setEnabled(false);
                     rbPremium.setText(Language.getString("THEORITICAL_PRICE"));
                  rbVolatility.setText(Language.getString("VOLATILITY"));
                } else {
                    txtVolatility.setEnabled(false);
                    txtPremium.setEnabled(true);
                    rbVolatility.setText(Language.getString("OC_IMPLIED_VOLATILITY"));
                  rbPremium.setText(Language.getString("INSTRUMENT_PREMIUM"));
                }*/
            }
        } catch (Exception e) {

        }

    }

    public String getSelectedCalculationMethod() {
        TWComboItem item = (TWComboItem) cmbCalculationMtd.getSelectedItem();
        String selected = item.getId();
        return selected;

    }

    public String getSelectedOptionType() {
        TWComboItem item = (TWComboItem) cmbOptionType.getSelectedItem();
        String selected = item.getValue();
        return selected;
    }

    public void setVisible(boolean value) {
        if (value) {
            validateCheckBox();
            //clearAllForCalculation();
        }
        super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void clearAllForCalculation() {
        txtBreakEvPrice.setText("");
        txtSpotPrice.setText("");
        txtStrikePrice.setText("");
        txtDateUntExpiary.setText("");
        txtInterestRates.setText("");
        txtDividendYield.setText("");
        txtNoOfSteps.setText("");
        txtVolatility.setText("");
        txtPremium.setText("");

        txtDelta.setText("");
        txtGama.setText("");
        txtTheta.setText("");
        txtVaga.setText("");
        txtRow.setText("");
        txtBreakEvPrice.setText("");
        txtIntrinsic.setText("");
        txtTimeVal.setText("");

    }

    public void changeCalcMtd() {
        txtDelta.setText("");
        txtGama.setText("");
        txtTheta.setText("");
        txtVaga.setText("");
        txtRow.setText("");
        txtBreakEvPrice.setText("");
        txtIntrinsic.setText("");
        txtTimeVal.setText("");
        txtNoOfSteps.setText("");
        txtVolatility.setText("");
        txtPremium.setText("");
    }


    private class OptionCalculatorForcusTravesalPolicy extends FocusTraversalPolicy {
        private Component[] firstTab = {cmbOptionType, txtInterestRates, txtSpotPrice, txtDividendYield, txtStrikePrice, txtDateUntExpiary, txtPremium, txtVolatility};
        private Component[] secondTab = {cmbOptionType, txtInterestRates, txtSpotPrice, txtDividendYield, txtStrikePrice, txtNoOfSteps, txtDateUntExpiary, txtPremium, txtVolatility};

        private Component[] components = firstTab; /*{cmbPortfolioNos, txtSymbol, cmbAction, cmbType, txtTPrice, txtAllOrNone, txtTQty, btnShowDays,
                txtTakeProfit, txtStopPrice, txtMinFill,  txtDisclosed, cmbexecTypeOperators, txtexecTimeValue, txtexecBlockValue,
                cmbConditionMethods, cmbConditionOperators, txtConditionValue, cmbConditionExpiry,
                btnBuy, btnSell, btnAmend,btnCancel, btnClose, btnQ};*/

        private int lastSelectedTab = 0;


        public Component getComponentAfter(Container aContainer, Component aComponent) {
            if (getSelectedCalculationMethod().equalsIgnoreCase(COX_ROSS)) {
                components = secondTab;
            } else {
                components = firstTab;
            }
            int currentComponent = findComponent(aComponent);
//            System.out.println("tab focus componenet =="+components[findNextComponentIndex(currentComponent)]);
            return components[findNextComponentIndex(currentComponent)];
        }

        public Component getComponentBefore(Container aContainer, Component aComponent) {
            int currentComponent = findComponent(aComponent);
            return components[findPreviousComponentIndex(currentComponent)];
        }

        public Component getFirstComponent(Container aContainer) {
            return cmbOptionType;  //To change body of implemented methods use File | Settings | File Templates.
        }

        public Component getLastComponent(Container aContainer) {
            return txtVolatility;  //To change body of implemented methods use File | Settings | File Templates.
        }

        public Component getDefaultComponent(Container aContainer) {
            return cmbOptionType;  //To change body of implemented methods use File | Settings | File Templates.
        }

        private int findComponent(Component aComponent) {
            for (int i = 0; i < components.length; i++) {
                if (aComponent.equals(components[i])) {
                    return i;
                }
            }
            return 0;
        }

        private int findNextComponentIndex(int current) {
            for (int i = current + 1; i < components.length; i++) {
                if (components[i].isEnabled() && components[i].isVisible()) {
                    return i;
                }
            }

            for (int i = 0; i < current; i++) {
                if (components[i].isEnabled() && components[i].isVisible()) {
                    return i;
                }
            }

            return current;
        }

        private int findPreviousComponentIndex(int current) {
            for (int i = current - 1; i >= 0; i--) {
                if (components[i].isEnabled() && components[i].isVisible()) {
                    return i;
                }
            }

            for (int i = components.length - 1; i > current; i--) {
                if (components[i].isEnabled() && components[i].isVisible()) {
                    return i;
                }
            }

            return current;
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("O")) {
            calculate();
        }

    }
}

