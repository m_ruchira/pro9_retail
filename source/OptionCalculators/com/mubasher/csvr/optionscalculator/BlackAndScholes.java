package com.mubasher.csvr.optionscalculator;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 1, 2008
 * Time: 7:04:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class BlackAndScholes {


    private static BlackAndScholes self;

    private double d1 = 0.00d;
    private double d2 = 0.00d;
    private double C_theorital = 0.00d;

    private double delta = 0.00d;
    private double gamma = 0.00d;
    private double vega = 0.00d;
    private double theta = 0.00d;
    private double rho = 0.00d;
    private double impliedVol = 0.00d;

    private double strikePrice_K = 0.00d;
    private double spotRate_S = 0.00d;
    private double timeToMaturity_T = 0.00d;
    private double theoreticalPrice_C = 0.00d;
    private double interestRate_r = 0.00d;
    private double volatility_Zigma = 0.00d;
    private double dividendYield_f = 0.00d;
    private double optionPrecision = 0.0005;
    private String type;
    private CDFNormal CdfNormal;


    public BlackAndScholes() {
        CdfNormal = new CDFNormal();
        initCalculator();
    }

    public static BlackAndScholes getSharedInstance() {
        if (self == null) {
            self = new BlackAndScholes();
        }
        return self;
    }


    public void initCalculator() {
        d1 = 0.00d;
        d2 = 0.00d;
        C_theorital = 0.00d;
        delta = 0.00d;
        gamma = 0.00d;
        vega = 0.00d;
        theta = 0.00d;
        rho = 0.00d;
        volatility_Zigma = 0.00d;
    }

    public void calculateTheoritaclPrice(String type, double spotRate, double years, double strikeprice, double interestrate, double volatility, double dividendyield, boolean impVol, double actMktVal) {
        initCalculator();
        this.type = type;
        spotRate_S = spotRate;
        timeToMaturity_T = years;
        strikePrice_K = strikeprice;
        interestRate_r = interestrate / 100;
        volatility_Zigma = volatility / 100;
        dividendYield_f = dividendyield / 100;

//
//        if (dividendYield_f > 0) {
//            spotRate_S = spotRate_S * Math.exp(-(dividendYield_f) * timeToMaturity_T);
//        }

        d1 = (Math.log(spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) / strikePrice_K) + ((interestRate_r + (volatility_Zigma * volatility_Zigma / 2)) * timeToMaturity_T)) / (volatility_Zigma * Math.sqrt(timeToMaturity_T));
        //   d1 = (Math.log(spotRate_S / strikePrice_K) + ((interestRate_r + (volatility_Zigma * volatility_Zigma / 2)) * timeToMaturity_T)) / (volatility_Zigma * Math.sqrt(timeToMaturity_T));
        d2 = d1 - (volatility_Zigma * Math.sqrt(timeToMaturity_T));
        // d2 = d1 - (volatility_Zigma * Math.sqrt(timeToMaturity_T));

        if (type.equalsIgnoreCase("call")) {

            //  C_theorital = (spotRate_S* CDFNormal.normp(d1)) - (strikePrice_K* Math.exp(-(interestRate_r)*timeToMaturity_T)* CDFNormal.normp(d2));
            C_theorital = (spotRate_S * CdfNormal.normp(d1) * Math.exp(-dividendYield_f * timeToMaturity_T)) - (strikePrice_K * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(d2));
            //   C_theorital = (spotRate_S* CdfNormal.normp(d1)) - (strikePrice_K* Math.exp(-(interestRate_r)*timeToMaturity_T)* CdfNormal.normp(d2));

        } else {
            C_theorital = (strikePrice_K * Math.exp(-interestRate_r * timeToMaturity_T) * CdfNormal.normp(-d2)) - (spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.normp(-d1));
            // C_theorital = (strikePrice_K*Math.exp(-interestRate_r*timeToMaturity_T)* CdfNormal.normp(-d2))- (spotRate_S*  CdfNormal.normp(-d1));
        }
        if (!impVol) {
            calculateDelata();
            calculateGmma();
            calculateVega();
            calculateTheta();
            calculateRho();
//			printOutput();
        }
        if (impVol) {
            impliedVol = CalculateImpliedVol(spotRate_S, strikePrice_K, interestRate_r, timeToMaturity_T, dividendYield_f, type, actMktVal) * 100;
        } else {
            impliedVol = 0.00;
        }

    }

    public void calculateDelata() {
        if (type.equalsIgnoreCase("call")) {
            delta = Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.normp(d1);
        } else {
            delta = Math.exp(-dividendYield_f * timeToMaturity_T) * (CdfNormal.normp(d1) - 1);
        }
    }

    public void calculateGmma() {
        // gamma= CDFNormal.PDF(d1)/(spotRate_S*volatility_Zigma*Math.sqrt(timeToMaturity_T));
        gamma = (Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.PDF(d1)) / (spotRate_S * volatility_Zigma * Math.sqrt(timeToMaturity_T));
    }

    public void calculateVega() {
        vega = Math.exp(-dividendYield_f * timeToMaturity_T) * spotRate_S * CdfNormal.PDF(d1) * Math.sqrt(timeToMaturity_T);
        //  vega =  spotRate_S * CdfNormal.PDF(d1) * Math.sqrt(timeToMaturity_T);
    }

    public void calculateTheta() {
        if (type.equalsIgnoreCase("call")) {
            theta = ((-spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.PDF(d1) * volatility_Zigma) / (2 * Math.sqrt(timeToMaturity_T))) - (interestRate_r * strikePrice_K * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(d2)) + (dividendYield_f * spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.normp(d1));
            //    theta = ((-spotRate_S *  CdfNormal.PDF(d1) * volatility_Zigma) / (2 * Math.sqrt(timeToMaturity_T))) - (interestRate_r * strikePrice_K * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(d2));// + (dividendYield_f * spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CDFNormal.normp(d1));
            //   theta = ((-spotRate_S *  CdfNormal.PDF(d1) * volatility_Zigma) / (2 * Math.sqrt(timeToMaturity_T))) - (interestRate_r * strikePrice_K * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(d2)) + (dividendYield_f * spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.normp(d1));

        } else {
            theta = ((-spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.PDF(d1) * volatility_Zigma) / (2 * Math.sqrt(timeToMaturity_T))) + (interestRate_r * strikePrice_K * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(-d2)) - (dividendYield_f * spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.normp(-d1));

            // theta = ((-spotRate_S *  CdfNormal.PDF(d1) * volatility_Zigma) / (2 * Math.sqrt(timeToMaturity_T))) + (interestRate_r * strikePrice_K * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(-d2));// - (dividendYield_f * spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CDFNormal.normp(-d1));
            //       theta = ((-spotRate_S *  CdfNormal.PDF(d1) * volatility_Zigma) / (2 * Math.sqrt(timeToMaturity_T))) + (interestRate_r * strikePrice_K * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(-d2)) - (dividendYield_f * spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) * CdfNormal.normp(-d1));
        }
    }

    public void calculateRho() {
        if (type.equalsIgnoreCase("call")) {
            rho = strikePrice_K * timeToMaturity_T * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(d2);

        } else {
            rho = -strikePrice_K * timeToMaturity_T * Math.exp(-(interestRate_r) * timeToMaturity_T) * CdfNormal.normp(-d2);
        }
    }

    public double CalculateImpliedVol(double spotRate, double strikePrice, double interest, double yearsToExpiary, double dividendYield, String optionType, double optionPremium) {
        double greaterThan = 0;
        double lessThan = 0;
        double guess = 0.5;
        double guessPremium = 0;

        while (lessThan <= 0) {
            guessPremium = CalculateGuessPremium(spotRate, strikePrice, guess, interest, yearsToExpiary, dividendYield, optionType);

            if (guessPremium < optionPremium) {
                greaterThan = guess;
                guess = guess * 2;
            } else {
                lessThan = guess;
                guess = greaterThan + (0.5 * (lessThan - greaterThan));
            }
        }

        while (lessThan - greaterThan > optionPrecision) {
            guessPremium = CalculateGuessPremium(spotRate, strikePrice, guess, interest, yearsToExpiary, dividendYield, optionType);

            if (guessPremium < optionPremium) {
                greaterThan = guess;
                guess = greaterThan + (0.5 * (lessThan - greaterThan));
            } else {
                lessThan = guess;
                guess = lessThan - (0.5 * (lessThan - greaterThan));
            }
        }
        return guess;
    }


    public double CalculateGuessPremium(double spotRate, double strikePrice, double guess, double interest, double yearsToExpiary, double dividendYield, String optionType) {
        double d1 = CalculateOption_d1(spotRate, strikePrice, guess, interest, yearsToExpiary, dividendYield, optionType);
        double d2 = CalculateOption_d2(spotRate, strikePrice, guess, interest, yearsToExpiary, dividendYield, optionType);
        double guessPremium = 0;
        if (yearsToExpiary == 0) {
            if (optionType.equalsIgnoreCase("call")) {
                guessPremium = Math.max(0, spotRate - strikePrice);
            } else {
                guessPremium = Math.max(0, strikePrice - spotRate);
            }
        } else {
            if (optionType.equalsIgnoreCase("call")) {
                guessPremium = spotRate * Math.exp(-(dividendYield) * yearsToExpiary) * CdfNormal.normp(d1) - (strikePrice * Math.exp(-interest * yearsToExpiary) * CdfNormal.normp(d2));
            } else {
                guessPremium = strikePrice * Math.exp(-interest * yearsToExpiary) * CdfNormal.normp(-d2) - spotRate * Math.exp(-(dividendYield) * yearsToExpiary) * CdfNormal.normp(-d1);
            }
        }
        return guessPremium;
    }

    public double CalculateOption_d1(double spotRate, double strikePrice, double volatility, double interest, double yearsToExpiary, double dividendYield, String optionType) {
        double d1 = (Math.log(spotRate * Math.exp(-dividendYield * yearsToExpiary) / strikePrice) + ((interest + (volatility * volatility / 2)) * yearsToExpiary)) / (volatility * Math.sqrt(yearsToExpiary));
        return d1;
    }

    public double CalculateOption_d2(double spotRate, double strikePrice, double volatility, double interest, double yearsToExpiary, double dividendYield, String optionType) {
        double d2 = ((Math.log(spotRate * Math.exp(-dividendYield * yearsToExpiary) / strikePrice) + ((interest + (volatility * volatility / 2)) * yearsToExpiary)) / (volatility * Math.sqrt(yearsToExpiary))) - ((volatility) * Math.sqrt(yearsToExpiary));
        return d2;
    }

    public void printOutput() {

        System.out.println("theoritical price " + C_theorital);
        System.out.println("delta " + delta);
        System.out.println("gamma " + gamma);
        System.out.println("vega " + vega);
        System.out.println("theta " + theta);
        System.out.println("rho " + rho);
        System.out.println("d1 " + d1);
        System.out.println("d2 " + d2);


    }

    public void fillDataToList(ArrayList<Double> list) {
        list.clear();
        list.add(C_theorital);
        list.add(delta);
        list.add(gamma);
        list.add(theta);
        list.add(vega);
        list.add(rho);
        list.add(impliedVol);

    }


    public void fillOnlyImpliedVol(ArrayList<Double> list) {
        if (list.size() == 7) {
            list.add(6, impliedVol);
        }
    }

    public double getTheoriticalPriceOnly(String type, double stockPrice, double years, double strikeprice,
                                          double RFrate, double volatility, double dividendyield) {

        //System.out.println("******** stock ******* "+stockPrice);
        initCalculator();
        this.type = type;
        spotRate_S = stockPrice;
        timeToMaturity_T = years;
        strikePrice_K = strikeprice;
        interestRate_r = RFrate / 100;
        volatility_Zigma = volatility / 100;
        dividendYield_f = dividendyield / 100;
//
//        if (dividendYield_f > 0) {
//            spotRate_S = spotRate_S * Math.exp(-(dividendYield_f) * timeToMaturity_T);
//        }

        if (strikeprice == 0) {
            System.out.println("fdfs");
        }
        d1 = (Math.log(spotRate_S * Math.exp(-dividendYield_f * timeToMaturity_T) / strikePrice_K) + ((interestRate_r + (volatility_Zigma * volatility_Zigma / 2)) * timeToMaturity_T)) / (volatility_Zigma * Math.sqrt(timeToMaturity_T));
        d2 = d1 - (volatility_Zigma * Math.sqrt(timeToMaturity_T));
        // d2 = d1 - (volatility_Zigma * Math.sqrt(timeToMaturity_T));

        if (type.equalsIgnoreCase("call")) {

            C_theorital = spotRate_S * CDF_Normal.normp(d1) - (strikePrice_K * Math.exp(-(interestRate_r) * timeToMaturity_T) * CDF_Normal.normp(d2));
            //System.out.println("******* CDF_Normal.normp(d1) CALL********* "+CDF_Normal.normp(d1));

        } else {
            C_theorital = (strikePrice_K * Math.exp(-interestRate_r * timeToMaturity_T) * CDF_Normal.normp(-d2)) - (spotRate_S * CDF_Normal.normp(-d1));
            //System.out.println("******* CDF_Normal.normp(d1) CALL********* "+CDF_Normal.normp(d1));
        }

        return C_theorital;
    }


}
