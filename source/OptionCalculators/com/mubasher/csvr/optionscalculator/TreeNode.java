package com.mubasher.csvr.optionscalculator;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 4, 2008
 * Time: 8:17:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class TreeNode {

    public int step;
    public int verticalRank;
    public double stockPrice_S;
    public double optionValue;

    public TreeNode(int step, int verticalRank) {
        this.step = step;
        this.verticalRank = verticalRank;
    }

    public void SetSimpleOptionValue(double strikePrice, BinomialCalculator.EnumCallPut putCall) {
        this.optionValue = SimpleValue(stockPrice_S, strikePrice, putCall);
    }

    public double SimpleValue(double price, double strike, BinomialCalculator.EnumCallPut putCall) {
        if (putCall == BinomialCalculator.EnumCallPut.Call) {
            return Math.max(0.0, price - strike);
        } else {
            return Math.max(0.0, strike - price);
        }
    }

    public void SetCalculatedOptionValue(double upMoveProbability, double optionValUpChild, double optionValDownChild, BinomialCalculator.EnumStyle style, BinomialCalculator.EnumCallPut putOrCall, double strikePrice, double discountFactor) {
        if (style == BinomialCalculator.EnumStyle.European) {
            this.optionValue = (upMoveProbability * (optionValUpChild) + (1 - upMoveProbability) * optionValDownChild) * discountFactor;
        } else {
            double tmpOptionVal = (upMoveProbability * (optionValUpChild) + (1 - upMoveProbability) * optionValDownChild) * discountFactor;
            this.optionValue = Math.max(SimpleValue(stockPrice_S, strikePrice, putOrCall), tmpOptionVal);

        }
    }
}
