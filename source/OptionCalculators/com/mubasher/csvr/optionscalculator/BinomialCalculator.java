package com.mubasher.csvr.optionscalculator;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 4, 2008
 * Time: 8:33:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class BinomialCalculator {

    BinomialTree nodeFactory = null;
    private double stockPrice = 0.0;
    private double strikeRate = 0.0;
    private double timeToExpiration = 0.0;
    private double volatility = 0.0;
    private double riskFreeRate = 0.0;
    private double dividendYield = 0;
    private EnumCallPut putCall = EnumCallPut.Call;
    private EnumStyle style = EnumStyle.European;
    private int noOfSteps = 0;
    private double deltaT = 0;
    private double optionUp = 0;
    private double optionDown = 0;
    private double discountFactor = 0;
    private double upMoveProbability = 0;

    public BinomialCalculator(double stockPrice, double strikeRate, double timeToExpiration, double volatility, double riskFreeRate, double dividendYield, EnumCallPut callPut, EnumStyle style, int numOfSteps) {
        this.stockPrice = stockPrice;
        this.strikeRate = strikeRate;
        this.timeToExpiration = timeToExpiration;
        this.volatility = volatility;
        this.riskFreeRate = riskFreeRate;
        this.dividendYield = dividendYield;
        this.putCall = callPut;
        this.style = style;
        this.noOfSteps = numOfSteps;
        ComputeVariables();
        CreateStockPriceTree();
    }

    public BinomialCalculator() {

    }

    public void initCalculator() {
        stockPrice = 0.0;
        strikeRate = 0.0;
        timeToExpiration = 0.0;
        volatility = 0.0;
        riskFreeRate = 0.0;
        dividendYield = 0;
        putCall = EnumCallPut.Call;
        style = EnumStyle.European;
        noOfSteps = 0;
        deltaT = 0;
        optionUp = 0;
        optionDown = 0;
        discountFactor = 0;
        upMoveProbability = 0;
        nodeFactory = null;
    }

    private void ComputeVariables() {
        deltaT = timeToExpiration / noOfSteps;
        discountFactor = CalculateDiscountFactor();
        optionUp = CalculateOptionUp();
        optionDown = CalculateOptionDown();
        upMoveProbability = CalculateUpMoveProbality();

    }

    private double CalculateOptionUp() {
        return Math.exp(volatility * Math.sqrt(deltaT));
    }

    private double CalculateOptionDown() {
        return Math.exp(-volatility * Math.sqrt(deltaT));
    }

    private double CalculateDiscountFactor() {
        return Math.exp(-1 * riskFreeRate * deltaT);
    }

    private double CalculateUpMoveProbality() {
        return ((Math.exp((riskFreeRate - dividendYield) * deltaT)) - optionDown) / (optionUp - optionDown);
    }


    public void CreateStockPriceTree() {
        nodeFactory = new BinomialTree();
        nodeFactory.resetNodes();

        {
            for (int jStep = 0; jStep <= noOfSteps; jStep++) {
                for (int i = 0; i <= jStep; i++) {
                    nodeFactory.CreateNode(jStep, i, optionUp, optionDown, stockPrice, deltaT);
                }
            }
        }
    }

    public double GetOptionValue(ArrayList<Double> store) {
        ComputeOptionValues();
        store.clear();
        store.add(nodeFactory.GetNode(0, 0).optionValue);
        store.add(0d);
        store.add(0d);
        store.add(0d);
        store.add(0d);
        store.add(0d);

        return nodeFactory.GetNode(0, 0).optionValue;
    }

    private void ComputeOptionValues() {

        {
            for (int ii = 0; ii <= noOfSteps; ii++) {
                nodeFactory.GetNode(noOfSteps, ii).SetSimpleOptionValue(strikeRate, putCall);
            }
            if (noOfSteps > 0) {
                for (int jStep = noOfSteps - 1; jStep >= 0; jStep--) {
                    for (int i = 0; i <= jStep; i++) {
                        nodeFactory.GetNode(jStep, i).SetCalculatedOptionValue(upMoveProbability, nodeFactory.GetNode(jStep + 1, i + 1).optionValue, nodeFactory.GetNode(jStep + 1, i).optionValue, style, putCall, strikeRate, discountFactor);
                    }
                }
            }
        }
    }

    public double getTheoreticalPriceOnly(double stockPrice, double strikeRate, double timeToExpiration, double volatility, double riskFreeRate, double dividendYield, EnumCallPut callPut, EnumStyle style, int numOfSteps) {
        this.stockPrice = stockPrice;
        this.strikeRate = strikeRate;
        this.timeToExpiration = timeToExpiration;
        this.volatility = volatility;
        this.riskFreeRate = riskFreeRate;
        this.dividendYield = dividendYield;
        this.putCall = callPut;
        this.style = style;
        this.noOfSteps = numOfSteps;

        ComputeVariables();
        CreateStockPriceTree();
        ComputeOptionValues();

        return nodeFactory.GetNode(0, 0).optionValue;
    }

    public enum EnumCallPut {
        Call, Put
    }

    public enum EnumStyle {
        American, European
    }

}
