package com.mubasher.csvr.optionscalculator;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 1, 2008
 * Time: 5:22:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptionCalculatorModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private static String[] parameter = {Language.getString("THEORITICAL_PRICE"), Language.getString("DELATA"), Language.getString("GAMMA"), Language.getString("THETA"),
            Language.getString("VEGA"), Language.getString("RHO"), Language.getString("OC_IMPLIED_VOLATILITY")};
    private ArrayList<Double> dataStore;
    private TWDecimalFormat formater = new TWDecimalFormat("#0.00000");


    public void setDataStore(ArrayList dataStore) {
        this.dataStore = dataStore;
    }

    public int getRowCount() {
        if (dataStore == null) {
            return 0;  //To change body of implemented methods use File | Settings | File Templates.
        } else {
            return 7;
        }
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            if (columnIndex == 0) {
                return parameter[rowIndex];
            } else if (columnIndex == 1 && !dataStore.isEmpty()) {
                return getFormatedValue(dataStore.get(rowIndex));
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public void setSymbol(String symbol) {
//To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int column) {
        return super.getViewSettings().getColumnHeadings()[column];
    }

    public Class<?> getColumnClass(int columnIndex) {
        return super.getColumnClass(columnIndex);    //To change body of overridden methods use File | Settings | File Templates.
    }

    private String getFormatedValue(double val) {
        String fval = "";
        try {
            fval = formater.format(val);
        } catch (Exception e) {
            fval = val + "";
        }
        return fval;
    }
}
