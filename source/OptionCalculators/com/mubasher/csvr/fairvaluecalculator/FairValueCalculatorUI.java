package com.mubasher.csvr.fairvaluecalculator;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.util.TWSpinnerNumberModel;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 2, 2008
 * Time: 4:04:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class FairValueCalculatorUI extends InternalFrame implements NonNavigatable {

    private static FairValueCalculatorUI self;
    TWDecimalFormat format = new TWDecimalFormat("#,##0.0000");
    private TWButton calculate;
    private TWButton cancel;
    private JLabel lblIndexPrice;
    private JLabel lblInterestRate;
    private JLabel lblDividend;
    private JLabel lblContractDays;
    private JLabel lblFairValues;
    private JSpinner spinIndexPrice;
    private JSpinner spinInterestRate;
    private JSpinner spinDividend;
    private JSpinner spinContractDays;
    private TWTextArea txtFairValue;

    private FairValueCalculatorUI() {
        createUI();

    }

    public static FairValueCalculatorUI getSharedInstance() {
        if (self == null) {
            self = new FairValueCalculatorUI();
        }
        return self;
    }

    private void createUI() {

        this.setSize(new Dimension(250, 220));
        this.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(false);
        this.setResizable(false);
        this.setShowServicesMenu(false);
        this.setTitle(Language.getString("FVC_TITLE"));
        this.hideTitleBarMenu();
        this.setLayout(new BorderLayout(5, 5));
        createItems();
        addComponents();
        addListeners();

        Client.getInstance().getDesktop().add(this);
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        GUISettings.applyOrientation(this);

    }

    private void createItems() {

        calculate = new TWButton(Language.getString("CALCULATE"));
        cancel = new TWButton(Language.getString("CANCEL"));

        lblIndexPrice = new JLabel(Language.getString("FVC_INDEX_PRICE"));
        lblInterestRate = new JLabel(Language.getString("FVC_INTEREST_RATE"));
        lblDividend = new JLabel(Language.getString("FVC_DIVIDEND"));
        lblContractDays = new JLabel(Language.getString("FVC_CONTRACT_DAYS"));
        lblFairValues = new JLabel(Language.getString("FVC_FAIR_VALUE"));

        spinIndexPrice = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        spinInterestRate = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        spinDividend = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        spinContractDays = new JSpinner(new TWSpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        spinIndexPrice.setInputVerifier(new TWNumberInputVerifier());
        spinInterestRate.setInputVerifier(new TWNumberInputVerifier());
        spinDividend.setInputVerifier(new TWNumberInputVerifier());
        spinContractDays.setInputVerifier(new TWNumberInputVerifier());
//        spinContractDays.
        txtFairValue = new TWTextArea();
        txtFairValue.setEditable(false);

    }

    private void addListeners() {

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                hideWindow();
            }
        });

        calculate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                calculateFairPrice();
            }
        });

    }

    private void addComponents() {
//            this.setLayout(new BorderLayout());
        JPanel panel1 = new JPanel();
        FlexGridLayout input = new FlexGridLayout(new String[]{"100", "100"}, new String[]{"20", "5", "20", "5", "20", "5", "20"});
        panel1.setLayout(input);

        panel1.setBorder(BorderFactory.createTitledBorder(Language.getString("INPUT_VALUES")));

        panel1.add(lblIndexPrice);
        panel1.add(spinIndexPrice);
        panel1.add(new JLabel());
        panel1.add(new JLabel());
        panel1.add(lblInterestRate);
        panel1.add(spinInterestRate);
        panel1.add(new JLabel());
        panel1.add(new JLabel());
        panel1.add(lblDividend);
        panel1.add(spinDividend);
        panel1.add(new JLabel());
        panel1.add(new JLabel());
        panel1.add(lblContractDays);
        panel1.add(spinContractDays);


        JPanel panel2 = new JPanel();
        panel2.setLayout(new BorderLayout());
        JPanel panelResult = new JPanel();
        panelResult.setLayout(new FlexGridLayout(new String[]{"3", "101", "100"}, new String[]{"20"}));
        panelResult.add(new JLabel());
        panelResult.add(lblFairValues);

        panelResult.add(txtFairValue);

        JPanel btnpanel = new JPanel();
        btnpanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        btnpanel.add(calculate);
        btnpanel.add(cancel);

        panel2.add(panelResult, BorderLayout.NORTH);
        panel2.add(btnpanel, BorderLayout.SOUTH);

        this.add(panel1, BorderLayout.CENTER);
        this.add(panel2, BorderLayout.SOUTH);
    }

    public void hideWindow() {
        this.setVisible(false);
    }

    public void showWindow() {
        initFields();
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setVisible(true);
    }

    public void initFields() {
        txtFairValue.setText("");
        spinIndexPrice.setValue(0.00);
        spinDividend.setValue(0.00);
        spinInterestRate.setValue(0.00);
        spinContractDays.setValue(0);


    }

    public void calculateFairPrice() {

        if (isInputsValid()) {
            double indexprice = (Double) spinIndexPrice.getValue();
            double div = (Double) spinDividend.getValue();
            double interest = ((Double) spinInterestRate.getValue()) / 100;
            int days = (Integer) spinContractDays.getValue();

            double fairvalue = ((1 + (interest * days / 360)) * (indexprice)) - div;
            txtFairValue.setText(format.format(fairvalue));

        }

    }

    public boolean isInputsValid() {
        double indexprice = (Double) spinIndexPrice.getValue();
        if (indexprice <= 0) {
            new ShowMessage(Language.getString("FVC_INVALID_INDEX"), "E");
            return false;
        }
        double div = (Double) spinDividend.getValue();
        if (div <= 0) {
            new ShowMessage(Language.getString("FVC_DIVIDEND_INVALID"), "E");
            return false;
        }
        double inte = (Double) spinInterestRate.getValue();
        if (inte <= 0) {
            new ShowMessage(Language.getString("FVC_INVALID_INTEREST"), "E");
            return false;
        }
        int days = (Integer) spinContractDays.getValue();
        if (days <= 0) {
            new ShowMessage(Language.getString("FVC_INVALID_CONTRACT_DAYS"), "E");
            return false;
        }
        return true;
    }

    private class TWNumberInputVerifier extends InputVerifier {
        public boolean verify(JComponent input) {
            JTextField tf = (JTextField) input;
            try {
                Double.parseDouble(tf.getText());
                return true;
            } catch (NumberFormatException e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return false;
            } catch (Exception ex) {
                return true;
            }
        }
    }


}
