package com.mubasher.mediaplayer;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Mar 2, 2009
 * Time: 9:21:12 AM
 */
public class URLComponentRecord {
    private long id;
    private String caption;
    private String url;
    private boolean isDefault;

    public URLComponentRecord(long id, String caption, String url, boolean isDefault) {
        this.id = id;
        this.caption = caption;
        this.url = url;
        this.isDefault = isDefault;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }
}
