package com.mubasher.mediaplayer;

import com.isi.csvr.shared.DownArrow;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Mar 2, 2009
 * Time: 10:44:59 AM
 */
public class URLSelector extends JPanel implements URLSelectionListener, MouseListener {
    private JLabel txtURL;
    private ArrayList<URLComponentRecord> customURLs;
    private ArrayList<URLComponentRecord> defaultURLs;
    private URLSelectionListener parent;

    public URLSelector() {
        customURLs = new ArrayList<URLComponentRecord>();
        defaultURLs = new ArrayList<URLComponentRecord>();
        txtURL = new JLabel(Language.getString("MEDIA_PLAYER_SELECT_CHANNEL"));
        txtURL.setOpaque(true);
        txtURL.setBackground(Color.black);
        txtURL.setForeground(Color.white);
        txtURL.setBorder(BorderFactory.createEmptyBorder());
        txtURL.addMouseListener(this);

        JLabel dropButton = new JLabel(new DownArrow(Color.white));
        dropButton.setBorder(BorderFactory.createEmptyBorder());
        dropButton.setBackground(Color.black);
        dropButton.addMouseListener(this);
        setLayout(new FlexGridLayout(new String[]{"100%", "25"}, new String[]{"23"}, 2, 0));
        add(txtURL);
        add(dropButton);
        setBackground(Color.black);
        GUISettings.applyOrientation(this);
    }

    public void addURLSelectionListener(URLSelectionListener parent) {
        this.parent = parent;
    }

    public void addURL(String caption, String url) {
        URLComponentRecord record = new URLComponentRecord(System.currentTimeMillis() + (int) (Math.random() * 100), caption, url, false);
        customURLs.add(record);
    }

    public void addURL(URLComponentRecord record) {
        if (record != null) {
            if (record.isDefault()) {
                defaultURLs.add(record);
            } else {
                customURLs.add(record);
            }
        }
    }

    public void removeURL(URLComponentRecord record) {
        if (record != null) {
            customURLs.remove(record);
        }
    }

    public ArrayList<URLComponentRecord> getURLs(boolean defaultURLs) {
        if (defaultURLs) {
            return this.defaultURLs;
        } else {
            return customURLs;
        }
    }

    public void URLSelected(URLComponentRecord url) {
        if (url != null) {
            txtURL.setText(url.getCaption());
        }
        if (parent != null) {
            parent.URLSelected(url);
        } else {

        }
    }

    public void deleteSelected(URLComponentRecord url) {
        if (parent != null) {
            String msg = Language.getString("MEDIA_PLAYER_DELETE_CHANNEL_MSG");
            msg = msg.replaceFirst("\\[CHANNEL\\]", url.getCaption());
            int result = SharedMethods.showConfirmMessage(msg, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                parent.deleteSelected(url);
            }
        }
    }

    public void editSelected(URLComponentRecord url) {
        if (parent != null) {
            parent.editSelected(url);
        }
    }

    public void mouseClicked(MouseEvent e) {
        int maxWidth = 0;
        JPopupMenu menu = new JPopupMenu();
        JPanel panel = new JPanel(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        JScrollPane scroll = new JScrollPane(panel);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        URLComponent component;

        for (URLComponentRecord record : defaultURLs) {
            component = new URLComponent(record);
            component.addURLSelectionListener(this);
            panel.add(component);
            maxWidth = Math.max(maxWidth, component.getPreferredSize().width);
        }
        panel.add(new JSeparator());

        for (URLComponentRecord record : customURLs) {
            component = new URLComponent(record);
            component.addURLSelectionListener(this);
            panel.add(component);
            maxWidth = Math.max(maxWidth, component.getPreferredSize().width);
        }

        component = new URLComponent(null);
        component.addURLSelectionListener(this);
        panel.add(component);

        int preferredHeight = (int) panel.getPreferredSize().getHeight();
        int urlWidth = txtURL.getParent().getWidth();
        //scroll.setSize(new Dimension(Math.max(urlWidth, maxWidth)-8,80));
        scroll.setPreferredSize(new Dimension(Math.max(urlWidth, maxWidth) - 8, Math.min(preferredHeight + 8, 200)));
        panel.setSize(new Dimension(Math.max(urlWidth, maxWidth) - 28 /* exclude the border width */, preferredHeight));
        panel.setPreferredSize(new Dimension(Math.max(urlWidth, maxWidth) - 28 /* exclude the border width */, preferredHeight));
        menu.add(scroll);
        menu.setBorder(GUISettings.getTWFrameBorder());
        menu.setLightWeightPopupEnabled(false);
        menu.show(txtURL, 0, 25);
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void clearDefaultURLs() {
        defaultURLs.clear();
    }
}
