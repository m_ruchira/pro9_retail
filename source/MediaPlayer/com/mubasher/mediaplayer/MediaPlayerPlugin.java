package com.mubasher.mediaplayer;

import com.isi.csvr.Client;
import com.isi.csvr.plugin.Plugin;
import com.isi.csvr.plugin.event.PluginEvent;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Feb 26, 2009
 * Time: 4:53:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class MediaPlayerPlugin implements Plugin {
    public static String urlCaption = "";
    private static Object sample = null;
    private static Rectangle rectangle;

    public static void showUI() {
        if (sample == null) {
            sample = new MediaPlayer();
            ((MediaPlayer) sample).getFrame().setLayer(GUISettings.TOP_LAYER);
            Client.getInstance().getDesktop().add(((MediaPlayer) sample).getFrame());
            if (rectangle != null) {
                ((MediaPlayer) sample).getFrame().setBounds(rectangle);
            } else {
                ((MediaPlayer) sample).getFrame().setSize(300, 300);
            }
            ((MediaPlayer) sample).getFrame().show();
        } else if (!((MediaPlayer) sample).getFrame().isVisible()) {
            ((MediaPlayer) sample).getFrame().setLayer(GUISettings.TOP_LAYER);
            Client.getInstance().getDesktop().add(((MediaPlayer) sample).getFrame());
            if (rectangle != null) {
                ((MediaPlayer) sample).getFrame().setBounds(rectangle);
            } else {
                ((MediaPlayer) sample).getFrame().setSize(300, 300);
            }
            ((MediaPlayer) sample).getFrame().show();
        }
    }

    public static void setRectangle(Rectangle rectangle, boolean destroy) {
        MediaPlayerPlugin.rectangle = rectangle;
        if (destroy && sample != null) {
            sample = null;
        }
    }

    public static String getLastSelectedURLID() {
        return urlCaption;
    }

    public static void setLastSelectedURLID(String caption) {
        MediaPlayerPlugin.urlCaption = caption;
    }

    public void load(Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void showUI(Object... params) {
        showUI();
    }

    public void save(Object... params) {

    }

    public boolean isLoaded() {
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCaption() {
        return "Media Player";  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void unload(Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void pluginEvent(PluginEvent event, Object... data) {
        if (event == PluginEvent.APPLICATION_READY_FOR_TRANSACTIONS) {
            ((MediaPlayer) sample).lordLastSelectedChanel();
        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isDataNeeded() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setData(int id, Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applyWorkspace(final String id, final String value) {
        if (id.equals(getViewSettingID())) {
            new Thread() {
                public void run() {
                    if (sample == null) {
                        sample = new MediaPlayer();
                        //((MediaPlayer) sample).showOleObject();
                    }
                    ((MediaPlayer) sample).setWorkspaceString(value);
                    if (isVisble(value)) {
                        ((MediaPlayer) sample).getFrame().setLayer(GUISettings.TOP_LAYER);
                        Client.getInstance().getDesktop().add(((MediaPlayer) sample).getFrame());
                        if (rectangle != null) {
                            ((MediaPlayer) sample).getFrame().setBounds(rectangle);
                        } else {
                            ((MediaPlayer) sample).getFrame().setSize(300, 300);
                        }
                        if (!urlCaption.equals("")) {
                            ((MediaPlayer) sample).getFrame().setTitle(Language.getString("MEDIA_PLAYER") + " - " + urlCaption);
                        } else {
                            ((MediaPlayer) sample).getFrame().setTitle(Language.getString("MEDIA_PLAYER"));
                        }
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        ((MediaPlayer) sample).getFrame().show();

                    }
                }
            }.start();
        }
    }

    public boolean isVisble(String value) {
        try {
            String[] results = value.split(";");
            boolean isVisible = Boolean.parseBoolean(results[0]);
            return isVisible;

        } catch (NumberFormatException e) {
            return false;
        }
    }

    public String getWorkspaceString(String id) {
        if (sample == null) {
            sample = new MediaPlayer();
        }
        if (id.equals(getViewSettingID())) {
            return ((MediaPlayer) sample).getWorkspaceString(rectangle);
        }
        return null;
    }

    public String getViewSettingID() {
        return "" + ViewSettingsManager.MEDIAPLAYER;
    }

    public int getWindowTypeID() {
        return Meta.IT_MEDIA_PLAYER;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getWindowTypeCategory() {
        return Meta.WINDOW_TYPE_CATEGORY_SYSTEM;
    }

    public void destroyPlayer() {
        sample = null;
    }
}
