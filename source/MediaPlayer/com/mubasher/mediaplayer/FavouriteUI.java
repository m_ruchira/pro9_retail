package com.mubasher.mediaplayer;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Mar 2, 2009
 * Time: 2:06:55 PM
 */
public class FavouriteUI extends JDialog implements ActionListener {

    private TWTextField caption;
    private TWTextField url;
    private Window owner;
    private URLComponentRecord record;

    public FavouriteUI(Window owner) {
        super(owner, ModalityType.APPLICATION_MODAL);
        this.owner = owner;
        createUI();
        GUISettings.applyOrientation(this);
    }

    public FavouriteUI(Window owner, URLComponentRecord record) {
        super(owner, ModalityType.APPLICATION_MODAL);
        this.owner = owner;
        this.record = record;
        createUI();
        caption.setText(record.getCaption());
        url.setText(record.getUrl());
        GUISettings.applyOrientation(this);
    }

    private void createUI() {
        if (record != null) {
            setTitle(Language.getString("EDIT_CHANNEL"));
        } else {
            setTitle(Language.getString("ADD_CHANNEL"));
        }

        JPanel mainPanel = new JPanel(new FlexGridLayout(new String[]{"100", "100%"}, new String[]{"23", "23", "23"}, 5, 5));
        caption = new TWTextField();
        url = new TWTextField();
        mainPanel.add(new JLabel(Language.getString("MEDIA_URL_CAPTION")));
        mainPanel.add(caption);

        mainPanel.add(new JLabel(Language.getString("MEDIA_URL")));
        JPanel urlanel = new JPanel(new FlexGridLayout(new String[]{"100%", "23"}, new String[]{"23"}, 5, 0, false, false));
        urlanel.add(url);
        JButton btnBrowse = new JButton("...");
        btnBrowse.addActionListener(this);
        btnBrowse.setActionCommand("BROWSE");
        urlanel.add(btnBrowse);
        mainPanel.add(urlanel);


        JPanel buttonPanel = new JPanel(new FlexGridLayout(new String[]{"100%", "100", "100"}, new String[]{"23"}, 5, 0, false, false));
        TWButton btnSave = new TWButton(Language.getString("SAVE"));
        btnSave.addActionListener(this);
        btnSave.setActionCommand("SAVE");
        TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);
        btnCancel.setActionCommand("CANCEL");
        buttonPanel.add(new JLabel()); // dummy
        buttonPanel.add(btnSave);
        buttonPanel.add(btnCancel);

        mainPanel.add(new JLabel()); // dummy
        mainPanel.add(buttonPanel);

        mainPanel.setPreferredSize(new Dimension(350, 89));
        add(mainPanel, BorderLayout.CENTER);
        setResizable(false);
        pack();
        GUISettings.setLocationRelativeTo(this, owner);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("BROWSE")) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(new MediaPlayerFileFilter());
            FileView fileView = new FileView() {
                public Icon getIcon(File f) {
                    try {
                        return FileSystemView.getFileSystemView().getSystemIcon(f);
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        return super.getIcon(f);
                    }
                }
            };
            fileChooser.setFileView(fileView);
            int result = fileChooser.showOpenDialog(owner);
            if (result == JFileChooser.APPROVE_OPTION) {
                url.setText(fileChooser.getSelectedFile().getAbsolutePath());
            }
        } else if (e.getActionCommand().equals("SAVE")) {
            if (record != null) { // edit mode
                if ((!url.getText().trim().equals("")) && (!caption.getText().trim().equals(""))) {
                    record.setUrl(url.getText());
                    record.setCaption(caption.getText());
                }
                dispose();
            } else { // add new mode
                if ((!caption.getText().trim().equals("")) && (!url.getText().trim().equals(""))) {
                    record = new URLComponentRecord(System.currentTimeMillis(), caption.getText(), url.getText(), false);
                    dispose();
                } else {
                    // dispose();
                }
            }
        } else {
            dispose();
        }
    }

    public URLComponentRecord getRecord() {
        return record;
    }

    private static class MediaPlayerFileFilter extends FileFilter {
        private static final String[] MOVIE_FILES_EXTENSIONS = new String[]{"mpeg", "mpg", "avi", "wmv"};
        private static final String DESCRIPTION = Language.getString("MEDIA_FILES") + " (*.mpeg, *.mpg, *.avi, *.wmv)";

        private static final String EXTENSION_SEPARATOR = ".";

        public boolean accept(File pathname) {
            if (pathname.isFile()) {
                String name = pathname.getName();
                int pos = name.lastIndexOf(EXTENSION_SEPARATOR);
                String extension = pos != -1 ? name.substring(pos + 1) : "";

                for (int i = 0; i < MOVIE_FILES_EXTENSIONS.length; i++) {
                    String movieExtension = MOVIE_FILES_EXTENSIONS[i];

                    if (movieExtension.equals(extension)) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public String getDescription() {
            return DESCRIPTION;
        }
    }
}
