package com.isi.csvr.optionstrategymaker;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Jan 26, 2009
 * Time: 3:14:11 PM
 * To change this template use File | Settings | File Templates.
 *//*----------------------------------------------------------------------------------------*
 * Parser.java version 1.0                                                    Jun 16 1996 *
 * Parser.java version 2.0                                                    Aug 25 1996 *
 * Parser.java version 2.1                                                    Oct 14 1996 *
 * Parser.java version 2.11                                                   Oct 25 1996 *
 * Parser.java version 2.2                                                    Nov  8 1996 *
 * Parser.java version 3.0                                                    May 17 1997 *
 * Parser.java version 3.01                                                   Oct 18 2001 *
 *                                                                                        *
 * Copyright (c) Yanto Suryono <yanto@fedu.uec.ac.jp>                                     *
 *                                                                                        *
 * This program is free software; you can redistribute it and/or modify it                *
 * under the terms of the GNU General Public License as published by the                  *
 * Free Software Foundation; either version 2 of the License, or (at your option)         *
 * any later version.                                                                     *
 *                                                                                        *
 * This program is distributed in the hope that it will be useful, but                    *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or          *
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for               *
 * more details.                                                                          *
 *                                                                                        *
 * You should have received a copy of the GNU General Public License along                *
 * with this program; if not, write to the Free Software Foundation, Inc.,                *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                                  *
 *                                                                                        *
 *----------------------------------------------------------------------------------------*/

import java.util.Hashtable;
import java.util.Vector;

/**
 * Indicates that an error occured in parser operation, and the operation
 * could not be completed. Used internally in <code>Parser</code> class.
 *
 * @see Parser
 */

final class ParserException extends Exception {
    private int errorcode;

    /**
     * The constructor of <code>ParserException</code>.
     *
     * @param code the error code
     */

    ParserException(int code) {
        super();
        errorcode = code;
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */

    public int getErrorCode() {
        return errorcode;
    }
}

/**
 * The class <code>Parser</code> is a mathematical expression parser.<p>
 * Example of code that uses this class:<p>
 * <p/>
 * <pre>
 * Parser parser = new Parser(1);    // creates parser with one variable
 * parser.defineVariable(1,"x");     // lets the variable be 'x'
 * parser.define("sin(x)/x");        // defines function: sin(x)/x
 * parser.parse();                   // parses the function
 *
 * // calculates: sin(x)/x with x = -5.0 .. +5.0 in 20 steps
 * // and prints the result to standard output.
 *
 * float result;
 * for (i=-10; i <= 10; i++) {
 *   parser.setVariable(1,(float)i/2.0f);
 *   result = parser.evaluate();
 *   System.out.println(result);
 * }
 * </pre>
 *
 * @author Yanto Suryono
 */

public final class Parser {

    // global variables

    /**
     * No error.
     */
    public static final int NO_ERROR = 0;
    /**
     * Syntax error.
     */
    public static final int SYNTAX_ERROR = 1;
    /**
     * Parentheses expected.
     */
    public static final int PAREN_EXPECTED = 2;
    /**
     * Attempt to evaluate an uncompiled function.
     */
    public static final int UNCOMPILED_FUNCTION = 3;
    /**
     * Expression expected.
     */
    public static final int EXPRESSION_EXPECTED = 4;
    /**
     * Unknown identifier.
     */
    public static final int UNKNOWN_IDENTIFIER = 5;
    /**
     * Operator expected.
     */
    public static final int OPERATOR_EXPECTED = 6;
    /**
     * Parenthesis mismatch.
     */
    public static final int PAREN_NOT_MATCH = 7;
    /**
     * Code damaged.
     */
    public static final int CODE_DAMAGED = 8;
    /**
     * Stack overflow.
     */
    public static final int STACK_OVERFLOW = 9;

    // variables used during parsing
    /**
     * Too many constants.
     */
    public static final int TOO_MANY_CONSTS = 10;
    /**
     * Comma expected.
     */
    public static final int COMMA_EXPECTED = 11;
    /**
     * Invalid operand.
     */
    public static final int INVALID_OPERAND = 12;
    /**
     * Invalid operator.
     */
    public static final int INVALID_OPERATOR = 13;

    // variables used during evaluating
    /**
     * No function definition to parse.
     */
    public static final int NO_FUNC_DEFINITION = 14;
    /**
     * Referenced name could not be found.
     */
    public static final int REF_NAME_EXPECTED = 15;
    private static final int MAX_NUM = 100;  // max numeric constants
    private static final int NO_FUNCS = 25;   // no. of built-in functions
    private static final int NO_EXT_FUNCS = 4;    // no. of extended functions
    private static final int STACK_SIZE = 50;   // evaluation stack size
    private static final double DEGTORAD = Math.PI / 180;

    // constants
    private static final double LOG10 = Math.log(10);
    private static final int FUNC_OFFSET = 1000;


    // references - version 3.0
    private static final int EXT_FUNC_OFFSET = FUNC_OFFSET + NO_FUNCS;
    private static final int VAR_OFFSET = 2000;

    // error codes
    private static final int REF_OFFSET = 3000;
    private static final char PI_CODE = (char) 253;
    private static final char E_CODE = (char) 254;
    private static final char NUMERIC = (char) 255;
    // Jump, followed by n : Displacement
    private static final char JUMP_CODE = (char) 1;
    // Relation less than (<)
    private static final char LESS_THAN = (char) 2;
    // Relation greater than (>)
    private static final char GREATER_THAN = (char) 3;
    // Relation less than or equal (<=)
    private static final char LESS_EQUAL = (char) 4;
    // Relation greater than or equal (>=)
    private static final char GREATER_EQUAL = (char) 5;
    // Relation not equal (<>)
    private static final char NOT_EQUAL = (char) 6;
    // Relation equal (=)
    private static final char EQUAL = (char) 7;
    // Conditional statement IF, followed by a conditional block :
    //   * Displacement (Used to jump to condition FALSE code)
    //   * Condition TRUE code
    //   * Jump to next code outside conditional block
    //   * Condition FALSE code
    //   * ENDIF
    private static final char IF_CODE = (char) 8;
    private static final char ENDIF = (char) 9;
    private static final char AND_CODE = (char) 10;    // Boolean AND
    private static final char OR_CODE = (char) 11;    // Boolean OR
    private static final char NOT_CODE = (char) 12;    // Boolean NOT

    // postfix codes
    private int var_count;             // number of variables
    private String var_name[];            // variables' name
    private double var_value[];           // value of variables
    private double number[];              // numeric constants in defined function
    private String function = "";         // function definition
    private String postfix_code = "";     // the postfix code
    private boolean valid = false;         // postfix code status
    private int error;                 // error code of last process
    private boolean ISBOOLEAN = false;     // boolean flag
    private boolean INRELATION = false;    // relation flag
    private int position;              // parsing pointer
    private int start;                 // starting position of identifier
    private int num;                   // number of numeric constants
    private char character;             // current character
    private boolean radian;               // radian unit flag
    private int numberindex;          // pointer to numbers/constants bank
    private double[] refvalue = null;      // value of references
    private Hashtable references = null;
    private Vector refnames = null;

    // built in functions
    private String funcname[] =

            {"sin", "cos", "tan", "ln", "log", "abs",
                    "int", "frac", "asin", "acos", "atan", "sinh",
                    "cosh", "tanh", "asinh", "acosh", "atanh", "ceil",
                    "floor", "round", "exp", "sqr", "sqrt", "sign", "norm"};

    // extended functions

    private String extfunc[] = {"min", "max", "mod", "atan2"};

    /**
     * The constructor of <code>Parser</code>.
     *
     * @param variablecount the number of variables
     */

    Parser(int variablecount) {
        var_count = variablecount;
        references = new Hashtable();
        refnames = new Vector();
        radian = true;

        // arrays are much faster than vectors (IMHO)

        var_name = new String[variablecount];
        var_value = new double[variablecount];
        number = new double[MAX_NUM];
    }

    /**
     * Converts error code to error string.
     *
     * @return the error string
     */

    public static String toErrorString(int errorcode) {
        String s = "";

        switch (errorcode) {
            case NO_ERROR:
                s = "no error";
                break;
            case SYNTAX_ERROR:
                s = "syntax error";
                break;
            case PAREN_EXPECTED:
                s = "parenthesis expected";
                break;
            case UNCOMPILED_FUNCTION:
                s = "uncompiled function";
                break;
            case EXPRESSION_EXPECTED:
                s = "expression expected";
                break;
            case UNKNOWN_IDENTIFIER:
                s = "unknown identifier";
                break;
            case OPERATOR_EXPECTED:
                s = "operator expected";
                break;
            case PAREN_NOT_MATCH:
                s = "parentheses not match";
                break;
            case CODE_DAMAGED:
                s = "internal code damaged";
                break;
            case STACK_OVERFLOW:
                s = "execution stack overflow";
                break;
            case TOO_MANY_CONSTS:
                s = "too many constants";
                break;
            case COMMA_EXPECTED:
                s = "comma expected";
                break;
            case INVALID_OPERAND:
                s = "invalid operand type";
                break;
            case INVALID_OPERATOR:
                s = "invalid operator";
                break;
            case NO_FUNC_DEFINITION:
                s = "bad reference definition (: expected)";
                break;
            case REF_NAME_EXPECTED:
                s = "reference name expected";
                break;
        }
        return s;
    }

    /**
     * Sets the angle unit to radian. Default upon construction.
     */

    public void useRadian() {
        radian = true;
    }

    /**
     * Sets the angle unit to degree.
     */

    public void useDegree() {
        radian = false;
    }

    /**
     * Sets the variable names.
     * Nothing happens if variable index > number of variables.
     *
     * @param index the variable index (one based)
     * @param name  the variable name
     */

    public void defineVariable(int index, String name) {
        if (index > var_count) return;
        var_name[index - 1] = name;
    }

    /**
     * Sets the variable value.
     * The variable is accessed by index.
     * Nothing happens if variable index > number of variables.
     *
     * @param index the variable index (one based)
     * @param value the variable value
     */

    public void setVariable(int index, double value) {
        if (index > var_count) return;
        var_value[index - 1] = value;
    }

    /**
     * Sets the variable value.
     * The variable is accessed by name.
     * Nothing happens if variable could not be found.
     *
     * @param name  the variable name
     * @param value the variable value
     */

    public void setVariable(String name, double value) {
        for (int i = 0; i < var_count; i++) {
            if (var_name[i].equals(name)) {
                var_value[i] = value;
                break;
            }
        }
    }

    /**
     * Defines a function. Current postfix code becomes invalid.
     *
     * @param definition the function definition
     */

    public void define(String definition) {
        function = definition;
        function.toLowerCase();
        valid = false;
    }

    /**
     * Parses defined function.
     */

    public void parse() {
        String allFunction = new String(function);
        String orgFunction = new String(function);
        int index;

        //valid = false;

        if (valid) return;
        num = 0;
        error = NO_ERROR;
        references.clear();
        refnames.removeAllElements();

        while ((index = allFunction.lastIndexOf(";")) != -1) {
            function = allFunction.substring(index + 1) + ')';
            allFunction = allFunction.substring(0, index++);

            // references are of form:   refname1:reffunc1;refname2:reffunc2;...

            String refname = null;
            int separator = function.indexOf(":");
            if (separator == -1) {
                error = NO_FUNC_DEFINITION;
                for (position = 0; position < function.length(); position++)
                    if (function.charAt(position) != ' ') break;
                position++;
            } else {
                refname = function.substring(0, separator);
                function = function.substring(separator + 1);
                refname = refname.trim();
                if (refname.equals("")) {
                    error = REF_NAME_EXPECTED;
                    position = 1;
                } else {
                    index += ++separator;
                    parseSubFunction();
                }
            }
            if (error != NO_ERROR) {
                position += index;
                break;
            } else {
                references.put(refname, postfix_code);
                refnames.addElement(refname);
            }
        }
        if (error == NO_ERROR) {
            function = allFunction + ')';
            parseSubFunction();
        }
        function = orgFunction;
        valid = (error == NO_ERROR);
    }

    /**
     * Evaluates compiled function.
     *
     * @return the result of the function
     */

    public double evaluate() {
        int size = refnames.size();
        double result;
        //valid = true;
        if (!valid) {
            error = UNCOMPILED_FUNCTION;
            return 0;
        }
        error = NO_ERROR;
        numberindex = 0;

        if (size != 0) {
            String orgPFC = postfix_code;
            refvalue = new double[size];

            for (int i = 0; i < refnames.size(); i++) {
                String name = (String) refnames.elementAt(i);
                postfix_code = (String) references.get(name);
                result = evaluateSubFunction();
                if (error != NO_ERROR) {
                    postfix_code = orgPFC;
                    refvalue = null;
                    return result;
                }
                refvalue[i] = result;
            }
            postfix_code = orgPFC;
        }
        result = evaluateSubFunction();
        refvalue = null;
        return result;
    }

    /**
     * Gets error code of last operation.
     *
     * @return the error code
     */

    public int getErrorCode() {
        return error;
    }

    /**
     * Gets error string/message of last operation.
     *
     * @return the error string
     */

    public String getErrorString() {
        return toErrorString(error);
    }

    /**
     * Gets error position. Valid only if error code != NO_ERROR
     *
     * @return error position (one based)
     */

    public int getErrorPosition() {
        return position;
    }

/*----------------------------------------------------------------------------------------*
*                            Private methods begin here                                  *
*----------------------------------------------------------------------------------------*/

    /**
     * Advances parsing pointer, skips pass all white spaces.
     *
     * @throws ParserException
     */

    private void skipSpaces() throws ParserException {
        try {
            while (function.charAt(position - 1) == ' ') position++;
            character = function.charAt(position - 1);
        } catch (StringIndexOutOfBoundsException e) {
            throw new ParserException(PAREN_NOT_MATCH);
        }
    }

    /**
     * Advances parsing pointer, gets next character.
     *
     * @throws ParserException
     */

    private void getNextCharacter() throws ParserException {
        position++;
        try {
            character = function.charAt(position - 1);
        } catch (StringIndexOutOfBoundsException e) {
            throw new ParserException(PAREN_NOT_MATCH);
        }
    }

    /**
     * Appends postfix code to compiled code.
     *
     * @param code the postfix code to append
     */

    private void addCode(char code) {
        postfix_code += code;
    }

    /**
     * Scans a number. Valid format: xxx[.xxx[e[+|-]xxx]]
     *
     * @throws ParserException
     */

    private void scanNumber() throws ParserException {
        String numstr = "";
        double value;

        if (num == MAX_NUM)
            throw new ParserException(TOO_MANY_CONSTS);
        do {
            numstr += character;
            getNextCharacter();
        } while ((character >= '0') && (character <= '9'));
        if (character == '.') {
            do {
                numstr += character;
                getNextCharacter();
            } while ((character >= '0') && (character <= '9'));
        }
        if (character == 'e') {
            numstr += character;
            getNextCharacter();
            if ((character == '+') || (character == '-')) {
                numstr += character;
                getNextCharacter();
            }
            while ((character >= '0') && (character <= '9')) {
                numstr += character;
                getNextCharacter();
            }
        }
        try {
            value = Double.valueOf(numstr).doubleValue();
        } catch (NumberFormatException e) {
            position = start;
            throw new ParserException(SYNTAX_ERROR);
        }
        number[num++] = value;
        addCode(NUMERIC);
    }

    /**
     * Scans a non-numerical identifier. Can be function call,
     * variable, reference, etc.
     *
     * @throws ParserException
     */

    private void scanNonNumeric() throws ParserException {
        String stream = "";

        if ((character == '*') || (character == '/') ||
                (character == '^') || (character == ')') ||
                (character == ',') || (character == '<') ||
                (character == '>') || (character == '=') ||
                (character == '&') || (character == '|'))
            throw new ParserException(SYNTAX_ERROR);
        do {
            stream += character;
            getNextCharacter();
        } while (!(
                (character == ' ') || (character == '+') ||
                        (character == '-') || (character == '*') ||
                        (character == '/') || (character == '^') ||
                        (character == '(') || (character == ')') ||
                        (character == ',') || (character == '<') ||
                        (character == '>') || (character == '=') ||
                        (character == '&') || (character == '|')
        ));
        if (stream.equals("pi")) {
            addCode(PI_CODE);
            return;
        } else if (stream.equals("e")) {
            addCode(E_CODE);
            return;
        }

        // if

        if (stream.equals("if")) {
            skipSpaces();
            if (character != '(')
                throw new ParserException(PAREN_EXPECTED);
            scanAndParse();
            if (character != ',')
                throw new ParserException(COMMA_EXPECTED);
            addCode(IF_CODE);
            String savecode = new String(postfix_code);
            postfix_code = "";
            scanAndParse();
            if (character != ',')
                throw new ParserException(COMMA_EXPECTED);
            addCode(JUMP_CODE);
            savecode += (char) (postfix_code.length() + 2);
            savecode += postfix_code;
            postfix_code = "";
            scanAndParse();
            if (character != ')')
                throw new ParserException(PAREN_EXPECTED);
            savecode += (char) (postfix_code.length() + 1);
            savecode += postfix_code;
            postfix_code = new String(savecode);
            getNextCharacter();
            return;
        }

        // built-in function

        for (int i = 0; i < NO_FUNCS; i++) {
            if (stream.equals(funcname[i])) {
                skipSpaces();
                if (character != '(')
                    throw new ParserException(PAREN_EXPECTED);
                scanAndParse();
                if (character != ')')
                    throw new ParserException(PAREN_EXPECTED);
                getNextCharacter();
                addCode((char) (i + FUNC_OFFSET));
                return;
            }
        }

        // extended functions

        for (int i = 0; i < NO_EXT_FUNCS; i++) {
            if (stream.equals(extfunc[i])) {
                skipSpaces();
                if (character != '(')
                    throw new ParserException(PAREN_EXPECTED);
                scanAndParse();
                if (character != ',')
                    throw new ParserException(COMMA_EXPECTED);
                String savecode = new String(postfix_code);
                postfix_code = "";
                scanAndParse();
                if (character != ')')
                    throw new ParserException(PAREN_EXPECTED);
                getNextCharacter();
                savecode += postfix_code;
                postfix_code = new String(savecode);
                addCode((char) (i + EXT_FUNC_OFFSET));
                return;
            }
        }

        // registered variables

        for (int i = 0; i < var_count; i++) {
            if (stream.equals(var_name[i])) {
                addCode((char) (i + VAR_OFFSET));
                return;
            }
        }

        // references

        int index = refnames.indexOf(stream);
        if (index != -1) {
            addCode((char) (index + REF_OFFSET));
            return;
        }

        position = start;
        throw new ParserException(UNKNOWN_IDENTIFIER);
    }

    /**
     * Gets an identifier starting from current parsing pointer.
     *
     * @return whether the identifier should be negated
     * @throws ParserException
     */

    private boolean getIdentifier() throws ParserException {
        boolean negate = false;

        getNextCharacter();
        skipSpaces();

        if (character == '!') {
            getNextCharacter();
            skipSpaces();
            if (character != '(')
                throw new ParserException(PAREN_EXPECTED);
            scanAndParse();
            if (character != ')')
                throw new ParserException(PAREN_EXPECTED);
            if (!ISBOOLEAN)
                throw new ParserException(INVALID_OPERAND);
            addCode(NOT_CODE);
            getNextCharacter();
            return false;
        }
        ISBOOLEAN = false;
        while ((character == '+') || (character == '-')) {
            if (character == '-') negate = !negate;
            getNextCharacter();
            skipSpaces();
        }
        start = position;
        if ((character >= '0') && (character <= '9'))
            scanNumber();
        else if (character == '(') {
            scanAndParse();
            getNextCharacter();
        } else scanNonNumeric();
        skipSpaces();
        return (negate);
    }

    /**
     * Scans arithmetic level 3 (highest). Power arithmetics.
     *
     * @throws ParserException
     */

    private void arithmeticLevel3() throws ParserException {
        boolean negate;

        if (ISBOOLEAN)
            throw new ParserException(INVALID_OPERAND);
        negate = getIdentifier();
        if (ISBOOLEAN)
            throw new ParserException(INVALID_OPERAND);
        if (character == '^') arithmeticLevel3();
        addCode('^');
        if (negate) addCode('_');
    }

    /**
     * Scans arithmetic level 2. Multiplications and divisions.
     *
     * @throws ParserException
     */

    private void arithmeticLevel2() throws ParserException {
        boolean negate;

        if (ISBOOLEAN)
            throw new ParserException(INVALID_OPERAND);
        do {
            char operator = character;
            negate = getIdentifier();
            if (ISBOOLEAN)
                throw new ParserException(INVALID_OPERAND);
            if (character == '^') arithmeticLevel3();
            if (negate) addCode('_');
            addCode(operator);
        } while ((character == '*') || (character == '/'));
    }

    /**
     * Scans arithmetic level 1 (lowest).
     * Additions and substractions.
     *
     * @throws ParserException
     */

    private void arithmeticLevel1() throws ParserException {
        boolean negate;

        if (ISBOOLEAN)
            throw new ParserException(INVALID_OPERAND);
        do {
            char operator = character;
            negate = getIdentifier();
            if (ISBOOLEAN)
                throw new ParserException(INVALID_OPERAND);
            if (character == '^') {
                arithmeticLevel3();
                if (negate) addCode('_');
            } else if ((character == '*') || (character == '/')) {
                if (negate) addCode('_');
                arithmeticLevel2();
            }
            addCode(operator);
        } while ((character == '+') || (character == '-'));
    }

    /**
     * Scans relation level.
     *
     * @throws ParserException
     */

    private void relationLevel() throws ParserException {
        char code = (char) 0;

        if (INRELATION)
            throw new ParserException(INVALID_OPERATOR);
        INRELATION = true;
        if (ISBOOLEAN)
            throw new ParserException(INVALID_OPERAND);
        switch (character) {
            case '=':
                code = EQUAL;
                break;
            case '<':
                code = LESS_THAN;
                getNextCharacter();
                if (character == '>') code = NOT_EQUAL;
                else if (character == '=') code = LESS_EQUAL;
                else position--;
                break;
            case '>':
                code = GREATER_THAN;
                getNextCharacter();
                if (character == '=') code = GREATER_EQUAL;
                else position--;
                break;
        }
        scanAndParse();
        INRELATION = false;
        if (ISBOOLEAN)
            throw new ParserException(INVALID_OPERAND);
        addCode(code);
        ISBOOLEAN = true;
    }

    /**
     * Scans boolean level.
     *
     * @throws ParserException
     */

    private void booleanLevel() throws ParserException {
        if (!ISBOOLEAN)
            throw new ParserException(INVALID_OPERAND);
        char operator = character;
        scanAndParse();
        if (!ISBOOLEAN)
            throw new ParserException(INVALID_OPERAND);
        switch (operator) {
            case '&':
                addCode(AND_CODE);
                break;
            case '|':
                addCode(OR_CODE);
                break;
        }
    }

    /**
     * Main method of scanning and parsing process.
     *
     * @throws ParserException
     */

    private void scanAndParse() throws ParserException {
        boolean negate;

        negate = getIdentifier();
        if ((character != '^') && (negate)) addCode('_');

        do {
            switch (character) {
                case '+':
                case '-':
                    arithmeticLevel1();
                    break;
                case '*':
                case '/':
                    arithmeticLevel2();
                    break;
                case '^':
                    arithmeticLevel3();
                    if (negate) addCode('_');
                    break;
                case ',':
                case ')':
                    return;

                case '=':
                case '<':
                case '>':
                    relationLevel();
                    break;
                case '&':
                case '|':
                    booleanLevel();
                    break;
                default:
                    throw new ParserException(OPERATOR_EXPECTED);
            }
        } while (true);
    }

    /**
     * Parses subfunction.
     */

    private void parseSubFunction() {
        position = 0;
        postfix_code = "";
        INRELATION = false;
        ISBOOLEAN = false;
        try {
            scanAndParse();
        } catch (ParserException e) {
            error = e.getErrorCode();
            if ((error == SYNTAX_ERROR) && (postfix_code == ""))
                error = EXPRESSION_EXPECTED;
        }
        if ((error == NO_ERROR) &&
                (position != function.length())) error = PAREN_NOT_MATCH;
    }

    /**
     * Built-in one parameter function call.
     *
     * @param function  the function index
     * @param parameter the parameter to the function
     * @return the function result
     */

    private double builtInFunction(int function, double parameter) {
        switch (function) {
            case 0:
                if (radian)
                    return Math.sin(parameter);
                else
                    return Math.sin(parameter * DEGTORAD);
            case 1:
                if (radian)
                    return Math.cos(parameter);
                else
                    return Math.cos(parameter * DEGTORAD);
            case 2:
                if (radian)
                    return Math.tan(parameter);
                else
                    return Math.tan(parameter * DEGTORAD);
            case 3:
                return Math.log(parameter);
            case 4:
                return Math.log(parameter) / LOG10;
            case 5:
                return Math.abs(parameter);
            case 6:
                return Math.rint(parameter);
            case 7:
                return parameter - Math.rint(parameter);
            case 8:
                if (radian)
                    return Math.asin(parameter);
                else
                    return Math.asin(parameter) / DEGTORAD;
            case 9:
                if (radian)
                    return Math.acos(parameter);
                else
                    return Math.acos(parameter) / DEGTORAD;
            case 10:
                if (radian)
                    return Math.atan(parameter);
                else
                    return Math.atan(parameter) / DEGTORAD;
            case 11:
                return (Math.exp(parameter) - Math.exp(-parameter)) / 2;
            case 12:
                return (Math.exp(parameter) + Math.exp(-parameter)) / 2;
            case 13:
                double a = Math.exp(parameter);
                double b = Math.exp(-parameter);
                return (a - b) / (a + b);
            case 14:
                return Math.log(parameter + Math.sqrt(parameter * parameter + 1));
            case 15:
                return Math.log(parameter + Math.sqrt(parameter * parameter - 1));
            case 16:
                return Math.log((1 + parameter) / (1 - parameter)) / 2;
            case 17:
                return Math.ceil(parameter);
            case 18:
                return Math.floor(parameter);
            case 19:
                return Math.round(parameter);
            case 20:
                return Math.exp(parameter);
            case 21:
                return parameter * parameter;
            case 22:
                return Math.sqrt(parameter);
            case 23:
                if (parameter == 0.0d)
                    return 0;
                else if (parameter > 0.0d)
                    return 1;
                else
                    return -1;
            case 24:
                //System.out.println("******* parameter ******* "+parameter);

                return normp(parameter);
            default:
                error = CODE_DAMAGED;
                return Double.NaN;
        }
    }

    public double normp(double z) {

        double zabs;
        double p;
        double expntl, pdf;

        final double p0 = 220.2068679123761;
        final double p1 = 221.2135961699311;
        final double p2 = 112.0792914978709;
        final double p3 = 33.91286607838300;
        final double p4 = 6.373962203531650;
        final double p5 = .7003830644436881;
        final double p6 = .3526249659989109E-01;

        final double q0 = 440.4137358247522;
        final double q1 = 793.8265125199484;
        final double q2 = 637.3336333788311;
        final double q3 = 296.5642487796737;
        final double q4 = 86.78073220294608;
        final double q5 = 16.06417757920695;
        final double q6 = 1.755667163182642;
        final double q7 = .8838834764831844E-1;

        final double cutoff = 7.071;
        final double root2pi = 2.506628274631001;

        zabs = Math.abs(z);

//  |z| > 37

        if (z > 37.0) {

            p = 1.0;

            return p;

        }

        if (z < -37.0) {

            p = 0.0;

            return p;

        }

//  |z| <= 37.

        expntl = Math.exp(-.5 * zabs * zabs);

        pdf = expntl / root2pi;

//  |z| < cutoff = 10/sqrt(2).

        if (zabs < cutoff) {

            p = expntl * ((((((p6 * zabs + p5) * zabs + p4) * zabs + p3) * zabs +
                    p2) * zabs + p1) * zabs + p0) / (((((((q7 * zabs + q6) * zabs +
                    q5) * zabs + q4) * zabs + q3) * zabs + q2) * zabs + q1) * zabs +
                    q0);

        } else {

            p = pdf / (zabs + 1.0 / (zabs + 2.0 / (zabs + 3.0 / (zabs + 4.0 /
                    (zabs + 0.65)))));

        }

        if (z < 0.0) {

            return p;

        } else {

            p = 1.0 - p;

            return p;

        }

    }

    /**
     * Built-in two parameters extended function call.
     *
     * @param function the function index
     * @param param1   the first parameter to the function
     * @param param2   the second parameter to the function
     * @return the function result
     */

    private double builtInExtFunction(int function, double param1, double param2) {
        switch (function) {
            case 0:
                return Math.min(param1, param2);
            case 1:
                return Math.max(param1, param2);
            case 2:
                return Math.IEEEremainder(param1, param2);
            case 3:
                return Math.atan2(param1, param2);
            default:
                error = CODE_DAMAGED;
                return Double.NaN;
        }
    }

    /**
     * Evaluates subfunction.
     *
     * @return the result of the subfunction
     */

    private double evaluateSubFunction() {
        double stack[];
        int stack_pointer = -1;
        int code_pointer = 0;
        int destination;
        char code;

        stack = new double[STACK_SIZE];
        while (true) {
            try {
                code = postfix_code.charAt(code_pointer++);
            } catch (StringIndexOutOfBoundsException e) {
                return stack[0];
            }
            try {
                switch (code) {
                    case '+':
                        stack[stack_pointer - 1] += stack[stack_pointer];
                        stack_pointer--;
                        break;
                    case '-':
                        stack[stack_pointer - 1] -= stack[stack_pointer];
                        stack_pointer--;
                        break;
                    case '*':
                        stack[stack_pointer - 1] *= stack[stack_pointer];
                        stack_pointer--;
                        break;
                    case '/':
                        stack[stack_pointer - 1] /= stack[stack_pointer];
                        stack_pointer--;
                        break;
                    case '^':
                        stack[stack_pointer - 1] = Math.pow(stack[stack_pointer - 1],
                                stack[stack_pointer]);
                        stack_pointer--;
                        break;
                    case '_':
                        stack[stack_pointer] = -stack[stack_pointer];
                        break;

                    case JUMP_CODE:
                        destination = code_pointer +
                                (int) postfix_code.charAt(code_pointer++);
                        while (code_pointer < destination) {
                            if (postfix_code.charAt(
                                    code_pointer++) == NUMERIC) numberindex++;
                        }
                        break;
                    case LESS_THAN:
                        stack_pointer--;
                        stack[stack_pointer] =
                                (stack[stack_pointer] <
                                        stack[stack_pointer + 1]) ? 1.0 : 0.0;
                        break;
                    case GREATER_THAN:
                        stack_pointer--;
                        stack[stack_pointer] =
                                (stack[stack_pointer] >
                                        stack[stack_pointer + 1]) ? 1.0 : 0.0;
                        break;
                    case LESS_EQUAL:
                        stack_pointer--;
                        stack[stack_pointer] =
                                (stack[stack_pointer] <=
                                        stack[stack_pointer + 1]) ? 1.0 : 0.0;
                        break;
                    case GREATER_EQUAL:
                        stack_pointer--;
                        stack[stack_pointer] =
                                (stack[stack_pointer] >=
                                        stack[stack_pointer + 1]) ? 1.0 : 0.0;
                        break;
                    case EQUAL:
                        stack_pointer--;
                        stack[stack_pointer] =
                                (stack[stack_pointer] ==
                                        stack[stack_pointer + 1]) ? 1.0 : 0.0;
                        break;
                    case NOT_EQUAL:
                        stack_pointer--;
                        stack[stack_pointer] =
                                (stack[stack_pointer] !=
                                        stack[stack_pointer + 1]) ? 1.0 : 0.0;
                        break;
                    case IF_CODE:
                        if (stack[stack_pointer--] == 0.0) {
                            destination = code_pointer +
                                    (int) postfix_code.charAt(code_pointer++);
                            while (code_pointer < destination) {
                                if (postfix_code.charAt(
                                        code_pointer++) == NUMERIC) numberindex++;
                            }
                        } else code_pointer++;
                        break;
                    case ENDIF:
                        break;  // same as NOP
                    case AND_CODE:
                        stack_pointer--;
                        if ((stack[stack_pointer] != 0.0) &&
                                (stack[stack_pointer + 1] != 0.0))
                            stack[stack_pointer] = 1.0;
                        else
                            stack[stack_pointer] = 0.0;
                        break;
                    case OR_CODE:
                        stack_pointer--;
                        if ((stack[stack_pointer] != 0.0) ||
                                (stack[stack_pointer + 1] != 0.0))
                            stack[stack_pointer] = 1.0;
                        else
                            stack[stack_pointer] = 0.0;
                        break;
                    case NOT_CODE:
                        stack[stack_pointer] =
                                (stack[stack_pointer] == 0.0) ? 1.0 : 0.0;
                        break;

                    case NUMERIC:
                        stack[++stack_pointer] = number[numberindex++];
                        break;
                    case PI_CODE:
                        stack[++stack_pointer] = Math.PI;
                        break;
                    case E_CODE:
                        stack[++stack_pointer] = Math.E;
                        break;

                    default:
                        if ((int) code >= REF_OFFSET)
                            stack[++stack_pointer] = refvalue[(int) code - REF_OFFSET];
                        else if ((int) code >= VAR_OFFSET)
                            stack[++stack_pointer] = var_value[(int) code - VAR_OFFSET];
                        else if ((int) code >= EXT_FUNC_OFFSET) {
                            stack[stack_pointer - 1] =
                                    builtInExtFunction((int) code - EXT_FUNC_OFFSET,
                                            stack[stack_pointer - 1],
                                            stack[stack_pointer]);
                            stack_pointer--;
                        } else if ((int) code >= FUNC_OFFSET) {
                            stack[stack_pointer] =
                                    builtInFunction((int) code - FUNC_OFFSET, stack[stack_pointer]);
                        } else {
                            error = CODE_DAMAGED;
                            return Double.NaN;
                        }
                }
            } catch (ArrayIndexOutOfBoundsException oe) {
                error = STACK_OVERFLOW;
                return Double.NaN;
            } catch (NullPointerException ne) {
                error = CODE_DAMAGED;
                return Double.NaN;
            }
        }
    }
}

