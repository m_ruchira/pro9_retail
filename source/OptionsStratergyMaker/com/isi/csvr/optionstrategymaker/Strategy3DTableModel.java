package com.isi.csvr.optionstrategymaker;

import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Jan 19, 2009
 * Time: 10:27:14 AM
 * To change this template use File | Settings | File Templates.
 */

public class Strategy3DTableModel extends CommonTable implements TableModel, CommonTableInterface {

    ArrayList<Strategy3DRow> objectStore;
    DoubleTransferObject doubleObj = new DoubleTransferObject();

    public Strategy3DTableModel(ArrayList<Strategy3DRow> store) {
        this.objectStore = store;
    }

    public int getRowCount() {
        return objectStore.size();
    }

    public int getColumnCount() {

        return getViewSettings().getColumnHeadings().length;
    }


    public Object getValueAt(int rowIndex, int columnIndex) {

        if (objectStore.size() == 0) {
            return "";
        }

        Strategy3DRow rowObject = objectStore.get(rowIndex);

        if (rowObject != null) {
            switch (columnIndex) {
                case 0:
                    return new String(rowObject.getIncrement() + "%");
                case 1:
                    //return  doubleObj.setValue(rowObject.getStrikePrice());
                    return rowObject.getStrikePrice();
                default:
                    double[] dates = rowObject.getDates();
                    return dates[columnIndex - 2];

            }
        }

        return null;
    }

    public Class getColumnClass(int col) {
        if (col == 0) {
            return String.class;
        } else {
            return Float.class;
        }
    }

    public String getColumnName(int columnIndex) {
        return getViewSettings().getColumnHeadings()[columnIndex];

    }

    public void setSymbol(String symbol) {

    }

    public void setObjectStore(ArrayList<Strategy3DRow> objectStore) {
        this.objectStore = objectStore;
    }
}
