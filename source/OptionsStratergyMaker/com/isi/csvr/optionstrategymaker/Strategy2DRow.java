package com.isi.csvr.optionstrategymaker;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jan 15, 2009
 * Time: 8:53:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class Strategy2DRow {

    private int increment;
    private double stockPrice;
    private double calcDate;
    private double expDate;

    public Strategy2DRow(int increment, double stockPrice, double calcDate, double expDate) {
        this.increment = increment;
        this.stockPrice = stockPrice;
        this.calcDate = calcDate;
        this.expDate = expDate;
    }

    public int getIncrement() {
        return increment;
    }

    public double getStockPrice() {
        return stockPrice;
    }

    public double getCalcDate() {
        return calcDate;
    }

    public void setCalcDate(double calcDate) {
        this.calcDate = calcDate;
    }

    public double getExpDate() {
        return expDate;
    }

    public void setExpDate(double expDate) {
        this.expDate = expDate;
    }
}
