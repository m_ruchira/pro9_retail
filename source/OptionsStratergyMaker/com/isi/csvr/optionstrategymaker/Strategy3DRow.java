package com.isi.csvr.optionstrategymaker;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Jan 19, 2009
 * Time: 2:48:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class Strategy3DRow {

    private int increment;
    private double strikePrice;
    private double[] dates;

    public Strategy3DRow(int increment, double strikePrice, double[] dates) {
        this.increment = increment;
        this.strikePrice = strikePrice;
        this.dates = dates;
    }

    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }

    public double getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public double[] getDates() {
        return dates;
    }

    public void setDates(double[] dates) {
        this.dates = dates;
    }
}
