package com.isi.csvr.optionstrategymaker;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.Client;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;
import com.mubasher.csvr.optionscalculator.BinomialCalculator;
import com.mubasher.csvr.optionscalculator.BlackAndScholes;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;


/**
 * Created by IntelliJ IDEA.
 * User: hasika
 * Changed by : charithn
 * Date: Jan 13, 2009
 * Time: 2:34:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptionStrategyMakerUI extends InternalFrame implements ActionListener, TWTabbedPaleListener, Themeable, ComponentListener, ChangeListener {

    private static final int MAX_DAYS_ALLOWED = 14;
    protected static long currentDayInMillis;
    private static OptionStrategyMakerUI self;
    private final String ID_BOTTOM_2D = "2d"; //using for card layouts
    private final String ID_BOTTOM_3D = "3d";
    private final int WINDOW_HEIGHT = 580;
    private final int WINDOW_WIDTH = 1000;
    private final int SPLIT_PANE_DIVIDER_SIZE = 3;
    private final String N_A_STRING = "N/A";
    private final float FACTOR = 1.5f;
    private final byte GREEK_VALUE_DELTA = 0;
    private final byte GREEK_VALUE_GAMMA = 1;
    private final byte GREEK_VALUE_THETA = 2;
    private final byte GREEK_VALUE_VEGA = 3;
    private final byte GREEK_VALUE_RHO = 4;
    private final int MAXIMUM_TICKS = 10;
    public long referenceDay = -1;
    JSplitPane splitPane2DBottom;
    JSplitPane splitPane3DBottom;
    BinomialCalculator.EnumCallPut tempEnum = null;
    private JPanel bottomPanel;
    private JPanel bottom2DPanel;
    private JPanel bottom3DPanel;
    private JPanel topSymbolsPannel;
    private TWComboBox cmbGreeks1;
    private TWComboBox cmbGreeks2;
    private TWComboBox cmbGreeks3;
    private TWComboBox cmbGreeks4;
    private JLabel lblTheoPrice1;
    private JLabel lblTheoPrice2;
    private JLabel lblTheoPrice3;
    private JLabel lblTheoPrice4;
    private JLabel lblGreeks1;
    private JLabel lblGreeks2;
    private JLabel lblGreeks3;
    private JLabel lblGreeks4;
    private JSpinner spnNoOfPoints;
    private JSpinner spnGraphIncrement;
    private JPanel pnl2DXAxisHeader;
    private JLabel lbl2DXAxis;
    private JPanel pnl3DXAxisHeader;
    private JLabel lbl3DXAxis;
    private JPanel pnlzAxisHeader;
    private JLabel lblZAxis;
    private TWTextField txtRFRate2D;
    private TWTextField txtVolatility2D;
    private TWTextField txtRFRate3D;
    private TWTextField txtVolatility3D;
    private TWTabbedPane tabpane;
    private SimpleLineChart individualChart;
    private SimpleLineChart combinedChart;
    private JSlider slider;
    private Table table2D;
    private Strategy2DTableModel model2D;
    private ArrayList<Strategy2DRow> objectStore;
    private CustomButton btnDraw;
    private JRadioButton btnAbs;
    private JRadioButton btnPct;
    private OptionsDataPanel dataPanel1;
    private OptionsDataPanel dataPanel2;
    private OptionsDataPanel dataPanel3;
    private OptionsDataPanel dataPanel4;
    private Stock stock;
    private DataManager dataManager;
    private Table table3D;
    private Strategy3DTableModel model3D;
    private ArrayList<Strategy3DRow> objectStore3D;
    private CustomButton btnDraw3D;
    private JRadioButton btnAbs3D;
    private JRadioButton btnPct3D;
    private JSpinner spnNoOfPoints3D;
    private JSpinner spnGraphIncrement3D;
    private JSpinner spnNoOfDays;
    private double[] xStrikePrice3D;
    private double[] yProfit3D;
    private double[] zDates3D;
    private Thread graph3DThread;
    private Simple3DChart canvas;
    private Thread surfaceThread;
    private int intervalDates;
    private JPanel panel3Dtable = null;
    private JPanel panel3DChart;
    private TWDecimalFormat greekFormatter = null;
    private TWDecimalFormat volFormatter = null;
    private JCheckBox chkUseStockPrice2D;
    private JCheckBox chkUseStockPrice3D;
    private boolean panel_1_valid = false;
    private boolean panel_2_valid = false;
    private boolean panel_3_valid = false;
    private boolean panel_4_valid = false;
    private byte option1 = 0;
    private byte option2 = 0;
    private byte option3 = 0;
    private byte option4 = 0;
    private double strikePrice1 = 1000;
    private double strikePrice2 = 1000;
    private double strikePrice3 = 1000;
    private double strikePrice4 = 1000;
    private double optionPrice1 = 100;
    private double optionPrice2 = 100;
    private double optionPrice3 = 100;
    private double optionPrice4 = 100;
    private int graphIncrement;
    private int numberOfPoints;
    private int graphIncrement3D;
    private int numberOfPoints3D;
    private long expDate1;
    private long expDate2;
    private long expDate3;
    private long expDate4;
    private long minExpDate = Long.MAX_VALUE;
    private double riskFreeRate = 7;
    private ValueFormatter rfRateFormatter = new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL, 6L, 2);
    private ValueFormatter volatilityFormatter = new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL, 6L, 2);
    private int qunatity1 = 1;
    private int qunatity2 = 1;
    private int qunatity3 = 1;
    private int qunatity4 = 1;
    private double impVolatlity = 30;
    private double centerPrice = 100;
    private BlackAndScholes blackCalculator = null;
    private BinomialCalculator coxRossCalculator = null;
    private double minPrice = Float.MAX_VALUE;
    private double maxPrice = -Float.MIN_VALUE;
    private int noOfPointsToPlot = 0;
    private int noOfDummyPoints = 0;
    private int calc_divisions = 0;
    private int noOfSPValues = 0; // x axis
    private int noOfProfitValues = 0; //y axis
    private int incrementInDays = 1; //z axis
    private Color labelBorderColor = Color.BLACK;
    private Color option1Color = Color.BLUE;
    private Color option2Color = Color.RED;
    private Color option3Color = Color.ORANGE;
    private Color option4Color = Color.GREEN;
    private Color calcChartColor = new Color(4, 200, 104);
    private Color expChartColor = new Color(204, 0, 204);
    private Parser parser1;
    private float minProfit = Float.MAX_VALUE;
    private float maxProfit = -Float.MIN_VALUE;
    private float minProfitSurface = Float.MAX_VALUE;
    private float maxProfitSurface = -Float.MIN_VALUE;
    private TWDateFormat dateFormatter;
    private double[] greekValues = new double[5];
    private boolean isLoadingEqVol = false;
    private int count = 0;
    private int calcMethod = Constants.BLACK_SCHOLES;
    private Stock underlyingEquity;
    private boolean draw3DChart = true;
    private boolean draw2DChart = true;
    private double currentPrice1 = 0.0;
    private double currentPrice2 = 0.0;
    private double currentPrice3 = 0.0;
    private double currentPrice4 = 0.0;
    private JSplitPane paneChart;

    /*private JPanel create2DChartPanel() {
            JPanel panel2D = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 2, 2));
            individualChart = new SimpleLineChart(SimpleLineChart.CHART_TYPE_INDIVIDIAL);
            combinedChart = new SimpleLineChart(SimpleLineChart.CHART_TYPE_COMBINE);
            paneChart = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, combinedChart, individualChart);

            paneChart.setDividerLocation(0.5);
            paneChart.setDividerSize(SPLIT_PANE_DIVIDER_SIZE);
            panel2D.add(paneChart);

            slider = new JSlider(JSlider.HORIZONTAL);
            slider.setFont(new Font("Arial", Font.BOLD, 12));
            slider.setForeground(Color.BLACK);
            slider.setPaintLabels(true);
            slider.setPaintTicks(true);
            slider.setVisible(false);
            slider.addChangeListener(this);

            JPanel panelBottom = new JPanel(new FlexGridLayout(new String[]{"53%"}, new String[]{"100%"}, 2, 0));
            panelBottom.add(slider);

            JPanel panelall = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"87%", "13%"}, 0, 0));
            panelall.add(panel2D);
            panelall.add(panelBottom);

            return panelall;
        }*/
    private double[] PRICES;
    private TWDateFormat formatter = new TWDateFormat("yyyy MMM dd");
    private TWDateFormat sliderDateFormatter = new TWDateFormat("yy MMM dd");
    private int TOTAL_DAYS = 2;
    private int NO_OF_MAJOR_POINTS = 6;
    private boolean volatilityChanged = false;
    private boolean canContinue = true;

    private OptionStrategyMakerUI() {

        initializeParams();
        createUI();

        this.setTitle(Language.getString("STRATEGY_TITLE"));
        this.hideTitleBarMenu();
        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setClosable(true);
        this.setLayer(GUISettings.TOP_LAYER);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        applyTheme();
        try {
            this.setMaximum(true);
        } catch (PropertyVetoException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static OptionStrategyMakerUI getSharedInstance() {

        if (self == null) {
            self = new OptionStrategyMakerUI();
        }
        return self;
    }

    /*======================================== UI related =======================================*/
    private void initializeParams() {

        blackCalculator = new BlackAndScholes();
        coxRossCalculator = new BinomialCalculator();
        dataManager = DataManager.getSharedInstance();

        table2D = new Table();
        table3D = new Table();

        chkUseStockPrice2D = new JCheckBox(Language.getString("STRATEGY_STOCK_CENTER"));
        chkUseStockPrice3D = new JCheckBox(Language.getString("STRATEGY_STOCK_CENTER"));

        btnAbs = new JRadioButton(Language.getString("STRATEGY_ABSOLUTE"));
        btnPct = new JRadioButton(Language.getString("STRATEGY_PCT"));
        btnAbs3D = new JRadioButton(Language.getString("STRATEGY_ABSOLUTE"));
        btnPct3D = new JRadioButton(Language.getString("STRATEGY_PCT"));

        cmbGreeks1 = getGreekCombo();
        cmbGreeks2 = getGreekCombo();
        cmbGreeks3 = getGreekCombo();
        cmbGreeks4 = getGreekCombo();

        lblGreeks1 = new JLabel();
        lblGreeks2 = new JLabel();
        lblGreeks3 = new JLabel();
        lblGreeks4 = new JLabel();

        lblTheoPrice1 = new JLabel();
        lblTheoPrice2 = new JLabel();
        lblTheoPrice3 = new JLabel();
        lblTheoPrice4 = new JLabel();

        lblTheoPrice1.setBorder(BorderFactory.createLineBorder(labelBorderColor));
        lblTheoPrice2.setBorder(BorderFactory.createLineBorder(labelBorderColor));
        lblTheoPrice3.setBorder(BorderFactory.createLineBorder(labelBorderColor));
        lblTheoPrice4.setBorder(BorderFactory.createLineBorder(labelBorderColor));

        lblGreeks1.setBorder(BorderFactory.createLineBorder(labelBorderColor));
        lblGreeks2.setBorder(BorderFactory.createLineBorder(labelBorderColor));
        lblGreeks3.setBorder(BorderFactory.createLineBorder(labelBorderColor));
        lblGreeks4.setBorder(BorderFactory.createLineBorder(labelBorderColor));

        lblTheoPrice1.setHorizontalAlignment(JTextField.RIGHT);
        lblTheoPrice2.setHorizontalAlignment(JTextField.RIGHT);
        lblTheoPrice3.setHorizontalAlignment(JTextField.RIGHT);
        lblTheoPrice4.setHorizontalAlignment(JTextField.RIGHT);

        lblGreeks1.setHorizontalAlignment(JTextField.RIGHT);
        lblGreeks2.setHorizontalAlignment(JTextField.RIGHT);
        lblGreeks3.setHorizontalAlignment(JTextField.RIGHT);
        lblGreeks4.setHorizontalAlignment(JTextField.RIGHT);

        dataPanel1 = new OptionsDataPanel(OptionsDataPanel.PANEL_ID_1);
        dataPanel2 = new OptionsDataPanel(OptionsDataPanel.PANEL_ID_2);
        dataPanel3 = new OptionsDataPanel(OptionsDataPanel.PANEL_ID_3);
        dataPanel4 = new OptionsDataPanel(OptionsDataPanel.PANEL_ID_4);

        btnDraw = new CustomButton(Language.getString("STRATEGY_DRAW"));

        greekFormatter = new TWDecimalFormat("###,###,##0.000000");
        volFormatter = new TWDecimalFormat("###,###,##0.00");
        dateFormatter = new TWDateFormat("dd-MM-yyyy");

        objectStore = new ArrayList<Strategy2DRow>();
        objectStore3D = new ArrayList<Strategy3DRow>();

        currentDayInMillis = getCurrentDayInMills();
    }

    private void createUI() {

        cmbGreeks1.setEnabled(false);
        cmbGreeks2.setEnabled(false);
        cmbGreeks3.setEnabled(false);
        cmbGreeks4.setEnabled(false);

        cmbGreeks1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {

                if (panel_1_valid && calcMethod == Constants.BLACK_SCHOLES) {
                    loadGreeks(dataPanel1);
                    cmbGreeks1.setEnabled(true);
                    byte index = (byte) cmbGreeks1.getSelectedIndex();
                    lblGreeks1.setText(greekFormatter.format(greekValues[index]));
                }
            }
        });

        cmbGreeks2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {

                if (panel_2_valid && calcMethod == Constants.BLACK_SCHOLES) {
                    loadGreeks(dataPanel2);
                    cmbGreeks2.setEnabled(true);
                    byte index = (byte) cmbGreeks2.getSelectedIndex();
                    lblGreeks2.setText(greekFormatter.format(greekValues[index]));
                }
            }
        });

        cmbGreeks3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {

                if (panel_3_valid && calcMethod == Constants.BLACK_SCHOLES) {
                    loadGreeks(dataPanel3);
                    cmbGreeks3.setEnabled(true);
                    byte index = (byte) cmbGreeks3.getSelectedIndex();
                    lblGreeks3.setText(greekFormatter.format(greekValues[index]));
                }
            }
        });

        cmbGreeks4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {

                if (panel_4_valid && calcMethod == Constants.BLACK_SCHOLES) {
                    loadGreeks(dataPanel4);
                    cmbGreeks4.setEnabled(true);
                    byte index = (byte) cmbGreeks4.getSelectedIndex();
                    lblGreeks4.setText(greekFormatter.format(greekValues[index]));
                }
            }
        });

//        JPanel mainPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"260", "100%"}, 2, 2, true, true));    //top panel height is fixed to 260
        JPanel mainPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 2, 2, true, true));

        bottom2DPanel = create2DBottomPanel();
        bottom3DPanel = create3DBottomPanel();

        bottomPanel = new JPanel(new CardLayout());
        bottomPanel.add(bottom2DPanel, ID_BOTTOM_2D);
        bottomPanel.add(bottom3DPanel, ID_BOTTOM_3D);

        JPanel commonTopPanel = createCommonTopPanel();

        JSplitPane sPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, commonTopPanel, bottomPanel);
        sPane.setDividerLocation(240);
        sPane.setDividerSize(SPLIT_PANE_DIVIDER_SIZE);
        mainPanel.add(sPane);

        this.add(mainPanel);
        addActionListenrs();
    }

    private JPanel create2DChartPanel() {
        JPanel panel2D = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 2, 2));
        individualChart = new SimpleLineChart(SimpleLineChart.CHART_TYPE_INDIVIDIAL);
        combinedChart = new SimpleLineChart(SimpleLineChart.CHART_TYPE_COMBINE);

        panel2D.add((combinedChart));
        //panel2D.add((new JLabel()));
        panel2D.add(individualChart);

        slider = new JSlider(JSlider.HORIZONTAL);
        slider.setFont(new Font("Arial", Font.BOLD, 12));
        slider.setForeground(Color.BLACK);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setVisible(false);
        slider.addChangeListener(this);

        JPanel panelBottom = new JPanel(new FlexGridLayout(new String[]{"53%"}, new String[]{"100%"}, 2, 0));
        //panelBottom.add(new JLabel());
        panelBottom.add(slider);

        //JPanel panelall = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"86%", "14%"}, 0, 0));
        JPanel panelall = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"87%", "13%"}, 0, 0));
        panelall.add(panel2D);
        panelall.add(panelBottom);

        return panelall;
    }

    private JPanel create2DTablePanel() {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 2, 2, true, true));
        ViewSetting viewSetting = ViewSettingsManager.getSummaryView("STRATEGY_2D");
//        ViewSetting viewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("STRATEGY_2D").getObject();
//		GUISettings.setColumnSettings(viewSetting, ChartInterface.getColumnSettings("STRATEGY_2D_DATA_COLS"));
        model2D = new Strategy2DTableModel(objectStore);
        model2D.setViewSettings(viewSetting);
        table2D.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
        table2D.setSortingEnabled();
        table2D.setModel(model2D);
        model2D.setTable(table2D, new Strategy2DRenderer(viewSetting.getColumnHeadings(), model2D.getRendIDs()));
        panel.add(table2D);
        ((SmartTable) table2D.getTable()).adjustColumnWidthsToFit(40);

        panel.setBorder(BorderFactory.createLineBorder(Theme.getColor("STRATEGY_PANEL_BORDER_COLOR")));

        return panel;
    }

    private JPanel create3DChartPanel() {

        panel3DChart = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0, true, true));
        panel3DChart.setBorder(BorderFactory.createLineBorder(Theme.getColor("STRATEGY_PANEL_BORDER_COLOR")));
        panel3DChart.setBackground(Theme.getColor("STRATEGY_CHART_OUTSIDE_COLOR"));

        return panel3DChart;
    }

    private JPanel create3DTablePanel() {

        panel3Dtable = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 2, 2, true, true));
        panel3Dtable.setBorder(BorderFactory.createLineBorder(Theme.getColor("STRATEGY_PANEL_BORDER_COLOR")));
        ViewSetting viewSetting = ViewSettingsManager.getSummaryView("STRATEGY_3D");
        GUISettings.setColumnSettings(viewSetting, ChartInterface.getColumnSettings("STRATEGY_3D_DATA_COLS"));
        model3D = new Strategy3DTableModel(objectStore3D);
        model3D.setViewSettings(viewSetting);
        table3D.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
        table3D.setSortingEnabled();
        table3D.setModel(model3D);
        model3D.setTable(table3D, new Strategy3DRenderer(viewSetting.getColumnHeadings(), model3D.getRendIDs()));
        panel3Dtable.add(table3D, 0);
        ((SmartTable) table3D.getTable()).adjustColumnWidthsToFit(40);
        return panel3Dtable;
    }

    private JPanel createRightNorthPanel() {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 5, 5));
        tabpane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "nw");

        tabpane.addTab(Language.getString("STRATEGY_2D_CHART"), create2DPanel());
        tabpane.addTab(Language.getString("STRATEGY_3D_CHART"), create3DPanel());

        panel.add(tabpane);
        tabpane.setSelectedIndex(0);
        tabpane.addTabPanelListener(this);
        panel.setBorder(BorderFactory.createLineBorder(Theme.getColor("STRATEGY_PANEL_BORDER_COLOR")));

        return panel;
    }

    private JPanel create2DPanel() {

        SpinnerModel incrModel = new SpinnerNumberModel(1, 1, 4, 1);
        spnGraphIncrement = new JSpinner(incrModel);
        SpinnerModel pointsModel = new SpinnerNumberModel(6, 1, 24, 1);
        spnNoOfPoints = new JSpinner(pointsModel);

        spnNoOfPoints.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {

                draw2DCharts();
            }
        });

        spnGraphIncrement.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {

                draw2DCharts();
            }
        });

        JPanel pnlResult = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "50", "25", "60", "60"}, 5, 2));

        pnl2DXAxisHeader = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pnl2DXAxisHeader.setBackground(Theme.getColor("STRATEGY_AXIS_BG_COLOR"));
        lbl2DXAxis = new JLabel(Language.getString("STRATEGY_X_AXIS"));
        lbl2DXAxis.setFont(new Font("Arial", Font.BOLD, 12));
        lbl2DXAxis.setForeground(Theme.getColor("STRATEGY_AXIS_FG_COLOR"));
        pnl2DXAxisHeader.add(lbl2DXAxis);

        JPanel pnlCombo = new JPanel(new FlexGridLayout(new String[]{"50%", "20%"}, new String[]{"18", "18"}, 0, 5));

        pnlCombo.add(new JLabel(Language.getString("STRATEGY_INCREMENT")));
        pnlCombo.add(spnGraphIncrement);
        pnlCombo.add(new JLabel(Language.getString("STRATEGY_POINTS")));
        pnlCombo.add(spnNoOfPoints);

        JPanel pnlRadioButtons = new JPanel(new FlexGridLayout(new String[]{"44%", "60%"}, new String[]{"20"}, 0, 5));

        ButtonGroup grpFormat = new ButtonGroup();
        grpFormat.add(btnAbs);
        grpFormat.add(btnPct);
        pnlRadioButtons.add(btnAbs);
        pnlRadioButtons.add(btnPct);
        btnAbs.setSelected(true);

        JPanel pnlBottom = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "25"}, 0, 5));
        JPanel pnlBottom1 = new JPanel(new FlexGridLayout(new String[]{"200"}, new String[]{"20"}, 0, 2));
        JPanel pnlBottom2 = new JPanel(new FlexGridLayout(new String[]{"60", "5", "45", "10", "60", "1", "45", "5", "62"}, new String[]{"20"}, 0, 2));
        pnlBottom1.add(chkUseStockPrice2D);

        txtRFRate2D = new TWTextField();
        txtRFRate2D.setDocument(rfRateFormatter);
        txtRFRate2D.setHorizontalAlignment(JTextField.RIGHT);
        txtRFRate2D.setText(Settings.getReskFreeInt());
        txtVolatility2D = new TWTextField();
        txtVolatility2D.setDocument(volatilityFormatter);
        txtVolatility2D.setHorizontalAlignment(JTextField.RIGHT);
        txtVolatility2D.setText(Double.toString(impVolatlity));

        pnlBottom2.add(new JLabel(Language.getString("STRATEGY_RF_RATE")));
        pnlBottom2.add(new JLabel());
        pnlBottom2.add(txtRFRate2D);
        pnlBottom2.add(new JLabel());
        pnlBottom2.add(new JLabel(Language.getString("STRATEGY_VOLATILITY")));
        pnlBottom2.add(new JLabel());
        pnlBottom2.add(txtVolatility2D);
        pnlBottom2.add(new JLabel());
        pnlBottom2.add(btnDraw);

//        JPanel pnlNew = new JPanel(new FlexGridLayout(new String[]{"100", "40%"}, new String[]{"20", "20"}, 0, 5));
//        pnlNew.add(new JLabel("RF Rate %"));
//        pnlNew.add(new TWTextField());
//        pnlNew.add(new JLabel("Volatility"));
//        pnlNew.add(new TWTextField());

        pnlBottom.add(pnlBottom1);
        pnlBottom.add(pnlBottom2);

        pnlResult.add(pnl2DXAxisHeader);
        pnlResult.add(pnlCombo);
        pnlResult.add(pnlRadioButtons);
        pnlResult.add(pnlBottom);
        //pnlResult.add(pnlNew);

        return pnlResult;
    }

    private JPanel create3DPanel() {

        SpinnerModel incrModel = new SpinnerNumberModel(1, 1, 4, 1);
        spnGraphIncrement3D = new JSpinner(incrModel);

        SpinnerModel pointsModel = new SpinnerNumberModel(18, 3, 24, 1);
        spnNoOfPoints3D = new JSpinner(pointsModel);

        SpinnerModel daysModel = new SpinnerNumberModel(1, 1, 14, 1);
        spnNoOfDays = new JSpinner(daysModel);

        spnGraphIncrement3D.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                draw3DCharts();
            }
        });
        spnNoOfPoints3D.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                draw3DCharts();
            }
        });
        spnNoOfDays.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                draw3DCharts();
            }
        });

        JPanel pnlResult = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "50", "25", "25", "75"}, 5, 2));

        pnl3DXAxisHeader = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pnl3DXAxisHeader.setBackground(Theme.getColor("STRATEGY_AXIS_BG_COLOR"));
        lbl3DXAxis = new JLabel("X-Axis");
        lbl3DXAxis.setFont(new Font("Arial", Font.BOLD, 12));
        lbl3DXAxis.setForeground(Theme.getColor("STRATEGY_AXIS_FG_COLOR"));
        pnl3DXAxisHeader.add(lbl3DXAxis);

        JPanel pnlCombo = new JPanel(new FlexGridLayout(new String[]{"50%", "20%"}, new String[]{"18", "18"}, 0, 5));

        pnlCombo.add(new JLabel(Language.getString("STRATEGY_INCREMENT")));
        pnlCombo.add(spnGraphIncrement3D);
        pnlCombo.add(new JLabel(Language.getString("STRATEGY_POINTS")));
        pnlCombo.add(spnNoOfPoints3D);

        JPanel pnlRadioButtons = new JPanel(new FlexGridLayout(new String[]{"44%", "60%"}, new String[]{"20"}, 0, 5));

        ButtonGroup grpFormat = new ButtonGroup();
        grpFormat.add(btnAbs3D);
        grpFormat.add(btnPct3D);
        pnlRadioButtons.add(btnAbs3D);
        pnlRadioButtons.add(btnPct3D);
        btnAbs3D.setSelected(true);

        JPanel pnlCheckBox = new JPanel(new FlexGridLayout(new String[]{"200", "100"}, new String[]{"20"}, 0, 7));
        pnlCheckBox.add(chkUseStockPrice3D);

        JPanel pnlBottom = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "25", "25"}, 0, 2));

        //z axis
        pnlzAxisHeader = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 3));
        pnlzAxisHeader.setBackground(Theme.getColor("STRATEGY_AXIS_BG_COLOR"));

        lblZAxis = new JLabel(Language.getString("STRATEGY_Z_AXIS"));
        lblZAxis.setFont(new Font("Arial", Font.BOLD, 12));
        lblZAxis.setForeground(Theme.getColor("STRATEGY_AXIS_FG_COLOR"));
        pnlzAxisHeader.add(lblZAxis);

        JPanel pnlBottom2 = new JPanel(new FlexGridLayout(new String[]{"50%", "20%"}, new String[]{"18"}, 0, 5));
        pnlBottom2.add(new JLabel(Language.getString("STRATEGY_INCR_DAYS")));
        pnlBottom2.add(spnNoOfDays);

//        JPanel pnlBottom3 = new JPanel(new FlexGridLayout(new String[]{"100"}, new String[]{"20"}, 0, 2));
        JPanel pnlBottom3 = new JPanel(new FlexGridLayout(new String[]{"60", "5", "45", "10", "60", "1", "45", "5", "62"}, new String[]{"20"}, 0, 2));
        btnDraw3D = new CustomButton(Language.getString("STRATEGY_DRAW"));
        txtRFRate3D = new TWTextField();
        txtRFRate3D.setDocument(rfRateFormatter);
        txtRFRate3D.setHorizontalAlignment(JTextField.RIGHT);
        txtRFRate3D.setText(Settings.getReskFreeInt());
        txtVolatility3D = new TWTextField();
        txtVolatility3D.setDocument(volatilityFormatter);
        txtVolatility3D.setHorizontalAlignment(JTextField.RIGHT);
        txtVolatility3D.setText("30.0");

        pnlBottom3.add(new JLabel(Language.getString("STRATEGY_RF_RATE")));
        pnlBottom3.add(new JLabel());
        pnlBottom3.add(txtRFRate3D);
        pnlBottom3.add(new JLabel());
        pnlBottom3.add(new JLabel(Language.getString("STRATEGY_VOLATILITY")));
        pnlBottom3.add(new JLabel());
        pnlBottom3.add(txtVolatility3D);
        pnlBottom3.add(new JLabel());
        pnlBottom3.add(btnDraw3D);

        pnlBottom.add(pnlzAxisHeader);
        pnlBottom.add(pnlBottom2);
        pnlBottom.add(pnlBottom3);

        pnlResult.add(pnl3DXAxisHeader);
        pnlResult.add(pnlCombo);
        pnlResult.add(pnlRadioButtons);
        pnlResult.add(pnlCheckBox);
        pnlResult.add(pnlBottom);

        return pnlResult;
    }

    private TWComboBox getGreekCombo() {

        TWComboBox cmb = new TWComboBox();
        cmb.addItem(Language.getString("DELATA"));
        cmb.addItem(Language.getString("GAMMA"));
        cmb.addItem(Language.getString("THETA"));
        cmb.addItem(Language.getString("VEGA"));
        cmb.addItem(Language.getString("RHO"));

        return cmb;
    }

    private JPanel createCommonTopPanel() {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"65%", "35%"}, new String[]{"100%"}, 2, 0, true, true));
        panel.add(createLeftNorthPanel());
        panel.add(createRightNorthPanel());
        return panel;
    }
    /*===================================== end of UI related ===================================*/

	/*======================================= 2D charts =========================================*/

    private JPanel create2DBottomPanel() {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0, true, true));
        splitPane2DBottom = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, create2DChartPanel(), create2DTablePanel());

        splitPane2DBottom.setDividerLocation(0.65);
        splitPane2DBottom.setDividerSize(SPLIT_PANE_DIVIDER_SIZE);
        panel.add(splitPane2DBottom);
        return panel;
    }

    private JPanel create3DBottomPanel() {

        JPanel panel3DBottom = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 2, 0, true, true));

        splitPane3DBottom = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, create3DChartPanel(), create3DTablePanel());
        splitPane3DBottom.setDividerLocation(WINDOW_WIDTH / 2);
        splitPane3DBottom.setDividerSize(SPLIT_PANE_DIVIDER_SIZE);
        panel3DBottom.add(splitPane3DBottom);

        return panel3DBottom;
    }

    private JPanel createLeftNorthPanel() {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%"}, 2, 0));
        panel.add(createTopSymbolsPanel());
        panel.add(createWestPanel());
        panel.setBorder(BorderFactory.createLineBorder(Theme.getColor("STRATEGY_PANEL_BORDER_COLOR")));

        return panel;
    }

    private JPanel createWestPanel() {

        JPanel panelResult = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"140", "2", "60"}));

        JPanel panelUpper = new JPanel(new FlexGridLayout(new String[]{"20%", "20%", "20%", "20%", "20%"}, new String[]{"100%"}, 5, 0, true, true));
        //JPanel pnleLeft = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "20", "20"}, 5, 5, true, true));
        JPanel pnleLeft = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "20"}, 5, 5, true, true));
        JPanel panelBorder = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"2"}));
        JPanel panelBottom = new JPanel(new FlexGridLayout(new String[]{"20%", "20%", "20%", "20%", "20%"}, new String[]{"20", "20"}, 5, 5, true, true));

        //	pnleLeft.add(new JLabel(Language.getString("STRATEGY_OPTION"), JLabel.LEFT));
        pnleLeft.add(new JLabel(Language.getString("STRATEGY_EXP"), JLabel.LEFT));
        pnleLeft.add(new JLabel(Language.getString("STRATEGY_STRIKE"), JLabel.LEFT));
        pnleLeft.add(new JLabel(Language.getString("STRATEGY_TYPE"), JLabel.LEFT));
        pnleLeft.add(new JLabel(Language.getString("STRATEGY_OPTION_PRICE"), JLabel.LEFT));
//        pnleLeft.add(new JLabel(Language.getString("STRATEGY_RF_RATE"), JLabel.LEFT));
        pnleLeft.add(new JLabel(Language.getString("QUANTITY"), JLabel.LEFT));

        panelUpper.add(pnleLeft);
        panelUpper.add(dataPanel1);
        panelUpper.add(dataPanel2);
        panelUpper.add(dataPanel3);
        panelUpper.add(dataPanel4);

        panelBorder.setBorder(BorderFactory.createLineBorder(Theme.getBlackColor()));

        panelBottom.add(new JLabel(Language.getString("STRATEGY_THEO_PRICE"), JLabel.LEFT));
        panelBottom.add(lblTheoPrice1);
        panelBottom.add(lblTheoPrice2);
        panelBottom.add(lblTheoPrice3);
        panelBottom.add(lblTheoPrice4);
        panelBottom.add(new JLabel(Language.getString("STRATEGY_GREEKS"), JLabel.LEFT));

        panelBottom.add(createGreekpan1());
        panelBottom.add(createGreekpan2());
        panelBottom.add(createGreekpan3());
        panelBottom.add(createGreekpan4());

        panelResult.add(panelUpper);
        panelResult.add(panelBorder);
        panelResult.add(panelBottom);

        return panelResult;
    }

    private JPanel createTopSymbolsPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 9, 3));
        panel.add(dataPanel1.getPnlSymbol());
        //   panel.setBorder(BorderFactory.createLineBorder(Color.RED));
        return panel;
    }

    private JPanel createGreekpan1() {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"46%", "2%", "52%"}, new String[]{"20"}));
        panel.add(cmbGreeks1);
        panel.add(new JLabel());
        panel.add(lblGreeks1);
        return panel;
    }

    private JPanel createGreekpan2() {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"46%", "2%", "52%"}, new String[]{"20"}));
        panel.add(cmbGreeks2);
        panel.add(new JLabel());
        panel.add(lblGreeks2);
        return panel;
    }

    private JPanel createGreekpan3() {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"46%", "2%", "52%"}, new String[]{"20"}));
        panel.add(cmbGreeks3);
        panel.add(new JLabel());
        panel.add(lblGreeks3);
        return panel;
    }

    private JPanel createGreekpan4() {
        JPanel pan = new JPanel(new FlexGridLayout(new String[]{"46%", "2%", "52%"}, new String[]{"20"}));
        pan.add(cmbGreeks4);
        pan.add(new JLabel());
        pan.add(lblGreeks4);
        return pan;
    }

    private void addActionListenrs() {
        btnAbs.addActionListener(this);
        btnPct.addActionListener(this);
        btnAbs3D.addActionListener(this);
        btnPct3D.addActionListener(this);
        btnDraw.addActionListener(this);
        btnDraw3D.addActionListener(this);
        chkUseStockPrice2D.addActionListener(this);
        chkUseStockPrice3D.addActionListener(this);
        this.addComponentListener(this);
    }

    private void process2DData() {
        count = 0;
        if (panel_1_valid) {
            if (calcMethod == Constants.BLACK_SCHOLES) {
                loadGreeks(dataPanel1);
            } else {
//                lblTheoPrice1.setText("");
                loadTheoreticalPriceCoxRoss(dataPanel1);
                cmbGreeks1.setEnabled(false);
                lblGreeks1.setText("");
            }
            count++;
        }
        if (panel_2_valid) {
            if (calcMethod == Constants.BLACK_SCHOLES) {
                loadGreeks(dataPanel2);
            } else {
//                lblTheoPrice2.setText("");
                loadTheoreticalPriceCoxRoss(dataPanel2);
                cmbGreeks2.setEnabled(false);
                lblGreeks2.setText("");
            }
            count++;
        }
        if (panel_3_valid) {
            if (calcMethod == Constants.BLACK_SCHOLES) {
                loadGreeks(dataPanel3);
            } else {
//                lblTheoPrice3.setText("");
                loadTheoreticalPriceCoxRoss(dataPanel3);
                cmbGreeks3.setEnabled(false);
                lblGreeks3.setText("");
            }
            count++;
        }
        if (panel_4_valid) {
            if (calcMethod == Constants.BLACK_SCHOLES) {
                loadGreeks(dataPanel4);
            } else {
//                lblTheoPrice4.setText("");
                loadTheoreticalPriceCoxRoss(dataPanel4);
                cmbGreeks4.setEnabled(false);
                lblGreeks4.setText("");
            }
            count++;
        }
        if (count == 0) {
			/*clear2DData();
			JOptionPane.showMessageDialog(this, Language.getString("STRATEGY_ERROR_MSG"), Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
			return;*/
        }

        populate2DTableData(centerPrice, graphIncrement, numberOfPoints);
    }

    private void populate2DTableData(double meanPrice, int graphIncrement, int noOfPoints) {

        objectStore.clear();

        final double[] prices = new double[2 * noOfPoints + 1];

        int count = 0;
        for (int i = noOfPoints - 1; i >= 0; i--) {

            double price = meanPrice * (1 + (graphIncrement * (i + 1) / 100f));
            objectStore.add(new Strategy2DRow(graphIncrement * (i + 1), price, 0, 0));
            prices[count] = price;
            count++;
        }

        prices[count] = meanPrice;
        count++;

        objectStore.add(new Strategy2DRow(0, meanPrice, 0, 0));

        for (int i = 0; i < noOfPoints; i++) {

            double price = meanPrice * (1 - (graphIncrement * (i + 1) / 100f));
            objectStore.add(new Strategy2DRow(-graphIncrement * (i + 1), price, 0, 0));
            prices[count] = price;
            count++;
        }

        PRICES = prices;
        generateteChartDataArray(prices);
        calculateEXPTableColumn(prices);
        calculateCALCTableColumn(prices);

        table2D.updateUI();
        table2D.repaint();
        ((SmartTable) table2D.getTable()).adjustColumnWidthsToFit(40);
        highLight2DTableCenterRow(table2D);
    }

    private void highLight2DTableCenterRow(Table table) {
        int rowNumber = (table.getModel().getRowCount() / 2);
        table.getTable().setRowSelectionInterval(rowNumber, rowNumber);
        Rectangle r = table.getTable().getCellRect(rowNumber, 0, true);
        table.getTable().scrollRectToVisible(table.getTable().getCellRect(table.getTable().getRowCount() - 1, 0, true));
        table.getTable().scrollRectToVisible(r);
        table.scrollRectToVisible(table.getTable().getCellRect(rowNumber - 1, 0, true));
    }

    private void calculateEXPTableColumn(double[] prices) {

        for (int i = prices.length - 1; i >= 0; i--) {

            Strategy2DRow row = objectStore.get(prices.length - 1 - i);
            double sum = 0;

            if (panel_1_valid) {    //TODO:Stock prices are different
                sum += dataManager.getYValueForExpDate(option1, prices[prices.length - 1 - i], strikePrice1, optionPrice1, riskFreeRate, impVolatlity, expDate1, minExpDate) * qunatity1;
            }
            if (panel_2_valid) {
                sum += dataManager.getYValueForExpDate(option2, prices[prices.length - 1 - i], strikePrice2, optionPrice2, riskFreeRate, impVolatlity, expDate2, minExpDate) * qunatity2;
            }
            if (panel_3_valid) {
                sum += dataManager.getYValueForExpDate(option3, prices[prices.length - 1 - i], strikePrice3, optionPrice3, riskFreeRate, impVolatlity, expDate3, minExpDate) * qunatity3;
            }
            if (panel_4_valid) {
                sum += dataManager.getYValueForExpDate(option4, prices[prices.length - 1 - i], strikePrice4, optionPrice4, riskFreeRate, impVolatlity, expDate4, minExpDate) * qunatity4;
            }

            row.setExpDate(sum);
        }
    }

    private void calculateCALCTableColumn(double[] prices) {

        for (int i = prices.length - 1; i >= 0; i--) {

            Strategy2DRow row = objectStore.get(prices.length - 1 - i);
            double sum = 0;

            if (panel_1_valid) {
                sum += dataManager.getYValueForCalcDate(option1, prices[prices.length - 1 - i], strikePrice1, optionPrice1, riskFreeRate, impVolatlity, expDate1, referenceDay) * qunatity1;
                //sum += dataManager.getYValueForCalcDate(option1, prices[prices.length - 1 - i], strikePrice1, optionPrice1, rfRate1, volatility1, minExpDate) * qunatity1;
            }
            if (panel_2_valid) {
                sum += dataManager.getYValueForCalcDate(option2, prices[prices.length - 1 - i], strikePrice2, optionPrice2, riskFreeRate, impVolatlity, expDate2, referenceDay) * qunatity2;
                //sum += dataManager.getYValueForCalcDate(option2, prices[prices.length - 1 - i], strikePrice2, optionPrice2, rfRate2, volatility2, minExpDate) * qunatity2;
            }
            if (panel_3_valid) {
                sum += dataManager.getYValueForCalcDate(option3, prices[prices.length - 1 - i], strikePrice3, optionPrice3, riskFreeRate, impVolatlity, expDate3, referenceDay) * qunatity3;
                //sum += dataManager.getYValueForCalcDate(option3, prices[prices.length - 1 - i], strikePrice3, optionPrice3, rfRate3, volatility3, minExpDate) * qunatity3;
            }
            if (panel_4_valid) {
                sum += dataManager.getYValueForCalcDate(option4, prices[prices.length - 1 - i], strikePrice4, optionPrice4, riskFreeRate, impVolatlity, expDate4, referenceDay) * qunatity4;
                //sum += dataManager.getYValueForCalcDate(option4, prices[prices.length - 1 - i], strikePrice4, optionPrice4, rfRate4, volatility4, minExpDate) * qunatity4;
            }
            row.setCalcDate(sum);
        }
    }

    private void generateteChartDataArray(double[] prices) {

        individualChart.getSeriesCollection().clear();

        if (dataPanel1.validate2DData()) {

            ChartSeries series = dataManager.getChartSeries(option1, prices, strikePrice1, optionPrice1, riskFreeRate, impVolatlity, qunatity1, expDate1, minExpDate, referenceDay);
            series.setName(Language.getString("STRATEGY_OPTION_1"));
            series.setChartColor(option1Color);
            series.setPanelId(OptionsDataPanel.PANEL_ID_1);
            individualChart.getSeriesCollection().add(series);
        }
        if (dataPanel2.validate2DData()) {

            ChartSeries series = dataManager.getChartSeries(option2, prices, strikePrice2, optionPrice2, riskFreeRate, impVolatlity, qunatity2, expDate2, minExpDate, referenceDay);
            series.setName(Language.getString("STRATEGY_OPTION_2"));
            series.setChartColor(option2Color);
            series.setPanelId(OptionsDataPanel.PANEL_ID_2);
            individualChart.getSeriesCollection().add(series);
        }
        if (dataPanel3.validate2DData()) {

            ChartSeries series = dataManager.getChartSeries(option3, prices, strikePrice3, optionPrice3, riskFreeRate, impVolatlity, qunatity3, expDate3, minExpDate, referenceDay);
            series.setName(Language.getString("STRATEGY_OPTION_3"));
            series.setChartColor(option3Color);
            series.setPanelId(OptionsDataPanel.PANEL_ID_3);
            individualChart.getSeriesCollection().add(series);
        }
        if (dataPanel4.validate2DData()) {

            ChartSeries series = dataManager.getChartSeries(option4, prices, strikePrice4, optionPrice4, riskFreeRate, impVolatlity, qunatity4, expDate4, minExpDate, referenceDay);
            series.setName(Language.getString("STRATEGY_OPTION_4"));
            series.setChartColor(option4Color);
            series.setPanelId(OptionsDataPanel.PANEL_ID_4);
            individualChart.getSeriesCollection().add(series);
        }

        individualChart.meanPrice = centerPrice;
        individualChart.setInAbsoluteValues(btnAbs.isSelected());
        individualChart.calculatePixelvalues();
        individualChart.repaint();

        combinedChart.getSeriesCollection().clear();
        ChartSeries series = new ChartSeries(DataManager.SERIRES_EXP, dataManager.combineExpDateXData, dataManager.combineExpDateYData);
        String name = Language.getString("STRATEGY_EXP") + " " + new TWDateFormat("yyyy MMM dd").format(minExpDate);
        series.setName(name);
        series.setChartColor(expChartColor);
        combinedChart.getSeriesCollection().add(series);

        ChartSeries series2 = new ChartSeries(DataManager.SERIRES_CALC, dataManager.combineCalcDateXData, dataManager.combineCalcDateYData);
        name = Language.getString("STRATEGY_CALC") + " " + new TWDateFormat("yyyy MMM dd").format(referenceDay);
        series2.setName(name);
        series2.setChartColor(calcChartColor);
        combinedChart.getSeriesCollection().add(series2);

        combinedChart.calculatePixelvalues();
        combinedChart.repaint();

        dataManager.combineExpDateXData = null;
        dataManager.combineExpDateYData = null;
        dataManager.combineCalcDateYData = null;
        dataManager.combineCalcDateXData = null;
    }

    private long getCurrentDayInMills() {

        long time = System.currentTimeMillis();
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.setTimeInMillis(time);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0, 0);

        return cal.getTimeInMillis();
    }

    public void actionPerformed(ActionEvent e) {

        Object obj = e.getSource();

        if (obj.equals(btnDraw)) {
            try {
                draw2DCharts();

            } catch (Exception ex) {

                ex.printStackTrace();
                System.out.println("********** exception in drawing 2D charts. ************");
            }
        } else if (obj.equals(btnAbs) || obj.equals(btnPct)) {
            individualChart.setInAbsoluteValues(btnAbs.isSelected());
            individualChart.repaint();
        } else if (obj.equals(btnAbs3D) || obj.equals(btnPct3D)) {
            if (canvas != null) {
                canvas.setInAbsoluteValues(btnAbs3D.isSelected());
                canvas.plotSurface();
                canvas.repaint(canvas.getBounds());
            }
        } else if (obj.equals(btnDraw3D)) {
            try {
                draw3DCharts();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("********** exception in drawing 3D charts. ************");
            }
        } else if (obj.equals(chkUseStockPrice2D)) {
            if (count > 0) {
                draw2DCharts();
            }
        } else if (obj.equals(chkUseStockPrice3D)) {
            if (count > 0) {
                draw3DCharts();
            }
        }
    }

    private void drawAllCharts() {
        draw2DCharts();
        draw3DCharts();
    }

    private void draw2DCharts() {
        long beginTime = System.currentTimeMillis();
        extractDataFromUI();
        //process2DData();
        if (count != 0 && canContinue) {
            slider.setVisible(true);
            setSliderParams();
            slider.setValue(0);
        } else {
            slider.setVisible(false);
        }
        //System.out.println("************ draw2DCharts *********** " + (double) (System.currentTimeMillis() - beginTime));
    }

    private void setSliderParams() {

        Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
        int minorGap = 1;
        int majorGap = 1;

        NO_OF_MAJOR_POINTS = 6;

        double days = ((double) (minExpDate - currentDayInMillis) / (double) DataManager.MILLIS_PER_DAY);
        TOTAL_DAYS = ((int) (Math.round(days) * 100)) / 100;
        if (TOTAL_DAYS < NO_OF_MAJOR_POINTS) {    // special case. no of days to expire is less than 6.
            NO_OF_MAJOR_POINTS = TOTAL_DAYS;
            minorGap = 1;
            majorGap = 1;
        } else {
            double tempGap = (double) TOTAL_DAYS / (NO_OF_MAJOR_POINTS - 1);
            majorGap = ((int) (Math.round(tempGap) * 100)) / 100;

            if (((double) majorGap / (double) MAXIMUM_TICKS) > 1.0) {
                tempGap = (double) TOTAL_DAYS / ((NO_OF_MAJOR_POINTS - 1) * MAXIMUM_TICKS);
                minorGap = ((int) (Math.round(tempGap) * 100)) / 100;
                majorGap = minorGap * (MAXIMUM_TICKS);
            }
        }

		/*
				System.out.println("********* TOTAL DAYS ********* " + TOTAL_DAYS);
				System.out.println("********* major Gap ********* " + majorGap);
				System.out.println("********* minor Gap ********* " + minorGap);
			   */

        for (int i = 0; i < NO_OF_MAJOR_POINTS; i++) {

            long t = currentDayInMillis + (i * majorGap * DataManager.MILLIS_PER_DAY);
            if (t <= minExpDate) {
                String date = sliderDateFormatter.format(t);
                labelTable.put(majorGap * i, new JLabel(date));
            }
        }

        slider.setMinimum(0);
        slider.setMaximum(TOTAL_DAYS - 1);
        slider.setMajorTickSpacing(majorGap);
        slider.setMinorTickSpacing(minorGap);
        slider.setLabelTable(labelTable);
        slider.setSnapToTicks(false);
    }

    public void loadGreeks(OptionsDataPanel panel) {
        try {
            blackCalculator.initCalculator();
            double yearsFromToday = (double) (panel.getExpDate() - currentDayInMillis) / (DataManager.MILLIS_PER_DAY * 365);

            String type = DataManager.TYPE_PUT;
            if (panel.getOptionType() == DataManager.SERIRES_BUY_CALL || panel.getOptionType() == DataManager.SERIRES_SELL_CALL) {
                type = DataManager.TYPE_CALL;
            }
            double imlVolatility = blackCalculator.CalculateImpliedVol(centerPrice, panel.getStrikePrice(), riskFreeRate, yearsFromToday, 0, type, panel.getOptionPrice()) * 100;

//			double spotPrice = stock.getPreviousClosed();
            double spotPrice = 0;
            if (underlyingEquity != null) {
//                spotPrice = stock.getLastTradedPrice();
                spotPrice = underlyingEquity.getPreviousClosed();
            }
//            double theoriticalPrice = blackCalculator.getTheoriticalPriceOnly(type, spotPrice, yearsFromToday, panel.getStrikePrice(), riskFreeRate, imlVolatility, 0);
            double theoriticalPrice = blackCalculator.getTheoriticalPriceOnly(type, spotPrice, yearsFromToday, panel.getStrikePrice(), riskFreeRate, imlVolatility, 0);

            blackCalculator.calculateTheoritaclPrice(type, centerPrice, yearsFromToday, panel.getStrikePrice(), riskFreeRate, imlVolatility, 0, false, 0);
            ArrayList<Double> results = new ArrayList<Double>();
            blackCalculator.fillDataToList(results);

            greekValues[GREEK_VALUE_DELTA] = results.get(1);
            greekValues[GREEK_VALUE_GAMMA] = results.get(2);
            greekValues[GREEK_VALUE_THETA] = results.get(3);
            greekValues[GREEK_VALUE_VEGA] = results.get(4);
            greekValues[GREEK_VALUE_RHO] = results.get(5);

            String strTheoritical = N_A_STRING;
            if (!isValueNan(theoriticalPrice)) {
                strTheoritical = greekFormatter.format(theoriticalPrice);
            }

            if (panel.getPanelID() == OptionsDataPanel.PANEL_ID_1) {
                cmbGreeks1.setEnabled(true);
                lblTheoPrice1.setText(strTheoritical);
                double value = greekValues[cmbGreeks1.getSelectedIndex()];
                setGreekValue(lblGreeks1, value);
            } else if (panel.getPanelID() == OptionsDataPanel.PANEL_ID_2) {
                cmbGreeks2.setEnabled(true);
                lblTheoPrice2.setText(strTheoritical);
                double value = greekValues[cmbGreeks2.getSelectedIndex()];
                setGreekValue(lblGreeks2, value);
            } else if (panel.getPanelID() == OptionsDataPanel.PANEL_ID_3) {
                cmbGreeks3.setEnabled(true);
                lblTheoPrice3.setText(strTheoritical);
                double value = greekValues[cmbGreeks3.getSelectedIndex()];
                setGreekValue(lblGreeks3, value);
            } else if (panel.getPanelID() == OptionsDataPanel.PANEL_ID_4) {
                cmbGreeks4.setEnabled(true);
                lblTheoPrice4.setText(strTheoritical);
                double value = greekValues[cmbGreeks4.getSelectedIndex()];
                setGreekValue(lblGreeks4, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	/*=================================== end of 2D charts =====================================*/

	/*======================================= 3D charts =========================================*/

    public void loadTheoreticalPriceCoxRoss(OptionsDataPanel panel) {
        BinomialCalculator.EnumCallPut tempEnum;
        try {
            coxRossCalculator.initCalculator();
            double yearsFromToday = (double) (panel.getExpDate() - currentDayInMillis) / (DataManager.MILLIS_PER_DAY * 365);
            double spotPrice = 0;
            if (stock != null) {
                spotPrice = stock.getLastTradeValue();
            }

            if (panel.getOptionType() == DataManager.SERIRES_BUY_CALL || panel.getOptionType() == DataManager.SERIRES_SELL_CALL) {
                tempEnum = BinomialCalculator.EnumCallPut.Call;
            } else {
                tempEnum = BinomialCalculator.EnumCallPut.Put;
            }
            double theoreticalPrice = coxRossCalculator.getTheoreticalPriceOnly(spotPrice, panel.getStrikePrice(), yearsFromToday, impVolatlity, riskFreeRate, 0, tempEnum, BinomialCalculator.EnumStyle.European, 10);
            String strTheoritical = N_A_STRING;
            if (!isValueNan(theoreticalPrice)) {
                strTheoritical = greekFormatter.format(theoreticalPrice);
            }
            if (panel.getPanelID() == OptionsDataPanel.PANEL_ID_1) {
                lblTheoPrice1.setText(strTheoritical);
            } else if (panel.getPanelID() == OptionsDataPanel.PANEL_ID_2) {
                lblTheoPrice2.setText(strTheoritical);
            } else if (panel.getPanelID() == OptionsDataPanel.PANEL_ID_3) {
                lblTheoPrice3.setText(strTheoritical);
            } else if (panel.getPanelID() == OptionsDataPanel.PANEL_ID_4) {
                lblTheoPrice4.setText(strTheoritical);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private boolean isValueNan(double value) {
        if (Double.compare(value, Double.NaN) == 0) {
            return true;
        }
        return false;
    }

    private void setGreekValue(JLabel label, double value) {
        if (isValueNan(value)) {
            label.setText(N_A_STRING);
        } else {
            label.setText(greekFormatter.format(value));
        }
    }

    public void process3DData() {
        count = 0;

        if (panel_1_valid) {
            if (calcMethod == Constants.BLACK_SCHOLES) {
                loadGreeks(dataPanel1);
            } else {
                loadTheoreticalPriceCoxRoss(dataPanel1);
                cmbGreeks1.setEnabled(false);
                lblGreeks1.setText("");
            }
            count++;
        }
        if (panel_2_valid) {
            if (calcMethod == Constants.BLACK_SCHOLES) {
                loadGreeks(dataPanel2);
            } else {
                loadTheoreticalPriceCoxRoss(dataPanel2);
                cmbGreeks2.setEnabled(false);
                lblGreeks2.setText("");
            }
            count++;
        }
        if (panel_3_valid) {
            if (calcMethod == Constants.BLACK_SCHOLES) {
                loadGreeks(dataPanel3);
            } else {
                loadTheoreticalPriceCoxRoss(dataPanel3);
                cmbGreeks3.setEnabled(false);
                lblGreeks3.setText("");
            }
            count++;
        }
        if (panel_4_valid) {
            if (calcMethod == Constants.BLACK_SCHOLES) {
                loadGreeks(dataPanel4);
            } else {
                loadTheoreticalPriceCoxRoss(dataPanel4);
                cmbGreeks4.setEnabled(false);
                lblGreeks4.setText("");
            }
            count++;
        }
        if (count == 0) {
			/*clear3DData();
			JOptionPane.showMessageDialog(this, Language.getString("STRATEGY_ERROR_MSG"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
			return;*/
        }
        populate3DTableData(centerPrice, graphIncrement3D, numberOfPoints3D);
    }

    public void bindProfitValuesForDates(double[] prices) {

        minProfit = Float.MAX_VALUE;
        maxProfit = -500000000f;

        long interval = minExpDate - currentDayInMillis;
        intervalDates = 1 + Math.round(interval / DataManager.MILLIS_PER_DAY) / incrementInDays;

        if (intervalDates > MAX_DAYS_ALLOWED) {
            intervalDates = MAX_DAYS_ALLOWED;  //maximum days allowed is 14
        }

        double years = 0;
        for (int i = prices.length - 1; i >= 0; i--) {

            Strategy3DRow row = objectStore3D.get(i);
            double price = prices[i];
            double[] dates = new double[intervalDates];

            for (int gap = 0; gap < intervalDates; gap++) {
                //TODO: need to finalize
                //years = ((minExpDate - (currentDayInMillis + DataManager.MILLIS_PER_DAY * gap * incrementInDays)) / (DataManager.MILLIS_PER_DAY * 365f));
                double sum = 0;
                if (panel_1_valid) {
                    years = ((expDate1 - (currentDayInMillis + DataManager.MILLIS_PER_DAY * gap * incrementInDays)) / (DataManager.MILLIS_PER_DAY * 365f));
                    sum += getProfit(option1, price, years, strikePrice1, riskFreeRate, impVolatlity, 0, optionPrice1) * qunatity1;
                }
                if (panel_2_valid) {
                    years = ((expDate2 - (currentDayInMillis + DataManager.MILLIS_PER_DAY * gap * incrementInDays)) / (DataManager.MILLIS_PER_DAY * 365f));
                    sum += getProfit(option2, price, years, strikePrice2, riskFreeRate, impVolatlity, 0, optionPrice2) * qunatity2;
                }
                if (panel_3_valid) {
                    years = ((expDate3 - (currentDayInMillis + DataManager.MILLIS_PER_DAY * gap * incrementInDays)) / (DataManager.MILLIS_PER_DAY * 365f));
                    sum += getProfit(option3, price, years, strikePrice3, riskFreeRate, impVolatlity, 0, optionPrice3) * qunatity3;
                }
                if (panel_4_valid) {
                    years = ((expDate4 - (currentDayInMillis + DataManager.MILLIS_PER_DAY * gap * incrementInDays)) / (DataManager.MILLIS_PER_DAY * 365f));
                    sum += getProfit(option4, price, years, strikePrice4, riskFreeRate, impVolatlity, 0, optionPrice4) * qunatity4;
                }
                if (Double.compare(sum, Double.NaN) == 0) {
                    //System.out.println("asdas");
                    sum = -150;
                }
                dates[gap] = sum;

                if (!(Float.compare((float) sum, Float.NaN) == 0 || Float.compare((float) sum, Float.NaN) == 0)) {
                    minProfit = Math.min(minProfit, (float) sum);
                    maxProfit = Math.max(maxProfit, (float) sum);
                } else {
                    //System.out.println(minProfit);
                    //System.out.println(maxProfit);
                }
            }

            row.setDates(dates);
        }
    }

    private void populate3DTableData(double meanPrice, int graphIncrement, int noOfPoints) {
        minPrice = Float.MAX_VALUE;
        maxPrice = -Float.MIN_VALUE;

        objectStore3D.clear();

        final double[] prices = new double[2 * noOfPoints + 1];
        int count = 0;
        for (int i = noOfPoints - 1; i >= 0; i--) {

            double price = meanPrice * (1 + (graphIncrement * (i + 1) / 100f));
            objectStore3D.add(new Strategy3DRow(graphIncrement * (i + 1), price, null));
            prices[count] = price;
            maxPrice = Math.max(maxPrice, price);
            minPrice = Math.min(minPrice, price);
            count++;
        }
        prices[count] = meanPrice;
        maxPrice = Math.max(maxPrice, meanPrice);
        minPrice = Math.min(minPrice, meanPrice);
        count++;

        objectStore3D.add(new Strategy3DRow(0, meanPrice, null));

        for (int i = 0; i < noOfPoints; i++) {

            double price = meanPrice * (1 - (graphIncrement * (i + 1) / 100f));
            objectStore3D.add(new Strategy3DRow(-graphIncrement * (i + 1), price, null));
            prices[count] = price;
            maxPrice = Math.max(maxPrice, price);
            minPrice = Math.min(minPrice, price);
            count++;
        }

        bindProfitValuesForDates(prices);

        Table tableNew = new Table();
        ViewSetting settings = table3D.getModel().getViewSettings();

        String[] newCols = new String[2 + intervalDates];
        String newIDs = "01";

        newCols[0] = Language.getString("STRATEGY_INCR");
        newCols[1] = Language.getString("STRATEGY_STOCK");

        for (int gap = 0; gap < intervalDates; gap++) {
            newCols[2 + gap] = dateFormatter.format(currentDayInMillis + gap * DataManager.MILLIS_PER_DAY * incrementInDays);
        }

        for (int i = 2; i < newCols.length; i++) {
            newIDs += String.valueOf(2);
        }

        settings.setRenderingIDs(newIDs);
        settings.setColumnHeadings(newCols);
        Strategy3DTableModel newModel = new Strategy3DTableModel(objectStore3D);
        newModel.setViewSettings(settings);

        tableNew.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
        tableNew.setModel(newModel);
        newModel.setTable(tableNew, new Strategy3DRenderer(settings.getColumnHeadings(), newModel.getRendIDs()));
        tableNew.setSortingEnabled();

        panel3Dtable.remove(0);
        panel3Dtable.add(tableNew, 0);
        tableNew.updateUI();
        tableNew.repaint();
        ((SmartTable) tableNew.getTable()).adjustColumnWidthsToFit(40);
        highLight3DCenterRow(tableNew);

        //generating x y z values for 3D graph plot
        int colCount = tableNew.getModel().getColumnCount();
        int rowCount = tableNew.getModel().getRowCount();

        xStrikePrice3D = new double[rowCount];
        zDates3D = new double[(colCount - 2)];
        yProfit3D = new double[(colCount - 2) * rowCount];

        try {
            for (int i = 0; i < rowCount; i++) {
                xStrikePrice3D[i] = Double.parseDouble(String.valueOf(tableNew.getModel().getValueAt(i, 1))) / 1000;
            }
            for (int j = 0; j < (colCount - 2); j++) {
                zDates3D[j] = j;
            }
            int value = 0;
            for (int zi = 0; zi < (colCount - 2); zi++) {
                for (int zj = 0; zj < rowCount; zj++) {
                    yProfit3D[value] = Double.parseDouble(String.valueOf(tableNew.getModel().getValueAt(zj, (zi + 2)))) / 10;
                    value++;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        plot3DSurface();
    }

    public void plot3DSurface() {
        minProfitSurface = Float.MAX_VALUE;
        maxProfitSurface = -500000000f;

        if (canvas == null) {
            canvas = new Simple3DChart();
            panel3DChart.add(canvas);
        }

        parser1 = new Parser(2);
        parser1.defineVariable(1, "x");
        parser1.defineVariable(2, "y");

        float x, y, z;
        int count = 0;
        try {

            float xmin = Float.valueOf((float) minPrice).floatValue();
            float xmax = Float.valueOf((float) maxPrice).floatValue();

//            float zmin = Float.valueOf(minProfit).floatValue();
//            float zmax = Float.valueOf(maxProfit).floatValue();

            float ymin = 0;
            float ymax = ((float) (minExpDate - currentDayInMillis) / (DataManager.MILLIS_PER_DAY * 365));

            calc_divisions = 20;
			/*
						float adj = (zmax - zmin)*0.5f;
						float adj =0;
						if ((zmin == 0) && zmax == 0) {
							zmin = -500;
							zmax = 500;
						} else if (zmax == zmin) {
							adj = zmax * .5f;
						}
						canvas.setRanges(xmin, xmax, ymin, ymax, (zmin - adj), (zmax + adj), calc_divisions);
						*/

            //x*exp(-0*y)*norm(((ln(x/390.0)+(473.0*y))/(30.0*sqrt(y))))-(390.0*exp(-23.0*y))*norm((ln(x/390.0)+(473.0*y))/(30.0*sqrt(y))-30.0*sqrt(y))-40.0

            float stepx = (xmax - xmin) / calc_divisions;
            float stepy = (ymax - ymin) / calc_divisions;

            int total = (calc_divisions + 1) * (calc_divisions + 1);

            SurfaceVertex[][] vertex = allocateMemory(true, false, total);
            if (vertex == null) return;

            float max = Float.NaN;
            float min = Float.NaN;

            canvas.destroyImage();

            int i = 0, j = 0, k = 0;
            x = xmin;
            y = ymin; //todo: change
//            y = ymax;

            float xfactor = 20 / (xmax - xmin);
            float yfactor = 20 / (ymax - ymin);
            float v = 0;
            float yearsToExp = 0;
            while (i <= calc_divisions) {

                parser1.setVariable(1, x);
                parser1.setVariable(2, y);
                v = 0;
                while (j <= calc_divisions) {
                    v = 0;
                    if (true) {
                        if (panel_1_valid) {
                            yearsToExp = (expDate1 / (DataManager.MILLIS_PER_DAY * 365f)) - y - (currentDayInMillis / (DataManager.MILLIS_PER_DAY * 365f));
                            float temp = (float) getProfit(option1, x, yearsToExp, strikePrice1, riskFreeRate, impVolatlity, 0, optionPrice1) * qunatity1;
                            if (Float.compare(temp, Float.NaN) != 0) {
                                v += temp;
                            }
                        }
                        if (panel_2_valid) {
                            yearsToExp = (expDate2 / (DataManager.MILLIS_PER_DAY * 365f)) - y - (currentDayInMillis / (DataManager.MILLIS_PER_DAY * 365f));
                            float temp2 = (float) getProfit(option2, x, yearsToExp, strikePrice2, riskFreeRate, impVolatlity, 0, optionPrice2) * qunatity2;
                            if (Float.compare(temp2, Float.NaN) != 0) {
                                v += temp2;
                            }
                        }

                        if (panel_3_valid) {
                            yearsToExp = (expDate3 / (DataManager.MILLIS_PER_DAY * 365f)) - y - (currentDayInMillis / (DataManager.MILLIS_PER_DAY * 365f));
                            float temp3 = (float) getProfit(option3, x, yearsToExp, strikePrice3, riskFreeRate, impVolatlity, 0, optionPrice3) * qunatity3;
                            if (Float.compare(temp3, Float.NaN) != 0) {
                                v += temp3;
                            }
                        }

                        if (panel_4_valid) {
                            yearsToExp = (expDate4 / (DataManager.MILLIS_PER_DAY * 365f)) - y - (currentDayInMillis / (DataManager.MILLIS_PER_DAY * 365f));
                            float temp4 = (float) getProfit(option4, x, yearsToExp, strikePrice4, riskFreeRate, impVolatlity, 0, optionPrice4) * qunatity4;
                            if (Float.compare(temp4, Float.NaN) != 0) {
                                v += temp4;
                            }
                        }

                        //System.out.println(v);
                        if (Float.isInfinite(v)) v = Float.NaN;
                        if (!Float.isNaN(v)) {
                            if (Float.isNaN(max) || (v > max)) max = v;
                            else if (Float.isNaN(min) || (v < min)) min = v;
                        }
                        vertex[0][k] = new SurfaceVertex((x - xmin) * xfactor - 10,
                                (y - ymin) * yfactor - 10, v);
                        if (!(Float.compare(v, Float.NaN) == 0 || Float.compare(v, Float.NaN) == 0)) {
                            minProfitSurface = Math.min(minProfitSurface, v);
                            maxProfitSurface = Math.max(maxProfitSurface, v);
                        }
                    }

                    j++;
                    y += stepy; //todo: change
//                    y -= stepy;
                    parser1.setVariable(2, y);
                    k++;
                }
                j = 0;
                y = ymin; // todo: change
//                y = ymax;
                i++;
                x += stepx;
            }
            float zmin = minProfitSurface;
            float zmax = maxProfitSurface;
            float adj = (zmax - zmin) * 0.1f;
//            float adj =0;

            int valMin = Math.round(zmin);
            int valMax = Math.round(zmax);
            if ((valMin == 0) && valMax == 0) {
                zmin = -100f;
                zmax = 100f;
            } else if (valMin == valMax) {
                adj = Math.abs(zmax) * 0.5f;

            }
//            canvas.setRanges(xmin, xmax, ymin, ymax, (zmin - adj), (zmax + adj), calc_divisions);
            canvas.setRanges(xmin, xmax, ymax, ymin, (zmin - adj), (zmax + adj), calc_divisions);
            canvas.setInAbsoluteValues(btnAbs3D.isSelected());
            canvas.setValuesArray(vertex);
            canvas.setDataAvailability(true);
            canvas.repaint();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void draw3DCharts() {
        long beginTime = System.currentTimeMillis();
        extractDataFromUI();
//        process3DData();
        //System.gc(); // TODO: free memory used by the calculatinos.
        //System.out.println("********** TIME TAKEN FOR 3D CHARTS ********* " + (double) (System.currentTimeMillis() - beginTime));

    }

    private SurfaceVertex[][] allocateMemory(boolean f1, boolean f2, int total) {
        SurfaceVertex[][] vertex = null;

        // Releases memory being used
        canvas.setValuesArray(null);
        try {
            vertex = new SurfaceVertex[2][total];
            if (!f1) vertex[0] = null;
            if (!f2) vertex[1] = null;
        } catch (OutOfMemoryError e) {
            System.out.println("Not enough memory");
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
        }
        return vertex;
    }

    private double getProfit(int optionType, double stockPrice, double years, double strikeprice,
                             double RFrate, double volatility, double dividendyield, double optionPrice) {
        double calcPrice = 0;
        double profit = 0;
        try {
            blackCalculator.initCalculator();
            coxRossCalculator.initCalculator();
            String type = DataManager.TYPE_CALL;
            tempEnum = BinomialCalculator.EnumCallPut.Call;
            if (optionType == DataManager.SERIRES_BUY_PUT || optionType == DataManager.SERIRES_SELL_PUT) {
                type = DataManager.TYPE_PUT;
                tempEnum = BinomialCalculator.EnumCallPut.Put;
            }
            if (calcMethod == Constants.COX_ROSS) {
                long time = System.currentTimeMillis();
                calcPrice = coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikeprice, years, (volatility / 100), (RFrate / 100), dividendyield, tempEnum, BinomialCalculator.EnumStyle.European, 10);
//				System.out.println("********** time taken ********* " + (double) (System.currentTimeMillis() - time));
            } else {
                calcPrice = blackCalculator.getTheoriticalPriceOnly(type, stockPrice, years, strikeprice, RFrate, volatility, dividendyield);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (optionType == DataManager.SERIRES_BUY_PUT || optionType == DataManager.SERIRES_BUY_CALL) {
            profit = calcPrice - optionPrice;
        } else {
            profit = optionPrice - calcPrice;
        }
        return profit;
    }

    public void tabStareChanged(TWTabEvent event) {
        if (event.getState() == TWTabEvent.STATE_SELECTED) {
            CardLayout cards = (CardLayout) bottomPanel.getLayout();
            if (tabpane.getSelectedIndex() == 0) {
                cards.show(bottomPanel, ID_BOTTOM_2D);
                if (!txtRFRate3D.getText().equalsIgnoreCase("")) {
                    txtRFRate2D.setText(txtRFRate3D.getText());
                }
                if (!txtVolatility3D.getText().equalsIgnoreCase("")) {
                    txtVolatility2D.setText(txtVolatility3D.getText());
                }
                int panelCount = validatePanels();
                if (panelCount != 0 && !txtRFRate2D.getText().equalsIgnoreCase("") && !txtVolatility2D.getText().equalsIgnoreCase("")) {
                    draw2DCharts();
                } else {
                    clear2DData();
                    slider.setVisible(false);
                }
            } else {
                if (tabpane.getSelectedIndex() == 1) {
                    cards.show(bottomPanel, ID_BOTTOM_3D);
                    if (!txtRFRate2D.getText().equalsIgnoreCase("")) {
                        txtRFRate3D.setText(txtRFRate2D.getText());
                    }
                    if (!txtVolatility2D.getText().equalsIgnoreCase("")) {
                        txtVolatility3D.setText(txtVolatility2D.getText());
                    }
                    int panelCount = validatePanels();
                    if (panelCount != 0 && !txtRFRate3D.getText().equalsIgnoreCase("") && !txtVolatility3D.getText().equalsIgnoreCase("")) {
                        draw3DCharts();
                    } else {
                        clear3DData();
                    }
                }
            }
        }
    }

    /* Printable method */
    public void internalFrameClosed(InternalFrameEvent e) {
        super.internalFrameClosed(e);    //To change body of overridden methods use File | Settings | File Templates.
    }

    private void extractDataFromUI() {
        int count = 0;
        int totalPrice = 0;
        canContinue = true;
        if (isLoadingEqVol) {
            return;
        }

        graphIncrement = Integer.parseInt(String.valueOf(spnGraphIncrement.getValue()));
        numberOfPoints = Integer.parseInt(String.valueOf(spnNoOfPoints.getValue()));
        graphIncrement3D = Integer.parseInt(String.valueOf(spnGraphIncrement3D.getValue()));
        numberOfPoints3D = Integer.parseInt(String.valueOf(spnNoOfPoints3D.getValue()));

        panel_1_valid = dataPanel1.validate2DData();
        panel_2_valid = dataPanel2.validate2DData();
        panel_3_valid = dataPanel3.validate2DData();
        panel_4_valid = dataPanel4.validate2DData();

        underlyingEquity = stock;

        if (volatilityChanged) {
            impVolatlity = stock.getAnnualVolatility() * 100;
            txtVolatility2D.setText(volFormatter.format(impVolatlity));
            txtVolatility3D.setText(volFormatter.format(impVolatlity));
            volatilityChanged = false;
        }

        minExpDate = Long.MAX_VALUE;
        if (panel_1_valid) {
            optionPrice1 = dataPanel1.getOptionPrice();
            option1 = dataPanel1.getOptionType();
            strikePrice1 = dataPanel1.getStrikePrice();
            expDate1 = dataPanel1.getExpDate();
            minExpDate = Math.min(minExpDate, expDate1);
            qunatity1 = dataPanel1.getQuantity();
            totalPrice += strikePrice1;
            count++;
        } else {
            clear2DData();
            JOptionPane.showMessageDialog(this,
                    Language.getString("STRATEGY_ERROR_MSG") + " (" + dataPanel1.errors.get(0) + ")",
                    Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
            dataPanel1.errors.clear();
            return;
        }

        if (panel_2_valid) {
            optionPrice2 = dataPanel2.getOptionPrice();
            option2 = dataPanel2.getOptionType();
            strikePrice2 = dataPanel2.getStrikePrice();
            expDate2 = dataPanel2.getExpDate();
            minExpDate = Math.min(minExpDate, expDate2);
            qunatity2 = dataPanel2.getQuantity();
            totalPrice += strikePrice2;
            count++;
        } else if (dataPanel2.getErrorCount() != 2) {
            clear2DData();
            JOptionPane.showMessageDialog(this,
                    Language.getString("STRATEGY_ERROR_MSG") + " (" + dataPanel2.errors.get(0) + ")",
                    Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
            dataPanel2.errors.clear();
            return;
        }

        if (panel_3_valid) {
            optionPrice3 = dataPanel3.getOptionPrice();
            option3 = dataPanel3.getOptionType();
            strikePrice3 = dataPanel3.getStrikePrice();
            expDate3 = dataPanel3.getExpDate();
            minExpDate = Math.min(minExpDate, expDate3);
            qunatity3 = dataPanel3.getQuantity();
            totalPrice += strikePrice3;
            count++;
        } else if (dataPanel3.getErrorCount() != 2) {
            clear2DData();
            JOptionPane.showMessageDialog(this,
                    Language.getString("STRATEGY_ERROR_MSG") + " (" + dataPanel3.errors.get(0) + ")",
                    Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
            dataPanel3.errors.clear();
            return;
        }

        if (panel_4_valid) {
            optionPrice4 = dataPanel4.getOptionPrice();
            option4 = dataPanel4.getOptionType();
            strikePrice4 = dataPanel4.getStrikePrice();
            expDate4 = dataPanel4.getExpDate();
            minExpDate = Math.min(minExpDate, expDate4);
            qunatity4 = dataPanel4.getQuantity();
            totalPrice += strikePrice4;
            count++;
        } else if (dataPanel4.getErrorCount() != 2) {
            clear2DData();
            JOptionPane.showMessageDialog(this,
                    Language.getString("STRATEGY_ERROR_MSG") + " (" + dataPanel4.errors.get(0) + ")",
                    Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
            dataPanel4.errors.clear();
            return;
        }

        if (count != 0) {
            centerPrice = totalPrice / count;
        }

        if (tabpane.getSelectedIndex() == 0) {
            if (!txtRFRate2D.getText().equalsIgnoreCase("")) {
                riskFreeRate = Double.parseDouble(txtRFRate2D.getText());
            } else {
                canContinue = false;
                clear2DData();
                JOptionPane.showMessageDialog(this,
                        Language.getString("STRATEGY_ERROR_MSG") + " (" + Language.getString("RF_RATE") + ")",
                        Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            if ((panel_1_valid && optionPrice1 == 0) || (panel_2_valid && optionPrice2 == 0) || (panel_3_valid && optionPrice3 == 0) || panel_4_valid && optionPrice4 == 0) {
                clear2DData();
                canContinue = false;
                JOptionPane.showMessageDialog(this, Language.getString("STRATEGY_ERROR_OPTION_PRICE"), Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            if ((panel_1_valid && qunatity1 == 0) || (panel_2_valid && qunatity2 == 0) || (panel_3_valid && qunatity3 == 0) || panel_4_valid && qunatity4 == 0) {
                clear2DData();
                canContinue = false;
                JOptionPane.showMessageDialog(this, Language.getString("STRATEGY_ERROR_QUANTITY"), Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (!txtVolatility2D.getText().equalsIgnoreCase("")) {
                impVolatlity = Double.parseDouble(txtVolatility2D.getText());
            } else {
                clear2DData();
                canContinue = false;
                JOptionPane.showMessageDialog(this,
                        Language.getString("STRATEGY_ERROR_MSG") + " (" + Language.getString("STRATEGY_VOLATILITY") + ")",
                        Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (chkUseStockPrice2D.isSelected()) {
                if (underlyingEquity != null && (underlyingEquity.getLastTradeValue() > 0)) {
                    centerPrice = underlyingEquity.getLastTradeValue();
                }
            }
            draw2DChart = true;
            draw3DChart = false;
        } else {
            if (!txtRFRate3D.getText().equalsIgnoreCase("")) {
                riskFreeRate = Double.parseDouble(txtRFRate3D.getText());
            } else {
                clear3DData();
                JOptionPane.showMessageDialog(this,
                        Language.getString("STRATEGY_ERROR_MSG") + " (" + Language.getString("RF_RATE") + ")",
                        Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            if ((panel_1_valid && optionPrice1 == 0) || (panel_2_valid && optionPrice2 == 0) || (panel_3_valid && optionPrice3 == 0) || panel_4_valid && optionPrice4 == 0) {
                clear3DData();
                JOptionPane.showMessageDialog(this, Language.getString("STRATEGY_ERROR_OPTION_PRICE"), Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            if ((panel_1_valid && qunatity1 == 0) || (panel_2_valid && qunatity2 == 0) || (panel_3_valid && qunatity3 == 0) || panel_4_valid && qunatity4 == 0) {
                clear3DData();
                JOptionPane.showMessageDialog(this, Language.getString("STRATEGY_ERROR_QUANTITY"), Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (!txtVolatility3D.getText().equalsIgnoreCase("")) {
                impVolatlity = Double.parseDouble(txtVolatility3D.getText());
            } else {
                clear3DData();
                JOptionPane.showMessageDialog(this,
                        Language.getString("STRATEGY_ERROR_MSG") + " (" + Language.getString("STRATEGY_VOLATILITY") + ")",
                        Language.getString("STRATEGY_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (chkUseStockPrice3D.isSelected()) {
                if (underlyingEquity != null && underlyingEquity.getLastTradeValue() > 0) {
                    centerPrice = underlyingEquity.getLastTradeValue();
                }
            }
            draw2DChart = false;
            draw3DChart = true;
        }

        incrementInDays = Integer.parseInt(String.valueOf(spnNoOfDays.getValue()));

        int insType = Meta.INSTRUMENT_INDEX_OPTION;

        if (stock != null) {
            insType = stock.getInstrumentType();
        }

        if (insType == Meta.INSTRUMENT_INDEX_OPTION) {//TODO: index name
            setCalcMethod(Integer.parseInt(Settings.getIndexValuation()));
        } else {
            setCalcMethod(Integer.parseInt(Settings.getStocksValuation()));
        }

        if (draw2DChart) {
            process2DData();
        }
        if (draw3DChart) {
            process3DData();
        }
    }

    protected void clearData() {
        dataPanel1.setOptionPrice("");
        dataPanel1.setQuanitity("1");

        lblTheoPrice1.setText("");
        lblTheoPrice2.setText("");
        lblTheoPrice3.setText("");
        lblTheoPrice4.setText("");

        cmbGreeks1.setSelectedIndex(0);
        cmbGreeks2.setSelectedIndex(0);
        cmbGreeks3.setSelectedIndex(0);
        cmbGreeks4.setSelectedIndex(0);

        cmbGreeks1.setEnabled(false);
        cmbGreeks2.setEnabled(false);
        cmbGreeks3.setEnabled(false);
        cmbGreeks4.setEnabled(false);

        lblGreeks1.setText("");
        lblGreeks2.setText("");
        lblGreeks3.setText("");
        lblGreeks4.setText("");

    }

    private void highLight3DCenterRow(Table table) {
        int rowNumber = (table.getModel().getRowCount() / 2);
        table.getTable().setRowSelectionInterval(rowNumber, rowNumber);
        Rectangle r = table.getTable().getCellRect(rowNumber - 1, 0, true);
        table.getTable().scrollRectToVisible(table.getTable().getCellRect(table.getTable().getRowCount() - 1, 0, true));
        table.getTable().scrollRectToVisible(r);
        table.scrollRectToVisible(table.getTable().getCellRect(rowNumber - 1, 0, true));
    }
	/*=================================== end of 3D charts =====================================*/

    public void applyTheme() {
        super.applyTheme();

        pnl2DXAxisHeader.setBackground(Theme.getColor("STRATEGY_AXIS_BG_COLOR"));
        panel3DChart.setBackground(this.getBackground());
        lbl2DXAxis.setForeground(Theme.getColor("STRATEGY_AXIS_FG_COLOR"));

        pnl3DXAxisHeader.setBackground(Theme.getColor("STRATEGY_AXIS_BG_COLOR"));
        lbl3DXAxis.setForeground(Theme.getColor("STRATEGY_AXIS_FG_COLOR"));

        pnlzAxisHeader.setBackground(Theme.getColor("STRATEGY_AXIS_BG_COLOR"));
        lblZAxis.setForeground(Theme.getColor("STRATEGY_AXIS_FG_COLOR"));

        option1Color = Theme.getColor("STRATEGY_OPTION1_COLOR");
        option2Color = Theme.getColor("STRATEGY_OPTION2_COLOR");
        option3Color = Theme.getColor("STRATEGY_OPTION3_COLOR");
        option4Color = Theme.getColor("STRATEGY_OPTION4_COLOR");

        calcChartColor = Theme.getColor("STRATEGY_CALC_COLOR");
        expChartColor = Theme.getColor("STRATEGY_EXP_COLOR");

        ArrayList<ChartSeries> collection = individualChart.getSeriesCollection();
        for (ChartSeries series : collection) {

            if (series.getPanelId() == OptionsDataPanel.PANEL_ID_1) {
                series.setChartColor(option1Color);
            } else if (series.getPanelId() == OptionsDataPanel.PANEL_ID_2) {
                series.setChartColor(option2Color);
            } else if (series.getPanelId() == OptionsDataPanel.PANEL_ID_3) {
                series.setChartColor(option3Color);
            } else if (series.getPanelId() == OptionsDataPanel.PANEL_ID_4) {
                series.setChartColor(option4Color);
            }
        }
        individualChart.repaint();

        ArrayList<ChartSeries> collection2 = combinedChart.getSeriesCollection();
        for (ChartSeries series : collection2) {
            if (series.getChartType() == DataManager.SERIRES_CALC) {
                series.setChartColor(calcChartColor);
            } else if (series.getChartType() == DataManager.SERIRES_EXP) {
                series.setChartColor(expChartColor);
            }
        }
        combinedChart.repaint();

        if (panel3DChart.getComponentCount() == 0) {
            panel3DChart.setBackground(Theme.getColor("STRATEGY_CHART_OUTSIDE_COLOR"));
        }

    }

    //getters and setters
    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    protected OptionsDataPanel getDataPanel1() {
        return dataPanel1;
    }

    protected OptionsDataPanel getDataPanel2() {
        return dataPanel2;
    }

    protected OptionsDataPanel getDataPanel3() {
        return dataPanel3;
    }

    protected OptionsDataPanel getDataPanel4() {
        return dataPanel4;
    }

    public long getMinExpDate() {
        return minExpDate;
    }

    public double getCenterPrice() {
        return centerPrice;
    }

    private void startEquitySymboThread(String sKey) {
        if (sKey != null) {
            final Stock equityStk = DataStore.getSharedInstance().getStockObject(sKey, true);
            underlyingEquity = DataStore.getSharedInstance().getStockObject(sKey);
            DataStore.getSharedInstance().addSymbolRequest(sKey);
            new Thread(new Runnable() {
                public void run() {
                    int loop = 5;
                    while (Double.compare(/*equityStk.getAAnnualVolatility()*/5, Double.NaN) == 0 && loop > 0) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        loop--;
                    }

                    if (Double.compare(/*equityStk.getAnnualVolatility()*/5, Double.NaN) == 0) {
                        impVolatlity = 0;
//                        equityStk.setAnnualVolatility(0);
                    } else {
                        impVolatlity = 5/*equityStk.getAnnualVolatility()*/;
                    }

                    isLoadingEqVol = false;
                    volatilityChanged = true;
                    extractDataFromUI();

                    //To change body of implemented methods use File | Settings | File Templates.
                }
            }).start();
        }

        isLoadingEqVol = false;
    }

    private int validatePanels() {
        count = 0;
        panel_1_valid = dataPanel1.validate2DData();
        panel_2_valid = dataPanel2.validate2DData();
        panel_3_valid = dataPanel3.validate2DData();
        panel_4_valid = dataPanel4.validate2DData();
        if (panel_1_valid) {
            count++;
        }
        if (panel_2_valid) {
            count++;
        }
        if (panel_3_valid) {
            count++;
        }
        if (panel_4_valid) {
            count++;
        }
        if ((panel_1_valid && (dataPanel1.getOptionPrice() == 0)) || (panel_2_valid && (dataPanel2.getOptionPrice() == 0)) || (panel_3_valid && (dataPanel3.getOptionPrice() == 0)) || (panel_4_valid && (dataPanel4.getOptionPrice() == 0))) {
            count = 0;
        }
        if ((panel_1_valid && (dataPanel1.getQuantity() == 0)) || (panel_2_valid && (dataPanel2.getQuantity() == 0)) || (panel_3_valid && (dataPanel3.getQuantity() == 0)) || (panel_4_valid && (dataPanel4.getQuantity() == 0))) {
            count = 0;
        }
        return count;
    }

    public boolean isLoadingEqVol() {
        return isLoadingEqVol;
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        isLoadingEqVol = false;
        if (getStock() != null) {
            DataStore.getSharedInstance().removeSymbolRequest(getStock().getKey());
        }
    }

    private OptionsDataPanel getPanelByID(int panelID) {
        switch (panelID) {
            case OptionsDataPanel.PANEL_ID_1:
                return dataPanel1;
            case OptionsDataPanel.PANEL_ID_2:
                return dataPanel2;
            case OptionsDataPanel.PANEL_ID_3:
                return dataPanel3;
            case OptionsDataPanel.PANEL_ID_4:
                return dataPanel4;
        }
        return dataPanel1;
    }

    protected void clearGreeksByPanelID(int panelID) {

        if (panelID == OptionsDataPanel.PANEL_ID_1) {
            lblTheoPrice1.setText("");
            cmbGreeks1.setSelectedIndex(0);
            cmbGreeks1.setEnabled(false);
            lblGreeks1.setText("");
        } else if (panelID == OptionsDataPanel.PANEL_ID_2) {
            lblTheoPrice2.setText("");
            cmbGreeks2.setSelectedIndex(0);
            cmbGreeks2.setEnabled(false);
            lblGreeks2.setText("");
        } else if (panelID == OptionsDataPanel.PANEL_ID_3) {
            lblTheoPrice3.setText("");
            cmbGreeks3.setSelectedIndex(0);
            cmbGreeks3.setEnabled(false);
            lblGreeks3.setText("");
        } else if (panelID == OptionsDataPanel.PANEL_ID_4) {
            lblTheoPrice4.setText("");
            cmbGreeks4.setSelectedIndex(0);
            cmbGreeks4.setEnabled(false);
            lblGreeks4.setText("");
        }
    }

    private void clear2DData() {

        individualChart.getSeriesCollection().clear();
        individualChart.repaint();
        combinedChart.getSeriesCollection().clear();
        combinedChart.repaint();

        if (objectStore != null) {
            objectStore.clear();
            table2D.updateUI();
            table2D.repaint();
        }
        if (slider != null) {
            slider.setVisible(false);
        }
    }

    private void clear3DData() {

        if (canvas != null && panel3DChart.getComponentCount() == 1) {

            panel3DChart.remove(canvas);
            panel3DChart.updateUI();
            canvas = null;

            objectStore3D.clear();
            if (panel3Dtable.getComponentCount() == 1) {
                panel3Dtable.remove(0);
                Table tableNew = new Table();
                ViewSetting viewSetting = ViewSettingsManager.getSummaryView("STRATEGY_3D");
                GUISettings.setColumnSettings(viewSetting, ChartInterface.getColumnSettings("STRATEGY_3D_DATA_COLS"));
                model3D = new Strategy3DTableModel(objectStore3D);
                model3D.setViewSettings(viewSetting);
                tableNew.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
                tableNew.setSortingEnabled();
                tableNew.setModel(model3D);
                model3D.setTable(tableNew, new Strategy3DRenderer(viewSetting.getColumnHeadings(), model3D.getRendIDs()));
                panel3Dtable.add(tableNew, 0);
                ((SmartTable) tableNew.getTable()).adjustColumnWidthsToFit(40);
            }
        }
    }

    public void stateChanged(ChangeEvent e) {

        long begin = System.currentTimeMillis();
        referenceDay = 0;
        JSlider source = (JSlider) e.getSource();
        int current = Integer.parseInt(String.valueOf(source.getValue()));
        referenceDay = currentDayInMillis + ((minExpDate - currentDayInMillis) / (TOTAL_DAYS)) * current;

        double tempRef = (currentDayInMillis + ((double) (minExpDate - currentDayInMillis) / (double) (TOTAL_DAYS)) * current);
        referenceDay = ((long) (Math.round(tempRef) * 10000)) / 10000;

        if (referenceDay > minExpDate) {
            referenceDay = minExpDate;
            System.out.println("+++++++++++++++++++");
        }
        extractDataFromUI();
        //process2DData();
        setSliderParams();

        //System.out.println("************ time in slider *********** " + (double) (System.currentTimeMillis() - begin));
    }

    public int getCalcMethod() {
        return calcMethod;
    }

    public void setCalcMethod(int calcMethod) {
        this.calcMethod = calcMethod;
    }

    public void componentResized(ComponentEvent e) {

        splitPane3DBottom.setDividerLocation(this.getWidth() / 2);
        splitPane2DBottom.setDividerLocation((int) ((this.getWidth()) * 0.65 - 4 - SPLIT_PANE_DIVIDER_SIZE));
        //paneChart.setDividerLocation(paneChart.getDividerLocation());
    }

    //getter setters for Current Price
    public double getCurrentPrice1() {
        return currentPrice1;
    }

    public void setCurrentPrice1(double currentPrice1) {
        this.currentPrice1 = currentPrice1;
    }

    public double getCurrentPrice2() {
        return currentPrice2;
    }

    public void setCurrentPrice2(double currentPrice2) {
        this.currentPrice2 = currentPrice2;
    }

    public double getCurrentPrice3() {
        return currentPrice3;
    }

    public void setCurrentPrice3(double currentPrice3) {
        this.currentPrice3 = currentPrice3;
    }

    public double getCurrentPrice4() {
        return currentPrice4;
    }

    public void setCurrentPrice4(double currentPrice4) {
        this.currentPrice4 = currentPrice4;
    }

    private class CustomButton extends TWButton {

        private CustomButton(String text) {
            super(text);
        }

        public Insets getInsets() {
            return new Insets(0, 0, 0, 0);
        }
    }

}


