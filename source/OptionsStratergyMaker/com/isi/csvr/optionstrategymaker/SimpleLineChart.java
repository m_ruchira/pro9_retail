package com.isi.csvr.optionstrategymaker;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jan 16, 2009
 * Time: 9:01:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleLineChart extends JPanel implements ComponentListener, Themeable {

    protected static byte CHART_TYPE_INDIVIDIAL = 0;
    private byte chartType = CHART_TYPE_INDIVIDIAL;
    protected static byte CHART_TYPE_COMBINE = 1;
    private final float[] PriceVolPitch = {1f, 2f, 4f, 5f, 8f};
    private final float Y_ADJ_FACTOR = 0.2f;
    private final float X_ADJ_FACTOR = 0.04f;
    private final String TITLE_INDIVIDUAL = Language.getString("STRATEGY_INDV_CHART");
    private final String TITLE_COMBINE = Language.getString("STRATEGY_COM_CHART");
    //private final double HALF_INCH = 72 / 3d;
    private final double HALF_INCH = Toolkit.getDefaultToolkit().getScreenResolution() / 2;
    protected double meanPrice = 0;
    private ArrayList<ChartSeries> seriesCollection = new ArrayList<ChartSeries>();
    private int chartWidth;
    private int chartHeight;
    private int legendHeight = 35;
    private int yAxisWidth = 50;
    private int xAxisHeight = 35;
    private double minY = Float.MAX_VALUE;
    private double maxY = -Float.MIN_VALUE;
    private double minX = Float.MAX_VALUE;
    private double maxX = -Float.MIN_VALUE;
    private double xFactor;
    private double yFactor;
    private double adjY = 10;
    private double adjX = 10;
    private double bestXPitch;
    private double bestYPitch;
    private double xStart;
    private double yStart;
    private double maxPrice = 0;
    private boolean inPctValues = true;
    private boolean inAbsoluteValues = true;
    private TWDecimalFormat priceFormatter = new TWDecimalFormat("###,###,###");
    private TWDecimalFormat percentageFormatter = new TWDecimalFormat("###,###,#0.00");
    private TWDecimalFormat formatter = new TWDecimalFormat("###,###,###,###.000");
    private float penWidth = 1.2f;
    private float[] dashArr = new float[]{3, 6, 3, 6};
    private BasicStroke stroke = new BasicStroke(penWidth, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 0, dashArr, 0);
    private Font axisFont = new Font("Arial", Font.PLAIN, 11);
    private Font xAxisFont = new Font("Arial", Font.BOLD, 10);
    private Font titleFont = new Font("MS sans", Font.BOLD, 11);
    private Font legendBottomFont = new Font("Arial", Font.BOLD, 11);
    //colors
    private Color chartOutSideColor;
    private Color chartBorderColor;
    private Color chartBackColor = new Color(200, 200, 250, 70);
    private Color chartGridColor = new Color(0, 0, 0, 100);
    private Color chartAxisFontColor = new Color(0, 0, 0);
    private Color chartTitleColor = new Color(255, 102, 0);
    private Color positivePctColor = new Color(153, 51, 0);
    private Color negativePctColor = Color.RED;
    private Image image = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif").getImage();
    private DecimalFormat microformat = new DecimalFormat("0.000"); //TODO; THIS WAS 9 eariler

    public SimpleLineChart(byte type) {
        super();
        this.chartType = type;
        this.addComponentListener(this);
        Theme.registerComponent(this);
        applyTheme();
    }

    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(chartOutSideColor);
        g.fillRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
        g.setColor(chartBorderColor);
        g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);

        /*if (OptionStrategyMakerUI.getSharedInstance().isLoadingEqVol()) {
            g.drawImage(image, (this.getWidth() - image.getWidth(this)) / 2, this.getHeight() / 2, this);
        }*/
        if (seriesCollection.size() < 1) {
            return;
        }
        g.setColor(chartBackColor);
        g.fillRect(yAxisWidth, legendHeight, chartWidth, chartHeight);
        g.setColor(chartBorderColor);
        g.drawRect(yAxisWidth, legendHeight, chartWidth, chartHeight);

        drawLegend(g);
        drawXAxisLabels(g);
        drawYAxisLabels(g);
        drawCharts(g);
    }

    private void drawCharts(Graphics g) {

        for (ChartSeries series : seriesCollection) {
            if (series.count > 1) {

                g.setColor(series.chartColor);
                ((Graphics2D) g).setStroke(new BasicStroke(penWidth));
                g.drawPolyline(series.xPoints, series.yPoints, series.xPoints.length);
            }
        }
    }

    private void drawXAxisLabels(Graphics g) {

        Graphics2D gg = (Graphics2D) g;
        double currentVal = minX + bestXPitch;
        //double currentVal = minX; //TODO:eariler
        float y = 0;

        while (currentVal <= maxX) {
            float x = getPixelForXValue(currentVal);
            g.setColor(chartGridColor);
            gg.setStroke(stroke);
            g.drawLine((int) x, legendHeight, (int) x, (legendHeight + chartHeight));
            gg.setStroke(new BasicStroke(.8f));
            g.setColor(chartAxisFontColor);
            g.drawLine((int) x, (legendHeight + chartHeight), (int) x, (legendHeight + chartHeight + 3));

            currentVal = currentVal + bestXPitch;
        }

        if (inAbsoluteValues) {
            //currentVal = xStart;
            currentVal = minX;

            g.setColor(chartAxisFontColor);
            g.setFont(xAxisFont);
            gg.rotate(Math.toRadians(-90));
            int labelHeight = 0;
            float x;

            double allowedPitch;
            if (xFactor == 0) {
                allowedPitch = bestXPitch;
            }
            allowedPitch = getAllowedPitch(bestXPitch, chartWidth, xFactor);
            priceFormatter = getNumberFormatter(allowedPitch);

            while (currentVal <= maxX) {
                x = getPixelForXValue(currentVal);
                y = getPixelForYValue(0);

                String value = priceFormatter.format(currentVal);
                labelHeight = g.getFontMetrics(xAxisFont).stringWidth(value);
                int labelWidth = g.getFontMetrics(xAxisFont).getHeight();
                y = y + labelHeight + 5;

                if (y > (legendHeight + chartHeight)) {
                    y = legendHeight + chartHeight - 5;
                }
                if ((x - 3 - labelWidth) > yAxisWidth && (x - 3 - labelWidth) < (yAxisWidth + chartWidth)) {
                    g.drawString(value, -(int) y, (int) x - 3);
                }
                currentVal = currentVal + bestXPitch;
            }

            gg.rotate(Math.toRadians(90));
        } else {

            //currentVal = xStart;// + bestXPitch;
            currentVal = minX;// + bestXPitch;

            g.setFont(xAxisFont);

            gg.rotate(Math.toRadians(-90));
            int labelHeight = 0;

            while (currentVal <= maxX) {
                float x = getPixelForXValue(currentVal);
                y = getPixelForYValue(0);
                double percentage = (((currentVal - meanPrice) / meanPrice) * 1000) / 10;
                if (percentage > 0) {
                    g.setColor(positivePctColor);

                } else {
                    g.setColor(negativePctColor);
                }
                String value = percentageFormatter.format(percentage) + " %";
                int labelWidth = g.getFontMetrics(xAxisFont).getHeight();
                labelHeight = g.getFontMetrics(xAxisFont).stringWidth(value);
                y = y + labelHeight + 5;

                if (y > (legendHeight + chartHeight)) {
                    y = legendHeight + chartHeight + 5;
                }

                if ((x - labelWidth - 3) > yAxisWidth && (x - 3 - labelWidth) < (yAxisWidth + chartWidth)) {
                    g.drawString(value, -(int) y, (int) x - 3);
                }
                currentVal = currentVal + bestXPitch;
            }

            gg.rotate(Math.toRadians(90));
        }
    }

    private void drawLegend(Graphics g) {

        //drawing the main title
        String title = "";
        if (chartType == SimpleLineChart.CHART_TYPE_INDIVIDIAL) {
            title = TITLE_INDIVIDUAL;
        } else {
            title = TITLE_COMBINE;
        }
        g.setColor(chartTitleColor);
        g.setFont(titleFont);
        int height = g.getFontMetrics(titleFont).getHeight();
        g.drawString(title, yAxisWidth, legendHeight - (legendHeight - height) / 2);  //drawing the chart title

        int xStart = 0;
        int yStart = 0;
        final int sqaureSize = 10;
        int len;

        for (int i = seriesCollection.size() - 1; i >= 0; i--) {

            ChartSeries s = seriesCollection.get(seriesCollection.size() - 1 - i);
            String name = s.getName();

            len = g.getFontMetrics(legendBottomFont).stringWidth(name);

            xStart = yAxisWidth + chartWidth - (len + sqaureSize + 12) * (i + 1);
            yStart = legendHeight + chartHeight + (xAxisHeight / 2) - 3;

            g.setColor(s.chartColor);
            g.drawRect(xStart, yStart, sqaureSize, sqaureSize);
            g.fillRect(xStart, yStart, sqaureSize, sqaureSize);

            g.setColor(chartAxisFontColor);
            g.setFont(legendBottomFont);
            g.drawString(name, xStart + sqaureSize + 4, yStart + sqaureSize);
        }

    }

    private void drawYAxisLabels(Graphics g) {
        float labelLength = 0;
        double currVal = bestYPitch;
        float x = yAxisWidth - 5;
        float yAdj = -1;

        double allowedPitch;
        if (yFactor == 0) {
            allowedPitch = bestYPitch;
        }
        allowedPitch = getAllowedPitch(bestYPitch, chartHeight, yFactor);
        priceFormatter = getNumberFormatter(allowedPitch);

        Graphics2D gg = (Graphics2D) g;

        while (currVal <= maxY) {
            float y = getPixelForYValue(currVal);
            g.setColor(chartGridColor);
            gg.setStroke(stroke);

            if (currVal == 0) {
                gg.setStroke(new BasicStroke(1.45f));  // draws the 0 horizontal line
            }

            g.drawLine(yAxisWidth, (int) y, yAxisWidth + chartWidth, (int) y);
            g.setColor(chartAxisFontColor);
            g.drawLine(yAxisWidth - 3, (int) y, yAxisWidth, (int) y);
            labelLength = g.getFontMetrics().stringWidth(priceFormatter.format(currVal));
            g.setColor(chartAxisFontColor);
            g.setFont(axisFont);
            g.drawString(priceFormatter.format(currVal), (int) (x - labelLength), (int) (y + yAdj));
            currVal += bestYPitch;
        }

        currVal = 0;

        while (currVal > minY) {

            if (currVal == 0) {
                g.setColor(new Color(0, 0, 0, 150));
                gg.setStroke(new BasicStroke(.4f));
            } else {
                g.setColor(chartGridColor);
                gg.setStroke(stroke);
            }

            float y = getPixelForYValue(currVal);
            g.drawLine(yAxisWidth, (int) y, (yAxisWidth + chartWidth), (int) y);
            g.setColor(chartAxisFontColor);
            g.drawLine(yAxisWidth - 3, (int) y, yAxisWidth, (int) y);
            labelLength = g.getFontMetrics().stringWidth(String.valueOf(priceFormatter.format((int) currVal)));
            g.setColor(chartAxisFontColor);
            g.setFont(axisFont);
            g.drawString(priceFormatter.format(currVal), (int) (x - labelLength), (int) (y + yAdj));
            currVal -= bestYPitch;
        }
    }

    protected void calculatePixelvalues() {

        minY = Float.MAX_VALUE;
        maxY = -Float.MIN_VALUE;
        minX = Float.MAX_VALUE;
        maxX = -Float.MIN_VALUE;
        maxPrice = -Float.MIN_VALUE;

        if (seriesCollection.size() == 0) {
            return;
        }

        for (ChartSeries s : seriesCollection) {
            for (int m = 0; m < s.count; m++) {

                maxY = Math.max(maxY, s.yData[m]);
                minY = Math.min(minY, s.yData[m]);

                minX = Math.min(minX, s.xData[m]);
                maxX = Math.max(maxX, s.xData[m]);

                maxPrice = Math.max(maxPrice, Math.abs(s.yData[m]));
            }
        }

        if (minX == maxX) {
            minX = minX - 1;
            maxX = maxX + 1;
        }
        if (minY == maxY) {
            minY = minY - 10;
            maxY = maxY + 10;
        }

        if (minY > 0) {
            minY = 0;
        }
        if (minX < 0) {
            minX = 0;
        }

        Graphics g = this.getGraphics();
        yAxisWidth = g.getFontMetrics(axisFont).stringWidth(formatter.format(maxPrice)) + 5;

        chartWidth = Math.max(this.getWidth() - 20 - yAxisWidth, 50);
        chartHeight = Math.max(this.getHeight() - legendHeight - xAxisHeight, 20);

        adjY = (maxY - minY) * Y_ADJ_FACTOR;
        adjX = (maxX - minX) * X_ADJ_FACTOR;
        maxY = maxY + adjY;
        minY = minY - adjY;
        maxX = maxX + adjX;
        minX = minX - adjX;

        yFactor = (chartHeight) / (maxY - minY);
        xFactor = (chartWidth) / (maxX - minX);

        bestXPitch = getBestInterval((maxX - minX) * 1, chartWidth);
        bestYPitch = getBestInterval(maxY - minY, chartHeight);

        //TODO: VERY IMPORTATANT. MAY BE NEED TO USE IN THE FUTURE
        /*
        xStart = getStart(minX, bestXPitch);
        yStart = getStart(minY, bestYPitch);
        */

        for (ChartSeries series : seriesCollection) {
            if (series.count > 0) {
                int count = series.count;
                Point2D.Float[] points = new Point2D.Float[count];

                int[] xPoints = new int[count];
                int[] yPoints = new int[count];

                double xValue = 1;
                double yValue = 1;
                for (int k = 0; k < points.length; k++) {
                    xValue = series.xData[k];
                    yValue = series.yData[k];
                    xPoints[k] = (int) getPixelForXValue(xValue);
                    yPoints[k] = (int) getPixelForYValue(yValue);

                }
                series.xPoints = xPoints;
                series.yPoints = yPoints;
            } else {
                series.points = null;
            }
        }
    }

    private float getBestInterval(double val, double pixLen) {

        if (seriesCollection.size() < 1) return 40;

        val = val / pixLen * HALF_INCH;

        float pitch = PriceVolPitch[0];
        for (long j = 1; j < Long.MAX_VALUE; j *= 10) {
            float Multiplier = ((float) j / 1000000000);
            for (float aPriceVolPitch : PriceVolPitch) {
                if (val >= (double) Multiplier * aPriceVolPitch) {
                    pitch = (Multiplier * aPriceVolPitch);
                } else {
                    return pitch;
                }
            }
            if (j > Long.MAX_VALUE / 10f) {
                return pitch;
            }
        }
        return pitch;
    }

    public double getAllowedPitch(double bestPitch, float height, double yfactor) {
        double minGap = getMinimumGap(height);
        double result = bestPitch;
        double currGap = result * yfactor;
        while (currGap < minGap) {
            if (currGap > height) break;
            result += bestPitch;
            currGap = result * yfactor;
        }
        return result;
    }

    private double getMinimumGap(float height) {
        double result = HALF_INCH / 2;
        if (height <= 2 * HALF_INCH) {
            result = HALF_INCH / 4;
        } else if (height <= 4 * HALF_INCH) {
            result = HALF_INCH / 3;
        }
        return result;
    }

    private TWDecimalFormat getNumberFormatter(double allowedPitch) {
        String pattern = "###,###,##0";
        // microformat here must have the pattern "0.000000000"
        String strNum = microformat.format(allowedPitch);
        int decimalCount = 3; //TODO; THIS WAS 9 eariler
        while (strNum.lastIndexOf("0") == strNum.length() - 1) {
            strNum = strNum.substring(0, strNum.length() - 1);
            decimalCount--;
        }
        if (decimalCount > 0) {
            pattern = "###,##0.00";
            for (int i = 2; i < decimalCount; i++) {
                pattern += "0";
            }
        }
        return new TWDecimalFormat(pattern);
    }

    private double getStart(double min, double bestPitch) {
        long i = 0;
        while (i < Double.MAX_VALUE / bestPitch) {
            if (i * bestPitch >= min) {
                return i * bestPitch;
            }
            i++;
        }
        return i * bestPitch;
    }

    private float getPixelForXValue(double value) {
        return (float) ((value - minX) * xFactor) + yAxisWidth;
    }

    private float getPixelForYValue(double value) {
        return (float) (chartHeight + legendHeight - (value - minY) * yFactor);
    }

    public void setInPctValues(boolean inPctValues) {
        this.inPctValues = inPctValues;
    }

    public void setInAbsoluteValues(boolean inAbsoluteValues) {
        this.inAbsoluteValues = inAbsoluteValues;
    }

    public ArrayList<ChartSeries> getSeriesCollection() {
        return seriesCollection;
    }

    public void setSeriesCollection(ArrayList<ChartSeries> seriesCollection) {
        this.seriesCollection = seriesCollection;
    }

    public void componentResized(ComponentEvent e) {
        calculatePixelvalues();
        repaint();
    }

    public void componentMoved(ComponentEvent e) {

    }

    public void componentShown(ComponentEvent e) {

    }

    public void componentHidden(ComponentEvent e) {

    }

    public void applyTheme() {

        chartOutSideColor = Theme.getColor("STRATEGY_CHART_OUTSIDE_COLOR");
        chartBorderColor = Theme.getColor("STRATEGY_CHART_BORDER_COLOR");
        chartBackColor = Theme.getColor("STRATEGY_CHART_BACK_COLOR");
        chartGridColor = Theme.getColor("STRATEGY_CHART_GRID_COLOR");
        chartAxisFontColor = Theme.getColor("STRATEGY_CHART_AXIS_FONT_COLOR");
        chartTitleColor = Theme.getColor("STRATEGY_CHART_TITLE_COLOR");
        positivePctColor = Theme.getColor("STRATEGY_POSITIVE_PCT_COLOR");
        negativePctColor = Theme.getColor("STRATEGY_NEGATIVE_PCT_COLOR");
    }
}
