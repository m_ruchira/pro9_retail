package com.isi.csvr.optionstrategymaker;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.OptionStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.options.OptionStrategyListener;
import com.isi.csvr.options.OptionStrategyStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by IntelliJ IDEA. User: janithk Date: Jan 16, 2009 Time: 7:01:49 PM To change this template use File |
 * Settings | File Templates.
 */

public class OptionsDataPanel extends JPanel implements ActionListener, Themeable, OptionStrategyListener, ItemListener {

    protected static final byte PANEL_ID_1 = 1;
    protected static final byte PANEL_ID_2 = 2;
    protected static final byte PANEL_ID_3 = 3;
    protected static final byte PANEL_ID_4 = 4;
    private final String TYPE_CALL_EUROPEAN = Language.getString("STRATEGY_CALL_EUROPEAN");
    private final String TYPE_CALL_AMERICAN = Language.getString("STRATEGY_CALL_AMERICAN");
    private final String TYPE_PUT_EUROPEAN = Language.getString("STRATEGY_PUT_EUROPEAN");
    private final String TYPE_PUT_AMERICAN = Language.getString("STRATEGY_PUT_AMERICAN");
    //    private JLabel lblCallPut;
    public TWButton btnSymSearch;
    public TWButton btnClear;
    public TWButton btnOptionPrice;
    public TWTextField txtSymbol;
    public TWComboBox cmbStrikePrice;
    public TWComboBox cmbExpDate;
    public ArrayList<String> errors = new ArrayList<String>();
    private TWComboBox cmbBuySell;
    private TWComboBox cmbCallPut;
    private TWTextField txtOptionPrice;
    private TWTextField txtRFRate;
    private TWTextField txtQty;
    private byte panelID;
    private ValueFormatter optionPriceFormatter = new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL, 10L, 5);
    private ValueFormatter qtyFormatter = new ValueFormatter(ValueFormatter.POSITIVE_INTEGER, 10L);
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private Color labelBorderColor = Color.BLACK;
    private ArrayList<TWComboItem> expiryDateList;
    private ArrayList<TWComboItem> strickPriceList;
    private ArrayList<String> monthlist = new ArrayList<String>();
    private String selectedMonth;
    private JPanel pnlSymbol;
    private JLabel symbolDescription;
    private int errorCount = 0;

    public OptionsDataPanel(byte panelid) {

        this.panelID = panelid;
        strickPriceList = new ArrayList<TWComboItem>();
        expiryDateList = new ArrayList<TWComboItem>();
        //   this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "20", "20"}, 0, 5));
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "20"}, 0, 5));
        cmbBuySell = GetBuySellCombo();
        cmbCallPut = GetPutCallCombo();
        btnSymSearch = new TWButton("..");
        btnSymSearch.addActionListener(this);

        btnClear = new TWButton();
        btnClear.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/strategy_clear.gif"));
        btnClear.setToolTipText(Language.getString("STRATEGY_CLEAR"));
        btnClear.addActionListener(this);
        txtSymbol = new TWTextField();

        btnOptionPrice = new TWButton();
        btnOptionPrice.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/strategy_option_price.gif"));
        btnOptionPrice.addActionListener(this);
        btnOptionPrice.setToolTipText(Language.getString("STRATEGY_CURRENT_OPTION_PRICE"));

        cmbStrikePrice = new TWComboBox(new TWComboModel(strickPriceList));
        cmbStrikePrice.setBorder(BorderFactory.createLineBorder(labelBorderColor));
        //        cmbStrikePrice.setHorizontalAlignment(JTextField.RIGHT);

        txtOptionPrice = new TWTextField();
        txtOptionPrice.setDocument(optionPriceFormatter);
        txtOptionPrice.setHorizontalAlignment(JTextField.RIGHT);

        cmbExpDate = new TWComboBox(new TWComboModel(expiryDateList));
        //        cmbExpDate.setHorizontalAlignment(JTextField.RIGHT);
        cmbExpDate.setBorder(BorderFactory.createLineBorder(labelBorderColor));

        txtQty = new TWTextField();
        txtQty.setDocument(qtyFormatter);
        txtQty.setHorizontalAlignment(JTextField.RIGHT);
        txtQty.setText("");

        txtSymbol.setEnabled(false);
        symbolDescription = new JLabel();

        if (panelID == PANEL_ID_1) {
            //  pnlSymbol = new JPanel(new FlexGridLayout(new String[]{"60","100", "25", "20","100%"}, new String[]{"20"}, 0, 0));
            pnlSymbol = new JPanel(new FlexGridLayout(new String[]{"20%", "13%", "3%", "3%", "5", "61%"}, new String[]{"20"}, 0, 0));
            pnlSymbol.add(new JLabel(Language.getString("STRATEGY_OPTION")));
            pnlSymbol.add(txtSymbol);
            pnlSymbol.add(btnSymSearch);
            pnlSymbol.add(btnClear);
            pnlSymbol.add(new JLabel(""));
            pnlSymbol.add(symbolDescription);
        } else {
            pnlSymbol = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 0, 0));
            pnlSymbol.add(txtSymbol);
        }
        // this.add(pnlSymbol);
        this.add(cmbExpDate);
        this.add(cmbStrikePrice);

        JPanel pnlBuySell = new JPanel(new FlexGridLayout(new String[]{"49%", "2%", "49%"}, new String[]{"20"}, 0, 0));
        pnlBuySell.add(cmbBuySell);
        pnlBuySell.add(new JLabel());
        pnlBuySell.add(cmbCallPut);
        this.add(pnlBuySell);

        //alteration for option price button
        JPanel pnlOptionPrice = new JPanel(new FlexGridLayout(new String[]{"85%", "15%"}, new String[]{"20"}, 0, 0));
        pnlOptionPrice.add(txtOptionPrice);
        pnlOptionPrice.add(btnOptionPrice);

        this.add(pnlOptionPrice);
        this.add(txtQty);

        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
    }

    public int getPanelID() {
        return panelID;
    }

    public JPanel getPnlSymbol() {
        return pnlSymbol;
    }

    private TWComboBox GetBuySellCombo() {

        TWComboBox cmb = new TWComboBox();
        cmb.addItem(Language.getString("STRATEGY_BUY"));
        cmb.addItem(Language.getString("STRATEGY_SELL"));

        return cmb;
    }

    private TWComboBox GetPutCallCombo() {

        TWComboBox cmb = new TWComboBox();
        cmb.addItem(TYPE_CALL_AMERICAN);
        cmb.addItem(TYPE_PUT_AMERICAN);

        return cmb;
    }

    public double getStrikePrice() {
        return Double.parseDouble(((TWComboItem) cmbStrikePrice.getSelectedItem()).getValue().trim());
    }

    public double getOptionPrice() {
        return Double.parseDouble(txtOptionPrice.getText().trim());
    }

    public void setOptionPrice(String text) {
        this.txtOptionPrice.setText(text);
    }

    public long getExpDate() {
        return getDate();
    }

    public double getRFRate() {
        return Double.parseDouble(txtRFRate.getText().trim());
    }

    public void setRFRate(String text) {
        this.txtRFRate.setText(text);
    }

    public int getQuantity() {
        return Integer.parseInt(txtQty.getText().trim());
    }

    public void setQuanitity(String text) {
        this.txtQty.setText(text);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnSymSearch) {
            if (OptionStrategyMakerUI.getSharedInstance().getStock() != null) {
                searchSymbol();
            } else {
                searchSymbol();
            }
        } else if (e.getSource() == btnClear) {
            clearAllData();
            OptionStrategyMakerUI.getSharedInstance().clearGreeksByPanelID(panelID);
            symbolDescription.setText("");

        } else if (e.getSource() == btnOptionPrice) {
            /*if (OptionStrategyMakerUI.getSharedInstance().getStock() != null && !txtSymbol.getText().equalsIgnoreCase("")) {
                   if (OptionStrategyMakerUI.getSharedInstance().getStock().getLastTradedPrice() !=0) {
                       txtOptionPrice.setText(Double.toString(OptionStrategyMakerUI.getSharedInstance().getStock().getLastTradedPrice()));
                   }else{
                       txtOptionPrice.setText(Double.toString(OptionStrategyMakerUI.getSharedInstance().getStock().getPreviousClosed()));
                   }
               }*/

            if (!txtSymbol.getText().equalsIgnoreCase("")) {
                switch (panelID) {
                    case PANEL_ID_1:
                        txtOptionPrice.setText(Double.toString(OptionStrategyMakerUI.getSharedInstance().getCurrentPrice1()));
                        break;
                    case PANEL_ID_2:
                        txtOptionPrice.setText(Double.toString(OptionStrategyMakerUI.getSharedInstance().getCurrentPrice2()));
                        break;
                    case PANEL_ID_3:
                        txtOptionPrice.setText(Double.toString(OptionStrategyMakerUI.getSharedInstance().getCurrentPrice3()));
                        break;
                    case PANEL_ID_4:
                        txtOptionPrice.setText(Double.toString(OptionStrategyMakerUI.getSharedInstance().getCurrentPrice4()));
                        break;
                }
            }
        }
    }


    public void searchSymbol() {
        try {
            Stock stock;
            Symbols symbols = new Symbols();
            SymbolSearch companies = SymbolSearch.getSharedInstance();
            companies.init();
            companies.setTitle(Language.getString("STRATEGY_TITLE"));
            companies.setSingleMode(true);
            companies.setIndexSearchMode(false);
            companies.setSymbols(symbols);
            companies.setShowDefaultExchangesOnly(false);
            companies.showDialog(true);
            companies = null;

            String key = null;
            try {
                key = symbols.getSymbols()[0];
            } catch (Exception e) {
                return;
            }
            if (OptionStrategyMakerUI.getSharedInstance().getStock() != null) {
                DataStore.getSharedInstance().removeSymbolRequest(OptionStrategyMakerUI.getSharedInstance().getStock().getKey());
            }
            stock = DataStore.getSharedInstance().createStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), SharedMethods.getInstrumentTypeFromKey(key));

            if (OptionStrategyMakerUI.getSharedInstance().getStock() != null && !stock.getSymbol().equals(OptionStrategyMakerUI.getSharedInstance().getStock().getSymbol())) {
                clearAllData();
                /*OptionStrategyMakerUI.getSharedInstance().getDataPanel1().clearData();
                OptionStrategyMakerUI.getSharedInstance().getDataPanel2().clearData();
                OptionStrategyMakerUI.getSharedInstance().getDataPanel3().clearData();
                OptionStrategyMakerUI.getSharedInstance().getDataPanel4().clearData();
                OptionStrategyMakerUI.getSharedInstance().clearData();*/
            }
            DataStore.getSharedInstance().addSymbolRequest(stock.getKey());
            requistOptionChain(SharedMethods.getKey(stock.getKey()), null, (byte) 0);
            OptionStrategyMakerUI.getSharedInstance().setStock(DataStore.getSharedInstance().getStockObject(key));
            txtSymbol.setText(stock.getDisplaySymbol());
            symbolDescription.setText(stock.getLongDescription());
            OptionStrategyMakerUI.getSharedInstance().getDataPanel2().txtSymbol.setText(stock.getDisplaySymbol());
            OptionStrategyMakerUI.getSharedInstance().getDataPanel3().txtSymbol.setText(stock.getDisplaySymbol());
            OptionStrategyMakerUI.getSharedInstance().getDataPanel4().txtSymbol.setText(stock.getDisplaySymbol());

            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setStrickPrice(ArrayList<Double> strikePrices, boolean addToAll) {

        int index = 0;
        if (addToAll) {
            OptionStrategyMakerUI.getSharedInstance().getDataPanel1().strickPriceList.clear();
            OptionStrategyMakerUI.getSharedInstance().getDataPanel2().strickPriceList.clear();
            OptionStrategyMakerUI.getSharedInstance().getDataPanel3().strickPriceList.clear();
            OptionStrategyMakerUI.getSharedInstance().getDataPanel4().strickPriceList.clear();
            for (double strikePrice : strikePrices) {
                OptionStrategyMakerUI.getSharedInstance().getDataPanel1().strickPriceList.add(new TWComboItem("" + strikePrice, "" + strikePrice));
                OptionStrategyMakerUI.getSharedInstance().getDataPanel2().strickPriceList.add(new TWComboItem("" + strikePrice, "" + strikePrice));
                OptionStrategyMakerUI.getSharedInstance().getDataPanel3().strickPriceList.add(new TWComboItem("" + strikePrice, "" + strikePrice));
                OptionStrategyMakerUI.getSharedInstance().getDataPanel4().strickPriceList.add(new TWComboItem("" + strikePrice, "" + strikePrice));
            }
            OptionStrategyMakerUI.getSharedInstance().getDataPanel1().cmbStrikePrice.updateUI();
            OptionStrategyMakerUI.getSharedInstance().getDataPanel1().cmbStrikePrice.setSelectedIndex(index);
            OptionStrategyMakerUI.getSharedInstance().getDataPanel2().cmbStrikePrice.updateUI();
            OptionStrategyMakerUI.getSharedInstance().getDataPanel2().cmbStrikePrice.setSelectedIndex(index);
            OptionStrategyMakerUI.getSharedInstance().getDataPanel3().cmbStrikePrice.updateUI();
            OptionStrategyMakerUI.getSharedInstance().getDataPanel3().cmbStrikePrice.setSelectedIndex(index);
            OptionStrategyMakerUI.getSharedInstance().getDataPanel4().cmbStrikePrice.updateUI();
            OptionStrategyMakerUI.getSharedInstance().getDataPanel4().cmbStrikePrice.setSelectedIndex(index);
        } else {
            strickPriceList.clear();
            for (double strikePrice : strikePrices) {
                strickPriceList.add(new TWComboItem("" + strikePrice, "" + strikePrice));
            }
            cmbStrikePrice.updateUI();
            cmbStrikePrice.setSelectedIndex(index);
        }
    }

    public void setMonths(String[] months) {
//        cmbExpDate.removeItemListener(this);
        OptionStrategyMakerUI.getSharedInstance().getDataPanel1().cmbExpDate.removeItemListener(OptionStrategyMakerUI.getSharedInstance().getDataPanel1());
        OptionStrategyMakerUI.getSharedInstance().getDataPanel2().cmbExpDate.removeItemListener(OptionStrategyMakerUI.getSharedInstance().getDataPanel2());
        OptionStrategyMakerUI.getSharedInstance().getDataPanel3().cmbExpDate.removeItemListener(OptionStrategyMakerUI.getSharedInstance().getDataPanel3());
        OptionStrategyMakerUI.getSharedInstance().getDataPanel4().cmbExpDate.removeItemListener(OptionStrategyMakerUI.getSharedInstance().getDataPanel4());
//        expiryDateList.clear();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel1().expiryDateList.clear();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel2().expiryDateList.clear();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel3().expiryDateList.clear();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel4().expiryDateList.clear();
        int index = 0;
        /*for(int i=0; i < months.length; i++){
            OptionStrategyMakerUI.getSharedInstance().getDataPanel1().expiryDateList.add(new TWComboItem(months[i], months[i]));
            OptionStrategyMakerUI.getSharedInstance().getDataPanel2().expiryDateList.add(new TWComboItem(months[i], months[i]));
            OptionStrategyMakerUI.getSharedInstance().getDataPanel3().expiryDateList.add(new TWComboItem(months[i], months[i]));
            OptionStrategyMakerUI.getSharedInstance().getDataPanel4().expiryDateList.add(new TWComboItem(months[i], months[i]));
        }*/


        String[] monthNames = Language.getString("MONTH_NAMES").split(",");
        try {
            monthlist.clear();
            for (int k = 0; k < months.length; k++) {
                monthlist.add(months[k]);
            }
            Collections.sort(monthlist);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        for (int i = 0; i < monthlist.size(); i++) {
            try {
                OptionStrategyMakerUI.getSharedInstance().getDataPanel1().expiryDateList.add(new TWComboItem(monthlist.get(i), monthlist.get(i).substring(0, 4) + "  " + monthNames[Integer.parseInt(monthlist.get(i).substring(4)) - 1]));
                OptionStrategyMakerUI.getSharedInstance().getDataPanel2().expiryDateList.add(new TWComboItem(monthlist.get(i), monthlist.get(i).substring(0, 4) + "  " + monthNames[Integer.parseInt(monthlist.get(i).substring(4)) - 1]));
                OptionStrategyMakerUI.getSharedInstance().getDataPanel3().expiryDateList.add(new TWComboItem(monthlist.get(i), monthlist.get(i).substring(0, 4) + "  " + monthNames[Integer.parseInt(monthlist.get(i).substring(4)) - 1]));
                OptionStrategyMakerUI.getSharedInstance().getDataPanel4().expiryDateList.add(new TWComboItem(monthlist.get(i), monthlist.get(i).substring(0, 4) + "  " + monthNames[Integer.parseInt(monthlist.get(i).substring(4)) - 1]));

            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        monthlist.clear();
        selectedMonth = months[0];

//        cmbExpDate.updateUI();
//        cmbExpDate.setSelectedIndex(index);
//        cmbExpDate.addItemListener(this);
        OptionStrategyMakerUI.getSharedInstance().getDataPanel1().cmbExpDate.setSelectedIndex(index);
        OptionStrategyMakerUI.getSharedInstance().getDataPanel1().cmbExpDate.updateUI();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel1().cmbExpDate.addItemListener(OptionStrategyMakerUI.getSharedInstance().getDataPanel1());

        OptionStrategyMakerUI.getSharedInstance().getDataPanel2().cmbExpDate.setSelectedIndex(index);
        OptionStrategyMakerUI.getSharedInstance().getDataPanel2().cmbExpDate.updateUI();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel2().cmbExpDate.addItemListener(OptionStrategyMakerUI.getSharedInstance().getDataPanel2());

        OptionStrategyMakerUI.getSharedInstance().getDataPanel3().cmbExpDate.updateUI();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel3().cmbExpDate.setSelectedIndex(index);
        OptionStrategyMakerUI.getSharedInstance().getDataPanel3().cmbExpDate.addItemListener(OptionStrategyMakerUI.getSharedInstance().getDataPanel3());

        OptionStrategyMakerUI.getSharedInstance().getDataPanel4().cmbExpDate.updateUI();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel4().cmbExpDate.setSelectedIndex(index);
        OptionStrategyMakerUI.getSharedInstance().getDataPanel4().cmbExpDate.addItemListener(OptionStrategyMakerUI.getSharedInstance().getDataPanel4());

    }

    private void requistOptionChain(String key, String month, byte viewType) {
//        OptionStore.getSharedInstance().addOpraQuoteObject(key, month, viewType, OptionStore.CHAIN_TYPE_OPTION);
        String exCode = SharedMethods.getExchangeFromKey(key);
        Stock stock = DataStore.getSharedInstance().getStockObject(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                symbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
            }
        }
        String reqString = Meta.OPTION_REQUEST + Meta.DS + exCode + Meta.FD + symbol + Meta.FD + instrument + Meta.FD + OptionStore.CHAIN_TYPE_OPTION;
        if (month != null) {
            reqString = reqString + Meta.FD + viewType + Meta.FD + month + Meta.FD + OptionStore.REQUEST_FROM_OPTION_STRATEGY;
        } else {
            reqString = reqString + Meta.FD + "" + Meta.FD + "" + Meta.FD + OptionStore.REQUEST_FROM_OPTION_STRATEGY;
        }
        int path = ExchangeStore.getSharedInstance().getDataPath(exCode);
        OptionStore.getSharedInstance().addOptionsStrategyListener(this);
        SendQFactory.addData(path, reqString);

    }

    private boolean isValidExpirationDate(long expDate) {
        long current = OptionStrategyMakerUI.currentDayInMillis;

        Calendar cal1 = Calendar.getInstance();
        cal1.clear();
        cal1.setTimeInMillis(current);

        Calendar cal2 = Calendar.getInstance();
        cal2.clear();
        cal2.setTimeInMillis(expDate);

        cal1.set(cal1.get(Calendar.YEAR), cal1.get(Calendar.MONTH), cal1.get(Calendar.DATE), 0, 0, 0);
        cal2.set(cal2.get(Calendar.YEAR), cal2.get(Calendar.MONTH), cal2.get(Calendar.DATE), 0, 0, 0);

        if (cal2.getTimeInMillis() < cal1.getTimeInMillis()) {
            return false;
        }
        return true;
    }

    public int getErrorCount() {
        return errorCount;
    }

    public boolean validate2DData() {
        errorCount = 0;
        Component parent = OptionStrategyMakerUI.getSharedInstance();
        if (txtSymbol.getText() != null && txtSymbol.getText().length() == 0) {
            errors.add("Symbol");
//            errorCount++;
            return false;
        }

        if (((TWComboItem) cmbExpDate.getSelectedItem()).getValue() != null && ((TWComboItem) cmbExpDate.getSelectedItem()).getValue().length() == 0) {
            return false;
        }
        try {
            double strikePrice = Double.parseDouble(((TWComboItem) cmbStrikePrice.getSelectedItem()).getValue());
        } catch (Exception e) {
            return false;
        }

        try {
            double optionPrice = Double.parseDouble(txtOptionPrice.getText());
        } catch (Exception e) {
            errorCount++;
            //JOptionPane.showMessageDialog(parent, "Invalid option price", "Error", JOptionPane.ERROR_MESSAGE);
            errors.add("Option Price");
//            return false;
        }

        try {
            int quantity = Integer.parseInt(txtQty.getText());
        } catch (Exception e) {
            errorCount++;
            //JOptionPane.showMessageDialog(parent, "Invalid option price", "Error", JOptionPane.ERROR_MESSAGE);
            errors.add("Quantity");
//            return false;
        }
        if (errorCount > 0) {
            return false;
        }
        return true;
    }

    public long getDate() {

        String expmonth = ((TWComboItem) cmbExpDate.getSelectedItem()).getId();
        OptionStrategyStore store = OptionStore.getSharedInstance().getOptionStrategyStore(OptionStrategyMakerUI.getSharedInstance().getStock().getKey());
        String expdate = store.getExpirationDate(expmonth);
        String date;
        SimpleDateFormat format;
        if ((expdate != null) && (!expdate.equalsIgnoreCase("null"))) {
            date = expmonth + expdate;
            format = new SimpleDateFormat("yyyyMMdd");
        } else {
            date = expmonth;
            format = new SimpleDateFormat("yyyyMM");
        }
        try {
            Date dt = format.parse(date);
            return dt.getTime();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return 0;
    }

    public boolean validate3DData() {
        return true;
    }

    public byte getOptionType() {

        int buyIndex = cmbBuySell.getSelectedIndex();
        String callIndex = (String) cmbCallPut.getSelectedItem();

        if (buyIndex == 0 && (callIndex.equals(TYPE_CALL_EUROPEAN) || callIndex.equals(TYPE_CALL_AMERICAN))) {
            return DataManager.SERIRES_BUY_CALL;
        } else if (buyIndex == 0 && (callIndex.equals(TYPE_PUT_EUROPEAN) || callIndex.equals(TYPE_PUT_AMERICAN))) {
            return DataManager.SERIRES_BUY_PUT;
        } else if (buyIndex == 1 && (callIndex.equals(TYPE_CALL_EUROPEAN) || callIndex.equals(TYPE_CALL_AMERICAN))) {
            return DataManager.SERIRES_SELL_CALL;
        } else {
            return DataManager.SERIRES_SELL_PUT;
        }
    }

    public void clearData() {
        txtSymbol.setText("");
        strickPriceList.clear();
        cmbStrikePrice.removeAllItems();
        txtOptionPrice.setText("");
        expiryDateList.clear();
        cmbExpDate.removeAllItems();
        txtQty.setText("");
        SharedMethods.updateComponent(cmbExpDate);
        SharedMethods.updateComponent(cmbStrikePrice);
    }

    public void clearAllData() {
        OptionStrategyMakerUI.getSharedInstance().getDataPanel1().clearData();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel2().clearData();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel3().clearData();
        OptionStrategyMakerUI.getSharedInstance().getDataPanel4().clearData();
    }

    public void loadGreeks() {

    }

    public void applyTheme() {
        btnClear.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/strategy_clear.gif"));
        btnOptionPrice.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/strategy_option_price.gif"));
    }

    public void dataReceived(String key, String selectedMonth) {
        OptionStrategyStore dataStore = OptionStore.getSharedInstance().getOptionStrategyStore(key);
        if (expiryDateList.isEmpty()) {
            setMonths(dataStore.getMonths());
            setStrickPrice(dataStore.getStrikePriceList(selectedMonth), true);
        } else {
            String month = ((TWComboItem) cmbExpDate.getSelectedItem()).getId();
            if (month.equals(selectedMonth)) {
                setStrickPrice(dataStore.getStrikePriceList(selectedMonth), false);
            }
        }
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == e.SELECTED) {
            Stock stock = OptionStrategyMakerUI.getSharedInstance().getStock();
            OptionStrategyStore store = OptionStore.getSharedInstance().getOptionStrategyStore(stock.getKey());
            String selectedMonth = ((TWComboItem) e.getItem()).getId();
            ArrayList<Double> strikeList = store.getStrikePriceList(selectedMonth);
            if (strikeList == null) {
                requistOptionChain(stock.getKey(), selectedMonth, store.getSelectedType());
            } else {
                setStrickPrice(strikeList, false);
            }
        }
        cmbExpDate.setSelectedItem(e.getItem());
    }

}