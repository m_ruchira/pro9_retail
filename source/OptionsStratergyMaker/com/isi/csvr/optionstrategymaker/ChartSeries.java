package com.isi.csvr.optionstrategymaker;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jan 16, 2009
 * Time: 7:38:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChartSeries {

    public Point2D.Float[] points;
    protected Color chartColor;
    protected int count;
    protected double[] xData;
    protected double[] yData;
    protected int[] xPoints;
    protected int[] yPoints;
    private String name = "";
    private int panelId = 0;
    private byte chartType = DataManager.SERIRES_SELL_CALL;

    public ChartSeries(byte type, String name, Color chartColor, int count) {
        this.chartType = type;
        this.name = name;
        this.chartColor = chartColor;
        this.count = count;
        xData = new double[count];
        yData = new double[count];
    }

    public ChartSeries(byte type, double[] xData, double[] yData) {
        this.chartType = type;
        this.xData = xData;
        this.yData = yData;
        this.count = xData.length;
    }

    public ChartSeries(byte type, String name, Color chartColor, double[] xData, double[] yData) {
        this.chartType = type;
        this.name = name;
        this.chartColor = chartColor;
        this.xData = xData;
        this.yData = yData;
        this.count = xData.length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getChartColor() {
        return chartColor;
    }

    public void setChartColor(Color chartColor) {
        this.chartColor = chartColor;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double[] getXData() {
        return xData;
    }

    public void setXData(double[] xData) {
        this.xData = xData;
    }

    public double[] getYData() {
        return yData;
    }

    public void setYData(double[] yData) {
        this.yData = yData;
    }

    public byte getChartType() {
        return chartType;
    }

    public int getPanelId() {
        return panelId;
    }

    public void setPanelId(int panelId) {
        this.panelId = panelId;
    }
}
