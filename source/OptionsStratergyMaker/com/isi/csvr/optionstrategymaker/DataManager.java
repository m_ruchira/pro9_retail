package com.isi.csvr.optionstrategymaker;

import com.isi.csvr.shared.Constants;
import com.mubasher.csvr.optionscalculator.BinomialCalculator;
import com.mubasher.csvr.optionscalculator.BlackAndScholes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jan 19, 2009
 * Time: 8:26:51 AM
 * To change this template use File | Settings | File Templates.
 */
public class DataManager {

    protected static final long MILLIS_PER_DAY = 24 * 3600 * 1000;
    protected static final String TYPE_CALL = "call";
    protected static final String TYPE_PUT = "put";
    protected static byte SERIRES_SELL_CALL = 0;
    protected static byte SERIRES_BUY_CALL = 1;
    protected static byte SERIRES_BUY_PUT = 2;
    protected static byte SERIRES_SELL_PUT = 3;
    protected static byte SERIRES_CALC = 4;
    protected static byte SERIRES_EXP = 5;
    private static DataManager manager;
    private final int NO_OF_POINTS = 500; //this no of points is plotted along the x axis
    protected double[] combineExpDateXData;
    protected double[] combineExpDateYData;
    protected double[] combineCalcDateXData;
    protected double[] combineCalcDateYData;
    private BlackAndScholes blackCalculator;
    private BinomialCalculator coxRossCalculator;

//    protected static final int CALC_METHOD_BLACK = 0;
//    protected static final int CALC_METHOD_COX = 1;

    private DataManager() {
        blackCalculator = new BlackAndScholes();
        coxRossCalculator = new BinomialCalculator();
    }

    public static DataManager getSharedInstance() {
        if (manager == null) {
            manager = new DataManager();
        }
        return manager;
    }

    public double getYValueForCalcDate(byte optionType, double stockPrice, double strikePrice, double optionPrice,
                                       double rfRate, double volatility, long expDate, long referenceDay) {

        blackCalculator.initCalculator();
        coxRossCalculator.initCalculator();
        //double yearsFromToday = (double) (expDate - OptionStrategyMakerUI.currentDayInMillis) / (MILLIS_PER_DAY * 365);
        double yearsFromToday = (double) (expDate - referenceDay) / (MILLIS_PER_DAY * 365);

        if (optionType == SERIRES_BUY_CALL) {
            if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                return (coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10) - optionPrice);
            } else {
                return (blackCalculator.getTheoriticalPriceOnly(DataManager.TYPE_CALL, stockPrice, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice);
            }

        } else if (optionType == SERIRES_SELL_CALL) {
            if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                return (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10));
            } else {
                return (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, stockPrice, yearsFromToday, strikePrice, rfRate, volatility, 0));
            }

        } else if (optionType == SERIRES_BUY_PUT) {
            if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                return (coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10) - optionPrice);
            } else {
                return (blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, stockPrice, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice);
            }

        } else if (optionType == SERIRES_SELL_PUT) {
            if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                return (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10));
            } else {
                return (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, stockPrice, yearsFromToday, strikePrice, rfRate, volatility, 0));
            }
        }
        return 0; //doesnt matter
    }

    public double getYValueForExpDate(byte optionType, double stockPrice, double strikePrice, double optionPrice,
                                      double rfRate, double volatility, long expDate, long minExpDate) {

        blackCalculator.initCalculator();
        coxRossCalculator.initCalculator();
        double yearsFromToday = 0;
        boolean shouldUseTheoriticalPrices = false;

        if (expDate > minExpDate) {
            shouldUseTheoriticalPrices = true;
        }

        if (optionType == SERIRES_BUY_CALL) {

            if (shouldUseTheoriticalPrices) { //getting the formula from udaya's side...

                yearsFromToday = (double) (expDate - minExpDate) / (MILLIS_PER_DAY * 365);
                if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                    return (coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10) - optionPrice);
                } else {
                    return (blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, stockPrice, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice);
                }

            } else {

                if (stockPrice <= strikePrice) {
                    return -optionPrice;
                } else {
                    return (stockPrice - strikePrice - optionPrice);
                }
            }

        } else if (optionType == SERIRES_SELL_CALL) {

            if (shouldUseTheoriticalPrices) { //getting the formula from udaya's side...

                yearsFromToday = (double) (expDate - minExpDate) / (MILLIS_PER_DAY * 365);
                if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                    return (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10));
                } else {
                    return (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, stockPrice, yearsFromToday, strikePrice, rfRate, volatility, 0));
                }

            } else {

                if (stockPrice <= strikePrice) {
                    return optionPrice;
                } else {
                    return (-stockPrice + strikePrice + optionPrice);
                }
            }

        } else if (optionType == SERIRES_BUY_PUT) {

            if (shouldUseTheoriticalPrices) { //getting the formula from udaya's side...
                yearsFromToday = (double) (expDate - minExpDate) / (MILLIS_PER_DAY * 365);
                if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                    return (coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10) - optionPrice);
                } else {
                    return blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, stockPrice, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice;
                }
            } else {
                if (stockPrice <= strikePrice) {
                    return (strikePrice - stockPrice - optionPrice);
                } else {
                    return -optionPrice;
                }
            }

        } else if (optionType == SERIRES_SELL_PUT) {

            if (shouldUseTheoriticalPrices) { //getting the formula from udaya's side...
                yearsFromToday = (double) (expDate - minExpDate) / (MILLIS_PER_DAY * 365);
                if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                    return (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(stockPrice, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10));
                } else {
                    return (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, stockPrice, yearsFromToday, strikePrice, rfRate, volatility, 0));
                }

            } else {
                if (stockPrice <= strikePrice) {
                    return (stockPrice - strikePrice + optionPrice);
                } else {
                    return optionPrice;
                }
            }
        }
        return 0;
    }

    private double getIncrement(double min, double max) {

        return (max - min) / NO_OF_POINTS;
    }

    public ChartSeries getChartSeries(byte optionType, double[] prices, double strikePrice, double optionPrice,
                                      double rfRate, double volatility, int quantity, long expDate, long minimumExpDate, long referenceDay) {

        blackCalculator.initCalculator();
        coxRossCalculator.initCalculator();

        double beginPrice = prices[prices.length - 1];
        double endPrice = prices[0];
        double increment = getIncrement(beginPrice, endPrice);

        //int arrayLength = Math.round((float) ((endPrice - beginPrice) / increment));
        int arrayLength = NO_OF_POINTS;

        if (combineExpDateXData == null) {
            combineExpDateXData = new double[arrayLength];
        }
        if (combineExpDateYData == null) {
            combineExpDateYData = new double[arrayLength];
        }

        if (combineCalcDateXData == null) {
            combineCalcDateXData = new double[arrayLength];
        }
        if (combineCalcDateYData == null) {
            combineCalcDateYData = new double[arrayLength];
        }

        //TODO: VERY IMPORTANT - need to corret here if this is wrong...
        double yearsFromToday = (double) (expDate - referenceDay) / (MILLIS_PER_DAY * 365);
        //double yearsFromToday = (double) (OptionStrategyMakerUI.getSharedInstance().getMinExpDate() - OptionStrategyMakerUI.currentDayInMillis) / (MILLIS_PER_DAY * 365);
        double yearsFromMinExpDate = (double) (expDate - minimumExpDate) / (MILLIS_PER_DAY * 365);

        if (optionType == SERIRES_BUY_CALL) {

            double[] yData = new double[arrayLength];
            double[] xData = new double[arrayLength];

            int index = 0;
            boolean shouldUseTheoriticalPrices = false;
            if (expDate > minimumExpDate) {
                shouldUseTheoriticalPrices = true;
            }
            if (shouldUseTheoriticalPrices) { //getting the formula from udaya's side..

                for (double i = beginPrice; i <= endPrice; i = i + increment) {

                    if (index > arrayLength - 1) {
                        continue;
                    }
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        yData[index] = (coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromMinExpDate, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10) - optionPrice) * quantity;
                    } else {
                        yData[index] = (blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, i, yearsFromMinExpDate, strikePrice, rfRate, volatility, 0) - optionPrice) * quantity;
                    }
                    xData[index] = i;

                    combineExpDateXData[index] = i;
                    combineExpDateYData[index] += yData[index];

                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        combineCalcDateYData[index] += (coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10) - optionPrice) * quantity;
                    } else {
                        combineCalcDateYData[index] += (blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, i, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice) * quantity;
                    }
                    combineCalcDateXData[index] = i;
                    index++;
                }

            } else {  // y = mx + c format
                for (double i = beginPrice; i <= endPrice; i = i + increment) {

                    if (index > arrayLength - 1) {
                        continue;
                    }
                    if (i <= strikePrice) {
                        yData[index] = (-optionPrice) * quantity;
                    } else {
                        yData[index] = (i - strikePrice - optionPrice) * quantity;
                    }
                    xData[index] = i;

                    combineExpDateXData[index] = i;
                    combineExpDateYData[index] += yData[index];

                    combineCalcDateXData[index] = i;   //TODO: NEED TO CORRECT
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        combineCalcDateYData[index] += (coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10) - optionPrice) * quantity;
                    } else {
                        combineCalcDateYData[index] += (blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, i, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice) * quantity;
                    }

                    /*
                    if (Double.isNaN((blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, i, yearsFromToday, strikePrice, rfRate, volatility, 0)))) {
                        System.out.println("*********** combineCalcDateYData[index] ******* " + combineCalcDateYData[index]);
                        //combineCalcDateYData[index] += combineCalcDateYData[index - 1];
                    } else {
                        //combineCalcDateYData[index] += (blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, i, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice) * quantity;
                    }
                    */
                    index++;
                }
            }
            return new ChartSeries(optionType, xData, yData);

        } else if (optionType == SERIRES_SELL_CALL) {

            double[] yData = new double[arrayLength];
            double[] xData = new double[arrayLength];

            int index = 0;
            boolean shouldUseTheoriticalPrices = false;
            if (expDate > minimumExpDate) {
                shouldUseTheoriticalPrices = true;
            }
            if (shouldUseTheoriticalPrices) {//getting the formula from udaya's side...

                for (double i = beginPrice; i <= endPrice; i = i + increment) {

                    if (index > arrayLength - 1) {
                        continue;
                    }
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        yData[index] = (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromMinExpDate, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10)) * quantity;
                    } else {
                        yData[index] = (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, i, yearsFromMinExpDate, strikePrice, rfRate, volatility, 0)) * quantity;
                    }
                    xData[index] = i;

                    combineExpDateXData[index] = i;
                    combineExpDateYData[index] += yData[index];

                    //TODO: CORRECT HERE
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        combineCalcDateYData[index] += (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10)) * quantity;
                    } else {
                        combineCalcDateYData[index] += (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, i, yearsFromToday, strikePrice, rfRate, volatility, 0)) * quantity;
                    }
                    combineCalcDateXData[index] = i;
                    index++;
                }

            } else { // y = mx + c format
                for (double i = beginPrice; i <= endPrice; i = i + increment) {

                    if (index > arrayLength - 1) {
                        continue;
                    }
                    if (i <= strikePrice) {
                        yData[index] = (optionPrice) * quantity;
                    } else {
                        yData[index] = (-i + strikePrice + optionPrice) * quantity;
                    }
                    xData[index] = i;
                    combineExpDateXData[index] = i;
                    combineExpDateYData[index] += yData[index];

                    combineCalcDateXData[index] = i;
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        combineCalcDateYData[index] += (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Call, BinomialCalculator.EnumStyle.European, 10)) * quantity;
                    } else {
                        combineCalcDateYData[index] += (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_CALL, i, yearsFromToday, strikePrice, rfRate, volatility, 0)) * quantity;
                    }
                    index++;
                }
            }
            return new ChartSeries(optionType, xData, yData);
        } else if (optionType == SERIRES_BUY_PUT) {

            double[] yData = new double[arrayLength];
            double[] xData = new double[arrayLength];

            int index = 0;

            boolean shouldUseTheoriticalPrices = false;
            if (expDate > minimumExpDate) {
                shouldUseTheoriticalPrices = true;
            }
            if (shouldUseTheoriticalPrices) {//getting the formula from udaya's side...

                for (double i = beginPrice; i <= endPrice; i = i + increment) {

                    if (index > arrayLength - 1) {
                        continue;
                    }
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        yData[index] = (coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromMinExpDate, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10) - optionPrice) * quantity;
                    } else {
                        yData[index] = (blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, i, yearsFromMinExpDate, strikePrice, rfRate, volatility, 0) - optionPrice) * quantity;
                    }
                    xData[index] = i;
                    combineExpDateXData[index] = i;
                    combineExpDateYData[index] += yData[index];
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        combineCalcDateYData[index] += (coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10) - optionPrice) * quantity;
                    } else {
                        combineCalcDateYData[index] += (blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, i, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice) * quantity;
                    }

                    combineCalcDateXData[index] = i;
                    index++;
                }
            } else {      //y = mx + c format
                for (double i = beginPrice; i <= endPrice; i = i + increment) {

                    if (index > arrayLength - 1) {
                        continue;
                    }
                    if (i <= strikePrice) {
                        yData[index] = (strikePrice - i - optionPrice) * quantity;
                    } else {
                        yData[index] = (-optionPrice) * quantity;
                    }
                    xData[index] = i;
                    combineExpDateXData[index] = i;
                    combineExpDateYData[index] += yData[index];

                    combineCalcDateXData[index] = i;
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        combineCalcDateYData[index] += (coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10) - optionPrice) * quantity;
                    } else {
                        combineCalcDateYData[index] += (blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, i, yearsFromToday, strikePrice, rfRate, volatility, 0) - optionPrice) * quantity;
                    }
                    index++;
                }
            }
            return new ChartSeries(optionType, xData, yData);
        } else if (optionType == SERIRES_SELL_PUT) {

            double[] yData = new double[arrayLength];
            double[] xData = new double[arrayLength];

            int index = 0;

            boolean shouldUseTheoriticalPrices = false;
            if (expDate > minimumExpDate) {
                shouldUseTheoriticalPrices = true;
            }
            if (shouldUseTheoriticalPrices) {//getting the formula from udaya's side...

                for (double i = beginPrice; i <= endPrice; i = i + increment) {

                    if (index > arrayLength - 1) {
                        continue;
                    }
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        yData[index] = (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromMinExpDate, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10)) * quantity;
                    } else {
                        yData[index] = (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, i, yearsFromMinExpDate, strikePrice, rfRate, volatility, 0)) * quantity;
                    }
                    xData[index] = i;
                    combineExpDateXData[index] = i;
                    combineExpDateYData[index] += yData[index];
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        combineCalcDateYData[index] += (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10)) * quantity;
                    } else {
                        combineCalcDateYData[index] += (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, i, yearsFromToday, strikePrice, rfRate, volatility, 0)) * quantity;
                    }
                    combineCalcDateXData[index] = i;
                    index++;
                }
            } else {
                for (double i = beginPrice; i <= endPrice; i = i + increment) {

                    if (index > arrayLength - 1) {
                        continue;
                    }
                    if (i <= strikePrice) {
                        yData[index] = (i - strikePrice + optionPrice) * quantity;
                    } else {
                        yData[index] = (optionPrice) * quantity;
                    }
                    xData[index] = i;
                    combineExpDateXData[index] = i;
                    combineExpDateYData[index] += yData[index];

                    combineCalcDateXData[index] = i;
                    if (OptionStrategyMakerUI.getSharedInstance().getCalcMethod() == Constants.COX_ROSS) {
                        combineCalcDateYData[index] += (optionPrice - coxRossCalculator.getTheoreticalPriceOnly(i, strikePrice, yearsFromToday, (volatility / 100), (rfRate / 100), 0, BinomialCalculator.EnumCallPut.Put, BinomialCalculator.EnumStyle.European, 10)) * quantity;
                    } else {
                        combineCalcDateYData[index] += (optionPrice - blackCalculator.getTheoriticalPriceOnly(TYPE_PUT, i, yearsFromToday, strikePrice, rfRate, volatility, 0)) * quantity;
                    }
                    index++;
                }
            }
            return new ChartSeries(optionType, xData, yData);
        }

        return null;//never executes
    }

    //TODO: if required should use this method
    public double getYearDifference(long minTime, long maxTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date minDate = new Date(minTime);
        Date maxDate = new Date(maxTime);

        Calendar cal = Calendar.getInstance();

        return 0;
    }

}
