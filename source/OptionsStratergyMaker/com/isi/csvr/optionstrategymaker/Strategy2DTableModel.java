package com.isi.csvr.optionstrategymaker;

import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jan 15, 2009
 * Time: 8:45:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class Strategy2DTableModel extends CommonTable implements TableModel, CommonTableInterface {

    ArrayList<Strategy2DRow> objectStore;
    DoubleTransferObject doubleObj = new DoubleTransferObject();


    public Strategy2DTableModel(ArrayList<Strategy2DRow> store) {
        this.objectStore = store;
    }

    public int getRowCount() {
        return objectStore.size();
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }


    public Object getValueAt(int rowIndex, int columnIndex) {

        if (objectStore.size() == 0) {
            return "";
        }

        Strategy2DRow rowObject = objectStore.get(rowIndex);

        if (rowObject != null) {
            switch (columnIndex) {
                case 0:
                    return new String(rowObject.getIncrement() + "%");
                case 1:
                    //return  doubleObj.setValue(rowObject.getStockPrice());
                    return rowObject.getStockPrice();
                case 2:
                    return rowObject.getCalcDate();
                case 3:
                    return rowObject.getExpDate();
            }
        }

        return null;
    }

    public Class getColumnClass(int col) {
        if (col == 0) {
            return String.class;
        } else {
            return Float.class;
        }
    }

    public String getColumnName(int columnIndex) {
        return getViewSettings().getColumnHeadings()[columnIndex];
    }

    public void setSymbol(String symbol) {

    }
}
