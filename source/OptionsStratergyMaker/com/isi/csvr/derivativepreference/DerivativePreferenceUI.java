package com.isi.csvr.derivativepreference;

import com.isi.csvr.Client;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: hasika
 * Date: May 27, 2009
 * Time: 1:12:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class DerivativePreferenceUI extends JDialog implements ActionListener, Themeable {

    private static DerivativePreferenceUI self;

    private int height = 160;
    private int width = 350;

    private TWTextField txtReskFreeInt;

    private JComboBox cmbIndVal;
    private JComboBox cmbStkVal;

    private ArrayList<TWComboItem> indValList;
    private TWComboModel indValModel;

    private ArrayList<TWComboItem> stkValList;
    private TWComboModel stkValModel;

    private TWButton btnOk;
    private TWButton btnReset;
    private TWButton btnClose;


    private DerivativePreferenceUI() {
        super(JOptionPane.getFrameForComponent(Client.getInstance().getDesktop()), true);
        createUI();
    }


    public static DerivativePreferenceUI getSharedInstance() {
        if (self == null) {
            self = new DerivativePreferenceUI();
        }
        return self;
    }

    private void createUI() {
        JPanel mainPan = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"80", "30"}, 5, 5, true, true));
        mainPan.add(createFirstPanel());
        mainPan.add(createSecPanel());

        this.add(mainPan);
        this.setSize(new Dimension(width, height));
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setResizable(false);
        this.setTitle(Language.getString("DERIVATIVE_PREFERENCES"));
        setLocationRelativeTo(Client.getInstance().getDesktop());
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        applyTheme();

    }

    private JPanel createSecPanel() {
        JPanel pan = new JPanel(new FlexGridLayout(new String[]{"100%", "80", "130", "80"}, new String[]{"20"}, 5, 5));

        btnOk = new TWButton(Language.getString("OK"));
        btnReset = new TWButton(Language.getString("RESET"));
        btnClose = new TWButton(Language.getString("BTN_CLOSE"));

        btnOk.addActionListener(this);
        btnReset.addActionListener(this);
        btnClose.addActionListener(this);

        pan.add(new JLabel());
        pan.add(btnOk);
        pan.add(btnReset);
        pan.add(btnClose);

        return pan;
    }

    public void setVisible(boolean b) {
        if (b) {
            setValues();
        }
        super.setVisible(b);    //To change body of overridden methods use File | Settings | File Templates.
    }

    private JPanel createFirstPanel() {
        JPanel pan = new JPanel(new FlexGridLayout(new String[]{"40%", "60%"}, new String[]{"20", "20", "20"}, 5, 5));
        JPanel fpan = new JPanel(new FlexGridLayout(new String[]{"90%", "10%"}, new String[]{"20"}));
        txtReskFreeInt = new TWTextField();
        Document textDocOne = new ValueFormatter(ValueFormatter.DECIMAL, 6, 2);
        txtReskFreeInt.setDocument(textDocOne);

        indValList = new ArrayList<TWComboItem>();
        indValModel = new TWComboModel(indValList);

        stkValList = new ArrayList<TWComboItem>();
        stkValModel = new TWComboModel(stkValList);

        cmbIndVal = new JComboBox(indValModel);
        cmbStkVal = new JComboBox(stkValModel);

        fpan.add(txtReskFreeInt);
        fpan.add(new JLabel(" %"));

        pan.add(new JLabel(Language.getString("RESK_FREE_INTEREST")));
        pan.add(fpan);
        pan.add(new JLabel(Language.getString("INDEX_VALUATION")));
        pan.add(cmbIndVal);
        pan.add(new JLabel(Language.getString("STOCKS_VALUATION")));
        pan.add(cmbStkVal);

        populateIndexVal();
        populateStocksVal();

        if (Settings.getReskFreeInt() == null || Settings.getReskFreeInt().equals("")) {
            txtReskFreeInt.setText("7.00");
            Settings.setReskFreeInt(txtReskFreeInt.getText(), Constants.COX_ROSS + "", Constants.BLACK_SCHOLES + "");
        } else {
            double val = Double.parseDouble(Settings.getReskFreeInt());
            if (val < 0) {
                txtReskFreeInt.setText("7.00");
                Settings.setReskFreeInt(txtReskFreeInt.getText(), ((TWComboItem) cmbIndVal.getSelectedItem()).getValue(), ((TWComboItem) cmbStkVal.getSelectedItem()).getValue());
            } else {
                txtReskFreeInt.setText(Settings.getReskFreeInt());
                if (Settings.getIndexValuation() != null) {
                    cmbIndVal.setSelectedIndex((Settings.getIndexValuation().equals(Constants.BLACK_SCHOLES + "")) ? 0 : 1);
                }
                if (Settings.getStocksValuation() != null) {
                    cmbStkVal.setSelectedIndex((Settings.getStocksValuation().equals(Constants.BLACK_SCHOLES + "")) ? 0 : 1);
                }

            }
        }

        return pan;
    }

    private void setValues() {
        if (Settings.getReskFreeInt() == null || Settings.getReskFreeInt().equals("")) {
            txtReskFreeInt.setText("7.00");
        } else {
            double val = Double.parseDouble(Settings.getReskFreeInt());
            if (val < 0) {
                txtReskFreeInt.setText("7.00");
            } else {
                txtReskFreeInt.setText(Settings.getReskFreeInt());
            }
        }
        if (Settings.getIndexValuation() != null) {
            cmbIndVal.setSelectedIndex((Settings.getIndexValuation().equals(Constants.BLACK_SCHOLES + "")) ? 0 : 1);
        }
        if (Settings.getStocksValuation() != null) {
            cmbStkVal.setSelectedIndex((Settings.getStocksValuation().equals(Constants.BLACK_SCHOLES + "")) ? 0 : 1);
        }
    }

    private void populateStocksVal() {
        stkValList.clear();
        stkValList.add(new TWComboItem(0, Language.getString("BLACK_SCHOLES")));
        stkValList.add(new TWComboItem(1, Language.getString("COX_ROSS")));
        cmbStkVal.setSelectedIndex(0);

    }

    private void populateIndexVal() {
        indValList.clear();
        indValList.add(new TWComboItem(0, Language.getString("BLACK_SCHOLES")));
        indValList.add(new TWComboItem(1, Language.getString("COX_ROSS")));
        cmbIndVal.setSelectedIndex(0);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnOk) {
            try {
                double val = Double.parseDouble(txtReskFreeInt.getText());
                if (val < 0) {
                    SharedMethods.showMessage(Language.getString("INVALID_PERCENTAGE_VALUE"), JOptionPane.ERROR_MESSAGE);
                } else {
                    Settings.setReskFreeInt(txtReskFreeInt.getText(), ((TWComboItem) cmbIndVal.getSelectedItem()).getId(), ((TWComboItem) cmbStkVal.getSelectedItem()).getId());
                    this.setVisible(false);
                }
            } catch (Exception e1) {
                SharedMethods.showMessage(Language.getString("INVALID_PERCENTAGE_VALUE"), JOptionPane.ERROR_MESSAGE);
                return;
            }
        } else if (e.getSource() == btnReset) {
            txtReskFreeInt.setText("7.00");
            cmbIndVal.setSelectedIndex(1);
            cmbStkVal.setSelectedIndex(0);
        } else if (e.getSource() == btnClose) {
            this.setVisible(false);
        }
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }

}
