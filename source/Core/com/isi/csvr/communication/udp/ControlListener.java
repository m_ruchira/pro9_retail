package com.isi.csvr.communication.udp;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.trading.security.TWSecurityManager;
import com.isi.util.TypeConvert;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 19, 2005
 * Time: 8:12:34 AM
 */
public class ControlListener implements Runnable {

    private MulticastSocket socket;

    public void run() {

        try {
            // Create the socket and bind it to port 'port'.
            socket = new MulticastSocket(new InetSocketAddress(Settings.SATELLITE_CARD_IP,
                    Settings.SATELLITE_CONTROL_PORT));
            socket.setReceiveBufferSize(100000);

            // join the multicast group
            socket.joinGroup(InetAddress.getByName(Settings.SATELLITE_DATA_IP));
            // Now the socket is set up and we are ready to receive packets

            // Create a DatagramPacket and do a receive
            System.out.println("Control listener Ready...");
            while (true) {
                try {
                    if (SatelliteSecurityManager.USB_OK) {
                        byte[] buf = new byte[1024];
                        DatagramPacket pack = new DatagramPacket(buf, buf.length);
                        socket.receive(pack);
                        if (pack == null) continue;

                        analyseControlData(pack.getData(), pack.getLength());

                        buf = null;
                        pack = null;
                    } else {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void analyseControlData(byte[] data, int len) {
        //check if it is for me or for everyone
        int userID = TypeConvert.byteArrayToInt(data, SatelliteConstants.CTRL_POSITION_USER_ID,
                SatelliteConstants.CTRL_LENGTH_USER_ID);
        if ((userID == 0) || (userID == Integer.parseInt(Settings.getUserID()))) {
            int messageType = data[SatelliteConstants.CTRL_POSITION_MESG_TYPE];
            int messageLength = TypeConvert.byteArrayToInt(data, SatelliteConstants.CTRL_POSITION_MESG_LENGTH,
                    SatelliteConstants.CTRL_LENGTH_MESG_LENGTH);
            int encryptionType = data[SatelliteConstants.CTRL_POSITION_ENCRYPTION_TYPE];

            switch (messageType) {
                case SatelliteConstants.CONTROL_TYPE_FORCE_DISCONNECTION:
                    UDPConnector.getSharedInstance().resetConnection();
                    break;
                case SatelliteConstants.CONTROL_TYPE_USER_MESSAGES:
                    if (encryptionType == SatelliteConstants.ENCRYPTION_NONE) {
                        showMessage(UnicodeUtils.getNativeString(new String(data, SatelliteConstants.CTRL_POSITION_MESG_BODY, messageLength)));
                    } else {
                        try {
                            byte[] message = TWSecurityManager.decryptData(data, SatelliteConstants.CTRL_POSITION_MESG_BODY, messageLength);
                            showMessage(UnicodeUtils.getNativeString(new String(message, 0, message.length)));
                            message = null;
                        } catch (BadPaddingException e) {
                            e.printStackTrace();
                            UDPConnector.getSharedInstance().resetConnection();
                        } catch (IllegalBlockSizeException e) {
                            e.printStackTrace();
                            UDPConnector.getSharedInstance().resetConnection();
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e2) {
                            }
                        }
                    }
                    break;
            }
        }
    }

    private void showMessage(String message) {
        new ShowMessage(true, message, "I");
    }
}
