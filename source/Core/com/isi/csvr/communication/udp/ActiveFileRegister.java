package com.isi.csvr.communication.udp;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 15, 2005
 * Time: 9:04:59 AM
 */
public class ActiveFileRegister {
    public String fileName;
    public String md5;
    public byte fileType;
    public long totalLength;
    public long receivedLength;
    public int version;
    public long fileID;
    public boolean listenMode;


    public String toString() {
        return "ActiveFileRegister{" +
                "fileName='" + fileName + "'" +
                ", md5='" + md5 + "'" +
                ", totalLength=" + totalLength +
                ", fileID=" + fileID +
                ", version=" + version +
                ", fleType=" + fileType +
                "}";
    }
}
