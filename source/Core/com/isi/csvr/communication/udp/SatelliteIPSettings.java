package com.isi.csvr.communication.udp;

import com.isi.csvr.shared.Settings;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 18, 2005
 * Time: 5:45:38 PM
 */
public class SatelliteIPSettings {

    public synchronized static void load() {
        DataInputStream fileIn = null;

        try {

            File ipFile = new File(Settings.SYSTEM_PATH + "/satip.dll");
            if (ipFile.exists()) {
                try {
                    fileIn = new DataInputStream(new FileInputStream(ipFile));
                    Settings.SATELLITE_CARD_IP = fileIn.readLine();
                    Settings.SATELLITE_DATA_IP = fileIn.readLine();
                    fileIn.close();
                    fileIn = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void save() {
        FileOutputStream fileOut = null;

        try {
            fileOut = new FileOutputStream(Settings.SYSTEM_PATH + "/satip.dll");
            if (Settings.SATELLITE_CARD_IP != null) {
                fileOut.write(Settings.SATELLITE_CARD_IP.getBytes());
                fileOut.write("\n".getBytes());
            } else {
                fileOut.write("\n".getBytes());
            }
            if (Settings.SATELLITE_DATA_IP != null) {
                fileOut.write(Settings.SATELLITE_DATA_IP.getBytes());
                fileOut.write("\n".getBytes());
            } else {
                fileOut.write("\n".getBytes());
            }
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fileOut = null;
    }
}
