package com.isi.csvr.communication.udp;

import com.isi.csvr.Client;
import com.isi.csvr.Login;
import com.isi.csvr.LoginWindow;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 11, 2004
 * Time: 1:19:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class Authenticator {
    public static boolean USER_VALID = false;
    public static Authenticator self = null;
    private String user;
    private String pass;

    private Authenticator() {
        self = this;
    }

    public static synchronized Authenticator getSharedInstance() {
        if (self == null) {
            self = new Authenticator();
        }
        return self;
    }

    public boolean getUserInfo() {
        //if(!isPasswordSaved()){
//        Login loginDialog = new Login(Client.getInstance().getFrame(), Login.MODE_PRICE_SERVERS);
        LoginWindow loginDialog = new LoginWindow(Client.getInstance().getFrame(), Login.MODE_PRICE_SERVERS);
        while (true) {
            if (loginDialog.showDialog()) {
                user = loginDialog.getUserName().trim();
                pass = loginDialog.getPassword().trim();
                Settings.setUserDetails(user, pass);
                loginDialog.dispose();
                loginDialog = null;
                return true;
            } else {
//                Client.getInstance().setTitleText();//Language.getString("STATUS_NOT_CONNECTED"));
                Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                loginDialog = null;
                return false; // login cancelled by user
            }
        }
        //}
        //return true;
    }

    public boolean isPasswordSaved() {
        pass = Settings.getUserPassword();

        if ((pass == null) || (pass.trim().equals(Constants.NULL_STRING))) {
            pass = null;
            return false;
        } else {
            // if the password is saved take the username from the file too
            user = Settings.getUserName();
            return true;
        }
    }

    public void createUser(String userData) {
        Object[] options = {
                Language.getString("OK")};

        boolean blocking = false;
        String message = null;
        String exchanges = null;
        String exchangeParams = null;
        String windowTypes = null;
        String expiryDate = null;
        String expiryDays = null;
        boolean authenticated = false;

//        USER_VALID = true;
        String[] fields = userData.split(Meta.FS);

        for (int i = 0; i < fields.length; i++) {
            String[] data = fields[i].split(Meta.DS);
            try {
                switch (Integer.parseInt(data[Meta.TAG])) {
                    case Meta.AUTH_RESULT:
                        USER_VALID = (SharedMethods.intValue(data[Meta.VALUE]) == Meta.YES);
                        break;
                    case Meta.AUTH_BLOCKING:
                        blocking = (SharedMethods.intValue(data[Meta.VALUE]) == Meta.YES);
                        break;
                    case Meta.WINDOW_TYPES:
                        windowTypes = data[Meta.VALUE];
                        System.out.println("Window Types " + windowTypes);
                        ExchangeStore.getSharedInstance().addWindowTypes(windowTypes);
                        break;
                    case Meta.AUTH_MESSAGE:
                        message = UnicodeUtils.getNativeString(Language.getLanguageSpecificString(data[Meta.VALUE]));
                        break;
                    case Meta.USERID:
                        Settings.setUserID(data[Meta.VALUE]);
                        break;
                    case Meta.EXCHANGE_PARAMETERS:
                        if (USER_VALID) {
                            exchangeParams = data[Meta.VALUE];
                            System.out.println("Exchange Params " + exchangeParams);
                            activateFileListeners(exchangeParams);
                        }
                        break;
                    case Meta.EXCHANGE_LIST:
                        if (USER_VALID) {
                            exchanges = data[Meta.VALUE];
                            System.out.println("Subscribed Exchanges " + exchanges);
                            ExchangeStore.getSharedInstance().addExchangeList(exchanges, exchangeParams);
                        }
                        break;
                        /*case Meta.TW_VERSION:
                        SharedMethods.printLine("=========>Version " + data[Meta.VALUE] + "<====");
                        TWUpdateDownloader.setServerTWVersionData(data[Meta.VALUE]);
                        break;*/
                    case Meta.DAYS_TO_EXPIRE:
                        expiryDays = data[Meta.VALUE];
                        break;
                    case Meta.EXPIRY_DATE:
                        expiryDate = data[Meta.VALUE];
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            data = null;
        }

        showExpiryMessages(expiryDate, expiryDays);

        if (blocking) {
            Settings.setUserDetails(user, null);
            JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    message,
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
        } else {
            if ((message != null) && (!message.equals("null"))) {
                Client.setDownloadStatus(message);
            }
        }
    }

    /*public boolean validateUSer(String userData){
        String authData[];
        try {
            authData = userData.split(",");
            System.out.println(authData[0] + " " + authData[1] + " " + authData[2]);
            System.out.println(user + " " + pass);
            if ((authData[0].equals(user)) && (authData[1].equals(pass))){
                USER_VALID = true;
                System.out.println("Authenticated ============");
                Settings.setAllowedWindowTypes(authData[2]);
                Client.getInstance().getMenus().checkMenuAuthentication();
                Client.getInstance().validatePopupMenuItems();
                Client.getInstance().closeInvalidFrames();
                Client.getInstance().setRecevingSnapshots();
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }finally{
            authData = null;
        }

    }*/

    private void activateFileListeners(String exchangeParams) {
        try {
            int filePort;
            String exchange;

            String[] parameters = exchangeParams.split("\\|");
            for (int i = 0; i < parameters.length; i++) {
                exchange = parameters[i].split(",")[0];
                filePort = Integer.parseInt(parameters[i].split(",")[3]);
                UDPConnector.getSharedInstance().activateFileListener(exchange, filePort);
                exchange = null;
            }
            parameters = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showExpiryMessages(String date, String days) {
        try {
            boolean critical = false;

            String message = "";

            try {
                if (Integer.parseInt(days) < 7) {
                    critical = true;
                }
            } catch (NumberFormatException e) {
                critical = false;
            }

            if ((date != null) && (!date.equalsIgnoreCase("null"))) {
                date = date.substring(6) + " / " + date.substring(4, 6) + " / " + date.substring(0, 4);
                message = Language.getString("STATUS_ACCOUNT_EXPIRE");
                if (Settings.isShowArabicNumbers()) {
                    message = message.replaceAll("\\[DAY\\]", UnicodeUtils.getArabicNumbers(date));
                } else {
                    message = message.replaceAll("\\[DAY\\]", date);
                }
            }
            Client.setExpiryStatus(message, critical);
            message = null;
        } catch (Exception e) {
        }
    }
}
