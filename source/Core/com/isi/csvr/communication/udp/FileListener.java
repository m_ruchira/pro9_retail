package com.isi.csvr.communication.udp;

import com.isi.csvr.SymbolMaster;
import com.isi.csvr.announcement.AnnouncementDownloadManager;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.downloader.*;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trade.DetailedTradeStore;
import com.isi.csvr.tradebacklog.TradeBacklogStore;
import com.isi.csvr.trading.security.TWSecurityManager;
import com.isi.util.TypeConvert;

import java.io.File;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 13, 2004
 * Time: 2:20:52 PM
 */
public class FileListener implements Runnable {
    private static final String FILE_LISTENER_LOCK = "FILE_LISTENER_LOCK";

    public static boolean USER_VALID = false;
    private static Hashtable<String, Float> inprogressStockMasters;
    private int port;
    private String exchange;
    private RandomAccessFile dumpFile;
    private File file;
    private byte frameType;
    private ActiveFileRegister fileRegister;
    private boolean active;
    private boolean twUpdateDownloaded = false;
    private MulticastSocket socket;

    public FileListener(int port, String exchange) {
        this.port = port;
        this.exchange = exchange;
        fileRegister = new ActiveFileRegister();

        synchronized (FILE_LISTENER_LOCK) {
            if (inprogressStockMasters == null)
                inprogressStockMasters = new Hashtable<String, Float>();
        }
    }

    public static void clearInProgressStockMaster() {
        inprogressStockMasters.clear();
    }

    public static void updateInProgressStockMaster(String exchange, float version) {
        SharedMethods.printLine("FileListener:updateInProgressStockMaster ====>" + exchange + " " + version, false);
        inprogressStockMasters.put(exchange, version);
    }

    public static float getInProgressStockMasterVersion(String exchange) {
        try {
            return inprogressStockMasters.get(exchange);
        } catch (Exception e) {
            return 0;
        }
    }

    public void run() {

        try {
            System.err.println("File Listener Started " + exchange + " " + port);
            // Create the socket and bind it to port 'port'.
            socket = new MulticastSocket(new InetSocketAddress(Settings.SATELLITE_CARD_IP, port));
            socket.setReceiveBufferSize(100000);

            // join the multicast group
            socket.joinGroup(InetAddress.getByName(Settings.SATELLITE_DATA_IP));
            // Now the socket is set up and we are ready to receive packets

            // Create a DatagramPacket and do a receive
            active = true;
            byte[] buf = new byte[1024];
            DatagramPacket pack = new DatagramPacket(buf, buf.length);
            while (active) {
                try {
                    socket.receive(pack);
                    if (pack == null) continue;
                    processPacket(buf, pack.getLength());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processPacket(byte[] data, int length) {
        try {
            if (!Authenticator.USER_VALID) return;

            //check if the frame is encrypted
            boolean encrypted = (data[SatelliteConstants.FILE_POSITION_FRAME_ENTRYPTED] == SatelliteConstants.ENCRYPTION_ON);

            if (encrypted) {
                data = TWSecurityManager.decryptData(data, 1, length - 1);
            }

            // check if the data belongs to a header
            frameType = data[SatelliteConstants.FILE_POSITION_FRAME_TYPE];
            if (frameType == SatelliteConstants.FRAME_HEADER) { // fond a header frame?
                fileRegister.listenMode = false;
                fileRegister.fileName = new String(data, SatelliteConstants.FILE_POSITION_FILE_NAME, SatelliteConstants.LENGTH_FILE_NAME);
                fileRegister.fileName = fileRegister.fileName.trim();
                fileRegister.md5 = new String(data, SatelliteConstants.FILE_POSITION_MD5_HEADER, SatelliteConstants.LENGTH_MD5);
                fileRegister.fileType = data[SatelliteConstants.FILE_POSITION_FILE_TYPE];
                fileRegister.totalLength = TypeConvert.byteArrayToLong(data, SatelliteConstants.FILE_POSITION_FILE_SIZE, SatelliteConstants.LENGTH_FILE_SIZE);
                fileRegister.receivedLength = 0;
                fileRegister.version = TypeConvert.byteArrayToInt(data, SatelliteConstants.FILE_POSITION_VERSION_HEADER, SatelliteConstants.LENGTH_VERSION);
                fileRegister.fileID = TypeConvert.byteArrayToLong(data, SatelliteConstants.FILE_POSITION_FILE_ID_HEADER, SatelliteConstants.LENGTH_FILE_ID);
                //System.err.println(fileRegister.toString());
                if (isNeedToDownload()) { // do i want this file to be saved. May be it is already saved?
                    allocateFileSize();
                    fileRegister.listenMode = true;
                    //System.err.println("Downloading     " + exchange + " " + fileRegister.fileName);
                } else {
                    //System.err.println("Not downloading " + exchange + " " + fileRegister.fileName);
                }
            } else if (frameType == SatelliteConstants.FRAME_BODY) { // fond a body frame?
                //System.err.println("body");
                if (fileRegister.listenMode) { // waiting for file data?
                    long fileID = TypeConvert.byteArrayToLong(data, SatelliteConstants.FILE_POSITION_FILE_ID_BODY, SatelliteConstants.LENGTH_FILE_ID);
                    //System.err.print("\r" + fileID + " " + fileRegister.fileID);
                    if (fileID == fileRegister.fileID) { // is correct file id
                        long dataPosition = TypeConvert.byteArrayToLong(data, SatelliteConstants.FILE_POSITION_SEQUENCE, SatelliteConstants.LENGTH_SEQUENCE);
                        int dataLength = TypeConvert.byteArrayToInt(data, SatelliteConstants.FILE_POSITION_DATA_LENGTH, SatelliteConstants.LENGTH_DATA);
                        writeToDump(dataPosition, dataLength, data); // write the packet to the file
                        fileRegister.receivedLength += dataLength;
                        if (fileRegister.receivedLength == fileRegister.totalLength) { // is the file complete?
                            if (isMD5OK()) { // all bits valid?
                                //System.err.println("File download success");
                                processFile();
                            } else {
                                System.err.println("File download failed");
                            }
                            file.delete();
                            closeDumpFile();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isNeedToDownload() {
        switch (fileRegister.fileType) {
            case SatelliteConstants.FILE_TYPE_ARCHIVE:
            case SatelliteConstants.FILE_TYPE_HISTORY:
                if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, fileRegister.fileName, fileRegister.totalLength)) {
                    return true;
                }
                break;
            case SatelliteConstants.FILE_TYPE_OHLC_HISTORY:
                if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, fileRegister.fileName, fileRegister.totalLength)) {
                    return true;
                }
                break;
            case SatelliteConstants.FILE_TYPE_AUTO_UPDATE:
                if ((!twUpdateDownloaded) && (Settings.getTwVersionInInt() != fileRegister.version)) {
                    return true;
                } else {
                    return false;
                }
            case SatelliteConstants.FILE_TYPE_PROPERTY:
                if (Settings.getPropertyVersion() < fileRegister.version) {
                    return true;
                } else {
                    return false;
                }
            case SatelliteConstants.FILE_TYPE_STOCK_MASTER:
                if (SymbolMaster.getVersion(exchange) < fileRegister.version) {
                    return true;
                } else {
                    return false;
                }
            case SatelliteConstants.FILE_TYPE_ANNOUNCEMENTS:
                if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, fileRegister.fileName, fileRegister.totalLength)) {
                    return true;
                }
            case SatelliteConstants.FILE_TYPE_TRADE_SUMMARY:
                if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, fileRegister.fileName, fileRegister.totalLength)) {
                    return true;
                }
            case SatelliteConstants.FILE_TYPE_DAILY_TRADE:
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TimeAndSalesHistory)) {
                    if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, fileRegister.fileName, fileRegister.totalLength)) {
                        return true;
                    }
                }
        }
        return false;
    }

    private void processFile() throws Exception {
        switch (fileRegister.fileType) {
            case SatelliteConstants.FILE_TYPE_ARCHIVE:
            case SatelliteConstants.FILE_TYPE_HISTORY:
                processArchiveFile();
                break;
            case SatelliteConstants.FILE_TYPE_OHLC_HISTORY:
                processOHLCFile();
                break;
            case SatelliteConstants.FILE_TYPE_AUTO_UPDATE:
                processAutoUpdate();
                break;
            case SatelliteConstants.FILE_TYPE_PROPERTY:
                processPropertyFile();
                break;
            case SatelliteConstants.FILE_TYPE_STOCK_MASTER:
                processStockMasterFile();
                break;
            case SatelliteConstants.FILE_TYPE_ANNOUNCEMENTS:
                processAnnouncements();
                break;
            case SatelliteConstants.FILE_TYPE_TRADE_SUMMARY:
                processTradeSummary();
                break;
            case SatelliteConstants.FILE_TYPE_DAILY_TRADE:
                processDailyTrades();
                break;
        }
    }

    private boolean isMD5OK() {
        try {
            String md5 = SharedMethods.getMD5File(file);
            if ((md5 != null) && (fileRegister.md5 != null) && (md5.equalsIgnoreCase(fileRegister.md5))) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void allocateFileSize() throws Exception {
        closeDumpFile();
        file = new File(Settings.getAbsolutepath() + "temp/downloads/" + exchange + "-" + fileRegister.fileName);
        dumpFile = new RandomAccessFile(file, "rwd");
        dumpFile.setLength(fileRegister.totalLength); // allocate size
    }

    private void writeToDump(long dataPosition, int dataLength, byte[] data) throws Exception {
        if (dumpFile != null) {
            dumpFile.seek(dataPosition);
            dumpFile.write(data, SatelliteConstants.FILE_POSITION_DATA, dataLength);
        }
    }

    private void closeDumpFile() {
        try {
            if (dumpFile != null) {
                file = null;
                dumpFile.close();
                dumpFile = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createFolders(String path) {
        try {
            File folder = new File(path);
            folder.mkdirs();
            folder = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processArchiveFile() {
        String path = Settings.getAbsolutepath() + "history/" + exchange + "/" + fileRegister.fileName;
        createFolders(path);
        HistoryDownloader.extractFile(path, file);
        HistoryArchiveRegister.getSharedInstance().addEntry(exchange, fileRegister.fileName,
                fileRegister.totalLength, true /* this parameter not used in satellite mode */);
    }

    private void processOHLCFile() {
        String path = Settings.getAbsolutepath() + "ohlc/" + exchange;// + "/" + file;
        createFolders(path);
        HistoryDownloader.extractFile(path, file);
        OHLCStore.getInstance().loadIntradayHistory(exchange);
        HistoryArchiveRegister.getSharedInstance().addEntry(exchange, fileRegister.fileName,
                fileRegister.totalLength, true /* this parameter not used in satellite mode */);
    }

    private void processStockMasterFile() {
        MarketDataDownloader.extractFile(file, exchange);
        updateInProgressStockMaster(exchange, fileRegister.version);
    }

    private void processPropertyFile() {
        PropertyFileDownloader.extractFile(file);
        Settings.setPropertyVersion(fileRegister.version);
        updateInProgressStockMaster(exchange, fileRegister.version);
    }

    public void processAutoUpdate() {
        try {
            // check if existing md5 code is equal to the active version's. if not save the active as he currebt
            Settings.setTwUpdateAvailable(false);
            if ((fileRegister.version > 0) && (Settings.getTwVersionInInt() != fileRegister.version)) { // must download new version
                if (isMD5OK()) {
                    twUpdateDownloaded = true;
                    File destination = new File(Settings.UPDATE_PATH + "/update.mdf");
                    SharedMethods.copFile(file, destination);
                    Settings.setTwUpdateAvailable(true);
                    TWUpdateDownloader.showDownloadCompletedMessage();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processAnnouncements() {
        AnnouncementDownloadManager.getSharedInstance().extractFile(file, exchange);
    }

    private void processTradeSummary() {
        TradeSummaryDownloader.extractFile(file, exchange);
    }

    private void processDailyTrades() {
        String archiveName = DetailedTradeStore.extractFile(file, exchange);
        if (archiveName != null) {
            TradeBacklogStore.getSharedInstance().updateListFromFiles(exchange);
            HistoryArchiveRegister.getSharedInstance().addEntry(exchange, archiveName,
                    fileRegister.totalLength, true /* this parameter not used in satellite mode */);
        }
    }

    public void setInavtive() {
        try {
            active = false;
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}