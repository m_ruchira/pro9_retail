package com.isi.csvr.communication.tcp;

import com.isi.csvr.Client;
import com.isi.csvr.RequestManager;
import com.isi.csvr.communication.SendQInterface;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.news.NewsProvider;
import com.isi.csvr.news.NewsProvidersStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.win32.NativeMethods;

import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.*;


/**
 * Created by IntelliJ IDEA. User: Uditha Nagahawatta Date: Mar 30, 2005 Time: 4:24:41 PM
 */
public class InternationalConnector implements Runnable, ConnectionListener, SendQInterface {
    private static String ip;
    private static InternationalConnector self = null;
    private Socket socket;
    private boolean active;
    private boolean primaryConnectionReady = false;
    private boolean ready;
    private List receiveBuffer;
    private boolean initiated;
    private OutputStream out;
    private InputStream in;
    private DataSender dataSender;
    private List<String> outBuffer;
    private LinkedList<String> offlineMessages = new LinkedList<String>();

    private InternationalConnector() {
        ConnectionNotifier.getInstance().addConnectionListener(this);
        outBuffer = Collections.synchronizedList(new ArrayList<String>());
        /*try {
            DataInputStream in = new DataInputStream(new FileInputStream("system/intlip.dll"));
            ip = in.readLine();
            in.close();
            in = null;
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public static synchronized InternationalConnector getSharedInstance() {
        if (self == null) {
            self = new InternationalConnector();
        }
        return self;
    }

    public static void setIp(String ip) {
        InternationalConnector.ip = ip;
    }

    public static String getActiveIP() {
        return ip;
    }

    public void setReceiveBuffer(List receiveBuffer) {
        this.receiveBuffer = receiveBuffer;
    }

    public void initiate() {
        if (!initiated) { // only once per applicaion
            setPrimaryConnectionReady();
            System.out.println("Starting international connector ++++++++++++++++++++");
            Thread thread = new Thread(this, "InternationalConnector-initiate");
            thread.start();
            initiated = true;
        } else {
            disconnect();
            setPrimaryConnectionReady();
        }
    }

    public boolean isRequestValidForDespatch(String request) {
        return true;
    }

    public synchronized void addValidateRequest(String symbol, String referenceID, String language) {
        String request = Meta.VALIDATE_SYMBOL + Meta.DS + symbol + Meta.FD + referenceID + Meta.FD + language;
        if (initiated && ready) {
            addData(request);
            request = null;
//        } else {
//            offlineMessages.add(request);
        }
    }

    public synchronized void addRemoveRequest(String exchange, String symbol, int type, int instrumentType) {
        String request = Meta.REMOVE_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;
        if (initiated && ready) {
            addData(request);
            request = null;
        } else {
            offlineMessages.add(request);
        }
    }

    public void addAnnouncementBodyRequest(String exchange, String id, String language) {
        String request = Meta.ANNOUNCEMENT_BODY_REQUEST + Meta.DS + exchange + Meta.FD + id + Meta.FD + language;
        if (initiated && ready) {
            addData(request);
            request = null;
        } else {
            offlineMessages.add(request);
        }
    }

    public void addNewsBodyRequest(String exchange, String id, String language) {
        String request = Meta.NEWS_BODY_REQUEST + Meta.DS + exchange + Meta.FD + id + Meta.FD + language;
        if (initiated && ready) {
            addData(request);
            request = null;
        } else {
            offlineMessages.add(request);
        }
    }

    public synchronized void addAddRequest(String exchange, String symbol, int type, int instrumentType) {
        String request;
        if (symbol == null) {
            request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
        } else {
            request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;
        }
        if (initiated && ready) {
            addData(request);
            System.out.println("==========> Intl add req " + request);
            request = null;
        } else {
            offlineMessages.add(request);
        }
    }

    public synchronized void writeData(String data) throws Exception {
        throw new Exception("Operation Not allowed");
//        if (initiated && ready) {
//        out.write(data.getBytes());
//        }
    }

    /**
     * Adds the given frame to the out queue.
     */
    public synchronized void addData(String sData) {
        if (initiated && ready) {
//            if (dataSender != null)
//                dataSender.send(sData + "\n");
//            System.out.println("International writeData=="+sData);
            outBuffer.add(sData + "\n");
        } else {
            offlineMessages.add(sData + "\n");
        }
    }


    /**
     * Clears the senc queue
     */
    public void clear() {
        if (initiated && ready) {
            dataSender.clearBuffer();
        }
    }

    public void activate() {
        if (ip != null)
            active = true;
    }

    private float getFreeDiskSpace() {
        try {
            return NativeMethods.getFreeDiskSpace(System.getProperties().getProperty("user.dir").substring(0, 2));
        } catch (Exception e) {
            return -1;
        }
    }

    public void disconnect() {
        try {
            ready = false;
            dataSender.clearBuffer();
            offlineMessages.clear();
        } catch (Exception e) {
        }
        try {
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            socket.close();
            socket = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openSocket() throws Exception {
        //todo - need to remove this
//        ip = "192.168.0.124";
        socket = new Socket(ip, Settings.SECONDARY_SERVER_PORT);
        active = true;
    }

    public void setPrimaryConnectionReady() {
        primaryConnectionReady = true;
    }

    public void run() {
        StringBuilder buffer;
        int value;
//        while (true) { // untill appllication stops

        while (active) { // for the session

            while (!Settings.isUSConnectionSuccess() && primaryConnectionReady) {
                try {
                    System.out.println("Connecting to Symbol Server " + ip);
                    openSocket();
                    Settings.setUSConnectionSuccess(true);
//                    primaryConnectionReady = false;
                    //                    Client.getInstance().setPriceConnectionBulbStatus();
                    //                    Client.getInstance().setUSConnectionSucess();
                    in = socket.getInputStream();
                    out = socket.getOutputStream();
                    System.out.println("Connected to Symbol Server");
                    buffer = new StringBuilder();

                    // Authenticate
                    StringBuffer clientInfo = new StringBuffer();
                    clientInfo.append(NativeMethods.getOSName());
                    clientInfo.append("|");
                    clientInfo.append(NativeMethods.getMemorySize());
                    clientInfo.append("|");
                    clientInfo.append(NativeMethods.getCPUSpeed());
                    clientInfo.append("|");
                    clientInfo.append(Settings.systemLocale);
                    clientInfo.append("|");
                    clientInfo.append(NativeMethods.getBuildDate());
                    clientInfo.append("|");
                    clientInfo.append(NativeMethods.getJetVersion());
                    clientInfo.append("|");
                    clientInfo.append(NativeMethods.getVolumeSerial());
                    clientInfo.append("|");
                    clientInfo.append(getFreeDiskSpace());

                    String ssoString = "";
                    if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
                        //if (!Settings.getTradeToken().equals("")) {
                        //    ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
                        //            Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.DS + Settings.getTradeToken();
                        // } else {
                        ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
                                Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.DS + Settings.getSingleSignOnID();
                        // }
                    } else if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                        ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
                                Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.DS + Settings.getSingleSignOnID();
                    }

                    String userName;
                    if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
                        // if (!Settings.getTradeToken().equals("")) {
                        //    userName = Settings.getTradeToken();
                        // } else {
                        userName = Settings.getSingleSignOnID();
                        // }
                    } else if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                        userName = Settings.getSingleSignOnID();
                    } else {
                        userName = SharedMethods.encrypt(Settings.getUserName());
                    }

                    String sessionID = "*";
                    //                if (inSafeMode){
                    //                    sessionID = "*";
                    //                }else {
                    //                    sessionID = Settings.getSessionID();
                    //                }

                    String sAuthString =
                            Meta.AUTHENTICATION_REQUEST_VERSION + Meta.DS + 1 + Meta.FS +
                                    Meta.USER_NAME + Meta.DS + userName + Meta.FS +
                                    Meta.USER_PASSWORD + Meta.DS + SharedMethods.encrypt(Settings.getUserPassword()) + Meta.FS +
                                    Meta.USER_VERSION + Meta.DS + Settings.TW_VERSION + Meta.FS +
                                    Meta.USER_SESSION + Meta.DS + sessionID + Meta.FS +
                                    Meta.CONNECTION_PATH + Meta.DS + Meta.CONNECTION_PATH_SECONDARY + Meta.FS +
                                    Meta.USER_INFO + Meta.DS + clientInfo.toString() + Meta.FS +
                                    Meta.USER_LANGUAGE + Meta.DS + Language.getSelectedLanguage() + Meta.FS +
                                    Meta.USER_TYPE + Meta.DS + Meta.USER_TYPE_NEW_TW + Meta.FS + ssoString + "\n";

                    System.out.println("International connector auth ==" + sAuthString);
                    out.write(sAuthString.getBytes());
                    StringBuilder logginbuffer = new StringBuilder();
                    value = 0;
                    while (value != '\n') {
                        value = in.read();
                        if (value == -1) {
                            throw new Exception("Error while Authenticating with International Server");
                        }
                        logginbuffer.append((char) value);
                    }
                    String frame = logginbuffer.toString();
                    System.out.println("international connector response ==" + frame);
                    if ((logginbuffer.length() > 0)) {
                        String[] fields = frame.split(Meta.FS);
                        boolean authenticated = false;
                        for (int i = 0; i < fields.length; i++) {
                            String[] data = fields[i].split(Meta.DS);
                            try {
                                switch (Integer.parseInt(data[Meta.TAG])) {
                                    case Meta.AUTH_RESULT:
                                        authenticated = (SharedMethods.intValue(data[Meta.VALUE]) == Meta.YES);
                                        break;
                                }
                            } catch (Exception ex) {

                            }
                        }
                        if (!authenticated) {
                            Settings.setUSConnectionSuccess(false);
                            Client.getInstance().setPriceConnectionBulbStatus();
                            break;
                        }
                    }
                    dataSender = null;
                    dataSender = new DataSender();

                    // set the state to ready
                    ready = true;
                    Client.getInstance().setPriceConnectionBulbStatus();

                    //                    sendInitialRequests();

                    // read data
                    while (true) {
                        value = in.read();
                        try {
                            if (value == -1)
                                throw new Exception("End of International Stream");
                            if (value != '\n') {
                                buffer.append((char) value);
                                continue;
                            }
                        } catch (InterruptedIOException e) {
                            continue;
                        }
                        if ((buffer != null) && (buffer.length() > 0)) {
                            receiveBuffer.add(buffer.toString());
                            //                            System.out.println("FROM INTL -> " + buffer.toString());
                            TCPConnector.getFrameAnalyser().addFrame(buffer.toString());
                            buffer = null;
                            buffer = new StringBuilder("");
                        }
                    }
                } catch (Exception e) {
                    Settings.setUSConnectionSuccess(false);
                    Client.getInstance().setPriceConnectionBulbStatus();
                    //                    Client.getInstance().setUSConnectionDown();
                    e.printStackTrace();
                } finally {
                    in = null;
                    out = null;
                    buffer = null;
                }
            }
            if (active) {
                sleep(5000);
            }
        }
//            System.out.println("International Connetor reader exit ++++++++++++++++");
//            sleep(5000);
//        }
    }

    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void twConnected() {
    }

    public void twDisconnected() {
        active = false;
        initiated = false;
        primaryConnectionReady = false;
        disconnect();
        System.out.println("International Connetor closed ++++++++++++++++");
    }

    public void loadOfflineRequests() {
        for (int i = offlineMessages.size() - 1; i >= 0; i--) {
            addData(offlineMessages.remove(i));
        }
        int count = RequestManager.getSharedInstance().getAddRequestSize();
        for (int i = 0; i < count; i++) {
            Object[] record = RequestManager.getSharedInstance().getAddRequest(i);
            String id = (String) record[RequestManager.ID];

            try { // first check if the request to be sent belog to subscribed exchanges
                if ((Byte) record[RequestManager.PATH] == Constants.PATH_SECONDARY) {
                    String symbolKey = id.split("\\|")[1];
                    String exchange = SharedMethods.getExchangeFromKey(symbolKey);
                    if (ExchangeStore.getSharedInstance().isValidExchange(exchange)) {
                        addData(record[RequestManager.REQUEST] + Meta.EOL);
                    }
                }
            } catch (Exception e) { // this may nnot be a exchange related request. send anyway
                addData(record[RequestManager.REQUEST] + Meta.EOL);
            }
        }
    }

    public void sendInitialRequests() {
 /*       Enumeration<String> newsProviders = NewsProvidersStore.getSharedInstance().getProviderKeys();
      Enumeration<Exchange> exchanges = null;
      exchanges = ExchangeStore.getSharedInstance().getExchanges();
      while (exchanges.hasMoreElements()) {
          Exchange exchange = exchanges.nextElement();
          if (!exchange.isDefault()) {
              addData(Meta.ADD_EXCHANGE_REQUEST + Meta.DS + Meta.ANNOUNCEMENTS + Meta.FD + exchange.getSymbol());
                newsProviders = NewsProvidersStore.getSharedInstance().getNewsProviders();
//                newsProviders = NewsProvidersStore.getSharedInstance().getProviderKeys();
//                while (newsProviders.hasMoreElements()) {
//                    try {
//                        String provider = newsProviders.nextElement();
//                        if (NewsProvidersStore.getSharedInstance().isGlobalProvider(provider)) {
//                            addData(Meta.NEWS + Meta.DS + provider + "." + exchange.getCountry());
//                        } else {
//                            addData(Meta.NEWS + Meta.DS + provider);
//                        }
//                    } catch(Exception e) {
//                        e.printStackTrace();
//                    }
//                }
          }
          exchange = null;
      }
      exchanges = null;
      newsProviders = null;*/
        Enumeration<NewsProvider> nProviders = NewsProvidersStore.getSharedInstance().getAllProviders();
        while (nProviders.hasMoreElements()) {
            try {
                NewsProvider np = nProviders.nextElement();
                /*if((np.isSelected()) && (np.getPath() == Constants.PATH_SECONDARY)){
                                addData(Meta.NEWS + Meta.DS + np.getID() + Meta.FD + 1); // + Meta.FD + Constants.NEWS_ADD_ID);
				}*/
                if ((np.isSelected())) {
                    addData(Meta.NEWS + Meta.DS + np.getID() + Meta.FD + 1); // + Meta.FD + Constants.NEWS_ADD_ID);
                }
            } catch (Exception e) {

            }
        }
        //**********************************************************************************************
//              addData(Meta.NEWS + Meta.DS + "MUBASHER.US");
//              addData(Meta.NEWS + Meta.DS + "MUBASHER");
//              addData(Meta.NEWS + Meta.DS + "DWJN");
        //**********************************************************************************************
    }

    private class DataSender implements Runnable {

        public DataSender() {
            Thread thread = new Thread(this, "InternationalConnector$DataSender-DataSender");
            thread.start();
        }

        public void send(String data) {
            outBuffer.add(data);
        }

        public void clearBuffer() {
            outBuffer.clear();
        }

        public void run() {
//            sendInitialRequests();
            loadOfflineRequests();
            DataStore.getSharedInstance().sendAddRequests(Constants.PATH_SECONDARY);
//            DataStore.getSharedInstance().sendAllSymbolRequests(Constants.PATH_SECONDARY); // register nondefault symbols with server
            while (active) {
                try {
                    while (outBuffer.size() > 0) {
                        String record = outBuffer.remove(0);
//                        SharedMethods.printLine("Int >> " + record);
                        System.out.println("Intl out-------->> " + record);
                        out.write(record.getBytes());
                        record = null;
                    }
                    sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                    disconnect();
                }
            }
            System.out.println("International Connetor writer exit ++++++++++++++++");
            ready = false;
        }
    }
}
