// Copyright (c) 2001 Integrated Systems International (ISI)
package com.isi.csvr.communication.tcp;

/**
 * This is the class where from which data is sent
 * to the server. Given data items are queued and
 * sent to the server in a FIFo manner.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.Client;
import com.isi.csvr.RequestManager;
import com.isi.csvr.communication.SendQInterface;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.io.OutputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SendQueue extends Thread implements SendQInterface {

    private List<String> g_oSendBuffer;
    private OutputStream g_oOut;
    private boolean active = true;

    /**
     * Constructor
     */
    public SendQueue(OutputStream oOut) {
        super("SendQueue");
        g_oSendBuffer = Collections.synchronizedList(new LinkedList<String>());
        g_oOut = oOut;
        active = true;
        start();
    }

    public void loadOfflineRequests() {
        int count = RequestManager.getSharedInstance().getAddRequestSize();
        for (int i = 0; i < count; i++) {
            Object[] record = RequestManager.getSharedInstance().getAddRequest(i);
            String id = (String) record[RequestManager.ID];


            try { // first check if the request to be sent belog to subscribed exchanges
                if ((Byte) record[RequestManager.PATH] == Constants.PATH_PRIMARY) {
                    String symbolKey = id.split("\\|")[1];
                    String exchange = SharedMethods.getExchangeFromKey(symbolKey);
                    if (ExchangeStore.getSharedInstance().isValidExchange(exchange)) {
                        g_oSendBuffer.add(record[RequestManager.REQUEST] + Meta.EOL);
                    }
                }
            } catch (Exception e) { // this may nnot be a exchange related request. send anyway
                g_oSendBuffer.add(record[RequestManager.REQUEST] + Meta.EOL);
            }
        }
//        DataStore.getSharedInstance().sendAddRequests(Constants.PATH_PRIMARY);
    }

    public void run() {
        while (Settings.isConnected()) {
            try {
                /* Loop while the client is connected to the server and there
                    are frames in the queue*/
                String sOutData = null;
                while (Settings.isConnected() && (g_oSendBuffer.size() > 0) && active) {
                    sOutData = g_oSendBuffer.remove(0);
                    if (isRequestValidForDespatch(sOutData)) {
                        writeData(sOutData);
                    }
                    sOutData = null;
                }
                Sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
                HistorySettings.interruptDownload();
                Client.setCanShowDisconnectMessage(true);
                while (!Settings.isAuthenticated()) Sleep(1000);
                Settings.setDisConnected();
                Client.getInstance().setDisConnectedStatus(false);

            }
        }
    }

    public boolean isRequestValidForDespatch(String request) {
        try {
            String[] requestData = request.split(Meta.DS);

            switch (Integer.parseInt(requestData[requestData.length - 1].substring(1))) {
                case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                    if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketDepthByPrice)) {
                        return true;
                    }
                case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                    if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketDepthByOrder)) {
                        return true;
                    }
                default:
                    return true;
            }
        } catch (Exception e) {
            return true;
        }
    }

    public synchronized void addValidateRequest(String symbol, String referenceID, String language) {
        String request;
        request = Meta.VALIDATE_SYMBOL + Meta.DS + symbol + Meta.FD + referenceID + Meta.FD + language;
        System.out.println("Symbol search request in TCP===" + request);
        addData(request);
        request = null;
    }

    public synchronized void addRemoveRequest(String exchange, String symbol, int type, int instrumentType) {
        String request;

        if (symbol == null)
            request = Meta.REMOVE_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
        else
            request = Meta.REMOVE_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;

        addData(request);
        request = null;
    }

    public void addAnnouncementBodyRequest(String exchange, String id, String language) {
        String request;
        request = Meta.COMPRESSED_ANNOUNCEMENT_BODY_REQUEST + Meta.DS + exchange + Meta.FD + id + Meta.FD + language;
        addData(request);
        request = null;
    }

    public void addNewsBodyRequest(String exchange, String id, String language) {
        String request;
        request = Meta.COMPRESSED_NEWS_BODY_REQUEST + Meta.DS + exchange + Meta.FD + id + Meta.FD + language;
        addData(request);
        request = null;
    }

    public synchronized void addAddRequest(String exchange, String symbol, int type, int instrumentType) {
        String request;
        if (symbol == null)
            request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
        else
            request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;

        System.out.println("Add Request " + request);
        addData(request);
        request = null;

    }

    public synchronized void writeData(String data) throws Exception {
        System.out.println("Primary writedata==" + data);
        g_oOut.write(data.getBytes());
    }

    /**
     * Adds the given frame to the out queue.
     */
    public synchronized void addData(String sData) {
        //System.out.println("Adding to q " + sData);
        if (g_oSendBuffer != null)
            g_oSendBuffer.add(sData + "\n");
    }


    /**
     * Clears the senc queue
     */
    public void clear() {
        g_oSendBuffer.clear();
    }

    /**
     * Sleeps the theread for the given interval
     */
    private void Sleep(long lDelay) {
        try {
            sleep(lDelay);
        } catch (Exception e) {
        }
    }
}