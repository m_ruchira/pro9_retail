package com.isi.csvr.communication;

import com.isi.csvr.Client;
import com.isi.csvr.communication.tcp.ReceiveQueue;

import java.io.InputStream;
import java.net.Socket;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 30, 2004
 * Time: 3:44:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReceiveQFactory {
    private static ReceiveQueue receiveQueue;

    public static ReceiveQInterface createReceiveQueue(Socket socket, InputStream in,
                                                       Client client, SendQInterface sendQueue, boolean compressionSupported) {
        receiveQueue = null;
        receiveQueue = new ReceiveQueue(socket, in, client, sendQueue, compressionSupported);
        return receiveQueue;
    }

    public static ReceiveQueue getReceiveQueue() {
        return receiveQueue;
    }
}
