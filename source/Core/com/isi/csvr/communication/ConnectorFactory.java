package com.isi.csvr.communication;

import com.isi.csvr.communication.tcp.TCPConnector;
import com.isi.csvr.communication.udp.UDPConnector;
import com.isi.csvr.shared.Settings;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 31, 2004
 * Time: 9:03:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConnectorFactory {
    public static ConnectorInterface createConnector() {
        if (Settings.COMMUNICATION_TYPE == Settings.COMMUNICATION_TYPE_TCP) {
            return new TCPConnector();
        } else {
            return new UDPConnector();
        }
    }
}
