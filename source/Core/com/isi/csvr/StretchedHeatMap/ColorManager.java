package com.isi.csvr.StretchedHeatMap;

import java.awt.*;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Janath
 * Date: Nov 3, 2006
 * Time: 11:49:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class ColorManager {

    static double finalDepth = 0;

    public ColorManager() {

    }

    public Vector getColorVect() {
        Vector colorVect = new Vector();

        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR1));    // green
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR2));
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR3));
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR4));
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR5));
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR6));    // white
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR7));
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR8));
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR9));
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR10));
        colorVect.addElement(new Color(MarketMapConstants.MAP_COLOR11));   //red

        return colorVect;
    }

    public Color getColor(Float value, double colorRange) {

        Color color = null;

        Vector colorVect = getColorVect();
        //StringTokenizer stringToken = new StringTokenizer(string, ",");

        // String value0 = stringToken.nextToken();
        //String value1 = stringToken.nextToken();

//        System.out.println("<CM> "+ value0+" "+value1+ " "+colorRange);
        double intValue = value;
        // System.out.println("intV : "+intValue);
        // System.out.println("ran : "+colorRange);
        double colorIndex = intValue / colorRange;
        // System.out.println("Color Index: " + colorIndex);

        if (colorIndex > 10)
            color = (Color) GridPanel.heatColors[22];
        else if (colorIndex > 9)
            color = (Color) GridPanel.heatColors[21];
        else if (colorIndex > 8)
            color = (Color) GridPanel.heatColors[20];
        else if (colorIndex > 7)
            color = (Color) GridPanel.heatColors[19];
        else if (colorIndex > 6)
            color = (Color) GridPanel.heatColors[18];
        else if (colorIndex == 5)
            color = (Color) GridPanel.heatColors[17];
        else if (colorIndex > 4)
            color = (Color) GridPanel.heatColors[16];
        else if (colorIndex > 3)
            color = (Color) GridPanel.heatColors[15];
        else if (colorIndex > 2)
            color = (Color) GridPanel.heatColors[14];
        else if (colorIndex > 1)
            color = (Color) GridPanel.heatColors[13];
        else if (colorIndex > 0)
            color = (Color) GridPanel.heatColors[12];
        else if (colorIndex == 0)
            color = (Color) GridPanel.heatColors[11];
        else if (colorIndex > -1)
            color = (Color) GridPanel.heatColors[10];
        else if (colorIndex > -2)
            color = (Color) GridPanel.heatColors[9];
        else if (colorIndex > -3)
            color = (Color) GridPanel.heatColors[8];
        else if (colorIndex > -4)
            color = (Color) GridPanel.heatColors[7];
        else if (colorIndex >= -5)
            color = (Color) GridPanel.heatColors[6];
        else if (colorIndex > -6)
            color = (Color) GridPanel.heatColors[5];
        else if (colorIndex > -7)
            color = (Color) GridPanel.heatColors[4];
        else if (colorIndex > -8)
            color = (Color) GridPanel.heatColors[3];
        else if (colorIndex > -9)
            color = (Color) GridPanel.heatColors[2];
        else if (colorIndex >= -10)
            color = (Color) GridPanel.heatColors[1];
        else if (colorIndex >= -11)
            color = (Color) GridPanel.heatColors[0];
        else if (colorIndex >= -12)
            color = (Color) GridPanel.heatColors[0];
        else if (colorIndex >= -13)
            color = (Color) GridPanel.heatColors[0];
        else
            color = Color.white;             //   colorIndex == 0 condition missing some values and they are catch from here.

        return color;

    }

    // input vector should be ordered...
    public double colorRangeCreator(Vector vect) {

        int noOfColors = 11;
        double range = 0;

        String minDepth = (String) vect.firstElement();
        StringTokenizer stringToken1 = new StringTokenizer(minDepth, ",");
        stringToken1.nextToken();
        double maxDepthValue = (new Double(stringToken1.nextToken())).doubleValue();
//        System.out.println("Max: " + maxDepthValue);

        String maxDepth = (String) vect.lastElement();
        StringTokenizer stringToken2 = new StringTokenizer(maxDepth, ",");
        stringToken2.nextToken();
        double minDepthValue = Math.abs((new Double(stringToken2.nextToken())).doubleValue());

//        System.out.println("Min: " + Math.abs(minDepthValue));

        if (Math.max(minDepthValue, maxDepthValue) == minDepthValue)
            finalDepth = minDepthValue;
        else
            finalDepth = maxDepthValue;

        range = finalDepth / noOfColors;

        return range;

    }

    public double getFinalDepth() {
        return finalDepth;
    }

}
