package com.isi.csvr.StretchedHeatMap;

import com.isi.csvr.HeatInterface;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: Mar 2, 2007
 * Time: 12:00:01 PM
 * To change this template use File | Settings | File Templates.
 */
//import com.isi.csvr.HeatInterface;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class StretchedHeatRecord {

    private String key;
    private String symbol = "test";
    private float[] value;
    private Color color;
    private Color fontColor;

    public StretchedHeatRecord(String key, float[] value) {
        this.key = key;
        this.value = value;
        this.symbol = HeatInterface.getSymbolFromKey(key);
        this.color = Color.white;
        this.fontColor = Color.BLACK;
    }

    public String getKey() {
        return key;
    }

    public String getSymbol() {
        return symbol;
    }

    public float getValue(int criteria) {
        if (criteria < value.length) {
            return value[criteria];
        } else {
            return value[0];
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color c) {
        color = c;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fc) {
        fontColor = fc;
    }

    public String getStringvalue(int e) {
//         if (1 < value.length) {
//            return toString(value[1]);
//        } else {
//            return new String(value[1]);
//        }
        return null;
    }
}