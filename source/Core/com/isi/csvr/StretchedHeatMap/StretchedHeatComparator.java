package com.isi.csvr.StretchedHeatMap;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: Mar 2, 2007
 * Time: 2:06:27 PM
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.HeatInterface;

import java.util.Comparator;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class StretchedHeatComparator implements Comparator {

    public final byte COMPARE_BY_VALUE_DESCENDING = 0;
    private byte compareMode = COMPARE_BY_VALUE_DESCENDING;
    public final byte COMPARE_BY_VALUE_ASCENDING = 1;
    public final byte COMPARE_BY_SYMBOL = 2;
    private byte criteria = HeatInterface.CRITERIA_PERC_CHANGE;

    public StretchedHeatComparator() {
    }

    public int compare(Object o1, Object o2) {
        StretchedHeatRecord hR1 = (StretchedHeatRecord) o1;
        StretchedHeatRecord hR2 = (StretchedHeatRecord) o2;

        float val1, val2;
        val1 = hR1.getValue(criteria);
        val2 = hR2.getValue(criteria);
        if (Float.isNaN(val1)) val1 = 0;
        if (Float.isNaN(val2)) val2 = 0;

        if (compareMode == COMPARE_BY_SYMBOL) {
            return hR1.getSymbol().compareTo(hR2.getSymbol());
        } else if (compareMode == COMPARE_BY_VALUE_ASCENDING) {
            if (val1 > val2) {
                return 1;
            } else if (val1 < val2) {
                return -1;
            } else {
                return hR1.getSymbol().compareTo(hR2.getSymbol());
            }
        } else {
            if (val1 > val2) {
                return -1;
            } else if (val1 < val2) {
                return +1;
            } else {
                return hR1.getSymbol().compareTo(hR2.getSymbol());
            }
        }
    }

    public byte getComparisonMode() {
        return compareMode;
    }

    public void setComparisonMode(byte mode) {
        compareMode = mode;
    }

    public void setCriteria(byte c) {
        criteria = c;
    }
}