/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: Mar 1, 2007
 * Time: 11:28:17 AM
 * To change this template use File | Settings | File Templates.
 */
package com.isi.csvr.StretchedHeatMap;

import com.isi.csvr.HeatInterface;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;
import java.util.StringTokenizer;
import java.util.Vector;

class GridPanel extends JPanel {
    public static final float DEFAULT_DOUBLE_VALUE = -9999;
    //------------------- Criteria List -------------------------//
    public final static byte CRITERIA_PE_RATIO = 0;
    public final static byte CRITERIA_PERC_CHANGE = 1;
    public final static byte CRITERIA_MARKET_CAP = 2;
    public final static byte CRITERIA_NUM_TRADES = 3;
    public final static byte CRITERIA_VOLUME = 4;
    public final static byte CRITERIA_BID_ASK_RATIO = 5;
    public final static byte CRITERIA_RANGE = 6;
    public final static byte CRITERIA_PCT_RANGE = 7;
    public final static byte CRITERIA_SPREAD = 8;
    public final static byte CRITERIA_PCT_SPREAD = 9;
    // NOT USED IN TWt
    public final static byte CRITERIA_SNP_STAR_RATING = 10;
    public final static byte CRITERIA_PER = 11;
    public final static byte CRITERIA_DIVIDEND_YIELD = 12;
    public final static byte CRITERIA_5_YEAR_GROWTH = 13;
    public final static byte CRITERIA_BETA = 14;
    public final static byte CRITERIA_INSTITUTIONAL_HOLDING = 15;
    public final static byte CRITERIA_PERC_GAIN = 16; // not used
    public final static byte CRITERIA_SNP_RANK = 17;
    public final static int CRITERIA_COUNT = 18;
    final int[] colorCount = new int[CRITERIA_COUNT];
    public static Color[] heatColors = new Color[23];
    final int
            GRID_SIZE = 50,
            DRAW = 0,
            FILL = 1,
            PAD = 0;
    final int MAX_COLOR_COUNT = 11;
    final int HALF_GAP = 5;
    final int STAR_RADIUS = 4;
    //public Hashtable heatBuffer = new Hashtable();
    final Object lock = new Object();
    public DynamicArray heatStore = new DynamicArray();
    public DynamicArray heatBuffer = new DynamicArray();
    public StretchedHeatComparator hComparator = new StretchedHeatComparator();
    double xInc, yInc;
    int[][] cells;
    Color upColor = new Color(0, 102, 102);
    Color downColor = new Color(204, 0, 0);
    Vector orderedVector;
    ColorManager colorM = new ColorManager();
    BasicStroke bsBorder = new BasicStroke(3, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    BasicStroke bsNormal = new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    private byte criteria = HeatInterface.CRITERIA_PERC_CHANGE;
    //     HeatPanel hp=new HeatPanel("sss");
    private int DARK_COUNT = 5;
    private boolean isTwoColor = true;
    private float maxVariation = 0f;
    private float midValue = 0f;
    private float midBuffer = 0f;
    private float maxVariationBuffer = 0f;
    private double colorRange = 0;
    private String selectedSymbol = null;

    public GridPanel() {
        initCells();
        heatStore.setComparator(hComparator);
        heatBuffer.setComparator(hComparator);
        // System.out.println("Inside gridpanel");
    }

    protected void paintComponent(Graphics g) {
        // System.out.println("Inside paint.gridpanel");
        super.paintComponent(g);
        StretchedHeatRecord shR;
        String value = "0.00";
        float val = 23.0f;
        int x, y, row, col;
        System.out.println("Store size : " + heatStore.size());
        for (int i = 0; i < heatStore.size(); i++) {
            col = i % heatStore.size();
            row = 1;
            int width = 16;// (1070 - (heatStore.size())) / heatStore.size();
            if (width <= 16) {
                width = 16;

            }
            x = (col + 1) * 2 + col * width;
            y = 2;//(row + 1) * 2 + row * 60 + 1;

            shR = (StretchedHeatRecord) heatStore.get(i);
            // System.out.println("testing color : "+shR.getColor());
            if (true) {
                g.setColor(shR.getColor());
                g.fillRect(x, y, width + 1, 60 + 1);
            }
            g.setColor(Color.BLACK);
            g.drawRect(x - 1, y - 1, width + 2, 60 + 2);

            if (true) {
                g.setColor(shR.getFontColor());
            }
            // g.setColor(Color.white);
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            drawCenteredString(g, shR.getSymbol(), x, y + 12, width);
            //val = hR.getValue(criteria);
            //value = getDisplayValue(val);
            // if ((criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) && (val > 0)) {
            //drawCenteredStars(g, val, x, y+valueHeight-5, cellWidth);
            //g.setFont(starFont);
            //drawCenteredString(g, value, x, y + 26, width);
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            // g.setFont(fontPlain);
//            if(i==0){
//                StretchedHeatMapframe.max_lbl.setBackground(heatColors[22]);
//                StretchedHeatMapframe.max_lbl.setText(""+shR.getValue(1));
//
//            }
//            if(i==(heatStore.size()-1)){
//               StretchedHeatMapframe.min_lbl.setBackground(heatColors[0]);
//               StretchedHeatMapframe.min_lbl.setText(""+shR.getValue(1));
//            }

        }
        if (true) {
            drawSelectedNormalCell(g);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////


    }

    private Graphics drawCenteredString(Graphics g, String str, int left, int y, int w) {

        if (w <= 16) {
            ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD, 10));
            if (g.getFontMetrics().stringWidth(str) >= 60) {
                ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD, 8));
            }
        } else {
            ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD, 11));
            if (g.getFontMetrics().stringWidth(str) >= 60) {
                ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD, 9));
            }
        }


        int strW = g.getFontMetrics(getFont()).getHeight();
        int x = left + (w - strW) / 2;
        if (((w - strW) / 2) <= 3) { // not enough space. use smaller font
            //g.setFont(fontBoldSmall9);
            strW = g.getFontMetrics(getFont()).getHeight();
            x = left + (w - strW) / 2;
            if (((w - strW) / 2) <= 3) { // not enough space. use smaller font
                // g.setFont(fontBoldSmall7);
                strW = g.getFontMetrics(getFont()).getHeight();
                x = left + (w - strW) / 2;
            }
        }

        int strH = g.getFontMetrics().stringWidth(str);
        int ye = y + (60 - strH) / 2;
        if (((60 - strH) / 2) <= 3) { // not enough space. use smaller font
            //g.setFont(fontBoldSmall9);
            strH = g.getFontMetrics().stringWidth(str);
            ye = y + (60 - strH) / 2;
            if (((w - strH) / 2) <= 3) { // not enough space. use smaller font
                // g.setFont(fontBoldSmall7);
                strH = g.getFontMetrics().stringWidth(str);
                ye = y + (60 - strW) / 2;
            }
        }


        // Graphics2D g2d = (Graphics2D)g;
        // g2d.translate(x, y);
        //  g2d.rotate(Math.PI/2.0);
        // g.drawString(str, x, y);
        // Graphics2D g2d = (Graphics2D)g;
        ((Graphics2D) g).translate(left, y);
        ((Graphics2D) g).rotate(Math.toRadians(270));
        //  System.out.println("Drawing string");
        // ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD,  7));
        ((Graphics2D) g).drawString(str, -20 - g.getFontMetrics().stringWidth(str) / 2, 5 + (w / 2));
        ((Graphics2D) g).rotate(Math.toRadians(90));
        ((Graphics2D) g).translate(-left, -y);
        //g2d.rotate((3*Math.PI)/2.0);
//        Graphics2D g2 = (Graphics2D)g;
//        AffineTransform af = new AffineTransform();
//        af.translate( w,60 );
//         af.rotate( Math.toRadians(90) );
//        ((Graphics2D)g).setTransform( af );
//        ((Graphics2D)g).drawString(str, x, y);
//        af.rotate( Math.toRadians(270) );
        // ((Graphics2D)g).setTransform( af );

//        int v = g.getFontMetrics(getFont()).getHeight();
//        System.out.println(v);
//        int j = 0;
//        int k = str.length();
//        int l= str.length();
//        while (j < k + 1) {
//            if (j == k)
//                g.drawString(str.substring(j), x, y + (l * 10));
//            else
//                g.drawString(str.substring(j, j + 1), x, y + (l * 10));
//            j++;
//           l--;
//        }
        return g;
    }


    public void toggleCellColor(int row, int col) {
        int mode = DRAW;
        if (cells[row][col] == DRAW)
            mode = FILL;
        cells[row][col] = mode;
        repaint();
    }

    private void initCells() {
        cells = new int[GRID_SIZE][GRID_SIZE];
        for (int row = 0; row < cells.length; row++)
            for (int col = 0; col < cells[0].length; col++)
                cells[row][col] = DRAW;
    }

    public void updateHeatStore(String[] sa) {
        StretchedHeatRecord hR;
        Stock stockObj;
        float val;
        String valS;
        heatBuffer.clear();
        try {
            for (int i = 0; i < sa.length; i++) {
                float[] valArr = new float[CRITERIA_COUNT];
                String[] symbol = sa[i].split(Constants.KEY_SEPERATOR_CHARACTER);
                String key;

                try {
                    key = SharedMethods.getKey(symbol[0], symbol[1], Integer.parseInt(symbol[2]));
                } catch (Exception e) {
                    key = sa[i];
                }
                stockObj = HeatInterface.getStockObject(key);
                if ((stockObj == null) || ((stockObj.getBestBidPrice() == 0) && (stockObj.getBestAskPrice() == 0) && (stockObj.getLastTradeValue() == 0)))
                    continue;


                val = (float) stockObj.getPercentChange();
                if (val == DEFAULT_DOUBLE_VALUE) {
                    val = 0;
                }
                valArr[CRITERIA_PERC_CHANGE] = val;
                //Market Cap - (value in millions, two decimals)
                valArr[CRITERIA_MARKET_CAP] = (float) (stockObj.getMarketCap() / 1000F); // conver to million format
                //% gains - % with two decimals
                valArr[CRITERIA_PERC_GAIN] = 0f;
                //PE Ratio - two decimals
                valArr[CRITERIA_PE_RATIO] = (float) stockObj.getPER();
                //Number of Trades
                valArr[CRITERIA_NUM_TRADES] = stockObj.getNoOfTrades();
                //Number of Trades
                valArr[CRITERIA_VOLUME] = stockObj.getVolume() / 1000;
                // bid ask ratio
                valArr[CRITERIA_BID_ASK_RATIO] = (float) stockObj.getBidaskRatio();
                // range
                valArr[CRITERIA_RANGE] = (float) stockObj.getRange();
                // pct range
                valArr[CRITERIA_PCT_RANGE] = (float) stockObj.getPctRange();
                // spread
                valArr[CRITERIA_SPREAD] = (float) stockObj.getSpread();
                // pct spread
                valArr[CRITERIA_PCT_SPREAD] = (float) stockObj.getPctSpread();//Float.isNaN ((float)stockObj.getPctSpread())?0:(float)stockObj.getPctSpread();

                valArr[10] = 0;
                valArr[11] = 0;
                valArr[12] = 0;
                valArr[13] = 0;
                valArr[14] = 0;
                valArr[15] = 0;
                valArr[17] = 0;


                hR = new StretchedHeatRecord(key, valArr);
                key = null;
                heatBuffer.insert(hR);
                // heatBuffer.put(i,hR);

            }

        } catch (Exception e) {
            System.out.println("Faliure updating store");
            e.printStackTrace();
        }
        //calculateHeatValues(heatBuffer);
        //orderedVector=orderVector(getHeatStoreValues(1));
        // colorRange=new ColorManager().colorRangeCreator(orderedVector);
        //setCellColor();
        synchronized (lock) {
            DynamicArray tmpDA = heatStore;
            heatStore = heatBuffer;
            heatBuffer = tmpDA;
            midValue = midBuffer;
            maxVariation = maxVariationBuffer;

        }
        orderedVector = orderVector(getHeatStoreValues(1));
        colorRange = new ColorManager().colorRangeCreator(orderedVector);
        setHeatColors(1);
        setCellColor();
    }

    public void setCellColor() {
        StretchedHeatRecord shR;
        for (int i = 0; i < heatStore.size(); i++) {
            shR = (StretchedHeatRecord) heatStore.get(i);
            shR.setColor(colorM.getColor(shR.getValue(1), colorRange));
        }

    }

    private void calcMaxVariationAndMidValue(DynamicArray htStore) {
        StretchedHeatRecord hR;
        float val, min = Float.MAX_VALUE, max = Float.MIN_VALUE;
        midBuffer = 0;
        maxVariationBuffer = 0;
        if (criteria == CRITERIA_SNP_STAR_RATING) {
            //1<3<5
            //case CRITERIA_SNP_STAR_RATING:
            midBuffer = 3f;
            maxVariationBuffer = 2.5f;
        } else if (criteria == CRITERIA_SNP_RANK) {
            //1<3<5
            //case CRITERIA_SNP_STAR_RATING:
            midBuffer = 14.5f;
            maxVariationBuffer = 15f;
        } else {
            for (int i = 0; i < htStore.size(); i++) {
                hR = (StretchedHeatRecord) htStore.get(i);
                switch (criteria) {
                    //-M<0<M
                    case CRITERIA_PE_RATIO:
                    case CRITERIA_5_YEAR_GROWTH:
                    case CRITERIA_PERC_CHANGE:
                    case CRITERIA_PERC_GAIN:
                        val = Math.abs(hR.getValue(criteria));
                        maxVariationBuffer = Math.max(maxVariationBuffer, val);
                        break;
                    //-N<1<M
                    case CRITERIA_BETA:

                    case CRITERIA_PCT_RANGE:
                    case CRITERIA_RANGE:
                    case CRITERIA_PCT_SPREAD:
                    case CRITERIA_SPREAD:
                        val = Math.abs(hR.getValue(criteria) - 1f);
                        if (Float.isInfinite(val) || Float.isNaN(val))
                            val = 0;
                        maxVariationBuffer = Math.max(maxVariationBuffer, val);
                        midBuffer = 1f;
                        break;
                    //range - one color
                    case CRITERIA_MARKET_CAP:
                    case CRITERIA_INSTITUTIONAL_HOLDING:
                    case CRITERIA_VOLUME:
                    case CRITERIA_PER:
                    case CRITERIA_NUM_TRADES:
                        val = hR.getValue(criteria);
                        max = Math.max(max, val);
                        min = Math.min(min, val);
                        maxVariationBuffer = (max - min);
                        midBuffer = 1;
                        break;
                    case CRITERIA_BID_ASK_RATIO:
                        val = hR.getValue(criteria);

                        if ((!Float.isInfinite(val)) && (!Float.isNaN(val)))
                            max = Math.max(max, val);
                        //if (max > 100) max = 100;
                        min = 0;
                        maxVariationBuffer = (max - min);
                        midBuffer = min;
                        break;
                    //range - two color
                    //case CRITERIA_SNP_RANK:
                    case CRITERIA_DIVIDEND_YIELD:
                    default:
                        val = hR.getValue(criteria);
                        max = Math.max(max, val);
                        min = Math.min(min, val);
                        maxVariationBuffer = (max - min) / 2f;
                        midBuffer = min + maxVariationBuffer;
                        break;
                }
            }
        }
    }

    public void setCriteria(byte c) {
        synchronized (lock) {
            criteria = c;
            hComparator.setCriteria(c);

        }
    }

    public void calculateHeatValues(DynamicArray htStore) {
        if (htStore.size() > 0) {
            StretchedHeatRecord hR;
            calcMaxVariationAndMidValue(htStore);
            int index = 0;
            for (int i = 0; i < htStore.size(); i++) {
                hR = (StretchedHeatRecord) htStore.get(i);
                if (maxVariationBuffer > 0) {
                    index = getClosestColorIndex(hR.getValue(criteria));
                    if (index == HeatInterface.DEFAULT_DOUBLE_VALUE) {
                        hR.setColor(Color.WHITE);
                        hR.setFontColor(Color.GRAY);
                        continue;
                    }
                    index = Math.min(Math.max(0, index), heatColors.length - 1);
                    //(int)Math.max(Math.ceil((hR.getValue(criteria)+maxVariationBuffer)*(2f*colorCount[criteria]+1f)/(2f*maxVariationBuffer))-1, 0);
                    hR.setColor(heatColors[index]);
                    if (((isTwoColor) && (index < DARK_COUNT)) || (index >= heatColors.length - DARK_COUNT)) {
                        if ((criteria == HeatInterface.CRITERIA_SNP_RANK) && (index == 0)) {
                            hR.setFontColor(Color.GRAY);
                        } else {
                            hR.setFontColor(Color.WHITE);
                        }
                    } else {
                        hR.setFontColor(Color.BLACK);
                    }
                } else {
                    hR.setColor(Color.WHITE);
                    hR.setFontColor(Color.GRAY);
                }
            }
        }
    }

    private void setHeatColorCount() {
        colorCount[CRITERIA_SNP_RANK] = 7;
        colorCount[CRITERIA_SNP_STAR_RATING] = 2;
        colorCount[CRITERIA_PE_RATIO] = 11;
        colorCount[CRITERIA_DIVIDEND_YIELD] = 11;
        colorCount[CRITERIA_5_YEAR_GROWTH] = 11;
        colorCount[CRITERIA_BETA] = 11;
        colorCount[CRITERIA_INSTITUTIONAL_HOLDING] = 11;
        colorCount[CRITERIA_PERC_CHANGE] = 11;
        colorCount[CRITERIA_MARKET_CAP] = 11;
        colorCount[CRITERIA_PERC_GAIN] = 11;
        colorCount[CRITERIA_BID_ASK_RATIO] = 11;
        colorCount[CRITERIA_SPREAD] = 11;
        colorCount[CRITERIA_RANGE] = 11;
        colorCount[CRITERIA_PCT_RANGE] = 11;
        colorCount[CRITERIA_PCT_SPREAD] = 11;
        colorCount[CRITERIA_PER] = 11;
        colorCount[CRITERIA_NUM_TRADES] = 11;
        colorCount[CRITERIA_VOLUME] = 11;
    }

    private void setIsTwoColor() {
        if ((criteria == HeatInterface.CRITERIA_MARKET_CAP) ||
                (criteria == HeatInterface.CRITERIA_NUM_TRADES) ||
                (criteria == HeatInterface.CRITERIA_VOLUME) ||
                (criteria == HeatInterface.CRITERIA_PER) ||
                (criteria == HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING)) {
            isTwoColor = false;
        } else {
            isTwoColor = true;
        }
    }

    private int getClosestColorIndex(float value) {
        if (value == DEFAULT_DOUBLE_VALUE) {
            return Math.round(value);
        }
        switch (criteria) {
            //0.5<3<5.5
            case CRITERIA_SNP_STAR_RATING:
                return Math.round(value) - 1;
            //-M<0<M
            case CRITERIA_PE_RATIO:
            case CRITERIA_5_YEAR_GROWTH:
            case CRITERIA_PERC_CHANGE:
            case CRITERIA_PERC_GAIN:

                return (int) Math.max(Math.ceil((value + maxVariationBuffer) * (2f * colorCount[criteria] + 1f) / (2f * maxVariationBuffer)) - 1, 0);
            //-N<1<M
            case CRITERIA_BETA:

            case CRITERIA_PCT_RANGE:
            case CRITERIA_RANGE:
            case CRITERIA_PCT_SPREAD:
            case CRITERIA_SPREAD:
                if (Float.isInfinite(value) || Float.isNaN(value))
                    value = 0;
                return (int) Math.max(Math.ceil((value - midBuffer + maxVariationBuffer) * (2f * colorCount[criteria] + 1f) / (2f * maxVariationBuffer)) - 1, 0);
            //range - one color
            case CRITERIA_MARKET_CAP:
            case CRITERIA_INSTITUTIONAL_HOLDING:
            case CRITERIA_PER:
            case CRITERIA_NUM_TRADES:
            case CRITERIA_VOLUME:
                return (int) Math.max(Math.ceil((value - midBuffer) * (colorCount[criteria] + 1f) / (maxVariationBuffer)) - 1, 0);
            case CRITERIA_BID_ASK_RATIO:
                //return (int)Math.max(Math.ceil((value-midBuffer+maxVariationBuffer)*(colorCount[criteria]+1f)/(2f*maxVariationBuffer))-1, 0);
                if (value < 1) {
                    return (int) Math.ceil(11 * value) - 1;
                } else {
                    return (int) Math.ceil((((12 * value) - 12) / (maxVariation - 1)) + 12) - 1;
//                    return (int)Math.ceil(((6 * value) + (5*maxVariation) - 12)/(maxVariation -1));
                }
                //range - two color
            case CRITERIA_SNP_RANK:
            case CRITERIA_DIVIDEND_YIELD:
            default:
                return (int) Math.max(Math.ceil((value - midBuffer + maxVariationBuffer) * (colorCount[criteria] + 1f) / (2f * maxVariationBuffer)) - 1, 0);

        }
    }

    public Vector getHeatStoreValues(int criteria) {

        float value;
        Vector sotedVector = new Vector();
        for (int i = 0; i < heatStore.size(); i++) {
            StretchedHeatRecord stock = (StretchedHeatRecord) heatStore.get(i);
            value = stock.getValue(1);
            sotedVector.addElement(stock.getSymbol() + "," + value);
            stock = null;

        }
        System.out.println("testing size : " + sotedVector.size());
        return sotedVector;
    }

    public Vector orderVector(Vector vect) {
        double[] values = new double[vect.size()];
        String[] valueString = new String[vect.size()];
        double cellValue = 0;
        for (int i = 0; i < vect.size(); i++) {
            String value = (String) vect.elementAt(i);
            StringTokenizer stringToken = new StringTokenizer(value, ",");
            valueString[i] = stringToken.nextToken();
            String value1 = stringToken.nextToken();
            cellValue = (new Double(value1)).doubleValue();
            values[i] = cellValue;
        }
        //  System.out.println("testing2 size : "+vect.size());
        return insertionSort(values, valueString);
    }


    private Vector insertionSort(double[] A, String[] B) {
        // Sort the array A into increasing order.

        int itemsSorted; // Number of items that have been sorted so far.

        for (itemsSorted = 1; itemsSorted < A.length; itemsSorted++) {
            // Assume that items A[0], A[1], ... A[itemsSorted-1]
            // have already been sorted.  Insert A[itemsSorted]
            // into the sorted list.

            double temp = A[itemsSorted];  // The item to be inserted.
            String tempS = B[itemsSorted];

            int loc = itemsSorted - 1;  // Start at end of list.

            while (loc >= 0 && A[loc] > temp) {
                A[loc + 1] = A[loc]; // Bump item from A[loc] up to loc+1.
                B[loc + 1] = B[loc];

                loc = loc - 1;       // Go on to next location.
            }

            A[loc + 1] = temp; // Put temp in last vacated space.
            B[loc + 1] = tempS;

        }

        Vector orderedVect = new Vector();
        for (int i = A.length - 1; i > -1; i--) {
            //s("Values: " + B[i]);
            orderedVect.addElement(B[i] + "," + A[i]);
        }
        // System.out.println("testing3 size : "+orderedVect.size());
        return orderedVect;
    }

    private void setHeatColors(int cry) {
        int r, g, b, rm, gm, bm;
        float factor;
        int colorCnt = 11; //colorCount[criteria];
        if ((cry == CRITERIA_MARKET_CAP) ||
                (cry == CRITERIA_PER) ||
                (cry == CRITERIA_NUM_TRADES) ||
                (cry == CRITERIA_VOLUME) ||
                (cry == CRITERIA_INSTITUTIONAL_HOLDING)) {
            heatColors = new Color[colorCnt + 1];
            heatColors[0] = Color.WHITE;
            r = upColor.getRed();
            g = upColor.getGreen();
            b = upColor.getBlue();
            for (int i = 0; i < colorCnt; i++) {
                factor = i / (float) colorCnt;
                rm = Math.round(r + (255 - r) * factor);
                gm = Math.round(g + (255 - g) * factor);
                bm = Math.round(b + (255 - b) * factor);
                heatColors[colorCnt - i] = new Color(rm, gm, bm);
            }
        } else {
            r = downColor.getRed();
            g = downColor.getGreen();
            b = downColor.getBlue();
            heatColors = new Color[2 * colorCnt + 1];
            for (int i = 0; i < colorCnt; i++) {
                factor = i / (float) colorCnt;
                rm = Math.round(r + (255 - r) * factor);
                gm = Math.round(g + (255 - g) * factor);
                bm = Math.round(b + (255 - b) * factor);
                //System.out.println("rm="+rm+", gm="+gm+", bm="+bm+", factor="+factor+", i="+i);
                heatColors[i] = new Color(rm, gm, bm);
            }
            heatColors[colorCnt] = Color.WHITE;
            r = upColor.getRed();
            g = upColor.getGreen();
            b = upColor.getBlue();
            for (int i = 0; i < colorCnt; i++) {
                factor = i / (float) colorCnt;
                rm = Math.round(r + (255 - r) * factor);
                gm = Math.round(g + (255 - g) * factor);
                bm = Math.round(b + (255 - b) * factor);
                heatColors[2 * colorCnt - i] = new Color(rm, gm, bm);
            }
        }
        //setting dark count
        DARK_COUNT = colorCnt / 2;
    }

    public int getSelectedIndex(String selectedSymbol) {
        StretchedHeatRecord hR;
        if (selectedSymbol != null) {
            for (int i = 0; i < heatStore.size(); i++) {
                hR = (StretchedHeatRecord) heatStore.get(i);
                if (selectedSymbol.equals(hR.getKey())) {
                    return i;
                }
            }
        }
        return -1;
    }

    private void drawSelectedNormalCell(Graphics g) {
        int selectedIndex = getSelectedIndex(selectedSymbol);
        if ((selectedIndex > -1) && (selectedIndex < heatStore.size())) {
            int x, y, row, col;
            col = selectedIndex % heatStore.size();
            row = 1;
            int width = 16;// (1070 - (heatStore.size())) / heatStore.size();
            if (width <= 16) {
                width = 16;
            }
            x = (col + 1) * 2 + col * width;
            y = 2;//(row + 1) * 2 + row * 60 + 1;
            g.setColor(Color.MAGENTA);
            ((Graphics2D) g).setStroke(bsBorder);
            g.drawRect(x - 1, y - 1, width + 2, 60 + 2);
            ((Graphics2D) g).setStroke(bsNormal);
        }
    }

    public void updateSelectedIndex(int xTip, int yTip) {
        synchronized (lock) {
            selectedSymbol = null;

            int noOfCells = heatStore.size();
            Rectangle rect;
            int width = 16;//(1070 - (heatStore.size())) / heatStore.size();
            if (width <= 16) {
                width = 16;
            }
            int col, row, xx, yy;
            for (int i = 0; i < noOfCells; i++) {
                col = i % heatStore.size();
                row = 1;

                xx = (col + 1) * 2 + col * width;
                yy = 2;//(row + 1) * 2 + row * 60 + 1;
                rect = new Rectangle(xx, yy, width, 60);
                if (rect.contains(xTip, yTip)) {
                    selectedSymbol = ((StretchedHeatRecord) heatStore.get(i)).getKey();
                    break;
                }
                rect = null;
            }


        }
    }

    public String getSelectedSymbol() {
        return selectedSymbol;
    }

    public int setPanelWidth() {
        int width = (1070 - (heatStore.size())) / heatStore.size();
        if (width <= 16) {
            return 17 * heatStore.size() + heatStore.size() + 3;

        } else {
            return 17 * heatStore.size() + heatStore.size() + 2;
        }

    }

    public void refreshPanel() {

    }

}