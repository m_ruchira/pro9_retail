package com.isi.csvr.StretchedHeatMap;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: Mar 2, 2007
 * Time: 4:41:27 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MarketMapConstants {

//    public static final int MAP_COLOR1  = 694802;
//	public static final int MAP_COLOR2  = 2918696;
//	public static final int MAP_COLOR3  = 7452790;
//	public static final int MAP_COLOR4  = 12773836;
//	public static final int MAP_COLOR5  = 15071725;
//    public static final int MAP_COLOR6  = 16777215;
//    public static final int MAP_COLOR7  = 16705266;
//    public static final int MAP_COLOR8  = 16692936;
//    public static final int MAP_COLOR9  = 16680341;
//    public static final int MAP_COLOR10 = 16664914;
//    public static final int MAP_COLOR11 = 16646659;

    public static final int MAP_COLOR1 = 2918696;
    public static final int MAP_COLOR2 = 694802;
    public static final int MAP_COLOR3 = 7452790;
    public static final int MAP_COLOR4 = 12773836;
    public static final int MAP_COLOR5 = 15071725;
    public static final int MAP_COLOR6 = 16777215;
    public static final int MAP_COLOR7 = 16705266;
    public static final int MAP_COLOR8 = 16692936;
    public static final int MAP_COLOR9 = 16680341;
    public static final int MAP_COLOR10 = 16664914;
    public static final int MAP_COLOR11 = 16646659;

}
