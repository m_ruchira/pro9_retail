package com.isi.csvr.sectoroverview;

import com.isi.csvr.bandwidthoptimizer.OptimizedSymbolNode;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.Sector;
import com.isi.csvr.datastore.SectorStore;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Nov 18, 2008
 * Time: 2:00:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class SectorOverviewUpdator implements Runnable {
    public static boolean isStart = true;
    public static ArrayList<Exchange> exchangesList;
    public static ArrayList<Exchange> tempexchangesList;
    public static SectorOverviewUpdator self;
    public static boolean active = true;
    public static boolean isWIndowActive = false;
    private static SectorOverviewUpdator feeder;
    Hashtable<String, Vector<String>> indexTable = new Hashtable<String, Vector<String>>();


    public SectorOverviewUpdator() {
        if (!isWIndowActive) {
            exchangesList = new ArrayList<Exchange>();
            tempexchangesList = new ArrayList<Exchange>();
        }
    }

    public static SectorOverviewUpdator getSharedInstance() {
        if (self == null) {
            self = new SectorOverviewUpdator();
        }
        return self;
    }

    public void run() {
        while (isWIndowActive) {
            try {
                sectorDetailsGatherer();
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void sectorExchangesGatherer(Exchange exString) {
        if (exchangesList.contains(exString)) {
        } else {
            exchangesList.add(exString);
        }
    }

    public void sectorTempExchangesGatherer(Exchange exString) {
        if (tempexchangesList.contains(exString)) {
        } else {
            tempexchangesList.add(exString);
        }
    }

    public void sectorExchangesRemover(Exchange exString) {
        if (exchangesList.contains(exString)) {
            exchangesList.remove(exString);
            tempexchangesList.remove(exString);
        } else {
            System.out.println("three is no such object-------");
        }
    }

    public void sectorDetailsGatherer() {
        for (int i = 0; i < exchangesList.size(); i++) {
            Exchange exchange = exchangesList.get(i);
            if (exchange != null) {

                Enumeration defaultEnum = DataStore.getSharedInstance().getFilteredList(exchange.getSymbol(), Meta.INSTRUMENT_INDEX);
                Vector defaultVector = new Vector();
                while (defaultEnum.hasMoreElements()) {

                    defaultVector.add(defaultEnum.nextElement());
                }
                indexTable.put(exchange.getSymbol(), defaultVector);
                Hashtable<String, SectorOverviewObject> sectorHash = new Hashtable<String, SectorOverviewObject>();

                Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exchange.getSymbol());
                if (sectorsEnum.hasMoreElements()) {
                    while (sectorsEnum.hasMoreElements()) {

                        Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exchange.getSymbol());
                        Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                        Sector sector = (Sector) sectorsEnum.nextElement();
                        SectorOverviewObject sectorObject = new SectorOverviewObject(sector.getId(), sector.getDescription());
                        sectorObject.setExchange(exchange.getSymbol());
                        sectorHash.put(sector.getId(), sectorObject);
                    }
                }

                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exchange.getSymbol());

                while (exchageSymbols.hasMoreElements()) {
                    String symbol = (String) exchageSymbols.nextElement();
                    Stock st = DataStore.getSharedInstance().getStockObject(exchange.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));

                    try {
                        SectorOverviewObject secObject = sectorHash.get(st.getSectorCode());
                        if (secObject != null) {
                            if (st.getChange() > 0)
                                secObject.setUps(1);
                            else if (st.getChange() < 0)
                                secObject.setDowns(1);
//                            else if (st.getPreviousClosed() > 0)
                            else if (st.getChange() == 0 && st.getNoOfTrades() != 0)
                                secObject.setNoChange(1);
                            secObject.addTurnover(st.getTurnover());
                            secObject.addVolume(st.getVolume());
                            secObject.setCashInTurnover(st.getCashInTurnover());
                            secObject.setCashOutTurnover(st.getCashOutTurnover());
                        }
                    } catch (Exception e) {
                    }
                }

                Vector symbolList = (Vector) indexTable.get(exchange.getSymbol());
                for (int j = 0; j < symbolList.size(); j++) {
                    String symbol = (String) symbolList.get(j);
                    Stock oIndex = DataStore.getSharedInstance().getStockObject(exchange.getSymbol(), symbol, Meta.INSTRUMENT_INDEX);
                    SectorOverviewObject secObject = sectorHash.get(symbol);

                    if (secObject != null) {
                        secObject.setIndexdescription(oIndex.getSymbol());
                        secObject.setIndexVal(oIndex.getLastTradeValue());
                        secObject.setHigh(oIndex.getHigh());
                        secObject.setLow(oIndex.getLow());
                        secObject.setChange(oIndex.getChange());
                        secObject.setPerChange(oIndex.getPercentChange());
                        secObject.setIndexAvailable(true);
                    }
                }
                symbolList = null;

                Enumeration keys = sectorHash.keys();
                while (keys.hasMoreElements()) {
                    String secId = (String) keys.nextElement();
                    SectorOverviewObject addingObject = sectorHash.get(secId);
                    SectorObjectStore.getSharedInstance().addObjects(exchange.getSymbol(), addingObject);
                }
            }
        }
    }
}


