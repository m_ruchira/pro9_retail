package com.isi.csvr.sectoroverview;

import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.TableUpdateListener;

import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Nov 19, 2008
 * Time: 5:08:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class SectorOverviewModal extends CommonTable
        implements TableModel, TableUpdateListener, CommonTableInterface, DDELinkInterface, ClipboardOwner {

    private static final DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private ArrayList<SectorOverviewObject> records;
    private Clipboard clip;

    public SectorOverviewModal(ArrayList<SectorOverviewObject> dataArray) {
        this.records = dataArray;
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    public static int compare(Object o1, Object o2, int sortColumn, int sortOrder) {
        SectorOverviewObject rec1 = (SectorOverviewObject) o1;
        SectorOverviewObject rec2 = (SectorOverviewObject) o2;
        switch (sortColumn) {

            case 5:
                return compareValues(rec1.getTurnover(), rec2.getTurnover(), sortOrder);
            case 8:
                return compareValues(rec1.getIndexVal(), rec2.getIndexVal(), sortOrder);
            case 9:
                return compareValues(rec1.getHigh(), rec2.getHigh(), sortOrder);
            case 10:
                return compareValues(rec1.getLow(), rec2.getLow(), sortOrder);
            case 11:
                return compareValues(rec1.getChange(), rec2.getChange(), sortOrder);
            case 12:
                return compareValues(rec1.getPerChange(), rec2.getPerChange(), sortOrder);
            case 13:
                return compareValues(rec1.getCashInTurnover(), rec2.getCashInTurnover(), sortOrder);
            case 14:
                return compareValues(rec1.getCashOutTurnover(), rec2.getCashOutTurnover(), sortOrder);
            case 15:
                return compareValues(rec1.getCashFlowPct(), rec2.getCashFlowPct(), sortOrder);
            default:
                return 0;
        }
    }

    private static int compareValues(double val1, double val2, int sortOrder) {
        if (Double.isNaN(val1) && Double.isNaN(val2)) {
            return 0;
        } else if (Double.isNaN(val1)) {
            return 1;
        } else if (Double.isNaN(val2)) {
            return -1;
        }
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

    public static boolean isDynamicData(int column) {
        switch (column) {
            case 0:
            case 1:
            case 7:
                return false;
            default:
                return true;
        }
    }

    public int getRowCount() {
        return records.size();
    }

    public void addData(SectorOverviewObject sob) {
        for (int i = 0; i < records.size(); i++) {
            if (records.get(i).getSectorID().equals(sob.getSectorID())) {
                records.remove(i);
            }
        }
        records.add(sob);
    }

    public ArrayList<SectorOverviewObject> getData() {
        return records;
    }

    public int getColumnCount() {
        return 16;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        SectorOverviewObject sectorOb = (SectorOverviewObject) records.get(rowIndex);
        if (sectorOb == null) {
            return "fff"; //null;
        }
        try {
            switch (columnIndex) {
                case -1:
                    return sectorOb.isIndexAvailable();
                case 0:
                    return " " + sectorOb.getDescription();
                case 1:
                    return " " + sectorOb.getSectorID();
                case 2:
                    return Double.valueOf("" + sectorOb.getUps());
                case 3:
                    return Double.valueOf("" + sectorOb.getDowns());//longTransferObject.setValue(splitRec.getStartDate());
                case 4:
                    return Double.valueOf("" + sectorOb.getNoChange());
                case 5:
                    return sectorOb.getTurnover();
                case 6:
                    return Double.valueOf("" + sectorOb.getVolume());
                case 7:
                    return " " + sectorOb.getIndexdescription();
                case 8:
                    return sectorOb.getIndexVal();
                case 9:
                    return sectorOb.getHigh();
                case 10:
                    return sectorOb.getLow();
                case 11:
                    return sectorOb.getChange();
                case 12:
                    return sectorOb.getPerChange();
                case 13:
                    return sectorOb.getCashInTurnover();
                case 14:
                    return sectorOb.getCashOutTurnover();
                case 15:
                    return sectorOb.getCashFlowPct();
                default:
                    return "ffff ";
            }


        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (iCol) {
            case 0:
            case 1:
                return String.class;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                return Double.class;
            default:
                return Object.class;
        }
    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tableUpdating() {
        this.getTable().updateUI();
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void getDDEString(Table table, boolean withHeadings) {
        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (withHeadings)
            buffer.append(copyHeaders(table));
        for (int r = row; r < (rows + row); r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    if (SectorOverviewModal.isDynamicData(modelIndex)) {
                        buffer.append("=MRegionalDdeServer|'");
                        buffer.append("SO");
                        buffer.append(this.getViewSettings().getID() + Constants.KEY_SEPERATOR_CHARACTER + ((String) (table.getTable().getModel().getValueAt(r, 1))).trim());
                        buffer.append("'!'");
                        buffer.append(modelIndex);
                        buffer.append("'");
                        buffer.append("*1");

                    } else {
                        buffer.append(table.getTable().getModel().getValueAt(r, modelIndex));
                    }

                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        for (int c = 0; c < cols; c++) {
            int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public String getDDEString(String id, int col) {
        for (int i = 0; i < getRowCount(); i++) {
            if (((SectorOverviewObject) records.get(i)).getSectorID().equals(id))
                return String.valueOf(getValueAt(i, col));
        }
        return "0";
    }
}
