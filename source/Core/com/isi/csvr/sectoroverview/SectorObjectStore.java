package com.isi.csvr.sectoroverview;

import com.isi.csvr.MainBoardLoader;

import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Nov 18, 2008
 * Time: 1:42:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class SectorObjectStore {

    public static SectorObjectStore self;
    private static Hashtable<String, SectorOverviewObject> sectorStore = new Hashtable<String, SectorOverviewObject>();

    public static SectorObjectStore getSharedInstance() {
        if (self == null) {
            self = new SectorObjectStore();
        }
        return self;
    }

    public static Hashtable<String, SectorOverviewObject> getSectorStore() {
        System.out.println("size of sector store ===" + sectorStore.size());
        return sectorStore;
    }

    public static void setSectorStore(Hashtable<String, SectorOverviewObject> sectorStore) {
        SectorObjectStore.sectorStore = sectorStore;
    }

    public void addObjects(String ob, SectorOverviewObject secOb) {
        if (sectorStore != null) {

            sectorStore.put(ob, secOb);

        }

        if (MainBoardLoader.tableStore.containsKey(ob)) {
            SectorOverviewUI secUI = MainBoardLoader.tableStore.get(ob);
            secUI.addData(secOb);
        }

    }
}
