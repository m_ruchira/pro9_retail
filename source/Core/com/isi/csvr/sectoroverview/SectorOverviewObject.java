package com.isi.csvr.sectoroverview;

import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Nov 18, 2008
 * Time: 1:39:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class SectorOverviewObject implements Comparable {

    public double cashFlowRatio;
    private String sectorID;
    private int noOfTrades;
    private int traded;
    private int ups;
    private int downs;
    private int noChange;
    private double turnover;
    private long volume;
    private String description;
    private String exchange;
    private double high;
    private double low;
    private double change;
    private double perChange;
    private String indexdescription;
    private double indexVal;
    private boolean isIndexAvailable = false;
    private double cashInTurnover;
    private double cashOutTurnover;


    public SectorOverviewObject(String sectorID, String description) {
        this.sectorID = sectorID;
        this.description = description;
    }

    public double getIndexVal() {
        return indexVal;
    }

    public void setIndexVal(double indexVal) {
        this.indexVal = indexVal;
    }

    public boolean isIndexAvailable() {
        return isIndexAvailable;
    }

    public void setIndexAvailable(boolean indexAvailable) {
        isIndexAvailable = indexAvailable;
    }

    public String getIndexdescription() {
        return indexdescription;
    }

    public void setIndexdescription(String indexdescription) {
        this.indexdescription = indexdescription;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getPerChange() {
        return perChange;
    }

    public void setPerChange(double perChange) {
        this.perChange = perChange;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getDescription() {
        return description;
    }

    public long getVolume() {
        return volume;
    }

    public void addVolume(long volume) {
        this.volume += volume;
    }

    public double getTurnover() {
        return turnover;
    }

    public void addTurnover(double turnover) {
        this.turnover += turnover;
    }

    /*public SectorObject(String sectorID) {
        this.sectorID = sectorID;
    }*/
    public int getDowns() {
        return downs;
    }

    public void setDowns(int downs) {
        this.downs += downs;
    }

    public int getNoChange() {
        return noChange;
    }

    public void setNoChange(int noChange) {
        this.noChange += noChange;
    }

    public int getNoOfTrades() {
        return noOfTrades;
    }

    public void setNoOfTrades(int noOfTrades) {
        this.noOfTrades += noOfTrades;
    }

    public String getSectorID() {
        return sectorID;
    }

    public int getTraded() {
        return traded;
    }

    public void setTraded(int traded) {
        this.traded += traded;
    }

    public int getUps() {
        return ups;
    }

    public void setUps(int ups) {
        this.ups += ups;
    }

    public double getCashInTurnover() {
        return cashInTurnover;
    }

    public void setCashInTurnover(double cashInTurnover) {
        this.cashInTurnover += cashInTurnover;
    }

    public double getCashOutTurnover() {
        return cashOutTurnover;
    }

    public void setCashOutTurnover(double cashOutTurnover) {
        this.cashOutTurnover += cashOutTurnover;
    }

    public String getSymbol() {
        String symbol = SharedMethods.getSymbolFromKey(sectorID);
        if (symbol == null)
            return "";
        else
            return symbol;
    }

    public int compareTo(Object o) {
        SectorOverviewObject sec = (SectorOverviewObject) o;
        return new String(this.description).compareTo(sec.getDescription());
    }

    public double getCashFlowRatio() {
        //return cashInTurnover / cashOutTurnover;

        try {
            if ((cashInTurnover == 0) && (cashOutTurnover == 0))
                cashFlowRatio = -1;
            else if (cashInTurnover == 0)
                cashFlowRatio = 0;
            else if (cashOutTurnover == 0)
                cashFlowRatio = Double.POSITIVE_INFINITY;
            else {
                cashFlowRatio = cashInTurnover / cashOutTurnover;
            }
        } catch (Exception e) {
            cashFlowRatio = -1;
        }
        return cashFlowRatio;
    }

    public void setCashFlowRatio(double cashFlowRatio) {
        this.cashFlowRatio = cashFlowRatio;
    }

    public double getCashFlowPct() {
        return (cashInTurnover) / (cashInTurnover + cashOutTurnover);
    }
}
