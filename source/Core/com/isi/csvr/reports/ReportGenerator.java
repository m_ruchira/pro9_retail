package com.isi.csvr.reports;

import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Market;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PrinterJob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;

/**
 * <p>Title: </p> <p>Description: </p> <p>Copyright: Copyright (c) 2002</p> <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class ReportGenerator extends JFrame implements ComponentListener, DateSelectedListener,
        Runnable, ExchangeListener, WindowListener {

    public static final int REPORT_SYMBOL_TRADING_SUMMARY = 1;
    public static final int REPORT_DAILY_MARKET_SUMMARY = 2;

    public static ReportGenerator self = null;
    public DailyMarketSummary mktSummary;
    private SymbolTradingSummaryPanel tradingSummary;
    private JButton btnPrint;
    private JButton btnClose;
    private JButton btnRefresh;
    private JButton btnDate;
    private JButton btnToday;
    private JScrollPane scrollPane;
    private DatePicker fromDate;
    private JComboBox cmbExchanges;
    private ArrayList<TWComboItem> exchangeList;
    private Calendar cal;
    private SimpleDateFormat format;
    private JDialog waitDialog;
    private ArrayList<TWComboItem> subMarketList;
    private String selectedExchange;
    private JComboBox cmbSubMarkets;
    private JPanel buttonPanel;
    private JLabel lblExchange;
    private JLabel lblLabel1;
    private JLabel lblMarket;
    private int selectedMarketIndex = 0;
    private String selectedMarket;

    private int reportType;
    private int selectedExcgIndex = 0;

    public ReportGenerator(int type) {
        ReportUtilities.loadImages(this);
        self = this;
        reportType = type;
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        this.getContentPane().setLayout(new BorderLayout());

        ExchangeStore.getSharedInstance().addExchangeListener(this);

        exchangeList = new ArrayList<TWComboItem>();
        TWComboModel comboModel = new TWComboModel(exchangeList);
        cmbExchanges = new JComboBox(comboModel);
        cmbExchanges.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedExcgIndex = cmbExchanges.getSelectedIndex();
                selectedExchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
                if (populateMarkets()) {
                    generateReportForTheSelectedMarket(selectedExchange, selectedMarket);
                } else {
                    generateReportForTheSelectedExchange();
                }

            }
        });
        if (exchangeList.size() == 1) {
            selectedExchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
            if (populateMarkets()) {
                generateReportForTheSelectedMarket(selectedExchange, selectedMarket);
            } else {
                generateReportForTheSelectedExchange();
            }
        }

        subMarketList = new ArrayList<TWComboItem>();
        TWComboModel marketModel = new TWComboModel(subMarketList);
        cmbSubMarkets = new JComboBox(marketModel);
        cmbSubMarkets.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedMarketIndex = cmbSubMarkets.getSelectedIndex();
                selectedMarket = ((TWComboItem) cmbSubMarkets.getSelectedItem()).getId();
                if (selectedMarketIndex != 0)
                    generateReportForTheSelectedMarket(selectedExchange, selectedMarket);
                else
                    generateReportForTheSelectedExchange();
            }
        });

        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary = new SymbolTradingSummaryPanel(this);
                scrollPane = new JScrollPane(tradingSummary);
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
//                topStocksGenerator = new TopStocksGenerator(this);
                mktSummary = new DailyMarketSummary(this); //, topStocksGenerator);
                scrollPane = new JScrollPane(mktSummary);
//                scrollPane.setBorder(BorderFactory.createLineBorder(Color.RED));
                break;
        }

        /*ExchangeStore.getSharedInstance().addExchangeListener(this);

        exchangeList = new ArrayList<TWComboItem>();
        TWComboModel comboModel = new TWComboModel(exchangeList);
        cmbExchanges = new JComboBox(comboModel);
        cmbExchanges.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedExcgIndex = cmbExchanges.getSelectedIndex();
                generateReportForTheSelectedExchange();
            }
        });*/

        btnRefresh = new JButton(Language.getString("REFRESH"));
        //btnPrint.setPreferredSize(new Dimension(70, 20));
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshReport();
            }
        });

        btnPrint = new JButton(Language.getString("PRINT"));
        //btnPrint.setPreferredSize(new Dimension(70, 20));
        btnPrint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                print();
            }
        });

        btnDate = new JButton(Language.getString("REPORT_DATE"));
        //btnPrint.setPreferredSize(new Dimension(70, 20));
        btnDate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setReportDate();
            }
        });

        btnToday = new JButton(Language.getString("CURRENT_REPORT"));
        //btnPrint.setPreferredSize(new Dimension(70, 20));
        btnToday.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                drawTodaysReport();
                drawTodaysReport();
            }
        });

        btnClose = new JButton(Language.getString("CLOSE"));
        //btnClose.setPreferredSize(new Dimension(70, 20));
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //System.exit(0);
                closeWindow();
            }
        });

        lblExchange = new JLabel(Language.getString("EXCHANGE"));
        lblLabel1 = new JLabel("");
        lblLabel1.setPreferredSize(new Dimension(30, 20));

        lblMarket = new JLabel(Language.getString("TOOLTIP_SUB_MARKET"));

        GUISettings.setSameSize(btnClose, btnPrint);

        buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 1));
        buttonPanel.add(lblExchange);
        buttonPanel.add(cmbExchanges);
        buttonPanel.add(lblMarket);
        buttonPanel.add(cmbSubMarkets);
        buttonPanel.add(lblLabel1);
        buttonPanel.add(btnRefresh);
        buttonPanel.add(btnPrint);
        buttonPanel.add(btnDate);
        buttonPanel.add(btnToday);
        buttonPanel.add(btnClose);

        fromDate = new DatePicker(this, true);
        fromDate.getCalendar().addDateSelectedListener(this);
        fromDate.getCalendar().setSelfDispose(false);

        this.getContentPane().add(scrollPane, BorderLayout.CENTER);
        this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        if (reportType == REPORT_DAILY_MARKET_SUMMARY) {
            lblMarket.setVisible(false);
            cmbSubMarkets.setVisible(false);
        }

        GUISettings.applyOrientation(this);
        setTitle(Language.getString("REPORTS"));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(this);
        showWindow();

        drawTodaysReport();
    }

    /* public static ReportGenerator getInstance(int type){
            if(self == null){
                self = new ReportGenerator(type);
            }
            return self;
        }
    */

    public static void refresh() {
        self.doLayout();
        System.out.println("refresh");
        self.scrollPane.doLayout();
    }

    private boolean populateMarkets() {

        if (reportType == REPORT_SYMBOL_TRADING_SUMMARY) {
            if ((ExchangeStore.getSharedInstance().getExchange(selectedExchange).hasSubMarkets()) && ExchangeStore.getSharedInstance().getExchange(selectedExchange).isUserSubMarketBreakdown()) {
                Market[] marArray = ExchangeStore.getSharedInstance().getExchange(selectedExchange).getSubMarkets();
                subMarketList.clear();
                subMarketList.add(new TWComboItem("*", Language.getString("ALL")));
                selectedMarketIndex = 0;
                for (int j = 0; j < marArray.length; j++) {
                    TWComboItem comboItem = new TWComboItem(marArray[j].getMarketID(), marArray[j].getDescription().trim());
                    subMarketList.add(comboItem);
                    if (marArray[j].isDefaultMarket()) {
                        selectedMarketIndex = j + 1;
                    }
                }
                cmbSubMarkets.setSelectedIndex(selectedMarketIndex);
                selectedMarket = ((TWComboItem) cmbSubMarkets.getSelectedItem()).getId();
//            selectedMarketIndex = 0;
                changeLayout(true);
                return true;
            } else {
                changeLayout(false);
                return false;
            }
        } else {
            if ((ExchangeStore.getSharedInstance().getExchange(selectedExchange).hasSubMarkets()) && ExchangeStore.getSharedInstance().getExchange(selectedExchange).isUserSubMarketBreakdown()) {
                Market[] marArray = ExchangeStore.getSharedInstance().getExchange(selectedExchange).getSubMarkets();
                subMarketList.clear();
                subMarketList.add(new TWComboItem("*", Language.getString("ALL")));
                selectedMarketIndex = 0;
                for (int j = 0; j < marArray.length; j++) {
                    TWComboItem comboItem = new TWComboItem(marArray[j].getMarketID(), marArray[j].getDescription().trim());
                    subMarketList.add(comboItem);
                    if (marArray[j].isDefaultMarket()) {
                        selectedMarketIndex = j + 1;
                    }
                }
                cmbSubMarkets.setSelectedIndex(selectedMarketIndex);
                selectedMarket = ((TWComboItem) cmbSubMarkets.getSelectedItem()).getId();
//            selectedMarketIndex = 0;
                changeLayout(true);
                return true;
            } else {
                changeLayout(false);
                return false;
            }
//            return false;
        }
    }

    private void changeLayout(boolean hasMarket) {
        if (hasMarket && (reportType == REPORT_SYMBOL_TRADING_SUMMARY)) {
            cmbSubMarkets.setVisible(true);
            lblMarket.setVisible(true);
//            buttonPanel.removeAll();
//            buttonPanel.invalidate();
//            buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 1));
//            buttonPanel.add(lblExchange);
//            buttonPanel.add(cmbExchanges);
//            buttonPanel.add(lblMarket);
//            buttonPanel.add(cmbSubMarkets);
//            buttonPanel.add(lblLabel1);
//            buttonPanel.add(btnRefresh);
//            buttonPanel.add(btnPrint);
//            buttonPanel.add(btnDate);
//            buttonPanel.add(btnToday);
//            buttonPanel.add(btnClose);
            buttonPanel.doLayout();
            buttonPanel.validate();
            buttonPanel.repaint();
        } else if (reportType == REPORT_DAILY_MARKET_SUMMARY) {
            cmbSubMarkets.setVisible(false);
            lblMarket.setVisible(false);
//            buttonPanel.removeAll();
//            buttonPanel.invalidate();
//            buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 1));
//            buttonPanel.add(lblExchange);
//            buttonPanel.add(cmbExchanges);
//            buttonPanel.add(lblMarket);
//            buttonPanel.add(cmbSubMarkets);
//            buttonPanel.add(lblLabel1);
//            buttonPanel.add(btnRefresh);
//            buttonPanel.add(btnPrint);
//            buttonPanel.add(btnDate);
//            buttonPanel.add(btnToday);
//            buttonPanel.add(btnClose);
            buttonPanel.doLayout();
            buttonPanel.validate();
            buttonPanel.repaint();
        } else {
            cmbSubMarkets.setVisible(false);
            lblMarket.setVisible(false);
//            buttonPanel.removeAll();
//            buttonPanel.invalidate();
//            buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 1));
//            buttonPanel.add(lblExchange);
//            buttonPanel.add(cmbExchanges);
//            buttonPanel.add(lblLabel1);
//            buttonPanel.add(btnRefresh);
//            buttonPanel.add(btnPrint);
//            buttonPanel.add(btnDate);
//            buttonPanel.add(btnToday);
//            buttonPanel.add(btnClose);
            buttonPanel.doLayout();
            buttonPanel.validate();
            buttonPanel.repaint();
        }
    }

    public void setReportDate() {
        Point location = btnDate.getLocation();
        SwingUtilities.convertPointToScreen(location, btnDate.getParent());
        location.translate(0, -(int) fromDate.getPreferredSize().getHeight());
        fromDate.setLocation(location);
        fromDate.showDialog();
    }

    public int getScrollBarWidth() {
        return scrollPane.getVerticalScrollBar().getPreferredSize().width;
    }

    /*public int getScrollbarWidth(){
        return getScrollBarWidth();
    }*/

    public void setViewPortSize(int height) {
        int width = Toolkit.getDefaultToolkit().getScreenSize().width;
        scrollPane.setPreferredSize(new Dimension(getScrollBarWidth(), height));
        scrollPane.updateUI();

        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.setPreferredSize(new Dimension(width - getScrollBarWidth() - 30, height)); //height-30));
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.setPreferredSize(new Dimension(width - (2 * getScrollBarWidth()), height)); //height-30));

                break;
        }

        this.doLayout();
    }

    public void showWindow() {
        int width = Toolkit.getDefaultToolkit().getScreenSize().width;
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;
        this.setSize(width, height - 30); //810, 555);
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.setPreferredSize(new Dimension(width - getScrollBarWidth() - 30, 2500)); //height-30));
                showHistoryButtons(true);
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.setPreferredSize(new Dimension(width - getScrollBarWidth() - 100, 1250)); //height-30));
                showHistoryButtons(true);
                break;
        }
        addNewExchanges();
        //this.setResizable(false);
        this.setVisible(true);
        createPreview();
        //this.repaint();
    }

    private void showHistoryButtons(boolean status) {
        btnDate.setVisible(status);
        btnToday.setVisible(status);
    }

    private void createPreview() {
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.initLayoutChangesForReport();
                //tradingSummary.repaint();
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.initReportLayout();
                break;
        }
    }

    public void print() {
        createPreview();
        PrinterJob job = PrinterJob.getPrinterJob();
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                job.setPrintable(tradingSummary, Settings.getPageFormat());
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                job.setPrintable(mktSummary, Settings.getPageFormat());
                break;
        }
        if (job.printDialog()) {
            try {
                job.print();
            } catch (Exception ex) {
                System.out.println(ex);
            }
            this.setVisible(false);
            closeWindow();
        } else {
            switch (reportType) {
                case REPORT_SYMBOL_TRADING_SUMMARY:
                    tradingSummary.setCursor(Cursor.getDefaultCursor());
                    break;
                case REPORT_DAILY_MARKET_SUMMARY:
                    mktSummary.setCursor(Cursor.getDefaultCursor());
                    break;
            }
        }

//        this.setVisible(false);
//        closeWindow();
    }

    private void closeWindow() {
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.setCursor(Cursor.getDefaultCursor());
                tradingSummary.disposeReport();
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.setCursor(Cursor.getDefaultCursor());
                mktSummary.disposeReport();
//                topStocksGenerator.disposeObjects();
                break;
        }
        this.setVisible(false);
        ExchangeStore.getSharedInstance().removeExchangeListener(this);
        this.dispose();
    }

    private void refreshReport() {
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.repaintReport();
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.repaintReport();
                break;
        }
    }

    /**
     * Invoked when the component's size changes.
     */
    public void componentResized(ComponentEvent e) {
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.initLayoutChangesForReport();
//                tradingSummary.setPreferredSize(this.getSize());
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.initReportLayout();
                break;
        }
    }

    /**
     * Invoked when the component's position changes.
     */
    public void componentMoved(ComponentEvent e) {
    }

    /**
     * Invoked when the component has been made visible.
     */
    public void componentShown(ComponentEvent e) {
    }

    /**
     * Invoked when the component has been made invisible.
     */
    public void componentHidden(ComponentEvent e) {
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        cal = Calendar.getInstance();
        cal.set(iYear, iMonth, iDay);
        fromDate.hide();
        format = new SimpleDateFormat("yyyyMMdd");
        Object[] objects = {new JLabel("")};
        JOptionPane waitMsg = new JOptionPane(Language.getString("MSB_GENERATING_REPORT"), JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, objects);
        waitDialog = waitMsg.createDialog(this, Language.getString("REPORTS"));
        //((JButton)waitMsg.getOptions()[0]).setVisible(false);
        waitDialog.setModal(false);
        waitDialog.show();
        Thread t = new Thread(this, "ReportGenerator");
        t.start();
    }

    private void drawTodaysReport() {
        try {
            switch (reportType) {
                case REPORT_SYMBOL_TRADING_SUMMARY:
                    tradingSummary.drawReport(null);
                    break;
                case REPORT_DAILY_MARKET_SUMMARY:
                    // bandu - Need to implement date change logic
                    mktSummary.drawReport(null);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                try {
                    waitDialog.repaint();
                    tradingSummary.drawReport(format.format(cal.getTime()));
                    waitDialog.dispose();

                    System.out.println("Report finished");
                    tradingSummary.SetFrameNotAdjusted();
                    tradingSummary.repaintReport();
                } catch (Exception e) {
                    waitDialog.dispose();
                    tradingSummary.repaintReport();
//                    new ShowMessage(Language.getString("MSG_NO_DATA"), "E");
                    e.printStackTrace();
                }
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                try {
                    waitDialog.repaint();
                    mktSummary.drawReport(format.format(cal.getTime()));
                    waitDialog.dispose();

                    System.out.println("Report finished");
                    mktSummary.setFrameNotAdjusted();
                    mktSummary.repaintReport();
                } catch (Exception e) {
                    waitDialog.dispose();
                    mktSummary.repaintReport();
//                    new ShowMessage(Language.getString("MSG_NO_DATA"), "E");
                    e.printStackTrace();
                }
                break;
        }
    }

    public void exchangeAdded(Exchange exchange) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        // Populate the Exchanges Vector
        exchangeList.clear();
        addNewExchanges();
        cmbExchanges.updateUI();
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    private void addNewExchanges() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                exchangeList.add(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
            }
            exchange = null;
        }
        Collections.sort(exchangeList);
        cmbExchanges.setSelectedIndex(selectedExcgIndex);
        selectedExchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
        if (populateMarkets()) {
            generateReportForTheSelectedMarket(selectedExchange, selectedMarket);
        } else {
            generateReportForTheSelectedExchange();
        }
//        populateMarkets();
//        generateReportForTheSelectedExchange();
        exchanges = null;
    }

    private void generateReportForTheSelectedMarket(String exchangeID, String marketID) {
        Market market = ExchangeStore.getSharedInstance().getMarket(exchangeID, marketID);

        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.setExchange(ExchangeStore.getSharedInstance().getExchange(exchangeID));
                tradingSummary.setMarket(market);
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.setExchange(ExchangeStore.getSharedInstance().getExchange(exchangeID));
//                mktSummary.setMarket(market);
                break;
        }
        market = null;
    }

    private void generateReportForTheSelectedExchange() {
        String exchangeSymbol = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeSymbol);

        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.setExchange(exchange);
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.setExchange(exchange);
                break;
        }

        exchange = null;
    }

    public Exchange getSelectedExchange() {
        if (cmbExchanges.getSelectedItem() == null) {
            cmbExchanges.setSelectedIndex(0);
        }
        String exchangeSymbol = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeSymbol);
        return exchange;
    }

    public boolean isWindowVisible() {
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                return tradingSummary.isVisible();
            case REPORT_DAILY_MARKET_SUMMARY:
                return mktSummary.isVisible();
        }
        return false;
    }

    public void repaintReport() {
        switch (reportType) {
            case REPORT_SYMBOL_TRADING_SUMMARY:
                tradingSummary.SetFrameNotAdjusted();
                tradingSummary.repaint();
                break;
            case REPORT_DAILY_MARKET_SUMMARY:
                mktSummary.setFrameNotAdjusted();
                try {
                    mktSummary.initReportDataStore();
                    mktSummary.initReportHeader();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mktSummary.repaint();
                break;
        }
        doLayout();
    }

    public void windowOpened(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {
//        System.out.println("exit 1243 -----------");

    }

    public void windowClosed(WindowEvent e) {
        try {
            if (this.reportType == REPORT_SYMBOL_TRADING_SUMMARY) {
                ReportManager.clearSymbolTradingReport();
            } else if (this.reportType == REPORT_DAILY_MARKET_SUMMARY) {
                ReportManager.clearDailyMarketReport();
            }
            exchangeList.clear();
            self = null;
            if (tradingSummary != null)
                tradingSummary.disposeReport();
            tradingSummary = null;
            if (mktSummary != null)
                mktSummary.disposeReport();
            mktSummary = null;
            if (waitDialog != null)
                waitDialog.dispose();
            waitDialog = null;
            if (cal != null) {
                cal.clear();
                cal = null;
            }
        } catch (Exception e1) {
            //e1.printStackTrace();
        }
    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {

    }

}