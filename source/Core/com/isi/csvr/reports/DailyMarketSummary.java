package com.isi.csvr.reports;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.Sector;
import com.isi.csvr.datastore.SectorStore;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class DailyMarketSummary extends JPanel
        implements Printable {

    private final int rightMargin = 5;
    private final int topMargin = 5;
    private TWDateFormat openDateFormat = new TWDateFormat("yyyy/MM/dd HH:mm:ss");
    private TWDateFormat closedDateFormat = new TWDateFormat("yyyyMMdd");
    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
    private TWDecimalFormat oChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    private int panelWidth = 800;   // In Pixels
    private int panelHeight = 500;   // In Pixels
    private int leftMargin = 5;
    private int headerHeight = 82; //100; //110; // 120
    private int headingHeight = 0;
    private int rowHeight = 0;

    private String sHeading;
    private int[] columnWidths1;
    private int[] columnWidths2;
    private int[] columnWidths3;
    private int totalColumnWidth3;
    private int[] colPercents1 = {25, 15, 17, 9, 8, 8, 8, 10};
    private int[] colPercents2 = {25, 15, 12, 12, 12, 12, 12};
    private int[] colPercents3 = {40, 17, 18, 25};
    private String[] header;
    private int[] alignments1 = {
            ReportUtilities.LEFT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT,
            ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT,
            ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT};
    private int[] alignments2 = {
            ReportUtilities.LEFT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT,
            ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT,
            ReportUtilities.RIGHT, ReportUtilities.RIGHT};
    private int[] alignments3 = {
            ReportUtilities.LEFT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT};

    private MarketSummaryReportDataStore marketSummaryReportStore;
    private String reportDate = null;
    private Exchange curExchange;
    private String[] equitySymbols;
    private String[] indexSymbols;
    private SectorObject[] sectorObjects;
    private TopStocksGenerator topStocksGenerator;
    private ReportGenerator parentWindow;
    private boolean isFrameNotAdjusted;

    public DailyMarketSummary(ReportGenerator parentWindow) { //, TopStocksGenerator topStocksGenerator) {
        this.parentWindow = parentWindow;
        marketSummaryReportStore = new MarketSummaryReportDataStore(parentWindow);
        topStocksGenerator = new TopStocksGenerator(parentWindow, marketSummaryReportStore);
//        this.setBackground(Color.BLUE);
//        this.setBorder(BorderFactory.createLineBorder(Color.GREEN));
        this.setOpaque(true);

//        this.topStocksGenerator = topStocksGenerator;
    }

    public void repaintReport() {
        topStocksGenerator.generateTopStocks();
        sHeading = null;
        initReportHeader();
        this.repaint();
    }

    public void initReportLayout() {
        panelWidth = this.getSize().width - (parentWindow.getScrollBarWidth());// - ReportGenerator.getScrollBarWidth();
        panelHeight = this.getSize().height;

        // Calculate Column widths for the tables
        columnWidths1 = calculateColumnWidths(panelWidth - 5, columnWidths1, colPercents1);
        columnWidths2 = calculateColumnWidths(panelWidth - 5, columnWidths2, colPercents2);
        columnWidths3 = calculateColumnWidths(panelWidth / 2, columnWidths3, colPercents3);
        for (int i : columnWidths3) {
            totalColumnWidth3 += i + 5;
        }
        totalColumnWidth3 -= 5;
        // Calculate Column Alignments for the tables
        alignments1 = adjustAlignments(alignments1);
        alignments2 = adjustAlignments(alignments2);
        alignments3 = adjustAlignments(alignments3);
    }

    public void setExchange(Exchange excg) {
        curExchange = excg;
        isFrameNotAdjusted = true;

        try {
            marketSummaryReportStore.reload();
        } catch (Exception e) {
//            e.printStackTrace();
        }

        try {
            initReportDataStore();
            initReportHeader();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.repaintReport();
    }

    public void initReportHeader() {
        if (reportDate == null) {
            if (curExchange.getMarketStatus() == Constants.CLOSED) {
                TWDateFormat clientTimeFormatter = new TWDateFormat(Language.getString("MARKET_TIME_CLOSE"));
                sHeading = curExchange.getDescription() + " - " + Language.getString("DAILY_MKT_SUMMARY") + " - " +
                        clientTimeFormatter.format(new Date(curExchange.getZoneAdjustedMarketTime()));
            } else {
                sHeading = curExchange.getDescription() + " - " + Language.getString("DAILY_MKT_SUMMARY") + " - " +
                        Client.getFormattedmarketTime(curExchange.getZoneAdjustedMarketTime());
            }
        } else {
            try {
                Date dt = closedDateFormat.parse(reportDate);
                TWDateFormat clientTimeFormatter = new TWDateFormat(Language.getString("MARKET_TIME_CLOSE"));
                sHeading = curExchange.getDescription() + " - " + Language.getString("DAILY_MKT_SUMMARY") + " - " +
                        clientTimeFormatter.format(dt);
                dt = null;
                clientTimeFormatter = null;
            } catch (Exception e) {
                sHeading = curExchange.getDescription() + " - " + Language.getString("DAILY_MKT_SUMMARY");
            }
        }
    }

    public void initReportDataStore() {
        int i2 = 0;
        String sIndexID = null;

        if (curExchange == null) {
            try {
                curExchange = parentWindow.getSelectedExchange(); //ExchangeStore.getSharedInstance().getDefaultExchange();
            } catch (Exception e) {
                return;
            }
        }
        initReportHeader();

        ArrayList<Sector> sectorObjs = new ArrayList<Sector>();
        Enumeration<Sector> sectorEnum = SectorStore.getSharedInstance().getSectors(curExchange.getSymbol());
        while (sectorEnum.hasMoreElements()) {
            sectorObjs.add(sectorEnum.nextElement());
        }
        Collections.sort(sectorObjs);
        sectorObjects = new SectorObject[sectorObjs.size()];
        int index = 0;
        for (Sector sector : sectorObjs) {
            sectorObjects[index] = new SectorObject(sector.getId(), sector.getDescription());
            index++;
        }

        Stock stock = null;
        ArrayList<String> arrayOfEquities = new ArrayList<String>();
        ArrayList<String> arrayOfIndexes = new ArrayList<String>();
        Enumeration<String> enume = DataStore.getSharedInstance().getSymbols(curExchange.getSymbol());
        String data = null;
        String symbol = null;
        int instrument = -1;
        while (enume.hasMoreElements()) {
            data = enume.nextElement();
            symbol = SharedMethods.getSymbolFromExchangeKey(data);
            instrument = SharedMethods.getInstrumentFromExchangeKey(data);
            stock = DataStore.getSharedInstance().getStockObject(curExchange.getSymbol(), symbol, instrument);
//            stock = marketSummaryReportStore.getStockObject(SharedMethods.getKey(curExchange.getSymbol(), enume.nextElement()));
            if ((stock != null) && (stock.getInstrumentType() == Meta.INSTRUMENT_INDEX)) {
                //arrayOfIndexes.add(stock.getKey());
//                arrayOfIndexes.add(new SectorObject(stock.getKey()));
            } else if (stock != null) {
                //arrayOfEquities.add(stock.getKey());
                if (curExchange.hasSubMarkets() && curExchange.isUserSubMarketBreakdown()) {
                    if (stock.getMarketID().equals(curExchange.getDefaultMarket().getMarketID())) {
                        arrayOfEquities.add(stock.getKey());
                    }
                } else {
                    arrayOfEquities.add(stock.getKey());
                }
            }

            stock = null;
        }
        //////////////         Change done by chandika, for Kevins crazy requirement         //////////////////////
        SmartProperties exgIndexes = new SmartProperties(Meta.DS, true);
        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + curExchange.getSymbol() + "/index_" + Language.getSelectedLanguage() + ".msf";
        try {
            File f = new File(sLangSpecFileName);
            boolean bIsLangSpec = f.exists();
            if (bIsLangSpec) {
                exgIndexes.loadCompressed(sLangSpecFileName);
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        Enumeration indexEnum = exgIndexes.keys();
        while (indexEnum.hasMoreElements()) {
            String skey = (String) indexEnum.nextElement();
            try {
                String keyR = SharedMethods.getKey(curExchange.getSymbol(), skey, Meta.INSTRUMENT_INDEX);
                arrayOfIndexes.add(keyR);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        //change over
        equitySymbols = arrayOfEquities.toArray(new String[0]);
        indexSymbols = arrayOfIndexes.toArray(new String[0]);

        enume = null;
        arrayOfEquities = null;
        arrayOfIndexes = null;

        //Arrays.sort(equitySymbols, new SymbolSortComparator());
        // Arrays.sort(sectorObjects, new SectorObjectComparator());


        for (int i = 0; i < equitySymbols.length; i++) {
            stock = marketSummaryReportStore.getStockObject(equitySymbols[i]);
            if (stock == null) {
                continue;
            }
            for (int j = 0; j < sectorObjects.length; j++) {
//                System.out.println("stock.getSectorCode() = " + stock.getSectorCode() + ", sectorObjects[j].getSymbol() = " + sectorObjects[j].getSymbol());
                if (stock.getSectorCode().equalsIgnoreCase(sectorObjects[j].getSectorID())) {
                    if (reportDate == null) {
                        if (stock.getChange() > 0)
                            sectorObjects[j].setUps(1);
                        else if (stock.getChange() < 0)
                            sectorObjects[j].setDowns(1);
                        else if (stock.getLastTradeValue() > 0)
                            sectorObjects[j].setNoChange(1);
                        sectorObjects[j].addTurnover(stock.getTurnover());
                        sectorObjects[j].addVolume(stock.getVolume());
                    } else {
//                        System.out.println("stock.getSectorCode() = " + stock.getSectorCode() + ", sectorObjects[j].getSymbol() = " + sectorObjects[j].getSymbol());
                        if (stock.getChange() > 0)
                            sectorObjects[j].setUps(1);
                        else if (stock.getChange() < 0)
                            sectorObjects[j].setDowns(1);
                        else if (stock.getPreviousClosed() > 0)
                            sectorObjects[j].setNoChange(1);
                        sectorObjects[j].addTurnover(stock.getTurnover());
                        sectorObjects[j].addVolume(stock.getVolume());
                    }

                    sectorObjects[j].setNoOfTrades(stock.getNoOfTrades());
                    if (stock.getNoOfTrades() > 0)
                        sectorObjects[j].setTraded(1);
                    break;
                }
            }
            stock = null;
        }

//        topStocksGenerator.clearStores();
        topStocksGenerator.setEquitySymbols(equitySymbols);

        enume = null;
        stock = null;
    }

    private int[] adjustAlignments(int[] alignments) {
        int[] newAlignments = new int[alignments.length];
        if (!Language.isLTR()) {
            for (int i = 0; i < alignments.length; i++) {
                if (alignments[i] == ReportUtilities.LEFT)
                    alignments[i] = ReportUtilities.RIGHT;
//                else if (alignments[i] == ReportUtilities.RIGHT)
//                    alignments[i] = ReportUtilities.LEFT;

                newAlignments[alignments.length - 1 - i] = alignments[i];
            }
            return newAlignments;
        }
        return alignments;
    }

    private int[] calculateColumnWidths(int width, int[] columnWidths, int[] colPercents) {
        int tableWidth = width - leftMargin - rightMargin;
        int[] newColWidths = new int[colPercents.length];
        for (int i = 0; i < colPercents.length; i++) {
            if (Language.isLTR())
                newColWidths[i] = tableWidth * colPercents[i] / 100;
            else
                newColWidths[i] = tableWidth * colPercents[colPercents.length - 1 - i] / 100;
        }
        return newColWidths;
    }

    public void drawReport(String reportDate) throws Exception {
        isFrameNotAdjusted = true;
        this.reportDate = reportDate;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
        if (format1.format(new Date(curExchange.getMarketDate())).equals(reportDate)) {
            this.reportDate = null;
            marketSummaryReportStore.setDate(null);
            try {
                drawReport(null); /*To fixed the bug, when current date select from calender . this is how it was fixed when click
                                        current date.Dont know why . 2009.04.29 - Shanika  */
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            marketSummaryReportStore.setDate(reportDate);
        }
        //        marketSummaryReportStore.setDate(reportDate);
        initReportDataStore();
        marketSummaryReportStore.reload();
        //initReportHeader();
        repaint();
    }

    public void paint(Graphics g) {
        try {
            drawReport(g, false);
        } catch (Exception ex) {
        }
    }


    public void setPreferredSize(Dimension preferredSize) {
//        SharedMethods.printLine("set Preffered Size width = "+preferredSize.getWidth() +" , height = "+preferredSize.getHeight(),true);
        super.setPreferredSize(preferredSize);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void update(Graphics g) {
        this.paint(g);
    }

    private void drawReport(Graphics g, boolean isPrinting) {
        int curYPos = 0;
        int curXPos = 0;
        int curRowNumber = 0;
        int status = ReportUtilities.NOCHANGE;
        Graphics2D g2D = null;
        boolean isEven = false;
        double curElemPrice = 0;
        int width = (int) getSize().getWidth();
        int height = (int) getSize().getHeight();

        try {
            g2D = (Graphics2D) g;
            g2D.setColor(Color.white); // new Color(200, 200, 200));
            g2D.fillRect(0, 0, width, height);
            g2D.setColor(Color.black); // new Color(200, 200, 200));

            if (reportDate == null) {
                marketSummaryReportStore.volume = curExchange.volume;
                marketSummaryReportStore.turnover = curExchange.turnover;
                marketSummaryReportStore.noOfDown = curExchange.noOfDown;
                marketSummaryReportStore.noOfNoChange = curExchange.noOfNoChange;
                marketSummaryReportStore.noOfUps = curExchange.noOfUps;
                marketSummaryReportStore.symbolsTraded = curExchange.symbolsTraded;
                marketSummaryReportStore.noOfTrades = curExchange.noOfTrades;
            }
            oPriceFormat = getPriceFormatter(curExchange.getPriceDecimalPlaces());
            if (ReportUtilities.isCustomizedVersion())
                headerHeight = 80;

            curYPos = topMargin;
            ReportUtilities.drawHeader(g2D, leftMargin, curYPos, width - leftMargin - rightMargin - (parentWindow.getScrollBarWidth()), headerHeight, isPrinting);
//            ReportUtilities.drawHeader(g2D, leftMargin, curYPos, width - 2 * leftMargin, headerHeight, isPrinting);
            curYPos += headerHeight + topMargin;

            // Draws the Main Title
            if (isPrinting)
                headingHeight = ReportUtilities.drawHeading(sHeading, g2D, leftMargin + width / 2, curYPos + 5, ReportUtilities.FONT_PRINT_LARGEST);
            else
                headingHeight = ReportUtilities.drawHeading(sHeading, g2D, leftMargin + width / 2, curYPos + 5, ReportUtilities.FONT_LARGEST);
            curYPos += headingHeight + topMargin;

            // ###########################################################################################
            //          DRAW TOP SECTOR SUMMARY TABLE
            // ###########################################################################################
            // Draws the Sector Table Title
            header = Language.getList("RPT_HEADER_SECTOR_SUMMARY");
            if (isPrinting)
                headingHeight = ReportUtilities.drawHeading(Language.getString("SECTOR_SUMMARY"), g2D, leftMargin + width / 2, curYPos + 5, ReportUtilities.FONT_PRINT_LARGE);
            else
                headingHeight = ReportUtilities.drawHeading(Language.getString("SECTOR_SUMMARY"), g2D, leftMargin + width / 2, curYPos + 5, ReportUtilities.FONT_LARGE);
            curYPos += headingHeight;

            curYPos += ReportUtilities.drawTableHeader(columnWidths1, header, g2D, leftMargin, curYPos, width - rightMargin, isPrinting);
            header = null;
            if (isPrinting)
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM));
            else
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));

            FontMetrics fm = g2D.getFontMetrics();
            int counter = 0;
            int colCounter = 0;
            rowHeight = fm.getHeight() + fm.getDescent() + 2; // + 5; // + fm.getAscent() + fm.getDescent();
            //Stock   index       = null;

            isEven = true;
            if (Language.isLTR()) {
                curXPos = ReportUtilities.drawTableColumn(curExchange.getDescription(), alignments1[colCounter], g2D,
                        leftMargin, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.volume), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(marketSummaryReportStore.turnover), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.noOfTrades), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.symbolsTraded), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.noOfUps), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.noOfDown), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.noOfNoChange), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
            } else {
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.noOfNoChange), alignments1[colCounter], g2D,
                        leftMargin, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.noOfDown), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.noOfUps), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.symbolsTraded), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.noOfTrades), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(marketSummaryReportStore.turnover), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(marketSummaryReportStore.volume), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                curXPos = ReportUtilities.drawTableColumn(curExchange.getDescription(), alignments1[colCounter], g2D,
                        curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
            }

            curYPos += rowHeight;
            colCounter = 0;
            curRowNumber = 0;
            for (int indexPos = 0; indexPos < sectorObjects.length; indexPos++) {
                //index = marketSummaryReportStore.getStockObject(sectorObjects[indexPos].getSectorID());
//                index = DataStore.getSharedInstance().getStockObject(sectorObjects[indexPos].getSectorID());

                //if (index == null)
                //    continue;

                isEven = ((curRowNumber % 2) == 0);
                curRowNumber++;

                if (Language.isLTR()) {
                    curXPos = ReportUtilities.drawTableColumn(sectorObjects[indexPos].getDescription(), alignments1[colCounter], g2D,
                            leftMargin, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getVolume()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(sectorObjects[indexPos].getTurnover()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getNoOfTrades()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getTraded()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getUps()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getDowns()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getNoChange()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                } else {
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getNoChange()), alignments1[colCounter], g2D,
                            leftMargin, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getDowns()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getUps()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getTraded()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getNoOfTrades()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(sectorObjects[indexPos].getTurnover()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(sectorObjects[indexPos].getVolume()), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(sectorObjects[indexPos].getDescription(), alignments1[colCounter], g2D,
                            curXPos, curYPos, columnWidths1[colCounter++], rowHeight, isEven, isPrinting);
                }
                counter++;
                curYPos += rowHeight;
                colCounter = 0;
                //index       = null;
            }
            //enum = null;
            counter = 0;

            // ###########################################################################################
            //          DRAW INDEX SUMMARY TABLE
            // ###########################################################################################
            Stock index = null;
            curYPos += topMargin * 4;
            // Draws the Sector Table Title
            headingHeight = ReportUtilities.drawHeading(Language.getString("INDEX_SUMMARY"), g2D, leftMargin + width / 2, curYPos + 5, ReportUtilities.FONT_LARGE);
            curYPos += headingHeight + topMargin;

            header = Language.getList("INDEX_SUMMARY_REPORT_COLS");
            curYPos += ReportUtilities.drawTableHeader(columnWidths2, header, g2D, leftMargin, curYPos, width, isPrinting);
            header = null;
            if (isPrinting)
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM));
            else
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));
            //enum    = IndexStore.getIndexIDs();
            //while (enum.hasMoreElements()) {

            curRowNumber = 0;
            for (int indID = 0; indID < indexSymbols.length; indID++) {
                index = marketSummaryReportStore.getStockObject(indexSymbols[indID]);
//                index = DataStore.getSharedInstance().getStockObject(sectorObjects[indID].getSectorID());
                if (index == null) {
                    continue;
                }
                oPriceFormat = getPriceFormatter(index.getDecimalCount());
                oChangeFormat = getPriceFormatter(index.getDecimalCount());
                oPChangeFormat = getPriceFormatter(index.getDecimalCount());

                if (reportDate == null) {
                    curElemPrice = index.getLastTradeValue();
                } else {
                    curElemPrice = index.getTodaysClose();
                }

                isEven = ((curRowNumber % 2) == 0);
                curRowNumber++;

                if (Language.isLTR()) {
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getCompanyName(index.getKey()).trim(), alignments2[colCounter], g2D,
                            leftMargin, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);

                    if (index.getChange() > 0)
                        status = ReportUtilities.UP;
                    else if (index.getChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(index.getChange()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, status, isEven, isPrinting);

                    if (index.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (index.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oPChangeFormat.format(index.getPercentChange()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(index.getHigh()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(index.getLow()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(index.getPreviousClosed()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);
                } else {
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(index.getPreviousClosed()), alignments2[colCounter], g2D,
                            leftMargin, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(index.getLow()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(index.getHigh()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);

                    if (index.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (index.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oPChangeFormat.format(index.getPercentChange()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, status, isEven, isPrinting);

                    if (index.getChange() > 0)
                        status = ReportUtilities.UP;
                    else if (index.getChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(index.getChange()), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getCompanyName(index.getKey()).trim(), alignments2[colCounter], g2D,
                            curXPos, curYPos, columnWidths2[colCounter++], rowHeight, isEven, isPrinting);
                }
                counter++;
                curYPos += rowHeight;
                colCounter = 0;
                index = null;
            }
            //enum = null;
            counter = 0;

            // ###########################################################################################
            //          DRAW TOP GAINERS TABLE
            // ###########################################################################################
            curYPos += topMargin * 4;
            int topYofCurTable = curYPos;
            // Draws the Sector Table Title
            if (isPrinting)
                headingHeight = ReportUtilities.drawHeading(Language.getString("TOP_GAINERS"), g2D, leftMargin + width / 4, curYPos + 5, ReportUtilities.FONT_PRINT_LARGE);
            else
                headingHeight = ReportUtilities.drawHeading(Language.getString("TOP_GAINERS"), g2D, leftMargin + width / 4, curYPos + 5, ReportUtilities.FONT_LARGE);
            curYPos += headingHeight + topMargin;

            header = Language.getList("TOP_GAINERS_REPORT_COLS");
            if (isPrinting) {
                curYPos += ReportUtilities.drawTableHeader(columnWidths3, header, g2D, leftMargin, curYPos, width / 2 - 2 * leftMargin, isPrinting);
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM));
            } else {
                curYPos += ReportUtilities.drawTableHeader(columnWidths3, header, g2D, leftMargin, curYPos, width / 2 - 2 * leftMargin, isPrinting);
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));
            }

            curRowNumber = 0;
            header = null;
            colCounter = 0;
            Stock stock = null;
            String[] symbols = topStocksGenerator.getListTopGainers(); //todo  Client.getInstance().getTopGainers();
//            System.out.println("#### TOP_GAINERS count " + symbols.length);
            for (int iS = 0; iS < symbols.length; iS++) {
                stock = marketSummaryReportStore.getStockObject(symbols[iS]);
//                stock = DataStore.getSharedInstance().getStockObject(symbols[iS]);
                if (stock == null) {
//                    System.out.println("Stock is NULL1 " + symbols[iS]);
                    continue;
                }
                oPriceFormat = getPriceFormatter(stock.getDecimalCount());
                oChangeFormat = getPriceFormatter(stock.getDecimalCount());
                oPChangeFormat = getPriceFormatter(stock.getDecimalCount());
                isEven = ((curRowNumber % 2) == 0);
                curRowNumber++;

                if (reportDate == null) {
                    curElemPrice = stock.getLastTradeValue();
                } else {
                    curElemPrice = stock.getPreviousClosed();
                }
                if (Language.isLTR()) {
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments3[colCounter], g2D,
                            leftMargin, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);

                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getPercentChange()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getVolume()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                } else {
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getVolume()), alignments3[colCounter], g2D,
                            leftMargin, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getPercentChange()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                }
                stock = null;
                counter++;
                curYPos += rowHeight;
                colCounter = 0;

                if (iS == 4)
                    break;
            }
            symbols = null;
            int lastYofPrevTable = curYPos;

            // ###########################################################################################
            //          DRAW TOP LOSERS TABLE
            // ###########################################################################################
            curYPos = topYofCurTable;
            // Draws the Sector Table Title
            if (isPrinting)
                headingHeight = ReportUtilities.drawHeading(Language.getString("TOP_LOSERS"), g2D, (int) (leftMargin + width * 0.75), curYPos + 5, ReportUtilities.FONT_PRINT_LARGE);
            else
                headingHeight = ReportUtilities.drawHeading(Language.getString("TOP_LOSERS"), g2D, (int) (leftMargin + width * 0.75), curYPos + 5, ReportUtilities.FONT_LARGE);
            curYPos += headingHeight + topMargin;

            header = Language.getList("RPT_HEADER_TOP_LOSERS");
            if (isPrinting) {
                curYPos += ReportUtilities.drawTableHeader(columnWidths3, header, g2D, leftMargin + width / 2, curYPos, width / 2 - 2 * leftMargin, isPrinting);
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM));
            } else {
                curYPos += ReportUtilities.drawTableHeader(columnWidths3, header, g2D, leftMargin + width / 2, curYPos, width / 2 - 2 * leftMargin, isPrinting);
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));
            }
            header = null;

            curRowNumber = 0;
            //curXPos = leftMargin + width / 2;
            symbols = topStocksGenerator.getListTopLosers(); //todo Client.getInstance().getTopLosers();
//            System.out.println("#### TOP_LOSERS count " + symbols.length);

            for (int iS = 0; iS < symbols.length; iS++) {
                stock = marketSummaryReportStore.getStockObject(symbols[iS]);
//                stock = DataStore.getSharedInstance().getStockObject(symbols[iS]);
                if (stock == null) {
//                    System.out.println("Stock is NULL2 " + symbols[iS]);
                    continue;
                }
                oPriceFormat = getPriceFormatter(stock.getDecimalCount());
                oChangeFormat = getPriceFormatter(stock.getDecimalCount());
                oPChangeFormat = getPriceFormatter(stock.getDecimalCount());

                isEven = ((curRowNumber % 2) == 0);
                curRowNumber++;

                if (reportDate == null) {
                    curElemPrice = stock.getLastTradeValue();
                } else {
                    curElemPrice = stock.getPreviousClosed();
                }

                if (Language.isLTR()) {
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments3[colCounter], g2D,
                            leftMargin + width / 2, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getPercentChange()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getVolume()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                } else {
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getVolume()), alignments3[colCounter], g2D,
                            leftMargin + width / 2, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getPercentChange()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                }
                stock = null;
                counter++;
                curYPos += rowHeight;
                colCounter = 0;

                if (iS == 4)
                    break;
            }
            symbols = null;

            // If the Top Gainers table have more entries than the Top Losers table, need to take the highest value
            if (curYPos < lastYofPrevTable) {
                curYPos = lastYofPrevTable;
            }
            // ###########################################################################################
            //          DRAW MOST ACTIVE BY VOLUME TABLE
            // ###########################################################################################
            curYPos += topMargin * 4;
            topYofCurTable = curYPos;
            // Draws the Sector Table Title
            if (isPrinting)
                headingHeight = ReportUtilities.drawHeading(Language.getString("MOST_ACTIVE_BY_VOLUME"), g2D, leftMargin + width / 4, curYPos + 5, ReportUtilities.FONT_PRINT_LARGE);
            else
                headingHeight = ReportUtilities.drawHeading(Language.getString("MOST_ACTIVE_BY_VOLUME"), g2D, leftMargin + width / 4, curYPos + 5, ReportUtilities.FONT_LARGE);
            curYPos += headingHeight + topMargin;

            header = Language.getList("RPT_HEADER_MOST_VOLUMES");
            if (isPrinting) {
                curYPos += ReportUtilities.drawTableHeader(columnWidths3, header, g2D, leftMargin, curYPos, width / 2 - 2 * leftMargin, isPrinting);
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM));
            } else {
                curYPos += ReportUtilities.drawTableHeader(columnWidths3, header, g2D, leftMargin, curYPos, width / 2 - 2 * leftMargin, isPrinting);
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));
            }
            header = null;

            curRowNumber = 0;
            colCounter = 0;
            symbols = topStocksGenerator.getListMostActiveByVolume();  //todo Client.getInstance().getMostActiveByVolume();
//            System.out.println("#### MOST_ACTIVE_BY_VOLUME count " + symbols.length);
            for (int iS = 0; iS < symbols.length; iS++) {
                stock = marketSummaryReportStore.getStockObject(symbols[iS]);
//                stock = DataStore.getSharedInstance().getStockObject(symbols[iS]);
                if (stock == null) {
//                    System.out.println("Stock is NULL3 " + symbols[iS]);
                    continue;
                }
                oPriceFormat = getPriceFormatter(stock.getDecimalCount());
                oChangeFormat = getPriceFormatter(stock.getDecimalCount());
                oPChangeFormat = getPriceFormatter(stock.getDecimalCount());

                isEven = ((curRowNumber % 2) == 0);
                curRowNumber++;

                if (reportDate == null) {
                    curElemPrice = stock.getLastTradeValue();
                } else {
                    curElemPrice = stock.getPreviousClosed();
                }

                if (Language.isLTR()) {
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments3[colCounter], g2D,
                            leftMargin, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getPercentChange()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getVolume()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                } else {
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getVolume()), alignments3[colCounter], g2D,
                            leftMargin, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getPercentChange()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                }
                stock = null;
                counter++;
                curYPos += rowHeight;
                colCounter = 0;

                if (iS == 4)
                    break;
            }
            symbols = null;
            lastYofPrevTable = curYPos;

            // ###########################################################################################
            //          DRAW MOST ACTIVE BY NO OF TRADES TABLE
            // ###########################################################################################
            curYPos = topYofCurTable;
            // Draws the Sector Table Title
            if (isPrinting)
                headingHeight = ReportUtilities.drawHeading(Language.getString("MOST_ACTIVE_BY_NO_TRADES"), g2D, (int) (leftMargin + width * 0.75), curYPos + 5, ReportUtilities.FONT_PRINT_LARGE);
            else
                headingHeight = ReportUtilities.drawHeading(Language.getString("MOST_ACTIVE_BY_NO_TRADES"), g2D, (int) (leftMargin + width * 0.75), curYPos + 5, ReportUtilities.FONT_LARGE);
            curYPos += headingHeight + topMargin;

            header = Language.getList("MOST_ACT_BY_TRADES_REPORT_COLS");
            if (isPrinting) {
                curYPos += ReportUtilities.drawTableHeader(columnWidths3, header, g2D, leftMargin + width / 2, curYPos, width / 2 - 2 * leftMargin, isPrinting);
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM));
            } else {
                curYPos += ReportUtilities.drawTableHeader(columnWidths3, header, g2D, leftMargin + width / 2, curYPos, width / 2 - 2 * leftMargin, isPrinting);
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));
            }
            header = null;

            curRowNumber = 0;
            //curXPos = leftMargin + width / 2;
            symbols = topStocksGenerator.getListMostActiveByTrades();  //todo Client.getInstance().getMostActiveByTrades();
//            System.out.println("#### MOST_ACTIVE_BY_NO_TRADES count " + symbols.length);
            for (int iS = 0; iS < symbols.length; iS++) {
                stock = marketSummaryReportStore.getStockObject(symbols[iS]);
//                stock = DataStore.getSharedInstance().getStockObject(symbols[iS]);
                if (stock == null) {
//                    System.out.println("Stock is NULL4 " + symbols[iS]);
                    continue;
                }
                oPriceFormat = getPriceFormatter(stock.getDecimalCount());
                oChangeFormat = getPriceFormatter(stock.getDecimalCount());
                oPChangeFormat = getPriceFormatter(stock.getDecimalCount());

                isEven = ((curRowNumber % 2) == 0);
                curRowNumber++;

                if (reportDate == null) {
                    curElemPrice = stock.getLastTradeValue();
                } else {
                    curElemPrice = stock.getPreviousClosed();
                }

                if (Language.isLTR()) {
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments3[colCounter], g2D,
                            leftMargin + width / 2, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getPercentChange()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getNoOfTrades()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                } else {
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getNoOfTrades()), alignments3[colCounter], g2D,
                            leftMargin + width / 2, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getPercentChange()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(curElemPrice), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments3[colCounter], g2D,
                            curXPos, curYPos, columnWidths3[colCounter++], rowHeight, isEven, isPrinting);
                }
                stock = null;
                counter++;
                curYPos += rowHeight;
                colCounter = 0;

                if (iS == 4)
                    break;
            }
            symbols = null;

            if (lastYofPrevTable > curYPos)
                curYPos = lastYofPrevTable;
            if (Math.abs(curYPos - panelHeight) > 20) {
                panelHeight = curYPos + 10;
                this.setPreferredSize(new Dimension(width - (parentWindow.getScrollBarWidth() * 2), panelHeight));
//                this.setPreferredSize(new Dimension(width - parentWindow.getScrollBarWidth()* 2 -100, panelHeight));
                this.setSize(new Dimension(width - (parentWindow.getScrollBarWidth() * 2), panelHeight));   // -parentWindow.getScrollBarWidth() - 100
//                this.setSize(new Dimension(width -parentWindow.getScrollBarWidth()* 2 -100, panelHeight));
                parentWindow.setViewPortSize(panelHeight);
                ReportGenerator.refresh();
            }
            /*if (isFrameNotAdjusted) {
                System.out.println("Report size " + curYPos);
                parentWindow.setViewPortSize(curYPos + 5);
                isFrameNotAdjusted = false;
            }*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        g2D = null;
    }

    public void setFrameNotAdjusted() {
        isFrameNotAdjusted = true;
    }

//    public Dimension getPreferredSize() {
////        return new Dimension((int)getSize().getWidth(), panelHeight);
//        return new Dimension((500), panelHeight);
//    }

    /**
     * The method @print@ must be implemented for @Printable@ interface.
     * Parameters are supplied by system.
     */
    public int print(Graphics g, PageFormat pf, int pageIndex) throws
            PrinterException {

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black); //set default foreground color to black
        //for faster printing, turn off double buffering
        //RepaintManager.currentManager(this).setDoubleBufferingEnabled(false);

        Dimension d = this.getSize(); //get size of document
        double panelWidth = d.width; //width in pixels
        double panelHeight = d.height; //height in pixels

        double pageHeight = pf.getImageableHeight(); //height of printer page
        double pageWidth = pf.getImageableWidth(); //width of printer page

        //panelWidth = pageWidth; // * 1.8;
        //panelHeight = panelHeight * (pageHeight/pageWidth);

        double scale = pageWidth / panelWidth;
//        int rowPerPage = (int)((pageHeight-headerHeight-headingHeight)/(rowHeight*scale));
//        int totalNumPages = Math.max((int)Math.ceil(symbols.length/(double)rowPerPage), 1);
        int totalNumPages = (int) Math.ceil(scale * panelHeight / pageHeight);
//System.out.println("totalNumPages = " + totalNumPages + " pageIndex = " + pageIndex);
        //make sure not print empty pages
        if ((pageIndex >= totalNumPages) || (pageIndex > 0)) {
            return Printable.NO_SUCH_PAGE;
        }

//        int iniRow = pageIndex*rowPerPage;
//        int endRow = Math.min(symbols.length, iniRow+rowPerPage);
//System.out.println("rowPerPage = " + rowPerPage + " pageHeight =" + pageHeight + " pageWidth =" + pageWidth);

        //shift Graphic to line up with beginning of print-imageable region
        g2.translate(pf.getImageableX(), pf.getImageableY());

        //shift Graphic to line up with beginning of next page to print
        //g2.translate(0f, -pageIndex * pageHeight);

        //scale the page so the width fits...
        g2.scale(scale, scale);

        // Calculate Column widths for the tables
        columnWidths1 = calculateColumnWidths((int) panelWidth, columnWidths1, colPercents1);
        columnWidths2 = calculateColumnWidths((int) panelWidth, columnWidths2, colPercents2);
        columnWidths3 = calculateColumnWidths((int) panelWidth / 2, columnWidths3, colPercents3);
        for (int i : columnWidths3) {
            totalColumnWidth3 += i + 5;
        }
        totalColumnWidth3 -= 5;
        try {
            drawReport(g2, true);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        g2 = null;

        return Printable.PAGE_EXISTS;
    }

    public void disposeReport() {
        topStocksGenerator.disposeObjects();

        topStocksGenerator = null;
        openDateFormat = null;
        oPriceFormat = null;
        oQuantityFormat = null;
        oChangeFormat = null;
        oPChangeFormat = null;
        sHeading = null;
        header = null;
        columnWidths1 = null;
        colPercents1 = null;
        alignments1 = null;
        columnWidths2 = null;
        colPercents2 = null;
        alignments2 = null;
        columnWidths3 = null;
        colPercents3 = null;
        alignments3 = null;
        sectorObjects = null;
//        indexSymbols.clear();
//        indexSymbols        = null;
    }

    private TWDecimalFormat getPriceFormatter(int decimals) {
        TWDecimalFormat oPriceFormat_1 = new TWDecimalFormat(" ###,##0.0  ");
        TWDecimalFormat oPriceFormat_2 = new TWDecimalFormat(" ###,##0.00  ");
        TWDecimalFormat oPriceFormat_3 = new TWDecimalFormat(" ###,##0.000  ");
        if (decimals == 1) {
            return oPriceFormat_1;
        } else if (decimals == 2) {
            return oPriceFormat_2;
        } else if (decimals == 3) {
            return oPriceFormat_3;
        } else {
            return oPriceFormat_2;
        }
    }

}