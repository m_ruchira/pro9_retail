package com.isi.csvr.reports;

import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Stock;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: sa
 * Date: Apr 7, 2005
 * Time: 2:19:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopStocksComparator implements Comparator {
    private int type = -1;
    private MarketSummaryReportDataStore dataStore = null;

    public TopStocksComparator(int type) {
        this.type = type;
    }

    public void setDataStore(MarketSummaryReportDataStore dataStore) {
        this.dataStore = dataStore;
    }

    // Comparator methods
    public int compare(Object o1, Object o2) {
//        Stock s1 = DataStore.getSharedInstance().getStockObject((String) o1);
//        Stock s2 = DataStore.getSharedInstance().getStockObject((String) o2);
        if (dataStore != null) {
            Stock s1 = dataStore.getStockObject((String) o1);
            Stock s2 = dataStore.getStockObject((String) o2);


            switch (type) {
                case Meta.MOST_ACTIVE_BY_VOLUME:
                    if (s2.getVolume() == s1.getVolume()) {
                        return s1.getShortDescription().compareTo(s2.getShortDescription());
                    } else {
                        return (int) (s2.getVolume() - s1.getVolume());
                    }
                case Meta.MOST_ACTIVE_BY_TRADES:
                    if (s2.getNoOfTrades() == s1.getNoOfTrades()) {
                        return s1.getShortDescription().compareTo(s2.getShortDescription());
                    } else {
                        return (s2.getNoOfTrades() - s1.getNoOfTrades());
                    }
                case Meta.TOP_GAINERS_BY_PCNTCHANGE:
                    if (s2.getPercentChange() == s1.getPercentChange()) {
                        return s1.getShortDescription().compareTo(s2.getShortDescription());
                    } else {
                        if (s2.getPercentChange() > s1.getPercentChange()) {
                            return 1;
                        } else if (s2.getPercentChange() < s1.getPercentChange()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                case Meta.TOP_LOSERS_BY_PCNTCHANGE:
                    if (s2.getPercentChange() == s1.getPercentChange()) {
                        return s1.getShortDescription().compareTo(s2.getShortDescription());
                    } else {
                        if (s2.getPercentChange() < s1.getPercentChange()) {
                            return 1;
                        } else if (s2.getPercentChange() > s1.getPercentChange()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                case Meta.TOP_GAINERS_BY_CHANGE:
                    if (s2.getChange() == s1.getChange()) {
                        return s1.getShortDescription().compareTo(s2.getShortDescription());
                    } else {
                        if (s2.getChange() > s1.getChange()) {
                            return 1;
                        } else if (s2.getChange() < s1.getChange()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                case Meta.TOP_LOSERS_BY_CHANGE:
                    if (s2.getChange() == s1.getChange()) {
                        return s1.getShortDescription().compareTo(s2.getShortDescription());
                    } else {
                        if (s2.getChange() < s1.getChange()) {
                            return 1;
                        } else if (s2.getChange() > s1.getChange()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                default:
                    return 0;
            }
        } else {
            return 0;
        }
    }
}