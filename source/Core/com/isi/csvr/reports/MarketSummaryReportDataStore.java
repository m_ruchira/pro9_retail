package com.isi.csvr.reports;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: sa Date: Apr 8, 2005 Time: 11:29:31 AM To change this template use File | Settings |
 * File Templates.
 */
public class MarketSummaryReportDataStore {
    // These variables will be used for the historical mode only.
    public long volume = 0;
    public double turnover = 0;
    public int noOfTrades = 0;
    public int symbolsTraded = 0;
    public int noOfUps = 0;
    public int noOfDown = 0;
    public int noOfNoChange = 0;
    private Hashtable dataStore;
    private String date = null;
    private ReportGenerator parentWindow;

    public MarketSummaryReportDataStore(ReportGenerator parentWindow) {
        this.parentWindow = parentWindow;
        dataStore = new Hashtable();
        try {
            loadData();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void reload() throws Exception {
        loadData();
    }

    private void loadData() throws Exception {
//        try {
        if (date == null) {
            dataStore = DataStore.getSharedInstance().getHashtable(parentWindow.getSelectedExchange().getSymbol());
        } else {
            dataStore = null;
            dataStore = new Hashtable();
            boolean dataFound = loadHistory(dataStore);

            if (!dataFound) {
                // bandu - This is to clear the entire report when there are no data available
                dataStore.clear();
                new ShowMessage(Language.getString("MSG_NO_DATA"), "E");

                parentWindow.repaintReport();
                throw new Exception("No data found");
            }
        }
//        } catch(Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
    }

    private boolean loadHistory(Hashtable hashtable) {
        Vector records = null;
        boolean dataFound = false;
        String firstDay = null;
        String symbol = null;
        String data = null;
        int instrumentType = -1;
        String key = null;
        Stock stock = null;
        int counter = 0;

        initMarketDetails();

        Enumeration symbols = DataStore.getSharedInstance().getSymbols(parentWindow.getSelectedExchange().getSymbol());
        while (symbols.hasMoreElements()) {
            data = (String) symbols.nextElement();
            symbol = SharedMethods.getSymbolFromExchangeKey(data);
            instrumentType = SharedMethods.getInstrumentFromExchangeKey(data);
            key = SharedMethods.getKey(parentWindow.getSelectedExchange().getSymbol(), symbol, instrumentType);
            //changed due to reports are generating for the Fridays also
            records = HistoryFilesManager.readHistoryData(symbol, parentWindow.getSelectedExchange().getSymbol(), date, date);
            if (records == null)
                continue;

            stock = new Stock(symbol, parentWindow.getSelectedExchange().getSymbol(), instrumentType);
            stock.setLongDescription(DataStore.getSharedInstance().getCompanyName(key));
            stock.setShortDescription(DataStore.getSharedInstance().getShortDescription(key));
            stock.setSectorCode(DataStore.getSharedInstance().getSectorCode(key));
            // hashtable.put(symbol, stock);
            hashtable.put(data, stock);

            if (records.size() > 0) {
                String record = (String) records.elementAt(0);
                StringTokenizer fields = new StringTokenizer(record, Meta.FS);
                firstDay = fields.nextToken(); //date

                if (date.equals(date)) { // there are records for the day
                    stock.setTodaysOpen(SharedMethods.floatValue(fields.nextToken()));      // open
                    stock.setHigh(SharedMethods.floatValue(fields.nextToken()));            // high
                    stock.setLow(SharedMethods.floatValue(fields.nextToken()));             // low
                    stock.setTodaysClose(SharedMethods.floatValue(fields.nextToken()));     // close
                    stock.setPercentChange(SharedMethods.floatValue(fields.nextToken()));   // % chg
                    stock.setChange(SharedMethods.floatValue(fields.nextToken()));          // chg
                    stock.setPreviousClosed(SharedMethods.floatValue(fields.nextToken()));  // prev close
                    stock.setVolume(SharedMethods.longValue(fields.nextToken()));           // vol
                    stock.setTurnover(SharedMethods.floatValue(fields.nextToken()));        // turnover
                    stock.setNoOfTrades(SharedMethods.intValue(fields.nextToken(), 0));     // no of trades

                    // Update market details
                    updateMarketDetails(stock);

                    stock = null;
                    dataFound = true;
                }

                if ((counter % 20) == 0) {
                    parentWindow.repaintReport();
                }
                counter++;
                fields = null;
            }
        }
//        if(counter ==0){
//            new ShowMessage(Language.getString("MSG_NO_DATA"), "E");
//        }
        parentWindow.repaintReport();

        symbols = null;
        return dataFound;
    }

    private void initMarketDetails() {
        this.noOfDown = 0;
        this.noOfUps = 0;
        this.noOfNoChange = 0;
        this.symbolsTraded = 0;
        this.volume = 0;
        this.turnover = 0;
        this.noOfTrades = 0;
    }

    private void updateMarketDetails(Stock stock) {
        /*if (DataStore.getSharedInstance().getStockObject(stock.getKey()).getInstrumentType() == Meta.INDEX) {
//            System.out.println("---- updateMarketDetails " + stock.getKey() + " " + stock.getVolume()[0]);
            this.volume += stock.getVolume();
            this.turnover += stock.getTurnover();
        } else {
            this.symbolsTraded++;
            this.noOfTrades += stock.getNoOfTrades();
            if (stock.getChange() > 0)
                this.noOfUps++;
            else if (stock.getChange() < 0)
                this.noOfDown++;
            else
                this.noOfNoChange++;
        }*/ //Bug ID <#0036> Volume and Turnover doubled for market data (8.20.001)
        Stock sector = DataStore.getSharedInstance().getStockObject(stock.getKey());
        if (sector.getInstrumentType() == Meta.INSTRUMENT_INDEX) {
        } else {
            this.volume += stock.getVolume();
            this.turnover += stock.getTurnover();
            this.symbolsTraded++;
            this.noOfTrades += stock.getNoOfTrades();
            if (stock.getChange() > 0)
                this.noOfUps++;
            else if (stock.getChange() < 0)
                this.noOfDown++;
            else
                this.noOfNoChange++;
        }
    }

    public Hashtable getDataStore() {
        return dataStore;
    }

    public Stock getStockObject(String key) {
        try {
            String symbol = SharedMethods.getExchangeKey(key);
//            String exchange = key.split(Constants.KEY_SEPERATOR_CHARACTER)[0];

            Stock stock = (Stock) dataStore.get(symbol);
            /*if (stock == null){
                stock = new Stock(symbol, exchange);
                stock.setLongDescription(Language.getString("NA"));
                stock.setShortDescription(Language.getString("NA"));
                stock.setSectorCode(Language.getString("NA"));
                dataStore.put(symbol,stock);
            }*/
            return stock;
        } catch (Exception e) {
//            System.out.println("ERROR GetStock " + key);
//            e.printStackTrace();
            return null;
        }
    }
}
