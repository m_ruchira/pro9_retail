package com.isi.csvr.reports;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 28, 2003
 * Time: 3:27:46 PM
 * To change this template use Options | File Templates.
 */
public class SymbolSummaryReportDataStore {
    private Hashtable dataStore;
    private String date = null;
    private ReportGenerator parentWindow;

    public SymbolSummaryReportDataStore(ReportGenerator parentWindow) {
        this.parentWindow = parentWindow;
        dataStore = new Hashtable();
        try {
            loadData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void reload() throws Exception {
        loadData();
    }

    private void loadData() throws Exception {
//        try {
        if (date == null) {
            dataStore = DataStore.getSharedInstance().getHashtable(parentWindow.getSelectedExchange().getSymbol());
        } else {
            dataStore = null;
            dataStore = new Hashtable();
            boolean dataFound = loadHistory(dataStore);

            if (!dataFound) {
                // bandu - This is to clear the entire report when there are no data available
                dataStore.clear();
                new ShowMessage(Language.getString("MSG_NO_DATA"), "E");
                throw new Exception("No data found");
            }
        }
//        } catch (Exception e) {
        //e.printStackTrace();
//        }
    }

    private boolean loadHistory(Hashtable hashtable) {
        Vector records = null;
        boolean dataFound = false;
        String firstDay = null;
        String symbol = null;
        String data = null;
        int instrumentType = -1;
        Stock stock = null;
        int counter = 0;

        Enumeration symbols = DataStore.getSharedInstance().getSymbols(parentWindow.getSelectedExchange().getSymbol());
        while (symbols.hasMoreElements()) {
            data = (String) symbols.nextElement();
            symbol = data;
            instrumentType = SharedMethods.getInstrumentFromExchangeKey(data);
//            System.out.println(symbol);
            records = HistoryFilesManager.readHistoryData(SharedMethods.getSymbolFromExchangeKey(symbol), parentWindow.getSelectedExchange().getSymbol(), date, date);
            if (records == null) {
                //todo add this as a setting 
//                Hashtable temp=DataStore.getSharedInstance().getHashtable(parentWindow.getSelectedExchange().getSymbol());
//                Stock st=(Stock)temp.get(symbol);
//                hashtable.put(symbol, st);

                continue;

            }

            stock = new Stock(symbol, parentWindow.getSelectedExchange().getSymbol(), instrumentType);
            stock.setLongDescription(DataStore.getSharedInstance().getCompanyName(symbol));
            stock.setShortDescription(DataStore.getSharedInstance().getShortDescription(symbol));
            stock.setSectorCode(DataStore.getSharedInstance().getSectorCode(symbol));
            hashtable.put(symbol, stock);
//            System.out.println("ADD ENTRY " + hashtable.size() + " " + symbol);

            if (records.size() > 0) {
                String record = (String) records.elementAt(0);
                StringTokenizer fields = new StringTokenizer(record, Meta.FS);
                firstDay = fields.nextToken(); //date

                if (date.equals(date)) { // thre are record for the day
                    stock.setTodaysOpen(SharedMethods.floatValue(fields.nextToken()));      // open
                    stock.setHigh(SharedMethods.floatValue(fields.nextToken()));            // high
                    stock.setLow(SharedMethods.floatValue(fields.nextToken()));             // low
                    stock.setTodaysClose(SharedMethods.floatValue(fields.nextToken()));     // close
                    stock.setPercentChange(SharedMethods.floatValue(fields.nextToken()));   // % chg
                    stock.setChange(SharedMethods.floatValue(fields.nextToken()));          // chg
                    stock.setPreviousClosed(SharedMethods.floatValue(fields.nextToken()));  // prev close
                    stock.setVolume(SharedMethods.longValue(fields.nextToken()));           // vol
                    stock.setTurnover(SharedMethods.floatValue(fields.nextToken()));        // turnover
                    stock.setMarketID(SymbolMaster.getMarketID(symbol, parentWindow.getSelectedExchange().getSymbol()));

                    stock = null;
                    dataFound = true;
                }
                fields = null;
                if ((counter % 20) == 0) {
                    //    parentWindow.repaintReport();
                }
                counter++;
            }
        }
//        if(counter ==0){
//            new ShowMessage(Language.getString("MSG_NO_DATA"), "E");
//        }
        // parentWindow.repaintReport();
        symbols = null;
//        System.out.println("No of Symbol records for the day " + hashtable.size());
        return dataFound;
    }

    public Hashtable getDataStore() {
        return dataStore;
    }

    public Stock getStockObject(String key) {
        String symbol = SharedMethods.getExchangeKey(key);
        //String exchange = key.split(Constants.KEY_SEPERATOR_CHARACTER)[0];

        Stock stock = (Stock) dataStore.get(symbol);
        /*if (stock == null){
            stock = new Stock(symbol, exchange);
            stock.setLongDescription(Language.getString("NA"));
            stock.setShortDescription(Language.getString("NA"));
            stock.setSectorCode(Language.getString("NA"));
            dataStore.put(symbol,stock);
        }*/
        return stock;
    }

    public boolean isSymbolExists(String symbol) {
        return dataStore.containsKey(symbol);
    }
}
