package com.isi.csvr.reports;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 5, 2005
 * Time: 11:23:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class ReportManager {
    private static ReportGenerator repSymbol;
    private static ReportGenerator repMarket;
    int type;

    public ReportManager() {
        /*this.type = Type;
        if(type == ReportGenerator.REPORT_SYMBOL_TRADING_SUMMARY){
            showSymbolTradingReport();
        }
        else if(type == ReportGenerator.REPORT_DAILY_MARKET_SUMMARY){
            showDailyMarketReport();
        }*/
    }

    public static void showSymbolTradingReport() {
        if (repSymbol == null) {
            repSymbol = new ReportGenerator(ReportGenerator.REPORT_SYMBOL_TRADING_SUMMARY);
        } else {
            repSymbol.dispose();
            repSymbol = null;
            repSymbol = new ReportGenerator(ReportGenerator.REPORT_SYMBOL_TRADING_SUMMARY);
        }
    }

    public static void showDailyMarketReport() {
        if (repMarket == null) {
            repMarket = new ReportGenerator(ReportGenerator.REPORT_DAILY_MARKET_SUMMARY);
        } else {
            repMarket.dispose();
            repMarket = null;
            repMarket = new ReportGenerator(ReportGenerator.REPORT_DAILY_MARKET_SUMMARY);
        }
    }

    public static void clearSymbolTradingReport() {
        if (repSymbol != null) {
            repSymbol = null;
        }
    }

    public static void clearDailyMarketReport() {
        if (repMarket != null) {
            repMarket = null;
        }
    }
}
