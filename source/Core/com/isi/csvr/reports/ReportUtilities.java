package com.isi.csvr.reports;

import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.io.File;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class ReportUtilities {

    public static final int LEFT = 1;
    public static final int CENTER = 2;
    public static final int RIGHT = 3;

    public static final int UP = 1;
    public static final int DOWN = 2;
    public static final int NOCHANGE = 3;

    public static final int FONT_SMALL = 12; //12;
    public static final int FONT_MEDIUM = 14; //14;
    public static final int FONT_LARGE = 18; //18;
    public static final int FONT_LARGEST = 20; //20;
    public static final int FONT_EXTREME = 28;

    public static final int FONT_PRINT_SMALL = 12; //15; //14;
    public static final int FONT_PRINT_MEDIUM = 14; //17; //16;
    public static final int FONT_PRINT_LARGE = 18; //20; //20;
    public static final int FONT_PRINT_LARGEST = 20; //22; //22;
    public static final int FONT_PRINT_EXTREME = 28; //28; //26;

    public static final BasicStroke BS_1p8f_solid = new BasicStroke(1.8f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final BasicStroke BS_1p5f_solid = new BasicStroke(1.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final BasicStroke BS_1p0f_solid = new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final BasicStroke BS_0p8f_solid = new BasicStroke(0.8f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final BasicStroke BS_0p5f_solid = new BasicStroke(0.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);

    public static Color upColor = Color.green.darker();
    public static Color downColor = Color.red;
    public static Color normalColor = Color.black;
    public static Color evenRowBGColor = new Color(246, 246, 246);
    public static Color oddRowBGColor = new Color(255, 255, 255);
    public static Color evenRowFGColor = Color.black;
    public static Color oddRowFGColor = Color.black;
    public static Color gridColor = Color.black;
    public static Color headerBGColor = new Color(167, 193, 213); //Color.black;
    public static Color headerFGColor = Color.black;
//    public static Color     headingBGColor      = new Color(167, 193, 213); //Color.black;
//    public static Color     headingFGColor      = Color.red;//black;

    private static Image uniQuotesLogo;
    private static Image companyLogo;
    private static boolean isCustomizedVersion = false;
    private static boolean isUniQuotesLogoExists = false;

    public static boolean isCustomizedVersion() {
        return isCustomizedVersion;
    }

    public static void loadImages(ReportGenerator rg) {
        File file = null;
        MediaTracker comp = null;
        try {
//            file = new File(".\\images\\Common\\companyLogo1.gif");
//            file = new File(".\\images\\Common\\mubasher_"+Language.getLanguageTag()+".gif");
//            if (file.isFile())
            isCustomizedVersion = true;
//            else
//                isCustomizedVersion = false;
//            file = null;

            file = new File(".\\images\\Common\\mubasher_" + Language.getLanguageTag() + ".gif");
            if (file.isFile())
                isUniQuotesLogoExists = true;
            else
                isUniQuotesLogoExists = false;

            companyLogo = Toolkit.getDefaultToolkit().getImage(".\\images\\Common\\report_" + Language.getLanguageTag() + ".gif");
//            companyLogo = Toolkit.getDefaultToolkit().getImage(".\\images\\Common\\companyLogo1.gif");
            uniQuotesLogo = Toolkit.getDefaultToolkit().getImage(".\\images\\Common\\mubasher_" + Language.getLanguageTag() + ".gif");
            comp = new MediaTracker(rg);
            comp.addImage(companyLogo, 0);
            comp.addImage(uniQuotesLogo, 1);
            comp.waitForID(0);
            comp.waitForID(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        file = null;
    }

    public static Graphics2D drawHeader(Graphics2D g2D, int x, int y, int width, int height, boolean isPrinting) {
        int midXPos = 0;//x + width/2; // + 80;
        int xPos = 0;
        int offset = 100;       // Address display offset from the center
        int yPos = y + 5;
        FontMetrics fm = null;
        int imgWidth = 0;

        try {
            if (Language.isLTR())
                midXPos = x + width / 2 + offset;
            else
                midXPos = x + width / 2 - offset;

//            if (companyLogo == null)
//                isCustomizedVersion = false;
//            else
//                isCustomizedVersion = true;

            g2D.setStroke(BS_1p5f_solid);
            g2D.drawRect(x, y, width, height);

            if (isCustomizedVersion) {
                xPos = x + 10;
                if (Language.isLTR()) {
//                  ---Commentd by Shanika ----  ***** uncommented by buddhikak ;) *****
                    g2D.drawImage(companyLogo, xPos, y + (height / 2) - companyLogo.getHeight(null) / 2, companyLogo.getWidth(null),
                            companyLogo.getHeight(null), null);

                    imgWidth = uniQuotesLogo.getWidth(null);
                    int imgHeight = uniQuotesLogo.getHeight(null);
                    g2D.drawImage(uniQuotesLogo, xPos + width - imgWidth - 15, y + (height / 2) - imgHeight / 2, imgWidth,
                            imgHeight, null);
                } else {
                    g2D.drawImage(uniQuotesLogo, xPos, y + (height / 2) - uniQuotesLogo.getHeight(null) / 2, uniQuotesLogo.getWidth(null),
                            uniQuotesLogo.getHeight(null), null);
                    imgWidth = companyLogo.getWidth(null);
                    int imgHeight = companyLogo.getHeight(null);

                    g2D.drawImage(companyLogo, xPos + width - imgWidth - 15, y + (height / 2) - imgHeight / 2, imgWidth,
                            imgHeight, null);
                }
            } else {
                //g2D.setStroke(BS_1p8f_solid);
                g2D.setFont(Theme.getDefaultFont(Font.BOLD, FONT_EXTREME));
                fm = g2D.getFontMetrics();

//                if (!Language.isLTR())
//                    xPos = x + width - fm.stringWidth(Language.getString("MENU_SYSTEM")) - 5;
//                else
//                    xPos = x + 10;
                if (!isUniQuotesLogoExists) {
                    if (!Language.isLTR())
                        xPos = x + width - fm.stringWidth(Language.getString("UNIQUOTES")) - 5;
                    else
                        xPos = x + 10;
                    g2D.drawString(Language.getString("UNIQUOTES"), xPos, y + (height / 2));
                } else {
                    imgWidth = uniQuotesLogo.getWidth(null);
                    if (!Language.isLTR())
                        xPos = x + width - imgWidth - 5;
                    else
                        xPos = x + 10;
                    g2D.drawImage(uniQuotesLogo, xPos, y + (height / 2) - uniQuotesLogo.getHeight(null) / 2, uniQuotesLogo.getWidth(null),
                            uniQuotesLogo.getHeight(null), null);
                }
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, FONT_PRINT_SMALL));

//                if (!isPrinting) {
//                    g2D.setColor(headingBGColor);
//                    g2D.fillRect(midXPos, yPos - fm.getAscent(), width - midXPos, height);
//                    g2D.setColor(headingFGColor);
//                }

                fm = g2D.getFontMetrics();
                yPos += fm.getAscent();
                xPos = midXPos;
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("RPT_ADDRESS_LINE_1")); //"National technology Group");
                g2D.drawString(Language.getString("RPT_ADDRESS_LINE_1"), xPos, yPos);
                yPos += fm.getHeight();
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("RPT_ADDRESS_LINE_2")); //"P O Box 220625");
                g2D.drawString(Language.getString("RPT_ADDRESS_LINE_2"), xPos, yPos);
                yPos += fm.getHeight();
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("RPT_ADDRESS_LINE_3")); //"Riyadh 11311, Saudi Arabia");
                g2D.drawString(Language.getString("RPT_ADDRESS_LINE_3"), xPos, yPos);
                yPos += fm.getHeight();
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("COMPANY_URL")); //"http://tadawul.uniquotes.com");
                g2D.drawString(Language.getString("COMPANY_URL"), xPos, yPos);
                yPos += fm.getHeight();
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("COMPANY_EMAIL")); //"uniquotes@acs.com.sa");
                g2D.drawString(Language.getString("COMPANY_EMAIL"), xPos, yPos);
            }
        } catch (Exception ex) {
        }
        fm = null;
        return g2D;
    }

    public static int drawHeading(String heading, Graphics2D g2D, int midX, int yPos, int fontSize) {
        int xPos = 0;
        FontMetrics fm = null;
        try {
            g2D.setFont(Theme.getDefaultFont(Font.BOLD, fontSize));
            fm = g2D.getFontMetrics();
            yPos += fm.getAscent();
            xPos = midX - fm.stringWidth(heading) / 2;
            g2D.setColor(normalColor);
            g2D.drawString(heading, xPos, yPos);
        } catch (Exception ex) {
        }
        return fm.getHeight() + fm.getDescent() + fm.getDescent();
    }

//    public static int drawHeading(String heading, Graphics2D g2D, int midX, int yPos, int fontSize, Color bgColor, Color fgColor, boolean isPrinting) {
//        int xPos = 0;
//        FontMetrics fm = null;
//        try {
//            g2D.setFont(Theme.getDefaultFont(Font.BOLD, fontSize));
//            fm = g2D.getFontMetrics();
//            yPos += fm.getAscent();
//            xPos = midX - fm.stringWidth(heading)/2;
//            if (!isPrinting) {
//                g2D.setColor(evenRowBGColor);
//
//            }
//            g2D.drawString(heading, xPos, yPos);
//        } catch (Exception ex) {
//        }
//        return fm.getHeight() + fm.getDescent() + fm.getDescent();
//    }

    public static int drawTableHeader(int[] widths, String[] header, Graphics2D g2D, int x, int y, int width, boolean isPrinting) {
        int xPos = x;
        int yPos = y;
        int height = 0;

        FontMetrics fm = null;
        try {
            if (isPrinting)
                g2D.setFont(Theme.getDefaultFont(Font.BOLD, FONT_PRINT_MEDIUM));
            else {
                g2D.setFont(Theme.getDefaultFont(Font.BOLD, FONT_MEDIUM));
//                g2D.setColor(Color.blue); // headerBGColor);
//                g2D.fillRect(x, y, width, height);
            }
//            g2D.setColor(headerFGColor);
            fm = g2D.getFontMetrics();
            height = fm.getAscent() + fm.getHeight() + fm.getDescent();
//            System.out.println("height = " + height + " " + yPos);
            //yPos += height/2;
            for (int i = 0; i < widths.length; i++) {
                if (Language.isLTR())
                    drawTableHeaderColumn(header[i], CENTER, g2D, xPos, yPos, widths[i], height, isPrinting);
                    //drawTableColumn(header[i], CENTER, g2D, xPos, yPos, widths[i], height);
                else
                    drawTableHeaderColumn(header[widths.length - 1 - i], CENTER, g2D, xPos, yPos, widths[i], height, isPrinting);
                xPos += widths[i];
            }
//            xPos = midX - fm.stringWidth(heading)/2;
//            g2D.drawString(heading, xPos, yPos);
        } catch (Exception ex) {
        }
        fm = null;
        return height;
    }

    /*public static int drawTableColumn(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height) {
        int xPos = x;
        FontMetrics fm = g2D.getFontMetrics();
        g2D.setStroke(BS_0p5f_solid);
//        g2D.setColor(gridColor);

        g2D.drawRect(x, y, width, height);
        y += g2D.getFontMetrics().getAscent() + 5;
        switch (alignment) {
            case LEFT :
                xPos += 5;
                break;
            case CENTER :
                xPos += (width - fm.stringWidth(data))/2;
                break;
            case RIGHT :
                xPos += (width - fm.stringWidth(data) - 5);
                break;
        }
        g2D.drawString(data, xPos, y);
        fm = null;
        return x + width;
    }*/

    public static int drawTableColumn(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height, int status, boolean isEven, boolean isPrinting) {
        int xPos = x;
        FontMetrics fm = g2D.getFontMetrics();

        if (!isPrinting) {
            if (isEven)
                g2D.setColor(evenRowBGColor);
            else
                g2D.setColor(oddRowBGColor);

            g2D.fillRect(x, y, width, height);
        } else {
            g2D.setColor(Color.white);
        }
        g2D.setStroke(BS_0p5f_solid);
        if (!isPrinting)
            g2D.setColor(gridColor);
        else
            g2D.setColor(Color.black);
        g2D.drawRect(x, y, width, height);

        y += g2D.getFontMetrics().getAscent() + 5;

        if (!isPrinting) {
            switch (status) {
                case UP:
                    g2D.setColor(upColor);
                    break;
                case DOWN:
                    g2D.setColor(downColor);
                    break;
                case NOCHANGE:
                    if (isEven)
                        g2D.setColor(evenRowFGColor);
                    else
                        g2D.setColor(oddRowFGColor);
                    break;
            }
        } else {
            g2D.setColor(Color.black);
        }

        switch (alignment) {
            case LEFT:
                xPos += 5;
                break;
            case CENTER:
                xPos += (width - fm.stringWidth(data)) / 2;
                break;
            case RIGHT:
                xPos += (width - fm.stringWidth(data) - 5);
                break;
        }
        g2D.drawString(data, xPos, y);

        if (!isPrinting) {
            if (isEven)
                g2D.setColor(evenRowFGColor);
            else
                g2D.setColor(oddRowFGColor);
        } else {
            g2D.setColor(Color.white);
        }
        fm = null;
        return x + width;
    }

    public static int drawTableColumn(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height, boolean isEven, boolean isPrinting) {
        int xPos = x;
        FontMetrics fm = g2D.getFontMetrics();

        if (!isPrinting) {
            if (isEven)
                g2D.setColor(evenRowBGColor);
            else
                g2D.setColor(oddRowBGColor);
            g2D.fillRect(x, y, width, height);
        } else {
            g2D.setColor(Color.white);
        }

        g2D.setStroke(BS_0p5f_solid);
        if (!isPrinting)
            g2D.setColor(gridColor);
        else
            g2D.setColor(Color.black);
        g2D.drawRect(x, y, width, height);
        y += g2D.getFontMetrics().getAscent() + 5;
        switch (alignment) {
            case LEFT:
//                xPos += 5;
                xPos += 2;
                break;
            case CENTER:
                xPos += (width - fm.stringWidth(data)) / 2;
                break;
            case RIGHT:
//                xPos += (width - fm.stringWidth(data) - 5);
                xPos += (width - fm.stringWidth(data) - 2);
                break;
        }
        if (!isPrinting) {
            if (isEven)
                g2D.setColor(evenRowFGColor);
            else
                g2D.setColor(oddRowFGColor);
        } else {
            g2D.setColor(Color.black);
        }

        g2D.drawString(data, xPos, y);
//        if (!isPrinting) {
//            if (isEven)
//                g2D.setColor(evenRowFGColor);
//            else
//                g2D.setColor(oddRowFGColor);
//        }
        fm = null;
        return x + width;
    }

    public static int drawTableHeaderColumn(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height, boolean isPrinting) {
        int xPos = x;
        FontMetrics fm = g2D.getFontMetrics();

        if (!isPrinting) {
            g2D.setColor(headerBGColor);
            g2D.fillRect(x, y, width, height);
        } else {
            g2D.setColor(Color.black);
            g2D.fillRect(x, y, width, height);
        }

        g2D.setStroke(BS_0p5f_solid);
        if (!isPrinting)
            g2D.setColor(gridColor);
        else
            g2D.setColor(Color.black);
        g2D.drawRect(x, y, width, height);
        y += g2D.getFontMetrics().getAscent() + 5;
        switch (alignment) {
            case LEFT:
                xPos += 5;
                break;
            case CENTER:
                xPos += (width - fm.stringWidth(data)) / 2;
                break;
            case RIGHT:
                xPos += (width - fm.stringWidth(data) - 5);
                break;
        }
        if (!isPrinting) {
            g2D.setColor(headerFGColor);
        } else {
            g2D.setColor(Color.white);
        }

        g2D.drawString(data, xPos, y);
//        if (!isPrinting) {
//            if (isEven)
//                g2D.setColor(evenRowFGColor);
//            else
//                g2D.setColor(oddRowFGColor);
//        }
        fm = null;
        return x + width;
    }

}