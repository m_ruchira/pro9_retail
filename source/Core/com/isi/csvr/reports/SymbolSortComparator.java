package com.isi.csvr.reports;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWControl;

import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class SymbolSortComparator implements Comparator {

    private int column = -1;
    private int sortOrder = 0;

    public SymbolSortComparator() {
        try {
            column = Integer.parseInt(TWControl.getItem("SYMBOL_TRADING_SUMMARY_SORT_COLUMN"));
        } catch (NumberFormatException e) {
            column = -1;
        }
    }

    private static int compareValues(int val1, int val2, int sortOrder) {
        return (int) ((val1 - val2) * sortOrder);
    }

    private static int compareValues(double val1, double val2, int sortOrder) {
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

    public int compare(Object o1, Object o2) {
        Stock obj1 = DataStore.getSharedInstance().getStockObject((String) o1);
        Stock obj2 = DataStore.getSharedInstance().getStockObject((String) o2);
//        return (obj1.getCompanyCode().compareTo(obj2.getCompanyCode()));
//        return StockData.compare(obj1, obj2, column, sortOrder);

        try {
            switch (column) {
                case 0:
                    return (obj1.getSymbol().compareTo(obj2.getSymbol()));
                case 1:
                    return (obj1.getShortDescription().compareTo(obj2.getShortDescription()));
                case 81:
                    return (obj1.getCompanyCode().compareTo(obj2.getCompanyCode()));
                case 3:
                    return compareValues(obj1.getTurnover(), obj2.getTurnover(), sortOrder);
                default:
                    return (obj1.getShortDescription().compareTo(obj2.getShortDescription()));
            }
        } catch (Exception e) {
            return (((String) o1).compareTo((String) o2));
        }
    }

}