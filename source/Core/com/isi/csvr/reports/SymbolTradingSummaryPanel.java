package com.isi.csvr.reports;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.Market;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class SymbolTradingSummaryPanel extends JPanel
        implements Printable {

    //    private TWDateFormat    g_oDateTimeFormat   = new TWDateFormat("yyyy/MM/dd HH:mm:ss");
    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
    private TWDecimalFormat oChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");

    private int panelWidth = 800;   // In Pixels
    private int panelHeight = 500;   // In Pixels
    private int leftMargin = 5;
    private int topMargin = 5;
    private int headerHeight = 82; //100; //110;
    private int headingHeight = 0;
    private int rowHeight = 0;

    private String sHeading;
    private int[] columnWidths;
    private int[] colPercents = {25, 8, 8, 9, 11, 15, 8, 8, 8};
    private String[] symbols;
    private String[] header; // = {"Company", "Last", "Chg.", "Change %", "Volume", "Turnover", "Open", "High", "Low"};
    private int[] alignments = {ReportUtilities.LEFT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT,
            ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT, ReportUtilities.RIGHT};

    private SymbolSummaryReportDataStore symbolSummaryReportStore;
    private String reportDate = null;
    private Exchange curExchange;
    private ReportGenerator parentWindow;
    private boolean isFrameNotAdjusted;

    private Market subMarket;
    private boolean checkSubMarket = false;
    private boolean showSubMarketID = false;

    public SymbolTradingSummaryPanel(ReportGenerator parentWindow) {
        try {
            this.parentWindow = parentWindow;
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        symbolSummaryReportStore = new SymbolSummaryReportDataStore(parentWindow);
    }

    public void setExchange(Exchange excg) {
        curExchange = excg;
        checkSubMarket = false;
        isFrameNotAdjusted = true;

        if (excg.hasSubMarkets() && excg.isUserSubMarketBreakdown()) {
            showSubMarketID = true;
        } else {
            showSubMarketID = false;
        }

        try {
            symbolSummaryReportStore.reload();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            initReportDataStore();
            initReportHeader();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.repaintReport();
    }

    public void initLayoutChangesForReport() {
        panelWidth = this.getSize().width;// - ReportGenerator.getScrollBarWidth();
        panelHeight = this.getSize().height;

        calculateColumnWidths(panelWidth);
        adjustAlignments();
    }

    private void initReportDataStore() {
        if (curExchange == null) {
            try {
                curExchange = parentWindow.getSelectedExchange(); //ExchangeStore.getSharedInstance().getDefaultExchange();
            } catch (Exception e) {
                return;
            }
        }
        initReportHeader();

        Stock stock = null;
        ArrayList<String> arrayOfSymbols = new ArrayList<String>();
        Enumeration<String> enume = DataStore.getSharedInstance().getSymbols(curExchange.getSymbol());
        String data = null;
        String symbol = null;
        int instrument = -1;
        while (enume.hasMoreElements()) {
            data = enume.nextElement();
            symbol = SharedMethods.getSymbolFromExchangeKey(data);
            instrument = SharedMethods.getInstrumentFromExchangeKey(data);
            stock = DataStore.getSharedInstance().getStockObject(curExchange.getSymbol(), symbol, instrument);
            if (getReportDate() == null) { //Bug ID <#0031>
                if ((stock != null) && (stock.getNoOfTrades() > 0) && (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX)) {
                    //            if ((stock != null) && (stock.getInstrumentType() != Meta.INDEX)) { //Bug ID <#0011>
                    if ((checkSubMarket)) {
                        if (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) {
                            try {
                                if ((stock.getSymbolCode().split(Constants.MARKET_SEPERATOR_CHARACTER)[1]).equals(subMarket.getMarketID())) {
                                    arrayOfSymbols.add(stock.getKey());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            if ((subMarket.getMarketID().equals(stock.getMarketID())))
                                arrayOfSymbols.add(stock.getKey());
                        }
                    } else {
                        arrayOfSymbols.add(stock.getKey());
                    }
                }
            } else {
                if ((stock != null) && (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX)) {
                    if ((checkSubMarket)) {
                        if ((stock.getMarketID() != null) && (stock.getMarketID().equals(Constants.DEFAULT_MARKET))) {
                            try {
                                if (((stock.getSymbolCode().split(Constants.MARKET_SEPERATOR_CHARACTER)[1]).equals(subMarket.getMarketID())) && (stock.getVolume() > 0)) {
                                    arrayOfSymbols.add(stock.getKey());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //if((subMarket.getMarketID().equals(stock.getMarketID()))  && (stock.getVolume()>0) ) // Bug ID <#????> report becomes blank for submarkets if mkt is not open / 8.50.003 / 5 Sep 2006
                            if (subMarket.getMarketID().equals(stock.getMarketID())) {
                                arrayOfSymbols.add(stock.getKey());
                            }
                        }
                    } else {
                        //if(stock.getVolume()>0) // Bug ID <#0033> report becomes blank during preopen / 8.20.011 / 4 July 2006
                        arrayOfSymbols.add(stock.getKey());
                    }
                }
            }
            stock = null;
        }
        symbols = arrayOfSymbols.toArray(new String[0]);

        enume = null;
        arrayOfSymbols = null;

        Arrays.sort(symbols, new SymbolSortComparator());
    }

    private void initReportHeader() {
        try {
            header = Language.getList("TRADING_SUMMARY");
        /*
        16/06/2010	1.0	Initial Draft of the CR 	Prasad Maduranga Gunawardane
        16/07/2010	1.1	The concern of displaying the previous days values were added as per the comments given by Madusha    	Prasad Maduranga Gunawardane
        Change done by Chandika Hewage

         */

            try {
//                SimpleDateFormat reportDateFormat = new SimpleDateFormat("yyyyMMdd");
//                Date dt1 = reportDateFormat.parse(reportDate);
//                SimpleDateFormat currentDay = new SimpleDateFormat("yyyyMMdd");
//                Date dt2 = new Date();
                if (reportDate == null) {
                    if (curExchange.getMarketStatus() == Constants.CLOSED) {
                        header[1] = Language.getString("FOREX_STATUS_CLOSED");
                    } else {
                        header[1] = Language.getString("BONDS_LAST_TRADE");
                    }
                } else {
                    header[1] = Language.getString("FOREX_STATUS_CLOSED");

                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (reportDate == null) {
//            sHeading = curExchange.getDescription() + " - " + Language.getString("SYMBOL_TRADING_SUMMARY") + " - " +
//                    Client.getFormattedmarketTime(curExchange.getZoneAdjustedMarketTime());

                if (curExchange.getMarketStatus() == Constants.CLOSED) {
                    TWDateFormat clientTimeFormatter = new TWDateFormat(Language.getString("MARKET_TIME_CLOSE"));
                    //sHeading = curExchange.getDescription() + " - " + Language.getString("SYMBOL_TRADING_SUMMARY") + " - " +
                    //    clientTimeFormatter.format(new Date(curExchange.getZoneAdjustedMarketTime()));
                    if (checkSubMarket) {
                        sHeading = curExchange.getDescription() + " - " + subMarket.getDescription() + " - " + Language.getString("SYMBOL_TRADING_SUMMARY") + " - " +
                                clientTimeFormatter.format(new Date(curExchange.getZoneAdjustedMarketTime()));
                    } else {
                        sHeading = curExchange.getDescription() + " - " + Language.getString("SYMBOL_TRADING_SUMMARY") + " - " +
                                clientTimeFormatter.format(new Date(curExchange.getZoneAdjustedMarketTime()));
                    }
                } else {
                    //sHeading = curExchange.getDescription() + " - " + Language.getString("SYMBOL_TRADING_SUMMARY") + " - " +
                    //    Client.getFormattedmarketTime(curExchange.getZoneAdjustedMarketTime());
                    if (checkSubMarket) {
                        sHeading = curExchange.getDescription() + " - " + subMarket.getDescription() + " - " + Language.getString("SYMBOL_TRADING_SUMMARY") + " - " +
                                Client.getFormattedmarketTime(curExchange.getZoneAdjustedMarketTime());
                    } else {
                        sHeading = curExchange.getDescription() + " - " + Language.getString("SYMBOL_TRADING_SUMMARY") + " - " +
                                Client.getFormattedmarketTime(curExchange.getZoneAdjustedMarketTime());
                    }
                }

            } else {
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                    Date dt = dateFormat.parse(reportDate);
                    TWDateFormat clientTimeFormatter = new TWDateFormat(Language.getString("MARKET_TIME_CLOSE"));
                    sHeading = curExchange.getDescription() + " - " + Language.getString("SYMBOL_TRADING_SUMMARY") + " - " +
                            clientTimeFormatter.format(dt);
                    dateFormat = null;
                    dt = null;
                    clientTimeFormatter = null;
                } catch (Exception e) {
                    sHeading = "";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getReportDate() {
        return reportDate;
    }

    public void drawReport(String reportDate) throws Exception {
        isFrameNotAdjusted = true;
        this.reportDate = reportDate;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
//        if(format1.format(new Date(System.currentTimeMillis())).equals(reportDate)){
        if (format1.format(new Date(curExchange.getMarketDate())).equals(reportDate)) {
            this.reportDate = null;
            symbolSummaryReportStore.setDate(null);
        } else {
            symbolSummaryReportStore.setDate(reportDate);
        }
        initReportDataStore();
        symbolSummaryReportStore.reload();
        //initReportHeader();
        repaint();
    }

    public void SetFrameNotAdjusted() {
        isFrameNotAdjusted = true;
    }

    public void repaintReport() {
//        sHeading = null;
        initReportHeader();
        repaint();
    }

    private void adjustAlignments() {
        int[] newAlignments = new int[alignments.length];
        if (!Language.isLTR()) {
            for (int i = 0; i < alignments.length; i++) {
                if (alignments[i] == ReportUtilities.LEFT)
                    alignments[i] = ReportUtilities.RIGHT;
//                else if (alignments[i] == ReportUtilities.RIGHT)
//                    alignments[i] = ReportUtilities.LEFT;

                newAlignments[alignments.length - 1 - i] = alignments[i];
            }
            alignments = null;
            alignments = newAlignments;
            newAlignments = null;
        }
    }

    private void calculateColumnWidths(int width) {
        int tableWidth = width - 2 * leftMargin;
        columnWidths = null;
        columnWidths = new int[9];
        for (int i = 0; i < columnWidths.length; i++) {
            if (Language.isLTR())
                columnWidths[i] = (int) tableWidth * colPercents[i] / 100;
            else
                columnWidths[i] = (int) tableWidth * colPercents[columnWidths.length - 1 - i] / 100;
        }
    }

    public void paint(Graphics g) {
        try {
//            g.drawImage(bufferImage, 0, 0, panelWidth, panelHeight, this);
            drawReport(g, 0, symbols.length, false);
        } catch (Exception ex) {
        }
    }

    public void update(Graphics g) {
        this.paint(g);
    }

    private void drawReport(Graphics g, int startRow, int endRow, boolean isPrinting) {
        int curYPos = 0;
        int curXPos = 0;
        int curRowNumber = 0;
        Graphics2D g2D = null;
        int width = (int) getSize().getWidth();
        int height = (int) getSize().getHeight();

        try {
            g2D = (Graphics2D) g;
            g2D.setColor(Color.white); // new Color(200, 200, 200));
            g2D.fillRect(0, 0, width, height);
            g2D.setColor(Color.black); // new Color(200, 200, 200));

            if (ReportUtilities.isCustomizedVersion())
                headerHeight = 80;
            //drawLogo(g2D, 20, 20, 120, 46);
            curYPos = topMargin;
//            if (!isPrinting)
//                headerHeight -= 10;
            ReportUtilities.drawHeader(g2D, leftMargin, curYPos, width - 2 * leftMargin, headerHeight, isPrinting);
            curYPos += headerHeight + topMargin;
            if (isPrinting)
                headingHeight = ReportUtilities.drawHeading(sHeading, g2D, leftMargin + width / 2, curYPos + 5, ReportUtilities.FONT_PRINT_LARGEST);
            else
                headingHeight = ReportUtilities.drawHeading(sHeading, g2D, leftMargin + width / 2, curYPos + 5, ReportUtilities.FONT_LARGE);
            curYPos += headingHeight + topMargin;

            curYPos += ReportUtilities.drawTableHeader(columnWidths, header, g2D, leftMargin, curYPos, width, isPrinting);

//            if (isPrinting)
//                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM));
//            else
            g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));
            Stock stock = null;
            FontMetrics fm = g2D.getFontMetrics();
            int colCounter = 0;
            int status = ReportUtilities.NOCHANGE;
            boolean isEven = false;
            rowHeight = fm.getHeight() + fm.getDescent() + 2; // + 5; // + fm.getAscent() + fm.getDescent();

            for (int i = startRow; i < endRow; i++) {
                stock = symbolSummaryReportStore.getStockObject(symbols[i]);

                if (stock == null) {
                    continue;
                }
                oPriceFormat = getPriceFormatter(stock.getDecimalCount());
                oChangeFormat = getPriceFormatter(stock.getDecimalCount());
                oPChangeFormat = getPriceFormatter(stock.getDecimalCount());
                if ((reportDate == null) && (stock.getNoOfTrades() <= 0)) {
                    continue;
                }

                isEven = ((curRowNumber % 2) == 0);
                curRowNumber++;

                if (Language.isLTR()) {
                    if (isPrinting) {
                        g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM + 2));
                    }

                    if (showSubMarketID) {
                        curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()) + " (" + stock.getMarketID() + ")",
                                alignments[colCounter], g2D,
                                leftMargin, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    } else {
                        curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()),
//                        curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getCompanyCode(stock.getKey()),
                                alignments[colCounter], g2D,
                                leftMargin, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    }
                    if (isPrinting)
                        g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));

                    if (getReportDate() == null) { // current report
                    /*
                    16/06/2010	1.0	Initial Draft of the CR 	Prasad Maduranga Gunawardane
                    16/07/2010	1.1	The concern of displaying the previous days values were added as per the comments given by Madusha
                    Prasad Maduranga Gunawardane
                    Change done by Chandika Hewage
                    */
                        if (curExchange.getMarketStatus() == Constants.CLOSED) {
                            curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getTodaysClose()), alignments[colCounter], g2D,
                                    curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                        } else {
                            if (stock.getLastTradeValue() == 0) {
                                curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getPreviousClosed()), alignments[colCounter], g2D,
                                        curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                            } else {
                                curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getLastTradeValue()), alignments[colCounter], g2D,
                                        curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                            }
                        }
                    } else {
                        curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getTodaysClose()), alignments[colCounter], g2D,
                                curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    }
                    if (stock.getChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getChange()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, status, isEven, isPrinting);
                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oPChangeFormat.format(stock.getPercentChange()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, status, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getVolume()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getTurnover()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getTodaysOpen()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getHigh()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getLow()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                } else {
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getLow()), alignments[colCounter], g2D,
                            leftMargin, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getHigh()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getTodaysOpen()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getTurnover()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    curXPos = ReportUtilities.drawTableColumn(oQuantityFormat.format(stock.getVolume()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);

                    if (stock.getPercentChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getPercentChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oPChangeFormat.format(stock.getPercentChange()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, status, isEven, isPrinting);

                    if (stock.getChange() > 0)
                        status = ReportUtilities.UP;
                    else if (stock.getChange() < 0)
                        status = ReportUtilities.DOWN;
                    else
                        status = ReportUtilities.NOCHANGE;
                    curXPos = ReportUtilities.drawTableColumn(oChangeFormat.format(stock.getChange()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, status, isEven, isPrinting);


                    if (getReportDate() == null) { // current report
                        if (stock.getLastTradeValue() == 0) {
                            curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getPreviousClosed()), alignments[colCounter], g2D,
                                    curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                        } else {
                            curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getLastTradeValue()), alignments[colCounter], g2D,
                                    curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                        }
                    } else {
                        curXPos = ReportUtilities.drawTableColumn(oPriceFormat.format(stock.getTodaysClose()), alignments[colCounter], g2D,
                                curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    }

                    if (isPrinting)
                        g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM + 2));
                    else
                        g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM + 2));
                    curXPos = ReportUtilities.drawTableColumn(DataStore.getSharedInstance().getShortDescription(stock.getKey()), alignments[colCounter], g2D,
                            curXPos, curYPos, columnWidths[colCounter++], rowHeight, isEven, isPrinting);
                    if (isPrinting)
                        g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_PRINT_MEDIUM));
                    else
                        g2D.setFont(Theme.getDefaultFont(Font.PLAIN, ReportUtilities.FONT_MEDIUM));
                }

                curYPos += rowHeight;
                colCounter = 0;
                stock = null;
            }
            fm = null;

            if (Math.abs(curYPos - panelHeight) > 20) {
                panelHeight = curYPos + 10;
                this.setPreferredSize(new Dimension(width, panelHeight));
                this.setSize(new Dimension(width, panelHeight));
                ReportGenerator.refresh();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        g2D = null;

        if (isFrameNotAdjusted) {
//            System.out.println("curYPos = " + curYPos);
            parentWindow.setViewPortSize(curYPos + 5);
            isFrameNotAdjusted = false;
        }
    }

    private TWDecimalFormat getPriceFormatter(int decimals) {
        TWDecimalFormat oPriceFormat_1 = new TWDecimalFormat(" ###,##0.0  ");
        TWDecimalFormat oPriceFormat_2 = new TWDecimalFormat(" ###,##0.00  ");
        TWDecimalFormat oPriceFormat_3 = new TWDecimalFormat(" ###,##0.000  ");
        if (decimals == 1) {
            return oPriceFormat_1;
        } else if (decimals == 2) {
            return oPriceFormat_2;
        } else if (decimals == 3) {
            return oPriceFormat_3;
        } else {
            return oPriceFormat_2;
        }
    }

    /**
     * The method @print@ must be implemented for @Printable@ interface.
     * Parameters are supplied by system.
     */
    public int print(Graphics g, PageFormat pf, int pageIndex) throws
            PrinterException {

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black); //set default foreground color to black
        //for faster printing, turn off double buffering
        //RepaintManager.currentManager(this).setDoubleBufferingEnabled(false);

        Dimension d = this.getSize(); //get size of document
        double panelWidth = d.width; //width in pixels
        double panelHeight = d.height; //height in pixels

        double pageHeight = pf.getImageableHeight(); //height of printer page
        double pageWidth = pf.getImageableWidth(); //width of printer page

        double scale = pageWidth / panelWidth;
        int rowPerPage = (int) ((pageHeight - headerHeight - headingHeight) / (rowHeight * scale));
        int totalNumPages = Math.max((int) Math.ceil(symbols.length / (double) rowPerPage), 1);
//        int totalNumPages = (int) Math.ceil(scale * panelHeight / pageHeight);
        //make sure not print empty pages
        if (pageIndex >= totalNumPages) {
            return Printable.NO_SUCH_PAGE;
        }

        int iniRow = pageIndex * rowPerPage;
        int endRow = Math.min(symbols.length, iniRow + rowPerPage);

        //shift Graphic to line up with beginning of print-imageable region
        g2.translate(pf.getImageableX(), pf.getImageableY());

        //shift Graphic to line up with beginning of next page to print
        //g2.translate(0f, -pageIndex * pageHeight);

        //scale the page so the width fits...
        g2.scale(scale, scale);

        calculateColumnWidths((int) panelWidth);
        drawReport(g2, iniRow, endRow, true);

        g2 = null;

        return Printable.PAGE_EXISTS;
    }

    public void setMarket(Market market) {
        subMarket = market;
        checkSubMarket = true;
        showSubMarketID = false;
        isFrameNotAdjusted = true;

//        try {
//            symbolSummaryReportStore.reload();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            initReportDataStore();
            initReportHeader();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.repaintReport();
    }

    public void disposeReport() {
//        g_oDateTimeFormat   = null;
        oPriceFormat = null;
        oQuantityFormat = null;
        oChangeFormat = null;
        oPChangeFormat = null;

        sHeading = null;
        columnWidths = null;
        colPercents = null;
        symbols = null;
        header = null;
        alignments = null;
    }
}
