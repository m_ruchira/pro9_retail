package com.isi.csvr.entitlements;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 28, 2005
 * Time: 1:17:17 PM
 */
public class EntitlementRecord extends JLabel implements Comparable {
    public boolean subscribed;
    public int type;
    public String id;
    public String description;
    public int recordType;
    private String displayId;

    public EntitlementRecord(String text, Icon icon, int type, int recType) {
        super(text, icon, JLabel.LEADING);
        this.type = type;
        this.description = text;
        this.recordType = recType;
    }

    public EntitlementRecord(String text, boolean isSubscribed) {
        super(text, null, JLabel.LEADING);
        this.subscribed = isSubscribed;
        this.description = text;
    }

    public boolean isSubscribed() {
//        return true;
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public int compareTo(Object o) {
        return getText().compareTo(((JLabel) o).getText());
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }
}
