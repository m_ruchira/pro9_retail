package com.isi.csvr.entitlements;

import com.isi.csvr.shared.Language;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 02-Jan-2008 Time: 10:13:36 To change this template use File | Settings
 * | File Templates.
 */
public class NamedVector extends Vector implements Comparable {
    //    private boolean isExpired=false;
    public static final byte VALID_EXCHANGE = 0;
    public static final byte EXPIRED_EXCHANGE = 1;
    public static final byte INACTIVE_EXCHANGE = 2;
    private static final int infoType_EXP = 1024;
    private String name;
    private String id;
    private String displayid;
    private boolean isSubscribed = false;
    private byte type = 0;

    public NamedVector(String name, String id, String dId) {
        this.name = name;
        this.id = id;
        this.displayid = dId;
    }

    public NamedVector(String name, String id, String dId, Object elements[], boolean isSub) {
        this.name = name;
        this.id = id;
        this.displayid = dId;
        this.isSubscribed = isSub;
        sortVector(elements);
//    for (int i = 0, n = elements.length; i < n; i++) {
//      add(elements[i]);
//    }
//
//    Collections.sort(this);
    }

    public NamedVector(String name, String id, String dId, Hashtable hash, boolean isSub, byte type) {
        this.name = name;
        this.id = id;
        this.displayid = dId;
        this.isSubscribed = isSub;
        this.type = type;
//    this.isExpired=isExp;
        EntitlementRecord er = (EntitlementRecord) hash.remove(infoType_EXP);

        Enumeration en = hash.elements();
        while (en.hasMoreElements()) {
            add(en.nextElement());

        }
        Collections.sort(this);
        if (er != null) {
            add(0, er);
        }
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public boolean isExpired() {
        return (type == EXPIRED_EXCHANGE);
    }

    public boolean isInactive() {
        return (type == INACTIVE_EXCHANGE);
    }

    public String toString() {

        if (isSubscribed) {
            if (id != null) {
                return name + "-" + displayid;
            } else {
                return name;
            }
        } else {
            if (id != null) {
                return name + "-" + displayid;
            } else {
                return name;
            }
        }
    }

    public int compareTo(Object o) {
        return toString().compareTo(o.toString());
    }

    private void sortVector(Object[] arr) {
        try {
            Vector temp = new Vector();
            for (int i = 0; i < arr.length; i++) {
                if (arr[i].toString().equals(Language.getString("ENTITLEMENT_COMMON_FEATURES"))) {
                    temp.add(arr[i]);
                } else {
                    add(arr[i]);
                }
                Collections.sort(this);
            }
            add(0, temp.get(0));
        } catch (Exception e) {

        }

    }
}