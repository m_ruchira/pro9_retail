package com.isi.csvr.entitlements;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.Decompress;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 01-Jan-2008 Time: 17:40:49 To change this template use File | Settings
 * | File Templates.
 */
public class EntitlementsTree extends InternalFrame implements ChangeListener, MouseListener, ExchangeListener, Themeable, ActionListener {
    public static final int TYPE_EXPDATE = 2;
    private static final int TYPE_COMMON = 0;
    private static final int TYPE_EXCHANGE = 1;
    private static final int infoType_EXP = 1024;
    private static EntitlementsTree self = null;
    //    String[] exchangeFeatureArray = {Language.getString("TIME_AND_SALES"       ),
//                                     Language.getString("MARKET_DEPTH_BY_PRICE"),
//                                     Language.getString("MARKET_DEPTH_BY_ORDER"),
//                                     Language.getString("PRICE_CALC"           )  };
//    String[] commonFeatureArray = {Language.getString("HISTORY_CHARTING"          ),
//                                   Language.getString("PROFESSIONAL_CHARTING"     ),
//                                   Language.getString("HEATMAP_TITLE"             ),
//                                   Language.getString("TIME_N_SALES_HISTORY"      ),
//                                   Language.getString("ALERTS"                    ),
//                                   Language.getString("METASTOCK"                 ),
//                                   Language.getString("HISTORY_ANALYZER"          ),
//                                   Language.getString("LINK_TO_XL"                ),
    //                                   Language.getString("NEWS"                      ),
    //                                   Language.getString("CHAT_WITH_CUSTOMER_SUPPORT") };
    private Vector commonVector;
    private Vector rootVector;
    private Object rootNodes[];
    private Hashtable<Integer, EntitlementRecord> commonEntitlementStore;
    private Hashtable<Integer, EntitlementRecord> exchangeEntitlementStore;
    private Hashtable<String, EntitlementRecord> exchangeStore;
    private Hashtable<String, Hashtable> exchangeEntlStore;
    private JPanel bottomPanel;
    private JCheckBox subscriptionCheckBox;
    private JTree tree;
    private TreeCellRenderer renderer;
    private JPanel mainPanel;

    public EntitlementsTree() {
        setLayout(new BorderLayout());
        exchangeStore = new Hashtable<String, EntitlementRecord>();
        commonEntitlementStore = new Hashtable<Integer, EntitlementRecord>();
        exchangeEntitlementStore = new Hashtable<Integer, EntitlementRecord>();
        exchangeEntlStore = new Hashtable<String, Hashtable>();
        bottomPanel = new JPanel();
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        subscriptionCheckBox = new JCheckBox(Language.getString("ENTITLEMENT_SHOW_ONLY_SUBSCRIBED"));
        subscriptionCheckBox.addActionListener(this);
        add(mainPanel, BorderLayout.CENTER);

        loadExchanges();
        createEntilementRecords();
        applyCommonPreferences();
        applyExchangeSelections();
        createUI();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        setTitle(Language.getString("ENTITLEMENTS"));
        setClosable(true);
        setSize(375, 465);
        setResizable(true);
        GUISettings.applyOrientation(this);
        super.hideTitleBarMenu();
//        pack();
    }

    public static synchronized EntitlementsTree getSharedInstance() {
        return self;
    }

    public static void initialize() {
        if (self == null) {
            self = new EntitlementsTree();
        }
    }

    private void createUI() {
        subscriptionCheckBox.setSelected(true);
//        setLayout(new BorderLayout());
        toggleTree(subscriptionCheckBox.isSelected());
        showAllNodes(false);
//        mainPanel.setLayout(new BorderLayout());
//        rootNodes = setExchangeVectors();
//        rootVector  = new NamedVector("Root",null, rootNodes,true);
//        tree        = new JTree(rootVector) ;
//        renderer = new EntitlementRenderer();
//        tree.setCellRenderer(renderer);
//        JScrollPane scrollPane  = new JScrollPane(tree);
//        mainPanel.add(scrollPane, BorderLayout.CENTER);
//        mainPanel.add(createBottomPanel(), BorderLayout.SOUTH);

    }

    public void showAllNodes(boolean forceExpand) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) tree.getModel().getRoot();
        Enumeration<DefaultMutableTreeNode> children = root.children();
        if ((forceExpand) || (root.getChildCount() < 4)) {
            while (children.hasMoreElements()) {
                tree.expandPath(new TreePath(children.nextElement().getPath()));
            }
        }
        //tree.expandPath();
    }

    private Object[] setExchangeVectors() {
        commonVector = new NamedVector(Language.getString("ENTITLEMENT_COMMON_FEATURES"), null, null, commonEntitlementStore, true, NamedVector.VALID_EXCHANGE);
        Vector tempVector = new Vector();
        tempVector.add(commonVector);
        Enumeration<EntitlementRecord> exchanges = exchangeStore.elements();
        while (exchanges.hasMoreElements()) {
            Vector exgVector;
            EntitlementRecord record = exchanges.nextElement();
            if (ExchangeStore.getSharedInstance().getExchange(record.getId()) != null) {
                if (ExchangeStore.getSharedInstance().getExchange(record.getId()).isExpired()) {
                    exgVector = new NamedVector(record.getDescription(), record.getId(), record.getDisplayId(), exchangeEntlStore.get(record.getId()), true, NamedVector.EXPIRED_EXCHANGE);
                } else if (ExchangeStore.getSharedInstance().getExchange(record.getId()).isInactive()) {
                    exgVector = new NamedVector(record.getDescription(), record.getId(), record.getDisplayId(), exchangeEntlStore.get(record.getId()), true, NamedVector.INACTIVE_EXCHANGE);
                } else {
                    exgVector = new NamedVector(record.getDescription(), record.getId(), record.getDisplayId(), exchangeEntlStore.get(record.getId()), true, NamedVector.VALID_EXCHANGE);
                }

            } else {
                exgVector = new NamedVector(record.getDescription(), record.getId(), record.getDisplayId(), new String[]{}, false);
            }
            tempVector.add(exgVector);
            exgVector = null;
            record = null;
        }
        exchanges = null;
        Object[] nodes = tempVector.toArray();
        return nodes;
    }

    private Object[] setSubscribedExchanges() {
        commonVector = new NamedVector(Language.getString("ENTITLEMENT_COMMON_FEATURES"), null, null, commonEntitlementStore, true, NamedVector.VALID_EXCHANGE);
        Vector tempVector = new Vector();
        tempVector.add(commonVector);
        Enumeration<EntitlementRecord> exchanges = exchangeStore.elements();
        while (exchanges.hasMoreElements()) {
            Vector exgVector;
            EntitlementRecord record = exchanges.nextElement();
            if (ExchangeStore.getSharedInstance().getExchange(record.getId()) != null) {
                if (ExchangeStore.getSharedInstance().getExchange(record.getId()).isExpired()) {
                    exgVector = new NamedVector(record.getDescription(), record.getId(), record.getDisplayId(), exchangeEntlStore.get(record.getId()), true, NamedVector.EXPIRED_EXCHANGE);
                } else if (ExchangeStore.getSharedInstance().getExchange(record.getId()).isInactive()) {
                    exgVector = new NamedVector(record.getDescription(), record.getId(), record.getDisplayId(), exchangeEntlStore.get(record.getId()), true, NamedVector.INACTIVE_EXCHANGE);
                } else {
                    exgVector = new NamedVector(record.getDescription(), record.getId(), record.getDisplayId(), exchangeEntlStore.get(record.getId()), true, NamedVector.VALID_EXCHANGE);
                }
                tempVector.add(exgVector);
                exgVector = null;

            }
            record = null;
        }
        exchanges = null;
        Object[] nodes = tempVector.toArray();
        return nodes;
    }

    public void loadExchanges() {
//        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        int count = ExchangeStore.getSharedInstance().count();
        for (int i = 0; i < count; i++) {
//        while (exchanges.hasMoreElements()) {
            try {
                Exchange exchangeTemp = ExchangeStore.getSharedInstance().getExchange(i);
//                Exchange exchangeTemp = (Exchange) exchanges.nextElement();
                Decompress decompress = new Decompress();
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchangeTemp.getSymbol() + "/exchange_" + Language.getSelectedLanguage() + ".msf";
                ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
                decompress.decompress();
                DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
                String record = null;

                do {
                    record = in.readLine();
                    if (record != null) {
                        String[] fields = record.split(Meta.DS);
                        if (fields.length <= 1)
                            continue; // it is possible to have garbage in the file. Never trust Servers
                        String description = Language.getLanguageSpecificString(fields[3]);
                        description = UnicodeUtils.getNativeString(description);
                        EntitlementRecord entitlementRecord =
                                new EntitlementRecord(description, null, 0, 0);
                        entitlementRecord.setId(fields[0]); // set the symbol
                        entitlementRecord.setDisplayId(fields[10]); // set the symbol
                        entitlementRecord.setOpaque(true);
                        entitlementRecord.setForeground(Theme.getColor("MENU_FGCOLOR"));
                        entitlementRecord.addMouseListener(this);
                        exchangeStore.put(entitlementRecord.getId(), entitlementRecord);
                        exchangeEntlStore.put(entitlementRecord.getId(), getNewExchagePreference(entitlementRecord.getId()));
                        description = null;
                        fields = null;
                    }
                } while (record != null);

                out = null;
                decompress = null;
                in.close();
                in = null;
                exchangeTemp = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public JPanel createBottomPanel() {
        bottomPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        bottomPanel.add(subscriptionCheckBox);
        bottomPanel.setVisible(false);
        return bottomPanel;
    }

    public void createEntilementRecords() {
        //common list
        createItem(TYPE_COMMON, Language.getString("HISTORY_CHARTING"), Constants.APPLICATION_EXCHANGE, Meta.IT_HistoryCharting);
        createItem(TYPE_COMMON, Language.getString("PROFESSIONAL_CHARTING"), Constants.APPLICATION_EXCHANGE, Meta.IT_ProfessionalCharting);
        createItem(TYPE_COMMON, Language.getString("HEATMAP_TITLE"), Constants.APPLICATION_EXCHANGE, Meta.IT_MarketMap);
        createItem(TYPE_COMMON, Language.getString("TIME_N_SALES_HISTORY"), Constants.APPLICATION_EXCHANGE, Meta.IT_TimeAndSalesHistory);
        createItem(TYPE_COMMON, Language.getString("ALERTS"), Constants.APPLICATION_EXCHANGE, Meta.IT_Alerts);
        createItem(TYPE_COMMON, Language.getString("PORTFOLIO_SIMULATOR"), Constants.APPLICATION_EXCHANGE, Meta.IT_Portfolio);
        createItem(TYPE_COMMON, Language.getString("FUNCTION_LISTS"), Constants.APPLICATION_EXCHANGE, Meta.IT_FunctionBuilder);
//        createItem(TYPE_COMMON, Language.getString("METASTOCK"), Constants.APPLICATION_EXCHANGE, Meta.IT_MetaStock);
        createItem(TYPE_COMMON, Language.getString("HISTORY_ANALYZER"), Constants.APPLICATION_EXCHANGE, Meta.IT_HistoryAnalyzer);
//        createItem(TYPE_COMMON, Language.getString("TIME_LAG_INDICATOR_WINDOW"), Constants.APPLICATION_EXCHANGE, Meta.IT_LatencyWindow);
        if (TWControl.isCSChatEnable()) {
            createItem(TYPE_COMMON, Language.getString("CHAT_WITH_CUSTOMER_SUPPORT"), Constants.APPLICATION_EXCHANGE, Meta.IT_Chat_Support);
        }
//        createItem(TYPE_COMMON, Language.getString("LINK_TO_XL"), Constants.APPLICATION_EXCHANGE, Meta.IT_DDE_Link);
//        createItem(TYPE_COMMON, Language.getString("ENABLE_TRADE_TICKER"), Constants.APPLICATION_EXCHANGE, Meta.IT_TradeTicker);
        createItem(TYPE_COMMON, Language.getString("OHLC_DOWNLOADER"), Constants.APPLICATION_EXCHANGE, Meta.IT_OHLCHistory);
//        createItem(TYPE_COMMON, Language.getString("PRICE_API"), Constants.APPLICATION_EXCHANGE, Meta.IT_PriceAPI);
//        createItem(TYPE_COMMON, Language.getString("CASHFLOW_WATCHER"), Constants.APPLICATION_EXCHANGE, Meta.IT_CashFlow_View);
//        createItem(TYPE_COMMON, Language.getString("VOLUME_WATCHER"), Constants.APPLICATION_EXCHANGE, Meta.IT_VolumnWatch_View);
        createItem(TYPE_COMMON, Language.getString("GLOBAL_MARKET_SUMMARY"), Constants.APPLICATION_EXCHANGE, Meta.IT_Global_Market_Summary);
//        createItem(TYPE_COMMON, Language.getString("CONFIGURE"), Constants.APPLICATION_EXCHANGE, Meta.IT_Config);
        createItem(TYPE_COMMON, Language.getString("NEWS"), Constants.APPLICATION_EXCHANGE, Meta.IT_News);
        createItem(TYPE_COMMON, Language.getString("MARKET_SUMMARY"), Constants.APPLICATION_EXCHANGE, Meta.IT_Market_Summary);
        createItem(TYPE_COMMON, Language.getString("MARKET_INDICES"), Constants.APPLICATION_EXCHANGE, Meta.IT_Market_indices);
        createItem(TYPE_COMMON, Language.getString("RADAR_WATCHLIST"), Constants.APPLICATION_EXCHANGE, Meta.IT_Strategy_Watchlist);
        createItem(TYPE_COMMON, Language.getString("TOP_STOCKS"), Constants.APPLICATION_EXCHANGE, Meta.IT_Topstock);
        createItem(TYPE_COMMON, Language.getString("VAS_MENU"), Constants.APPLICATION_EXCHANGE, Meta.IT_VAS);

        //exchange list
//        createItem(TYPE_EXPDATE, "Expiary Date", Constants.APPLICATION_EXCHANGE, infoType_EXP   );
//        createItem(TYPE_EXCHANGE, Language.getString("TIME_AND_SALES"       ), Constants.APPLICATION_EXCHANGE, Meta.IT_SymbolTimeAndSales      );
//        createItem(TYPE_EXCHANGE, Language.getString("MARKET_DEPTH_BY_PRICE"), Constants.APPLICATION_EXCHANGE, Meta.IT_MarketDepthByPrice);
//        createItem(TYPE_EXCHANGE, Language.getString("MARKET_DEPTH_BY_ORDER"), Constants.APPLICATION_EXCHANGE, Meta.IT_MarketDepthByOrder);
//        createItem(TYPE_EXCHANGE, Language.getString("PRICE_CALC"           ), Constants.APPLICATION_EXCHANGE, Meta.IT_DepthCalculator   );


    }

    public Hashtable getNewExchagePreference(String exchange) {
        Hashtable<Integer, EntitlementRecord> exgPref = new Hashtable<Integer, EntitlementRecord>();
        exgPref.put(infoType_EXP, new EntitlementRecord("Expiary Date", null, infoType_EXP, TYPE_EXPDATE));
        Enumeration<String> features = ExchangeStore.getSharedInstance().getExchangeFeatures(exchange);
        System.out.println("entitlements for exchange==" + exchange);
        while (features.hasMoreElements()) {
            try {
                int windowType = Integer.parseInt(features.nextElement());
                System.out.println("windowType ==" + windowType);
                switch (windowType) {
                    case Meta.IT_SymbolTimeAndSales:
                        exgPref.put(Meta.IT_SymbolTimeAndSales, new EntitlementRecord(Language.getString("TIME_AND_SALES"), null, Meta.IT_SymbolTimeAndSales, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_MarketDepthByPrice:
                        exgPref.put(Meta.IT_MarketDepthByPrice, new EntitlementRecord(Language.getString("MARKET_DEPTH_BY_PRICE"), null, Meta.IT_MarketDepthByPrice, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_MarketDepthByOrder:
                        exgPref.put(Meta.IT_MarketDepthByOrder, new EntitlementRecord(Language.getString("MARKET_DEPTH_BY_ORDER"), null, Meta.IT_MarketDepthByOrder, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_DepthCalculator:
                        exgPref.put(Meta.IT_DepthCalculator, new EntitlementRecord(Language.getString("PRICE_CALC"), null, Meta.IT_DepthCalculator, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_CompanyProfile:
                        exgPref.put(Meta.IT_CompanyProfile, new EntitlementRecord(Language.getString("COMPANY_PROFILE"), null, Meta.IT_CompanyProfile, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_SpecialOrderBook:
                        exgPref.put(Meta.IT_SpecialOrderBook, new EntitlementRecord(Language.getString("SPECIAL_ORDER_BOOK"), null, Meta.IT_SpecialOrderBook, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_TimeAndSalesBacklog:
                        exgPref.put(Meta.IT_TimeAndSalesBacklog, new EntitlementRecord(Language.getString("TIME_AND_SALES_BACKLOG"), null, Meta.IT_TimeAndSalesBacklog, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_MarketTimeAndSales:
                        exgPref.put(Meta.IT_MarketTimeAndSales, new EntitlementRecord(Language.getString("MARKET_TIME_N_SALES"), null, Meta.IT_MarketTimeAndSales, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_StockReports:
                        exgPref.put(Meta.IT_StockReports, new EntitlementRecord(Language.getString("POPUP_STOCKREPORTS"), null, Meta.IT_StockReports, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_RegionalQuotes:
                        exgPref.put(Meta.IT_RegionalQuotes, new EntitlementRecord(Language.getString("POPUP_REGIONAL_QUOTES"), null, Meta.IT_RegionalQuotes, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_FundamentalData:
                        exgPref.put(Meta.IT_FundamentalData, new EntitlementRecord(Language.getString("POPUP_FD2"), null, Meta.IT_FundamentalData, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_CorporateActions:
                        exgPref.put(Meta.IT_CorporateActions, new EntitlementRecord(Language.getString("CORPORATE_ACTIONS"), null, Meta.IT_CorporateActions, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_OptionChain:
                        exgPref.put(Meta.IT_OptionChain, new EntitlementRecord(Language.getString("POPUP_OPTIONS"), null, Meta.IT_OptionChain, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_FuturesChain:
                        exgPref.put(Meta.IT_FuturesChain, new EntitlementRecord(Language.getString("POPUP_FUTURES"), null, Meta.IT_FuturesChain, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_CashFlow:
                        exgPref.put(Meta.IT_CashFlow, new EntitlementRecord(Language.getString("CASHFLOW_DQ"), null, Meta.IT_CashFlow, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_FullQuote:
                        exgPref.put(Meta.IT_FullQuote, new EntitlementRecord(Language.getString("MULTI_DIALOG_MENU"), null, Meta.IT_FullQuote, TYPE_EXCHANGE));
                        break;
                    case Meta.IT_TimeAndSales_Summary:
                        exgPref.put(Meta.IT_TimeAndSales_Summary, new EntitlementRecord(Language.getString("TRADE_SUMMARY"), null, Meta.IT_TimeAndSales_Summary, TYPE_EXCHANGE));
                        break;
//                    case Meta.IT_Topstock:
//                        exgPref.put(Meta.IT_Topstock, new EntitlementRecord(Language.getString("TOP_STOCKS"), null, Meta.IT_Topstock, TYPE_EXCHANGE));
//                        break;
//                    case Meta.IT_Market_Summary:
//                        exgPref.put(Meta.IT_Market_Summary, new EntitlementRecord(Language.getString("MARKET_SUMMARY"), null, Meta.IT_Market_Summary, TYPE_EXCHANGE));
//                        break;
//                    case Meta.IT_Market_indices:
//                        exgPref.put(Meta.IT_Market_indices, new EntitlementRecord(Language.getString("MARKET_INDICES"), null, Meta.IT_Market_indices, TYPE_EXCHANGE));
//                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return exgPref;
    }

    private JLabel createItem(int type, String description, String exchange, int infoType) {
        EntitlementRecord record;
        if (type == TYPE_COMMON) {
            record = new EntitlementRecord(description, null, infoType, type);
            record.setBackground(Theme.getColor("MENU_BGCOLOR"));
            record.setForeground(Theme.getColor("MENU_FGCOLOR"));
            commonEntitlementStore.put(infoType, record);
        } else if (type == TYPE_EXPDATE) {
            record = new EntitlementRecord(description, null, infoType, type);
            exchangeEntitlementStore.put(infoType, record);
        } else {
            record = new EntitlementRecord(description, null, infoType, type);
            exchangeEntitlementStore.put(infoType, record);
        }
        return record;

    }

    private void applyCommonPreferences() {
        try {
            Exchange applicationExchange = ExchangeStore.getSharedInstance().getApplicationExchange();
            applyExchangePreferences(TYPE_COMMON, applicationExchange);

            if (applicationExchange.isValidIinformationType(Meta.IT_ProfessionalCharting)) {
                EntitlementRecord record = getItem(Constants.APPLICATION_EXCHANGE, Meta.IT_HistoryCharting);
                if (record != null) {
                    record.subscribed = true;
                }
            }
            applicationExchange = null;
        } catch (Exception e) {
        }

    }

    private synchronized void applyExchangePreferences(int type, Exchange exchange) {
        try {
            Enumeration<EntitlementRecord> infoRecords;
            if (type == TYPE_COMMON) {
                infoRecords = commonEntitlementStore.elements();
            } else {
                infoRecords = exchangeEntlStore.get(exchange.getSymbol()).elements(); //exchangeEntitlementStore.elements();
            }
            while (infoRecords.hasMoreElements()) {
                EntitlementRecord record = infoRecords.nextElement();
                if ((exchange != null) || (exchange.getInfoTypes() != null)) {
                    if ((exchange.isValidIinformationType(record.type))) {
                        record.subscribed = true;
                        record.setSubscribed(true);
                    } else {
                        record.subscribed = false;
                        record.setSubscribed(false);
                    }
                }

//                record = null;
            }
            infoRecords = null;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ENTL");
        }
    }

    private void applyExchangeSelections() {
        Enumeration<EntitlementRecord> exchanges = exchangeStore.elements();
        while (exchanges.hasMoreElements()) {
            EntitlementRecord record = exchanges.nextElement();
            if (ExchangeStore.getSharedInstance().getExchange(record.getId()) != null) {
                record.subscribed = true;
                applyExchangePreferences(TYPE_EXCHANGE, ExchangeStore.getSharedInstance().getExchange(record.getId()));
            } else {
                record.subscribed = false;
            }
            record = null;

        }
        exchanges = null;
    }

    private EntitlementRecord getItem(String exchange, int type) {
        return commonEntitlementStore.get(type);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(subscriptionCheckBox)) {
            if (subscriptionCheckBox.isSelected()) {
                toggleTree(true);


            } else {
                toggleTree(false);
            }

        }


    }

    public void toggleTree(boolean subscribed) {
        applyCommonPreferences();
        applyExchangeSelections();
        if (subscribed) {
            rootNodes = setSubscribedExchanges();
        } else {
            rootNodes = setExchangeVectors();
        }
        try {
            mainPanel.removeAll();
        } catch (Exception e) {
            e.printStackTrace(); //To change body of catch statement use File | Settings | File Templates.
        }
        rootVector = new NamedVector("Root", null, null, rootNodes, true);
        tree = new JTree(rootVector);
        tree.putClientProperty("JTree.lineStyle", "Vertical");
        renderer = new EntitlementRenderer();
        tree.setCellRenderer(renderer);
        tree.setToggleClickCount(1);
        JScrollPane scrollPane = new JScrollPane(tree);
        mainPanel.add(scrollPane, BorderLayout.CENTER);
        mainPanel.add(createBottomPanel(), BorderLayout.SOUTH);
        GUISettings.applyOrientation(mainPanel);
        tree.updateUI();
        tree.repaint();
        mainPanel.repaint();
        mainPanel.updateUI();
        this.repaint();
    }

    public void exchangeAdded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesAdded(boolean offlineMode) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
        applyExchangeSelections();
        applyCommonPreferences();
//        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(0);
//        selectedExchange = exchange.getSymbol();
//        selectExchange();
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void stateChanged(ChangeEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        super.internalFrameClosing(e);    //To change body of overridden methods use File | Settings | File Templates.
        this.dispose();
        self = null;
    }
}
