package com.isi.csvr.entitlements;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 02-Jan-2008 Time: 14:37:51 To change this template use File | Settings
 * | File Templates.
 */

public class EntitlementRenderer implements TreeCellRenderer, Themeable {
    private static Color unsubscribedForgoundColor;
    private static ImageIcon g_oLeafIcon_sub = null;
    private static ImageIcon g_oLeafIcon_unSub = null;
    private static ImageIcon g_oNewIcon = null;
    private static ImageIcon g_oClosedIcon = null;
    private static ImageIcon g_oExpandedIcon = null;
    private JLabel titleLabel;
    private JLabel renderer;
    private DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
    private Color backgroundSelectionColor;
    private Color backgroundNonSelectionColor;
    private String path = System.getProperties().getProperty("user.dir").replaceAll("\\\\", "/");

    public EntitlementRenderer() {
        reload();
        renderer = new JLabel("");
        renderer.setForeground(Color.blue);
        backgroundSelectionColor = defaultRenderer.getBackgroundSelectionColor();
        backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();

        try {
            g_oLeafIcon_sub = new ImageIcon("images/common/tick.gif");
            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            g_oNewIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeLeafNew.gif");
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            unsubscribedForgoundColor = Theme.getColor("TAB_PANE_UNSELECTED_FGCOLOR");
        } catch (Exception e) {

        }
    }

    public static void reload() {
        try {
            g_oLeafIcon_sub = new ImageIcon("images/common/tick.gif");
            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            g_oNewIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeLeafNew.gif");
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            unsubscribedForgoundColor = Theme.getColor("TAB_PANE_UNSELECTED_FGCOLOR");
        } catch (Exception e) {

        }
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean selected, boolean expanded,
                                                  boolean leaf, int row,
                                                  boolean hasFocus) {
        Component returnValue = null;
        if ((value != null) && (value instanceof DefaultMutableTreeNode)) {
            Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
            if (userObject instanceof EntitlementRecord) {
                EntitlementRecord er = (EntitlementRecord) userObject;
                if (er.isSubscribed()) {
                    renderer.setForeground(Color.GREEN);
                    renderer.setIcon(g_oLeafIcon_sub);
                    renderer.setText("<HTML><B><font color=green>" + " " + er.getDescription()); //<img SRC='file:"+path+"/images/common/tick.gif'>
                } else {

                    if (er.recordType == EntitlementsTree.TYPE_EXPDATE) {
                        String[] exgCode = (((DefaultMutableTreeNode) value).getParent().toString()).split("-");
                        Exchange exg = ExchangeStore.getSharedInstance().getExchangeByDisplayExchange(exgCode[exgCode.length - 1]);
                        if (exg != null) {
                            if (exg.isExpired()) {
                                renderer.setIcon(null);
                                renderer.setText("<HTML><B><font color=Black>" + " " + Language.getString("ENTITLEMENT_EXPIRED_ON") + " " + "<font color=red>" + exg.getExpiaryDate());
                            } else if (exg.isInactive()) {
                                renderer.setIcon(null);
                                renderer.setText("<HTML><B><font color=Black>" + " " + Language.getString("ENTITLEMENT_INACTIVE") + " " + "<font color=red>" + exg.getExpiaryDate());      //+"<font color=red>"+exg.getExpiaryDate()
                            } else {
                                renderer.setIcon(null);
                                renderer.setText("<HTML><B><font color=Black>" + " " + Language.getString("ENTITLEMENT_EXPIRES_ON") + " " + exg.getExpiaryDate());
                            }
                        }
                        exg = null;
                    } else {
                        renderer.setForeground(Color.RED);
                        renderer.setIcon(g_oLeafIcon_unSub);
                        renderer.setText("<HTML><B><font color=red>" + " " + er.getDescription()); //<img SRC='file:"+path+"/images/common/untick.gif'>
                    }
                }
                if (selected) {
                    renderer.setBackground(backgroundSelectionColor);
                } else {
                    renderer.setBackground(backgroundNonSelectionColor);
                }
                renderer.setEnabled(tree.isEnabled());
                returnValue = renderer;
            } else if (userObject instanceof NamedVector) {
                NamedVector nv = (NamedVector) userObject;
                if (expanded) {
                    renderer.setIcon(g_oExpandedIcon);
                    if (nv.isSubscribed()) {
                        renderer.setForeground(Color.GREEN);
                        if (nv.isExpired()) {
                            renderer.setText("<HTML><B><font color=black>" + " " + nv.toString() + "<B><font color=red>" + " (" + Language.getString("BRODCAST_MESSAGE_EXPIRED") + ") ");
                        } else if (nv.isInactive()) {
                            renderer.setText("<HTML><B><font color=black>" + " " + nv.toString() + "<B><font color=red>" + " (" + Language.getString("BRODCAST_MESSAGE_INACTIVE") + ") ");
                        } else {
                            renderer.setText("<HTML><B><font color=black>" + " " + nv.toString());
                        }
                    } else {
                        renderer.setForeground(unsubscribedForgoundColor);
                        renderer.setText(nv.toString() + " (" + Language.getString("ENTITLEMENT_NOT_SUBSCRIBED") + " )");
//                        renderer.setText("<HTML><B><font color=black>" + " " + nv.toString() + "<B><font color=red>" + " (" + Language.getString("ENTITLEMENT_NOT_SUBSCRIBED") + " )");
                    }
                } else {
                    renderer.setIcon(g_oClosedIcon);
                    if (nv.isSubscribed()) {
                        if (nv.isExpired()) {
                            renderer.setText("<HTML><B><font color=black>" + " " + nv.toString() + "<B><font color=red>" + " (" + Language.getString("BRODCAST_MESSAGE_EXPIRED") + ") ");
                        } else if (nv.isInactive()) {
                            renderer.setText("<HTML><B><font color=black>" + " " + nv.toString() + "<B><font color=red>" + " (" + Language.getString("BRODCAST_MESSAGE_INACTIVE") + ") ");
                        } else {
                            renderer.setText("<HTML><B><font color=black>" + " " + nv.toString());
                        }
                    } else {
                        renderer.setForeground(unsubscribedForgoundColor);
                        renderer.setText(nv.toString() + " (" + Language.getString("ENTITLEMENT_NOT_SUBSCRIBED") + " )");
//                        renderer.setText("<HTML><B><font color=black>" + " " + nv.toString() + "<B><font color=red>" + " (" + Language.getString("ENTITLEMENT_NOT_SUBSCRIBED") + " )");
                    }
                }
                renderer.setEnabled(tree.isEnabled());
                returnValue = renderer;
            }
        }
        if (returnValue == null) {
            returnValue = defaultRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }
        return returnValue;
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        try {
            g_oLeafIcon_sub = new ImageIcon("images/common/tick.gif");
            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            g_oNewIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeLeafNew.gif");
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            unsubscribedForgoundColor = Theme.getColor("TAB_PANE_UNSELECTED_FGCOLOR");
        } catch (Exception e) {

        }
    }
}