package com.isi.csvr.entitlements;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.Decompress;
import com.isi.util.ColumnLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 28, 2005
 * Time: 10:27:57 AM
 */
public class Entitlements extends InternalFrame
        implements ChangeListener, MouseListener, ExchangeListener, Themeable {
    private static final int TYPE_COMMON = 0;
    private static final int TYPE_EXCHANGE = 1;
    private static Entitlements self = null;
    private ImageIcon subscribedIcon;
    private ImageIcon unsubscribedIcon;
    private Hashtable<Integer, EntitlementRecord> commonEntitlementStore;
    private Hashtable<String, EntitlementRecord> exchangeStore;
    private Hashtable<Integer, EntitlementRecord> exchangeEntitlementStore;
    private JTabbedPane tabbedPane;
    private JPanel defaultPanel;
    private JPanel exchangeSubscriptioPanel;
    private JPanel exchangePanel;
    private JPanel subscriptionPanel;
    private String selectedExchange;

    public Entitlements() {
        commonEntitlementStore = new Hashtable<Integer, EntitlementRecord>();
        exchangeEntitlementStore = new Hashtable<Integer, EntitlementRecord>();
        exchangeStore = new Hashtable<String, EntitlementRecord>();
        subscribedIcon = new ImageIcon("images/common/tick.gif");
        unsubscribedIcon = new ImageIcon("images/common/untick.gif");

        loadExchanges();
        createUI();
        applyCommonPreferences();
        applyExchangeSelections();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Theme.registerComponent(this);
    }

    public static synchronized Entitlements getSharedInstance() {
        return self;
    }

    public static void initialize() {
        if (self == null) {
            self = new Entitlements();
        }
    }

    private void createUI() {
        setLayout(new BorderLayout());

        // create the Tab Pane
        tabbedPane = new JTabbedPane();
        tabbedPane.addChangeListener(this);
        add(tabbedPane, BorderLayout.NORTH);

        //create the default info types pane
        defaultPanel = new JPanel(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        defaultPanel.setOpaque(true);
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("HISTORY_CHARTING"), Constants.APPLICATION_EXCHANGE, Meta.IT_HistoryCharting));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("PROFESSIONAL_CHARTING"), Constants.APPLICATION_EXCHANGE, Meta.IT_ProfessionalCharting));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("HEATMAP_TITLE"), Constants.APPLICATION_EXCHANGE, Meta.IT_MarketMap));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("TIME_N_SALES_HISTORY"), Constants.APPLICATION_EXCHANGE, Meta.IT_TimeAndSalesHistory));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("ALERTS"), Constants.APPLICATION_EXCHANGE, Meta.IT_Alerts));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("METASTOCK"), Constants.APPLICATION_EXCHANGE, Meta.IT_MetaStock));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("HISTORY_ANALYZER"), Constants.APPLICATION_EXCHANGE, Meta.IT_HistoryAnalyzer));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("LINK_TO_XL"), Constants.APPLICATION_EXCHANGE, Meta.IT_DDE_Link));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("NEWS"), Constants.APPLICATION_EXCHANGE, Meta.IT_News));
        defaultPanel.add(createItem(TYPE_COMMON, Language.getString("CHAT_WITH_CUSTOMER_SUPPORT"), Constants.APPLICATION_EXCHANGE, Meta.IT_Chat_Support));
        tabbedPane.addTab(Language.getString("COMMON"), defaultPanel);

        //create the exchange vise subscription types pane
        exchangeSubscriptioPanel = new JPanel(new BorderLayout());
        tabbedPane.addTab(Language.getString("EXCHANGES"), exchangeSubscriptioPanel);

        exchangePanel = new JPanel(new ColumnLayout());
        exchangePanel.setOpaque(true);
        Enumeration<EntitlementRecord> exchanges = exchangeStore.elements();
        ArrayList<EntitlementRecord> list = new ArrayList<EntitlementRecord>();
        while (exchanges.hasMoreElements()) {
            list.add(exchanges.nextElement());
        }
        Collections.sort(list);
        for (EntitlementRecord record : list) {
            exchangePanel.add(record);
        }
        exchangeSubscriptioPanel.add(exchangePanel, BorderLayout.CENTER);
        list = null;

        subscriptionPanel = new JPanel(new ColumnLayout());
        subscriptionPanel.setOpaque(true);
        subscriptionPanel.setBackground(Theme.MENU_SELECTION_COLOR);
        subscriptionPanel.add(createItem(TYPE_EXCHANGE, Language.getString("TIME_AND_SALES"), Constants.APPLICATION_EXCHANGE, Meta.IT_SymbolTimeAndSales));
        subscriptionPanel.add(createItem(TYPE_EXCHANGE, Language.getString("MARKET_DEPTH_BY_PRICE"), Constants.APPLICATION_EXCHANGE, Meta.IT_MarketDepthByPrice));
        subscriptionPanel.add(createItem(TYPE_EXCHANGE, Language.getString("MARKET_DEPTH_BY_ORDER"), Constants.APPLICATION_EXCHANGE, Meta.IT_MarketDepthByOrder));
        subscriptionPanel.add(createItem(TYPE_EXCHANGE, Language.getString("PRICE_CALC"), Constants.APPLICATION_EXCHANGE, Meta.IT_DepthCalculator));
//        subscriptionPanel.add(createItem(TYPE_EXCHANGE, Language.getString("COMPANY_PROFILE"), Constants.APPLICATION_EXCHANGE, Meta.IT_CompanyProfile));
        if (Language.isLTR()) {
            exchangeSubscriptioPanel.add(subscriptionPanel, BorderLayout.EAST);
        } else {
            exchangeSubscriptioPanel.add(subscriptionPanel, BorderLayout.WEST);
        }

        GUISettings.applyOrientation(this);
        setTitle(Language.getString("ENTITLEMENTS"));
        setClosable(true);
        GUISettings.applyOrientation(this);
        pack();
    }

    private JLabel createItem(int type, String description, String exchange, int infoType) {
        EntitlementRecord record = new EntitlementRecord(description, unsubscribedIcon, infoType, 0);
        if (type == TYPE_COMMON) {
            record.setBackground(Theme.getColor("MENU_BGCOLOR"));
            record.setForeground(Theme.getColor("MENU_FGCOLOR"));
            commonEntitlementStore.put(infoType, record);
        } else {
            exchangeEntitlementStore.put(infoType, record);
        }
        return record;
        /*if (subscribed){
            return new JLabel(description, subscribedIcon, SwingConstants.LEADING);
        }else{
            return new JLabel(description, unsubscribedIcon, SwingConstants.LEADING);
        }*/
    }

    private EntitlementRecord getItem(String exchange, int type) {
        return commonEntitlementStore.get(type);
    }

    /*private Hashtable<Integer, EntitlementRecord> getExchangeStore(String exchange) {
        Hashtable<Integer, EntitlementRecord> exchangeStore = commonEntitlementStore.get(exchange);
        if (exchangeStore == null) {
            exchangeStore = new Hashtable<Integer, EntitlementRecord>();
            commonEntitlementStore.put(exchange, exchangeStore);
        }
        return exchangeStore;
    }*/

    private void applyCommonPreferences() {
        try {
            Exchange applicationExchange = ExchangeStore.getSharedInstance().getApplicationExchange();
            applyExchangePreferences(TYPE_COMMON, applicationExchange);

            if (applicationExchange.isValidIinformationType(Meta.IT_ProfessionalCharting)) {
                EntitlementRecord record = getItem(Constants.APPLICATION_EXCHANGE, Meta.IT_HistoryCharting);
                if (record != null) {
                    record.subscribed = true;
                    record.setIcon(subscribedIcon);
                }
            }
            applicationExchange = null;
        } catch (Exception e) {
        }

        /*Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            applyExchangePreferences(exchange);
            exchange = null;
        }
        exchanges = null;*/
    }

    private synchronized void applyExchangePreferences(int type, Exchange exchange) {
        try {
            Enumeration<EntitlementRecord> infoRecords;
            if (type == TYPE_COMMON) {
                infoRecords = commonEntitlementStore.elements();
            } else {
                infoRecords = exchangeEntitlementStore.elements();
            }
            while (infoRecords.hasMoreElements()) {
                EntitlementRecord record = infoRecords.nextElement();
                if ((exchange == null) || (exchange.getInfoTypes() == null) || (!exchange.isValidIinformationType(record.type))) {
                    record.subscribed = false;
                    record.setIcon(unsubscribedIcon);
                } else {
                    record.subscribed = true;
                    record.setIcon(subscribedIcon);
                }
                record = null;
            }
            infoRecords = null;
        } catch (Exception e) {
        }
    }

    private void applyExchangeSelections() {
        Enumeration<EntitlementRecord> exchanges = exchangeStore.elements();
        while (exchanges.hasMoreElements()) {
            EntitlementRecord record = exchanges.nextElement();
            if (ExchangeStore.getSharedInstance().getExchange(record.getId()) != null) {
                record.subscribed = true;
                record.setIcon(subscribedIcon);
            } else {
                record.subscribed = false;
                record.setIcon(unsubscribedIcon);
            }

            record = null;
        }
        exchanges = null;
    }

    private void loadExchanges() {
        try {
            Decompress decompress = new Decompress();
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/exchanges_" + Language.getSelectedLanguage() + ".msf";
            ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;

            do {
                record = in.readLine();
                if (record != null) {
                    String[] fields = record.split(Meta.DS);
                    if (fields.length <= 1) continue; // it is possible to have garbage in the file. Never trust Servers
                    String description = Language.getLanguageSpecificString(fields[3]);
                    description = UnicodeUtils.getNativeString(description);
                    EntitlementRecord entitlementRecord =
                            new EntitlementRecord(description, null, 0, 0);
                    entitlementRecord.setId(fields[0]); // set the symbol
                    entitlementRecord.setOpaque(true);
                    entitlementRecord.setForeground(Theme.getColor("MENU_FGCOLOR"));
                    entitlementRecord.addMouseListener(this);
                    exchangeStore.put(entitlementRecord.getId(), entitlementRecord);
                    description = null;
                    fields = null;
                }
            } while (record != null);

            out = null;
            decompress = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectExchange() {
        boolean exchangeFound = false;
        Enumeration<EntitlementRecord> entitlements = exchangeStore.elements();
        while (entitlements.hasMoreElements()) {
            EntitlementRecord item = entitlements.nextElement();
            item.setBackground(Theme.getColor("MENU_BGCOLOR"));
            item.setForeground(Theme.getColor("MENU_FGCOLOR"));
            if ((selectedExchange != null) && (item.getId().equals(selectedExchange))) {
                exchangeFound = true;
                applyExchangePreferences(TYPE_EXCHANGE, ExchangeStore.getSharedInstance().getExchange(selectedExchange));
                markSelectedExchange(selectedExchange);
            }
            item = null;
        }

        if (!exchangeFound) { // clear all selections
            applyExchangePreferences(TYPE_EXCHANGE, null);
        }


    }

    public void applyTheme() {
        tabbedPane.updateUI();
        defaultPanel.updateUI();
        exchangeSubscriptioPanel.updateUI();
        exchangePanel.updateUI();
        subscriptionPanel.setBackground(Theme.getColor("MENU_SELECTION_BGCOLOR"));
        subscriptionPanel.setForeground(Theme.getColor("MENU_SELECTION_FGCOLOR"));
        subscriptionPanel.updateUI();
        selectExchange();
    }


    public void show() {
        super.show();

        try {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(0);
            selectedExchange = exchange.getSymbol();
            selectExchange();
            exchange = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void markSelectedExchange(String exchangeID) {
        EntitlementRecord entitlementRecord = exchangeStore.get(exchangeID);
        entitlementRecord.setBackground(Theme.getColor("MENU_SELECTION_BGCOLOR"));
        entitlementRecord.setForeground(Theme.getColor("MENU_SELECTION_FGCOLOR"));
        entitlementRecord = null;
    }

    //Change lisiener
    public void stateChanged(ChangeEvent e) {

    }

    // Mouse lisetener
    public void mouseClicked(MouseEvent e) {
        EntitlementRecord record = (EntitlementRecord) e.getSource();
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(record.getId());
        if (exchange != null) {
            selectedExchange = exchange.getSymbol();
        } else {
            selectedExchange = null;
        }
        selectExchange();
        //record.setBackground(Theme.MENU_SELECTION_COLOR);
        record.setBackground(Theme.getColor("MENU_SELECTION_BGCOLOR"));
        record.setForeground(Theme.getColor("MENU_SELECTION_FGCOLOR"));
        exchange = null;
        record = null;
    }

    // Exchange Listener
    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {
        applyExchangeSelections();
        applyCommonPreferences();
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(0);
        selectedExchange = exchange.getSymbol();
        selectExchange();
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

}
