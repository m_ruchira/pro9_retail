package com.isi.csvr;

import java.math.BigInteger;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Sep 18, 2007
 * Time: 12:51:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class ColumnTemplate {

    private String description;
    private String id;
    private BigInteger defaultColumns;
    private BigInteger allColumns;


    public ColumnTemplate(String id, String description, BigInteger defaultColumns, BigInteger allColumns) {
        this.description = description;
        this.id = id;
        this.defaultColumns = defaultColumns;
        this.allColumns = allColumns;
    }

    public ColumnTemplate(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
//        return Language.getLanguageSpecificString(description);
    }

    public String getId() {
        return id;
    }

    public BigInteger getDefaultColumns() {
        return defaultColumns;
    }

    public BigInteger getAllColumns() {
        return allColumns;
    }
}
