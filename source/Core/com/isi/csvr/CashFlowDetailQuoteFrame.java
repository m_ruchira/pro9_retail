package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWColumnSettings;
import com.isi.csvr.table.CashFlowDetailQuoteModel;
import com.isi.csvr.table.Table;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 31, 2008
 * Time: 11:51:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowDetailQuoteFrame extends InternalFrame implements LinkedWindowListener {

    boolean loadedFromWorkspace = false;
    CashFlowDetailQuoteModel dqModel;
    private String selectedKey = null;
    private boolean isLinked;
    private ViewSetting oViewSetting = null;
    private Table oDetailQuote;
    private ViewSettingsManager g_oViewSettings;
    private boolean symbolRegistered;

    public CashFlowDetailQuoteFrame(Table oTable, WindowDaemon oDaemon, String selectedKey, boolean loadedFromWorkspace, boolean linked, boolean symbolRegistered, String linkgroup) {
        super(oTable, oDaemon);
        oDetailQuote = oTable;
        this.selectedKey = selectedKey;
        this.loadedFromWorkspace = loadedFromWorkspace;
        isLinked = linked;
        this.symbolRegistered = symbolRegistered;
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        setLinkedGroupID(linkgroup);
        initUI();
    }

    public void initUI() {
        oDetailQuote.hideHeader();
        oDetailQuote.getPopup().hideCopywHeadMenu();
        oDetailQuote.setSortingDisabled();
        oDetailQuote.setDQTableType();
        oDetailQuote.hideCustomizer();
//            oDetailQuote.getPopup().showSetDefaultMenu();
        oDetailQuote.setWindowType(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW);

        if (oViewSetting == null && loadedFromWorkspace && isLinked) {
            ViewSetting oSetting = g_oViewSettings.getSnapView(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                oViewSetting = oSetting;
            }
        }
        if (oViewSetting == null) {
            oViewSetting = g_oViewSettings.getSnapView(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW
                    + "|" + selectedKey);
        }
        int fontSize = 0;
        if (oViewSetting == null) {
            oViewSetting = g_oViewSettings.getSymbolView("DETAIL_QUOTE_CASH_FLOW");
            oViewSetting = oViewSetting.getObject(); // clone the object
            GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("DETAIL_QUOTE_CASH_FLOW_COLS"));
            SharedMethods.applyCustomViewSetting(oViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
            //oViewSetting.setID(selectedKey);
            oViewSetting.setID(System.currentTimeMillis() + "");
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);

            g_oViewSettings.putSnapView(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW
                    + "|" + selectedKey, oViewSetting);
            //todo added by Dilum
            fontSize = oViewSetting.getFont().getSize();
        } else {
            loadedFromWorkspace = true;
        }

        dqModel = new CashFlowDetailQuoteModel();
        dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(selectedKey));
        dqModel.setSymbol(selectedKey);
        dqModel.setViewSettings(oViewSetting);
        oDetailQuote.setGroupableCashFlowModel(dqModel);
        oDetailQuote.setUseSameFontForHeader(true);
        dqModel.setTable(oDetailQuote);
        oDetailQuote.getTable().getTableHeader().setReorderingAllowed(false);
        dqModel.applyColumnSettings();

        oDetailQuote.updateGUI();

        oDetailQuote.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        oDetailQuote.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        oDetailQuote.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
//            final ViewSetting oViewSettingRef = oViewSetting;
//            final Table  oDetailQuoteRef = oDetailQuote;
//            oViewSettingRef = oViewSetting;
//            oDetailQuoteRef = oDetailQuote;
        oViewSetting.setParent(this);
        this.setShowServicesMenu(true);
        this.setDetachable(true);
        this.setPrintable(true);
//            frame.setSize(oViewSetting.getSize());
        this.setLocation(oViewSetting.getLocation());
        this.getContentPane().add(oDetailQuote);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setLinkGroupsEnabled(true);
        //frame.addComponentListener(frame);		//L 4743 after the inner class
        //L 4781 inside  if
        Client.getInstance().oTopDesktop.add(this);
        this.setTitle(Language.getString("CASHFLOW_DQ") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getStockObject(selectedKey).getLongDescription());
        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        this.updateUI();
        this.applySettings();

        if (!symbolRegistered) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        } else if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }

        dqModel.updateGUI();

        this.setLayer(GUISettings.TOP_LAYER);
        this.setOrientation();
        if (loadedFromWorkspace && isLinked) {
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" +
                    LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" +
                    selectedKey, this);
        }
        //((SmartTable) oDetailQuote.getTable()).adjustColumnWidthsToFit(40);
        this.updateUI();
        oDetailQuote.packFrame();

        if (!loadedFromWorkspace) {

//                frame.setLocationRelativeTo(oTopDesktop);
//                oViewSetting.setLocation(frame.getLocation());
            if (!oViewSetting.isLocationValid()) {
                this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                oViewSetting.setLocation(this.getLocation());
            } else {
                this.setLocation(oViewSetting.getLocation());
            }
        }
        this.setOrigDimention(this.getSize());

//            String requestID = Meta.PRICE_DEPTH + "|" + selectedKey;
//            addDepthRequest(requestID, selectedKey, Meta.PRICE_DEPTH);
//            frame.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
        this.show();
        // this.setisLoadedFromWsp(false);
    }

    public void symbolChanged(String sKey) {
        if (sKey != null && !sKey.isEmpty()) {
            String oldKey = oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
//            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oldKey))) {
//                DataStore.getSharedInstance().removeSymbolRequest(oldKey);
//
//            } else if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sKey))) {
//                DataStore.getSharedInstance().addSymbolRequest(sKey);
//            }
            selectedKey = sKey;
            dqModel.setSymbol(selectedKey);
            this.setTitle(Language.getString("CASHFLOW_DQ") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                    + ") " + DataStore.getSharedInstance().getStockObject(selectedKey).getLongDescription());
            oDetailQuote.getTable().updateUI();
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
        }


    }
}
