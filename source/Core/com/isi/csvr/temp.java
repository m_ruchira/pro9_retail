package com.isi.csvr;

import com.isi.csvr.shared.KotuKotuLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 12, 2005
 * Time: 11:49:17 PM
 */
public class temp implements DragSourceListener, DragGestureListener, DropTargetListener {

    DropTarget dropTarget;
    DragSource dragSource;

    public temp() {
        JFrame frame = new JFrame("Test");

        ArrayList<Component> components = new ArrayList<Component>();
        DraggableButton b1 = new DraggableButton("Alo1");
        b1.setActionCommand("b1");
        components.add(b1);
        frame.add(b1);
        DraggableButton b2 = new DraggableButton("Alo2");
        b2.setActionCommand("b2");
        components.add(b2);
        frame.add(b2);
        DraggableButton b3 = new DraggableButton("Alo3");
        b3.setActionCommand("b3");
        components.add(b3);
        frame.add(b3);
        DraggableButton b4 = new DraggableButton("Alo4");
        b4.setActionCommand("b4");
        components.add(b4);
        frame.add(b4);
        DraggableButton b5 = new DraggableButton("Alo5");
        b5.setActionCommand("b5");
        components.add(b5);
        frame.add(b5);
        DraggableButton b6 = new DraggableButton("Alo6");
        b6.setActionCommand("b6");
        components.add(b6);
        frame.add(b6);

        frame.setLayout(new KotuKotuLayout(components, 100, 150));
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.show();

        dragSource = new DragSource();
        dragSource.createDefaultDragGestureRecognizer(b6, DnDConstants.ACTION_COPY, this);
        dropTarget = new DropTarget(b1, this);
    }

    public static void main(String[] args) {
        Properties prop2 = null;
        Properties prop1 = null;
        Properties prop3 = new Properties();
        try {
            FileInputStream in = new FileInputStream("languages/en.properties");
            prop1 = new Properties();
            prop1.load(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileInputStream in = new FileInputStream("languages/FR.properties");
            prop2 = new Properties();
            prop2.load(in);
        } catch (Exception e) {
        }
        try {
            HashSet<String> hashtable = new HashSet<String>();
            HashSet<String> hashtable2 = new HashSet<String>();
            Enumeration en = prop1.keys();
            String key = null;
            while (en.hasMoreElements()) {
                key = (String) en.nextElement();
                if (prop2.getProperty(key) == null) {
                    prop3.put(key, prop1.getProperty(key));
                    hashtable.add(key + "=" + prop1.getProperty(key));
                    hashtable2.add(prop1.getProperty(key));
                } else if (prop2.getProperty(key).equals(prop1.getProperty(key))) {
                    hashtable.add(key + "=" + prop1.getProperty(key));
                    hashtable2.add(prop1.getProperty(key));
                    prop2.remove(key);
                } else {
                    prop2.remove(key);
                }
            }
            FileOutputStream out = new FileOutputStream("languages/distinct.properties");
            Iterator<String> itar = hashtable.iterator();
            while (itar.hasNext()) {
                out.write(itar.next().getBytes());
                out.write("\r\n".getBytes());
            }
            out.close();

            out = new FileOutputStream("languages/translations.properties");
            Iterator<String> itar2 = hashtable2.iterator();
            while (itar2.hasNext()) {
                out.write(itar2.next().getBytes());
                out.write("\r\n".getBytes());
            }
            out.close();
            FileOutputStream out2 = new FileOutputStream("languages/more.properties");
            prop2.store(out2, "");
            out2.close();
           /* while (itar.hasNext()){
                out.write(itar.next().getBytes());
                out.write("\r\n".getBytes());
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        /*try{
            FileInputStream in = new FileInputStream("languages/en.properties");
            Properties prop = new Properties();
            prop.load(in);

            HashSet<String> hashtable = new HashSet<String>();
            Enumeration en = prop.elements();
            while (en.hasMoreElements()){
                hashtable.add((String)en.nextElement());
            }

            FileOutputStream out = new FileOutputStream("languages/distinct.txt");
            Iterator<String> itar  = hashtable.iterator();
            while (itar.hasNext()){
                out.write(itar.next().getBytes());
                out.write("\r\n".getBytes());
            }
            out.close();

        } catch (Exception e) {

        }*/
    }

    public void dragEnter(DragSourceDragEvent dsde) {
    }

    public void dragOver(DragSourceDragEvent dsde) {
    }

    public void dropActionChanged(DragSourceDragEvent dsde) {
    }

    public void dragExit(DragSourceEvent dse) {
    }

    public void dragDropEnd(DragSourceDropEvent dsde) {
    }

    public void dragGestureRecognized(DragGestureEvent dge) {
        String id = ((DraggableButton) dge.getComponent()).getActionCommand();
        Transferable transferable = new StringSelection(id);
        dragSource.startDrag(dge, null, null, null, transferable, this);
    }


    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent dtde) {
        try {
            String id = (String) dtde.getTransferable().getTransferData(DataFlavor.stringFlavor);
            System.out.println(id);
        } catch (UnsupportedFlavorException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    class DraggableButton extends JButton {

        public DraggableButton(String text) {
            super(text);
        }
    }

}
