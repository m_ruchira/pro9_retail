package com.isi.csvr;

import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.TWWindowMenuItemSorter;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Arrays;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Apr 24, 2006
 * Time: 4:46:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class WindowSelectorDialog extends JDialog implements ActionListener, KeyListener {

    private Hashtable<String, JInternalFrame> frameList;
    //change start
    private boolean isListNull = true;

    public WindowSelectorDialog() throws HeadlessException {
        super(Client.getInstance().getFrame(), true);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
//        panel.setLayout(new ColumnLayout());
        panel.setBorder(BorderFactory.createCompoundBorder(GUISettings.getTWFrameBorder(),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        setUndecorated(true);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JLabel label = new JLabel(Language.getString("WINDOWS"));
        label.setFont(label.getFont().deriveFont(Font.BOLD));
        label.setPreferredSize(new Dimension(this.getWidth(), 15));
        label.setAlignmentX(Component.LEFT_ALIGNMENT);
        panel.add(label);

        Object[] list = ((TWDesktop) Client.getInstance().getDesktop()).getWindows();
        if (list == null) {
            //change start
            isListNull = true;
            return;
        } else {
            TWWindowMenuItemSorter sorter = new TWWindowMenuItemSorter();
            Arrays.sort(list, sorter);
            sorter = null;
        }

        frameList = new Hashtable<String, JInternalFrame>();
        CharacterIterator it = new StringCharacterIterator("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        TWDesktopInterface frame = null;
        int index = 0;
        char ch;
        for (int i = 0; i < list.length; i++) {
            if (index >= 25) break;
            frame = (TWDesktopInterface) list[i];
            if (!((JInternalFrame) frame).isVisible())
                continue;
            index = index + 1;
            String frameTitle = (frame).getTitle().trim();
            JButton button;
            if (index < 10) {
                frameList.put("" + index, (JInternalFrame) frame);
                button = new JButton(index + " - " + frameTitle);
                button.setActionCommand("" + index);
            } else {
                ch = it.current();
                frameList.put("" + ch, (JInternalFrame) frame);
                button = new JButton(ch + " - " + frameTitle);
                button.setActionCommand("" + ch);
                it.next();
            }
            //change start
            if (frameList.size() == 0) {
                isListNull = true;
            } else {
                isListNull = false;
            }
            //change end
            button.setFont(button.getFont().deriveFont(Font.PLAIN));
            button.setBorderPainted(false);
            button.setContentAreaFilled(false);
            button.setAlignmentX(Component.LEFT_ALIGNMENT);
            button.addActionListener(this);
            button.addKeyListener(this);
            button.setCursor(new Cursor(Cursor.HAND_CURSOR));
            panel.add(button);
            frameTitle = null;
        }
        frame = null;
        it = null;
        list = null;
        getContentPane().add(panel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(Client.getInstance().getFrame());
    }

    private void setSelectedFrame(JInternalFrame item) {
        if (item instanceof com.isi.csvr.chart.ChartFrame) {
            Client.getInstance().showProChart();

        } else if (item instanceof com.isi.csvr.PortfolioTable) {
            if (Client.getInstance().isValidSystemWindow(Meta.IT_Portfolio, true)) {
                try {
                    item.setIcon(false);
                } catch (Exception ex) {
                }
                item.show();
                try {
                    item.setSelected(true);
                } catch (Exception ex) {
                }
            }
        } else {
            try {
                item.setIcon(false);
            } catch (Exception ex) {
            }
            try {
                item.show();
                item.setSelected(true);
                close();
            } catch (Exception e) {
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        JInternalFrame item = frameList.get(e.getActionCommand());
        setSelectedFrame(item);
        item = null;
//        close();
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        JInternalFrame item = frameList.get("" + e.getKeyChar());
        setSelectedFrame(item);
        item = null;
//        close();
    }

    private void close() {
        frameList.clear();
        frameList = null;
        dispose();
    }

    //change start
    public boolean isListNull() {
        return isListNull;
    }
    //change end
}
