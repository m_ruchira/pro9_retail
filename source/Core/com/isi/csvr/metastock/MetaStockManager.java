/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 8, 2005
 * Time: 9:51:40 AM
 * To change this template use File | Settings | File Templates.
 */
package com.isi.csvr.metastock;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.win32.NativeMethods;

import java.util.ArrayList;
import java.util.Hashtable;

public class MetaStockManager {

    public static ArrayList<String> autoUpdateEnableMarkets;
    private static Hashtable<String, MetaStockIntrdayUpdator> updators = new Hashtable<String, MetaStockIntrdayUpdator>();
    private static MetaStockManager ourInstance = new MetaStockManager();

    private MetaStockManager() {
        autoUpdateEnableMarkets = Settings.autoUpdateEnableMarkets;
    }

    public static synchronized MetaStockManager getInstance() {
        return ourInstance;
    }

    public synchronized static void startIntrdayUpdator(String exchange) {

//        if ((!ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock))
//                || (!Settings.isMetaStockInstalled())) return; //only for advanced users

        if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock))
            return; //only for advanced users

        if (autoUpdateEnableMarkets.contains(exchange)) {
            MetaStockIntrdayUpdator metaStockIntrdayUpdator = updators.get(exchange);

            if (metaStockIntrdayUpdator == null) {
                metaStockIntrdayUpdator = new MetaStockIntrdayUpdator(exchange);
                updators.put(exchange, metaStockIntrdayUpdator);
                metaStockIntrdayUpdator.start();
            } else {
                metaStockIntrdayUpdator.clear();
                metaStockIntrdayUpdator.setActive(true);
            }
        }
    }

    public synchronized static void stopIntrdayUpdator(String exchange) {
        MetaStockIntrdayUpdator metaStockIntrdayUpdator = updators.get(exchange);

        if (metaStockIntrdayUpdator != null) {
            metaStockIntrdayUpdator.setActive(false);
        }
    }

    /**
     * Initializing the MetaStock System.
     */
    public void init() {
        try {
            //if ((Settings.isMetaStockInstalled())) {
//                boolean ret = NativeMethods.MSInit("Mubasher", "MubasherUser", "109-4100798");
            // NativeMethods.MLInit();
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* ----- MetaStock Intrday Methods ----- */

    public void shutDownSystem() {
        try {
            //NativeMethods.MSCloseSession();
            NativeMethods.MLCloseConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateHistory(String exchange) {
        MetaStockHistoryUpdator metaStock2HistoryUpdator = new MetaStockHistoryUpdator(MetaStockHistoryUpdator.HISTORY,
                exchange, MetaStockHistoryUpdator.AUTO, false);
        metaStock2HistoryUpdator.update();
        metaStock2HistoryUpdator = null;
    }

    public void addIntrdayRecord(IntraDayOHLC record) {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) {
            String exchange = SharedMethods.getExchangeFromKey(record.getSymbol());
            MetaStockIntrdayUpdator metaStockIntrdayUpdator = updators.get(exchange);
            if (metaStockIntrdayUpdator != null) {
                metaStockIntrdayUpdator.addRecord(record);
            }
        }
    }

    public void updateIntrdayHistory(String exchange, String date) {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) {
            MetaStockIntrdayUpdator metaStockIntrdayUpdator = updators.get(exchange);

            if (metaStockIntrdayUpdator != null) {
                metaStockIntrdayUpdator.updateHistory(date);
            }
        }
    }

    public void updateIntrdayHistory(String exchange) {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) {
            MetaStockIntrdayUpdator metaStockIntrdayUpdator = updators.get(exchange);

            if (metaStockIntrdayUpdator != null) {
                NativeMethods.MLInit();
                metaStockIntrdayUpdator.updateHistory();
                NativeMethods.MLCloseConnection();
            }
        }
    }
}
