package com.isi.csvr.metastock;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.win32.NativeMethods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 23, 2004
 * Time: 3:53:45 PM
 */
public class MetaStockIntrdayUpdator extends Thread implements ExchangeListener {

    public static final short HISTORY = 0;
    public static final short INTRADAY = 1;

    public static final int AUTO = 0;
    public static final int MANUAL = 1;
    public static final int REALTIME = 2;

    public static final String INTRA_LOCK = "INTRA_LOCK";
    ArrayList<String> MSSymbolList;
    private String exchange;
    private Hashtable<String, List> store;
    private boolean active;
    private boolean exiting = false;
    private int folder = -1;
    private String path, historyPath;
    private Hashtable<String, Integer> keyHandleTable;
    private Vector<String> symbolList;


    public MetaStockIntrdayUpdator(String exchange) {
        this.exchange = exchange;
        store = new Hashtable<String, List>();
        symbolList = new Vector<String>();

        path = Settings.META_STOCK_DB_PATH + "\\Intraday\\" + exchange;
        historyPath = Settings.META_STOCK_DB_PATH + "\\History\\" + exchange;
        createFolder();
        createMSFiles(exchange);

        active = true;
        addExitThread();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public Vector<String> getSymbolList() {
        return symbolList;
    }

    private void createFolder() {
        try {
            File folder = new File(path);
            path = folder.getCanonicalPath();
            folder.mkdirs();
            folder = null;

            File historyFolder = new File(historyPath);
            historyPath = historyFolder.getCanonicalPath();
            historyFolder.mkdirs();
            historyFolder = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createMSFiles(String exchange) {
        NativeMethods.MLInit();
        MSSymbolList = NativeMethods.MLGetSecurityInfo(path);

        Enumeration stocks = DataStore.getSharedInstance().getExchangeSymbolStore(exchange).keys();
        ArrayList<MetaStockSecurityInfo> infoList = new ArrayList<MetaStockSecurityInfo>();
        while (stocks.hasMoreElements()) {
            String data = stocks.nextElement().toString();
            String symbol = SharedMethods.getSymbolFromExchangeKey(data);
            int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
            String key = SharedMethods.getKey(exchange, symbol, instrument);
            infoList.add(new MetaStockSecurityInfo(symbol, getCompanyName(key)));
        }

        if (MSSymbolList.size() == 0) {
            NativeMethods.MLAddSymbols(path, infoList, true);
            MSSymbolList = NativeMethods.MLGetSecurityInfo(path);
        }

        NativeMethods.MLAddSymbols(historyPath, infoList, false);
        NativeMethods.MLCloseConnection();

    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void clear() {
        try {
            store.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        NativeMethods.MLInit();
        System.out.println("Meta Updator Active " + exchange);
        while (true) {
            try {
                Thread.sleep(15000); // update every 15 sec
            } catch (InterruptedException e) {
            }

            if ((Settings.isConnected()) && (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock))) {
                synchronized (INTRA_LOCK) {
                    processOHLC();
                }
            }
        }
    }

    /**
     * Add the record to the MetaStock DB
     * Records are recieved from 3 paths
     * 1. From csv files, when TW loads
     * 2. From intraday Trades
     * 3. From OHLC intraday Trades
     *
     * @param record
     */
    public void addRecord(IntraDayOHLC record) {
        List list = store.get(record.getSymbol());
        if (list == null) {
            list = Collections.synchronizedList(new LinkedList());
            store.put(record.getSymbol(), list);
        }
        list.add(record);
    }

    public List getRecordsFor(String symbol) {
        return store.get(symbol);
    }

    private void updateHistoryChart(String key, String path) {
        String symbol = SharedMethods.getSymbolFromKey(key);
        Stock stock = ChartInterface.getStockObject(key);

        if ((stock != null) && (stock.getLastTradeTime() > 0)) {
            if ((stock.getTodaysOpen() > 0) && (stock.getLastTradeTime() > 0)) {
                long timeInd = stock.getLastTradeTime() / (24 * 60 * 60 * 1000); // done to round off to nearest day
                timeInd = timeInd * 24 * 60;  //same as divided by 60000 ( 60 and 1000 omitted here)
                float open = (float) stock.getTodaysOpen();
                float high = (float) stock.getHigh();
                float low = (float) stock.getLow();
                float vwap = (float) stock.getAvgTradePrice();
                float close = (float) stock.getLastTradeValue();
                if (close == 0) {
                    close = (float) stock.getPreviousClosed();
                }
                long volume = stock.getVolume();
                double turnOver = stock.getTurnover();
                IntraDayOHLC ohlcRecord = new IntraDayOHLC(key, timeInd, open, high, low, close, volume, vwap, turnOver);

                NativeMethods.MLUpdateHistoryChart(symbol, path, ohlcRecord, exchange);
            }
        }
    }

    private synchronized void processOHLC() {
        try {
            if (!isActive()) return;

            //MSSymbolList = NativeMethods.MLGetSecurityInfo(path);
            Enumeration<String> keys = store.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                try {
                    if (exiting) { // client is closing
                        System.out.println("Meta Exit");
                        break;
                    }
                    final String symbol = SharedMethods.getSymbolFromKey(key);
                    List records = getRecordsFor(key);
                    if ((records != null) && (records.size() > 0)) {

                        if (MSSymbolList.contains(symbol)) {

                            int success = writeRecords(symbol, path, records);
                            updateHistoryChart(key, historyPath);
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("------MS error cannot find symbol " + symbol);
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }
            }

            //System.out.println("------------->>>>>>>>>>>>>>>>>>>>>>>>> no more elements in store");

        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }

    }

    /*public void initDB() {
        synchronized (this) {
            NativeMethods.MSDeleteDB(folder);

            int hSecurity = -1;
            for (int i = 0; i < keys.length; i++) {
                String symbol = SharedMethods.getSymbolFromKey(keys[i]);
                hSecurity = NativeMethods.MSAddSecutiry(folder, getCompanyName(keys[i]),
                        symbol, INTRADAY, (short) 0, (byte) 0);
                if (hSecurity > 0) {
                    IntradaySecurityTable.getSharedInstance().addRecord(keys[i], hSecurity);
                }
            }
        }
    }*/

    private int openFolder() {
        return NativeMethods.MSOpenFiles(path);
    }


    public String getCompanyName(String key) {
        try {
            if (DataStore.getSharedInstance().isValidSymbol(key)) {
                String companyName = SymbolMaster.getEnglishCompanyName(key);
                if (companyName != null) {
                    return companyName;
                } else {
                    return SharedMethods.getSymbolFromKey(key);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public int writeRecords(String symbol, String dirPath, List records) {
        return NativeMethods.MLAddIntraRecords(symbol, dirPath, records, exchange);
    }

    /*public void writeRecord(String symbol, String dirPath, IntraDayOHLC record) {
        NativeMethods.MLAddIntraRecord(symbol, dirPath, record, exchange);

    }*/

    public void writeRecord(int hSecurity, IntraDayOHLC record) {

        /*try {
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, record.getTime() * 60000));
            NativeMethods.MSAddIntraRecord2(hSecurity, date.get(Calendar.YEAR), date.get(Calendar.MONTH) + 1, date.get(Calendar.DAY_OF_MONTH),
                    date.get(Calendar.HOUR_OF_DAY), date.get(Calendar.MINUTE), record.getOpen(), record.getHigh(),
                    record.getLow(), record.getClose(), (int) record.getVolume(), 0D);
            date = null;
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private boolean isValidSession() {
        return folder >= 0;
    }

    /**
     * Update the MetaStock DB manually
     */
    public synchronized void updateHistory() {
        System.out.println("MetaStock Intrday History manual update Started ");
        File root = new File(Settings.getAbsolutepath() + "ohlc/" + exchange);
        File[] folders = root.listFiles();
        for (File folder : folders) {
            if (folder.isDirectory()) {
                updateHistory(folder.getName());
            }
        }
        processOHLC();
        System.out.println("MetaStock Intrday History manual update Completed ");
    }

    public synchronized void updateHistory(String date) {

        System.out.println("------- UPDATING HISTORY ------->>>>>>>>>>>> " + date);
        String symbol;
        String data;
        int instrument = -1;
        Enumeration symbols = DataStore.getSharedInstance().getSymbols(exchange);
        while (symbols.hasMoreElements()) {
            data = (String) symbols.nextElement();
            symbol = SharedMethods.getSymbolFromExchangeKey(data);
            instrument = SharedMethods.getInstrumentFromExchangeKey(data);
            try {
                updateHistryForSymbol(exchange, symbol, instrument, date);

            } catch (Exception ex) {
                //ex.printStackTrace();
            }
        }

    }

    private synchronized void updateHistryForSymbol(String exchange, String symbol, int instrument, String date) {
        try {
            String data;
            String[] ohlcData;

            File source = new File(Settings.getAbsolutepath() + "ohlc/" + exchange + "/" + date + "/" + symbol + ".csv");
            String key = SharedMethods.getKey(exchange, symbol, instrument);

            BufferedReader in = new BufferedReader(new FileReader(source));
            while (true) {
                data = in.readLine();
                if (data == null) break;
                ohlcData = data.split(Meta.FS);
                IntraDayOHLC record = new IntraDayOHLC(key, Long.parseLong(ohlcData[0]),
                        Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
                        Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
                        Long.parseLong(ohlcData[5]), 0, 0); // todo intraday vwap
                addRecord(record);
                record = null;
            }
            in.close();
            in = null;
            source = null;
            Thread.sleep(1);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private int getRecordCount() {
        Enumeration<List> stores = store.elements();
        int count = 0;
        while (stores.hasMoreElements()) {
            count += stores.nextElement().size();
        }

        return count;
    }

    public void addExitThread() {

        final MetaStockIntrdayUpdator self = this;
        Thread t = new Thread("MetaStockIntrdayUpdator") {
            public void run() {
                try {
                    exiting = true;
                    System.out.println("Meta Exit state");
                    synchronized (self) {
                        if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) return;
                        //NativeMethods.MSCloseDirectory(folder); sha
                        folder = -1;
                        active = false;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        Runtime.getRuntime().addShutdownHook(t);
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
//        if (exchange.getSymbol().equals(this.exchange)){
//            initDB();
//        }
    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void exchangeInformationTypesChanged() {

    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }
}