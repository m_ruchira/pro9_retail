package com.isi.csvr.metastock;

import com.isi.csvr.*;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.history.ExportListener;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.win32.NativeMethods;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 26, 2004
 * Time: 1:52:43 PM
 */
public class MetaStockHistoryUpdator implements Runnable {
    public static final short HISTORY = 0;

    public static final int AUTO = 0;
    public static final int MANUAL = 1;
    public static final String META_LOCK = "MetaLock";
    WorkInProgressIndicator workIn = null;
    private int processType;
    private String start;
    private String end;
    private String destination;
    private ExportListener exportListener;
    private short type;
    private String exchange;
    private boolean deleteOldData;
    private ArrayList<String> MSSymbolList;
    private String path;

    public MetaStockHistoryUpdator(short type, final String exchange, final int processType, boolean deleteOldData) {
        this(type, exchange, processType, "0", "99999999", null, deleteOldData, null);
    }

    public MetaStockHistoryUpdator(short type, final String exchange, final int processType,
                                   final String start, final String end,
                                   final String destination, boolean deleteOldData, final ExportListener exportListener) {
        this.type = type;
        this.start = start;
        this.end = end;
        this.exchange = exchange;
        this.processType = processType;
        this.destination = destination;
        this.exportListener = exportListener;
        this.deleteOldData = deleteOldData;

        path = Settings.META_STOCK_DB_PATH + "\\History\\" + exchange;
        createFolder();
        createMSFiles(exchange);
    }

    public void run() {
        update();
    }

    private void createFolder() {
        try {
            File folder = new File(path);
            path = folder.getCanonicalPath();
            folder.mkdirs();
            folder = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createMSFiles(String exchange) {
        Enumeration stocks = DataStore.getSharedInstance().getExchangeSymbolStore(exchange).keys();
        ArrayList<MetaStockSecurityInfo> infoList = new ArrayList<MetaStockSecurityInfo>();
        while (stocks.hasMoreElements()) {
            String data = stocks.nextElement().toString();
            String symbol = SharedMethods.getSymbolFromExchangeKey(data);
            int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
            String key = SharedMethods.getKey(exchange, symbol, instrument);
            infoList.add(new MetaStockSecurityInfo(symbol, getCompanyName(key)));
        }

        NativeMethods.MLInit();
        MSSymbolList = NativeMethods.MLGetSecurityInfo(path);
        if (MSSymbolList.size() == 0) {
            NativeMethods.MLAddSymbols(path, infoList, false);
            MSSymbolList = NativeMethods.MLGetSecurityInfo(path);
        }
        NativeMethods.MLCloseConnection();

    }

    public void update() {


//        if ((!ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock))
//                || (!Settings.isMetaStockInstalled())) return; //only for advanced users

        if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock))
            return; //only for advanced users

        long startTime = System.currentTimeMillis();
        synchronized (META_LOCK) {
            try {
                NativeMethods.MLInit();
                System.out.println("[Updating MetaStock " + exchange + "]" + SharedMethods.getMemoryDetails());
                Client.setDownloadStatusOn(Meta.METASTOCK_HISTORY);
                Client.getInstance().getMenus().setMetaStockUpdateInProgress(true);
                System.out.println("Starting MetaStock update for " + exchange);
                boolean sucess = false;

                /*String path = null;
                if (destination == null) {
                    path =Settings.META_STOCK_DB_PATH + "\\History\\" + exchange;
                    try {
                        File folder = new File(path);
                        folder.mkdirs();
                        folder.setWritable(true);
                        folder = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    path = destination;
                }*/

                if (deleteOldData) {
                    //NativeMethods.MLDeleteDB(path);
                }

                //ArrayList<String> MSSymbolList = NativeMethods.MLGetSecurityInfo(path);
                sucess = true; // folder open. it is successfull

                Enumeration symbols = DataStore.getSharedInstance().getSymbols(exchange);
                while (symbols.hasMoreElements()) {
                    String data = (String) symbols.nextElement();
                    String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                    int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                    String key = SharedMethods.getKey(exchange, symbol, instrument);
                    try {

                        int result = 0;
                        if (!MSSymbolList.contains(symbol)) { //security in cache
                            result = NativeMethods.MLAddSecutiry(path, getCompanyName(key), symbol);  //1 for history
                            MSSymbolList.add(symbol);
                        }

                        if (result > -1) {
                            if (type == HISTORY) {
                                File[] paths = HistoryFilesManager.getHistoryPaths(exchange);
                                for (int j = 0; j < paths.length; j++) {
                                    NativeMethods.MLAddRecords(symbol, paths[j].getAbsolutePath() + "\\" + symbol + ".csv", path);
                                }
                                paths = null;
                            }
                        }
                        //System.out.println("-------- MS finished symbol = "+symbol);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                    }
                }

                if (!sucess) {
                    if (processType == MANUAL)
                        new ShowMessage(Language.getString("METASTOCK_ERROR_MANUAL"), "E");
                    else if (processType == AUTO)
                        new ShowMessage(Language.getString("METASTOCK_ERROR_AUTO"), "E");
                    System.out.println("Error occured while updating MetaStock at " + path);
                }

                if (exportListener != null) {
                    if (sucess) {
                        exportListener.exportCompleted(null);
                    } else {
                        exportListener.exportAborted(null);
                    }
                }

                if (sucess && (processType == MANUAL) && (exportListener == null) && (type == HISTORY)) {
                    if (SystemTrayManager.isSupported()) {
                        SystemTrayManager.showMessage(Language.getString("METASTOCK_HISTORY_DB_UPDATED"));
                    } else {
                        new ShowMessage(Language.getString("METASTOCK_HISTORY_DB_UPDATED"), "I");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                NativeMethods.MLCloseConnection();
            }

            Client.setDownloadStatusOn(Meta.NONE);
            Client.setDownloadStatusProgress(0);

            Client.getInstance().getMenus().setMetaStockUpdateInProgress(false);
        }
        // release memory

        start = null;
        end = null;
        destination = null;
        exportListener = null;

        System.out.println("[Updating MetaStock completed " + exchange + "]" + SharedMethods.getMemoryDetails());
        System.out.println("--------->>>>>Meta Calc time = " + (System.currentTimeMillis() - startTime));

    }

    public void writeRecord(int hSecurity, IntraDayOHLC record) {
        /*public static native int MSAddIntraRecord(int hSecurity, int year, int month, int day,
                 int hour, int minute, double OpenPrice, double High, double Low, double ClosePrice,
				 int Volume, double OpenInt);*/
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(record.getTime() * 60000);
        /*NativeMethods.MSAddIntraRecord2(hSecurity, date.get(Calendar.YEAR), date.get(Calendar.MONTH) + 1, date.get(Calendar.DAY_OF_MONTH),
                date.get(Calendar.HOUR_OF_DAY), date.get(Calendar.MINUTE), record.getOpen(), record.getHigh(),
                record.getLow(), record.getClose(), (int) record.getVolume(), 0D);*/
        date = null;
    }

    public String getCompanyName(String key) {
        try {
            if (DataStore.getSharedInstance().isValidSymbol(key)) {
                String companyName = SymbolMaster.getEnglishCompanyName(key);
                if (companyName != null) {
                    return companyName;
                } else {
                    return SharedMethods.getSymbolFromKey(key);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private boolean deleteDB(String path) {
        try {
            File filePath = new File(path);
            File[] files = filePath.listFiles();
            for (int i = 0; i < files.length; i++) {
                files[i].delete();
            }
            files = null;
            filePath = null;
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
