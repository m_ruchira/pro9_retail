package com.isi.csvr.metastock;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.util.FlexGridLayout;
import com.isi.util.JCheckBoxList;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 17-May-2007
 * Time: 11:13:26
 * To change this template use File | Settings | File Templates.
 */
public class MetaStockAutoUpdateFrame extends InternalFrame implements InternalFrameListener, ActionListener {
    public static JCheckBoxList defaultMarketList;
    static MetaStockAutoUpdateFrame self;
    private JPanel topPanel;
    private JPanel upperPanel;
    private JPanel middlePanel;
    private JPanel lowerPanel;
    private TWButton applyBtn;
    private TWButton cancelBtn;
    private JScrollPane defaultMarketScroll;
    private JLabel messageLabel;
    private Object defaultMaeketSymbols;
    private ArrayList<Exchange> list;

    public MetaStockAutoUpdateFrame() {
        super();
        topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
//        topPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"0", "150", "30"}, 4, 4));
        topPanel.add(createUpperPanel(), BorderLayout.NORTH);
        topPanel.add(createMiddlePanel(), BorderLayout.CENTER);
        topPanel.add(createLowerPanel(), BorderLayout.SOUTH);

        getContentPane().add(topPanel);
        this.setFont(new TWFont("Arial", Font.BOLD, 12));
        this.setTitle(Language.getString("METASTOCK_AUTOUPDATE"));
        this.pack();
//        this.setSize(new Dimension(300, 275));
        this.setResizable(true);
        this.setClosable(true);
        this.setIconifiable(true);
        Client.getInstance().getDesktop().add(this);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        self = this;
        GUISettings.applyOrientation(this);
    }

    public static MetaStockAutoUpdateFrame getSharedInstannce() {
        if (self == null) {
            self = new MetaStockAutoUpdateFrame();
        }
        return self;
    }

    private JPanel createUpperPanel() {
        upperPanel = new JPanel();
        messageLabel = new JLabel(Language.getString("METASTOCK_AUTOUPDATE_MESSAGE"));
        GUISettings.applyOrientation(messageLabel);
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"0"}, 4, 4));
        upperPanel.add(messageLabel);
        return upperPanel;
    }

    public JPanel createMiddlePanel() {

        middlePanel = new JPanel();
        middlePanel.setLayout(new BorderLayout());
        list = new ArrayList<Exchange>();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                list.add(exchange);
            }
            exchange = null;
        }
        Collections.sort(list);
        Object[] defaultMarkets = new Object[list.size()];
        for (int i = 0; i < list.size(); i++) {
            Exchange exchange = list.get(i);
            defaultMarkets[i] = ExchangeStore.getSharedInstance().getDescription(exchange.getSymbol());
        }

        defaultMarketList = new JCheckBoxList(defaultMarkets);
        setAllreadySelectedMarkets();
        defaultMarketScroll = new JScrollPane(defaultMarketList);
        middlePanel.add(defaultMarketScroll, BorderLayout.CENTER);
        Dimension dimension = defaultMarketList.getPreferredSize();
        dimension.setSize(Math.max(dimension.getWidth(), 310), 180);
        defaultMarketScroll.setPreferredSize(dimension);
        return middlePanel;
    }

    public JPanel createLowerPanel() {
        lowerPanel = new JPanel();
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"100%", "75", "5", "75"}, new String[]{"0"}, 4, 4));
        applyBtn = new TWButton(Language.getString("OK"));
        applyBtn.addActionListener(this);
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        cancelBtn.addActionListener(this);
        lowerPanel.add(new JLabel());
        lowerPanel.add(applyBtn);
        lowerPanel.add(new JLabel());
        lowerPanel.add(cancelBtn);
        return lowerPanel;
    }

    public void showWindow() {
        setVisible(true);
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        setVisible(false);

    }

    private void setAllreadySelectedMarkets() {
        if (Settings.autoUpdateEnableMarkets != null) {
            for (int i = 0; i < Settings.autoUpdateEnableMarkets.size(); i++) {
                if (list.contains(ExchangeStore.getSharedInstance().getExchange(Settings.autoUpdateEnableMarkets.get(i)))) {
                    defaultMarketList.setSelected(list.indexOf(ExchangeStore.getSharedInstance().getExchange(Settings.autoUpdateEnableMarkets.get(i))), true);
                }

            }
        }
    }

    private String makeSelectedMarketString() {
        String element = "";
        int[] selectedIndices = defaultMarketList.getSelectedIndices();
        for (int i = 0; i < selectedIndices.length; i++) {
            element = element + "," + list.get(selectedIndices[i]);
        }
        String marketList = element.substring(1);
        return marketList;
    }

    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource() == applyBtn) {
            if (defaultMarketList.getSelectedIndices().length > 0) {
                Settings.setAutoUpdateforSelectedMarkets(true, makeSelectedMarketString());
            } else {
                Settings.setAutoUpdateforSelectedMarkets(false, "");
            }
            setVisible(false);

        } else if (e.getSource() == cancelBtn) {

            setVisible(false);
        }
    }
}
