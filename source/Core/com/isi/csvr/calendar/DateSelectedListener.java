package com.isi.csvr.calendar;

// Copyright (c) 2000 Integrated Systems International (ISI)

public interface DateSelectedListener {
    public void dateSelected(Object source, int iYear, int iMonth, int iDay);
} 