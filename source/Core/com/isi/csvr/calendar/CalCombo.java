package com.isi.csvr.calendar;

import com.isi.csvr.shared.DownArrow;
import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 23, 2004
 * Time: 2:52:28 PM
 */
public class CalCombo extends JPanel implements ActionListener, DateSelectedListener {
    private CalDialog calendar;
    private DatePicker popup;
    //private Popup popup;
    private Calendar date;
    private JLabel label;
    private JButton dropButton;

    public CalCombo() {
        init();
    }

    private void init() {
        setLayout(new BorderLayout());
        label = new JLabel("");
        label.setOpaque(true);
        label.setBackground(UIManager.getDefaults().getColor("TextField.background"));
        dropButton = new JButton(new DownArrow());
        dropButton.setActionCommand("TD");
        dropButton.setPreferredSize(new Dimension(20, 20));
        dropButton.setBorder(BorderFactory.createEmptyBorder());
        dropButton.setBackground(UIManager.getDefaults().getColor("TextField.background"));
        dropButton.addActionListener(this);
        add(label, BorderLayout.CENTER);
        add(dropButton, BorderLayout.EAST);

        calendar = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
        calendar.setSelfDispose(true);
        calendar.addDateSelectedListener(this);

        //popup.setVisible(false);
    }

    public void actionPerformed(ActionEvent e) {
        //calendar.setVisible(false);
        if (popup != null) {
            //popup.setVisible(false);
            hidePopup();
        }
        Point point = new Point(0, getHeight());
        SwingUtilities.convertPointToScreen(point, this);
        calendar.setVisible(true);
        //popup = PopupFactory.getSharedInstance().getPopup(this,calendar, (int)point.getX(), (int)point.getY());
        //popup.setVisible(true);
        popup = new DatePicker(this, true);
        popup.getCalendar().addDateSelectedListener(this);
        popup.setLocation((int) point.getX(), (int) point.getY());
        popup.show();
        point = null;
    }

    public void hidePopup() {
        try {
            if (popup != null) {
                popup.hide();
                popup = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(long date) {
        SimpleDateFormat f = new SimpleDateFormat(" dd / MM / yyyy ");
        label.setText(f.format(new Date(date)));
        this.date = Calendar.getInstance();
    }

    public void dateSelected(Object source, int year, int month, int day) {
        hidePopup();
        date = Calendar.getInstance();
        date.set(year, month, day);
        SimpleDateFormat format = new SimpleDateFormat("dd / MM / yyyy");
        label.setText(format.format(date.getTime()));
    }

    public void setEnabled(boolean status) {
        label.setEnabled(status);
        dropButton.setEnabled(status);
    }

    public void setDate(String date) {
        label.setText(date);
    }

    /*  public void setDate(Date date){
          label.setText(new SimpleDateFormat(" dd / MM / yyyy ").format(date));
          this.date = Calendar.getInstance();
          this.date.setTime(date);
      }*/
    public void setDate(Date date) {
        label.setText(new SimpleDateFormat(" dd / MM / yyyy ").format(date));
        this.date = Calendar.getInstance();
        this.date.setTime(date);
    }
}
