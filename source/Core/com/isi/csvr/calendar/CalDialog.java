package com.isi.csvr.calendar;

/*
    A basic extension of the java.awt.Dialog class
 */

import com.isi.csvr.shared.TWComboBox;
import com.isi.csvr.shared.TWControl;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalDialog extends JPanel {
    TWComboBox chMonth = new TWComboBox();
    TWComboBox chYear = new TWComboBox();
    CalLabel txtDay1 = new CalLabel();
    CalLabel txtDay2 = new CalLabel();
    CalLabel txtDay3 = new CalLabel();
    CalLabel txtDay4 = new CalLabel();
    CalLabel txtDay5 = new CalLabel();
    CalLabel txtDay6 = new CalLabel();
    CalLabel txtDay7 = new CalLabel();
    CalLabel txtDay8 = new CalLabel();
    CalLabel txtDay9 = new CalLabel();
    CalLabel txtDay10 = new CalLabel();
    CalLabel txtDay11 = new CalLabel();
    CalLabel txtDay12 = new CalLabel();
    CalLabel txtDay13 = new CalLabel();
    CalLabel txtDay14 = new CalLabel();
    CalLabel txtDay15 = new CalLabel();
    CalLabel txtDay16 = new CalLabel();
    CalLabel txtDay17 = new CalLabel();
    CalLabel txtDay18 = new CalLabel();
    CalLabel txtDay19 = new CalLabel();
    CalLabel txtDay20 = new CalLabel();
    CalLabel txtDay21 = new CalLabel();
    CalLabel txtDay22 = new CalLabel();
    CalLabel txtDay23 = new CalLabel();
    CalLabel txtDay24 = new CalLabel();
    CalLabel txtDay25 = new CalLabel();
    CalLabel txtDay26 = new CalLabel();
    CalLabel txtDay27 = new CalLabel();
    CalLabel txtDay28 = new CalLabel();
    CalLabel txtDay29 = new CalLabel();
    CalLabel txtDay30 = new CalLabel();
    CalLabel txtDay31 = new CalLabel();
    CalLabel txtDay32 = new CalLabel();
    CalLabel txtDay33 = new CalLabel();
    CalLabel txtDay34 = new CalLabel();
    CalLabel txtDay35 = new CalLabel();
    CalLabel txtDay36 = new CalLabel();
    CalLabel txtDay37 = new CalLabel();
    CalLabel txtTest = new CalLabel();
    JLabel label1 = new JLabel();
    JLabel label2 = new JLabel();
    JLabel label3 = new JLabel();
    JLabel label4 = new JLabel();
    JLabel label5 = new JLabel();
    JLabel label6 = new JLabel();
    JLabel label7 = new JLabel();

    JLabel txtDays[];
    String[] g_asDays;
    String g_sDay = "";
    DateSelectedListener parent;
    int lastSelectedDate = 0;
    SimpleDateFormat dateFormatFull = new SimpleDateFormat("yyyyMMdd");
    // Used for addNotify check.
    boolean fComponentsAdjusted = false;
    private boolean selfDispose = true;
    private CalLabel currentDate = null;

    public CalDialog(String[] asMonths, String[] asDays) {
        //this();
        g_asDays = asDays;
        loadCalDialog();
//setVisible(true);

        for (int i = 0; i < asMonths.length; i++) {
            chMonth.addItem(asMonths[i]);
        }


        txtDays = new JLabel[38];
        txtDays[0] = null;
        txtDays[1] = txtDay1;
        txtDays[2] = txtDay2;
        txtDays[3] = txtDay3;
        txtDays[4] = txtDay4;
        txtDays[5] = txtDay5;
        txtDays[6] = txtDay6;
        txtDays[7] = txtDay7;
        txtDays[8] = txtDay8;
        txtDays[9] = txtDay9;
        txtDays[10] = txtDay10;
        txtDays[11] = txtDay11;
        txtDays[12] = txtDay12;
        txtDays[13] = txtDay13;
        txtDays[14] = txtDay14;
        txtDays[15] = txtDay15;
        txtDays[16] = txtDay16;
        txtDays[17] = txtDay17;
        txtDays[18] = txtDay18;
        txtDays[19] = txtDay19;
        txtDays[20] = txtDay20;
        txtDays[21] = txtDay21;
        txtDays[22] = txtDay22;
        txtDays[23] = txtDay23;
        txtDays[24] = txtDay24;
        txtDays[25] = txtDay25;
        txtDays[26] = txtDay26;
        txtDays[27] = txtDay27;
        txtDays[28] = txtDay28;
        txtDays[29] = txtDay29;
        txtDays[30] = txtDay30;
        txtDays[31] = txtDay31;
        txtDays[32] = txtDay32;
        txtDays[33] = txtDay33;
        txtDays[34] = txtDay34;
        txtDays[35] = txtDay35;
        txtDays[36] = txtDay36;
        txtDays[37] = txtDay37;

        Calendar cal = Calendar.getInstance();
        //chYear.addItem(cal.get(Calendar.YEAR) + "");

        int minYear = 0;
        try {
            minYear = Integer.parseInt(TWControl.getItem("MIN_CALENDAR_YEAR"));
        } catch (NumberFormatException e) {
            minYear = 1998;
        }
        for (int i = -1; (cal.get(Calendar.YEAR) - i) >= minYear; i++) {
            chYear.addItem((cal.get(Calendar.YEAR) - i) + "");
        }

        chYear.setSelectedIndex(1); // select the current year
        //g_sDay = dt.getDate() + "";
        g_sDay = cal.get(Calendar.DAY_OF_MONTH) + ""; //select the current month

        //chMonth.setSelectedIndex(dt.getMonth());
        chMonth.setSelectedIndex(cal.get(Calendar.MONTH));

        this.setOpaque(true);
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(chMonth);
        if (lastSelectedDate == 0) {
            currentDate = getDatePosition("" + Calendar.getInstance().get(Calendar.DATE));//(CalLabel)txtDays[Calendar.getInstance().get(Calendar.DATE)];
            currentDate.setOpaque(true);
//            cLbl.setBorder(new LineBorder(Theme.getBlackColor(), 1));
        } else {
            if ((chMonth.getSelectedIndex() == cal.get(Calendar.MONTH)) || (chYear.getSelectedIndex() == cal.get(Calendar.YEAR))) {
                currentDate = getDatePosition("" + Calendar.getInstance().get(Calendar.DATE));//(CalLabel)txtDays[Calendar.getInstance().get(Calendar.DATE)];
                currentDate.setOpaque(true);
                //            cLbl.setBorder(new LineBorder(Theme.getBlackColor(), 1));
            }
        }
        SwingUtilities.updateComponentTreeUI(currentDate);
        showCal((cal.get(Calendar.YEAR)) + "", cal.get(Calendar.MONTH));
        updateUI();
        applyTheme();
        //this.setVisible(true);
    }

    public void loadCalDialog() {
        //super(parent);

        // This code is automatically generated by Visual Cafe when you add
        // components to the visual environment. It instantiates and initializes
        // the components. To modify the code, only use code syntax that matches
        // what Visual Cafe can generate, or Visual Cafe may be unable to back
        // parse your Java file into its visual environment.
        //{{INIT_CONTROLS
        setLayout(null);
        //setBackground(java.awt.Color.lightGray);
        setSize(175, 185);
        setPreferredSize(new Dimension(175, 185));
        //setVisible(false);
        try {
            chMonth.setSelectedIndex(0);
        } catch (IllegalArgumentException e) {
        }
        add(chMonth);
        chMonth.setBounds(4, 6, 88, 25);
        add(chYear);
        chYear.setBounds(107, 6, 63, 25);
        //txtDay1.setHorizontalAlignment(JLabel.CENTER);
        add(txtDay1);
        //txtDay1.setBackground(new java.awt.Color(135,134,95));
        //txtDay1.setForeground(java.awt.Color.black);
        txtDay1.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay1.setBounds(6, 56, 17, 16);
        //txtDay2.setAlignmentX(JLabel.CENTER);
        add(txtDay2);
        //txtDay2.setBackground(new java.awt.Color(135,134,95));
        //txtDay2.setForeground(java.awt.Color.black);
        txtDay2.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay2.setBounds(29, 56, 17, 16);
        //txtDay3.setAlignmentX(JLabel.CENTER);
        add(txtDay3);
        //txtDay3.setBackground(new java.awt.Color(135,134,95));
        //txtDay3.setForeground(java.awt.Color.black);
        txtDay3.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay3.setBounds(53, 56, 17, 16);
        //txtDay4.setAlignmentX(JLabel.CENTER);
        add(txtDay4);
        //txtDay4.setBackground(new java.awt.Color(135,134,95));
        //txtDay4.setForeground(java.awt.Color.black);
        txtDay4.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay4.setBounds(77, 56, 17, 16);
        //txtDay5.setAlignmentX(JLabel.CENTER);
        add(txtDay5);
        //txtDay5.setBackground(new java.awt.Color(135,134,95));
        //txtDay5.setForeground(java.awt.Color.black);
        txtDay5.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay5.setBounds(101, 56, 17, 16);
        //txtDay6.setAlignmentX(JLabel.CENTER);
        add(txtDay6);
        //txtDay6.setBackground(new java.awt.Color(135,134,95));
        //txtDay6.setForeground(java.awt.Color.black);
        txtDay6.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay6.setBounds(125, 56, 17, 16);
        //txtDay7.setAlignmentX(JLabel.CENTER);
        add(txtDay7);
        //txtDay7.setBackground(new java.awt.Color(135,134,95));
        //txtDay7.setForeground(java.awt.Color.black);
        txtDay7.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay7.setBounds(150, 56, 17, 16);
        //txtDay8.setAlignmentX(JLabel.CENTER);
        add(txtDay8);
        //txtDay8.setBackground(new java.awt.Color(135,134,95));
        //txtDay8.setForeground(java.awt.Color.black);
        txtDay8.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay8.setBounds(6, 78, 17, 16);
        //txtDay9.setAlignmentX(JLabel.CENTER);
        add(txtDay9);
        //txtDay9.setBackground(new java.awt.Color(135,134,95));
        //txtDay9.setForeground(java.awt.Color.black);
        txtDay9.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay9.setBounds(29, 78, 17, 16);
        //txtDay10.setAlignmentX(JLabel.CENTER);
        add(txtDay10);
        //txtDay10.setBackground(new java.awt.Color(135,134,95));
        //txtDay10.setForeground(java.awt.Color.black);
        txtDay10.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay10.setBounds(53, 78, 17, 16);
        //txtDay11.setAlignmentX(JLabel.CENTER);
        add(txtDay11);
        //txtDay11.setBackground(new java.awt.Color(135,134,95));
        //txtDay11.setForeground(java.awt.Color.black);
        txtDay11.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay11.setBounds(77, 78, 17, 16);
        //txtDay12.setAlignmentX(JLabel.CENTER);
        add(txtDay12);
        //txtDay12.setBackground(new java.awt.Color(135,134,95));
        //txtDay12.setForeground(java.awt.Color.black);
        txtDay12.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay12.setBounds(101, 78, 17, 16);
        //txtDay13.setAlignmentX(JLabel.CENTER);
        add(txtDay13);
        //txtDay13.setBackground(new java.awt.Color(135,134,95));
        //txtDay13.setForeground(java.awt.Color.black);
        txtDay13.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay13.setBounds(125, 78, 17, 16);
        //txtDay14.setAlignmentX(JLabel.CENTER);
        add(txtDay14);
        //txtDay14.setBackground(new java.awt.Color(135,134,95));
        //txtDay14.setForeground(java.awt.Color.black);
        txtDay14.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay14.setBounds(150, 78, 17, 16);
        //txtDay15.setAlignmentX(JLabel.CENTER);
        add(txtDay15);
        //txtDay15.setBackground(new java.awt.Color(135,134,95));
        //txtDay15.setForeground(java.awt.Color.black);
        txtDay15.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay15.setBounds(6, 100, 17, 16);
        //txtDay16.setAlignmentX(JLabel.CENTER);
        add(txtDay16);
        //txtDay16.setBackground(new java.awt.Color(135,134,95));
        //txtDay16.setForeground(java.awt.Color.black);
        txtDay16.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay16.setBounds(29, 100, 17, 16);
        //txtDay17.setAlignmentX(JLabel.CENTER);
        add(txtDay17);
        //txtDay17.setBackground(new java.awt.Color(135,134,95));
        //txtDay17.setForeground(java.awt.Color.black);
        txtDay17.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay17.setBounds(53, 100, 17, 16);
        //txtDay18.setAlignmentX(JLabel.CENTER);
        add(txtDay18);
        //txtDay18.setBackground(new java.awt.Color(135,134,95));
        //txtDay18.setForeground(java.awt.Color.black);
        txtDay18.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay18.setBounds(77, 100, 17, 16);
        //txtDay19.setAlignmentX(JLabel.CENTER);
        add(txtDay19);
        //txtDay19.setBackground(new java.awt.Color(135,134,95));
        //txtDay19.setForeground(java.awt.Color.black);
        txtDay19.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay19.setBounds(101, 100, 17, 16);
        //txtDay20.setAlignmentX(JLabel.CENTER);
        add(txtDay20);
        //txtDay20.setBackground(new java.awt.Color(135,134,95));
        //txtDay20.setForeground(java.awt.Color.black);
        txtDay20.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay20.setBounds(125, 100, 17, 16);
        //txtDay21.setAlignmentX(JLabel.CENTER);
        add(txtDay21);
        //txtDay21.setBackground(new java.awt.Color(135,134,95));
        //txtDay21.setForeground(java.awt.Color.black);
        txtDay21.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay21.setBounds(150, 100, 17, 16);
        //txtDay22.setAlignmentX(JLabel.CENTER);
        add(txtDay22);
        //txtDay22.setBackground(new java.awt.Color(135,134,95));
        //txtDay22.setForeground(java.awt.Color.black);
        txtDay22.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay22.setBounds(6, 121, 17, 16);
        //txtDay23.setAlignmentX(JLabel.CENTER);
        add(txtDay23);
        //txtDay23.setBackground(new java.awt.Color(135,134,95));
        //txtDay23.setForeground(java.awt.Color.black);
        txtDay23.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay23.setBounds(29, 121, 17, 16);
        //txtDay24.setAlignmentX(JLabel.CENTER);
        add(txtDay24);
        //txtDay24.setBackground(new java.awt.Color(135,134,95));
        //txtDay24.setForeground(java.awt.Color.black);
        txtDay24.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay24.setBounds(53, 121, 17, 16);
        //txtDay25.setAlignmentX(JLabel.CENTER);
        add(txtDay25);
        //txtDay25.setBackground(new java.awt.Color(135,134,95));
        //txtDay25.setForeground(java.awt.Color.black);
        txtDay25.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay25.setBounds(77, 121, 17, 16);
        //txtDay26.setAlignmentX(JLabel.CENTER);
        add(txtDay26);
        //txtDay26.setBackground(new java.awt.Color(135,134,95));
        //txtDay26.setForeground(java.awt.Color.black);
        txtDay26.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay26.setBounds(101, 121, 17, 16);
        //txtDay27.setAlignmentX(JLabel.CENTER);
        add(txtDay27);
        //txtDay27.setBackground(new java.awt.Color(135,134,95));
        //txtDay27.setForeground(java.awt.Color.black);
        txtDay27.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay27.setBounds(125, 121, 17, 16);
        //txtDay28.setAlignmentX(JLabel.CENTER);
        add(txtDay28);
        //txtDay28.setBackground(new java.awt.Color(135,134,95));
        //txtDay28.setForeground(java.awt.Color.black);
        txtDay28.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay28.setBounds(150, 121, 17, 16);
        //txtDay29.setAlignmentX(JLabel.CENTER);
        add(txtDay29);
        //txtDay29.setBackground(new java.awt.Color(135,134,95));
        //txtDay29.setForeground(java.awt.Color.black);
        txtDay29.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay29.setBounds(6, 143, 17, 16);
        //txtDay30.setAlignmentX(JLabel.CENTER);
        add(txtDay30);
        //txtDay30.setBackground(new java.awt.Color(135,134,95));
        //txtDay30.setForeground(java.awt.Color.black);
        txtDay30.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay30.setBounds(29, 143, 17, 16);
        //txtDay31.setAlignmentX(JLabel.CENTER);
        add(txtDay31);
        //txtDay31.setBackground(new java.awt.Color(135,134,95));
        //txtDay31.setForeground(java.awt.Color.black);
        txtDay31.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay31.setBounds(53, 143, 17, 16);
        //txtDay32.setAlignmentX(JLabel.CENTER);
        add(txtDay32);
        //txtDay32.setBackground(new java.awt.Color(135,134,95));
        //txtDay32.setForeground(java.awt.Color.black);
        txtDay32.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay32.setBounds(77, 143, 17, 16);
        //txtDay33.setAlignmentX(JLabel.CENTER);
        add(txtDay33);
        //txtDay33.setBackground(new java.awt.Color(135,134,95));
        //txtDay33.setForeground(java.awt.Color.black);
        txtDay33.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay33.setBounds(101, 143, 17, 16);
        //txtDay34.setAlignmentX(JLabel.CENTER);
        add(txtDay34);
        //txtDay34.setBackground(new java.awt.Color(135,134,95));
        //txtDay34.setForeground(java.awt.Color.black);
        txtDay34.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay34.setBounds(125, 143, 17, 16);
        //txtDay35.setAlignmentX(JLabel.CENTER);
        add(txtDay35);
        //txtDay35.setBackground(new java.awt.Color(135,134,95));
        //txtDay35.setForeground(java.awt.Color.black);
        txtDay35.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay35.setBounds(150, 143, 17, 16);
        //txtDay36.setAlignmentX(JLabel.CENTER);
        add(txtDay36);
        //txtDay36.setBackground(new java.awt.Color(135,134,95));
        //txtDay36.setForeground(java.awt.Color.black);
        txtDay36.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay36.setBounds(6, 164, 17, 16);
        //txtDay37.setAlignmentX(JLabel.CENTER);
        add(txtDay37);
        //txtDay37.setBackground(new java.awt.Color(135,134,95));
        //txtDay37.setForeground(java.awt.Color.black);
        txtDay37.setFont(new TWFont("Dialog", Font.BOLD, 12));
        txtDay37.setBounds(29, 164, 17, 16);
        label1.setText(g_asDays[0]);
        label1.setAlignmentX(JLabel.CENTER);
        add(label1);
        //label1.setBackground(new java.awt.Color(111,111,0));
        //label1.setForeground(java.awt.Color.white);
        label1.setFont(new TWFont("Dialog", Font.BOLD, 12));
        label1.setBounds(4, 36, 20, 16);
        label2.setText(g_asDays[1]);
        label2.setAlignmentX(JLabel.CENTER);
        add(label2);
        //label2.setBackground(new java.awt.Color(111,111,0));
        //label2.setForeground(java.awt.Color.white);
        label2.setFont(new TWFont("Dialog", Font.BOLD, 12));
        label2.setBounds(28, 36, 20, 16);
        label3.setText(g_asDays[2]);
        label3.setAlignmentX(JLabel.CENTER);
        add(label3);
        //label3.setBackground(new java.awt.Color(111,111,0));
        //label3.setForeground(java.awt.Color.white);
        label3.setFont(new TWFont("Dialog", Font.BOLD, 12));
        label3.setBounds(52, 36, 20, 16);
        label4.setText(g_asDays[3]);
        label4.setAlignmentX(JLabel.CENTER);
        add(label4);
        //label4.setBackground(new java.awt.Color(111,111,0));
        //label4.setForeground(java.awt.Color.white);
        label4.setFont(new TWFont("Dialog", Font.BOLD, 12));
        label4.setBounds(76, 36, 20, 16);
        label5.setText(g_asDays[4]);
        label5.setAlignmentX(JLabel.CENTER);
        add(label5);
        //label5.setBackground(new java.awt.Color(111,111,0));
        //label5.setForeground(java.awt.Color.white);
        label5.setFont(new TWFont("Dialog", Font.BOLD, 12));
        label5.setBounds(100, 36, 20, 16);
        label6.setText(g_asDays[5]);
        label6.setAlignmentX(JLabel.CENTER);
        add(label6);
        //label6.setBackground(new java.awt.Color(111,111,0));
        //label6.setForeground(java.awt.Color.white);
        label6.setFont(new TWFont("Dialog", Font.BOLD, 12));
        label6.setBounds(124, 36, 20, 16);
        label7.setText(g_asDays[6]);
        label7.setAlignmentX(JLabel.CENTER);
        add(label7);
        //label7.setBackground(new java.awt.Color(111,111,0));
        //label7.setForeground(java.awt.Color.white);
        label7.setFont(new TWFont("Dialog", Font.BOLD, 12));
        label7.setBounds(149, 36, 20, 16);
        //}}

        //{{REGISTER_LISTENERS
        //SymWindow aSymWindow = new SymWindow();
        //this.addWindowListener(aSymWindow);
        //}}

        SymMouse aSymMouse = new SymMouse();
        txtDay1.addMouseListener(aSymMouse);
        SymItem lSymItem = new SymItem();
        chMonth.addItemListener(lSymItem);
        chYear.addItemListener(lSymItem);
        txtDay2.addMouseListener(aSymMouse);
        txtDay3.addMouseListener(aSymMouse);
        txtDay4.addMouseListener(aSymMouse);
        txtDay5.addMouseListener(aSymMouse);
        txtDay6.addMouseListener(aSymMouse);
        txtDay7.addMouseListener(aSymMouse);
        txtDay8.addMouseListener(aSymMouse);
        txtDay9.addMouseListener(aSymMouse);
        txtDay10.addMouseListener(aSymMouse);
        txtDay11.addMouseListener(aSymMouse);
        txtDay12.addMouseListener(aSymMouse);
        txtDay13.addMouseListener(aSymMouse);
        txtDay14.addMouseListener(aSymMouse);
        txtDay15.addMouseListener(aSymMouse);
        txtDay16.addMouseListener(aSymMouse);
        txtDay17.addMouseListener(aSymMouse);
        txtDay18.addMouseListener(aSymMouse);
        txtDay19.addMouseListener(aSymMouse);
        txtDay20.addMouseListener(aSymMouse);
        txtDay21.addMouseListener(aSymMouse);
        txtDay22.addMouseListener(aSymMouse);
        txtDay23.addMouseListener(aSymMouse);
        txtDay24.addMouseListener(aSymMouse);
        txtDay25.addMouseListener(aSymMouse);
        txtDay26.addMouseListener(aSymMouse);
        txtDay27.addMouseListener(aSymMouse);
        txtDay28.addMouseListener(aSymMouse);
        txtDay29.addMouseListener(aSymMouse);
        txtDay30.addMouseListener(aSymMouse);
        txtDay31.addMouseListener(aSymMouse);
        txtDay32.addMouseListener(aSymMouse);
        txtDay33.addMouseListener(aSymMouse);
        txtDay34.addMouseListener(aSymMouse);
        txtDay35.addMouseListener(aSymMouse);
        txtDay36.addMouseListener(aSymMouse);
        txtDay37.addMouseListener(aSymMouse);


    }

    private CalLabel getDatePosition(String day) {
        for (int i = 1; i < 38; i++) {
            CalLabel calb = (CalLabel) txtDays[i];
            if (calb.getText().equals(day)) {
                return calb;
            }

        }
        return null;
    }

    public void showCal(String sYear, int iMonth) {
        Date dt = new Date();
        Calendar cal = Calendar.getInstance();
        CalObject c = new CalObject();
        int iYear = 0;

        try {
            iYear = Integer.parseInt(sYear);// - 1900;
        } catch (Exception e) {
        }
        c.setDate(iYear, iMonth);

        int f = c.getFirstDayOfWeek() - 1;
        int i = 1;

        for (int j = 1; j <= f; j++) {
            txtDays[j].setVisible(false);
        }

        while (c.isSameMonth(i)) {
            txtDays[f + i].setText("" + i);
            txtDays[f + i].setVisible(true);
            i++;
        }
        for (int j = (f + i); j < 38; j++)
            txtDays[j].setVisible(false);

        this.setBackground(Theme.getColor("BACKGROUND_COLOR"));
        applyTheme();
        //this.setVisible(true);
    }

    public String getDate() {
        /*int iTimeZoneOffset = 0;

        iTimeZoneOffset = iOffset;
        SimpleTimeZone simpleTimeZone = new SimpleTimeZone(iTimeZoneOffset * 60 * 1000,"GMT");
        TimeZone.setDefault(simpleTimeZone);*/

        //this.show();
        int iYear = (new Integer((String) chYear.getSelectedItem())).intValue();
        int iMonth = (new Integer(chMonth.getSelectedIndex())).intValue();
        int iDay = (new Integer(g_sDay)).intValue();
        if (isValiDate(iYear, iMonth, iDay))
            return g_sDay + " " + chMonth.getSelectedItem() + " " + chYear.getSelectedItem();
        else
            return "*";

    }

    private boolean isValiDate(int iYear, int iMonth, int iDay) {
        //967380887330L

        //Date sysDate  = new Date();
        //Date selected = new Date(iYear,iMonth,iDay);
        Calendar selected = Calendar.getInstance();
        selected.set(iYear, iMonth, iDay);
        Date selectedDate = selected.getTime();

        //Date sysDate2 = new Date(sysDate.getYear(),sysDate.getMonth(),sysDate.getDate() );
        Date sysDate = Calendar.getInstance().getTime();

        if (selectedDate.after(sysDate))
            return false;
        else
            return true;


    }

    void txtDay_MouseClicked(Object source) {
        g_sDay = ((JLabel) source).getText();
        dateSelected();
        //this.hide();
    }

    void Year_Month_Changed(java.awt.event.ItemEvent event) {
        // to do: code goes here.
        try {
            showCal((String) chYear.getSelectedItem(), chMonth.getSelectedIndex());
        } catch (Exception e) {
// ignore
        }

    }

    public void dateSelected() {
        int iYear = Integer.parseInt((String) chYear.getSelectedItem());// -1900;
        int iMonth = chMonth.getSelectedIndex();
        int iDay = Integer.parseInt(g_sDay);
        lastSelectedDate = (Integer.parseInt(g_sDay));
        parent.dateSelected(this, iYear, iMonth, iDay);
        if (txtDays.length > 1) {
            for (int i = 1; i < txtDays.length; i++) {
                if (txtDays[i].getText() != null || !txtDays[i].getText().equals("")) {
                    if (("" + iDay).equalsIgnoreCase(txtDays[i].getText())) {
                        CalLabel cLbl = (CalLabel) txtDays[i];
                        cLbl.setBorder(new LineBorder(Theme.getBlackColor(), 1));
                        break;
                    }
                }
            }
        }
        if (isSelfDispose())
            this.setVisible(false);
    }

    public void addDateSelectedListener(DateSelectedListener parent) {
        this.parent = parent;
    }

    /* date event */

    public boolean isSelfDispose() {
        return selfDispose;
    }

    public void setSelfDispose(boolean selfDispose) {
        this.selfDispose = selfDispose;
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        txtTest.updateUI();
        this.setBackground(Theme.getColor("BACKGROUND_COLOR"));
        Date sysDate = Calendar.getInstance().getTime();
        String dateString = dateFormatFull.format(sysDate);

        int iYear = Integer.parseInt((String) chYear.getSelectedItem());// -1900;
        int iMonth = chMonth.getSelectedIndex();

        int currentYear = Integer.parseInt(dateString.substring(0, 4));
        int currentMonth = Integer.parseInt(dateString.substring(4, 6));
        if (currentYear == iYear && currentMonth == iMonth + 1) {
            currentDate.setBackground(txtTest.getForeground());
            currentDate.setForeground(txtTest.getBackground());
        }
    }

    public TWComboBox getChYear() {
        return chYear;
    }

    /* -- */

    /*static public void main(String args[])
	{
		CalDialog f =new CalDialog("xx","1,2,3,4,5,6,7,8,9,10,11,12");
        f.show();
	}*/

    public TWComboBox getChMonth() {
        return chMonth;
    }

    public JLabel[] getTxtDays() {
        return txtDays;
    }

    class SymMouse extends java.awt.event.MouseAdapter {
        public void mouseClicked(java.awt.event.MouseEvent event) {
            txtDay_MouseClicked(event.getSource());
        }
    }

    class SymItem implements java.awt.event.ItemListener {
        public void itemStateChanged(java.awt.event.ItemEvent event) {
            Year_Month_Changed(event);
        }
    }

}
