package com.isi.csvr.calendar;

import java.util.Calendar;

public class CalObject {
    int g_iMonth = 0;
    int g_iYear = 0;
    //Date g_dtDate = null;
    Calendar g_dtDate = null;

    CalObject() {
    }

    public void setDate(int iYear, int iMonth) {
        g_iMonth = iMonth;
        g_iYear = iYear;
        //g_dtDate = new Date(iYear,iMonth,1);
        g_dtDate = Calendar.getInstance();
        g_dtDate.set(iYear, iMonth, 1);
    }

    public int getFirstDayOfWeek() {
        //return g_dtDate.getDay();
        return g_dtDate.get(Calendar.DAY_OF_WEEK);
    }

    public boolean isSameMonth(int iDay) {
        int iMonth;

        //Date dtDate = new Date(g_iYear,g_iMonth,iDay);
        //return (g_iMonth == dtDate.getMonth());
        Calendar cal = Calendar.getInstance();
        cal.set(g_iYear, g_iMonth, iDay);
        return (g_iMonth == cal.get(Calendar.MONTH));
    }

}