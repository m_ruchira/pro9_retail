package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.DataStoreInterface;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.MultiDialogTableModel;
import com.isi.csvr.table.Table;
import com.isi.csvr.trade.CompanyTradeStore;
import com.isi.csvr.trade.FilterableTradeStore;
import com.isi.csvr.trade.TimeNSalesModel;
import com.isi.csvr.trade.TradeStore;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 26, 2006
 * Time: 4:56:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class MultiDialogWindow extends InternalFrame implements ActionListener, ApplicationListener, LinkedWindowListener {

    public static final byte COMBINED_MDEPTH_TYPE = 2;
    public static final String RECORD_SEPERATOR = "!";
    private static final byte NORMAL_MDEPTH_TYPE = 1;
    public static String TABLE_ID = "xxxx";
    private static MultiDialogWindow self = null;
    MultiDialogTableModel dqModel;
    private String sSelectedSymbol = null;
    private ViewSetting oViewSetting;
    private InternalFrame marketDepth;
    private InternalFrame timeAndSales;
    private Hashtable<String, InternalFrame> subWindows;
    private JSplitPane bottomsplitPane;
    private Table oDetailQuote;
    private boolean isMarketDepthShowing = false;
    private boolean isTimeNSalesShowing = false;
    private boolean isViewSettingsCreated = false;
    private int splitLocation = 0;
    private boolean isLinked;
    private boolean isLoadedFromWSP = false;
    private ViewSettingsManager g_oViewSettingsManager;

    private ViewSetting depthSetting1;
    private ViewSetting depthSetting2;
    private ViewSetting timeNSalesSetting;

    public MultiDialogWindow() {
        this.getContentPane().setLayout(new BorderLayout());
        oDetailQuote = new Table();
        subWindows = new Hashtable<String, InternalFrame>();
        oViewSetting = ViewSettingsManager.getSummaryView("MULTIWINDOW");
        if (!Client.getInstance().getSelectedSymbol().equals("")) {
            sSelectedSymbol = Client.getInstance().getSelectedSymbol();
        }
        if (oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY) != null) {
            try {
                sSelectedSymbol = (oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY));
                if (sSelectedSymbol == null || sSelectedSymbol.equals("")) {
                    Enumeration symbols = DataStore.getSharedInstance().getSymbols(ExchangeStore.getSelectedExchangeID());
                    String data = null;
                    String symbol = null;
                    int instrument = -1;
                    while (symbols.hasMoreElements()) {
                        data = (String) symbols.nextElement();
                        symbol = SharedMethods.getSymbolFromExchangeKey(data);
                        instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                        sSelectedSymbol = SharedMethods.getKey(ExchangeStore.getSelectedExchangeID(), symbol, instrument);
                        break;
                    }
                    symbols = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            createtable();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        bottomsplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        bottomsplitPane.setDividerSize(5);
        splitLocation = 165;
        bottomsplitPane.setDividerLocation(splitLocation);
        bottomsplitPane.setContinuousLayout(true);

        this.getContentPane().add(oDetailQuote, BorderLayout.NORTH);
        this.getContentPane().add(bottomsplitPane, BorderLayout.CENTER);

        oViewSetting.setParent(this);
        this.setTable(oDetailQuote);

        this.setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(this);
        this.setSize(oViewSetting.getSize());
        this.setLocation(oViewSetting.getLocation());
        this.getTableComponent().getSmartTable().adjustColumnWidthsToFit(20);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setPrintable(false);
        this.setDetachable(true);
        oDetailQuote.getPopup().hidePrintMenu();
        this.setDesktopItemType(TWDesktopInterface.POPUP_TYPE);
        this.setResizable(true);
        this.setVisible(false);
        this.setTitle(Language.getString("MULTI_DIALOG_TITLE"));
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        GUISettings.applyOrientation(this);
        this.applySettings();
        this.getContentPane().addMouseListener(this);
        Application.getInstance().addApplicationListener(this);
        bottomsplitPane.setFocusable(true);
        bottomsplitPane.setEnabled(true);
//        System.out.println("end of the constructor");
        //  registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public MultiDialogWindow(WindowDaemon oDaemon, String sKey, boolean fromWSP, boolean isLinked, String linkGrp) {
        super(oDaemon);
        sSelectedSymbol = sKey;
        g_oViewSettingsManager = Client.getInstance().getViewSettingsManager();
        setLinkedGroupID(linkGrp);
        this.getContentPane().setLayout(new BorderLayout());
        oDetailQuote = new Table();
        subWindows = new Hashtable<String, InternalFrame>();
        this.isLinked = isLinked;
        isLoadedFromWSP = fromWSP;
        if (fromWSP && isLinked) {
            oViewSetting = Client.getInstance().getViewSettingsManager().getFullQuoteView(ViewSettingsManager.FULL_QUOTE_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
        } else {
            oViewSetting = Client.getInstance().getViewSettingsManager().getFullQuoteView(ViewSettingsManager.FULL_QUOTE_VIEW + "|" + sSelectedSymbol);
        }
        if (oViewSetting == null) {
            oViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("FULL_QUOTE_VIEW");
            oViewSetting = oViewSetting.getObject();
            oViewSetting.setID(System.currentTimeMillis() + "");
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            oViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
            Client.getInstance().getViewSettingsManager().putFullQuoteMainView(ViewSettingsManager.FULL_QUOTE_VIEW
                    + "|" + sSelectedSymbol, oViewSetting);

        }

        if (!Client.getInstance().getSelectedSymbol().equals("")) {
            sSelectedSymbol = Client.getInstance().getSelectedSymbol();
        }
        if (oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY) != null) {
            try {
                sSelectedSymbol = (oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY));
                if (sSelectedSymbol == null || sSelectedSymbol.equals("")) {
                    Enumeration symbols = DataStore.getSharedInstance().getSymbols(ExchangeStore.getSelectedExchangeID());
                    String data = null;
                    String symbol = null;
                    int instrument = -1;
                    while (symbols.hasMoreElements()) {
                        data = (String) symbols.nextElement();
                        symbol = SharedMethods.getSymbolFromExchangeKey(data);
                        instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                        sSelectedSymbol = SharedMethods.getKey(ExchangeStore.getSelectedExchangeID(), symbol, instrument);
                        break;
                    }
                    symbols = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            createtable();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        bottomsplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        bottomsplitPane.setDividerSize(5);
        splitLocation = 165;
        bottomsplitPane.setDividerLocation(splitLocation);
        bottomsplitPane.setContinuousLayout(true);

        this.getContentPane().add(oDetailQuote, BorderLayout.NORTH);
        this.getContentPane().add(bottomsplitPane, BorderLayout.CENTER);

        oViewSetting.setParent(this);
        this.setTable(oDetailQuote);

        Client.getInstance().getDesktop().add(this);
        this.setSize(oViewSetting.getSize());
        this.setLocation(oViewSetting.getLocation());
        this.getTableComponent().getSmartTable().adjustColumnWidthsToFit(20);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setPrintable(false);
        this.setDetachable(true);
        this.setLinkGroupsEnabled(true);
        oDetailQuote.getPopup().hidePrintMenu();
        this.setDesktopItemType(TWDesktopInterface.POPUP_TYPE);
        this.setResizable(true);
        this.setVisible(false);
        this.setTitle(Language.getString("MULTI_DIALOG_TITLE"));
        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        GUISettings.applyOrientation(this);
        this.setLayer(GUISettings.TOP_LAYER);
        this.applySettings();
        this.getContentPane().addMouseListener(this);
        Application.getInstance().addApplicationListener(this);
        bottomsplitPane.setFocusable(true);
        bottomsplitPane.setEnabled(true);
        if ((marketDepth == null) || (!isMarketDepthShowing) || (timeAndSales == null) || (!isTimeNSalesShowing)) {
            createSubTables();
        }

        splitLocation = 165;
        if (isLoadedFromWSP) {
            if (oViewSetting.getProperty(ViewConstants.SECOND_SPLIT_LOCATION) != null) {
                try {
                    splitLocation = (int) Double.parseDouble(oViewSetting.getProperty(ViewConstants.SECOND_SPLIT_LOCATION));
                    //                System.out.println("split location ="+splitLocation);
                } catch (Exception e) {
                    //                e.printStackTrace();
                }
            }
        }

        bottomsplitPane.setDividerLocation(splitLocation);

        if (bottomsplitPane.getDividerLocation() <= 0) {
            bottomsplitPane.setDividerLocation(splitLocation);
        }
//            System.out.println("set symbol called from set visible in Multi Dialog");
        setSymbol(sSelectedSymbol);
        marketDepth.setVisible(true);
        timeAndSales.setVisible(true);
        super.setVisible(true);
        if (isLinked && fromWSP) {
            Client.getInstance().getViewSettingsManager().setWindow(ViewSettingsManager.FULL_QUOTE_VIEW + "|" +
                    LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            Client.getInstance().getViewSettingsManager().setWindow(ViewSettingsManager.FULL_QUOTE_VIEW + "|" + sSelectedSymbol, this);
        }

        if (!fromWSP) {
            if (!oViewSetting.isLocationValid()) {
                this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                oViewSetting.setLocation(this.getLocation());
            } else {
                this.setLocation(oViewSetting.getLocation());
            }
        }


    }

    public void createMarketDepthFrame() {
        try {
            if (Settings.getFullQuoteMDepthType() == COMBINED_MDEPTH_TYPE) {
                marketDepth = Client.getInstance().depthByPriceCombined(sSelectedSymbol, isLoadedFromWSP, Constants.FULL_QUOTE_TYPE, isLinked, oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
            } else {
                marketDepth = Client.getInstance().depthByPriceNormal(sSelectedSymbol, isLoadedFromWSP, Constants.FULL_QUOTE_TYPE, isLinked, oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                ((DepthByPriceNormalFrame) (marketDepth)).setFullquote(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (marketDepth != null) {
                subWindows.put("" + marketDepth.getWindowType(), marketDepth);

                String requestID = Meta.PRICE_DEPTH + "|" + sSelectedSymbol;
                Client.getInstance().addDepthRequest(requestID, sSelectedSymbol, Meta.PRICE_DEPTH);
                marketDepth.getTableComponent().hideHeader();
                marketDepth.getModel1().getViewSettings().setHeaderVisible(false);
                Component titleBar = ((BasicInternalFrameUI) marketDepth.getUI()).getNorthPane();
                if (titleBar != null) {
                    ((BasicInternalFrameUI) marketDepth.getUI()).setNorthPane(null);
                    marketDepth.revalidate();
                }

                marketDepth.setBorder(BorderFactory.createEmptyBorder());
                marketDepth.setDesktopItemType(TWDesktopInterface.BOARD_TYPE);
                marketDepth.getTableComponent().getPopup().hideTitleBarMenu();
                marketDepth.getTableComponent().getPopup().hidePrintMenu();
                marketDepth.getTableComponent().getPopup().hideExcelLinkMenus();
//                marketDepth.getTableComponent().getModel().getViewSettings().setParent(this);

                if (Settings.getFullQuoteMDepthType() == COMBINED_MDEPTH_TYPE) {
//                    marketDepth.getTableComponent().getModel().setViewSettings(Client.getInstance().getViewSettingsManager().
//                            getFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW));
                    bottomsplitPane.setLeftComponent(marketDepth.getTableComponent());
                    depthSetting1 = marketDepth.getTableComponent().getModel().getViewSettings();

                } else {
                    marketDepth.getSubTableComponent().getPopup().hideTitleBarMenu();
                    marketDepth.getSubTableComponent().getPopup().hidePrintMenu();
                    marketDepth.getSubTableComponent().getPopup().hideExcelLinkMenus();
//                    marketDepth.getTableComponent().getModel().setViewSettings(Client.getInstance().getViewSettingsManager().
//                            getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW));
//                    marketDepth.getSubTableComponent().getModel().setViewSettings(Client.getInstance().getViewSettingsManager().
//                            getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW));
                    //  bottomsplitPane.setLeftComponent(marketDepth.getSplitPane());
                    bottomsplitPane.setLeftComponent(marketDepth);
                    depthSetting1 = marketDepth.getTableComponent().getModel().getViewSettings();
                    depthSetting2 = marketDepth.getSubTableComponent().getModel().getViewSettings();
//                    marketDepth.getSubTableComponent().getModel().getViewSettings().setParent(this);
                }
//                marketDepth.setRequestFocusEnabled(true);
//                marketDepth.setFocusable(true);
                marketDepth.setShowServicesMenu(false);
//                marketDepth.setPrintable(false);

                marketDepth.setVisible(true);
                marketDepth.updateUI();
                marketDepth.applySettings();

                isMarketDepthShowing = true;
//                System.out.println("market depth created");
                this.updateUI();
//            }else{
//                System.out.println("market depth window is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createTimeNSalesFrame() {

        timeAndSales = Client.getInstance().mnu_TimeNSalesSymbol(sSelectedSymbol, Constants.FULL_QUOTE_TYPE, true, false, LinkStore.LINK_NONE);
        if (timeAndSales != null) {
            subWindows.put("" + timeAndSales.getWindowType(), timeAndSales);
        }
        try {
            if (timeAndSales != null) {
//                String requestID2 = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + sSelectedSymbol;
                String symbol = SharedMethods.getSymbolFromKey(sSelectedSymbol);
                try {
                    Stock stock = DataStore.getSharedInstance().getStockObject(sSelectedSymbol);
                    if ((stock != null) && (stock.getMarketCenter() != null)) {
                        if (symbol.endsWith("." + stock.getMarketCenter())) {
                            symbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                String requestID2 = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + SharedMethods.getKey(SharedMethods.getExchangeFromKey(sSelectedSymbol), symbol,
                        SharedMethods.getInstrumentTypeFromKey(sSelectedSymbol));
                Client.getInstance().addTradeRequest(requestID2, sSelectedSymbol);
                timeAndSales.getTableComponent().hideHeader();
                timeAndSales.getModel1().getViewSettings().setHeaderVisible(false);
                timeAndSales.getTableComponent().setPopupActive(true);
                timeAndSales.getTableComponent().getPopup().hidePrintMenu();
                timeAndSales.setResizable(false);
                Component titleBar = ((BasicInternalFrameUI) timeAndSales.getUI()).getNorthPane();
                if (titleBar != null) {
                    ((BasicInternalFrameUI) timeAndSales.getUI()).setNorthPane(null);
                    timeAndSales.revalidate();
                }
                timeAndSales.setVisible(true);
                timeAndSales.setBorder(BorderFactory.createEmptyBorder());
                timeAndSales.setDesktopItemType(TWDesktopInterface.BOARD_TYPE);
//                timeAndSales.applySettings();
//                timeAndSales.getTableComponent().getModel().setViewSettings(Client.getInstance().getViewSettingsManager().
//                        getFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2));
                timeNSalesSetting = timeAndSales.getTableComponent().getModel().getViewSettings();
                bottomsplitPane.setRightComponent(timeAndSales.getTableComponent());
                timeAndSales.getTableComponent().getPopup().hideTitleBarMenu();
                timeAndSales.getTableComponent().getPopup().hidePrintMenu();
                //timeAndSales.getTableComponent().getModel().getViewSettings().setParent(this);
                timeAndSales.updateUI();
                isTimeNSalesShowing = true;
                timeAndSales.getTableComponent().getModel().applySettings();
                timeAndSales.applySettings();
                this.updateUI();
//                System.out.println("time and sales created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    public static MultiDialogWindow getSharedInstance(){
//        if(self ==null){
//            self = new MultiDialogWindow();
//        }
//        return self;
//    }

    public String getSelectedSymbol() {
        return sSelectedSymbol;
    }

//    public void setVisible(boolean status) {
////        System.out.println("ste visible called status =="+status);
//        if(status){
//            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.DetailQuote, "FullQuote");
//            if(Client.getInstance().getSelectedSymbol()!=null && !Client.getInstance().getSelectedSymbol().equals("")){
//                sSelectedSymbol = Client.getInstance().getSelectedSymbol();
//            }
//            if(sSelectedSymbol==null){
//                try {
//                    Enumeration symbols = DataStore.getSharedInstance().getSymbols(ExchangeStore.getSelectedExchangeID());
//                    String data = null;
//                    String symbol = null;
//                    int instrument = -1;
//                    while(symbols.hasMoreElements()){
//                        data = (String)symbols.nextElement();
//                        symbol = SharedMethods.getSymbolFromExchangeKey(data);
//                        instrument = SharedMethods.getInstrumentFromExchangeKey(data);
//                        sSelectedSymbol = SharedMethods.getKey(ExchangeStore.getSelectedExchangeID(),symbol, instrument);
//                        break;
//                    }
//                    symbols = null;
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            if((marketDepth==null) || (!isMarketDepthShowing) || (timeAndSales==null) || (!isTimeNSalesShowing)){
//                createSubTables();
//            }
//
//            if(bottomsplitPane.getDividerLocation()<=0){
//                bottomsplitPane.setDividerLocation(splitLocation);
//            }
////            System.out.println("set symbol called from set visible in Multi Dialog");
//            setSymbol(sSelectedSymbol);
//            marketDepth.setVisible(true);
//            timeAndSales.setVisible(true);
//            super.setVisible(true);
//        }else{
//            super.setVisible(false);
//        }
////        System.out.println("set visible ended");
//    }

    private void createtable() {
        oDetailQuote.hideHeader();
        oDetailQuote.setAutoResize(true);
        oDetailQuote.getPopup().hideCopywHeadMenu();
        oDetailQuote.setSortingDisabled();
        oDetailQuote.setDQTableType();
        oDetailQuote.hideCustomizer();
        oDetailQuote.setWindowType(ViewSettingsManager.COMBINED_WINDOW_VIEW);
        dqModel = new MultiDialogTableModel();
        if (sSelectedSymbol != null) {
            try {
                dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
            } catch (NullPointerException e) {
                try {
                    Enumeration symbols = DataStore.getSharedInstance().getSymbols(ExchangeStore.getSelectedExchangeID());
                    String data = null;
                    String symbol = null;
                    int instrument = -1;
                    while (symbols.hasMoreElements()) {
                        data = (String) symbols.nextElement();
                        symbol = SharedMethods.getSymbolFromExchangeKey(data);
                        instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                        sSelectedSymbol = SharedMethods.getKey(ExchangeStore.getSelectedExchangeID(), symbol, instrument);
                        break;
                    }
                    symbols = null;
                    dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        } else {
            dqModel.setDecimalCount((byte) 2);
        }
        if (sSelectedSymbol != null) {
            dqModel.setSymbol(sSelectedSymbol);
        }
        dqModel.setViewSettings(oViewSetting);
        oDetailQuote.setModel(dqModel);
        oDetailQuote.getScrollPane().setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        dqModel.setTable(oDetailQuote);
        oDetailQuote.setUseSameFontForHeader(true);
        oDetailQuote.getTable().getTableHeader().setReorderingAllowed(false);
        oDetailQuote.updateGUI();
        dqModel.applyColumnSettings();
    }

    public void setSymbol(String symbol) {
        System.out.println("set symbol called");
        try {
//            System.out.println("selected symbol ===== "+symbol);
            this.sSelectedSymbol = symbol;
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
            String name = DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription();
            setTitle(Language.getString("MULTI_DIALOG_TITLE") + " : " + "(" + SharedMethods.getSymbolFromKey(sSelectedSymbol) + ") " + name);
            dqModel.setSymbol(sSelectedSymbol);
            dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
            try {
                if (Settings.getFullQuoteMDepthType() == COMBINED_MDEPTH_TYPE) {
                    ((CommonTableInterface) marketDepth.getTable1().getModel()).setSymbol(sSelectedSymbol);
//                     ((CombinedDepthModel)marketDepth.getTable1().getModel()).setDecimalCount((ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sSelectedSymbol))).getPriceDecimalPlaces());
                    marketDepth.getModel1().setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
                    ((DepthByPriceCombinedFrame) marketDepth).setSelectedKey(sSelectedSymbol);

                } else {
                    ((CommonTableInterface) marketDepth.getTable1().getModel()).setSymbol(sSelectedSymbol);
//                    ((DepthPriceModel)marketDepth.getTable1().getModel()).setDecimalCount((ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sSelectedSymbol))).getPriceDecimalPlaces());
                    marketDepth.getModel1().setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
                    ((CommonTableInterface) marketDepth.getTable2().getModel()).setSymbol(sSelectedSymbol);
//                    ((DepthPriceModel)marketDepth.getTable2().getModel()).setDecimalCount((ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sSelectedSymbol))).getPriceDecimalPlaces());
                    marketDepth.getModel2().setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
                    ((DepthByPriceNormalFrame) marketDepth).setSelectedKey(sSelectedSymbol);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            //marketDepth.getModel1().setDecimalCount((ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sSelectedSymbol))).getPriceDecimalPlaces());
            marketDepth.updateUI();
            String requestID = Meta.PRICE_DEPTH + "|" + sSelectedSymbol;
            Client.getInstance().addDepthRequest(requestID, sSelectedSymbol, Meta.PRICE_DEPTH);

            ((CommonTableInterface) timeAndSales.getModel1()).setSymbol(sSelectedSymbol);
            FilterableTradeStore store = CompanyTradeStore.getSharedInstance().getCompanyTradesStore(sSelectedSymbol);
            ((TimeNSalesModel) timeAndSales.getModel1()).setDataStore(store);
            timeAndSales.getModel1().setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
            timeAndSales.updateUI();
            ((TimeNSalesSymbolFrame) timeAndSales).setSelectedSymbol(sSelectedSymbol);
//            String requestID2 = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + sSelectedSymbol;
            String sSymbol = SharedMethods.getSymbolFromKey(sSelectedSymbol);
            try {
                Stock stock = DataStore.getSharedInstance().getStockObject(sSelectedSymbol);
                if ((stock != null) && (stock.getMarketCenter() != null)) {
                    if (symbol.endsWith("." + stock.getMarketCenter())) {
                        sSymbol = sSymbol.substring(0, sSymbol.lastIndexOf("." + stock.getMarketCenter()));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            String requestID2 = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + SharedMethods.getKey(SharedMethods.getExchangeFromKey(sSelectedSymbol), sSymbol,
                    SharedMethods.getInstrumentTypeFromKey(sSelectedSymbol));
            Client.getInstance().addTradeRequest(requestID2, sSelectedSymbol);
            this.setDataRequestID(new DataStoreInterface[]{DepthStore.getInstance(), TradeStore.getSharedInstance()}, sSelectedSymbol, new String[]{requestID, requestID2});
            oDetailQuote.updateUI();
            dqModel.updateGUI();
            this.getTableComponent().getSmartTable().adjustColumnWidthsToFit(20);
            depthSetting1.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
            if (depthSetting2 != null) {
                depthSetting2.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
            }
            timeNSalesSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
        } catch (Exception e) {
//            e.printStackTrace();
        }
        System.out.println("set symbol ended");
    }


    public void applicationLoading(int percentage) {
    }

    public void applicationLoaded() {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationExiting() {
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void workspaceWillLoad() {
    }

    public void workspaceLoaded() {
//        System.out.println("workspace loaded started =="+(System.currentTimeMillis()/1000));
//        splitLocation = 255;
//        if(oViewSetting.getProperty(ViewConstants.SECOND_SPLIT_LOCATION)!=null){
//            try {
//                splitLocation = (int)Double.parseDouble(oViewSetting.getProperty(ViewConstants.SECOND_SPLIT_LOCATION));
////                System.out.println("split location ="+splitLocation);
//            } catch (Exception e) {
////                e.printStackTrace();
//            }
//        }
//        isViewSettingsCreated = false;
//        createSubTables();
//        if((oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY)!=null) && (!oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(""))){
////            System.out.println("set symbol called from workspace loaded");
//            setSymbol(oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY));
//        }
//        bottomsplitPane.setDividerLocation(splitLocation);
//        this.applySettings();
//        this.updateUI();
//        System.out.println("workspace loaded end =="+(System.currentTimeMillis()/1000));
    }

    public String getSubWindowTypesString() {
        //todo added to temparary purpose
        String str = "";
        if (Settings.getFullQuoteMDepthType() == COMBINED_MDEPTH_TYPE) {
            str = "" + ViewSettingsManager.COMBINED_DEPTH_VIEW;
        } else {
            str = "" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW;
        }
        str = str + ":";
        str = str + ViewSettingsManager.TIMEnSALES_VIEW2;
        return str;

        //todo removed for temparary
//        String str = "";
//        Enumeration<String> keys = subWindows.keys();
//        while(keys.hasMoreElements()){
//            str= str +":";
//            str = str + keys.nextElement();
//        }
//        str = str.replaceFirst(":","");
//        return str;
    }

    public String getSubWindowViews() {
        //todo added to temparary purpose
        String str = "";

        try {
            if (Settings.getFullQuoteMDepthType() == COMBINED_MDEPTH_TYPE) {
                //  str = "[" + Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW).toWorkspaceString();
                str = "[" + depthSetting1.toWorkspaceString();
                str = str + "]";
            } else {
//                str = "[" + Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW).toWorkspaceString();
//                str = str + " / " + Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW).toWorkspaceString() + "]";
                str = "[" + depthSetting1.toWorkspaceString();
                str = str + " / " + depthSetting2.toWorkspaceString() + "]";
            }
            str = str + ":";
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        // str = str + "[" + Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2).toWorkspaceString() + "]";
        str = str + "[" + timeNSalesSetting.toWorkspaceString() + "]";
        return str;

        //todo removed for temparary
//        String str = "";
//        Enumeration<String> keys = subWindows.keys();
//        while(keys.hasMoreElements()){
//            int windowType =Integer.parseInt(keys.nextElement());//  subWindows.get(i).getWindowType();
//            str= str +":";
//            if((windowType == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) || (windowType == ViewSettingsManager.DEPTH_BID_VIEW)
//                    || (windowType == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW)){
//                str = str + "[" + marketDepth.getTableComponent().getModel().getViewSettings().toWorkspaceString() +" / "
//                            + marketDepth.getSubTableComponent().getModel().getViewSettings().toWorkspaceString() +"]";
//            }else {
//                str = str + "[" + timeAndSales.getViewSetting().toWorkspaceString() +"]";
//            }
//        }
//        str = str.replaceFirst(":","");
//        return str;
    }

    public void workspaceWillSave() {
        try {
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
        } catch (Exception e) {
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, "");
        }
        try {
            oViewSetting.putProperty(ViewConstants.SECOND_SPLIT_LOCATION, bottomsplitPane.getDividerLocation());
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            oViewSetting.putProperty(ViewConstants.SUB_WINDOW_TYPES, getSubWindowTypesString());
            oViewSetting.putProperty(ViewConstants.SUB_WINDOW_VIEWS, getSubWindowViews());
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void createSubTables() {
//        System.out.println("create sub tables called");
        if (!isViewSettingsCreated) {
//            System.out.println("create sub viewsettings called");
            createSubViewSettings();
        }
        marketDepth = null;
        isMarketDepthShowing = false;
        timeAndSales = null;
        isTimeNSalesShowing = false;
        createMarketDepthFrame();
        createTimeNSalesFrame();

        try {
            marketDepth.applySettings();
            marketDepth.getTableComponent().getModel().applyColumnSettings();
            marketDepth.getTableComponent().getModel().applySettings();
            if (Settings.getFullQuoteMDepthType() != COMBINED_MDEPTH_TYPE) {
                marketDepth.getSubTableComponent().getModel().applyColumnSettings();
                marketDepth.getSubTableComponent().getModel().applySettings();
            }
            marketDepth.applySettings();
            marketDepth.updateUI();

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            timeAndSales.getTableComponent().getModel().applyColumnSettings();
            timeAndSales.getTableComponent().getModel().applySettings();
            timeAndSales.applySettings();
            timeAndSales.updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        bottomsplitPane.setDividerLocation(splitLocation);
//        System.out.println("sub tables created ended");
    }


    private void createSubViewSettings() {
        String subTypes = oViewSetting.getProperty(ViewConstants.SUB_WINDOW_TYPES);
        String subViews = oViewSetting.getProperty(ViewConstants.SUB_WINDOW_VIEWS);
        long timestamp = System.currentTimeMillis();
        try {
            if (subTypes != null) {
                String[] subWindowViews = subViews.split(":");
                String[] subWindows = subTypes.split(":");
                for (int i = 0; i < subWindows.length; i++) {
                    String windowViewString = subWindowViews[i];
                    windowViewString = windowViewString.replaceFirst("\\[", "");
                    windowViewString = windowViewString.replaceFirst("\\]", "");
                    if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.DEPTH_PRICE_BID_VIEW)) {

                        String[] views = windowViewString.split("/");

                        ViewSetting oBidViewSetting = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW);
                        if (oBidViewSetting == null) {
                            oBidViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_PRICE_BID_VIEW");
                            oBidViewSetting = oBidViewSetting.getObject();
                            oBidViewSetting.setRecordSeperator(RECORD_SEPERATOR);
                            oBidViewSetting.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            oBidViewSetting.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            oBidViewSetting.setSize(this.getSize());
                            //    Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW, oBidViewSetting);
                            GUISettings.setColumnSettings(oBidViewSetting, TWColumnSettings.getItem("DEPTH_PRICE_COLS"));
                        }
                        try {
                            if (views[0].indexOf("\u001E") > 0) {
                                oBidViewSetting.setWorkspaceRecord(views[0].split("\u001E")[1]);
                            } else {
                                oBidViewSetting.setWorkspaceRecord(views[0]);
                            }
//                            oBidViewSetting.setWorkspaceRecord(views[0].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        ViewSetting oAskViewSetting = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
                        if (oAskViewSetting == null) {
                            oAskViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_PRICE_ASK_VIEW");
                            oAskViewSetting = oAskViewSetting.getObject();
                            oAskViewSetting.setRecordSeperator(RECORD_SEPERATOR);
                            oAskViewSetting.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            oAskViewSetting.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            oAskViewSetting.setSize(this.getSize());
                            //  Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW, oAskViewSetting);
                            GUISettings.setColumnSettings(oAskViewSetting, TWColumnSettings.getItem("DEPTH_PRICE_COLS"));
                        }
                        try {
                            if (views[1].indexOf("\u001E") > 0) {
                                oAskViewSetting.setWorkspaceRecord(views[1].split("\u001E")[1]);
                            } else {
                                oAskViewSetting.setWorkspaceRecord(views[1]);
                            }
                        } catch (Exception e) {
//                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        oBidViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                        oAskViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP));


                        if (oViewSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                            oBidViewSetting.putProperty(ViewConstants.VC_LINKED, true);
                            oAskViewSetting.putProperty(ViewConstants.VC_LINKED, true);
                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + LinkStore.linked + "_" + oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oBidViewSetting);
                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + LinkStore.linked + "_" + oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oAskViewSetting);
                        } else {
                            oBidViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                            oAskViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oBidViewSetting);
                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oAskViewSetting);
                        }


                    } else if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.DEPTH_BID_VIEW)) {
                        String[] views = windowViewString.split("/");
                        ViewSetting oBidViewSetting = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_BID_VIEW);
                        if (oBidViewSetting == null) {
                            oBidViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_BID_VIEW");
                            oBidViewSetting = oBidViewSetting.getObject();
                            oBidViewSetting.setRecordSeperator(RECORD_SEPERATOR);
                            oBidViewSetting.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            oBidViewSetting.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            oBidViewSetting.setSize(this.getSize());
                            Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_BID_VIEW, oBidViewSetting);
                            GUISettings.setColumnSettings(oBidViewSetting, TWColumnSettings.getItem("DEPTH_ORDER_COLS"));
                        }
                        try {
                            if (views[0].indexOf("\u001E") > 0) {
                                oBidViewSetting.setWorkspaceRecord(views[0].split("\u001E")[1]);
                            } else {
                                oBidViewSetting.setWorkspaceRecord(views[0]);
                            }
//                            oBidViewSetting.setWorkspaceRecord(views[0].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        ViewSetting oAskViewSetting = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_ASK_VIEW);
                        if (oAskViewSetting == null) {
                            oAskViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_ASK_VIEW");
                            oAskViewSetting = oAskViewSetting.getObject();
                            oAskViewSetting.setRecordSeperator(RECORD_SEPERATOR);
                            oAskViewSetting.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            oAskViewSetting.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            oAskViewSetting.setSize(this.getSize());
                            Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_ASK_VIEW, oAskViewSetting);
                            GUISettings.setColumnSettings(oAskViewSetting, TWColumnSettings.getItem("DEPTH_ORDER_COLS"));
                        }
                        try {
                            if (views[1].indexOf("\u001E") > 0) {
                                oAskViewSetting.setWorkspaceRecord(views[1].split("\u001E")[1]);
                            } else {
                                oAskViewSetting.setWorkspaceRecord(views[1]);
                            }
//                            oAskViewSetting.setWorkspaceRecord(views[1].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    } else if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW)) {
                        String[] views = windowViewString.split("/");
                        ViewSetting oBidViewSetting = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW);
                        if (oBidViewSetting == null) {
                            oBidViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_SPECIAL_BID_VIEW");
                            oBidViewSetting = oBidViewSetting.getObject();
                            oBidViewSetting.setRecordSeperator(RECORD_SEPERATOR);
                            oBidViewSetting.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            oBidViewSetting.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            oBidViewSetting.setSize(this.getSize());
                            Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW, oBidViewSetting);
                            GUISettings.setColumnSettings(oBidViewSetting, TWColumnSettings.getItem("DEPTH_SPECIAL_COLS"));
                        }
                        try {
                            oBidViewSetting.setWorkspaceRecord(views[0].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        ViewSetting oAskViewSetting = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
                        if (oAskViewSetting == null) {
                            oAskViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_SPECIAL_ASK_VIEW");
                            oAskViewSetting = oAskViewSetting.getObject();
                            oAskViewSetting.setRecordSeperator(RECORD_SEPERATOR);
                            oAskViewSetting.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            oAskViewSetting.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            oAskViewSetting.setSize(this.getSize());
                            Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW, oAskViewSetting);
                            GUISettings.setColumnSettings(oAskViewSetting, TWColumnSettings.getItem("DEPTH_SPECIAL_COLS"));
                        }
                        try {
                            oAskViewSetting.setWorkspaceRecord(views[1].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    } else if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.TIMEnSALES_VIEW2)) {
                        ViewSetting oViewSetting = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2);   //sSelectedSymbol + "*"+ type);
                        if (oViewSetting == null) {
                            oViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("TIME_AND_SALES");
                            oViewSetting = oViewSetting.getObject(); // clone the object
                            oViewSetting.setRecordSeperator(RECORD_SEPERATOR);
                            oViewSetting.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            oViewSetting.setID(MultiDialogWindow.TABLE_ID);   //sSelectedSymbol+ "*"+type);
                            oViewSetting.setSize(this.getSize());
                            // Client.getInstance().getViewSettingsManager().putFullQuoteSubView(""+ViewSettingsManager.TIMEnSALES_VIEW2, oViewSetting);
                            GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("SYMBOL_TIME_N_SALES_COLS"));
                        }
                        try {
                            if (windowViewString.indexOf("\u001E") > 0) {
                                oViewSetting.setWorkspaceRecord(windowViewString.split("\u001E")[1]);
                            } else {
                                oViewSetting.setWorkspaceRecord(windowViewString);
                            }
//                            oViewSetting.setWorkspaceRecord(windowViewString.split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        oViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, this.oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP));


                        if (oViewSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                            oViewSetting.putProperty(ViewConstants.VC_LINKED, true);
                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + LinkStore.linked + "_" + this.oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oViewSetting);
                        } else {
                            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + this.oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oViewSetting);
                        }
                    } else if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.COMBINED_DEPTH_VIEW)) {
                        ViewSetting oViewSetting = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW);   //sSelectedSymbol + "*"+ type);
                        if (oViewSetting == null) {
                            oViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("COMBINED_DEPTH");
                            oViewSetting = oViewSetting.getObject(); // clone the object
                            oViewSetting.setRecordSeperator(RECORD_SEPERATOR);
                            oViewSetting.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            oViewSetting.setID(MultiDialogWindow.TABLE_ID);   //sSelectedSymbol+ "*"+type);
                            oViewSetting.setSize(this.getSize());
                            //  Client.getInstance().getViewSettingsManager().putFullQuoteSubView(""+ViewSettingsManager.COMBINED_DEPTH_VIEW, oViewSetting);
                            GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("COMBINED_ORDER_COLS"));
                        }
                        try {
                            if (windowViewString.indexOf("\u001E") > 0) {
                                oViewSetting.setWorkspaceRecord(windowViewString.split("\u001E")[1]);
                            } else {
                                oViewSetting.setWorkspaceRecord(windowViewString);
                            }
//                            oViewSetting.setWorkspaceRecord(windowViewString.split("\u001E")[1]);
                        } catch (Exception e) {
//                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        oViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, this.oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP));


                        if (oViewSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                            oViewSetting.putProperty(ViewConstants.VC_LINKED, true);
                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + LinkStore.linked + "_" + this.oViewSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oViewSetting);
                        } else {
                            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + this.oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oViewSetting);
                        }
                    }
                }
                isViewSettingsCreated = true;
            } else {
                isViewSettingsCreated = false;
            }
        } catch (Exception e) {
            isViewSettingsCreated = false;
        }
//        System.out.println("create sub viewsettings ended");
    }

    public void workspaceSaved() {
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        //  this.setDataRequestID((DataStoreInterface[])null,sSelectedSymbol,(String[])null);
        super.internalFrameClosing(e);
    }


    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        System.out.println("ESC");
    }

    public void symbolChanged(String sKey) {
        String oldKey = oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        DataStore.getSharedInstance().removeSymbolRequest(oldKey);
        DataStore.getSharedInstance().addSymbolRequest(sKey);
        setSymbol(sKey);
    }

    public InternalFrame getMarketDepth() {
        return marketDepth;
    }

    public InternalFrame getTimeAndSales() {
        return timeAndSales;
    }

    public void adjustDetachedFrameSize() {
        if (isDetached()) {
            try {
                JFrame frame = (JFrame) getDetachedFrame();
                if (oViewSetting.getSize().getHeight() > oDetailQuote.getSize().height + marketDepth.getSize().height + 85) {
                    frame.setSize(frame.getWidth(), oViewSetting.getSize().height);
                } else {
                    frame.setSize(frame.getWidth(), oDetailQuote.getSize().height + marketDepth.getSize().height + 85);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } else {
            final InternalFrame frame = this;
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    SwingUtilities.updateComponentTreeUI(frame);
                }
            });

//            try {
//
//                if (oViewSetting.getSize().getHeight() > oDetailQuote.getSize().height + marketDepth.getSize().height + 85) {
//                    this.setSize(this.getWidth(), oViewSetting.getSize().height);
//                } else {
//                    this.setSize(this.getWidth(), oDetailQuote.getSize().height + marketDepth.getSize().height +85);
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
            //   this.setSize(this.getWidth(), oDetailQuote.getSize().height + marketDepth.getSize().height +timeAndSales.getSize().height);
        }
    }
}
