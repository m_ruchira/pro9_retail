package com.isi.csvr.brokers;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Meta;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 23, 2007
 * Time: 3:05:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopBrokersStore implements Runnable, Comparator {
    private static TopBrokersStore self = null;
    private boolean continueLoop;
    private TopBrokersWindow parent;
    private int requestType = -1;
    private String exchange;
    private boolean isDefaultExchange;
    private DynamicArray store;
    //    private ArrayList list;
    private Broker[] list;

    public TopBrokersStore() {
        store = new DynamicArray();
    }

    public static synchronized TopBrokersStore getSharedInstance() {
        if (self == null) {
            self = new TopBrokersStore();
        }
        return self;
    }

    public void setParent(TopBrokersWindow parent) {
        this.parent = parent;
    }

    public void clear() {
        store.clear();
    }


//    public void setBrokers(ArrayList list) {
//        this.list = list;
//    }

    public void setBrokers(Broker[] list) {
        this.list = list;
    }

    public DynamicArray getTopStocks() {
        return store;
    }

    public void setTopStocks(TopBrokerObject oTops) {
        store.add(oTops);
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }


    public void run() {
        continueLoop = true;
        while (continueLoop) {
            if (parent.isWindowVisible()) { //Settings.isConnected() &&
                if (isDefaultExchange) {
                    updateData();
                    sleepMe(1000);
                } else {
                    requestTopStocksData();
                    sleepMe(10000);
                }
            } else {
                sleepMe(3000);
            }
        }
    }


    public void requestTopStocksData() {
        String strReq = "";

        try {
            if ((exchange != null) && (requestType >= 0)) {
                if (!ExchangeStore.isExpired(exchange) && !ExchangeStore.isInactive(exchange)) {
//                strReq = Meta.TOP_STOCKS + Meta.DS + exchange + Meta.FD + requestType;

                    SendQFactory.addData(Constants.PATH_PRIMARY, strReq);
                }
            }
        } catch (Exception ex) {
        }
        strReq = null;
    }

    private void updateData() {
        try {
            boolean valid = false;
            int index = 0;
            Arrays.sort(list);
//            Collections.sort(list, this);
            clear();
//            for (int i = 0; (i < list.size()) && (index < 10); i++) {
            for (int i = 0; (i < list.length) && (index < 10); i++) {
//                Broker stock = BrokerStore.getSharedInstance().getBroker((String)list.get(i));
                Broker stock = list[i];
                switch (requestType) {
                    case Meta.MOST_ACTIVE_BY_VOLUME:
                        valid = stock.getTotalVolume() > 0;
                        break;
                    case Meta.MOST_ACTIVE_BY_TRADES:
                        valid = stock.getTransFrequency() > 0;
                        break;
                    case Meta.MOST_ACTIVE_BY_VALUE:
                        valid = stock.getTotalValue() > 0;
                        break;
                    default:
                        valid = false;
                }
                if (valid) {
                    TopBrokerObject topstocksObject = new TopBrokerObject();
                    topstocksObject.setSymbol(stock.getId());
                    topstocksObject.setDescription(stock.getName());
                    topstocksObject.setValue(stock.getTotalValue());
                    topstocksObject.setVolume(stock.getTotalVolume());
                    topstocksObject.setTrades(stock.getTransFrequency());
                    setTopStocks(topstocksObject);
                    index = index + 1;
                    topstocksObject = null;
                }
                stock = null;
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setExchange(String exchange, boolean isDefault) {
        this.exchange = exchange;
        this.isDefaultExchange = isDefault;
    }

    private void sleepMe(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
        }
    }

    public void setRecords(String data) {
        String[] records = data.split(Meta.FD);

        if (records.length > 2) {
            clear();
            for (int i = 2; i < records.length; i++) {
                String[] fields = records[i].split(Meta.ID);
                TopBrokerObject topstocksObject = new TopBrokerObject();
                topstocksObject.setData(fields);
                setTopStocks(topstocksObject);
                fields = null;
                topstocksObject = null;
            }
        }
        records = null;
    }

    public void exitFromLoop() {
        continueLoop = false;
    }

    // Comparator methods
    public int compare(Object o1, Object o2) {
//        Broker s1 = BrokerStore.getSharedInstance().getBroker((String) o1);
//        Broker s2 = BrokerStore.getSharedInstance().getBroker((String) o2);
        Broker s1 = ExchangeStore.getSharedInstance().getBroker(exchange, (String) o1);
        Broker s2 = ExchangeStore.getSharedInstance().getBroker(exchange, (String) o2);

        switch (requestType) {
            case Meta.MOST_ACTIVE_BY_VOLUME:
                if (s2.getTotalVolume() == s1.getTotalVolume()) {
                    return s1.getName().compareTo(s2.getName());
                } else {
                    return (int) (s2.getTotalVolume() - s1.getTotalVolume());
                }
            case Meta.MOST_ACTIVE_BY_TRADES:
                if (s2.getTransFrequency() == s1.getTransFrequency()) {
                    return s1.getName().compareTo(s2.getName());
                } else {
                    return (int) (s2.getTransFrequency() - s1.getTransFrequency());
                }
            case Meta.MOST_ACTIVE_BY_VALUE:
                if (s2.getTotalValue() == s1.getTotalValue()) {
                    return s1.getName().compareTo(s2.getName());
                } else {
                    return (int) (s2.getTotalValue() - s1.getTotalValue());
                }
            default:
                return 0;
        }
    }
}
