package com.isi.csvr.brokers;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.TWComboBox;
import com.isi.csvr.shared.TWComboItem;
import com.isi.csvr.shared.TWComboModel;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 21, 2007
 * Time: 3:06:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class BrokersWindow extends InternalFrame implements MouseListener, Themeable, ExchangeListener, ItemListener,
        ActionListener {

    private static BrokersWindow self = null;
    private ViewSetting viewSetting;
    private Table table;
    private BrokerModel model;
    private JPanel exchangePanel;
    private TWComboBox excgCombo;
    private ArrayList<TWComboItem> exchangeList;
    private String exchange;

    private BrokersWindow() {
        createUI();
        Theme.registerComponent(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);

    }

    public static BrokersWindow getSharedInstance() {
        if (self == null) {
            self = new BrokersWindow();
        }
        return self;
    }


    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }

    private void createUI() {
        viewSetting = ViewSettingsManager.getSummaryView("BROKERS");
        table = new Table();
        table.setSortingEnabled();
        model = new BrokerModel();
        table.setHeaderPopupActive(true);
        model.setViewSettings(viewSetting);
        table.setModel(model);
        model.setTable(table);
        viewSetting.setParent(this);

        table.setWindowType(ViewSettingsManager.BROKERS_VIEW);
        table.getTable().addMouseListener(this);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setLocation(viewSetting.getLocation());
        this.setSize(viewSetting.getSize());
        this.setIconifiable(true);
        this.setDetachable(true);
        this.setColumnResizeable(true);
        this.setTable(table);

        exchangePanel = new JPanel(); //new FlowLayout(FlowLayout.LEADING, 8, 2));
        exchangePanel.setLayout(new FlexGridLayout(new String[]{"30%", "30%", "40%"}, new String[]{"25"}, 5, 5));

        exchangeList = new ArrayList<TWComboItem>();
        excgCombo = new TWComboBox(new TWComboModel(exchangeList));
        excgCombo.setPreferredSize(new Dimension(150, 25));
        exchangePanel.add(excgCombo);
        getContentPane().add(exchangePanel, BorderLayout.NORTH);
        getContentPane().add(table, BorderLayout.CENTER);
        excgCombo.addActionListener(this);
        this.setVisible(viewSetting.isVisible());
        this.setDetached(viewSetting.isDetached);
        populateExchanges();
//        try {
//            this.setVisible(viewSetting.isVisible());
//        } catch (Exception e) {
//            viewSetting.setVisible(true);
//            this.setVisible(viewSetting.isVisible());
//        }
        Client.getInstance().getDesktop().add(this);
        this.setTitle(viewSetting.getCaption());
        this.setLayer(GUISettings.TOP_LAYER);
        GUISettings.applyOrientation(this);
//        this.setLayer();
        applyTheme();
        try {
            applySettings();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);    //To change body of overridden methods use File | Settings | File Templates.
    }


    public void exchangeAdded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getSelectedExchange() {
        try {
            return ((TWComboItem) excgCombo.getSelectedItem()).getId();
        } catch (Exception ex) {
            return null;
        }
    }

    private void populateExchanges() {
        excgCombo.removeItemListener(this);
        exchangeList.clear();

        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                exchangeList.add(comboItem);
            }
            exchange = null;
        }

//        for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
//            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
//            TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
//            exchangeList.add(comboItem);
//            exchange = null;
//        }

        Collections.sort(exchangeList);
        excgCombo.updateUI();
        if (excgCombo.getSelectedIndex() < 0) {
            excgCombo.setSelectedIndex(0);
        }
        excgCombo.addItemListener(this);
    }

    public void exchangesAdded(boolean offlineMode) {

        populateExchanges();
        try {
            if (exchange == null) {
                exchange = getSelectedExchange();
            }
            if (exchange == null) {
                try {
                    exchange = exchangeList.get(0).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        try {
            for (int i = 0; i < excgCombo.getItemCount(); i++) {
                if (((TWComboItem) excgCombo.getItemAt(i)).getId().equals(exchange)) {
                    excgCombo.setSelectedIndex(i);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
        model.setExchange(getSelectedExchange());
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == excgCombo) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                exchange = getSelectedExchange();
                model.setExchange(exchange);
                this.repaint();
            }
        }
    }


    public void actionPerformed(ActionEvent e) {
//        if (e.getSource() == excgCombo) {
//            exchange = getSelectedExchange();
//            model.setExchange(exchange);
//        }
    }
}
