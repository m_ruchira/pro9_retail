package com.isi.csvr.brokers;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 21, 2007
 * Time: 3:13:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class BrokerModel extends CommonTable
        implements CommonTableInterface {

    String exchange;
    Broker[] brokers;
    int count = 0;
    //    private DoubleTransferObject doubleTransferObject;
//    private LongTransferObject longTransferObject;
    private String defaultString;
//    private TWDecimalFormat decimalFormat;
//    private TWDecimalFormat numberFormat;

    public BrokerModel() {
//        doubleTransferObject = new DoubleTransferObject();
//        longTransferObject = new LongTransferObject();
        defaultString = Language.getString("NA");
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
        try {
            brokers = ExchangeStore.getSharedInstance().getExchange(exchange).getBrokers();
            count = ExchangeStore.getSharedInstance().getExchange(exchange).getBrokerCount();
        } catch (Exception e) {
            brokers = new Broker[0];
            count = 0;
        }
    }

    public int getRowCount() {
        return count;
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }


    public Object getValueAt(int rowIndex, int columnIndex) {
        Broker broker = brokers[rowIndex];
        switch (columnIndex) {
            case 0:
                return broker.getId();
            case 1:
                return broker.getName();
            case 2:
                return "" + broker.getBoughtValue();          //doubleTransferObject.setValue
            case 3:
                return "" + broker.getSoldValue();         //doubleTransferObject.setValue
            case 4:
                return "" + broker.getTotalValue();       //doubleTransferObject.setValue
            case 5:
                return "" + broker.getBoughtVolume();       //longTransferObject.setValue
            case 6:
                return "" + broker.getSoldVolume();        //longTransferObject.setValue
            case 7:
                return "" + broker.getTotalVolume();      //longTransferObject.setValue
            case 8:
                return "" + broker.getTransFrequency();   //longTransferObject.setValue
            default:
                return defaultString;
        }
    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];

    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
//            case 2:
//            case 3:
//            case 4:
//            case 5:
//            case 6:
//            case 7:
//            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return String.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
}
