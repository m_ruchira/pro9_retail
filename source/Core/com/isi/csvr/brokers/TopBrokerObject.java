package com.isi.csvr.brokers;

import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 23, 2007
 * Time: 3:07:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopBrokerObject {
    private String symbol;
    private long volume;
    private long trades;
    private double value;
    private String description;

    public void setData(String[] data) {
        this.symbol = data[0];
        this.description = data[0];
        this.value = (float) SharedMethods.getDouble(data[3]);
        this.volume = SharedMethods.getLong(data[4]);
        this.trades = SharedMethods.getLong(data[5]);
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public long getTrades() {
        return trades;
    }

    public void setTrades(long trades) {
        this.trades = trades;
    }

    public String getDescription() {
//        if ((description == null) || (description.equals("")))
//            return symbol;
//        else
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
