package com.isi.csvr.brokers;

import com.isi.csvr.TableSorter;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.ExchangeFormatInterface;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 23, 2007
 * Time: 9:57:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class BrokerTableRenderer implements TableCellRenderer, TWTableRenderer {

    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();
    private static Color g_oUpColor;
    private static Color g_oDownColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private String[] g_asColumns;
    private int[] g_asRendIDs;
    private boolean g_bLTR;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private String[] g_asMarketStatus = new String[4];
    private String g_sNA = "NA";
    private Color upColor;
    private Color downColor;
    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oPriceFormatLong;
    private TWDecimalFormat oNumericFormat;
    private TWDecimalFormat oQuantityFormat;
    private DefaultTableCellRenderer lblRenderer;
    private ImageIcon g_oUPImage;
    private ImageIcon g_oDownImage;
    private double doubleValue;
    private long longValue;
    private Long longObjectValue;
    private int intValue;

    public BrokerTableRenderer(String[] asColumns, int[] asRendIDs) {
        g_asColumns = asColumns;
        g_asRendIDs = asRendIDs;
        g_bLTR = Language.isLTR();

        reload();
        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oPriceFormatLong = new TWDecimalFormat(" ###,##0.0000 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oNumericFormat = new TWDecimalFormat(" ########## ");

        try {
            g_oUPImage = new ImageIcon("images/Common/up.gif");
        } catch (Exception e) {
            g_oUPImage = null;
        }
        try {
            g_oDownImage = new ImageIcon("images/Common/down.gif");
        } catch (Exception e) {
            g_oDownImage = null;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oUpColor = Color.black;
        g_oDownColor = Color.black;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        try {
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
        } catch (Exception e) {
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void propertyChanged(int property) {
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        Component renderer = DEFAULT_RENDERER.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        lblRenderer = (DefaultTableCellRenderer) renderer;

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            upColor = g_oUpColor;
            downColor = g_oDownColor;
        }

        renderer.setForeground(foreground);
        renderer.setBackground(background);

        try {
            try {
                oPriceFormat = (((ExchangeFormatInterface) table.getModel()).getDecimalFormat());
            } catch (Exception e) {
                oPriceFormat = (((ExchangeFormatInterface) ((TableSorter) table.getModel()).getModel()).getDecimalFormat());
            }
        } catch (Exception e) {
            // do nothing
        }

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE

                    doubleValue = toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'R': // PRICE
                    doubleValue = toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormatLong.format(doubleValue)));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    }
                    break;
                case 'N': // Numeric
                    //adPrice = (double[])value;
                    lblRenderer.setText(oNumericFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
                case 5: // CHANGE
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 6: // % CHANGE
                    //adPrice = (double[])value;
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 9: // TICK
                    intValue = toIntValue(value);
                    switch (intValue) {
                        case Settings.TICK_UP:
                            lblRenderer.setIcon(g_oUPImage);
                            lblRenderer.setText("");
                            break;
                        case Settings.TICK_DOWN:
                            lblRenderer.setIcon(g_oDownImage);
                            lblRenderer.setText("");
                            break;
                        default:
                            lblRenderer.setIcon(null);
                            lblRenderer.setText("");
                    }
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }


        return renderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }
}
