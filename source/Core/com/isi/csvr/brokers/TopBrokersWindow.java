package com.isi.csvr.brokers;

import com.isi.csvr.Client;
import com.isi.csvr.ClientTable;
import com.isi.csvr.TWDesktop;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.topstocks.TopStocksStore;
import com.isi.csvr.topstocks.TopStocksTypeComboItem;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 23, 2007
 * Time: 3:06:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopBrokersWindow implements InternalFrameListener, ItemListener, MouseListener,
        ActionListener, MenuListener, ExchangeListener, TWDesktopInterface, ApplicationListener {

    private static TopBrokersWindow self = null;
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private int FRAME_WIDTH = 530;
    private int FRAME_HEIGHT = 300;
    private Table table;
    private int selectedTab;
    private boolean justLoaded = true;
    //    private TWButton btnRefresh;
    private JPanel exchangePanel;
    private TopBrokersStore dataUpdator;
    private Thread dataUpdatorThread;
    private TWComboBox excgCombo;
    private ArrayList<TWComboItem> exchangeList;
    private TWComboBox typeCombo;
    //    private TWMenu gainersToMainboard;
    private String exchange;
    private ViewSetting viewSetting;
    private InternalFrame frame;

    private TopBrokersWindow() {

        createLayout();

        dataUpdator = TopBrokersStore.getSharedInstance();
        dataUpdator.setParent(this);
        dataUpdatorThread = new Thread(dataUpdator, "TW Top Brokers");
        dataUpdatorThread.setPriority(Thread.NORM_PRIORITY - 2);
        dataUpdatorThread.start();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Application.getInstance().addApplicationListener(this);
    }

    public static synchronized TopBrokersWindow getSharedInstance() {
        if (self == null) {
            self = new TopBrokersWindow();
        }
        return self;
    }

    public static void init() {
        self = new TopBrokersWindow();
    }

    public void createLayout() {
        viewSetting = ViewSettingsManager.getSummaryView("TOP_BROKERS");
        createDataPanels();

        frame = new InternalFrame(table);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setPrintable(true);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setRequestFocusEnabled(false);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.addInternalFrameListener(this);
        frame.setTitle(viewSetting.getCaption());
        viewSetting.setParent(frame);

        exchangePanel = new JPanel(); //new FlowLayout(FlowLayout.LEADING, 8, 2));
        exchangePanel.setLayout(new FlexGridLayout(new String[]{"30%", "30%", "40%"}, new String[]{"25"}, 5, 5));

        exchangeList = new ArrayList<TWComboItem>();
        excgCombo = new TWComboBox(new TWComboModel(exchangeList));
        excgCombo.setPreferredSize(new Dimension(150, 25));


        typeCombo = new TWComboBox();
        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_MOST_ACTIVE_BY_VOLUME"), Meta.MOST_ACTIVE_BY_VOLUME));
        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_MOST_ACTIVE_BY_VALUE"), Meta.MOST_ACTIVE_BY_VALUE));
        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_MOST_ACTIVE_BY_TRADES"), Meta.MOST_ACTIVE_BY_TRADES));
        typeCombo.addItemListener(this);

        exchangePanel.add(excgCombo);
        exchangePanel.add(typeCombo);
        setMarketCombo(null);
        JLabel messageLabel = new JLabel(Language.getString("TOP_STOCKS_DELAY_MESG"));
        messageLabel.setHorizontalAlignment(JLabel.CENTER);
        messageLabel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED, UIManager.getColor("controlLtHighlight"), UIManager.getColor("controlShadow")));

        Client.getInstance().getDesktop().add(frame);
        frame.setLayer(GUISettings.TOP_LAYER);

        frame.getContentPane().add(exchangePanel, BorderLayout.NORTH);
        frame.getContentPane().add(table, BorderLayout.CENTER);

        frame.setSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        applyTheme();
    }

    private void createDataPanels() {
        createTable();
        table.getModel().setViewSettings(viewSetting);
        applyTableSettings(table);
        /*gainersToMainboard = new TWMenu(Language.getString("POPUP_ADD_TO_MAINBOARD"), "addtomainboard.gif");
        table.getPopup().setMenu(gainersToMainboard);
        gainersToMainboard.addMenuListener(this);*/
    }


    private void applyTableSettings(Table oTable) {
        try {
            oTable.setBorder(null);
            oTable.setRequestFocusEnabled(false);
            oTable.getModel().applySettings();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void applyTheme() {
        try {
            SwingUtilities.updateComponentTreeUI(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void switchPanels() {
        if (selectedTab < 3) {
            selectedTab++;
        } else {
            selectedTab = 0;
        }
    }

    private void showStartPanel() {
        TopStocksTypeComboItem item = (TopStocksTypeComboItem) typeCombo.getSelectedItem();
        dataUpdator.setRequestType(item.getType());
        item = null;
    }

    public void setRecords(String data) {
        dataUpdator.setRecords(data);
    }

    public String getSelectedExchange() {
        try {
            return ((TWComboItem) excgCombo.getSelectedItem()).getId();
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean isVisible() {
        return frame.isVisible();
    }

    public void setVisible(boolean status) {
        frame.setVisible(status);
    }

    public boolean isSelected() {
        return frame.isSelected();
    }

    public void setSelected(boolean status) throws Exception {
        frame.setSelected(status);
    }

    public boolean isIcon() {
        return frame.isIcon();
    }

    public void setIcon(boolean status) throws Exception {
        frame.setIcon(status);
    }

    /**
     * Invoked when a internal frame has been opened.
     *
     * @see javax.swing.JInternalFrame#show
     */
    public void internalFrameOpened(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is in the process of being closed.
     * The close operation can be overridden at this point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {
        if (justLoaded) {
            showStartPanel();
            justLoaded = false;
        }
    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {

    }

    private void createTable() {
        try {
            Table oTable = new Table();
            oTable.setHeaderPopupActive(true);
            TopBrokersModel model = new TopBrokersModel();
            model.setViewSettings(viewSetting);
            oTable.setModel(model);
            model.setTable(oTable);
            oTable.getTable().addMouseListener(this);
            if (oTable != null) {
                if (viewSetting.getID().equals("TOP_BROKERS")) {
                    model.setDataStore(TopBrokersStore.getSharedInstance().getTopStocks());
                    table = oTable;
                }
            }

            oTable = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int intValue(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void closeWindow() {
        if (frame.getDefaultCloseOperation() == JFrame.HIDE_ON_CLOSE) {
            frame.setVisible(false);
        } else {
            frame.dispose();
        }
    }

    public void invalidateWindow() {
        dataUpdator.exitFromLoop();
    }

    public int getDesktopItemType() {
        return POPUP_TYPE;
    }

    public boolean isWindowVisible() {
        return frame.isVisible();
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == excgCombo) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                TopStocksStore.getSharedInstance().clear();
                exchange = getSelectedExchange();
                setMarketCombo(exchange);
//                setBrokers(BrokerStore.getSharedInstance().getBrokers());
                setBrokers(ExchangeStore.getSharedInstance().getExchange(exchange).getBrokers());
                dataUpdator.setExchange(exchange, ExchangeStore.getSharedInstance().isDefault(exchange));
                viewSetting.putProperty(ViewConstants.VC_TOP_STOCK_EXCHANGE, getSelectedExchange());
//                viewSetting.putProperty(ViewConstants.VC_TOP_STOCK_EXCHANGE, getSelectedExchange());
                dataUpdatorThread.interrupt();
                if (!ExchangeStore.getSharedInstance().isDefault(exchange)) {
                    dataUpdator.requestTopStocksData();
                }

            }
        } else if (e.getSource() == typeCombo) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                sendRequest();
            }
        }
    }

    public void sendRequest() {

        TopStocksTypeComboItem item = (TopStocksTypeComboItem) typeCombo.getSelectedItem();
        dataUpdator.setRequestType(item.getType());
        TopStocksStore.getSharedInstance().clear();
        viewSetting.putProperty(ViewConstants.VC_TOP_STOCKS_TYPE, item.getType());
        item = null;
        if (!ExchangeStore.getSharedInstance().isDefault(exchange)) {
            dataUpdator.requestTopStocksData();
        }
    }

    private void setInitialParams() {
        try {
            String exchangesID = viewSetting.getProperty(ViewConstants.VC_TOP_STOCK_EXCHANGE);
            String[] data = new String[1];
            String exchangeID = null;
            try {
                data = exchangesID.split(Constants.MARKET_SEPERATOR_CHARACTER);
                exchangeID = data[0];
            } catch (Exception e) {
                data[0] = exchangesID;
                exchangeID = exchangesID;
            }
            if ((exchangeID != null) && (ExchangeStore.getSharedInstance().isValidExchange(exchangeID))) {
                TopStocksStore.getSharedInstance().clear();
                exchange = exchangeID;
                setMarketCombo(exchange);
                String marketID = null;
                try {
                    marketID = data[1];
                } catch (Exception e) {
                    marketID = null;
                }
//                setBrokers(BrokerStore.getSharedInstance().getBrokers());
                setBrokers(ExchangeStore.getSharedInstance().getExchange(exchange).getBrokers());
                dataUpdator.setExchange(exchange, ExchangeStore.getSharedInstance().isDefault(exchange));
                if (!ExchangeStore.getSharedInstance().isDefault(exchange)) {
                    dataUpdator.requestTopStocksData();
                }
            }

            exchangeID = null;
//            marketID = null;
//            exchangesID = null;
//            data = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            for (int i = 0; i < excgCombo.getItemCount(); i++) {
                if (((TWComboItem) excgCombo.getItemAt(i)).getId() == exchange) {
                    excgCombo.setSelectedIndex(i);
                    break;
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }

//        try {
//            for (int i = 0; i < excgCombo.getItemCount(); i++) {
//                if (((TWComboItem) typeCombo.getItemAt(i)).getId() == exchange) {
//                    typeCombo.setSelectedIndex(i);
//                    break;
//                }
//            }
//        } catch (Exception e) {
//            //e.printStackTrace();  //To change body of catch statement use Options | File Templates.
//        }

        try {
            int lattType = Integer.parseInt(viewSetting.getProperty(ViewConstants.VC_TOP_STOCKS_TYPE));
            for (int i = 0; i < typeCombo.getItemCount(); i++) {
                if (((TopStocksTypeComboItem) typeCombo.getItemAt(i)).getType() == lattType) {
                    typeCombo.setSelectedIndex(i);
                    break;
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }

//##############################################################################
//     MouseListener Interface Methods
//##############################################################################

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void showSummaryQuote(String symbol) {
//        if (ExchangeStore.getSharedInstance().isDefault(exchange))
//        Client.getInstance().showDetailQuote(SharedMethods.getKey(exchange,  symbol), true);
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof JTable) {
            if (e.getClickCount() > 1) {
                JTable table = (JTable) e.getSource();
                showSummaryQuote("" + table.getValueAt(table.getSelectedRow(), -1));
                table = null;
            }
        }
    }

    public void tableFontChanged(Font font) {
        table.getTable().setFont(font);
        table.getTable().repaint();
        viewSetting.setFont(font);
    }

//    public void setBrokers(ArrayList symbols) {
//        dataUpdator.setBrokers(symbols);
//    }

    public void setBrokers(Broker[] symbols) {
        dataUpdator.setBrokers(symbols);
    }

    public void headerFontChanged(Font font) {
    }

    public void applySettings() {
        frame.applySettings();
        table.getModel().applySettings();
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        int tableType = Integer.parseInt(command.substring(0, 1));
        command = command.substring(1);
        String symbol = null;
        int row;

        switch (tableType) {
            case 0:
                row = table.getTable().getSelectedRow();
                row = table.getTable().getSelectedRow();
                symbol = (String) table.getTable().getModel().getValueAt(row, 0);
                break;
        }
    }

    private void createWSPopUp(JMenu menu) {
        try {
            String wsName = null;
            JMenuItem wsItemMenu = null;
            int id = 0;

            /*if (menu == gainersToMainboard) {
                id = 0;
            }*/
            Object[] windowList = ((TWDesktop) Client.getInstance().getDesktop()).getWindows();
            if (windowList == null)
                return;
            ViewSetting oSettings = null;
            menu.removeAll();
            for (int i = 0; i < windowList.length; i++) {
                try {
                    JInternalFrame frame = (JInternalFrame) windowList[i];
                    if (!(frame instanceof ClientTable)) {
                        continue;
                    }
                    wsName = frame.getTitle();
                    oSettings = ((ClientTable) frame).getViewSettings();
                    wsItemMenu = new JMenuItem(wsName);
                    wsItemMenu.addActionListener(this);
                    wsItemMenu.setActionCommand(id + oSettings.getID());
                    menu.add(wsItemMenu);
                    frame = null;
                } catch (Exception ex) {
                }
            }
            windowList = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMarketCombo(String exchange) {
        String excg;
        if (exchange == null)
            excg = getSelectedExchange();
        else
            excg = exchange;
        if (excg != null) {
            exchangePanel.removeAll();
            exchangePanel.setLayout(new FlexGridLayout(new String[]{"30%", "40%"}, new String[]{"25"}, 5, 5));
            exchangePanel.add(excgCombo);
            exchangePanel.add(typeCombo);
            exchangePanel.doLayout();
            exchangePanel.repaint();
        }
        excg = null;
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        excgCombo.removeItemListener(this);
        exchangeList.clear();

        for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
            if (!exchange.isExpired() && !exchange.isInactive()) {
                TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                exchangeList.add(comboItem);
                exchange = null;
            }
        }

        Collections.sort(exchangeList);
        excgCombo.updateUI();
        if ((excgCombo.getSelectedIndex() < 0) && (exchangeList.size() > 0)) {
            excgCombo.setSelectedIndex(0);
        }
        excgCombo.addItemListener(this);

        try {
            if (exchange == null) {
                exchange = getSelectedExchange();
            }
            if (exchange == null) {
                try {
                    exchange = exchangeList.get(0).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            setMarketCombo(exchange);
//            setBrokers(BrokerStore.getSharedInstance().getBrokers());
            setBrokers(ExchangeStore.getSharedInstance().getExchange(exchange).getBrokers());
            dataUpdator.setExchange(exchange, ExchangeStore.getSharedInstance().isDefault(exchange));
            if (!ExchangeStore.getSharedInstance().isDefault(exchange)) {
                dataUpdator.requestTopStocksData();
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        try {
            for (int i = 0; i < excgCombo.getItemCount(); i++) {
                if (((TWComboItem) excgCombo.getItemAt(i)).getId().equals(exchange)) {
                    excgCombo.setSelectedIndex(i);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public JTable getTable1() {
        return table.getTable();
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        createWSPopUp((JMenu) e.getSource());
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void menuSelected(MenuEvent e) {
        createWSPopUp((JMenu) e.getSource());
    }

    public void menuDeselected(MenuEvent e) {
    }

    public void menuCanceled(MenuEvent e) {
    }

    public String getTitle() {
        return frame.getTitle();
    }

    public int getWindowType() {
        return frame.getWindowType();
    }

    public void setWindowType(int type) {
        frame.setWindowType(type);
    }

    public void applicationExiting() {

    }

    public void applicationLoaded() {

    }

    public void applicationLoading(int percentage) {

    }

    public void applicationReadyForTransactions() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void workspaceLoaded() {
        setInitialParams();
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }
}
