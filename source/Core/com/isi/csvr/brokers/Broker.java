package com.isi.csvr.brokers;

import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 21, 2007
 * Time: 1:42:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class Broker implements Comparable {

    public static final char BATE_BOUGHT_VALUE = 'A';
    public static final char BATE_SOLD_VALUE = 'B';
    public static final char BATE_TOTAL_VALUE = 'C';
    public static final char BATE_BOUGHT_VOLUME = 'D';
    public static final char BATE_SOLD_VOLUME = 'E';
    public static final char BATE_TOTAL_VOLUME = 'F';
    public static final char BATE_TRANS_FREQUENCY = 'G';
    private String id = null;
    private String name = null;
    private double boughtValue = 0;
    private double soldValue = 0;
    private double totalValue = 0;
    private long boughtVolume = 0;
    private long soldVolume = 0;
    private long totalVolume = 0;
    private long transFrequency = 0;

    public Broker(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBoughtValue() {
        return boughtValue;
    }

    public double getSoldValue() {
        return soldValue;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public long getBoughtVolume() {
        return boughtVolume;
    }

    public long getSoldVolume() {
        return soldVolume;
    }

    public long getTotalVolume() {
        return totalVolume;
    }

    public long getTransFrequency() {
        return transFrequency;
    }

    public String setData(String[] data) {
        if (data == null)
            return "";

        char tag;
        for (int i = 0; i < data.length; i++) {
            tag = data[i].charAt(0);
            switch (tag) {
                case BATE_BOUGHT_VALUE:
                    boughtValue = SharedMethods.getDouble(data[i].substring(1));
                    break;
                case BATE_SOLD_VALUE:
                    soldValue = SharedMethods.getDouble(data[i].substring(1));
                    break;
                case BATE_TOTAL_VALUE:
                    totalValue = SharedMethods.getDouble(data[i].substring(1));
                    break;
                case BATE_BOUGHT_VOLUME:
                    boughtVolume = SharedMethods.getLong(data[i].substring(1));
                    break;
                case BATE_SOLD_VOLUME:
                    soldVolume = SharedMethods.getLong(data[i].substring(1));
                    break;
                case BATE_TOTAL_VOLUME:
                    totalVolume = SharedMethods.getLong(data[i].substring(1));
                    break;
                case BATE_TRANS_FREQUENCY:
                    transFrequency = SharedMethods.getLong(data[i].substring(1));
                    break;
            }
        }
        return getId();
    }


    public int compareTo(Object o) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
