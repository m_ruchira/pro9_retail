package com.isi.csvr.announcementnew;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.announcement.*;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.news.SearchedNewsStore;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.AnnouncementModel;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;
import com.mubasher.outlook.Reminder;
import com.mubasher.outlook.ReminderUI;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by IntelliJ IDEA.
 * User: Hasika
 * Date: May 8, 2008
 * Time: 5:50:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class AnnouncementSearchWindow extends InternalFrame implements ExchangeListener, ActionListener, KeyListener, Themeable, FocusListener, ApplicationListener,
        ConnectionListener {

    private static final String NULL_EXCHANGE = "";
    private static final String ALL_EXCHANGE = "All";
    public static AnnouncementSearchWindow self;
    final ImageIcon icon = new ImageIcon("images\\Common\\busy.gif");
    final ImageIcon iconBusy = new ImageIcon("images\\Common\\busy.gif");
    final String searching = Language.getString("SEARCHING");
    ArrayList<TWComboItem> exchangesArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> sectorArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> typeArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> mktArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> exchangeArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> predefArray = new ArrayList<TWComboItem>();
    ViewSetting oSetting1;
    ViewSetting oSetting2;
    private JPanel mainPanel;
    private TWTabbedPane announcementTabPanel;
    private JPanel realTimePanel;
    private JPanel searchPanel;
    private JPanel realTimeLowerPanel;
    private JPanel realTimeUpperPanel;
    private JPanel searchUpper;
    private JPanel criteriaPanel;
    private JPanel generalSearchPanell;
    private JPanel periodPanel;
    private JPanel buttonPanel;
    private JPanel hidePanel;
    private JPanel searchLowerPanel;
    private JLabel exchangeLabel, typeLabel, languageLabel, keywordLabel, busyLabel;
    private TWComboBox cmbLanguage, cmboExchanges;
    //private CWTabbedPaneUI tabbedPaneUI;
    private TWComboBox cmbExchange;
    private TWComboBox typeCombo;
    private TWComboBox mktCombo;
    private TWComboBox sectorCombo;
    private TWComboBox cmbPredef;
    private JTextField keywordField;
    private JTextField symbolField;
    private TWButton btnDownArraow;
    private TWMenuItem mnuOutlook;
    private boolean isSymbolSearch = false;
    private String[][] periods = {
            {Language.getString("ALL"), "" + Calendar.YEAR, "100"},
            {Language.getString("1_MONTH"), "" + Calendar.MONTH, "1"},
            {Language.getString("3_MONTH"), "" + Calendar.MONTH, "3"},
            {Language.getString("6_MONTH"), "" + Calendar.MONTH, "6"},
            {Language.getString("1_YEAR"), "" + Calendar.YEAR, "1"},
            {Language.getString("2_YEAR"), "" + Calendar.YEAR, "2"},
            {Language.getString("3_YEAR"), "" + Calendar.YEAR, "3"},
            {Language.getString("5_YEAR"), "" + Calendar.YEAR, "5"},
            {Language.getString("10_YEAR"), "" + Calendar.YEAR, "10"}};
    private JLabel lblexchange;
    private TWComboModel exchangeModel;
    private TWComboModel exchangesModel;
    private TWComboModel sectorModel;
    private TWComboModel preDefModel;
    private TWComboModel mktModel;
    private TWComboModel typeModel;
    private TWButton btnNext;
    private TWButton btnPrev;
    private TWButton btnNextL;
    private TWButton btnPrevL;
    private TWButton btnSearch;
    private TWButton btnAdvSearch;
    private TWButton btnHide;
    private JButton btnShow;
    private JRadioButton rbPreDef = new JRadioButton(Language.getString("PRE_DEFINED"));
    private JRadioButton rbCustom = new JRadioButton(Language.getString("CUSTOM_NEWS"));
    private ButtonGroup periodGroup = new ButtonGroup();
    private DateCombo fromDateCombo;
    private DateCombo toDateCombo;
    private Table table1;
    private AnnouncementModel model1;
    private Table table2;
    private AnnouncementModel model2;
    private boolean isLatest = true;
    private AnnouncementMouseListener selectionListener;
    private int width = 775;
    private int height = 550;
    private boolean isconnected = true;
    //adding auto popup enable panel
    private JPanel autoPopUpPanal = new JPanel();
    private JCheckBox chkDoNotAutoPopup;
    private long fromDate = 0;
    private long toDate = 0;
    private AdvanceAnnouncementWindow advanceframe;

    private AnnouncementSearchWindow() {
        mainPanel = new JPanel();
        realTimePanel = new JPanel();
        searchPanel = new JPanel();
        announcementTabPanel = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "nw");
        setSelectedLanguage(Language.getSelectedLanguage());
        createUI();
    }

    public static AnnouncementSearchWindow getSharedInstance() {
        if (self == null) {
            self = new AnnouncementSearchWindow();
        }
        return self;
    }

    public void createUI() {
        oSetting1 = ViewSettingsManager.getSummaryView("ANNOUNCEMENT_SEARCH");
        oSetting1.setParent(this);
        oSetting2 = ViewSettingsManager.getSummaryView("BTW_ANNOUNCEMENT");
        oSetting2.setParent(this);
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        mainPanel.add(announcementTabPanel);
        lblexchange = new JLabel(Language.getString("EXCHANGE"));
        sectorModel = new TWComboModel(sectorArray);
        exchangesModel = new TWComboModel(exchangesArray);
        exchangeModel = new TWComboModel(exchangeArray);
        cmbExchange = new TWComboBox(exchangeModel);
        cmbExchange.addActionListener(this);
        mktModel = new TWComboModel(mktArray);
        btnNext = new TWButton(Language.getString("NEXT"));
        btnNext.addActionListener(this);
        btnNext.setActionCommand("NEXT");
        btnPrev = new TWButton(Language.getString("PREVIOUS"));
        btnPrev.addActionListener(this);
        btnPrev.setActionCommand("PREVIOUS");
        btnNext.setEnabled(false);
        btnPrev.setEnabled(false);
        btnNextL = new TWButton(Language.getString("NEXT"));
        btnNextL.addActionListener(this);
        btnNextL.setActionCommand("NEXTL");
        btnPrevL = new TWButton(Language.getString("PREVIOUS"));
        btnPrevL.addActionListener(this);
        btnPrevL.setActionCommand("PREVIOUSL");
        btnNextL.setEnabled(false);
        btnPrevL.setEnabled(false);
        btnSearch = new TWButton(Language.getString("WINDOW_TITLE_SEARCH"));
        btnSearch.addActionListener(this);
        btnSearch.setActionCommand("SEARCH");
        btnAdvSearch = new TWButton(Language.getString("NEWS_AD_SEARCH"));
        btnAdvSearch.addActionListener(this);
        btnAdvSearch.setActionCommand("ADVSEARCH");
        btnHide = new TWButton(Language.getString("HIDE"));
        btnHide.addActionListener(this);
        btnHide.setActionCommand("HIDE");
        btnShow = new JButton(new DownArrow());
        btnShow.addActionListener(this);
        btnShow.setActionCommand("SHOW");
        btnShow.setBorder(BorderFactory.createEmptyBorder());
        searchUpper = new JPanel();
        criteriaPanel = new JPanel();
        generalSearchPanell = new JPanel();
        periodPanel = new JPanel();
        buttonPanel = new JPanel();
        hidePanel = new JPanel();
        searchLowerPanel = new JPanel();
        exchangeLabel = new JLabel(Language.getString("EXCHANGE"));
        typeLabel = new JLabel(Language.getString("TYPE"));
        languageLabel = new JLabel(Language.getString("LANGUAGE_CAPTION"));
        keywordLabel = new JLabel(Language.getString("SYMBOL"));
        keywordField = new TWTextField();
        keywordField.addKeyListener(this);
        periodGroup.add(rbPreDef);
        periodGroup.add(rbCustom);
        rbPreDef.addFocusListener(this);
        rbPreDef.setSelected(true);
        rbCustom.addFocusListener(this);
        rbPreDef.addActionListener(this);
        rbCustom.addActionListener(this);
        preDefModel = new TWComboModel(predefArray);
        cmbLanguage = new TWComboBox();
        cmboExchanges = new TWComboBox(exchangesModel);
        cmbPredef = new TWComboBox(preDefModel);
        symbolField = new TWTextField();
        SearchedAnnouncementStore.getSharedInstance().setButtonReferences(btnNextL, btnPrevL, btnSearch);
        populateExchanges();
        populateDuration();
        announcementTabPanel.addTab(Language.getString("REALTIME_ANNOUNCEMENTS"), createRealTimePanel());
        announcementTabPanel.addTab(Language.getString("WINDOW_TITLE_SEARCH"), createSearchPanel());
        announcementTabPanel.selectTab(0);

        // search panel
        btnDownArraow = new TWButton("...");
        btnDownArraow.addActionListener(this);
        btnDownArraow.setActionCommand("ARROW");
        sectorCombo = new TWComboBox(sectorModel);
        mktCombo = new TWComboBox(mktModel);

        setTitle(Language.getString("ANNOUNCEMENT_SEARCH"));
        this.setSize(oSetting2.getSize());
        this.setLocation(oSetting2.getLocation());
//        this.setSize(width, height);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        this.add(mainPanel);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        Client.getInstance().getDesktop().add(this);
        Application.getInstance().addApplicationListener(this);
//        setLocationRelativeTo(Client.getInstance().getDesktop());
        setLayer(GUISettings.TOP_LAYER);
        GUISettings.applyOrientation(this);
    }

    public void selectRealtimeAnnoucement() {
        announcementTabPanel.setSelectedIndex(0);
    }

    public void selectAnnoucementSearch() {
        announcementTabPanel.setSelectedIndex(1);
    }

    public void searchForSymbol(String key, int interval, int intervalCount) {
        String searchString;

        String symbol = SharedMethods.getSymbolFromKey(key).toUpperCase();
        String exchange = SharedMethods.getExchangeFromKey(key);
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
        SimpleDateFormat dateFormatFull = new SimpleDateFormat("yyyyMMdd");

        Calendar calendar = Calendar.getInstance();
        String endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
        calendar.add(interval, -intervalCount);
        String startDate = dateFormatFull.format(calendar.getTime());


        announcementTabPanel.setSelectedIndex(1);

        for (TWComboItem item : exchangeArray) {
            if (item.getId().equals(exchange)) {
                cmboExchanges.setSelectedItem(item);
                break;
            }
        }
        typeCombo.setSelectedIndex(1);
//        removeItems();
//        criteriaPanel.add(createSymbolPanal());
//        setItems();
//        symbolField.setText(symbol);
//        searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
//                symbol + Meta.FD + startDate + Meta.FD + endDate;

        searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
                exchange + Meta.FD + symbol + Meta.FD + startDate + Meta.FD + endDate;

        SearchedAnnouncementStore.getSharedInstance().initSearch();
        SearchedAnnouncementStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), Integer.MAX_VALUE, exchange);
        SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(true);
        setBusyMode(true);
        SearchedAnnouncementStore.getSharedInstance().disableButtons();
        btnSearch.setText(Language.getString("CANCEL"));
        btnSearch.setActionCommand("CANCEL");

        AnnouncementSearchWindow.getSharedInstance().setLocationRelativeTo(Client.getInstance().getDesktop());
        AnnouncementSearchWindow.getSharedInstance().setVisible(true);
        keywordField.setText(symbol);
//           Client.getInstance().showAnnouncementSearchWIndow();
//           Client.getInstance().setAnnouncementSearchTitle(SharedMethods.getDisplayKey(exchange,symbol), Language.getString("SEARCHING"));

        searchString = null;
        calendar = null;
        startDate = null;
        endDate = null;
    }

    public void searchForSymbol(String key, long from, long end) {
        String searchString;

        String symbol = SharedMethods.getSymbolFromKey(key).toUpperCase();
        String exchange = SharedMethods.getExchangeFromKey(key);
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");


        announcementTabPanel.setSelectedIndex(1);

        for (TWComboItem item : exchangeArray) {
            if (item.getId().equals(exchange)) {
                cmboExchanges.setSelectedItem(item);
                break;
            }
        }
        typeCombo.setSelectedIndex(1);
//          removeItems();
//          criteriaPanel.add(createSymbolPanal());
//          setItems();
        symbolField.setText(symbol);
//        searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
//                symbol + Meta.FD + startDate + Meta.FD + endDate;

        searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
                exchange + Meta.FD + symbol + Meta.FD + dateFormat.format(new Date(from)) + Meta.FD + dateFormat.format(new Date(end));

        SearchedAnnouncementStore.getSharedInstance().initSearch();
        SearchedAnnouncementStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), Integer.MAX_VALUE, exchange);
        SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(true);
        SearchedAnnouncementStore.getSharedInstance().disableButtons();
        btnSearch.setText(Language.getString("CANCEL"));
        btnSearch.setActionCommand("CANCEL");

//          AnnouncementSearchWindow.getSharedInstance().setLocationRelativeTo(Client.getInstance().getDesktop());
        AnnouncementSearchWindow.getSharedInstance().setVisible(true);
//           Client.getInstance().showAnnouncementSearchWIndow();
//           Client.getInstance().setAnnouncementSearchTitle(SharedMethods.getDisplayKey(exchange,symbol), Language.getString("SEARCHING"));

        searchString = null;

    }


    private void populateLanguageCombo() {
        String[] saLanguages;
        String[] saLanguageIDs;

        saLanguages = Language.getLanguageCaptions();
        saLanguageIDs = Language.getLanguageIDs();

        for (int i = 0; i < saLanguages.length; i++) {
            TWComboItem item = new TWComboItem(saLanguageIDs[i], saLanguages[i]);
            cmbLanguage.addItem(item);
            if (item.getId().equals(Language.getSelectedLanguage())) {
                cmbLanguage.setSelectedItem(item);
            }
        }
    }


    private JPanel createRealTimePanel() {

        realTimePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%"}, 0, 0));
        realTimeUpperPanel = new JPanel();
        realTimeLowerPanel = new JPanel();
        realTimeUpperPanel.setLayout(new FlexGridLayout(new String[]{"70", "10", "120", "10", "70", "10", "120", "100%"}, new String[]{"100%"}, 5, 5, true, true));
        //adding componets to realTimeUpper
        realTimeUpperPanel.add(lblexchange);
        realTimeUpperPanel.add(new JLabel());
        realTimeUpperPanel.add(cmbExchange);
        realTimeUpperPanel.add(new JLabel());
        realTimeUpperPanel.add(new JLabel());
        realTimeUpperPanel.add(new JLabel());
        realTimeUpperPanel.add(new JLabel());
//        realTimeUpperPanel.add(cmbLanguage);
        realTimeUpperPanel.add(new JLabel());
        realTimeLowerPanel.setLayout(new FlexGridLayout(new String[]{"80", "100%", "80"}, new String[]{"100%"}, 5, 5, true, true));
        table1 = new Table();
        model1 = new AnnouncementModel();
        model1.setDirectionLTR(Language.isLTR());
//        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_ANNOUNCEMENT");
        model1.setViewSettings(oSetting1);
        table1.setModel(model1);
        this.setTable(table1);
//        this.setViewSetting(oSetting);
        //todo
        table1.setAutoResize(false);
        AnnouncementMouseListener oSelectionListener = new AnnouncementMouseListener(table1.getTable(),
                ANNOUNCMENT_TYPE.ALL);
        table1.getTable().getSelectionModel().addListSelectionListener(oSelectionListener);
        table1.getTable().addMouseListener(oSelectionListener);
        table1.getPopup().setAutoWidthAdjustMenuVisible(false);
        table1.getPopup().hidePrintMenu();
        table1.setPreferredSize(new Dimension(500, 100));

        model1.setDataStore(AnnouncementStore.getSharedInstance().getFilteredAnnouncements());
        table1.setWindowType(ViewSettingsManager.ANNOUNCEMENT_VIEW);
        model1.setTable(table1);
        model1.applyColumnSettings();
        table1.updateGUI();
        autoPopUpPanal.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 2));
        chkDoNotAutoPopup = new JCheckBox(Language.getString("DONOT_POPUP_ANNOUNCE_WINDOW"), !Settings.isPopupAnnounceWindow());
        chkDoNotAutoPopup.setActionCommand("DONOT_POPUP_ANNOUNCE_WINDOW");
        chkDoNotAutoPopup.addActionListener(this);
        Client.getInstance().getMenus().addAnnouncementAutoPopupModeListener(chkDoNotAutoPopup);
        autoPopUpPanal.add(chkDoNotAutoPopup);
//        autoPopUpPanal.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        table1.setSouthPanel(autoPopUpPanal);
        table1.getScrollPane().setBorder(BorderFactory.createEmptyBorder());

        mnuOutlook = new TWMenuItem(Language.getString("OUTLOOK_MENU"), "outlook.gif");
        mnuOutlook.addActionListener(this);
        mnuOutlook.setActionCommand("OUTLOOK");

        table1.getPopup().setMenuItem(mnuOutlook);
        ExchangeStore.getSharedInstance().addExchangeListener(this);

        //adding Componets to realTimeLower
        realTimeLowerPanel.add(btnPrev);
        realTimeLowerPanel.add(new JLabel());
        realTimeLowerPanel.add(btnNext);

        //adding components to realTimePanel
        realTimePanel.add(realTimeUpperPanel);
        realTimePanel.add(table1);
//        realTimePanel.add(realTimeLowerPanel);
//        cmbExchange.addActionListener(this);
        cmbLanguage.addActionListener(this);
        oSetting1.setTableNumber(1);
        super.applySettings();
        cmbExchange.setEnabled(true);
        cmbLanguage.setEnabled(true);
        GUISettings.applyOrientation(realTimeUpperPanel);
        GUISettings.applyOrientation(realTimeLowerPanel);
        GUISettings.applyOrientation(realTimePanel);
        return realTimePanel;

    }

    private JPanel createSearchPanel() {

        populateLanguageCombo();
        searchLowerPanel.setLayout(new FlexGridLayout(new String[]{"90", "100%", "90"}, new String[]{"100%"}, 5, 5, true, true));
        searchLowerPanel.add(btnPrevL);
        searchLowerPanel.add(new JLabel());
        searchLowerPanel.add(btnNextL);

        searchPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%", "30"}, 0, 0));
        searchUpper.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
//        searchUpper.add(createCriteriaPanel());
//        searchUpper.add(createPeriodPanel());
//        searchUpper.add(createButtonPanel());
        searchUpper.add(createGeneralSearchPanel());
//        searchUpper.add(new JLabel());
//        searchUpper.add(new JLabel());
//        searchUpper.add(new JLabel());
        //creating searched Announcement table
        table2 = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (!isLatest) {
                    if (SearchedAnnouncementStore.getSharedInstance().isSearchInProgress()) {
                        FontMetrics fontMetrics = table2.getFontMetrics(getFont());
                        g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                                (getHeight()) / 2);
                        g.drawImage(icon.getImage(),
                                (getWidth() - (fontMetrics.stringWidth(searching)) / 2) - (getWidth() / 2),
                                (getHeight() - icon.getImage().getHeight(this)) / 2 + 15, table2.getBackground(), this);
                        // + fontMetrics.stringWidth(searching) + 10
                        fontMetrics = null;
                    }
                }
            }
        };
        Table searchTbl[] = new Table[1];
        searchTbl[0] = table2;
        this.setTable2(searchTbl);
        model2 = new AnnouncementModel();
        model2.setDirectionLTR(Language.isLTR());
        table2.setWindowType(ViewSettingsManager.ANNOUNCEMENT_VIEW);
        model2.setViewSettings(oSetting2);
        table2.setModel(model2);
        table2.setAutoResize(false);
        selectionListener = new AnnouncementMouseListener(table2.getTable(), ANNOUNCMENT_TYPE.SEARCHED);
        table2.getTable().getSelectionModel().addListSelectionListener(selectionListener);
        table2.getTable().addMouseListener(selectionListener);
        table2.getPopup().setAutoWidthAdjustMenuVisible(false);
        table2.getPopup().hidePrintMenu();
        table2.setPreferredSize(new Dimension(600, 100));
        model2.setDataStore(SearchedAnnouncementStore.getSharedInstance().getStore());
        model2.setTable(table2);
        model2.applyColumnSettings();
        table2.updateGUI();
        searchPanel.add(searchUpper);
        searchPanel.add(table2);
        searchPanel.add(searchLowerPanel);
        oSetting2.setTableNumber(2);
        //super.applySettings();
        GUISettings.applyOrientation(searchUpper);
        GUISettings.applyOrientation(searchLowerPanel);
        GUISettings.applyOrientation(searchPanel);
        return searchPanel;
    }

    private JPanel createGeneralSearchPanel() {
        String[] width = {"10", "80", "5", "150", "5", "80", "5", "130", "100%", "100", "5", "130", "10"};
        String[] height = {"100%"};
        generalSearchPanell.setLayout(new FlexGridLayout(width, height, 0, 3));
//        criteriaPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CRITERIA")));
//        criteriaPanel.doLayout();
        generalSearchPanell.add(new JLabel());
        generalSearchPanell.add(exchangeLabel);
        generalSearchPanell.add(new JLabel());
        generalSearchPanell.add(cmboExchanges);
        generalSearchPanell.add(new JLabel());
        generalSearchPanell.add(keywordLabel);
        generalSearchPanell.add(new JLabel());
        generalSearchPanell.add(keywordField);
        generalSearchPanell.add(new JLabel());
        generalSearchPanell.add(btnSearch);
        generalSearchPanell.add(new JLabel());
        generalSearchPanell.add(btnAdvSearch);
        generalSearchPanell.add(new JLabel());
//        criteriaPanel.add(typeLabel);
        typeArray.add(new TWComboItem("*", Language.getString("LBL_ALL")));
        typeArray.add(new TWComboItem("sy", Language.getString("SYMBOL")));
        typeArray.add(new TWComboItem("se", Language.getString("SECTOR")));
        typeArray.add(new TWComboItem("mk", Language.getString("MARKET")));
        typeModel = new TWComboModel(typeArray);
        typeCombo = new TWComboBox(typeModel);
        typeCombo.setSelectedIndex(0);
//        criteriaPanel.add(createAllPanal());
        typeCombo.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SY")) {
                    removeItems();
//                    criteriaPanel.add(createSymbolPanal());
                    setItems();
                } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SE")) {
                    removeItems();
//                    criteriaPanel.add(createSectorPanal());
                    setItems();

                } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("MK")) {
                    removeItems();
//                    criteriaPanel.add(createMktPanal());
                    setItems();
                } else {
                    removeItems();
//                    criteriaPanel.add(createAllPanal());
                    setItems();
                }
            }
        });

//        criteriaPanel.add(languageLabel);
//        criteriaPanel.add(cmbLanguage);
        cmboExchanges.addActionListener(this);
        GUISettings.applyOrientation(generalSearchPanell);
        return generalSearchPanell;
    }

    private JPanel createCriteriaPanel() {
        String[] width = {"40%", "60%"};
        String[] height = {"20", "20", "20", "20"};
        criteriaPanel.setLayout(new FlexGridLayout(width, height, 5, 5));
        criteriaPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CRITERIA")));
        criteriaPanel.doLayout();
        criteriaPanel.add(exchangeLabel);
        criteriaPanel.add(cmboExchanges);
        criteriaPanel.add(typeLabel);
        typeArray.add(new TWComboItem("*", Language.getString("LBL_ALL")));
        typeArray.add(new TWComboItem("sy", Language.getString("SYMBOL")));
        typeArray.add(new TWComboItem("se", Language.getString("SECTOR")));
        typeArray.add(new TWComboItem("mk", Language.getString("MARKET")));
        typeModel = new TWComboModel(typeArray);
        typeCombo = new TWComboBox(typeModel);
        typeCombo.setSelectedIndex(0);
        criteriaPanel.add(createAllPanal());
        typeCombo.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SY")) {
                    removeItems();
                    criteriaPanel.add(createSymbolPanal());
                    setItems();
                } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SE")) {
                    removeItems();
                    criteriaPanel.add(createSectorPanal());
                    setItems();

                } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("MK")) {
                    removeItems();
                    criteriaPanel.add(createMktPanal());
                    setItems();
                } else {
                    removeItems();
                    criteriaPanel.add(createAllPanal());
                    setItems();
                }
            }
        });
        criteriaPanel.add(keywordLabel);
        criteriaPanel.add(keywordField);
        criteriaPanel.add(languageLabel);
        criteriaPanel.add(cmbLanguage);
        cmboExchanges.addActionListener(this);
        GUISettings.applyOrientation(criteriaPanel);
        return criteriaPanel;
    }

    private void removeItems() {
        try {
//            criteriaPanel.remove(3);
//            criteriaPanel.remove(4);
//            criteriaPanel.remove(5);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void setItems() {
//        criteriaPanel.add(keywordLabel);
//        criteriaPanel.add(keywordField);
//        criteriaPanel.add(languageLabel);
//        criteriaPanel.add(cmbLanguage);
//        GUISettings.applyOrientation(criteriaPanel);
//        GUISettings.reloadGUIForNumberChange();
    }

    private JPanel createSymbolPanal() {

        String[] width = {"40%", "60%"};
        String[] height = {"20"};
        JPanel symbolPanel = new JPanel(new FlexGridLayout(width, height, 0, 0));
        symbolPanel.setLayout(new FlexGridLayout(width, height, 0, 0));
        symbolPanel.add(typeCombo);
        symbolPanel.add(createSymbolPanel());
        return symbolPanel;
    }

    private JPanel createSectorPanal() {

        String[] width = {"40%", "60%"};
        String[] height = {"20"};
        JPanel pan = new JPanel(new FlexGridLayout(width, height, 0, 0));
        populateSectors(((TWComboItem) cmboExchanges.getSelectedItem()).getId());
        pan.setLayout(new FlexGridLayout(width, height, 0, 0));
        pan.add(typeCombo);
        if (sectorCombo.getItemCount() < 2) {
            pan.add(getEmptyTextField());
        } else {
            sectorCombo.setSelectedIndex(0);
            pan.add(sectorCombo);
        }
        return pan;
    }

    private JPanel createAllPanal() {
        String[] width = {"40%", "60%"};
        String[] height = {"20"};
        JPanel pan = new JPanel(new FlexGridLayout(width, height, 0, 0));
        pan.setLayout(new FlexGridLayout(width, height, 0, 0));
        pan.add(typeCombo);
        pan.add(getEmptyTextField());
        return pan;
    }

    private JTextField getEmptyTextField() {
        JTextField temp = new JTextField();
        temp.setEnabled(false);
        temp.setEditable(false);
        return temp;
    }

    private JPanel createMktPanal() {

        String[] width = {"40%", "60%"};
        String[] height = {"20"};
        JPanel pan = new JPanel(new FlexGridLayout(width, height, 0, 0));
        populateMkt(cmboExchanges.getSelectedItem().toString());
        pan.setLayout(new FlexGridLayout(width, height, 0, 0));
        pan.add(typeCombo);
        if (mktCombo.getItemCount() < 2) {
            pan.add(getEmptyTextField());
        } else {
            mktCombo.setSelectedIndex(0);
            pan.add(mktCombo);
        }
        return pan;
    }

    private ArrayList populateMkt(String exchangeName) {
        mktArray.clear();
        try {
            Enumeration sectorlist = ExchangeStore.getSharedInstance().getExchanges();
            while (sectorlist.hasMoreElements()) {
                Exchange exchange = (Exchange) sectorlist.nextElement();
                if (exchange.getDescription().equalsIgnoreCase(exchangeName)) {
                    if (exchange.isUserSubMarketBreakdown() && exchange.hasSubMarkets()) {
                        Market[] submkt = exchange.getSubMarkets();
                        for (int i = 0; i < exchange.getSubMarketCount(); i++) {
                            mktArray.add(new TWComboItem(submkt[i].getMarketID(), submkt[i].getDescription()));
                        }
                    }
                }
                exchange = null;
            }

            Collections.sort(mktArray);
            mktArray.add(0, new TWComboItem("*", Language.getString("ALL")));

        } catch (Exception err) {
        }

        return mktArray;
    }

    private JPanel createPeriodPanel() {
        String[] dateWidths = {"20%", "80%"};
        String[] dateHeights = {"20"};
        fromDateCombo = new DateCombo(Client.getInstance().getFrame());
        fromDateCombo.addFocusListener(this);
        JPanel fromPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 5));
        fromPanel.add(new JLabel(Language.getString("FROM")));
        fromPanel.add(fromDateCombo);
        fromPanel.doLayout();
        toDateCombo = new DateCombo(Client.getInstance().getFrame());
        toDateCombo.addFocusListener(this);
        JPanel toPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 5));
        toPanel.add(new JLabel(Language.getString("TO")));
        toPanel.add(toDateCombo);
        toPanel.doLayout();

        String[] widthdPanel = {"40%", "60%"};
        String[] heightdPanel = {"20", "25", "25"};
        periodPanel.setLayout(new FlexGridLayout(widthdPanel, heightdPanel, 5, 5));
        periodPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("PERIOD")));
        periodPanel.add(rbPreDef);
        periodPanel.add(cmbPredef);
        periodPanel.add(rbCustom);
        periodPanel.add(fromPanel);
        periodPanel.add(new JLabel());
        periodPanel.add(toPanel);
        GUISettings.applyOrientation(periodPanel);
        return periodPanel;
    }

    private JPanel createButtonPanel() {
        //create busy label
        busyLabel = new JLabel();
        busyLabel.setOpaque(true);
        buttonPanel.setLayout(new FlexGridLayout(new String[]{"20%", "60%", "20%"}, new String[]{"1", "25", "25", "100%"}, 0, 5));
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        buttonPanel.add(btnSearch);
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        buttonPanel.add(btnHide);
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        buttonPanel.add(new JLabel());
        GUISettings.applyOrientation(buttonPanel);
        return buttonPanel;
    }

    private JPanel createShowHidepanel() {
        hidePanel.setLayout(new FlexGridLayout(new String[]{"0", "0"}, new String[]{"100%"}, 0, 0));
        hidePanel.setPreferredSize(new Dimension(775, 20));
        btnShow.setPreferredSize(new Dimension(20, 20));
        JLabel lbDummy = new JLabel();
        lbDummy.setPreferredSize(new Dimension(755, 20));
        hidePanel.add(lbDummy);
        hidePanel.add(btnShow);
        GUISettings.applyOrientation(hidePanel);
        return hidePanel;
    }

    private void hidePanels() {
        searchPanel.removeAll();
        searchPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "100%", "25"}, 0, 0));
        searchPanel.add(createShowHidepanel());
        searchPanel.add(table2);
        searchPanel.add(searchLowerPanel);
        searchPanel.revalidate();
        searchPanel.updateUI();
    }

    private void showPanels() {
        searchPanel.removeAll();
        searchPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"130", "100%", "25"}, 0, 0));
        searchUpper.setLayout(new FlexGridLayout(new String[]{"325", "325", "125", "100%"}, new String[]{"100%"}, 0, 0));
        searchPanel.add(searchUpper);
        searchPanel.add(table2);
        searchPanel.add(searchLowerPanel);
        searchPanel.revalidate();
        searchPanel.updateUI();

    }

    private JPanel createSymbolPanel() {
        String[] widths = {"100%", "20"};
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights);

        JPanel panel = new JPanel(layout);
        symbolField = new TWTextField();
        symbolField.addFocusListener(this);
        panel.add(symbolField);
        panel.add(btnDownArraow);
        GUISettings.applyOrientation(panel);
        return panel;
    }

    public void populateExchanges() {
        exchangesArray.clear();
        exchangeArray.clear();
        SharedMethods.populateExchangesForAnnouncementsCombo(exchangesArray, false);
        SharedMethods.populateExchangesForAnnouncementsCombo(exchangeArray, true);
//        Enumeration exchangeObjects = ExchangeStore.getSharedInstance().getExchanges();
//        TWComboItem t1;
//        TWComboItem t2;
//        while (exchangeObjects.hasMoreElements()) {
//            Exchange exchanges = (Exchange) exchangeObjects.nextElement();
//            if (!exchanges.isExpired() && !exchanges.isInactive()) {
//                t1=new TWComboItem(exchanges.getSymbol(), exchanges.getDescription());
//                t2=new TWComboItem(exchanges.getSymbol(), exchanges.getDescription());
//                exchangesArray.add(t1);
//                exchangeArray.add(t2);
//            }
//            exchanges = null;
//        }
//        exchangeObjects = null;
//        Collections.sort(exchangesArray);
//        Collections.sort(exchangeArray);
        if (exchangeArray.size() > 1) {
            exchangeArray.add(0, new TWComboItem("*", Language.getString("ALL")));
        }
        try {
            cmbExchange.removeActionListener(this);
            cmbExchange.setSelectedIndex(0);
            cmbExchange.addActionListener(this);
            cmboExchanges.setSelectedIndex(0);
        } catch (Exception e) {
        }
        GUISettings.applyOrientation(cmbExchange);
        GUISettings.applyOrientation(cmboExchanges);
    }

    private ArrayList populateSectors(String exchange) {
        sectorArray.clear();
        int count = 1;
        if ((exchange == null) || (exchange.equalsIgnoreCase(ALL_EXCHANGE)) || (exchange.equalsIgnoreCase(NULL_EXCHANGE))) {
            sectorArray.add(new TWComboItem(NULL_EXCHANGE, ""));
        } else {
            try {
                Enumeration<Sector> sectorlist = SectorStore.getSharedInstance().getSectors(exchange);
                while (sectorlist.hasMoreElements()) {
                    Sector sector = sectorlist.nextElement();
                    sectorArray.add(new TWComboItem(sector.getId(), sector.getDescription()));
                }
                if (sectorArray.size() == 0) { // no sectors for the exchange
                    sectorArray.add(0, new TWComboItem("*", Language.getString("LBL_ALL")));
                    return sectorArray;
                }
                Collections.sort(sectorArray);
                sectorArray.add(0, new TWComboItem("*", Language.getString("LBL_ALL")));

            } catch (Exception err) {
                sectorArray.add(new TWComboItem(NULL_EXCHANGE, ""));
            }
        }
        return sectorArray;
    }


    private void populateDuration() {
        predefArray.clear();
        predefArray.add(new TWComboItem("*", Language.getString("LBL_ALL")));
        predefArray.add(new TWComboItem(1, Language.getString("1_MONTH")));
        predefArray.add(new TWComboItem(2, Language.getString("3_MONTH")));
        predefArray.add(new TWComboItem(3, Language.getString("6_MONTH")));
        predefArray.add(new TWComboItem(4, Language.getString("1_YEAR")));
        predefArray.add(new TWComboItem(5, Language.getString("2_YEAR")));
        predefArray.add(new TWComboItem(6, Language.getString("3_YEAR")));
        predefArray.add(new TWComboItem(7, Language.getString("5_YEAR")));
        predefArray.add(new TWComboItem(8, Language.getString("10_YEAR")));

        try {
            cmbPredef.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDefaultValues() {
        cmbExchange.setSelectedIndex(0);
        TWComboItem item1 = (TWComboItem) cmbExchange.getSelectedItem();
        AnnouncementStore.getSharedInstance().setFilter(item1.getValue().trim());

    }

    public void setDataStore(boolean isLatestAnnouncement) {
        if (isLatestAnnouncement) {
            model1.setDataStore(AnnouncementStore.getSharedInstance().getFilteredAnnouncements());       //getFilteredNewss()
            table1.repaint();
        } else {
            model2.setDataStore(AnnouncementStore.getSharedInstance().getFilteredAnnouncements());
            table2.repaint();
        }
    }

    public void setSearchedDataStore() {
        model2.setDataStore(SearchedAnnouncementStore.getSharedInstance().getStore());
    }

    public void setSelectedExchange(String exchange) {
        for (TWComboItem item : exchangesArray) {
            if (exchange.equals(item.getId())) {
                cmboExchanges.setSelectedItem(item);
                return;
            }
        }
    }

    public void setLatest(boolean isLatest) {
        this.isLatest = isLatest;
    }

    private void searchSymbol() {
        try {
            Symbols symbols = new Symbols();
//            symbols.appendSymbol(SharedMethods.getKey(((TWComboItem) cmboExchanges.getSelectedItem()).getId(), symbolField.getText()));
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
            oCompanies.setSelectedExchange(((TWComboItem) cmboExchanges.getSelectedItem()).getId());
            oCompanies.setTitle(getTitle());
            oCompanies.setSingleMode(true);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true);
            oCompanies = null;

            String key = symbols.getSymbols()[0];
            String exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);

            symbolField.setText(symbol);

            for (TWComboItem item : exchangesArray) {
                if (item.getId().equalsIgnoreCase(exchange)) {
                    isSymbolSearch = true;
                    cmboExchanges.setSelectedItem(item);
                    break;
                }
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void search() {

        String symbol;
        String exchange;
        String startDate;
        String endDate;
        String symbolType;
        String subMkt;
        String language = ((TWComboItem) cmbLanguage.getSelectedItem()).getId();


        SimpleDateFormat dateFormatFull = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dateFormatyyyy = new SimpleDateFormat("yyyy");

        Calendar calendar = Calendar.getInstance();

        if (isValidInput()) {
            if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SY")) {
                symbol = symbolField.getText().toUpperCase();
                symbolType = "" + Meta.TYPE_SYMBOL;
                exchange = ((TWComboItem) cmboExchanges.getSelectedItem()).getId();
            } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("MK")) {
                symbol = ((TWComboItem) mktCombo.getSelectedItem()).getId();
                symbolType = "" + Meta.TYPE_MKT;
                exchange = ((TWComboItem) cmboExchanges.getSelectedItem()).getId();
            } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("*")) {
                symbol = "*";
                symbolType = "" + Meta.TYPE_SYMBOL;
                exchange = ((TWComboItem) cmboExchanges.getSelectedItem()).getId();
            } else {
                symbol = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
                symbolType = "" + Meta.TYPE_SECTOR;
                exchange = ((TWComboItem) cmboExchanges.getSelectedItem()).getId();
            }

            if (rbPreDef.isSelected()) {
                int index = cmbPredef.getSelectedIndex();
                int field = Integer.parseInt(periods[index][1]);
                if (field == -1) {// ytd type
                    endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
                    startDate = dateFormatyyyy.format(new Date(System.currentTimeMillis())) + "0101";
                } else {
                    endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
                    calendar.add(Integer.parseInt(periods[index][1]), -Integer.parseInt(periods[index][2]));
                    startDate = dateFormatFull.format(calendar.getTime());
                }
            } else {
                endDate = fromDateCombo.getDateString();
                startDate = toDateCombo.getDateString();
            }

            long lStart = Long.parseLong(startDate);
            long lEnd = Long.parseLong(endDate);
            endDate = "" + Math.max(lStart, lEnd);
            startDate = "" + Math.min(lStart, lEnd);
            symbol = keywordField.getText().toUpperCase();
            if ((symbol == null) || (symbol.equals(""))) {
                symbol = "*";
            }
            String searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + symbolType + Meta.FD +
                    exchange + Meta.FD + symbol + Meta.FD + startDate + Meta.FD + endDate;

            SearchedAnnouncementStore.getSharedInstance().initSearch();
            SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(true);
            SearchedAnnouncementStore.getSharedInstance().doSearch(searchString, language, Integer.MAX_VALUE, exchange);

            AnnouncementSearchWindow.getSharedInstance().disableButtons();
            setDefaultValues();
            announcementTabPanel.selectTab(1);
            setLatest(false);
            Theme.unRegisterComponent(this);

            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Announcements, "Search");

            symbolType = null;
            symbol = null;
            dateFormatFull = null;
            dateFormatyyyy = null;
            calendar = null;
            startDate = null;
            endDate = null;

            updateSearchedTable();

            setVisible(true);
        } else {
//            SharedMethods.showMessage(Language.getString("MSG_FILL_DATE_ENTRY"), JOptionPane.ERROR_MESSAGE);
        }
    }

    public void setSearchDates(long from, long end) {
        fromDate = from;
        toDate = end;

    }

    public void disableButtons() {
        try {
            btnNext.setEnabled(false);
            btnPrev.setEnabled(false);
            btnNextL.setEnabled(false);
            btnPrevL.setEnabled(false);
//            btnReload.setEnabled(false);
//            rbArabic.setEnabled(false);
//            rbEnglish.setEnabled(false);
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void enableButtons() {
        btnPrev.setEnabled(true);
        btnPrevL.setEnabled(true);
    }


    public boolean isValidInput() {
        boolean returnValue = false;
        if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("sy")) {
            if (DataStore.getSharedInstance().isValidCompany(((TWComboItem) cmboExchanges.getSelectedItem()).getId(), keywordField.getText().toUpperCase())) {     //symbolField.getText().toUpperCase())
                returnValue = true;
            } else {
                setBusyMode(false);
                new ShowMessage(Language.getString("INVALID_SYMBOL"), "E");
                returnValue = false;

                return returnValue;
            }
        } else {
            returnValue = true;
        }
        return returnValue;
    }

    public void updateSearchedTable() {
        table2.updateUI();
        table2.updateGUI();
        table1.getModel().getViewSettings().setParent(this);
        table1.updateGUI();
    }

    public void clearSymbolField() {
        keywordField.setText("");

    }

    public void setBusyMode(boolean isSearching) {
        if (isSearching) {
//            busyLabel.setHorizontalAlignment(JLabel.CENTER);
//            busyLabel.setIcon(iconBusy);
        } else {
//            busyLabel.setIcon(null);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("HIDE") && e.getSource() instanceof TWMenuItem) {
            super.actionPerformed(e);
        } else if (e.getActionCommand().equals("ADVSEARCH")) {
            showAdvanceSearchFrame();

        } else if (e.getActionCommand().equals("SHOW")) {
            e.getClass().desiredAssertionStatus();
            showPanels();
        } else if (e.getActionCommand().equals("OK")) {
            if (Settings.isConnected()) {
                search();
                updateSearchedTable();
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getSource() == cmboExchanges) {
            if (!isSymbolSearch) {
                removeItems();
//                criteriaPanel.add(createAllPanal());
//                setItems();
//                typeCombo.setSelectedIndex(0);
//                criteriaPanel.updateUI();
            }
            isSymbolSearch = false;
        } else if (e.getActionCommand().equals("CANCEL")) {
            SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(false);
            SearchedAnnouncementStore.getSharedInstance().initSearch();
            setBusyMode(false);
            btnSearch.setText(Language.getString("WINDOW_TITLE_SEARCH"));
            btnSearch.setActionCommand("SEARCH");
            btnAdvSearch.setEnabled(true);
            SearchedAnnouncementStore.getSharedInstance().enableButtons();

        } else if (e.getActionCommand().equals("RELOAD")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            SearchedAnnouncementStore.getSharedInstance().doReSearch();
        } else if (e.getActionCommand().equals("PREVIOUS")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            SearchedAnnouncementStore.getSharedInstance().doPreviuosSearch();
        } else if (e.getActionCommand().equals("SEARCH")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            if (isConnected()) {
                setBusyMode(true);
                setSearchedDataStore();
                search();
                updateSearchedTable();
                btnSearch.setText(Language.getString("CANCEL"));
                btnSearch.setActionCommand("CANCEL");
                btnAdvSearch.setEnabled(false);

            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getActionCommand().equals("NEXT")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            SearchedAnnouncementStore.getSharedInstance().doNextSearch();
        } else if (e.getActionCommand().equals("EXCHANGES")) {
            populateSectors(((TWComboItem) cmboExchanges.getSelectedItem()).getId());
        } else if (e.getActionCommand().equals("ARROW")) {
            searchSymbol();
        } else if (e.getSource().equals(cmbExchange)) {
            try {
                if (cmbExchange.getSelectedItem().toString().equalsIgnoreCase(Language.getString("ALL"))) {
                    AnnouncementStore.getSharedInstance().setFilter(null);
                    AnnouncementStore.getSharedInstance().applyFilter();
                } else {
                    TWComboItem item1 = (TWComboItem) cmbExchange.getSelectedItem();
                    AnnouncementStore.getSharedInstance().setFilter(item1.getId());
                    AnnouncementStore.getSharedInstance().applyFilter();
                }
                String id = ((TWComboItem) cmbExchange.getSelectedItem()).getId();
                oSetting1.putProperty(ViewConstants.ANN_SELECTED_EXG, id);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        } else if (e.getSource().equals(btnNextL)) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
            SearchedAnnouncementStore.getSharedInstance().doNextSearch();
            this.updateUI();
            btnNextL.setEnabled(SearchedAnnouncementStore.getSharedInstance().enableNext());
            btnPrevL.setEnabled(SearchedAnnouncementStore.getSharedInstance().enablePrev());
            SearchedAnnouncementStore.getSharedInstance().enableButtons();
            btnSearch.setText(Language.getString("CANCEL"));
            btnSearch.setActionCommand("CANCEL");
            btnAdvSearch.setEnabled(false);
        } else if (e.getSource().equals(btnPrevL)) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
            SearchedAnnouncementStore.getSharedInstance().doPreviuosSearch();
            this.updateUI();
            btnNextL.setEnabled(SearchedAnnouncementStore.getSharedInstance().enableNext());
            btnPrevL.setEnabled(SearchedAnnouncementStore.getSharedInstance().enablePrev());
            SearchedAnnouncementStore.getSharedInstance().enableButtons();
            btnSearch.setText(Language.getString("CANCEL"));
            btnSearch.setActionCommand("CANCEL");
            btnAdvSearch.setEnabled(false);
        } else if (e.getActionCommand().equals("OUTLOOK")) {
            if (Reminder.checkAvailability()) {
                ReminderUI rUI = new ReminderUI(Client.getInstance().getFrame());
                int row = table1.getTable().getSelectedRow();
//				row = ((TableSorter) table1.getTable().getModel()).getUnsortedRowFor(row);
                String title = (String) table1.getTable().getModel().getValueAt(row, 3);
                String body = (String) table1.getTable().getModel().getValueAt(row, 2);
                rUI.showDiaog(title, Language.getString("LOCATION_UNKNOWN"), body);
            } else {
                new ShowMessage(Language.getString("MSG_NO_OUTLOOK_SUPPORT"), "E");
            }
        }
//        else if (e.getSource().equals(cmbLanguage)) {
//            try {
////                TWComboItem item1 = (TWComboItem) cmbExchange.getSelectedItem();
//                TWComboItem item2 = (TWComboItem) cmbLanguage.getSelectedItem();
//                AnnouncementStore.getSharedInstance().setFilter(item2.getId());
//                AnnouncementStore.getSharedInstance().applyFilter();
//            } catch (Exception e1) {
//                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
//
//        }
        else {
            setSelectedLanguage(e.getActionCommand());
        }
        if (e.getActionCommand().equals("DONOT_POPUP_ANNOUNCE_WINDOW")) {
            Client.getInstance().getMenus().setAnnouncementAutoPopupMode(!chkDoNotAutoPopup.isSelected());
            Settings.setPopupAnnounceWindow(!chkDoNotAutoPopup.isSelected());
        }
//        if (e.getActionCommand().equals("HIDE")) {
//            hidePanels();
//
//        } else if (e.getActionCommand().equals("SHOW")) {
//            showPanels();
//        } else if (e.getSource() == btnNext) {
//            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
////  SearchedNewsStore.getSharedInstance().doNextSearch();
//            this.updateUI();
//            btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
//            SearchedNewsStore.getSharedInstance().enableButtons();
//        } else if (e.getSource() == btnPrev) {
//            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
////  SearchedNewsStore.getSharedInstance().doPreviuosSearch();
//            this.updateUI();
//            btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
//            SearchedNewsStore.getSharedInstance().enableButtons();
//        } else if (e.getActionCommand().equals("ARROW")) {
//            searchSymbol();
//        } else if (e.getActionCommand().equals("SEARCH")) {
//            if (isConnected()) {
//                NewsWindow.getSharedInstance().setSearchedDataStore();
//                search();
//            } else {
//                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
//            }
//        } else if (e.getSource().equals(btnNextL)) {
//            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
////  SearchedNewsStore.getSharedInstance().doNextSearch();
//            this.updateUI();
//            btnNextL.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrevL.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
//            SearchedNewsStore.getSharedInstance().enableButtons();
//        } else if (e.getSource().equals(btnPrevL)) {
//            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
//// SearchedNewsStore.getSharedInstance().doPreviuosSearch();
//            this.updateUI();
//            btnNextL.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrevL.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
//            SearchedNewsStore.getSharedInstance().enableButtons();
//        } else if (e.getSource() == cmboExchanges) {
//            int index = cmboExchanges.getSelectedIndex();
//            if (index == 0) {
//                typeCombo.setEnabled(false);
//                mktCombo.setEnabled(false);
//                sectorCombo.setEnabled(false);
//                btnDownArraow.setEnabled(false);
//            } else {
//                typeCombo.setEnabled(true);
//                mktCombo.setEnabled(true);
//                sectorCombo.setEnabled(true);
//                btnDownArraow.setEnabled(true);
//                removeItems();
//                criteriaPanel.add(createAllPanal());
//                setItems();
//                typeCombo.setSelectedIndex(0);
//            }
//        } else if (e.getSource() == rbPreDef) {
//            if (rbPreDef.isSelected()) {
//                rbPreDef.setSelected(true);
//                rbCustom.setSelected(false);
//                fromDateCombo.clearDate();
//                toDateCombo.clearDate();
//            } else {
//                rbCustom.setSelected(false);
//                rbPreDef.setSelected(true);
//            }
//        } else if (e.getSource() == rbCustom) {
//            if (rbCustom.isSelected()) {
//                rbCustom.setSelected(true);
//                rbPreDef.setSelected(false);
//                cmbPredef.setSelectedIndex(0);
//            } else {
//                rbPreDef.setSelected(false);
//                rbCustom.setSelected(true);
//                cmbPredef.setSelectedIndex(0);
//
//            }
//        } else if (e.getSource().equals(cmbExchange)) {
//            try {
//                TWComboItem item1 = (TWComboItem) cmbExchange.getSelectedItem();
//                TWComboItem item2 = (TWComboItem) cmbLanguage.getSelectedItem();
//                NewsStore.getSharedInstance().setFilter(item1.getId(), item2.getId());
//                NewsStore.getSharedInstance().applyNewsFilter();
//
//            } catch (Exception e1) {
//                e1.printStackTrace();
//            }
//
//        } else if (e.getSource().equals(cmbLanguage)) {
//            try {
//                TWComboItem item1 = (TWComboItem) cmbExchange.getSelectedItem();
//                TWComboItem item2 = (TWComboItem) cmbLanguage.getSelectedItem();
//                NewsStore.getSharedInstance().setFilter(item1.getId(), item2.getId());
//                NewsStore.getSharedInstance().applyNewsFilter();
//            } catch (Exception e1) {
//                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
//        }

    }

    public void setSelectedLanguage(String selectedLanguage) {
        SearchedAnnouncementStore.getSharedInstance().setSelectedLanguage(selectedLanguage);
    }

    public boolean isConnected() {
        return isconnected;
    }

    public void exchangeAdded(Exchange exchange) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        populateExchanges();
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void subMarketStatsChanged(int oldStatus, Exchange exchange, Market subMarket) {
    }

    public void subMarketMustInitialize(Exchange exchange, Market market) {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusGained(FocusEvent e) {
        if (e.getSource() == fromDateCombo) {
            rbCustom.setSelected(true);
            rbPreDef.setSelected(false);
        } else if (e.getSource() == toDateCombo) {
            rbCustom.setSelected(true);
            rbPreDef.setSelected(false);
        } else if (e.getSource() == rbPreDef) {
        } else if (e.getSource() == rbCustom) {
        }
    }

    public void focusLost(FocusEvent e) {
    }

    public void twConnected() {
        isconnected = true;
    }

    public void twDisconnected() {
        isconnected = false;
        SearchedNewsStore.getSharedInstance().setSearchInProgress(false);
        btnSearch.setText(Language.getString("WINDOW_TITLE_SEARCH"));
        btnSearch.setActionCommand("SEARCH");
        btnAdvSearch.setEnabled(true);
        Client.getInstance().setNewsSearchTitle(null);
        Client.getInstance().getDesktop().repaint();
    }

    public void enableNextButtons() {
        btnNext.setEnabled(true);
        btnNextL.setEnabled(true);
    }

    public void disaableNextButtons() {
        btnNext.setEnabled(false);
        btnNextL.setEnabled(false);
    }

    public void enablePrevButtons() {
        btnNext.setEnabled(true);
        btnNextL.setEnabled(true);
    }

    public void resetSearchBtn() {
        btnSearch.setText(Language.getString("WINDOW_TITLE_SEARCH"));
        btnSearch.setActionCommand("SEARCH");
        btnAdvSearch.setEnabled(true);
    }

    public void setCancelActions() {
        btnSearch.setText(Language.getString("CANCEL"));
        btnSearch.setActionCommand("CANCEL");
        btnAdvSearch.setEnabled(false);
    }


    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private void showAdvanceSearchFrame() {
        advanceframe = AdvanceAnnouncementWindow.getSharedInstance();
        advanceframe.setExchange(cmboExchanges.getSelectedIndex());
        advanceframe.clearFields();
        advanceframe.setLocationRelativeTo(Client.getInstance().getDesktop());
        GUISettings.applyOrientation(advanceframe);
        advanceframe.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        try {
            advanceframe.setSelected(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        advanceframe.setVisible(true);
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        AdvanceAnnouncementWindow.getSharedInstance().setVisible(false);
    }

    public TWTabbedPane getAnnouncementTabPanel() {
        return announcementTabPanel;
    }

    public void applicationReadyForTransactions() {
        try {
            String selex = (String) oSetting1.getProperty(ViewConstants.ANN_SELECTED_EXG);
            if (selex != null && !selex.isEmpty()) {
                for (int i = 0; i < exchangeArray.size(); i++) {
                    TWComboItem item = (TWComboItem) exchangeArray.get(i);
                    if (item.getId().equals(selex)) {
                        cmbExchange.setSelectedIndex(i);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void keyTyped(KeyEvent e) {      //DEF1-Saudi
        if (e.getSource().equals(keywordField)) {
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
                if (isConnected()) {
                    setBusyMode(true);
                    setSearchedDataStore();
                    search();
                    updateSearchedTable();
                    btnSearch.setText(Language.getString("CANCEL"));
                    btnSearch.setActionCommand("CANCEL");
                    btnAdvSearch.setEnabled(false);

                } else {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                }
            }
        }

        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
