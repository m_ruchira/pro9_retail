package com.isi.csvr.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Aug 19, 2008
 * Time: 2:13:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class AdvaneSellTableModel extends CommonTable implements TableModel {


    private boolean textFieldeeditmode = false;


    private boolean editMode = false;

    private TransactRecord record;
    private ArrayList dataStore;
    private int availableHolding;
    private int selltemp;


    private long editingTrId;

    public AdvaneSellTableModel() {

        availableHolding = 0;
        selltemp = 0;

    }

    public void setDataStore(ArrayList dataStore) {
        this.dataStore = dataStore;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public void setEditingTrId(long editingTrId) {
        this.editingTrId = editingTrId;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];

    }

    public String[] getColumnNames() {
        return super.getViewSettings().getColumnHeadings();
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:

                return Boolean.class;
            case 1:
            case 2:
            case 3:
            case 5:
                return String.class;
            case 4:
                return Object.class;
            default:
                return Object.class;
        }

//        try {
//            return getValueAt(0, columnIndex).getClass();
//        } catch (Exception ex) {
//            return Object.class;
//        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 3) {
            //    textFieldeeditmode = true;
            return true;
        } else {
            return false;
        }
    }

    public int getRowCount() {
        if (dataStore != null) {
            return dataStore.size();
        } else {
            return 0;
        }
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            record = (TransactRecord) dataStore.get(rowIndex);
            Stock stock = PortfolioInterface.getStockObject(record.getSKey());
            if (record.getTxnType() == PFUtilities.BUY) {
                Integer specificsell = new Integer(0);
                if (editMode) {
                    specificsell = record.getSpecificSellQty(editingTrId);
                    if (specificsell == null) {
                        specificsell = 0;
                    }
                    //       record.setSellTempVal(specificsell);
                }
                switch (columnIndex) {

                    case -4:
                        return 2;
                    case 0:
                        return record.getTxnDate() + "";         // transaction date
                    case 1:
                        return record.getPrice() + "";          // price @ which stock was bought
                    case 2:
                        if (!editMode) {
                            int availableqty = record.getRemainingHolding() - record.getSellTempVal();
                            String avaliable = availableqty + "";
                            if (availableqty < 0) {
                                avaliable = "0";
                                record.setSellTempVal(0);

                            }
                            return avaliable + "";


                        } else {
                            int availableqty = record.getRemainingHolding() + specificsell - record.getSellTempVal();
                            String avaliable = availableqty + "";
                            if (availableqty < 0) {
                                avaliable = "0";
                                record.setSellTempVal(specificsell);


                            }
                            return avaliable + "";   // avilable holding
                        }
                    case 3:
                        return record.getSellTempVal() + "";      // amount about to sell fron the holding
                    case 4:
                        int val = (int) stock.getLastTradedPrice() * record.getSellTempVal();
                        return val + "";
                    default:
                        return "";


                }


            } else {

                return null;

            }


        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }


    }

    public boolean isTextFieldeeditmode() {
        return textFieldeeditmode;
    }

    public void setTextFieldeeditmode(boolean textFieldeeditmode) {
        this.textFieldeeditmode = textFieldeeditmode;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        if (aValue != null && columnIndex == 3 && rowIndex < getRowCount() && Integer.parseInt((String) aValue) >= 0) {
            int val = Integer.parseInt((String) aValue);


            ((TransactRecord) dataStore.get(rowIndex)).setSellTempVal(Integer.parseInt((String) aValue));
            textFieldeeditmode = false;


        }

    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
