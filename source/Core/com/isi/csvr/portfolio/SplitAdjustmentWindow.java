package com.isi.csvr.portfolio;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.table.LongCellEditor;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Aug 26, 2008
 * Time: 12:12:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class SplitAdjustmentWindow implements ActionListener {

    private Table table;
    private SplitAdjustmentModal modal;
    private InternalFrame frame;
    private JPanel southPanel;
    private SplitRecord split;

    private TWButton okBtn;
    private TWButton cancelBtn;
    private ArrayList<SplitAdjustmentObject> store;

    public SplitAdjustmentWindow(SplitRecord split) {
        this.split = split;
        store = PFStore.getInstance().getRecordsForSplitAdjustment(split);


        table = new Table();
        // table.setSortingDisabled();
        table.setSortingEnabled();
        table.setPreferredSize(new Dimension(300, 100));
        modal = new SplitAdjustmentModal(store);
        modal.setSplitRecord(split);
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_SPLIT_ADJUSTMENT");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("SPLIT_ADJUSTMENT_COLS"));
        modal.setViewSettings(oSetting);
        /*String[] g_asHeadings = oSetting.getColumnHeadings();
        int[] g_aiRendIDs = new int[g_asHeadings.length];
        for (int i = 0; i < g_aiRendIDs.length; i++) {
            try {
                g_aiRendIDs[i] = oSetting.getRendererID(i);
            } catch (Exception e) {
                g_aiRendIDs[i] = 0;
            }
        }*/
        table.setWindowType(ViewSettingsManager.SPLIT_ADJUSTMENT_VIEW);
        table.setModel(modal);
        modal.setTable(table);
//        table.getTable().setDefaultEditor(Number.class, new LongCellEditor(new TWTextField()));
        table.getTable().setDefaultEditor(Long.class, new LongCellEditor(new TWTextField()));
        modal.applyColumnSettings();
        modal.applySettings();
        modal.updateGUI();

        frame = new InternalFrame(table);
        frame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(false);
        frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        oSetting.setParent(frame);
        frame.updateUI();
        Client.getInstance().getDesktop().add(frame);
//        frame.setLayer(GUISettings.TOP_LAYER);

        getBtnPanel();
        frame.getContentPane().add(table, BorderLayout.CENTER);
        frame.getContentPane().add(southPanel, BorderLayout.SOUTH);

        frame.setSize(oSetting.getSize());
        frame.setLocation(oSetting.getLocation());
        frame.applySettings();
        frame.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        frame.setVisible(false);
        frame.setDetachable(false);

//        table.getPopup().showLinkMenus();
//        table.setSouthPanel(southPanel);
        GUISettings.applyOrientation(frame);
    }

    public void setVisible(boolean status) {
        frame.setVisible(status);
    }

    private void getBtnPanel() {
        southPanel = new JPanel();
        okBtn = new TWButton(Language.getString("OK"));
        okBtn.addActionListener(this);
        okBtn.setActionCommand("OK");
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        cancelBtn.addActionListener(this);
        cancelBtn.setActionCommand("CANCEL");
        southPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
        southPanel.add(okBtn);
        southPanel.add(cancelBtn);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("CANCEL")) {
            setVisible(false);
            PFStore.getInstance().removeSplitRecord(split.getId());
        } else if (e.getActionCommand().equals("OK")) {
            okActionPerformed();
        }
    }

    private void okActionPerformed() {
        PFStore.getInstance().adjustSplits(store, split);
        Client.getInstance().getPortfolioWindow().updateValuationLists();
        setVisible(false);
    }
}
