package com.isi.csvr.portfolio;

import com.isi.csvr.shared.Language;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 10, 2008
 * Time: 8:42:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class PriliminaryTxnObject {

    public static int VALID = 0;
    public static int INVALIDQTY = 1;

    private String symbol;
    private String sKey;
    private String exchange;
    private byte type;
    private String currency;
    private String memo = "";

    private Date date;
    private double price;
    private int holding;
    private double brokerage;
    private int instrumentType = 0;
    private int cumilativeQty = 0;
    private int validated = 0;
    private long datestamp = 0l;

    private int rowId;
    private long ID;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public String getTypeString() {
        if (type == PFUtilities.BUY) {
            return Language.getString("BUY");
        } else if (type == PFUtilities.SELL) {
            return Language.getString("SELL");
        } else {
            return "-";
        }
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getHolding() {
        return holding;
    }

    public void setHolding(int holding) {
        this.holding = holding;
    }

    public double getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(double brokerage) {
        this.brokerage = brokerage;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getSKey() {
        return sKey;
    }

    public void setSKey(String sKey) {
        this.sKey = sKey;
    }

    public int getCumilativeQty() {
        return cumilativeQty;
    }

    public void setCumilativeQty(int cumilativeQty) {
        this.cumilativeQty = cumilativeQty;
    }

    public int getValidated() {
        return validated;
    }

    public void setValidated(int validated) {
        this.validated = validated;
    }

    public long getDatestamp() {
        return datestamp;
    }

    public void setDatestamp(long datestamp) {
        this.datestamp = datestamp;
    }
}

