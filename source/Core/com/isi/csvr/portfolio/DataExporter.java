package com.isi.csvr.portfolio;

// Copyright (c) 2000 Home

import com.isi.csvr.ShowMessage;
import com.isi.csvr.shared.Language;
import com.isi.csvr.util.PageSetup;
import com.isi.csvr.util.WordWriter;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
//import com.isi.csvr.history.ExportListener;

public class DataExporter extends Thread {

    public final static int CSV = 0;
    public final static int TXT = 1;
    public final static int QIF = 2;
    public final static int DOC = 3;

    private String[] g_asSymbols;
    private long g_PFID;
    private long g_iFomDate;
    private long g_iToDate;
    private int g_iType;
    private String g_sDestination;
    private ExportListener g_oParent;

    /**
     * Constructor
     */
    public DataExporter(long pfID, String[] asSymbols, String sFomDate,
                        String sToDate, int iType, String sDestination) {
        super("PortfolioSimulatorDataExporter");
        g_PFID = pfID;
        g_asSymbols = asSymbols;
        g_iFomDate = (new Integer(sFomDate)).intValue();
        g_iToDate = (new Integer(sToDate)).intValue();
//System.out.println("= " + g_iFomDate + " > " + g_iToDate);
        g_iType = iType;
        g_sDestination = sDestination;
    }

    private boolean isDataWithinSelectedInterval(long time) {
        Calendar cal = Calendar.getInstance();
        Date date = new Date(time);
        cal.setTime(date);
        int iDate = (cal.get(Calendar.YEAR) * 10000) + ((cal.get(Calendar.MONTH) + 1) * 100) + cal.get(Calendar.DATE);
        cal = null;
        date = null;

        if ((iDate > g_iFomDate) && (iDate <= g_iToDate))
            return true;
        else
            return false;
    }

    public void addExportListener(ExportListener oParent) {
        this.g_oParent = oParent;
    }

    public void run() {
        int counter = 0;
        if (g_asSymbols != null)
            System.out.println("Starting DataExporter " + g_asSymbols.length + " " + g_asSymbols[0]);
        if (g_asSymbols == null) return;

        ArrayList list = null;
        TransactRecord record = null;

        if ((g_iType == CSV) || (g_iType == TXT)) {
            try {
                PrintStream oOut = new PrintStream(new FileOutputStream(g_sDestination));
                oOut.println(Language.getString("PF_CSV_HISTORY_COLS")); // getColumnHeadings());

                list = PFStore.getInstance().getTransactionList();
                for (int i = 0; i < list.size(); i++) {
                    record = (TransactRecord) list.get(i);
                    if (record.getPfID() == g_PFID) {
                        for (int j = 0; j < g_asSymbols.length; j++) {
                            if ((g_asSymbols[0].equalsIgnoreCase(Language.getString("ALL")))) {
                                //                            System.out.println("g_asSymbols[0] = " + g_asSymbols[0]);
                                if (isDataWithinSelectedInterval(record.getTxnDate())) {
                                    oOut.write(record.toString(PFUtilities.CSV).getBytes());
                                    counter++;
                                    break;
                                }
                            } else if (PFStore.isSymbolSpecificTransaction(record.getTxnType()) && (record.getSKey().equals(g_asSymbols[j]))) {
                                if (isDataWithinSelectedInterval(record.getTxnDate())) {
                                    oOut.write(record.toString(PFUtilities.CSV).getBytes());
                                    counter++;
                                    break;
                                }
                            }
                        }
                    }
                    record = null;
                }
                oOut.close();
                oOut = null;
                String sMesg = "<html><center>" + Language.getString("MSG_EXPORT_COMPLETED") + "</center><br>";
                if (Language.isLTR())
                    sMesg += Language.getString("MSG_NO_OF_RECORDS_EXPORTED") + " : <b>" + PFUtilities.getNormalizedQty(counter) + "</b></html>";
                else
                    sMesg += "<b>" + counter + "</b> : " + Language.getString("MSG_NO_OF_RECORDS_EXPORTED") + "</html>";
                ShowMessage oMessage = new ShowMessage(sMesg, "I");
//                oMessage.setVisible(true);
                oMessage = null;
//                ShowMessage oMessage = new ShowMessage(Language.getString("PF_MSG_DELETE_CURRENT_TRANSACTION"), "I");

            } catch (Exception ex) {
                ex.printStackTrace();
                ShowMessage oMessage = new ShowMessage(Language.getString("MSG_EXPORT_FAILED"), "I");
            }
            System.out.println("Export completed!!!");

        } else if (g_iType == DOC) {
            try {
                ///PrintStream oOut = new PrintStream(new FileOutputStream(g_sDestination));
                WordWriter writer = new WordWriter();
//                writer.setText(Language.getString("PF_CSV_HISTORY_COLS"));
                writer.addColumnNames(Language.getString("PF_CSV_HISTORY_COLS") + "\n");
                ///oOut.println(Language.getString("PF_CSV_HISTORY_COLS")); // getColumnHeadings());

                list = PFStore.getInstance().getTransactionList();
                for (int i = 0; i < list.size(); i++) {
                    record = (TransactRecord) list.get(i);
                    if (record.getPfID() == g_PFID) {
                        for (int j = 0; j < g_asSymbols.length; j++) {
                            if ((g_asSymbols[0].equalsIgnoreCase(Language.getString("ALL")))) {
                                //                            System.out.println("g_asSymbols[0] = " + g_asSymbols[0]);
                                if (isDataWithinSelectedInterval(record.getTxnDate())) {
                                    ///  oOut.write(record.toString(PFUtilities.CSV).getBytes());
//                                    writer.appendText(record.toString(PFUtilities.CSV));
                                    writer.addDetails(record.toString(PFUtilities.CSV) + "\n");
                                    counter++;
                                    break;
                                }
                            } else if (PFStore.isSymbolSpecificTransaction(record.getTxnType()) && (record.getSKey().equals(g_asSymbols[j]))) {
                                if (isDataWithinSelectedInterval(record.getTxnDate())) {
                                    /// oOut.write(record.toString(PFUtilities.CSV).getBytes());
//                                    writer.appendText(record.toString(PFUtilities.CSV));
                                    writer.addDetails(record.toString(PFUtilities.CSV) + "\n");
                                    counter++;
                                    break;
                                }
                            }
                        }
                    }
                    record = null;
                }
                writer.setPage(PageSetup.selectedOrientation, PageSetup.selectedPaperSize, PageSetup.selectedLeftMargin, PageSetup.selectedRightMargin, PageSetup.selectedTopMargin, PageSetup.selectedBottomMargin);
                writer.writeTable();
                writer.saveFile(g_sDestination);
                String sMesg = "<html><center>" + Language.getString("MSG_EXPORT_COMPLETED") + "</center><br>";
                if (Language.isLTR())
                    sMesg += Language.getString("MSG_NO_OF_RECORDS_EXPORTED") + " : <b>" + PFUtilities.getNormalizedQty(counter) + "</b></html>";
                else
                    sMesg += "<b>" + counter + "</b> : " + Language.getString("MSG_NO_OF_RECORDS_EXPORTED") + "</html>";
                ShowMessage oMessage = new ShowMessage(sMesg, "I");
//                oMessage.setVisible(true);
                oMessage = null;
//                ShowMessage oMessage = new ShowMessage(Language.getString("PF_MSG_DELETE_CURRENT_TRANSACTION"), "I");

            } catch (Exception ex) {
                ex.printStackTrace();
                ShowMessage oMessage = new ShowMessage(Language.getString("MSG_EXPORT_FAILED"), "I");
            }
            System.out.println("Export completed!!!");
        } else if (g_iType == QIF) {
            try {
//                g_sDestination += ".qif";
                PrintStream oOut = new PrintStream(new FileOutputStream(g_sDestination));
//                String fileName = "";
//                fileName = "\\" + "Data.qif";
//                PrintStream oOut = new PrintStream(new FileOutputStream(g_sDestination + fileName));
//                PrintStream oOut = new PrintStream(new FileOutputStream(g_sDestination + "\\" + fileName));
//                oOut.println(Language.getString("PF_CSV_HISTORY_COLS")); // getColumnHeadings());
                String dataStr = "!Type:Invst\r\n";
                oOut.write(dataStr.getBytes());
                dataStr = null;
                list = PFStore.getInstance().getTransactionList();
                for (int i = 0; i < list.size(); i++) {
                    record = (TransactRecord) list.get(i);
                    if (record.getPfID() == g_PFID) {
                        for (int j = 0; j < g_asSymbols.length; j++) {
                            if ((g_asSymbols[0].equalsIgnoreCase(Language.getString("ALL")))) {
                                //                            System.out.println("g_asSymbols[0] = " + g_asSymbols[0]);
                                if (isDataWithinSelectedInterval(record.getTxnDate())) {
                                    oOut.write(record.toString(PFUtilities.QIF).getBytes());
                                    counter++;
                                    break;
                                }
                            } else if (PFStore.isSymbolSpecificTransaction(record.getTxnType()) && (record.getSKey().equals(g_asSymbols[j]))) {
                                if (isDataWithinSelectedInterval(record.getTxnDate())) {
                                    oOut.write(record.toString(PFUtilities.QIF).getBytes());
                                    counter++;
                                    break;
                                }
                            }
                        }
                    }
                    record = null;
                }
                oOut.close();
                oOut = null;
                String sMesg = "<html><center>" + Language.getString("MSG_EXPORT_COMPLETED") + "</center><br>";
                if (Language.isLTR())
                    sMesg += Language.getString("MSG_NO_OF_RECORDS_EXPORTED") + " : <b>" + PFUtilities.getNormalizedQty(counter) + "</b></html>";
                else
                    sMesg += "<b>" + counter + "</b> : " + Language.getString("MSG_NO_OF_RECORDS_EXPORTED") + "</html>";
                ShowMessage oMessage = new ShowMessage(sMesg, "I");
//                oMessage.setVisible(true);
                oMessage = null;
//                ShowMessage oMessage = new ShowMessage(Language.getString("PF_MSG_DELETE_CURRENT_TRANSACTION"), "I");

            } catch (Exception ex) {
                ex.printStackTrace();
//                ShowMessage oMessage = new ShowMessage(Language.getString("MSG_EXPORT_FAILED"), "I");
            }
            System.out.println("Export to QIF completed!!!");
        }

        if (g_oParent != null)
            g_oParent.exportCompleted(null);
    }

    private String getColumnHeadings() {
        String[] headings = Language.getList("PF_CSV_HISTORY_COLS");
//        long columns        = g_oViewSetting.getColumns();
        StringBuffer buffer = new StringBuffer("");

        for (int i = 0; i < headings.length; i++) {
//            if ((columns & 1) == 1){
            buffer.append(",");
            buffer.append(headings[i]);
//            }
//            columns >>= 1;
        }
        return buffer.substring(1);
    }

}
