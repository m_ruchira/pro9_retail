package com.isi.csvr.portfolio;

// Copyright (c) 2000 Home

import com.isi.csvr.Client;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.TWFileFilter;
import com.isi.csvr.calendar.CalDialog;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Customizer;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.util.PageSetup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Vector;
//import com.isi.csvr.history.ExportListener;

/**
 * A Swing-based dialog class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class ExportDialog extends JDialog
        implements ActionListener, MouseListener,
        DateSelectedListener, ItemListener, ExportListener, KeyListener {
    JPanel mainPanel = new JPanel();
    JPanel fromDatePanel;
    JPanel toDatePanel;
    JPanel portfolioPanel;
    JPanel symbolsPanel;
    JPanel exportPanel;
    JComboBox cmdExportTypes;
    JComboBox cmdSymbols;
    JComboBox cmdPortfolios;
    GridLayout mainLayout = new GridLayout();
    CalDialog fromDate = null;
    CalDialog toDate = null;
    JLabel lblFromDate;
    JLabel lblToDate;
    JPanel panel3;
    JPanel panel4;
    JLabel lblSelectedPortfolio;
    JLabel lblSelectedSymbols;
    JLabel lblExportLocation;
    TWButton btnFromDate;
    TWButton btnToDate;
    TWButton btnSelectSymbol;
    JLayeredPane oLayer;
    String[] g_asSymbols;
    Symbols g_oSymbols;
    int g_iFromDate = -1;
    int g_iToDate = -1;
    DataExporter g_oDataExporter = null;
    boolean g_bCustomRange = false;
    JProgressBar g_oJProgressBar;
    JPanel g_oProgressPanel;
    TWButton btnExport;
    TWButton btnCancel;
    TWButton btnCustomize;
    ViewSetting settings = null;
    Point origin = new Point(10, 20);
    private PortfolioWindow pfWindow;
    //    private String          selSymbols;
    private Vector symbolList;

    /**
     * Constructs a new instance.
     *
     * @param parent
     * @param title
     * @param modal
     */
    public ExportDialog(JFrame parent, boolean modal) {
        super(parent, Language.getString("EXPORT_HISTORY"), modal);
        //Language l = new Language();
        //l.setLanguage("EN");
        fromDate = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
        toDate = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));

        settings = new ViewSetting();
        settings.setColumnHeadings(Language.getList("EXPORTER_CSV_COLS"));
        //change start
        settings.setColumns(BigInteger.valueOf(Integer.MAX_VALUE));
//        settings.setColumns(Integer.MAX_VALUE);
        g_oSymbols = PortfolioInterface.getSymbolsObject(); //new Symbols();
        symbolList = new Vector();

        try {
            jbInit();
            //pack();
        } catch (Exception e) {
            e.printStackTrace();
        }
        createGUI();
        loadAvailablePortfolios();
        loadSymbolsForTheSelectedPF(0);
        //this.setSize(450,400);
        pack();
        centerUI();
    }

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */

    public static void main(String[] args) {
        //(new ExportDialog()).show();
    }

    public void setParentWindow(PortfolioWindow pfWindow) {
        this.pfWindow = pfWindow;
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        mainPanel.setLayout(mainLayout);
        mainLayout.setRows(10); //9);
        mainLayout.setColumns(2);
        mainLayout.setVgap(2);

        mainPanel.setOpaque(true);
        mainPanel.setForeground(Color.red);
        //getContentPane().add(mainPanel);

    }

    private void createGUI() {
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");

        oLayer = new JLayeredPane();
        oLayer.setPreferredSize(new Dimension(420, 330));
        oLayer.setBorder(BorderFactory.createLineBorder(Color.black));
        oLayer.add(mainPanel, new Integer(0));
//        mainPanel.setBounds(3,3,415,300);
        mainPanel.setBounds(3, 3, 415, 325);
        oLayer.setOpaque(true);
        oLayer.addMouseListener(this);

        getContentPane().add(oLayer);

        JPanel panel0 = new JPanel();
        panel0.setLayout(new FlowLayout());
        JLabel lblSectPortfolio = new JLabel(Language.getString("PORTFOLIO_LIST"));
        lblSectPortfolio.setPreferredSize(new Dimension(200, 25));
        panel0.add(lblSectPortfolio);
        cmdPortfolios = new JComboBox();
        cmdPortfolios.setPreferredSize(new Dimension(175, 25));
        cmdPortfolios.addItemListener(this);
        panel0.add(cmdPortfolios);
        mainPanel.add(panel0);

        JPanel panel1 = new JPanel();
        panel1.setLayout(new FlowLayout());
        JLabel lblSectSymbols = new JLabel(Language.getString("SELECT_SYMBOLS"));
        lblSectSymbols.setPreferredSize(new Dimension(200, 25));
        panel1.add(lblSectSymbols);
        mainPanel.add(panel1);

        symbolsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        symbolsPanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        symbolsPanel.setPreferredSize(new Dimension(175, 25));
        symbolsPanel.addMouseListener(this);
        lblSelectedSymbols = new JLabel("");
        lblSelectedSymbols.setPreferredSize(new Dimension(150, 20));
        lblSelectedSymbols.setText(Language.getString("ALL"));
        symbolsPanel.add(lblSelectedSymbols);
        btnSelectSymbol = new TWButton(new DownArrow());
        btnSelectSymbol.setBorder(null);
        btnSelectSymbol.setPreferredSize(new Dimension(20, 20));
        btnSelectSymbol.addActionListener(this);
        btnSelectSymbol.setActionCommand("SS");
        symbolsPanel.add(btnSelectSymbol);
        panel1.add(symbolsPanel);
        mainPanel.add(panel1);

        ButtonGroup buttonGroup = new ButtonGroup();
        JPanel panel8 = new JPanel();
        panel8.setLayout(new FlowLayout());
        JRadioButton chkFullRange = new JRadioButton(Language.getString("COMPLETE_HISTORY"), true);
        chkFullRange.addActionListener(this);
        chkFullRange.setActionCommand("FULL");
        chkFullRange.setPreferredSize(new Dimension(400, 25));
        panel8.add(chkFullRange);
        buttonGroup.add(chkFullRange);
        mainPanel.add(panel8);

        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout());
        JRadioButton chkCustomRange = new JRadioButton(Language.getString("CUSTOM_DATE_RANGE"), false);
        chkCustomRange.addActionListener(this);
        chkCustomRange.setActionCommand("CUSTOM");
        chkCustomRange.setPreferredSize(new Dimension(400, 25));
        panel2.add(chkCustomRange);
        buttonGroup.add(chkCustomRange);
        mainPanel.add(panel2);

        panel3 = new JPanel();
        panel3.setLayout(new FlowLayout());
        JLabel lblNull1 = new JLabel("");
        lblNull1.setPreferredSize(new Dimension(40, 20));
        panel3.add(lblNull1);
        lblFromDate = new JLabel("");
        JLabel lblFromDateLabel = new JLabel(Language.getString("FROM_DATE"));
        lblFromDateLabel.setPreferredSize(new Dimension(156, 25));
        panel3.add(lblFromDateLabel);
        fromDatePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        fromDatePanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        fromDatePanel.setPreferredSize(new Dimension(175, 25));
        //lblFromDate.setBorder(new LineBorder(Color.black,1));
        lblFromDate.setPreferredSize(new Dimension(150, 20));
        lblFromDate.setOpaque(true);
        fromDatePanel.add(lblFromDate);
        btnFromDate = new TWButton(new DownArrow());
        btnFromDate.setBorder(null);
        btnFromDate.addActionListener(this);
        btnFromDate.setActionCommand("FD");
        btnFromDate.setPreferredSize(new Dimension(20, 20));
        fromDatePanel.add(btnFromDate);
        fromDatePanel.addMouseListener(this);
        panel3.add(fromDatePanel);
        mainPanel.add(panel3);

        fromDate.setVisible(false);
        fromDate.setBorder(BorderFactory.createEtchedBorder());
        fromDate.addMouseListener(this);
        fromDate.addDateSelectedListener(this);
        oLayer.add(fromDate, new Integer(1));

        panel4 = new JPanel();
        panel4.setLayout(new FlowLayout());
        JLabel lblNull2 = new JLabel("");
        lblNull2.setPreferredSize(new Dimension(40, 20));
        panel4.add(lblNull2);
        JLabel lblToDateLabel = new JLabel(Language.getString("TO_DATE"));
        lblToDateLabel.setPreferredSize(new Dimension(156, 25));
        panel4.add(lblToDateLabel);
        lblToDate = new JLabel("");
        toDatePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        toDatePanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        toDatePanel.setPreferredSize(new Dimension(175, 25));
        lblToDate.setPreferredSize(new Dimension(150, 20));
        toDatePanel.add(lblToDate);
        btnToDate = new TWButton(new DownArrow());
        btnToDate.setBorder(null);
        btnToDate.addActionListener(this);
        btnToDate.setActionCommand("TD");
        btnToDate.setPreferredSize(new Dimension(20, 20));
        toDatePanel.add(btnToDate);
        toDatePanel.addMouseListener(this);
        panel4.add(toDatePanel);
        mainPanel.add(panel4);

        toDate.setVisible(false);
        toDate.setBorder(BorderFactory.createEtchedBorder());
        toDate.addMouseListener(this);
        toDate.addDateSelectedListener(this);
        oLayer.add(toDate, new Integer(1));

        JPanel panel5 = new JPanel();
        panel5.setLayout(new FlowLayout());
        JLabel lblExportLabel = new JLabel(Language.getString("EXPORT_TYPE"));
        lblExportLabel.setPreferredSize(new Dimension(200, 25));
        panel5.add(lblExportLabel);
        cmdExportTypes = new JComboBox();
        cmdExportTypes.addItem(Language.getString("CSV_FORMAT"));
        cmdExportTypes.addItem(Language.getString("TEXT_FORMAT"));
        cmdExportTypes.addItem(Language.getString("QIF_FORMAT"));
        //cmdExportTypes.addItem(Language.getString("DOC_FORMAT"));
        cmdExportTypes.addItemListener(this);
        cmdExportTypes.addActionListener(this);
        //cmdExportTypes.addMouseListener(this);
        cmdExportTypes.setActionCommand("TYPE");
        cmdExportTypes.setPreferredSize(new Dimension(175, 25));
        panel5.add(cmdExportTypes);
        mainPanel.add(panel5);

        JPanel panel6 = new JPanel();
        panel6.setLayout(new FlowLayout());
        JLabel lblExportLocationLabel = new JLabel(Language.getString("EXPORT_TO"));
        lblExportLocationLabel.setPreferredSize(new Dimension(200, 25));
        panel6.add(lblExportLocationLabel);
        exportPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        exportPanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        exportPanel.setPreferredSize(new Dimension(175, 25));
        exportPanel.addMouseListener(this);
        lblExportLocation = new JLabel("");
        lblExportLocation.setPreferredSize(new Dimension(150, 20));
        exportPanel.add(lblExportLocation);
        TWButton btnExportLocation = new TWButton(new DownArrow());
        btnExportLocation.setBorder(null);
        btnExportLocation.addActionListener(this);
        btnExportLocation.setPreferredSize(new Dimension(20, 20));
        btnExportLocation.setActionCommand("DESTINATION");
        exportPanel.add(btnExportLocation);
        panel6.add(exportPanel);
        mainPanel.add(panel6);

        JPanel panel7 = new JPanel();
        panel7.setLayout(new FlowLayout());

        btnCustomize = new TWButton(Language.getString("CUSTOMIZE"));
        btnCustomize.addActionListener(this);
        btnCustomize.addKeyListener(this);
        btnCustomize.setActionCommand("CUSTOMIZE");
        btnCustomize.setPreferredSize(new Dimension(133, 25));
//        panel7.add(btnCustomize);

        btnExport = new TWButton(Language.getString("EXPORT"));
        btnExport.addActionListener(this);
        btnExport.addKeyListener(this);
        btnExport.setActionCommand("EXPORT");
        btnExport.setPreferredSize(new Dimension(133, 25));
        panel7.add(btnExport);

        btnCancel = new TWButton(Language.getString("CLOSE"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.addActionListener(this);
        btnCancel.addKeyListener(this);
        btnCancel.setPreferredSize(new Dimension(133, 25));
        panel7.add(btnCancel);
        mainPanel.add(panel7);

        g_oProgressPanel = new JPanel(new FlowLayout());
        g_oJProgressBar = new JProgressBar();
        g_oJProgressBar.setPreferredSize(new Dimension(400, 25));
        g_oJProgressBar.setStringPainted(true);
        g_oJProgressBar.setBorder(null);
        g_oJProgressBar.setMaximum(100);
        g_oJProgressBar.setMinimum(0);
        g_oJProgressBar.setValue(0);
        g_oProgressPanel.add(g_oJProgressBar);
        g_oProgressPanel.setVisible(false);

        mainPanel.add(g_oProgressPanel);
        selectFullRange();
        if (!Language.isLTR())
            GUISettings.applyOrientation(mainPanel, ComponentOrientation.RIGHT_TO_LEFT);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setResizable(false);
    }
/*    private void loadSymbolList() {
        cmdSymbols.removeAllItems();
        cmdSymbols.addItem(Language.getString("COMMON_ALL"));
        ValuationRecord record = null;
        ArrayList list = PFStore.getInstance().getValuationList();
        for (int i = 0; i < list.size(); i++) {
            record = (ValuationRecord)list.get(i);
            cmdSymbols.addItem(PortfolioInterface.getCompanyName(record.getSKey()));
//            cmdSymbols.addItem(PortfolioInterface.getSymbolFromKey(record.getSKey()));
            record = null;
        }
        list = null;
    } */

    private void showSymbolsPanel() {
        int popWidth = 0;
        SymbolList sl = new SymbolList(this, pfWindow.getSelectedPFIDs(), symbolList);
        if (Language.isLTR()) {
            popWidth = (int) sl.getPreferredSize().getWidth();
        }
        sl.show(btnSelectSymbol, btnSelectSymbol.getWidth() - popWidth, btnSelectSymbol.getHeight() - 4);
        sl = null;
    }

    private void centerUI() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == cmdExportTypes) {
            //System.out.println("item changed " );
            initDestination();
            initCustomize();
        } else if (e.getSource() == cmdPortfolios) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                setSymbols("");
                loadSymbolsForTheSelectedPF(cmdPortfolios.getSelectedIndex());
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        //System.out.println("Com :" + e.getActionCommand());
        if (e.getActionCommand().equals("FD"))
            showFromDateCal();
        else if (e.getActionCommand().equals("TD"))
            showToDateCal();
        else if (e.getActionCommand().equals("SS")) {
            hideCals();
            showSymbolsPanel();
        } else if (e.getActionCommand().equals("EXPORT")) {
            hideCals();
            exportData();
        } else if (e.getActionCommand().equals("CANCEL")) {
            this.dispose();
        } else if (e.getActionCommand().equals("DESTINATION")) {
            hideCals();
            lblExportLocation.setText(getDestination());
        } else if (e.getActionCommand().equals("FULL")) {
            hideCals();
            selectFullRange();
        } else if (e.getActionCommand().equals("CUSTOM")) {
            hideCals();
            selectCustomRange();
        } else if (e.getActionCommand().equals("TYPE")) {
            //System.out.println("combo");
            hideCals();
        } else if (e.getActionCommand().equals("CUSTOMIZE")) {
            settings = showSelectColumns();
        }
    }

    private void initDestination() {
        lblExportLocation.setText("");
    }

    private void initCustomize() {
        if (cmdExportTypes.getSelectedIndex() == 0)
            btnCustomize.setEnabled(true);
        else
            btnCustomize.setEnabled(false);
    }

    private void selectFullRange() {
        g_iFromDate = 0;
        g_iToDate = 99999999;
        btnToDate.setEnabled(false);
        btnFromDate.setEnabled(false);
        btnToDate.setIcon(null);
        btnFromDate.setIcon(null);
        lblFromDate.setText("");
        lblToDate.setText("");
        g_bCustomRange = false;
    }

    private void selectCustomRange() {
        g_iFromDate = -1;
        g_iToDate = -1;
        btnToDate.setEnabled(true);
        btnFromDate.setEnabled(true);
        btnToDate.setIcon(new DownArrow());
        btnFromDate.setIcon(new DownArrow());
        g_bCustomRange = true;
    }

    private void exportData() {
        if (!lblSelectedSymbols.getText().trim().equals(g_oSymbols.toString())) {
            g_oSymbols = PortfolioInterface.getSymbolsObject(lblSelectedSymbols.getText().trim()); //new Symbols(lblSelectedSymbols.getText().trim(),false);
        }

        long selPFID = (PFStore.getInstance().getPortfolio(cmdPortfolios.getSelectedIndex())).getPfID();
        if (!validateInput()) return;

        g_asSymbols = g_oSymbols.getSymbols();
        switch (cmdExportTypes.getSelectedIndex()) {
            case 0:
                g_oProgressPanel.setVisible(true);
                g_oDataExporter = new DataExporter(selPFID, g_asSymbols, "" + g_iFromDate,
                        "" + g_iToDate, DataExporter.CSV, lblExportLocation.getText());
                break;
            case 1:
                g_oProgressPanel.setVisible(true);
                g_oDataExporter = new DataExporter(selPFID, g_asSymbols, "" + g_iFromDate,
                        "" + g_iToDate, DataExporter.TXT, lblExportLocation.getText());
                break;
            case 2:
                g_oProgressPanel.setVisible(true);
                g_oDataExporter = new DataExporter(selPFID, g_asSymbols, "" + g_iFromDate,
                        "" + g_iToDate, DataExporter.QIF, lblExportLocation.getText());
                break;
            case 3:
                PageSetup setup = new PageSetup(Client.getInstance().getWindow());
                setup.setLocationRelativeTo(Client.getInstance().getWindow());
                int val = setup.showDialog();
                if (val == PageSetup.STATUS_OK) {
                    g_oProgressPanel.setVisible(true);
                    g_oDataExporter = new DataExporter(selPFID, g_asSymbols, "" + g_iFromDate,
                            "" + g_iToDate, DataExporter.DOC, lblExportLocation.getText());
                }
                break;

        }
        g_oDataExporter.addExportListener(this);
        g_oDataExporter.start();
        this.setEnabled(false);
    }

    private ViewSetting showSelectColumns() {
        if (settings == null) {
            settings = new ViewSetting();
            settings.setColumnHeadings(Language.getList("EXPORTER_CSV_COLS"));
            //change start
            settings.setColumns(BigInteger.valueOf(Integer.MAX_VALUE));
//            settings.setColumns(Long.MAX_VALUE); //select all columnd
        }
        Customizer c = new Customizer(settings);
        c.showDialog();
        //System.out.println(settings.getColumns());
        return settings;
    }

    private boolean validateInput() {
        boolean bReturnValue = false;

        if (lblSelectedSymbols.getText().trim().equals(""))
            JOptionPane.showMessageDialog(this, Language.getString("MSG_SELECT_A_SYMBOL"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else if (g_iFromDate == -1)
            JOptionPane.showMessageDialog(this, Language.getString("MSG_SELECT_FROM_DATE"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else if (g_iToDate == -1)
            JOptionPane.showMessageDialog(this, Language.getString("MSG_SELECT_TO_DATE"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else if (g_iFromDate > g_iToDate)
            JOptionPane.showMessageDialog(this, Language.getString("MSG_INVALID_DATE_RANGE"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else if (lblExportLocation.getText().trim().equals(""))
            JOptionPane.showMessageDialog(this, Language.getString("MSG_SELECT_DESTINATION"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
//        else if (validateSymbols(g_oSymbols.getSymbols()) != null)
//            JOptionPane.showMessageDialog(this,Language.getString("INVALID_SYMBOL")
//                + validateSymbols(g_oSymbols.getSymbols())
//                ,Language.getString("MSG_TITLE_ERROR"),JOptionPane.OK_OPTION);
        else
            bReturnValue = true;

        return bReturnValue;
    }

//    private void getSymbols()
//    {
//        String validateResult = null;
//
//        g_oSymbols = new Symbols(lblSelectedSymbols.getText().trim(),false);
//        validateResult = validateSymbols(g_oSymbols.getSymbols());
//
//        if (validateResult== null){
//            Companies oCompanies = new Companies();
//            oCompanies.setSymbols(g_oSymbols);
//            this.setVisible(false);
//            oCompanies.showDialog();
//            oCompanies.dispose();
//            g_asSymbols = g_oSymbols.getSymbols();
//            if ((g_asSymbols != null) && (g_asSymbols.length > 0))
//                lblSelectedSymbols.setText(g_oSymbols.toString());
//            else
//                lblSelectedSymbols.setText("");
//            this.setVisible(true);
//        }else{
//            //this.setVisible(false);
//            JOptionPane.showMessageDialog(this,Language.getString("INVALID_SYMBOL") +
//                validateResult
//                ,Language.getString("MSG_TITLE_ERROR"),JOptionPane.OK_OPTION);
//            //(new ShowMessage(false,"Invalid Symbol : " + validateResult,"E"));
//            //this.setVisible(true);
//        }
//    }

    /**
     * Check if all symbols in the array are valid
     * <p/>
     * private String validateSymbols(String[] symbols){
     * int count = symbols.length;
     * <p/>
     * //System.out.println("inside validate");
     * for (int index=0;index<count;index++){
     * // System.out.println("inside validate " + symbols[index]);
     * //           if (!DataStore.isValidSymbol(symbols[index])){
     * if (!PortfolioInterface.isContainedSymbol(symbols[index])){
     * return symbols[index];
     * }
     * }
     * return null; // sucess, all symbols valid
     * }
     */

    private void showFromDateCal() {
        toDate.setVisible(false);
        if (!fromDate.isVisible()) {
            Point location = SwingUtilities.convertPoint(panel3, fromDatePanel.getLocation(), mainPanel);
            fromDate.setLocation((int) location.getX() + 3,
                    (int) (location.getY()));
            fromDate.setVisible(true);
        } else {
            fromDate.setVisible(false);
        }
    }

    private void showToDateCal() {
        fromDate.setVisible(false);
        if (!toDate.isVisible()) {
            Point location = SwingUtilities.convertPoint(panel4, toDatePanel.getLocation(), mainPanel);
            toDate.setLocation((int) location.getX() + 3,
                    (int) (location.getY() - 25));
            toDate.setVisible(true);
        } else {
            toDate.setVisible(false);
        }
    }

    private JLabel createColoredLabel(String text,
                                      Color color,
                                      Point origin) {
        JLabel label = new JLabel(text);
        label.setVerticalAlignment(JLabel.TOP);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setOpaque(true);
        label.setBackground(color);
        label.setForeground(Color.black);
        label.setBorder(BorderFactory.createLineBorder(Color.black));
        label.setBounds(origin.x, origin.y, 140, 140);
        return label;
    }

    private void hideCals() {
        fromDate.setVisible(false);
        toDate.setVisible(false);
    }

    public void mouseClicked(MouseEvent e) {
        //if(e.getSource() == oLayer)
        {
            hideCals();
        }
        if (e.getSource() == symbolsPanel) {
            showSymbolsPanel();
        } else if (e.getSource() == exportPanel) {
            lblExportLocation.setText(getDestination());
        } else if (e.getSource() == fromDatePanel) {
            if (g_bCustomRange)
                showFromDateCal();
        } else if (e.getSource() == toDatePanel) {
            if (g_bCustomRange)
                showToDateCal();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        if (source == fromDate) {
            lblFromDate.setText(PFUtilities.getConvertedData(" " + iYear + " / " + (iMonth + 1) + " / " + iDay));
            g_iFromDate = (iYear * 10000) + ((iMonth + 1) * 100) + iDay;
        } else {
            lblToDate.setText(PFUtilities.getConvertedData(" " + iYear + " / " + (iMonth + 1) + " / " + iDay));
            g_iToDate = (iYear * 10000) + ((iMonth + 1) * 100) + iDay;
        }
    }

    public String getDestination() {
        int status = 0;
        String sMeta = null;
        String[] extentions = new String[1];

        SmartFileChooser oFC;
//        if ((cmdExportTypes.getSelectedIndex() == 0) || (cmdExportTypes.getSelectedIndex() == 2)) {
//
//            UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILECHOOSER_FOLDERNAME_LABEL_TEXT"));
//            oFC = new SmartFileChooser(".");
//            GUISettings.localizeFileChooserHomeButton(oFC);
//            oFC.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
//            oFC.setDialogTitle(Language.getString("EXPORT_TO_FOLDER"));
//			oFC.hideContainerOf((String)UIManager.get("FileChooser.fileNameLabelText"));
//			oFC.hideContainerOf((String)UIManager.get("FileChooser.filesOfTypeLabelText"));
//
//        } else {
        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILENAME"));
        oFC = new SmartFileChooser(Settings.getAbsolutepath());
        GUISettings.localizeFileChooserHomeButton(oFC);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        oFC.setDialogTitle(Language.getString("EXPORT_TO_FILE"));
        oFC.setAcceptAllFileFilterUsed(false);
        if (cmdExportTypes.getSelectedIndex() == 0) {
            extentions[0] = "csv";
            sMeta = Language.getString("CSV");
        } else if (cmdExportTypes.getSelectedIndex() == 1) {
            extentions[0] = "txt";
            sMeta = Language.getString("TXT");
        } else if (cmdExportTypes.getSelectedIndex() == 2) {
            extentions[0] = "qif";
            sMeta = Language.getString("QIF");
        } else if (cmdExportTypes.getSelectedIndex() == 3) {
            extentions[0] = "doc";
            sMeta = Language.getString("DOC");
        }
        TWFileFilter oFilter = new TWFileFilter(sMeta, extentions);
        oFC.setFileFilter(oFilter);
        oFC.setSelectedFile(new File((PFStore.getInstance().getPortfolio(cmdPortfolios.getSelectedIndex())).getName())); // "TestFileName"));
//        }
        // Reset the UIManager for the folder name
        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILENAME"));

        status = oFC.showOpenDialog(this);
        if (status == JFileChooser.APPROVE_OPTION) {
            String sFile = oFC.getSelectedFile().getAbsolutePath();
            System.out.println("File Name " + sFile);
            String sExtension = null;
            switch (cmdExportTypes.getSelectedIndex()) {
                case DataExporter.CSV:
                    sExtension = ".csv";
                    break;
                case DataExporter.TXT:
                    sExtension = ".txt";
                    break;
                case DataExporter.QIF:
                    sExtension = ".qif";
                    break;
                case DataExporter.DOC:
                    sExtension = ".doc";
                    break;
            }
            if (!sFile.trim().endsWith(sExtension))
                sFile += sExtension;
            sExtension = null;

            File[] fileList;
            try {
                fileList = oFC.getCurrentDirectory().listFiles();
                for (File file : fileList) {
                    if (file.getPath().equalsIgnoreCase(sFile)) {
                        String message = Language.getString("REPLACE_FILE_MSG");
                        int option = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                        if (option == JOptionPane.YES_OPTION) {
                            break;
                        } else {
                            sFile = "";
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return sFile;
        } else {
            return "";
        }
    }

/*
        public String getDestination() {
            int status = 0;
            String[] extentions = new String[1];

            SmartFileChooser oFC;
            if ((cmdExportTypes.getSelectedIndex() == 0) || (cmdExportTypes.getSelectedIndex() == 2))
            {

                UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILECHOOSER_FOLDERNAME_LABEL_TEXT"));
                oFC = new SmartFileChooser(".");
                GUISettings.localizeFileChooserHomeButton(oFC);
                oFC.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                oFC.setDialogTitle(Language.getString("EXPORT_TO_FOLDER"));
                oFC.hideContainerOf((String)UIManager.get("FileChooser.fileNameLabelText"));
                oFC.hideContainerOf((String)UIManager.get("FileChooser.filesOfTypeLabelText"));

            }
            else
            {
                UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILECHOOSER_FILENAME_LABEL_TEXT"));
                oFC = new SmartFileChooser(".");
                GUISettings.localizeFileChooserHomeButton(oFC);
                oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
                oFC.setDialogTitle(Language.getString("EXPORT_TO_FILE"));
                oFC.setAcceptAllFileFilterUsed(false);
                extentions[0] = "txt";
                TWFileFilter oFilter = new
                    TWFileFilter(Language.getString("FILECHOOSER_TEXT_META"),extentions);
                oFC.setFileFilter(oFilter);

            }
            // Reset the UIManager for the folder name
            UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILECHOOSER_FILENAME_LABEL_TEXT"));

            status = oFC.showOpenDialog(this);
            if (status == JFileChooser.APPROVE_OPTION)
            {
                String sFile = oFC.getSelectedFile().getAbsolutePath();
                if (cmdExportTypes.getSelectedIndex() == 1)
                {
                    if (!sFile.trim().endsWith(".txt"))
                        sFile+=".txt";
                }
                return sFile;
            } else
            {
                return "";
            }
        }

*/

    public void exportCompleted(Exception e) {
        if (e != null)
            JOptionPane.showMessageDialog(this, e.toString(),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION + JOptionPane.ERROR_MESSAGE);
//        else
//            JOptionPane.showMessageDialog(this,Language.getString("MSG_EXPORT_COMPLETED"),
//                Language.getString("INFORMATION"),JOptionPane.OK_OPTION + JOptionPane.INFORMATION_MESSAGE);

        this.setEnabled(true);
        g_oJProgressBar.setValue(0);
        g_oProgressPanel.setVisible(false);
        g_oDataExporter = null;
    }

    public void exportProgressChanged(int iProgress, int iMax) {
        g_oJProgressBar.setValue(iProgress * 100 / iMax);
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == btnCancel) {
                this.dispose();
            } else {
                hideCals();
                exportData();
            }
        }
    }

    public void setSymbols(String text) {
//        selSymbols = text;
        lblSelectedSymbols.setText(text);
    }

    private void loadAvailablePortfolios() {
        ArrayList list = PFStore.getInstance().getPortfolioList();
        for (int i = 0; i < list.size(); i++) {
            cmdPortfolios.addItem(((PortfolioRecord) list.get(i)).getName());
        }
        list = null;
    }

    private void loadSymbolsForTheSelectedPF(int index) {
        TransactRecord record = null;
        symbolList.removeAllElements();
        ArrayList list = PFStore.getInstance().getTransactionList();
        PortfolioRecord pfRecord = PFStore.getInstance().getPortfolio(index);
        if (pfRecord != null) {
            for (int i = 0; i < list.size(); i++) {
                record = (TransactRecord) list.get(i);
                if ((record.getPfID() == pfRecord.getPfID()) && (PFStore.isSymbolSpecificTransaction(record.getTxnType()))) {
                    boolean isSymbolExists = false;
                    for (int j = 0; j < symbolList.size(); j++) {
                        if (((String) symbolList.elementAt(j)).equals(record.getSKey())) {
                            isSymbolExists = true;
                            break;
                        }
                    }
                    if (!isSymbolExists) {
                        symbolList.addElement(record.getSKey());
                    }
                }
                record = null;
            }
//            cmdSymbols.removeAllItems();
//            for (int k = 0; k < symbolList.size(); k++) {
//                cmdSymbols.addItem((String)symbolList.elementAt(k));
//            }
        }
        record = null;
        list = null;
//        symbolList  = null;
        pfRecord = null;
    }
}

