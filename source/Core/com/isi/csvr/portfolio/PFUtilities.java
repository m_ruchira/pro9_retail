package com.isi.csvr.portfolio;

import com.isi.csvr.TWFileFilter;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class PFUtilities {

    public static final byte INTERNATIONAL_VERSION = 44;
    public static final byte TADAWUL_VERSION = 45;

    public static final byte NONE = 0;
    public static final byte BUY = 1;
    public static final byte SELL = 2;
    public static final byte OP_BAL = 3;
    public static final byte DIVIDEND = 4;
    public static final byte DEPOSITE = 5;
    public static final byte WITHDRAWAL = 6;
    public static final byte CHARGES = 7;
    public static final byte REFUNDS = 8;

    public static final byte DATA = 4;
    public static final byte TXT = 5;
    public static final byte CSV = 6;
    public static final byte QIF = 7;

    public static final byte FILE_OPEN_DIALOG_TYPE = 8;
    public static final byte FILE_SAVE_DIALOG_TYPE = 9;

    public static final byte TRANSACTION_RECORD = 7;
    public static final byte PORTFOLIO_RECORD = 8;
    public static final byte SPLIT_RECORD = 9;
    public static final byte COMMISSION_RECORD = 10;

    public static final long EVERY_10_MINUTE = 600000;   // 10 minutes
    public static final long EVERY_15_MINUTE = 900000;   // 15 minutes
    public static final long EVERY_30_MINUTE = 1800000;  // 30 minutes

    public static final String backUpPath = Settings.getAbsolutepath() + "PortfolioData";

    //    private static SimpleDateFormat dateFormatter   = new SimpleDateFormat("dd/MM/yyyy");
//    private static SimpleDateFormat dateWithTimeFormatter = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", new Locale("ar", "SA"));
    private static SimpleDateFormat dateWithTimeFormatter = new SimpleDateFormat("dd/MM/yyyy");
    private static SimpleDateFormat qifDateFormatter = new SimpleDateFormat("MM/dd/yyyy");

    private static DecimalFormat oPriceFormat = new DecimalFormat(" ###,##0.00 ");
    private static DecimalFormat oQuantityFormat = new DecimalFormat(" ###,##0 ");

    private static TWDateFormat oNormalizedDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
    private static TWDecimalFormat oNormalizedPriceFormat_2 = new TWDecimalFormat(" ###,##0.00 ");
    private static TWDecimalFormat oNormalizedPriceFormat_3 = new TWDecimalFormat(" ###,##0.000 ");
    private static TWDecimalFormat oNormalizedQuantityFormat = new TWDecimalFormat(" ###,##0 ");

    private static DecimalFormat qifQuantityFormat = new DecimalFormat("###,##0");
    private static DecimalFormat qifPriceFormat = new DecimalFormat("###,##0.00");

    public PFUtilities() {
//        dateWithTimeFormatter = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", new Locale("asia/riyadh"));
    }

    public static String getTxnTypeString(byte type) {
        switch (type) {
            case BUY:
                return Language.getString("BUY");
            case SELL:
                return Language.getString("SELL");
            case OP_BAL:
                return Language.getString("OPENING_BAL");
            case DIVIDEND:
                return Language.getString("DIVIDEND");
            case DEPOSITE:
                return Language.getString("DEPOSITES");
            case WITHDRAWAL:
                return Language.getString("WITHDROWALS");
            case CHARGES:
                return Language.getString("CHARGES");
            case REFUNDS:
                return Language.getString("REFUNDS");
            default:
                return "";
        }
    }

    public static String getQIFTxnTypeString(byte type) {
        switch (type) {
            case BUY:
            case OP_BAL:
            case DIVIDEND:
                return Language.getString("BUY");
            case SELL:
                return Language.getString("SELL");
            default:
                return "";
        }
    }

    public static String getFormattedPrice(double price) {
        return oPriceFormat.format(price);
    }

    public static String getFormattedQty(long qty) {
        return oQuantityFormat.format(qty);
    }

    public static String getNormalizedPrice(double price) {
        return oNormalizedPriceFormat_2.format(price);
    }

    public static String getNormalizedPrice(double price, int decimals) {
        if (decimals == 3) {
            return oNormalizedPriceFormat_3.format(price);
        } else {
            return oNormalizedPriceFormat_2.format(price);
        }
    }

    public static String getNormalizedQty(long qty) {
        return oNormalizedQuantityFormat.format(qty);
    }

    public static String getFormattedDate(Date date) {
        return oNormalizedDateFormat.format(date);
    }

    public static String getFormattedDate(long millis) {
        return oNormalizedDateFormat.format(new Date(millis));
    }

    public static String getFormattedDateWithTime(long millis) {
        return dateWithTimeFormatter.format(new Date(millis));
    }

    public static String getFormattedQIFDate(long millis) {
        String sData = qifDateFormatter.format(new Date(millis));
        byte[] bArray = sData.getBytes();
        for (int i = bArray.length - 1; i >= 0; i--) {
//            System.out.println("==== " + bArray[i] + " - " + (char)bArray[i]);
            if (bArray[i] == 47) { //(byte)'/') {
                bArray[i] = 39; //(byte)'\'';
                break;
//            } else {
//                System.out.println("<<<< " + bArray[i] + " - " + (char)bArray[i]);
            }
        }
//System.out.println(">>>> " + new String(bArray) + "  -  " + '\'');
        return new String(bArray); // sData;
    }

    public static String getFormattedQIFQty(long qty) {
        return qifQuantityFormat.format(qty);
    }

    public static String getFormattedQIFPrice(double price) {
        return qifPriceFormat.format(price);
    }

    /*
    * Open/Save the selected Portfolios file
    */
    public static String getSelectedFilePath(Component parent, byte type, int mode, String sPath) {
        int status = 0;
        String sTitle = null;
        String extentions[] = new String[1];

        JFileChooser oFC = null;
        try {
            oFC = new JFileChooser(sPath); //"./PFData");
        } catch (Exception ex) {
            oFC = new JFileChooser(PFUtilities.backUpPath);
        }
        oFC.setAcceptAllFileFilterUsed(false);
        GUISettings.localizeFileChooserHomeButton(oFC);
        GUISettings.applyOrientation(oFC);
        oFC.setFileSelectionMode(mode); //JFileChooser.FILES_ONLY);
        if (type == FILE_SAVE_DIALOG_TYPE)
            sTitle = Language.getString("EXPORT_TO_FILE");
        else if (type == FILE_OPEN_DIALOG_TYPE)
            sTitle = Language.getString("IMPORT_FROM_FILE");
        oFC.setDialogTitle(sTitle);
        extentions[0] = "dat";
        TWFileFilter oFilter = new TWFileFilter(Language.getString("DAT"), extentions);
        oFC.setFileFilter(oFilter);

        if (type == FILE_SAVE_DIALOG_TYPE)
            status = oFC.showSaveDialog(parent);
        else if (type == FILE_OPEN_DIALOG_TYPE)
            status = oFC.showOpenDialog(parent);
        if (status == JFileChooser.APPROVE_OPTION) {
            String sFile = oFC.getSelectedFile().getAbsolutePath();

            if (mode == JFileChooser.FILES_ONLY) {
                if (!sFile.trim().endsWith(extentions[0]))
                    sFile += "." + extentions[0];
            }

            if (type == FILE_SAVE_DIALOG_TYPE) {
                File[] fileList;
                try {
                    fileList = oFC.getCurrentDirectory().listFiles();
                    for (File file : fileList) {
                        if (file.getPath().equalsIgnoreCase(sFile)) {
                            String message = Language.getString("REPLACE_FILE_MSG");
                            int option = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                            if (option == JOptionPane.YES_OPTION) {
                                break;
                            } else {
                                sFile = null;
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            oFC = null;
            extentions = null;
            oFilter = null;
            return sFile;
        } else {
            return null;
        }
    }

    public static String getConvertedData(String sData) {
        if (Settings.isShowArabicNumbers())
            return GUISettings.arabize(sData);
        else
            return sData;
    }

    public static boolean isPriorDate(Date date1, Date date2) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        if (formatter.format(date1).compareTo(formatter.format(date2)) >= 0)
            return false;
        else
            return true;
    }

}