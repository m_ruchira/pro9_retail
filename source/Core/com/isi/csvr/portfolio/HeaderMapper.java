package com.isi.csvr.portfolio;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWComboItem;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 10, 2008
 * Time: 3:50:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class HeaderMapper {

    public static String SYMBOL = "symbol";
    public static String EXCHANGE = "exchange";
    public static String DATE = "date";
    public static String PRICE = "price";
    public static String COMISSION = "commission";
    public static String TYPE = "type";
    public static String HOLDING = "holding";
    public static String CURRENCY = "currency";
    public static String MEMO = "memo";
    private static String[] list = {SYMBOL, EXCHANGE, DATE, PRICE, COMISSION, TYPE, HOLDING, CURRENCY, MEMO};
    private static String[] noExglist = {SYMBOL, DATE, PRICE, COMISSION, TYPE, HOLDING, CURRENCY, MEMO};
    public static String NA = "N/A";
    public static String[] mappedHeadings;
    private static TWComboItem exchangeItem;

    public static int getHeaderCount() {
        return 9;
    }

    public static TWComboItem getExchangeComboItem() {
        return exchangeItem;
    }

    public static int getSpecificRow(String type) {

        if (mappedHeadings != null && mappedHeadings.length > 0) {
            for (int i = 0; i < mappedHeadings.length; i++) {
                String val = mappedHeadings[i];
                if (val.equals(type)) {
                    return i;
                }
            }
            return -1;
        }
        return -1;

    }

    public static int getComboItemNumber(String item) {
        for (int i = 0; i < list.length; i++) {
            if (list[i].equalsIgnoreCase(item)) {
                return i;
            }

        }
        return -1;
    }

    public static int getComboItemNumberSingleExchange(String item) {
        for (int i = 0; i < noExglist.length; i++) {
            if (noExglist[i].equalsIgnoreCase(item)) {
                return i;
            }

        }
        return -1;
    }

    public TWComboItem[] getItemList(String[] headings) {
        TWComboItem[] items;

        items = new TWComboItem[headings.length];

        for (int i = 1; i < headings.length; i++) {       // exclude the number column
            TWComboItem item = new TWComboItem(list[i - 1], headings[i]);
            if (i - 1 == 1) {
                exchangeItem = item;
            }
            items[i - 1] = item;

        }
        items[items.length - 1] = new TWComboItem(NA, Language.getString("NA"));

        return items;

    }


}
