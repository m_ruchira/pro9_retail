package com.isi.csvr.portfolio;


import com.isi.csvr.shared.Language;

import java.util.StringTokenizer;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class PortfolioRecord {

    private long pfID;
    private String name;
    private String currency;
    private String commisionName;
    private double cashBalance;
    private boolean isDefault;

    public PortfolioRecord() {
        pfID = System.currentTimeMillis();
    }

    public double getCashBalance() {
        return cashBalance;
    }

    public void setCashBalance(double cashBalance) {
        this.cashBalance = cashBalance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getPfID() {
        return pfID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefaultPortfolio(boolean status) {
        this.isDefault = status;
    }

    public String getCommisionName() {
        return commisionName;
    }

    public void setCommisionName(String commisionName) {
        this.commisionName = commisionName;
    }

    public void retrievePortfolio(String sData) {
        StringTokenizer st = new StringTokenizer(sData, "|", false);

        pfID = Long.parseLong(st.nextToken());
        name = st.nextToken();
        currency = st.nextToken();
        cashBalance = Float.parseFloat(st.nextToken());
        if ((st.countTokens() == 2)) {
            commisionName = st.nextToken();
        } else {
            commisionName = Language.getString("MANUAL");
        }
        int status = 0;
        try {
            status = Integer.parseInt(st.nextToken());
        } catch (NumberFormatException ex) {
        }
        if (status == 1)
            isDefault = true;
        else
            isDefault = false;
        st = null;
    }

//    public String toString(byte fileType) {
//        String delim = "";
//        switch (fileType) {
//            case PFRecord.TXT :
//                delim = "|";
//                break;
//            case PFRecord.CSV :
//                delim = ",";
//                break;
//            case PFRecord.QIF :
//                delim = "|";
//                break;
//        }

    public String toString(byte fileType) {
        String delim = "";
        switch (fileType) {
            case PFUtilities.DATA:
                delim = "|";
                break;
            case PFUtilities.TXT:
                delim = "\t";
                break;
            case PFUtilities.CSV:
                delim = ",";
                break;
            case PFUtilities.QIF:
                delim = "|";
                break;
        }
        int status = 0;
        if (isDefault)
            status = 1;
        else
            status = 0;
        return PFUtilities.PORTFOLIO_RECORD + delim + pfID + delim + name + delim + currency +
                delim + cashBalance + delim + commisionName + delim + status;
    }

}