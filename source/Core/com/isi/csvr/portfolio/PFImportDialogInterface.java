package com.isi.csvr.portfolio;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 11, 2008
 * Time: 1:57:28 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PFImportDialogInterface {
    public void setSymbol(String key, int instrument);

    public void setData(Object data);

    void notifyInvalidSymbol(String symbol);


}
