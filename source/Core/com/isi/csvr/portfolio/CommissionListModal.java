package com.isi.csvr.portfolio;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Sep 2, 2008
 * Time: 11:21:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommissionListModal extends CommonTable
        implements TableModel, CommonTableInterface {

    private ArrayList<String> store;

    public CommissionListModal() {
        this.store = PFStore.getInstance().getCommissionList();
    }

    public int getRowCount() {
        if (store != null) {
            return store.size();
        }
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int iCol) {
        try {
            CommissionObject obj = PFStore.getInstance().getCommission(store.get(rowIndex));
            switch (iCol) {
                case -1:
                    return obj;
                case 0:
                    return obj.getId();
                case 1:
                    return obj.getMinPrice();
                case 2:
                    return obj.getPercentage();
                case 3:
                    return "1";
                case 4:
                    return "1";
                default:
                    return "";
            }
        } catch (Exception e) {
            return Constants.NULL_STRING;
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        //return getValueAt(0, iCol).getClass();
        switch (iCol) {
            case 0:
                return String.class;
            case 1:
            case 2:
                return Double.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
