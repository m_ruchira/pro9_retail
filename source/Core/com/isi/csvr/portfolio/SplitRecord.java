package com.isi.csvr.portfolio;

import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Aug 26, 2008
 * Time: 10:10:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class SplitRecord {

    private long id;
    private String key;
    private long startDate = 0;
    private byte firstFactor = 0;
    private byte secondFactor = 0;

    public SplitRecord() {
    }

    public SplitRecord(long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public byte getFirstFactor() {
        return firstFactor;
    }

    public void setFirstFactor(byte firstFactor) {
        this.firstFactor = firstFactor;
    }

    public byte getSecondFactor() {
        return secondFactor;
    }

    public void setSecondFactor(byte secondFactor) {
        this.secondFactor = secondFactor;
    }

    public long getId() {
        return id;
    }

    public void retrieveSplit(String sData) {
        try {
            StringTokenizer st = new StringTokenizer(sData, "|", false);
            id = Long.parseLong(st.nextToken());
            key = st.nextToken();
            startDate = Long.parseLong(st.nextToken());
            firstFactor = Byte.parseByte(st.nextToken());
            secondFactor = Byte.parseByte(st.nextToken());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String toString(byte fileType) {
        String delim = "";
        switch (fileType) {
            case PFUtilities.DATA:
                delim = "|";
                break;
            case PFUtilities.TXT:
                delim = "\t";
                break;
            case PFUtilities.CSV:
                delim = ",";
                break;
            case PFUtilities.QIF:
                delim = "|";
                break;
        }
        return PFUtilities.SPLIT_RECORD + delim + id + delim + key + delim + startDate +
                delim + firstFactor + delim + secondFactor + "\n";
    }
}
