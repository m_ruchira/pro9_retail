package com.isi.csvr.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.TransactionDialogInterface;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWComboItem;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 9, 2008
 * Time: 4:19:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class PFImportDataStore {

    public static int VALIDATION_LOAD_COMPLETE = 0;
    public static int VALIDATION_NEUMARIC_COMPLETE = 1;
    public static int VALIDATION_SYMBOL_COMPLETE = 2;
    public static int VALIDATION_CUMILATIVE_COMPLETE = 3;
    private static PFImportDataStore selfRef;
    private Hashtable<Integer, StringTableRowObj> stringObjList;
    private Hashtable<Integer, StringTableRowObj> filteredstringObjList;
    private Hashtable<Long, StringTableRowObj> tempfilteredstringObjList;
    private ArrayList<PriliminaryTxnObject> neumericValidated;
    private ArrayList<PriliminaryTxnObject> symbolValidated;
    private Hashtable<String, ArrayList<PriliminaryTxnObject>> symbolWiseTable;
    private Hashtable<String, ArrayList<PriliminaryTxnObject>> rejectedSymbolWiseTable;
    private ArrayList<PriliminaryTxnObject> cumilativeSymbolWiseList;
    private ArrayList symbolSpecificBuys;
    private Hashtable validatedObjList;
    private ArrayList tableHeaderList;
    private ArrayList orderedTableHeaderList;
    private PortfolioImportedDataUI impDataUiRef;
    private HeaderMapper mapper;
    private boolean isReadingCSV = true;
    private boolean isNeumaricVal;
    private boolean isTransVal;
    private long selPfId = 0l;
    private int completedPhase = 0;
    private int startRow = 0;
    private long prevTimeStamp = 0l;


    private PFImportDataStore() {
        stringObjList = new Hashtable();
        validatedObjList = new Hashtable();
        neumericValidated = new ArrayList<PriliminaryTxnObject>();
        filteredstringObjList = new Hashtable<Integer, StringTableRowObj>();
        tempfilteredstringObjList = new Hashtable<Long, StringTableRowObj>();
        symbolValidated = new ArrayList<PriliminaryTxnObject>();
        tableHeaderList = new ArrayList();
        orderedTableHeaderList = new ArrayList();
        symbolWiseTable = new Hashtable<String, ArrayList<PriliminaryTxnObject>>();
        cumilativeSymbolWiseList = new ArrayList<PriliminaryTxnObject>();
        rejectedSymbolWiseTable = new Hashtable<String, ArrayList<PriliminaryTxnObject>>();
        symbolSpecificBuys = new ArrayList();
        mapper = new HeaderMapper();
    }

    public static PFImportDataStore getSharedInstance() {
        if (selfRef == null) {
            selfRef = new PFImportDataStore();
        }
        return selfRef;

    }

    public static void validateSymbol(String key, String callerID, String exchange, Object caller, long timeStamp) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            //String requestID = callerID + ":" + System.currentTimeMillis();
            String requestID = callerID + ":" + timeStamp;

            DataStore.getSharedInstance().checkSymbolAvailability(key, requestID, exchange, 0);
//            SendQFactory.addValidateRequest(key, requestID, exchange, 0);
            // SendQFactory.addValidateRequest(key, requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, caller);
            //return true;
        } else {
//            if ((DataStore.getSharedInstance().isValidSymbol(key))) {
            String symbol = SymbolMaster.getExchangeForSymbol(key, false);
            if ((symbol != null)) {
                ((TransactionDialogInterface) caller).setSymbol(symbol);
            } else {
                ((TransactionDialogInterface) caller).setSymbol(null);
                new ShowMessage(false, Language.getString("MSG_INVLID_SYMBOL"), "E");
            }
            symbol = null;
        }
    }

    public static void validateSymbol(String key, String callerID, String exchange, long timestamp) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            //String requestID = callerID + ":" + System.currentTimeMillis();
            String requestID = callerID + ":" + timestamp;
            DataStore.getSharedInstance().checkSymbolAvailability(key, requestID, exchange, 0);
//            SendQFactory.addValidateRequest(key, requestID, exchange, 0);
            // SendQFactory.addValidateRequest(key, requestID, null, 0);
            // SymbolsRegistry.getSharedInstance().rememberRequest(requestID, caller);
            //return true;
        }
    }

    public static void validateSymbolNoExchange(String key, String callerID, String exchange, Object caller, long timeStamp) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            //String requestID = callerID + ":" + System.currentTimeMillis();
            String requestID = callerID + ":" + timeStamp + ":" + exchange;
            //   SendQFactory.addValidateRequest(key, requestID, exchange, 0);
            SendQFactory.addValidateRequest(key, requestID, "", 0);
            // SendQFactory.addValidateRequest(key, requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, caller);
            //return true;
        } else {
//            if ((DataStore.getSharedInstance().isValidSymbol(key))) {
            String symbol = SymbolMaster.getExchangeForSymbol(key, false);
            if ((symbol != null)) {
                ((TransactionDialogInterface) caller).setSymbol(symbol);
            } else {
                ((TransactionDialogInterface) caller).setSymbol(null);
                new ShowMessage(false, Language.getString("MSG_INVLID_SYMBOL"), "E");
            }
            symbol = null;
        }
    }

    public void reload() {
        stringObjList.clear();
        validatedObjList.clear();
        orderedTableHeaderList.clear();
        neumericValidated.clear();
        filteredstringObjList.clear();
        tempfilteredstringObjList.clear();
        symbolValidated.clear();
        symbolWiseTable.clear();
        cumilativeSymbolWiseList.clear();
        rejectedSymbolWiseTable.clear();
        symbolSpecificBuys.clear();

        impDataUiRef = null;
        isNeumaricVal = false;
    }

    public PortfolioImportedDataUI getImpDataUiRef() {
        return impDataUiRef;
    }

    public void setImpDataUiRef(PortfolioImportedDataUI impDataUiRef) {
        this.impDataUiRef = impDataUiRef;
    }

//    public void setStringObjList(Hashtable stringObjList) {
//        this.stringObjList = stringObjList;
//    }

    public long getSelPfId() {
        return selPfId;
    }

    public void setSelPfId(long selPfId) {
        this.selPfId = selPfId;
    }

    public Hashtable getStringObjList() {
        return stringObjList;
    }

    public Hashtable getValidatedObjList() {
        return validatedObjList;
    }

    public void setValidatedObjList(Hashtable validatedObjList) {
        this.validatedObjList = validatedObjList;
    }

    public ArrayList getTableHeaderList() {
        return tableHeaderList;
    }

    public void setTableHeaderList(ArrayList tableHeaderList) {
        this.tableHeaderList = tableHeaderList;
    }

    public ArrayList getOrderedTableHeaderList() {
        return orderedTableHeaderList;
    }

    public void setOrderedTableHeaderList(ArrayList orderedTableHeaderList) {
        this.orderedTableHeaderList = orderedTableHeaderList;
    }

    public boolean isReadingCSV() {
        return isReadingCSV;
    }

    public void setReadingCSV(boolean readingCSV) {
        isReadingCSV = readingCSV;
    }

    public boolean isNeumaricVal() {
        return isNeumaricVal;
    }

    public void setNeumaricVal(boolean neumaricVal) {
        isNeumaricVal = neumaricVal;

    }

    public boolean isTransVal() {
        return isTransVal;
    }

    public void setTransVal(boolean transVal) {
        isTransVal = transVal;
    }

    public int getCompletedPhase() {
        return completedPhase;
    }

    public void setCompletedPhase(int completedPhase) {
        this.completedPhase = completedPhase;

        goToNextPhase();
        // PortfolioImport.getSharedInstance().validationComplete(completedPhase);
        PortfolioImport.getSharedInstance().validateButtonsForPhase(completedPhase);
        PortfolioImport.getSharedInstance().setCompletedPhase(completedPhase);

    }

    public void loadValuesFromCSV(File selFile) {
        final File selfile = selFile;

        Thread thrd = new Thread(new Runnable() {
            public void run() {
                FileReader reader = null;
                List list = null;
                try {
                    reader = new FileReader(selfile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                CSVReader parser = new CSVReader(reader, ',', '"');
                try {
                    list = parser.readAll();
                } catch (Exception e) {

                }

                if (!list.isEmpty()) {
                    long id = System.currentTimeMillis();
                    for (int i = 0; i < list.size(); i++) {

                        String[] row = (String[]) list.get(i);
                        StringTableRowObj rowobj = new StringTableRowObj(row, i, id);
                        stringObjList.put(i, rowobj);
                        if (i == 0) {
                            int rowcnt = rowobj.getColCount();
                            fireColCountDetermined(rowcnt);
                        }
                        id = id + 1l;

                    }
                }

                isReadingCSV = false;
                completedPhase = VALIDATION_LOAD_COMPLETE;
                fireDataStoreLoaded();
            }
        });
        thrd.start();


    }

    public void StartNeumericalValidation(ArrayList orderedclms, String dateFormat, int startRow) {
        isNeumaricVal = true;
        neumericValidated.clear();
        symbolValidated.clear();
        this.startRow = startRow;
        PfImportNeumaricValidator validator = new PfImportNeumaricValidator(orderedclms, stringObjList, dateFormat, startRow, neumericValidated, symbolValidated);
        Thread t = new Thread(validator);
        t.start();

    }

    private void fireColCountDetermined(int count) {
        //     impDataUiRef.UpdateModelandCreateTable(count);
        PortfolioImport.getSharedInstance().fireColCountDetermined(count);

    }

    private void fireDataStoreLoaded() {
        //   impDataUiRef.setDataStoreForEditTable(stringObjList);
        PortfolioImport.getSharedInstance().fireDataStoreLoaded(stringObjList);
    }

    private void initColumnHeaderList() {


    }

    public TWComboItem[] getTWComboItemlist(String[] headerlist) {
        return mapper.getItemList(headerlist);

    }

    public void goToNextPhase() {

        if (completedPhase == VALIDATION_NEUMARIC_COMPLETE) {

            boolean invalidpresent = createFilteredList();
            int i = -1;
            if (invalidpresent) {
                PortfolioImport.getSharedInstance().hideMessageFrame();
                i = PortfolioImport.getSharedInstance().confirmSkip();
            } else {
                PortfolioImport.getSharedInstance().hideMessageFrame();
                PortfolioImport.getSharedInstance().symbolValidateStarted(filteredstringObjList, invalidpresent);
            }

            if (i == JOptionPane.OK_OPTION) {
                PortfolioImport.getSharedInstance().symbolValidateStarted(filteredstringObjList, invalidpresent);
            } else if (i == JOptionPane.CANCEL_OPTION) {
                PortfolioImport.getSharedInstance().validateButtonsForPhase(completedPhase);
            }
        } else if (completedPhase == VALIDATION_SYMBOL_COMPLETE) {
            sortSymbolValidated();
            if (neumericValidated.size() != symbolValidated.size() && symbolValidated.size() > 0) {
                int i = -1;
                PortfolioImport.getSharedInstance().hideMessageFrame();
                i = PortfolioImport.getSharedInstance().confirmSkip();
                if (i == JOptionPane.OK_OPTION) {

                    PortfolioImport.getSharedInstance().LoadLogicValidatorUI(symbolValidated);
                } else {
                    PortfolioImport.getSharedInstance().hideMessageFrame();
                    PortfolioImport.getSharedInstance().validateButtonsForPhase(completedPhase);
                }

            } else if (symbolValidated.size() > 0) {
                PortfolioImport.getSharedInstance().LoadLogicValidatorUI(symbolValidated);

            } else if (symbolValidated.size() == 0) {
                PortfolioImport.getSharedInstance().hideMessageFrame();
                PortfolioImport.getSharedInstance().allSymInvalid();
                PortfolioImport.getSharedInstance().cancelPressed();
            }

            ////   createSymbolWiseTable();
            //   validateCumilativeAmounts();
            System.out.println("Complete");
        }

    }

    public void validateTransactions() {
        symbolWiseTable.clear();
        cumilativeSymbolWiseList.clear();
        rejectedSymbolWiseTable.clear();
        createSymbolWiseTable();
        validateCumilativeAmounts();
        PortfolioImport.getSharedInstance().transactionsValidated(cumilativeSymbolWiseList, rejectedSymbolWiseTable);

    }


    private boolean createFilteredList() {
        int count = 0;
        boolean invalidpresent = false;
        for (int i = startRow; i < stringObjList.size(); i++) {

            StringTableRowObj row = stringObjList.get(i);
            if (row.getValidStatus() == StringTableRowObj.VALID) {
                filteredstringObjList.put(count, row);
                tempfilteredstringObjList.put(row.getId(), row);

                count += 1;
            } else {
                invalidpresent = true;
            }
        }
        return invalidpresent;
    }

    public void continueValidation() {
        if (completedPhase == VALIDATION_NEUMARIC_COMPLETE) {
            isNeumaricVal = true;
            symbolValidated.clear();
            PfImportSymbolValidator validator = new PfImportSymbolValidator(tempfilteredstringObjList, neumericValidated, symbolValidated);
            Thread t = new Thread(validator);
            t.start();

        }

    }

    public boolean isNeuValidatedListEmpty() {
        return neumericValidated.isEmpty();
    }

    public boolean isSymValidatedListEmpty() {
        return symbolValidated.isEmpty();
    }

    private void sortSymbolValidated() {
        Collections.sort(symbolValidated, new RowComparator());

    }

    private void createSymbolWiseTable() {
        for (int i = 0; i < symbolValidated.size(); i++) {
            PriliminaryTxnObject txn = symbolValidated.get(i);
            String sKey = txn.getSKey();
            if (symbolWiseTable.containsKey(sKey)) {
                (symbolWiseTable.get(sKey)).add(txn);
            } else {
                ArrayList<PriliminaryTxnObject> list = new ArrayList();
                list.add(txn);
                symbolWiseTable.put(sKey, list);
            }
        }
    }

    private void validateCumilativeAmounts() {
        if (!symbolWiseTable.isEmpty()) {

            Enumeration<String> en = symbolWiseTable.keys();
            while (en.hasMoreElements()) {
                String sKey = en.nextElement();
                ArrayList list = symbolWiseTable.get(sKey);
                processSymWiseList(list, sKey);
            }
        }

    }

    private void processSymWiseList(ArrayList<PriliminaryTxnObject> list, String sKey) {
        if (!list.isEmpty()) {
            PriliminaryTxnObject cumTxn = new PriliminaryTxnObject();
            PriliminaryTxnObject opbal = new PriliminaryTxnObject();
            int opqty = PFStore.getInstance().getAvailableQuantity(selPfId, sKey);
            cumTxn.setCumilativeQty(opqty);
            cumTxn.setSKey(sKey);
            cumilativeSymbolWiseList.add(cumTxn);
            ArrayList<PriliminaryTxnObject> rejected = new ArrayList();
            if (opqty > 0) {

                opbal.setCumilativeQty(opqty);
                opbal.setSKey(sKey);
                opbal.setDate(new Date(System.currentTimeMillis()));
                opbal.setMemo(Language.getString("OPENING_BALANCE"));
                rejected.add(opbal);
            }

            rejectedSymbolWiseTable.put(sKey, rejected);
            for (int i = 0; i < list.size(); i++) {
                PriliminaryTxnObject txn = list.get(i);

                if (i == 0) {
                    cumTxn.setSymbol(txn.getSymbol());
                    opbal.setSymbol(txn.getSymbol());
                    cumTxn.setExchange(txn.getExchange());
                    opbal.setExchange(txn.getExchange());
                    cumTxn.setCurrency(txn.getCurrency());
                    opbal.setCurrency(txn.getCurrency());
                }
                int cumQty = cumTxn.getCumilativeQty();
                if (cumQty >= 0) {
                    int qty = txn.getHolding();
                    byte type = txn.getType();
                    if (type == PFUtilities.SELL) {
                        cumQty = cumQty - qty;
                    } else if (type == PFUtilities.BUY) {
                        cumQty = cumQty + qty;
                    }
                    cumTxn.setCumilativeQty(cumQty);
                    txn.setCumilativeQty(cumQty);
                    rejected.add(txn);

                } else {
                    cumTxn.setValidated(PriliminaryTxnObject.INVALIDQTY);
                    txn.setValidated(PriliminaryTxnObject.INVALIDQTY);

                    return;
                }


            }
            rejectedSymbolWiseTable.remove(sKey);
        }
    }

    private void createRejectedSymWiseTable() {
        if (!cumilativeSymbolWiseList.isEmpty()) {
            for (int i = 0; i < cumilativeSymbolWiseList.size(); i++) {
                PriliminaryTxnObject cumtxn = cumilativeSymbolWiseList.get(i);
                if (cumtxn.getValidated() != PriliminaryTxnObject.VALID) {

                }

            }
        }


    }

    public void addTransactionToPFStore() {
        long[] sel = new long[1];
        sel[0] = selPfId;
        PFStore.getInstance().createFilteredTransactionList(sel, null, PFUtilities.NONE, null, null);
        if (!symbolValidated.isEmpty()) {
            for (int j = 0; j < symbolValidated.size(); j++) {
                PriliminaryTxnObject txn = symbolValidated.get(j);

                boolean invalid = rejectedSymbolWiseTable.containsKey(txn.getSKey());
                if (!invalid) {
                    if (txn.getType() == PFUtilities.BUY) {
                        addBuyTxn(txn, j);
                    } else if (txn.getType() == PFUtilities.SELL) {
                        addSellTxn(txn, j);
                    }

                }

            }
        }
//
//        if(!cumilativeSymbolWiseList.isEmpty()){
//            for(int i=0; i<cumilativeSymbolWiseList.size();i++){
//                PriliminaryTxnObject cumtxn = cumilativeSymbolWiseList.get(i);
//                if(cumtxn.getValidated()== PriliminaryTxnObject.VALID){
//                       createNewTransactions(symbolWiseTable.get(cumtxn.getSKey()));
//                }
//
//
//            }
//        }

    }

    private void createNewTransactions(ArrayList<PriliminaryTxnObject> list) {
        if (list != null && !list.isEmpty()) {

            for (int i = 0; i < list.size(); i++) {
                PriliminaryTxnObject txn = list.get(i);
                if (txn.getType() == PFUtilities.BUY) {
                    addBuyTxn(txn, i);
                } else if (txn.getType() == PFUtilities.SELL) {
                    addSellTxn(txn, i);
                }
            }
        }

    }

    private void addBuyTxn(PriliminaryTxnObject txn, int timestampCorrection) {
        TransactRecord record = null;
        byte txnType = txn.getType();
        int quantity = txn.getHolding();
        double price = txn.getPrice();
        double amount = 0f;
        double oldAmount = 0f;
        double brokerage = txn.getBrokerage();
        PortfolioRecord pfRecord = PFStore.getInstance().getPortfolio(selPfId);
        String curr = null;
        String comID = "Manual";
        String sKey = txn.getSKey();

        try {
            curr = PortfolioInterface.getStockObject(sKey).getCurrencyCode();
        } catch (Exception e) {
            curr = txn.getCurrency();
        }
        record = new TransactRecord();
        record.adjustTimestamp(timestampCorrection);
        record.setSKey(sKey);
        record.setPfID(selPfId);

        amount = convertToSelectedCurrency(curr, ((quantity * price)));
        amount = (amount / (ExchangeStore.getSharedInstance().getExchange(txn.getExchange()).getPriceModificationFactor()));
        amount = amount + convertToSelectedCurrency(curr, brokerage);
        pfRecord.setCashBalance(pfRecord.getCashBalance() - amount);
        record.setCurrency(curr);
        record.setTxnType(txnType);
        record.setQuantity(quantity);
        record.setBrokerage(brokerage);
        record.setPrice(price);
        Long dateStamp = txn.getDate().getTime();

        record.setTxnDate(txn.getDate().getTime());

        record.setComissionID(comID);
        record.setMemo(txn.getMemo());
        PFStore.getInstance().addTransaction(record);
        long[] sel = new long[1];
        sel[0] = selPfId;
        PFStore.getInstance().createFilteredTransactionList(sel, null, PFUtilities.NONE, null, null);


    }

    private void addSellTxn(PriliminaryTxnObject txn, int timestampCorrection) {

        TransactRecord record = null;
        byte txnType = txn.getType();
        int quantity = txn.getHolding();
        double price = txn.getPrice();
        double amount = 0f;
        double oldAmount = 0f;
        double brokerage = txn.getBrokerage();
        PortfolioRecord pfRecord = PFStore.getInstance().getPortfolio(selPfId);
        String curr = null;
        String comID = "Manual";
        String sKey = txn.getSKey();

        try {
            curr = PortfolioInterface.getStockObject(sKey).getCurrencyCode();
        } catch (Exception e) {
            curr = txn.getCurrency();
        }
        record = new TransactRecord();
        record.adjustTimestamp(timestampCorrection);
        record.setSKey(sKey);
        record.setPfID(selPfId);
        loadSymbolSpecificBuys(sKey);

        amount = convertToSelectedCurrency(curr, (quantity * price - brokerage));
        amount = (amount / (ExchangeStore.getSharedInstance().getExchange(txn.getExchange()).getPriceModificationFactor()));
        pfRecord.setCashBalance(pfRecord.getCashBalance() + amount);
        ExecuteHiddenFIFOSellInWAMode(quantity, record.getId(), record);
        record.setCurrency(curr);
        record.setTxnType(txnType);
        record.setQuantity(quantity);
        record.setBrokerage(brokerage);
        record.setPrice(price);
        record.setTxnDate(txn.getDate().getTime());
        record.setComissionID(comID);
        record.setMemo(txn.getMemo());
        PFStore.getInstance().addTransaction(record);
        long[] sel = new long[1];
        sel[0] = selPfId;
        PFStore.getInstance().createFilteredTransactionList(sel, null, PFUtilities.NONE, null, null);


    }

    private double convertToSelectedCurrency(String baseCurr, double value) {
        return value * CurrencyStore.getRate(baseCurr, PFStore.getInstance().getPortfolio(selPfId).getCurrency());
    }

    private void ExecuteHiddenFIFOSellInWAMode(int qty, long trID, TransactRecord buyRecord) {

        if (symbolSpecificBuys != null && !symbolSpecificBuys.isEmpty()) {

            int size = symbolSpecificBuys.size();
            int i = 0;
            while (qty > 0) {

                TransactRecord tr = (TransactRecord) symbolSpecificBuys.get(i);
                int availableqty = tr.getRemainingHolding();

                if (availableqty >= qty) {

                    tr.sellQuantity(trID, qty);
                    buyRecord.putBuyQty(tr.getId(), qty);
                    qty = 0;
                    break;
                } else if (availableqty > 0 && availableqty < qty) {

                    tr.sellQuantity(trID, availableqty);
                    buyRecord.putBuyQty(tr.getId(), qty);
                    qty = qty - availableqty;
                    i = i + 1;

                } else {

                    i = i + 1;
                }

                if (i == size) {
                    break;
                }
            }
        }
    }

    private boolean loadSymbolSpecificBuys(String sKey) {
        if (sKey == null || sKey.isEmpty()) {
            return false;
        }
        ArrayList transactionRecords = PFStore.getInstance().getFilteredTransactionList();
        TransactRecord trRec = null;
        symbolSpecificBuys.clear();
        for (int i = 0; i < transactionRecords.size(); i++) {

            trRec = (TransactRecord) transactionRecords.get(i);
            if (trRec.getSKey() != null && trRec.getSKey().equals(sKey) && trRec.getTxnType() == PFUtilities.BUY) {
                if (trRec.getRemainingHolding() > 0) {
                    symbolSpecificBuys.add(trRec);
                }

            }
        }
        if (!symbolSpecificBuys.isEmpty()) {
            return true;
        } else {
            return false;
        }


    }

    public void adjustStringObjectsForSingleExchange(String exchangeID) {
        if (stringObjList != null && !stringObjList.isEmpty()) {
            for (int i = 0; i < stringObjList.size(); i++) {
                StringTableRowObj obj = stringObjList.get(i);
                obj.addExchangeToList(exchangeID);
            }


        }

    }

    private class RowComparator implements Comparator {


        public int compare(Object o1, Object o2) {
            PriliminaryTxnObject txn1 = (PriliminaryTxnObject) o1;
            PriliminaryTxnObject txn2 = (PriliminaryTxnObject) o2;
            if (txn1.getRowId() > txn2.getRowId()) {
                return 1;
            } else {
                return 0;
            }
        }
    }


}
