package com.isi.csvr.portfolio;

import com.isi.csvr.*;
import com.isi.csvr.commission.CommissionConfig;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.CurrencyListener;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.CommonPopup;
import com.isi.csvr.table.MultiSpanCellComboTableUI;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.wicalculator.WhatiFCalculatorPortfolioFrame;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class PortfolioWindow
        extends InternalFrame
        implements ItemListener, PopupMenuListener, //ChangeListener,
        ActionListener, MouseListener, Themeable, TWDesktopInterface,
        InternalFrameListener, WindowWrapper, CurrencyListener, ApplicationListener, TWTabbedPaleListener {  // WindowWrapper,

    public static final int VALUATION = 1;
    public static final int TRANS_HISTORY = 2;
    public static final int VALUATION_ALL = 3;

    public static final String VALUATION_PANEL = "VALUATION";
    public static final String HISTORY_PANEL = "HISTORY";
    public static final String VALUATION_ALL_PANEL = "ALL";

//    private static DecimalFormat oPriceFormat   = new DecimalFormat(" ###,##0.00 ");
//    private SimpleDateFormat dateFormatter      = new SimpleDateFormat("yyyy/MM/dd");
    private static JLabel lblCashBal = new JLabel();
    private final byte VALUATION_TYPE = 0;
    private final byte TRANSACTION_TYPE = 1;
    TWMenu mnuDecimalPlaces;
    TWRadioButtonMenuItem[] mnuNoDecimal = new TWRadioButtonMenuItem[2];
    TWRadioButtonMenuItem[] mnuDefaultDecimal = new TWRadioButtonMenuItem[2];
    TWRadioButtonMenuItem[] mnuOneDecimal = new TWRadioButtonMenuItem[2];
    TWRadioButtonMenuItem[] mnuTwoDecimal = new TWRadioButtonMenuItem[2];
    TWRadioButtonMenuItem[] mnuThrDecimal = new TWRadioButtonMenuItem[2];
    TWRadioButtonMenuItem[] mnuFouDecimal = new TWRadioButtonMenuItem[2];
    TWMenuItem whatifcal;
    TWMenuItem customerAvgCost;
    JLabel descripLable;
    WhatiFCalculatorPortfolioFrame wiframe;
    TransactionDialog dialog;
    private int windowType;
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private Object[] currencyIDs;
    private JPanel toolBarPanel;
    private JPanel tablePanel;
    private JPanel allTablePanel;
    private JPanel valuationPanel;
    private JPanel txHistoryPanel;
    private JPanel statusPanel;
    private JPanel middleContainerPanel;
    private FilterPanel filterPanel;
    private TWMenuBar g_mnuMain = new TWMenuBar();
    private JMenu g_mnuFile = new JMenu();
    private JMenu g_mnuView = new JMenu();
    private JMenu g_mnuManage = new JMenu();
    private TWMenu g_mnuAnalysis = new TWMenu();
    private JMenu g_mnuTools = new JMenu();
    private JMenu g_mnuHelp = new JMenu();
    // File menu contents
    private TWMenuItem g_mnuCreateNewPF;
    private TWMenuItem g_mnuPrintPF;
    // Manage menu contents
    private TWMenuItem g_mnuEditPF;
    private TWMenuItem g_mnuDeletePF;
    private TWMenuItem g_mnuBackUp;
    private TWMenuItem g_mnuRestoreBackUp;
    private TWMenuItem g_mnuApplySplit;
    private TWMenuItem g_mnuCommission;
    private TWMenuItem g_mnuCommissionList;
    // View menu contents
    private TWCheckBoxMenuItem g_mnuValuation;
    private TWCheckBoxMenuItem g_mnuTxHistory;
    private TWCheckBoxMenuItem g_mnuShowZeroHoldings;
    private ButtonGroup btnGroup;
    private TWMenuItem g_mnuReports;
    // Tools menu contents
    private TWMenuItem g_mnuAddTrans;
    private TWMenuItem g_mnuExportTxHistory;
    private TWMenuItem g_mnuImportTxHistory;
    private TWMenuItem mnuCommission;
    private TWMenuItem g_mnuHelpContent;
    private ButtonGroup btnGroupCurrency;
    private JPopupMenu titleBarMenu;
    private JPopupMenu headerPopup;
    private JPopupMenu currencyPopup;
    private TWMenuItem fontMenu;
    private TWMenuItem hideShowTitleMenu;
    private TWMenuItem editCurPF;
    private TWMenuItem delCurPF;
    private TWMenuItem filterCurTransactions;
    private TWMenuItem whatifCalculator;
    private JButton btnCreatePF;
    private JButton btnAnalysis;
    private JButton btnValuationView;
    private JButton btnHistoryView;
    private JButton btnAddTransaction;
    private JButton btnApplyFilter;
    private JButton btnDisableFilter;
    private JButton btnPortfolioReview;
    private JToggleButton toggleIncludeCash; // = new JToggleButton();
    private JLabel lblAvailablePFs = new JLabel();
    private JLabel btnAvailablePFs = new JLabel();
    private JPanel pnlAvailablePFs = new JPanel(new FlowLayout(FlowLayout.TRAILING, 1, 1));
    private JLabel lblSelectedCurrencyID = new JLabel();
    private JLabel lblCurrencyIDs = new JLabel();
    private JLabel btnCurrencyIDs = new JLabel();
    private JPanel pnlCurrencyIDs = new JPanel(new FlowLayout(FlowLayout.TRAILING, 1, 1));
    private JLabel lblCashBalDesc = new JLabel();
    private JLabel lblFilterDesc = new JLabel();
    private JLabel lblApplyFilter = new JLabel();
    private PortfolioWindow selfRef;
    private ValuationModel valuationModel;
    private TransHistoryModel transHistoryModel;
    private VAluationModelAll valuationModelAll;
    private Table valuationTable;
    private Table transHistoryTable;
    private Table valuationTableAll;
    private PFStore dataStore;
    private ViewSetting oSettings;
    private BackUpDaemon backupDaemon;
    private Thread backupDaemonThread;
    private InitialPromptWindow initWindow;
    private long[] selectedPFIDs;
    private ArrayList portfolioIDS;
    private ArrayList allTableList;
    private boolean isFilterApplied;
    private String selectedSKey;
    private byte selectedType;
    private Date selectedStartDate;
    private Date selectedEndDate;
    private int selectedPanel;
    private boolean isRegisteredSymbols;
    private String[] oldSymbolList;
    private byte decimalPlaces = Constants.ONE_DECIMAL_PLACES;
    private TWCheckBoxMenuItem g_mnuValuationPrice_last;
    private TWCheckBoxMenuItem g_mnuValuationPrice_VWAP;
    private TWCheckBoxMenuItem sellingMtdFIFO;
    private TWCheckBoxMenuItem sellingMtdWA;
    private ButtonGroup btnGroup_valuationPrice;
    private TWMenu ValuationPrice;
    private TWMenu sellingMethod;
    private TransactionDialog editDialog;
    private CommissionListWindow comissionList;
    private CommissionWindow comissionWindow;
    // private boolean FIFOSelected=false;
    private TWTabbedPane tabbedPane;
    private boolean allPannelSelected;
    private CurrencySelection tableCurrencyLable;
    private boolean isRestoreInProgress = false;

    public PortfolioWindow(ViewSetting oSettings) {
        isRegisteredSymbols = true;
//        isFirstTime = true;
        selectedPanel = VALUATION;
        this.oSettings = oSettings;
        dataStore = PFStore.getInstance();
        backupDaemon = new BackUpDaemon();
        backupDaemonThread = new Thread(backupDaemon, "PortfolioWindow");
        backupDaemonThread.start();

        g_mnuFile.setOpaque(false);
        g_mnuView.setOpaque(false);
        g_mnuManage.setOpaque(false);
        g_mnuAnalysis.setOpaque(false);
        g_mnuTools.setOpaque(false);
        g_mnuHelp.setOpaque(false);

//        createFilteredTransactionList(null, PFRecord.NONE, null, null);
//        this.createValuationList();
//        Enumeration ids = CurrencyStore.getSharedInstance().getCurrencyIDs();
//        ArrayList idlist = new ArrayList();
//        while (ids.hasMoreElements()) {
//            idlist.add(ids.nextElement());
//        }
        currencyIDs = new String[0];
//        currencyIDs = idlist.toArray(currencyIDs);
        createLayout();
        setTextLabels();
        lblSelectedCurrencyID.setText(dataStore.getBaseCurrency());
        tableCurrencyLable.setText("(" + dataStore.getBaseCurrency() + ")");
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        loadDefaultPortfolio();
        CurrencyStore.getSharedInstance().addCurrencyListener(this);
        displayCashBalance();
        setDescriptionLable();
        PortfolioImport.getSharedInstance().setPfWindow(this);

//        reregisterAllSymbols();
//        setTitle(Language.getString("PORTFOLIO"));
    }

    /*   protected final JRootPane createRootPane() {
      return new DetachableRootPane();
    }*/

    public static double convertToSelectedCurrency(String sCurrency, double value) {
        return value * CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency());

    }

    private void createLayout() {
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
        this.setTitle(Language.getString("PORTFOLIO_SIMULATOR")); // "Portfolio Manager");
        this.getContentPane().setLayout(new BorderLayout());
        selfRef = this;
        createTitlebarManu();
        this.setJMenuBar(createManinMenu());
        createToolbar();
        createHistoryTable();
        createValuationTable();
        createVAluationAllTable();
        setDetachable(true);

        JPanel northBasePanel = new JPanel();
        northBasePanel.setLayout(new BoxLayout(northBasePanel, BoxLayout.LINE_AXIS)); //new GridLayout(2, 1));

        // Create available Portfolio List
        pnlAvailablePFs.setPreferredSize(new Dimension(90, 24));
        btnAvailablePFs.setPreferredSize(new Dimension(10, 22));
        btnAvailablePFs.setIcon(new ImageIcon("images/Common/graph_down_pop.gif"));
        btnAvailablePFs.setVisible(false);

        btnAvailablePFs.addMouseListener(this);
        lblAvailablePFs.addMouseListener(this);
        lblAvailablePFs.setVisible(false);

        pnlAvailablePFs.add(lblAvailablePFs);
        pnlAvailablePFs.add(btnAvailablePFs);

        // Create Currency List
        pnlCurrencyIDs.setPreferredSize(new Dimension(90, 24));
        btnCurrencyIDs.setPreferredSize(new Dimension(10, 22));
        btnCurrencyIDs.setIcon(new ImageIcon("images/Common/graph_down_pop.gif"));

        btnCurrencyIDs.addMouseListener(this);
        lblCurrencyIDs.addMouseListener(this);

        pnlCurrencyIDs.add(lblCurrencyIDs);
        pnlCurrencyIDs.add(btnCurrencyIDs);
        tableCurrencyLable = new CurrencySelection();


        tableCurrencyLable.addMouseListener(this);

        lblSelectedCurrencyID.setPreferredSize(new Dimension(80, 20));
        tableCurrencyLable.setPreferredSize(new Dimension(200, 20));
//        currencyList = new JComboBox();
//        currencyList.setBorder(BorderFactory.createEmptyBorder());

//        lblCashBal.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        lblCashBal.setFont(Theme.getDefaultFont(Font.BOLD, 12));
        lblCashBal.setHorizontalAlignment(JLabel.TRAILING);
        lblCashBal.setPreferredSize(new Dimension(100, 20));

        northBasePanel.add(toolBarPanel);
//        northBasePanel.add(toggleIncludeCash);
        northBasePanel.add(Box.createHorizontalGlue());
        northBasePanel.add(pnlAvailablePFs);
        northBasePanel.add(Box.createHorizontalStrut(5));
        northBasePanel.add(pnlCurrencyIDs);
        northBasePanel.add(Box.createHorizontalStrut(2));
        northBasePanel.add(lblSelectedCurrencyID);
        northBasePanel.add(Box.createHorizontalStrut(2));
        northBasePanel.add(lblCashBalDesc);
        northBasePanel.add(lblCashBal);
        northBasePanel.add(Box.createHorizontalStrut(1));
//        northBasePanel.add(northPanel);

//        tabbedPane = new JTabbedPane();
//        tabbedPane.addChangeListener(this);
        tablePanel = new JPanel(new CardLayout(1, 1));
        tabbedPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Simulated, "dt");
        middleContainerPanel = new JPanel(new BorderLayout());

        allTablePanel = new JPanel(new BorderLayout());
        allTablePanel.add(valuationTableAll, BorderLayout.CENTER);

//        tablePanel.setBorder(BorderFactory.createEtchedBorder());

        valuationPanel = new JPanel(new BorderLayout());
//        valuationPanel.setBorder(BorderFactory.createEtchedBorder(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"), Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR")));
        valuationPanel.add(valuationTable, BorderLayout.CENTER);

        txHistoryPanel = new JPanel(new BorderLayout());
//        txHistoryPanel.setBorder(BorderFactory.createEtchedBorder(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"), Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR")));
        transHistoryTable.getTable().addMouseListener(this);

        statusPanel = new JPanel();
        statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.LINE_AXIS));

        lblApplyFilter.addMouseListener(this);

        statusPanel.add(lblFilterDesc);
        statusPanel.add(Box.createHorizontalGlue());
//        statusPanel.add(lblApplyFilter);
        statusPanel.add(Box.createHorizontalStrut(1));

        txHistoryPanel.add(transHistoryTable, BorderLayout.CENTER);
        txHistoryPanel.add(statusPanel, BorderLayout.SOUTH);
        createPortfolioTabs();
        tablePanel.add(VALUATION_PANEL, valuationPanel);
        tablePanel.add(HISTORY_PANEL, txHistoryPanel);
        tablePanel.add(VALUATION_ALL_PANEL, allTablePanel);
        // tabbedPane.addTab(VALUATION_PANEL, valuationPanel);
        tabbedPane.showTab(0);
        tabbedPane.addTabPanelListener(this);
        middleContainerPanel.add(tabbedPane, BorderLayout.NORTH);
        middleContainerPanel.add(tablePanel, BorderLayout.CENTER);

        this.getContentPane().add(northBasePanel, BorderLayout.NORTH);
        //   this.getContentPane().add(tablePanel, BorderLayout.CENTER);
        this.getContentPane().add(middleContainerPanel, BorderLayout.CENTER);
//        this.getContentPane().add(statusPanel, BorderLayout.SOUTH);

        createDecimalMenu(valuationTable.getPopup(), VALUATION_TYPE);
        createDecimalMenu(transHistoryTable.getPopup(), TRANSACTION_TYPE);
        creatCalculatorMenu(valuationTable.getPopup());
        setDecimalPlaces(decimalPlaces);
        //createCustomerAverageCostMenu(valuationTable.getPopup());
        //    creatCalculatorMenu(transHistoryTable.getPopup());
        // k
//        getCustomMenuItem(valuationTable.getPopup());

        getCustomMenuItem(transHistoryTable.getPopup());
        //transHistoryTable.getPopup().enableUnsort(false);
        transHistoryTable.getPopup().enableUnsort(false);

        transHistoryTable.getPopup().addPopupMenuListener(this);
        valuationTable.getPopup().addPopupMenuListener(this);
        Application.getInstance().addApplicationListener(this);
//        btnCreatePF.setEnabled(false);
        this.addInternalFrameListener(this);
    }

    private void createPortfolioTabs() {

        ArrayList portfolios = PFStore.getInstance().getPortfolioList();
        if (portfolios != null && !portfolios.isEmpty()) {
            String pfName = "";
            int size = portfolios.size();
            portfolioIDS = new ArrayList(portfolios.size());
            for (int i = 0; i < portfolios.size(); i++) {

                PortfolioRecord record = (PortfolioRecord) portfolios.get(i);
                portfolioIDS.add(i, record.getPfID());
                if (Language.isLTR())
                    pfName = record.getName() + " (" + record.getCurrency() + ")";
                else
                    pfName = " (" + record.getCurrency() + ")" + record.getName();

                tabbedPane.addTab(i, pfName, tablePanel);
            }
            tabbedPane.addTab(size, Language.getString("ALL"), allTablePanel);
        }
    }

    private void setTextLabels() {
        lblAvailablePFs.setText(Language.getString("PORTFOLIO_LIST"));
        lblCurrencyIDs.setText(Language.getString("CURRENCY"));
        lblCashBalDesc.setText(Language.getString("CASH_BALANCE"));
        lblCashBal.setText("0.00");
        lblApplyFilter.setText(Language.getString("APPLY_FILTER"));
        lblFilterDesc.setText("");

        g_mnuManage.setText(Language.getString("MANAGE"));
        g_mnuCreateNewPF.setText(Language.getString("CREATE_PORTFOLIO"));
//        g_mnuOpen.setText(Language.getString("PF_OPEN_PORTFOLIO"));
        g_mnuPrintPF.setText(Language.getString("PRINT"));
        g_mnuEditPF.setText(Language.getString("EDIT_PORTFOLIO"));
        g_mnuDeletePF.setText(Language.getString("DELETE_PORTFOLIO"));
        g_mnuApplySplit.setText(Language.getString("ADJUST_SPLIT_FACTOR"));
        g_mnuCommission.setText(Language.getString("COMMISSION_WINDOW"));
        g_mnuCommissionList.setText(Language.getString("COMMISSION_LIST_TITLE"));
        g_mnuView.setText(Language.getString("VIEW"));
        g_mnuValuation.setText(Language.getString("VALUATION_VIEW"));
        g_mnuShowZeroHoldings.setText(Language.getString("SHOW_ZERO_HOLDINGS"));
        g_mnuTxHistory.setText(Language.getString("TXN_HISTORY_VIEW"));
        g_mnuReports.setText(Language.getString("POERFOLIO_REVIEW"));
        g_mnuAnalysis.setText(Language.getString("ANALYSIS"));
        g_mnuFile.setText(Language.getString("FILE"));
        g_mnuTools.setText(Language.getString("TOOLS"));
        g_mnuHelp.setText(Language.getString("HELP"));
        g_mnuHelpContent.setText(Language.getString("HELP_CONTENTS"));
        g_mnuAddTrans.setText(Language.getString("ADD_TXN"));
        g_mnuExportTxHistory.setText(Language.getString("EXPORT_TX_HISTORY"));
        g_mnuImportTxHistory.setText(Language.getString("IMPORT_TX_HISTORY"));
        g_mnuValuationPrice_last.setText(Language.getString("LAST_PRICE"));
        g_mnuValuationPrice_VWAP.setText(Language.getString("VWAP"));
        ValuationPrice.setText(Language.getString("VALUATION_METHOD"));
        sellingMethod.setText(Language.getString("SELLING_METHOD"));
        sellingMtdFIFO.setText(Language.getString("METHOD_FIFO"));
        sellingMtdWA.setText(Language.getString("METHOD_WA"));

        btnCreatePF.setToolTipText(Language.getString("CREATE_PORTFOLIO"));
        btnAnalysis.setToolTipText(Language.getString("ANALYSIS"));
        btnValuationView.setToolTipText(Language.getString("VALUATION_VIEW"));
        btnHistoryView.setToolTipText(Language.getString("TXN_HISTORY_VIEW"));
        btnAddTransaction.setToolTipText(Language.getString("ADD_TXN"));
        btnApplyFilter.setToolTipText(Language.getString("APPLY_FILTER"));
        btnDisableFilter.setToolTipText(Language.getString("REMOVE_FILTER"));
        btnPortfolioReview.setToolTipText(Language.getString("PORTFOLIO_REPORT_TITLE"));
        editCurPF.setText(Language.getString("EDIT_TRANSACTION"));
        delCurPF.setText(Language.getString("DELETE_TRANSACTION"));
        filterCurTransactions.setText(Language.getString("FILTER_TXN_HISTORY"));
        g_mnuBackUp.setText(Language.getString("BACKUP_TRANSACTIONS"));
        g_mnuRestoreBackUp.setText(Language.getString("RESTORE_TRANSACTIONS"));
        toggleIncludeCash.setToolTipText(Language.getString("INCLUDE_CASH_BALANCE"));
    }

    public void displayCashBalance() {
        lblCashBal.setText(PFUtilities.getFormattedPrice(getCashBalanceForSelectedPFs()));
    }

    public void displayCashBalnceforAll() {
        lblCashBal.setText(PFUtilities.getFormattedPrice(getCashBalanceForAllPortfolios()));
    }

    public double getCashBalanceForSelectedPFs() {
        double cashBalance = 0d;
        PortfolioRecord record = null;
        try {
            for (int i = 0; i < selectedPFIDs.length; i++) {
                record = dataStore.getPortfolio(selectedPFIDs[i]);
                if (record != null) {
                    cashBalance += convertToSelectedCurrency(record.getCurrency(), record.getCashBalance());
                }
                record = null;
            }
        } catch (Exception ex) {
            cashBalance = 0d;
        }
        return cashBalance;
    }

    public double getCashBalanceForAllPortfolios() {
        double cashBalance = 0d;
        PortfolioRecord record = null;
        ArrayList<PortfolioRecord> list = dataStore.getPortfolioList();
        try {
            for (int i = 0; i < list.size(); i++) {
                record = list.get(i);
                if (record != null) {
                    cashBalance += convertToSelectedCurrency(record.getCurrency(), record.getCashBalance());
                }
                record = null;
            }
        } catch (Exception ex) {
            cashBalance = 0d;
        }
        return cashBalance;


    }

    public Double getCashBlanceForPortfolio(long pfId) {
        double cashBalance = 0d;
        try {
            PortfolioRecord record = dataStore.getPortfolio(pfId);
            cashBalance = convertToSelectedCurrency(record.getCurrency(), record.getCashBalance());
        } catch (Exception e) {
            cashBalance = 0d;
        }
        return cashBalance;

    }

    /**
     * Create the main menu
     */
    public JMenuBar createManinMenu() {
        g_mnuMain.setLayout(new BoxLayout(g_mnuMain, BoxLayout.LINE_AXIS));

        // FILE MENU CONTENTS
        g_mnuCreateNewPF = new TWMenuItem("Create Portfolio");
        g_mnuCreateNewPF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createPortfolio();
            }
        });
        g_mnuFile.add(g_mnuCreateNewPF);

//        g_mnuOpen = new TWMenuItem("Open Portfolio");
//        g_mnuOpen.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                openPortfolio();
//            }
//        });
//        g_mnuFile.add(g_mnuOpen);

        g_mnuPrintPF = new TWMenuItem("Print Portfolio");
        g_mnuPrintPF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                printPortfolio();
            }
        });
        g_mnuFile.add(g_mnuPrintPF);

        // MANAGE MENU CONTENTS
        g_mnuEditPF = new TWMenuItem("Edit Portfolio"); //Language.getString("MENU_NEW"));
        g_mnuEditPF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                editPortfolio();
            }
        });
        g_mnuManage.add(g_mnuEditPF);

        g_mnuDeletePF = new TWMenuItem("Delete Portfolio"); //Language.getString("MENU_NEW"));
        g_mnuDeletePF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deletePortfolio();
            }
        });
        g_mnuManage.add(g_mnuDeletePF);

        g_mnuCommission = new TWMenuItem();
        g_mnuCommission.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (comissionWindow == null) {
                    comissionWindow = new CommissionWindow();
                    comissionWindow.clearFields();
                    comissionWindow.setVisible(true);
                } else {
                    comissionWindow.clearFields();
                    comissionWindow.setVisible(true);
                }
            }
        });
        g_mnuManage.add(g_mnuCommission);

        g_mnuCommissionList = new TWMenuItem();
        g_mnuCommissionList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (comissionList == null) {
                    comissionList = new CommissionListWindow();
                    comissionList.setVisible(true);
                } else {
                    comissionList.setVisible(true);

                }
            }
        });
        g_mnuManage.add(g_mnuCommissionList);

        g_mnuManage.add(new TWSeparator());

        g_mnuBackUp = new TWMenuItem("Back up Transactions");
        g_mnuBackUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                displayBackUpWizard();
            }
        });
        g_mnuManage.add(g_mnuBackUp);

        g_mnuRestoreBackUp = new TWMenuItem("Restore Transactions");
        g_mnuRestoreBackUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                restoreBackedUpTransactions();
            }
        });
        g_mnuManage.add(g_mnuRestoreBackUp);

        g_mnuApplySplit = new TWMenuItem("Adjust Split Factor");
        g_mnuApplySplit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SplitFactorWindow.getsharedInstance().setVisible(true);
                updateValuationLists();

            }
        });
        g_mnuManage.add(g_mnuApplySplit);

        // VIEW MENU CONTENTS
        btnGroup = new ButtonGroup();
        g_mnuValuation = new TWCheckBoxMenuItem("Valuation");
        g_mnuValuation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (g_mnuValuation.isSelected())
                    showSelectedPanel(VALUATION_PANEL);
            }
        });
        g_mnuValuation.setSelected(true);
        g_mnuView.add(g_mnuValuation);
        btnGroup.add(g_mnuValuation);

        g_mnuTxHistory = new TWCheckBoxMenuItem("Tx History");
        g_mnuTxHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (g_mnuTxHistory.isSelected())
                    showSelectedPanel(HISTORY_PANEL);
            }
        });
        g_mnuView.add(g_mnuTxHistory);
        btnGroup.add(g_mnuTxHistory);

        //g_mnuView.addSeparator();
        g_mnuShowZeroHoldings = new TWCheckBoxMenuItem("Valuation");
        g_mnuShowZeroHoldings.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (g_mnuShowZeroHoldings.isSelected()) {
                    PFStore.setShowZeroHoldings(true);
                } else {
                    PFStore.setShowZeroHoldings(false);
                }
                createValuationList();
                updateValuationLists();
            }
        });
        g_mnuShowZeroHoldings.setSelected(false);
        g_mnuView.add(g_mnuShowZeroHoldings);

        g_mnuReports = new TWMenuItem("Reports");
        final PortfolioWindow pfWindow = this;
        g_mnuReports.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PFReportFrame.showReport(pfWindow);
            }
        });
        g_mnuView.add(g_mnuReports);

        // TOOLS MENU CONTENTS
        g_mnuAddTrans = new TWMenuItem("Add Tx");
        g_mnuAddTrans.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createTransaction();
            }
        });
        g_mnuTools.add(g_mnuAddTrans);
        /*whatifCalculator = new TWMenuItem(Language.getString("FIX_AVERAGE_COST"));
        whatifCalculator.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createWhatiFCalc("");
            }
        });
        g_mnuTools.add(whatifCalculator);*/


        ValuationPrice = new TWMenu();


        btnGroup_valuationPrice = new ButtonGroup();
        g_mnuValuationPrice_last = new TWCheckBoxMenuItem();
        g_mnuValuationPrice_last.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (g_mnuValuationPrice_last.isSelected()) {
                    dataStore.setVWAPMode(false);
                    //ValuationModel.VWAPMode=false;
                    setDescriptionLable();
                }

            }
        });
        btnGroup_valuationPrice.add(g_mnuValuationPrice_last);
        //g_mnuTools.add(g_mnuValuationPrice_last);

        //g_mnuValuationPrice_last.setSelected(dataStore.isVWAPMode());


        g_mnuValuationPrice_VWAP = new TWCheckBoxMenuItem();
        g_mnuValuationPrice_VWAP.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (g_mnuValuationPrice_VWAP.isSelected()) {
                    //ValuationModel.VWAPMode=true;
                    dataStore.setVWAPMode(true);
                    setDescriptionLable();
                }
            }
        });


        btnGroup_valuationPrice.add(g_mnuValuationPrice_VWAP);
        // g_mnuValuationPrice_VWAP.setSelected(dataStore.isVWAPMode());

        if (dataStore.isVWAPMode()) {
            g_mnuValuationPrice_VWAP.setSelected(true);
        } else {
            g_mnuValuationPrice_last.setSelected(true);
        }
        //g_mnuTools.add(g_mnuValuationPrice_VWAP);
        ValuationPrice.add(g_mnuValuationPrice_last);
        ValuationPrice.add(g_mnuValuationPrice_VWAP);
        g_mnuTools.add(ValuationPrice);

        g_mnuExportTxHistory = new TWMenuItem("Export");
        g_mnuExportTxHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exportHstory();
            }
        });
        g_mnuTools.add(g_mnuExportTxHistory);

        g_mnuImportTxHistory = new TWMenuItem("Import");
        g_mnuImportTxHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PortfolioImport.getSharedInstance().startImport();
                // PortfolioImport.getSharedInstance().createLoadTableUI();


            }
        });
        g_mnuTools.add(g_mnuImportTxHistory);

        mnuCommission = new TWMenuItem(Language.getString("EDIT_COMMISSION"));
        mnuCommission.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new CommissionConfig();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.PortfolioSimulator, "EditCommission");
            }
        });
        //  g_mnuTools.add(mnuCommission);

        sellingMethod = new TWMenu();

        sellingMtdWA = new TWCheckBoxMenuItem();
        sellingMtdWA.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                sellingMtdWA.setSelected(true);
                sellingMtdFIFO.setSelected(false);
                dataStore.setFIFOSellMode(false);
                updateValuationLists();

                //  createValuationList();

            }
        });


        sellingMtdFIFO = new TWCheckBoxMenuItem();
        sellingMtdFIFO.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sellingMtdFIFO.setSelected(true);
                sellingMtdWA.setSelected(false);
                dataStore.setFIFOSellMode(true);
                updateValuationLists();


            }
        });

        if (dataStore.isFIFOSellMode()) {
            sellingMtdFIFO.setSelected(true);
        } else {
            sellingMtdWA.setSelected(true);
        }

        sellingMethod.add(sellingMtdWA);
        sellingMethod.add(sellingMtdFIFO);

        g_mnuTools.add(sellingMethod);

        //g_mnuHelp = new JMenu();

//        g_mnuHelp.addMenuListener(new MenuListener(){
//            public void menuSelected(MenuEvent e) {
//                NativeMethods.showPortfolioSimulatorHelp();
//            }
//
//            public void menuDeselected(MenuEvent e) {
//            }
//
//            public void menuCanceled(MenuEvent e) {
//            }
//        });

        g_mnuHelpContent = new TWMenuItem(Language.getString("HELP_CONTENTS"));
        g_mnuHelpContent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_PORTFOLIO_SIMULATOR"));
            }
        });
        g_mnuHelp.add(g_mnuHelpContent);

        g_mnuMain.add(g_mnuFile);
        g_mnuMain.add(g_mnuView);
        g_mnuMain.add(g_mnuManage);
        g_mnuMain.add(g_mnuTools);
        g_mnuMain.add(g_mnuHelp);

        g_mnuMain.add(Box.createHorizontalGlue());
//        g_mnuMain.add(Box.createHorizontalGlue());

        descripLable = new JLabel("");
        descripLable.setFont(descripLable.getFont().deriveFont(Font.BOLD));
        descripLable.setForeground(Theme.getColor("MAIN_MENU_FGCOLOR"));
        g_mnuMain.add(descripLable);
        g_mnuMain.add(Box.createHorizontalStrut(5));

        g_mnuFile.getPopupMenu().addPopupMenuListener(this);
        g_mnuManage.getPopupMenu().addPopupMenuListener(this);

        return g_mnuMain;
    }

    private void setDescriptionLable() {
        String str = Language.getString("PFWINDOW_DESC_MESSAGE");
        if (PFStore.isVWAPMode()) {
            str = str.replaceFirst("\\[VALUATION_METHOD\\]", Language.getString("VWAP"));
        } else {
            str = str.replaceFirst("\\[VALUATION_METHOD\\]", Language.getString("LAST_PRICE"));
        }
        str = str.replaceFirst("\\[CURRENCY\\]", lblSelectedCurrencyID.getText());
        descripLable.setText(str);
        str = null;
    }

    private void showSelectedPanel(String panelID) {
        btnHistoryView.setEnabled(true);
        btnValuationView.setEnabled(true);
        g_mnuValuation.setEnabled(true);
        g_mnuTxHistory.setEnabled(true);
        btnPortfolioReview.setEnabled(true);
        g_mnuReports.setEnabled(true);
        g_mnuReports.setEnabled(true);
        g_mnuEditPF.setVisible(true);
        g_mnuDeletePF.setVisible(true);
        g_mnuPrintPF.setVisible(false);
        if (panelID.equals(VALUATION_PANEL)) {
            selectedPanel = VALUATION;
            g_mnuValuation.setSelected(true);
            g_mnuTxHistory.setSelected(false);
            btnApplyFilter.setEnabled(false);
            btnDisableFilter.setEnabled(false);
            toggleIncludeCash.setEnabled(true);
            g_mnuPrintPF.setVisible(true);
            valuationTable.getTable().updateUI();
        } else if (panelID.equals(HISTORY_PANEL)) {
            selectedPanel = TRANS_HISTORY;
            g_mnuValuation.setSelected(false);
            g_mnuTxHistory.setSelected(true);
            btnApplyFilter.setEnabled(true);
            btnDisableFilter.setEnabled(true);
            toggleIncludeCash.setEnabled(false);
            g_mnuPrintPF.setVisible(true);
            transHistoryTable.getTable().updateUI();
        } else if (panelID.equals(VALUATION_ALL_PANEL)) {
            //   selectedPanel = VALUATION_ALL; //todo: uncommented
            btnApplyFilter.setEnabled(false);
            btnDisableFilter.setEnabled(false);
            btnHistoryView.setEnabled(false);
            btnValuationView.setEnabled(false);
            g_mnuValuation.setEnabled(false);
            g_mnuTxHistory.setEnabled(false);
            btnPortfolioReview.setEnabled(false);
            g_mnuReports.setEnabled(false);
            g_mnuReports.setEnabled(false);
            g_mnuEditPF.setVisible(false);
            g_mnuDeletePF.setVisible(false);
            g_mnuPrintPF.setVisible(false);
            //    g_mnuManage.revalidate();
            valuationModelAll.getTable().updateUI();
        }
        CardLayout cl = (CardLayout) (tablePanel.getLayout());
        if (!(panelID.equals(HISTORY_PANEL) && allPannelSelected)) {
            cl.show(tablePanel, panelID);
        }
        cl = null;
    }


    private void loadDefaultPortfolio() {
        try {
            PortfolioRecord record = dataStore.getDefaultPortfolio();
            long[] pfIDs = {record.getPfID()};
            if (pfIDs.length == 1) {
                int index = portfolioIDS.indexOf(pfIDs[0]);
                if (index >= 0) {
                    tabbedPane.setSelectedIndex(index);
                }
            }
            selectedPFIDs = null;
            selectedPFIDs = new long[1];
            selectedPFIDs[0] = record.getPfID();
            setSelectedCuttencyForSinglePF(pfIDs);
            setSelectedPFIDs(pfIDs);
            //   setSelectedCuttencyForSinglePF(pfIDs);
            record = null;
            displayCashBalance();
        } catch (Exception ex) {

        }
    }

    public void loadNewlyAddedPortfolio(long newID, PortfolioRecord precord) {
//        if (selectedPFIDs == null) {
        selectedPFIDs = null;
        selectedPFIDs = new long[1];
        selectedPFIDs[0] = newID;
        addNewTab(precord);

        setSelectedPFIDs(selectedPFIDs);
//        } else {
//            boolean isNewElem = true;
//            for (int i = 0; i < selectedPFIDs.length; i++) {
//                if (selectedPFIDs[i] == newID) {
//                    isNewElem = false;
//                    break;
//                }
//            }
//            if (isNewElem) {
//                long[] newPFArray = new long[selectedPFIDs.length + 1];
//                for (int i = 0; i < selectedPFIDs.length; i++) {
//                    newPFArray[i] = selectedPFIDs[i];
//                }
//                newPFArray[selectedPFIDs.length] = newID;
//                setSelectedPFIDs(newPFArray);
//    //            selectedPFIDs = null;
//    //            selectedPFIDs = newPFArray;
//                newPFArray = null;
//            } else
//                setSelectedPFIDs(selectedPFIDs);
//        }

        lblSelectedCurrencyID.setText(precord.getCurrency());
        tableCurrencyLable.setText(precord.getCurrency());
        PFStore.getInstance().setBaseCurrency(precord.getCurrency());
        setDescriptionLable();
        activateCurrentSymbols();
        displayCashBalance();
    }

    private void addNewTab(PortfolioRecord record) {
        int count = tabbedPane.getTabCount();
        if (portfolioIDS == null) {
            portfolioIDS = new ArrayList();
        }

        if (record != null && !portfolioIDS.contains(record.getPfID())) {
            String pfName;
            portfolioIDS.add(record.getPfID());
            if (Language.isLTR()) {
                pfName = record.getName() + " (" + record.getCurrency() + ")";
            } else {
                pfName = " (" + record.getCurrency() + ")" + record.getName();
            }

            tabbedPane.addTab(count - 1, pfName, tablePanel);
            if (PFStore.getInstance().getPortfolioList().size() == 1 && count == 0) {

                tabbedPane.addTab(1, Language.getString("ALL"), allTablePanel);

            }
            tabbedPane.updateUI();
        } else if (portfolioIDS.contains(record.getPfID())) {
            String pfName;
            int index = portfolioIDS.indexOf(record.getPfID());
            if (Language.isLTR()) {
                pfName = record.getName() + " (" + record.getCurrency() + ")";
            } else {
                pfName = " (" + record.getCurrency() + ")" + record.getName();
            }

            tabbedPane.setTitleAt(index, pfName);

        }

    }

    public void createFilteredTransactionList(String sKey, byte type, Date startDate, Date endDate) {
        selectedSKey = sKey;
        selectedType = type;
        selectedStartDate = startDate;
        selectedEndDate = endDate;
//        System.out.println("Creating Filtered Transaction List " + sKey + " " + type);
        dataStore.createFilteredTransactionList(selectedPFIDs, sKey, type, startDate, endDate);
        transHistoryTable.getTable().updateUI();
    }

    public void createValuationList() {

        dataStore.createValuationList(selectedPFIDs, null);
        valuationModel.includeCashBalance(toggleIncludeCash.isSelected());
        valuationModelAll.includeCashBalance(toggleIncludeCash.isSelected());
        valuationTable.getTable().updateUI();
        activateCurrentSymbols();
    }

    public void createValuationAllList() {
        dataStore.createAllValuationList(toggleIncludeCash.isSelected());
        valuationModelAll.includeCashBalance(toggleIncludeCash.isSelected());
        valuationTableAll.getTable().updateUI();
    }

    public void updateValuationLists() {
        if (selectedPanel == VALUATION && (!isAllPannelSelected())) {
            createValuationList();
        } else if (selectedPanel == TRANS_HISTORY) {
            createFilteredTransactionList(null, PFUtilities.NONE, null, null);
        } else {
            createValuationAllList();
        }


    }

    public void createPortfolio() {
        if (ExchangeStore.isValidSystemFinformationType(Meta.WT_NewPortfolio) && PortfolioInterface.isValidForTransactions()) {
            CreatePortfolio createPF = new CreatePortfolio(valuationTable, this);
            createPF.showDialog();

            createFilteredTransactionList(null, PFUtilities.NONE, null, null);
            setFilterDescription(false, null, null, null, PFUtilities.NONE);
            createValuationList();
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.PortfolioSimulator, "Create");
        }
    }

    private void editPortfolio() {
        if ((selectedPFIDs == null) || (selectedPFIDs.length == 0)) {
            new ShowMessage(Language.getString("MSG_PLS_SELECT_FORTFOLIO"), "I");
        } else {
            CreatePortfolio editPF = new CreatePortfolio(valuationTable, this);
            editPF.setRecord(dataStore.getPortfolio(selectedPFIDs[0]));
            editPF.showDialog();
        }
    }

    private void deletePortfolio() {
        if ((selectedPFIDs == null) || (selectedPFIDs.length == 0)) {
            if (dataStore.getPortfolioList().size() == 0) {
                String mesg = "<html><center><p><b>" +
                        Language.getString("MSG_NO_PORTFOLIOS_DEFINED") + "</b><br>" +
                        Language.getString("MSG_PLEASE_CREATE_A_PORTFOLIO") + "</p></center></html>";
                new ShowMessage(mesg, "I");
            } else
                new ShowMessage(Language.getString("MSG_PLS_SELECT_FORTFOLIO"), "I");
        } else {
            String sMesg = "<html>" + Language.getString("MSG_DELETE_CURR_PORTFOLIO") +
                    "</b></html>";
            ShowMessage oMessage = new ShowMessage(sMesg, "W", true);
            int i = oMessage.getReturnValue();
            oMessage = null;
            sMesg = null;
            if (i == 1) {
                removeTab(selectedPFIDs[0]);
                dataStore.deletePortfolio(selectedPFIDs[0]);
                long[] tempArray = new long[selectedPFIDs.length - 1];
                for (int j = 1; j < selectedPFIDs.length; j++) {
                    tempArray[j - 1] = selectedPFIDs[j];
                }
                selectedPFIDs = null;
                selectedPFIDs = tempArray;
                createFilteredTransactionList(null, PFUtilities.NONE, null, null);
                setFilterDescription(false, null, null, null, PFUtilities.NONE);
                createValuationList();
                displayCashBalance();
                setSelectedPFIDs(selectedPFIDs);
                tabbedPane.selectTab(0);
            }
        }
    }

    private void removeTab(long pfID) {
        int index = portfolioIDS.indexOf(pfID);
        if (index >= 0) {
            tabbedPane.removeTab(index);
            tabbedPane.updateUI();
            portfolioIDS.remove(pfID);
            portfolioIDS.trimToSize();
            //  createPortfolioTabs();
        }
    }

    public void createToolbar() {
        toolBarPanel = new JPanel();
        toolBarPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 2, 2));

        btnCreatePF = new JButton();
        btnCreatePF.setPreferredSize(new Dimension(25, 25));
        btnCreatePF.setBorder(new EmptyBorder(0, 0, 0, 0));
        btnCreatePF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                createPortfolio();
            }
        });
        toolBarPanel.add(btnCreatePF);

        btnAnalysis = new JButton();
        btnAnalysis.setPreferredSize(new Dimension(25, 25));
        btnAnalysis.setBorder(new EmptyBorder(0, 0, 0, 0));
        btnAnalysis.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
//        toolBarPanel.add(btnAnalysis);

        btnValuationView = new JButton();
        btnValuationView.setPreferredSize(new Dimension(25, 25));
        btnValuationView.setBorder(new EmptyBorder(0, 0, 0, 0));
        btnValuationView.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showSelectedPanel(VALUATION_PANEL);
//                if (tabbedPane.getSelectedIndex() == 0)
//                    tabbedPane.setSelectedIndex(1);
//                else if (tabbedPane.getSelectedIndex() == 1) {
//                    tabbedPane.setSelectedIndex(0);
//                    createValuationList();
//                }
            }
        });
        toolBarPanel.add(btnValuationView);

        btnHistoryView = new JButton();
        btnHistoryView.setPreferredSize(new Dimension(25, 25));
        btnHistoryView.setBorder(new EmptyBorder(0, 0, 0, 0));
        btnHistoryView.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showSelectedPanel(HISTORY_PANEL);
//                tabbedPane.setSelectedIndex(1);
            }
        });
        toolBarPanel.add(btnHistoryView);

        btnAddTransaction = new JButton();
        btnAddTransaction.setPreferredSize(new Dimension(25, 25));
        btnAddTransaction.setBorder(new EmptyBorder(0, 0, 0, 0));
        btnAddTransaction.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                createTransaction();
            }
        });
        toolBarPanel.add(btnAddTransaction);

        btnApplyFilter = new JButton();
        btnApplyFilter.setPreferredSize(new Dimension(25, 25));
        btnApplyFilter.setBorder(new EmptyBorder(0, 0, 0, 0));
        btnApplyFilter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                applyTxHistoryFilter();
            }
        });
        toolBarPanel.add(btnApplyFilter);
        btnApplyFilter.setEnabled(false);

        btnDisableFilter = new JButton();
        btnDisableFilter.setPreferredSize(new Dimension(25, 25));
        btnDisableFilter.setBorder(new EmptyBorder(0, 0, 0, 0));
        btnDisableFilter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                disableTxHistoryFilter();
            }
        });
        toolBarPanel.add(btnDisableFilter);
        btnDisableFilter.setEnabled(false);

        toggleIncludeCash = new JToggleButton();
        toggleIncludeCash.setPreferredSize(new Dimension(25, 25));
        toggleIncludeCash.setBorder(new EmptyBorder(0, 0, 0, 0));
        toggleIncludeCash.addMouseListener(this);
        toolBarPanel.add(toggleIncludeCash);

        btnPortfolioReview = new JButton();
        btnPortfolioReview.setPreferredSize(new Dimension(25, 25));
        btnPortfolioReview.setBorder(new EmptyBorder(0, 0, 0, 0));
        final PortfolioWindow pfWindow = this;
        btnPortfolioReview.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PFReportFrame.showReport(pfWindow);
            }
        });
        toolBarPanel.add(btnPortfolioReview);

        loadButtonImages();
    }

    private void createTransaction() {
        if (PortfolioInterface.isValidForTransactions()) {
            if (dataStore.getPortfolioList().size() == 0) {
                new ShowMessage(Language.getString("MSG_NO_PORTFOLIOS_DEFINED"), "I");
            } else {
                if (dialog != null) {
                    return;
                }
                TransactionDialog txnDialog = new TransactionDialog(Client.getInstance().getFrame(), "");
                GUISettings.applyOrientation(txnDialog);
                Client.getInstance().getDesktop().add(txnDialog);
                txnDialog.setParent(selfRef);
                txnDialog.setSelectedPF(selectedPFIDs[0]);
                txnDialog.setSelectedCommision(selectedPFIDs[0]);
                Thread txnThread = new Thread(txnDialog, "PortfolioWindow");
                txnDialog.setCurrentThread(txnThread);
                txnThread.start();
                txnDialog.showDialog();
                txnDialog.setSellValuationMode(PFStore.isFIFOSellMode());
                dialog = txnDialog;
                //txnDialog = null;
            }
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.PortfolioSimulator, "AddTransaction");
        }
    }

    public void addTxnWindowClosed() {
        dialog = null;
        this.requestFocus();
    }

    private void createTitlebarManu() {
        titleBarMenu = new JPopupMenu();
        TWMenuItem hideTitlemenu = new TWMenuItem(Language.getString("HIDE_TITLEBAR"));
        hideTitlemenu.setActionCommand("TITLE_HIDE");
        hideTitlemenu.addActionListener(this);
        titleBarMenu.add(hideTitlemenu);
        addTitlebarManu();
    }

    private void createHistoryTable() {
        ViewSetting oSettings;
        try {
            // Read the default view in the view settings file
            // get the default view
//            if (Client.getInstance().getViewSettingsManager() == null)
//                System.out.println("ViewSettingsManager is NULL");
            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "2").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
//            if (sID == null)
//                oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//            else
//                oSettings.setID(store.getId());
//            oSettings.setCaptions(sNewName);
        } catch (Exception e) {
            e.printStackTrace();
//            System.out.println("Exiting at createHistoryTable()");
            return;
        }
        //String sColOrder = Settings.getItem("Gio");
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_HISTORY_COLS"));

//        if (sID == null)
//            oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//        else
//        oSettings.setID(store.getId());
        oSettings.setTableNumber(2);
        transHistoryModel = new TransHistoryModel(dataStore.getFilteredTransactionList());
        transHistoryModel.setViewSettings(oSettings);
        transHistoryTable = new Table(); // table with no popup support inherited
        //transHistoryTable.setSortingEnabled();
        transHistoryTable.setSortingEnabled();
        transHistoryTable.setWindowType(ViewSettingsManager.PORTFOLIO_TXN_HISTORY);

//        oSettings.setParent(transHistoryModel);
        oSettings.setParent(selfRef);
        transHistoryTable.setModel(transHistoryModel);
        transHistoryModel.setPortfolioTable(transHistoryTable, false);
        Theme.registerComponent(transHistoryTable);
        transHistoryTable.getPopup().setPrintTarget(transHistoryTable.getTable(), PrintManager.PRINT_JTABLE);

//        transHistoryTable.getTable().addMouseListener(g_oSymbolMouseListener);
        //System.out.println("Portfolio cols " + oSettings.getColumns() );
        transHistoryTable.getModel().updateGUI();
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);
//        System.out.println("Table created in createHistoryTable()");
    }

    private void createValuationTable() {
        ViewSetting oSettings;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "3").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
//            if (sID == null)
//                oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//            else
//                oSettings.setID(store.getId());
//            oSettings.setCaptions(sNewName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        //String sColOrder = Settings.getItem("Gio");
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_VALUATION_COLS"));

//        if (sID == null)
//            oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//        else
//        oSettings.setID(store.getId());
        oSettings.setTableNumber(1);

        valuationModel = new ValuationModel(dataStore.getValuationList());
        valuationModel.setViewSettings(oSettings);
        valuationTable = new Table(); // table with no popup support inherited
        //  valuationTable.setSortingEnabled();
        valuationTable.setWindowType(ViewSettingsManager.PORTFOLIO_VALUATION);
        valuationModel.setPFWindow(this);

//        oSettings.setParent(valuationModel); // selfRef);
        oSettings.setParent(selfRef);
        //valuationTable.setSortingEnabled();
        valuationTable.setSortingEnabled();
        valuationTable.setGroupablePortfolioValuationModel(valuationModel);
        // valuationTable.setModel(valuationModel);
        valuationModel.setPortfolioTable(valuationTable, true);
        Theme.registerComponent(valuationTable);
        valuationTable.getTable().addMouseListener(this);
        valuationTable.getPopup().setPrintTarget(valuationTable.getTable(), PrintManager.PRINT_JTABLE);
        valuationTable.getTable().getTableHeader().setReorderingAllowed(false);
        //valuationTable.getPopup().enableUnsort(false);
        valuationTable.getPopup().enableUnsort(false);

        valuationTable.getModel().updateGUI();
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);
    }

    public void createVAluationAllTable() {
        ViewSetting oSettings;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "7").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
//            if (sID == null)
//                oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//            else
//                oSettings.setID(store.getId());
//            oSettings.setCaptions(sNewName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        //String sColOrder = Settings.getItem("Gio");
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_VALUATION_COLS"));
        oSettings.setTableNumber(1);

        valuationModelAll = new VAluationModelAll(dataStore.getAllValuationRecords());
        valuationModelAll.setViewSettings(oSettings);
        valuationTableAll = new Table(); // table with no popup support inherited

        //valuationTableAll.setSortingEnabled();
        valuationTableAll.setSortingDisabled();

        valuationTableAll.setWindowType(ViewSettingsManager.PORTFOLIO_VALUATION);
        valuationModelAll.setPFWindow(this);

//        oSettings.setParent(valuationModel); // selfRef);
        oSettings.setParent(selfRef);
        valuationTableAll.setGroupablePortfolioValuationModel(valuationModelAll);
        valuationModelAll.setPortfolioTableAll(valuationTableAll);
        Theme.registerComponent(valuationTableAll);
        valuationTableAll.getTable().setUI(new MultiSpanCellComboTableUI());
        valuationTableAll.getTable().addMouseListener(this);
        //  valuationTableAll.getPopup().setPrintTarget(valuationTableAll.getTable(), PrintManager.PRINT_JTABLE);
        valuationTableAll.getTable().getTableHeader().setReorderingAllowed(false);
        //valuationTableAll.getPopup().enableUnsort(false);
        valuationTableAll.getPopup().enableUnsort(true);

        valuationTableAll.updateUI();
        valuationTableAll.getModel().updateGUI();
        valuationTableAll.getPopup().hidePrintMenu();
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);
    }
//    return valuationTable.getModel().getViewSettings();
//}
//
//public ViewSetting getTransHistorySettings() {
//    return transHistoryTable.getModel().getViewSettings();

    @Override
    public ViewSetting getViewSetting() {
        return valuationTable.getModel().getViewSettings();
    }

    public void applySettings() {
//        ViewSetting oSettings = null;
//        if (table != null) {
//            oSettings = table.getModel().getViewSettings();
//            table.getModel().applySettings();
//            this.setSize(oSettings.getSize());
//        }
        if ((oSettings == null) || (valuationTable.getModel().getViewSettings() == null) ||
                (transHistoryTable.getModel().getViewSettings() == null))
            return;
//System.out.println("Applying table Settings");
        valuationModel.applySettings();
        transHistoryModel.applySettings();
        valuationModelAll.applySettings();
        oSettings = transHistoryModel.getViewSettings();
        this.setLocation(oSettings.getLocation());
        if (oSettings.isVisible()) {
            if (Settings.isPutAllToSameLayer()) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSettings.getIndex());
            } else {
                this.getDesktopPane().setLayer(this, getViewSetting().getLayer(), oSettings.getIndex());
            }
            this.setVisible(true);
            try {
                this.setSelected(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (Settings.isPutAllToSameLayer()) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
            } else {
                this.getDesktopPane().setLayer(this, getViewSetting().getLayer(), 0);
            }
        }

        try {
            switch (oSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    this.setIcon(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (oSettings.getLocation() != null) {
//            System.out.println(">>> Loc " + oSettings.getLocation());
            this.setLocation(oSettings.getLocation());
        }

        if (oSettings.getSize() != null) {
//            System.out.println(">>> Size " + oSettings.getSize().width + " " + oSettings.getSize().height);
            this.setSize(oSettings.getSize());
        }
        //System.out.println("My title " + oSettings.getCaptionID());
        this.setVisible(oSettings.isVisible());
        //this.setTitle(oSettings.getCaption());
        revalidateHeader();
        //table.getModel().adjustColumns()
    }

    public void addTitlebarManu() {
        Component np = (Component) ((BasicInternalFrameUI) getUI()).getNorthPane();
        try {
            np.removeMouseListener(this); // if existing
        } catch (Exception e) {
        }
        np.addMouseListener(this);
    }

    public void revalidateHeader() {
//        System.out.println("revalidateHeader() called");
        try {
//            if (!table.getModel().getViewSettings().isHeaderVisible()){
            if (!oSettings.isHeaderVisible()) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                this.revalidate();
            }
        } catch (Exception e) {
        }
    }

    /*private void toggleTitleBar() {
        setTitleVisible(!isTitleVisible());
    }*/

    public void show() {
        super.show();
        setBaseCurrency();
        revalidateHeader();
    }

    public void updateUI() {
        super.updateUI();
        addTitlebarManu();
        revalidateHeader();
        this.setBorder(GUISettings.getTWFrameBorder());
    }

    public boolean isTitleVisible() {
        if (oSettings != null) {
            return oSettings.isHeaderVisible(); // table.getModel().getViewSettings().isHeaderVisible();
        } else {
            return true;
        }
    }

    public void setTitleVisible(boolean setVisible) {
        ViewSetting settings = oSettings; // null; //table.getModel().getViewSettings();
        if (setVisible) {
            if (settings != null) {
                settings.setHeaderVisible(true);
            }
            updateUI();
        } else {
            Component titleBar = ((BasicInternalFrameUI) getUI()).getNorthPane();
            if (titleBar != null) {
                //System.out.println("hiding");
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                revalidate();
                if (settings != null) {
                    settings.setHeaderVisible(false);
                }
            }
        }
        settings = null;
    }

    public void windowClosing() {

    }

    public void updateGUI() {
//        this.setTitle(table.getModel().getViewSettings().getCaption());
//        table.getModel().updateGUI();
    }

    public String toString() {
        return getTitle();
    }

    public void setBaseCurrency() {
        try {
//            if (currencyList.getSelectedIndex() >= 0) {
//                dataStore.setBaseCurrency((String)currencyIDs[currencyList.getSelectedIndex()]);//(String)currencyList.getSelectedItem());
//                table.getTable().updateUI();
//            }
        } catch (Exception ex) {
        }
    }

    public void activateCurrentSymbols() {
        if (isRestoreInProgress) {
            return;
        }
        boolean isSymbolFound = false;
        String sKey = null;
        String[] list = null;
        String[] curList = null;

//        if (oldSymbolList == null) {
//            list = PFStore.getAllSymbolList();      This creates a problem when loadin from the WSP
//        } else {
//            list = oldSymbolList;
//        }
        list = oldSymbolList;
        //Removed by Udaya
        //send symbol validations
//        long timestamp = System.currentTimeMillis();
//        for(int i=0;i<list.length;i++){
//            String exg=SharedMethods.getExchangeFromKey(list[i]);
//            String sym=SharedMethods.getSymbolFromKey(list[i]);
//            PFImportDataStore.validateSymbol(sym,"PF_IMPRT_DLG",exg,this, timestamp);
//            timestamp = timestamp +1 ;
//        }


        if (allPannelSelected) {
            curList = PFStore.getAllSymbolList();
        } else {
            curList = PFStore.getCurrentSymbolList();
        }
        oldSymbolList = curList;
        activate(list, curList);
//        if (list!= null) {
//            for (int i = 0; i < list.length; i++) {
//                sKey = list[i];
//                for (int j = 0; j < curList.length; j++) {
//                    if (sKey.equals(curList[j])) {
//                        isSymbolFound = true;
//                        break;
//                    }
//                }
//                if (!isSymbolFound) {
//                    PortfolioInterface.removeSymbol(sKey);
//                } else
//                    isSymbolFound = false;
//                sKey = null;
//            }
//        }
//
//        for (int i = 0; i < curList.length; i++) {
//            sKey = curList[i];
//            for (int j = 0; j < list.length; j++) {
//                if (sKey.equals(list[j])) {
//                    isSymbolFound = true;
//                    break;
//                }
//            }
//            if (!isSymbolFound) {
//                System.out.println("Adding newly added Symbol " + sKey);
////                PFImportDataStore.validateSymbol(key,"PF_IMPRT_DLG",exchange,this, timestamp);
//                PortfolioInterface.setSymbol(true, sKey);
//            } else
//                isSymbolFound = false;
//
//            sKey = null;
//        }

        sKey = null;
        list = null;
        curList = null;
    }

    private void activate(String[] oldList, String[] newList) {
        if (newList != null) {
            for (int i = 0; i < newList.length; i++) {
                String sKey = newList[i];
                System.out.println("PF Symbol add " + sKey);
                PortfolioInterface.setSymbol(true, sKey);
            }
        }
        if (oldList != null) {
            for (int i = 0; i < oldList.length; i++) {
                String sKey = oldList[i];
                System.out.println("PF Symbol Remove " + sKey);
                PortfolioInterface.removeSymbol(sKey);
            }
        }

    }

    private void unregisterSymbols() {
        if (!isRegisteredSymbols)
            return;
        isRegisteredSymbols = false;
        String[] list = PFStore.getCurrentSymbolList();
        for (int i = 0; i < list.length; i++) {
            PortfolioInterface.removeSymbol(list[i]);
        }
    }

    private void reregisterSymbols() {
//        if (PortfolioInterface.getPortfolioVersion() != PFUtilities.INTERNATIONAL_VERSION)
//            return;
        if (isRegisteredSymbols)
            return;
        isRegisteredSymbols = true;

        String[] list = PFStore.getCurrentSymbolList();
        for (int i = 0; i < list.length; i++) {
            PortfolioInterface.setSymbol(false, list[i]);
        }
    }

    /*private void reregisterAllSymbols() {
        if (PortfolioInterface.getPortfolioVersion() != PFUtilities.INTERNATIONAL_VERSION)
            return;
        String[] list = dataStore.getAllSymbolList();
        for (int i = 0; i < list.length; i++) {
            PortfolioInterface.setSymbol(false, list[i]);
        }
    }*/

    private void loadTransactionToEdit(TransactRecord record) {


        if (editDialog == null) {
            editDialog = new TransactionDialog(Client.getInstance().getFrame(), "");
            editDialog.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
            Client.getInstance().getDesktop().add(editDialog);
            editDialog.addInternalFrameListener(this);
        }

        editDialog.clearSymbolspecificBuys();
        editDialog.setParent(selfRef);
        if (record != null)
            editDialog.setRecord(record);

        Thread txnThread = new Thread(editDialog, "PortfolioWindow");
        editDialog.setCurrentThread(txnThread);

        txnThread.start();
        editDialog.showDialog();
        editDialog.disableSymbolField();
        editDialog.setVisible(true);
        editDialog.show();
        editDialog.setSellValuationMode(PFStore.isFIFOSellMode());


    }

    private void getCustomMenuItem(CommonPopup popup) {
        editCurPF = new TWMenuItem(Language.getString("EDIT_TRANSACTION"), "edittransaction.gif");
        delCurPF = new TWMenuItem(Language.getString("DELETE_TRANSACTION"), "deletetransaction.gif");
        filterCurTransactions = new TWMenuItem(Language.getString("FILTER_TRANSACTION"), "sorttransactions.gif");

        editCurPF.addActionListener(this);

        delCurPF.addActionListener(this);
        filterCurTransactions.addActionListener(this);

        editCurPF.setActionCommand("EDTPF");
        delCurPF.setActionCommand("DELPF");
        filterCurTransactions.setActionCommand("FILTER");

        popup.add(new TWSeparator());
        popup.setMenuItem(editCurPF);
        popup.setMenuItem(delCurPF);
        popup.setMenuItem(filterCurTransactions);
    }

    private void editSelectedTransaction() {
        long txnID = (Long) transHistoryTable.getTable().getValueAt(transHistoryTable.getTable().getSelectedRow(), 12);

        TransactRecord record = dataStore.getTransaction(txnID);
        if (record.getTxnType() == PFUtilities.BUY || record.getTxnType() == PFUtilities.SELL) {
            boolean isSplit = checkForSplits(record);
            if (isSplit) {
                new ShowMessage(Language.getString("MSG_CANNOT_EDIT") + "" +
                        ", " + Language.getString("MSG_CANNOT_DELETE_REASON_SPLIT"), "I");

            } else {
                loadTransactionToEdit(dataStore.getTransaction(txnID));
            }
        } else {
            loadTransactionToEdit(dataStore.getTransaction(txnID));
        }
    }

    private boolean deleteSelectedTransaction() {
        long txnID = (Long) transHistoryTable.getTable().getValueAt(transHistoryTable.getTable().getSelectedRow(), 12);
        TransactRecord record = dataStore.getTransaction(txnID);
        if (record.getTxnType() == PFUtilities.BUY || record.getTxnType() == PFUtilities.SELL) {
            boolean canDelete = canTransactionBeDeleted(record);
            boolean isSplit = checkForSplits(record);
            if (isSplit) {
                new ShowMessage(Language.getString("MSG_CANNOT_DELETE") + "" +
                        ", " + Language.getString("MSG_CANNOT_DELETE_REASON_SPLIT"), "I");
                return false;


            }
            if (!canDelete) {
                new ShowMessage(Language.getString("MSG_CANNOT_DELETE") + "" +
                        ", " + Language.getString("MSG_CANNOT_DELETE_REASON_SELLLINKED"), "I");
                return false;
            }
        }

        ShowMessage oMessage = new ShowMessage(Language.getString("MSG_DELETE_CURRENT_TRANSACTION"), "W", true);
        int i = oMessage.getReturnValue();
        oMessage = null;
        if (i == 1) {

            undoFIFOSells(dataStore.getTransaction(txnID));
            TransactRecord transactRecord = dataStore.deleteTransaction(txnID, true);
            PortfolioInterface.removeSymbol(transactRecord.getSKey());
            createFilteredTransactionList(null, PFUtilities.NONE, null, null);
            setFilterDescription(false, null, null, null, PFUtilities.NONE);
            displayCashBalance();
            transactRecord = null;
            return true;
        }
        return false;
    }

    private void undoFIFOSells(TransactRecord rec) {
        String sKey = rec.getSKey();
        long id = rec.getId();

        ArrayList transactionRecords = PFStore.getInstance().getFilteredTransactionList();
        for (int i = 0; i < transactionRecords.size(); i++) {

            TransactRecord trRec = (TransactRecord) transactionRecords.get(i);
            if (trRec.getSKey() != null && trRec.getSKey().equals(sKey) && trRec.getTxnType() == PFUtilities.BUY) {

                Integer val = trRec.getSpecificSellQty(id);
                if (val != null) {
                    trRec.deleteSellQuantity(id);
                }


            }

        }

    }

    private boolean checkForSplits(TransactRecord rec) {
        if (rec.getTxnType() == PFUtilities.BUY) {

            return rec.isSplitAdjusted();

        } else if (rec.getTxnType() == PFUtilities.SELL) {

            return rec.isSellLocked();

        }


        return true;

    }

    private boolean canTransactionBeDeleted(TransactRecord record) {
        if (record.getTxnType() == PFUtilities.BUY) {

            if (record.getSoldHolding() > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void applyTxHistoryFilter() {
        filterPanel = new FilterPanel(Client.getInstance().getFrame(), "", true, this);
        filterPanel.setLastFilterSettings(selectedSKey, selectedType, selectedStartDate, selectedEndDate);
        filterPanel.showDialog();
    }

    private void disableTxHistoryFilter() {
        selectedSKey = null;
        selectedType = PFUtilities.NONE;
        selectedStartDate = null;
        selectedEndDate = null;

        createFilteredTransactionList(null, PFUtilities.NONE, null, null);
        setFilterDescription(false, null, null, null, PFUtilities.NONE);
        String sDesc = "";
        if (Language.isLTR())
            sDesc = Language.getString("CURRENT_VIEW") + " : " + Language.getString("PF_MSG_CURRENT_VIEW");
        else
            sDesc = Language.getString("PF_MSG_CURRENT_VIEW") + " : " + Language.getString("CURRENT_VIEW");
        lblFilterDesc.setText(sDesc);
        sDesc = null;
    }

    public void itemStateChanged(ItemEvent e) {
//        if ((e.getStateChange() == e.SELECTED) && (currencyList.getSelectedItem()!=null)){
//            //System.out.println("selected " +(String)currencyList.getSelectedItem());
//            try{
//                dataStore.setBaseCurrency((String)currencyIDs[currencyList.getSelectedIndex()]);//(String)currencyList.getSelectedItem());
//                table.getTable().updateUI();
//            }catch(Exception ex){
//            }
        //dataStore.setBaseCurrency((String)currencyList.getSelectedItem());
//        } else {
        CardLayout cl = (CardLayout) (tablePanel.getLayout());
        cl.show(tablePanel, (String) e.getItem());
        cl = null;
//        }
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        if (e.getSource().equals(transHistoryTable.getPopup())) {
            try {
                long txnID = (Long) transHistoryTable.getTable().getValueAt(transHistoryTable.getTable().getSelectedRow(), 12);
                editCurPF.setEnabled(true);
                delCurPF.setEnabled(true);
                filterCurTransactions.setEnabled(true);
            } catch (Exception e1) {
                editCurPF.setEnabled(false);
                delCurPF.setEnabled(false);
                filterCurTransactions.setEnabled(false);
            }
            /*if (transHistoryTable.getTable().getSelectedRowCount() > 1) {
                editCurPF.setEnabled(false);
                delCurPF.setEnabled(false);
                filterCurTransactions.setEnabled(false);
            } else {
                editCurPF.setEnabled(true);
                delCurPF.setEnabled(true);
                filterCurTransactions.setEnabled(true);
            }*/
        } else if (e.getSource().equals(valuationTable.getPopup())) {
            try {
                String symbol = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -1);
                if (symbol != null) {
                    whatifcal.setEnabled(true);
                } else {
                    whatifcal.setEnabled(false);
                }
            } catch (Exception e1) {
                whatifcal.setEnabled(false);
            }
        } else if (e.getSource().equals(g_mnuFile.getPopupMenu())) {
            g_mnuCreateNewPF.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_NewPortfolio));
        } else if (e.getSource().equals(g_mnuManage.getPopupMenu())) {
            g_mnuDeletePF.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_NewPortfolio));
            g_mnuEditPF.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigPortfolio));
        }

        //System.out.println("alo");
//        currencyIDs = CurrencyStore.getSymbols();
        //currencyList.updateUI();
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof javax.swing.plaf.metal.MetalInternalFrameTitlePane) {
            if (SwingUtilities.isRightMouseButton(e)) {
                GUISettings.showPopup(titleBarMenu, this, e.getX(), e.getY());
            }
        } else if (e.getSource() == toggleIncludeCash) {
            valuationModel.includeCashBalance(toggleIncludeCash.isSelected());
            valuationModelAll.includeCashBalance(toggleIncludeCash.isSelected());
            if (toggleIncludeCash.isSelected()) {
                setCashBalanceStatus(true);

            } else {
                setCashBalanceStatus(false);
            }

        } else if (e.getSource() == lblApplyFilter) {
            applyTxHistoryFilter();
        } else if (e.getSource() == btnAvailablePFs) {
            portfolioButtonClicked();
        } else if (e.getSource() == lblAvailablePFs) {
            portfolioButtonClicked();
        } else if (e.getSource() == lblCurrencyIDs) {
            currencyButtonClicked(false, null);
        } else if (e.getSource() == btnCurrencyIDs) {
            currencyButtonClicked(false, null);
        } else if (e.getSource() instanceof JTable) {
            int rowcnt;
            int selcol;
            int selrow;
            if (allPannelSelected) {
                rowcnt = valuationTableAll.getTable().getRowCount();
                selcol = valuationTableAll.getTable().getSelectedColumn();
                selrow = valuationTableAll.getTable().getSelectedRow();
            } else {
                rowcnt = valuationTable.getTable().getRowCount();
                selcol = valuationTable.getTable().getSelectedColumn();
                selrow = valuationTable.getTable().getSelectedRow();


            }


            if (selrow == rowcnt - 1 && selcol < 8 && !(selectedPanel == TRANS_HISTORY)) {

                //if(selrow == rowcnt-1 && SwingUtilities.getAncestorOfClass(PortfolioRenderer.IconLable.class,((JTable)e.getSource())) != null &&  !(selectedPanel == TRANS_HISTORY) ){

                currencyButtonClicked(true, e);
            }

        }
        if (e.getSource().equals(transHistoryTable.getTable())) {
            if (e.getClickCount() > 1) {
                long txnID = (Long) transHistoryTable.getTable().getValueAt(transHistoryTable.getTable().getSelectedRow(), 12);
                loadTransactionToEdit(dataStore.getTransaction(txnID));
            }
        }
        if (e.getSource().equals(valuationTable.getTable())) {
            if (e.getClickCount() > 1) {
                String symbol = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -1);
                Client.getInstance().mnuDetailQuoteSymbol(symbol);
                //Client.getInstance().mnuPortfolioDQ(valuationTable.getTable().getSelectedRow(),dataStore);
            }
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("TITLE_HIDE")) {
            toggleTitleBar();
        } else if (e.getActionCommand().equals("EDTPF")) {
            editSelectedTransaction();
        } else if (e.getActionCommand().equals("DELPF")) {
            if (deleteSelectedTransaction())
                createValuationList();
        } else if (e.getActionCommand().equals("FILTER")) {
            applyTxHistoryFilter();
        } else { //if (e.getActionCommand().equals("USD")) {
            String sItem = null;
            for (int i = 0; i < currencyIDs.length; i++) {
                sItem = (String) currencyIDs[i];
                if (e.getActionCommand().equals(sItem)) {
//                    System.out.println("cur ID " + sItem + " Action Cmd " + e.getActionCommand());
                    lblSelectedCurrencyID.setText(sItem);
                    tableCurrencyLable.setText(sItem);
                    dataStore.setBaseCurrency(sItem);
                    displayCashBalance();
                    setDescriptionLable();
                    valuationTable.getTable().updateUI();
                }
                sItem = null;
            }
        }
    }

    public void setSelectedCuttencyForSinglePF(long[] selePFId) {

        if (selePFId != null && selePFId.length == 1) {

            PortfolioRecord pfRecord = PFStore.getInstance().getPortfolio(selePFId[0]);
            String sItem = pfRecord.getCurrency();
            lblSelectedCurrencyID.setText(sItem);
            tableCurrencyLable.setText(sItem);
            dataStore.setBaseCurrency(sItem);
            setDescriptionLable();
            valuationTable.getTable().updateUI();

        }


    }
//} else if (e.getSource() == valuationTable.getTable()){
//    if (SwingUtilities.isRightMouseButton(e)){
//        System.out.println("Clicked on Valuation Table");
//        processMenuClicked(VALUATION);
//    }
//} else if (e.getSource() == transHistoryTable.getTable()){
//    if (SwingUtilities.isRightMouseButton(e)){
//        System.out.println("Clicked on Transaction History Table");
//        processMenuClicked(TRANS_HISTORY);
//    }

    public void currencyButtonClicked(boolean istable, MouseEvent e) {
        TWMenuItem menuItem = null;
        String sItem = null;
        if ((currencyPopup == null) || (currencyPopup.getComponentCount() != currencyIDs.length)) {
            btnGroupCurrency = new ButtonGroup();
            currencyPopup = new JPopupMenu();

            for (int i = 0; i < currencyIDs.length; i++) {
                sItem = (String) currencyIDs[i];
                menuItem = new TWMenuItem(sItem);
                menuItem.setActionCommand(sItem);
                menuItem.addActionListener(this);
                currencyPopup.add(menuItem);
                btnGroupCurrency.add(menuItem);
                menuItem = null;
            }
            GUISettings.applyOrientation(currencyPopup);
            // SwingUtilities.updateComponentTreeUI(currencyPopup);

        }

        int popWidth = 0;
        if (Language.isLTR()) {
            popWidth = (int) currencyPopup.getPreferredSize().getWidth();
        }
        if (istable) {
            if (allPannelSelected) {
                GUISettings.showPopup(currencyPopup, e.getComponent(), e.getX(), e.getY());
            } else {
                GUISettings.showPopup(currencyPopup, e.getComponent(), e.getX(), e.getY());
            }
            // currencyPopup.show(tableCurrencyLable, tableCurrencyLable.getWidth() - popWidth, btnCurrencyIDs.getHeight() - 4);
        } else {
            currencyPopup.show(btnCurrencyIDs, btnCurrencyIDs.getWidth() - popWidth, btnCurrencyIDs.getHeight() - 4);
        }
        this.applyTheme();
//        currencyPopup = null;
    }

    private void portfolioButtonClicked() {
        int popWidth = 0;
        PortfolioList pf = new PortfolioList(this, selectedPFIDs);
        if (Language.isLTR()) {
            popWidth = (int) pf.getPreferredSize().getWidth();
        }
        pf.show(btnAvailablePFs, btnAvailablePFs.getWidth() - popWidth, btnAvailablePFs.getHeight() - 4);
        pf = null;
    }

    private void loadButtonImages() {
        int themeID = PortfolioInterface.getThemeID();
        btnCreatePF.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_create.gif"));
        btnCreatePF.updateUI();

        btnAnalysis.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_analysis.gif"));
        btnAnalysis.updateUI();

        btnValuationView.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_valuation.gif"));
        btnValuationView.updateUI();

        btnHistoryView.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_txnhistory.gif"));
        btnHistoryView.updateUI();

        btnAddTransaction.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_addtxn.gif"));
        btnAddTransaction.updateUI();

        btnApplyFilter.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_txnfilter.gif"));
        btnApplyFilter.updateUI();

        btnDisableFilter.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_txnfilterDisable.gif"));
        btnDisableFilter.updateUI();

        toggleIncludeCash.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_include_cashBal.gif"));
        toggleIncludeCash.updateUI();

        btnPortfolioReview.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_report.gif"));
        btnPortfolioReview.updateUI();
    }

    public void applyTheme() {
        descripLable.setForeground(Theme.getColor("MAIN_MENU_FGCOLOR"));
        SwingUtilities.updateComponentTreeUI(this);
//        table.getTable().updateUI();
//        table.getTable().getTableHeader().updateUI();
        SwingUtilities.updateComponentTreeUI(titleBarMenu);
        //SwingUtilities.updateComponentTreeUI(currencyPopup);
        loadButtonImages();
        if (currencyPopup != null) {
            SwingUtilities.updateComponentTreeUI(currencyPopup);
            currencyPopup.setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
            currencyPopup.setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
            currencyPopup.updateUI();
        }
        Theme.applyFGColor(g_mnuFile, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(g_mnuView, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(g_mnuManage, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(g_mnuTools, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(g_mnuHelp, "MAIN_MENU_FGCOLOR");

//        g_mnuMain.setForeground(Color.red);
//        g_mnuMain.setBackground(Color.blue);
        /*currencyPopup.setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
        currencyPopup.setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
        currencyPopup.updateUI();*/
//        lblCashBal.setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
//        lblCashBal.setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
//        lblCashBal.updateUI();
    }

    public int getDesktopItemType() {
        return BOARD_TYPE;
    }

// New Chnages
/*
     private void loadButtonImages() {
             int themeID = PortfolioInterface.getThemeID();
             btnCreatePF.setIcon(new ImageIcon("images/Theme" + themeID +  "/pf_create.gif"));
             btnCreatePF.updateUI();

             btnAnalysis.setIcon(new ImageIcon("images/Theme" + themeID +  "/pf_analysis.gif"));
             btnAnalysis.updateUI();

             btnValuationView.setIcon(new ImageIcon("images/Theme" + themeID +  "/pf_valuation.gif"));
             btnValuationView.updateUI();

             btnHistoryView.setIcon(new ImageIcon("images/Theme" + themeID +  "/pt_txnhistory.gif"));
             btnHistoryView.updateUI();

             btnAddTransaction.setIcon(new ImageIcon("images/Theme" + themeID +  "/pf_addtxn.gif"));
             btnAddTransaction.updateUI();
         }

         public void applyTheme() {

             SwingUtilities.updateComponentTreeUI(this);
//        table.getTable().updateUI();
//        table.getTable().getTableHeader().updateUI();
             SwingUtilities.updateComponentTreeUI(titleBarMenu);
             loadButtonImages();
         }
*/

    public int getWindowType() {
        return windowType;
    }

    public void setWindowType(int type) {
        windowType = type;
    }

    public void closeWindow() {
//		if (getDefaultCloseOperation() == HIDE_ON_CLOSE){
//			setVisible(false);
//		}else{
//			dispose();
//		}
        doDefaultCloseAction();
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void doDefaultCloseAction() {
        super.doDefaultCloseAction();
        focusNextWindow();
    }

    private void focusNextWindow() {
        TWDesktop desktop = (TWDesktop) Client.getInstance().getDesktop();
        desktop.focusNextWindow(desktop.getListPosition(this, true), true);
        desktop = null;
    }

    /**
     * Invoked when a internal frame has been opened.
     *
     * @see javax.swing.JInternalFrame#show
     */
    public void internalFrameOpened(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is in the process of being closed.
     * The close operation can be overridden at this point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {
        if (e.getSource().equals(this)) {
            unregisterSymbols();
        }
    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {
        if (e.getSource().equals(this)) {
            unregisterSymbols();
        } else if (e.getSource().equals(editDialog)) {

            editDialog = null;

        }
    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {
//         System.out.println("PortfolioTable.internalFrameIconified");
        if (e.getSource().equals(this)) {
            unregisterSymbols();
        }
    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {
        if (e.getSource().equals(this)) {
            reregisterSymbols();
        }
    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {
        if (e.getSource().equals(this)) {
            reregisterSymbols();
        }
    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {

    }

    public JTable getTable2() {
        return transHistoryTable.getTable();
    }

//     public void stateChanged(ChangeEvent e) {
    //System.err.println("state changed " + tabbedPane.getSelectedIndex());
//         if (tabbedPane.getSelectedIndex() == 0) {
//         } else if (tabbedPane.getSelectedIndex() == 1) {
//         }
//     }

    public JTable getTable1() {
        return valuationTable.getTable();
    }

    public boolean isWindowVisible() {
        return isVisible();
    }

    public int getZOrder() {
        return getDesktopPane().getPosition(this);
    }

    public void printTable() {

    }

    public int getWindowStyle() {
        if (isMaximum())
            return ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
    }

    public void setFilterDescription(boolean isApplied, Date fromDate, Date toDate, String sKey, byte type) {
//        String sDesc = "<html>" + Language.getString("CURRENT_VIEW") + " : " + Language.getString("PF_TRANSACTIONS") + "&nbsp;";
        String sDesc = "<html>" + Language.getString("CURRENT_VIEW") + " : " + "&nbsp;";
        if (isApplied) {
            if (fromDate != null) {
                sDesc += Language.getString("FROM") + "&nbsp;<b>" + PFUtilities.getFormattedDate(fromDate) + "</b>&nbsp;,&nbsp;";
            }
            if (toDate != null) {
                sDesc += Language.getString("TO") + "&nbsp;<b>" + PFUtilities.getFormattedDate(toDate) + "</b>&nbsp;,&nbsp;";
            }
            if (sKey != null) {
                sDesc += Language.getString("SYMBOL") + "&nbsp;<b>" + PortfolioInterface.getSymbolFromKey(sKey) + "</b>&nbsp;,&nbsp;";
            }
            switch (type) {
                case PFUtilities.NONE:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("ALL") + "</b>";
                    break;
                case PFUtilities.BUY:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("BUY") + "</b>";
                    break;
                case PFUtilities.SELL:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("SELL") + "</b>";
                    break;
                case PFUtilities.OP_BAL:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("OPENING_BAL") + "</b>";
                    break;
                case PFUtilities.DIVIDEND:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("DIVIDEND") + "</b>";
                    break;
                case PFUtilities.DEPOSITE:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("DEPOSITES") + "</b>";
                    break;
                case PFUtilities.WITHDRAWAL:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("WITHDROWALS") + "</b>";
                    break;
                case PFUtilities.CHARGES:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("CHARGES") + "</b>";
                    break;
                case PFUtilities.REFUNDS:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("REFUNDS") + "</b>";
                    break;
            }
            sDesc += "</html>";
        } else {
//            sDesc = Language.getString("CURRENT_VIEW") + " : " + Language.getString("PF_MSG_CURRENT_VIEW");
            if (Language.isLTR())
                sDesc = Language.getString("CURRENT_VIEW") + " : " + Language.getString("PF_MSG_CURRENT_VIEW");
            else
                sDesc = Language.getString("PF_MSG_CURRENT_VIEW") + " : " + Language.getString("CURRENT_VIEW");
        }
        lblFilterDesc.setText(sDesc);
    }

    public void setVisible(boolean status) {
        super.setVisible(status);
        if (status && (ExchangeStore.isValidSystemFinformationType(Meta.WT_NewPortfolio))) {
            if (PFStore.getInstance().getPortfolioList().isEmpty() && ((selectedPFIDs == null) || (selectedPFIDs.length == 0))) {
                if (initWindow == null) {
                    initWindow = new InitialPromptWindow(Client.getInstance().getFrame(),
                            "", false, this);
                }
                initWindow.showDialog();
            }
        }
//        if (backupDaemon != null) {
//            backupDaemon.setAutoBackUpStatus(status);
//            System.out.println("setAutoBackUpStatus is " + status);
//        }
        if (status) {
            activateCurrentSymbols();
        }

    }

    private void setCashBalanceStatus(boolean status) {
        if (!status) {
            toggleIncludeCash.setToolTipText(Language.getString("INCLUDE_CASH_BALANCE"));
        } else {
            toggleIncludeCash.setToolTipText(Language.getString("EXCLUDE_CASH_BALANCE"));
        }
        includeCashBalselected();
//        valuationModel.includeCashBalance(status);
//        valuationTable.getTable().updateUI();
//
//        try {
//            dataStore.createAllValuationList(toggleIncludeCash.isSelected());
//            valuationModelAll.includeCashBalance(status);
//            valuationTableAll.getTable().updateUI();
//        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
    }

    private void exportHstory() {
        ExportDialog export = new ExportDialog(Client.getInstance().getFrame(), true);
        export.setParentWindow(this);
        export.setVisible(true);
    }

    public long[] getSelectedPFIDs() {
        return selectedPFIDs;
    }

    public void setSelectedPFIDs(long[] pfIDs) {
        String sTitle = Language.getString("PORTFOLIO_SIMULATOR") + " - ";
        PortfolioRecord pfRecord = null;
        selectedPFIDs = pfIDs;
        for (int i = 0; i < selectedPFIDs.length; i++) {
            pfRecord = PFStore.getInstance().getPortfolio(selectedPFIDs[i]);
            if (pfRecord != null)
                sTitle += pfRecord.getName();
            if (i != (selectedPFIDs.length - 1))
                sTitle += ", ";
        }
        //change start
        if (sTitle.trim().endsWith(",")) {
            sTitle = sTitle.substring(0, sTitle.lastIndexOf(","));
        }
        if (sTitle.trim().endsWith("-")) {
            sTitle = sTitle.substring(0, sTitle.lastIndexOf("-"));
        }
        //change end

        this.setTitle(sTitle);
        createFilteredTransactionList(null, PFUtilities.NONE, null, null);
        setFilterDescription(false, null, null, null, PFUtilities.NONE);
        createValuationList();
    }

    public ViewSetting getValuationSettings() {
        return valuationTable.getModel().getViewSettings();
    }

    public ViewSetting getValuationAllSettings() {
        return valuationTableAll.getModel().getViewSettings();
    }

    public ViewSetting getTransHistorySettings() {
        return transHistoryTable.getModel().getViewSettings();
    }

    public ViewSetting getPortfolioViewSettings() {
        return this.oSettings;
    }

    public ValuationModel getValuationModel() {
        return valuationModel;
    }

    public Table getValuationTable() {
        return valuationTable;
    }

    private void displayBackUpWizard() {
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.PortfolioSimulator, "Backup");
        BackUpWizard bw = new BackUpWizard(Client.getInstance().getFrame(), "", true, selfRef, backupDaemon);
        bw.showDialog();
    }

    private void restoreBackedUpTransactions() {
        RestoreBackups backup = new RestoreBackups();
        Thread t = new Thread(backup, "Restore Portfolio Backup");
        t.start();
      /*  if (PortfolioInterface.isValidForTransactions()) {
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.PortfolioSimulator, "Restore");
            String sPath = PFUtilities.getSelectedFilePath(this, PFUtilities.FILE_OPEN_DIALOG_TYPE,
                    JFileChooser.FILES_ONLY, PFUtilities.backUpPath);
            selectedPFIDs = null;
            setTitle(Language.getString("PORTFOLIO_SIMULATOR"));
            if (sPath != null && new File(sPath).exists()) {
                PFStore.getInstance().clearAllStores();
                prepareWindoForReload();

                PFStore.getInstance().loadTransactions(sPath);

                createPortfolioTabs();
                loadDefaultPortfolio();
                createFilteredTransactionList(null, PFUtilities.NONE, null, null);
                setFilterDescription(false, null, null, null, PFUtilities.NONE);
                long timestamp = System.currentTimeMillis();
                String[] list = PFStore.getAllSymbolList();
                if (list != null) {
                    for (int i = 0; i < list.length; i++) {
                        String exg = SharedMethods.getExchangeFromKey(list[i]);
                        String sym = SharedMethods.getSymbolFromKey(list[i]);
                        PFImportDataStore.validateSymbol(sym, "PF_RESTORE_DLG", exg, timestamp);
                        timestamp = timestamp + 1;
                    }
                }

                createValuationList();
                displayCashBalance();
                tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
                tabStareChanged(new TWTabEvent(this, TWTabEvent.STATE_SELECTED));

            }
        }*/
    }

    private void prepareWindoForReload() {
        int i = tabbedPane.getTabCount();
        if (i > 0) {
            for (int j = 0; j < i; j++) {
                tabbedPane.removeTab(0);    // tab indexes are auto decremented when a tab is removed
            }

        }
        portfolioIDS = null;
        selectedPFIDs = null;


    }

    public void currencyAdded() {
        Enumeration ids = CurrencyStore.getSharedInstance().getCurrencyIDs();
        ArrayList idlist = new ArrayList();
        while (ids.hasMoreElements()) {
            idlist.add(ids.nextElement());
        }
        currencyIDs = new String[0];
        currencyIDs = idlist.toArray(currencyIDs);
        //change start
        if (dataStore.getBaseCurrency() == null) {
            dataStore.setBaseCurrency((String) currencyIDs[0]);
            lblSelectedCurrencyID.setText((String) currencyIDs[0]);
            tableCurrencyLable.setText((String) currencyIDs[0]);
        }
        //change end
    }

    private void printPortfolio() {
        switch (selectedPanel) {
            case VALUATION:
                valuationTable.getPopup().printPFTable(PrintManager.TABLE_TYPE_DEFAULT);
                break;
            case TRANS_HISTORY:
                transHistoryTable.getPopup().printTable(PrintManager.TABLE_TYPE_DEFAULT);
                break;
        }
    }

    public String getSelectedCurrency() {
        return CurrencyStore.getSharedInstance().getCurrencyDescription(lblSelectedCurrencyID.getText());
    }

    private void creatCalculatorMenu(JPopupMenu parent) {
        whatifcal = new TWMenuItem(Language.getString("FIX_AVERAGE_COST"), "fixavgcost.gif");
        GUISettings.applyOrientation(whatifcal);
        parent.add(whatifcal);
        whatifcal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String symbol = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -1);
                createWhatiFCalc(symbol);
                /*wiframe = WhatiFCalculatorPortfolioFrame.getSharedInstannce(symbol, WhatiFCalculatorPortfolioFrame.MODE_SIMULATOR);
                wiframe.setSize(new Dimension(290, 320));
                wiframe.setClosable(true);
                wiframe.setIconifiable(true);
                wiframe.setVisible(true);
                wiframe.setLayer(GUISettings.TICKER_LAYER);
                wiframe.setLocationRelativeTo(Client.getInstance().getDesktop());*/
            }
        });

    }

    public void createWhatiFCalc(String symbol) {
        //String symbol = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -1);
        wiframe = new WhatiFCalculatorPortfolioFrame(symbol, WhatiFCalculatorPortfolioFrame.MODE_SIMULATOR);
        wiframe.setSize(new Dimension(290, 320));
        wiframe.setClosable(true);
        wiframe.setIconifiable(true);
        wiframe.setVisible(true);
        wiframe.setLayer(GUISettings.TICKER_LAYER);
        wiframe.setLocationRelativeTo(Client.getInstance().getDesktop());
    }

    private void createDecimalMenu(JPopupMenu parent, int type) {
        TWMenu mnuDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        parent.add(mnuDecimalPlaces);

        //transHistoryTable.getPopup().add(mnuDecimalPlaces);
        ButtonGroup oGroup = new ButtonGroup();
        mnuDefaultDecimal[type] = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDecimalPlaces.add(mnuDefaultDecimal[type]);
        mnuDefaultDecimal[type].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuDefaultDecimal[type]);

        mnuNoDecimal[type] = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDecimalPlaces.add(mnuNoDecimal[type]);
        mnuNoDecimal[type].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuNoDecimal[type]);

        mnuOneDecimal[type] = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDecimalPlaces.add(mnuOneDecimal[type]);
        mnuOneDecimal[type].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuOneDecimal[type]);
        mnuTwoDecimal[type] = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDecimalPlaces.add(mnuTwoDecimal[type]);
        mnuTwoDecimal[type].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuTwoDecimal[type]);
        mnuThrDecimal[type] = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDecimalPlaces.add(mnuThrDecimal[type]);
        mnuThrDecimal[type].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuThrDecimal[type]);
        mnuFouDecimal[type] = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDecimalPlaces.add(mnuFouDecimal[type]);
        mnuFouDecimal[type].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuFouDecimal[type]);
        GUISettings.applyOrientation(mnuDecimalPlaces);
    }

    private void setDecimalPlaces(byte places) {
        ((SmartTable) valuationTable.getTable()).setDecimalPlaces(places);
        valuationTable.updateGUI();
        valuationTable.getTable().updateUI();
        ((SmartTable) transHistoryTable.getTable()).setDecimalPlaces(places);
        transHistoryTable.updateGUI();
        transHistoryTable.getTable().updateUI();
        ((SmartTable) valuationTableAll.getTable()).setDecimalPlaces(places);
        oSettings.putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        if (places == Constants.NO_DECIMAL_PLACES) {
            mnuNoDecimal[VALUATION_TYPE].setSelected(true);
            mnuNoDecimal[TRANSACTION_TYPE].setSelected(true);
        } else if (places == Constants.ONE_DECIMAL_PLACES) {
            mnuOneDecimal[VALUATION_TYPE].setSelected(true);
            mnuOneDecimal[TRANSACTION_TYPE].setSelected(true);
        } else if (places == Constants.TWO_DECIMAL_PLACES) {
            mnuTwoDecimal[VALUATION_TYPE].setSelected(true);
            mnuTwoDecimal[TRANSACTION_TYPE].setSelected(true);
        } else if (places == Constants.THREE_DECIMAL_PLACES) {
            mnuThrDecimal[VALUATION_TYPE].setSelected(true);
            mnuThrDecimal[TRANSACTION_TYPE].setSelected(true);
        } else if (places == Constants.FOUR_DECIMAL_PLACES) {
            mnuFouDecimal[VALUATION_TYPE].setSelected(true);
            mnuFouDecimal[TRANSACTION_TYPE].setSelected(true);
        } else if (places == Constants.UNASSIGNED_DECIMAL_PLACES) {
            mnuDefaultDecimal[VALUATION_TYPE].setSelected(true);
            mnuDefaultDecimal[TRANSACTION_TYPE].setSelected(true);
        }
    }

    public void applicationExiting() {
    }

    public void applicationLoaded() {
//        btnCreatePF.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_NewPortfolio));
    }

    public void applicationLoading(int percentage) {
    }

    public void applicationReadyForTransactions() {
        btnCreatePF.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_NewPortfolio));
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void workspaceLoaded() {
        try {
            decimalPlaces = Byte.parseByte(oSettings.getProperty(ViewConstants.VC_DECIMAL_PLACES));
        } catch (Exception e) {
            decimalPlaces = Constants.TWO_DECIMAL_PLACES;
        }
        setDecimalPlaces(decimalPlaces);
        try {
            lblSelectedCurrencyID.setText(dataStore.getBaseCurrency());
            tableCurrencyLable.setText(dataStore.getBaseCurrency());
        } catch (Exception e) {
        }
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }

    public void tabStareChanged(TWTabEvent event) {
        if (event.getState() == TWTabEvent.STATE_REMOVED || event.getState() == TWTabEvent.STATE_ADDED) {
            return;
        }
        int selected = tabbedPane.getSelectedIndex();
        long pfId;
        try {
            pfId = (Long) portfolioIDS.get(selected);
        } catch (Exception e) {
            pfId = 0l;
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if (pfId != 0l) {

            allPannelSelected = false;
            long[] selPf = new long[1];
            selPf[0] = pfId;
            setSelectedPFIDs(selPf);
            showSelectedPanel(getSelectedPanelName(selectedPanel));
            setSelectedCuttencyForSinglePF(selPf);
            //  activateCurrentSymbols();
            displayCashBalance();
            setFilterDescription(false, null, null, null, PFUtilities.NONE);

        } else if (selected >= 0 && ((JButton) tabbedPane.getTabComponentAt(selected)).getText() == Language.getString("ALL")) {

            dataStore.createFilteredTransactionList(dataStore.getPortfolioIDList(), null, PFUtilities.NONE, null, null);
            showSelectedPanel(VALUATION_ALL_PANEL);
            dataStore.createAllValuationList(toggleIncludeCash.isSelected());
            allPannelSelected = true;
            activateCurrentSymbols();
            displayCashBalnceforAll();
            String sTitle = Language.getString("PORTFOLIO_SIMULATOR") + " - " + Language.getString("ALL");
            this.setTitle(sTitle);


        }


    }

    public boolean isAllPannelSelected() {
        return allPannelSelected;
    }

    public void includeCashBalselected() {
        tabStareChanged(new TWTabEvent(tabbedPane, 0));
    }

    private String getSelectedPanelName(int pannelid) {

        if (pannelid == TRANS_HISTORY) {
            return HISTORY_PANEL;
        } else if (pannelid == VALUATION) {
            return VALUATION_PANEL;
        } else {
            return VALUATION_ALL_PANEL;
        }

    }


    public ArrayList getPortfolioIDS() {
        return portfolioIDS;
    }

    public CurrencySelection getTableCurrencyLable() {
        return tableCurrencyLable;
    }

    public String getTableCurrencyString() {
        return tableCurrencyLable.getText();
    }

    public void updateSelectedTable() {
        tabStareChanged(new TWTabEvent(this, 0));
    }

    private class CurrencySelection extends JLabel implements TableCellRenderer {


        public void setText(String text) {

            String Txt = "";
            if (Language.isLTR()) {
                Txt = Language.getString("CURRENCY") + " " + text;
            } else {
                Txt = text + " " + Language.getString("CURRENCY");
            }
            super.setText(Txt);
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            return this;  //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    public class RestoreBackups implements Runnable, PFImportDialogInterface {
        private CountDownLatch latch;
        private int requestCount = 0;

        public RestoreBackups() {

        }

        public void prepareRestore() {
            if (PortfolioInterface.isValidForTransactions()) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.PortfolioSimulator, "Restore");
                String sPath = PFUtilities.getSelectedFilePath(selfRef, PFUtilities.FILE_OPEN_DIALOG_TYPE,
                        JFileChooser.FILES_ONLY, PFUtilities.backUpPath);
                selectedPFIDs = null;
                setTitle(Language.getString("PORTFOLIO_SIMULATOR"));
                if (sPath != null && new File(sPath).exists()) {
                    PFStore.getInstance().clearAllStores();
                    prepareWindoForReload();
                    isRestoreInProgress = true;
                    PFStore.getInstance().loadTransactions(sPath);

                    createPortfolioTabs();
                    loadDefaultPortfolio();
                    createFilteredTransactionList(null, PFUtilities.NONE, null, null);
                    setFilterDescription(false, null, null, null, PFUtilities.NONE);
                    long timestamp = System.currentTimeMillis();
                    String[] list = PFStore.getAllSymbolList();
                    if (list != null) {
                        latch = new CountDownLatch(list.length);
                        requestCount = list.length;
                        for (int i = 0; i < list.length; i++) {
                            String exg = SharedMethods.getExchangeFromKey(list[i]);
                            String sym = SharedMethods.getSymbolFromKey(list[i]);
                            PFImportDataStore.validateSymbolNoExchange(sym, "PF_RESTORE_DLG", exg, this, timestamp);
                            timestamp = timestamp + 1;
                        }
                    }
                    try {
                        latch.await(20000, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (requestCount > 0) {
                        new ShowMessage(Language.getString("BACKUP_NOT_SUCCESSFIL"), "E");
                        PFStore.getInstance().clearAllStores();
                        prepareWindoForReload();
                        return;
                    }
                    isRestoreInProgress = false;
                    createValuationList();
                    displayCashBalance();
//                    tabbedPane.setSelectedIndex(tabbedPane.getTabCount() - 1);
//                    tabStareChanged(new TWTabEvent(this, TWTabEvent.STATE_SELECTED));

                }
            }
        }

        public void run() {
            try {
                prepareRestore();
            } catch (Exception e) {
                isRestoreInProgress = false;
            }
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void setSymbol(String key, int instrument) {
            latch.countDown();
            requestCount = requestCount - 1;
        }

        public void setData(Object data) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void notifyInvalidSymbol(String symbol) {
            latch.countDown();
            requestCount = requestCount - 1;
        }
    }

}
