package com.isi.csvr.portfolio;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Sep 2, 2008
 * Time: 10:52:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommissionListWindow {

    TWMenuItem g_mnuCommission;
    CommissionWindow comissionWindow;
    private Table table;
    private CommissionListModal modal;
    private InternalFrame frame;
//    private ArrayList<String> store;

    public CommissionListWindow() {
//         store = PFStore.getInstance().getCommissionList();

        table = new Table();
        table.setSortingEnabled();

        table.setPreferredSize(new Dimension(300, 100));
        modal = new CommissionListModal();
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_COMMISSION_LIST");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("SPLIT_ADJUSTMENT_COLS"));
        modal.setViewSettings(oSetting);
        String[] g_asHeadings = oSetting.getColumnHeadings();
        int[] g_aiRendIDs = new int[g_asHeadings.length];
        for (int i = 0; i < g_aiRendIDs.length; i++) {
            try {
                g_aiRendIDs[i] = oSetting.getRendererID(i);
            } catch (Exception e) {
                g_aiRendIDs[i] = 0;
            }
        }
//        table.setWindowType(ViewSettingsManager.SPLIT_ADJUSTMENT_VIEW);
        table.setModel(modal);
        modal.setTable(table);
//        table.getTable().setDefaultEditor(Number.class, new LongCellEditor(new TWTextField()));
//        table.getTable().setDefaultEditor(Long.class, new LongCellEditor(new TWTextField()));
        table.getTable().setDefaultRenderer(Object.class, new CommissionListRenderer(g_aiRendIDs));
        table.getTable().setDefaultRenderer(Long.class, new CommissionListRenderer(g_aiRendIDs));
        table.getTable().setDefaultRenderer(Double.class, new CommissionListRenderer(g_aiRendIDs));
        table.getTable().setDefaultRenderer(Boolean.class, new CommissionListRenderer(g_aiRendIDs));
        addMenuItems();
        table.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = table.getTable().getSelectedRow();
                int col = table.getTable().convertColumnIndexToModel(table.getTable().getSelectedColumn());

                if ((col == 3) && (SwingUtilities.isLeftMouseButton(e))) {
                    CommissionObject obj = (CommissionObject) modal.getValueAt(row, -1);
                    CommissionWindow window = new CommissionWindow();
                    window.adjustTitleToEdit();
                    window.setCommissionObject(obj);
                    window.setModal(true);
                    window.setVisible(true);
                    table.getTable().updateUI();
                } else if ((col == 4) && (SwingUtilities.isLeftMouseButton(e))) {
                    CommissionObject obj = (CommissionObject) modal.getValueAt(row, -1);
                    String msg = Language.getString("COMMISSION_DELETE_MESSAGE");
                    //  int start= msg.indexOf("[");
                    // int end= msg.indexOf("]");
                    msg.replace("[NAME]", obj.getId());
                    //int type = SharedMethods.showConfirmMessage(msg, JOptionPane.WARNING_MESSAGE);
                    int type = SharedMethods.showConfirmMessage((Language.getString("COMMISSION_DELETE_MESSAGE")).replace("[NAME]", obj.getId()), JOptionPane.WARNING_MESSAGE);
                    if (type == JOptionPane.YES_OPTION) {
                        PFStore.getInstance().removeCommission(obj.getId(), obj.getTimeStamp());
                    }
                }
            }
        });
        modal.applyColumnSettings();
        modal.applySettings();
        modal.updateGUI();

        frame = new InternalFrame(table);
        frame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(false);
        frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        oSetting.setParent(frame);
        frame.updateUI();
        Client.getInstance().getDesktop().add(frame);
        frame.setLayer(GUISettings.TOP_LAYER);

        frame.getContentPane().add(table, BorderLayout.CENTER);

        frame.setSize(oSetting.getSize());
        frame.setLocation(oSetting.getLocation());
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);

//        table.getPopup().showLinkMenus();
//        table.setSouthPanel(southPanel);
        GUISettings.applyOrientation(frame);
    }

    public void setVisible(boolean status) {
        frame.setVisible(status);
    }

    public void addMenuItems() {

        table.getPopup().setTMTavailable(false);
        table.getPopup().removeAll();
        g_mnuCommission = new TWMenuItem();
        g_mnuCommission.setText(Language.getString("COMMISSION_WINDOW"));
        g_mnuCommission.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (comissionWindow == null) {
                    comissionWindow = new CommissionWindow();
                    comissionWindow.setVisible(true);
                } else {
                    comissionWindow.setVisible(true);
                }
            }
        });
        table.getPopup().add(g_mnuCommission);

    }

}
