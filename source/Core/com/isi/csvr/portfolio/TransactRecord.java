package com.isi.csvr.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWDecimalFormat;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class TransactRecord {

//    public static final byte    NONE    = 0;
//    public static final byte    BUY     = 1;
//    public static final byte    SELL    = 2;
//    public static final byte    OP_BAL  = 3;
//
//    public static final byte    DATA    = 4;
//    public static final byte    TXT     = 5;
//    public static final byte    CSV     = 6;
//    public static final byte    QIF     = 7;
//
//    public static final byte    TRANSACTION_RECORD  = 7;
//    public static final byte    PORTFOLIO_RECORD    = 8;

//    private static int     txn_ID;         // Auto increment int field

    private static final String SPLIT_RECORD_SEPERATOR = ";";
    private static final String SPLIT_DATA_SEPERATOR = ":";
    private long transID;
    private String sKey;
    private long txnDate;
    private double price;                  //
    private double brokerage;              // Commission
    private byte txnType;                // Buy / Sell / Opening Balance
    private int holding;                // Qty
    private String currency;
    private String memo;
    private long pfID;                   // PF ID that belongs this transaction
    private Hashtable<Long, Integer> soldQuantities;    // ammounts sold from this txn's total quantittiy in different sell trnsactions
    private Hashtable<Long, Integer> boughtQuantities;    // ammounts sold from this txn's total quantittiy in different sell trnsactions
    private int remainingHolding;
    private int soldHolding = 0;
    private int sellTempVal = 0;
    private String splitAdjustments = "";
    private boolean isSplitAdjusted = false;
    private boolean isFirstTime = false;
    private boolean isSellLocked = false;
    private String comissionID = "";

    public TransactRecord() {
        transID = System.currentTimeMillis(); //txn_ID;
        soldQuantities = new Hashtable();
        boughtQuantities = new Hashtable();
        isFirstTime = true;
    }

    public int getSoldHolding() {
        return soldHolding;
    }

    public void adjustTimestamp(int i) {
        transID = transID + i;
    }

    public double getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(double brokerage) {
        this.brokerage = brokerage;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getQuantity() {
        return holding;

    }

    public void setQuantity(int holding) {
        this.holding = holding;

        remainingHolding = holding - soldHolding;

    }

    public String getComissionID() {
        return comissionID;
    }

    public void setComissionID(String comissionID) {
        this.comissionID = comissionID;
    }

    public double getTransactionCost() {
//        if(isSplitAdjusted){
//            return remainingHolding*price + brokerage;
//        }else{

        return holding * price + brokerage;
        //   }


    }

    public int getQtyforWA() {
//        if(isSplitAdjusted){
//            return remainingHolding;
//        }else{
        return holding;
        //   }
    }

    public long getId() {
        return transID;
    }

    public long getPfID() {
        return pfID;
    }

    public void setPfID(long pfID) {
        this.pfID = pfID;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSKey() {
        return sKey;
    }

    public void setSKey(String sKey) {
        this.sKey = sKey;
    }

    public long getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(long txnDate) {
        this.txnDate = txnDate;
    }

    public byte getTxnType() {
        return txnType;
    }

    public void setTxnType(byte txnType) {
        this.txnType = txnType;
    }

    public int getSellTempVal() {
        return sellTempVal;
    }

    public void setSellTempVal(int sellTempVal) {
        this.sellTempVal = sellTempVal;
    }

    public boolean sellQuantityInCache(long sellRecord, int qty) {
        if (remainingHolding - qty >= 0) {
            soldHolding += qty;
            if (txnType == PFUtilities.BUY && qty > 0) {
                soldQuantities.put(sellRecord, qty);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean sellQuantity(long sellRecord, int qty) {
//        if(soldHolding+qty<=holding ){
//            soldHolding += qty;
//            remainingHolding = holding-soldHolding;
        if (remainingHolding - qty >= 0) {
            soldHolding += qty;
            remainingHolding = remainingHolding - qty;

            if (txnType == PFUtilities.BUY && qty > 0) {

                soldQuantities.put(sellRecord, qty);
                return true;


            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    public boolean editSellQuantity(long sellRecord, int qty) {


        boolean keyexsists = soldQuantities.containsKey(new Long(sellRecord));
        if (keyexsists) {
            Integer val = soldQuantities.get(new Long(sellRecord));
            if (keyexsists && qty == 0) {
                //  Integer val = soldQuantities.get(new Long(sellRecord));
                soldHolding -= val;
                remainingHolding = holding - soldHolding;
                soldQuantities.remove(new Long(sellRecord));
                return true;

            } else if (keyexsists && val > qty) {

                soldHolding = soldHolding - (val - qty);
                remainingHolding = holding - soldHolding;
                soldQuantities.put(sellRecord, qty);
                return true;
            } else if (keyexsists && val < qty) {

                soldHolding = soldHolding + (qty - val);
                remainingHolding = holding - soldHolding;
                soldQuantities.put(sellRecord, qty);
                return true;
            }


            return false;

        } else {
            return sellQuantity(sellRecord, qty);
        }


    }

    public boolean deleteSellQuantity(long buyrec) {

        boolean keyexsists = soldQuantities.containsKey(new Long(buyrec));
        if (keyexsists) {
            Integer val = soldQuantities.get(new Long(buyrec));
            soldHolding -= val;
            remainingHolding = holding - soldHolding;
            soldQuantities.remove(new Long(buyrec));
            return true;


        } else {
            return false;
        }


    }

    public Integer getSpecificSellQty(long trID) {

        Integer val = soldQuantities.get(new Long(trID));
        if (val != null) {
            return val;
        } else {
            return null;
        }
    }

    public int getRemainingHolding() {
        return remainingHolding;
//        if(txnType == PFUtilities.BUY){
//           remainingHolding = holding-soldHolding;
//            return remainingHolding;
//       }else{
//           return 0;
//       }
    }

    public void setRemainingHolding(int remainingHolding) {
        this.remainingHolding = remainingHolding;
    }

    public void putBuyQty(long buyRecord, int qty) {
//         String sp ="F";
//        if(isSplit){
//            sp="T";
//        }
//
//        String qtyString = qty+SPLIT_DATA_SEPERATOR+sp;
        boughtQuantities.put(buyRecord, qty);

    }

    public boolean isAnyBuyRecoedSplit() {

        Enumeration en = boughtQuantities.keys();
        while (en.hasMoreElements()) {
            TransactRecord rec = PFStore.getInstance().getTransaction((Long) en.nextElement());
            if (rec != null) {
                if (rec.isSplitAdjusted()) {
                    return true;
                }
            }
        }
        return false;

    }

    public double getRealizedGain() {
        double value = 0;
        long totQty = 0;
        long sellID = 0;
        long sellQty = 0;
        Enumeration<Long> sells = soldQuantities.keys();
        while (sells.hasMoreElements()) {
            try {
                sellID = sells.nextElement();
                sellQty = soldQuantities.get(sellID);
                value += PFStore.getInstance().getTransaction(sellID).getPrice() * sellQty;
                totQty += sellQty;
            } catch (Exception e) {
                e.printStackTrace();
            }
            sellID = 0;
        }
        // return ((getPrice() * totQty) - value) ;
        return (value - (getPrice() * totQty));
    }

    public void retrieveTransaction(String sData) {
        String sKey = null;
        StringTokenizer st = new StringTokenizer(sData, "|", false);
        sKey = st.nextToken();
        if (sKey.equals("null")) {
            sKey = null;
        }
        if (sKey != null && !SharedMethods.isFullKey(sKey)) {
            sKey = sKey + "~0";
        }
        if ((sKey != null) && (!PortfolioInterface.isContainedSymbol(sKey))) {
            //  PortfolioInterface.setSymbol(true, sKey);
        }
        this.setSKey(sKey);
        this.setTxnDate(Long.parseLong(st.nextToken()));
        this.setPrice(Float.parseFloat(st.nextToken()));
        this.setBrokerage(Float.parseFloat(st.nextToken()));
        this.setTxnType(Byte.parseByte(st.nextToken()));
        this.setQuantity(Integer.parseInt(st.nextToken()));
        this.setPfID(Long.parseLong(st.nextToken()));
        this.transID = Long.parseLong(st.nextToken());
        // this.setRemainingHolding(Integer.parseInt(st.nextToken()));
        //  this.soldHolding = Integer.parseInt(st.nextToken());
        try {
            this.setSellLocked(Boolean.parseBoolean(st.nextToken()));
        } catch (Exception e) {
            this.setSellLocked(false);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            sKey = st.nextToken();
            if (sKey.equals("null"))
                sKey = "";
            this.setMemo(sKey);
        } catch (Exception ex) {
            this.setMemo("");
        }
        try {
            this.setComissionID(st.nextToken());
        } catch (Exception e) {
            this.setComissionID(Language.getString("MANUAL"));
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            this.setCurrency(st.nextToken());
        } catch (Exception e) {
            try {
                String currency = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getActiveCurrency();
                this.setCurrency(currency);
            } catch (Exception e1) {

                this.setCurrency(PFStore.getBaseCurrency());
            }

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            processFIFOSellString(st.nextToken());
        } catch (Exception e) {
            // e.printStackTrace();
        }
        try {
            processFIFOByuString(st.nextToken());
        } catch (Exception e) {
            // e.printStackTrace();
        }
        try {
            isSplitAdjusted = (Boolean.parseBoolean(st.nextToken()));
        } catch (Exception e) {
            isSplitAdjusted = false;
            //  e.printStackTrace();
        }
        try {
            sKey = st.nextToken();
            if (sKey.equals("null"))
                sKey = "";
            this.splitAdjustments = sKey;
        } catch (Exception ex) {
            this.splitAdjustments = "";
        }
        sKey = null;
        st = null;
    }

    public String toString(byte fileType) {
        String delim = "";
        switch (fileType) {
            case PFUtilities.DATA:
                delim = "|";
                break;
            case PFUtilities.TXT:
                delim = "\t";
                break;
            case PFUtilities.CSV:
                delim = ",";
                break;
            case PFUtilities.QIF:
                delim = "|";
                break;
        }
        if (fileType == PFUtilities.DATA) {
            String dataStr = PFUtilities.TRANSACTION_RECORD + delim;
            if ((sKey == null) || (sKey.equals(""))) {
                dataStr += "null" + delim;
            } else {
                dataStr += sKey + delim;
            }
            dataStr += txnDate + delim + formatter().format(price) + delim + formatter().format(brokerage) + delim + txnType + delim +
                    holding + delim + pfID + delim + transID + delim + isSellLocked + delim;                 /*remainingHolding + delim + soldHolding + delim +*/
            if ((memo == null) || (memo.equals(""))) {
                dataStr += "null" + delim;
            } else {
                dataStr += memo + delim;
            }
            if ((comissionID == null) || (comissionID.equals(""))) {
                dataStr += "null" + delim;
            } else {
                dataStr += comissionID + delim;
            }
            dataStr += currency + delim + getFIFOSellQtyString() + delim + getFIFOBuyQtySring() + delim;
            if (splitAdjustments.equals("")) {
                splitAdjustments = "null";
            }
            dataStr += isSplitAdjusted + delim + splitAdjustments + "\n";
            return dataStr;
//            return PFUtilities.TRANSACTION_RECORD + delim + sKey + delim + txnDate + delim + price + delim + brokerage + delim + txnType + delim +
//                    holding + delim + pfID + delim + transID + delim + memo + delim + currency + delim + "\n";

        } else if (fileType == PFUtilities.CSV) {
            String dataStr = "";
            String symbol = (sKey == null) || (sKey.equalsIgnoreCase("null")) ? "" : sKey;
            String sym = getSymbol();
            String exchg = getExchange();
            dataStr += sym + delim + exchg + delim + PFUtilities.getFormattedDateWithTime(txnDate) + delim +
                    formatter().format(price) + delim +
                    formatter().format(brokerage) + delim +
                    PFUtilities.getTxnTypeString(txnType) + delim +
                    holding + delim;
            if (memo == null) {
                dataStr += "" + delim;
            } else {
                dataStr += memo + delim;
            }
            dataStr += currency + "\r\n";
            return dataStr;
        } else if (fileType == PFUtilities.TXT) {
            String dataStr = "";
            String symbol = (sKey == null) || (sKey.equalsIgnoreCase("null")) ? "" : sKey;

            dataStr += symbol + delim + PFUtilities.getFormattedDateWithTime(txnDate) + delim +
                    //PFUtilities.getFormattedPrice(price) + delim +
                    formatter().format(price) + delim +
                    // PFUtilities.getFormattedPrice(brokerage) + delim +
                    formatter().format(brokerage) + delim +
                    PFUtilities.getTxnTypeString(txnType) + delim +
                    PFUtilities.getFormattedQty(holding) + delim;
            if (memo == null) {
                dataStr += "" + delim;
            } else {
                dataStr += memo + delim;
            }
            dataStr += currency + delim + "\r\n";
            return dataStr;
        } else if (fileType == PFUtilities.QIF) {
            String dataStr = "";
            dataStr += "D" + PFUtilities.getFormattedQIFDate(txnDate) + "\r\n";
            dataStr += "N" + PFUtilities.getQIFTxnTypeString(txnType) + "\r\n";
            dataStr += "I" + PFUtilities.getFormattedQIFPrice(price) + "\r\n";
            dataStr += "Q" + PFUtilities.getFormattedQIFQty(holding) + "\r\n";
            dataStr += "Y" + PortfolioInterface.getSymbolFromKey(sKey) + "\r\n";
            dataStr += "O" + PFUtilities.getFormattedQIFPrice(brokerage) + "\r\n";
            if (memo != null) {
                dataStr += "M" + memo + "\r\n";
            }
            double totCost = 0d;
            if ((txnType == PFUtilities.BUY) || (txnType == PFUtilities.OP_BAL)) {
                totCost = price * holding + brokerage;
            } else if (txnType == PFUtilities.SELL) {
                totCost = price * holding - brokerage;
            }
            dataStr += "T" + PFUtilities.getFormattedQIFPrice(totCost) + "\r\n";
//            dataStr += "C" + currency + "\r\n";
            dataStr += "^" + "\r\n";
            return dataStr;
        }
        return "";
    }

    public TWDecimalFormat formatter() {
        TWDecimalFormat format;
        format = SharedMethods.getDecimalFormatNoCommaNoSpace(sKey);
        if (format != null) {
            return format;
        } else {
            return new TWDecimalFormat("##0.00");
        }

    }

    private String getSymbol() {
        if (sKey == null) {
            return "";
        }
        Stock stock = PortfolioInterface.getStockObject(getSKey());
        if (stock != null) {
            return PortfolioInterface.getSymbol(stock);
        } else {
            return "";
        }

    }

    private String getExchange() {
        if (sKey == null) {
            return "";
        }
        Stock stock = PortfolioInterface.getStockObject(getSKey());
        if (stock != null) {
            return PortfolioInterface.getExchangeCode(stock);
        } else {
            return "";
        }

    }

    public String getFIFOSellQtyString() {
        if (!soldQuantities.isEmpty()) {
            String dlim = "#";
            String recdlim = ">";
            String data = "";
            Enumeration<Long> enu = soldQuantities.keys();
            while (enu.hasMoreElements()) {
                Long id = enu.nextElement();
                Integer qty = soldQuantities.get(id);
                if (id > 0 && qty > 0) {
                    data = data + id.toString() + recdlim + qty.toString();
                }
                if (enu.hasMoreElements()) {
                    data = data + dlim;
                }
            }
            return data;
        } else {
            return "null";
        }
    }

    public String getFIFOBuyQtySring() {

        if (!boughtQuantities.isEmpty()) {
            String dlim = "#";
            String recdlim = ">";
            String data = "";
            Enumeration<Long> enu = boughtQuantities.keys();
            while (enu.hasMoreElements()) {
                Long id = enu.nextElement();
                Integer qty = boughtQuantities.get(id);
                if (id > 0 && qty > 0) {
                    data = data + id.toString() + recdlim + qty.toString();
                }
                if (enu.hasMoreElements()) {
                    data = data + dlim;
                }
            }
            return data;
        } else {
            return "null";
        }


    }

    public void processFIFOSellString(String FIFOsell) {
        if (this.txnType == PFUtilities.BUY && FIFOsell != null && !FIFOsell.equals("null")) {

            if (soldQuantities == null) {
                soldQuantities = new Hashtable<Long, Integer>();
            }
            //   StringTokenizer st = new StringTokenizer(FIFOsell, "#", false);
            String[] records = FIFOsell.split("#");
            for (int i = 0; i < records.length; i++) {

                String[] pair = records[i].split(">");
                if (pair.length == 2) {
                    try {
                        Long id = Long.parseLong(pair[0]);
                        Integer qty = Integer.parseInt(pair[1]);
                        this.sellQuantity(id, qty);

                        //   soldQuantities.put(id,qty);

                    } catch (Exception e) {
                        System.out.println("Record Loading error ");

                    }
                }
            }
        }


    }

    public void processFIFOByuString(String FIFOsell) {
        if (this.txnType == PFUtilities.SELL && FIFOsell != null && !FIFOsell.equals("null")) {

            if (boughtQuantities == null) {
                boughtQuantities = new Hashtable<Long, Integer>();
            }
            //   StringTokenizer st = new StringTokenizer(FIFOsell, "#", false);
            String[] records = FIFOsell.split("#");
            for (int i = 0; i < records.length; i++) {

                String[] pair = records[i].split(">");
                if (pair.length == 2) {
                    try {
                        Long id = Long.parseLong(pair[0]);
                        Integer qty = Integer.parseInt(pair[1]);
                        this.putBuyQty(id, qty);

                        //   soldQuantities.put(id,qty);

                    } catch (Exception e) {
                        System.out.println("Record Loading error ");

                    }
                }
            }
        }


    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public boolean isSplitAdjusted() {
        return isSplitAdjusted;
    }

    public void appendSplitData(long id, double prevPrice, long prevQty) {
        String data = id + SPLIT_DATA_SEPERATOR + prevPrice + SPLIT_DATA_SEPERATOR + prevQty + SPLIT_RECORD_SEPERATOR;
        splitAdjustments = data + splitAdjustments;
        isSplitAdjusted = true;
    }

    public void removeSplitData(long id) {
        String[] records = splitAdjustments.split("\\" + SPLIT_RECORD_SEPERATOR);
        String newString = "";
        String[] data = null;
        boolean foundSplit = false;
        for (String record : records) {
            if (foundSplit) {
                newString = newString + record + SPLIT_RECORD_SEPERATOR;
            }
            data = record.split("\\|");
            if (data[0].equals("" + id)) {
                foundSplit = true;
            }
        }
        splitAdjustments = newString;
        if (newString.equals("")) {
            isSplitAdjusted = false;
        }
    }

    public boolean isSellLocked() {
        return isSellLocked;
    }

    public void setSellLocked(boolean sellLocked) {
        isSellLocked = sellLocked;
    }

    public void LockSellTransactions() {

        if (!soldQuantities.isEmpty()) {

            Enumeration en = soldQuantities.keys();
            while (en.hasMoreElements()) {
                Long pfId = (Long) en.nextElement();
                TransactRecord sell = PFStore.getInstance().getTransaction(pfId);
                if (sell != null) {
                    sell.setSellLocked(true);
                }


            }

        }


    }
}
