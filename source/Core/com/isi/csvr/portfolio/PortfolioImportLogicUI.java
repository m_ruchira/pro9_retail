package com.isi.csvr.portfolio;

import com.isi.csvr.Client;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.shared.TWComboBox;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 13, 2008
 * Time: 11:19:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioImportLogicUI extends JPanel implements ActionListener, MouseListener {


    private static final String SYM_TABLE = "sym";
    private static final String CUMELATIVE_TABE = "cumelative";
    private static final String TXN_TABLE = "txn";
    //pannels
    private JPanel topPanel;
    private JPanel tablePanel;
    private JPanel bottomPanel;
    private JPanel parameterPanel;
    private JPanel advParamsPanel;
    private JPanel btnPanel;
    private JPanel messagePanel;

    // components
    private Table validatedTable;
    private Table errorTable;
    private Table cumilativeValidatedTable;
    private JSpinner startFrom;
    private TWComboBox dateFormat;
    private TWButton cancel;
    private TWButton validate;
    private TWButton next;
    private TWButton finalImport;

    //lables
    private JLabel lblstartFrom;
    private JLabel lbldateFormat;
    // private JLabel lblError;
    private JTextPane lblError;

    private PortfolioLogicImportModel logicmodel;
    private PortfolioLogicImportModel validtedlogicmodel;
    private PortfolioLogicImportModel errorlogicmodel;
    private PortfolioImportModel importmodelSymVal;
    private ViewSetting oSettings;
    private ArrayList validatedSymWiseList;
    private Hashtable errorSymWiseTable;
    private String currentpannel;

    private InternalFrame errorFrame;

    public PortfolioImportLogicUI() {

        initUI();
        errorSymWiseTable = new Hashtable();


    }

    private void initUI() {

        initComponents();
        setLables();
        addComponents();

        addListeners();
        //    this.setTitle(Language.getString("TRANSACTION_VALIDATION"));
        //    this.setResizable(true);
        ///    this.setClosable(true);
        //    this.setMaximizable(false);
        //    this.setIconifiable(true);
        //    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //    setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        //   this.setSize(700, 350);


    }

    public void showwindow() {
        this.setVisible(true);
        //  this.setLocationRelativeTo(Client.getInstance().getDesktop());

    }

    private void initComponents() {

        topPanel = new JPanel(new FlexGridLayout(new String[]{"450", "200"}, new String[]{"30"}, 4, 4, true, true));
        tablePanel = new JPanel();
        bottomPanel = new JPanel(new BorderLayout());
        parameterPanel = new JPanel(new FlexGridLayout(new String[]{"100", "90", "10", "100", "90"}, new String[]{"22"}, 4, 4, true, true));
        advParamsPanel = new JPanel();
        btnPanel = new JPanel(new FlexGridLayout(new String[]{"90", "90", "90"}, new String[]{"25"}, 4, 4, true, true));
        messagePanel = new JPanel();
        //   validatedTable = new Table();
        validatedTable = new Table();

        //  createEditableTable();
        createValidatedTable();
        createCumilativeValidatedTable();

        startFrom = new JSpinner(new SpinnerNumberModel(1, 0, 10, 1));
        dateFormat = new TWComboBox();
        cancel = new TWButton();
        validate = new TWButton();
        finalImport = new TWButton();
        next = new TWButton();


        lbldateFormat = new JLabel();
        lblstartFrom = new JLabel();
        lblError = new JTextPane();
        lblError.setPreferredSize(new Dimension(500, 60));
        lblError.setEditable(false);
        lblError.setOpaque(false);
    }

    private void setLables() {

        cancel.setText(Language.getString("CANCEL"));
        validate.setText(Language.getString("VALIDATE"));
        finalImport.setText(Language.getString("IMPORT"));
        next.setText(Language.getString("NEXT"));
        lbldateFormat.setText(Language.getString("DATE_FORMAT"));
        lblstartFrom.setText(Language.getString("START_FROM"));
    }

    private void addComponents() {
        parameterPanel.add(lblstartFrom);
        parameterPanel.add(startFrom);
        parameterPanel.add(new JLabel());
        parameterPanel.add(lbldateFormat);
        parameterPanel.add(dateFormat);

        topPanel.add(parameterPanel);
        topPanel.add(advParamsPanel);

        tablePanel.setLayout(new CardLayout(1, 1));
        //   tablePanel.setLayout(new BorderLayout());

        //  btnPanel.add(validate);
        btnPanel.add(next);
        //next.setEnabled(false);
        btnPanel.add(finalImport);
        btnPanel.add(cancel);
        //messagePanel.setPreferredSize(new Dimension(200, 60));
        // lblError.setPreferredSize(new Dimension(200,60));
        messagePanel.add(lblError);
        lblError.setText(Language.getString("PFVAL_TXN_MESSAGE"));
        // messagePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        // btnPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        bottomPanel.add(messagePanel, BorderLayout.WEST);
        bottomPanel.add(btnPanel, BorderLayout.EAST);

        //  tablePanel.setBorder(BorderFactory.createLineBorder(Color.blue, 3));
        tablePanel.add(SYM_TABLE, validatedTable);
        tablePanel.add(CUMELATIVE_TABE, cumilativeValidatedTable);

        //   next.setEnabled(false);
        finalImport.setEnabled(false);

        createErrorFrame();
        this.setLayout(new BorderLayout());
        // this.add(topPanel, BorderLayout.NORTH);
        this.add(tablePanel, BorderLayout.CENTER);
        this.add(bottomPanel, BorderLayout.SOUTH);
        showPannel(SYM_TABLE);


    }

    private void createErrorFrame() {
        errorFrame = new InternalFrame();
        errorFrame.setTitle(Language.getString("ERROR_LIST"));
        errorFrame.setSize(new Dimension(400, 200));
        errorFrame.setMaximizable(false);
        errorFrame.setResizable(true);
        errorFrame.setClosable(true);
        errorFrame.setIconifiable(false);
        errorFrame.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        errorFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        createErrorTable();
        errorFrame.getContentPane().setLayout(new BorderLayout());
        errorFrame.getContentPane().add(errorTable, BorderLayout.CENTER);
        Client.getInstance().getDesktop().add(errorFrame);


    }

    private void addListeners() {
        next.addActionListener(this);
        validate.addActionListener(this);
        finalImport.addActionListener(this);
        cancel.addActionListener(this);
    }


    private void createValidatedTable() {
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "8").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
//            if (sID == null)
//                oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//            else
//                oSettings.setID(store.getId());
//            oSettings.setCaptions(sNewName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        //String sColOrder = Settings.getItem("Gio");
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_IMPORT_COLUMNS"));
        oSettings.setTableNumber(1);
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");
        validatedTable = new Table();


        validatedTable.setWindowType(ViewSettingsManager.PORTFOLIO_IMPORT);
        logicmodel = new PortfolioLogicImportModel(PortfolioLogicImportModel.NORMAL);
        logicmodel.setViewSettings(oSettings);
        oSettings.setParent(this);
        //validatedTable.setImportPortfolioEditableHeaderModel(importmodel,itemlist);
        validatedTable.setModel(logicmodel);
        logicmodel.setTable(validatedTable);
        Theme.registerComponent(validatedTable);
        validatedTable.getModel().updateGUI();
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);


    }

    public void createCumilativeValidatedTable() {
        ViewSetting oSettings;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "9").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
//            if (sID == null)
//                oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//            else
//                oSettings.setID(store.getId());
//            oSettings.setCaptions(sNewName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        //String sColOrder = Settings.getItem("Gio");
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_CUMQTY_COLUMNS"));
        oSettings.setTableNumber(1);
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");
        cumilativeValidatedTable = new Table();


        // validatedTable.setWindowType(ViewSettingsManager.PORTFOLIO_IMPORT);
        validtedlogicmodel = new PortfolioLogicImportModel(PortfolioLogicImportModel.VALIDATED);
        validtedlogicmodel.setViewSettings(oSettings);
        oSettings.setParent(this);
        //validatedTable.setImportPortfolioEditableHeaderModel(importmodel,itemlist);
        cumilativeValidatedTable.setModel(validtedlogicmodel);
        validtedlogicmodel.setTable(cumilativeValidatedTable);
        Theme.registerComponent(cumilativeValidatedTable);
        cumilativeValidatedTable.getTable().setDefaultRenderer(Object.class, new PortfolioImportLogicRenderer(validtedlogicmodel.getRendIDs()));
        cumilativeValidatedTable.getTable().setDefaultRenderer(Number.class, new PortfolioImportLogicRenderer(validtedlogicmodel.getRendIDs()));
        cumilativeValidatedTable.getTable().setDefaultRenderer(Boolean.class, new PortfolioImportLogicRenderer(validtedlogicmodel.getRendIDs()));
        cumilativeValidatedTable.getTable().addMouseListener(this);
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);

    }

    private void createErrorTable() {

        ViewSetting oSettings;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "10").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
//            if (sID == null)
//                oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//            else
//                oSettings.setID(store.getId());
//            oSettings.setCaptions(sNewName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        //String sColOrder = Settings.getItem("Gio");
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_CUMQTY_ERROR_COLUMNS"));
        oSettings.setTableNumber(1);
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");
        errorTable = new Table();


        // validatedTable.setWindowType(ViewSettingsManager.PORTFOLIO_IMPORT);
        errorlogicmodel = new PortfolioLogicImportModel(PortfolioLogicImportModel.ERROR);
        errorlogicmodel.setViewSettings(oSettings);
        oSettings.setParent(this);
        //validatedTable.setImportPortfolioEditableHeaderModel(importmodel,itemlist);
        errorTable.setModel(errorlogicmodel);
        errorlogicmodel.setTable(errorTable);
        errorTable.getTable().setDefaultRenderer(Object.class, new PortfolioImportLogicRenderer(errorlogicmodel.getRendIDs()));
        errorTable.getTable().setDefaultRenderer(Number.class, new PortfolioImportLogicRenderer(errorlogicmodel.getRendIDs()));
        errorTable.getTable().setDefaultRenderer(Boolean.class, new PortfolioImportLogicRenderer(errorlogicmodel.getRendIDs()));
        Theme.registerComponent(errorTable);
        errorTable.getModel().updateGUI();

        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);

    }

    public void setDataStoreInModel(ArrayList dataStore) {
        logicmodel.setDataStore(dataStore);
        validatedTable.getModel().updateGUI();

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == next) {
            next.setEnabled(false);
            PFImportDataStore.getSharedInstance().validateTransactions();

        } else if (e.getSource() == finalImport) {
            if (validatedSymWiseList.size() == errorSymWiseTable.size()) {

            } else {
                if (!errorSymWiseTable.isEmpty()) {
                    int i = -1;
                    PortfolioImport.getSharedInstance().hideMessageFrame();
                    i = PortfolioImport.getSharedInstance().confirmSkip();
                    if (i == JOptionPane.OK_OPTION) {
                        PFImportDataStore.getSharedInstance().addTransactionToPFStore();
                        //    new ShowMessage(Language.getString("MSG_IMPORT_SUCCES") , "I");
                        PortfolioImport.getSharedInstance().cancelPressed();

                    }
                } else {
                    PFImportDataStore.getSharedInstance().addTransactionToPFStore();
                    //   new ShowMessage(Language.getString("MSG_IMPORT_SUCCES") , "I");
                    PortfolioImport.getSharedInstance().cancelPressed();

                }
//                PFImportDataStore.getSharedInstance().addTransactionToPFStore();
//                new ShowMessage(Language.getString("MSG_IMPORT_SUCCES") , "I");
//                PortfolioImport.getSharedInstance().cancelPressed();

            }
        } else if (e.getSource() == cancel) {
            PortfolioImport.getSharedInstance().cancelPressed();
        }
    }

    public void setValidatedDataStores(ArrayList list, Hashtable table) {
        validatedSymWiseList = list;
        errorSymWiseTable = table;
        validtedlogicmodel.setDataStore(validatedSymWiseList);
        PortfolioImport.getSharedInstance().setTitleForFinalImport();
        if (validatedSymWiseList != null && !validatedSymWiseList.isEmpty()) {
            //  this.setTitle(Language.getString("CUMILATIVE_EFFECT"));
            showPannel(CUMELATIVE_TABE);
            lblError.setText(Language.getString("PFVAL_TXNFINAL_MESSAGE"));
            cumilativeValidatedTable.getTable().updateUI();
            if (validatedSymWiseList.size() == errorSymWiseTable.size()) {
                finalImport.setEnabled(false);

            } else {
                finalImport.setEnabled(true);
                PortfolioImport.getSharedInstance().setReadyForNextPhase(true);
                PortfolioImport.getSharedInstance().setCompletedPhase(PFImportDataStore.VALIDATION_CUMILATIVE_COMPLETE);
            }
        }

    }

    private void showPannel(String panelid) {
        CardLayout cl = (CardLayout) (tablePanel.getLayout());
        cl.show(tablePanel, panelid);
        currentpannel = panelid;

    }

    public void mouseClicked(MouseEvent e) {

        if (currentpannel == CUMELATIVE_TABE && e.getClickCount() > 1) {
            String sKey = getSelectedSymbol();
            showErrorTable(sKey);

        }

    }

    private String getSelectedSymbol() {
        int row = cumilativeValidatedTable.getTable().getSelectedRow();
        String sKey = (String) cumilativeValidatedTable.getModel().getValueAt(row, -2);
        return sKey;
    }

    public void showErrorTable(String sKey) {
        if (errorSymWiseTable.containsKey(sKey)) {

            errorlogicmodel.setDataStore((ArrayList) errorSymWiseTable.get(sKey));
            errorFrame.setVisible(true);
            errorFrame.setLocationRelativeTo(Client.getInstance().getDesktop());

        }

    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void clickNext() {
        next.doClick();
    }

    public void clickFinalImport() {
        finalImport.doClick();
    }

}
