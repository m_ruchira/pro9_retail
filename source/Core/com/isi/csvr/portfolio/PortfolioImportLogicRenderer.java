package com.isi.csvr.portfolio;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 13, 2008
 * Time: 9:20:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioImportLogicRenderer extends TWBasicTableRenderer {

    private static Color g_oUpFGColor;
    private static Color g_oDownFGColor;
    private static Color g_oUpBGColor;
    private static Color g_oDownBGColor;
    private static Color g_oUpColor;
    private static Color g_oDownColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oBuyBGColor;
    private static Color g_oSellBGColor;
    private static Color g_oOpBalBGColor;
    private static Color g_oBuyFGColor;
    private static Color g_oSellFGColor;
    private static Color g_oOpBalFGColor;
    private static Color importError;
    private static Border selectedBorder;
    private static Border unselectedBorder;
    private static Border border;
    private static ImageIcon upImage = null;
    private static ImageIcon downImage = null;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private String g_sNA = "NA";
    private Color upColor;
    private Color downColor;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private TWDecimalFormat oChangeFormat;
    private TWDecimalFormat oPChangeFormat;
    private TWDateFormat g_oDateFormat;
    private TWDateFormat g_oDateTimeFormatHM;
    private TWDateFormat g_oDateTimeFormatHMS;
    private TWDateFormat g_oTimeFormat;
    private Double floatValue;
    private Double[] floatValuePair;
    private long longValue;
    private int intValue;
    private Date date;
    private Color foreground = null;
    private Color background = null;
    private String sBuyType = null;
    private String sSellType = null;
    private String sOpBalType = null;
    private String sDIvidendType = null;
    private String sDepositeType = null;
    private String sWithdrawalType = null;
    private String sChargesType = null;
    private String sRefundsType = null;
    private String sTxType = null;
    private int rowCount;
    private boolean isLastRow;
    private boolean isValuationType = false;
    private int iRendID = 0;

    private byte decimalPlaces = Constants.TWO_DECIMAL_PLACES;
    private byte tableDecimals = Constants.TWO_DECIMAL_PLACES;
    private byte stockDecimals = Constants.TWO_DECIMAL_PLACES;

    public PortfolioImportLogicRenderer(int[] asRendIDs) {
        date = new Date();

        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oChangeFormat = new TWDecimalFormat(" ###,##0.00 ");
        oPChangeFormat = new TWDecimalFormat(" ###,##0.00 ");

        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HM_FORMAT"));//" yyyy/MM/dd HH:mm ");
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));//" dd:MM:yyyy '-' HH:mm:ss ");
        g_oTimeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));//" HH:mm:ss ");

        g_asRendIDs = asRendIDs;
        reload();

        sBuyType = Language.getString("BUY");
        sSellType = Language.getString("SELL");
        sOpBalType = Language.getString("OPENING_BAL");
        sDIvidendType = Language.getString("DIVIDEND");
        sDepositeType = Language.getString("DEPOSITES");
        sWithdrawalType = Language.getString("WITHDROWALS");
        sChargesType = Language.getString("CHARGES");
        sRefundsType = Language.getString("REFUNDS");
        g_sNA = Language.getString("NA");

        try {
            upImage = new ImageIcon("images/Common/up.gif");
        } catch (Exception e) {
            upImage = null;
        }
        try {
            downImage = new ImageIcon("images/Common/down.gif");
        } catch (Exception e) {
            downImage = null;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;

        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oUpColor = Color.black;
        g_oDownColor = Color.black;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
        g_oBuyBGColor = Color.white;
        g_oSellBGColor = Color.white;
        g_oOpBalBGColor = Color.white;
        g_oBuyFGColor = Color.black;
        g_oSellFGColor = Color.black;
        g_oOpBalFGColor = Color.black;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");

            g_oBuyBGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_BUY_BGCOLOR");
            g_oSellBGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_SELL_BGCOLOR");
            g_oOpBalBGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR");
            g_oBuyFGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_BUY_FGCOLOR");
            g_oSellFGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_SELL_FGCOLOR");
            g_oOpBalFGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_FGCOLOR");

            importError = Theme.getColor("PF_IMPORT_INVALIDATED");

            selectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oSelectedFG);
            unselectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oFG1);
        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            g_oBuyBGColor = Color.white;
            g_oSellBGColor = Color.white;
            g_oOpBalBGColor = Color.white;
            g_oBuyFGColor = Color.black;
            g_oSellFGColor = Color.black;
            g_oOpBalFGColor = Color.black;
            importError = Color.red;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void propertyChanged(int property) {
    }

    public void setValuationType() {
        isValuationType = true;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);


        try {

            boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
            CommonTableSettings sett = null;
            if (isCustomThemeEnabled) {
                sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
                if (isSelected) {
                    foreground = sett.getSelectedColumnFG();
                    background = sett.getSelectedColumnBG();
                } else {
                    if (row % 2 == 0) {
                        foreground = sett.getRowColor1FG();
                        background = sett.getRowColor1BG();
                    } else {
                        foreground = sett.getRowColor2FG();
                        background = sett.getRowColor2BG();
                    }
                }
                upColor = sett.getPositiveChangeFG();
                downColor = sett.getNegativeChangeFG();
            } else {
                if (isSelected) {
                    foreground = g_oSelectedFG;
                    background = g_oSelectedBG;
                    border = selectedBorder;
                } else if (row % 2 == 0) {
                    foreground = g_oFG1;
                    background = g_oBG1;
                    border = unselectedBorder;
                } else {
                    foreground = g_oFG2;
                    background = g_oBG2;
                    border = unselectedBorder;
                }
                upColor = g_oUpColor;
                downColor = g_oDownColor;
            }

            Boolean b = (Boolean) table.getModel().getValueAt(row, -1);
            if (!b && !isSelected) {
                background = importError;
            }

            lblRenderer.setForeground(foreground);
            lblRenderer.setBackground(background);


            iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];


            switch (iRendID) {
                case 0: // DEFAULT
                    if (value == null)
                        lblRenderer.setText(g_sNA);
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                case 2: // DESCRIPTION
                    if (value == null)
                        lblRenderer.setText(g_sNA);
                    else {
                        if (Settings.isShowArabicNumbers())
                            lblRenderer.setText(GUISettings.arabize((String) value));
                        else
                            lblRenderer.setText((String) value);
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    try {
                        lblRenderer.setText(oPriceFormat.format(((Double) value).doubleValue()));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    } catch (Exception e) {
                        lblRenderer.setText("-");
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    }
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(((Long) (value)).longValue()));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 5: // CHANGE
                    floatValue = ((Double) value).doubleValue(); // toFloatValue(value);
                    lblRenderer.setText(oPriceFormat.format(floatValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (floatValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (floatValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 6: // % CHANGE
                    //adPrice = (float[])value;
                    floatValue = ((Double) value).doubleValue(); // toFloatValue(value);
                    lblRenderer.setText(oPriceFormat.format(floatValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (floatValue == Double.POSITIVE_INFINITY) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        if (floatValue > 0)
                            lblRenderer.setForeground(upColor);
                        else if (floatValue < 0)
                            lblRenderer.setForeground(downColor);
                    }
                    break;
                case 7: // DATE
                    try {
                        longValue = ((Long) value).longValue(); //toLongValue(value);
                        if (longValue == 0)
                            lblRenderer.setText(g_sNA);
                        else {
                            date.setTime(longValue); //+Settings.getTimeOffset(longValue));
                            lblRenderer.setText(g_oDateFormat.format(date));
                        }
                    } catch (Exception e) {
                        lblRenderer.setText("-");
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 8: {// DATE TIME
                    longValue = ((Long) value).longValue(); //toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue); //+Settings.getTimeOffset(longValue));
                        lblRenderer.setText(g_oDateTimeFormatHM.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                }
                case 'D': {// DATE TIME with secs
                    longValue = ((Long) value).longValue(); //toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue); //+Settings.getTimeOffset(longValue));
                        lblRenderer.setText(g_oDateTimeFormatHMS.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                }
                case 9: // TICK
                    intValue = (int) toLongValue(value);
                    switch (intValue) {
                        case Settings.TICK_UP:
                            lblRenderer.setIcon(upImage);
                            lblRenderer.setText("");
                            break;
                        case Settings.TICK_DOWN:
                            lblRenderer.setIcon(downImage);
                            lblRenderer.setText("");
                            break;
                        default:
                            lblRenderer.setIcon(null);
                            lblRenderer.setText("");
                    }
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    break;
                case 'A': // TIME
                    longValue = ((Long) value).longValue(); //toLongValue(value);
                    if (longValue == 0) {
                        lblRenderer.setText(g_sNA);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        date.setTime(longValue); //+Settings.getTimeOffset(longValue));
                        lblRenderer.setText(g_oTimeFormat.format(date));
                        lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    }
                    break;
                case 'B': // Broker ID
                    if ((value == null) || (((String) value)).equals("")
                            || (((String) value)).equals("null"))
                        lblRenderer.setText(g_sNA);
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 'P': // PRICE
                    floatValue = ((Double) value).doubleValue();
                    lblRenderer.setText(oPriceFormat.format(floatValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'p': // PRICE without decimals
                    floatValue = ((Double) value).doubleValue();
                    lblRenderer.setText(oQuantityFormat.format(floatValue));
                    break;
                case 'F': // PRICE
                    floatValuePair = (Double[]) value;
                    lblRenderer.setText(oPriceFormat.format(floatValuePair[0]));
                    //lblRenderer.setHorizontalAlignment(JLabel.RIGHT);
                    if (floatValuePair.length > 1) {
                        //System.out.println(" inside" );
                        if (floatValuePair[0] > floatValuePair[1]) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (floatValuePair[0] < floatValuePair[1]) {
                            lblRenderer.setBackground(g_oDownBGColor);
                            lblRenderer.setForeground(g_oDownFGColor);
                        } else {
                            if (floatValuePair[0] > 0)
                                lblRenderer.setForeground(upColor);
                            else if (floatValuePair[0] < 0)
                                lblRenderer.setForeground(downColor);
                            else
                                lblRenderer.setForeground(foreground);
                        }
                        floatValuePair[1] = floatValuePair[0];
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'T': // PORTFOLIO TRANSACTION TYPE
                    if (sTxType != null)
                        lblRenderer.setText(sTxType);
                    else
                        lblRenderer.setText(g_sNA);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                default:
                    lblRenderer.setText("");
            }

            if (isValuationType && column == 0 && isLastRow) {

                lblRenderer.add(new TWComboBox());
            }

        } catch (Exception e) {
            lblRenderer.setText("");
        }

        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    public Border getBorder(JTable table, boolean isLastRow, int column, Border border, Border cellBorder) {
        if ((isLastRow) && (table.convertColumnIndexToModel(column) >= 8) && (table.convertColumnIndexToModel(column) != 16)) {
            return border;
        } else {
            return cellBorder;
        }
    }

    private void applyDecimalPlaces(byte decimalPlaces) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                break;
            case Constants.ONE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case Constants.TWO_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case Constants.THREE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
        }
        this.decimalPlaces = decimalPlaces;
    }

    class IconLable extends JPanel {
        //        private ImageIcon icon;
//        private int width;
        private JLabel label;
        private ImageIcon imageicon;
        private ImageIcon imageiconL;
        private ImageIcon imageiconR;
        private ImageIcon imageicnBtn;
        private int imagewidth;
        private Color bgClor;


        public IconLable() {
            label = new JLabel();
            this.setLayout(new BorderLayout());
            this.add(label, BorderLayout.CENTER);

            try {
                imageicon = null;
                //imageTile = null;
                // imageGraphBG=null;
                imageicon = new ImageIcon(Theme.getTheamedImagePath("indextitletile"));
                imageiconL = new ImageIcon(Theme.getTheamedImagePath("indextitletileL"));
                imageiconR = new ImageIcon(Theme.getTheamedImagePath("indextitletileR"));
                imageicnBtn = new ImageIcon("images/Common/graph_down_pop.gif");

                //imageTile = new ImageIcon("images/theme" + Theme.getID()+"/indexpaneltile.png");
                //   imageGraphBG= new ImageIcon(Theme.getTheamedImagePath("indexGraphBG"));
                imagewidth = imageicon.getIconWidth();
//            imageTilewidth = imageTile.getIconWidth();
            } catch (Exception e) {
                imageicon = null;
                imagewidth = 0;
            }
        }

        public void setText(String text) {
            label.setText(text);
        }

        public void setBackground(Color bg) {
            // label.setBackground(bg);
            bgClor = bg;
        }

        public void setFont(Font font) {
            super.setFont(font);
            if (label != null)
                label.setFont(font);    //.deriveFont(Font.BOLD));
        }

        public void setForeground(Color fg) {
            super.setForeground(fg);
            if (label != null)
                label.setForeground(fg);
        }

        public void setHorizontalAlignment(int alignment) {
            label.setHorizontalAlignment(alignment);
        }

//        public void setImage(ImageIcon icon){
//            this.icon = icon;
//            width = icon.getIconWidth();
//        }

        public void paint(Graphics g) {
            try {
                if ((imageicon != null) && (imagewidth > 0)) {
                    int width = getWidth();
                    int strwidth = (getFontMetrics(getFont())).stringWidth(label.getText());
                    Color old = g.getColor();
                    g.setColor(bgClor);
                    g.fillRect(0, 0, getWidth(), getHeight());
                    g.setColor(old);
                    if (!Language.isLTR()) {

                        g.drawImage(imageicnBtn.getImage(), getWidth() - (strwidth + 5), 5, this);
                    } else {

                        g.drawImage(imageicnBtn.getImage(), strwidth + 5, 5, this);
                    }
//                    for (int i = 0; i <= width; i+=imagewidth) {
//                        g.drawImage(imageicon.getImage(),i,0,this);
//                    }
//                    if (imageiconL != null){
//                        g.drawImage(imageiconL.getImage(),0,0,this);
//                    }
//                    if (imageiconR != null){
//                        g.drawImage(imageiconR.getImage(),getWidth()-imageiconR.getIconWidth(),0,this);
//                    }
                    super.paintChildren(g);
                        /*if(this.getBorder()!=null){
                            this.getBorder().paintBorder(this,g,0,0,this.getWidth(),this.getHeight());
                        }*/
                } else {
                    super.paint(g);
                }
            } catch (Exception e) {

            }
//            super.paintChildren(g);
//            super.paint(g);
        }
    }

}
