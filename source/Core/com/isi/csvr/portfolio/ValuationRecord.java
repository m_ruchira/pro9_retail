package com.isi.csvr.portfolio;

/**
 * <p>Title: TW International</p>
 * <p>Description: Contains teh calculated Transaction details for the selected Portfolio(s)
 * transactions</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Bandula
 * @version 1.0
 */

public class ValuationRecord {

    private String sKey;
    private int holding;
    private double totCost;
    private String currency;
    private double avgCost;
    private double cashDividends;
    private double realizedGain;
    private double totCostForWACalculation;
    private int totQtyWACalculation;
    private boolean isFIFOSell = false;

    public ValuationRecord() {
    }

    public double getAvgCost() {
        if (totQtyWACalculation > 0) {
            avgCost = totCostForWACalculation / totQtyWACalculation;
        }
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getHolding() {
        return holding;
    }

    public void setHolding(int holding) {
        this.holding = holding;
    }

    public String getSKey() {
        return sKey;
    }

    public void setSKey(String sKey) {
        this.sKey = sKey;
    }

    public double getCumCost() {
        if (isFIFOSell) {
            return totCost;
        } else {

            return getAvgCost() * holding;

        }
    }

    public void setCumCost(double totCost) {
        this.totCost = totCost;
    }

    public double getCashDividends() {
        return cashDividends;
    }

    public void setCashDividends(double cashDividends) {
        this.cashDividends = cashDividends;
    }

    public double getRealizedGain() {
        return realizedGain;
    }

    public void setRealizedGain(double realizedGain) {
        this.realizedGain = realizedGain;
    }

    public double getTotCostForWACalculation() {
        return totCostForWACalculation;
    }

    public void setTotCostForWACalculation(double totCostForWACalculation) {
        this.totCostForWACalculation = totCostForWACalculation;
    }

    public int getTotQtyWACalculation() {
        return totQtyWACalculation;
    }

    public void setTotQtyWACalculation(int totQtyWACalculation) {
        this.totQtyWACalculation = totQtyWACalculation;
    }

    public boolean isFIFOSell() {
        return isFIFOSell;
    }

    public void setFIFOSell(boolean FIFOSell) {
        isFIFOSell = FIFOSell;
    }
}