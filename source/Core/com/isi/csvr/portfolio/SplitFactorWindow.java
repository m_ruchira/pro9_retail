package com.isi.csvr.portfolio;

import com.isi.csvr.*;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.table.TWTextField;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Aug 25, 2008
 * Time: 12:17:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class SplitFactorWindow extends JDialog implements ActionListener, FocusListener, KeyListener, TransactionDialogInterface {

    private static SplitFactorWindow self;
    private TWTextField txtSymbol;
    private TWTextField txtSplitFactor1;
    private TWTextField txtSplitFactor2;
    private TWButton btnOk;
    private TWButton btnCancel;
    private TWButton btnDownArraow;
    private String key;
    private String symbol;
    private String exchange;
    private SplitRecord record = null;
    //    private int instrument;
    private DateCombo fromDateCombo;
    private boolean isTextChanged;
    private JPanel symbolPanel;
    private JPanel mainPanel;
    private JPanel buttonPanel;
    private JPanel splitFactorPanel;

    private SplitFactorWindow() {
        super(Client.getInstance().getFrame(), true);
        createUI();
    }

    public static SplitFactorWindow getsharedInstance() {
        if (self == null) {
            self = new SplitFactorWindow();
        }
        return self;
    }

    private void createUI() {
        symbolPanel = createSymbolPanel();
        symbolPanel.setSize(180, 22);
        fromDateCombo = new DateCombo(Client.getInstance().getFrame());
        txtSplitFactor1 = new TWTextField(new ValueFormatter(ValueFormatter.INTEGER), "", 2);
        txtSplitFactor1.setSize(30, 20);
        txtSplitFactor2 = new TWTextField(new ValueFormatter(ValueFormatter.INTEGER), "", 2);
        txtSplitFactor2.setSize(30, 20);
        mainPanel = new JPanel();
        splitFactorPanel = new JPanel(new FlexGridLayout(new String[]{"0", "10", "0", "100%"}, new String[]{"22"}, 0, 0, false, false));
//        splitFactorPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        splitFactorPanel.setSize(180, 22);
        splitFactorPanel.setOpaque(true);
        splitFactorPanel.add(txtSplitFactor1);
        splitFactorPanel.add(new JLabel(" : "));
        splitFactorPanel.add(txtSplitFactor2);
        splitFactorPanel.add(new JLabel(" "));
        mainPanel.setSize(300, 100);
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100", "180"}, new String[]{"22", "22", "24"}, 4, 4, true, true));
        mainPanel.add(new TWPanel(Language.getString("PF_SPLIT_SYMBOL")));
        mainPanel.add(symbolPanel);
        mainPanel.add(new TWPanel(Language.getString("PF_SPLIT_DATE")));
        mainPanel.add(fromDateCombo);
        mainPanel.add(new TWPanel(Language.getString("PF_SPLIT_FACTOR")));
        mainPanel.add(splitFactorPanel);
//        mainPanel.add(new JLabel(Language.getString("PF_SPLIT_MESSAGE")));
//        mainPanel.add(new JLabel(""));
        fromDateCombo.applyTheme();
        buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        buttonPanel.setSize(300, 22);
        btnOk = new TWButton(Language.getString("OK"));
        btnOk.addActionListener(this);
        btnOk.setActionCommand("OK");
        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);
        btnCancel.setActionCommand("CANCEL");
        buttonPanel.add(btnOk);
        buttonPanel.add(btnCancel);
        JLabel lblSplitMsg = new JLabel(Language.getString("PF_SPLIT_MESSAGE"));
        lblSplitMsg.setSize(300, 22);
        this.getContentPane().setLayout(new ColumnLayout());
        this.getContentPane().add(new JLabel("  "));
        this.getContentPane().add(mainPanel);
//        this.getContentPane().add(lblSplitMsg);
        this.getContentPane().add(new JLabel("  "));
        this.getContentPane().add(buttonPanel);
        this.setTitle(Language.getString("ADJUST_SPLIT_FACTOR"));
        this.setSize(310, 210);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        GUISettings.applyOrientation(this);
    }

    private JPanel createSymbolPanel() {
        String[] widths = {"100%", "20"};
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights);

        JPanel panel = new JPanel(layout);
        txtSymbol = new TWTextField();
        //  txtSymbol.addFocusListener(this);
        txtSymbol.addKeyListener(this);
        //  txtSymbol.setEnabled(false);
        panel.add(txtSymbol);
        btnDownArraow = new TWButton("...");
        btnDownArraow.addActionListener(this);
        btnDownArraow.setActionCommand("ARROW");
        panel.add(btnDownArraow);

        return panel;
    }

    private void searchSymbol() {
        try {
            Symbols symbols = new Symbols();
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
            oCompanies.setTitle(Language.getString("PF_SPLIT_SYMBOL_SEARCH"));
            oCompanies.setSingleMode(true);
            oCompanies.init();
            oCompanies.setSelectedExchange(exchange);
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true);
            oCompanies = null;

            key = symbols.getSymbols()[0];
            exchange = SharedMethods.getExchangeFromKey(key);
            symbol = SharedMethods.getSymbolFromKey(key);
            txtSymbol.setText(symbol);
            validateComponents(true);
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("ARROW")) {
            searchSymbol();
        } else if (e.getActionCommand().equals("CANCEL")) {
            setVisible(false);
        } else if (e.getActionCommand().equals("OK")) {
            okActionPerformed();
        }
    }

    private void okActionPerformed() {
        if (isInputValid()) {
            record = PFStore.getInstance().createSplitRecord(-1);
            record.setFirstFactor(Byte.parseByte(txtSplitFactor1.getText()));
            record.setSecondFactor(Byte.parseByte(txtSplitFactor2.getText()));
            record.setKey(key);
            record.setStartDate(fromDateCombo.getDateLong());

            if (!PFStore.getInstance().getRecordsForSplitAdjustment(record).isEmpty()) {
                new SplitAdjustmentWindow(record).setVisible(true);
                setVisible(false);
            } else {
                new ShowMessage(Language.getString("NO_SPLIT_AVAILABlE"), "E");
            }
        }
    }

    private boolean isInputValid() {
        if (txtSplitFactor1.getText().trim().equals("")) {
            new ShowMessage(Language.getString("INVALID_SPLIT_FACTOR"), "E");
            return false;
        }
        if (txtSplitFactor2.getText().trim().equals("")) {
            new ShowMessage(Language.getString("INVALID_SPLIT_FACTOR"), "E");
            return false;
        }
        if (fromDateCombo.getText().trim().equals("")) {
            new ShowMessage(Language.getString("INVALID_SPLIT_DATE"), "E");
            return false;
        }
        if (!checkifValidDate(fromDateCombo.getDateLong())) {
            new ShowMessage(Language.getString("INVALID_SPLIT_DATE"), "E");
            return false;
        }
        if (key == null) {
            new ShowMessage(false, Language.getString("MSG_INVLID_SYMBOL"), "E");
            return false;
        } else if (PFStore.getKeyForValidSymbol(key) == null) {
            new ShowMessage(false, Language.getString("MSG_NOT_IN_PF"), "E");
            return false;
        }
        return true;
    }

    public void focusGained(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusLost(FocusEvent e) {
        String symbol = txtSymbol.getText().trim().toUpperCase();
        if (symbol.length() > 0) {
            if (isTextChanged) {
                key = null;
                key = SymbolMaster.getExchangeForSymbol(symbol, false);
                if ((key != null)) {
                    this.symbol = SharedMethods.getSymbolFromKey(key);
                    this.exchange = SharedMethods.getExchangeFromKey(key);
                    txtSymbol.setText(symbol);
                } else {
                    txtSymbol.setText("");
                    new ShowMessage(false, Language.getString("MSG_INVLID_SYMBOL"), "E");
                }
            }
            isTextChanged = false;
        }
    }

    public void keyTyped(KeyEvent e) {
        isTextChanged = true;
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            String symbol = txtSymbol.getText().trim().toUpperCase();
            if (symbol.length() > 0) {
                PortfolioInterface.validateSymbol(symbol, "PF_SPLIT_DLG", this);
            }
        }
    }

    public void setSymbol(String key) {
        this.key = key;
        exchange = SharedMethods.getExchangeFromKey(key);
        this.symbol = SharedMethods.getSymbolFromKey(key);
        validateComponents(true);
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void notifyInvalidSymbol(String symbol) {
        validateComponents(false);
        txtSymbol.setText("");
    }

    private boolean checkifValidDate(long date) {

        if (exchange != null) {
            Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
            long mktDate = 0l;
            if (ex != null) {
                mktDate = ex.getMarketDate();
            }


            if (mktDate > date) {
                return true;
            } else {
                TWDateFormat formatter = new TWDateFormat("yyyy-MM-dd");
                String mkt = formatter.format(mktDate);
                String dt = formatter.format(date);
                if (mkt.equals(dt)) {
                    return true;
                }
                return false;
            }
        } else {
            return false;
        }


    }

    public void setVisible(boolean b) {
        if (b) {
            validateComponents(false);
            clearData();
        }
        super.setVisible(b);    //To change body of overridden methods use File | Settings | File Templates.
    }

    private void validateComponents(boolean status) {
        txtSplitFactor1.setEnabled(status);
        txtSplitFactor1.setEditable(status);
        txtSplitFactor2.setEnabled(status);
        txtSplitFactor2.setEditable(status);
        fromDateCombo.setEnable(status);

    }

    private void clearData() {
        try {
            txtSplitFactor1.setText("");
            txtSplitFactor2.setText("");
            txtSymbol.setText("");
            fromDateCombo.clearDate();
        } catch (Exception e) {
        }
    }

    class TWPanel extends JPanel {
        String text;

        TWPanel(String text) {
            super(new BorderLayout(0, 0));
            this.text = text;
            this.setOpaque(true);
            JLabel lblText = new JLabel(text);
            lblText.setOpaque(true);
            JLabel lblSep = new JLabel(" : ");
            lblSep.setOpaque(true);
            if (Language.isLTR()) {
                super.add(lblText, BorderLayout.CENTER);
                super.add(lblSep, BorderLayout.EAST);
            } else {
                super.add(lblText, BorderLayout.CENTER);
                super.add(lblSep, BorderLayout.WEST);
            }
//            GUISettings.applyOrientation(this);
        }

    }
}
