package com.isi.csvr.portfolio;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Aug 26, 2008
 * Time: 3:17:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class ValuationRecordAll extends ValuationRecord {

    public static int VALUATIONREC_TYPE = 1;
    public static int VERTUAL_HEADETR_TYPE = 2;
    public static int PF_TOTAL_TYPE = 3;
    public static int PF_TOTAL_CASH_TYPE = 4;
    public static int PF_GRAND_TOTAL_TYPE = 5;
    public static int EMPTY_ROW = 6;
    public static int PFNAME_ROW = 7;

    private int recordType;
    private double valuation;
    private double realisedGain;
    private double unRealisedGain;
    private double portfolioTotal;
    private double totGainLoss;
    private double todayGainLoss;
    private double totPercentageGainLoss;
    private String pfCurrency;
    private long pfID;


    public ValuationRecordAll(int recordType) {
        this.recordType = recordType;
    }

    public double getPortfolioTotal() {
        return portfolioTotal;
    }

    public void setPortfolioTotal(double portfolioTotal) {
        this.portfolioTotal = portfolioTotal;
    }

    public int getRecordType() {
        return recordType;
    }

    public void setRecordType(int recordType) {
        this.recordType = recordType;
    }

    public double getValuation() {
        return valuation;
    }

    public void setValuation(double valuation) {
        this.valuation = valuation;
    }

    public double getRealisedGain() {
        return realisedGain;
    }

    public void setRealisedGain(double realisedGain) {
        this.realisedGain = realisedGain;
    }

    public double getUnRealisedGain() {
        return unRealisedGain;
    }

    public void setUnRealisedGain(double unRealisedGain) {
        this.unRealisedGain = unRealisedGain;
    }

    public double getTotGainLoss() {
        return totGainLoss;
    }

    public void setTotGainLoss(double totGainLoss) {
        this.totGainLoss = totGainLoss;
    }

    public double getTotPercentageGainLoss() {
        return totPercentageGainLoss;
    }

    public void setTotPercentageGainLoss(double totPercentageGainLoss) {
        this.totPercentageGainLoss = totPercentageGainLoss;
    }

    public String getPfCurrency() {
        return pfCurrency;
    }

    public void setPfCurrency(String pfCurrency) {
        this.pfCurrency = pfCurrency;
    }

    public long getPfID() {
        return pfID;
    }

    public void setPfID(long pfID) {
        this.pfID = pfID;

    }

    public double getTodayGainLoss() {
        return todayGainLoss;
    }

    public void setTodayGainLoss(double todayGainLoss) {
        this.todayGainLoss = todayGainLoss;
    }

    public void setSuperValues(ValuationRecord rec) {
        super.setAvgCost(rec.getAvgCost());
        super.setCashDividends(rec.getCashDividends());
        super.setCumCost(rec.getCumCost());
        super.setCurrency(rec.getCurrency());
        super.setHolding(rec.getHolding());
        super.setSKey(rec.getSKey());
        super.setFIFOSell(rec.isFIFOSell());
        super.setRealizedGain(rec.getRealizedGain());

    }


}
