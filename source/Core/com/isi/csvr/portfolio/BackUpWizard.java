package com.isi.csvr.portfolio;

import com.isi.csvr.shared.DownArrow;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.util.FlexNullLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class BackUpWizard extends JDialog implements ActionListener {

    private JPanel centerPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    private JLabel lblAutoBackup = new JLabel();
    private JLabel lblExportLocationDesc = new JLabel();
    private JLabel lblExportLocation = new JLabel();

    //    private JLabel      lblEnableAutoBackup = new JLabel();
//    private JLabel      lblBackInterval     = new JLabel();
    private JLabel lblManualInterval = new JLabel();
    private JComboBox cmbInterval = new JComboBox();
    private TWButton btnOK = new TWButton();
    private TWButton btnCancel = new TWButton();
    private TWButton btnBackupNow = new TWButton();
    private JCheckBox chkAutoBackup = new JCheckBox();
    private TWButton btnExportLocation;

    private int buttonPanelHeight = 30;
    private int width;
    private int height;
    private PortfolioWindow parent;
    private BackUpDaemon backupDaemon;

    public BackUpWizard(Frame frame, String title, boolean modal, PortfolioWindow invoker, BackUpDaemon daemon) {
        super(frame, title, modal);
        try {
            this.parent = invoker;
            this.backupDaemon = daemon;
            jbInit();
//            pack();
            GUISettings.applyOrientation(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setResizable(false);
        getContentPane().setLayout(new BorderLayout());
        centerPanel.setLayout(new FlexNullLayout()); // new GridLayout(3, 2, 2, 4));
        southPanel.setLayout(new FlowLayout(1, 2, 4));

        btnExportLocation = new TWButton(new DownArrow());
        btnExportLocation.setBorder(null);
        btnExportLocation.addActionListener(this);
        btnExportLocation.setActionCommand("DESTINATION");

        lblExportLocation.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (lblExportLocation.isEnabled()) {
                    setAutoBackUpPath();
                }
            }
        });

//        lblExportLocation.setBorder(BorderFactory.createLineBorder());
        setComponentLabels();
        defineComponentBounds();

        centerPanel.add(lblAutoBackup);
        centerPanel.add(chkAutoBackup);
        centerPanel.add(lblExportLocationDesc);
        centerPanel.add(lblExportLocation);
        centerPanel.add(btnExportLocation);
//        centerPanel.add(lblEnableAutoBackup);
//        centerPanel.add(lblBackInterval);
        centerPanel.add(cmbInterval);
        centerPanel.add(lblManualInterval);
        centerPanel.add(btnBackupNow);

        btnBackupNow.addActionListener(this);
        btnOK.addActionListener(this);
        btnCancel.addActionListener(this);
        chkAutoBackup.addActionListener(this);

        southPanel.add(btnOK);
        southPanel.add(btnCancel);

        getContentPane().add(centerPanel, BorderLayout.CENTER);
        getContentPane().add(southPanel, BorderLayout.SOUTH);
        populateCombos();
        if (BackUpDaemon.getAutoBackUpStatus()) {
            chkAutoBackup.setSelected(true);
            enableComponents(true);
        } else {
            cmbInterval.setEnabled(false);
            enableComponents(false);
        }
        if (BackUpDaemon.getBackUpInterval() == PFUtilities.EVERY_10_MINUTE)
            cmbInterval.setSelectedIndex(0);
        else if (BackUpDaemon.getBackUpInterval() == PFUtilities.EVERY_15_MINUTE)
            cmbInterval.setSelectedIndex(1);
        else if (BackUpDaemon.getBackUpInterval() == PFUtilities.EVERY_30_MINUTE)
            cmbInterval.setSelectedIndex(2);
        lblExportLocation.setText(BackUpDaemon.getBackUpLoaction());
    }

    private void setComponentLabels() {
//        String cashDescr = "<html>" + Language.getString("PF_ENTER_CASH_BALANCE") + "</html>";
        lblAutoBackup.setText(Language.getString("AUTOMATIC_BACKUP"));
        lblExportLocationDesc.setText(Language.getString("BACKUP_LOCATION"));
        chkAutoBackup.setText(Language.getString("ENABLE_AUTOMATIC_BACKUP"));
//        lblBackInterval.setText(Language.getString("BACKUP_LOCATION"));
        lblManualInterval.setText(Language.getString("MANUAL_BACKUP"));

        btnBackupNow.setText(Language.getString("BACKUP_NOW"));
        btnOK.setText(Language.getString("OK"));
        btnCancel.setText(Language.getString("CANCEL"));

//        chkAutoBackup.setText(Language.getString("SET_AS_DEFAULT_PORTFOLIO"));

        this.setTitle(Language.getString("BACKUP_TRANSACTIONS"));
    }

    private void defineComponentBounds() {
        int lblWidth1 = 120;
        int lblWidth2 = 150;
//        int lblWidth3 = 110;
        int xPos1 = 10;
        int xPos2 = xPos1 + 10;
        int xPos3 = xPos2 + lblWidth2 + 5;
        int fieldHeight = 20;
        int curYPos = 10;

        lblAutoBackup.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
        curYPos += fieldHeight + 5;

        chkAutoBackup.setBounds(new Rectangle(xPos2, curYPos, 200, fieldHeight));
//        lblEnableAutoBackup.setBounds(new Rectangle(xPos2+25, curYPos, 200, fieldHeight));
        curYPos += fieldHeight + 5;

        lblExportLocationDesc.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth1, fieldHeight));
        lblExportLocation.setBounds(new Rectangle(xPos3 + 5, curYPos, lblWidth1, fieldHeight));
        btnExportLocation.setBounds(new Rectangle(xPos3 + 7 + lblWidth1, curYPos, 20, fieldHeight));
        curYPos += fieldHeight + 5;

//        lblBackInterval.setBounds(new Rectangle(xPos2+5, curYPos, lblWidth1, fieldHeight));
        cmbInterval.setBounds(new Rectangle(xPos3 + 5, curYPos, lblWidth1, fieldHeight));
        curYPos += fieldHeight + 5;

        lblManualInterval.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth2, fieldHeight));
//        txtOpeningBalance.setBounds(new Rectangle(xPos2+5, curYPos+15, lblWidth2, fieldHeight));
        curYPos += fieldHeight + 5;

        btnBackupNow.setBounds(new Rectangle(xPos3 + 5, curYPos, lblWidth1, fieldHeight));
        curYPos += fieldHeight + 5;

        width = lblWidth1 + lblWidth2 + 80;
        centerPanel.setBounds(new Rectangle(5, xPos1, width - 5, curYPos));
        southPanel.setPreferredSize(new Dimension(width, buttonPanelHeight));
        height += curYPos + buttonPanelHeight * 2;

        btnOK.setPreferredSize(new Dimension(80, 20));
        btnCancel.setPreferredSize(new Dimension(80, 20));
    }

    private void populateCombos() {
        cmbInterval.addItem(PFUtilities.getConvertedData(Language.getString("10_MINUTES")));
        cmbInterval.addItem(PFUtilities.getConvertedData(Language.getString("15_MINUTES")));
        cmbInterval.addItem(PFUtilities.getConvertedData(Language.getString("30_MINUTES")));
        cmbInterval.setSelectedIndex(0);
    }

    public void showDialog() {
        this.setSize(new Dimension(width, height));
        centerUI();
        this.setVisible(true);
    }

    private void centerUI() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }

    private void closeDialog() {
//        this.exitFromLoop();
        parent = null;
        backupDaemon = null;

        this.dispose();
    }

    private void setAutoBackUpPath() {
        String sPath = PFUtilities.getSelectedFilePath(this,
                PFUtilities.FILE_SAVE_DIALOG_TYPE,
                JFileChooser.DIRECTORIES_ONLY,
                backupDaemon.getBackUpLoaction());
        if (sPath != null)
            lblExportLocation.setText(sPath);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("DESTINATION")) {
            setAutoBackUpPath();
        } else if (e.getSource() == chkAutoBackup) {
            if (chkAutoBackup.isSelected()) {
                enableComponents(true);
            } else {
                enableComponents(false);
            }
        } else if (e.getSource() == btnBackupNow) {
            backUpTransactionsHistory();
        } else if (e.getSource() == btnOK) {
            applyConfigurations();
            closeDialog();
        } else if (e.getSource() == btnCancel) {
            closeDialog();
        }
    }

    private void enableComponents(boolean isEnable) {
        cmbInterval.setEnabled(isEnable);
        lblExportLocationDesc.setEnabled(isEnable);
        lblExportLocation.setEnabled(isEnable);
        btnExportLocation.setEnabled(isEnable);
    }

    private void applyConfigurations() {
        switch (cmbInterval.getSelectedIndex()) {
            case 0:
                backupDaemon.setBackUpInterval(PFUtilities.EVERY_10_MINUTE);
                break;
            case 1:
                backupDaemon.setBackUpInterval(PFUtilities.EVERY_15_MINUTE);
                break;
            case 2:
                backupDaemon.setBackUpInterval(PFUtilities.EVERY_30_MINUTE);
                break;
        }
        backupDaemon.setAutoBackUpStatus(chkAutoBackup.isSelected());
        backupDaemon.setBackUpLoaction(lblExportLocation.getText());
        backupDaemon.saveConfiguration();
    }

    private void backUpTransactionsHistory() {
        String sPath = PFUtilities.getSelectedFilePath(this, PFUtilities.FILE_SAVE_DIALOG_TYPE,
                JFileChooser.FILES_ONLY, PFUtilities.backUpPath);
        System.out.println("sPath = " + sPath);
        if (sPath != null)
            PFStore.saveTransactions(sPath);
    }

//    /*
//    * Open/Save the selected Portfolios file
//    */
//    public String getSelectedFilePath(byte type) {
//        int     status = 0;
//        String  sTitle = null;
//        String  extentions[] = new String[1];
//
//        JFileChooser oFC = new JFileChooser("./PFBackups");
//        GUISettings.localizeFileChooserHomeButton(oFC);
//        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
//        if (type == FILE_SAVE_DIALOG_TYPE)
//            sTitle = Language.getString("EXPORT_TO_FILE");
//        else if (type == FILE_OPEN_DIALOG_TYPE)
//            sTitle = Language.getString("PF_FILECHOOSER_OPEN_FILE_DIALOG_TITLE");
//        oFC.setDialogTitle(sTitle);
//        extentions[0] = ".dat";
//        TWFileFilter oFilter = new TWFileFilter(Language.getString("DAT"), extentions);
//        oFC.setFileFilter(oFilter);
//
//        if (type == FILE_SAVE_DIALOG_TYPE)
//            status = oFC.showSaveDialog(this);
//        else if (type == FILE_OPEN_DIALOG_TYPE)
//            status = oFC.showOpenDialog(this);
//        if (status == JFileChooser.APPROVE_OPTION) {
//            String sFile = oFC.getSelectedFile().getAbsolutePath();
//            if (!sFile.trim().endsWith(extentions[0]))
//                sFile+= extentions[0];
//            oFC         = null;
//            extentions  = null;
//            oFilter     = null;
//            return sFile;
//        } else {
//            return null;
//        }
//    }

}
