package com.isi.csvr.portfolio;

import com.isi.csvr.TWTableCellRendererComponent;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Sep 2, 2008
 * Time: 11:25:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommissionListRenderer extends TWBasicTableRenderer {

    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static ImageIcon amendImage;
    private static ImageIcon cancelImage;
    private static String amendToolTip;
    private static String cancelToolTip;
    private TWDecimalFormat oPriceFormat;
    private Color foreground, background;
    private DefaultTableCellRenderer lblRenderer;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private String g_sNA = "NA";


    public CommissionListRenderer(int[] asRendIDs) {
        oPriceFormat = new TWDecimalFormat(" ###,##0.000 ");
        g_asRendIDs = asRendIDs;

        reload();
        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
        g_iNumberAlign = JLabel.RIGHT;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
        try {
            amendImage = new ImageIcon("images/Theme" + Theme.getID() + "/menu/amendorder.gif");
        } catch (Exception e) {
            amendImage = null;
        }
        try {
            cancelImage = new ImageIcon("images/Theme" + Theme.getID() + "/menu/cancelorder.gif");
        } catch (Exception e) {
            cancelImage = null;
        }
        amendToolTip = Language.getString("EDIT_COMMISSION_TOOLTIP");
        cancelToolTip = Language.getString("DELETE_COMMISSION_TOOLTIP");
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public void propertyChanged(int property) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        // lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer = new TWTableCellRendererComponent();
        if (isSelected) {
            foreground = g_oSelectedFG;
            background = g_oSelectedBG;
        } else if (row % 2 == 0) {
            foreground = g_oFG1;
            background = g_oBG1;
        } else {
            foreground = g_oFG2;
            background = g_oBG2;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    lblRenderer.setText(oPriceFormat.format(toDoubleValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'A': // Amend image
                    //lblRenderer = new TWTableCellRendererComponent();
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    if (value.equals(""))
                        lblRenderer.setIcon(null);
                    else
                        lblRenderer.setIcon(amendImage);
                    lblRenderer.setToolTipText(amendToolTip);
                    break;
                case 'C': // Cancel Image

                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    if (value.equals(""))
                        lblRenderer.setIcon(null);
                    else
                        lblRenderer.setIcon(cancelImage);
                    lblRenderer.setToolTipText(cancelToolTip);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return lblRenderer;
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return ((Double) oValue).doubleValue();
    }

    public Border getBorder(JTable table, boolean isLastRow, int column, Border border, Border cellBorder) {
        if ((isLastRow) && (table.convertColumnIndexToModel(column) >= 8) && (table.convertColumnIndexToModel(column) != 16)) {
            return border;
        } else {
            return cellBorder;
        }
    }
}
