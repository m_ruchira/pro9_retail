package com.isi.csvr.portfolio;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.table.CommonTable;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 13, 2008
 * Time: 11:47:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioLogicImportModel extends CommonTable {

    public static int NORMAL = 1;
    public static int VALIDATED = 2;
    public static int ERROR = 3;
    TWDateFormat g_oDateFormat;
    private ArrayList<PriliminaryTxnObject> dataStore;
    private int mode = 1;

    public PortfolioLogicImportModel(int mode) {
        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));
        this.mode = mode;
    }

    public void setDataStore(ArrayList<PriliminaryTxnObject> dataStore) {
        this.dataStore = dataStore;
    }

    public int getRowCount() {
        if (dataStore != null) {
            return dataStore.size();
        } else {
            return 0;
        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        try {
            PriliminaryTxnObject txn = dataStore.get(rowIndex);
            if (mode == NORMAL) {
                switch (columnIndex) {
                    case -2:
                        return txn.getSKey();
                    case -1:
                        return new Boolean(txn.getValidated() == PriliminaryTxnObject.VALID);
                    case 0:
                        return txn.getSymbol();
                    case 1:
                        return txn.getExchange();
                    case 2:
                        return g_oDateFormat.format(txn.getDate());
                    case 3:
                        return txn.getPrice() + "";
                    case 4:
                        return txn.getBrokerage() + "";
                    case 5:
                        return txn.getTypeString();
                    case 6:
                        return txn.getHolding() + "";
                    case 7:
                        return txn.getCurrency();
                    case 8:
                        return txn.getMemo();
                    default:
                        return -1;


                }
            } else if (mode == VALIDATED) {
                switch (columnIndex) {
                    case -2:
                        return txn.getSKey();
                    case -1:
                        return new Boolean(txn.getValidated() == PriliminaryTxnObject.VALID);
                    case 0:
                        return txn.getSymbol();
                    case 1:
                        return txn.getExchange();
                    case 2:
                        return txn.getCurrency();
                    case 3:
                        return new Long(txn.getCumilativeQty());
                    default:
                        return -1;

                }

            } else if (mode == ERROR) {
                switch (columnIndex) {
                    case -1:
                        return new Boolean(txn.getValidated() == PriliminaryTxnObject.VALID);
                    case 0:
                        return txn.getSymbol();
                    case 1:
                        return txn.getExchange();
                    case 2:
                        return txn.getDate().getTime();
                    case 3:
                        return txn.getPrice();
                    case 4:
                        return txn.getBrokerage();
                    case 5:
                        return txn.getTypeString();
                    case 6:
                        return txn.getCurrency();
                    case 7:
                        return txn.getMemo();
                    case 8:
                        return new Long(txn.getHolding());
                    case 9:
                        return new Long(txn.getCumilativeQty());

                    default:
                        return -1;
                }
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }


//        try {
//            StringTableRowObj rowobj = datastore.get(rowIndex);
//           // return  (rowobj.getCellValue(columnIndex)).toString();
//            return  "values";
//        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            return "";
//        }


    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        if (mode == NORMAL) {
            return String.class;
        } else if (mode == VALIDATED) {
            switch (iCol) {
                case 0:
                case 1:
                case 2:
                    return String.class;
                case 3:
                    return Integer.class;

                default:
                    return Object.class;
            }
        } else if (mode == ERROR) {
            switch (iCol) {
                case 0:
                case 1:
                case 5:
                case 6:
                case 7:
                    return String.class;
                case 2:
                    return Long.class;
                case 3:
                case 4:
                    return Object.class;
                case 8:
                case 9:
                    return Long.class;

                default:
                    return Object.class;
            }

        } else {
            return Object.class;
        }

    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }
}
