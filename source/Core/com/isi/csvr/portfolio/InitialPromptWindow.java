package com.isi.csvr.portfolio;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class InitialPromptWindow extends JDialog {

    private JPanel centerPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    private JLabel lblDescr = new JLabel();
    //    private JLabel      lblCreateNew    = new JLabel();
    private JButton btnCreate = new JButton();
    private JButton btnCancel = new JButton();

    private int buttonPanelHeight = 30;
    private int width = 400;
    private int height = 100;
    private PortfolioRecord record;
    private boolean isEditMode;
    private PortfolioWindow parent;

    public InitialPromptWindow(Frame frame, String title, boolean modal, PortfolioWindow invoker) {
        super(frame, title, modal);
        try {
            this.parent = invoker;
            jbInit();
//            pack();
            GUISettings.applyOrientation(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//    public CreatePortfolio() {
//        this(null, "", false, null);
//    }

    private void jbInit() throws Exception {
        this.setTitle(Language.getString("INFORMATION"));
        getContentPane().setLayout(new BorderLayout());
        centerPanel.setLayout(new BorderLayout(10, 10)); // new GridLayout(3, 2, 2, 4));
        southPanel.setLayout(new FlowLayout(1, 2, 4));

        centerPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        setComponentLabels();
//        defineComponentBounds();

//        lblCreateNew.setCursor(new Cursor(Cursor.HAND_CURSOR));
        centerPanel.add(lblDescr, BorderLayout.CENTER);
//        centerPanel.add(lblCreateNew, BorderLayout.SOUTH);

        btnCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (createPortfolio())
                    closeDialog();
            }
        });

        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
        });

        btnCreate.setPreferredSize(new Dimension(150, 20));
        btnCancel.setPreferredSize(new Dimension(80, 20));

        southPanel.add(btnCreate);
        southPanel.add(btnCancel);

        getContentPane().add(centerPanel, BorderLayout.CENTER);
        getContentPane().add(southPanel, BorderLayout.SOUTH);
    }

    private void setComponentLabels() {
        String cashDescr = "<html>" + Language.getString("MSG_INIT_PROMT_WINDOW") + "<br></html>";
        lblDescr.setText(cashDescr);

//        cashDescr = "<html><b>" + Language.getString("CREATE_PORTFOLIO") + "</b><br></html>";
//        lblCreateNew.setText(cashDescr);
        btnCreate.setText(Language.getString("CREATE_PORTFOLIO"));
        btnCancel.setText(Language.getString("CANCEL"));
    }

    private void defineComponentBounds() {
        int lblWidth1 = 120;
        int lblWidth2 = 150;
//        int lblWidth3 = 110;
        int xPos1 = 10;
        int xPos2 = xPos1 + lblWidth1 + 5;
//        int xPos3 = xPos2 + lblWidth2 + 5;
        int fieldHeight = 20;
        int curYPos = 10;

        width = xPos2 + lblWidth2 + 20;
//        centerPanel.setBounds(new Rectangle(5, xPos1, width-5, curYPos));
        southPanel.setPreferredSize(new Dimension(width, buttonPanelHeight));
        height += curYPos + buttonPanelHeight * 2;

        btnCreate.setPreferredSize(new Dimension(120, 20));
        btnCancel.setPreferredSize(new Dimension(60, 20));
    }

    public void showDialog() {
        this.setSize(new Dimension(width, height));
        centerUI();
        this.setVisible(true);
    }

    private void centerUI() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }

    private void closeDialog() {
//        this.exitFromLoop();
        this.dispose();
    }

    private boolean createPortfolio() {
        this.setVisible(false);
        parent.createPortfolio();
        return true;
    }
}