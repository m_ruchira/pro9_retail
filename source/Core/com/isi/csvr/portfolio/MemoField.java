package com.isi.csvr.portfolio;

//import javax.swing.JPopupMenu;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.LimitedLengthDocument;
import com.isi.csvr.shared.NonNavigatable;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.table.TWTextArea;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class MemoField extends JPopupMenu implements NonNavigatable {

    private JPanel basePanel;
    private JPanel btnPanel;
    //    private TWButton     btnOK;
    private TWButton btnCancel;
    private JScrollPane pane;
    private TWTextArea text;
    private TransactionDialog parent;

    public MemoField(TransactionDialog pf) {
        parent = pf;
        basePanel = new JPanel(new BorderLayout(5, 5));
        text = new TWTextArea();
        text.setEditable(true);
        text.setLineWrap(true);
        text.setDocument(new LimitedLengthDocument(60));
        pane = new JScrollPane(text);

        btnPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 2));
        btnCancel = new TWButton(Language.getString("CLOSE"));
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                parent.setMemoDescription(text.getText());
                disposePopup();
            }
        });

        text.setPreferredSize(new Dimension(200, 60));
        btnCancel.setPreferredSize(new Dimension(80, 20));

        btnPanel.add(btnCancel);

        basePanel.add(pane, BorderLayout.CENTER);
        basePanel.add(btnPanel, BorderLayout.SOUTH);

        this.add(basePanel);
    }

    public void setBackground(Color bg) {
        if (bg != null) {
            System.out.println("color set for memo field");
//            super.setBackground(bg);
//            text.setBackground(bg);
        }
    }

    public String getText() {
        return text.getText();
    }

    public void setText(String sText) {
        text.setText(sText);
    }

    private void disposePopup() {
        this.setVisible(false);
    }
}