package com.isi.csvr.portfolio;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.shared.ValueFormatter;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Sep 1, 2008
 * Time: 10:46:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommissionWindow extends JDialog implements ActionListener {
    private TWTextField txtName;
    private TWTextField txtMinPrice;
    private TWTextField txtPercentage;
    private JPanel mainPanel;
    private JPanel btnPanel;
    private TWButton okBtn;
    private TWButton cancelBtn;
    private CommissionObject commObj = null;
    private boolean isEditMode = false;

    public CommissionWindow() {
        super(Client.getInstance().getFrame());
        this.setTitle(Language.getString("COMMISSION_WINDOW"));
        createUI();
        GUISettings.applyOrientation(this);
    }

    public void adjustTitleToEdit() {
        this.setTitle(Language.getString("COMMISSION_WINDOW_EDIT"));
    }

    private void createUI() {
        txtName = new TWTextField();
        txtName.setSize(150, 20);
        txtMinPrice = new TWTextField(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL), "", 4);
        txtMinPrice.setSize(150, 20);
        txtPercentage = new TWTextField(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL), "", 4);
        txtPercentage.setSize(150, 20);
        JLabel lblName = new JLabel(Language.getString("NAME"));
        JLabel lblMinPrice = new JLabel(Language.getString("MIN_PRICE"));
        JLabel lblPercentage = new JLabel(Language.getString("PERCENTAGE"));
        mainPanel = new JPanel(new FlexGridLayout(new String[]{"100", "100%"}, new String[]{"10", "22", "22", "22"}, 4, 4, true, true));
        mainPanel.setOpaque(true);
        mainPanel.add(new JLabel(""));
        mainPanel.add(new JLabel(""));
        mainPanel.add(lblName);
        mainPanel.add(txtName);
        mainPanel.add(lblMinPrice);
        mainPanel.add(txtMinPrice);
        mainPanel.add(lblPercentage);
        mainPanel.add(txtPercentage);
        okBtn = new TWButton(Language.getString("OK"));
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        okBtn.addActionListener(this);
        cancelBtn.addActionListener(this);
        btnPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 4, 4));
        btnPanel.add(okBtn);
        btnPanel.add(cancelBtn);
//        this.getContentPane().add(new JLabel(""), BorderLayout.NORTH);
        this.getContentPane().add(mainPanel, BorderLayout.CENTER);
        this.getContentPane().add(btnPanel, BorderLayout.SOUTH);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setSize(280, 180);
    }

    public void clearFields() {
        txtMinPrice.setText("");
        txtName.setText("");
        txtPercentage.setText("");

    }

    public void setCommissionObject(CommissionObject commObj) {
        this.commObj = commObj;
        txtName.setText(commObj.getId());
        txtMinPrice.setText("" + commObj.getMinPrice());
        txtPercentage.setText("" + commObj.getPercentage());
        isEditMode = true;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cancelBtn) {
            isEditMode = false;
            this.setVisible(false);
        } else if (e.getSource() == okBtn) {
            if (isInputValid()) {
                if (isEditMode) {
                    String oldTrading = commObj.getId();

                    commObj.setMinPrice(Double.parseDouble(txtMinPrice.getText()));
                    commObj.setPercentage(Double.parseDouble(txtPercentage.getText()));
                    commObj.setId(txtName.getText());
                    PFStore.getInstance().editCommision(commObj, oldTrading);
                    isEditMode = false;
                    this.setVisible(false);
//                    PFStore.getInstance().addCommission(comobjn.getId());
//                    commObj.setId(txtName.getText().trim());

                } else {
                    String id = txtName.getText().trim();
                    //CommissionObject obj = PFStore.getInstance().addCommission(id);
                    CommissionObject obj = new CommissionObject(id);
                    obj.setMinPrice(Double.parseDouble(txtMinPrice.getText()));
                    obj.setPercentage(Double.parseDouble(txtPercentage.getText()));
                    PFStore.getInstance().addCommission(obj);
                    isEditMode = false;
                    this.setVisible(false);
                }
            }

        }
    }

    private boolean isInputValid() {
        String id = txtName.getText().trim();

        if (id.equals("")) {
            new ShowMessage(Language.getString("INVALID_ID"), "E");
            return false;
        }
        try {
            double minPrice = Double.parseDouble(txtMinPrice.getText());
        } catch (Exception e) {
            new ShowMessage(Language.getString("INVALID_MIN_PRICE"), "E");
            return false;
        }
        try {
            double percentage = Double.parseDouble(txtPercentage.getText());
            if (percentage > 100d) {
                new ShowMessage(Language.getString("INVALID_PERCENTAGE_VALUE"), "E");
                return false;
            }
        } catch (Exception e) {
            new ShowMessage(Language.getString("INVALID_PERCENTAGE_VALUE"), "E");
            return false;
        }
        if (!isEditMode) {
            if (PFStore.getInstance().isNameValid(id)) {
                new ShowMessage(Language.getString("INVALID_ID"), "E");
                return false;
            }
        }
        return true;
    }
}
