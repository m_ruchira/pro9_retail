package com.isi.csvr.portfolio;

import com.isi.csvr.shared.Language;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 9, 2008
 * Time: 4:58:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class StringTableRowObj {

    public static int VALID = 0;
    public static int INVALID_PRICE_FORMAT = 2;
    public static int INVALID_BROKERAGE_FORMAT = 3;
    public static int INVALID_HOLDING_FORMAT = 4;
    public static int INVALID_DATE_FORMAT = 5;
    public static int INVALID_CURRENCY_FORMAT = 6;
    public static int INVALID_EXCHANGE = 7;
    public static int INVALID_SYMBOL = 8;
    public static int INVALID_TYPE = 9;


    private String[] colvalues;
    private int validStatus;
    private int rowId;
    private long id;
    private int invalidRowID;

    public StringTableRowObj(String[] colvalues, int rowid, long id) {
        this.colvalues = colvalues;
        validStatus = VALID;
        rowId = rowid;
        // id = System.currentTimeMillis();
        this.id = id;
    }

    public String getCellValue(int cell) {
        if (colvalues != null && cell <= colvalues.length - 1) {
            return colvalues[cell];
        } else {
            return "NULL";
        }
    }

    public int getValidStatus() {
        return validStatus;
    }

    public void setValidStatus(int validStatus) {
        this.validStatus = validStatus;
    }

    public int getColCount() {
        if (colvalues != null) {
            return colvalues.length;
        } else {
            return 0;
        }
    }

    public long getId() {
        return id;
    }

    public int getRowId() {
        return rowId;
    }

    public String getStatusMessage() {
        switch (validStatus) {
            case 0:
                return Language.getString("VALID");
            // return "";
            case 2:
                return Language.getString("INVALID_PRICE_FORMAT");
            case 3:
                return Language.getString("INVALID_BROKERAGE_FORMAT");
            case 4:
                return Language.getString("INVALID_HOLDING_FORMAT");
            case 5:
                return Language.getString("INVALID_DATE_FORMAT");
            case 6:
                return Language.getString("INVALID_CURRENCY_FORMAT");
            case 7:
                return Language.getString("INVALID_EXCHANGE");
            case 8:
                return Language.getString("INVALID_SYMBOL");
            case 9:
                return Language.getString("INVALID_TYPE");
            default:
                return "";


        }


    }

    public int getInvalidColNumber() {
        switch (validStatus) {
            case 0:
                return -2;
            case 2:
                return HeaderMapper.getSpecificRow(HeaderMapper.PRICE);
            case 3:
                // return Language.getString("INVALID_BROKERAGE_FORMAT");
                return HeaderMapper.getSpecificRow(HeaderMapper.COMISSION);
            case 4:
                return HeaderMapper.getSpecificRow(HeaderMapper.HOLDING);
            case 5:
                return HeaderMapper.getSpecificRow(HeaderMapper.DATE);
            case 6:
                return HeaderMapper.getSpecificRow(HeaderMapper.CURRENCY);
            case 7:
                return HeaderMapper.getSpecificRow(HeaderMapper.EXCHANGE);
            case 8:
                return HeaderMapper.getSpecificRow(HeaderMapper.SYMBOL);
            case 9:
                return HeaderMapper.getSpecificRow(HeaderMapper.TYPE);
            default:
                return -2;
        }
    }

    public void addExchangeToList(String exchange) {

        try {
            String[] nwcolvalues = new String[colvalues.length + 1];
            System.arraycopy(colvalues, 0, nwcolvalues, 0, colvalues.length);
            nwcolvalues[colvalues.length] = exchange;
            colvalues = nwcolvalues;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
}
