package com.isi.csvr.portfolio;

//import javax.swing.JPopupMenu;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class PortfolioList extends JPopupMenu {

    private JPanel basePanel;
    private JPanel btnPanel;
    private JCheckBox[] checkPFs;
    //    private Vector      pfVector;
    private TWButton btnOK;
    private TWButton btnCancel;
    private TWButton btnCreatePF;
    private PortfolioWindow parent;

    public PortfolioList(PortfolioWindow parentIn, long[] selIDs) {
        this.parent = parentIn;

        ArrayList pfRecords = PFStore.getInstance().getPortfolioList();
        basePanel = new JPanel(new BorderLayout());
        int noOfElems = pfRecords.size();
        JPanel centerPanel = new JPanel(new GridLayout(noOfElems, 1));
        btnPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 2));
        checkPFs = new JCheckBox[noOfElems];
        if (noOfElems == 0) {
            JLabel nullLabel1 = new JLabel("");
            centerPanel.add(nullLabel1);
            String mesg = "<html><center><p><b>" +
                    Language.getString("MSG_NO_PORTFOLIOS_DEFINED") + "</b><br>" +
                    Language.getString("MSG_PLEASE_CREATE_A_PORTFOLIO") + "</p></center></html>";
            JLabel displayMesg = new JLabel(mesg);
            centerPanel.add(displayMesg);
            JLabel nullLabel2 = new JLabel("");
            centerPanel.add(nullLabel2);

            btnCreatePF = new TWButton(Language.getString("CREATE_PORTFOLIO"));
            btnCreatePF.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    hidePopup();
                    parent.createPortfolio();
                    disposePopup();
                }
            });
            btnCreatePF.setPreferredSize(new Dimension(150, 20));
            btnPanel.add(btnCreatePF);
        } else {
            String pfName = null;
            PortfolioRecord record = null;
            for (int i = 0; i < pfRecords.size(); i++) {
                record = (PortfolioRecord) pfRecords.get(i);
                if (Language.isLTR())
                    pfName = record.getName() + " (" + record.getCurrency() + ")";
                else
                    pfName = " (" + record.getCurrency() + ")" + record.getName();
                checkPFs[i] = new JCheckBox(pfName);
                if (selIDs != null) {
                    for (int j = 0; j < selIDs.length; j++) {
                        if (record.getPfID() == selIDs[j])
                            checkPFs[i].setSelected(true);
                    }
                }
                centerPanel.add(checkPFs[i]);
            }
            pfRecords = null;
            btnOK = new TWButton(Language.getString("OK"));
            btnOK.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    applyCustomFilter();
                    disposePopup();
                }
            });
            btnCancel = new TWButton(Language.getString("CANCEL"));
            btnCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    disposePopup();
                }
            });

            btnOK.setPreferredSize(new Dimension(80, 20));
            btnCancel.setPreferredSize(new Dimension(80, 20));

            GUISettings.setSameSize(btnOK, btnCancel);
            btnPanel.add(btnOK);
            btnPanel.add(btnCancel);
        }
        basePanel.add(centerPanel, BorderLayout.CENTER);
        basePanel.add(btnPanel, BorderLayout.SOUTH);

        this.add(basePanel);
        GUISettings.applyOrientation(this);
    }

    private void applyCustomFilter() {
        int counter = 0;
        long[] selPFIDs = null;
        int noOfItemsSelected = 0;
        for (int i = 0; i < checkPFs.length; i++) {
            if (checkPFs[i].isSelected()) {
                noOfItemsSelected++;
            }
        }
        selPFIDs = new long[noOfItemsSelected];
        for (int i = 0; i < checkPFs.length; i++) {
            if (checkPFs[i].isSelected()) {
                selPFIDs[counter++] = (PFStore.getInstance().getPortfolio(i)).getPfID();
            }
        }
        parent.setSelectedPFIDs(selPFIDs);
        if (noOfItemsSelected == 1) {

            parent.setSelectedCuttencyForSinglePF(selPFIDs);
        }
        parent.activateCurrentSymbols();
        parent.displayCashBalance();
        parent.setFilterDescription(false, null, null, null, PFUtilities.NONE);
    }

    private void hidePopup() {
        this.setVisible(false);
    }

    private void disposePopup() {
        this.setVisible(false);
        checkPFs = null;
        parent = null;
//        pfVector.removeAllElements();
//        pfVector    = null;
    }
}