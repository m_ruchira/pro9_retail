package com.isi.csvr.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Aug 26, 2008
 * Time: 1:29:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class SplitAdjustmentModal extends CommonTable
        implements TableModel, CommonTableInterface {

    private ArrayList<SplitAdjustmentObject> store;
    private SplitRecord record;

    public SplitAdjustmentModal(ArrayList<SplitAdjustmentObject> store) {
        this.store = store;
    }

    public void setSplitRecord(SplitRecord record) {
        this.record = record;
    }

    public int getRowCount() {
        if (store != null) {
            return store.size();
        }
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int iCol) {
        try {
            SplitAdjustmentObject obj = store.get(rowIndex);
            switch (iCol) {
                case -4:
                    return PortfolioInterface.getStockObject(record.getKey()).getDecimalCount();
                case -1:
                    return record.getKey();
                case 0:
                    return obj.getPortfolioName();
                case 1:
                    return obj.getOldPrice();
                case 2:
                    return obj.getOldQty();
                case 3:
                    return obj.getNewPrice();
                case 4:
                    return obj.getNewQty();
                default:
                    return "";
            }
        } catch (Exception e) {
            return Constants.NULL_STRING;
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        //return getValueAt(0, iCol).getClass();
        switch (iCol) {
            case 0:
                return String.class;
            case 2:
            case 4:
                return Long.class;
            case 1:
            case 3:
                return Double.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        if (col == 4) {
            return true;
        }
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex == 4) {
            try {
                long newQty = Long.parseLong((String) aValue);
                SplitAdjustmentObject obj = store.get(rowIndex);
                if (newQty > 0) {
                    obj.setNewQty(newQty);
                    obj.setNewPrice(obj.getOldPrice() * obj.getOldQty() / newQty);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return;
            }
        }
    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

}
