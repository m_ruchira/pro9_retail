package com.isi.csvr;

import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.CurrencyListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.CurrencyModel;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 8, 2005
 * Time: 2:30:48 PM
 */
//change start
public class CurrencyWindow implements ApplicationListener, MouseListener, CurrencyListener, Themeable {

    private InternalFrame frame;
    private Table table;
    private JPanel buttonPanel;
    private ArrayList<String> selectedCrr;
    private ArrayList<String> selectedDecimal;
    private CurrencyModel model;
    private ViewSetting oSetting;
    private JLabel btnAvailablePFs;
    private JLabel btnDecimalPlaces;
    private JLabel lblselected;
    private JLabel label;
    private JLabel decimals;
    private int noOfAllCrr = 0;
    private int noOfSelCrr = 0;

    public CurrencyWindow() {
        selectedCrr = new ArrayList<String>();
        table = new Table();

        model = new CurrencyModel();
        model.setDirectionLTR(Language.isLTR());
        oSetting = ViewSettingsManager.getSummaryView("CURRENCY_TABLE");
        //change start
        oSetting.setColumns(BigInteger.valueOf((long) Math.pow(2, oSetting.getColumnHeadings().length) - 1));
        oSetting.setColumnHeadings(getCurrencyList(selectedCrr));
        model.setViewSettings(oSetting);
        table.setModel(model);
        table.getTable().setAutoCreateColumnsFromModel(true);
        table.setUseSameFontForHeader(true);
        table.setPreferredSize(new Dimension(500, 100));
        table.setWindowType(ViewSettingsManager.CURRENCY_VIEW);
        table.getTable().getTableHeader().setReorderingAllowed(false);
        model.setTable(table);
        model.applyColumnSettings();
        table.hideCustomizer();
        table.updateGUI();

        frame = new InternalFrame(table);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(frame);
        frame.getContentPane().add(table);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        frame.setTitle(Language.getString(oSetting.getCaptionID()));
        createUI();
        table.setNorthPanel(buttonPanel);
        frame.updateUI();
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);

        String informationLabel = " " + Language.getString("CURRENCY_MESSAGE") + " ";
        if ((informationLabel != null) && (!informationLabel.equals(""))) {
            JLabel label = new JLabel(informationLabel);
            label.setHorizontalAlignment(JLabel.LEADING);
            table.setSouthPanel(label);
        }

        GUISettings.applyOrientation(frame);
        Theme.registerComponent(this);
        applyTheme();
        setDecimalPlaces();
        setSavedCurrencies();
        //change start
        CurrencyStore.getSharedInstance().addCurrencyListener(this);
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(frame);
    }

    private String[] getCurrencyList(ArrayList<String> array) {
        StringBuilder columns = new StringBuilder();
        columns.append("");
        try {
            if (array == null) {
                for (String currency : CurrencyStore.getCurrencyIndex()) {
                    columns.append(",");
                    columns.append(currency);
                }
            } else {
                for (String currency : array) {
                    columns.append(",");
                    columns.append(currency);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return columns.toString().split(",");
    }

    public InternalFrame getFrame() {
        return frame;
    }

    private void createUI() {
        label = new JLabel(Language.getString("SELECT_CURRENCIES"));
        label.addMouseListener(this);
        label.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lblselected = new JLabel("");
        btnAvailablePFs = new JLabel("");
        btnAvailablePFs.setPreferredSize(new Dimension(10, 22));
        btnAvailablePFs.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnAvailablePFs.setIcon(new ImageIcon("images/Common/graph_down_pop.gif"));
        btnAvailablePFs.addMouseListener(this);

        btnDecimalPlaces = new JLabel("");
        btnDecimalPlaces.setPreferredSize(new Dimension(10, 22));
        btnDecimalPlaces.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnDecimalPlaces.setIcon(new ImageIcon("images/Common/graph_down_pop.gif"));
        btnDecimalPlaces.addMouseListener(this);

        decimals = new JLabel(Language.getString("DECIMAL_PLACES"));
        decimals.addMouseListener(this);
        decimals.setCursor(new Cursor(Cursor.HAND_CURSOR));

        buttonPanel = new JPanel(new FlexGridLayout(new String[]{"10", "80", "10", "10", "70", "10", "100%"}, new String[]{"25"}));
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(label);
        buttonPanel.add(btnAvailablePFs);
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(decimals);
        buttonPanel.add(btnDecimalPlaces);
        buttonPanel.add(lblselected);

        Application.getInstance().addApplicationListener(this);
    }

    public void setMessage() {
        String mess = Language.getString("MSG_SELECTED_CURRENCY");
        mess = mess.replace("[SELECTED]", noOfSelCrr + "");
        mess = mess.replace("[ALL]", noOfAllCrr + "");
        lblselected.setText(mess);
        lblselected.setHorizontalAlignment(JLabel.TRAILING);
        mess = null;
    }

    public void setNoOfAllCurrencies(int count) {
        noOfAllCrr = count;
    }

    public void setNoOfSellCurrencies(int count) {
        noOfSelCrr = count;
    }

    public void setSelectedCurrencies(ArrayList<String> array) {
//        selectedCrr = null;
        this.selectedCrr = array;
        noOfSelCrr = array.size();
        noOfAllCrr = CurrencyStore.getSharedInstance().getSize();
//        noOfAllCrr = CurrencyStore.getCurrencyIndex().size();
        model.setArray(selectedCrr);
        oSetting.putProperty(ViewConstants.VC_CURRENCY_LIST, array.toString());
        setMessage();
        //change start - viewSetting change
        table.saveColumnPositions(oSetting);
        int[][] columnSettings = oSetting.getColumnSettings();
        oSetting.setColumnHeadings(getCurrencyList(selectedCrr));
        table.getTable().createDefaultColumnsFromModel();
        table.setColumnIdentifiers();
        table.getTable().updateUI();
        model.adjustColumns(table.getTable(), oSetting.getColumns());
//        table.setUseSameFontForHeader(true);
        //change start - viewSetting change
        oSetting.setColumnSettings(columnSettings);
        model.applyColumnSettings();
//        table.getTable().sizeColumnsToFit(JTable.AUTO_RESIZE_OFF);
        ((SmartTable) table.getTable()).adjustColumnWidthsToFit(10);

        table.updateUI();
    }

    public void mouseClicked(MouseEvent e) {
        if ((e.getSource() == btnAvailablePFs) || (e.getSource() == label)) {
            //int popWidth = 0;
            CurrencyPopup pf = new CurrencyPopup(this, selectedCrr);
            /*if (Language.isLTR()) {
                popWidth = (int) pf.getPreferredSize().getWidth();
            }*/
            // btnAvailablePFs.getWidth() - popWidth
            pf.show(label, 2, label.getHeight() - 4);
            pf = null;
        } else if ((e.getSource() == btnDecimalPlaces) || (e.getSource() == decimals)) {
            //int popWidth = 0;
            JPopupMenu decimalPopup = new JPopupMenu();
            for (int i = 0; i < 11; i++) {
                final TWMenuItem item = new TWMenuItem("" + i);
                item.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        int index = Integer.parseInt(item.getText());
                        model.setDecimalCount(index);
                        decimals.setText(Language.getString("DECIMAL_PLACES") + " " + index);
                        ((SmartTable) table.getTable()).adjustColumnWidthsToFit(10);
                        oSetting.putProperty(ViewConstants.VC_DECIMAL_PLACES, index);
                    }
                });
                decimalPopup.add(item);
            }
            GUISettings.applyOrientation(decimalPopup);
//            decimalPopup.add(new TWMenuItem("0"));
//            decimalPopup.add(new TWMenuItem("1"));
//            decimalPopup.add(new TWMenuItem("2"));
//            decimalPopup.add(new TWMenuItem("3"));
//            decimalPopup.add(new TWMenuItem("4"));
//            decimalPopup.add(new TWMenuItem("5"));
//            decimalPopup.add(new TWMenuItem("6"));
//            decimalPopup.add(new TWMenuItem("7"));
//            decimalPopup.add(new TWMenuItem("8"));
//            decimalPopup.add(new TWMenuItem("9"));
//            decimalPopup.add(new TWMenuItem("10"));
            /*if (Language.isLTR()) {
                popWidth = (int) decimalPopup.getPreferredSize().getWidth();
            }*/
            // btnAvailablePFs.getWidth() - popWidth
            decimalPopup.show(decimals, 2, decimals.getHeight() - 4);
//            pf = null;
        }
    }

    private void setSavedCurrencies() {
        try {
            String str = oSetting.getProperty(ViewConstants.VC_CURRENCY_LIST);
            if (str.indexOf("[") > -1)
                str = str.replace("[", " ");
            if (str.indexOf("]") > -1)
                str = str.replace("]", " ");
            String[] selectedList = str.split(",");
            /*for(String item: selectedList){
                selectedCrr.add(item.trim());
            }*/
            for (String item : selectedList) {
                if (!selectedCrr.contains(item.trim()))
                    selectedCrr.add(item.trim());
            }
//            noOfAllCrr = CurrencyStore.getCurrencyIndex().size();
            noOfAllCrr = CurrencyStore.getSharedInstance().getSize();
            setSelectedCurrencies(selectedCrr);
//            frame.applySettings();
//            model.adjustColumns(table.getTable(), oSetting.getColumns());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDecimalPlaces() {
        try {
            String value = oSetting.getProperty(ViewConstants.VC_DECIMAL_PLACES);
            if (value.indexOf("[") > -1) {
                value = value.replaceAll("[", "");
            }
            if (value.indexOf("]") > -1) {
                value = value.replaceAll("]", "");
            }
            value = value.trim();
            model.setDecimalCount(Integer.parseInt(value));
            decimals.setText(Language.getString("DECIMAL_PLACES") + " " + value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void adjustColumns() {
        ((SmartTable) table.getTable()).adjustColumnWidthsToFit(10);
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void applicationLoading(int percentage) {
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationExiting() {
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void workspaceWillLoad() {
    }

    public void workspaceLoaded() {
        setDecimalPlaces();
        setSavedCurrencies();
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void selectedExchangeChanged(Exchange exchange) {

    }

    //change start
    public void currencyAdded() {
        noOfAllCrr = CurrencyStore.getSharedInstance().getSize();
//        noOfAllCrr = CurrencyStore.getCurrencyIndex().size();
        setMessage();
    }
    //change end

}