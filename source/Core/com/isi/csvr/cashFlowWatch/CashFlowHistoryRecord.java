package com.isi.csvr.cashFlowWatch;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 16, 2008
 * Time: 4:45:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowHistoryRecord {
    private long date;
    private double lastPrice;
    private long cashInOrders;
    private long cashInVloume;
    private double cashInTurnover;
    private long cashoutOrders;
    private long cashoutVolume;
    private double cashoutTurnover;
    private double netValue;
    private double percentage;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(double lastPrice) {
        this.lastPrice = lastPrice;
    }

    public long getCashInOrders() {
        return cashInOrders;
    }

    public void setCashInOrders(long cashInOrders) {
        this.cashInOrders = cashInOrders;
    }

    public long getCashInVloume() {
        return cashInVloume;
    }

    public void setCashInVloume(long cashInVloume) {
        this.cashInVloume = cashInVloume;
    }

    public double getCashInTurnover() {
        return cashInTurnover;
    }

    public void setCashInTurnover(double cashInTurnover) {
        this.cashInTurnover = cashInTurnover;
    }

    public long getCashoutOrders() {
        return cashoutOrders;
    }

    public void setCashoutOrders(long cashoutOrders) {
        this.cashoutOrders = cashoutOrders;
    }

    public long getCashoutVolume() {
        return cashoutVolume;
    }

    public void setCashoutVolume(long cashoutVolume) {
        this.cashoutVolume = cashoutVolume;
    }

    public double getCashoutTurnover() {
        return cashoutTurnover;
    }

    public void setCashoutTurnover(double cashoutTurnover) {
        this.cashoutTurnover = cashoutTurnover;
    }

    public double getNetValue() {
        netValue = cashInTurnover - cashoutTurnover;
        return netValue;
    }


    public double getPercentage() {
        percentage = (cashInTurnover) / (cashInTurnover + cashoutTurnover);

        return percentage;
    }

    public long getNetOrders() {

        return cashInOrders - cashoutOrders;
    }
}
