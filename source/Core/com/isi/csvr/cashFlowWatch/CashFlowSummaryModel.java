package com.isi.csvr.cashFlowWatch;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;

import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 19, 2008
 * Time: 11:47:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowSummaryModel extends CommonTable implements TableModel, CommonTableInterface, DDELinkInterface, ClipboardOwner {
    private static final int HISTORY_MODE = 1;
    private static final int INTRADAY_MODE = 0;
    ArrayList<Stock> intraDayStore = new ArrayList<Stock>();
    ArrayList<Stock> intraDayStoreALl = new ArrayList<Stock>();
    private ArrayList datastore;
    private int mode = 0;
    private String exchange;
    private Clipboard clip;

    public CashFlowSummaryModel() {
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    public static boolean isDynamicData(int column) {
        switch (column) {
            case 0:
            case 1:
                return false;
            default:
                return true;
        }
    }

    public void setDatastore(ArrayList datastore, String exchange) {
        this.datastore = datastore;
        intraDayStore.clear();
        intraDayStoreALl.clear();
        this.exchange = exchange;
        getIntradayStore();
    }

    public int getRowCount() {
        if (datastore != null) {
            if (getMode() == HISTORY_MODE) {
                return datastore.size();
            } else {
                return intraDayStore.size();
            }
        } else {
            return 0;
        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        if (getMode() == HISTORY_MODE) {
            try {
                CashFlowSummaryRecord rec = (CashFlowSummaryRecord) datastore.get(rowIndex);

                switch (columnIndex) {
                    case -2:
                        return rec.getSKey();
                    case -1:
                        return rec;
                    case 0:
                        return rec.getSymbol();
                    case 1:
                        return rec.getDescription();
                    case 2:
                        return rec.getVolume();
                    case 3:
                        return rec.getTurnover();
                    case 4:
                        return rec.getPercentage();
//                        return new DoubleTransferObject().setValue(rec.getPercentage());
                    case 5:
                        return rec.getCashInTurnover();
                    case 6:
                        return rec.getCashOutTurnover();
                    case 7:
                        return rec.getNetValue();
                    default:
                        return "";

                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return "";
            }
        } else {
            try {
                Stock oStock = (Stock) intraDayStore.get(rowIndex);
                //   Stock oStock = DataStore.getSharedInstance().getStockObject(rec.getSKey());

                switch (columnIndex) {
                    case -2:
                        return oStock.getKey();
                    case -1:
                        return oStock;
                    case 0:
                        return oStock.getSymbol();
                    case 1:
                        return oStock.getShortDescription();
                    case 2:
                        try {
                            return oStock.getVolume();
                        } catch (Exception e) {
                            return 0;
                        }
                    case 3:
                        try {
                            return oStock.getTurnover();
                        } catch (Exception e) {
                            return 0;
                        }
                    case 4:
//                            if (!Double.isNaN(oStock.getCashFlowPct())) {
                        try {
                            return oStock.getCashFlowPct();
                        } catch (Exception e) {
                            return 0;  //To change body of catch statement use File | Settings | File Templates.
                        }
//                            } else {
//                                return 0;
//                            }
                        //                        return new DoubleTransferObject().setValue(rec.getPercentage());
                    case 5:
                        try {
                            return oStock.getCashInTurnover();
                        } catch (Exception e) {
                            return 0d;  //To change body of catch statement use File | Settings | File Templates.
                        }
                    case 6:
                        try {
                            return oStock.getCashOutTurnover();
                        } catch (Exception e) {
                            return 0d;   //To change body of catch statement use File | Settings | File Templates.
                        }

                    case 7:
                        try {
                            return oStock.getCashFlowNet();
                        } catch (Exception e) {
                            return 0d;   //To change body of catch statement use File | Settings | File Templates.
                        }
                    default:
                        return "";

                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return "";
            }
        }

//        try {
//            StringTableRowObj rowobj = datastore.get(rowIndex);
//           // return  (rowobj.getCellValue(columnIndex)).toString();
//            return  "values";
//        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            return "";
//        }


    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (iCol) {

            case 0:
            case 1:
                return String.class;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return Double.class;
            case 2:
                return Long.class;
            default:
                return Object.class;

        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    private void getIntradayStore() {
        String exch = exchange;
        if (exch == null || exch.trim().isEmpty()) {
            if (datastore != null && datastore.size() > 0) {
                CashFlowSummaryRecord rec = (CashFlowSummaryRecord) datastore.get(0);
                exch = rec.getExchange();
            }
        }
        if (exch != null && (!exch.trim().isEmpty())) {
            Symbols en = DataStore.getSharedInstance().getSymbolsObject(exch);
            if (en != null) {
                String[] sym = en.getSymbols();
                if (sym != null) {
                    for (int i = 0; i < sym.length; i++) {
                        Stock oStock = DataStore.getSharedInstance().getStockObject(sym[i]);
                        if (oStock != null) {
                            intraDayStore.add(oStock);
                            intraDayStoreALl.add(oStock);
                        }
                    }

                }
            }
        }

//        if (datastore!=null) {
//            try {
//                for(int i=0;i<datastore.size();i++){
//                    CashFlowSummaryRecord rec= (CashFlowSummaryRecord)datastore.get(i);
//                    Stock oStock= DataStore.getSharedInstance().getStockObject(rec.getSKey());
//                    DataStore.getSharedInstance().getSymbols()
//                    if(oStock!=null){
//                        intraDayStore.add(rec);
//                    }
//
//                 }
//            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
//        }
    }

    public void filterIntradayStore(String sectorID, String subMktID, String indexID) {
        intraDayStore.clear();
        if (sectorID == null)
            sectorID = "";
        if (subMktID == null)
            subMktID = "";
        if (indexID == null)
            indexID = "";
        if (!intraDayStoreALl.isEmpty()) {

            for (int i = 0; i < intraDayStoreALl.size(); i++) {
                Stock stock = intraDayStoreALl.get(i);
                if (!sectorID.equals("DEFAULT")) {
                    if (sectorID.equalsIgnoreCase(Language.getString("ALL"))) {
                        if (!subMktID.equals("DEFAULT")) {
                            if (!subMktID.equals("")) {
                                if (subMktID.equalsIgnoreCase(Language.getString("ALL"))) {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                intraDayStore.add(stock);
                                            }
                                        } else {
                                            intraDayStore.add(stock);
                                        }
                                    }

                                } else if (stock != null && (stock.getMarketID().equals(subMktID))) {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                intraDayStore.add(stock);
                                            }
                                        } else {
                                            intraDayStore.add(stock);
                                        }
                                    }
                                }
                            } else {
                                if (!indexID.equals("DEFAULT")) {
                                    /*if (!indexID.equals("")) {      // Shanaka - Cashflow all symbol
                                        //   if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                        intraDayStore.add(stock);
                                        //     }
                                    } else {
                                        intraDayStore.add(stock);
                                    }*/
                                    if (!indexID.equals("")) {
                                        if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                            intraDayStore.add(stock);
                                        }
                                    } else {
                                        intraDayStore.add(stock);
                                    }
                                }
                            }
                        }
                    } else if (stock != null && (stock.getSectorCode().equals(sectorID))) {
                        if (!subMktID.equals("DEFAULT")) {
                            if (!subMktID.equals("")) {
                                if (subMktID.equalsIgnoreCase(Language.getString("ALL"))) {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                intraDayStore.add(stock);
                                            }
                                        } else {
                                            intraDayStore.add(stock);
                                        }
                                    }
                                } else if (stock != null && (stock.getMarketID().equals(subMktID))) {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                intraDayStore.add(stock);
                                            }
                                        } else {
                                            intraDayStore.add(stock);
                                        }
                                    }
                                }
                            } else {
                                if (!indexID.equals("DEFAULT")) {
                                    if (!indexID.equals("")) {
                                        if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                            intraDayStore.add(stock);
                                        }
                                    } else {
                                        intraDayStore.add(stock);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void searchIntraDayStore(String sectorID, String subMktID, String indexID, String criteria) {
        intraDayStore.clear();
        if (!intraDayStoreALl.isEmpty()) {
            for (int i = 0; i < intraDayStoreALl.size(); i++) {
                Stock stock = intraDayStoreALl.get(i);
                if ((stock.getSymbol().contains(criteria.toUpperCase())) || (stock.getShortDescription().contains(criteria.substring(0, 1).toUpperCase() + criteria.substring(1))) || (stock.getShortDescription().contains(criteria))) {
                    if (!sectorID.equals("DEFAULT")) {
                        if (sectorID.equalsIgnoreCase(Language.getString("ALL"))) {
                            if (!subMktID.equals("DEFAULT")) {
                                if (!subMktID.equals("")) {
                                    if (subMktID.equalsIgnoreCase(Language.getString("ALL"))) {
                                        if (!indexID.equals("DEFAULT")) {
                                            if (!indexID.equals("")) {
                                                if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                    intraDayStore.add(stock);
                                                }
                                            } else {
                                                intraDayStore.add(stock);
                                            }
                                        }
                                    } else if (stock != null && (stock.getMarketID().equals(subMktID))) {
                                        if (!indexID.equals("DEFAULT")) {
                                            if (!indexID.equals("")) {
                                                if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                    intraDayStore.add(stock);
                                                }
                                            } else {
                                                intraDayStore.add(stock);
                                            }
                                        }
                                    }
                                } else {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                intraDayStore.add(stock);
                                            }
                                        } else {
                                            intraDayStore.add(stock);
                                        }
                                    }
                                }
                            }
                        } else if (stock != null && (stock.getSectorCode().equals(sectorID))) {
                            if (!subMktID.equals("DEFAULT")) {
                                if (!subMktID.equals("")) {
                                    if (subMktID.equals(Language.getString("ALL"))) {
                                        if (!indexID.equals("DEFAULT")) {
                                            if (!indexID.equals("")) {
                                                if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                    intraDayStore.add(stock);
                                                }
                                            } else {
                                                intraDayStore.add(stock);
                                            }
                                        }
                                    } else if (stock != null && (stock.getMarketID().equals(subMktID))) {
                                        if (!indexID.equals("DEFAULT")) {
                                            if (!indexID.equals("")) {
                                                if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                    intraDayStore.add(stock);
                                                }
                                            } else {
                                                intraDayStore.add(stock);
                                            }
                                        }
                                    }
                                } else {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && CashFlowHistorySummaryUI.getSharedInstance().getIndexSymbols().isAlreadyIn(stock.getKey())) {
                                                intraDayStore.add(stock);
                                            }
                                        } else {
                                            intraDayStore.add(stock);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void getDDEString(Table table, boolean withHeadings) {
        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (withHeadings)
            buffer.append(copyHeaders(table));
        for (int r = row; r < (rows + row); r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    if (CashFlowSummaryModel.isDynamicData(modelIndex)) {
                        buffer.append("=MRegionalDdeServer|'");
                        buffer.append("CF");
                        buffer.append(table.getTable().getModel().getValueAt(r, -2));
                        buffer.append("'!'");
                        buffer.append(modelIndex);
                        buffer.append("'");
                        buffer.append("*1");

                    } else {
                        buffer.append(table.getTable().getModel().getValueAt(r, modelIndex));
                    }

                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        for (int c = 0; c < cols; c++) {
            int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public String getDDEString(String id, int col) {
        for (int i = 0; i < getRowCount(); i++) {
            if (getMode() == HISTORY_MODE) {
                if (((CashFlowSummaryRecord) datastore.get(i)).getSKey().equals(id))
                    return (String) getValueAt(i, col);
            } else if (getMode() == INTRADAY_MODE) {
                if (((Stock) intraDayStore.get(i)).getKey().equals(id))
                    return String.valueOf(getValueAt(i, col));
            }
        }
        return "0";
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }
}
