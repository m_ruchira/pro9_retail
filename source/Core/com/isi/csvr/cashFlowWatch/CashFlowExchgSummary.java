package com.isi.csvr.cashFlowWatch;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 17, 2008
 * Time: 4:11:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowExchgSummary {

    private String exchange;
    private Hashtable<String, ArrayList<CashFlowSummaryRecord>> dayWiseSummaryList;
    private Hashtable<String, ArrayList<CashFlowHistoryRecord>> symWiseDetailList;

    public CashFlowExchgSummary() {
        dayWiseSummaryList = new Hashtable<String, ArrayList<CashFlowSummaryRecord>>();
        symWiseDetailList = new Hashtable<String, ArrayList<CashFlowHistoryRecord>>();
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public ArrayList<CashFlowSummaryRecord> getSummaryListForPeriod(String period) {
        if (period != null) {
            ArrayList<CashFlowSummaryRecord> list = dayWiseSummaryList.get(period);
            return list;
        } else {
            return null;
        }
    }

    public ArrayList<CashFlowHistoryRecord> getDetailedListForSymbol(String sKey) {
        if (sKey != null) {
            ArrayList<CashFlowHistoryRecord> list = symWiseDetailList.get(sKey);
            return list;
        } else {
            return null;
        }
    }

    public void setSummaryListForPeriod(ArrayList<CashFlowSummaryRecord> list, String period) {
        if (list != null && !list.isEmpty() && period != null && !period.isEmpty()) {
            dayWiseSummaryList.put(period, list);
        }
    }

    public void setDetailedListForSymbol(ArrayList<CashFlowHistoryRecord> list, String sKey) {
        if (list != null && !list.isEmpty() && sKey != null && !sKey.isEmpty()) {
            symWiseDetailList.put(sKey, list);
        }
    }
}
