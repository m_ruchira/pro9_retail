package com.isi.csvr.cashFlowWatch;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWMenu;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.forex.ForexBuySell;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.CommonPopup;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 18, 2008
 * Time: 10:19:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowHistorySummaryUI extends InternalFrame implements ApplicationListener, KeyListener, TWTabbedPaleListener, Themeable {

    private static final int HISTORY_MODE = 1;
    private static final int INTRADAY_MODE = 0;
    private static int SEL_EXCHANGE = 441;
    private static int SEL_SUB_MKT = 443;
    private static int SEL_SECTOR = 442;
    private static int SEL_PEROD = 445;
    private static int SEL_INDEX = 446;
    private static CashFlowHistorySummaryUI self;
    private final String DEFAULT = "----";
    ArrayList<CashFlowSummaryRecord> allSymList;
    ArrayList<CashFlowSummaryRecord> filteredList;
    private JPanel topPanel;
    private JPanel tablePanel;
    private JLabel lblSector;
    private JLabel lblPerid;
    private JLabel lblMarket;
    private JLabel lblIndex;
    private TWComboBox exchangeCombo;
    private ArrayList exchangeList;
    private ArrayList sectorList;
    private ArrayList marketList;
    private ArrayList indexList;
    private TWComboBox sectorCombo;
    private TWComboBox marketCombo;
    private TWComboBox periodCombo;
    private TWComboBox indexCombo;
    private Table summaryTable;
    private ViewSetting oSetting;
    private CashFlowSummaryModel oModel;
    private String selectedExchange;
    private boolean isDataLoading = false;
    private boolean isLoadedFromWsp = false;
    private boolean isFirsttime = false;
    private boolean isFirstDiscnctd = false;
    private JPanel searchPanel;
    private TWTextField searchField;
    private ToolBarButton searchBtn;
    private JButton btnHelp;
    private TWTabbedPane tabPanel;
    private Image tabImage;
    private String selectedKey;
    private TWMenu mnuTrade;
    private TWMenuItem mnuSell;
    private TWMenuItem mnuBuy;
    private TWMenuItem mnuGraph;
    private TWMenuItem mnuTimeNSales;
    private TWMenuItem mnuTimeNSalesSummary;
    private TWMenuItem mnuDTQ;
    private TWMenuItem mnuSnapQ;
    private TWMenuItem mnuCashFQ;
    private TWMenuItem mnuTPCalc;
    private CommonPopup popup;
    private Symbols indexSymbols = new Symbols();

    private CashFlowHistorySummaryUI() {
        createUI();
        allSymList = new ArrayList<CashFlowSummaryRecord>();
        filteredList = new ArrayList<CashFlowSummaryRecord>();
//        ExchangeStore.getSharedInstance().loadCashFlowExchangeIndices();
        Theme.registerComponent(this);
    }

    public static CashFlowHistorySummaryUI getSharedInstance() {
        if (self == null) {
            self = new CashFlowHistorySummaryUI();
            return self;
        } else {
            return self;
        }
    }


    private void createUI() {
        tabImage = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "dt_tabcenter.jpg")).getImage();
        this.setLayout(new BorderLayout());
        topPanel = new JPanel();
        /*{
            public void paint(Graphics g) {
//                super.paint(g);    //To change body of overridden methods use File | Settings | File Templates.
//                for (int i = 0; i < (getWidth()); i ++) {
//                    g.drawImage(tabImage, i, 0, this);
//                }
            }
        };*/
        tablePanel = new JPanel();
        exchangeList = new ArrayList();
        populateExchanges();
        exchangeCombo = new TWComboBox(new TWComboModel(exchangeList));
        sectorList = new ArrayList();
        marketList = new ArrayList();
        indexList = new ArrayList();
        sectorCombo = new TWComboBox(new TWComboModel(sectorList));
        marketCombo = new TWComboBox(new TWComboModel(marketList));
        indexCombo = new TWComboBox(new TWComboModel(indexList));
        periodCombo = new TWComboBox();
        populateperiodCombo();
        lblPerid = new JLabel(Language.getString("PERIOD"));
        lblMarket = new JLabel(Language.getString("CF_HISTORY_MARKET"));
        lblSector = new JLabel(Language.getString("SECTOR"));
        lblIndex = new JLabel(Language.getString("INDEX"));
        tabPanel = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "dt");
        tabPanel.addTabPanelListener(this);
        tabPanel.addTab(Language.getString("CFA_INTRADAY"), new JLabel());
        tabPanel.addTab(Language.getString("CFA_HISTORY"), new JLabel());
//        tabPanel.setSelectedIndex(0);
        btnHelp = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/charts/indicator_help.gif"));
        btnHelp.setContentAreaFilled(false);
        btnHelp.addActionListener(this);
        btnHelp.setBorder(BorderFactory.createEmptyBorder());

        searchPanel = new JPanel();
        searchField = new TWTextField();
        searchField.addKeyListener(this);
//        searchField.addFocusListener(this);
//        searchField.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchField.requestFocus(false);


        searchBtn = new ToolBarButton();
        searchBtn.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/cf_search.gif"));

        searchBtn.addActionListener(this);
        searchField.addKeyListener(this);
        createSummayTable();
        addComponents();
        this.setLocation(oSetting.getLocation());
        oSetting.setParent(this);
        this.setViewSetting(oSetting);
        this.setTable(summaryTable);
        this.applySettings();
        Application.getInstance().addApplicationListener(this);
        isFirsttime = true;
        GUISettings.applyOrientation(this);
        popup = summaryTable.getPopup();
        createPopop();

        popup.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                validatePopmenuItems();
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        applyTheme();
        for (int i = 0; i < tabPanel.getTabCount(); i++) {
            if (i == tabPanel.getSelectedIndex()) {
                tabPanel.getTabComponentAt(i).setForeground(Theme.getColor("CASHFLOW_ANALYSER_SELECTED_TAB_COLOR"));
            } else {
                tabPanel.getTabComponentAt(i).setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));
            }
        }
    }

    private JPanel createSearchPanel() {
        searchPanel.setLayout(new FlexGridLayout(new String[]{"85", "20"}, new String[]{"20"}));
        searchField.setBorder(null);
        searchBtn.setPreferredSize(new Dimension(20, 20));
        searchBtn.setBorder(null);

        searchPanel.add(searchField);
        searchPanel.add(searchBtn);
        return searchPanel;
    }

    private void populateExchanges() {
        SharedMethods.populateExchangesForTWCombo(exchangeList, true);
    }

    private void populateperiodCombo() {
        periodCombo.addItem(Language.getString("CF_SUMMARY_7D"));
        periodCombo.addItem(Language.getString("CF_SUMMARY_30D"));
        periodCombo.addItem(Language.getString("CF_SUMMARY_90D"));
    }

    private void addComponents() {
        this.getContentPane().setLayout(new BorderLayout());
        //0    1    2      3    4     5     6     7     8   9     10     11  12   13    14   15  16     17   18      19      20    21
        topPanel.setLayout(new FlexGridLayout(new String[]{"130", "3", "110", "3", "35", "1", "110", "3", "65", "1", "110", "3", "30", "1", "110", "3", "30", "1", "110", "100%", "105", "30"}, new String[]{"20"}, 2, 3));
        topPanel.setPreferredSize(new Dimension((int) oSetting.getSize().getWidth(), 26));
        topPanel.setOpaque(true);
        try {
            Color bgColor = Theme.getOptionalColor("TAB_" + "DT_" + "BGCOLOR");
            topPanel.setBackground(bgColor);
        } catch (Exception e) {
            topPanel.setBackground(null);
        }

        topPanel.add(tabPanel);
        topPanel.add(new JLabel());
        topPanel.add(exchangeCombo);
        topPanel.add(new JLabel());
        topPanel.add(lblSector);
        topPanel.add(new JLabel());
        topPanel.add(sectorCombo);
        topPanel.add(new JLabel());
        topPanel.add(lblMarket);
        topPanel.add(new JLabel());
        topPanel.add(marketCombo);
        topPanel.add(new JLabel());
        topPanel.add(lblIndex);
        topPanel.add(new JLabel());
        topPanel.add(indexCombo);
        topPanel.add(new JLabel());
        topPanel.add(lblPerid);
        topPanel.add(new JLabel());
        topPanel.add(periodCombo);
        topPanel.add(new JLabel());
        topPanel.add(createSearchPanel());
        topPanel.add(btnHelp);
//        topPanel.add(new JLabel());

//        topPanel.add(tabPanel);
//        topPanel.add(lblMkt);
//        topPanel.add(new JLabel());
//        topPanel.add(exchangeCombo);
//        topPanel.add(new JLabel());
//        topPanel.add(lblSector);
//        topPanel.add(new JLabel());
//        topPanel.add(sectorCombo);
//        topPanel.add(new JLabel());
//        topPanel.add(lblMarket);
//        topPanel.add(new JLabel());
//        topPanel.add(marketCombo);
//        topPanel.add(new JLabel());
//        topPanel.add(lblPerid);
//        topPanel.add(new JLabel());
//        topPanel.add(periodCombo);
//        topPanel.add(new JLabel());
////        topPanel.add(new JLabel());
//        topPanel.add(createSearchPanel());
//        topPanel.add(new JLabel());

        tablePanel.setLayout(new BorderLayout());
        tablePanel.add(summaryTable, BorderLayout.CENTER);

        this.getContentPane().add(topPanel, BorderLayout.NORTH);
        this.getContentPane().add(tablePanel, BorderLayout.CENTER);
        this.setSize(oSetting.getSize());
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setResizable(true);
        this.setIconifiable(true);
        this.setMaximizable(true);
        this.setClosable(true);
        this.setTitle(Language.getString("CASHFLOW_ANALYZER"));
        GUISettings.applyOrientation(topPanel);
        setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(this);
        addListeners();
        if (!exchangeList.isEmpty()) {
            // exchangeCombo.setSelectedIndex(0);
        }
        topPanel.updateUI();
        tabPanel.setSelectedIndex(0);
    }

    public void showWindow(boolean isVisable) {

        this.setVisible(isVisable);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        if (isFirsttime && !isLoadedFromWsp) {
            isFirsttime = false;
            firstTimeLoad();
        }
        if (isVisable) {
            if (oModel.getMode() == INTRADAY_MODE) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.CashFlow, "IntraDay");
            } else {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.CashFlow, "History");
            }

            try {
                if (this.isIcon())
                    this.setIcon(false);
                this.setSelected(true);

            } catch (Exception e) {

            }
        }

    }

    private void firstTimeLoad() {
        if (exchangeCombo.getItemCount() > 0) {
            exchangeCombo.setSelectedIndex(0);
        }
    }

    public void addListeners() {
        exchangeCombo.addActionListener(this);
        periodCombo.addActionListener(this);
        sectorCombo.addActionListener(this);
        marketCombo.addActionListener(this);
        indexCombo.addActionListener(this);
    }


    private void createSummayTable() {
        try {
            oSetting = ViewSettingsManager.getSummaryView("CASHFLOWHISTORY_SUMMARY_VIEW");
            if (oSetting == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        //  GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("CASHFLOWHISTORY_SUMMARY_COLS"));
        oModel = new CashFlowSummaryModel();
        oModel.setViewSettings(oSetting);
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");
        summaryTable = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (isDataLoading) {
                    FontMetrics fontMetrics = summaryTable.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                            (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - (fontMetrics.stringWidth(searching)) / 2) - (getWidth() / 2),
                            (getHeight() - icon.getImage().getHeight(this)) / 2 + 15, summaryTable.getBackground(), this);
//                    g.drawImage(icon.getImage(),
//                            (getWidth() - fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
//                            (getHeight() - icon.getImage().getHeight(this)) / 2, this);
                    fontMetrics = null;
                }
            }
        };
        summaryTable.setSortingEnabled();
        summaryTable.setModel(oModel);
        oModel.setTable(summaryTable);
        summaryTable.getTable().setDefaultRenderer(Object.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        summaryTable.getTable().setDefaultRenderer(Number.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        summaryTable.getTable().setDefaultRenderer(Boolean.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        summaryTable.getTable().setDefaultRenderer(Long.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        summaryTable.getTable().setDefaultRenderer(Double.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));

        summaryTable.getModel().updateGUI();
        summaryTable.getTable().addMouseListener(this);
        summaryTable.getPopup().showLinkMenus();
        ((AbstractTableModel) summaryTable.getTable().getModel()).fireTableDataChanged();


    }

    public boolean isDataLoading() {
        return isDataLoading;
    }

    public void setDataLoading(boolean dataLoading) {
        isDataLoading = dataLoading;
        exchangeCombo.setEnabled(!isDataLoading);
        periodCombo.setEnabled(!isDataLoading);
        sectorCombo.setEnabled(!isDataLoading);
        marketCombo.setEnabled(!isDataLoading);
        indexCombo.setEnabled(!isDataLoading);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == exchangeCombo) {
            String id = ((TWComboItem) exchangeCombo.getSelectedItem()).getId();
            selectedExchange = id;
            Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
            byte decimals = exg.getPriceDecimalPlaces();
            oModel.setDecimalCount(decimals);
            oSetting.putProperty(SEL_EXCHANGE, selectedExchange);

            loadSectors();
            String subMarketID = "";
            String indexID = "";

            if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                isFirstDiscnctd = true;
                populateMarketCombo(selectedExchange);
                try {
                    subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                } catch (Exception e1) {
                    TWComboItem marketAll = getDefaultMarket(marketList);
                    if (marketAll != null) {
                        marketCombo.setSelectedItem(marketAll);
                    } else {
                        marketCombo.setSelectedIndex(0);
                    }
                    subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                }
                marketCombo.setVisible(true);
                lblMarket.setVisible(true);
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(8, "65");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(9, "1");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(10, "110");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(11, "3");

            } else {
                lblMarket.setVisible(false);
                marketCombo.setVisible(false);
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(8, "1");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(9, "1");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(10, "1");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(11, "1");
            }

            if (exg.cashFlowExchageIndicesExists()) {
                populateIndexCombo(selectedExchange);
                try {
                    indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                } catch (Exception e1) {
                    indexCombo.setSelectedIndex(0);
                    indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                }
                indexCombo.setVisible(true);
                lblIndex.setVisible(true);
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(12, "30");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(13, "1");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(14, "110");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(15, "3");

            } else {
                indexCombo.setVisible(false);
                lblIndex.setVisible(false);
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(12, "1");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(13, "1");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(14, "1");
                ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(15, "1");
            }
            loadSummaryFile(false);
            createFilteredList(((TWComboItem) sectorCombo.getSelectedItem()).getId(), subMarketID, indexID);
            if ((!searchField.getText().equals("")) && (searchField.getText() != null)) {
                searchTable(searchField.getText());
            }
        } else if (e.getSource() == periodCombo && exchangeCombo.isEnabled()) {
            loadSummaryFile(true);
            String period = (String) periodCombo.getSelectedItem();
            oSetting.putProperty(SEL_PEROD, period);
        } else if (e.getSource() == sectorCombo && exchangeCombo.isEnabled()) {
            String subMkt = "";
            String index = "";
            String id = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
            Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
            if ((!exg.isUserSubMarketBreakdown()) || (exg.getSubMarketCount() == 0))
                subMkt = "";
            else {
                try {
                    subMkt = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                } catch (Exception e1) {
                    subMkt = "DEFAULT";
                }
            }

            if (exg.cashFlowExchageIndicesExists()) {
                try {
                    index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                } catch (Exception e1) {
                    index = "DEFAULT";
                }
            } else
                index = "";

            loadSummaryFile(true);
            createFilteredList(id, subMkt, index);
            if ((!searchField.getText().equals("")) && (searchField.getText() != null)) {
                searchTable(searchField.getText());
            }
            if (!isLoadedFromWsp) {
                oSetting.putProperty(SEL_SECTOR, id);
            }
        } else if (e.getSource().equals(marketCombo)) {

            String subMkt = "";
            String index = "";
            String id = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
            Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
            if ((!exg.isUserSubMarketBreakdown()) || (exg.getSubMarketCount() == 0))
                subMkt = "";
            else {
                try {
                    subMkt = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                } catch (Exception e1) {
                    subMkt = "DEFAULT";
                }
            }

            if (exg.cashFlowExchageIndicesExists()) {
                try {
                    index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                } catch (Exception e1) {
                    index = "DEFAULT";
                }
            } else
                index = "";

            loadSummaryFile(!isFirstDiscnctd);
            isFirstDiscnctd = false;

            createFilteredList(id, subMkt, index);
            if ((!searchField.getText().equals("")) && (searchField.getText() != null)) {
                searchTable(searchField.getText());
            }
            if (!isLoadedFromWsp) {
                oSetting.putProperty(SEL_SUB_MKT, subMkt);
            }
        } else if (e.getSource().equals(indexCombo)) {
            String subMkt = "";
            String index = "";
            String id = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
            Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
            if ((!exg.isUserSubMarketBreakdown()) || (exg.getSubMarketCount() == 0))
                subMkt = "";
            else {
                try {
                    subMkt = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                } catch (Exception e1) {
                    subMkt = "DEFAULT";
                }
            }

            if (exg.cashFlowExchageIndicesExists()) {
                try {
                    index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                    loadIndexSymbols(index);
                } catch (Exception e1) {
                    indexCombo.setSelectedIndex(0);
                    index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                }
            } else
                index = "";

            loadSummaryFile(!isFirstDiscnctd);
            isFirstDiscnctd = false;

            createFilteredList(id, subMkt, index);
            if ((!searchField.getText().equals("")) && (searchField.getText() != null)) {
                searchTable(searchField.getText());
            }
            if (!isLoadedFromWsp) {
                oSetting.putProperty(SEL_INDEX, index);
            }
        } else if (e.getSource().equals(searchBtn)) {
            loadSummaryFile(true);
            if ((!searchField.getText().equals("")) && (searchField.getText() != null)) {
                if (isValidInput())
                    searchTable(searchField.getText());
                else
                    searchField.setText("");
            } else {
                String subMkt = "";
                String index = "";
                String id = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
                Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
                if ((!exg.isUserSubMarketBreakdown()) || (exg.getSubMarketCount() == 0))
                    subMkt = "";
                else {
                    try {
                        subMkt = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                    } catch (Exception e1) {
                        subMkt = "DEFAULT";
                    }
                }

                if (exg.cashFlowExchageIndicesExists()) {
                    try {
                        index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                    } catch (Exception e1) {
                        index = "DEFAULT";
                    }
                } else
                    index = "";

                createFilteredList(id, subMkt, index);

            }

        } else if (e.getSource() == btnHelp) {
            NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_CASHFLOW_ANALYZER"));
        }

    }

    private void loadIndexSymbols(String index) {
        indexSymbols.clear();
        if (index == null || index == "" || index.equals("DEFAULT"))
            return;

        Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
        if (index.equals(Language.getString("ALL"))) {
            Enumeration<String> keyEnum = exg.getCashFlowExchangeIndices().keys();
            while (keyEnum.hasMoreElements()) {
                ArrayList<String> symbols = exg.getCashFlowExchangeIndices().get(keyEnum.nextElement());
                for (String symbol : symbols) {
                    if (!indexSymbols.isAlreadyIn(symbol))
                        indexSymbols.appendSymbol(symbol);
                }
            }
        } else {
            ArrayList<String> symbols = exg.getCashFlowExchangeIndices().get(index);
            for (String symbol : symbols) {
                indexSymbols.appendSymbol(symbol);
            }
        }

    }

    private void loadSummaryFile(boolean show) {
        oModel.setDatastore(null, null);
        CashFlowHistory.getSharedInstance().loadSummaryFile(selectedExchange, (String) periodCombo.getSelectedItem(), show);
    }

    public void loadDataToModel(ArrayList<CashFlowSummaryRecord> list) {
        if (list != null) {
            allSymList = list;
            String subMkt = "";
            String index = "";

            Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
            if ((!exg.isUserSubMarketBreakdown()) || (exg.getSubMarketCount() == 0))
                subMkt = "";
            else {
                try {
                    subMkt = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                } catch (Exception e) {
                    subMkt = "DEFAULT";
                }
            }

            if (exg.cashFlowExchageIndicesExists()) {
                try {
                    index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                } catch (Exception e) {
                    index = "DEFAULT";
                }
            } else
                index = "";

            createFilteredList(((TWComboItem) sectorCombo.getSelectedItem()).getId(), subMkt, index);

            if ((!searchField.getText().equals("")) && (searchField.getText() != null)) {
                searchTable(searchField.getText());
            }
            oModel.setDatastore(filteredList, exg.getSymbol());
            oModel.filterIntradayStore(((TWComboItem) sectorCombo.getSelectedItem()).getId(), subMkt, index);
            summaryTable.getTable().updateUI();
        }

    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == summaryTable.getTable() && e.getClickCount() > 1) {
            if (tabPanel.getSelectedIndex() == HISTORY_MODE) {
                int row = summaryTable.getTable().getSelectedRow();
                CashFlowSummaryRecord rec;
                if (summaryTable.isSortingEnabled() && summaryTable.getG_iLastSortedCol() != -1) {
                    rec = (CashFlowSummaryRecord) summaryTable.getTable().getModel().getValueAt(row, -1);
                    // CashFlowSummaryRecord rec = (CashFlowSummaryRecord) oModel.getValueAt(row, -1);
                } else {
                    rec = (CashFlowSummaryRecord) oModel.getValueAt(row, -1);
                }

                CashFlowHistory.getSharedInstance().showDetailWondow(rec.getExchange(), rec.getSKey());
            } else {
                int row = summaryTable.getTable().getSelectedRow();
                Stock rec;
                if (summaryTable.isSortingEnabled() && summaryTable.getG_iLastSortedCol() != -1) {
                    rec = (Stock) summaryTable.getTable().getModel().getValueAt(row, -1);
                    // CashFlowSummaryRecord rec = (CashFlowSummaryRecord) oModel.getValueAt(row, -1);
                } else {
                    rec = (Stock) oModel.getValueAt(row, -1);
                }
                // CashFlowSummaryRecord rec = (CashFlowSummaryRecord) oModel.getValueAt(row, -1);
                Client.getInstance().showCashFlowDetailQuote(rec.getKey(), false, false, false, LinkStore.LINK_NONE);
            }


        } else if (e.getSource() == summaryTable.getTable() && SwingUtilities.isRightMouseButton(e)) {
            int row = summaryTable.getTable().getSelectedRow();
//            CashFlowSummaryRecord rec = (CashFlowSummaryRecord) oModel.getValueAt(row, -1);
            CashFlowSummaryRecord rec = (CashFlowSummaryRecord) summaryTable.getTable().getModel().getValueAt(row, -1);

            selectedKey = rec.getSKey();
//            validatePopmenuItems();
            if (popup == null) {
                popup = summaryTable.getPopup();
                createPopop();
                popup.addPopupMenuListener(new PopupMenuListener() {
                    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                        validatePopmenuItems();
                    }

                    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                        //To change body of implemented methods use File | Settings | File Templates.
                    }

                    public void popupMenuCanceled(PopupMenuEvent e) {
                        //To change body of implemented methods use File | Settings | File Templates.
                    }
                });
            }

        }
//        super.mouseClicked(e);
    }


    public boolean finalizeWindow() {
        CashFlowHistory.getSharedInstance().cancellSummaryDownloads();
        setDataLoading(false);
//        oSetting.setLocation(this.getLocationOnScreen());
//        oSetting.setSize(this.getSize());
        return super.finalizeWindow();    //To change body of overridden methods use File | Settings | File Templates.
    }

    private void loadSectors() {
        sectorList.clear();
        sectorList.add(new TWComboItem("DEFAULT", DEFAULT));
        SharedMethods.populateSectorsForTWCombo(sectorList, selectedExchange);
        sectorCombo.revalidate();
        sectorCombo.repaint();
        sectorCombo.setSelectedIndex(0);
        try {
            sectorCombo.updateUI();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    private void createFilteredList(String sectorID, String subMktID, String indexID) {
        filteredList.clear();

        if (sectorID == null)
            sectorID = "";
        if (subMktID == null)
            subMktID = "";
        if (indexID == null)
            indexID = "";

        if (!allSymList.isEmpty()) {

            for (int i = 0; i < allSymList.size(); i++) {
                CashFlowSummaryRecord record = allSymList.get(i);
                String sKey = record.getSKey();
                Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
                if (!sectorID.equals("DEFAULT")) {
                    if (((TWComboItem) sectorCombo.getSelectedItem()).getId().equals(Language.getString("ALL"))) {
                        if (!subMktID.equals("DEFAULT")) {
                            if (!subMktID.equals("")) {
                                if (((TWComboItem) marketCombo.getSelectedItem()).getId().equals(Language.getString("ALL"))) {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                filteredList.add(record);
                                            }
                                        } else {
                                            filteredList.add(record);
                                        }
                                    }
                                } else if (stock != null && (stock.getMarketID().equals(subMktID))) {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                filteredList.add(record);
                                            }
                                        } else {
                                            filteredList.add(record);
                                        }
                                    }
                                }
                            } else {
                                if (!indexID.equals("DEFAULT")) {
                                     /*if(!indexID.equals("")){   // Shanaka - Cashflow all symbol
//                                        if(stock != null && indexSymbols.isAlreadyIn(stock.getKey())){
//                                            filteredList.add(record);
                                                filteredList.add(allSymList.get(i));
//                                        }
                                    }else{
                                        filteredList.add(record);
                                    }*/
                                    if (!indexID.equals("")) {
                                        if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                            filteredList.add(record);
                                        }
                                    } else {
                                        filteredList.add(record);
                                    }
                                }
                            }
                        }
                    } else if (stock != null && (stock.getSectorCode().equals(sectorID))) {
                        if (!subMktID.equals("DEFAULT")) {
                            if (!subMktID.equals("")) {
                                if (((TWComboItem) marketCombo.getSelectedItem()).getId().equals(Language.getString("ALL"))) {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                filteredList.add(record);
                                            }
                                        } else {
                                            filteredList.add(record);
                                        }
                                    }
                                } else if (stock != null && (stock.getMarketID().equals(subMktID))) {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                filteredList.add(record);
                                            }
                                        } else {
                                            filteredList.add(record);
                                        }
                                    }
                                }
                            } else {
                                if (!indexID.equals("DEFAULT")) {
                                    if (!indexID.equals("")) {
                                        if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                            filteredList.add(record);
                                        }
                                    } else {
                                        filteredList.add(record);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        oModel.filterIntradayStore(sectorID, subMktID, indexID);
        summaryTable.getTable().updateUI();
        ((AbstractTableModel) summaryTable.getTable().getModel()).fireTableDataChanged();
    }


    private void applySavedSettings() {
        try {
            String selex = (String) oSetting.getProperty(SEL_EXCHANGE);
            String subMarketID = "";
            String indexID = "";
            if (selex != null && !selex.isEmpty()) {
                for (int i = 0; i < exchangeList.size(); i++) {
                    TWComboItem item = (TWComboItem) exchangeList.get(i);
                    if (item.getId().equals(selex)) {
                        exchangeCombo.setSelectedIndex(i);
                        break;
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////
            if ((selex != null) && (!selex.equals(""))) {
                Exchange exg = ExchangeStore.getSharedInstance().getExchange(selex);
                if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                    populateMarketCombo(selectedExchange);
                    try {
                        subMarketID = oSetting.getProperty(SEL_SUB_MKT);
                        if ((subMarketID != null) && (!subMarketID.equals(""))) {
                            for (int i = 0; i < marketList.size(); i++) {
                                TWComboItem item = (TWComboItem) marketList.get(i);
                                if (item.getId().equals(subMarketID)) {
                                    marketCombo.setSelectedIndex(i);
                                    break;
                                }
                            }
                        } else {
                            subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                        }

                    } catch (Exception e1) {

                        TWComboItem marketAll = getDefaultMarket(marketList);
                        if (marketAll != null) {
                            marketCombo.setSelectedItem(marketAll);
                        } else {
                            marketCombo.setSelectedIndex(0);
                        }
                        subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                    }
                    lblMarket.setVisible(true);
                    marketCombo.setVisible(true);
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(8, "65");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(9, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(10, "110");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(11, "3");
                } else {
                    lblMarket.setVisible(false);
                    marketCombo.setVisible(false);
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(8, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(9, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(10, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(11, "1");
                }

                if (exg.cashFlowExchageIndicesExists()) {
                    populateIndexCombo(selectedExchange);
                    try {
                        indexID = oSetting.getProperty(SEL_INDEX);
                        if ((indexID != null) || !(indexID.equals(""))) {
                            for (int i = 0; i < indexList.size(); i++) {
                                TWComboItem item = (TWComboItem) indexList.get(i);
                                if (item.getId().equals(indexID)) {
                                    indexCombo.setSelectedIndex(i);
                                    break;
                                }
                            }
                        } else
                            indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                    } catch (Exception e1) {
                        indexCombo.setSelectedIndex(0);
                        indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                    }
                    indexCombo.setVisible(true);
                    lblIndex.setVisible(true);
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(12, "30");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(13, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(14, "110");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(15, "3");

                } else {
                    indexCombo.setVisible(false);
                    lblIndex.setVisible(false);
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(12, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(13, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(14, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(15, "1");
                }
            }
            /////////////////////////////////////////////////////////////////////
            String selSub = (String) oSetting.getProperty(SEL_SECTOR);
            if (selSub != null && !selSub.isEmpty()) {
                for (int i = 0; i < sectorList.size(); i++) {
                    TWComboItem item = (TWComboItem) sectorList.get(i);
                    if (item.getId().equals(selSub)) {
                        sectorCombo.setSelectedIndex(i);
                        break;
                    }
                }
            }


            String day = (String) oSetting.getProperty(SEL_PEROD);
            if (day != null) {
                periodCombo.setSelectedItem(day);
            } else {
                periodCombo.setSelectedIndex(0);
            }
            oModel.filterIntradayStore(selSub, subMarketID, indexID);
            summaryTable.setG_iLastSortedCol(oSetting.getSortColumn());
            summaryTable.setG_bSortOrder(oSetting.getSortOrder() == 1);
            summaryTable.applyInitialSorting();

//            /////////////////////////////////////////////////////////////////
//            String exgID = oSetting.getProperty(SEL_EXCHANGE);
//            String marketID = oSetting.getProperty(SEL_SUB_MKT);
//            populateMarketCombo(exgID);
//            if ((marketID != null) && (!marketID.equals(""))) {
////                  symbolsObject = DataStore.getSharedInstance().getSymbolsObject(exgID, marketID);
//                for (int i = 0; i < marketList.size(); i++) {
//                    TWComboItem item = (TWComboItem) marketList.get(i);
//                    if (item.getId().equals(marketID)) {
//                        marketCombo.setSelectedIndex(i);
//                        break;
//                    }
//                }
//            } else {
////                  symbolsObject = DataStore.getSharedInstance().getSymbolsObject(exgID, null);
//            }
            /////////////////////////////////////////////////////////////////
        } catch (Exception e) {
            isLoadedFromWsp = false;
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void applicationReadyForTransactions() {
        if (isLoadedFromWsp) {
            applySavedSettings();
            isLoadedFromWsp = false;
        }
    }

    public boolean isLoadedFromWsp() {
        return isLoadedFromWsp;
    }

    public void setLoadedFromWsp(boolean loadedFromWsp) {
        isLoadedFromWsp = loadedFromWsp;
        if (isLoadedFromWsp) {
            isFirsttime = false;
        }
    }

    public int getSelectedDateRange() {
        String period = (String) periodCombo.getSelectedItem();
        if (period != null && !period.isEmpty()) {
            if (period.equals(Language.getString("CF_SUMMARY_7D"))) {
                return 7;
            } else if (period.equals(Language.getString("CF_SUMMARY_30D"))) {
                return 30;
            } else if (period.equals(Language.getString("CF_SUMMARY_90D"))) {
                return 90;
            }
        }
        return -1;
    }

    private void populateMarketCombo(String exchange) {
        marketList.clear();
        marketList.add(new TWComboItem("DEFAULT", DEFAULT));
        marketList.add(new TWComboItem(Language.getString("ALL"), Language.getString("ALL")));
        SharedMethods.populateSubMarketsForTWCombo(marketList, true, exchange);
        marketCombo.setSelectedIndex(0);
        marketCombo.updateUI();
    }

    private void populateIndexCombo(String exchange) {
        Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
        indexList.clear();
        indexList.add(new TWComboItem("DEFAULT", DEFAULT));
        indexList.add(new TWComboItem(Language.getString("ALL"), Language.getString("ALL")));
        Enumeration indices = ex.getCashFlowExchangeIndices().keys();
        while (indices.hasMoreElements()) {
            String index = (String) indices.nextElement();
            indexList.add(new TWComboItem(index, index));
        }
        indexCombo.setSelectedIndex(0);
        indexCombo.updateUI();
    }

    private void changeMode() {

    }

    private void searchTable(String searchText) {
        filteredList.clear();
        String sectorID = "";
        String subMktID = "";
        String indexID = "";
        if (!allSymList.isEmpty()) {

            for (int i = 0; i < allSymList.size(); i++) {
                CashFlowSummaryRecord record = allSymList.get(i);
                String sKey = record.getSKey();
                Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
                String criteria = searchText;
                try {
                    sectorID = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
                } catch (Exception e) {
                    sectorID = "";
                }
                try {
                    subMktID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                } catch (Exception e) {
                    subMktID = "";
                }
                try {
                    indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                } catch (Exception e) {
                    indexID = "";
                }
                Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
                if ((!exg.isUserSubMarketBreakdown()) || (exg.getSubMarketCount() == 0))
                    subMktID = "";
                if (!exg.cashFlowExchageIndicesExists())
                    indexID = "";

                if ((record.getSymbol().contains(criteria.toUpperCase())) || (record.getDescription().contains(criteria.substring(0, 1).toUpperCase() + criteria.substring(1))) || (record.getDescription().contains(criteria))) {
                    if (!sectorID.equals("DEFAULT")) {
                        if (((TWComboItem) sectorCombo.getSelectedItem()).getId().equals(Language.getString("ALL"))) {
                            if (!subMktID.equals("DEFAULT")) {
                                if (!subMktID.equals("")) {
                                    if (((TWComboItem) marketCombo.getSelectedItem()).getId().equals(Language.getString("ALL"))) {
                                        if (!indexID.equals("DEFAULT")) {
                                            if (!indexID.equals("")) {
                                                if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                    filteredList.add(record);
                                                }
                                            } else {
                                                filteredList.add(record);
                                            }
                                        }
                                    } else if (stock != null && (stock.getMarketID().equals(subMktID))) {
                                        if (!indexID.equals("DEFAULT")) {
                                            if (!indexID.equals("")) {
                                                if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                    filteredList.add(record);
                                                }
                                            } else {
                                                filteredList.add(record);
                                            }
                                        }
                                    }
                                } else {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                filteredList.add(record);
                                            }
                                        } else {
                                            filteredList.add(record);
                                        }
                                    }
                                }
                            }
                        } else if (stock != null && (stock.getSectorCode().equals(sectorID))) {
                            if (!subMktID.equals("DEFAULT")) {
                                if (!subMktID.equals("")) {
                                    if (((TWComboItem) marketCombo.getSelectedItem()).getId().equals(Language.getString("ALL"))) {
                                        if (!indexID.equals("DEFAULT")) {
                                            if (!indexID.equals("")) {
                                                if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                    filteredList.add(record);
                                                }
                                            } else {
                                                filteredList.add(record);
                                            }
                                        }
                                    } else if (stock != null && (stock.getMarketID().equals(subMktID))) {
                                        if (!indexID.equals("DEFAULT")) {
                                            if (!indexID.equals("")) {
                                                if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                    filteredList.add(record);
                                                }
                                            } else {
                                                filteredList.add(record);
                                            }
                                        }
                                    }
                                } else {
                                    if (!indexID.equals("DEFAULT")) {
                                        if (!indexID.equals("")) {
                                            if (stock != null && indexSymbols.isAlreadyIn(stock.getKey())) {
                                                filteredList.add(record);
                                            }
                                        } else {
                                            filteredList.add(record);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        oModel.searchIntraDayStore(sectorID, subMktID, indexID, searchText);
        summaryTable.getTable().updateUI();
        ((AbstractTableModel) summaryTable.getTable().getModel()).fireTableDataChanged();

    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
//        if (e.getSource().equals(searchField)) {
//            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
//                loadSummaryFile(true);
//                if ((!searchField.getText().equals("")) && (searchField.getText() != null)) {
//                    if(isValidInput())
//                        searchTable(searchField.getText());
//                    else
//                        searchField.setText("");
//                } else {
//
//                    String subMkt = "";
//                    String index = "";
//                    String id = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
//                    Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
//                    if ((!exg.isUserSubMarketBreakdown()) || (exg.getSubMarketCount() == 0))
//                        subMkt = "";
//                    else{
//                        try {
//                            subMkt = ((TWComboItem) marketCombo.getSelectedItem()).getId();
//                        } catch (Exception e1) {
//                            subMkt = "DEFAULT";
//                        }
//                    }
//
//                    if(exg.cashFlowExchageIndicesExists()){
//                        try {
//                            index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
//                        } catch (Exception e1) {
//                            index = "DEFAULT";
//                        }
//                    }else
//                        index = "";
//
//                    createFilteredList(id, subMkt,index);
//                }
//
//            }
//        }
    }

    public void applyTheme() {
        tabImage = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "dt_tabcenter.jpg")).getImage();
        tabPanel.setSelectedFontColor(Theme.getColor("CASHFLOW_ANALYSER_SELECTED_TAB_COLOR"));
        tabPanel.setUnselectedFontColor(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));

        lblSector.setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));
        lblPerid.setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));
        lblMarket.setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));
        lblIndex.setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));

        sectorCombo.setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));
        periodCombo.setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));
        marketCombo.setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));
        indexCombo.setForeground(Theme.getColor("CASHFLOW_ANALYSER_UNSELECTED_TAB_COLOR"));

        try {
            Color bgColor = Theme.getOptionalColor("TAB_" + "DT_" + "BGCOLOR");
            topPanel.setBackground(bgColor);
        } catch (Exception e) {
            topPanel.setBackground(null);
        }

        searchBtn.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/cf_search.gif"));
        searchBtn.setBorder(null);
        searchField.setBorder(null);
        //  super.applyTheme();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {
        if (e.getSource().equals(searchField)) {
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                loadSummaryFile(true);
                if ((!searchField.getText().equals("")) && (searchField.getText() != null)) {
                    if (isValidInput())
                        searchTable(searchField.getText());
                    else
                        searchField.setText("");
                } else {

                    String subMkt = "";
                    String index = "";
                    String id = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
                    Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
                    if ((!exg.isUserSubMarketBreakdown()) || (exg.getSubMarketCount() == 0))
                        subMkt = "";
                    else {
                        try {
                            subMkt = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                        } catch (Exception e1) {
                            subMkt = "DEFAULT";
                        }
                    }

                    if (exg.cashFlowExchageIndicesExists()) {
                        try {
                            index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                        } catch (Exception e1) {
                            index = "DEFAULT";
                        }
                    } else
                        index = "";

                    createFilteredList(id, subMkt, index);
                }

            }
        }
    }

    public void tabStareChanged(TWTabEvent event) {
        //To change body of implemented methods use File | Settings | File Templates.
        try {
            if (oModel != null) {
                if (tabPanel.getSelectedIndex() == INTRADAY_MODE) {
                    System.out.println("Tab selected : " + tabPanel.getSelectedIndex());
                    oModel.setMode(INTRADAY_MODE);
                    periodCombo.setVisible(false);
                    lblPerid.setVisible(false);


                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(16, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(17, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(18, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(19, "100%");


                    summaryTable.resort();
                } else if (tabPanel.getSelectedIndex() == HISTORY_MODE) {
                    System.out.println("Tab selected : " + tabPanel.getSelectedIndex());
                    oModel.setMode(HISTORY_MODE);
                    periodCombo.setVisible(true);
                    lblPerid.setVisible(true);


                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(16, "38");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(17, "1");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(18, "110");
                    ((FlexGridLayout) topPanel.getLayout()).setColumnWidth(19, "100%");


                    summaryTable.resort();
                }
            }
            if (isVisible()) {
                if (oModel.getMode() == INTRADAY_MODE) {
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.CashFlow, "IntraDay");
                } else {
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.CashFlow, "History");
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void createPopop() {

        mnuTrade = new TWMenu(Language.getString("MUBASHER_TRADE"), "trade.gif");
        mnuTrade.setBackground(Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1"));
        mnuTrade.setEnabled(false);
        mnuBuy = new TWMenuItem(Language.getString("BUY"), "buy.gif");
        mnuBuy.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((DataStore.getSharedInstance().getStockObject(selectedKey)).getInstrumentType() == Meta.FOREX) {
                    ForexBuySell fBS = ForexBuySell.getSharedInstance(1, null);//getInstance();
                    fBS.setKey(selectedKey);
                    fBS.setVisible(true);
                    GUISettings.setLocationRelativeTo(fBS, Client.getInstance().getDesktop());

                } else {
                    Client.getInstance().doTransaction(TradeMeta.BUY, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                }
            }
        });
        mnuTrade.add(mnuBuy);

        mnuSell = new TWMenuItem(Language.getString("SELL"), "sell.gif");
        mnuSell.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        mnuSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((DataStore.getSharedInstance().getStockObject(selectedKey)).getInstrumentType() == Meta.FOREX) {
                    ForexBuySell fBS = ForexBuySell.getSharedInstance(1, null);//getInstance();
                    fBS.setKey(selectedKey);
                    fBS.setVisible(true);
                    GUISettings.setLocationRelativeTo(fBS, Client.getInstance().getDesktop());
                } else {
                    Client.getInstance().doTransaction(TradeMeta.SELL, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                }
            }
        });
        mnuTrade.add(mnuSell);

        mnuGraph = new TWMenuItem(Language.getString("GRAPH"), "graph.gif");
        mnuGraph.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //ShowGraph_Selected();
                Client.getInstance().showChart(null, null, null);
            }
        });
        mnuTimeNSales = new TWMenuItem(Language.getString("TIME_AND_SALES"), "time&sales.gif");
        mnuTimeNSales.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().mnu_TimeNSalesSymbol(selectedKey, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
            }
        });

        mnuTimeNSalesSummary = new TWMenuItem(Language.getString("TRADE_SUMMARY"), "time&salessum.gif");
        mnuTimeNSalesSummary.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().mnu_TimeNSalesSummaryForSymbol(selectedKey, false, false, LinkStore.LINK_NONE);
            }
        });
        mnuDTQ = new TWMenuItem(Language.getString("DETAIL_QUOTE"), "detailquote.gif");
        mnuDTQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().showDetailQuote(selectedKey);
            }
        });
        mnuSnapQ = new TWMenuItem(Language.getString("SNAP_QUOTE"), "snapquote.gif");
        mnuSnapQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(selectedKey), Meta.IT_SnapQuote)) {
                    Client.getInstance().showSnapQuote(selectedKey, false, false, false, LinkStore.LINK_NONE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(selectedKey)).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }

            }
        });
        mnuCashFQ = new TWMenuItem(Language.getString("CASHFLOW_DQ"), "cashflowdetailquote.gif");
        mnuCashFQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(selectedKey), Meta.IT_CashFlow)) {
//                    Client.getInstance().showCashFlowDetailQuote(selectedKey, false, false, false, LinkStore.LINK_NONE);
                    if (tabPanel.getSelectedIndex() == HISTORY_MODE) {
                        CashFlowHistory.getSharedInstance().showDetailWondow(SharedMethods.getExchangeFromKey(selectedKey), selectedKey);
                    } else {
                        Client.getInstance().showCashFlowDetailQuote(selectedKey, false, false, false, LinkStore.LINK_NONE);
                    }
                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(selectedKey)).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }
            }
        });
        mnuTPCalc = new TWMenuItem(Language.getString("PRICE_CALC"), "ordercalc.gif");
        mnuTPCalc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().mnu_MDepth_Calculator(selectedKey, TWTypes.TradeSides.BUY, false, false, LinkStore.LINK_NONE);
            }
        });
        if (popup != null) {
            popup.add(mnuTrade);
            popup.add(mnuTimeNSales);
//            popup.add(mnuTimeNSalesSummary);
            popup.add(mnuDTQ);
            popup.add(mnuSnapQ);
            popup.add(mnuCashFQ);
            popup.add(mnuTPCalc);
            GUISettings.applyOrientation(popup); //
        }

    }

    private void validatePopmenuItems() {
        if (tabPanel.getSelectedIndex() == INTRADAY_MODE) {
            CashFlowSummaryRecord rec = (CashFlowSummaryRecord) summaryTable.getTable().getModel().getValueAt((summaryTable.getTable().getSelectedRow()), -1);
            selectedKey = rec.getSKey();
            try {
                mnuTrade.setEnabled(TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(selectedExchange));
            } catch (Exception e) {

            }
            mnuTimeNSales.setVisible(Client.getInstance().isValidInfoType(selectedKey, Meta.IT_SymbolTimeAndSales, true));

            Exchange ex = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
            if (ex != null && (ex.getMarketStatus() == Meta.MARKET_CLOSE || ex.getMarketStatus() == Meta.MARKET_PRECLOSE)) {
                mnuTimeNSalesSummary.setVisible(Client.getInstance().isValidInfoType(selectedKey, Meta.IT_TimeAndSales_Summary, true));
            } else {
                mnuTimeNSalesSummary.setVisible(Client.getInstance().isValidInfoType(selectedKey, Meta.IT_TimeAndSales_Summary, true));
                mnuTimeNSalesSummary.setEnabled(false);
            }
//            mnuTimeNSalesSummary.setVisible(Client.getInstance().isValidInfoType(selectedKey, Meta.IT_TimeAndSales_Summary, true));
            mnuDTQ.setVisible(true);
            if (ExchangeStore.getSharedInstance().checkFeatureAvailability(selectedExchange, Meta.IT_SnapQuote)) {
                mnuSnapQ.setVisible(Client.getInstance().isValidInfoType(selectedKey, Meta.IT_SnapQuote, true));
            } else {
                mnuSnapQ.setVisible(false);
            }
            mnuCashFQ.setVisible(Client.getInstance().isValidInfoType(selectedKey, Meta.IT_CashFlow, true));
            mnuTPCalc.setVisible(Client.getInstance().isValidInfoType(selectedKey, Meta.IT_DepthCalculator, true));
            mnuTrade.setVisible(true);
            mnuTimeNSales.setVisible(true);
//            mnuTimeNSalesSummary.setVisible(true);
            mnuDTQ.setVisible(true);
            mnuSnapQ.setVisible(true);
            mnuCashFQ.setVisible(true);
            mnuTPCalc.setVisible(true);
            mnuCashFQ.setText(Language.getString("CASHFLOW_DQ"));

        } else if (tabPanel.getSelectedIndex() == HISTORY_MODE) {
            mnuTrade.setVisible(true);
            mnuTimeNSales.setVisible(true);
            mnuTimeNSalesSummary.setVisible(true);
            mnuDTQ.setVisible(true);
            mnuSnapQ.setVisible(true);
            mnuCashFQ.setVisible(true);
            mnuTPCalc.setVisible(true);
            mnuCashFQ.setText(Language.getString("CASHFLOWHISTORY_DETAIL_TITLE"));


        }
    }

    public void sortTable() {
        if ((summaryTable != null) && (oSetting != null)) {
            summaryTable.setG_iLastSortedCol(oSetting.getSortColumn());
            summaryTable.setG_bSortOrder(oSetting.getSortOrder() == 1);
            summaryTable.resort();
        }

    }

    private TWComboItem getDefaultMarket(ArrayList marketList) {
        Iterator it = marketList.iterator();
        while (it.hasNext()) {
            TWComboItem item = (TWComboItem) it.next();
            if (item.getId().equals(Language.getString("ALL"))) {
                return item;
            }
        }
        return null;
    }

    public Table getSummaryTable() {
        return summaryTable;
    }

    public Symbols getIndexSymbols() {
        return indexSymbols;
    }

    private boolean isValidInput() {
        boolean returnValue = false;
        if (DataStore.getSharedInstance().isValidCompany(((TWComboItem) exchangeCombo.getSelectedItem()).getId(), searchField.getText().toUpperCase())) {     //symbolField.getText().toUpperCase())
            returnValue = true;
        } else {
            new ShowMessage(Language.getString("INVALID_SYMBOL"), "E");
            returnValue = false;
        }

        return returnValue;
    }
}
