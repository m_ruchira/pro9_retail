package com.isi.csvr.cashFlowWatch;

import com.isi.csvr.TableSorter;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.ExchangeFormatInterface;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.table.TWTableRenderer;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.variationmap.VariationImage;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 16, 2008
 * Time: 7:30:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowHistoryRenderer extends TWBasicTableRenderer //todo
        implements TWTableRenderer {

    private static Color g_oUpColor;
    private static Color g_oDownColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static ImageBorder imageBorder;
    private final VariationImage variationImage;
    private String[] g_asColumns;
    private int[] g_asRendIDs;
    private boolean g_bLTR;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private TWDateFormat g_oDateFormat;
    private TWDateFormat g_oDateTimeFormatHM;
    private TWDateFormat g_oDateTimeFormatHMS;
    private TWDateFormat g_oTimeFormat;
    private String[] g_asMarketStatus = new String[4];
    private String g_sNA = "NA";
    private Color upColor;
    private Color downColor;
    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oPriceFormatLong;
    private TWDecimalFormat oNumericFormat;
    private TWDecimalFormat oQuantityFormat;
    private DefaultTableCellRenderer lblRenderer;
    private ImageIcon g_oUPImage;
    private ImageIcon g_oDownImage;
    private double doubleValue;
    private long longValue;
    private Long longObjectValue;
    private int intValue;
    private Date date;
    private boolean expandable;
    private boolean expanded;
    private TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");


    public CashFlowHistoryRenderer(String[] asColumns, int[] asRendIDs) {
        g_asColumns = asColumns;
        g_asRendIDs = asRendIDs;
        g_bLTR = Language.isLTR();

        reload();
        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HM_FORMAT"));//" yyyy/MM/dd HH:mm ");
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));//" dd:MM:yyyy '-' HH:mm:ss ");
        g_oTimeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));//" HH:mm:ss ");

        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oPriceFormatLong = new TWDecimalFormat(" ###,##0.0000 ");
        oQuantityFormat = new TWDecimalFormat(" ###,###,##0 ");
        oNumericFormat = new TWDecimalFormat(" ########## ");
        variationImage = new VariationImage();
        date = new Date();

        try {
            g_asMarketStatus[0] = Language.getString("STATUS_PREOPEN");
            g_asMarketStatus[1] = Language.getString("STATUS_OPEN");
            g_asMarketStatus[2] = Language.getString("STATUS_CLOSE");
            g_asMarketStatus[3] = Language.getString("STATUS_PRECLOSE");
        } catch (Exception e) {
            g_asMarketStatus[0] = g_sNA;
            g_asMarketStatus[1] = g_sNA;
            g_asMarketStatus[2] = g_sNA;
            g_asMarketStatus[3] = g_sNA;
        }

        try {
            g_oUPImage = new ImageIcon("images/Common/up.gif");
        } catch (Exception e) {
            g_oUPImage = null;
        }
        try {
            g_oDownImage = new ImageIcon("images/Common/down.gif");
        } catch (Exception e) {
            g_oDownImage = null;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oUpColor = Color.black;
        g_oDownColor = Color.black;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();  //todo
        try {
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
        } catch (Exception e) {
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
        imageBorder = new ImageBorder(1, 0, 1, 0, g_oSelectedBG);
    }

    public void propertyChanged(int property) {
    }


    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {


        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column); //todo


        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            upColor = g_oUpColor;
            downColor = g_oDownColor;
        }
        variationImage.setHeight(table.getRowHeight());
        lblRenderer.setForeground(foreground);
//        setForeground(foreground);
//        setBackground(background);
        lblRenderer.setBackground(background);

        try {
            try {
                oPriceFormat = (((ExchangeFormatInterface) table.getModel()).getDecimalFormat());
            } catch (Exception e) {
                oPriceFormat = (((ExchangeFormatInterface) ((TableSorter) table.getModel()).getModel()).getDecimalFormat());
            }
        } catch (Exception e) {
            // do nothing
        }


        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE

                    // doubleValue = toDoubleValue(value);
                    doubleValue = ((Double) value).doubleValue();
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;


                case 4: // QUANTITY
                    long qty = (Long) value;
                    lblRenderer.setText(oQuantityFormat.format(qty));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
//                case 5: // CHANGE
//                    //doubleValue = toDoubleValue(value);
//                    doubleValue = (Double)(value);
//                    lblRenderer.setText(oPriceFormat.format(doubleValue));
//                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
//                    if (doubleValue > 0)
//                        lblRenderer.setForeground(upColor);
//                    else if (doubleValue < 0)
//                        lblRenderer.setForeground(downColor);
//                    break;
//                case 6: // % CHANGE
//                    //adPrice = (double[])value;
//                    doubleValue = toDoubleValue(value);
//                    lblRenderer.setText(oPriceFormat.format(doubleValue));
//                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
//                    if (doubleValue > 0)
//                        lblRenderer.setForeground(upColor);
//                    else if (doubleValue < 0)
//                        lblRenderer.setForeground(downColor);
//                    break;
                case 7: // DATE

                    //longValue = toLongValue(value);
                    longValue = (Long) (value);
                    if (longValue <= Constants.ONE_DAY)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
//                case 'L': // LONG DATE for Net change calculator
//                    longObjectValue = (Long) value;
//                    if (longObjectValue.longValue() <= Constants.ONE_DAY)
//                        lblRenderer.setText(g_sNA);
//                    else {
//                        date.setTime(longObjectValue.longValue());
//                        lblRenderer.setText(g_oDateFormat.format(date));
//                    }
//                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
//                    break;


                case 'h': // Cash Map
                    doubleValue = ((Double) value);
//                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    variationImage.setType(VariationImage.TYPE_CASH_MAP);
                    variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setHorizontalTextPosition(g_iCenterAlign);
                    lblRenderer.setForeground(Color.black);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        variationImage.setValue(0);
                        lblRenderer.setText("");
                    } else {
                        variationImage.setValue(doubleValue);
                        lblRenderer.setIcon(variationImage);
                        lblRenderer.setText(oPChangeFormat.format(doubleValue * 100));
                    }
                    lblRenderer.setBackground(Color.BLACK);
                    if (isSelected) {
                        lblRenderer.setBorder(imageBorder);
                    } else {
                        lblRenderer.setBorder(null);
                    }
                    break;
//                case 'A': // TIME
//                    longValue = toLongValue(value);
//                    if (longValue == 0) {
//                        lblRenderer.setText(g_sNA);
//                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
//                    } else {
//                        date.setTime(longValue);
//                        lblRenderer.setText(g_oTimeFormat.format(date));
//                        lblRenderer.setHorizontalAlignment(JLabel.CENTER);
//                    }
//                    break;
//                case 'B': // Broker ID
//                    if ((value == null) || (((String) value)).equals("")
//                            || (((String) value)).equals("null"))
//                        lblRenderer.setText(g_sNA);
//                    else
//                        lblRenderer.setText((String) value);
//                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
//                    break;
//                case 'M': // Market Status
//                    try {
//                        lblRenderer.setText(g_asMarketStatus[toIntValue(value) - 1]);
//                    } catch (Exception e) {
//                        lblRenderer.setText(g_asMarketStatus[2]);
//                    }
//                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
//                    break;
//                case 'Z': // Market Status
//                    lblRenderer.setText((String) value);
//                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
//                    break;
//                case 'q':
//                   lblRenderer.setText(oQuantityFormat.format(toIntValue(value)));
//                   lblRenderer.setHorizontalAlignment(g_iNumberAlign);
//                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    public static class ImageBorder extends MatteBorder {


        ImageBorder(int top, int left, int bottom, int right, Color matteColor) {
            super(top, left, bottom, right, matteColor);
        }

        ImageBorder(Insets borderInsets, Color matteColor) {
            super(borderInsets, matteColor);
        }

        ImageBorder(int top, int left, int bottom, int right, Icon tileIcon) {
            super(top, left, bottom, right, tileIcon);
        }

        ImageBorder(Insets borderInsets, Icon tileIcon) {
            super(borderInsets, tileIcon);
        }

        ImageBorder(Icon tileIcon) {
            super(tileIcon);
        }

        public void setColor(Color color) {
            super.color = color;
        }
    }

}
