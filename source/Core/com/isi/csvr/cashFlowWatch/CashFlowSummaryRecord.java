package com.isi.csvr.cashFlowWatch;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 17, 2008
 * Time: 3:57:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowSummaryRecord {
    private String exchange;
    private String symbol;
    private String description;
    private long volume;
    private double turnover;
    private double percentage;
    private double cashInTurnover;
    private double cashOutTurnover;
    private double netValue;

    private String sKey;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public double getTurnover() {
        return turnover;
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public String getSKey() {
        return sKey;
    }

    public void setSKey(String sKey) {
        this.sKey = sKey;
    }

    public double getCashInTurnover() {
        return cashInTurnover;
    }

    public void setCashInTurnover(double cashInTurnover) {
        this.cashInTurnover = cashInTurnover;
    }

    public double getCashOutTurnover() {
        return cashOutTurnover;
    }

    public void setCashOutTurnover(double cashOutTurnover) {
        this.cashOutTurnover = cashOutTurnover;
    }

    public double getNetValue() {
        netValue = cashInTurnover - cashOutTurnover;
        return netValue;
    }

}
