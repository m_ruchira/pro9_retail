package com.isi.csvr.cashFlowWatch;

import javax.swing.*;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 17, 2008
 * Time: 8:19:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowHostoryTableUI extends BasicTableUI {

    public void paint(Graphics g, JComponent c) {
        Rectangle oldClipBounds = g.getClipBounds();
        Rectangle clipBounds = new Rectangle(oldClipBounds);
        int tableWidth = table.getColumnModel().getTotalColumnWidth();
        clipBounds.width = Math.min(clipBounds.width, tableWidth);
        g.setClip(clipBounds);

        int firstIndex = table.rowAtPoint(new Point(0, clipBounds.y));
        int lastIndex = table.getRowCount() - 1;

        Rectangle rowRect = new Rectangle(0, 0,
                tableWidth, table.getRowHeight() + table.getRowMargin());
        rowRect.y = firstIndex * rowRect.height;

        for (int index = firstIndex; index <= lastIndex; index++) {
            if (rowRect.intersects(clipBounds)) {
                paintRow(g, index);
            }
            rowRect.y += rowRect.height;
        }
        g.setClip(oldClipBounds);
    }

    private void paintRow(Graphics g, int row) {
        Rectangle rect = g.getClipBounds();
        // boolean drawn  = false;


        int numColumns = table.getColumnCount();
        int totRows = table.getRowCount();

        for (int column = 0; column < numColumns; column++) {
            Rectangle cellRect = table.getCellRect(row, column, true);

//            if (row == totRows-1 && column<5) {
//                if (Language.isLTR())
//                    cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true),table.getCellRect(row, 2, true),
//                    table.getCellRect(row, 3, true),table.getCellRect(row, 4, true));
//                else
//                    cellRect = combine(table.getCellRect(row, 4, true), table.getCellRect(row, 3, true),table.getCellRect(row, 2, true),
//                    table.getCellRect(row, 1, true),table.getCellRect(row, 0, true));
//            }
//             if (row == totRows-1 && column<2) {
//                if (Language.isLTR())
//                    cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
//                else
//                    cellRect = combine(table.getCellRect(row, 4, true), table.getCellRect(row, 3, true),table.getCellRect(row, 2, true),
//                    table.getCellRect(row, 1, true),table.getCellRect(row, 0, true));
//            }
//            if (row == 10) {
//                if (((column == 0) || (column == 1))) {
//                    if (Language.isLTR())
//                        cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
//                    else
//                        cellRect = combine(table.getCellRect(row, 1, true), table.getCellRect(row, 0, true));
//                } else if (((column == 2) || (column == 3))) {
//                    if (Language.isLTR())
//                        cellRect = combine(table.getCellRect(row, 2, true), table.getCellRect(row, 3, true));
//                    else
//                        cellRect = combine(table.getCellRect(row, 3, true), table.getCellRect(row, 2, true));
//                }
//            }
            if (cellRect.intersects(rect)) {
                //drawn = true;
                paintCell(g, cellRect, row, column);
            } //else {
            //   if (drawn) break;
            //}

        }
    }

    private void paintCell(Graphics g, Rectangle cellRect, int row, int column) {
        int spacingHeight = table.getRowMargin();
        int spacingWidth = table.getColumnModel().getColumnMargin();

        try {
            cellRect.setBounds(cellRect.x + spacingWidth / 2,
                    cellRect.y + spacingHeight / 2,
                    cellRect.width - spacingWidth, cellRect.height - spacingHeight);

            if (table.isEditing() && table.getEditingRow() == row &&
                    table.getEditingColumn() == column) {
                Component component = table.getEditorComponent();
                component.setBounds(cellRect);
                component.validate();
            } else {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component component = table.prepareRenderer(renderer, row, column);

                if (component.getParent() == null) {
                    rendererPane.add(component);
                }
                rendererPane.paintComponent(g, component, table, cellRect.x, cellRect.y,
                        cellRect.width, cellRect.height, true);
                if (table.getShowHorizontalLines()) {
                    g.setColor(table.getGridColor());
                    g.drawLine(cellRect.x, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                    if (table.getRowCount() == row + 1) {
                        g.drawLine(cellRect.x, cellRect.y + cellRect.height, cellRect.x + cellRect.width, cellRect.y + cellRect.height);
                    }
                }
                if (table.getShowVerticalLines()) {
                    g.setColor(table.getGridColor());
                    g.drawLine(cellRect.x, cellRect.y, cellRect.x, cellRect.y + cellRect.height);
                    if (table.getColumnCount() == column + 1) {
                        g.drawLine(cellRect.x + cellRect.width, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                    }
                }
            }
        } catch (Exception e) {

        }
    }
}
