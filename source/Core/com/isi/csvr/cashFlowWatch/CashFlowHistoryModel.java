package com.isi.csvr.cashFlowWatch;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 16, 2008
 * Time: 12:17:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowHistoryModel extends CommonTable implements TableModel, CommonTableInterface {

    private ArrayList datastore;
    private int range;

    public CashFlowHistoryModel() {
    }

    public static int compare(Object o1, Object o2, int sortColumn, int sortOrder) {
        CashFlowHistoryRecord rec1 = (CashFlowHistoryRecord) o1;
        CashFlowHistoryRecord rec2 = (CashFlowHistoryRecord) o2;
        switch (sortColumn) {
            case 0:
                return compareValues(rec1.getDate(), rec2.getDate(), sortOrder);
            case 1:
                return compareValues(rec1.getLastPrice(), rec2.getLastPrice(), sortOrder);
            case 2:
                return compareValues(rec1.getCashInOrders(), rec2.getCashInOrders(), sortOrder);
            case 3:
                return compareValues(rec1.getCashInVloume(), rec2.getCashInVloume(), sortOrder);
            case 4:
                return compareValues(rec1.getCashInTurnover(), rec2.getCashInTurnover(), sortOrder);
            case 5:
                return compareValues(rec1.getCashoutOrders(), rec2.getCashoutOrders(), sortOrder);
            case 6:
                return compareValues(rec1.getCashoutVolume(), rec2.getCashoutVolume(), sortOrder);
            case 7:
                return compareValues(rec1.getCashoutTurnover(), rec2.getCashoutTurnover(), sortOrder);
            case 8:
                return compareValues(rec1.getNetValue(), rec2.getNetValue(), sortOrder);
            case 9:
                return compareValues(rec1.getPercentage(), rec2.getPercentage(), sortOrder);
            default:
                return 0;
        }
    }

    private static int compareValues(String val1, String val2, int sortOrder) {
        return ((val1.toUpperCase()).compareTo((val2.toUpperCase())) * sortOrder);
    }

    private static int compareValues(int val1, int val2, int sortOrder) {
        return ((val1 - val2) * sortOrder);
    }

    private static int compareValues(long val1, long val2, int sortOrder) {
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

    private static int compareValues(double val1, double val2, int sortOrder) {
        if (Double.isNaN(val1) && Double.isNaN(val2)) {
            return 0;
        } else if (Double.isNaN(val1)) {
            return 1;
        } else if (Double.isNaN(val2)) {
            return -1;
        }
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

    public void setDatastore(ArrayList datastore) {
        this.datastore = datastore;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getRowCount() {
        if (datastore != null) {

            int storesize = datastore.size();
            if (range == -1) {
                return storesize;
            } else if (storesize < range) {
                return storesize;
            } else {
                return range;
            }

        } else {
            return 0;
        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        try {
            CashFlowHistoryRecord rec = (CashFlowHistoryRecord) datastore.get(rowIndex);

            switch (columnIndex) {

                case 0:
                    return rec.getDate();
                case 1:
                    return rec.getLastPrice();
                case 2:
                    return rec.getCashInOrders();
                case 3:
                    return rec.getCashInVloume();
                case 4:
                    return rec.getCashInTurnover();
                case 5:
                    return rec.getCashoutOrders();
                case 6:
                    return rec.getCashoutVolume();
                case 7:
                    return rec.getCashoutTurnover();
                case 8:
                    return rec.getNetValue();
                case 9:
                    return rec.getPercentage();
//                        return new DoubleTransferObject().setValue(rec.getPercentage());
                default:
                    return "";

            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return "";
        }

    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (iCol) {


            case 1:
            case 4:
            case 7:
            case 8:
            case 9:
                return Double.class;
            case 0:
            case 2:
            case 3:
            case 5:
            case 6:
                return Long.class;
            default:
                return Object.class;

        }


    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }
}


