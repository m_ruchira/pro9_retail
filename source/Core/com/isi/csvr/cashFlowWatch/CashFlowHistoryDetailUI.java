package com.isi.csvr.cashFlowWatch;

import com.isi.csvr.Client;
import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.chart.GraphFrame;
import com.isi.csvr.downloader.CFHistoryDownloadManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.ColumnGroup;
import com.isi.csvr.util.GroupableTableHeader;

import javax.swing.*;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 16, 2008
 * Time: 11:17:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowHistoryDetailUI extends InternalFrame implements TWTabbedPaleListener, Themeable {

    public static final String TABLE_PANEL = "tablepanel";
    public static final String CHART_PANEL = "chartpanel";
    public static CashFlowHistoryDetailUI selfRef;
    SortButtonRenderer renderer;
    String sInitKey = "xxx~xxxx~0";
    private ViewSetting oSetting;
    private Table detailTable;
    private CashFlowHistoryModel oModel;
    private JPanel middlePanel;
    private JPanel tablePanel;
    private JPanel chartPanel;
    private JPanel topPanel;
    private TWTabbedPane tabpane;
    private GraphFrame cashFlowGraph;
    private SimpleDateFormat format;
    private boolean isDataLoading;
    private ArrayList<CashFlowHistoryRecord> historyRecords;

    private CashFlowHistoryDetailUI() {
        createUI();
        historyRecords = new ArrayList<CashFlowHistoryRecord>();
    }

    public static CashFlowHistoryDetailUI getSharedInstance() {
        if (selfRef == null) {
            selfRef = new CashFlowHistoryDetailUI();
        }
        return selfRef;
    }

    private void createUI() {

        //  middlePanel = new JPanel(new CardLayout(1,1));
        format = new SimpleDateFormat("yyyyMMdd");
        middlePanel = new JPanel(new BorderLayout());
        topPanel = new JPanel();
        tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());
        chartPanel = new JPanel();
        tabpane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "dt");
        tabpane.addTabPanelListener(this);
        createTablePanel();

        this.setLayout(new BorderLayout());
        addComponents();
        this.setLocation(oSetting.getLocation());
        oSetting.setParent(this);
        this.setViewSetting(oSetting);
        oSetting.setLayer(GUISettings.TOP_LAYER);
        //   this.setViewSetting(oSetting);
        setTitle(oSetting.getCaption());
        this.setSize(oSetting.getSize());
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTable(detailTable);
//        setLayer(GUISettings.TOP_LAYER);
        this.applySettings();
        Client.getInstance().getDesktop().add(this);
        setLayer(GUISettings.TOP_LAYER);
        this.addComponentListener(this);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);

    }

    public ArrayList<CashFlowHistoryRecord> getHistoryDetailArray() {
        return new ArrayList();
    }

    public void showwidow() {
        this.setVisible(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        // new CashFlowDetailDownloader("TDWL","1010",0,Meta.CASHFLOW_HISTORY,Constants.CONTENT_PATH_PRIMARY);
        //  new CashFlowSummaryDownloader("TDWL",Meta.CASHFLOW_SUMMARY,Constants.CONTENT_PATH_PRIMARY);

        if (tabpane.getSelectedIndex() == 0) {
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.CashFlow, "HistoryDetails");
        } else if (tabpane.getSelectedIndex() == 1) {
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.CashFlow, "HistoryChart");
        }
    }

    public void setGraphBasesymbol(String key) {
        cashFlowGraph.setGraphBaseSymbol(key);
        cashFlowGraph.applyCashFlowTemplate();
        cashFlowGraph.setCashFlowGraphMode(false, false);
//        GraphFrame cfGraph =   (GraphFrame)tabpane.getComponentAt(1);
//        cfGraph.setGraphBaseSymbol(key);
//        cfGraph.setVisible(true);
        try {
//        cfGraph.setSelected(true);
        } catch (Exception e) {

        }
//        tabpane.setSelectedIndex(0);


    }

    private void addComponents() {
        tablePanel.add(detailTable, BorderLayout.CENTER);
        tabpane.addTab(Language.getString("DATA"), tablePanel);

        CreateGrpah();
        chartPanel.setLayout(new BorderLayout());
        chartPanel.add(cashFlowGraph, BorderLayout.CENTER);
        tabpane.addTab(Language.getString("CHART"), chartPanel);

//        tabpane.addTab(Language.getString("CHART"), cashFlowGraph);
        //   middlePanel.add(ta, BorderLayout.CENTER);
        this.getContentPane().add(topPanel, BorderLayout.NORTH);
        this.getContentPane().add(tabpane, BorderLayout.CENTER);
        GUISettings.applyOrientation(this.getContentPane());
        tabpane.setSelectedIndex(0);


    }

    public void CreateGrpah() {

        ViewSetting defaultSettings;
        cashFlowGraph = new GraphFrame(null);
        cashFlowGraph.getGraph().setCashFlowMode(true);
//        cashFlowGraph.setCashFlowMode(true);
        cashFlowGraph.setTitleVisible(false);
        cashFlowGraph.setDetached(true);
        cashFlowGraph.setResizable(true);
//                    cashFlowGraph.setMaximizable(true);
        cashFlowGraph.setMaximizable(false);
//                    cashFlowGraph.setIconifiable(true);
        cashFlowGraph.setIconifiable(false);
        cashFlowGraph.setClosable(true);
        try {
            cashFlowGraph.setSelected(true);
        } catch (Exception ex) {
        }

        ViewSettingsManager vsManager = Client.getInstance().getViewSettingsManager();
        defaultSettings = vsManager.getSymbolView("OUTER_CHART").getObject();
        if (defaultSettings != null) {
            defaultSettings.setParent(cashFlowGraph);
            cashFlowGraph.setViewSettings(defaultSettings);
            cashFlowGraph.setSize(defaultSettings.getSize());
            // GUISettings.setLocationRelativeTo(graph, oTopDesktop);
            defaultSettings.setID("" + System.currentTimeMillis());
            cashFlowGraph.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }

        // setting the symbol

        cashFlowGraph.isSendOHLCRequest = false;

        cashFlowGraph.popupCashFlowGraphData(sInitKey);
        cashFlowGraph.setCashFlowGraphMode(false, false);
        cashFlowGraph.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        defaultSettings = null;
        cashFlowGraph.show();
        /****************************toggling not enabled */
//            if (ChartSettings.getSettings().isOpenInProChart() && id == null) { //TODO
//                graph.toggleDetach();
//            }
//            cashFlowGraph = null;

    }

    private void createTablePanel() {

        try {
            oSetting = ViewSettingsManager.getSummaryView("CASHFLOWHISTORY_DETAIL_VIEW");
            if (oSetting == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        //  GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("CASHFLOWHISTORY_DETAIL_COLS"));
        oModel = new CashFlowHistoryModel();
        oModel.setViewSettings(oSetting);
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");

        detailTable = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (isDataLoading) {
                    FontMetrics fontMetrics = detailTable.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                            (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - (fontMetrics.stringWidth(searching)) / 2) - (getWidth() / 2),
                            (getHeight() - icon.getImage().getHeight(this)) / 2 + 15, detailTable.getBackground(), this);
//                    g.drawImage(icon.getImage(),
//                            (getWidth() - fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
//                            (getHeight() - icon.getImage().getHeight(this)) / 2, this);
                    fontMetrics = null;
                    /*
                     if (SearchedAnnouncementStore.getSharedInstance().isSearchInProgress()) {
                        FontMetrics fontMetrics = table2.getFontMetrics(getFont());
                        g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                                (getHeight()) / 2);
                        g.drawImage(icon.getImage(),
                                (getWidth() - (fontMetrics.stringWidth(searching)) / 2)-(getWidth()/2),
                                (getHeight() - icon.getImage().getHeight(this)) / 2+15, table2.getBackground(), this);
                        // + fontMetrics.stringWidth(searching) + 10
                        fontMetrics = null;
                    }
                     */
                }
            }
        };
        oSetting.setParent(this);
        detailTable.setSortingEnabled();
        detailTable.setGroupableHeaderModel(oModel);
        //detailTable.setModel(oModel);

        oModel.setTable(detailTable);
        detailTable.getTable().setDefaultRenderer(Object.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        detailTable.getTable().setDefaultRenderer(Number.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        detailTable.getTable().setDefaultRenderer(Boolean.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        detailTable.getTable().setDefaultRenderer(Long.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        detailTable.getTable().setDefaultRenderer(Double.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
        detailTable.getTable().getTableHeader().setReorderingAllowed(false);
        detailTable.getPopup().enableUnsort(false);
        detailTable.getModel().updateGUI();
        Theme.registerComponent(detailTable);

        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSetting);
        renderer = new SortButtonRenderer(true);
        renderer.applyTheme();

        TableColumnModel cm = detailTable.getTable().getColumnModel();
        ColumnGroup cin = new ColumnGroup(Language.getString("CF_CASH_IN"));

        cin.add(cm.getColumn(2));
        cin.add(cm.getColumn(3));
        cin.add(cm.getColumn(4));
        //cin.setHeaderRenderer(renderer);
        oSetting.getColumnHeadings()[2] = Language.getString("CF_CASH_IN") + " - " + oSetting.getColumnHeadings()[2];
        oSetting.getColumnHeadings()[3] = Language.getString("CF_CASH_IN") + " - " + oSetting.getColumnHeadings()[3];
        oSetting.getColumnHeadings()[4] = Language.getString("CF_CASH_IN") + " - " + oSetting.getColumnHeadings()[4];

        ColumnGroup cout = new ColumnGroup(Language.getString("CF_CASH_OUT"));
        cout.add(cm.getColumn(5));
        cout.add(cm.getColumn(6));
        cout.add(cm.getColumn(7));
//        cout.setHeaderRenderer(renderer);
        oSetting.getColumnHeadings()[5] = Language.getString("CF_CASH_OUT") + " - " + oSetting.getColumnHeadings()[5];
        oSetting.getColumnHeadings()[6] = Language.getString("CF_CASH_OUT") + " - " + oSetting.getColumnHeadings()[6];
        oSetting.getColumnHeadings()[7] = Language.getString("CF_CASH_OUT") + " - " + oSetting.getColumnHeadings()[7];

        GroupableTableHeader header = (GroupableTableHeader) (detailTable.getTable().getTableHeader());
        header.addGroup(cin);
        header.addGroup(cout);
//
//        detailTable.getTable().setDefaultRenderer(Object.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings() , oModel.getRendIDs()));
//        detailTable.getTable().setDefaultRenderer(Number.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings() , oModel.getRendIDs()));
//        detailTable.getTable().setDefaultRenderer(Boolean.class, new CashFlowHistoryRenderer(oModel.getViewSettings().getColumnHeadings() , oModel.getRendIDs()));

        TableColumnModel colmodel = detailTable.getTable().getColumnModel();
        int n = oModel.getColumnCount();

        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }
        detailTable.createGroupableTable();


        detailTable.getTable().getTableHeader().setDefaultRenderer(renderer);
        detailTable.getTable().getTableHeader().updateUI();
        detailTable.getTable().getTableHeader().repaint();
        detailTable.getModel().updateGUI();
        // loadDataStore();
        //   new CashFlowDetailDownloader("TWDL","1010",0,Meta.CASHFLOW_HISTORY,Constants.CONTENT_PATH_PRIMARY);


    }

    public void tabStareChanged(TWTabEvent event) {
        try {
            if (tabpane.getSelectedIndex() == 0) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.CashFlow, "HistoryDetails");
            } else if (tabpane.getSelectedIndex() == 1) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.CashFlow, "HistoryChart");
            }
        } catch (Exception e) {
        }
    }

    public boolean isDataLoading() {
        return isDataLoading;
    }

    public void setDataLoading(boolean dataLoading) {
        isDataLoading = dataLoading;
    }

    public void loadDataToModel(ArrayList<CashFlowHistoryRecord> list) {
        if (list != null) {
            historyRecords = list;
            oModel.setDatastore(list);
            // CashFlowHistorySummaryUI.getSharedInstance().getSelectedDateRange();
            oModel.setRange(CashFlowHistorySummaryUI.getSharedInstance().getSelectedDateRange());
            //  detailTable.getModel().updateGUI();
            detailTable.getTable().updateUI();
            cashFlowGraph.setCashFlowGraphMode(false, false);
        }

    }

    public ArrayList<CashFlowHistoryRecord> getHistoryRecords() {
        return historyRecords;
    }

    public void setHistoryRecords(ArrayList<CashFlowHistoryRecord> historyRecords) {
        this.historyRecords = historyRecords;
    }

    public boolean finalizeWindow() {
        CFHistoryDownloadManager.detailSem.release();
        isDataLoading = false;
        CashFlowHistory.getSharedInstance().cancellDetailDownloads();
        return super.finalizeWindow();
    }

    public void componentResized(ComponentEvent e) {

    }

    public void clearStore() {
        oModel.setDatastore(null);
    }

    public void adjustTitle(String sKey) {
        String nwTitle = oSetting.getCaption() + "  " + SharedMethods.getSymbolFromKey(sKey);
        this.setTitle(nwTitle);
    }

    public void applyTheme() {
        renderer.applyTheme();
    }

}
