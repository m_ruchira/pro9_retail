package com.isi.csvr;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWColumnSettings;
import com.isi.csvr.table.OtherSymbolEventModel;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Nov 20, 2008
 * Time: 3:35:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class OtherSymbolEventsFrame extends InternalFrame {

    ViewSetting oSetting;
    OtherSymbolEventModel oModel;
    private Table eventTable;
    private String sKey;


    public OtherSymbolEventsFrame(String sKey) {
        this.sKey = sKey;
        initUI();
    }

    public void initUI() {

        try {
            oSetting = ViewSettingsManager.getSummaryView("OTHER_SYMBOL_EVENTS");
            if (oSetting == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("OTHER_SYMBOL_EVENTS_COLS"));
        this.setSize(oSetting.getSize());
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setResizable(true);
        this.setIconifiable(true);
        this.setMaximizable(true);
        this.setClosable(true);
        String caption = oSetting.getCaption();
        if (Language.isLTR()) {
            caption = caption + SharedMethods.getSymbolFromKey(sKey);
        } else {
            caption = SharedMethods.getSymbolFromKey(sKey) + caption;
        }
        this.setTitle(caption);
        oModel = new OtherSymbolEventModel(sKey);
        oModel.setViewSettings(oSetting);
        eventTable = new Table();
        eventTable.setModel(oModel);
        oModel.setTable(eventTable);

        oModel.getTable().updateUI();
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(eventTable, BorderLayout.CENTER);
        this.add(panel);


    }
}
