// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

import com.isi.csvr.shared.*;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;

/**
 * A Swing-based dialog class to create a new custom view.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class CreateView extends
        JDialog implements KeyListener, ActionListener, WindowListener {
    JPanel jPanel1 = new JPanel();
    JLabel lblName = new JLabel();
    JTextField txtName = new JTextField();
    TWButton btnOk = new TWButton();
    TWButton btnCancel = new TWButton();
    String g_sNewName = null;
    String g_sInvalidChars = "~!#&*(),:;/\\";
    String[] g_asCaptions;
    TWComboBox cmbType;
    ArrayList<TWComboItem> typeList;
    int selectedType = 0;
    boolean showTypes = false;

    // Panel to hold buttons
    JPanel buttonPanel;

    //panel to hold text fields
    JPanel viewPanel;

//    public static final int TYPE_NORMAL = 0;
//    public static final int TYPE_FUTURES = 1;
//    public static final int TYPE_FOREX = 2;
//    public static final int TYPE_NORMAL_2 = 3;

    /**
     * Constructs a new instance.
     *
     * @param parent
     * @param title
     * @param modal
     */
    public CreateView(Frame parent, String title, boolean modal, boolean showTypes) {
        super(parent, title, modal);
        this.showTypes = showTypes;
        try {
            jbInit();
            pack();
            g_asCaptions = new String[Language.getLanguageIDs().length];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */
    public CreateView(String sTitle) {
        this(null, sTitle, true, false);
        //this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    /*
    * Show the dialog and accept the new name
    */
    public String getNewName() {
        Dimension dim = this.getParent().getSize();
        /*  if (showTypes) {
           this.setSize(280, 140);
       } else {
           this.setSize(280, 110);
       }
       this.setSize(320, 140);*/

        this.setResizable(false);
        this.setBounds((int) ((dim.getWidth() - this.getWidth()) / 2),
                ((int) (dim.getHeight() - this.getHeight()) / 2), this.getWidth(), this.getHeight());
        this.setVisible(true);
        if (g_sNewName == null)
            return null;
        if (g_sNewName.equals(""))
            return null;
        else
            return getCaptions(g_sNewName);

    }

    public String getTypedName() {
        return g_sNewName.trim();
    }

    public int getSelectedType() {
        return selectedType;
    }


    public void setCaptions(String sCaptions) {
        int i = 0;

        StringTokenizer oCaptions = new StringTokenizer(sCaptions, ",");

        try {
            while (oCaptions.hasMoreElements()) {
                g_asCaptions[i] = oCaptions.nextToken();
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        txtName.setText(Language.getLanguageSpecificString(sCaptions, ","));
    }

    private String getCaptions(String sCaption) {
        int iCount = 0; //Language.getLanguageCount();
        try {
            iCount = g_asCaptions.length;

            for (int i = 0; i < iCount; i++) {
                if ((g_asCaptions[i] == null) || (g_asCaptions[i].equals("")) || (!g_asCaptions[i].equals(sCaption)))
                    g_asCaptions[i] = sCaption;
            }

//            g_asCaptions[Language.getLanguageID()] = sCaption;
            g_asCaptions[Language.getLanguageID()] = sCaption;

            String sCaptions = "";
            for (int i = 0; i < iCount; i++) {
                sCaptions += ("," + g_asCaptions[i]);
            }

            return sCaptions.substring(1);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return sCaption;
        }
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        //this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(this);

        //  jPanel1.setLayout(new FlexNullLayout());
        this.getContentPane().setLayout(new BorderLayout());
        buttonPanel = new JPanel(new FlexGridLayout(new String[]{"90", "80", "10", "80", "10"}, new String[]{"20", "10"}, 0, 0));
        String[] width = new String[]{"10", "65", "15", "170", "10"};
        String[] heightShowType = new String[]{"10", "20", "5", "20", "10"};
        String[] height = new String[]{"10", "20", "10"};
        if (showTypes) {
            viewPanel = new JPanel(new FlexGridLayout(width, heightShowType, 0, 0));
        } else {
            viewPanel = new JPanel(new FlexGridLayout(width, height, 0, 0));
        }


        this.setResizable(true);
        lblName.setText(Language.getString("NAME"));
        lblName.setPreferredSize(new Dimension(65, 20));

//        lblName.setBounds(new Rectangle(9, 15, 68, 20));
//        txtName.setBounds(new Rectangle(91, 15, 175, 20));
        btnOk.setEnabled(false);
        btnOk.setText(Language.getString("OK"));
        btnOk.setPreferredSize(new Dimension(80, 20));
        btnOk.setDefaultCapable(true);

        JLabel label = new JLabel(Language.getString("SELECT_MYSTOCK_TYPE"));
        label.setPreferredSize(new Dimension(65, 20));


        typeList = new ArrayList<TWComboItem>();
        TWComboModel model = new TWComboModel(typeList);
        cmbType = new TWComboBox(model);
        cmbType.setPreferredSize(new Dimension(170, 20));

        populateTypeList();

        txtName.setPreferredSize(new Dimension(170, 20));


        /*
        if(showTypes){
            label.setBounds(9, 45, 68, 20);
            cmbType.setBounds(91,45,175,20);
            btnOk.setBounds(new Rectangle(91, 75, 82, 20));
            btnCancel.setBounds(new Rectangle(184, 75, 82, 20));
            this.setSize(320, 140);
        } else {
            btnOk.setBounds(new Rectangle(91, 45, 82, 20));
            btnCancel.setBounds(new Rectangle(184, 45, 82, 20));
            this.setSize(320, 110);
        }*/

        btnOk.setActionCommand("O");
        btnOk.addActionListener(this);
        /*btnOk.addKeyListener(new KeyAdapter()
        {
        	public void keyPressed(KeyEvent e)
            {
            	if (e.getKeyCode() == KeyEvent.VK_ENTER)
					btn_actionPerformed(e);
            }
        });*/

        /*btnOk.addActionListener(new java.awt.event.ActionListener()
        {

            public void actionPerformed(ActionEvent e)
            {
                btn_actionPerformed(e);
            }
        });*/
        btnCancel.setText(Language.getString("CANCEL"));
        btnCancel.setPreferredSize(new Dimension(80, 25));
//        btnCancel.setBounds(new Rectangle(225, 60, 82, 27));
        btnCancel.setActionCommand("C");
        btnCancel.addActionListener(this);
        /*btnCancel.addActionListener(new java.awt.event.ActionListener()
        {

            public void actionPerformed(ActionEvent e)
            {
                btn_actionPerformed(e);
            }
        });*/
        /*btnCancel.addKeyListener(new KeyAdapter()
        {
        	public void keyPressed(KeyEvent e)
            {
            	if (e.getKeyCode() == KeyEvent.VK_ENTER)
					btn_actionPerformed(e);
            }
        });*/
        // getContentPane().add(jPanel1);

        getContentPane().add(viewPanel, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        //add components to view panel

        viewPanel.add(new JLabel(""));
        viewPanel.add(new JLabel(""));
        viewPanel.add(new JLabel(""));
        viewPanel.add(new JLabel(""));
        viewPanel.add(new JLabel(""));

        viewPanel.add(new JLabel(""));
        viewPanel.add(lblName);
        viewPanel.add(new JLabel(""));
        viewPanel.add(txtName);
        viewPanel.add(new JLabel(""));


        if (showTypes) {
            viewPanel.add(new JLabel(""));
            viewPanel.add(new JLabel(""));
            viewPanel.add(new JLabel(""));
            viewPanel.add(new JLabel(""));
            viewPanel.add(new JLabel(""));

            viewPanel.add(new JLabel(""));
            viewPanel.add(label);
            viewPanel.add(new JLabel(""));
            viewPanel.add(cmbType);
            viewPanel.add(new JLabel(""));
        }

        viewPanel.add(new JLabel(""));
        viewPanel.add(new JLabel(""));
        viewPanel.add(new JLabel(""));
        viewPanel.add(new JLabel(""));
        viewPanel.add(new JLabel(""));
        //add components to button panel

        buttonPanel.add(new JLabel(""));


        buttonPanel.add(btnOk);
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(btnCancel);
        buttonPanel.add(new JLabel(""));

        buttonPanel.add(new JLabel(""));
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(new JLabel(""));
        GUISettings.applyOrientation(buttonPanel);

        txtName.addKeyListener(this);

        btnOk.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke
                (KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        btnCancel.registerKeyboardAction(this, "C", KeyStroke.getKeyStroke
                (KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

        GUISettings.applyOrientation(this);
//        GUISettings.applyOrientation();
    }

    private void populateTypeList() {
        try {
            TWComboItem defaultItem = null;
            typeList.clear();
            Enumeration<ColumnTemplate> elements = ColumnTemplateStore.getSharedInstance().getTemplates();
            ColumnTemplate template;
            while (elements.hasMoreElements()) {
                template = elements.nextElement();
                TWComboItem item = new TWComboItem(template.getId(), template.getDescription());
                typeList.add(item);
                if (template.getId().equals("0")) {
                    defaultItem = item;
                }
            }
//        typeList.add(new TWComboItem(TYPE_NORMAL,Language.getString("NORMAL_MODE")));
//        typeList.add(new TWComboItem(TYPE_FUTURES,Language.getString("FUTURES_MODE")));
//        typeList.add(new TWComboItem(TYPE_FOREX,Language.getString("FOREX_MODE")));
//        typeList.add(new TWComboItem(TYPE_NORMAL_2,Language.getString("NORMAL_MODE")));             
            cmbType.setSelectedItem(defaultItem);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * A button is clicked
     */
    void btn_actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("C")) {
            cancelRequest();
        } else if (e.getActionCommand().equals("O")) {
            // check if enter was pressed while on cancel pressed
            //if (SwingUtilities.findFocusOwner(this) == btnCancel){ depricated as of 1.4
            if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == btnCancel) {
                cancelRequest();
            } else {
                approveRequest();
            }
        }
    }

    public void clearCaptions() {
        int iCount = g_asCaptions.length; //Language.getLanguageCount();

        for (int i = 0; i < iCount; i++) {
            g_asCaptions[i] = "";
        }
    }

    private void cancelRequest() {
        g_sNewName = null;
        this.dispose();
    }

    private void approveRequest() {
        g_sNewName = txtName.getText().trim();
        if (isValidName()) {
            this.dispose();
        } else {
            new ShowMessage(false, Language.getString("MSG_INVALID_CHARS"), "E");
        }
        if (showTypes) {
            selectedType = Integer.parseInt(((TWComboItem) cmbType.getSelectedItem()).getId());
        }
    }

    private boolean isValidName() {
        int iRestrctedLen = g_sInvalidChars.length();
        int i = 0;

        while (i < iRestrctedLen) {
            if (g_sNewName.indexOf(g_sInvalidChars.substring(i, i + 1)) >= 0)
                return false;
            i++;
        }
        return true;
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        String sText = txtName.getText().trim();
        if (sText.length() > 20) {
            txtName.setText(sText.substring(0, 20));
            Toolkit.getDefaultToolkit().beep();
        }
        if (sText.length() > 0)
            btnOk.setEnabled(true);
        else
            btnOk.setEnabled(false);
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            approveRequest();
        }
    }

    public void actionPerformed(ActionEvent e) {
        btn_actionPerformed(e);
    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
        g_sNewName = null;
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
    }


}


