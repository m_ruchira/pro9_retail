package com.isi.csvr.wicalculator;

import com.isi.csvr.Client;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.portfolio.ValuationRecord;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 04-Apr-2007
 * Time: 15:30:13
 * To change this template use File | Settings | File Templates.
 */
public class WhatiFCalculatorPortfolioFrame extends InternalFrame implements Themeable, Runnable, KeyListener, DateSelectedListener, MouseListener, FocusListener, InternalFrameListener, ActionListener {
    public static final int MODE_SIMULATOR = 0;
    public static final int MODE_TRADING = 1;
    static WhatiFCalculatorPortfolioFrame self;
    PFStore pfstore;
    TradePortfolios tradeportfolios;
    private JPanel topPanel;
    private JPanel descriptionPanel;
    private JPanel upperPanel;
    private JPanel tabbedPanel;
    private JPanel NoToBuyTabPanel;
    private JPanel VWAPTabPanel;
    private TWTabbedPane calculationTabbedPane;
    private JLabel descriptionLabel;
    private JLabel symbolLabel;
    private JLabel vwapLabel;
    private JLabel holdingLabel;
    private JLabel currentPriceLabel;
    private JLabel expectedVWAP;
    private JLabel noToBuy;
    private JLabel expectedNoToBuy;
    private JLabel vwapCalculated;
    private TWTextField symbolField;
    private TWTextField vwapField;
    private TWTextField holdingField;
    private TWTextField currentPriceField;
    private TWTextField expectedVWAPField;
    private TWTextField noToBuyField;
    private TWTextField expectedNoToBuyField;
    private TWTextField vwapCalculatedField;
    private TWButton calculateBtnVWAP;
    private TWButton calculateBtnNoToBuy;
    private boolean isCurrentPriceUpdating = true;
    private Thread thread;
    private String sskey = null;
    private int mode = 0;

    /*public static WhatiFCalculatorPortfolioFrame getSharedInstannce(String skey, int mode) {
        if (self == null) {
            self = new WhatiFCalculatorPortfolioFrame(skey, mode);
            return self;
        *//*} else {
            self.dispose();
            self = new WhatiFCalculatorPortfolioFrame(skey, mode);
            return self;*//*
        }
        return self;
    }*/


    public WhatiFCalculatorPortfolioFrame(String skey, int mode) {
        super();
        this.sskey = skey;
        this.mode = mode;
        topPanel = new JPanel();
        topPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "100", "5", "145"}, 4, 4));
        topPanel.add(createDescriptionpanel());
        topPanel.add(createUpperPanel(skey));
        topPanel.add(new JSeparator());
        topPanel.add(createTabbedPanel());
        getContentPane().add(topPanel);
        this.setFont(new TWFont("Arial", Font.BOLD, 12));
        this.setTitle(Language.getString("FIX_AVERAGE_COST"));
        this.setSize(new Dimension(290, 300));
        this.setClosable(true);
        this.setIconifiable(true);
        this.setVisible(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        Client.getInstance().getDesktop().add(this);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        getValues(skey);
        thread = new Thread(this, "WhatiFCalculatorPortfolioFrame");
        thread.start();
        expectedVWAPField.addKeyListener(this);
        noToBuyField.addKeyListener(this);
        GUISettings.applyOrientation(this);

    }

    /*public void activate(){
        thread = new Thread(this, "WhatiFCalculatorPortfolioFrame");
        thread.start();
        setVisible(true);
    }*/

    private JPanel createDescriptionpanel() {
        descriptionPanel = new JPanel();
        descriptionPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 4, 4));
        descriptionLabel = new JLabel();
        descriptionLabel.setForeground(Color.BLACK);
        descriptionLabel.setFont(new TWFont("Arial", 1, 14));
        descriptionLabel.setHorizontalAlignment(SwingConstants.LEADING);
        descriptionPanel.add(descriptionLabel);

        return descriptionPanel;
    }

    private JPanel createUpperPanel(String skey) {
        symbolLabel = new JLabel(Language.getString("WHAT_IF_CAL_PORTFOLIO_SYMBOL"));
        vwapLabel = new JLabel(Language.getString("WHAT_IF_CAL_PORTFOLIO_VWAP"));
        holdingLabel = new JLabel(Language.getString("HOLDING"));
        currentPriceLabel = new JLabel(Language.getString("WHAT_IF_CAL_PORTFOLIO_CURRENT_PRICE"));
        symbolField = new TWTextField();
        //symbolField.setHorizontalAlignment(SwingConstants.RIGHT);
        symbolField.setEditable(false);
        vwapField = new TWTextField();
        vwapField.setHorizontalAlignment(SwingConstants.RIGHT);
        vwapField.setEditable(false);
        holdingField = new TWTextField();
        holdingField.setHorizontalAlignment(SwingConstants.RIGHT);
        holdingField.setEditable(false);
        currentPriceField = new TWTextField();
        currentPriceField.setHorizontalAlignment(SwingConstants.RIGHT);
        currentPriceField.setEditable(false);
        symbolField.setText(skey);
        upperPanel = new JPanel();
        upperPanel.setLayout(new FlexGridLayout(new String[]{"5", "120", "10", "120", "5"}, new String[]{"20", "20", "20", "20"}, 4, 4));
        upperPanel.add(new JLabel());
        upperPanel.add(symbolLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(symbolField);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(vwapLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(vwapField);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(holdingLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(holdingField);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(currentPriceLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(currentPriceField);
        upperPanel.add(new JLabel());
        return upperPanel;
    }

    private JPanel createTabbedPanel() {
        tabbedPanel = new JPanel();
        tabbedPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 4, 4));
        calculationTabbedPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "wi");
        calculationTabbedPane.addTab(Language.getString("WHAT_IF_CAL_PORTFOLIO_SHARES_TOBUY"), createNotoBuyCalculationTab());
        calculationTabbedPane.addTab(Language.getString("WHAT_IF_CAL_PORTFOLIO_EXP_VWAP"), createVWAPCalculationTab());
        calculationTabbedPane.showTab(0);
        tabbedPanel.add(calculationTabbedPane);
        return tabbedPanel;
    }

    private JPanel createNotoBuyCalculationTab() {
        expectedVWAP = new JLabel(Language.getString("WHAT_IF_CAL_PORTFOLIO_EXP_VWAP"));
        noToBuy = new JLabel(Language.getString("WHAT_IF_CAL_PORTFOLIO_SHARES_TOBUY"));
        calculateBtnNoToBuy = new TWButton(Language.getString("CALCULATE"));
        expectedVWAPField = new TWTextField();
        expectedVWAPField.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL, 20L, 4));
        expectedVWAPField.setHorizontalAlignment(SwingConstants.RIGHT);
        noToBuyField = new TWTextField();
        noToBuyField.setHorizontalAlignment(SwingConstants.RIGHT);
        noToBuyField.setEditable(false);
        NoToBuyTabPanel = new JPanel();
        NoToBuyTabPanel.setLayout(new FlexGridLayout(new String[]{"5", "110", "10", "110", "5"}, new String[]{"10", "20", "20", "10", "20"}, 4, 4));
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(expectedVWAP);
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(expectedVWAPField);
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(noToBuy);
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(noToBuyField);
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(new JLabel());
        NoToBuyTabPanel.add(calculateBtnNoToBuy);
        NoToBuyTabPanel.add(new JLabel());
        expectedVWAPField.addKeyListener(this);
        noToBuyField.addKeyListener(this);
        calculateBtnNoToBuy.addActionListener(this);

        return NoToBuyTabPanel;

    }

    private JPanel createVWAPCalculationTab() {
        expectedNoToBuy = new JLabel(Language.getString("WHAT_IF_CAL_PORTFOLIO_SHARES_TOBUY"));
        vwapCalculated = new JLabel(Language.getString("WHAT_IF_CAL_PORTFOLIO_EXP_VWAP"));
        calculateBtnVWAP = new TWButton(Language.getString("CALCULATE"));
        expectedNoToBuyField = new TWTextField();
        expectedNoToBuyField.setHorizontalAlignment(SwingConstants.RIGHT);
        expectedNoToBuyField.setDocument(new ValueFormatter(ValueFormatter.INTEGER));
        vwapCalculatedField = new TWTextField();
        vwapCalculatedField.setHorizontalAlignment(SwingConstants.RIGHT);
        vwapCalculatedField.setEditable(false);
        VWAPTabPanel = new JPanel();
        VWAPTabPanel.setLayout(new FlexGridLayout(new String[]{"5", "110", "10", "110", "5"}, new String[]{"10", "20", "20", "10", "20"}, 4, 4));
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(expectedNoToBuy);
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(expectedNoToBuyField);
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(vwapCalculated);
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(vwapCalculatedField);
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(new JLabel());
        VWAPTabPanel.add(calculateBtnVWAP);
        VWAPTabPanel.add(new JLabel());
        calculateBtnVWAP.addActionListener(this);

        return VWAPTabPanel;
    }

    private void getValues(String skey) {
        try {
            if (mode == MODE_SIMULATOR) {
                pfstore = PFStore.getInstance();
                ValuationRecord vrecord = (ValuationRecord) pfstore.getValuationRecord(skey);
                holdingField.setText("" + vrecord.getHolding());
                vwapField.setText(SharedMethods.getDecimalFormatNoComma(SharedMethods.getExchangeFromKey(skey), SharedMethods.getSymbolFromKey(skey), SharedMethods.getInstrumentTypeFromKey(skey)).format(vrecord.getAvgCost()));
                double current_price = DataStore.getSharedInstance().getStockObject(skey).getLastTradeValue();
                if (current_price == 0) {
                    current_price = DataStore.getSharedInstance().getStockObject(skey).getPreviousClosed();
                }
                currentPriceField.setText(SharedMethods.getDecimalFormatNoComma(SharedMethods.getExchangeFromKey(skey), SharedMethods.getSymbolFromKey(skey), SharedMethods.getInstrumentTypeFromKey(skey)).format(current_price));
                descriptionLabel.setText(DataStore.getSharedInstance().getStockObject(skey).getLongDescription());
            } else if (mode == MODE_TRADING) {
                tradeportfolios = TradePortfolios.getInstance();
                com.isi.csvr.trading.portfolio.ValuationRecord vrecord = (com.isi.csvr.trading.portfolio.ValuationRecord) tradeportfolios.getValuationRecord(skey);
                holdingField.setText("" + vrecord.getOwned(Meta.INSTRUMENT_QUOTE));
                vwapField.setText(SharedMethods.getDecimalFormatNoComma(SharedMethods.getExchangeFromKey(skey), SharedMethods.getSymbolFromKey(skey), SharedMethods.getInstrumentTypeFromKey(skey)).format(vrecord.getAvgCost()));
                double current_price = DataStore.getSharedInstance().getStockObject(skey).getLastTradeValue();
                if (current_price == 0) {
                    current_price = DataStore.getSharedInstance().getStockObject(skey).getPreviousClosed();
                }
                currentPriceField.setText(SharedMethods.getDecimalFormatNoComma(SharedMethods.getExchangeFromKey(skey), SharedMethods.getSymbolFromKey(skey), SharedMethods.getInstrumentTypeFromKey(skey)).format(current_price));
                descriptionLabel.setText(DataStore.getSharedInstance().getStockObject(skey).getLongDescription());
            }
        } catch (Exception e) {
            System.out.println("Getting values failed......");
            e.printStackTrace();
        }
    }

    private void calculateNoToBuy() {
        double expectedVWAP = 0;
        try {
            expectedVWAP = Double.parseDouble(expectedVWAPField.getText());
        } catch (NumberFormatException e) {
            System.out.println("Invalid input....");
        }
        int holdings = Integer.parseInt(holdingField.getText());
        double vwap = Double.parseDouble(vwapField.getText());
        double currentPrice = Double.parseDouble(currentPriceField.getText());
        double NoToBuy = ((expectedVWAP * holdings) - (holdings * vwap)) / (currentPrice - expectedVWAP);
        if (currentPrice == expectedVWAP) {
            noToBuyField.setText(Language.getString("WHAT_IF_CAL_PORTFOLIO_ERROR"));

        } else if (NoToBuy < 0) {
            if (NoToBuy * (-1) >= holdings) {
                noToBuyField.setText(Language.getString("WHAT_IF_CAL_PORTFOLIO_ERROR"));

            } else {
                noToBuyField.setText(SharedMethods.getDecimalFormatNoComma(SharedMethods.getExchangeFromKey(sskey), SharedMethods.getSymbolFromKey(sskey), SharedMethods.getInstrumentTypeFromKey(sskey)).format(Math.ceil(NoToBuy)));

            }

        } else {
            noToBuyField.setText(SharedMethods.getDecimalFormatNoComma(SharedMethods.getExchangeFromKey(sskey), SharedMethods.getSymbolFromKey(sskey), SharedMethods.getInstrumentTypeFromKey(sskey)).format(Math.ceil(NoToBuy)));
        }

    }

    private void calculateVWAP() {
        double noToBuy = 0;
        try {
            noToBuy = Double.parseDouble(expectedNoToBuyField.getText());
        } catch (NumberFormatException e) {
            System.out.println("invalid input.......");
        }
        int holdings = Integer.parseInt(holdingField.getText());
        double vwap = Double.parseDouble(vwapField.getText());
        double currentPrice = Double.parseDouble(currentPriceField.getText());
        double calculatedVWAP = ((holdings * vwap) + (currentPrice * noToBuy)) / (holdings + noToBuy);
//       System.out.println("Calculated VWAP : "+calculatedVWAP);
        vwapCalculatedField.setText(SharedMethods.getCurrencyDecimalFormat(SharedMethods.getExchangeFromKey(sskey), SharedMethods.getSymbolFromKey(sskey), SharedMethods.getInstrumentTypeFromKey(sskey)).format(calculatedVWAP));
        //isCalculatingVWAP=false;

    }

    public void run() {
        while (isCurrentPriceUpdating) {
            try {
                double current_price = DataStore.getSharedInstance().getStockObject(sskey).getLastTradeValue();
                if (current_price == 0) {
                    current_price = DataStore.getSharedInstance().getStockObject(sskey).getPreviousClosed();
                }
                currentPriceField.setText(SharedMethods.getDecimalFormatNoComma(SharedMethods.getExchangeFromKey(sskey), SharedMethods.getSymbolFromKey(sskey), SharedMethods.getInstrumentTypeFromKey(sskey)).format(current_price));
//                System.out.println("Current price updated......");
                Thread.sleep(1000);

            } catch (Exception e) {
                System.out.println("Error Occured while Current price updating ");
                e.printStackTrace();
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }

        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == calculateBtnNoToBuy) {
//            System.out.println("Calculate btn....tobuy");
            if ((expectedVWAPField.getText() != null) || (!(expectedVWAPField.getText().equals("")))) {
                calculateNoToBuy();
            }

        } else if (e.getSource() == calculateBtnVWAP) {
//            System.out.println("Calculate btn ....vwap");
            if ((expectedNoToBuyField.getText() != null) && (!(expectedNoToBuyField.getText().equals("")))) {
                calculateVWAP();
            }

        }

    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource() == expectedVWAPField) {

        }

    }

    public void keyPressed(KeyEvent e) {
        if (e.getSource() == expectedVWAPField) {

        }
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusGained(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusLost(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        //  thread.destroy();
        isCurrentPriceUpdating = false;
        this.dispose();
        self = null;

    }
}
