package com.isi.csvr.wicalculator;

import com.isi.csvr.Client;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.*;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created Chandika Hewage
 * User: admin
 * Date: 27-Mar-2007
 * Time: 16:55:09
 * To change this template use File | Settings | File Templates.
 */
public class WhatifCalculatorFrame extends InternalFrame implements Themeable, Runnable, ActionListener,
        KeyListener, DateSelectedListener, MouseListener, FocusListener, InternalFrameListener {
    static WhatifCalculatorFrame self;
    public Thread t;
    public boolean marketPrice_update = true;
    private JPanel topPanel;
    private JPanel upperPanel;
    private JPanel middlePanel;
    private JPanel lowerPanel;
    private JPanel description_panel;
    private JLabel symbol_lbl;
    private JLabel date_lbl;
    private JLabel openVal_lbl;
    private JLabel todaysval_lbl;
    private JLabel qty_lbl;
    private JLabel loss_profit_lbl;
    private JLabel lossProfit_val_lbl;
    private JLabel lossProfit_currency_lbl;
    private JLabel description_lbl;
    private TWButton calc_btn;
    private JPanel datePicker;
    private JLabel datePicker_lbl;
    private TWButton btnShowDays;
    private DatePicker date_Picker;
    private TWTextField txtSymbols;
    private TWButton symbol_picker;
    private TWTextField currentValue;
    private TWTextField qty_fdl;
    private TWTextField open_value;
    private String exchange;                  // = "";
    private String skey = "";
    private String ssymbol;                   // = "";
    private int instrument = -1;                   // = "";
    private boolean isSymbolSelected;          //= false;
    private boolean isDateSelected;            //= false;
    private Date updateDate;
    private boolean keyTyped;

    public WhatifCalculatorFrame() {
        super();
        isSymbolSelected = false;
        isDateSelected = false;
        topPanel = new JPanel();
        topPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "60", "5", "110"}, 4, 4));
        topPanel.add(createDescriptionpanel());
        topPanel.add(createUpperpanel());
        topPanel.add(new JSeparator());
        topPanel.add(createMiddlePanel());
        getContentPane().add(topPanel);
        this.setFont(new TWFont("Arial", Font.BOLD, 12));
        this.setTitle(Language.getString("WHAT_IF_CALC"));
        this.setSize(new Dimension(290, 245));
        this.setClosable(true);
        this.setIconifiable(true);
        Client.getInstance().getDesktop().add(this);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        qty_fdl.addKeyListener(this);
        self = this;
        GUISettings.applyOrientation(this);
    }

    public static WhatifCalculatorFrame getSharedInstannce() {
        if (self == null) {
            self = new WhatifCalculatorFrame();
        }
        return self;
    }

    public JPanel createDescriptionpanel() {
        description_panel = new JPanel();
        description_panel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 4, 4));
        description_lbl = new JLabel();
//        description_lbl.setForeground(Color.BLACK);
        description_lbl.setFont(new TWFont("Arial", 1, 14));
        description_lbl.setHorizontalAlignment(SwingConstants.LEADING);
        description_panel.add(description_lbl);

        return description_panel;
    }

    public JPanel createUpperpanel() {
        symbol_lbl = new JLabel(Language.getString("WHAT_IF_CALC_SYMBOL"));
        symbol_lbl.setFont(new TWFont("Arial", Font.BOLD, 12));
        todaysval_lbl = new JLabel(Language.getString("WHAT_IF_CALC_CVALUE"));
        todaysval_lbl.setFont(new TWFont("Arial", Font.BOLD, 12));
        currentValue = new TWTextField();
        currentValue.setEditable(false);
        currentValue.setHorizontalAlignment(SwingConstants.RIGHT);
        upperPanel = new JPanel();
//        upperPanel.setLayout(new FlexGridLayout(new String[]{"5", "120", "10", "120", "5"}, new String[]{"20", "20"}, 4, 4));
        upperPanel.setLayout(new FlexGridLayout(new String[]{"5", "100", "10", "140", "5"}, new String[]{"20", "20"}, 4, 4));
        upperPanel.add(new JLabel());
        upperPanel.add(symbol_lbl);
        upperPanel.add(new JLabel());
        upperPanel.add(createSymbolPanel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(todaysval_lbl);
        upperPanel.add(new JLabel());
        upperPanel.add(currentValue);
        upperPanel.add(new JLabel());

        return upperPanel;
    }

    public JPanel createMiddlePanel() {
        openVal_lbl = new JLabel(Language.getString("WHAT_IF_CALC_OVALUE"));
        openVal_lbl.setFont(new TWFont("Arial", Font.BOLD, 12));
        date_lbl = new JLabel(Language.getString("WHAT_IF_CALC_DATE"));
        date_lbl.setFont(new TWFont("Arial", Font.BOLD, 12));
        qty_lbl = new JLabel(Language.getString("WHAT_IF_CALC_QTY"));
        qty_lbl.setFont(new TWFont("Arial", Font.BOLD, 12));
        calc_btn = new TWButton(Language.getString("CALCULATE"));
        loss_profit_lbl = new JLabel(Language.getString("WHAT_IF_CALC_PROLOS"));
        loss_profit_lbl.setFont(new TWFont("Arial", Font.BOLD, 12));
        lossProfit_val_lbl = new JLabel();
        lossProfit_currency_lbl = new JLabel();
        JPanel lossProfitPnl = new JPanel(new FlexGridLayout(new String[]{"100%", "35"}, new String[]{"18"}, 0, 0));
        lossProfitPnl.add(lossProfit_val_lbl);
        lossProfitPnl.add(lossProfit_currency_lbl);
        GUISettings.applyOrientation(lossProfitPnl);
        lossProfit_currency_lbl.setHorizontalAlignment(SwingConstants.LEFT);
        lossProfit_val_lbl.setHorizontalAlignment(SwingConstants.LEFT);
        lossProfitPnl.setBorder(UIManager.getBorder("TextField.border"));
        qty_fdl = new TWTextField();
        qty_fdl.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        qty_fdl.setHorizontalAlignment(SwingConstants.RIGHT);
        open_value = new TWTextField();
        open_value.setEditable(false);
        open_value.setHorizontalAlignment(SwingConstants.RIGHT);
        middlePanel = new JPanel();
//        middlePanel.setLayout(new FlexGridLayout(new String[]{"5", "120", "10", "120", "5"}, new String[]{"20", "20", "20", "20"}, 4, 4));
        middlePanel.setLayout(new FlexGridLayout(new String[]{"5", "100", "10", "140", "5"}, new String[]{"20", "20", "20", "20"}, 4, 4));

        middlePanel.add(new JLabel());
        middlePanel.add(date_lbl);
        middlePanel.add(new JLabel());
        middlePanel.add(getDateCombo());
        middlePanel.add(new JLabel());
        middlePanel.add(new JLabel());
        middlePanel.add(openVal_lbl);
        middlePanel.add(new JLabel());
        middlePanel.add(open_value);
        middlePanel.add(new JLabel());
        middlePanel.add(new JLabel());
        middlePanel.add(qty_lbl);
        middlePanel.add(new JLabel());
        middlePanel.add(qty_fdl);
        middlePanel.add(new JLabel());
        loss_profit_lbl.setOpaque(true);
        loss_profit_lbl.setForeground(Color.BLUE);
        lossProfit_val_lbl.setOpaque(true);
        lossProfit_val_lbl.setForeground(Color.white);
        lossProfit_val_lbl.setHorizontalAlignment(SwingConstants.TRAILING);
        lossProfit_val_lbl.setFont(new TWFont("Arial", 1, 14));
        middlePanel.add(new JLabel());
        middlePanel.add(loss_profit_lbl);
        middlePanel.add(new JLabel());
        middlePanel.add(lossProfitPnl);
        middlePanel.add(new JLabel());
        GUISettings.applyOrientation(middlePanel);
        return middlePanel;
    }

    private JPanel getDateCombo() {
//        datePicker = new JPanel(new FlexGridLayout(new String[]{"104", "100%"}, new String[]{"100%"}, 0, 0));
        datePicker = new JPanel(new FlexGridLayout(new String[]{"115", "100%"}, new String[]{"100%"}, 0, 0));
        datePicker.setBorder(BorderFactory.createEtchedBorder());
        datePicker_lbl = new JLabel();
        datePicker_lbl.setOpaque(false);
        datePicker.add(datePicker_lbl);
        btnShowDays = new TWButton(new DownArrow());
        btnShowDays.setEnabled(false);
        btnShowDays.setBorder(null);
        btnShowDays.addActionListener(this);
        btnShowDays.setContentAreaFilled(false);
        btnShowDays.setOpaque(false);
        datePicker.add(btnShowDays);
        datePicker.addMouseListener(this);

        return datePicker;
    }

    private JPanel createSymbolPanel() {
        String[] widths = {"100%", "20"};
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights);
        JPanel panel = new JPanel(layout);
        txtSymbols = new TWTextField();
        txtSymbols.addFocusListener(this);
        txtSymbols.addKeyListener(this);
        panel.add(txtSymbols);
        symbol_picker = new TWButton("...");
        symbol_picker.addActionListener(this);
        symbol_picker.setActionCommand("ARROW");
        panel.add(symbol_picker);

        return panel;
    }

    public void createLowerPanel() {

    }

    private void searchSymbol() {
        try {
            Symbols symbols = new Symbols();
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();
            oCompanies.setTitle(Language.getString("WHAT_IF_CALC"));
            oCompanies.setSingleMode(true);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(true);
            oCompanies.showDialog(true);
            oCompanies = null;

            if (symbols.getSymbols()[0] != null) {
                String key = symbols.getSymbols()[0];
                this.skey = symbols.getSymbols()[0];
                exchange = SharedMethods.getExchangeFromKey(key);
                ssymbol = SharedMethods.getSymbolFromKey(key);
                instrument = SharedMethods.getInstrumentTypeFromKey(key);
                lossProfit_currency_lbl.setText(DataStore.getSharedInstance().getCurrencyCode(skey));
                String symbol = SharedMethods.getSymbolFromKey(key);
                int instrument = SharedMethods.getInstrumentTypeFromKey(key);
                double current_price = DataStore.getSharedInstance().getStockObject(key).getLastTradeValue();
                if (current_price == 0) {
                    current_price = DataStore.getSharedInstance().getStockObject(key).getPreviousClosed();
                }
                txtSymbols.setText(symbol);
                currentValue.setText(SharedMethods.getDecimalFormatNoComma(exchange, symbol, instrument).format(current_price));
                description_lbl.setText(DataStore.getSharedInstance().getStockObject(key).getLongDescription());
                btnShowDays.setEnabled(true);
                isSymbolSelected = true;
            }
            if (!datePicker_lbl.getText().equals("")) {
                getOpenValue(updateDate);
            }
            if (updateChecker()) {
                lossProfit_val_lbl.setText(SharedMethods.getCurrencyDecimalFormat(exchange, ssymbol, instrument).format(autoCalculateLoss_Profit()));
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource() == btnShowDays) {
//            if (date_Picker == null) {
            date_Picker = null;
            date_Picker = new DatePicker(Client.getInstance().getFrame(), true);
//            }
            date_Picker.getCalendar().addDateSelectedListener(this);
            Point location = new Point(datePicker_lbl.getX(), (datePicker_lbl.getY()) + datePicker_lbl.getBounds().height);
            SwingUtilities.convertPointToScreen(location, datePicker_lbl.getParent());
            date_Picker.setLocation(location);
            date_Picker.setVisible(true);
        } else if (e.getActionCommand().equals("ARROW")) {
            searchSymbol();
        }
    }

    public void getOpenValue(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

        if (!skey.equals("")) {
            boolean historyAvailable = true;
            String exchange = SharedMethods.getExchangeFromKey(skey);
            Exchange exch = ExchangeStore.getSharedInstance().getExchange(exchange);

            if (exchange.equals("KSE")) {
                Stock stock = DataStore.getSharedInstance().getStockObject(skey);
                String marketID = stock.getMarketID();

                if (!(marketID.equals("R") || marketID.equals("O"))) {
                    historyAvailable = false;
                }

            }

            if (historyAvailable) {
                open_value.setText("" + PortfolioInterface.getMarketPriceForDay(skey, format.format(date)));
                if (open_value.getText().equals("0.0")) {
                    qty_fdl.setText("");
                    lossProfit_val_lbl.setText("");
                }
            } else {
                new ShowMessage(Language.getString("WHAT_IF_CAL_MSG_NO_HISTORY"), "I");
            }

        } else {
            datePicker_lbl.setText("");
            new ShowMessage(Language.getString("MSG_SYMBOL_CANNOT_BE_BLANK"), "I");
        }
    }

    public double autoCalculateLoss_Profit() {
        double current_val = Double.parseDouble(currentValue.getText());
        double open_val = Double.parseDouble(open_value.getText());
        double quantity = 0;
        double loss_profit = 0;
        float priceFactor = 1;

        try {
            priceFactor = ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            quantity = Double.parseDouble(qty_fdl.getText());
            loss_profit = (current_val - open_val) * quantity;
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "Whatif");
        } catch (NumberFormatException e) {
            quantity = 0;
            loss_profit = 0;
        }
        lossProfit_val_lbl.setFont(new TWFont("Arial", 1, 14));
        if (loss_profit >= 0) {
            lossProfit_val_lbl.setForeground(Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
        } else {
            lossProfit_val_lbl.setForeground(Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
        }

//        System.out.println("loss profit=="+loss_profit);
        return loss_profit / priceFactor;
    }

    private boolean doInternalValidation(String symbol) {
        symbol = symbol.toUpperCase();
        this.skey = SymbolMaster.getExchangeForSymbol(symbol, false);
        if (skey != null) {
            this.exchange = SharedMethods.getExchangeFromKey(skey);
            this.ssymbol = SharedMethods.getSymbolFromKey(skey);
            instrument = SharedMethods.getInstrumentTypeFromKey(skey);
            lossProfit_currency_lbl.setText(DataStore.getSharedInstance().getCurrencyCode(skey));
            if (this.ssymbol != null) {
                txtSymbols.setText(this.ssymbol);
                description_lbl.setText(DataStore.getSharedInstance().getStockObject(skey).getLongDescription());
                isSymbolSelected = true;
                if (!datePicker_lbl.getText().equals("")) {
                    getOpenValue(updateDate);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private void invalidateStock() {
        /*ssymbol = null;
        exchange = null;
        instrument = -1;*/
    }

    private void validateSymbol(String symbol) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            String requestID = "WhatifCalculatorFrame" + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol.toUpperCase(), requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
//            System.out.println("Validate request sent");
        }
    }

    public void setInvalidSymbol() {
        skey = "";
        currentValue.setText("");
        open_value.setText("");
        qty_fdl.setText("");
        lossProfit_val_lbl.setText("");
        description_lbl.setText("");
    }

    public void reset() {
        skey = "";
        currentValue.setText("");
        open_value.setText("");
        qty_fdl.setText("");
        lossProfit_val_lbl.setText("");
        datePicker_lbl.setText("");
        txtSymbols.setText("");
        description_lbl.setText("");

    }

    public void setCurrentPrice(String key1) {
        this.skey = key1;
        this.exchange = SharedMethods.getExchangeFromKey(key1);
        String symbol = SharedMethods.getSymbolFromKey(key1);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key1);
        Stock stock = DataStore.getSharedInstance().getStockObject(key1);
        double current_price = stock.getLastTradeValue();
        if (current_price == 0) {
            current_price = stock.getPreviousClosed();
        }
        txtSymbols.setText(symbol);
        currentValue.setText(SharedMethods.getDecimalFormatNoComma(exchange, symbol, instrument).format(current_price));
        description_lbl.setText(stock.getLongDescription());
        isSymbolSelected = true;
        btnShowDays.setEnabled(true);
        if (!datePicker_lbl.getText().equals("")) {
            getOpenValue(updateDate);
        }
        lossProfit_currency_lbl.setText(DataStore.getSharedInstance().getCurrencyCode(skey));
        if (updateChecker()) {
            lossProfit_val_lbl.setText(SharedMethods.getCurrencyDecimalFormat(exchange, ssymbol, instrument).format(autoCalculateLoss_Profit()));
        }
    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource() == qty_fdl) {
            updateProfitLoss();
        }
    }

    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_ENTER) {
            e.consume();
            if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
                if (keyTyped) {
                    boolean validated = doInternalValidation(txtSymbols.getText());
                    if (!validated) {
                    }
                }
            } else {
                if (keyTyped) {
                    invalidateStock();
                    validateSymbol(txtSymbols.getText());
                }
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getSource() == txtSymbols) {
            invalidateStock();
            if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
                doInternalValidation(txtSymbols.getText());
            } else {
                keyTyped = true;
            }
        }
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        date_Picker.hide();
        Calendar cal = Calendar.getInstance();
        cal.set(iYear, iMonth, iDay);
        String dateStr = TradingShared.formatGoodTill(cal.getTime());
        datePicker_lbl.setText(dateStr);
        isDateSelected = true;
        updateDate = cal.getTime();
        getOpenValue(cal.getTime());
        if (updateChecker()) {
            lossProfit_val_lbl.setText(SharedMethods.getCurrencyDecimalFormat(exchange, ssymbol, instrument).format(autoCalculateLoss_Profit()));
        }
        dateStr = null;
        cal = null;

    }

    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
    }

    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
    }

    public void focusGained(FocusEvent e) {

    }

    public void focusLost(FocusEvent e) {
        if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
            if (keyTyped) {
                boolean validated = doInternalValidation(txtSymbols.getText());
                if (!validated) {
                    currentValue.setText("");
                    open_value.setText("");
                }
            }
            btnShowDays.setEnabled(false);
        } else {
            if (keyTyped) {
                invalidateStock();
                validateSymbol(txtSymbols.getText());
            }
            btnShowDays.setEnabled(false);
        }
    }

    public boolean updateChecker() {
        if (!qty_fdl.getText().equals("")) {
            if ((isSymbolSelected) && (isDateSelected)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void updateProfitLoss() {
        try {
            if (isSymbolSelected) {
                if (isDateSelected) {
                    lossProfit_val_lbl.setText(SharedMethods.getCurrencyDecimalFormat(exchange, ssymbol, instrument).format(autoCalculateLoss_Profit()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void showWindow() {
        marketPrice_update = true;
        t = new Thread(this, "What_If Calculator");
        t.setPriority(Thread.NORM_PRIORITY - 2);
        t.start();
        reset();
        setVisible(true);
    }

    public void run() {
        while (marketPrice_update) {
            try {
                if (skey == null || skey.equals("")) {

                } else {
                    double current_price = DataStore.getSharedInstance().getStockObject(skey).getLastTradeValue();
                    if (current_price == 0) {
                        current_price = DataStore.getSharedInstance().getStockObject(skey).getPreviousClosed();
                    }
                    currentValue.setText(SharedMethods.getDecimalFormatNoComma(exchange, SharedMethods.getSymbolFromKey(skey), SharedMethods.getInstrumentTypeFromKey(skey)).format(current_price));
                    btnShowDays.setEnabled(true);
                    //btnShowDays.setEnabled(true);
                    if (updateChecker()) {
                        lossProfit_val_lbl.setText(SharedMethods.getCurrencyDecimalFormat(exchange, ssymbol, instrument).format(autoCalculateLoss_Profit()));
                    }
                }
            } catch (Exception e) {
                System.out.println("Error Occured while updating ");
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }

        }
//        System.out.println("Thread exit");
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        marketPrice_update = false;
        setVisible(false);
        //this.dispose();
        //self = null;
    }
}
