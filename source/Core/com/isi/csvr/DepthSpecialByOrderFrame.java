package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.marketdepth.DepthObject;
import com.isi.csvr.marketdepth.DepthSpecialOrderModel;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.MarketAskDepthRenderer;
import com.isi.csvr.table.MarketBidDepthRenderer;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 29, 2008
 * Time: 4:06:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class DepthSpecialByOrderFrame extends InternalFrame implements LinkedWindowListener {

    boolean loadedFromWorkspace = false;
    byte stockDecimalPlaces = 2;
    byte type;
    private Table tableBid;
    private Table tableAsk;
    private ViewSetting oBidViewSetting = null;
    private ViewSetting oAskViewSetting = null;
    private DepthSpecialOrderModel askModel;
    private DepthSpecialOrderModel bidModel;
    private String selectedKey;
    private ViewSettingsManager g_oViewSettings;
    private long timeStamp;
    private boolean isLinked = false;

    public DepthSpecialByOrderFrame(String sThreadID, WindowDaemon oDaemon, Table oTable, Table[] oTable2, String selectedKey, boolean loadedFromWorkspace, boolean linked, String linkgroup) {
        super(sThreadID, oDaemon, oTable, oTable2);
        this.tableBid = oTable;
        this.tableAsk = oTable2[0];
        this.selectedKey = selectedKey;
        this.loadedFromWorkspace = loadedFromWorkspace;
        //this.type = type;
        isLinked = linked;
        setLinkedGroupID(linkgroup);
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        initUI();
    }

    public void initUI() {
        tableBid.setAutoResize(true);
        tableBid.getPopup().showLinkMenus();
        tableBid.getPopup().showSetDefaultMenu();
        tableBid.setWindowType(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW);

        bidModel = new DepthSpecialOrderModel(selectedKey, DepthObject.BID);

        if (oBidViewSetting == null && loadedFromWorkspace && isLinked) {
            ViewSetting oSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                oBidViewSetting = oSetting;
            }
        }
        if (oBidViewSetting == null) {
            oBidViewSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW +
                    "|" + selectedKey);
        }
        if (oBidViewSetting == null) {
            oBidViewSetting = g_oViewSettings.getSymbolView("DEPTH_SPECIAL_BID_VIEW");
            oBidViewSetting = oBidViewSetting.getObject();
            GUISettings.setColumnSettings(oBidViewSetting, TWColumnSettings.getItem("DEPTH_SPECIAL_COLS"));
            //todo added by Dilum
            SharedMethods.applyCustomViewSetting(oBidViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oBidViewSetting.getType(), oBidViewSetting);
            timeStamp = System.currentTimeMillis();
            oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
            oBidViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            oBidViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
            //oBidViewSetting.setID(selectedKey);
            oBidViewSetting.setID(timeStamp + "");
            g_oViewSettings.putDepthView(ViewSettingsManager.DEPTH_BID_VIEW +
                    "|" + selectedKey, oBidViewSetting);
        } else {
            loadedFromWorkspace = true;
        }

        bidModel.setViewSettings(oBidViewSetting);
        tableBid.setModel(bidModel);
        bidModel.setTable(tableBid, new MarketBidDepthRenderer(), true);  // Add the renderer...
        bidModel.applyColumnSettings();

            /* Create the Ask Market Depth Table */

        tableAsk.setAutoResize(true);
        tableAsk.getPopup().showLinkMenus();
        tableAsk.getPopup().showSetDefaultMenu();
        tableAsk.setWindowType(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW);

        askModel = new DepthSpecialOrderModel(selectedKey, DepthObject.ASK);

        if (oAskViewSetting == null && loadedFromWorkspace && isLinked) {
            ViewSetting oSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                oAskViewSetting = oSetting;
            }
        }
        if (oAskViewSetting == null) {
            oAskViewSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW +
                    "|" + selectedKey);
        }
        if (oAskViewSetting == null) {
            oAskViewSetting = g_oViewSettings.getSymbolView("DEPTH_SPECIAL_ASK_VIEW");
            oAskViewSetting = oAskViewSetting.getObject();
            // oAskViewSetting.setID(selectedKey);
            // timeStamp = System.currentTimeMillis();
            GUISettings.setColumnSettings(oAskViewSetting, TWColumnSettings.getItem("DEPTH_SPECIAL_COLS"));
            //todo added by Dilum
            SharedMethods.applyCustomViewSetting(oAskViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oAskViewSetting.getType(), oAskViewSetting);
            oAskViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
            oAskViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            oAskViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
            //oBidViewSetting.setID(selectedKey);
            oAskViewSetting.setID(timeStamp + "");
            g_oViewSettings.putDepthView(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW +
                    "|" + selectedKey, oAskViewSetting);
        }
        askModel.setViewSettings(oAskViewSetting);
        tableAsk.setModel(askModel);
        askModel.setTable(tableAsk, new MarketAskDepthRenderer(), false);  // Add the renderer...
        askModel.applyColumnSettings();

//            InternalFrame frame = new InternalFrame(Constants.NULL_STRING, g_oWindowDaemon, tableBid, tableAsk);

        oAskViewSetting.setParent(this);
        oAskViewSetting.setTableNumber(2);
        oBidViewSetting.setParent(this);
        oBidViewSetting.setTableNumber(1);
        this.setShowServicesMenu(true);
        this.getContentPane().setLayout(new BorderLayout());
        this.setSize(oBidViewSetting.getSize());
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setLinkGroupsEnabled(true);
        Client.getInstance().oTopDesktop.add(this);
        this.setTitle(Language.getString("SPECIAL_ORDER_BOOK") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getStockObject(selectedKey).getLongDescription());
        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);

        JSplitPane splitPane;
        if (Language.isLTR()) {
            this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, tableBid, tableAsk) {
                public int getDividerSize() {
                    return 4;
                }
            };
            splitPane.setOneTouchExpandable(true);
            splitPane.setDividerLocation((int) (oBidViewSetting.getSize().getWidth() / 2));
            this.getContentPane().add(BorderLayout.CENTER, splitPane);
            splitPane.updateUI();
        } else {
            this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, tableAsk, tableBid) {
                public int getDividerSize() {
                    return 4;
                }
            };
            splitPane.setOneTouchExpandable(true);
            splitPane.setDividerLocation((int) (oBidViewSetting.getSize().getWidth() / 2));
            this.getContentPane().add(BorderLayout.CENTER, splitPane);
            splitPane.updateUI();

        }
        GUISettings.applyOrientation(this);
        tableBid.updateGUI();
        tableAsk.updateGUI();
        this.applySettings();
        this.setSplitPane(splitPane);
        splitPane.updateUI();
        bidModel.adjustRows();
        askModel.adjustRows();
        setDecimalPlaces();
        this.setLayer(GUISettings.TOP_LAYER);

        if (loadedFromWorkspace && isLinked) {
            g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" +
                    LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" +
                    selectedKey, this);
        }
        String requestID = Meta.SPECIAL_DEPTH + "|" + selectedKey;
        Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.SPECIAL_DEPTH);
        this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
        if (!loadedFromWorkspace) {
//                frame.setLocationRelativeTo(oTopDesktop);
//                oBidViewSetting.setLocation(frame.getLocation());
            if (!oBidViewSetting.isLocationValid()) {
                this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                oBidViewSetting.setLocation(this.getLocation());
            } else {
                this.setLocation(oBidViewSetting.getLocation());
            }
        }
        this.show();


        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }


    }

    public void setDecimalPlaces() {
        try {
            stockDecimalPlaces = SharedMethods.getDecimalPlaces(selectedKey);
        } catch (Exception e) {
            stockDecimalPlaces = 2;
        }
        tableBid.getModel().setDecimalCount(stockDecimalPlaces);
        tableBid.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);
        tableAsk.getModel().setDecimalCount(stockDecimalPlaces);
        tableAsk.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);
    }

    public void symbolChanged(String sKey) {
        selectedKey = sKey;
        String oldKey = oBidViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oldKey))) {
            DataStore.getSharedInstance().removeSymbolRequest(oldKey);

        }
        String oldReq = getDataRequestID();
        RequestManager.getSharedInstance().removeRequest(oldReq);
        String requestID = Meta.SPECIAL_DEPTH + "|" + selectedKey;
        Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.SPECIAL_DEPTH);
        this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }
        this.setTitle(Language.getString("MARKET_DEPTH_BY_ORDER") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getCompanyName(selectedKey));
        askModel.setSymbol(selectedKey);
        bidModel.setSymbol(selectedKey);
        tableAsk.getTable().updateUI();
        tableBid.getTable().updateUI();
        this.updateUI();
        oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
        oAskViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
    }
}
