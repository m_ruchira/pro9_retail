package com.isi.csvr.debug;

import bsh.EvalError;
import bsh.Interpreter;
import com.isi.csvr.Client;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.util.MD5;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 28, 2005
 * Time: 10:05:33 AM
 */
public class Debug implements ActionListener, KeyListener, Comparator, ListModel {

    private static final int VISIBLE_COUNT = 10;
    private TWTextArea expressionPanell;
    private TWTextField packages;
    private Interpreter interpreter;
    private JList methoList;
    private ArrayList listStore;
    private JFrame frame;
    private Popup pop;
    private boolean popupVisible;
    private JScrollPane scrollPane;

    public Debug() throws Exception {
        interpreter = new Interpreter();
        interpreter.setOut(System.out);
        interpreter.setErr(System.err);
        listStore = new ArrayList();

        String pw = JOptionPane.showInputDialog(Client.getInstance().getFrame(), "Password :");
        if (pw != null) {
            System.out.println(MD5.getHashString(pw));
            if ("20ee80e63596799a1543bc9fd88d8878".equals(MD5.getHashString(pw))) {
                createUI();
            } else {
                throw new RuntimeException("Invalid PW");
            }
        } else {
            throw new RuntimeException("Invalid PW");
        }

        createUI();

    }

    public static void main(String[] args) {
        try {
            Debug debug = new Debug();
            debug.frame.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createUI() {

        frame = new JFrame("debug");

        String[] widths = {"100%"};
        String[] heights = {"25", "100%", "35"};
        frame.setLayout(new FlexGridLayout(widths, heights));

        packages = new TWTextField();
        frame.add(packages);

        expressionPanell = new TWTextArea();
        expressionPanell.addKeyListener(this);
        frame.add(new JScrollPane(expressionPanell));

        JPanel buttonPanel1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton btnExecute = new JButton("Execute");
        btnExecute.setActionCommand("EXE");
        btnExecute.addActionListener(this);
        buttonPanel1.add(btnExecute);
        JButton btnClear = new JButton("Clear");
        btnClear.setActionCommand("CLR1");
        btnClear.addActionListener(this);
        buttonPanel1.add(btnClear);
        frame.add(buttonPanel1);

        frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
        frame.setSize(400, 300);

        methoList = new JList(this);
        methoList.setVisibleRowCount(VISIBLE_COUNT);
    }

    private void execute() {
        interpreter.getNameSpace().importPackage(packages.getText());

        String expr = expressionPanell.getText();

        try {
            Object value = interpreter.eval(expr);
            System.out.println(value);
        } catch (EvalError evalError) {
            evalError.printStackTrace();
        }
    }

    private void getMethods() {
        interpreter.getNameSpace().importPackage(packages.getText());

        listStore.clear();
        ArrayList list = new ArrayList();
//        JPopupMenu pop = new JPopupMenu();
        boolean instance = false;

        try {
            Method[] methods;
            try {
                try {
                    methods = (Method[]) interpreter.eval(expressionPanell.getText() + "class.getMethods()");
                    instance = false;
                } catch (EvalError evalError) {
                    methods = (Method[]) interpreter.eval(expressionPanell.getText() + "getClass().getMethods()");
                    instance = true;
                }

                for (int i = 0; i < methods.length; i++) {
                    list.add(methods[i]);
                }

            } catch (EvalError evalError) {
                evalError.printStackTrace();
            }


            Field[] fields;
            try {
                if (instance) {
                    fields = (Field[]) interpreter.eval(expressionPanell.getText() + "getClass().getFields()");
                } else {
                    fields = (Field[]) interpreter.eval(expressionPanell.getText() + "class.getFields()");
                }
                for (int i = 0; i < fields.length; i++) {
                    list.add(fields[i]);
                }

            } catch (EvalError evalError) {
                evalError.printStackTrace();
            }

            Collections.sort(list, this);


            for (int i = 0; i < list.size(); i++) {
                Member member = (Member) list.get(i);
                if (member instanceof Field) {
                    Field field = (Field) member;
                    if (Modifier.isPublic(field.getModifiers())) {
                        if (Modifier.isStatic(field.getModifiers()) || instance) {
                            MethodItem item = new MethodItem();
                            item.record = getFieldString(field);
                            item.data = field.getName();
                            listStore.add(item);
                        }
                    }
                } else {
                    Method method = (Method) member;
                    if (Modifier.isPublic(method.getModifiers())) {
                        if (Modifier.isStatic(method.getModifiers()) || instance) {
                            MethodItem item = new MethodItem();
                            item.record = getMethodString(method);
                            item.data = method.getName() + "()";
                            listStore.add(item);
                        }
                    }
                }
            }

            /*for (int i = 0; i < listStore.size(); i++) {
                MethodItem item = (MethodItem)listStore.get(i);
                System.out.println(item.toString());

            }*/

            if (methoList.getModel().getSize() > 0) {
                methoList.setSelectedIndex(0);
                scrollPane = new JScrollPane(methoList);
                scrollPane.setBorder(BorderFactory.createLineBorder(Color.red));
                pop = PopupFactory.getSharedInstance().getPopup(frame, scrollPane, 100, 100);
//                JOptionPane.showConfirmDialog(frame, scrollPane);
                pop.show();
                popupVisible = true;
            }
//            pop.show(frame, 100, 100);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getMethodString(Method method) {
        StringBuffer sb = new StringBuffer();
        sb.append("<HTML><FONT COLOR=blue>" + getTypeName(method.getReturnType()) + "</FONT>&nbsp; ");
        sb.append(method.getName() + "(");
        Class[] params = method.getParameterTypes(); // avoid clone
        for (int j = 0; j < params.length; j++) {
            sb.append(getTypeName(params[j]));
            if (j < (params.length - 1))
                sb.append(",");
        }
        sb.append(")");

        return sb.toString();
    }

    private String getFieldString(Field field) {
        StringBuffer sb = new StringBuffer();
        sb.append("<HTML><FONT COLOR=blue>" + getTypeName(field.getType()));
        sb.append("</FONT>&nbsp;");
        sb.append(field.getName());
        sb.append("</HTML>");

        return sb.toString();
    }

    private String getTypeName(Class type) {
        if (type.isArray()) {
            try {
                Class cl = type;
                int dimensions = 0;
                while (cl.isArray()) {
                    dimensions++;
                    cl = cl.getComponentType();
                }
                StringBuffer sb = new StringBuffer();
                sb.append(cl.getName());
                for (int i = 0; i < dimensions; i++) {
                    sb.append("[]");
                }
                return sb.toString();
            } catch (Throwable e) { /*FALLTHRU*/ }
        }
        return type.getName();
    }

    public void show() {
        frame.show();
    }

    private void selectItem(int code) {
        try {
            if (popupVisible) {
                if (code == KeyEvent.VK_DOWN) {
                    methoList.setSelectedIndex(methoList.getSelectedIndex() + 1);
                    //if (methoList.getSelectedIndex() >= VISIBLE_COUNT){
                    //doVerticalScrolling(methoList.getSelectedIndex() - VISIBLE_COUNT + 1, true);
                    if (methoList.getLastVisibleIndex() < methoList.getSelectedIndex()) {
                        doVerticalScrolling(methoList.getSelectedIndex(), true);
                    }
                    //}
                } else if (code == KeyEvent.VK_UP) {
                    methoList.setSelectedIndex(methoList.getSelectedIndex() - 1);
                    if (methoList.getFirstVisibleIndex() > methoList.getSelectedIndex()) {
                        doVerticalScrolling(methoList.getFirstVisibleIndex() - methoList.getSelectedIndex(), false);
                    }
                } else if (code == KeyEvent.VK_ENTER) {
                    MethodItem methodItem = (MethodItem) methoList.getSelectedValue();
                    expressionPanell.insert(methodItem.data, expressionPanell.getCaretPosition());
                    pop.hide();
                    popupVisible = false;
                } else if (code == KeyEvent.VK_ESCAPE) {
                    pop.hide();
                    popupVisible = false;
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private void doVerticalScrolling(int noOfRows, boolean isScrollDown) {
        JScrollBar vBar = scrollPane.getVerticalScrollBar();
        if (isScrollDown)
            vBar.setValue(vBar.getUnitIncrement(1) * noOfRows);
        else
            vBar.setValue(vBar.getValue() - vBar.getUnitIncrement(-1) * noOfRows);

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() instanceof JMenuItem) {
            expressionPanell.insert(e.getActionCommand(), expressionPanell.getCaretPosition());
        } else if (e.getActionCommand().equals("EXE")) {
            execute();
        }
    }

    public void keyPressed(KeyEvent e) {
        if (popupVisible) {
            e.consume();
        }
    }

    public void keyReleased(KeyEvent e) {
        selectItem(e.getKeyCode());
        e.consume();
    }

    public void keyTyped(KeyEvent e) {
        if (e.isControlDown()) {
            if (e.getKeyChar() == KeyEvent.VK_SPACE) {
                getMethods();
            }
        } else if (e.getKeyChar() == (char) KeyEvent.VK_ESCAPE) {
            if (popupVisible) {
                pop.hide();
                popupVisible = false;
            }
        } else if (popupVisible) {
            e.consume();
        }
    }

    public void addListDataListener(ListDataListener l) {
    }

    public Object getElementAt(int index) {
        return listStore.get(index);
//        return "";
    }

    public void removeListDataListener(ListDataListener l) {
    }

    public int getSize() {
        return listStore.size();
//        return 10;
    }

    public int compare(Object o1, Object o2) {
        Member method1 = (Member) o1;
        Member method2 = (Member) o2;

        return method1.getName().compareTo(method2.getName());
    }

    class MethodItem {
        public String record;
        public String data;

        public String toString() {
            return record;
        }
    }
}
