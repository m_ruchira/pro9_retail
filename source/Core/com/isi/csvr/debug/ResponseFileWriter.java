package com.isi.csvr.debug;

import com.dfn.mtr.mix.beans.MIXObject;
import com.isi.csvr.shared.DynamicArray;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: shanakak
 * Date: Jul 25, 2012
 * Time: 12:53:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResponseFileWriter {
    private static ResponseFileWriter self;

    private ResponseFileWriter() {
    }

    public static synchronized ResponseFileWriter getSharedInstance() {
        if (self == null) {
            self = new ResponseFileWriter();
        }
        return self;
    }

    public static void writeToFile2(DynamicArray<String> response) {
        try {
            String write = "%s\n";
            // Create file
            Calendar c = Calendar.getInstance();

            FileWriter fstream = new FileWriter("D:/out.txt", true);
//            FileWriter fstream = new FileWriter("/System/out.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            for (int i = 0; i < response.size(); i++) {
                out.write(String.format(write, response.get(i)));
            }
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void writeToFile(MIXObject response, Byte path) {
        try {
            String write = "%s\n";
            // Create file
            Calendar c = Calendar.getInstance();

            FileWriter fstream = new FileWriter("D:/out.txt", true);
//            FileWriter fstream = new FileWriter("/System/out.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(String.format(write, c.getTime().toString(), path, response.toString()));
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void writeToFile(MIXObject resObject) {
        writeToFile(resObject, null);
    }

    /*public static void main(String[] args) {
        String a1 = "archive2";
        String a2 = "Archive2";
        String b = a2.substring(a2.toLowerCase().indexOf("archive"));
        if(a2.endsWith("ve")){
            System.out.println("okay");
        }
        System.out.println(a1.substring(7));
    }*/

   /* public static void main(String[] args) {
        String s = "123"+'\035'+"456"+'\035'+"789";
        System.out.println(s.length());
        s = s.replace("\035","");
        System.out.println(s.length()+" "+s);

    }*/

    /*public static void readFile()throws Exception{
        Scanner s = new Scanner(new File("D:/new.txt"));
        String line = "";
        double d = 0;
        try{
        while((line = s.nextLine())!=null){
            d += Double.parseDouble(line.substring(0,line.indexOf(".")+4));
            System.out.println(line.substring(0,line.indexOf(".")+4));
        }
        }catch(Exception e){}
        System.out.println("Val: "+d);
    }

    public static void main(String[] args)throws Exception {
        readFile();        
    }*/
}
