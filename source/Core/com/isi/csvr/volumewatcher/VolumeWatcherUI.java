package com.isi.csvr.volumewatcher;

import com.isi.csvr.Client;
import com.isi.csvr.ClientTable;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 15-Apr-2008 Time: 08:53:58 To change this template use File | Settings
 * | File Templates.
 */
public class VolumeWatcherUI implements ActionListener, Runnable, ExchangeListener, ApplicationListener {

    private static final int selectedExchangeID = 441;
    private static final int selectedCriteriaID = 442;
    private static final int triggerPct = 443;
    private static final int selectedMktID = 444;
    private static final int selectedIndexID = 446;
    public static VolumeWatcherUI self = null;
    private final String DEFAULT = "----";
    private ClientTable volumeTable;
    private JPanel northPanel;
    private TWComboBox exchangeCombo;
    private ArrayList marketList;
    private ArrayList<TWComboItem> exchanges;
    private TWComboBox criteriaCombo;
    private JLabel lblMarket;
    private TWComboBox marketCombo;
    private ArrayList<TWComboItem> criteria;
    private JLabel lblIndex;
    private TWComboBox indexCombo;
    private ArrayList indexList;
    private ViewSetting g_oSetting;
    private String selectedExchange = null;
    private String prevExchange = null;
    private String selectedMarket = null;
    private String selectedIndex = null;
    private Symbols volumeWatcherSymbols;
    private JLabel triggerLabel;
    private TWTextField triggerPctFdl;
    private JButton btnHelp;
    private boolean isLoadedFromWsp = false;
    private boolean isFirsttime = false;
    private Symbols indexSymbols = new Symbols();


    private VolumeWatcherUI() {
        g_oSetting = ViewSettingsManager.getSummaryView("VOLUME_WATCHER_VIEW");
        g_oSetting.setRenderingIDs(ViewSettingsManager.getMainBoardRenderingIDs());
//        ExchangeStore.getSharedInstance().loadCashFlowExchangeIndices();
        createUI();
        Thread searchTRD = new Thread(this, "volume_Watcher");
        searchTRD.start();
    }

    public static VolumeWatcherUI getSharedInstance() {
        if (self == null) {
            self = new VolumeWatcherUI();
        }
        return self;
    }

    private void createUI() {
        try {
            northPanel = new JPanel();
            exchanges = new ArrayList<TWComboItem>();
            exchangeCombo = new TWComboBox(new TWComboModel(exchanges));
            marketList = new ArrayList();
            lblMarket = new JLabel(Language.getString("VOL_WATCH_MKT"));
            indexList = new ArrayList();
            indexCombo = new TWComboBox(new TWComboModel(indexList));
            indexCombo.addActionListener(this);
            lblIndex = new JLabel(Language.getString("INDEX"));
            criteria = new ArrayList<TWComboItem>();
            marketCombo = new TWComboBox(new TWComboModel(marketList));
            marketCombo.addActionListener(this);
            criteriaCombo = new TWComboBox(new TWComboModel(criteria));
            triggerLabel = new JLabel(Language.getString("TRIGGER_PCT"));
            triggerPctFdl = new TWTextField() {
                public String getText() {
                    String text = super.getText();
                    if (text == null || text.isEmpty()) {
                        text = "0";
                    }
                    return text;
                }
            };
            triggerLabel.setHorizontalAlignment(JTextField.CENTER);
            triggerPctFdl.setHorizontalAlignment(JTextField.TRAILING);
            triggerPctFdl.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
            btnHelp = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/charts/indicator_help.gif"));
            btnHelp.setContentAreaFilled(false);
            btnHelp.setBorder(BorderFactory.createEmptyBorder());
            btnHelp.addActionListener(this);
            populateExchangesCombo();
            populateCriteriaCombo();
            exchangeCombo.addActionListener(this);
            criteriaCombo.addActionListener(this);
            northPanel.setLayout(new FlexGridLayout(new String[]{"10", "120", "10", "200", "7", "65", "7", "200", "7", "38", "7", "200", "7", "60", "7", "170", "7", "150", "10", "60", "20", "30", "100%"}, new String[]{"20"}, 3, 3));
            northPanel.setPreferredSize(new Dimension((int) g_oSetting.getSize().getWidth(), 26));
            northPanel.add(new JLabel());
            JLabel lblExchange = new JLabel(Language.getString("SELECT_EXCHANGE"));
            northPanel.add(lblExchange);
            northPanel.add(new JLabel());
            northPanel.add(exchangeCombo);
            northPanel.add(new JLabel());
            northPanel.add(lblMarket);
            northPanel.add(new JLabel());
            northPanel.add(marketCombo);
            northPanel.add(new JLabel());
            northPanel.add(lblIndex);
            northPanel.add(new JLabel());
            northPanel.add(indexCombo);
            northPanel.add(new JLabel());
            JLabel lblCriteria = new JLabel(Language.getString("CRITERIA"));
            lblCriteria.setHorizontalAlignment(JTextField.CENTER);
            northPanel.add(lblCriteria);
            northPanel.add(new JLabel());
            northPanel.add(criteriaCombo);
            northPanel.add(new JLabel());
            northPanel.add(triggerLabel);
            northPanel.add(new JLabel());
            northPanel.add(triggerPctFdl);
            northPanel.add(new JLabel("%"));
            northPanel.add(btnHelp);
            northPanel.add(new JLabel());

            GUISettings.applyOrientation(northPanel);
            volumeTable = new ClientTable();
            volumeTable.setSize(g_oSetting.getSize());
            volumeTable.setResizable(true);
            volumeTable.setMaximizable(true);
            volumeTable.setIconifiable(true);
            volumeTable.setClosable(true);
            volumeTable.setLinkWindowControl(true);
            volumeTable.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
            volumeTable.setSortable(true);
            doColumnRenamings();
            volumeTable.setViewSettings(g_oSetting);
            volumeTable.createTable(true);
            volumeTable.setVisible(true);
            volumeTable.getTable().addMouseListener(Client.getInstance().getSymbolMouseListener());
            volumeTable.setTypeDescription(Language.getString("VOLUME_WATCHER"));
            volumeTable.setNorthPanel(northPanel);
            volumeTable.setPreferredLayer(GUISettings.TOP_LAYER);
            Client.getInstance().oTopDesktop.add(volumeTable);
            volumeTable.getScrollPane().getViewport().addMouseListener(Client.getInstance().getTableMouseListener());
            ExchangeStore.getSharedInstance().addExchangeListener(this);
            volumeTable.updateClientTable();
            volumeTable.adjustColumns();
            volumeTable.setVisible(g_oSetting.isVisible());
            initUI();
            volumeTable.applySettings();
            Application.getInstance().addApplicationListener(this);
            isFirsttime = true;
            volumeTable.setPreferredLayer(GUISettings.TOP_LAYER);
            volumeTable.applyTheme();
            volumeTable.updateUI();
            volumeTable.updateGUI();
        } catch (Exception e) {
            System.out.println("Create UI");
            e.printStackTrace();
        }


    }

    public void setViewSetting() {
        try {
            Vector oViewSettingVec = ViewSettingsManager.getViewsVector();
            Object[] views = oViewSettingVec.toArray();
            for (int j = 0; j < views.length; j++) {
                ViewSetting oSettings = ((ViewSetting) views[j]).getObject();
                if (oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW) {
                    if (oSettings.isVolumeWatcherType()) {
                        g_oSetting = (ViewSetting) oSettings.clone();
                        ViewSettingsManager.addMainBoardSetting(g_oSetting);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateExchangesCombo() {
        SharedMethods.populateExchangesForTWCombo(exchanges, true);
        if (exchanges.size() > 1) {
            exchanges.add(0, new TWComboItem("ALL", Language.getString("ALL")));
        }
//        exchangeCombo.setSelectedIndex(0);
//        try {
//            exchangeCombo.setSelectedIndex(Integer.parseInt(g_oSetting.getProperty(selectedExchangeID)));
//        } catch (Exception e) {
//            try {
//                exchangeCombo.setSelectedIndex(0);
//            } catch (Exception e1) {
//
//            }
//        }
        exchangeCombo.updateUI();
    }

    private void populateCriteriaCombo() {
        criteria.add(new TWComboItem(Constants.VOLUME_WATCHER_SEVENDAY, Language.getString("VOLUME_WATCHER_SEVENDAY")));
        criteria.add(new TWComboItem(Constants.VOLUME_WATCHER_THIRTYDAY, Language.getString("VOLUME_WATCHER_THIRTYDAY")));
        criteria.add(new TWComboItem(Constants.VOLUME_WATCHER_NINETYDAY, Language.getString("VOLUME_WATCHER_NINETYDAY")));
        criteriaCombo.setSelectedIndex(0);
        try {
            criteriaCombo.setSelectedIndex(Integer.parseInt(g_oSetting.getProperty(selectedCriteriaID)));
        } catch (Exception e) {
            criteriaCombo.setSelectedIndex(0);
        }
        criteriaCombo.updateUI();
    }

    private void populateIndexCombo(String exchange) {
        Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
        indexList.clear();
        indexList.add(new TWComboItem("DEFAULT", DEFAULT));
        indexList.add(new TWComboItem("ALL", Language.getString("ALL")));
        Enumeration indices = ex.getCashFlowExchangeIndices().keys();
        while (indices.hasMoreElements()) {
            String index = (String) indices.nextElement();
            indexList.add(new TWComboItem(index, index));
        }
        indexCombo.setSelectedIndex(0);
//        try {
//            indexCombo.setSelectedIndex(Integer.parseInt(g_oSetting.getProperty(selectedIndexID)));
//        } catch (Exception e) {
//            indexCombo.setSelectedIndex(0);
//        }
        indexCombo.updateUI();
    }

    private void loadIndexSymbols(String index) {
        indexSymbols.clear();
        if (index == null || index == "" || index.equals("DEFAULT"))
            return;

        Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
        if (index.equals("ALL")) {
            Enumeration<String> keyEnum = exg.getCashFlowExchangeIndices().keys();
            while (keyEnum.hasMoreElements()) {
                ArrayList<String> symbols = exg.getCashFlowExchangeIndices().get(keyEnum.nextElement());
                for (String symbol : symbols) {
                    if (!indexSymbols.isAlreadyIn(symbol))
                        indexSymbols.appendSymbol(symbol);
                }
            }
        } else {
            ArrayList<String> symbols = exg.getCashFlowExchangeIndices().get(index);
            for (String symbol : symbols) {
                indexSymbols.appendSymbol(symbol);
            }
        }

    }

    private void firstTimeLoad() {
        if (exchangeCombo.getItemCount() > 0) {
            exchangeCombo.setSelectedIndex(0);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(exchangeCombo)) {
            getSelectdExchange();
            if (!(selectedExchange.equals("ALL"))) {
                Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
                if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                    populateMarketCombo(selectedExchange);
                    String subMarketID = null;
                    try {
                        subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                    } catch (Exception e1) {
                        TWComboItem marketAll = getDefaultMarket(marketList);
                        if (marketAll != null) {
                            marketCombo.setSelectedItem(marketAll);
                        } else {
                            marketCombo.setSelectedIndex(0);
                        }
                        subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                    }
                    selectedMarket = subMarketID;
                    marketCombo.setVisible(true);
                    lblMarket.setVisible(true);
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(5, "65");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(6, "2");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(7, "110");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(8, "3");

                } else {
                    lblMarket.setVisible(false);
                    marketCombo.setVisible(false);
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(5, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(6, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(7, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(8, "1");
                }

                if (exg.cashFlowExchageIndicesExists()) {
                    String indexID = "";
                    populateIndexCombo(selectedExchange);
                    try {
                        indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                    } catch (Exception e1) {
                        indexCombo.setSelectedIndex(0);
                        indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                    }
                    indexCombo.setVisible(true);
                    lblIndex.setVisible(true);
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(9, "30");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(10, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(11, "110");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(12, "3");

                } else {
                    indexCombo.setVisible(false);
                    lblIndex.setVisible(false);
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(9, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(10, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(11, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(12, "1");
                }
            } else {
                lblMarket.setVisible(false);
                marketCombo.setVisible(false);
                ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(5, "1");
                ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(6, "1");
                ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(7, "1");
                ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(8, "1");

                indexCombo.setVisible(false);
                lblIndex.setVisible(false);
                ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(9, "1");
                ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(10, "1");
                ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(11, "1");
                ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(12, "1");
            }

        } else if (e.getSource().equals(marketCombo)) {
            String subMkt = "";
            try {
                subMkt = ((TWComboItem) marketCombo.getSelectedItem()).getId();
            } catch (Exception e1) {
                subMkt = "";
            }
            selectedMarket = subMkt;
            if (!isLoadedFromWsp) {
                g_oSetting.putProperty(selectedMktID, marketCombo.getSelectedIndex());
            }

        } else if (e.getSource().equals(indexCombo)) {
            String index = "";
            try {
                index = ((TWComboItem) indexCombo.getSelectedItem()).getId();
            } catch (Exception e1) {
                index = "";
            }
            selectedIndex = index;
            loadIndexSymbols(index);
            if (!isLoadedFromWsp) {
                g_oSetting.putProperty(selectedIndexID, indexCombo.getSelectedIndex());
            }

        } else if (e.getSource().equals(criteriaCombo)) {
            String id = ((TWComboItem) criteriaCombo.getSelectedItem()).getId();
            StockData.setVolumeWatcherMode(Integer.parseInt(id));
            g_oSetting.putProperty(selectedCriteriaID, criteriaCombo.getSelectedIndex() /*selectedCriteria*/);
            doColumnRenamings();
            volumeTable.updateUI();
        } else if (e.getSource() == btnHelp) {
            NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_VOLUME_WATCHER"));
        }
    }

    private void initUI() {
        try {
            volumeWatcherSymbols = new Symbols();
            volumeTable.setSymbols(volumeWatcherSymbols);
            if (g_oSetting.getProperty(triggerPct) == null || g_oSetting.getProperty(triggerPct).isEmpty()) {
                triggerPctFdl.setText("100");
            } else {
                triggerPctFdl.setText(g_oSetting.getProperty(triggerPct));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (((TWComboItem) exchangeCombo.getSelectedItem()).getId().equals("ALL")) {
//            lblMarket.setVisible(false);
//            marketCombo.setVisible(false);
//            ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(5, "1");
//            ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(6, "1");
//            ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(7, "1");
//            ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(8, "1");
//
//            indexCombo.setVisible(false);
//            lblIndex.setVisible(false);
//            ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(9, "1");
//            ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(10, "1");
//            ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(11, "1");
//            ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(12, "1");
//        }
    }

    private void applySavedSettings() {

        try {
            String selexIndex = (String) g_oSetting.getProperty(selectedExchangeID);
            String selex = "";
            if (selexIndex != null && !selexIndex.isEmpty()) {
                try {
                    exchangeCombo.setSelectedIndex(Integer.parseInt(selexIndex));
                } catch (NumberFormatException e) {
                    exchangeCombo.setSelectedIndex(0);
                }
                selex = ((TWComboItem) exchangeCombo.getSelectedItem()).getId();
            }
            /////////////////////////////////////////////////////////////////////
            if ((selex != null) && (!selex.equals(""))) {
                if (!(selex.equals("ALL"))) {
                    Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        populateMarketCombo(selectedExchange);
                        String subMarketID = null;
                        try {
                            marketCombo.setSelectedIndex(Integer.parseInt(g_oSetting.getProperty(selectedMktID)));
                            subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                        } catch (Exception e1) {
                            TWComboItem marketAll = getDefaultMarket(marketList);
                            if (marketAll != null) {
                                marketCombo.setSelectedItem(marketAll);
                            } else {
                                marketCombo.setSelectedIndex(0);
                            }
                            subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                        }
                        selectedMarket = subMarketID;
                        marketCombo.setVisible(true);
                        lblMarket.setVisible(true);
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(5, "65");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(6, "2");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(7, "110");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(8, "3");

                    } else {
                        lblMarket.setVisible(false);
                        marketCombo.setVisible(false);
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(5, "1");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(6, "1");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(7, "1");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(8, "1");
                    }

                    if (exg.cashFlowExchageIndicesExists()) {
                        String indexID = "";
                        populateIndexCombo(selectedExchange);
                        try {
                            indexCombo.setSelectedIndex(Integer.parseInt(g_oSetting.getProperty(selectedIndexID)));
                            indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                        } catch (Exception e1) {
                            indexCombo.setSelectedIndex(0);
                            indexID = ((TWComboItem) indexCombo.getSelectedItem()).getId();
                        }
                        selectedIndex = indexID;
                        indexCombo.setVisible(true);
                        lblIndex.setVisible(true);
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(9, "30");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(10, "1");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(11, "110");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(12, "3");

                    } else {
                        indexCombo.setVisible(false);
                        lblIndex.setVisible(false);
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(9, "1");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(10, "1");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(11, "1");
                        ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(12, "1");
                    }

                } else {
                    lblMarket.setVisible(false);
                    marketCombo.setVisible(false);
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(5, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(6, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(7, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(8, "1");

                    indexCombo.setVisible(false);
                    lblIndex.setVisible(false);
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(9, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(10, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(11, "1");
                    ((FlexGridLayout) northPanel.getLayout()).setColumnWidth(12, "1");
                }
            }

        } catch (Exception e) {
            isLoadedFromWsp = false;
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void doColumnRenamings() {
        String[] columnHeadings = g_oSetting.getColumnHeadings();
        columnHeadings[10] = Language.getString("VOLUME_WATCH_CVOLUME");
        columnHeadings[118] = getColumnNameForVolume();
        columnHeadings[119] = getColumnNameForPctVolume();
        try {
            g_oSetting.getColumnHeadings()[10] = Language.getString("VOLUME_WATCH_CVOLUME");
            g_oSetting.getColumnHeadings()[118] = getColumnNameForVolume();
            g_oSetting.getColumnHeadings()[119] = getColumnNameForPctVolume();
        } catch (Exception e) {
        }
        if (volumeTable.getTable() != null) {
            volumeTable.upDateColumnHeadings();
            volumeTable.fireColumnChanged();
        }
    }

    private String getColumnNameForVolume() {
        if (criteriaCombo.getSelectedIndex() == 0) {
            return Language.getString("VOLUME_WATCH_7DAY");
        } else if (criteriaCombo.getSelectedIndex() == 1) {
            return Language.getString("VOLUME_WATCH_30DAY");
        } else if (criteriaCombo.getSelectedIndex() == 2) {
            return Language.getString("VOLUME_WATCH_90DAY");
        } else {
            return "Avg. Volume";
        }
    }

    private String getColumnNameForPctVolume() {
        if (criteriaCombo.getSelectedIndex() == 0) {
            return Language.getString("VOLUME_WATCH_7DAYCHANGE");
        } else if (criteriaCombo.getSelectedIndex() == 1) {
            return Language.getString("VOLUME_WATCH_30DAYCHANGE");
        } else if (criteriaCombo.getSelectedIndex() == 2) {
            return Language.getString("VOLUME_WATCH_90DAYCHANGE");
        } else {
            return "Volume Chg";
        }
    }

    private void getSelectdExchange() {
        try {
            selectedExchange = ((TWComboItem) exchangeCombo.getSelectedItem()).getId();
            g_oSetting.putProperty(selectedExchangeID, exchangeCombo.getSelectedIndex() /*selectedExchange*/);
            try {
                Thread.interrupted();
            } catch (Exception e1) {
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void run() {
        Symbols symbolsObject = volumeTable.getSymbols();
        boolean dataChanged = false;
        while (true) {
            try {
                dataChanged = false;
                if ((volumeTable.isVisible()) || (volumeTable.isDetached())) {
                    if (selectedExchange == null) {
                        getSelectdExchange();
                    }
                    if ((prevExchange == null) || (!prevExchange.equals(selectedExchange))) {
                        symbolsObject.clear();
                        prevExchange = selectedExchange;
                        dataChanged = true;
                    }
                    if (selectedExchange.equals("ALL")) {
                        Enumeration exchanngeEnum = ExchangeStore.getSharedInstance().getExchanges();
                        while (exchanngeEnum.hasMoreElements()) {
                            Exchange exg = (Exchange) exchanngeEnum.nextElement();
                            if (exg.isDefault()) {
                                Enumeration symbolsEnum = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                while (symbolsEnum.hasMoreElements()) {
                                    String data = (String) symbolsEnum.nextElement();
                                    String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                                    int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                                    Stock st = DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(exg.getSymbol(), symbol, instrument));
                                    double trigger = Double.parseDouble(triggerPctFdl.getText());
                                    if ((st.getVolume() > 0) && (st.getAvgVolume(StockData.getVolumeWatcherMode()) > 0)) {

//                                        if ((triggerPctFdl.getText().length() > 0) && (st.getVolume() > (st.getAvgVolume(getAVGVolumeMode()) * Double.parseDouble(triggerPctFdl.getText()) / 100))) {
                                        if ((triggerPctFdl.getText().length() > 0) &&
                                                (trigger <= (((st.getVolume() - st.getAvgVolume(getAVGVolumeMode())) * 100D / st.getAvgVolume(getAVGVolumeMode())) /*+ 100*/))) { // +100 removed  	 SAUMUBMKT-1333
                                            if (!symbolsObject.isAlreadyIn(st.getKey())) {
                                                symbolsObject.appendSymbol(st.getKey());
                                                dataChanged = true;
                                                g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                            }
                                        } else {
                                            if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                symbolsObject.removeSymbol(st.getKey());
                                                dataChanged = true;
                                                g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                            }
                                        }
                                    } else {
                                        if (symbolsObject.isAlreadyIn(st.getKey())) {
                                            symbolsObject.removeSymbol(st.getKey());
                                            dataChanged = true;
                                            g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                        }
                                    }
                                }
                            }
                        }

                    } else {
                        Enumeration symbolsEnum = DataStore.getSharedInstance().getSymbols(selectedExchange);
                        Exchange exg = ExchangeStore.getSharedInstance().getExchange(selectedExchange);
                        boolean hasSubMkts = false;
                        boolean hasIndices = false;
                        if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0))
                            hasSubMkts = true;
                        if (exg.cashFlowExchageIndicesExists())
                            hasIndices = true;

                        while (symbolsEnum.hasMoreElements()) {
                            String data = (String) symbolsEnum.nextElement();
                            String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                            int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                            double trigger = Double.parseDouble(triggerPctFdl.getText());
                            Stock st = DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(selectedExchange, symbol, instrument));

                            if (hasSubMkts || hasIndices) {
                                if (hasSubMkts) {
                                    if ((selectedMarket != null) || !(selectedMarket.equals(""))) {
                                        if (selectedMarket.equals("ALL")) {
                                            if ((st.getVolume() > 0) && (st.getAvgVolume(StockData.getVolumeWatcherMode()) > 0)) {

                                                if ((triggerPctFdl.getText().length() > 0) &&
                                                        (trigger <= (((st.getVolume() - st.getAvgVolume(getAVGVolumeMode())) * 100D / st.getAvgVolume(getAVGVolumeMode())) /*+ 100*/))) {
//                                        (st.getVolume() > (st.getAvgVolume(getAVGVolumeMode()) * Double.parseDouble(triggerPctFdl.getText()) / 100))) {
                                                    if (!symbolsObject.isAlreadyIn(st.getKey())) {
                                                        symbolsObject.appendSymbol(st.getKey());
                                                        dataChanged = true;
                                                        g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                        System.out.println("Stock : " + st.getKey());
                                                    }
                                                } else {
                                                    if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                        symbolsObject.removeSymbol(st.getKey());
                                                        dataChanged = true;
                                                        g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                    }
                                                }
                                            } else {
                                                if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                    symbolsObject.removeSymbol(st.getKey());
                                                    dataChanged = true;
                                                    g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                }
                                            }
                                        } else if (st.getMarketID().equals(selectedMarket)) {
                                            if ((st.getVolume() > 0) && (st.getAvgVolume(StockData.getVolumeWatcherMode()) > 0)) {
                                                if ((triggerPctFdl.getText().length() > 0) &&
                                                        (trigger <= (((st.getVolume() - st.getAvgVolume(getAVGVolumeMode())) * 100D / st.getAvgVolume(getAVGVolumeMode())) /*+ 100*/))) {
//                                        (st.getVolume() > (st.getAvgVolume(getAVGVolumeMode()) * Double.parseDouble(triggerPctFdl.getText()) / 100))) {
                                                    if (!symbolsObject.isAlreadyIn(st.getKey())) {
                                                        symbolsObject.appendSymbol(st.getKey());
                                                        dataChanged = true;
                                                        g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                    }
                                                } else {
                                                    if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                        symbolsObject.removeSymbol(st.getKey());
                                                        dataChanged = true;
                                                        g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                    }
                                                }
                                            } else {
                                                if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                    symbolsObject.removeSymbol(st.getKey());
                                                    dataChanged = true;
                                                    g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                }
                                            }
                                        } else {
                                            if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                symbolsObject.removeSymbol(st.getKey());
                                                dataChanged = true;
                                                g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                            }
                                        }
                                    }
                                }

                                if (hasIndices) {
                                    if ((selectedIndex != null) || !(selectedIndex.equals(""))) {
                                        if (indexSymbols.isAlreadyIn(st.getKey())) {
                                            if ((st.getVolume() > 0) && (st.getAvgVolume(StockData.getVolumeWatcherMode()) > 0)) {
                                                if ((triggerPctFdl.getText().length() > 0) &&
                                                        (trigger <= (((st.getVolume() - st.getAvgVolume(getAVGVolumeMode())) * 100D / st.getAvgVolume(getAVGVolumeMode())) /*+ 100*/))) {
                                                    if (!symbolsObject.isAlreadyIn(st.getKey())) {
                                                        symbolsObject.appendSymbol(st.getKey());
                                                        dataChanged = true;
                                                        g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                    }
                                                } else {
                                                    if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                        symbolsObject.removeSymbol(st.getKey());
                                                        dataChanged = true;
                                                        g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                    }
                                                }
                                            } else {
                                                if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                    symbolsObject.removeSymbol(st.getKey());
                                                    dataChanged = true;
                                                    g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                                }
                                            }
                                        } else {
                                            if (symbolsObject.isAlreadyIn(st.getKey())) {
                                                symbolsObject.removeSymbol(st.getKey());
                                                dataChanged = true;
                                                g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                            }
                                        }
                                    }
                                }

                                if (hasIndices && hasSubMkts) {
                                    if (selectedMarket.equals("DEFAULT") || selectedIndex.equals("DEFAULT")) {
                                        if (symbolsObject.isAlreadyIn(st.getKey())) {
                                            symbolsObject.removeSymbol(st.getKey());
                                            dataChanged = true;
                                            g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                        }
                                    }
                                }

                            } else {
                                if ((st.getVolume() > 0) && (st.getAvgVolume(StockData.getVolumeWatcherMode()) > 0)) {

                                    if ((triggerPctFdl.getText().length() > 0) &&
                                            (trigger <= (((st.getVolume() - st.getAvgVolume(getAVGVolumeMode())) * 100D / st.getAvgVolume(getAVGVolumeMode())) /*+ 100*/))) {
//                                        (st.getVolume() > (st.getAvgVolume(getAVGVolumeMode()) * Double.parseDouble(triggerPctFdl.getText()) / 100))) {
                                        if (!symbolsObject.isAlreadyIn(st.getKey())) {
                                            symbolsObject.appendSymbol(st.getKey());
                                            dataChanged = true;
                                            g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                        }
                                    } else {
                                        if (symbolsObject.isAlreadyIn(st.getKey())) {
                                            symbolsObject.removeSymbol(st.getKey());
                                            dataChanged = true;
                                            g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                        }
                                    }
                                } else {
                                    if (symbolsObject.isAlreadyIn(st.getKey())) {
                                        symbolsObject.removeSymbol(st.getKey());
                                        dataChanged = true;
                                        g_oSetting.putProperty(triggerPct, triggerPctFdl.getText());
                                    }
                                }
                            }
                        }
                    }

                    if (dataChanged) {
                        symbolsObject.sortSymbols();
                        ((AbstractTableModel) volumeTable.getTable().getModel()).fireTableDataChanged();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    private void populateMarketCombo(String exchange) {
        marketList.clear();
        marketList.add(new TWComboItem("DEFAULT", DEFAULT));
        marketList.add(new TWComboItem("ALL", Language.getString("ALL")));
        SharedMethods.populateSubMarketsForTWCombo(marketList, true, exchange);
        marketCombo.setSelectedIndex(0);
//        try {
//            marketCombo.setSelectedIndex(Integer.parseInt(g_oSetting.getProperty(selectedMktID)));
//        } catch (Exception e) {
//            marketCombo.setSelectedIndex(0);
//        }
        marketCombo.updateUI();
    }


    public void showVolumeWatcher(boolean isVisible) {
        try {
            if (isFirsttime && !isLoadedFromWsp) {
                isFirsttime = false;
                firstTimeLoad();
            }
            if (volumeTable.isIcon()) {
                volumeTable.setIcon(false);
            } else {
                volumeTable.setVisible(isVisible);
            }
            volumeTable.setSelected(true);
            if (isVisible) {
                SharedMethods.updateGoogleAnalytics("VolumeWatcher");
            }
        } catch (PropertyVetoException e) {
        }
    }

    public void exchangeAdded(Exchange exchange) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        populateExchangesCombo();
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void cellSelectionCell() {
        (volumeTable.getTable()).setCellSelectionEnabled(true);
        (volumeTable.getTable()).setRowSelectionAllowed(true);
        (volumeTable.getTable()).setColumnSelectionAllowed(true);
    }

    public void cellSelectionRow() {
        (volumeTable.getTable()).setCellSelectionEnabled(true);
        (volumeTable.getTable()).setRowSelectionAllowed(true);
        (volumeTable.getTable()).setColumnSelectionAllowed(false);
    }

    private int getAVGVolumeMode() {
        try {
            return Integer.parseInt(g_oSetting.getProperty(selectedCriteriaID));
        } catch (Exception e) {
            return StockData.getVolumeWatcherMode();
        }
    }

    private TWComboItem getDefaultMarket(ArrayList marketList) {
        Iterator it = marketList.iterator();
        while (it.hasNext()) {
            TWComboItem item = (TWComboItem) it.next();
            if (item.getId().equals(Language.getString("DEFAULT"))) {
                return item;
            }
        }
        return null;
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        if (isLoadedFromWsp) {
            applySavedSettings();
            isLoadedFromWsp = false;
        }
    }

    public void setLoadedFromWsp(boolean loadedFromWsp) {
        isLoadedFromWsp = loadedFromWsp;
        if (isLoadedFromWsp) {
            isFirsttime = false;
        }
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
