package com.isi.csvr.volumewatcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Feb 24, 2008
 * Time: 8:36:20 PM
 */
public class ExchangeInfo {

    public static final int TRIGGER_BY_PERCENTAGE = 0;
    public static final int TRIGGER_BY_QUNTITIY = 1;
    final static int TRIGGER_BY_VOLUME = 0; //wherther volume  or last trade etc
    final static int TRIGGER_BY_LTD = 1;
    //exchange info panel  data store .keep track with that
    public int triggerUnit; //whether volume or ltd
    public int triggerMethod;//whether trigger using percentage or perticular volume
    public long triggerAmount;  //keep either percentage value and quntiy
    Hashtable<String, VolumeWatcherData> trigeredSymbolsHash;
    List<VolumeWatcherData> triggersymbolArray;
    List<VolumeWatcherData> tempTriggersymbolArray;
    private String id;
    private boolean isCustomizationEnable;
    private boolean dataLoaded;


    public ExchangeInfo(String id) {
        this.id = id;
        trigeredSymbolsHash = new Hashtable<String, VolumeWatcherData>();
        triggersymbolArray = Collections.synchronizedList(new ArrayList<VolumeWatcherData>());
        tempTriggersymbolArray = Collections.synchronizedList(new ArrayList<VolumeWatcherData>());
    }

    public ExchangeInfo() {


    }//info data

    public String getId() {
        return id;
    }

    public int getTriggerUnit() {
        return triggerUnit;
    }

    public void setTriggerUnit(int triggerUnit) {
        this.triggerUnit = triggerUnit;
    }

    public int getTriggerMethod() {
        return triggerMethod;
    }

    public void setTriggerMethod(int triggerMethod) {
        this.triggerMethod = triggerMethod;
    }

    public long getTriggerAmount() {
        return triggerAmount;
    }

    public void setTriggerAmount(long triggerAmount) {
        this.triggerAmount = triggerAmount;
    }

    public boolean isCustomizationEnable() {
        return isCustomizationEnable;
    }

    public void setCustomizationEnable(boolean customizationEnable) {
        isCustomizationEnable = customizationEnable;
    }

    public List<VolumeWatcherData> getTriggersymbolArray() {
        return triggersymbolArray;
    }

    public void setTriggersymbolArray(ArrayList<VolumeWatcherData> triggersymbolArray) {
        this.triggersymbolArray = triggersymbolArray;
    }

    public List<VolumeWatcherData> getTempTriggersymbolArray() {
        return tempTriggersymbolArray;
    }

    public void setTempTriggersymbolArray(ArrayList<VolumeWatcherData> tempTriggersymbolArray) {
        this.tempTriggersymbolArray = tempTriggersymbolArray;
    }

    public Hashtable<String, VolumeWatcherData> getTrigeredSymbolsHash() {
        return trigeredSymbolsHash;
    }

    public void setTrigeredSymbolsHash(Hashtable<String, VolumeWatcherData> trigeredSymbolsHash) {
        this.trigeredSymbolsHash = trigeredSymbolsHash;
    }

    public boolean isDataLoaded() {
        return dataLoaded;
    }

    public void setDataLoaded(boolean dataLoaded) {
        this.dataLoaded = dataLoaded;
    }
}
