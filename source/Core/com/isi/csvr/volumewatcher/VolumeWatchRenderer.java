package com.isi.csvr.volumewatcher;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Feb 6, 2008
 * Time: 12:46:14 PM
 */
public class VolumeWatchRenderer extends TWBasicTableRenderer {

    static Border selectedBorder;
    static Border unselectedBorder;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private Color foreground = null;
    private Color background = null;
    private String g_sNA = "NA";
    private int g_iCenterAlign;
    private int[] g_asRendIDs;
    private TWDecimalFormat oQuantityFormat;
    private int g_iNumberAlign;
    private int g_iStringAlign;

    public VolumeWatchRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        g_iCenterAlign = JLabel.CENTER;
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        g_iNumberAlign = JLabel.RIGHT;
        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        reload();
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            selectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oSelectedFG);
            unselectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oFG1);
        } catch (Exception e) {
            g_oSelectedFG = Color.green;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void propertyChanged(int property) {
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;

            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;

            } else {
                foreground = g_oFG2;
                background = g_oBG2;

            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        int iRendID = 0;
        iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        switch (iRendID) {
            case 0: // DEFAULT
                if (value == null)
                    lblRenderer.setText(g_sNA);
                else
                    lblRenderer.setText((String) value);
                lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                break;
            case 1:       //symbol
                if (Settings.isShowArabicNumbers())
                    lblRenderer.setText(GUISettings.arabize((String) value));
                else
                    lblRenderer.setText((String) value);
                lblRenderer.setHorizontalAlignment(g_iStringAlign);
                break;

            case 2: // DESCRIPTION
                lblRenderer.setText((String) value);
                lblRenderer.setHorizontalAlignment(g_iStringAlign);
                break;
            case 4: // QUANTITY
                lblRenderer.setText(oQuantityFormat.format(((Long) (value))));
                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                break;

            default:
                lblRenderer.setText("");
        }
        return lblRenderer;
    }
}
