package com.isi.csvr.volumewatcher;

import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Feb 6, 2008
 * Time: 9:40:25 AM
 */

public class VolumeWatchModel extends CommonTable
        implements TableModel, CommonTableInterface {
    //constructor
    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    List<VolumeWatcherData> vMDataStore;


    public VolumeWatchModel(List<VolumeWatcherData> vMDataStore) {
        this.vMDataStore = vMDataStore;

    }

    //---------------------------------overide methods from common table ---------------------
    public void setSymbol(String symbol) {

    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {

        //todo for now : should use a datastore
        return vMDataStore.size();
    }

    public Object getValueAt(int row, int col) {

        //todo for now
        VolumeWatcherData vWData = (VolumeWatcherData) vMDataStore.get(row);

        switch (col) {
            case 0:     // SYMBOL
                return vWData.getSymbol();
            case 1:
                return vWData.getSymbDescription();
            case 2:
                return vWData.getAvgVolume();
            case 3:
                return vWData.getCurrentVolume();
            default:
                return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    //---------------------------------overide methods from common table ---------------------


}

