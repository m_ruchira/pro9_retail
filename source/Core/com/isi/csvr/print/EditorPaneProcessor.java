package com.isi.csvr.print;

import com.isi.csvr.shared.Settings;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.attribute.DocAttributeSet;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class EditorPaneProcessor extends JPanel
        implements Printable, Doc {

    private DocFlavor flavour;
    private DocAttributeSet attr;
    //    private Font            headerFont;
//    private Font            tableFont;
//    private FontMetrics     fHeaderMetrics;
    private FontMetrics fContentMetrics;
    private Object[] objects;

    private int panelWidth = 800;        // In Pixels
    private int panelHeight = 600;        // In Pixels
    private int leftMargin = 5;
    private int topMargin = 5;
    //    private int         headerHeight    = 82;       //100; //110;
    private int headingHeight = 33;
    private int footerHeight = 50;
    private int tableHeaderHeight = 20;
    private int footerAllowance = 35;
    private int rowHeight = 0;
    private int noOfRows = 0;        // Total No of Rows in the Table
    private int noOfRowsPerPage = 0;        // No of Rows per a Page
    private int totalNumPages = 0;        // Total no of Pages
    private int eachPageHeight = 0;
    //    private int         totalColumnWidth= 0;        // Cumulative widths for all the columns
//    private int         maxFieldWidth   = 0;
//    private int         tableType       = 0;
    private String sTitle;
    private String[] arrayContents;

    public EditorPaneProcessor(Object[] objects, String sTitle, int type) {
        try {
            this.sTitle = sTitle;
            this.objects = objects;
//            this.tableType = type;
            flavour = new DocFlavor("application/x-java-jvm-local-objectref", "java.awt.print.Printable");

            this.setBackground(PrintUtilities.pageBorderColor);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void initReport() {
        if (this.getSize().width > 0)
            panelWidth = this.getSize().width;// - ReportGenerator.getScrollBarWidth();
        if (this.getSize().height > 0)
            panelHeight = this.getSize().height;

        JEditorPane editorPane = (JEditorPane) objects[0];
        String sContent = null;
        try {
            sContent = (editorPane.getDocument()).getText(0, (editorPane.getDocument()).getLength());
        } catch (BadLocationException e) {
            sContent = "";
            e.printStackTrace();
        }
        //StringTokenizer st = new StringTokenizer(sContent, "\n", false);
        /*noOfRows = st.countTokens();
        arrayContents = new String[noOfRows];
        int counter = 0;
        while (st.hasMoreTokens()) {
            arrayContents[counter++] = st.nextToken();
        }*/

        sContent = sContent.replaceAll("\n", " \n");

        fContentMetrics = this.getFontMetrics(PrintUtilities.defaultFixedWidthFont);
        arrayContents = parseMessage(sContent);
        noOfRows = arrayContents.length;
        editorPane = null;
        sContent = null;
        //st          = null;


        rowHeight = fContentMetrics.getHeight() + fContentMetrics.getDescent() + 2;
        noOfRowsPerPage = getNofRowsPerPage();
        eachPageHeight = (noOfRowsPerPage * rowHeight) + headingHeight + footerHeight + footerAllowance;
    }

    private String[] parseMessage(String mesg) {
        int charWidth = 0;
        int strWidth = 0;
        Vector vecWords = null;
        ArrayList<String> sentences = new ArrayList<String>();

        try {
            if ((mesg == null) || (mesg.length() == 0))
                return new String[0];
            strWidth = fContentMetrics.stringWidth(mesg);

            if (strWidth > panelWidth) {
                charWidth = fContentMetrics.charWidth('m');
                vecWords = getWords(mesg);
                int wordLength = 0;
                int sentenceLength = 0;
                String word = null;
                String sentence = "";
                for (int i = 0; i < vecWords.size(); i++) {
                    word = (String) vecWords.elementAt(i);
                    wordLength = fContentMetrics.stringWidth(word); // word.length() * charWidth;
                    if (word.endsWith("\n")) {
                        sentences.add(sentence);
                        sentenceLength = 0;
                        sentence = "";

                        sentence += " " + word;
                        sentenceLength += wordLength + charWidth;
                        wordLength = 0;
                        word = null;
                    } else if ((sentenceLength + wordLength) <= panelWidth) {
                        sentence += " " + word;
                        sentenceLength += wordLength + charWidth; // Leave room for additional white space
                    } else {
                        sentences.add(sentence);
                        sentenceLength = 0;
                        sentence = "";

                        sentence += " " + word;
                        sentenceLength += wordLength + charWidth;
                        wordLength = 0;
                        word = null;
                    }
                }
                sentences.add(sentence);
                word = null;
                sentence = null;
            } else {

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sentences.toArray(new String[0]);
    }

    private Vector getWords(String mesg) {
        Vector words = new Vector();
        StringTokenizer st = new StringTokenizer(mesg, " ", false);
        while (st.hasMoreElements()) {
            words.addElement(st.nextToken());
        }
        st = null;
        return words;
    }

    public void paint(Graphics g) {
        int startRow = 0;
        int lastRow = 0;
        int pageNo = 1;
        int curYPos = 0;

        try {
            while (startRow < noOfRows) {
                if ((startRow + noOfRowsPerPage) < noOfRows)
                    lastRow = startRow + noOfRowsPerPage; // - 1;
                else
                    lastRow = noOfRows; // - 1;
                drawReport(g, curYPos, panelWidth, eachPageHeight, startRow, lastRow, pageNo++, false);
                startRow += noOfRowsPerPage;
                curYPos += eachPageHeight;
            }
        } catch (Exception ex) {
        }
        startRow = 0;
    }

    public void update(Graphics g) {
        this.repaint();
    }

    private void drawReport(Graphics g, int startYPos, int width, int height, int startRow, int endRow, int pageNo, boolean isPrinting) {
        int curYPos = 0;
        int curXPos = 0;
        int curRow = startRow;
        int curCol = 0;
        Graphics2D g2D = null;

        try {
            g2D = (Graphics2D) g;

            if (!isPrinting) {
                g2D.setColor(PrintUtilities.pageBGColor);
                g2D.fillRect(0, startYPos, width, height + 10); //180);
            }
            g2D.setColor(PrintUtilities.pageFGColor);

            curYPos = topMargin + startYPos;
            headingHeight = PrintUtilities.drawHeading(sTitle, g2D, leftMargin, curYPos + 5, panelWidth, PrintUtilities.FONT_LARGE);
            curYPos += headingHeight + topMargin;
            // -------------------------------------------------------------------------------------
            //  Draw the Table Contents
            // -------------------------------------------------------------------------------------
//            FontMetrics fm          = g2D.getFontMetrics();
            int origYPos = curYPos;
            int origXPos = leftMargin;
            // Draw the Table Contents
            g2D.setFont(PrintUtilities.defaultFixedWidthFont);
//            fm        = g2D.getFontMetrics();
//            rowHeight = fm.getHeight() + fm.getDescent() + 2; // + 5; // + fm.getAscent() + fm.getDescent();
            for (int iRow = startRow; iRow < endRow; iRow++) {
                curXPos = origXPos; // + 2;
                curXPos = PrintUtilities.drawCell(arrayContents[iRow], false, PrintUtilities.LEFT,
                        g2D, curXPos, curYPos, panelWidth, rowHeight, false, true);
                curYPos += rowHeight;
            }
            origXPos = curXPos + 2;

            if (totalNumPages > pageNo) {
                PrintUtilities.drawFooter(g2D, leftMargin, curYPos + 5, panelWidth, footerHeight, pageNo, isPrinting);
                if (!isPrinting) {
                    curYPos += footerHeight + 10;
                    g2D.setStroke(PrintUtilities.BS_1p0f_dashed);
                    g.drawLine(0, curYPos, panelWidth, curYPos);
                }
            } else
                PrintUtilities.drawFooter(g2D, leftMargin, startYPos + eachPageHeight - footerHeight, panelWidth, footerHeight, pageNo, isPrinting);
//            PrintUtilities.drawFooter(g2D, leftMargin, startYPos+eachPageHeight-footerHeight, panelWidth, footerHeight, pageNo, isPrinting);
        } catch (Exception e) {
            e.printStackTrace();
        }
        g2D = null;
//        fm  = null;
    }

    public void revalidatePreview() {
        int totHeight = totalNumPages * eachPageHeight + 10;
        this.setPreferredSize(new Dimension(this.getSize().width, totHeight));
//System.out.println("" + this.getSize().width + ", " + totHeight + " totalNumPages=" + totalNumPages + " eachPageHeight=" + eachPageHeight);
        PrintManager.revalidateScrollPane();
    }

    /**
     * The method @print@ must be implemented for @Printable@ interface.
     * Parameters are supplied by system.
     */
    public int print(Graphics g, PageFormat pf, int pageIndex) throws
            PrinterException {

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black); //set default foreground color to black
        //for faster printing, turn off double buffering
        //RepaintManager.currentManager(this).setDoubleBufferingEnabled(false);

        Dimension d = this.getSize(); //get size of document
        double panelWidth2 = d.width; //width in pixels
        double panelHeight2 = d.height; //height in pixels

        if (panelWidth2 == 0)
            panelWidth2 = panelWidth;
        if (panelHeight2 == 0)
            panelHeight2 = panelHeight;

        double pageHeight = pf.getImageableHeight(); //height of printer page
        double pageWidth = pf.getImageableWidth(); //width of printer page

        double scale = pageWidth / panelWidth2;
        int rowPerPage = (int) ((pageHeight - headingHeight - footerHeight - tableHeaderHeight - footerAllowance) / (rowHeight * scale));
//        int totalNumPages = Math.max((int)Math.ceil(noOfRows/(double)rowPerPage), 1);
//        int totalNumPages = (int) Math.ceil(scale * panelHeight / pageHeight);
//System.out.println("Printing " + totalNumPages + " " + pageIndex);
        //make sure not print empty pages
        if (pageIndex >= totalNumPages) {
            return Printable.NO_SUCH_PAGE;
        }

        int iniRow = pageIndex * rowPerPage;
        int endRow = Math.min(noOfRows, iniRow + rowPerPage);
//System.out.println("rowPerPage = " + rowPerPage + " pageHeight =" + pageHeight + " pageWidth =" + pageWidth);

        //shift Graphic to line up with beginning of print-imageable region
        g2.translate(pf.getImageableX(), pf.getImageableY());

        //scale the page so the width fits...
        g2.scale(scale, scale);

        drawReport(g2, 0, (int) panelWidth2, (int) panelHeight2, iniRow, endRow, pageIndex + 1, true);

        d = null;
        g2 = null;

        return Printable.PAGE_EXISTS;
    }

    private int getNofRowsPerPage() {
        PageFormat pf = Settings.getPageFormat();
        Dimension d = this.getSize(); //get size of document
        double panelWidth2 = d.width; //width in pixels
        double panelHeight2 = d.height; //height in pixels

        if (panelWidth2 == 0)
            panelWidth2 = panelWidth;
        if (panelHeight2 == 0)
            panelHeight2 = panelHeight;

        double pageHeight = pf.getImageableHeight(); //height of printer page
        double pageWidth = pf.getImageableWidth(); //width of printer page

        double scale = pageWidth / panelWidth2;
        int rowPerPage = (int) ((pageHeight - headingHeight - footerHeight - footerAllowance) / (rowHeight * scale));
        totalNumPages = Math.max((int) Math.ceil(noOfRows / (double) rowPerPage), 1);
//System.out.println("totalNumPages = " + totalNumPages + " rowPerPage=" + rowPerPage);

        d = null;
        pf = null;
        return rowPerPage;
    }

    public DocFlavor getDocFlavor() {
        return flavour;
    }

    public Object getPrintData() throws IOException {
        return this;
    }

    public DocAttributeSet getAttributes() {
        return attr;
    }

    public Reader getReaderForText() throws IOException {
        return null;
    }

    public InputStream getStreamForBytes() throws IOException {
        return null;
    }

    public void disposeReport() {
//        headerFont      = null;
//        tableFont       = null;
//        fHeaderMetrics  = null;
        fContentMetrics = null;
        objects = null;
        sTitle = null;
        arrayContents = null;
    }

}