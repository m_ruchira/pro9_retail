package com.isi.csvr.print;

import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.theme.Theme;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.attribute.DocAttributeSet;
import javax.swing.*;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.swing.text.View;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Arrays;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class TableProcessor extends JPanel
        implements Printable, Doc {

    DocFlavor flavour;
    DocAttributeSet attr;

    private Font headerFont;
    private Font tableFont;
    private double scale;
    private FontMetrics fHeaderMetrics;
    private FontMetrics fTableMetrics;
    private Object[] objects;
    private int[] colWidths;                  // Holds the column widths for all the tables
    private float[] adjustedColWidths;          // Holds the Adjusted column widths for all the tables
    private Object[] colHeadings;

    private int panelWidth = 800;        // In Pixels
    private int panelHeight = 600;        // In Pixels
    private double pageHeight;
    private int leftMargin = 5;
    private int topMargin = 5;
    private int headerHeight = 82;       //100; //110;
    private int headingHeight = 33;
    private int footerHeight = 50;
    private int tableHeaderHeight = 20;
    private int footerAllowance = 35;
    private int rowHeight = 0;
    private int noOfRows = 0;        // Total No of Rows in the Table
    private int noOfRowsPerPage = 0;        // No of Rows per a Page
    private int totalNumPages = 0;        // Total no of Pages
    private int eachPageHeight = 0;
    private int totalColumnWidth = 0;        // Cumulative widths for all the columns
    private int maxFieldWidth = 0;
    private int tableType = 0;
    private String sTitle;
    private String text;
    private Document d;
    private boolean isFirstTime = false;

    private String[][] topStringArray;
    private int topStringCols;
    private int rowsInString = 0;
    private int rowsPrinted = 0;
    private int lastRow = 0;
    private int previousPage = -1;
    private boolean isHorizontalPrintmode = true;
//    private int         accessCounter   = 0;

    public TableProcessor(Object[] objects, String sTitle, int type, boolean printMode) {
        try {
            this.sTitle = sTitle;
            this.objects = objects;
            this.tableType = type;
            this.isHorizontalPrintmode = printMode;
            flavour = new DocFlavor("application/x-java-jvm-local-objectref", "java.awt.print.Printable");

//            this.accessCounter = 0;
//            isFirstTime = true;
            this.setBackground(PrintUtilities.pageBorderColor);
//            initReport();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String[][] getTopString() {
        return topStringArray;
    }

    public void setTopString(String[][] str) {
        if (str != null) {
            isFirstTime = true;
            topStringArray = str;
            this.topStringCols = (str[0].length) / 2;
            rowsInString = str.length + 1;
        }
    }

    public void initReport() {
        System.out.println("TableProcessor - initReport()");
        int noOfColumns = 0;
        int counter = 0;
        JTable table = null;
        isFirstTime = false;

        totalColumnWidth = 0;
        colWidths = null;
        adjustedColWidths = null;
        if (this.getSize().width > 0)
            panelWidth = this.getSize().width;// - ReportGenerator.getScrollBarWidth();
        if (this.getSize().height > 0)
            panelHeight = this.getSize().height;
        maxFieldWidth = panelWidth / 5;

        for (int i = 0; i < objects.length; i++) {
            noOfColumns += ((JTable) objects[i]).getColumnCount();
        }
        colHeadings = new Object[objects.length];  // Create the Column Headings Array
        colWidths = new int[noOfColumns];        // Create the Original Column Widths Array
        adjustedColWidths = new float[noOfColumns];        // Create the Adjusted Col Widths Array
        int[] tempWidths = null;
        for (int i = 0; i < objects.length; i++) {
            table = ((JTable) objects[i]);
            tempWidths = adjustColumnWidthsToFit(table, i);     // Calculate the Column widths for the Table
            if (noOfRows < (table.getRowCount()))
                noOfRows = table.getRowCount();
            for (int j = 0; j < tempWidths.length; j++) {
                colWidths[counter++] = tempWidths[j];
                totalColumnWidth += tempWidths[j];
            }
            table = null;
            tempWidths = null;
        }

        switch (tableType) {
            case PrintManager.TABLE_TYPE_WATCHLIST:
            case PrintManager.TABLE_TYPE_MIST_VIEW:
                noOfRows--;
                break;
        }
        if (panelWidth < totalColumnWidth) {
            boolean isWidthRestricted = false;
            for (int k = 0; k < colWidths.length; k++) {
                if (colWidths[k] > maxFieldWidth) {
                    colWidths[k] = maxFieldWidth;
                    isWidthRestricted = true;
                }
            }
            if (isWidthRestricted) {
                totalColumnWidth = leftMargin;
                for (int k = 0; k < colWidths.length; k++) {
                    totalColumnWidth += colWidths[k];
                }
            }
        } else {
            totalColumnWidth += leftMargin;
        }

        for (int k = 0; k < colWidths.length; k++) {
//            adjustedColWidths[k] = (colWidths[k] * panelWidth) / totalColumnWidth;
            adjustedColWidths[k] = colWidths[k];
        }

        Font font = Theme.getDefaultFont(Font.PLAIN, PrintUtilities.FONT_MEDIUM);
        FontMetrics fm = this.getFontMetrics(font);
        rowHeight = fm.getHeight() + fm.getDescent() + 2; // + 5; // + fm.getAscent() + fm.getDescent();

        noOfRowsPerPage = getNofRowsPerPage();
        if (tableType == PrintManager.TABLE_TYPE_MIST_VIEW)
            eachPageHeight = (noOfRowsPerPage * rowHeight) + headingHeight + footerHeight + footerAllowance + tableHeaderHeight * 3; // + noOfRowsPerPage / 2;
        else
            eachPageHeight = (noOfRowsPerPage * rowHeight) + headingHeight + footerHeight + footerAllowance + tableHeaderHeight; //+ (rowsInString* rowHeight);
        // footerAllowance = 35 = footer adjustments
        font = null;
        fm = null;

    }

    public void revalidatePreview() {
        int totHeight = totalNumPages * eachPageHeight + 10 + (rowsInString * rowHeight);
        this.setPreferredSize(new Dimension(this.getSize().width, totHeight));
        PrintManager.revalidateScrollPane();
    }

    private int[] adjustColumnWidthsToFit(JTable table, int tableIndex) {
        int model;
        int[] colWidths = new int[table.getColumnCount()];
        String colHead[] = new String[table.getColumnCount()];
        colHeadings[tableIndex] = colHead;

        headerFont = Theme.getDefaultFont(Font.PLAIN, PrintUtilities.FONT_MEDIUM);
        tableFont = Theme.getDefaultFont(Font.PLAIN, PrintUtilities.FONT_MEDIUM);

        Arrays.fill(colWidths, 0);
        int spacing = (int) table.getIntercellSpacing().getWidth();

        JLabel comp = null;
        try {
            comp = (JLabel) (table.getCellRenderer(0, 0).getTableCellRendererComponent(
                    table, table.getModel().getValueAt(0, 0), false, false, 0, 0));
        } catch (Exception e) {
            comp = new JLabel("");
        }
        Insets insets = comp.getInsets();
        int gap = insets.left + insets.right;
        //insets = null;

        SortButtonRenderer headerRenderer = (SortButtonRenderer) table.getColumnModel().getColumn(0).getHeaderRenderer();
        fHeaderMetrics = table.getFontMetrics(headerFont); //headerRenderer.getFont());
        gap += 15;

        if (tableType == PrintManager.TABLE_TYPE_MIST_VIEW) {
            colHead = Language.getList("COL_MIST_VIEW");
        }

        String colName = "";
        for (int column = 0; column < colWidths.length; column++) {
            try {
                if (tableType == PrintManager.TABLE_TYPE_MIST_VIEW)
                    colName = colHead[column];
                else
                    colName = table.getColumnName(column);

                if ((fHeaderMetrics.stringWidth(colName) + gap) > colWidths[column]) {
                    //colWidths[column] = fHeaderMetrics.stringWidth(colName) + gap;
                    if (tableType != PrintManager.TABLE_TYPE_MIST_VIEW)
                        colHead[column] = colName;
                }
                colName = null;
            } catch (Exception ex) {
            }
        }
        headerRenderer = null;


        fTableMetrics = table.getFontMetrics(tableFont); //table.getFont());
        gap = insets.left + insets.right + 2;


        for (int column = 0; column < table.getColumnCount(); column++) {
            TableColumn col = table.getColumnModel().getColumn(column);
            if (col.getWidth() > 0) {
                colWidths[column] = col.getWidth() + gap; //fTableMetrics.stringWidth(comp.getText()) + gap;
            } else {
                colWidths[column] = 0;
            }
        }

        /*for (int row = 0; row < table.getRowCount(); row++) {
            for (int column = 0; column < colWidths.length; column++) {
                model = table.convertColumnIndexToModel(column);


                try {
                    comp = (JLabel) table.getCellRenderer(row, column).getTableCellRendererComponent(
                            table, table.getModel().getValueAt(row, model), false, false, row, column);
                } catch (Exception e) {
                    comp = new JLabel("");
                }
                try {
                    Icon icon = comp.getIcon();
                    if ((icon != null) && ((comp.getText().equals("")) || (comp.getText() == null))) {
                        // this is an image cell
                        if ((comp.getWidth() + gap) > colWidths[column]) {
                            colWidths[column] = comp.getWidth() + gap;
                        }
                    } else {
                        if ((fTableMetrics.stringWidth(comp.getText()) + gap) > colWidths[column]) {
                            if (comp.getWidth() > 0){
                                colWidths[column] = comp.getWidth() + gap; //fTableMetrics.stringWidth(comp.getText()) + gap;
                            } else {
                                colWidths[column] = 0;
                            }
                        }
                    }
                    icon = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }*/
        /*for (int column = 0; column < colWidths.length; column++) {
            if (table.getColumnModel().getColumn(column).getPreferredWidth() == 0)
                colWidths[column] = 0;
        }*/

        insets = null;
        comp = null;
        headerRenderer = null;

        return colWidths;
    }

    public void paint(Graphics g) {
        int startRow = 0;
        int lastRow = 0;
        int pageNo = 1;
        int curYPos = 0;

        try {
//            g.drawImage(bufferImage, 0, 0, panelWidth, panelHeight, this);
//            drawReport(g, panelWidth, panelHeight, 0, noOfRows-1, 1, false);
            while (startRow < noOfRows) {
                if ((startRow + noOfRowsPerPage) < noOfRows)
                    lastRow = startRow + noOfRowsPerPage; // - 1;
                else
                    lastRow = noOfRows; // - 1;
                //Rectangle rect = PrintManager.getScrollPane().getViewport().getViewRect();
                //g.setClip((int)rect.getX(),(int)rect.getY(),(int)rect.getWidth(),(int)rect.getHeight());
                //rect = null;
                drawReport(g, curYPos, panelWidth, eachPageHeight, startRow, lastRow, pageNo++, false);
                startRow += noOfRowsPerPage;
                curYPos += eachPageHeight;
            }
        } catch (Exception ex) {
        }
        startRow = 0;
    }

    public void update(Graphics g) {
        this.repaint();
    }

    private void drawReport(Graphics g, int startYPos, int width, int height, int startRow, int endRow, int pageNo, boolean isPrinting) {
        int curYPos = 0;
        int curXPos = 0;
        int curRow = startRow;
        int curCol = 0;
        int headerPos = 0;
        Graphics2D g2D = null;
        boolean isBoldFontRow = false;
        boolean isEvenRow = false;

        try {
            g2D = (Graphics2D) g;
            g2D.setColor(PrintUtilities.pageFGColor);

            curYPos = topMargin + startYPos;
            headingHeight = PrintUtilities.drawHeading(sTitle, g2D, leftMargin, curYPos + 5, panelWidth, PrintUtilities.FONT_LARGE);
            curXPos = PrintUtilities.drawHeading(sTitle, g2D, leftMargin, curYPos + 5, panelWidth, PrintUtilities.FONT_LARGE);
            curYPos += headingHeight + topMargin;

            if (pageNo == 1) {
                if (isFirstTime) {
                    int topStringHeight = 0;
                    for (int i = 0; i < topStringArray.length; i++) {
                        int k = 0;
                        for (int j = 0; j < topStringArray[i].length; j = j + 2) {
                            int alignment = Integer.parseInt(topStringArray[i][j]);
                            if ((alignment == JLabel.TRAILING) || (alignment == JLabel.RIGHT)) {
                                if (Language.isLTR()) {
                                    topStringHeight = PrintUtilities.drawHeading(topStringArray[i][j + 1], g2D, leftMargin - fTableMetrics.stringWidth(topStringArray[i][j + 1]) - 30 + panelWidth * (k + 1) / topStringCols, curYPos,
                                            fTableMetrics.stringWidth(topStringArray[i][j + 1]) + 30, PrintUtilities.FONT_MEDIUM);
                                } else {
                                    topStringHeight = PrintUtilities.drawHeading(topStringArray[i][j + 1], g2D, panelWidth * (topStringCols - k - 1) / topStringCols, curYPos,
                                            panelWidth / topStringCols, PrintUtilities.FONT_MEDIUM);
                                }
                            } else {
                                if (Language.isLTR()) {
                                    topStringHeight = PrintUtilities.drawHeading(topStringArray[i][j + 1], g2D, leftMargin + panelWidth * (k) / topStringCols, curYPos,
                                            panelWidth / topStringCols, PrintUtilities.FONT_MEDIUM);
                                } else {
                                    topStringHeight = PrintUtilities.drawHeading(topStringArray[i][j + 1], g2D, panelWidth * (topStringCols - k - 1) / topStringCols, curYPos,
                                            panelWidth / topStringCols, PrintUtilities.FONT_MEDIUM);
                                }
                            }
                            k++;
                        }
                        curYPos += topStringHeight + 5; // + topMargin;
                    }
                }
            }

            // -------------------------------------------------------------------------------------
            //  Draw the Table Contents
            // -------------------------------------------------------------------------------------
            int counter = 0;    // Added on 22-08-03
            JTable table = null;
            JLabel cellComp = null;
            String[] colHead = null;
            String[] tempColHead = null;
            int[] widths = null;
            FontMetrics fm = g2D.getFontMetrics();
            int curTableWidth = 0;
            int colCounter = 0;
            int colIndex = 0;
            int origYPos = curYPos;
            int origXPos = leftMargin;
            int xPosWhileByPassing = 0;
            headerPos = leftMargin;

            for (int i = 0; i < objects.length; i++) {
                if (!isHorizontalPrintmode) {
                    headerPos = leftMargin;
                    origXPos = leftMargin;
                    curYPos = origYPos + curYPos;
                } else {
                    curYPos = origYPos;
                }
                widths = null;
                colHead = null;
                colHead = (String[]) colHeadings[i];
                widths = new int[colHead.length];
                tempColHead = new String[colHead.length];
                table = ((JTable) objects[i]);

                // _____________________________________________________________________________________________________
                for (int j = 0; j < colHead.length; j++) {
                    widths[j] = (int) adjustedColWidths[colCounter++];

                    // Calculate the Current Table Width
                    curTableWidth += widths[j];

                    if (Language.isLTR()) {
                        tempColHead[j] = colHead[j];
                    } else {
                        tempColHead[colHead.length - j - 1] = colHead[j];
                    }
                }
                colHead = tempColHead;
                // _____________________________________________________________________________________________________

                // Draw the Table Header
                if (tableType == PrintManager.TABLE_TYPE_MIST_VIEW)
                    curYPos += PrintUtilities.drawMISTTableHeader(widths, Language.getList("COL_MIST_VIEW"),
                            g2D, headerPos, curYPos, width, isPrinting);
                else if ((tableType != PrintManager.TABLE_TYPE_DEFAULT_NO_HEAD) &&
                        (tableType != PrintManager.TABLE_TYPE_SNAP_QUOTE) && (tableType != PrintManager.TABLE_TYPE_DEPTH_CALC)) {
                    int[] curPos = PrintUtilities.drawTableHeader(widths, colHead, g2D, headerPos, curYPos, width, isPrinting);
                    curYPos += curPos[1];
                    curXPos = curPos[0];
                }

                // Draw the Table Contents
                g2D.setFont(PrintUtilities.defaultPlainFont);
                fm = g2D.getFontMetrics();
                rowHeight = fm.getHeight() + fm.getDescent() + 2; // + 5; // + fm.getAscent() + fm.getDescent();
                for (int iRow = startRow; iRow < endRow; iRow++) {
                    curXPos = origXPos; // + 2;

                    int noOfCols = table.getColumnCount();
                    int iCol = 0;
                    boolean continueLoop = true;
                    if (Language.isLTR()) {
                        iCol = 0;
                    } else {
                        iCol = noOfCols - 1;
                    }
                    while (continueLoop) {
                        if (widths[iCol] > 0) {

                            g2D.setColor(PrintUtilities.pageFGColor);

                            colIndex = table.convertColumnIndexToModel(iCol);
                            try {
                                cellComp = (JLabel) table.getCellRenderer(iRow, iCol).getTableCellRendererComponent(
                                        table, table.getModel().getValueAt(iRow, colIndex), false, false, iRow, iCol);
                            } catch (Exception e) {
                                cellComp = new JLabel("");
                            }
                            Icon icon = (Icon) cellComp.getIcon();

                            View v = (View) cellComp.getClientProperty(BasicHTML.propertyKey);
                            if (v != null) {

                                d = v.getDocument();
//                                System.out.println("new"+d);
                                text = d.getText(0, d.getLength()).trim();

                                //sContent = (editorPane.getDocument()).getText(0,(editorPane.getDocument()).getLength());
                            } else {
                                text = cellComp.getText();
                            }

                            // String text = cellComp.getText();
                            if (((text == null) || (text.equals(""))) && (icon != null)) {
                                curXPos = PrintUtilities.drawCell(icon, true, cellComp.getHorizontalAlignment(),
                                        g2D, curXPos, curYPos, widths[iCol], rowHeight, true);
                            } else {
                                if (tableType == PrintManager.TABLE_TYPE_SNAP_QUOTE) {
                                    if (iRow == 9) {
                                        if (iCol == 3) {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[0] + widths[1] + widths[2] + widths[3], rowHeight, true);
                                        }
                                    } else if (iRow == 10) {
                                        if (iCol == 1) {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[0] + widths[1], rowHeight, true);
                                        } else if (iCol == 3) {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[2] + widths[3], rowHeight, true);
                                        }
                                    } else {
                                        curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                g2D, curXPos, curYPos, widths[iCol], rowHeight, true);
                                    }
                                } else if (tableType == PrintManager.TABLE_TYPE_MIST_VIEW) {
                                    isEvenRow = (counter % 2) == 0;
                                    if ((iCol == 1) && isEvenRow) {
                                        if (Language.isLTR())
                                            iCol++;
                                        else
                                            iCol--;
                                        continue;
                                    }
                                    if (colIndex == 0) {
                                        if (!isEvenRow)
                                            isBoldFontRow = false;
                                        else
                                            isBoldFontRow = true;
                                    } else {
                                        isBoldFontRow = false;
                                    }
                                    if (isBoldFontRow) {
                                        g2D.setFont(PrintUtilities.defaultBoldFont);
                                        // (String)table.getModel().getValueAt(iRow, 1)
                                        curXPos = PrintUtilities.drawCell((String) table.getModel().getValueAt(iRow, 1), false, cellComp.getHorizontalAlignment(),
                                                g2D, curXPos, curYPos, widths[iCol] + widths[iCol + 1], rowHeight, true);

//                                            curXPos = PrintUtilities.drawCell(cellComp.getText(), false, cellComp.getHorizontalAlignment(),
//                                                                    g2D, curXPos, curYPos, widths[iCol]+widths[iCol+1], rowHeight, true);
                                        g2D.setFont(PrintUtilities.defaultPlainFont);
                                    } else {
                                        curXPos = PrintUtilities.drawCell(text, false,
                                                cellComp.getHorizontalAlignment(),
                                                g2D, curXPos, curYPos, widths[iCol],
                                                rowHeight, true);
                                    }
                                    cellComp = null;
                                } else if (tableType == PrintManager.TABLE_TYPE_DEPTH_CALC) {
                                    if (iRow == 0) {
                                        if (iCol == 1) {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[0] + widths[1], rowHeight, true);
                                        } else if (iCol == 3) {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[2] + widths[3], rowHeight, true);
                                        }
                                    } else if ((iRow == 5) || (iRow == 11)) { // || (iRow == 8)) {
                                        if (iCol == 1) {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[0] + widths[1], rowHeight, true);
                                        } else if (iCol == 3) {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[2] + widths[3], rowHeight, true);
                                        }
                                    } else {
                                        curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                g2D, curXPos, curYPos, widths[iCol], rowHeight, true);
                                    }
                                } else {
                                    if (tableType == PrintManager.TABLE_TYPE_DEPTH_VIEWS) {
                                        if ((iCol == 0)) { // || (iCol == 1) || (iCol == 2)) {
                                            if (SharedMethods.doubleValue(text, 0) == 0) {
                                                // TODO - Has to check for the correctness of the print out
                                                origXPos = curTableWidth + 4; //widths.length * 2; //xPosWhileByPassing + 2;
                                                continueLoop = false;
                                            } else {
                                                curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                        g2D, curXPos, curYPos, widths[iCol], rowHeight, true);
                                                xPosWhileByPassing = curXPos;
                                            }
                                        } else {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[iCol], rowHeight, true);
                                        }
                                    } else {
                                        if (widths[iCol] > 0) {
                                            curXPos = PrintUtilities.drawCell(text, false, cellComp.getHorizontalAlignment(),
                                                    g2D, curXPos, curYPos, widths[iCol], rowHeight, true);
                                        }
                                    }
                                }
                            }
                        }

                        if (Language.isLTR()) {
                            iCol++;
                            if (iCol == noOfCols)
                                continueLoop = false;
                        } else {
                            iCol--;
                            if (iCol < 0)
                                continueLoop = false;
                        }
                        //icon = null;
                        text = null;
                        cellComp = null;
                        //v = null;
                        d = null;
                    }
                    counter++;
                    if ((tableType == PrintManager.TABLE_TYPE_MIST_VIEW) && !isEvenRow)
                        curYPos += rowHeight + 1;
                    else
                        curYPos += rowHeight;
                }
                origXPos = curXPos + 2;
                headerPos = curXPos + 2;

                table = null;
                cellComp = null;
                colHead = null;
                widths = null;
                fm = null;
            }

            if (totalNumPages > pageNo) {
//                PrintUtilities.drawFooter(g2D, leftMargin, curYPos + 5, panelWidth, footerHeight, pageNo, isPrinting);
                PrintUtilities.drawFooter(g2D, leftMargin, (int) pageHeight - footerHeight, panelWidth, footerHeight, pageNo, isPrinting);
                if (!isPrinting) {
                    curYPos += footerHeight + 10;
                    g2D.setStroke(PrintUtilities.BS_1p0f_dashed);
                    g.drawLine(0, curYPos, panelWidth, curYPos);
                }
            } else
                //((Graphics2D)g).scale(1/scale,1/scale);
                PrintUtilities.drawFooter(g2D, leftMargin, (int) pageHeight - footerHeight, panelWidth, footerHeight, pageNo, isPrinting);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The method @print@ must be implemented for @Printable@ interface.
     * Parameters are supplied by system.
     */
    public int print(Graphics g, PageFormat pf, int pageIndex) throws
            PrinterException {

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black); //set default foreground color to black
        //for faster printing, turn off double buffering
        //RepaintManager.currentManager(this).setDoubleBufferingEnabled(false);

        Dimension d = this.getSize(); //get size of document
        double panelWidth2 = d.width; //width in pixels
        double panelHeight2 = d.height; //height in pixels

        if (panelWidth2 == 0)
            panelWidth2 = panelWidth;
        if (panelHeight2 == 0)
            panelHeight2 = panelHeight;

        pageHeight = pf.getImageableHeight(); //height of printer page
        double pageWidth = pf.getImageableWidth(); //width of printer page

        if (pageWidth < totalColumnWidth) {
            scale = pageWidth / totalColumnWidth;
        } else {
            scale = 1;
        }

        int rowPerPage = 0;
        if (tableType == PrintManager.TABLE_TYPE_MIST_VIEW) {
            if (pageIndex == 0) {
                rowPerPage = (int) ((pageHeight - headingHeight - footerHeight - tableHeaderHeight * 3 - footerAllowance - (rowsInString * rowHeight)) / (rowHeight * scale));
            } else {
                rowPerPage = (int) ((pageHeight - headingHeight - footerHeight - tableHeaderHeight * 3 - footerAllowance) / (rowHeight * scale));
            }
        } else {
            if (pageIndex == 0) {
                rowPerPage = (int) ((pageHeight - headingHeight - footerHeight - tableHeaderHeight - footerAllowance - (rowsInString * rowHeight)) / (rowHeight * scale));
            } else {
                rowPerPage = (int) ((pageHeight - headingHeight - footerHeight - tableHeaderHeight - footerAllowance) / (rowHeight * scale));
            }
        }
        //make sure not print empty pages
        if (pageIndex >= totalNumPages) {
            return Printable.NO_SUCH_PAGE;
        }

        int rows = rowPerPage;
//        if(previousPage!= pageIndex){
//            rows += rowsPrinted; //rowsPrinted + rowPerPage
        if (previousPage == pageIndex) {
            if (lastRow >= rowsPrinted) {
                rowsPrinted = 0;
            } else {
                rowsPrinted = rowsPrinted - lastRow;
            }
        }
        int endRow = Math.min(noOfRows, rows + rowsPrinted);

        g2.translate(pf.getImageableX(), pf.getImageableY());

        //shift Graphic to line up with beginning of next page to print
        //g2.translate(0f, -pageIndex * pageHeight);

        //scale the page so the width fits...
        g2.scale(scale, scale);
        if ((pageIndex == 0) || (rowsPrinted != endRow))
            drawReport(g2, 0, totalColumnWidth, (int) panelHeight2, rowsPrinted, endRow, pageIndex + 1, true);
        g2 = null;
        lastRow = endRow - rowsPrinted;
        rowsPrinted = endRow;
        if (previousPage != pageIndex) {
            previousPage = pageIndex;
//            lastRow = endRow;
        }
        return Printable.PAGE_EXISTS;
    }

    private int getNofRowsPerPage() { //Graphics g, PageFormat pf, int pageIndex) {

        PageFormat pf = Settings.getPageFormat();
        //Dimension d = this.getSize(); //get size of document
        //double panelWidth2 = d.width; //width in pixels
        //double panelHeight2 = d.height; //height in pixels

        //if (panelWidth2 == 0)
        //    panelWidth2 = panelWidth;
        //if (panelHeight2 == 0)
        //    panelHeight2 = panelHeight;

        double pageHeight = pf.getImageableHeight(); //height of printer page
        double pageWidth = pf.getImageableWidth(); //width of printer page

        double scale = pageWidth / totalColumnWidth;
        int rowPerPage = 0;
        if (tableType == PrintManager.TABLE_TYPE_MIST_VIEW)
            rowPerPage = (int) ((pageHeight - headingHeight - footerHeight - tableHeaderHeight * 3 - footerAllowance) / (rowHeight * scale));
        else
            rowPerPage = (int) ((pageHeight - headingHeight - footerHeight - tableHeaderHeight - footerAllowance) / (rowHeight * scale));
        totalNumPages = Math.max((int) Math.ceil((noOfRows + rowsInString / scale) / (double) rowPerPage), 1);

        return rowPerPage;
    }

    public void disposeReport() {
        headerFont = null;
        tableFont = null;
        fHeaderMetrics = null;
        fTableMetrics = null;
        objects = null;
    }

    public DocFlavor getDocFlavor() {
        return flavour;
    }

    public Object getPrintData() throws IOException {
        return this;
    }

    public DocAttributeSet getAttributes() {
        return attr;
    }

    public Reader getReaderForText() throws IOException {
        return null;
    }

    public InputStream getStreamForBytes() throws IOException {
        return null;
    }
}