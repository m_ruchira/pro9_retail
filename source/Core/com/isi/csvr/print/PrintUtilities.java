package com.isi.csvr.print;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class PrintUtilities {

    public static final int LEFT = JLabel.LEFT; // 1;
    public static final int CENTER = JLabel.CENTER; // 2;
    public static final int RIGHT = JLabel.RIGHT; // 3;

    public static final int UP = 1;
    public static final int DOWN = 2;
    public static final int NOCHANGE = 3;

    public static final int FONT_SMALL = 12; //12;
    public static final int FONT_MEDIUM = 14; //14;
    public static final int FONT_LARGE = 18; //18;
    public static final int FONT_LARGEST = 20; //20;
    public static final int FONT_EXTREME = 28;

//    public static final int FONT_PRINT_SMALL  = 12; //15; //14;
//    public static final int FONT_PRINT_MEDIUM = 14; //17; //16;
//    public static final int FONT_PRINT_LARGE  = 18; //20; //20;
//    public static final int FONT_PRINT_LARGEST= 20; //22; //22;
//    public static final int FONT_PRINT_EXTREME= 28; //28; //26;

    public static final BasicStroke BS_1p8f_solid = new BasicStroke(1.8f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final BasicStroke BS_1p5f_solid = new BasicStroke(1.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final BasicStroke BS_1p0f_solid = new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final BasicStroke BS_0p8f_solid = new BasicStroke(0.8f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final BasicStroke BS_0p5f_solid = new BasicStroke(0.5f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public static final float[] dashArr = {10, 5};
    public static final BasicStroke BS_1p0f_dashed = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND,
            10f, dashArr, 0);
    //    public static Color     upColor             = Color.green.darker();
//    public static Color     downColor           = Color.red;
//    public static Color     normalColor         = Color.black;
//    public static Color     evenRowBGColor      = new Color(246, 246, 246);
//    public static Color     oddRowBGColor       = new Color(255, 255, 255);
//    public static Color     evenRowFGColor      = Color.black;
//    public static Color     oddRowFGColor       = Color.black;
//    public static Color     gridColor           = Color.black;
//    public static Color     headerBGColor       = new Color(167, 193, 213); //Color.black;
//    public static Color     headerFGColor       = Color.black;
//    public static Color     headingBGColor      = new Color(167, 193, 213); //Color.black;
//    public static Color     headingFGColor      = Color.black;
    public static Color pageBorderColor = Color.gray;
    public static Color pageBGColor = Color.white;
    public static Color pageFGColor = Color.black;
    public static Color footerFGColor = Color.gray;
    public static Font defaultPlainFont;
    public static Font defaultBoldFont;
    public static Font defaultFixedWidthFont;
    private static SimpleDateFormat dateTimeFormat = new SimpleDateFormat(" dd/MM/yyyy - HH:mm:ss ");

    //private static  Image    uniQuotesLogo;
//    private static  Image    companyLogo;
//    private static  boolean  isCustomizedVersion     = false;
//    private static  boolean  isUniQuotesLogoExists   = false;

//    public static boolean isCustomizedVersion() {
//        return isCustomizedVersion;
//    }

    public static void loadImages(PrintManager pm) {
        File file = null;
        MediaTracker comp = null;
        try {
            defaultFixedWidthFont = new TWFont("Courier", Font.PLAIN, PrintUtilities.FONT_LARGEST);
            defaultPlainFont = Theme.getDefaultFont(Font.PLAIN, PrintUtilities.FONT_MEDIUM);
            defaultBoldFont = Theme.getDefaultFont(Font.BOLD, PrintUtilities.FONT_MEDIUM);

//            file = new File(".\\images\\Common\\print_footer_"+Language.getLanguageTag()+".gif");

//            companyLogo = Toolkit.getDefaultToolkit().getImage(".\\images\\Common\\companyLogo1.gif");
//            uniQuotesLogo = Toolkit.getDefaultToolkit().getImage(".\\images\\Common\\print_footer_"+Language.getLanguageTag()+".gif");
            comp = new MediaTracker(pm);
//            comp.addImage(companyLogo, 1);
//            comp.addImage(uniQuotesLogo, 0);
            comp.waitForID(0);
//            comp.waitForID(1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        file = null;
    }

    public static int drawHeading(String heading, Graphics2D g2D, int x, int yPos, int width, int fontSize) {
        int xPos = 0;
        FontMetrics fm = null;
        String sTitle = null;

        try {
            g2D.setFont(Theme.getDefaultFont(Font.BOLD, fontSize));
            fm = g2D.getFontMetrics();
            yPos += fm.getAscent();
            if (Language.isLTR()) {
                xPos = x; // + offset;
            } else {
                xPos = x + width - fm.stringWidth(heading);
            }
            g2D.drawString(heading, xPos, yPos);

//            g2D.setColor(footerFGColor);
            g2D.setFont(defaultPlainFont); //Theme.getDefaultFont(Font.PLAIN, FONT_MEDIUM));
            fm = g2D.getFontMetrics();

            sTitle = "";//Language.getString("APPLICATION_NAME");
            if (Language.isLTR()) {
                xPos = width - fm.stringWidth(sTitle) - 10;
            } else {
                xPos = x; // + offset;
            }
//            g2D.drawString(sPage, xPos, yPos);
//            g2D.setColor(pageFGColor);
            g2D.drawString(sTitle, xPos, yPos);
//            g2D.setStroke(BS_0p5f_solid);
//            g2D.drawLine(x, yPos + 10, width-10, yPos + 10);
//            g2D.setColor(pageFGColor);
        } catch (Exception ex) {
        }
        return fm.getHeight() + fm.getDescent() + fm.getDescent() + 10;
    }

/*      Original Method - 2003/03/04
        public static int drawHeading(String heading, Graphics2D g2D, int midX, int yPos, int fontSize) {
            int xPos = 0;
            FontMetrics fm = null;
            try {
                g2D.setFont(Theme.getDefaultFont(Font.BOLD, fontSize));
                fm = g2D.getFontMetrics();
                yPos += fm.getAscent();
                xPos = midX - fm.stringWidth(heading)/2;
                g2D.drawString(heading, xPos, yPos);
            } catch (Exception ex) {
            }
            return fm.getHeight() + fm.getDescent() + fm.getDescent();
        }
*/

    public static int drawCell(Object data, boolean imageType, int alignment, Graphics2D g2D, int x, int y, int width, int height,
                               boolean isBorderEnable) {
        return drawCell(data, imageType, alignment, g2D, x, y, width, height, isBorderEnable, false);
    }

    public static int drawCell(Object data, boolean imageType, int alignment, Graphics2D g2D, int x, int y, int width, int height,
                               boolean isBorderEnable, boolean isDoNotTrim) {
        int xPos = x;
        int hGap = 5;

        FontMetrics fm = g2D.getFontMetrics();
        if (isBorderEnable) {
            g2D.setStroke(BS_0p5f_solid);
            g2D.drawRect(x, y, width, height);
        }

        if (data == null)
            return xPos + width;

        String processData = null;
        if (imageType) {
            if (width > 0) {
                /*xPos += (width - ((ImageIcon)data).getImage().getWidth(Client.getInstance()))/2;
                y += (height - ((ImageIcon)data).getImage().getHeight(Client.getInstance()))/2;*/
                xPos += (width - ((Icon) data).getIconWidth()) / 2;
                y += (height - ((Icon) data).getIconHeight()) / 2;
                //g2D.drawImage(((ImageIcon)data).getImage(), xPos, y,Client.getInstance());

                if ((width - hGap) < ((Icon) data).getIconWidth()) {
                    float scale = (float) (width - hGap) / (float) ((Icon) data).getIconWidth();
                    g2D.scale(scale, 1);
//                    ((Icon)data).paintIcon(null,g2D,(int)(xPos+hGap/2),y);
                    ((Icon) data).paintIcon(null, g2D, (int) (Math.ceil((x) / scale)), y);
                    g2D.scale(1 / scale, 1);
                } else {
                    ((Icon) data).paintIcon(null, g2D, xPos, y);
                }
            }
        } else {
            y += g2D.getFontMetrics().getAscent() + 5;
            if (!isDoNotTrim)
                processData = ((String) data).trim();
            else
                processData = (String) data;
            if ((width - hGap) < fm.stringWidth(processData)) {
                processData = truncateColumnData(fm, width - hGap, processData);
            }

            switch (alignment) {
                case LEFT:
                    xPos += hGap;
                    break;
                case CENTER:
                    xPos += (width - fm.stringWidth(processData)) / 2;
                    break;
                case RIGHT:
                    xPos += (width - fm.stringWidth(processData) - hGap);
                    break;
            }
            g2D.drawString(processData, xPos, y);
        }

        processData = null;
        fm = null;
        return x + width;
    }

    private static String truncateColumnData(FontMetrics fMetrics, int maxWidth, String dataStr) {
        StringBuffer holder = new StringBuffer();
        char[] chars = dataStr.toCharArray();
        for (int count = 0; count < dataStr.length(); count++) {
//            if ((fMetrics.stringWidth(holder + chars[count] + "") >= (maxWidth-8))) {
            if ((fMetrics.stringWidth(holder.toString() + chars[count] + "") >= (maxWidth - 2))) {
//                holder += "...";
//                holder.append("...");
                break;
            }
//            holder += chars[count] + "";
            holder.append(chars[count]);
        }
        chars = null;
        return holder.toString();
    }


    public static int[] drawTableHeader(int[] widths, String[] header, Graphics2D g2D, int x, int y, int width, boolean isPrinting) {
        int xPos = x;
        int yPos = y;
        int height = 0;
        int[] curPos = new int[2];                  //gives the current positions [y,x]

        FontMetrics fm = null;
        try {
//            if (isPrinting)
//                g2D.setFont(Theme.getDefaultFont(Font.BOLD, FONT_PRINT_MEDIUM));
//            else {
            g2D.setFont(defaultBoldFont); //Theme.getDefaultFont(Font.BOLD, FONT_MEDIUM));
//            }
//            g2D.setColor(headerFGColor);
            fm = g2D.getFontMetrics();
            height = fm.getAscent() + fm.getHeight(); // + fm.getDescent();
//            System.out.println("height = " + height + " " + yPos);
            //yPos += height/2;
            for (int i = 0; i < widths.length; i++) {
                if (Language.isLTR()) {
                    drawCell(header[i], false, CENTER, g2D, xPos, yPos, widths[i], height, true);
//                    drawTableHeaderColumn(header[i], CENTER, g2D, xPos, yPos, widths[i], height, isPrinting);
                    xPos += widths[i];
                } else {
//                    drawCell(header[widths.length-1-i],false, CENTER, g2D, xPos, yPos, widths[i], height, true);
                    drawCell(header[i], false, CENTER, g2D, xPos, yPos, widths[widths.length - 1 - i], height, true);
//                drawTableHeaderColumn(header[widths.length-1-i], CENTER, g2D, xPos, yPos, widths[i], height, isPrinting);
                    xPos += widths[widths.length - 1 - i];
                }
            }
//            xPos = midX - fm.stringWidth(heading)/2;
//            g2D.drawString(heading, xPos, yPos);
        } catch (Exception ex) {
        }
        fm = null;
        curPos[1] = height;
        curPos[0] = xPos;
        return curPos;
        //return height;
    }

    public static int drawFooter(Graphics2D g2D, int x, int y, int width, int height, int pageNo, boolean isPrinting) {
        int midXPos = x + width / 2; // + 80;
        int xPos = 0;
        int xPos1 = 0;
        int offset = 20;
        int yPos = y + 10;
        int imgHeight = 0;
        int imgWidth = 0;
        FontMetrics fm = null;
        Date dateTime = null;
        String sDateTime = null;
        String sPage = null;

        try {
            g2D.setColor(footerFGColor);
            g2D.setFont(defaultPlainFont); //Theme.getDefaultFont(Font.PLAIN, FONT_MEDIUM));
            fm = g2D.getFontMetrics();

            dateTime = new Date(Settings.getLocalTimeFor(System.currentTimeMillis()));
            sDateTime = dateTimeFormat.format(dateTime);

            if (Language.isLTR()) {
                xPos = x + offset;
                xPos1 = width - fm.stringWidth(sDateTime) - offset;
            } else {
                xPos = width - fm.stringWidth(Language.getString("PRINT_FOOTER_CLIENT_TITLE")) - offset;
                xPos1 = x + offset;
            }

            if (Language.isLTR()) {
                sPage = Language.getString("PAGE") + " " + pageNo;
            } else {
                sPage = pageNo + " " + Language.getString("PAGE");
            }
            int textBase = yPos + height / 2;
            g2D.drawString(Language.getString("PRINT_FOOTER_CLIENT_TITLE"), xPos, textBase);
            // Print the Page Number
            g2D.drawString(sPage, midXPos, textBase);
            // Print the Current Date & Time
            g2D.drawString(sDateTime, xPos1, textBase);
            g2D.setColor(pageFGColor);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fm = null;
        dateTime = null;
        sDateTime = null;

        return yPos;
    }

    public static int drawMISTTableHeader(int[] widths, String[] header, Graphics2D g2D, int x, int y, int width, boolean isPrinting) {
        int xPos = x;
        int yPos = y;
        int height = 0;
        int set1Width = 0;
        int set2Width = 0;
        int set3Width = 0;
        int set4Width = 0;
        int headerXPos = 0;
        FontMetrics fm = null;

        try {
            g2D.setFont(defaultBoldFont); //Theme.getDefaultFont(Font.BOLD, FONT_MEDIUM));
            fm = g2D.getFontMetrics();
            height = fm.getAscent() + fm.getHeight(); // + fm.getDescent();
            set1Width = getGroupWidth(0, 2, widths);        //  DESCRIPTION
            set2Width = getGroupWidth(2, 5, widths);        //  COMBINED_TRADE
            set3Width = getGroupWidth(5, 8, widths);        //  ASK
            set4Width = getGroupWidth(8, 11, widths);       //  BID
            headerXPos = xPos;
            if (Language.isLTR()) {
                drawCell(Language.getString("DESCRIPTION"), false, CENTER, g2D, headerXPos, yPos, set1Width, height, true);
                headerXPos += set1Width;
            } else {
//                drawCell("BID", false, CENTER, g2D, headerXPos, yPos, set4Width, height, true);
                drawCell(Language.getString("BID"), false, CENTER, g2D, headerXPos, yPos, set4Width, height, true);
                headerXPos += set4Width;
            }
            if (Language.isLTR()) {
                drawCell(Language.getString("TRADE"), false, CENTER, g2D, headerXPos, yPos, set2Width, height, true);
                headerXPos += set2Width;
            } else {
                drawCell(Language.getString("ASK"), false, CENTER, g2D, headerXPos, yPos, set3Width, height, true);
                headerXPos += set3Width;
            }
            if (Language.isLTR()) {
                drawCell(Language.getString("ASK"), false, CENTER, g2D, headerXPos, yPos, set3Width, height, true);
                headerXPos += set3Width;
            } else {
                drawCell(Language.getString("TRADE"), false, CENTER, g2D, headerXPos, yPos, set2Width, height, true);
                headerXPos += set2Width;
            }
            if (Language.isLTR()) {
                drawCell(Language.getString("BID"), false, CENTER, g2D, headerXPos, yPos, set4Width, height, true);
                headerXPos += set4Width;
            } else {
//                drawCell("DESCRIPTION", false, CENTER, g2D, headerXPos, yPos, set1Width, height, true);
                drawCell(Language.getString("DESCRIPTION"), false, CENTER, g2D, headerXPos, yPos, set1Width, height, true);
                headerXPos += set1Width;
            }

            yPos += height;

            for (int i = 0; i < widths.length; i++) {
                if (Language.isLTR()) {
                    if (header[i].indexOf("/") > 0) {
                        drawCell(header[i].substring(0, header[i].indexOf("/")), false, CENTER, g2D, xPos, yPos, widths[i], height, true);
                        drawCell(header[i].substring(header[i].indexOf("/") + 1), false, CENTER, g2D, xPos, yPos + height, widths[i], height, true);
                    } else {
                        drawCell(header[i], false, CENTER, g2D, xPos, yPos, widths[i], height * 2, true);
                    }
                } else {
                    if (header[widths.length - 1 - i].indexOf("/") > 0) {
                        drawCell(header[widths.length - 1 - i].substring(0, header[widths.length - 1 - i].indexOf("/")), false, CENTER, g2D, xPos, yPos, widths[widths.length - 1 - i], height, true);
                        drawCell(header[widths.length - 1 - i].substring(header[widths.length - 1 - i].indexOf("/") + 1), false, CENTER, g2D, xPos, yPos + height, widths[widths.length - 1 - i], height, true);
                    } else {
                        drawCell(header[widths.length - 1 - i], false, CENTER, g2D, xPos, yPos, widths[widths.length - 1 - i], height * 2, true);
                    }
                }
//                if (header[i].indexOf("/") > 0) {
//                    if (Language.isLTR())
//                        drawCell(header[i].substring(0, header[i].indexOf("/")), false, CENTER, g2D, xPos, yPos, widths[i], height, true);
//                    else
//                        drawCell(header[i].substring(0, header[i].indexOf("/")), false, CENTER, g2D, xPos, yPos, widths[widths.length-1-i], height, true);
////                    yPos += height;
//
//                    if (Language.isLTR())
//                        drawCell(header[i].substring(header[i].indexOf("/")+1), false, CENTER, g2D, xPos, yPos+height, widths[i], height, true);
//                    else
//                        drawCell(header[i].substring(header[i].indexOf("/")+1), false, CENTER, g2D, xPos, yPos+height, widths[widths.length-1-i], height, true);
//                } else {
//                    if (Language.isLTR())
//                        drawCell(header[i], false, CENTER, g2D, xPos, yPos, widths[i], height*2, true);
//                    else
//                        drawCell(header[i], false, CENTER, g2D, xPos, yPos, widths[widths.length-1-i], height*2, true);
//                }
//                if (Language.isLTR())
//                    drawCell(header[i], CENTER, g2D, xPos, yPos, widths[i], height, true);
//                else
//                    drawCell(header[widths.length-1-i], CENTER, g2D, xPos, yPos, widths[i], height, true);
                if (Language.isLTR())
                    xPos += widths[i];
                else
                    xPos += widths[widths.length - 1 - i];
            }
/*
            for (int i = 0; i < widths.length; i++) {
                if (header[i].indexOf("/") > 0) {
                    if (Language.isLTR())
                        drawCell(header[i].substring(0, header[i].indexOf("/")), false, CENTER, g2D, xPos, yPos, widths[i], height, true);
                    else
                        drawCell(header[widths.length-1-i].substring(0, header[widths.length-1-i].indexOf("/")), false, CENTER, g2D, xPos, yPos, widths[i], height, true);
//                    yPos += height;

                    if (Language.isLTR())
                        drawCell(header[i].substring(header[i].indexOf("/")+1), false, CENTER, g2D, xPos, yPos+height, widths[i], height, true);
                    else
                        drawCell(header[widths.length-1-i].substring(header[widths.length-1-i].indexOf("/")+1), false, CENTER, g2D, xPos, yPos+height, widths[i], height, true);
                } else {
                    if (Language.isLTR())
                        drawCell(header[i], false, CENTER, g2D, xPos, yPos, widths[i], height*2, true);
                    else
                        drawCell(header[widths.length-1-i], false, CENTER, g2D, xPos, yPos, widths[i], height*2, true);
                }
//                if (Language.isLTR())
//                    drawCell(header[i], CENTER, g2D, xPos, yPos, widths[i], height, true);
//                else
//                    drawCell(header[widths.length-1-i], CENTER, g2D, xPos, yPos, widths[i], height, true);
                xPos += widths[i];
            }

*/
        } catch (Exception ex) {
        }
        fm = null;
        return height * 3;
    }

    private static int getGroupWidth(int startIndex, int endIndex, int[] widths) {
        int width = 0;
        for (int i = startIndex; i < endIndex; i++) {
            width += widths[i];
        }
        return width;
    }

//    public static int drawCell(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height,
//                               boolean isBorderEnable, boolean isDoNotTrim) {
//        int xPos    = x;
//        int hGap    = 5;
//
//        FontMetrics fm = g2D.getFontMetrics();
//        if (isBorderEnable) {
//            g2D.setStroke(BS_0p5f_solid);
//            g2D.drawRect(x, y, width, height);
//        }
//
//        y += g2D.getFontMetrics().getAscent() + 5;
//
//        if (data == null)
//            return xPos + width;
//
//        String processData = null;
//        if (!isDoNotTrim)
//            processData = data.trim();
//        else
//            processData = data;
//        if ((width-hGap) < fm.stringWidth(processData)) {
//            processData = truncateColumnData(fm, width-hGap, processData);
//        }
//
//        switch (alignment) {
//            case LEFT :
//                xPos += hGap;
//                break;
//            case CENTER :
//                xPos += (width - fm.stringWidth(processData))/2;
//                break;
//            case RIGHT :
//                xPos += (width - fm.stringWidth(processData) - hGap);
//                break;
//        }
//        g2D.drawString(processData, xPos, y);
//
//        processData = null;
//        fm          = null;
//        return x + width;
//    }

    // ---------------------------------------------------------------------------------------------
    // Existing Old Methods start from here
    // ---------------------------------------------------------------------------------------------
/*    public static Graphics2D drawHeader(Graphics2D g2D, int x, int y, int width, int height, boolean isPrinting) {
        int midXPos = 0;//x + width/2; // + 80;
        int xPos    = 0;
        int offset  = 100;       // Address display offset from the center
        int yPos    = y + 5;
        FontMetrics fm      = null;
        int imgWidth    = 0;

        try {
            if (Language.isLTR())
                midXPos = x + width/2 + offset;
            else
                midXPos = x + width/2 - offset;

//            if (companyLogo == null)
//                isCustomizedVersion = false;
//            else
//                isCustomizedVersion = true;

            g2D.setStroke(BS_1p5f_solid);
            g2D.drawRect(x, y, width, height);

            if (isCustomizedVersion) {
                xPos = x + 10;
                g2D.drawImage(companyLogo, xPos, y + (height / 2)-companyLogo.getHeight(null)/2, companyLogo.getWidth(null),
                              companyLogo.getHeight(null), null);
                imgWidth = uniQuotesLogo.getWidth(null);
                int imgHeight= uniQuotesLogo.getHeight(null);
                g2D.drawImage(uniQuotesLogo, xPos+width-imgWidth-30, y + (height / 2)-imgHeight/2, imgWidth,
                              imgHeight, null);
            } else {
                //g2D.setStroke(BS_1p8f_solid);
                g2D.setFont(Theme.getDefaultFont(Font.BOLD, FONT_EXTREME));
                fm = g2D.getFontMetrics();

//                if (!Language.isLTR())
//                    xPos = x + width - fm.stringWidth(Language.getString("PRINT_PAGE_TIT_SNE")) - 5;
//                else
//                    xPos = x + 10;
                if (!isUniQuotesLogoExists) {
                    if (!Language.isLTR())
                        xPos = x + width - fm.stringWidth(Language.getString("PRINT_PAGE_TIT_SNE")) - 5;
                    else
                        xPos = x + 10;
                    g2D.drawString(Language.getString("MENU_SYSTEM"), xPos, y + (height / 2));
                } else {
                    imgWidth = uniQuotesLogo.getWidth(null);
                    if (!Language.isLTR())
                        xPos = x + width - imgWidth - 5;
                    else
                        xPos = x + 10;
                    g2D.drawImage(uniQuotesLogo, xPos, y + (height / 2)-uniQuotesLogo.getHeight(null)/2, uniQuotesLogo.getWidth(null),
                                  uniQuotesLogo.getHeight(null), null);
                }
                g2D.setFont(Theme.getDefaultFont(Font.PLAIN, FONT_PRINT_SMALL));

//                if (!isPrinting) {
//                    g2D.setColor(headingBGColor);
//                    g2D.fillRect(midXPos, yPos - fm.getAscent(), width - midXPos, height);
//                    g2D.setColor(headingFGColor);
//                }

                fm = g2D.getFontMetrics();
                yPos += fm.getAscent();
                xPos = midXPos;
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("RPT_ADDRESS_LINE_1")); //"National technology Group");
                g2D.drawString(Language.getString("RPT_ADDRESS_LINE_1"), xPos, yPos);
                yPos += fm.getHeight();
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("RPT_ADDRESS_LINE_2")); //"P O Box 220625");
                g2D.drawString(Language.getString("RPT_ADDRESS_LINE_2"), xPos, yPos);
                yPos += fm.getHeight();
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("RPT_ADDRESS_LINE_3")); //"Riyadh 11311, Saudi Arabia");
                g2D.drawString(Language.getString("RPT_ADDRESS_LINE_3"), xPos, yPos);
                yPos += fm.getHeight();
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("COMPANY_URL")); //"http://tadawul.uniquotes.com");
                g2D.drawString(Language.getString("COMPANY_URL"), xPos, yPos);
                yPos += fm.getHeight();
                if (!Language.isLTR())
                    xPos = midXPos - fm.stringWidth(Language.getString("COMPANY_EMAIL")); //"uniquotes@acs.com.sa");
                g2D.drawString(Language.getString("COMPANY_EMAIL"), xPos, yPos);
            }
        } catch (Exception ex) {
        }
        fm = null;
        return g2D;
    } */

//    public static int drawHeading(String heading, Graphics2D g2D, int midX, int yPos, int fontSize, Color bgColor, Color fgColor, boolean isPrinting) {
//        int xPos = 0;
//        FontMetrics fm = null;
//        try {
//            g2D.setFont(Theme.getDefaultFont(Font.BOLD, fontSize));
//            fm = g2D.getFontMetrics();
//            yPos += fm.getAscent();
//            xPos = midX - fm.stringWidth(heading)/2;
//            if (!isPrinting) {
//                g2D.setColor(evenRowBGColor);
//
//            }
//            g2D.drawString(heading, xPos, yPos);
//        } catch (Exception ex) {
//        }
//        return fm.getHeight() + fm.getDescent() + fm.getDescent();
//    }

/*    public static int drawTableColumn(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height) {
        int xPos = x;
        FontMetrics fm = g2D.getFontMetrics();
        g2D.setStroke(BS_0p5f_solid);
//        g2D.setColor(gridColor);
        g2D.drawRect(x, y, width, height);
        y += g2D.getFontMetrics().getAscent() + 5;
        switch (alignment) {
            case LEFT :
                xPos += 5;
                break;
            case CENTER :
                xPos += (width - fm.stringWidth(data))/2;
                break;
            case RIGHT :
                xPos += (width - fm.stringWidth(data) - 5);
                break;
        }
        g2D.drawString(data, xPos, y);
        fm = null;
        return x + width;
    } */

/*    public static int drawTableColumn(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height, int status, boolean isEven, boolean isPrinting) {
        int xPos = x;
        FontMetrics fm = g2D.getFontMetrics();

        if (!isPrinting) {
            if (isEven)
                g2D.setColor(evenRowBGColor);
            else
                g2D.setColor(oddRowBGColor);
            g2D.fillRect(x, y, width, height);
        }
        g2D.setStroke(BS_0p5f_solid);
        g2D.setColor(gridColor);
        g2D.drawRect(x, y, width, height);

        y += g2D.getFontMetrics().getAscent() + 5;

        if (!isPrinting) {
            switch (status) {
                case UP :
                    g2D.setColor(upColor);
                    break;
                case DOWN :
                    g2D.setColor(downColor);
                    break;
                case NOCHANGE :
                    if (isEven)
                        g2D.setColor(evenRowFGColor);
                    else
                        g2D.setColor(oddRowFGColor);
                    break;
            }
        }

        switch (alignment) {
            case LEFT :
                xPos += 5;
                break;
            case CENTER :
                xPos += (width - fm.stringWidth(data))/2;
                break;
            case RIGHT :
                xPos += (width - fm.stringWidth(data) - 5);
                break;
        }
        g2D.drawString(data, xPos, y);

        if (!isPrinting) {
            if (isEven)
                g2D.setColor(evenRowFGColor);
            else
                g2D.setColor(oddRowFGColor);
        }
        fm = null;
        return x + width;
    }

    public static int drawTableColumn(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height, boolean isEven, boolean isPrinting) {
        int xPos = x;
        FontMetrics fm = g2D.getFontMetrics();

        if (!isPrinting) {
            if (isEven)
                g2D.setColor(evenRowBGColor);
            else
                g2D.setColor(oddRowBGColor);
            g2D.fillRect(x, y, width, height);
        }

        g2D.setStroke(BS_0p5f_solid);
        g2D.setColor(gridColor);
        g2D.drawRect(x, y, width, height);
        y += g2D.getFontMetrics().getAscent() + 5;
        switch (alignment) {
            case LEFT :
//                xPos += 5;
                xPos += 2;
                break;
            case CENTER :
                xPos += (width - fm.stringWidth(data))/2;
                break;
            case RIGHT :
//                xPos += (width - fm.stringWidth(data) - 5);
                xPos += (width - fm.stringWidth(data) - 2);
                break;
        }
        if (!isPrinting) {
            if (isEven)
                g2D.setColor(evenRowFGColor);
            else
                g2D.setColor(oddRowFGColor);
        }

        g2D.drawString(data, xPos, y);
//        if (!isPrinting) {
//            if (isEven)
//                g2D.setColor(evenRowFGColor);
//            else
//                g2D.setColor(oddRowFGColor);
//        }
        fm = null;
        return x + width;
    }

    public static int drawTableHeaderColumn(String data, int alignment, Graphics2D g2D, int x, int y, int width, int height, boolean isPrinting) {
        int xPos = x;
        FontMetrics fm = g2D.getFontMetrics();

        if (!isPrinting) {
            g2D.setColor(headerBGColor);
            g2D.fillRect(x, y, width, height);
        }

        g2D.setStroke(BS_0p5f_solid);
        g2D.setColor(gridColor);
        g2D.drawRect(x, y, width, height);
        y += g2D.getFontMetrics().getAscent() + 5;
        switch (alignment) {
            case LEFT :
                xPos += 5;
                break;
            case CENTER :
                xPos += (width - fm.stringWidth(data))/2;
                break;
            case RIGHT :
                xPos += (width - fm.stringWidth(data) - 5);
                break;
        }
        if (!isPrinting) {
            g2D.setColor(headerFGColor);
        }

        g2D.drawString(data, xPos, y);
//        if (!isPrinting) {
//            if (isEven)
//                g2D.setColor(evenRowFGColor);
//            else
//                g2D.setColor(oddRowFGColor);
//        }
        fm = null;
        return x + width;
    } */

}