package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trade.TradeSummaryModel;
import com.isi.csvr.trade.TradeSummaryStore;
import com.isi.csvr.util.ColumnGroup;
import com.isi.csvr.util.GroupableTableHeader;

import javax.swing.*;
import javax.swing.table.TableColumnModel;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 22, 2008
 * Time: 11:41:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class TimeNSalesSymbolSummaryFrame extends InternalFrame implements LinkedWindowListener, Themeable {
    TradeSummaryModel model;
    SortButtonRenderer renderer;
    private Table oSummaryTable;
    private ViewSetting oViewSetting;
    private boolean loadedFromWorkspace = false;
    private String sSelectedSymbol;
    private byte stockDecimalPlaces = 2;
    private boolean isFromWSP;
    private boolean isLinked;

    public TimeNSalesSymbolSummaryFrame(ThreadGroup oThreadGroup, String sThreadID, WindowDaemon oDaemon, Table oTable, String sSelectedSymbol, byte stockDecimalPlaces, boolean isFromWSP, boolean isLinked, String linkgroup) {
        super(oThreadGroup, sThreadID, oDaemon, oTable);
        oSummaryTable = oTable;
        this.sSelectedSymbol = sSelectedSymbol;
        this.stockDecimalPlaces = stockDecimalPlaces;
        this.isFromWSP = isFromWSP;
        this.isLinked = isLinked;
        setLinkedGroupID(linkgroup);
        initUI();
    }

    public void initUI() {
        oSummaryTable.setWindowType(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW);
        oSummaryTable.setAutoResize(false);
        oSummaryTable.setThreadID(Constants.ThreadTypes.TIME_N_SALES);
        model = new TradeSummaryModel();
        model.setDataStore(TradeSummaryStore.getSharedInstance().getSummaryTradesForSymbol(sSelectedSymbol));
        if (oViewSetting == null && isFromWSP && isLinked) {
            ViewSetting oSetting = Client.getInstance().getViewSettingsManager().getTimenSalesView(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(sSelectedSymbol)) {
                oViewSetting = oSetting;
            }
        }

        if (oViewSetting == null) {
            oViewSetting = Client.getInstance().getViewSettingsManager().getTimenSalesView(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW
                    + "|" + sSelectedSymbol);
            if (oViewSetting != null) {
                String[] columns = oViewSetting.getColumnHeadings().clone();// need to clone headings since the groupable
                oViewSetting.setColumnHeadings(columns);                    // model modify the the original array for grouping
            }
        }
        if (oViewSetting == null) {
            oViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("TRADE_SUMMARY");
            oViewSetting = oViewSetting.getObject(); // clone the object
            String[] columns = oViewSetting.getColumnHeadings().clone();// need to clone headings since the groupable
            oViewSetting.setColumnHeadings(columns);                    // model modify the the original array for grouping
            oViewSetting.setID(System.currentTimeMillis() + "");
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            Client.getInstance().getViewSettingsManager().putTimenSalesView(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW
                    + "|" + sSelectedSymbol, oViewSetting);
        } else {
            loadedFromWorkspace = true;
        }


        model.setViewSettings(oViewSetting);
        oSummaryTable.setSortingEnabled();
        oSummaryTable.setGroupableHeaderModel(model);
//        oSummaryTable.setModel(model);
//        model.setTable(oSummaryTable); //==before
        model.setTable(oSummaryTable, new TimeNSalesSummeryRenderer());
        model.applyColumnSettings();
        renderer = new SortButtonRenderer(true);

        //-------shanika ---------
        TableColumnModel cm = oSummaryTable.getTable().getColumnModel();
        ColumnGroup cin = new ColumnGroup(Language.getString("CF_CASH_IN"));
        if (!isFromWSP) {
            GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("TRADE_SUMMARY_COLS"));
        }
        cin.add(cm.getColumn(3));
        cin.add(cm.getColumn(4));
        cin.add(cm.getColumn(5));
        oViewSetting.getColumnHeadings()[3] = Language.getString("CF_CASH_IN") + " - " + oViewSetting.getColumnHeadings()[3];
        oViewSetting.getColumnHeadings()[4] = Language.getString("CF_CASH_IN") + " - " + oViewSetting.getColumnHeadings()[4];
        oViewSetting.getColumnHeadings()[5] = Language.getString("CF_CASH_IN") + " - " + oViewSetting.getColumnHeadings()[5];


        ColumnGroup cout = new ColumnGroup(Language.getString("CF_CASH_OUT"));
        cout.add(cm.getColumn(6));
        cout.add(cm.getColumn(7));
        cout.add(cm.getColumn(8));
        oViewSetting.getColumnHeadings()[6] = Language.getString("CF_CASH_OUT") + " - " + oViewSetting.getColumnHeadings()[6];
        oViewSetting.getColumnHeadings()[7] = Language.getString("CF_CASH_OUT") + " - " + oViewSetting.getColumnHeadings()[7];
        oViewSetting.getColumnHeadings()[8] = Language.getString("CF_CASH_OUT") + " - " + oViewSetting.getColumnHeadings()[8];

        GroupableTableHeader header = (GroupableTableHeader) (oSummaryTable.getTable().getTableHeader());
        header.addGroup(cin);
        header.addGroup(cout);

        TableColumnModel colmodel = oSummaryTable.getTable().getColumnModel();
        int n = model.getColumnCount();

        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }
        oSummaryTable.createGroupableTable();


        oSummaryTable.getTable().getTableHeader().setDefaultRenderer(renderer);
        oSummaryTable.getTable().getTableHeader().updateUI();
        oSummaryTable.getTable().getTableHeader().repaint();
        oSummaryTable.getModel().updateGUI();

        //---------------------end ------------------------------

        oViewSetting.setParent(this);
        this.setShowServicesMenu(true);
        this.setSize(oViewSetting.getSize());
        this.setLocation(oViewSetting.getLocation());
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.setLinkGroupsEnabled(true);
        Client.getInstance().getDesktop().add(this);
        this.setTitle(Language.getString("TRADE_SUMMARY") + " : " +
                SharedMethods.getDisplayKey(sSelectedSymbol) + " - " +
                DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription());

        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        this.getContentPane().add(oSummaryTable);
        oSummaryTable.updateGUI();
        this.applySettings();
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        this.show();
        model.adjustRows();
        this.setLayer(GUISettings.TOP_LAYER);
        this.setOrientation();
        if (isFromWSP && isLinked) {
            Client.getInstance().getViewSettingsManager().setWindow(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" +
                    LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            Client.getInstance().getViewSettingsManager().setWindow(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" +
                    sSelectedSymbol, this);
        }
        oSummaryTable.getModel().setDecimalCount(stockDecimalPlaces);
//        table.updateGUI();
        oSummaryTable.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);


        if (!loadedFromWorkspace) {
            if (!oViewSetting.isLocationValid()) {
                this.setLocationRelativeTo(Client.getInstance().getDesktop());
                oViewSetting.setLocation(this.getLocation());
            } else {
                this.setLocation(oViewSetting.getLocation());
            }
        }

        JLabel message = new JLabel(Language.getString("TIME_N_SALES_SUMMARY_STATUS_MESSAGE"));
        GUISettings.applyOrientation(message);
        oSummaryTable.setSouthPanel(message);

//            String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + sSelectedSymbol;
//            addTradeRequest(requestID, sSelectedSymbol);
//            frame.setDataRequestID(TradeStore.getSharedInstance(), sSelectedSymbol, requestID);

        GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("TRADE_SUMMARY_COLS"));
    }

    public void setDecimalPlaces() {
        try {
            stockDecimalPlaces = SharedMethods.getDecimalPlaces(sSelectedSymbol);
        } catch (Exception e) {
            stockDecimalPlaces = 2;
        }
        oSummaryTable.getModel().setDecimalCount(stockDecimalPlaces);
        oSummaryTable.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);
    }

    public void symbolChanged(String sKey) {
        String exchange = SharedMethods.getExchangeFromKey(sKey);
        Exchange ex = null;
        if (exchange == null || exchange.isEmpty()) {
            return;
        } else {
            ex = ExchangeStore.getSharedInstance().getExchange(exchange);
            if (ex == null) {
                return;
            }
        }

        if (ex.isDefault()) {
            sSelectedSymbol = sKey;
            model.setDataStore(TradeSummaryStore.getSharedInstance().getSummaryTradesForSymbol(sSelectedSymbol));
            this.setTitle(Language.getString("TRADE_SUMMARY") + " : " +
                    SharedMethods.getDisplayKey(sSelectedSymbol) + " - " +
                    DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription());
            oSummaryTable.getTable().updateUI();
            this.updateUI();
            setDecimalPlaces();
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    public void applyTheme() {
        renderer.applyTheme();
        renderer.updateUI();
        oSummaryTable.getTable().getTableHeader().updateUI();
        oSummaryTable.getTable().getTableHeader().repaint();
    }
}
