package com.isi.csvr.tabbedpane;

import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.ChoiceFormat;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Dec 15, 2007
 * Time: 10:03:24 PM
 */
public class TWTabbedPane extends JPanel implements Themeable, MouseListener {

    private static int autoIncrementID = 0;
    private TabPanel tabbPanel;
    private JPanel bodyPanel;
    private CardLayout bodyLayout;
    private Image tabLeft;
    private Image tabRight;
    private Image tabCenter;
    private Image tabLeftHighlighted;
    private Image tabRightHighlighted;
    private Image tabCenterHighlighted;
    private Image tabLeftSelected;
    private Image tabRightSelected;
    private Image tabCenterSelected;
    private int tabLeftWidth;
    private int tabRightWidth;
    private int tabCenterWidth;
    private int tabHeight;
    private TWTabbedPaleListener listener;
    private java.util.List<Tab> tabs;
    private java.util.List<Component> components;
    private boolean topPlacement;
    private boolean absolutePlacement;
    private String locationID;
    private Color slectedFontColor;
    private Font selectedFont;
    private Color unslectedFontColor;
    private Font unselectedFont;
    private boolean tabLineVisible;
    private MouseListener mouseListener;
    private TAB_PLACEMENT tabPlacement;
    private LAYOUT_POLICY layoutPolicy;

    public TWTabbedPane() {
        this(TAB_PLACEMENT.Top, CONTENT_PLACEMENT.Absolute, COMPONENT_PLACEMENT.Right, LAYOUT_POLICY.ScrollTabLayout, "sc", false);
    }

    public TWTabbedPane(LAYOUT_POLICY layoutPpolicy) {
        this(TAB_PLACEMENT.Top, CONTENT_PLACEMENT.Absolute, COMPONENT_PLACEMENT.Right, layoutPpolicy, "", false);
    }

    public TWTabbedPane(TAB_PLACEMENT tabPlacement, CONTENT_PLACEMENT contentPlacement, String locationID) {
        this(tabPlacement, contentPlacement, COMPONENT_PLACEMENT.Right, LAYOUT_POLICY.ScrollTabLayout, locationID, false);
    }

    public TWTabbedPane(TAB_PLACEMENT tabPlacement, CONTENT_PLACEMENT contentPlacement, String locationID, boolean tabLineVisible) {
        this(tabPlacement, contentPlacement, COMPONENT_PLACEMENT.Right, LAYOUT_POLICY.ScrollTabLayout, locationID, tabLineVisible);
    }

    public TWTabbedPane(TAB_PLACEMENT tabPlacement, CONTENT_PLACEMENT contentPlacement,
                        COMPONENT_PLACEMENT componentPlacement, LAYOUT_POLICY layoutPolicy, String locationID, boolean tabLineVisible) {
        absolutePlacement = contentPlacement == CONTENT_PLACEMENT.Absolute;
        this.tabPlacement = tabPlacement;
        this.locationID = locationID;
        this.tabLineVisible = tabLineVisible;
        this.layoutPolicy = layoutPolicy;

        String currentLocationID = locationID;
        File file = new File("images/theme" + Theme.getID() + "/tab/" + locationID + "_tabLeft.jpg");
        if (!file.exists()) {
            currentLocationID = "";
        }

        /*tabLeft = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabLeft.jpg")).getImage();
        tabLeftWidth = tabLeft.getWidth(this);
        tabLeftSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabLeftSelected.jpg")).getImage();
        tabLeftHighlighted = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabLefthighlighted.jpg")).getImage();
        tabRight = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabRight.jpg")).getImage();
        tabRightWidth = tabRight.getWidth(this);
        tabRightSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabRightSelected.jpg")).getImage();
        tabRightHighlighted = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabRighthighlighted.jpg")).getImage();
        tabCenter = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabCenter.jpg")).getImage();
        tabCenterWidth = tabCenter.getWidth(this);
        tabCenterSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabCenterSelected.jpg")).getImage();
        tabCenterHighlighted = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "_tabCenterhighlighted.jpg")).getImage();
        tabHeight = tabCenter.getHeight(this);*/
        tabs = Collections.synchronizedList(new ArrayList<Tab>());
        components = Collections.synchronizedList(new ArrayList<Component>());
        topPlacement = tabPlacement == TAB_PLACEMENT.Top;
        createUI(tabPlacement, componentPlacement);
        applyTheme();

    }

    public Component addTab(String text, Component component) {
        return addTab(-1, text, component);
    }

    public Component addTab(String text, Component component, String toolTip) {
        return addTab(-1, text, component, toolTip);
    }

    public Component addTab(int index, String text, Component component) {
        return addTab(index, text, component, null);
    }

    public Component addTab(int index, String text, Component component, String toolTip) {
        autoIncrementID = autoIncrementID + 1;
        String id = "" + autoIncrementID;
        Tab tab = new Tab(text);
        tab.setContentAreaFilled(false);
        tab.setToolTipText(toolTip);
//        tab.setBorder(BorderFactory.createEmptyBorder());
        /*if((tabs.size()>0) && (id.equals(tabs.get(tabs.size()-1).getActionCommand()))){
            id = ""+ (System.currentTimeMillis() +1);
        }*/
        tab.setActionCommand(id);
        if (index >= 0) {
            tabbPanel.addTab(tab, index);
            tabs.add(index, tab);
            components.add(index, component);
        } else {
            tabbPanel.addTab(tab);
            tabs.add(tab);
            components.add(component);
        }
        if (absolutePlacement) {
            bodyPanel.add(component, id);
        }

        tab.addMouseListener(this);
        tabbPanel.updateUI();
        fireChangeEvent(TWTabEvent.STATE_ADDED);
        if (index >= 0) {
            showTab(index);
        } else {
            showTab(getTabCount() - 1);
        }
        return component;
    }

    public void removeTab(int index) {
        try {
            if (absolutePlacement) {
                bodyLayout.removeLayoutComponent(components.get(index));
            }
            tabbPanel.removeTab(index);
            tabs.remove(index);
            bodyPanel.remove(components.remove(index));
            tabbPanel.doLayout();

            fireChangeEvent(TWTabEvent.STATE_REMOVED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedMethods.updateComponent(tabbPanel);
//        tabbPanel.updateUI();
    }

    public void removeTab(Component toBeRemoved) {
        for (int i = 0; i < components.size(); i++) {
            Component component = components.get(i);
            if (component.equals(toBeRemoved)) {
                removeTab(i);
                break;
            }

        }
    }

    public void hideTab(int index) {
        int compomentCount = tabbPanel.getTComponentCount();
        int currentTab = 0;
        for (int i = 0; i < compomentCount; i++) {
            if (tabbPanel.getTComponent(i) instanceof Tab) {
                if (currentTab == index) {
                    tabbPanel.getTComponent(i).setVisible(false);
                    break;
                } else {
                    currentTab++;
                }
            }
        }
        try {
            try {
                tabs.get(index + 1).requestFocus();
                if (absolutePlacement) {
                    bodyLayout.show(bodyPanel, (tabs.get(index + 1)).getActionCommand());
                }
            } catch (Exception e) {
                tabs.get(index - 1).requestFocus();
                if (absolutePlacement) {
                    bodyLayout.show(bodyPanel, (tabs.get(index - 1)).getActionCommand());
                }
            }
        } catch (Exception e) {

        }
    }

    public void showTab(int index) {
        try {
            int compomentCount = tabbPanel.getTComponentCount();
            int currentTab = 0;
            Tab tab;
            for (int i = 0; i < compomentCount; i++) {
                if (tabbPanel.getTComponent(i) instanceof Tab) {
                    if (currentTab == index) {
                        tabbPanel.getTComponent(i).setVisible(true);
                        tab = ((Tab) tabbPanel.getTComponent(i));
                        if (!tab.isSelected()) {
                            selectTab((Tab) tabbPanel.getTComponent(i));
                            if (absolutePlacement) {
                                bodyLayout.show(bodyPanel, tab.getActionCommand());
                            }
                            tabbPanel.getTComponent(i).requestFocus();
                            fireChangeEvent(TWTabEvent.STATE_SELECTED);
                        }
                        break;
                    } else {
                        currentTab++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void selectTab(int index) {
        boolean alreadySelected = (tabs.get(index).isSelected());

        if (absolutePlacement) {
            bodyLayout.show(bodyPanel, (tabs.get(index)).getActionCommand());
        }

        selectTab(tabs.get(index));
        tabs.get(index).requestFocus();
        if (!alreadySelected) {
            fireChangeEvent(TWTabEvent.STATE_SELECTED);
        }
    }

    public void highlightTab(int index, boolean highlight) {
        boolean alreadyHighlighted = tabs.get(index).isHighlighted();
        Tab tab = tabs.get(index);
        tab.setHighlighted(highlight);
        if (highlight && (!alreadyHighlighted)) { // newly highlighted
            fireChangeEvent(TWTabEvent.STATE_HIGHLIGHTED);
        }
        tab.updateUI();
    }

    public void addTabMouseListenerToTab(MouseListener listener) {
        this.mouseListener = listener;
    }

    public void addSpace(int width) {
        tabbPanel.add(Box.createHorizontalStrut(width));
    }

    public void addSpring() {
        tabbPanel.add(Box.createGlue());
    }

    public void addComponent(Component component) {
        tabbPanel.add(component);
    }

    public int getTabCount() {
        return tabs.size();
    }

    public void setTitleAt(int index, String title) {
        try {
            tabs.get(index).setText(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Component getTabComponentAt(int index) {
        try {
            return tabs.get(index);
        } catch (Exception e) {
            return null;
        }
    }

    public Component getComponentAt(int index) {
        try {
            return components.get(index);
        } catch (Exception e) {
            return null;
        }
    }

    public Component getSelectedComponent() {
        for (int i = 0; i < tabs.size(); i++) {
            Tab tab = tabs.get(i);
            if (tab.isSelected()) {
                return components.get(i);
            }
        }
        return null;
    }

    public void setSelectedComponent(Component toBeSelected) {
        for (int i = 0; i < components.size(); i++) {
            Component component = components.get(i);
            if (component.equals(toBeSelected)) {
                showTab(i);
                break;
            }
        }
    }

    public int getSelectedIndex() {
        for (int i = 0; i < tabs.size(); i++) {
            Tab tab = tabs.get(i);
            if (tab.isSelected()) {
                return i;
            }
        }
        return -1;
    }

    public void setSelectedIndex(int index) {
        showTab(index);
    }

    public boolean isVisible(int index) {
        try {
            return tabs.get(index).isVisible();
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isVisible(Component toCompare) {
        try {
            for (int i = 0; i < components.size(); i++) {
                Component component = components.get(i);
                if (component.equals(toCompare)) {
                    return tabs.get(i).isVisible();
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean contains(Component toCompare) {
        try {
            for (int i = 0; i < components.size(); i++) {
                Component component = components.get(i);
                if (component.equals(toCompare)) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public void addTabPanelListener(TWTabbedPaleListener listener) {
        this.listener = listener;
    }

    public void removeTabPaneListener() {
        this.listener = null;
    }

    public void fireChangeEvent(int state) {
        if (listener != null) {
            TWTabEvent event = new TWTabEvent(this, state);
            listener.tabStareChanged(event);
        }
    }

    private void createUI(TAB_PLACEMENT placement, COMPONENT_PLACEMENT componentPlacement) {
        setLayout(new BorderLayout(0, 0));
        tabbPanel = new TabPanel();
        //BoxLayout tabPanelLayout = new BoxLayout(tabbPanel, BoxLayout.LINE_AXIS);
//        tabbPanel.setLayout(tabPanelLayout);
        bodyLayout = new CardLayout();
        bodyPanel = new JPanel(bodyLayout);

//        if (tabLineVisible){
        if (placement == TAB_PLACEMENT.Top) {
            add(tabbPanel, BorderLayout.NORTH);
        } else {
            add(tabbPanel, BorderLayout.SOUTH);
        }
//        }
        if (absolutePlacement) {
            add(bodyPanel, BorderLayout.CENTER);
        }
        Theme.registerComponent(this);
    }

    public boolean isTabLineVisible() {
        return tabLineVisible;
    }

    public void setTabLineVisible(boolean status) {
        tabLineVisible = status;
        /* first remove existing components */
        try {
            this.remove(tabbPanel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.remove(bodyPanel);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Add components again */
        if (tabLineVisible) {
            if (tabPlacement == TAB_PLACEMENT.Top) {
                add(tabbPanel, BorderLayout.NORTH);
            } else {
                add(tabbPanel, BorderLayout.SOUTH);
            }
        }
        if (absolutePlacement) {
            add(bodyPanel, BorderLayout.CENTER);
        }

        /* redo the layout */
        try {
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unregisterTheme() {
        Theme.unRegisterComponent(this);
    }

    public void mouseClicked(MouseEvent e) {
        if (mouseListener != null) {
            mouseListener.mouseClicked(e);
        }
        try {
            /*Tab source = (Tab)e.getSource();
            System.out.println("Source selected  " + source.getText() + " " + source.isSelected());
            if (source.isSelected()){ // if already selected
                return;
            }*/

            for (Tab tab : tabs) {
                if (tab.equals(e.getSource())) {
                    if (absolutePlacement) {
                        bodyLayout.show(bodyPanel, ((JButton) e.getSource()).getActionCommand());
                    }
                    tab.setSelected(true);
                } else {
                    tab.setSelected(false);
                }
                tab.repaint();
            }
            fireChangeEvent(TWTabEvent.STATE_SELECTED);
        } catch (Exception e1) {
        }
        //to catch mouse clicked event
        if (mouseListener != null) {
            Point destReltivePoint = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), this);
            MouseEvent tabPanelEvent = new MouseEvent(this, e.getID(), e.getWhen(), e.getModifiers(), destReltivePoint.x,
                    destReltivePoint.y, e.getClickCount(), e.isPopupTrigger(), e.getButton());
            e.setSource(this);
            mouseListener.mouseClicked(tabPanelEvent);
        }
    }

    public void mouseEntered(MouseEvent e) {
        if (mouseListener != null) {
            Point destReltivePoint = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), this);
            MouseEvent tabPanelEvent = new MouseEvent(this, e.getID(), e.getWhen(), e.getModifiers(), destReltivePoint.x
                    , destReltivePoint.y, e.getClickCount(), e.isPopupTrigger(), e.getButton());
            mouseListener.mouseEntered(tabPanelEvent);
            //  mouseListener.mouseEntered(e);
        }
    }

    public void mouseExited(MouseEvent e) {
        if (mouseListener != null) {
            mouseListener.mouseExited(e);
        }
    }

    /*private boolean isToomanyTabs(){
        return (tabbPanel.getPreferredSize().width > tabbPanelHolder.getWidth());
    }*/

    public void mousePressed(MouseEvent e) {
        if (mouseListener != null) {
            mouseListener.mousePressed(e);
        }
    }

    public void mouseReleased(MouseEvent e) {
        if (mouseListener != null) {
            mouseListener.mouseReleased(e);
        }
    }

    //focus listener
    public void focusGained(FocusEvent e) {
        try {
            /*Tab source = (Tab)e.getSource();
            System.out.println("Source selected  " + source.getText() + " " + source.isSelected());
            if (source.isSelected()){ // if already selected
                return;
            }*/

            for (Tab tab : tabs) {
                if (tab.equals(e.getSource())) {
                    if (absolutePlacement) {
                        bodyLayout.show(bodyPanel, ((JButton) e.getSource()).getActionCommand());
                    }
                    tab.setSelected(true);
                } else {
                    tab.setSelected(false);
                }
                tab.repaint();
            }
            fireChangeEvent(TWTabEvent.STATE_SELECTED);
        } catch (Exception e1) {
        }
    }

    public void focusLost(FocusEvent e) {
    }

    private void selectTab(Tab toBeSelected) {
        for (Tab tab : tabs) {
            if (tab.equals(toBeSelected)) {
                tab.setSelected(true);
                tabbPanel.showTab(tab);
            } else {
                tab.setSelected(false);
            }
        }
    }

    public void applyTheme() {
        String currentLocationID = locationID + "_";
        File file = new File("images/theme" + Theme.getID() + "/tab/" + locationID + "_tabLeft.jpg");
        if (!file.exists()) {
            currentLocationID = "_";
        }
        tabLeft = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabLeft.jpg")).getImage();
        tabLeftWidth = tabLeft.getWidth(this);
        tabLeftSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabLeftSelected.jpg")).getImage();
        tabLeftHighlighted = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabLefthighlighted.jpg")).getImage();

        tabRight = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabRight.jpg")).getImage();
        tabRightWidth = tabRight.getWidth(this);
        tabRightSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabRightSelected.jpg")).getImage();
        tabRightHighlighted = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabRighthighlighted.jpg")).getImage();

        tabCenter = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabCenter.jpg")).getImage();
        tabCenterWidth = tabCenter.getWidth(this);
        tabCenterSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabCenterSelected.jpg")).getImage();
        tabCenterHighlighted = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabCenterhighlighted.jpg")).getImage();

        tabLeftWidth = tabLeft.getWidth(this);
        tabRightWidth = tabRight.getWidth(this);
        tabCenterWidth = tabCenter.getWidth(this);

        tabHeight = tabCenter.getHeight(this);


        try {
            tabbPanel.buttonL.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "scrollL.jpg"));
            tabbPanel.buttonR.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "scrollR.jpg"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // color settings

        if (currentLocationID.equals("_")) {
            currentLocationID = "";
        }

        try {
            Color bgColor = Theme.getOptionalColor("TAB_" + currentLocationID.toUpperCase() + "BGCOLOR");
            tabbPanel.setTabBgColor(bgColor);
        } catch (Exception e) {
            tabbPanel.setBackground(null);
        }

        slectedFontColor = Theme.getColor("TAB_" + currentLocationID.toUpperCase() + "SELECTED_FGCOLOR");
        unslectedFontColor = Theme.getColor("TAB_" + currentLocationID.toUpperCase() + "UNSELECTED_FGCOLOR");
        Color lineColor = Theme.getOptionalColor("TAB_" + currentLocationID.toUpperCase() + "LINE_COLOR");
        tabbPanel.setLineColor(lineColor);


        String[] sStyles = {"PLAIN", "BOLD", "ITALIC", "BOLD/ITALIC", "ITALIC/BOLD"};
        double[] dStyles = {Font.PLAIN, Font.BOLD, Font.ITALIC, Font.ITALIC + Font.BOLD, Font.ITALIC + Font.BOLD};
        ChoiceFormat format = new ChoiceFormat(dStyles, sStyles);

        try {
            String selectedFontStyle = Theme.getString("TAB_" + currentLocationID.toUpperCase() + "SELECTED_FONT_STYLE");
            int selectedFontSize = (int) Theme.getLong("TAB_" + currentLocationID.toUpperCase() + "SELECTED_FONT_SIZE");
            if ((selectedFontStyle != null) && (selectedFontSize > 0)) {
                selectedFont = Theme.getDefaultFont(format.parse(selectedFontStyle.trim().toUpperCase()).intValue(), selectedFontSize);
            } else {
                selectedFont = (Font) UIManager.get("Button.font");
            }
        } catch (Exception e) {
//            e.printStackTrace();
            selectedFont = null;
        }

        try {
            String unselectedFontStyle = Theme.getString("TAB_" + currentLocationID.toUpperCase() + "UNSELECTED_FONT_STYLE");
            int unselectedFontSize = (int) Theme.getLong("TAB_" + currentLocationID.toUpperCase() + "UNSELECTED_FONT_SIZE");
            if ((unselectedFontStyle != null) && (unselectedFontSize > 0)) {
                unselectedFont = Theme.getDefaultFont(format.parse(unselectedFontStyle.trim().toUpperCase()).intValue(), unselectedFontSize);
            } else {
                unselectedFont = (Font) UIManager.get("Button.font");
            }
        } catch (Exception e) {
//            e.printStackTrace();
            unselectedFont = null;
        }
        tabbPanel.updateUI();
        for (Tab tab : tabs) {
            if (tab.isSelected()) {
                tab.setForeground(slectedFontColor);
                if (selectedFont != null) {
                    tab.setFont(selectedFont);
                }
            } else {
                tab.setForeground(unslectedFontColor);
                if (unselectedFont != null) {
                    tab.setFont(unselectedFont);
                }
            }
            tab.repaint();
        }

        /* Set the body border */
        try {
            int borderThickness = (int) Theme.getLong("TAB_" + currentLocationID.toUpperCase() + "BORDER_THICKNESS");
            Color borderColor = Theme.getColor("TAB_" + currentLocationID.toUpperCase() + "BORDER_COLOR");
            if (borderThickness > 0) {
                bodyPanel.setBorder(BorderFactory.createLineBorder(borderColor, borderThickness));
            }
        } catch (Exception e) {
        }

    }

    protected boolean isRTL() {
        return getComponentOrientation() == ComponentOrientation.RIGHT_TO_LEFT;
    }

    public int getIndexForText(String text) {

        for (int i = 0; i < tabs.size(); i++) {
            if (tabs.get(i).getText().equals(text)) {
                return i;
            }
        }

        return 0;
    }

    public void setSelectedFontColor(Color color) {
        slectedFontColor = color;
    }

    public void setUnselectedFontColor(Color color) {
        unslectedFontColor = color;
    }

    public static enum COMPONENT_PLACEMENT {Left, Right}

    public static enum TAB_PLACEMENT {Top, Bottom}

    public static enum CONTENT_PLACEMENT {Absolute, Simulated}

    public static enum LAYOUT_POLICY {ScrollTabLayout, WrapTabLayout}

    protected class Tab extends JButton {
        private int width;
        private boolean selected;
        private boolean highlighted;

        private Tab(String text) {
            super(text);
            setBorder(BorderFactory.createEmptyBorder());
            setFocusPainted(false);
//            setFont(Theme.getDefaultFont());
        }

        public Dimension getPreferredSize() {
            width = (int) super.getPreferredSize().getWidth();
            return new Dimension(width + tabLeftWidth + tabRightWidth, tabHeight);
        }

        public Dimension getMinimumSize() {
            return getPreferredSize();
        }

        public Dimension getMaximumSize() {
            return getPreferredSize();
        }

        public Dimension getSize() {
            return getPreferredSize();
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean status) {
            selected = status;
            if (status == true) {
                setHighlighted(false);
                setForeground(slectedFontColor);
                if (selectedFont != null) {
                    setFont(selectedFont);
                }
            } else {
                setForeground(unslectedFontColor);
                if (unselectedFont != null) {
                    setFont(unselectedFont);
                }
            }
        }

        public boolean isHighlighted() {
            return highlighted;
        }

        public void setHighlighted(boolean highlighted) {
            this.highlighted = highlighted;
        }

        public void paint(Graphics g) {
            if (topPlacement) {
                if (isRTL()) {
                    if (highlighted) {
                        g.drawImage(tabRightHighlighted,
                                tabRightHighlighted.getWidth(this), 0, 0, tabRightHighlighted.getHeight(this),
                                0, 0, tabRightHighlighted.getWidth(this), tabRightHighlighted.getHeight(this), this);
                        for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenterHighlighted, i, 0, this);
                        }
                        g.translate(getWidth() - tabLeftWidth, 0);
                        g.drawImage(tabLeftHighlighted,
                                tabLeftSelected.getWidth(this), 0, 0, tabLeftHighlighted.getHeight(this),
                                0, 0, tabLeftHighlighted.getWidth(this), tabLeftHighlighted.getHeight(this), this);
                        g.translate(-getWidth() + tabLeftWidth, 0);
                    } else if (selected) {
                        g.drawImage(tabRightSelected,
                                tabRightSelected.getWidth(this), 0, 0, tabRightSelected.getHeight(this),
                                0, 0, tabRightSelected.getWidth(this), tabRightSelected.getHeight(this), this);
                        for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenterSelected, i, 0, this);
                        }
                        g.translate(getWidth() - tabLeftWidth, 0);
                        g.drawImage(tabLeftSelected,
                                tabLeftSelected.getWidth(this), 0, 0, tabLeftSelected.getHeight(this),
                                0, 0, tabLeftSelected.getWidth(this), tabLeftSelected.getHeight(this), this);
                        g.translate(-getWidth() + tabLeftWidth, 0);
                    } else {
                        g.drawImage(tabRight,
                                tabRight.getWidth(this), 0, 0, tabRight.getHeight(this),
                                0, 0, tabRight.getWidth(this), tabRight.getHeight(this), this);
                        for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenter, i, 0, this);
                        }
                        g.translate(getWidth() - tabLeftWidth, 0);
                        g.drawImage(tabLeft,
                                tabLeft.getWidth(this), 0, 0, tabLeft.getHeight(this),
                                0, 0, tabLeft.getWidth(this), tabLeft.getHeight(this), this);
                        g.translate(-getWidth() + tabLeftWidth, 0);
                    }
                } else {
                    if (highlighted) {
                        g.drawImage(tabLeftHighlighted, 0, 0, this);
                        for (int i = tabLeftWidth; i < (getWidth() - tabRightWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenterHighlighted, i, 0, this);
                        }
                        g.drawImage(tabRightHighlighted, getWidth() - tabRightWidth, 0, this);
                    } else if (selected) {
                        g.drawImage(tabLeftSelected, 0, 0, this);
                        for (int i = tabLeftWidth; i < (getWidth() - tabRightWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenterSelected, i, 0, this);
                        }
                        g.drawImage(tabRightSelected, getWidth() - tabRightWidth, 0, this);
                    } else {
                        g.drawImage(tabLeft, 0, 0, this);
                        for (int i = tabLeftWidth; i < (getWidth() - tabRightWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenter, i, 0, this);
                        }
                        g.drawImage(tabRight, getWidth() - tabRightWidth, 0, this);
                    }
                }
            } else { // bottom placement
                if (isRTL()) {
                    if (highlighted) {
                        g.drawImage(tabLeftHighlighted,
                                getWidth(), tabHeight, getWidth() - tabLeftWidth, 0, 0, 0, tabLeftWidth, tabHeight, this);
                        for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenterHighlighted, i + tabCenterWidth, tabHeight, i, 0,
                                    0, 0, tabCenterWidth, tabHeight, this);
                        }
                        g.drawImage(tabRightHighlighted, tabRightWidth, tabHeight, 0, 0, 0, 0, tabRightWidth, tabHeight, this);
                    } else if (selected) {
                        g.drawImage(tabLeftSelected,
                                getWidth(), tabHeight, getWidth() - tabLeftWidth, 0, 0, 0, tabLeftWidth, tabHeight, this);
                        for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenterSelected, i + tabCenterWidth, tabHeight, i, 0,
                                    0, 0, tabCenterWidth, tabHeight, this);
                        }
                        g.drawImage(tabRightSelected, tabRightWidth, tabHeight, 0, 0, 0, 0, tabRightWidth, tabHeight, this);
                    } else {
                        g.drawImage(tabLeft,
                                getWidth(), tabHeight, getWidth() - tabLeftWidth, 0, 0, 0, tabLeftWidth, tabHeight, this);
                        for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenter, i + tabCenterWidth, tabHeight, i, 0, 0, 0, tabCenterWidth, tabHeight, this);
                        }
                        g.drawImage(tabRight, tabRightWidth, tabHeight, 0, 0, 0, 0, tabRightWidth, tabHeight, this);
                    }
                } else {
                    if (highlighted) {
                        g.drawImage(tabLeftHighlighted, 0, tabHeight, tabLeftWidth, 0, 0, 0, tabLeftWidth, tabHeight, this);
                        for (int i = tabLeftWidth; i < (getWidth() - tabRightWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenterHighlighted, i, tabHeight, tabCenterWidth + i, 0, 0, 0, tabCenterWidth, tabHeight, this);
                        }
                        g.drawImage(tabRightHighlighted, getWidth() - tabRightWidth, tabHeight, getWidth(), 0, 0, 0, tabRightWidth, tabHeight, this);
                    } else if (selected) {
                        g.drawImage(tabLeftSelected, 0, tabHeight, tabLeftWidth, 0, 0, 0, tabLeftWidth, tabHeight, this);
                        for (int i = tabLeftWidth; i < (getWidth() - tabRightWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenterSelected, i, tabHeight, tabCenterWidth + i, 0, 0, 0, tabCenterWidth, tabHeight, this);
                        }
                        g.drawImage(tabRightSelected, getWidth() - tabRightWidth, tabHeight, getWidth(), 0, 0, 0, tabRightWidth, tabHeight, this);
                    } else {
                        g.drawImage(tabLeft, 0, tabHeight, tabLeftWidth, 0, 0, 0, tabLeftWidth, tabHeight, this);
                        for (int i = tabLeftWidth; i < (getWidth() - tabRightWidth); i += tabCenterWidth) {
                            g.drawImage(tabCenter, i, tabHeight, tabCenterWidth + i, 0, 0, 0, tabCenterWidth, tabHeight, this);
                        }
                        g.drawImage(tabRight, getWidth() - tabRightWidth, tabHeight, getWidth(), 0, 0, 0, tabRightWidth, tabHeight, this);
                    }
                }
            }
            super.paint(g);
        }

        public String getText() {
            String text = super.getText();
            if (text.length() > 25) {
                return text.substring(0, 25) + "...";
            } else {
                return text;
            }
        }
    }

    class TabPanel extends JPanel implements ActionListener, LayoutManager, MouseListener, Runnable {
        protected JButton buttonL;
        protected JButton buttonR;
        private JPanel tabHolderScroller;
        private JPanel tabHolder;
        private int location = Integer.MAX_VALUE;
        private boolean scrolling;
        private int direction;
        private Thread thread;

        TabPanel() {
            tabHolder = new JPanel() {
                public Dimension getPreferredSize() {
                    if (layoutPolicy == LAYOUT_POLICY.ScrollTabLayout) {
                        return super.getPreferredSize();
                    } else {
                        int width = 0;
                        int height = 0;
                        int currentLineWidth = 0;

                        int parentWidth = getParent().getWidth();

                        Component[] items = getComponents();
                        for (Component item : items) {
                            if (currentLineWidth + item.getPreferredSize().getWidth() <= parentWidth) {
                                currentLineWidth += item.getPreferredSize().getWidth();
                                width = Math.max(width, currentLineWidth);
                                height = Math.max(height, (int) item.getPreferredSize().getHeight());
                            } else {
                                currentLineWidth = (int) item.getPreferredSize().getWidth();
                                width = Math.max(width, currentLineWidth);
                                height += item.getPreferredSize().getHeight();
                            }
                        }
                        System.out.println(width + " " + height);
                        return new Dimension(width + 5, height + 5);
                    }
                }
            };
            BoxLayout tabPanelLayout = new BoxLayout(tabHolder, BoxLayout.LINE_AXIS);
//            TabLayout tabLayout = new TabLayout(FlowLayout.LEADING,0,0);
            if (layoutPolicy == LAYOUT_POLICY.ScrollTabLayout) {
                tabHolder.setLayout(tabPanelLayout);
            } else {
                tabHolder.setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
            }
//            tabHolder.addComponentListener(tabLayout);
//            tabHolder.setOpaque(true);
            tabHolder.setLocation(0, 0);
            tabHolder.setOpaque(false);
//            tabHolder.setBorder(BorderFactory.createLineBorder(Color.red));
            this.setLayout(this);//new FlexGridLayout(widths, heights));

            if (layoutPolicy == LAYOUT_POLICY.ScrollTabLayout) {
                tabHolderScroller = new JPanel(this);
                tabHolderScroller.add(tabHolder);
            } else {
                tabHolderScroller = new JPanel(new BorderLayout());
                tabHolderScroller.add(tabHolder, BorderLayout.NORTH);
            }
            tabHolderScroller.setPreferredSize(new Dimension(0, 0));
            tabHolderScroller.setOpaque(false);

            this.add(tabHolderScroller);

            buttonL = new JButton();
            buttonL.setContentAreaFilled(false);
            buttonL.setBorder(BorderFactory.createEmptyBorder());
            buttonL.addActionListener(this);
            buttonL.addMouseListener(this);
            buttonL.setVisible(false);
            buttonR = new JButton();
            buttonR.setContentAreaFilled(false);
            buttonR.setBorder(BorderFactory.createEmptyBorder());
            buttonR.addActionListener(this);
            buttonR.addMouseListener(this);
            buttonR.setVisible(false);
            add(buttonL);
            add(buttonR);
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(buttonL)) {
                scroll(-10);
            } else {
                scroll(10);
            }
        }

        public void addTab(Component comp, int index) {
            tabHolder.add(comp, index);
            tabHolderScroller.doLayout();
        }

        public void addTab(Component comp) {
            tabHolder.add(comp);
            tabHolderScroller.doLayout();
        }

        public Component addComponent(Component comp) {
            return tabHolder.add(comp);
        }

        public void removeTab(int index) {
            tabHolder.remove(index);
        }

        public void addSpace(int width) {
            tabHolder.add(Box.createHorizontalStrut(width));
        }

        public void addSpring() {
            tabHolder.add(Box.createHorizontalGlue());
        }

        public Component getTComponent(int n) {
            return tabHolder.getComponent(n);
        }

        public int getTComponentCount() {
            return tabHolder.getComponentCount();
        }

        /*public Dimension getPreferredSize() {
            return new Dimension(2000,30);
        }*/

        /*public Dimension getSize() {
            return new Dimension(200,30);
        }*/

        public void setLineColor(Color color) {
            if (color != null)
                if (tabPlacement == TAB_PLACEMENT.Top)
                    tabbPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, color));
                else
                    tabbPanel.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, color));
            else
                tabbPanel.setBorder(BorderFactory.createEmptyBorder());
        }

        public int scroll(int distance) {
            if (distance < 0) {
                location = (int) tabHolder.getLocation().getX();
                if ((location + tabHolder.getWidth()) > tabHolderScroller.getWidth()) {
                    location += distance;
                    tabHolderScroller.doLayout();
                } else {
                    scrolling = false;
                }
            } else {
                if (location < 0) {
                    location += distance;
                    tabHolderScroller.doLayout();
                } else {
                    scrolling = false;
                }
            }
            return location;
        }

        public void addLayoutComponent(String name, Component comp) {
        }

        public void doLayout() {
            super.doLayout();
            tabHolder.doLayout();
            tabHolderScroller.doLayout();
            tabHolder.repaint();
            tabHolderScroller.repaint();
        }

        public void layoutContainer(Container parent) {
            if (parent.equals(tabHolderScroller)) {
                Component tabs = parent.getComponent(0);
                tabs.setSize(tabs.getPreferredSize());
                tabs.setLocation(location, 0);


                if (tabHolder.getWidth() > parent.getParent().getWidth()) {
                    buttonL.setVisible(true);
                    buttonR.setVisible(true);
                } else {
                    buttonL.setVisible(false);
                    buttonR.setVisible(false);
                }
            } else if (parent.equals(TabPanel.this)) { // layout the tabHolderScroller and the button
                Component tabs = parent.getComponent(0); // tabHolderScroller

                int top;
                int bottom;
                try {
                    MatteBorder border = (MatteBorder) ((JComponent) parent).getBorder();
                    top = border.getBorderInsets().top;
                    bottom = border.getBorderInsets().bottom;
                } catch (Exception e) {
                    top = 0;
                    bottom = 0;
                }

                if (buttonL.isVisible()) {
                    tabs.setSize(parent.getWidth() - (int) buttonL.getPreferredSize().getWidth() * 2, (int) tabHolder.getPreferredSize().getHeight());
                    if (parent.getComponentOrientation().isLeftToRight()) {
                        tabs.setLocation(0, top);
                        buttonL.setSize(buttonL.getPreferredSize());
                        buttonL.setLocation(parent.getWidth() - (int) buttonL.getPreferredSize().getWidth() * 2, (parent.getHeight() - buttonL.getHeight()) / 2);
                        buttonR.setSize(buttonL.getPreferredSize());
                        buttonR.setLocation(parent.getWidth() - (int) buttonL.getPreferredSize().getWidth(), (parent.getHeight() - buttonR.getHeight()) / 2);
                    } else {
                        tabs.setLocation((int) buttonL.getPreferredSize().getWidth() * 2, top);
                        buttonL.setLocation(0, top);
                        buttonL.setSize(buttonL.getPreferredSize());
                        buttonR.setLocation((int) buttonL.getPreferredSize().getWidth(), top);
                        buttonR.setSize(buttonL.getPreferredSize());
                    }
                } else {


                    buttonL.setSize(0, 0);
                    buttonR.setSize(0, 0);
                    if (top == 1) {
                        buttonR.setLocation(0, 1);
                        tabs.setLocation(0, 1);
                        buttonL.setLocation(0, 1);
                    } else {
                        buttonR.setLocation(0, 0);
                        tabs.setLocation(0, 0);
                        buttonL.setLocation(0, 0);
                    }
                    tabs.setSize(parent.getWidth(), (int) tabHolder.getPreferredSize().getHeight() - top - bottom);
                }

                if (parent.getComponentOrientation().isLeftToRight()) {
                    location = 0;
                } else {
                    if (tabHolder.getWidth() > 0) {
                        location = tabHolderScroller.getWidth() - tabHolder.getWidth();
                    }
                }
            }
        }

        protected void setTabBgColor(Color color) {
            if (color == null) {
                tabHolderScroller.setOpaque(false);
            } else {
                this.setOpaque(true);
                this.setBackground(color);
            }
        }

        public void showTab(final Component tab) {
            Thread t = new Thread() {
                public void run() {
                    int xOffset = (int) SwingUtilities.convertPoint(tab.getParent(), 0, 0, tab.getParent().getParent()).getX();
                    if (xOffset < 10000) { // xOffset can be very large while initial stages. must ignore it
                        while ((tab.getX() + xOffset) < 0) {
                            scroll(10);
                            xOffset += 10;
                            tabHolder.repaint();
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {

                            }
                        }
                    }

                    if (xOffset < 10000) { // xOffset can be very large while initial stages. must ignore it
                        while ((tab.getX() + tab.getWidth() + xOffset) > getParent().getWidth()) {
                            scroll(-10);
                            xOffset -= 10;
                            tabHolder.repaint();
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                }
            };
            t.start();
        }

        public Dimension minimumLayoutSize(Container parent) {
            if (parent.equals(TabPanel.this)) {
                //if (tabLineVisible){
                return new Dimension(parent.getWidth(), (int) tabHolder.getPreferredSize().getHeight());
                //} else {
                //    return new Dimension(0,0);
                //}
            } else {
                return null;
            }
        }

        public Dimension preferredLayoutSize(Container parent) {
            if (parent.equals(TabPanel.this)) {
                //return new Dimension(parent.getWidth(), (int)tabHolder.getPreferredSize().getHeight());
                int top;
                int bottom;
                try {
                    MatteBorder border = (MatteBorder) ((JComponent) parent.getParent()).getBorder();
                    top = border.getBorderInsets().top;
                    bottom = border.getBorderInsets().bottom;
                } catch (Exception e) {
                    top = 0;
                    bottom = 0;
                }
                //if (tabLineVisible){
                return new Dimension(parent.getWidth(), (int) tabHolder.getPreferredSize().getHeight() - top - bottom);
                //} else {
                //    return new Dimension(0,0);
                //}
            } else {
                return null;
            }
        }

        public void removeLayoutComponent(Component comp) {

        }

        public void mouseClicked(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
            if (thread == null) {
                thread = new Thread(this);
                if (e.getSource().equals(buttonL)) {
                    direction = -10;
                    //scroll(-10);
                } else {
                    direction = 10;
//                    scroll(10);
                }
                scrolling = true;
                thread.start();
            }
        }

        public void mouseExited(MouseEvent e) {
            scrolling = false;
            thread = null;
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void run() {
            while (scrolling) {
                scroll(direction);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }
            }
            thread = null;
        }
    }

    class TabLayout extends FlowLayout implements ComponentListener {
        TabLayout() {
        }

        TabLayout(int align) {
            super(align);
        }

        TabLayout(int align, int hgap, int vgap) {
            super(align, hgap, vgap);
        }

/*        public Dimension minimumLayoutSize(Container parent) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
}*/

        public Dimension preferredLayoutSize(Container parent) {
            int width = 0;
            int height = 0;
            int currentLineWidth = 0;

            int parentWidth = parent.getParent().getWidth();

            Component[] items = parent.getComponents();
            for (Component item : items) {
                if (currentLineWidth + item.getPreferredSize().getWidth() <= parentWidth) {
                    currentLineWidth += item.getPreferredSize().getWidth();
                    width = Math.max(width, currentLineWidth);
                    height = Math.max(height, (int) item.getPreferredSize().getHeight());
                } else {
                    currentLineWidth = (int) item.getPreferredSize().getWidth();
                    width = Math.max(width, currentLineWidth);
                    height += item.getPreferredSize().getHeight();
                }
            }
            System.out.println(width + " " + height);
            return new Dimension(width, height);
        }

        public void componentHidden(ComponentEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void componentMoved(ComponentEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void componentResized(ComponentEvent e) {
            super.layoutContainer((Container) e.getComponent());
        }

        public void componentShown(ComponentEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }
}

