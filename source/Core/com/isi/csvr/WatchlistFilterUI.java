package com.isi.csvr;

import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.mist.MISTTable;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWCustomCheckBox;
import com.isi.csvr.theme.Theme;
import com.isi.util.ColumnLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 1, 2005
 * Time: 2:13:09 PM
 */
public class WatchlistFilterUI extends JPanel implements MouseListener {

    private TWCustomCheckBox noFilterItem;
    private JPanel exchangePanel;
    private JPanel currencyPanel;
    private JLabel okButton;
    private ClientTable clientTable;
    private MISTTable MISTTable;
    private boolean isClientTable = true;

    public WatchlistFilterUI() {
        createUI();
    }

    private void createUI() {
        setLayout(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 10));

        noFilterItem = new TWCustomCheckBox(Language.getString("NO_FILTER"),
                SwingUtilities.LEADING,
                TWCustomCheckBox.CHECK_UNCHECK);
        noFilterItem.addMouseListener(this);
        add(noFilterItem);
        add(new JSeparator());

        add(new JLabel(Language.getString("EXCHANGE")));
        exchangePanel = new JPanel(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        exchangePanel.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 10));
        add(exchangePanel);

        add(new JLabel(Language.getString("CURRENCY")));
        currencyPanel = new JPanel(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        currencyPanel.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 10));

        add(currencyPanel);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        okButton = new JLabel("  " + Language.getString("OK") + "  ", SwingUtilities.CENTER);
//        okButton.setBorder(BorderFactory.createEtchedBorder());
        okButton.setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
        okButton.addMouseListener(this);
        buttonPanel.add(okButton);
        add(buttonPanel);

        GUISettings.applyOrientation(this);
    }

    public void populateUI() {

        clrarExchanges();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            addExchange(exchange.getSymbol(), exchange.getDescription());
            exchange = null;
        }
        exchanges = null;

        clearCurrency();
        Enumeration<String> ids = CurrencyStore.getSharedInstance().getCurrencyIDs();
        while (ids.hasMoreElements()) {
            String id = ids.nextElement();
            addCurrency(id, CurrencyStore.getSharedInstance().getCurrencyDescription(id));
            id = null;
        }

        updateUIFromSavedData();
        GUISettings.applyOrientation(this);
        updateUI();
    }

    public void clrarExchanges() {
        exchangePanel.removeAll();
    }

    public void addExchange(String id, String description) {
        TWCustomCheckBox item = new TWCustomCheckBox(description, SwingUtilities.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
        item.setTag(id);
        exchangePanel.add(item);
        item.addMouseListener(this);
        item = null;
    }

    public void clearCurrency() {
        currencyPanel.removeAll();
    }

    public void addCurrency(String id, String description) {
        TWCustomCheckBox item = new TWCustomCheckBox(description, SwingUtilities.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
        currencyPanel.add(item);
        item.addMouseListener(this);
        item.setTag(id);
        item = null;
    }

    public void updateCurrencyPanel() {
        currencyPanel.updateUI();
    }

    public void setClientTable(ClientTable clientTable) {
        this.isClientTable = true;
        this.clientTable = clientTable;
    }

    public void setMISTTable(MISTTable mistTable) {
        this.MISTTable = mistTable;
        this.isClientTable = false;
    }

    private void updateUIFromSavedData() {
        try {
            clearAllSelections();
            //String filterString = clientTable.getViewSettings().getWatchlistFilter();
            String filterString;
            if (isClientTable)
                filterString = clientTable.getViewSettings().getWatchlistFilter();
            else
                filterString = MISTTable.getViewSettings().getWatchlistFilter();
            if ((filterString == null) || (filterString.trim().equals("")) || (filterString.trim().toLowerCase().equals("null"))) {
                noFilterItem.setSelected(true);
            } else {
                noFilterItem.setSelected(false);

                String[] filterData = filterString.split("\\|");

                try {
                    String[] exchanges = filterData[0].split(",");
                    for (int i = 0; i < exchanges.length; i++) {
                        checkItem(exchanges[i], exchangePanel);
                    }
                    exchanges = null;
                } catch (Exception e) {
//                    e.printStackTrace();
                }

                try {
                    String[] currencies = filterData[1].split(",");
                    for (int i = 0; i < currencies.length; i++) {
                        checkItem(currencies[i], currencyPanel);
                    }
                    currencies = null;
                } catch (Exception e) {
//                    e.printStackTrace();
                }

                filterData = null;
            }
        } catch (Exception e) {

        }
    }

    private void checkItem(String id, JPanel panel) {
        Component[] components = panel.getComponents();
        for (int i = 0; i < components.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) components[i];
            if (item.getTag().equals(id)) {
                item.setSelected(true);
                //}else{
                //    item.setSelected(false);
            }
            item = null;
        }
        components = null;
    }

    private void applyFilter() {
        //WatchlistFilter filter = (WatchlistFilter) clientTable.getSymbols().getFilter();

        WatchlistFilter filter;
        ArrayList<String> exchangeList = new ArrayList<String>();
        ArrayList<String> currencyList = new ArrayList<String>();
        if (isClientTable)
            filter = (WatchlistFilter) clientTable.getSymbols().getFilter();
        else
            filter = (WatchlistFilter) MISTTable.getViewSettings().getSymbols().getFilter();

        if (noFilterItem.isSelected()) {
            filter.setFilterCriteria(null, null);
            //clientTable.getViewSettings().setWatchlistFilter("");
            if (isClientTable)
                clientTable.getViewSettings().setWatchlistFilter("");
            else
                MISTTable.getViewSettings().setWatchlistFilter("");
        } else {
            StringBuilder exchangeString = new StringBuilder();
            Component[] components = exchangePanel.getComponents();
            for (int i = 0; i < components.length; i++) {
                try {
                    TWCustomCheckBox item = (TWCustomCheckBox) components[i];
                    if (item.isSelected()) {
                        exchangeList.add(item.getTag());
                        exchangeString.append(item.getTag());
                        exchangeString.append(",");
                    }
                    item = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            components = null;

            StringBuilder currencyString = new StringBuilder();
            components = currencyPanel.getComponents();
            for (int i = 0; i < components.length; i++) {
                try {
                    TWCustomCheckBox item = (TWCustomCheckBox) components[i];
                    if (item.isSelected()) {
                        currencyList.add(item.getTag());
                        currencyString.append(item.getTag());
                        currencyString.append(",");
                    }
                    item = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            components = null;

            filter.setFilterCriteria(exchangeList, currencyList);
//            filter.setFilterCriteria(exchangeString.toString(), currencyString.toString());
            if (isClientTable) {
                clientTable.getViewSettings().setWatchlistFilter(exchangeString.toString() + "|" + currencyString.toString());
            } else {
                MISTTable.getViewSettings().setWatchlistFilter(exchangeString.toString() + "|" + currencyString.toString());
            }
            exchangeString = null;
            currencyString = null;
        }

        if (isClientTable) {
            clientTable.getSymbols().reFilter();
            Client.getInstance().getTablePopup().setVisible(false);
            SharedMethods.updateComponent(clientTable.getTable());
            clientTable = null;
        } else {
            MISTTable.getViewSettings().getSymbols().reFilter();
            Client.getInstance().getTablePopup().setVisible(false);
            SharedMethods.updateComponent(MISTTable.getTable());
            MISTTable = null;
        }
    }

    private void clearAllSelections() {
        Component[] components = exchangePanel.getComponents();
        for (int i = 0; i < components.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) components[i];
            item.setSelected(false);
            item = null;
        }
        components = null;

        components = currencyPanel.getComponents();
        for (int i = 0; i < components.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) components[i];
            item.setSelected(false);
            item = null;
        }
        components = null;
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == okButton) {
            applyFilter();
        } else if (e.getSource() == noFilterItem) {
            if (noFilterItem.isSelected()) {
                clearAllSelections();
            }
        } else {
            noFilterItem.setSelected(false);
        }
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        okButton.setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
    }
}
