// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

/**
 * Generats pulses in a given interval.
 * also this class updates the server timer which
 * is displayed on the gui
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;

public class PulseGenerator extends Thread {
    private static int newsCountDown = Constants.NEWS_TIME_OUT_TIME;
    private int g_iCounter = 0;

    /**
     * Constructor
     */
    public PulseGenerator() {
        super("PulseGenerator");
        start();
    }

    public static synchronized void setNewsCountDownTime(int time) {
//        System.out.println("news count down time resetted");
        newsCountDown = time;
    }

    /**
     * run method of the thread. updates the timer of the client
     * in each second and generates pulses in each 20 seconds
     */
    public void run() {
        while ((Settings.isConnected())) {
            if (g_iCounter > Settings.HEART_BEAT) {
                g_iCounter = 0;
                SendQFactory.addData(Constants.PATH_PRIMARY, Meta.PULSE + Meta.DS + Meta.PULSE);
                SendQFactory.addData(Constants.PATH_SECONDARY, Meta.PULSE + Meta.DS + Meta.PULSE);
//                InternationalConnector.getSharedInstance().addData(Meta.PULSE + Meta.DS + Meta.PULSE);
            } else {
                g_iCounter++;
            }

            //news bulb deactivation
            DataStore.getSharedInstance().resetNewsBulbs();
            /*if (Settings.getNewsIndicatorExpiryTime() != -1) {
            if(newsCountDown==0){
                DataStore.getSharedInstance().resetNewsBulbs();
                     setNewsCountDownTime(Settings.getNewsIndicatorExpiryTime() * 60);
            }else{
                    setNewsCountDownTime((newsCountDown - 1));
                }
            }*/
//            DataStore.getSharedInstance().initFields();
            Sleep(1000);
        }
    }

    /**
     * Sleeps the theresd for the given interval
     */
    private void Sleep(long lDelay) {
        try {
            sleep(lDelay);
        } catch (Exception e) {
        }
    }
}

 