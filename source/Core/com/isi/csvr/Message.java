package com.isi.csvr;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 28, 2006
 * Time: 10:39:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class Message implements Comparable, Serializable {

    private String body;
    private String titile;
    private String id;
    private String date;
    private boolean isExpired = false;
    private int newMessage = 1;

    public Message(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitile() {
        return titile;
    }

    public void setTitile(String titile) {
        this.titile = titile;
    }

    public String getId() {
        return id;
    }

    public int getNewMessage() {
        return newMessage;
    }

    public void setNewMessage() {
        this.newMessage = 1;
    }

    public void setOldMessage() {
        this.newMessage = 0;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean expired) {
        isExpired = expired;
    }

    public int compareTo(Object o) {
        if (getDate().equals(((Message) o).getDate())) {
            return getId().compareTo(((Message) o).getId());
        }
        return getDate().compareTo(((Message) o).getDate());
    }
}
