package com.isi.csvr.mutualfund;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 12, 2003
 * Time: 9:00:58 AM
 * To change this template use Options | File Templates.
 */
public class MutualFundModel extends CommonTable implements TableModel,
        CommonTableInterface {

    private MutualFundStore store;

    /**
     * Constructor
     */
    public MutualFundModel(MutualFundStore store) {
        this.store = store;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        return store.size();
    }

    public Object getValueAt(int iRow, int iCol) {
        MutualFund fund = MutualFundStore.getInstance().getRecord(iRow);
        switch (iCol) {
            case 0:
                return "" + fund.getFundName();
            case 1:
                return "" + fund.getManager();
            case 2:
                return "" + fund.getCurrency();
            case 3:
                return "" + fund.getCategory();
            case 4:
                return "" + fund.getSubCategory();
            case 5:
                return "" + fund.getClassification();
            case 6:
                return "" + fund.getUnitPrice();
            case 7:
                return "" + fund.getYtdChange();
            case 8:
                return "" + fund.getValuationDate();
            default:
                return "0";
        }
    }


    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (iCol) {
            case 6:
            case 7:
            case 8:
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

//    public CustomizerRecord[] getCustomizerRecords() {
//        CustomizerRecord[] customizerRecords = new CustomizerRecord[3];
//        customizerRecords[0] = new CustomizerRecord(Language.getString("ROW_COLOR_1"), FIELD_BASIC_ROW1, Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1"),Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1"));
//        customizerRecords[1] = new CustomizerRecord(Language.getString("ROW_COLOR_2"), FIELD_BASIC_ROW2, Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2"),Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2"));
//        customizerRecords[2] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"),Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
//
//        return customizerRecords;
//    }
}