package com.isi.csvr.mutualfund;

import com.isi.csvr.event.ProcessListener;
import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 18, 2003
 * Time: 4:42:29 PM
 * To change this template use Options | File Templates.
 */
public class MutualFundPanel extends JPanel implements ItemListener, ProcessListener {
    private JComboBox managers;
    private JComboBox currencies;

    public MutualFundPanel() {
        createUI();
    }

    public void createUI() {
        setLayout(new FlowLayout(FlowLayout.LEADING));
        JLabel label = new JLabel(Language.getString("MANAGER"));
        add(label);
        managers = new JComboBox(new MutualFundManagerModel(MutualFundManagerModel.MANAGER));
        managers.setPreferredSize(new Dimension(150, 20));
        managers.addItem(Language.getString("ALL"));
        managers.addItemListener(this);
        add(managers);
        JLabel label2 = new JLabel(Language.getString("CURRENCY"));
        add(label2);
        currencies = new JComboBox(new MutualFundManagerModel(MutualFundManagerModel.CURRENCY));
        currencies.setPreferredSize(new Dimension(100, 20));
        currencies.addItem(Language.getString("ALL"));
        currencies.addItemListener(this);
        add(currencies);
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED)
            if (e.getSource() == managers)
                MutualFundStore.getInstance().setFilterIndex(managers.getSelectedIndex(), currencies.getSelectedIndex());
            else
                MutualFundStore.getInstance().setFilterIndex(managers.getSelectedIndex(), currencies.getSelectedIndex());
    }

    public void processCompleted(boolean completed, Object object) {
        if (completed) {
            managers.setEnabled(true);
            managers.updateUI();
            currencies.setEnabled(true);
            currencies.updateUI();
        } else {
            managers.setEnabled(false);
            managers.setPopupVisible(false);
            currencies.setEnabled(false);
            currencies.setPopupVisible(false);
        }
    }

    public void processSarted(Object object) {

    }

}
