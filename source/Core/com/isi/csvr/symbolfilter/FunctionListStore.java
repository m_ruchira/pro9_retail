package com.isi.csvr.symbolfilter;

import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.*;
import com.mubasher.formulagen.TWCompiler;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 7, 2003
 * Time: 12:19:25 PM
 * To change this template use Options | File Templates.
 */
public class FunctionListStore extends Symbols {

    private String caption = null;
    private String id = null;
    private String captions = null;
    private String function = null;
    private FunctionFilterInterface functionObj = null;
    private JInternalFrame parent;

    public FunctionListStore(byte symbolTypeIn) {
        super(symbolTypeIn);
    }

    public FunctionListStore(String symbols) {
        super(symbols, true);
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCaptions() {
        return captions;
    }

    public void setCaptions(String captions) {
        setCaption(filterCaption(UnicodeUtils.getNativeString(captions)));
        this.captions = UnicodeUtils.getNativeString(captions);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JInternalFrame getParent() {
        return parent;
    }

    public void setParent(JInternalFrame parent) {
        this.parent = parent;
    }

    public void setSymbols(String[] symbols) {

        super.clear();
        for (int i = 0; i < symbols.length; i++) {
            if (!symbols[i].trim().equals(Constants.NULL_STRING))
                appendSymbol(symbols[i]); // apend the symbol without the instrument type
        }
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
        createClass();
        FunctionFilter functionFilter = new FunctionFilter(functionObj);
        super.setFilter(functionFilter);
    }

    private void createClass() {
        try {
            // create the java file
            String className = "FunctionFilterObject" + System.currentTimeMillis();
            File file = new File(Settings.getAbsolutepath() + "formula/" + className + ".java");
            FileOutputStream out = new FileOutputStream(file);
            out.write("package com.isi.csvr.symbolfilter;\n".getBytes());
            out.write("\n".getBytes());
            out.write(("public class " + className + " implements FunctionFilterInterface {\n").getBytes());
            out.write("\tpublic boolean isValidSymbol(com.isi.csvr.shared.FunctionStockInterface o) {\n".getBytes());
            out.write("\t\treturn ".getBytes());
            out.write(function.getBytes());
            out.write(";\n".getBytes());
            out.write("\t}\n}".getBytes());
            out.flush();
            out.close();
            out = null;


            // load the compiler
            //NativeMethods.loadCompilerLibrary();
            //Class x = Class.forName("com.mubasher.formulagen.Compiler");

            // compile the java file
            //com.mubasher.formulagen.Compiler.compile(file.getAbsolutePath(), "./jittemp");
            //x.getDeclaredMethod("compile", new Class[]{String.class, String.class}).invoke(null, file.getAbsolutePath(), "./formula");
            TWCompiler.compile(file.getAbsolutePath(), Settings.getAbsolutepath() + "formula");
//            MubasherCompiler.compile(file.getAbsolutePath(), "./formula");


            // load class
            functionObj = (FunctionFilterInterface) Class.forName("com.isi.csvr.symbolfilter." + className, false, new FunctionLoader()).newInstance();

            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the string representation of the store
     *
     * @return string representation of the store
     */
    public String toString() {
        StringBuffer tempString = new StringBuffer();
        //PortfolioRecord record = null;

        tempString.append(ViewConstants.VC_ID);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(id);
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);

        tempString.append(ViewConstants.VC_CAPTIONS);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(UnicodeUtils.getUnicodeString(captions));
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);

        tempString.append(ViewConstants.VC_DEPTH_CALC_VALUE);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(function);

        return tempString.toString();
    }

    private String filterCaption(String caption) {
        try {
            String[] captions = caption.split("\\,");
            if (captions.length > 1) {
                return captions[Language.getLanguageID()];
            } else
                return caption;
        } catch (Exception e) {
            return caption;
        }
    }
}
