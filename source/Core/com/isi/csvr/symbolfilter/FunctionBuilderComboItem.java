package com.isi.csvr.symbolfilter;

import java.io.Serializable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class FunctionBuilderComboItem implements Comparable, Serializable {

    private String value;
    private String description;


    public FunctionBuilderComboItem(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return description;
    }

    public int compareTo(Object o) {
        return description.compareTo(((FunctionBuilderComboItem) o).getDescription());
    }

    public boolean equals(Object obj) {
        try {
            return description.equals(((FunctionBuilderComboItem) obj).getDescription());
        } catch (Exception e) {
            return false;
        }
    }
}