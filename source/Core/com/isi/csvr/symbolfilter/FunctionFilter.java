package com.isi.csvr.symbolfilter;

import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.shared.FunctionStockInterface;

import java.util.Enumeration;
import java.util.LinkedList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 18, 2003
 * Time: 12:35:52 PM
 * To change this template use Options | File Templates.
 */
public class FunctionFilter implements SymbolFilter {
    //    private Interpreter interpreter;
    private FunctionFilterInterface filter;

    public FunctionFilter(FunctionFilterInterface filter) {
//        interpreter = new Interpreter();
        this.filter = filter;
    }

    public String[] getFilteredList(String[] symbolsArray) {
        if ((!Application.isReady()) || (filter == null) || (filter.equals("null"))) {
            return new String[0];
        } else {
            try {
                LinkedList<String> filteredSymbols = new LinkedList<String>();
                Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
//                FunctionStockHolder functionStockHolder = new FunctionStockHolder();
//                interpreter.set("o", functionStockHolder);
                while (exchanges.hasMoreElements()) {
                    Exchange exchange = (Exchange) exchanges.nextElement();
                    if (exchange.isDefault()) {
                        try {

                            Symbols symbols = DataStore.getSharedInstance().getSymbolsObject(exchange.getSymbol());
                            for (int i = 0; i < symbols.getSymbols().length; i++) {
                                FunctionStockInterface stock = DataStore.getSharedInstance().getStockObject(symbols.getSymbols()[i]);
                                if (filter.isValidSymbol(stock)) {
                                    filteredSymbols.add(symbols.getSymbols()[i]);
                                }
                                stock = null;
                                /*Object o = interpreter.eval(filter);
                                if (((Boolean) o).booleanValue()) {
                                    filteredSymbols.add(symbols.getSymbols()[i]);
                                }
                                o = null;*/
                            }
                            symbols = null;

                        } catch (Exception e) {
                            //e.printStackTrace();

                        }
                    }
                    exchange = null;
                }
//                interpreter.unset("o");
                exchanges = null;


                String[] asSelected = filteredSymbols.toArray(new String[0]);
                filteredSymbols = null;
                return asSelected;
            } catch (Exception e2) {
                e2.printStackTrace();
                return new String[0];
            }
        }

    }

}
