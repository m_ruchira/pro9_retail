package com.isi.csvr.symbolfilter;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 18, 2003
 * Time: 2:31:09 PM
 * To change this template use Options | File Templates.
 */
public class FunctionBuilder extends JPanel
        implements Themeable, ActionListener, MouseListener, ExchangeListener {
    private static FunctionBuilder self = null;
    private FunctionStore functionStore;
    private SmartTable table;
    private boolean actionCanceled;
    // private FunctionListModel OperatorsModel;
    // private FunctionSelector equalitiesSelector;
    // private FunctionListModel functionListModel;
    private FunctionBuilderComboItem[] exchanges;
    private FunctionBuilderComboItem[] functions;
    private FunctionBuilderComboItem[] equalities;
    private FunctionBuilderComboItem[] operators;
    private String g_sInvalidChars = "~!#&*(),:;/\\";
    private JTextField txtCaption;
    private TWButton btnOK;
    private FunctionBuilderDialog functionBuilderDialog;
    private JButton btnAdd;
    private String[] captions;
    private JDialog dialog;

    public FunctionBuilder(JFrame parent) {
        //super(parent, true);
        captions = new String[Language.getLanguageIDs().length];
        buildUI();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        self = this;
    }

    public static FunctionBuilder getSharedInstance() {
        return self;
    }

    public void buildUI() {
        this.setLayout(new BorderLayout());
        //setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        //setClosable(true);
        table = new SmartTable();
        functionStore = FunctionStore.getSharedInstance();
        table.setModel(new FunctionTableModel(table, functionStore, Language.getList("FUNCTION_BUILDER_COLUMNS")));
        JScrollPane pane = new JScrollPane(table);
        this.add(pane, BorderLayout.CENTER);
        setSize(650, 310);
        //setResizable(false);
        table.setRowHeight(20);
        int[] rendIDs = {3, 4, 2, 2, 2, 1, 0};
        table.setDefaultRenderer(String.class, new FunctionTableRenderer(rendIDs));
        table.setDefaultRenderer(Number.class, new FunctionTableRenderer(rendIDs));
        table.getTableHeader().setResizingAllowed(false);
        table.getTableHeader().setReorderingAllowed(false);
        table.setRowSelectionAllowed(false);
        table.addMouseListener(this);
        table.setBorder(BorderFactory.createEmptyBorder());
        pane.setBorder(BorderFactory.createEmptyBorder());
        table.getColumnModel().getColumn(0).setPreferredWidth(20);
        table.getColumnModel().getColumn(1).setPreferredWidth(20);
        table.getColumnModel().getColumn(2).setPreferredWidth(210); //80
        table.getColumnModel().getColumn(3).setPreferredWidth(140);
        table.getColumnModel().getColumn(4).setPreferredWidth(120); //160
        table.getColumnModel().getColumn(5).setPreferredWidth(100);
        table.getColumnModel().getColumn(6).setPreferredWidth(80); //100

        exchanges = new FunctionBuilderComboItem[1];
        exchanges[0] = new FunctionBuilderComboItem("(!o.stock.getExchange().equals(\"\"))&& ", Language.getString("ALL"));

        functions = new FunctionBuilderComboItem[35];
        //Warning - Never change equations unles you arer sure what you are doing. don't even add a space
        functions[0] = new FunctionBuilderComboItem("o.getAvgTradeVolume()", Language.getString("AVG_TRADE_VOLUME"));
        functions[1] = new FunctionBuilderComboItem("o.getBestAskPrice()", Language.getString("BEST_ASK_PRICE"));
        functions[2] = new FunctionBuilderComboItem("o.getBestAskQuantity()", Language.getString("BEST_ASK_QUANTITY"));
        functions[3] = new FunctionBuilderComboItem("o.getBestBidPrice()", Language.getString("BEST_BID_PRICE"));
        functions[4] = new FunctionBuilderComboItem("o.getBestBidQuantity()", Language.getString("BEST_BID_QUANTITY"));
        functions[5] = new FunctionBuilderComboItem("o.getBidaskRatio()", Language.getString("BIDASK_RATIO"));
        functions[6] = new FunctionBuilderComboItem("o.getChange()", Language.getString("CHANGE"));
        functions[7] = new FunctionBuilderComboItem("o.getHigh()", Language.getString("HIGH"));
        functions[8] = new FunctionBuilderComboItem("o.getHighPriceOf52Weeks()", Language.getString("HIGH_PRICE_OF_52WEEKS"));
        functions[9] = new FunctionBuilderComboItem("o.getLastTradeValue()", Language.getString("LAST_TRADE"));
        functions[10] = new FunctionBuilderComboItem("o.getLastTradedPrice()", Language.getString("LAST_TRADEDPRICE"));
        functions[11] = new FunctionBuilderComboItem("o.getLow()", Language.getString("LOW"));
        functions[12] = new FunctionBuilderComboItem("o.getLowPriceOf52Weeks()", Language.getString("LOW_PRICE_OF_52WEEKS"));
        functions[13] = new FunctionBuilderComboItem("o.getMarketCap()", Language.getString("MARKET_CAP"));
        functions[14] = new FunctionBuilderComboItem("o.getMaxPrice()", Language.getString("MAX_PRICE"));
        functions[15] = new FunctionBuilderComboItem("o.getMinPrice()", Language.getString("MIN_PRICE"));
        functions[16] = new FunctionBuilderComboItem("o.getNoOfAsks()", Language.getString("NO_OF_ASKS"));
        functions[17] = new FunctionBuilderComboItem("o.getNoOfBids()", Language.getString("NO_OF_BIDS"));
        functions[18] = new FunctionBuilderComboItem("o.getNoOfTrades()", Language.getString("NO_OF_TRADES"));
        functions[19] = new FunctionBuilderComboItem("o.getPBR()", Language.getString("PBR"));
        functions[20] = new FunctionBuilderComboItem("o.getPER()", Language.getString("PER"));
        functions[21] = new FunctionBuilderComboItem("o.getPercentChange()", Language.getString("PERCENT_CHANGE"));
        functions[22] = new FunctionBuilderComboItem("o.getPreviousClosed()", Language.getString("PREVIOUS_CLOSED"));
        functions[23] = new FunctionBuilderComboItem("o.getTodaysClose()", Language.getString("TODAYS_CLOSE"));
        functions[24] = new FunctionBuilderComboItem("o.getTodaysOpen()", Language.getString("TODAYS_OPEN"));
        functions[25] = new FunctionBuilderComboItem("o.getTradeQuantity()", Language.getString("LASTTRADE_QTY"));
        functions[26] = new FunctionBuilderComboItem("o.getTurnover()", Language.getString("TURNOVER"));
        functions[27] = new FunctionBuilderComboItem("o.getVolume()", Language.getString("VOLUME"));
        functions[28] = new FunctionBuilderComboItem("o.getYield()", Language.getString("YIELD"));
        functions[29] = new FunctionBuilderComboItem("o.getTotalBidQty()", Language.getString("TOTAL_BID_QTY"));
        functions[30] = new FunctionBuilderComboItem("o.getTotalAskQty()", Language.getString("TOTAL_OFFER_QTY"));
        functions[31] = new FunctionBuilderComboItem("o.getSpread()", Language.getString("SPREAD"));
        functions[32] = new FunctionBuilderComboItem("o.getRange()", Language.getString("RANGE"));
        functions[33] = new FunctionBuilderComboItem("o.getPctRange()", Language.getString("PCT_RANGE"));
        functions[34] = new FunctionBuilderComboItem("o.getPctSpread()", Language.getString("PCT_SPREAD"));

        Arrays.sort(functions);

        equalities = new FunctionBuilderComboItem[6];
        //Warning - Never change equations unles you arer sure what you are doing. don't even add a space
        equalities[0] = new FunctionBuilderComboItem(" == ", Language.getString("EQUAL"));
        equalities[1] = new FunctionBuilderComboItem(" != ", Language.getString("NOT_EQUAL"));
        equalities[2] = new FunctionBuilderComboItem(" > ", Language.getString("GREATER_THAN"));
        equalities[3] = new FunctionBuilderComboItem(" >= ", Language.getString("GREATER_THAN_OR_EQUAL"));
        equalities[4] = new FunctionBuilderComboItem(" < ", Language.getString("LESS_THAN"));
        equalities[5] = new FunctionBuilderComboItem(" <= ", Language.getString("LESS_THAN_OR_EQUAL"));
        Arrays.sort(equalities);

        operators = new FunctionBuilderComboItem[2];
        //Warning - Never change equations unles you arer sure what you are doing. don't even add a space
        operators[0] = new FunctionBuilderComboItem(" && ", Language.getString("BOLD_AND"));
        operators[1] = new FunctionBuilderComboItem(" || ", Language.getString("BOLD_OR"));
        Arrays.sort(operators);

        functionBuilderDialog = new FunctionBuilderDialog(exchanges, functions, equalities);
        //functionBuilderDialog.show();

        JComboBox operatorList = new JComboBox(new FunctionListModel(operators));
        table.setDefaultEditor(Object.class, new FunctionListCellEditor(operatorList));
        TWTextField textField = new TWTextField();
        textField.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        table.setDefaultEditor(Number.class, new DefaultCellEditor(textField));

        // Top panel
        String[] namePanelWidths = {"100", "120", "100%", "130"};
        String[] namePanelHeights = {"25"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(namePanelWidths, namePanelHeights);
        JPanel namePanel = new JPanel(flexGridLayout);
        JLabel lblCaption = new JLabel(Language.getString("NAME"), JTextField.CENTER);
        namePanel.add(lblCaption);
        txtCaption = new JTextField();
        namePanel.add(txtCaption);
        namePanel.add(new JLabel()); // dummy component
        txtCaption.setDocument(new LimitedLengthDocument(20));
        btnAdd = new JButton(Language.getString("ADD_FUNCTION_UNDERLINE"));
        btnAdd.setBorder(BorderFactory.createEmptyBorder());
        btnAdd.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnAdd.setContentAreaFilled(false);
        btnAdd.setActionCommand("ADD");
        btnAdd.addActionListener(this);
        namePanel.add(btnAdd);
        this.add(namePanel, BorderLayout.NORTH);

        // bottom button panel
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        btnOK = new TWButton(Language.getString("OK"));
        btnOK.setActionCommand("OK");
        btnOK.addActionListener(this);
        buttonPanel.add(btnOK);
        TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.addActionListener(this);
        buttonPanel.add(btnCancel);
        this.add(buttonPanel, BorderLayout.SOUTH);
        //setTitle(Language.getString("FUNCTION_BUILDER"));
        //setDefaultCloseOperation(HIDE_ON_CLOSE);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
    }

    private void loadExchanges() {
        //Warning - Never change equations unles you arer sure what you are doing. don't even add a space
        if (ExchangeStore.getSharedInstance().count() > 0) {
            Enumeration records = ExchangeStore.getSharedInstance().getExchanges();
            int i = 1;
            //todo - 21/12/2006
            if (ExchangeStore.getSharedInstance().count() == 1) {
                exchanges = new FunctionBuilderComboItem[1];
                i = 0;
            } else {
                exchanges = new FunctionBuilderComboItem[ExchangeStore.getSharedInstance().defaultCount() + 1];
                exchanges[0] = new FunctionBuilderComboItem("(!o.getExchange().equals(\"\"))&& ", Language.getString("ALL"));
                i = 1;
            }

            while (records.hasMoreElements()) {
                Exchange exchange = (Exchange) records.nextElement();
                if (exchange.isDefault()) { // Bug ID <#0019> only default exchanges should be listed
                    exchanges[i] = new FunctionBuilderComboItem("(o.getExchange().equals(\"" + exchange.getSymbol() + "\"))&& ", exchange.getDescription());
                    exchange = null;
                    i++;
                }
            }
            records = null;
            try {
                Arrays.sort(exchanges, 1, exchanges.length);
            } catch (Exception e) {
            }
            functionBuilderDialog.setExchanges(exchanges);
        }
    }

    public String getFunction() {
//        show();
        if (isActionCanceled())
            return null;
        else
            return functionStore.getFunction();
    }

    public void setFunction(String function) {
        if (function != null) {
            try {
                functionStore.removeFunction();

                function = function.replaceAll("\\(\\)", "{}");
                function = function.replaceAll("\\(", "");
                function = function.replaceAll("\\)", "");
                function = function.replaceAll("\\{\\}", "()");
                String[] fields = function.split(" ");

                int i = 0;
                int row = 0;
                FunctionBuilderComboItem value;
                while (i < fields.length) {
                    value = getExchange(fields[i++]);
                    if (value == null)
                        break;
                    else
                        functionStore.setExchange(row, value);

                    value = getFunction(fields[i++]);
                    if (value == null)
                        break;
                    else
                        functionStore.setFunction(row, value);

                    value = getEquality(fields[i++]);
                    if (value == null)
                        break;
                    else
                        functionStore.setEquality(row, value);

                    String sValue = fields[i++];
                    if ((sValue != null) && (sValue.endsWith("D")))
                        functionStore.setValue(row, sValue.substring(0, sValue.length() - 1));
                    else
                        functionStore.setValue(row, sValue);

                    value = getOperator(fields[i++]);
                    if (value == null)
                        break;
                    else
                        functionStore.setOperator(row, value);

                    row++;
                }
            } catch (Exception e) {
            }
        } else {
            functionStore.removeFunction();
        }
    }

    public String getName() {
        if (isActionCanceled())
            return null;
        else
            return getCaptions(txtCaption.getText().trim());
    }

    public void setName(String name) {

        int i = 0;
        StringTokenizer oCaptions = new StringTokenizer(name, ",");
        if (oCaptions.countTokens() == 0) {
            for (int j = 0; j < captions.length; j++) {
                captions[j] = "";
            }
        } else if (oCaptions.countTokens() == 1) {
            for (int j = 0; j < captions.length; j++) {
                captions[j] = name;
            }
        } else {
            try {
                while (oCaptions.hasMoreElements()) {
                    captions[i] = oCaptions.nextToken();
                    i++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        txtCaption.setText(getCaption());
    }

    public String getCaption() {
        String str = "";
        try {
            str = captions[Language.getLanguageID()];
            if (str == null) {
                str = captions[Language.getEnglishLangID()];
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            str = captions[Language.getEnglishLangID()];
        }
        return str;
    }

    private String getCaptions(String sCaption) {
        int iCount = captions.length;
        for (int i = 0; i < iCount; i++) {
            if ((captions[i] == null) || (captions[i].equals("") || (!captions[i].equals(sCaption))))
                captions[i] = sCaption;
        }

        try {
            captions[Language.getLanguageID()] = sCaption;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String sCaptions = "";
        for (int i = 0; i < iCount; i++) {
            sCaptions += ("," + captions[i]);
        }

        return sCaptions.substring(1);
    }

    public void show() {
        setActionCanceled(true);
        dialog = new JDialog(Client.getInstance().getFrame(), true);
        dialog.setTitle(Language.getString("FUNCTION_BUILDER"));
        dialog.setResizable(false);
        dialog.setSize(650, 320);
        dialog.getContentPane().add(this);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setLocationRelativeTo(Client.getInstance().getDesktop());
        dialog.setVisible(true);
        dialog.repaint();
        //SwingUtilities.updateComponentTreeUI(dialog);
        GUISettings.applyOrientation(dialog);
        super.show();
    }

    public void update() {
        table.updateUI();
    }

    public FunctionBuilderComboItem getAndItem() {
        return operators[0];
    }

    private FunctionBuilderComboItem getExchange(String method) {
        try {
            String methodValue = method.replaceAll("\\(", "").replaceAll("\\)", "");
            for (int i = 0; i < exchanges.length; i++) {
                FunctionBuilderComboItem item = exchanges[i];
                String itemValue = item.getValue().trim().replaceAll("\\(", "").replaceAll("\\)", "");

                if (itemValue.equals(methodValue)) { // compare without brackets
                    return item;
                }
                item = null;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private FunctionBuilderComboItem getFunction(String method) {
        for (int i = 0; i < functions.length; i++) {
            FunctionBuilderComboItem item = functions[i];
            if (item.getValue().trim().equals(method)) {
                return item;
            }
            item = null;
        }
        return null;
    }

    private FunctionBuilderComboItem getOperator(String method) {
        for (int i = 0; i < operators.length; i++) {
            FunctionBuilderComboItem item = operators[i];
            if (item.getValue().trim().equals(method)) {
                return item;
            }
            item = null;
        }
        return null;
    }

    private FunctionBuilderComboItem getEquality(String method) {
        for (int i = 0; i < equalities.length; i++) {
            FunctionBuilderComboItem item = equalities[i];
            if (item.getValue().trim().equals(method)) {
                return item;
            }
            item = null;
        }
        return null;
    }

    public void applyTheme() {
        btnAdd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/function_add.gif"));
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(table);
        table.getTableHeader().setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
        table.getTableHeader().setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
        table.updateUI();
        SwingUtilities.updateComponentTreeUI(functionBuilderDialog);
        //SwingUtilities.updateComponentTreeUI(dialog);
    }

    private boolean isValidName() {
        int iRestrctedLen = g_sInvalidChars.length();
        int i = 0;

        while (i < iRestrctedLen) {
            if (txtCaption.getText().indexOf(g_sInvalidChars.substring(i, i + 1)) >= 0)
                return false;
            i++;
        }
        return true;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("CANCEL")) {
            setActionCanceled(true);
            dialog.dispose();
            //hide();
            //repaint();
            //updateUI();
        } else if (e.getActionCommand().equals("ADD")) {
            functionBuilderDialog.addNewFunction();
        } else {
            okPressed();

        }
    }

    public void okPressed() {
        if (txtCaption.getText().trim().equals("")) {
            new ShowMessage(false, Language.getString("MSG_FILTER_NAME_BLANK"), "E");
        } else if (isValidName()) {
            if (functionStore.size() == 0) {
                new ShowMessage(false, Language.getString("MSG_NO_FUNCTIONS"), "E");
            } else {
                setVisible(false);
                setActionCanceled(false);
                dialog.dispose();
                //update();
            }
        } else {
            new ShowMessage(false, Language.getString("MSG_INVALID_CHARS"), "E");
        }
    }

    public boolean isActionCanceled() {
        return actionCanceled;
    }

    public void setActionCanceled(boolean actionCanceled) {
        this.actionCanceled = actionCanceled;
    }

    private void removeItem(int index) {
        ShowMessage message = new ShowMessage(Language.getString("MSG_REMOVE_FUNCTION"), "W", true);
        int result = message.getReturnValue();
        if (result == 1) {
            functionStore.removeFunction(index);
        }
    }

    public void mouseClicked(MouseEvent e) {
        int row = table.rowAtPoint(e.getPoint());
        int col = table.columnAtPoint(e.getPoint());
        if (col == 0) {
            FunctionBuilderComboItem field = (FunctionBuilderComboItem) FunctionStore.getSharedInstance().getFunction(row);
            if (field != null)
                removeItem(row);
        } else if (col == 1) {
            functionBuilderDialog.editFunction(row);
        }
        table.updateUI();
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        loadExchanges();
    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }
}
