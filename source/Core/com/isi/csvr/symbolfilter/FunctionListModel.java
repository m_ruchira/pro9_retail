package com.isi.csvr.symbolfilter;

import javax.swing.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class FunctionListModel extends DefaultComboBoxModel {

    private FunctionBuilderComboItem[] functions = null;
    //private JComboBox parent;

    public FunctionListModel(FunctionBuilderComboItem[] functions) {
        this.functions = functions;
    }

    public void setList(FunctionBuilderComboItem[] functions) {
        this.functions = functions;
    }

    public Object getSelectedItem() {
        try {
            return (FunctionBuilderComboItem) super.getSelectedItem();
        } catch (Exception ex) {
            return super.getSelectedItem();
        }
    }

    public int getSize() {
        try {
            return functions.length;
        } catch (Exception e) {
            return 0;
        }
    }

    public Object getElementAt(int index) {
        return functions[index];
    }
}