package com.isi.csvr.symbolfilter;

import com.isi.csvr.shared.FunctionStockInterface;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 14, 2006
 * Time: 11:41:24 AM
 */
public interface FunctionFilterInterface {

    public boolean isValidSymbol(FunctionStockInterface functionStockInterface);
}
