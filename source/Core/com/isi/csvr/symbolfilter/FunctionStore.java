package com.isi.csvr.symbolfilter;

import com.isi.csvr.shared.DynamicArray;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 18, 2003
 * Time: 3:50:41 PM
 * To change this template use Options | File Templates.
 */
public class FunctionStore {
    private static FunctionStore self = null;
    private DynamicArray functionArray;

    private FunctionStore() {
        functionArray = new DynamicArray();
    }

    public static FunctionStore getSharedInstance() {
        if (self == null) {
            self = new FunctionStore();
        }
        return self;
    }

    public void setExchange(int index, Object exchange) {
        if (index == functionArray.size()) {
            Object[] row = new Object[5];
            functionArray.add(row);
            row[0] = exchange;
            row = null;
        } else if (index < functionArray.size()) {
            Object[] row = (Object[]) functionArray.get(index);
            row[0] = exchange;
        }
    }

    public void setFunction(int index, Object function) {
        if (index == functionArray.size()) {
            Object[] row = new Object[5];
            functionArray.add(row);
            row[1] = function;
            row = null;
        } else if (index < functionArray.size()) {
            Object[] row = (Object[]) functionArray.get(index);
            row[1] = function;
        }
    }

    public void setEquality(int index, Object equality) {
        if (index == functionArray.size()) {
            Object[] row = new Object[5];
            functionArray.add(row);
            row[2] = equality;
            row = null;
        } else if (index < functionArray.size()) {
            Object[] row = (Object[]) functionArray.get(index);
            row[2] = equality;
        }
    }

    public void setValue(int index, Object value) {
        if (index == functionArray.size()) {
            Object[] row = new Object[5];
            functionArray.add(row);
            row[3] = value;
            row = null;
        } else if (index < functionArray.size()) {
            Object[] row = (Object[]) functionArray.get(index);
            row[3] = value;
        }
    }

    public void setOperator(int index, Object operator) {
        if (index == functionArray.size()) {
            Object[] row = new Object[5];
            functionArray.add(row);
            row[4] = operator;
            row = null;
        } else if (index < functionArray.size()) {
            Object[] row = (Object[]) functionArray.get(index);
            row[4] = operator;
        }
        getFunction();
    }

    public Object getExchange(int index) {
        return ((Object[]) functionArray.get(index))[0];
    }

    public Object getFunction(int index) {
        return ((Object[]) functionArray.get(index))[1];
    }

    public Object getEquality(int index) {
        return ((Object[]) functionArray.get(index))[2];
    }

    public Object getValue(int index) {
        return ((Object[]) functionArray.get(index))[3];
    }

    public Object getOperator(int index) {
        return ((Object[]) functionArray.get(index))[4];
    }

    public void removeFunction(int index) {
        try {
            functionArray.remove(index);
        } catch (Exception e) {

        }
    }

    public void removeFunction() {
        functionArray.clear();
    }

    public int size() {
        return functionArray.size();
    }

    public String getFunction() {
        try {
            StringBuffer buffer = new StringBuffer();
            buffer.append("(");
            for (int i = 0; i < functionArray.size(); i++) {
                Object[] line = (Object[]) functionArray.get(i);
                if (line[0] != null) {
                    buffer.append(((FunctionBuilderComboItem) line[0]).getValue());
                    buffer.append("(");
                    buffer.append(((FunctionBuilderComboItem) line[1]).getValue());
                    buffer.append(((FunctionBuilderComboItem) line[2]).getValue());
                    buffer.append(line[3]);
                    if (!((String) line[3]).endsWith("D"))
                        buffer.append("D"); // make sure the value is of type double
                    buffer.append(")");
                    // ignore operator for last item of the selection
                    if (((i + 1) < functionArray.size()) && (functionArray.get(i + 1) != null))
                        buffer.append(((FunctionBuilderComboItem) line[4]).getValue());

//                    (((stock.getExchange().equals(TDWL)) && stock.getPercentChange() > 1D))
                } else {
                    if (i == 0)
                        return null;
                    else
                        break;
                }
            }
            buffer.append(")");
            System.out.println(buffer.toString());
            if (buffer.toString().equals("()"))
                return null;
            else
                return buffer.toString();
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }
}
