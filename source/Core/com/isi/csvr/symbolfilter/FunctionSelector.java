package com.isi.csvr.symbolfilter;

import com.isi.csvr.shared.NonNavigatable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.ExtendedComboInterface;
import com.isi.csvr.util.ExtendedComboUI;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class FunctionSelector extends JComboBox implements ExtendedComboInterface, Themeable, NonNavigatable {
    protected int popupWidth;
    protected ExtendedComboUI ui = null;
    private FunctionListModel model = null;
    //private JComboBox parent;

    public FunctionSelector() {
        super();
        Theme.registerComponent(this);
        ui = new ExtendedComboUI();
        setUI(ui);
        popupWidth = 0;
        calculatePopupWidth();
    }

    public FunctionSelector(ComboBoxModel aModel) {
        super(aModel);
        Theme.registerComponent(this);
        model = (FunctionListModel) aModel;
        ui = new ExtendedComboUI();
        setUI(ui);
        popupWidth = 0;
        calculatePopupWidth();
    }

/*    public CompanySelector(final Object[] items) {
        super(items);
        ui = new CompanySelectorUI();
        setUI(ui);
        popupWidth = 0;
    }

    public CompanySelector(Vector items) {
        super(items);
        ui = new CompanySelectorUI();
        setUI(ui);
        popupWidth = 0;
    }*/


    public void setPopupWidth(int width) {
        popupWidth = width;
    }

    public Dimension getPopupSize() {
        Dimension size = getSize();
        if (popupWidth < size.width) popupWidth = size.width;
        return new Dimension(popupWidth, size.height);
    }

    /*public void setParent(JComboBox parent){
        this.parent = parent;
    }*/

    private void calculatePopupWidth() {
        int size = 0;
        int length = 0;
        int maxLength = 0;
        FunctionBuilderComboItem item;
        JLabel lbl = new JLabel();
        FontMetrics metrix = lbl.getFontMetrics(lbl.getFont());
        size = model.getSize();
        for (int j = 0; j < size; j++) {
            item = (FunctionBuilderComboItem) model.getElementAt(j);
            length = metrix.stringWidth(item.getDescription());
            if (maxLength < length) {
                maxLength = length;
            }
        }
        popupWidth = maxLength + 25;
        lbl = null;
    }

    public void applyTheme() {

        calculatePopupWidth();
        super.updateUI();
        setUI(ui);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void updateUI() {

    }

//    public void actionPerformed(ActionEvent e){
//        super.actionPerformed(e);
//        System.err.println("******************************");
//    }
}

