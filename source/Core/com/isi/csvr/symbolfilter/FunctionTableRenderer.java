// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.symbolfilter;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;


public class FunctionTableRenderer extends TWBasicTableRenderer {
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static ImageIcon deteteImage;
    private static ImageIcon editImage;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private String g_sNA = "NA";
    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private DefaultTableCellRenderer lblRenderer;
    private String editToolTip;
    private String deleteToolTip;


    public FunctionTableRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;

        reload();

        try {
            deteteImage = new ImageIcon("images/Theme" + Theme.getID() + "/function_delete.gif");
        } catch (Exception e) {
            deteteImage = null;
        }
        try {
            editImage = new ImageIcon("images/Theme" + Theme.getID() + "/function_change.gif");
        } catch (Exception e) {
            editImage = null;
        }

        oPriceFormat = new TWDecimalFormat(" ###,##0.## ");

        editToolTip = Language.getString("EDIT");
        deleteToolTip = Language.getString("DELETE");

        g_iStringAlign = JLabel.LEADING;
        g_iNumberAlign = JLabel.TRAILING;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
    }

    public static void reload() {
        try {
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
        } catch (Exception e) {
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
        }
        try {
            deteteImage = new ImageIcon("images/Theme" + Theme.getID() + "/function_delete.gif");
        } catch (Exception e) {
            deteteImage = null;
        }
        try {
            editImage = new ImageIcon("images/Theme" + Theme.getID() + "/function_change.gif");
        } catch (Exception e) {
            editImage = null;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void propertyChanged(int property) {
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        foreground = g_oFG1;
        background = g_oBG1;

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setToolTipText(null);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText(oPriceFormat.format(((Double) value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    lblRenderer.setToolTipText(null);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    lblRenderer.setToolTipText(null);
                    break;
                case 3: // Delete image
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    if (value.equals(""))
                        lblRenderer.setIcon(null);
                    else
                        lblRenderer.setIcon(deteteImage);
                    lblRenderer.setToolTipText(deleteToolTip);
                    break;
                case 4: // Edit Image
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    if (value.equals(""))
                        lblRenderer.setIcon(null);
                    else
                        lblRenderer.setIcon(editImage);
                    lblRenderer.setToolTipText(editToolTip);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
        }


        return lblRenderer;
    }
}