package com.isi.csvr.symbolfilter;

import com.isi.csvr.Client;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 21, 2003
 * Time: 8:58:44 AM
 * To change this template use Options | File Templates.
 */
public class FunctionBuilderDialog extends JPanel implements ActionListener, Themeable {

    private TWComboBox exchangeList;
    private TWComboBox functionList;
    private TWComboBox equalitiesList;
    private FunctionListModel exchangeModel;
    private JTextField value;
    private int editingRow = -1;
    private JLabel lblFunction;
    private JDialog dialog;


    public FunctionBuilderDialog(FunctionBuilderComboItem[] exchanges, FunctionBuilderComboItem[] functions, FunctionBuilderComboItem[] equalities) {
        //super(parent, Language.getString("ADD_FUNCTION"), true);
        //setResizable(false);
        exchangeModel = new FunctionListModel(exchanges);
        exchangeList = new TWComboBox(exchangeModel);
        functionList = new TWComboBox(new FunctionListModel(functions));
        functionList.setSelectedIndex(0);
        equalitiesList = new TWComboBox(new FunctionListModel(equalities));
        equalitiesList.setSelectedIndex(0);
        value = new JTextField();

        buildGUI();
        Theme.registerComponent(this);
    }

    private void buildGUI() {
        String[] heights = {"20", "20", "20", "20", "25"};
        String[] widths = {"100", "170"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(widths, heights, 5, 5, true, true);
        this.setLayout(flexGridLayout);

        JLabel lblExchanges = new JLabel(Language.getString("EXCHANGE"));
        lblExchanges.setHorizontalAlignment(SwingConstants.LEADING);
        this.add(lblExchanges);
        this.add(exchangeList);

        JLabel lblField = new JLabel(Language.getString("FIELD"));
        lblField.setHorizontalAlignment(SwingConstants.LEADING);
        this.add(lblField);
        this.add(functionList);

        lblFunction = new JLabel(Language.getString("CONDITION"));
        lblFunction.setHorizontalAlignment(SwingConstants.LEADING);
        this.add(lblFunction);
        this.add(equalitiesList);

        JLabel lblValue = new JLabel(Language.getString("VALUE"));
        lblValue.setHorizontalAlignment(SwingConstants.LEADING);
        this.add(lblValue);
        //value.setDocument(new ValueFormatter(ValueFormatter.DECIMAL)); Bug ID <#0020> must allow negative figures
        value.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        this.add(value);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        TWButton btnOK = new TWButton(Language.getString("OK"));
        btnOK.setActionCommand("OK");
        btnOK.setPreferredSize(new Dimension(80, 20));
        btnOK.addActionListener(this);
        buttonPanel.add(btnOK);

        buttonPanel.add(Box.createHorizontalGlue());

        TWButton btnCancel = new TWButton(Language.getString("CLOSE"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.setPreferredSize(new Dimension(80, 20));
        btnCancel.addActionListener(this);
        buttonPanel.add(btnCancel);

        this.add(new JLabel()); // dummy
        this.add(buttonPanel);

        //setDefaultCloseOperation(HIDE_ON_CLOSE);
        //pack();
        //setLocationRelativeTo(FunctionBuilder.getSharedInstance());
        GUISettings.applyOrientation(buttonPanel);
        GUISettings.applyOrientation(this);
    }

    public void setExchanges(FunctionBuilderComboItem[] comboItems) {
        try {
            exchangeModel.setList(comboItems);
            exchangeList.updateUI();
            if (exchangeList.getSelectedIndex() < 0) {
                exchangeList.setSelectedIndex(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFunction(FunctionBuilderComboItem exchange, FunctionBuilderComboItem field, FunctionBuilderComboItem function, String value) {
        int newIndex;
        if (editingRow < 0) {
            newIndex = FunctionStore.getSharedInstance().size();
        } else {
            newIndex = editingRow;
        }
        FunctionStore.getSharedInstance().setExchange(newIndex, exchange);
        FunctionStore.getSharedInstance().setFunction(newIndex, field);
        FunctionStore.getSharedInstance().setEquality(newIndex, function);
        FunctionStore.getSharedInstance().setValue(newIndex, value);
        FunctionStore.getSharedInstance().setOperator(newIndex, FunctionBuilder.getSharedInstance().getAndItem());
        try {
            if (FunctionStore.getSharedInstance().getOperator(newIndex - 1) == null) {
                FunctionStore.getSharedInstance().setOperator(newIndex - 1, FunctionBuilder.getSharedInstance().getAndItem());
            }
        } catch (Exception e) {
            // let fail if the pre row does not exist
        }
        FunctionBuilder.getSharedInstance().update();

    }

    public void addNewFunction() {
        value.setText("0");
        editingRow = -1;

        this.showDialog(Language.getString("ADD_FUNCTION"));
    }

    public void editFunction(int row) {
        if (row < FunctionStore.getSharedInstance().size()) {
            editingRow = row;
            FunctionBuilderComboItem field = (FunctionBuilderComboItem) FunctionStore.getSharedInstance().getFunction(row);
            FunctionListModel model = (FunctionListModel) functionList.getModel();
            for (int i = 0; i < model.getSize(); i++) {
                if (model.getElementAt(i).equals(field))
                    functionList.setSelectedIndex(i);
            }
            field = (FunctionBuilderComboItem) FunctionStore.getSharedInstance().getEquality(row);
            model = (FunctionListModel) equalitiesList.getModel();
            for (int i = 0; i < model.getSize(); i++) {
                if (model.getElementAt(i).equals(field))
                    equalitiesList.setSelectedIndex(i);
            }
            field = (FunctionBuilderComboItem) FunctionStore.getSharedInstance().getExchange(row);
            model = (FunctionListModel) exchangeList.getModel();
            for (int i = 0; i < model.getSize(); i++) {
                if (model.getElementAt(i).equals(field))
                    exchangeList.setSelectedIndex(i);
            }
            value.setText((String) FunctionStore.getSharedInstance().getValue(row));
            this.showDialog(Language.getString("EDIT_FUNCTION"));
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            addFunction((FunctionBuilderComboItem) exchangeList.getSelectedItem(), (FunctionBuilderComboItem) functionList.getSelectedItem(),
                    (FunctionBuilderComboItem) equalitiesList.getSelectedItem(), value.getText());
            //hide();
            if (dialog != null) {
                dialog.dispose();
            }
        } else {
            //hide();
            if (dialog != null) {
                dialog.dispose();
            }
        }
    }

    public void showDialog(String title) {

        dialog = new JDialog(Client.getInstance().getFrame(), true);
        if (title != null && !title.equals("")) {
            dialog.setTitle(title);
        } else {
            dialog.setTitle(Language.getString("ADD_FUNCTION"));
        }
        dialog.setResizable(false);
        dialog.setSize(290, 165);
        dialog.getContentPane().add(this);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setLocationRelativeTo(FunctionBuilder.getSharedInstance());
        dialog.setVisible(true);
        dialog.repaint();
        //SwingUtilities.updateComponentTreeUI(dialog);
        GUISettings.applyOrientation(dialog);
        super.show();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        //SwingUtilities.updateComponentTreeUI(dialog);
    }
}
