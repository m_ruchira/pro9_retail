package com.isi.csvr;

import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.NonEscapable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 21, 2005
 * Time: 10:14:17 AM
 */
public class WorkInProgressIndicator extends JDialog implements ProgressListener, NonEscapable {

    //private SimpleProgressBar progressBar;
    public static final int DOWNLOADING = 0;
    public static final int EXITING = 1;
    public static final int INITIALIZIG = 2;
    public static final int EXPOPRTING = 3;
    public static final int APPLYING_THEME = 4;
    public static final int APPLYING_WORKSPACE = 5;
    public static final int UPDATING_METASTOCK = 6;
    public static final int DOWNLOADING_NONBLOKING = 7;
    public static final int APPLYING_OFFLINE_DATA = 8;
    public static final int CHART_LOADING_IN_PROGRESS = 10;
    public static final int SIMULATION_IN_PROGRESS = 11;
    public static final int APPLYING_SETTINGS = 12;
    public static final int UPDATING_TRADESTATION = 13;
    public static final int APPLYING_TABLE_SETTINGS = 14;
    private static WorkInProgressIndicator selectedObject = null;

    public WorkInProgressIndicator(int type) throws HeadlessException {
        super(Client.getInstance().getFrame(), true);

        String path = System.getProperties().getProperty("user.dir").replaceAll("\\\\", "/");
        String message;
        switch (type) {
            case DOWNLOADING:
                message = Language.getString("ESSENTIAL_DOWNLOAD_MESSAGE").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case EXITING:
                message = Language.getString("APPLICATION_EXITING_MESSAGE").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case INITIALIZIG:
                message = Language.getString("APPLICATION_INITITLIZING_MESSAGE").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case EXPOPRTING:
                message = Language.getString("MSG_EXPORT_IN_PROGRESS").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case APPLYING_THEME:
                message = Language.getString("MSG_APPLY_THEME_PROGRESS").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case APPLYING_WORKSPACE:
                message = Language.getString("MSG_APPLY_WORKSPACE_PROGRESS").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case UPDATING_METASTOCK:
                message = Language.getString("MSG_UPDATING_METASTOCK").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case UPDATING_TRADESTATION:
                message = Language.getString("MSG_UPDATING_TRADESTATION").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case DOWNLOADING_NONBLOKING:
                message = Language.getString("MSG_DOWNLOADING").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case APPLYING_OFFLINE_DATA:
                message = Language.getString("APPLYING_OFFLINE_DATA").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case CHART_LOADING_IN_PROGRESS:
                message = Language.getString("CHART_LOADING_PROGRESS").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case APPLYING_SETTINGS:
                message = Language.getString("APPLYING_SETTINGS").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            case APPLYING_TABLE_SETTINGS:
                message = Language.getString("MSG_APPLY_TABLESETTINGS_PROGRESS").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
            default:
                message = Language.getString("APPLICATION_INITITLIZING_MESSAGE").replaceAll("\\[PATH\\]", path);
                setModal(false);
                break;
        }
        /*if (type == DOWNLOADING) {
            message = Language.getString("ESSENTIAL_DOWNLOAD_MESSAGE").replaceAll("\\[PATH\\]", path);
        } else if (type == EXITING) {
            message = Language.getString("APPLICATION_EXITING_MESSAGE").replaceAll("\\[PATH\\]", path);
        } else if (type == EXPOPRTING) {
            message = Language.getString("MSG_EXPORT_IN_PROGRESS").replaceAll("\\[PATH\\]", path);
            setModal(false);
        } else if (type == APPLYING_THEME) {
//            System.out.println("apply theme called in  work in progressee");
            message = Language.getString("MSG_APPLY_THEME_PROGRESS").replaceAll("\\[PATH\\]", path);
            setModal(false);
        } else if (type == APPLYING_WORKSPACE) {
//            System.out.println("apply worksapce called in  work in progressee");
            message = Language.getString("MSG_APPLY_WORKSPACE_PROGRESS").replaceAll("\\[PATH\\]", path);
            setModal(false);
        } else if (type == UPDATING_METASTOCK) {
//            System.out.println("apply worksapce called in  work in progressee");
            message = Language.getString("MSG_UPDATING_METASTOCK").replaceAll("\\[PATH\\]", path);
            setModal(false);
        } else if (type == DOWNLOADING_NONBLOKING) {
//            System.out.println("apply worksapce called in  work in progressee");
            message = Language.getString("MSG_DOWNLOADING").replaceAll("\\[PATH\\]", path);
            setModal(false);
        } else {
            message = Language.getString("APPLICATION_INITITLIZING_MESSAGE").replaceAll("\\[PATH\\]", path);
            setModal(false);
        }*/
        setLayout(new BorderLayout());
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(GUISettings.getTWFrameBorder());
        this.add(panel, BorderLayout.CENTER);

        JLabel label = new JLabel(message);
        panel.add(label, BorderLayout.CENTER);
        setSize(200, 100);
        setUndecorated(true);
        setLocationRelativeTo(Client.getInstance().getFrame());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    /*public void setVisible(boolean b) {
        super.setVisible(b);    //To change body of overridden methods use File | Settings | File Templates.
//        if(b){
//            selectedObject = this;
//        } else {
//            selectedObject = null;
//        }
    }*/

    public static WorkInProgressIndicator getSelectedIndicator() {
        return selectedObject;
    }

    public void setProcessStarted(String id) {

    }

    public void setMax(String id, int max) {
    }

    public void setProcessCompleted(String id, boolean sucess) {

    }

    public void setProgress(String id, int progress) {
    }
}
