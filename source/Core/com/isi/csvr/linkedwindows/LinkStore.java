package com.isi.csvr.linkedwindows;

import com.isi.csvr.Client;
import com.isi.csvr.MultiDialogWindow;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.chart.GraphFrame;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.marketdepth.CombinedDepthModel;
import com.isi.csvr.marketdepth.DepthOrderModel;
import com.isi.csvr.marketdepth.DepthPriceModel;
import com.isi.csvr.marketdepth.DepthSpecialOrderModel;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trade.TimeNSalesModel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;


/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 17, 2008
 * Time: 12:21:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class LinkStore {

    public static String linked = "Linked";
    public static String LINK_RED = "red";
    public static String LINK_GREEN = "green";
    public static String LINK_BLUE = "blue";
    public static String LINK_YELLOW = "yellow";
    public static String LINK_WHITE = "white";
    public static String LINK_NONE = "notLinked";
    private static LinkStore self;
    private Hashtable<String, InternalFrame> linkedWindows;
    private Hashtable<String, ArrayList<InternalFrame>> linkedGroups;
    private ArrayList<InternalFrame> redLinkedGroup;
    private ArrayList<InternalFrame> greenLinkedGroup;
    private ArrayList<InternalFrame> blueLinkedGroup;
    private ArrayList<InternalFrame> yellowLinkedGroup;
    private ArrayList<InternalFrame> whiteLinkedGroup;
    private ArrayList<String> linkGroupRegister;

    //  private Hashtable<String, InternalFrame>

    private LinkStore() {
        linkedWindows = new Hashtable<String, InternalFrame>();
        linkedGroups = new Hashtable<String, ArrayList<InternalFrame>>();
        redLinkedGroup = new ArrayList<InternalFrame>();
        greenLinkedGroup = new ArrayList<InternalFrame>();
        blueLinkedGroup = new ArrayList<InternalFrame>();
        yellowLinkedGroup = new ArrayList<InternalFrame>();
        whiteLinkedGroup = new ArrayList<InternalFrame>();
        linkGroupRegister = new ArrayList<String>();

        linkedGroups.put(LINK_RED, redLinkedGroup);
        linkedGroups.put(LINK_BLUE, blueLinkedGroup);
        linkedGroups.put(LINK_GREEN, greenLinkedGroup);
        linkedGroups.put(LINK_YELLOW, yellowLinkedGroup);
        linkedGroups.put(LINK_WHITE, whiteLinkedGroup);
    }

    public static LinkStore getSharedInstance() {
        if (self == null) {
            self = new LinkStore();
        }
        return self;
    }

    public void putLinkedWindow(String key, InternalFrame frame) {

        linkedWindows.put(key, frame);

    }

    public void LinkToSidebar(InternalFrame frame) {
        if (frame != null) {
            ViewSetting oSetting = frame.getViewSetting();

            byte subtype = 0;
            try {
                subtype = oSetting.getSubType();
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (frame instanceof GraphFrame) {
                subtype = ((GraphFrame) frame).getViewSettings().getSubType();
                oSetting = ((GraphFrame) frame).getViewSettings();
            }

            if (subtype == ViewSettingsManager.DETAIL_QUOTE) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                String key = ViewSettingsManager.DETAIL_QUOTE + "|" + "Linked";
                linkedWindows.put(key, frame);
                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE);
                Client.getInstance().getViewSettingsManager().adjustDTQViewToLinked(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sKey);

            } else if (subtype == ViewSettingsManager.SNAP_QUOTE) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String key = ViewSettingsManager.SNAP_QUOTE + "|" + "Linked";
                linkedWindows.put(key, frame);
                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.SNAP_QUOTE
                        + "|" + sKey, ViewSettingsManager.SNAP_QUOTE);
                Client.getInstance().getViewSettingsManager().adjustSnapViewToLinked(ViewSettingsManager.SNAP_QUOTE
                        + "|" + sKey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);

            } else if (subtype == ViewSettingsManager.TIMEnSALES_VIEW2) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String key = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + "Linked";
                linkedWindows.put(key, frame);
                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.TIMEnSALES_VIEW2
                        + "|" + sKey, ViewSettingsManager.TIMEnSALES_VIEW2);
                Client.getInstance().getViewSettingsManager().adjustTimenSalesViewToLinked(ViewSettingsManager.TIMEnSALES_VIEW2
                        + "|" + sKey, ViewSettingsManager.TIMEnSALES_VIEW2);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);

            } else if (subtype == ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + "Linked";
                linkedWindows.put(key, frame);
                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW
                        + "|" + sKey, ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW);
                Client.getInstance().getViewSettingsManager().adjustTimenSalesViewToLinked(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW
                        + "|" + sKey, ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
            } else if (subtype == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + "Linked";
                linkedWindows.put(key, frame);
                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_PRICE_BID_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_BID_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthPriceModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);

            } else if (subtype == ViewSettingsManager.DEPTH_BID_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.DEPTH_BID_VIEW + "|" + "Linked";
                linkedWindows.put(key, frame);
                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_ASK_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_ASK_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_BID_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_BID_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_ASK_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
            } else if (subtype == ViewSettingsManager.OUTER_CHART) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.OUTER_CHART + "|" + "Linked";
                linkedWindows.put(key, frame);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
            } else if (subtype == ViewSettingsManager.COMBINED_DEPTH_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + "Linked";
                linkedWindows.put(key, frame);

                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.COMBINED_DEPTH_VIEW
                        + "|" + sKey, ViewSettingsManager.COMBINED_DEPTH_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.COMBINED_DEPTH_VIEW
                        + "|" + sKey, ViewSettingsManager.COMBINED_DEPTH_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
            } else if (subtype == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + "Linked";
                linkedWindows.put(key, frame);

                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthSpecialOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
            } else if (subtype == ViewSettingsManager.DEPTH_ODDLOT_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + "Linked";
                linkedWindows.put(key, frame);

                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_ODDLOT_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_ODDLOT_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_ODDLOT_VIEW
                        + "|" + sKey, ViewSettingsManager.DEPTH_ODDLOT_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
            } else if (subtype == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + "Linked";
                linkedWindows.put(key, frame);
                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW
                        + "|" + sKey, ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW);
                Client.getInstance().getViewSettingsManager().adjustDepthCalcViewToLinked(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW
                        + "|" + sKey, ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
            } else if (subtype == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + "Linked";
                linkedWindows.put(key, frame);
                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW
                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW);
                Client.getInstance().getViewSettingsManager().adjustDepthCalcViewToLinked(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW
                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
            }

        }

    }

    public void addFrameTolinkedGroup(InternalFrame frame, String oldgroup) {
        if (frame != null) {
            String newGroup = frame.getLinkedGroupID();
            ViewSetting oSetting = frame.getViewSetting();

            byte subtype = 0;
            try {
                subtype = oSetting.getSubType();
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (frame instanceof GraphFrame) {
                subtype = ((GraphFrame) frame).getViewSettings().getSubType();
                oSetting = ((GraphFrame) frame).getViewSettings();
            }

            if (!oldgroup.equals(newGroup) && oldgroup.equals(LINK_NONE)) {
                convertToLinkedType(frame);
            } else if (!oldgroup.equals(newGroup) && newGroup.equals(LINK_NONE)) {
                convertToOriginalType(frame, oldgroup);
            } else {
                changeLinkGroups(frame, oldgroup);
            }

            addFrameToHashTable(frame, newGroup, oldgroup);
            oSetting.putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);

        }


    }

    private void convertToLinkedType(InternalFrame frame) {
        if (frame != null) {
            String newGroup = frame.getLinkedGroupID();
            ViewSetting oSetting = frame.getViewSetting();

            byte subtype = 0;
            try {
                subtype = oSetting.getSubType();
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (frame instanceof GraphFrame) {
                subtype = ((GraphFrame) frame).getViewSettings().getSubType();
                oSetting = ((GraphFrame) frame).getViewSettings();
            }

            if (subtype == ViewSettingsManager.DETAIL_QUOTE) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.DETAIL_QUOTE + "|" + "Linked" + "_" + newGroup;
                // linkedWindows.put(key,frame);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE, newGroup);
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE + "|" + linked + "_" + newGroup);
//                Client.getInstance().getViewSettingsManager().adjustDTQViewToLinked(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, newGroup);
                Client.getInstance().getViewSettingsManager().changeDTQViewKey(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE + "|" + linked + "_" + newGroup);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.DETAIL_QUOTE, newGroup);


            } else if (subtype == ViewSettingsManager.SNAP_QUOTE) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String newkey = ViewSettingsManager.SNAP_QUOTE + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.SNAP_QUOTE + "|" + sKey;
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeSnapViewKey(oldKey, newkey);
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.SNAP_QUOTE
//                        + "|" + sKey, ViewSettingsManager.SNAP_QUOTE);
//                Client.getInstance().getViewSettingsManager().adjustSnapViewToLinked(ViewSettingsManager.SNAP_QUOTE
//                        + "|" + sKey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.SNAP_QUOTE, newGroup);

            } else if (subtype == ViewSettingsManager.OUTER_CHART) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.OUTER_CHART + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.OUTER_CHART + "|" + sKey;
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.OUTER_CHART, newGroup);
            } else if (subtype == ViewSettingsManager.TIMEnSALES_VIEW2) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String newkey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeTimenSalesViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.TIMEnSALES_VIEW2, newGroup);

            } else if (subtype == ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeTimenSalesViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW, newGroup);
            } else if (subtype == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeDepthCalcViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW, newGroup);

            } else if (subtype == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeDepthCalcViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW, newGroup);

            } else if (subtype == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + sKey;

                String newkey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + linked + "_" + newGroup;
                String oldKey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey2, newkey2);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_PRICE_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthPriceModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((DepthPriceModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                registerLinkedWindowType(ViewSettingsManager.DEPTH_PRICE_BID_VIEW, newGroup);

            } else if (subtype == ViewSettingsManager.DEPTH_BID_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_BID_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.DEPTH_BID_VIEW + "|" + sKey;

                String newkey2 = ViewSettingsManager.DEPTH_ASK_VIEW + "|" + linked + "_" + newGroup;
                String oldKey2 = ViewSettingsManager.DEPTH_ASK_VIEW + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey2, newkey2);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((DepthOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                registerLinkedWindowType(ViewSettingsManager.DEPTH_BID_VIEW, newGroup);

            } else if (subtype == ViewSettingsManager.COMBINED_DEPTH_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + sKey;
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.COMBINED_DEPTH_VIEW
//                        + "|" + sKey, ViewSettingsManager.COMBINED_DEPTH_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.COMBINED_DEPTH_VIEW
//                        + "|" + sKey, ViewSettingsManager.COMBINED_DEPTH_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.COMBINED_DEPTH_VIEW, newGroup);

            } else if (subtype == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + sKey;

                String newkey2 = ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + linked + "_" + newGroup;
                String oldKey2 = ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey2, newkey2);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);
//
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthSpecialOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((DepthSpecialOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                registerLinkedWindowType(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW, newGroup);

            } else if (subtype == ViewSettingsManager.DEPTH_ODDLOT_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_ODDLOT_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ODDLOT_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_ODDLOT_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ODDLOT_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.DEPTH_ODDLOT_VIEW, newGroup);

            }
            if (subtype == ViewSettingsManager.FULL_QUOTE_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String key = ViewSettingsManager.FULL_QUOTE_VIEW + "|" + "Linked" + "_" + newGroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(ViewSettingsManager.FULL_QUOTE_VIEW
                        + "|" + sKey, ViewSettingsManager.FULL_QUOTE_VIEW + "|" + linked + "_" + newGroup);
                Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(ViewSettingsManager.FULL_QUOTE_VIEW
                        + "|" + sKey, ViewSettingsManager.FULL_QUOTE_VIEW + "|" + linked + "_" + newGroup);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                registerLinkedWindowType(ViewSettingsManager.FULL_QUOTE_VIEW, newGroup);

                InternalFrame depthframe = ((MultiDialogWindow) frame).getMarketDepth();
                if (Settings.getFullQuoteMDepthType() == 1) {

                    String newkey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + newGroup;
                    String oldKey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + sKey;

                    String newkey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + linked + "_" + newGroup;
                    String oldKey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + sKey;

                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey1, newkey1);
                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey2, newkey2);
                    ((DepthPriceModel) depthframe.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                    ((DepthPriceModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                    ((DepthPriceModel) depthframe.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                    ((DepthPriceModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                } else {
                    String newkey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + linked + "_" + newGroup;
                    String oldKey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + sKey;
                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey1, newkey1);
                    ((CombinedDepthModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                    ((CombinedDepthModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, true);

                }

                InternalFrame timensales = ((MultiDialogWindow) frame).getTimeAndSales();
                String newkey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey, newkey);
                ((TimeNSalesModel) timensales.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((TimeNSalesModel) timensales.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);

            }


        }


    }

    private void changeLinkGroups(InternalFrame frame, String oldGroup) {

        if (frame != null) {
            String newGroup = frame.getLinkedGroupID();
            ViewSetting oSetting = frame.getViewSetting();

            byte subtype = 0;
            try {
                subtype = oSetting.getSubType();
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (frame instanceof GraphFrame) {
                subtype = ((GraphFrame) frame).getViewSettings().getSubType();
                oSetting = ((GraphFrame) frame).getViewSettings();
            }

            if (subtype == ViewSettingsManager.DETAIL_QUOTE) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.DETAIL_QUOTE + "|" + "Linked" + "_" + newGroup;
                String oldkey = ViewSettingsManager.DETAIL_QUOTE + "|" + "Linked" + "_" + oldGroup;
                // linkedWindows.put(key,frame);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE, newGroup);
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldkey, newkey);
//                Client.getInstance().getViewSettingsManager().adjustDTQViewToLinked(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, newGroup);
                Client.getInstance().getViewSettingsManager().changeDTQViewKey(oldkey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.DETAIL_QUOTE, oldGroup, newGroup);


            } else if (subtype == ViewSettingsManager.SNAP_QUOTE) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String newkey = ViewSettingsManager.SNAP_QUOTE + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.SNAP_QUOTE + linked + "_" + oldGroup;
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeSnapViewKey(oldKey, newkey);
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.SNAP_QUOTE
//                        + "|" + sKey, ViewSettingsManager.SNAP_QUOTE);
//                Client.getInstance().getViewSettingsManager().adjustSnapViewToLinked(ViewSettingsManager.SNAP_QUOTE
//                        + "|" + sKey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.SNAP_QUOTE, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.OUTER_CHART) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.OUTER_CHART + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.OUTER_CHART + "|" + sKey;
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.OUTER_CHART, oldGroup, newGroup);
            } else if (subtype == ViewSettingsManager.TIMEnSALES_VIEW2) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String newkey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + linked + "_" + oldGroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeTimenSalesViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.TIMEnSALES_VIEW2, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + linked + "_" + oldGroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeTimenSalesViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + linked + "_" + oldGroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeDepthCalcViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + linked + "_" + oldGroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeDepthCalcViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + oldGroup;

                String newkey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + linked + "_" + newGroup;
                String oldKey2 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + oldGroup;


                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_PRICE_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthPriceModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((DepthPriceModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.DEPTH_PRICE_BID_VIEW, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.DEPTH_BID_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_BID_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.DEPTH_BID_VIEW + "|" + linked + "_" + oldGroup;

                String newkey2 = ViewSettingsManager.DEPTH_ASK_VIEW + "|" + linked + "_" + newGroup;
                String oldKey2 = ViewSettingsManager.DEPTH_ASK_VIEW + "|" + linked + "_" + oldGroup;

                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((DepthOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.DEPTH_BID_VIEW, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.COMBINED_DEPTH_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + linked + "_" + oldGroup;
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.COMBINED_DEPTH_VIEW
//                        + "|" + sKey, ViewSettingsManager.COMBINED_DEPTH_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.COMBINED_DEPTH_VIEW
//                        + "|" + sKey, ViewSettingsManager.COMBINED_DEPTH_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.COMBINED_DEPTH_VIEW, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + linked + "_" + oldGroup;

                String newkey2 = ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + linked + "_" + newGroup;
                String oldKey2 = ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + linked + "_" + oldGroup;

                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);
//
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                ((DepthSpecialOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((DepthSpecialOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW, oldGroup, newGroup);

            } else if (subtype == ViewSettingsManager.DEPTH_ODDLOT_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + linked + "_" + newGroup;
                String oldKey1 = ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + linked + "_" + oldGroup;

                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_ODDLOT_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ODDLOT_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_ODDLOT_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ODDLOT_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.DEPTH_ODDLOT_VIEW, oldGroup, newGroup);

            }
            if (subtype == ViewSettingsManager.FULL_QUOTE_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.FULL_QUOTE_VIEW + "|" + "Linked" + "_" + newGroup;
                String oldkey = ViewSettingsManager.FULL_QUOTE_VIEW + "|" + "Linked" + "_" + oldGroup;
                // linkedWindows.put(key,frame);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE, newGroup);
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldkey, newkey);
//                Client.getInstance().getViewSettingsManager().adjustDTQViewToLinked(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, newGroup);
                Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldkey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, true);
                changeLinkedWindowTypeInRegister(ViewSettingsManager.FULL_QUOTE_VIEW, oldGroup, newGroup);
                InternalFrame depthframe = ((MultiDialogWindow) frame).getMarketDepth();
                if (Settings.getFullQuoteMDepthType() == 1) {

                    String newkey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + newGroup;
                    String oldKey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + oldGroup;

                    String newkey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + linked + "_" + newGroup;
                    String oldKey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + linked + "_" + oldGroup;

                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey1, newkey1);
                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey2, newkey2);
                    ((DepthPriceModel) depthframe.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                    ((DepthPriceModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);
                } else {
                    String newkey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + linked + "_" + newGroup;
                    String oldKey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + sKey;
                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey1, newkey1);
                    ((CombinedDepthModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);

                }

                InternalFrame timensales = ((MultiDialogWindow) frame).getTimeAndSales();
                String newKey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey, newKey);
                ((TimeNSalesModel) timensales.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);

            }


        }

    }

    private void convertToOriginalType(InternalFrame frame, String oldgroup) {

        if (frame != null) {
            String newGroup = frame.getLinkedGroupID();
            ViewSetting oSetting = frame.getViewSetting();

            byte subtype = 0;
            try {
                subtype = oSetting.getSubType();
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (frame instanceof GraphFrame) {
                subtype = ((GraphFrame) frame).getViewSettings().getSubType();
                oSetting = ((GraphFrame) frame).getViewSettings();
            }

            if (subtype == ViewSettingsManager.DETAIL_QUOTE) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.DETAIL_QUOTE + "|" + "Linked" + "_" + oldgroup;
                // linkedWindows.put(key,frame);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE, newGroup);
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(key, ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sKey);
//                Client.getInstance().getViewSettingsManager().adjustDTQViewToLinked(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, newGroup);
                Client.getInstance().getViewSettingsManager().changeDTQViewKey(key, ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sKey);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.DETAIL_QUOTE, oldgroup);
                //frame.updateUI();


            } else if (subtype == ViewSettingsManager.SNAP_QUOTE) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String newkey = ViewSettingsManager.SNAP_QUOTE + "|" + sKey;
                String oldKey = ViewSettingsManager.SNAP_QUOTE + "|" + linked + "_" + oldgroup;
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeSnapViewKey(oldKey, newkey);
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.SNAP_QUOTE
//                        + "|" + sKey, ViewSettingsManager.SNAP_QUOTE);
//                Client.getInstance().getViewSettingsManager().adjustSnapViewToLinked(ViewSettingsManager.SNAP_QUOTE
//                        + "|" + sKey);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.SNAP_QUOTE, oldgroup);
            } else if (subtype == ViewSettingsManager.OUTER_CHART) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.OUTER_CHART + "|" + linked + "_" + newGroup;
                String oldKey = ViewSettingsManager.OUTER_CHART + "|" + sKey;
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.OUTER_CHART, oldgroup);
            } else if (subtype == ViewSettingsManager.TIMEnSALES_VIEW2) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);

                String newkey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + sKey;
                String oldKey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + linked + "_" + oldgroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeTimenSalesViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.TIMEnSALES_VIEW2, oldgroup);

            } else if (subtype == ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + sKey;
                String oldKey = ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + linked + "_" + oldgroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeTimenSalesViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW, oldgroup);

            } else if (subtype == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + sKey;
                String oldKey = ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + linked + "_" + oldgroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeDepthCalcViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW, oldgroup);

            } else if (subtype == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey = ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + sKey;
                String oldKey = ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + linked + "_" + oldgroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey, newkey);
                Client.getInstance().getViewSettingsManager().changeDepthCalcViewKey(oldKey, newkey);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW, oldgroup);

            } else if (subtype == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String oldKey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + oldgroup;
                String newkey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + sKey;

                String oldKey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + linked + "_" + oldgroup;
                String newkey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + sKey;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey2, newkey2);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_PRICE_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                ((DepthPriceModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, false);
                ((DepthPriceModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, LinkStore.LINK_NONE);
                unregisterLinkedWindowType(ViewSettingsManager.DEPTH_PRICE_BID_VIEW, oldgroup);

            } else if (subtype == ViewSettingsManager.DEPTH_BID_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_BID_VIEW + "|" + sKey;
                String oldKey1 = ViewSettingsManager.DEPTH_BID_VIEW + "|" + linked + "_" + oldgroup;

                String newkey2 = ViewSettingsManager.DEPTH_ASK_VIEW + "|" + sKey;
                String oldKey2 = ViewSettingsManager.DEPTH_ASK_VIEW + "|" + linked + "_" + oldgroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey2, newkey2);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                ((DepthOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((DepthOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                unregisterLinkedWindowType(ViewSettingsManager.DEPTH_BID_VIEW, oldgroup);

            } else if (subtype == ViewSettingsManager.COMBINED_DEPTH_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + sKey;
                String oldKey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + linked + "_" + oldgroup;
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.COMBINED_DEPTH_VIEW
//                        + "|" + sKey, ViewSettingsManager.COMBINED_DEPTH_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.COMBINED_DEPTH_VIEW
//                        + "|" + sKey, ViewSettingsManager.COMBINED_DEPTH_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.COMBINED_DEPTH_VIEW, oldgroup);

            } else if (subtype == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW) {

                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + sKey;
                String oldKey1 = ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + linked + "_" + oldgroup;

                String newkey2 = ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + sKey;
                String oldKey2 = ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + linked + "_" + oldgroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey2, newkey2);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey2, newkey2);
//
//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                ((DepthSpecialOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((DepthSpecialOrderModel) frame.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                unregisterLinkedWindowType(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW, oldgroup);

            } else if (subtype == ViewSettingsManager.DEPTH_ODDLOT_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String newkey1 = ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + sKey;
                String oldKey1 = ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + linked + "_" + oldgroup;

                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(oldKey1, newkey1);
                Client.getInstance().getViewSettingsManager().changeDepthViewKey(oldKey1, newkey1);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DEPTH_ODDLOT_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ODDLOT_VIEW);
//                Client.getInstance().getViewSettingsManager().adjustDepthViewToLinked(ViewSettingsManager.DEPTH_ODDLOT_VIEW
//                        + "|" + sKey, ViewSettingsManager.DEPTH_ODDLOT_VIEW);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.DEPTH_ODDLOT_VIEW, oldgroup);

            }
            if (subtype == ViewSettingsManager.FULL_QUOTE_VIEW) {
                String sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                String key = ViewSettingsManager.FULL_QUOTE_VIEW + "|" + "Linked" + "_" + oldgroup;
                // linkedWindows.put(key,frame);

//                Client.getInstance().getViewSettingsManager().adjustWindowToLinkedType(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, ViewSettingsManager.DETAIL_QUOTE, newGroup);
                Client.getInstance().getViewSettingsManager().changeKeyOfFrame(key, ViewSettingsManager.FULL_QUOTE_VIEW
                        + "|" + sKey);
//                Client.getInstance().getViewSettingsManager().adjustDTQViewToLinked(ViewSettingsManager.DETAIL_QUOTE
//                        + "|" + sKey, newGroup);
                Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(key, ViewSettingsManager.FULL_QUOTE_VIEW
                        + "|" + sKey);
                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                unregisterLinkedWindowType(ViewSettingsManager.FULL_QUOTE_VIEW, oldgroup);
                //frame.updateUI();
                InternalFrame depthframe = ((MultiDialogWindow) frame).getMarketDepth();
                if (Settings.getFullQuoteMDepthType() == 1) {

                    String newkey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + sKey;
                    String oldKey1 = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + linked + "_" + newGroup;

                    String newkey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + sKey;
                    String oldKey2 = ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + linked + "_" + newGroup;

                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey1, newkey1);
                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey2, newkey2);
                    ((DepthPriceModel) depthframe.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, false);
                    ((DepthPriceModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, false);
                    ((DepthPriceModel) depthframe.getTable2().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                    ((DepthPriceModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                } else {
                    String newkey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + sKey;
                    String oldKey1 = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + linked + "_" + newGroup;
                    Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey1, newkey1);
                    ((CombinedDepthModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, false);
                    ((CombinedDepthModel) depthframe.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);

                }

                InternalFrame timensales = ((MultiDialogWindow) frame).getTimeAndSales();
                String newkey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + sKey;
                String oldKey = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + linked + "_" + newGroup;

                Client.getInstance().getViewSettingsManager().changeFullQuoteViewKey(oldKey, newkey);
                ((TimeNSalesModel) timensales.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED, true);
                ((TimeNSalesModel) timensales.getTable1().getModel()).getViewSettings().putProperty(ViewConstants.VC_LINKED_GROUP, newGroup);

            }


        }

    }

    public void addFrameToHashTable(InternalFrame frame, String group, String oldgrp) {
        if (frame != null) {
            if (oldgrp.equals(group)) {
                return;
            } else if (group.equals(LINK_NONE)) {

                linkedGroups.get(oldgrp).remove(frame);

            } else if (oldgrp.equals(LINK_NONE)) {
                linkedGroups.get(group).add(frame);
            } else {

                linkedGroups.get(oldgrp).remove(frame);
                linkedGroups.get(group).add(frame);

            }


        }

    }


    public void addLinkedWindowFromWSP(InternalFrame frame) {
        if (frame != null) {
            ViewSetting oSetting = frame.getViewSetting();

            byte subtype = 0;
            try {
                subtype = oSetting.getSubType();
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (frame instanceof GraphFrame) {
                subtype = ((GraphFrame) frame).getViewSettings().getSubType();
                oSetting = ((GraphFrame) frame).getViewSettings();
            }

            if (subtype == ViewSettingsManager.COMBINED_DEPTH_VIEW) {
                // String key = ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + linked + "_"+frame.getLinkedGroupID();
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                //linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW) {
//                String key = ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + "Linked";
//                linkedWindows.put(key, frame);
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
            } else if (subtype == ViewSettingsManager.DEPTH_ODDLOT_VIEW) {
//                String key = ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + "Linked";
//                linkedWindows.put(key, frame);
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
            } else if (subtype == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                //String key = ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + "Linked";
                // linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) {
                //String key = ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW + "|" + "Linked";
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                // linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.DETAIL_QUOTE) {
                String key = ViewSettingsManager.DETAIL_QUOTE + "|" + "Linked";
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                // linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.SNAP_QUOTE) {
                String key = ViewSettingsManager.SNAP_QUOTE + "|" + "Linked";
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                //   linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.TIMEnSALES_VIEW2) {
                String key = ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + "Linked";
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                //linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW) {
                String key = ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW + "|" + "Linked";
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                //linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) {
                // String key = ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + "Linked";
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                // linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.DEPTH_BID_VIEW) {
                //String key = ViewSettingsManager.DEPTH_BID_VIEW + "|" + "Linked";
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                // linkedWindows.put(key, frame);
            } else if (subtype == ViewSettingsManager.FULL_QUOTE_VIEW) {
                String key = ViewSettingsManager.FULL_QUOTE_VIEW + "|" + "Linked";
                addFrameToHashTable(frame, frame.getLinkedGroupID(), LINK_NONE);
                registerLinkedWindowType(oSetting.getSubType(), frame.getLinkedGroupID());
                // linkedWindows.put(key, frame);
            }
        }

    }

    public void symbolChanged(String sKey) {

        if (!linkedWindows.isEmpty()) {
            if (!checkIfSymbolEnabled(sKey)) {
                String error = Language.getString("SYMBOL_DESAIBLED");
                String shortD = DataStore.getSharedInstance().getShortDescription(sKey);
                if (shortD == null) {
                    shortD = "";
                }
                String mesage = "<html>" + error + "<BR><B>" + SharedMethods.getSymbolFromKey(sKey) + "</B> &nbsp <B>" + shortD + "</B></html>";

                new ShowMessage(mesage, "I");
                return;
            }

            Enumeration en = linkedWindows.keys();
            while (en.hasMoreElements()) {
                String key = (String) en.nextElement();
                InternalFrame frame = linkedWindows.get(key);
                Byte subType = Byte.parseByte(key.substring(0, (key.indexOf("L") - 1)));
                if (subType == ViewSettingsManager.DETAIL_QUOTE && frame != null && frame.isShowing()) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);
//                    StockDetailQuoteModel model = (StockDetailQuoteModel)frame.getTable1().getModel();
//                    model.setSymbol(sKey);
//                    frame.getViewSetting().putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY,sKey);
//                    frame.setTitle(Language.getString("DETAIL_QUOTE") + " : (" + SharedMethods.getSymbolFromKey(sKey)
//                    + ") " + DataStore.getSharedInstance().getStockObject(sKey).getLongDescription());
//                    frame.updateUI();
//                    frame.show();
                } else if (subType == ViewSettingsManager.SNAP_QUOTE && frame != null && frame.isShowing()) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_SnapQuote, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
//                    SnapQuoteModel model = (SnapQuoteModel)frame.getTable1().getModel();
//                    model.setSymbol(sKey);
//                    LinkWindowRequestManager.getSharedInstance().addDepthRequestForSnapQuote(frame,sKey);
//                    frame.getViewSetting().putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY,sKey);
//                    frame.setTitle(Language.getString("SNAP_QUOTE") + " : (" + SharedMethods.getSymbolFromKey(sKey)
//                    + ") " + DataStore.getSharedInstance().getStockObject(sKey).getLongDescription());
//                    frame.updateUI();
//                    frame.show();

                } else if (subType == ViewSettingsManager.TIMEnSALES_VIEW2 && frame != null && frame.isShowing()) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);

                } else if (subType == ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW && frame != null && frame.isShowing()) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);
                } else if ((subType == ViewSettingsManager.DEPTH_PRICE_BID_VIEW || subType == ViewSettingsManager.DEPTH_BID_VIEW
                        || subType == ViewSettingsManager.COMBINED_DEPTH_VIEW || subType == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW
                        || subType == ViewSettingsManager.DEPTH_ODDLOT_VIEW) && frame != null && frame.isShowing()) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);
                } else if (subType == ViewSettingsManager.OUTER_CHART) {
                    try {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else if (subType == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);
                } else if (subType == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_CashFlow, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                } else if (subType == ViewSettingsManager.FULL_QUOTE_VIEW) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_FullQuote, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                }
            }
        }
    }

    public void fireSymbolChangedForGroup(String group, String sKey) {

        if (group != LINK_NONE) {

            ArrayList<InternalFrame> frames = linkedGroups.get(group);
            if (frames != null && !frames.isEmpty()) {
                symbolChanged(frames, sKey);
            }

        }

    }

    private void symbolChanged(ArrayList<InternalFrame> frames, String sKey) {

        if (!frames.isEmpty()) {
            if (!checkIfSymbolEnabled(sKey)) {
//                String error = Language.getString("SYMBOL_DESAIBLED");
//                String shortD = DataStore.getSharedInstance().getShortDescription(sKey);
//                if (shortD == null) {
//                    shortD = "";
//                }
//                String mesage = "<html>" + error + "<BR><B>" + SharedMethods.getSymbolFromKey(sKey) + "</B> &nbsp <B>" + shortD + "</B></html>";
//
//                new ShowMessage(mesage, "I");
                return;
            }

            // Enumeration en = linkedWindows.keys();
            for (int j = 0; j < frames.size(); j++) {
//            while(en.hasMoreElements()){
                // String key = (String) en.nextElement();
                InternalFrame frame = frames.get(j);
                ViewSetting oSetting = frame.getViewSetting();

                byte subType = 0;
                try {
                    subType = oSetting.getSubType();
                } catch (Exception e) {
                    //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                if (frame instanceof GraphFrame) {
                    subType = ((GraphFrame) frame).getViewSettings().getSubType();
                    oSetting = ((GraphFrame) frame).getViewSettings();
                }

                //  Byte subType = Byte.parseByte(key.substring(0, (key.indexOf("L") - 1)));
                if (subType == ViewSettingsManager.DETAIL_QUOTE && frame != null && frame.isShowing()) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);
//                    StockDetailQuoteModel model = (StockDetailQuoteModel)frame.getTable1().getModel();
//                    model.setSymbol(sKey);
//                    frame.getViewSetting().putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY,sKey);
//                    frame.setTitle(Language.getString("DETAIL_QUOTE") + " : (" + SharedMethods.getSymbolFromKey(sKey)
//                    + ") " + DataStore.getSharedInstance().getStockObject(sKey).getLongDescription());
//                    frame.updateUI();
//                    frame.show();
                } else if (subType == ViewSettingsManager.SNAP_QUOTE && frame != null && frame.isShowing()) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_SnapQuote, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
//                    SnapQuoteModel model = (SnapQuoteModel)frame.getTable1().getModel();
//                    model.setSymbol(sKey);
//                    LinkWindowRequestManager.getSharedInstance().addDepthRequestForSnapQuote(frame,sKey);
//                    frame.getViewSetting().putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY,sKey);
//                    frame.setTitle(Language.getString("SNAP_QUOTE") + " : (" + SharedMethods.getSymbolFromKey(sKey)
//                    + ") " + DataStore.getSharedInstance().getStockObject(sKey).getLongDescription());
//                    frame.updateUI();
//                    frame.show();

                } else if (subType == ViewSettingsManager.TIMEnSALES_VIEW2 && frame != null && frame.isShowing()) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);

                } else if (subType == ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW && frame != null && frame.isShowing()) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);
                } else if ((subType == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) && frame != null && frame.isShowing()) {

                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_MarketDepthByPrice, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                } else if (subType == ViewSettingsManager.DEPTH_BID_VIEW) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_MarketDepthByOrder, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                } else if (subType == ViewSettingsManager.COMBINED_DEPTH_VIEW) {
                    ((LinkedWindowListener) frame).symbolChanged(sKey);
                } else if (subType == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_SpecialOrderBook, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                } else if (subType == ViewSettingsManager.DEPTH_ODDLOT_VIEW) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_MarketDepthByPrice, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                } else if (subType == ViewSettingsManager.OUTER_CHART) {
                    try {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else if (subType == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_DepthCalculator, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                } else if (subType == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_CashFlow, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                } else if (subType == ViewSettingsManager.FULL_QUOTE_VIEW) {
                    if (Client.getInstance().isValidInfoType(sKey, Meta.IT_FullQuote, true)) {
                        ((LinkedWindowListener) frame).symbolChanged(sKey);
                    }
                }
            }
        }

    }

    public boolean canLinkToSidebar(byte type) {
        String key = type + "|" + linked;
        if (linkedWindows.containsKey(key) && linkedWindows.get(key) != null) {

            return false;
        } else {
            return true;
        }
    }

    public boolean isColorGrpAvailableForwindowType(byte type, String grp) {
        String key = type + "|" + grp;
        for (int i = 0; i < linkGroupRegister.size(); i++) {
            String item = linkGroupRegister.get(i);
            if (item.equals(key)) {
                return false;
            }

        }
        return true;

    }

    public void registerLinkedWindowType(byte type, String grp) {
        String key = type + "|" + grp;
        linkGroupRegister.add(key);

    }

    public void changeLinkedWindowTypeInRegister(byte type, String oldGrp, String newGrp) {
        String oldkey = type + "|" + oldGrp;
        String newkey = type + "|" + newGrp;
        for (int i = 0; i < linkGroupRegister.size(); i++) {
            String item = linkGroupRegister.get(i);
            if (item.equals(oldkey)) {
                linkGroupRegister.remove(i);
                linkGroupRegister.add(i, newkey);
            }

        }


    }

    public void unregisterLinkedWindowType(byte type, String grp) {
        String key = type + "|" + grp;
        for (int i = 0; i < linkGroupRegister.size(); i++) {
            String item = linkGroupRegister.get(i);
            if (item.equals(key)) {
                linkGroupRegister.remove(i);
            }

        }

    }

    public void removeLinkedWindow(byte subtype) {
        String key = subtype + "|" + linked;
        linkedWindows.remove(key);
    }

    public void removeLinkedWindowFromRegister(byte subtype, String group, InternalFrame frame) {
        String key = subtype + "|" + linked;
        unregisterLinkedWindowType(subtype, group);
        removeFromHashTable(group, frame);


    }

    public void removeLinkedWindow(InternalFrame frame) {
        if (linkedWindows.contains(frame)) {
            //linkedWindows.
            linkedWindows.remove(frame);
        }
    }

    public void removeFromHashTable(String group, InternalFrame frame) {

        try {
            linkedGroups.get(group).remove(frame);
        } catch (Exception e) {

        }

    }

    public boolean checkIfSymbolEnabled(String sKey) {
        Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
        boolean isEnabled = true;
        if (stock != null) {
            isEnabled = stock.isSymbolEnabled();
        }
        return isEnabled;
    }

    public Icon getColoredIconForButton(String id) {
        if (id == null) {
            return null;
        }
        if (id.equalsIgnoreCase(LINK_RED)) {
            return getIcon("InternalFrame.linkgrpRed");
        } else if (id.equalsIgnoreCase(LINK_GREEN)) {
            return getIcon("InternalFrame.linkgrpGreen");
        } else if (id.equalsIgnoreCase(LINK_BLUE)) {
            return getIcon("InternalFrame.linkgrpBlue");
        } else if (id.equalsIgnoreCase(LINK_YELLOW)) {
            return getIcon("InternalFrame.linkgrpYellow");
        } else if (id.equalsIgnoreCase(LINK_WHITE)) {
            return getIcon("InternalFrame.linkgrpWhite");
        } else {
            return getIcon("InternalFrame.linkgrpNotLink");
        }
    }

    private Icon getIcon(String id) {
        try {
            if ((UIManager.getIcon(id).getIconHeight()) > 0) {
                return UIManager.getIcon(id);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }


    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath("/frame/" + iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    public String getOldGroupID(String id) {
        if (id.equals(LINK_RED)) {
            return LINK_RED;
        } else if (id.equals(LINK_GREEN)) {
            return LINK_GREEN;
        } else if (id.equals(LINK_BLUE)) {
            return LINK_BLUE;
        } else if (id.equals(LINK_YELLOW)) {
            return LINK_YELLOW;
        } else if (id.equals(LINK_WHITE)) {
            return LINK_WHITE;
        } else {
            return LINK_NONE;
        }

    }
}
