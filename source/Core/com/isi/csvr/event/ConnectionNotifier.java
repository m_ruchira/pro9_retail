/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 31, 2003
 * Time: 8:23:15 AM
 * To change this template use Options | File Templates.
 */
package com.isi.csvr.event;

import java.util.Vector;

public class ConnectionNotifier {
    private static ConnectionNotifier self;
    private Vector listeners;


    private ConnectionNotifier() {
        listeners = new Vector(10, 5);
    }

    public synchronized static ConnectionNotifier getInstance() {
        if (self == null) {
            self = new ConnectionNotifier();
        }
        return self;
    }

    public synchronized void addConnectionListener(Object listener) {
        listeners.addElement(listener);
    }

    public synchronized void removeConnectionListener(Object listener) {
        listeners.remove(listener);
    }

    public synchronized void fireTWConnected() {

        for (int i = 0; i < listeners.size(); i++) {
            try {
                ConnectionListener listener = (ConnectionListener) listeners.get(i);
                listener.twConnected();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireTWDisconnected() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ConnectionListener listener = (ConnectionListener) listeners.get(i);
                listener.twDisconnected();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

