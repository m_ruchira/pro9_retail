package com.isi.csvr.event;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public interface ProcessListener {
    /**
     * Process Started
     *
     * @param object
     */
    public void processSarted(Object object);

    /**
     * Process completed
     *
     * @param sucessfull
     * @param object
     */
    public void processCompleted(boolean sucessfull, Object object);
}