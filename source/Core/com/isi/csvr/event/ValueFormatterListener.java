package com.isi.csvr.event;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 24, 2006
 * Time: 9:15:04 AM
 */
public interface ValueFormatterListener {

    public void vauleChanged(String value);

    public void vauleIgnored(String value);
}
