package com.isi.csvr.event;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 22, 2005
 * Time: 9:34:07 AM
 */
public interface ProgressListener {

    /**
     * Process started
     *
     * @param id
     */
    public void setProcessStarted(String id);

    /**
     * max possible value of the process progress
     *
     * @param id
     * @param max
     */
    public void setMax(String id, int max);

    /**
     * Sets the current progress
     *
     * @param id
     * @param progress
     */
    public void setProgress(String id, int progress);

    /**
     * Process completed
     *
     * @param id
     * @param sucess
     */
    public void setProcessCompleted(String id, boolean sucess);
}
