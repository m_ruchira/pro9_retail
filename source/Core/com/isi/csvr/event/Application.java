package com.isi.csvr.event;

import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.plugin.PluginStore;
import com.isi.csvr.plugin.event.PluginEvent;

import java.util.TimeZone;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 22, 2005
 * Time: 5:39:21 PM
 */
public class Application {

    private static Application self;
    private static boolean ready;
    private static boolean loaded;
    private Vector listeners;


    private Application() {
        listeners = new Vector(10, 5);
    }

    public synchronized static Application getInstance() {
        if (self == null) {
            self = new Application();
        }
        return self;
    }

    public static boolean isReady() {
        return ready;
    }

    public static boolean isLoaded() {
        return loaded;
    }

    public synchronized void addApplicationListener(Object listener) {
        listeners.addElement(listener);
    }

    public synchronized void removeApplicationListener(Object listener) {
        listeners.remove(listener);
    }

    public synchronized void fireApplicationLoading(int percentage) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.applicationLoading(percentage);
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireApplicationLoaded() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.applicationLoaded();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        loaded = true;
    }

    public synchronized void fireWorkspaceWillSave() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.workspaceWillSave();
                listener = null;
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    public synchronized void fireWorkspaceSaved() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.workspaceSaved();
                listener = null;
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
    }

    public synchronized void fireApplicationExiting() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.applicationExiting();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireApplicationReadyForTransactions() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.applicationReadyForTransactions();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            PluginStore.firePluginEvent(PluginEvent.APPLICATION_READY_FOR_TRANSACTIONS);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        ready = true;
    }

    public synchronized void fireApplicationTimeZoneChanged(TimeZone zone) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.applicationTimeZoneChanged(zone);
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireWorkspaceWillLoad() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.workspaceWillLoad();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireWorkspaceLoaded() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.workspaceLoaded();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireLoadOfflineData() {
        ExchangeStore.getSharedInstance().notifyExchangesLoaded();
        CurrencyStore.getSharedInstance().notifyCurrenciesLoaded();
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.loadOfflineData();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public synchronized void fireSnapshotStarted(Exchange exchange) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.snapshotProcessingStarted(exchange);
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireSnapshotEnded(Exchange exchange) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.snapshotProcessingEnded(exchange);
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireSectedExchangeChanged(Exchange exchange) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ApplicationListener listener = (ApplicationListener) listeners.get(i);
                listener.selectedExchangeChanged(exchange);
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
