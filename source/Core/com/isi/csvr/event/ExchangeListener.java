package com.isi.csvr.event;

import com.isi.csvr.datastore.Exchange;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 3, 2005
 * Time: 1:38:41 PM
 */
public interface ExchangeListener {
    /**
     * A new exchange has been added
     *
     * @param exchange
     */
    public void exchangeAdded(Exchange exchange);

    /**
     * Exchange removed from the system
     *
     * @param exchange
     */
    public void exchangeRemoved(Exchange exchange);

    /**
     * Exchange upgraded to default status
     *
     * @param exchange
     */
    public void exchangeUpgraded(Exchange exchange);

    /**
     * Exchange downgraded from default status to normal
     *
     * @param exchange
     */
    public void exchangeDowngraded(Exchange exchange);


    /**
     * Exchanges sucessfully added
     *
     * @param offlineMode
     */
    public void exchangesAdded(boolean offlineMode);

    /**
     * Exchanges sucessfully loaded from the archive
     */
    public void exchangesLoaded();

    /**
     * Data of this market must be initialized
     *
     * @param exchange
     */
    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate);

    /**
     * Exchange master file for this exchange has been loaded
     *
     * @param exchange
     */
    public void exchangeMasterFileLoaded(String exchange);

    /**
     * Time zone of this exchange changed manually
     *
     * @param exchange
     */
    public void exchangeTimeZoneChanged(Exchange exchange);

    /**
     * Currency this exchange changed manually
     *
     * @param exchange
     */
    public void exchangeCurrencyChanged(Exchange exchange);

    /**
     * Information types changed
     */
    public void exchangeInformationTypesChanged();

    /**
     * Trading Information types changed
     */
    public void exchangeTradingInformationTypesChanged();

    /**
     * Market Status changed
     *
     * @param exchange
     * @param oldStatus
     */
    public void marketStatsChanged(int oldStatus, Exchange exchange);
}
