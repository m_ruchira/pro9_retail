// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * This dialog class is used to change the current password
 * of the user.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class ChangePass extends JDialog implements WindowListener, KeyListener, ActionListener {
    private static int clickedBtn = 2;
    Modes mode;
    //private JPanel jPanel1 = new JPanel();
    private JLabel lblOld = new JLabel();
    private JLabel lblNew1 = new JLabel();
    private JLabel lblNew2 = new JLabel();
    private JLabel lblMessage = new JLabel();
    //---------------------------------------------------------------------------------------------------------------------------
    //private JPasswordField txtOld = new JPasswordField();
    //private JPasswordField txtNew1 = new JPasswordField();
    //private JPasswordField txtNew2 = new JPasswordField();
    private TWPasswordField txtOld;
    private TWPasswordField txtNew1;
    private TWPasswordField txtNew2;
    //---------------------------------------------------------------------------------------------------------------------------
    private TWButton btnCancel = new TWButton();
    private TWButton btnOk = new TWButton();
    private String newPassword;
    private String oldPassword;
    private String message;
    private boolean messageType;

    ;

    /**
     * Constructs a new instance.
     *
     * @param parent
     * @param title
     */
    public ChangePass(Frame parent, String title, String message, Modes mode) {
        super(parent, title, true);
        try {
            this.mode = mode;
            this.message = message;
            messageType = (message != null);
            jbInit();
            pack(); // New
            this.addWindowListener(this);
            this.addKeyListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // Server g_oServerConnector;

    public static boolean isOkClicked() {
        boolean isOkClicked = false;
        if (clickedBtn == Constants.OK_PRESSED) {
            isOkClicked = true;
        }
        return isOkClicked;
    }

    public void windowClosing(WindowEvent ev) {
        //clickedBtn = Constants.CANCEL_PRESSED;
    }

    public void windowIconified(WindowEvent ev) {
    }

    public void windowDeactivated(WindowEvent ev) {
    }

    public void windowDeiconified(WindowEvent ev) {
    }

    public void windowActivated(WindowEvent ev) {
    }

    public void windowClosed(WindowEvent ev) {
        if (clickedBtn == Constants.OK_PRESSED) {
        } else {
            clickedBtn = Constants.CANCEL_PRESSED;
        }
    }

    public void windowOpened(WindowEvent ev) {
    }

    public void keyPressed(KeyEvent ev) {
    }

    public void keyReleased(KeyEvent ev) {
        if (ev.getKeyChar() == '\n') {
            ev.consume();
            btnOk_actionPerformed();
        }
    }

    public void keyTyped(KeyEvent ev) {
    }

    public void actionPerformed(ActionEvent ev) {
        if (ev.getSource() == btnCancel) {
            btnCancel_actionPerformed();
        } else if (ev.getSource() == btnOk) {
            btnOk_actionPerformed();
        }
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        setLayout(new BorderLayout());
        this.setBackground(Theme.getColor("BOARD_TABLE_BGCOLOR")); //BACKGROUND_COLOR"));
        this.setResizable(false);
        lblOld.setText(Language.getString("PASSWORD_OLD"));
//        lblOld.setBounds(new Rectangle(18, 15, 132, 26));
        lblNew1.setText(Language.getString("PASSWORD_NEW1"));
//        lblNew1.setBounds(new Rectangle(18, 55, 132, 27));
        lblNew2.setText(Language.getString("PASSWORD_NEW2"));
//        lblNew2.setBounds(new Rectangle(18, 96, 132, 23));
//        txtOld.setBounds(new Rectangle(154, 19, 164, 21));
//        txtNew1.setBounds(new Rectangle(154, 58, 164, 21));
//        txtNew2.setBounds(new Rectangle(154, 98, 164, 21));

//-----------------------------------------------------------------------------------------------------------------------------------
        txtNew2 = new TWPasswordField(this, false, false);
        txtNew2.addKeyListener(this);
        txtNew1 = new TWPasswordField(this, false, false);
        txtNew1.addKeyListener(this);
        txtOld = new TWPasswordField(this, false, false);
        ;
        txtOld.addKeyListener(this);

        txtOld.setUsername(Settings.getItemFromBulk("DEFAULT_USER"));
        txtNew1.setUsername(Settings.getItemFromBulk("DEFAULT_USER"));
        txtNew2.setUsername(Settings.getItemFromBulk("DEFAULT_USER"));
//-----------------------------------------------------------------------------------------------------------------------------------
        /*txtNew1.setEditable(true);
        txtNew2.setEditable(true);
        txtOld.setEditable(true);*/
        JPanel oPanel = (JPanel) this.getContentPane();
        oPanel.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        oPanel.registerKeyboardAction(this, "C", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);


        JPanel inputPanel = new JPanel();
        String[] inputwidths = {"10", "115", "15", "175", "10"};
        String[] inputheights = {"10", "20", "5", "20", "5", "20", "10"};
        // inputPanel.setLayout(new FlexGridLayout(inputwidths, inputheights, 5, 5));
        inputPanel.setLayout(new FlexGridLayout(inputwidths, inputheights, 0, 0));

        //10 pixels upper
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));

        // old password row
        inputPanel.add(new JLabel(""));
        inputPanel.add(lblOld);
        inputPanel.add(new JLabel(""));
        inputPanel.add(txtOld);
        inputPanel.add(new JLabel(""));

        // 5 pixeld between
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));

        //new password row
        inputPanel.add(new JLabel(""));
        inputPanel.add(lblNew1);
        inputPanel.add(new JLabel(""));
        inputPanel.add(txtNew1);
        inputPanel.add(new JLabel(""));

        // 5 pixels between
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));

        //confirm password row
        inputPanel.add(new JLabel(""));
        inputPanel.add(lblNew2);
        inputPanel.add(new JLabel(""));
        inputPanel.add(txtNew2);
        inputPanel.add(new JLabel(""));

        // 10 pixels below
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));
        inputPanel.add(new JLabel(""));

        if (mode == Modes.MODE_NO_OLD_PW) {
            txtOld.setEnabled(false);
        }
        int buttonTopOffset = 0;
        if (messageType) {
            buttonTopOffset = 100;
            //lblMessage.setBounds(new Rectangle(18, 137, 300, 101));
            lblMessage.setText(message);
        }
        btnCancel.setText(Language.getString("CANCEL"));
        btnCancel.setPreferredSize(new Dimension(80, 20));
        btnCancel.setActionCommand("C");
        //btnCancel.setBounds(new Rectangle(243, 137 + buttonTopOffset, 75, 29));
        btnCancel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnCancel_actionPerformed();
            }
        });
        btnCancel.addActionListener(this);
        btnOk.setText(Language.getString("OK"));
        btnOk.setPreferredSize(new Dimension(80, 20));
        btnCancel.setActionCommand("O");
        // btnOk.setBounds(new Rectangle(154, 137 + buttonTopOffset, 75, 29));
        btnOk.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnOk_actionPerformed();
            }
        });
        btnOk.addActionListener(this);

        JPanel buttonPanel = new JPanel();
        // String[] buttonwidths = {"100%", "100", "100"};
        String[] buttonwidths = {"140", "80", "15", "80", "10"};
        String[] buttonheights = {"20", "10"};
        buttonPanel.setLayout(new FlexGridLayout(buttonwidths, buttonheights, 0, 0));
        // buttonPanel.add(new JLabel());

        buttonPanel.add(new JLabel(""));
        buttonPanel.add(btnOk);
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(btnCancel);
        buttonPanel.add(new JLabel(""));

        buttonPanel.add(new JLabel(""));
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(new JLabel(""));


        add(inputPanel, BorderLayout.NORTH);
        add(lblMessage, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);

        //getContentPane().add(jPanel1);
        /*jPanel1.add(lblOld, null);
        jPanel1.add(lblNew1, null);
        jPanel1.add(lblNew2, null);
        jPanel1.add(txtOld, null);
        jPanel1.add(txtNew2, null);
        jPanel1.add(txtNew1, null);
        jPanel1.add(lblMessage, null);
        jPanel1.add(btnCancel, null);
        jPanel1.add(btnOk, null);*/

        pack();
        setLocationRelativeTo(Client.getInstance().getFrame());
        GUISettings.applyOrientation(getContentPane());

    }

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */
//    public ChangePass(JFrame oParent, String title, String message, Modes mode) {
//        this(oParent, title, message, true, mode);
//g_oServerConnector = oServerConnector;
//    }

    /**
     * Displays the dialog
     */
    public void showDialog() {
        /*Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        if (messageType) {
            this.setBounds((int) ((oDm.getWidth() - 340) / 2),
                    (int) ((oDm.getHeight() - 300) / 2), 340, 300);
        } else {
            this.setBounds((int) ((oDm.getWidth() - 340) / 2),
                    (int) ((oDm.getHeight() - 200) / 2), 340, 200);
        }*/
        newPassword = null;
        oldPassword = null;
        this.setVisible(true);
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * Validates input to make sure the new password is
     * typed correctly
     */
    private boolean validateInput() {
        boolean isValid = false;
        if (new String(txtNew1.getPassword()).trim().equals(new String(txtNew2.getPassword()).trim())) {
            isValid = true;
        } else
            isValid = false;
        return isValid;
    }

    /**
     * When Ok Button is pressed
     */
    void btnOk_actionPerformed() {
        Object[] options = {Language.getString("OK")};
        clickedBtn = Constants.OK_PRESSED;
        if (validateInput()) {
            if ((new String(txtNew1.getPassword()).trim().equals("")) || (new String(txtNew2.getPassword()).trim().equals(""))) {
                JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                        Language.getString("MSG_INVALID_PASSWORD"),
                        Language.getString("ERROR"),
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE,
                        null,
                        options,
                        options[0]);
                return;
            } else if (mode == Modes.MODE_INCLUDE_OLD_PW && new String(txtOld.getPassword()).trim().equals("")) {
                JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                        Language.getString("MSG_ENTER_OLD_PASSWORD"),
                        Language.getString("ERROR"),
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE,
                        null,
                        options,
                        options[0]);
                return;
            }

            oldPassword = new String(txtOld.getPassword()).trim();
            newPassword = new String(txtNew1.getPassword()).trim();
            hide();

//        	try
//            {
//                SendQueue.addData(
//                    Meta.NEWPASSWORD + Meta.DS
//                	+ SharedMethods.encrypt(new String(txtNew1.getPassword()).trim()) + Meta.FS
//                    + Meta.USER_PASSWORD + Meta.DS
//                    + SharedMethods.encrypt(new String(txtOld.getPassword()).trim())  + Meta.FS
//                    + Meta.USER_SESSION + Meta.DS + Settings.getSessionID()
//                    );
//                this.dispose();
//            }
//            catch(Exception e2)
//            {
//            	e2.printStackTrace();
//	        	JOptionPane.showOptionDialog(Client.getInstance(),
//                    Language.getString("MSG_COMM_FAIL"),
//                    Language.getString("ERROR"),
//                    JOptionPane.OK_OPTION,
//                    JOptionPane.ERROR_MESSAGE,
//                    null,
//                    options,
//                    options[0]);
//            }
            //this.hide();
        } else {
            JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    Language.getString("MSG_NEW_PASS1_PASS2_ERROR"),
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
        }
    }

    /**
     * When cancel button is pressed
     */
    void btnCancel_actionPerformed() {
        //clearData();
        clickedBtn = Constants.CANCEL_PRESSED;
        oldPassword = null;
        newPassword = null;
        this.hide();
    }

    public static enum Modes {
        MODE_NO_OLD_PW, MODE_INCLUDE_OLD_PW
    }

//    public static void clearData()
//    {
//    	txtOld.setText("");
//        txtNew1.setText("");
//        txtNew2.setText("");
//    }
}


