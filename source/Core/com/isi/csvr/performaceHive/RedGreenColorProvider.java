package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 1:38:46 PM
 * To change this template use File | Settings | File Templates.
 */

import java.awt.*;
import java.util.Vector;

public class RedGreenColorProvider extends ColorProvider {
    public static Vector colorVector = new Vector();
    private int tabColor[];

    public RedGreenColorProvider() {
        tabColor = new int[25];
        int baseCode = 255;
        for (int i = 24; i >= 0; i--, baseCode -= 5) {
            tabColor[i] = baseCode;
        }
        int xCursor = tabColor.length - 1;
        for (int i = tabColor.length - 1; i >= 0; i--) {
            colorVector.add(new Color(0, tabColor[i], 0));
            xCursor--;
        }
        xCursor = (tabColor.length - 1);
        for (int i = 0, j = 0; i < 25; i++, j += 10) {
            colorVector.add(new Color(j, 255, j));
            xCursor++;
        }

        for (int i = 0, j = 250; i < 25; i++, j -= 10) {
            colorVector.add(new Color(255, j, j));
            xCursor++;
        }

        for (int i = tabColor.length - 1; i >= 0; i--) {
            colorVector.add(new Color(tabColor[i], 0, 0));
            xCursor++;
        }
    }

    public Color getColor(Value value) {
        return (Color) colorVector.get((int) value.getValue() - 1);
    }

}

