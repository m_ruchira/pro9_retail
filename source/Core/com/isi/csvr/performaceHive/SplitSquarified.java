package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:48:29 AM
 * To change this template use File | Settings | File Templates.
 */

import java.util.Iterator;
import java.util.Vector;

public class SplitSquarified extends SplitStrategy {

    private int w1;
    private int h1;
    private int x;
    private int y;
    private int w;
    private int h;
    private int x2;
    private int y2;
    private int w2;
    private int h2;

    public SplitSquarified() {
    }

    public void splitElements(Vector v, Vector v1, Vector v2) {
        int mid = 0;
        double weight0 = sumWeight(v);
        double a = ((TreeMapNode) v.get(mid)).getWeight() / weight0;
        double b = a;
        if (w < h) {
            do {
                if (mid >= v.size())
                    break;
                double aspect = normAspect(h, w, a, b);
                double q = ((TreeMapNode) v.get(mid)).getWeight() / weight0;
                if (normAspect(h, w, a, b + q) > aspect)
                    break;
                mid++;
                b += q;
            } while (true);
            int i;
            for (i = 0; i <= mid && i < v.size(); i++)
                v1.add(v.get(i));

            for (; i < v.size(); i++)
                v2.add(v.get(i));

            h1 = (int) Math.round((double) h * b);
            w1 = w;
            x2 = x;
            y2 = (int) Math.round((double) y + (double) h * b);
            w2 = w;
            h2 = h - h1;
        } else {
            do {
                if (mid >= v.size())
                    break;
                double aspect = normAspect(w, h, a, b);
                double q = ((TreeMapNode) v.get(mid)).getWeight() / weight0;
                if (normAspect(w, h, a, b + q) > aspect)
                    break;
                mid++;
                b += q;
            } while (true);
            int i;
            for (i = 0; i <= mid && i < v.size(); i++)
                v1.add(v.get(i));

            for (; i < v.size(); i++)
                v2.add(v.get(i));

            h1 = h;
            w1 = (int) Math.round((double) w * b);
            x2 = (int) Math.round((double) x + (double) w * b);
            y2 = y;
            w2 = w - w1;
            h2 = h;
        }
    }

    protected void calculatePositionsRec(int x0, int y0, int w0, int h0, double weight0, Vector v) {
        Vector vClone = new Vector(v);
        sortVector(vClone);
        if (vClone.size() <= 2) {
            SplitBySlice.splitInSlice(x0, y0, w0, h0, vClone, sumWeight(vClone));
            calculateChildren(vClone);
        } else {
            Vector v1 = new Vector();
            Vector v2 = new Vector();
            x = x0;
            y = y0;
            w = w0;
            h = h0;
            splitElements(vClone, v1, v2);
            int prevX2 = x2;
            int prevY2 = y2;
            int prevW2 = w2;
            int prevH2 = h2;
            SplitBySlice.splitInSlice(x0, y0, w1, h1, v1, sumWeight(v1));
            calculateChildren(v1);
            calculatePositionsRec(prevX2, prevY2, prevW2, prevH2, sumWeight(v2), v2);
        }
    }

    private double aspect(double big, double small, double a, double b) {
        return (big * b) / ((small * a) / b);
    }

    private void calculateChildren(Vector v) {
        for (Iterator i$ = v.iterator(); i$.hasNext(); ) {
            TreeMapNode node = (TreeMapNode) i$.next();
            if (node.isLeaf()) {
                node.setX(node.getX() + TreeMapNode.getBorder());
                node.setY(node.getY() + TreeMapNode.getBorder());
                int width = node.getWidth() - TreeMapNode.getBorder();
                if (width < 0)
                    width = 0;
                int height = node.getHeight() - TreeMapNode.getBorder();
                if (height < 0)
                    height = 0;
                node.setHeight(height);
                node.setWidth(width);
            } else {
                int bSub;
                if (TreeMapNode.getBorder() > 1)
                    bSub = 2;
                else if (TreeMapNode.getBorder() == 1)
                    bSub = 1;
                else
                    bSub = 0;
                int width = node.getWidth() - bSub;
                if (width < 0)
                    width = 0;
                int height = node.getHeight() - bSub;
                if (height < 0)
                    height = 0;
                TreeMapNode.setBorder(TreeMapNode.getBorder() - bSub);
                calculatePositionsRec(node.getX() + bSub, node.getY() + bSub, width, height, node.getWeight(), node.getChildren());
                TreeMapNode.setBorder(TreeMapNode.getBorder() + bSub);
            }
        }

    }

    private double normAspect(double big, double small, double a, double b) {
        double xCalc = aspect(big, small, a, b);
        if (xCalc < 1.0D)
            return 1.0D / xCalc;
        else
            return xCalc;
    }
}