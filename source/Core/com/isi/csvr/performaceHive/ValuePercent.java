package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:46:25 AM
 * To change this template use File | Settings | File Templates.
 */

import java.text.NumberFormat;

public class ValuePercent extends Value {

    private double value;
    private NumberFormat nf;

    public ValuePercent() {
        nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        nf.setMinimumIntegerDigits(1);
    }

    public ValuePercent(double value) {
        this();
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double d) {
        value = d;
    }

    public String getLabel() {
        if (value >= 0.0D)
            return (new StringBuilder()).append("+").append(nf.format(value)).append(" %").toString();
        else
            return (new StringBuilder()).append(nf.format(value)).append(" %").toString();
    }

    public void setLabel(String s) {
    }

}

