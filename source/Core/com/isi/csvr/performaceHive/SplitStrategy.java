package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:47:19 AM
 * To change this template use File | Settings | File Templates.
 */

import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

public abstract class SplitStrategy
        implements Serializable {

    public SplitStrategy() {
    }

    public void calculatePositions(TreeMapNode root) {
        if (root == null)
            return;
        Vector v = root.getChildren();
        if (v != null)
            calculatePositionsRec(root.getX(), root.getY(), root.getWidth(), root.getHeight(), sumWeight(v), v);
    }

    public abstract void splitElements(Vector vector, Vector vector1, Vector vector2);

    public double sumWeight(Vector v) {
        double d = 0.0D;
        if (v != null) {
            int size = v.size();
            for (int i = 0; i < size; i++)
                d += ((TreeMapNode) v.elementAt(i)).getWeight();

        }
        return d;
    }

    protected void calculatePositionsRec(int x0, int y0, int w0, int h0, double weight0, Vector v) {
        if (v.size() == 1) {
            TreeMapNode f = (TreeMapNode) v.elementAt(0);
            if (f.isLeaf()) {
                int w = w0 - TreeMapNode.getBorder();
                if (w < 0)
                    w = 0;
                int h = h0 - TreeMapNode.getBorder();
                if (h < 0)
                    h = 0;
                f.setDimension(x0 + TreeMapNode.getBorder(), y0 + TreeMapNode.getBorder(), w, h);
            } else {
                f.setDimension(x0, y0, w0, h0);
                int bSub;
                if (TreeMapNode.getBorder() > 1)
                    bSub = 2;
                else if (TreeMapNode.getBorder() == 1)
                    bSub = 1;
                else
                    bSub = 0;
                int w = w0 - bSub;
                if (w < 0)
                    w = 0;
                int h = h0 - bSub;
                if (h < 0)
                    h = 0;
                TreeMapNode.setBorder(TreeMapNode.getBorder() - bSub);
                calculatePositionsRec(x0 + bSub, y0 + bSub, w, h, weight0, f.getChildren());
                TreeMapNode.setBorder(TreeMapNode.getBorder() + bSub);
            }
        } else {
            Vector v1 = new Vector();
            Vector v2 = new Vector();
            splitElements(v, v1, v2);
            double weight1 = sumWeight(v1);
            double weight2 = sumWeight(v2);
            int w1;
            int w2;
            int h1;
            int h2;
            int x2;
            int y2;
            if (w0 > h0) {
                w1 = (int) (((double) w0 * weight1) / weight0);
                w2 = w0 - w1;
                h1 = h0;
                h2 = h0;
                x2 = x0 + w1;
                y2 = y0;
            } else {
                w1 = w0;
                w2 = w0;
                h1 = (int) (((double) h0 * weight1) / weight0);
                h2 = h0 - h1;
                x2 = x0;
                y2 = y0 + h1;
            }
            calculatePositionsRec(x0, y0, w1, h1, weight1, v1);
            calculatePositionsRec(x2, y2, w2, h2, weight2, v2);
        }
    }

    protected void sortVector(Vector v) {
        for (int i = 0; i < v.size(); i++) {
            for (int j = v.size() - 1; j > i; j--)
                if (((TreeMapNode) v.elementAt(j)).getWeight() > ((TreeMapNode) v.elementAt(j - 1)).getWeight()) {
                    TreeMapNode tmn = (TreeMapNode) v.elementAt(j);
                    v.setElementAt(v.elementAt(j - 1), j);
                    v.setElementAt(tmn, j - 1);
                }

        }

    }

    protected void workOutWeight(Vector v1, Vector v2, Vector vClone, double sumWeight) {
        double memWeight = 0.0D;
        double elemWeight = 0.0D;
        for (Iterator i = vClone.iterator(); i.hasNext(); ) {
            TreeMapNode tmn = (TreeMapNode) i.next();
            elemWeight = tmn.getWeight();
            if (memWeight + elemWeight >= sumWeight / 2D) {
                if (sumWeight / 2D - memWeight > (memWeight + elemWeight) - sumWeight / 2D) {
                    memWeight += elemWeight;
                    v1.addElement(tmn);
                } else if (v1.isEmpty())
                    v1.addElement(tmn);
                else
                    v2.addElement(tmn);
                while (i.hasNext()) {
                    tmn = (TreeMapNode) i.next();
                    v2.addElement(tmn);
                }
            } else {
                memWeight += elemWeight;
                v1.addElement(tmn);
            }
        }

    }
}
