package com.isi.csvr.performaceHive;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 29, 2009
 * Time: 5:11:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class PerformaceHiveWindow extends InternalFrame implements Themeable, ActionListener, MouseListener, ChangeListener, DocumentListener {

    public static final int GROUP_BY_EXCHANGE = 0;
    public static int selectedGroupOption = GROUP_BY_EXCHANGE;
    public static final int GROUP_BY_VOLUME = 1;
    public static final int GROUP_BY_CHANGE = 2;
    public static final int GROUP_BY_PERCENT_CHANGE = 3;
    public static final int GROUP_BY_PERCENT_CASHFLOW = 4;
    public static final int GROUP_BY_NO_GROUP = 5;
    public static final int SIZE_BY_VOLUME = 0;
    public static int selectedSizeOption = SIZE_BY_VOLUME;
    public static final int SIZE_BY_CHANGE = 1;
    public static final int SIZE_BY_PERCENT_CHANGE = 2;
    public static final int SIZE_BY_PERCENT_CASHFLOW = 3;
    public static final int SIZE_BY_BID_QTY = 4;
    public static final int SIZE_BY_ASK_QTY = 5;
    public static final int SIZE_BY_TURNOVER = 6;
    public static final int SIZE_BY_TRADES = 7;
    public static final int SIZE_BY_EQUAL_SIZE = 8;
    public static final int COLOR_BY_VOLUME = 0;
    public static int selectedColorOption = COLOR_BY_VOLUME;
    public static final int COLOR_BY_CHANGE = 1;
    public static final int COLOR_BY_PERCENT_CHANGE = 2;
    public static final int COLOR_BY_PERCENT_CASHFLOW = 3;
    public static final int COLOR_BY_BID_QTY = 4;
    public static final int COLOR_BY_ASK_QTY = 5;
    public static final int COLOR_BY_TURNOVER = 6;
    public static final int COLOR_BY_TRADES = 7;
    public static int selectedLowPriceRank = 1;
    public static int selectedHighPriceRank = 100;
    public static int selectedLowVolumeRank = 1;
//    private CardLayout cardLayout;
    public static int selectedHighVolumeRank = 100;
    public static int selectedLowBidQtyRank = 1;
    public static int selectedHighBidQtyRank = 100;
    public static int selectedLowOfferQtyRank = 1;
    public static int selectedHighOfferQtyRank = 100;
    public static int selectedLowPositionRank = 1;
    public static int selectedHighPositionRank = 100;
    private static PerformaceHiveWindow self = null;
    private int width = 800;
    private int height = 550;
    private JPanel topPanel;
    private JPanel bottomPanel;
    private JPanel comboPanel;
    private JPanel colorPanel;
    private JPanel mapPanel;
    private JPanel filterPanel;
    private RangeSlider priceSlider;
    private RangeSlider volumeSlider;
    private RangeSlider bidQtySlider;
    private RangeSlider offerQtySlider;
    private RangeSlider positionSlider;
    private JCheckBox chkPrice;
    private JCheckBox chkVolume;
    private JCheckBox chkBidQty;
    private JCheckBox chkOfferQty;
    private JCheckBox chkPosition;
    private JList baseList;
    private JComboBox cmbGroupBy;
    private JComboBox cmbSizeBy;
    private JComboBox cmbColor;
    private TWTextField searchTxtField;
    private Document searchDoc;
    private TreeMapNode root;
    private JTreeMap jTreeMap;
    private ColorProvider cp;
    private JPanel panelLegend;
    private Vector groupByVector = new Vector();
    private Vector sizeByVector = new Vector();
    private Vector colorByVector = new Vector();
    private Vector baseVector = new Vector();
    private int[] selectedBases = {0};

    private PerformaceHiveWindow() {
        setGroupByVect();
        setSizeByVect();
        setColorByVect();
        PerformaceHiveProcessor.getSharedInstance().getMaxMinSymbolDetails();
        setBaseList();
        setBases();
        PerformaceHiveProcessor.getSharedInstance().adjustBaseToRank();
        createMap();
        createWindow();
        this.setLayer(GUISettings.TOP_LAYER);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.hideTitleBarMenu();
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        applyTheme();
        setVisible(true);
    }

    public static PerformaceHiveWindow getSharedInstance() {
        if (self == null) {
            self = new PerformaceHiveWindow();
        }
        return self;
    }

    /*
    * creates and adds all components in the hive window
    */
    private void createWindow() {
        try {
            this.setSize(width, height);
            this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"50", "100%"}, 0, 0));
            this.setResizable(true);
            this.setClosable(true);
            this.setMaximizable(true);
            this.setIconifiable(true);
            this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            this.setTitle(Language.getString("PERFORMANCE_HIVE"));

            topPanel = new JPanel(new FlexGridLayout(new String[]{"60%", "40%"}, new String[]{"100%"}, 0, 2));
            bottomPanel = new JPanel(new FlexGridLayout(new String[]{"100%", "200"}, new String[]{"100%"}));
            comboPanel = new JPanel(new FlexGridLayout(new String[]{"120", "120", "120"}, new String[]{"50%", "50%"}, 5, 0));
            colorPanel = new JPanel(new FlexGridLayout(new String[]{"60%", "50%"}, new String[]{"100%"}));
            mapPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));
            filterPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"120", "20", "40", "20", "40", "20", "40", "20", "40", "20", "10", "20", "10", "20"}, 2, 2));
            panelLegend = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"10", "100%", "10"}, 2, 2));

            cmbGroupBy = new JComboBox(groupByVector);
            cmbGroupBy.addActionListener(this);
            cmbSizeBy = new JComboBox(sizeByVector);
            cmbSizeBy.addActionListener(this);
            cmbColor = new JComboBox(colorByVector);
            cmbColor.addActionListener(this);
            baseList = new JList(baseVector);
            baseList.setFont(new TWFont("Ariel", TWFont.PLAIN, 12));
            baseList.setBackground(Color.white);
            baseList.setSelectedIndex(0);
            baseList.addMouseListener(this);

            comboPanel.add(new JLabel(Language.getString("GROUP_BY")));
            comboPanel.add(new JLabel(Language.getString("SIZE_BY")));
            comboPanel.add(new JLabel(Language.getString("COLOR_BY")));
            comboPanel.add(cmbGroupBy);
            comboPanel.add(cmbSizeBy);
            comboPanel.add(cmbColor);
            colorPanel.add(panelLegend);
            colorPanel.add(new JLabel());

            cp = new RedGreenColorProvider();
            panelLegend.add(new JLabel());
            panelLegend.add(new JLabel());
//            panelLegend.add(new FilterLegend());
            panelLegend.add(new JLabel());
            jTreeMap.setColorProvider(cp);

            chkPrice = new JCheckBox(Language.getString("HIVE_PRICE_RANK"), true);
            chkPrice.addChangeListener(this);
            priceSlider = new RangeSlider(1, 100);
            priceSlider.setValue(1);
            priceSlider.setUpperValue(100);
            priceSlider.setPaintTicks(true);
            priceSlider.setMinorTickSpacing(5);
            priceSlider.setMajorTickSpacing(25);
            priceSlider.setPaintLabels(true);
            priceSlider.setPaintTrack(true);
            priceSlider.addChangeListener(this);

            chkVolume = new JCheckBox(Language.getString("HIVE_VOLUME_RANK"), true);
            chkVolume.addChangeListener(this);
            volumeSlider = new RangeSlider(1, 100);
            volumeSlider.setValue(1);
            volumeSlider.setUpperValue(100);
            volumeSlider.setPaintTicks(true);
            volumeSlider.setMinorTickSpacing(5);
            volumeSlider.setMajorTickSpacing(25);
            volumeSlider.setPaintLabels(true);
            volumeSlider.setPaintTrack(true);
            volumeSlider.addChangeListener(this);

            chkBidQty = new JCheckBox(Language.getString("HIVE_BID_RANK"), true);
            chkBidQty.addChangeListener(this);
            bidQtySlider = new RangeSlider(1, 100);
            bidQtySlider.setValue(1);
            bidQtySlider.setUpperValue(100);
            bidQtySlider.setPaintTicks(true);
            bidQtySlider.setMinorTickSpacing(5);
            bidQtySlider.setMajorTickSpacing(25);
            bidQtySlider.setPaintLabels(true);
            bidQtySlider.setPaintTrack(true);
            bidQtySlider.addChangeListener(this);

            chkOfferQty = new JCheckBox(Language.getString("HIVE_OFFER_RANK"), true);
            chkOfferQty.addChangeListener(this);
            offerQtySlider = new RangeSlider(1, 100);
            offerQtySlider.setValue(1);
            offerQtySlider.setUpperValue(100);
            offerQtySlider.setPaintTicks(true);
            offerQtySlider.setMinorTickSpacing(5);
            offerQtySlider.setMajorTickSpacing(25);
            offerQtySlider.setPaintLabels(true);
            offerQtySlider.setPaintTrack(true);
            offerQtySlider.addChangeListener(this);

            chkPosition = new JCheckBox(Language.getString("HIVE_POSITION_RANK"), true);
            chkPosition.addChangeListener(this);
            positionSlider = new RangeSlider(1, 100);
            positionSlider.setValue(1);
            positionSlider.setUpperValue(100);
            positionSlider.setPaintTicks(false);
            positionSlider.setPaintLabels(false);
            positionSlider.setPaintTrack(true);
            positionSlider.addChangeListener(this);

            searchTxtField = new TWTextField("", 10);
            searchTxtField.setForeground(Color.LIGHT_GRAY);
            searchTxtField.setText("Symbol Search");
            searchTxtField.addFocusListener(new FocusListener() {
                public void focusGained(FocusEvent e) {
                    searchTxtField.setForeground(Color.black);
                    searchTxtField.setText("");
                }

                public void focusLost(FocusEvent e) {
                    searchTxtField.setForeground(Color.LIGHT_GRAY);
                    searchTxtField.setText("Symbol Search");
                }
            });
            searchDoc = searchTxtField.getDocument();
            searchDoc.addDocumentListener(this);

            mapPanel.add(jTreeMap);
            JScrollPane sp = new JScrollPane(baseList);
            sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            filterPanel.add(sp);
            filterPanel.add(chkPrice);
            filterPanel.add(priceSlider);
            filterPanel.add(chkVolume);
            filterPanel.add(volumeSlider);
            filterPanel.add(chkBidQty);
            filterPanel.add(bidQtySlider);
            filterPanel.add(chkOfferQty);
            filterPanel.add(offerQtySlider);
            filterPanel.add(chkPosition);
            filterPanel.add(new FilterLegend());
            filterPanel.add(positionSlider);
            filterPanel.add(new JLabel());
            filterPanel.add(searchTxtField);

            topPanel.add(comboPanel);
            topPanel.add(colorPanel);
            bottomPanel.add(mapPanel);
            bottomPanel.add(filterPanel);

            this.add(topPanel);
            this.add(bottomPanel);
        } catch (Exception e) {
        }
    }

    /*
    * creates the tree map
    */
    private void createMap() {
        try {
            JTree treeView = new JTree();
            root = PerformaceHiveProcessor.getSharedInstance().buildMap();
            jTreeMap = new JTreeMap(root, treeView);
            jTreeMap.setFont(new Font(null, 1, 16));
            jTreeMap.setPreferredSize(new Dimension(600, 400));
            jTreeMap.setBorder(BorderFactory.createEtchedBorder(1));
            jTreeMap.setTreeView(treeView);
            jTreeMap.setActiveLeaf(null);
            jTreeMap.repaint();
        } catch (Exception e) {
        }
    }

    /*
    * adds entries to the groupByVector.
    * groupByVector contains the options in which the stocks can be grouped.
    * options are entered into the vector in desired positions.
    */
    private void setGroupByVect() {
        groupByVector.add(GROUP_BY_EXCHANGE, Language.getString("EXCHANGE"));
        groupByVector.add(GROUP_BY_VOLUME, Language.getString("VOLUME"));
        groupByVector.add(GROUP_BY_CHANGE, Language.getString("CHANGE"));
        groupByVector.add(GROUP_BY_PERCENT_CHANGE, Language.getString("PCT_CHANGE"));
        groupByVector.add(GROUP_BY_PERCENT_CASHFLOW, Language.getString("PERCENT_CASH_FLOW"));
        groupByVector.add(GROUP_BY_NO_GROUP, Language.getString("NO_GROUPING"));
    }

    /*
    * adds entries to the sizeByVector.
    * sizeByVector contains the options in which the stocks can be assigned sizes in the Hive.
    * options are entered into the vector in desired positions.
    */
    private void setSizeByVect() {
        sizeByVector.add(SIZE_BY_VOLUME, Language.getString("VOLUME"));
        sizeByVector.add(SIZE_BY_CHANGE, Language.getString("CHANGE"));
        sizeByVector.add(SIZE_BY_PERCENT_CHANGE, Language.getString("PCT_CHANGE"));
        sizeByVector.add(SIZE_BY_PERCENT_CASHFLOW, Language.getString("PERCENT_CASH_FLOW"));
        sizeByVector.add(SIZE_BY_BID_QTY, Language.getString("BEST_BID_QUANTITY"));
        sizeByVector.add(SIZE_BY_ASK_QTY, Language.getString("ASK_QUANTITY"));
        sizeByVector.add(SIZE_BY_TURNOVER, Language.getString("TURNOVER"));
        sizeByVector.add(SIZE_BY_TRADES, Language.getString("TRADES"));
        sizeByVector.add(SIZE_BY_EQUAL_SIZE, Language.getString("EQUAL_SIZE"));
    }

    /*
    * adds entries to the colorByVector.
    * colorByVector contains the options in which each stock can be assigned colors in the Hive.
    * options are entered into the vector in desired positions.
    */
    private void setColorByVect() {
        colorByVector.add(COLOR_BY_VOLUME, Language.getString("VOLUME"));
        colorByVector.add(COLOR_BY_CHANGE, Language.getString("CHANGE"));
        colorByVector.add(COLOR_BY_PERCENT_CHANGE, Language.getString("PCT_CHANGE"));
        colorByVector.add(COLOR_BY_PERCENT_CASHFLOW, Language.getString("PERCENT_CASH_FLOW"));
        colorByVector.add(COLOR_BY_BID_QTY, Language.getString("BEST_BID_QUANTITY"));
        colorByVector.add(COLOR_BY_ASK_QTY, Language.getString("ASK_QUANTITY"));
        colorByVector.add(COLOR_BY_TURNOVER, Language.getString("TURNOVER"));
        colorByVector.add(COLOR_BY_TRADES, Language.getString("TRADES"));
    }

    /*
    * sets values to the baseVector.
    * baseVector contains the names of bases of stocks.
    * initially bases are assigned according to the exchanges. Therefore the exchange names are added to the baseVector.
    * subsequently all watchlist names are added to the baseVector.
    * finally a base containing all available sysmbols are added to the baseVector.
    * baseVector is passed to the constructor of JList as an argument when creating the JList named baseList
    *
    * baseSymbolHash is a HashTable which contains the base names and the corresponding symbol names relevant to each base.
    * baseSymbolHash is also filled when baseVector is filled.
    */
    private void setBaseList() {
        baseVector.clear();
        PerformaceHiveProcessor.getSharedInstance().baseSymbolHash.clear();

        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                Exchange ex = (Exchange) exchanges.nextElement();
                baseVector.add(ex.getDescription());
                Enumeration enu = DataStore.getSharedInstance().getSymbols(ex.toString());
                Vector list = new Vector();
                while (enu.hasMoreElements()) {
                    list.add(ex.toString() + "~" + enu.nextElement());
                }
                PerformaceHiveProcessor.getSharedInstance().baseSymbolHash.put(baseVector.indexOf(ex.getDescription()), list.elements());
            } catch (Exception e) {
            }

        }
        WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
        for (WatchListStore watchlist : watchlists) {
            Vector list = new Vector();
            try {
                baseVector.add(watchlist.getCaption());
                for (int i = 0; i < watchlist.getSymbols().length; i++) {
                    list.add(watchlist.getSymbols()[i]);
                }
                PerformaceHiveProcessor.getSharedInstance().baseSymbolHash.put(baseVector.indexOf(watchlist.getCaption()), list.elements());
            } catch (Exception e) {
            }

        }
        baseVector.add(Language.getString("ALL"));
        PerformaceHiveProcessor.getSharedInstance().baseSymbolHash.put(baseVector.indexOf(Language.getString("ALL")), DataStore.getSharedInstance().getAllSymbols());
    }

    /*
    * repaints the map when user selection is changed in grouping, size or color.
    */
    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == cmbSizeBy) {
                selectedSizeOption = cmbSizeBy.getSelectedIndex();
            } else if (e.getSource() == cmbColor) {
                cp = new RedGreenColorProvider();
                selectedColorOption = cmbColor.getSelectedIndex();
                PerformaceHiveProcessor.getSharedInstance().getMaxMinColorValues();
            } else if (e.getSource() == cmbGroupBy) {
                selectedGroupOption = cmbGroupBy.getSelectedIndex();
            }
            setBaseList();
            setBases();
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToRank();
            jTreeMap.setRoot(PerformaceHiveProcessor.getSharedInstance().buildMap());
            jTreeMap.setActiveLeaf(null);
            jTreeMap.setColorProvider(cp);
            jTreeMap.repaint();
        } catch (Exception e1) {
        }
    }

    /*
    * selectedBases - array which contains the selcted indices from the base list
    * on mouse click the selectedBases is updated and the JTreeMap is repainted according to the selected bases
    */
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == baseList) {
            selectedBases = baseList.getSelectedIndices();
            setBaseList();
            setBases();
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToRank();
            jTreeMap.setRoot(PerformaceHiveProcessor.getSharedInstance().buildMap());
            jTreeMap.setActiveLeaf(null);
            jTreeMap.setColorProvider(cp);
            jTreeMap.repaint();
        }
    }

    /*
    * relevant symbols are extracted from baseSymbolHash and added to tempArray.
    * then duplicates are removed from tempArray.
    * new MapObjects are created and added to the baseSymbolPool.
    */
    private void setBases() {
        PerformaceHiveProcessor.getSharedInstance().baseSymbolPool.clear();
        int[] selected = selectedBases;
        ArrayList tempArray = new ArrayList();
        for (int i = 0; i < selected.length; i++) {
            try {
                Enumeration enu = (Enumeration) PerformaceHiveProcessor.getSharedInstance().baseSymbolHash.get(selected[i]);
                while (enu.hasMoreElements()) {
                    try {
                        String symbol = enu.nextElement().toString();
                        tempArray.add(symbol);
                    } catch (Exception e1) {
                    }
                }
            } catch (Exception e1) {
            }
        }
        Object[] totArray = tempArray.toArray();
        Arrays.sort(totArray);
        for (int j = 0; j < totArray.length; j++) {
            try {
                if (totArray[j].equals(totArray[j + 1])) {
                    continue;
                } else {
                    Stock stock = DataStore.getSharedInstance().getStockObject((String) totArray[j]);
                    MapObject mapObject = new MapObject((String) totArray[j], stock.getSymbol(), String.valueOf(stock.getLastTradedPrice()), String.valueOf(stock.getPercentChange()), String.valueOf(stock.getVolume()), String.valueOf(stock.getTradeQuantity()), String.valueOf(stock.getChange()), String.valueOf(stock.getAvgTradePrice()), String.valueOf(stock.getTurnover()), String.valueOf(stock.getTradeQuantity()), stock.getLongDescription());
                    mapObject.setBidQty(stock.getBestBidQuantity());
                    mapObject.setAskQty(stock.getBestAskQuantity());
                    mapObject.setPercentCashFlow(stock.getCashFlowPct());

                    PerformaceHiveProcessor.getSharedInstance().assignPriceRank(mapObject);
                    PerformaceHiveProcessor.getSharedInstance().assignVolumeRank(mapObject);
                    PerformaceHiveProcessor.getSharedInstance().assignBidQtyRank(mapObject);
                    PerformaceHiveProcessor.getSharedInstance().assignAskQtyRank(mapObject);
                    PerformaceHiveProcessor.getSharedInstance().assignPositionRank(mapObject);
                    PerformaceHiveProcessor.getSharedInstance().baseSymbolPool.add(mapObject);
                }
            } catch (Exception e1) {
            }
        }
    }

    /*
    * repaints the JTreeMap when the sliders are moved.
    * */
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == chkPrice) {
            if (chkPrice.isSelected()) {
                priceSlider.setEnabled(true);
            } else {
                priceSlider.setEnabled(false);
            }
        } else if (e.getSource() == chkVolume) {
            if (chkVolume.isSelected()) {
                volumeSlider.setEnabled(true);
            } else {
                volumeSlider.setEnabled(false);
            }
        } else if (e.getSource() == chkBidQty) {
            if (chkBidQty.isSelected()) {
                bidQtySlider.setEnabled(true);
            } else {
                bidQtySlider.setEnabled(false);
            }
        } else if (e.getSource() == chkOfferQty) {
            if (chkOfferQty.isSelected()) {
                offerQtySlider.setEnabled(true);
            } else {
                offerQtySlider.setEnabled(false);
            }
        } else if (e.getSource() == chkPosition) {
            if (chkPosition.isSelected()) {
                positionSlider.setEnabled(true);
            } else {
                positionSlider.setEnabled(false);
            }
        } else if ((e.getSource() == priceSlider) || (e.getSource() == volumeSlider) || (e.getSource() == bidQtySlider) || (e.getSource() == offerQtySlider) || (e.getSource() == positionSlider)) {
            selectedLowPriceRank = priceSlider.getValue();
            selectedHighPriceRank = priceSlider.getUpperValue();
            selectedLowVolumeRank = volumeSlider.getValue();
            selectedHighVolumeRank = volumeSlider.getUpperValue();
            selectedLowBidQtyRank = bidQtySlider.getValue();
            selectedHighBidQtyRank = bidQtySlider.getUpperValue();
            selectedLowOfferQtyRank = offerQtySlider.getValue();
            selectedHighOfferQtyRank = offerQtySlider.getUpperValue();
            selectedLowPositionRank = positionSlider.getValue();
            selectedHighPositionRank = positionSlider.getUpperValue();
            setBaseList();
            setBases();
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToRank();
            jTreeMap.setRoot(PerformaceHiveProcessor.getSharedInstance().buildMap());
            jTreeMap.setActiveLeaf(null);
            jTreeMap.setColorProvider(cp);
            jTreeMap.repaint();
        }
    }

    /*
    * updates the JTreeMap when the text in the symbol search textfield is inserted
    * */
    public void insertUpdate(DocumentEvent e) {
        if (e.getDocument() == searchDoc) {
            setBaseList();
            setBases();
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToSymbolSearch(searchTxtField.getText());
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToRank();
            jTreeMap.setRoot(PerformaceHiveProcessor.getSharedInstance().buildMap());
            jTreeMap.setActiveLeaf(null);
            jTreeMap.setColorProvider(cp);
            jTreeMap.repaint();
        }
    }

    /*
    * updates the JTreeMap when text is removed from the symbol search TextField
    * */
    public void removeUpdate(DocumentEvent e) {
        if (e.getDocument() == searchDoc) {
            setBaseList();
            setBases();
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToSymbolSearch(searchTxtField.getText());
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToRank();
            jTreeMap.setRoot(PerformaceHiveProcessor.getSharedInstance().buildMap());
            jTreeMap.setActiveLeaf(null);
            jTreeMap.setColorProvider(cp);
            jTreeMap.repaint();
        }
    }

    /*
    * updates the JTreeMAp when the text in symbol search TextField is changed
    * */
    public void changedUpdate(DocumentEvent e) {
        if (e.getDocument() == searchDoc) {
            setBaseList();
            setBases();
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToSymbolSearch(searchTxtField.getText());
            PerformaceHiveProcessor.getSharedInstance().adjustBaseToRank();
            jTreeMap.setRoot(PerformaceHiveProcessor.getSharedInstance().buildMap());
            jTreeMap.setActiveLeaf(null);
            jTreeMap.setColorProvider(cp);
            jTreeMap.repaint();
        }
    }
}
