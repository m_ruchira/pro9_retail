package com.isi.csvr.performaceHive;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:42:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class PerformaceHiveProcessor {

    private static Hashtable mapHash = new Hashtable<String, ArrayList>();  // hash map used to draw the tree
    private static PerformaceHiveProcessor self = null;
    public Hashtable baseSymbolHash = new Hashtable<Integer, Enumeration>(); //stores symbol lists seperately which is related to each element in the base
    public ArrayList baseSymbolPool = new ArrayList(); // stores all the symbols in selected bases
    private double maxLastTraded = 0, minLastTraded = 0, lastTradedFactor = 1;
    private long maxVolume = 0, minVolume = 0, volumeFactor = 1;
    private long maxBidQty = 0, minBidQty = 0, bidQtyFactor = 1;
    private long maxOfferQty = 0, minOfferQty = 0, offerQtyFactor = 1;
    private double maxChange = 0, minChange = 0;
    private double maxPercentChange = 0, minPercentChange = 0;
    private double maxPercentCashFlow = 0, minPercentCashFlow = 0;
    private double maxTurnOver = 0, minTurnOver = 0;
    private long maxTrades = 0, minTrades = 0;
    private double maxPosition = 0, minPosition = 0, positionFactor = 1;

    private PerformaceHiveProcessor() {
    }

    public static PerformaceHiveProcessor getSharedInstance() {
        if (self == null) {
            self = new PerformaceHiveProcessor();
        }
        return self;
    }

    /*
    * groups, assign sizes and assign colors to the <code>MapObject</code> objects in <code>baseSymbolPool</code>
    * <code>mapHash</code> - a Hash Table which includes all the <code>MapObject</code> objects after they have been grouped assigned colors and sizes.
    *                          the key of the Hash Table is the group name.
    * returns the <code>TreeMapNode</code> after all the relevant child nodes are added according to the groups
    * */
    public TreeMapNode buildMap() {
        setGroupByValue();
        setSizeByValue();
        setColorByValue();
        TreeMapNodeBuilder builder = new TreeMapNodeBuilder();
        TreeMapNode rootNode = builder.buildBranch("Root", null);
        Enumeration keys = mapHash.keys();
        while (keys.hasMoreElements()) {
            String branchString = keys.nextElement().toString();
            TreeMapNode branch = builder.buildBranch(branchString, rootNode);
            ArrayList leaves = (ArrayList) mapHash.get(branchString);
            for (int i = 0; i < leaves.size(); i++) {
                MapObject mapObject = (MapObject) leaves.get(i);
//                Value value = new ValuePercent(mapObject.getColor());
                Value value = new ValuePercent(mapObject.getPositionRank());
                builder.buildLeaf(mapObject.getLabel(), mapObject.getSize(), value, mapObject.getSymbol(), mapObject.getLastPrice(), mapObject.getPercentChange(), mapObject.getVolume(), mapObject.getLastQty(), mapObject.getChange(), mapObject.getVWap(), mapObject.getTurnOver(), mapObject.getTrades(), mapObject.getDescription(), branch);
            }
        }
        return builder.getRoot();
    }

    /*
    * groups the <code>MapObject</code> objects according to the exchange of the symbols
    * */
    private void groupByExcahnge() {
        mapHash.clear();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                String ex = exchanges.nextElement().toString();
                ArrayList stockArray = new ArrayList();
                for (int i = 0; i < baseSymbolPool.size(); i++) {
                    try {
                        String symbol = ((MapObject) baseSymbolPool.get(i)).getLabel();
                        Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
                        if (stock.getExchange().equals(ex)) {
                            stockArray.add(baseSymbolPool.get(i));
                        }
                    } catch (Exception e) {
                    }
                }
                mapHash.put(ex, stockArray);
            } catch (Exception e) {
            }
        }
    }

    /*
    * groups the <code>MapObject</code> in the <code>baseSymbolPool</code> into 4 quarters according to the volume
    * */
    private void groupByVolume() {
        mapHash.clear();
        long quarter1 = ((maxVolume - minVolume)) / 4 + minVolume;
        long quarter2 = ((maxVolume - minVolume) * 2) / 4 + minVolume;
        long quarter3 = ((maxVolume - minVolume) * 3) / 4 + minVolume;
        long quarter4 = maxVolume;

        ArrayList quarter1Array = new ArrayList();
        ArrayList quarter2Array = new ArrayList();
        ArrayList quarter3Array = new ArrayList();
        ArrayList quarter4Array = new ArrayList();

        for (int i = 0; i < baseSymbolPool.size(); i++) {
            String symbol = ((MapObject) baseSymbolPool.get(i)).getLabel();
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            if ((stock.getVolume() >= minVolume) && (stock.getVolume() < quarter1)) {
                quarter1Array.add(baseSymbolPool.get(i));
            } else if ((stock.getVolume() > quarter1) && (stock.getVolume() <= quarter2)) {
                quarter2Array.add(baseSymbolPool.get(i));
            } else if ((stock.getVolume() > quarter2) && (stock.getVolume() <= quarter3)) {
                quarter3Array.add(baseSymbolPool.get(i));
            } else if ((stock.getVolume() > quarter3) && (stock.getVolume() <= quarter4)) {
                quarter4Array.add(baseSymbolPool.get(i));
            }
        }
        mapHash.put(Language.getString("HIVE_QUARTILE4"), quarter4Array);
        mapHash.put(Language.getString("HIVE_QUARTILE3"), quarter3Array);
        mapHash.put(Language.getString("HIVE_QUARTILE2"), quarter2Array);
        mapHash.put(Language.getString("HIVE_QUARTILE1"), quarter1Array);
    }

    /*
    * groups the <code>MapObject</code> in the <code>baseSymbolPool</code> into 4 quarters according to the change
    * */
    private void groupByChange() {
        mapHash.clear();
//        double max = 0, min = 0;
//        Enumeration enu = DataStore.getSharedInstance().getAllSymbols();
//        for (; enu.hasMoreElements();) {
//            Stock stock = DataStore.getSharedInstance().getStockObject((String) enu.nextElement());
//            if (stock.getChange() > max) {
//                max = stock.getChange();
//            } else if (stock.getChange() < min) {
//                min = stock.getChange();
//            }
//        }

        double quarter1 = ((maxChange - minChange)) / 4 + minChange;
        double quarter2 = ((maxChange - minChange) * 2) / 4 + minChange;
        double quarter3 = ((maxChange - minChange) * 3) / 4 + minChange;
        double quarter4 = maxChange;

        ArrayList quarter1Array = new ArrayList();
        ArrayList quarter2Array = new ArrayList();
        ArrayList quarter3Array = new ArrayList();
        ArrayList quarter4Array = new ArrayList();

        for (int i = 0; i < baseSymbolPool.size(); i++) {
            String symbol = ((MapObject) baseSymbolPool.get(i)).getLabel();
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            if ((stock.getChange() >= minChange) && (stock.getChange() < quarter1)) {
                quarter1Array.add(baseSymbolPool.get(i));
            } else if ((stock.getChange() > quarter1) && (stock.getChange() <= quarter2)) {
                quarter2Array.add(baseSymbolPool.get(i));
            } else if ((stock.getChange() > quarter2) && (stock.getChange() <= quarter3)) {
                quarter3Array.add(baseSymbolPool.get(i));
            } else if ((stock.getChange() > quarter3) && (stock.getChange() <= quarter4)) {
                quarter4Array.add(baseSymbolPool.get(i));
            }
        }
        mapHash.put(Language.getString("HIVE_QUARTILE4"), quarter4Array);
        mapHash.put(Language.getString("HIVE_QUARTILE3"), quarter3Array);
        mapHash.put(Language.getString("HIVE_QUARTILE2"), quarter2Array);
        mapHash.put(Language.getString("HIVE_QUARTILE1"), quarter1Array);
    }

    /*
    * groups the <code>MapObject</code> in the <code>baseSymbolPool</code> into 4 quarters according to the percentage cash flow
    * */
    private void groupByPercentCashFlow() {
        mapHash.clear();
//        double max = 0, min = 0;
//        Enumeration enu = DataStore.getSharedInstance().getAllSymbols();
//        for (; enu.hasMoreElements();) {
//            Stock stock = DataStore.getSharedInstance().getStockObject((String) enu.nextElement());
//            if (stock.getCashFlowPct() > max) {
//                max = stock.getCashFlowPct();
//            } else if (stock.getCashFlowPct() < min) {
//                min = stock.getCashFlowPct();
//            }
//        }

        double quarter1 = ((maxPercentCashFlow - minPercentCashFlow)) / 4 + minPercentCashFlow;
        double quarter2 = ((maxPercentCashFlow - minPercentCashFlow) * 2) / 4 + minPercentCashFlow;
        double quarter3 = ((maxPercentCashFlow - minPercentCashFlow) * 3) / 4 + minPercentCashFlow;
        double quarter4 = maxPercentCashFlow;

        ArrayList quarter1Array = new ArrayList();
        ArrayList quarter2Array = new ArrayList();
        ArrayList quarter3Array = new ArrayList();
        ArrayList quarter4Array = new ArrayList();

        for (int i = 0; i < baseSymbolPool.size(); i++) {
            String symbol = ((MapObject) baseSymbolPool.get(i)).getLabel();
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            if ((stock.getCashFlowPct() >= minPercentCashFlow) && (stock.getCashFlowPct() < quarter1)) {
                quarter1Array.add(baseSymbolPool.get(i));
            } else if ((stock.getCashFlowPct() > quarter1) && (stock.getCashFlowPct() <= quarter2)) {
                quarter2Array.add(baseSymbolPool.get(i));
            } else if ((stock.getCashFlowPct() > quarter2) && (stock.getCashFlowPct() <= quarter3)) {
                quarter3Array.add(baseSymbolPool.get(i));
            } else if ((stock.getCashFlowPct() > quarter3) && (stock.getCashFlowPct() <= quarter4)) {
                quarter4Array.add(baseSymbolPool.get(i));
            }
        }
        mapHash.put(Language.getString("HIVE_QUARTILE4"), quarter4Array);
        mapHash.put(Language.getString("HIVE_QUARTILE3"), quarter3Array);
        mapHash.put(Language.getString("HIVE_QUARTILE2"), quarter2Array);
        mapHash.put(Language.getString("HIVE_QUARTILE1"), quarter1Array);
    }

    /*
    * groups the <code>MapObject</code> in the <code>baseSymbolPool</code> into 4 quarters according to the percentage change
    * */
    private void groupByPercentChange() {
        mapHash.clear();
//        double max = 0, min = 0;
//        Enumeration enu = DataStore.getSharedInstance().getAllSymbols();
//        for (; enu.hasMoreElements();) {
//            Stock stock = DataStore.getSharedInstance().getStockObject((String) enu.nextElement());
//            if (stock.getPercentChange() > max) {
//                max = stock.getPercentChange();
//            } else if (stock.getPercentChange() < min) {
//                min = stock.getPercentChange();
//            }
//        }

        double quarter1 = ((maxPercentChange - minPercentChange)) / 4 + minPercentChange;
        double quarter2 = ((maxPercentChange - minPercentChange) * 2) / 4 + minPercentChange;
        double quarter3 = ((maxPercentChange - minPercentChange) * 3) / 4 + minPercentChange;
        double quarter4 = maxPercentChange;

        ArrayList quarter1Array = new ArrayList();
        ArrayList quarter2Array = new ArrayList();
        ArrayList quarter3Array = new ArrayList();
        ArrayList quarter4Array = new ArrayList();

        for (int i = 0; i < baseSymbolPool.size(); i++) {
            String symbol = ((MapObject) baseSymbolPool.get(i)).getLabel();
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            if ((stock.getPercentChange() >= minPercentChange) && (stock.getPercentChange() < quarter1)) {
                quarter1Array.add(baseSymbolPool.get(i));
            } else if ((stock.getPercentChange() > quarter1) && (stock.getPercentChange() <= quarter2)) {
                quarter2Array.add(baseSymbolPool.get(i));
            } else if ((stock.getPercentChange() > quarter2) && (stock.getPercentChange() <= quarter3)) {
                quarter3Array.add(baseSymbolPool.get(i));
            } else if ((stock.getPercentChange() > quarter3) && (stock.getPercentChange() <= quarter4)) {
                quarter4Array.add(baseSymbolPool.get(i));
            }
        }
        mapHash.put(Language.getString("HIVE_QUARTILE4"), quarter4Array);
        mapHash.put(Language.getString("HIVE_QUARTILE3"), quarter3Array);
        mapHash.put(Language.getString("HIVE_QUARTILE2"), quarter2Array);
        mapHash.put(Language.getString("HIVE_QUARTILE1"), quarter1Array);
    }

    /*
    * adds all the <code>MapObject</code> objects in the <code>baseSymbolPool</code> into a single group
    * */
    private void groupByNoGroup() {
        mapHash.clear();
        mapHash.put(Language.getString("ALL"), baseSymbolPool);
    }

    /*
    * decides the criteria in which the <code>MapObject</code> objects should be grouped.
    * decided by the value of <code>PerformaceHiveWindow.selectedGroupOption</code> which varies depending on the user selction.
    * */
    private void setGroupByValue() {
        try {
            if (PerformaceHiveWindow.selectedGroupOption == PerformaceHiveWindow.GROUP_BY_EXCHANGE) {
                groupByExcahnge();
            } else if (PerformaceHiveWindow.selectedGroupOption == PerformaceHiveWindow.GROUP_BY_VOLUME) {
                groupByVolume();
            } else if (PerformaceHiveWindow.selectedGroupOption == PerformaceHiveWindow.GROUP_BY_CHANGE) {
                groupByChange();
            } else if (PerformaceHiveWindow.selectedGroupOption == PerformaceHiveWindow.GROUP_BY_PERCENT_CHANGE) {
                groupByPercentChange();
            } else if (PerformaceHiveWindow.selectedGroupOption == PerformaceHiveWindow.GROUP_BY_PERCENT_CASHFLOW) {
                groupByPercentCashFlow();
            } else if (PerformaceHiveWindow.selectedGroupOption == PerformaceHiveWindow.GROUP_BY_NO_GROUP) {
                groupByNoGroup();
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of ca.tch statement use File | Settings | File Templates.
        }
    }

    /*
    * sets the size attribute of all the <code>MapObject</code> objects depending on the <code>PerformaceHiveWindow.selectedSizeOption</code>
    * which varies depending on user selection
    * */
    private void setSizeByValue() {
        try {
            for (int i = 0; i < baseSymbolPool.size(); i++) {
                try {
                    MapObject mapObject = (MapObject) baseSymbolPool.get(i);
                    String symbol = mapObject.getLabel();
                    Stock stock = DataStore.getSharedInstance().getStockObject(symbol);

                    if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_VOLUME) {
                        mapObject.setSize(stock.getVolume());
                    } else if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_CHANGE) {
                        mapObject.setSize(stock.getChange());
                    } else if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_PERCENT_CHANGE) {
                        mapObject.setSize(stock.getPercentChange());
                    } else if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_PERCENT_CASHFLOW) {
                        mapObject.setSize(stock.getCashFlowPct());
                    } else if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_BID_QTY) {
                        mapObject.setSize(stock.getBestBidQuantity());
                    } else if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_ASK_QTY) {
                        mapObject.setSize(stock.getBestAskQuantity());
                    } else if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_TURNOVER) {
                        mapObject.setSize(stock.getTurnover());
                    } else if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_TRADES) {
                        mapObject.setSize(stock.getTradeQuantity());
                    } else if (PerformaceHiveWindow.selectedSizeOption == PerformaceHiveWindow.SIZE_BY_EQUAL_SIZE) {
                        mapObject.setSize(5D);
                    }
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
    }

    /*
    * sets the color(position) attribute of all the <code>MapObject</code> objects depending on the <code>PerformaceHiveWindow.selectedColorOption</code>
    * which varies depending on user selection
    * */
    private void setColorByValue() {
        for (int i = 0; i < baseSymbolPool.size(); i++) {
            try {
                MapObject mapObject = (MapObject) baseSymbolPool.get(i);
                String symbol = mapObject.getLabel();
                Stock stock = DataStore.getSharedInstance().getStockObject(symbol);

                if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_CHANGE) {
                    mapObject.setColor(stock.getChange());
                } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_VOLUME) {
                    mapObject.setColor(stock.getVolume());
                } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_PERCENT_CHANGE) {
                    mapObject.setColor(stock.getPercentChange());
                } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_PERCENT_CASHFLOW) {
                    mapObject.setColor(stock.getCashFlowPct());
                } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_BID_QTY) {
                    mapObject.setColor(stock.getBestBidQuantity());
                } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_ASK_QTY) {
                    mapObject.setColor(stock.getBestAskQuantity());
                } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_TURNOVER) {
                    mapObject.setColor(stock.getTurnover());
                } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_TRADES) {
                    mapObject.setColor(stock.getTradeQuantity());
                }

            } catch (Exception e) {
            }
        }
    }

    /**
     * sets maximum and minimum values of attributes. All available symbols in the symbol store is taken into account when calculating these values.
     * maxLastTraded - maximum last traded value.
     * minLastTraded - minimum last traded value.
     * maxVolume - maximum volume.
     * minVolume - minimum volume.
     * maxBidQty - maximum bid quantity.
     * minBidQty - minimum bid quantity.
     * maxOfferQty - maximum offer quantity.
     * minOfferQty - minmum offer quantity.
     * maxChange - maximum change.
     * minBidQty - minimum change.
     * maxPercentChange - maximum percentage change.
     * minPercentChange - minimum percentage change.
     * maxPercentCashFlow - maximum percentage cash flow.
     * minPercentCashFlow - minimum percentage cash flow.
     * maxTurnOver - maximum turnover.
     * minTurnOver - minimum turnover.
     * maxTrades - maximum trades.
     * minTrades - minimum trades.
     */
    public void getMaxMinSymbolDetails() {
        maxLastTraded = 0;
        minLastTraded = 0;
        maxVolume = 0;
        minVolume = 0;
        maxBidQty = 0;
        minBidQty = 0;
        maxOfferQty = 0;
        minOfferQty = 0;
        maxChange = 0;
        minChange = 0;
        maxPercentChange = 0;
        minPercentChange = 0;
        maxPercentCashFlow = 0;
        minPercentCashFlow = 0;
        maxTurnOver = 0;
        minTurnOver = 0;
        maxTrades = 0;
        minTrades = 0;

        Enumeration enu = DataStore.getSharedInstance().getAllSymbols();
        for (; enu.hasMoreElements(); ) {
            Stock stock = DataStore.getSharedInstance().getStockObject((String) enu.nextElement());
            if (stock.getLastTradedPrice() > maxLastTraded) {
                maxLastTraded = stock.getVolume();
            } else if (stock.getVolume() < minLastTraded) {
                minLastTraded = stock.getVolume();
            }

            if (stock.getVolume() > maxVolume) {
                maxVolume = stock.getVolume();
            } else if (stock.getVolume() < minVolume) {
                minVolume = stock.getVolume();
            }

            if (stock.getBestBidQuantity() > maxBidQty) {
                maxBidQty = stock.getBestBidQuantity();
            } else if (stock.getBestBidQuantity() < minBidQty) {
                minBidQty = stock.getBestBidQuantity();
            }

            if (stock.getBestAskQuantity() > maxOfferQty) {
                maxOfferQty = stock.getBestAskQuantity();
            } else if (stock.getBestAskQuantity() < minOfferQty) {
                minOfferQty = stock.getBestAskQuantity();
            }

            if (stock.getChange() > maxChange) {
                maxChange = stock.getChange();
            } else if (stock.getChange() < minChange) {
                minChange = stock.getChange();
            }

            if (stock.getPercentChange() > maxPercentChange) {
                maxPercentChange = stock.getPercentChange();
            } else if (stock.getPercentChange() < minPercentChange) {
                minPercentChange = stock.getPercentChange();
            }

            if (stock.getCashFlowPct() > maxPercentCashFlow) {
                maxPercentCashFlow = stock.getCashFlowPct();
            } else if (stock.getCashFlowPct() < minPercentCashFlow) {
                minPercentCashFlow = stock.getCashFlowPct();
            }

            if (stock.getTurnover() > maxTurnOver) {
                maxTurnOver = stock.getTurnover();
            } else if (stock.getTurnover() < minTurnOver) {
                minTurnOver = stock.getTurnover();
            }

            if (stock.getTradeQuantity() > maxTrades) {
                maxTrades = stock.getTradeQuantity();
            } else if (stock.getTradeQuantity() < minTrades) {
                minTrades = stock.getTradeQuantity();
            }
        }
        getMaxMinColorValues();
        lastTradedFactor = (maxLastTraded - minLastTraded) / 100;
        volumeFactor = (maxVolume - minVolume) / 100;
        bidQtyFactor = (maxBidQty - minBidQty) / 100;
        offerQtyFactor = (maxOfferQty - minOfferQty) / 100;
    }

    /*
    * assigns a price rank to the <code>MapObject</code>
    * */
    public void assignPriceRank(MapObject mapObject) {
        double objectPrice = Double.parseDouble(mapObject.getLastPrice());
        int rank = (int) Math.round((maxLastTraded - objectPrice) / lastTradedFactor);
        if (rank == 0) {
            mapObject.setPriceRank(1);
        } else {
            mapObject.setPriceRank(rank);
        }
    }

    /*
    * assigns a volume rank to the <code>MapObject</code>
    * */
    public void assignVolumeRank(MapObject mapObject) {
        long objectVolume = Long.parseLong(mapObject.getVolume());
        int rank = Math.round((maxVolume - objectVolume) / volumeFactor);
        if (rank == 0) {
            mapObject.setVolumeRank(1);
        } else {
            mapObject.setVolumeRank(rank);
        }
    }

    /*
    * assigns a bid quantity rank to the <code>MapObject</code>
    * */
    public void assignBidQtyRank(MapObject mapObject) {
        long objectBidQty = mapObject.getBidQty();
        int rank = Math.round((maxBidQty - objectBidQty) / bidQtyFactor);
        if (rank == 0) {
            mapObject.setBidQtyRank(1);
        } else {
            mapObject.setBidQtyRank(rank);
        }
    }

    /*
    * assigns a ask quantity rank to the <code>MapObject</code>
    * */
    public void assignAskQtyRank(MapObject mapObject) {
        long objectAskQty = mapObject.getAskQty();
        int rank = Math.round((maxOfferQty - objectAskQty) / offerQtyFactor);
        if (rank == 0) {
            mapObject.setAskQtyRank(1);
        } else {
            mapObject.setAskQtyRank(rank);
        }
    }

    /*
    * assigns a position rank to the <code>MapObject</code>
    * */
    public void assignPositionRank(MapObject mapObject) {
        double objectPosition = 0;

        if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_CHANGE) {
            objectPosition = Double.parseDouble(mapObject.getChange());
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_VOLUME) {
            objectPosition = Double.parseDouble(mapObject.getVolume());
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_PERCENT_CHANGE) {
            objectPosition = Double.parseDouble(mapObject.getPercentChange());
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_PERCENT_CASHFLOW) {
            objectPosition = mapObject.getPercentCashFlow();
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_BID_QTY) {
            objectPosition = mapObject.getBidQty();
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_ASK_QTY) {
            objectPosition = mapObject.getAskQty();
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_TURNOVER) {
            objectPosition = Double.parseDouble(mapObject.getTurnOver());
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_TRADES) {
            objectPosition = Double.parseDouble(mapObject.getTrades());
        }
        int rank = (int) Math.round((maxPosition - objectPosition) / positionFactor);
        if (rank == 0) {
            mapObject.setPositionRank(1);
        } else {
            mapObject.setPositionRank(rank);
        }
    }

    /*
    * adjusts the <code>baseSymbolPool</code> to contain objects within the range */
    public void adjustBaseToRank() {
        for (int i = 0; i < baseSymbolPool.size(); i++) {
            MapObject mapObject = (MapObject) baseSymbolPool.get(i);
            int priceRank = mapObject.getPriceRank();
            int volumeRank = mapObject.getVolumeRank();
            int bidQtyRank = mapObject.getBidQtyRank();
            int askQtyRank = mapObject.getAskQtyRank();
            int positionRank = mapObject.getPositionRank();
            boolean priceRankCondition = (priceRank >= PerformaceHiveWindow.selectedLowPriceRank) && (priceRank <= PerformaceHiveWindow.selectedHighPriceRank);
            boolean volumeRankCondition = (volumeRank >= PerformaceHiveWindow.selectedLowVolumeRank) && (volumeRank <= PerformaceHiveWindow.selectedHighVolumeRank);
            boolean bidQtyRankCondition = (bidQtyRank >= PerformaceHiveWindow.selectedLowBidQtyRank) && (bidQtyRank <= PerformaceHiveWindow.selectedHighBidQtyRank);
            boolean askQtyRankCondition = (askQtyRank >= PerformaceHiveWindow.selectedLowOfferQtyRank) && (askQtyRank <= PerformaceHiveWindow.selectedHighOfferQtyRank);
            boolean positionRankCondition = (positionRank >= PerformaceHiveWindow.selectedLowPositionRank) && (positionRank <= PerformaceHiveWindow.selectedHighPositionRank);
            if (priceRankCondition && volumeRankCondition && bidQtyRankCondition && askQtyRankCondition && positionRankCondition) {
                continue;
            } else {
                baseSymbolPool.remove(mapObject);
                i--;
            }
        }
    }

    public void adjustBaseToSymbolSearch(String text) {
        if (!text.equals("")) {
            for (int i = 0; i < baseSymbolPool.size(); i++) {
                MapObject mapObject = (MapObject) baseSymbolPool.get(i);
                String symbol = mapObject.getSymbol();
                if (symbol.contains(text.subSequence(0, text.length()))) {
                    continue;
                } else {
                    baseSymbolPool.remove(mapObject);
                    i--;
                }
            }
        }
    }

    public void getMaxMinColorValues() {
        maxPosition = 0;
        minPosition = 0;
        if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_CHANGE) {
            maxPosition = maxChange;
            minPosition = minChange;
            positionFactor = (maxPosition - minPosition) / 100;
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_VOLUME) {
            maxPosition = maxVolume;
            minPosition = 0;
            positionFactor = (maxPosition - minPosition) / 50;
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_PERCENT_CHANGE) {
            maxPosition = maxPercentChange;
            minPosition = minPercentChange;
            positionFactor = (maxPosition - minPosition) / 100;
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_PERCENT_CASHFLOW) {
            maxPosition = maxPercentCashFlow;
            minPosition = minPercentCashFlow;
            positionFactor = (maxPosition - minPosition) / 100;
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_BID_QTY) {
            maxPosition = maxBidQty;
            minPosition = 0;
            positionFactor = (maxPosition - minPosition) / 50;
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_ASK_QTY) {
            maxPosition = maxOfferQty;
            minPosition = 0;
            positionFactor = (maxPosition - minPosition) / 50;
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_TURNOVER) {
            maxPosition = maxTurnOver;
            minPosition = 0;
            positionFactor = (maxPosition - minPosition) / 50;
        } else if (PerformaceHiveWindow.selectedColorOption == PerformaceHiveWindow.COLOR_BY_TRADES) {
            maxPosition = maxTrades;
            minPosition = 0;
            positionFactor = (maxPosition - minPosition) / 50;
        }
    }
}
