package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 31, 2009
 * Time: 11:42:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class MapObject {
    private String label;
    private String symbol;
    private String lastPrice;
    private String percentChange;
    private String volume;
    private String lastQty;
    private String change;
    private String vWap;
    private String turnOver;
    private String trades;
    private String description;
    private long bidQty = 0;
    private long askQty = 0;
    private double percentCashFlow = 0;
    private double sizeComponent = 5D;
    private double colorComponent = 5D;

    private int priceRank = 100;
    private int volumeRank = 100;
    private int bidQtyRank = 100;
    private int askQtyRank = 100;
    private int positionRank = 100;

    public MapObject(String label, String symbol, String lastPrice, String percentChange, String volume, String lastQty, String change, String vWap, String turnOver, String trades, String description) {
        this.label = label;
        this.symbol = symbol;
        this.lastPrice = lastPrice;
        this.percentChange = percentChange;
        this.volume = volume;
        this.lastQty = lastQty;
        this.change = change;
        this.vWap = vWap;
        this.turnOver = turnOver;
        this.trades = trades;
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public double getSize() {
        return sizeComponent;
    }

    public void setSize(double size) {
        this.sizeComponent = size;
    }

    public double getColor() {
        return colorComponent;
    }

    public void setColor(double color) {
        this.colorComponent = color;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getLastPrice() {
        return lastPrice;
    }

    public String getPercentChange() {
        return percentChange;
    }

    public String getLastQty() {
        return lastQty;
    }

    public String getDescription() {
        return description;
    }

    public String getChange() {
        return change;
    }

    public String getVWap() {
        return vWap;
    }

    public String getTurnOver() {
        return turnOver;
    }

    public String getTrades() {
        return trades;
    }

    public String getVolume() {
        return volume;
    }

    public long getBidQty() {
        return bidQty;
    }

    public void setBidQty(long qty) {
        this.bidQty = qty;
    }

    public long getAskQty() {
        return askQty;
    }

    public void setAskQty(long qty) {
        this.askQty = qty;
    }

    public double getPercentCashFlow() {
        return percentCashFlow;
    }

    public void setPercentCashFlow(double value) {
        this.percentCashFlow = value;
    }

    public int getPriceRank() {
        return priceRank;
    }

    public void setPriceRank(int rank) {
        this.priceRank = rank;
    }

    public int getVolumeRank() {
        return volumeRank;
    }

    public void setVolumeRank(int rank) {
        this.volumeRank = rank;
    }

    public int getBidQtyRank() {
        return bidQtyRank;
    }

    public void setBidQtyRank(int rank) {
        this.bidQtyRank = rank;
    }

    public int getAskQtyRank() {
        return askQtyRank;
    }

    public void setAskQtyRank(int rank) {
        this.askQtyRank = rank;
    }

    public int getPositionRank() {
        return positionRank;
    }

    public void setPositionRank(int rank) {
        this.positionRank = rank;
    }
}
