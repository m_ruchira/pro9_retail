package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:50:41 AM
 * To change this template use File | Settings | File Templates.
 */

import javax.swing.*;
import java.awt.*;

public class UniqueColorProvider extends ColorProvider {
    private static final Color DEFAULT_COLOR = new Color(153, 153, 51);
    private Color color;
    private JPanel legend;

    public UniqueColorProvider() {
        color = DEFAULT_COLOR;
    }


    public UniqueColorProvider(Color color) {
        this.color = color;
    }

    public Color getColor(Value value) {
        return color;
    }

    public JPanel getLegendPanel() {
        if (legend == null)
            legend = new Legend();
        return legend;
    }

    private static class Legend extends JPanel {

        public Legend() {
            setPreferredSize(new Dimension(100, 40));
        }

        public void paint(Graphics g) {
            g.setColor(Color.black);
            g.drawString("Unique Color Provider", 20, 20);
        }
    }
}

