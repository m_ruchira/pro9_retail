package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:45:37 AM
 * To change this template use File | Settings | File Templates.
 */

import java.io.Serializable;

public class TreeMapNodeBuilder implements Serializable {
    private TreeMapNode root;

    public TreeMapNodeBuilder() {
    }

    public TreeMapNode buildBranch(String label, TreeMapNode parent) {
        TreeMapNode node = new TreeMapNode(label);
        if (parent != null)
            parent.add(node);
        else if (root == null)
            root = node;
        return node;
    }

    public TreeMapNode buildLeaf(String label, double weight, Value value, String symbol, String lastPrice, String percentChange, String volume, String lastQty, String change, String vWap, String turnOver, String trades, String description, TreeMapNode parent) {
        TreeMapNode node = new TreeMapNode(label, weight, value, symbol, lastPrice, percentChange, volume, lastQty, change, vWap, turnOver, trades, description);
        if (parent != null)
            parent.add(node);
        else if (root == null)
            root = node;
        return node;
    }

    public TreeMapNode getRoot() {
        return root;
    }
}