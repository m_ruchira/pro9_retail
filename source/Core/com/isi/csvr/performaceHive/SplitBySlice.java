package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:53:39 AM
 * To change this template use File | Settings | File Templates.
 */

import java.util.Iterator;
import java.util.Vector;

public class SplitBySlice extends SplitStrategy {

    public SplitBySlice() {
    }

    public static void splitInSlice(int x0, int y0, int w0, int h0, Vector v, double sumWeight) {
        int offset = 0;
        boolean vertical = h0 > w0;
        for (Iterator i$ = v.iterator(); i$.hasNext(); ) {
            TreeMapNode node = (TreeMapNode) i$.next();
            if (vertical) {
                node.setX(x0);
                node.setWidth(w0);
                node.setY(y0 + offset);
                node.setHeight((int) Math.round(((double) h0 * node.getWeight()) / sumWeight));
                offset += node.getHeight();
            } else {
                node.setX(x0 + offset);
                node.setWidth((int) Math.round(((double) w0 * node.getWeight()) / sumWeight));
                node.setY(y0);
                node.setHeight(h0);
                offset += node.getWidth();
            }
        }

        if (!v.isEmpty()) {
            TreeMapNode node = (TreeMapNode) v.lastElement();
            if (vertical && h0 != offset)
                node.setHeight((node.getHeight() - offset) + h0);
            else if (!vertical && w0 != offset)
                node.setWidth((node.getWidth() - offset) + w0);
        }
    }

    public void splitElements(Vector vector, Vector vector1, Vector vector2) {
    }

    protected void calculatePositionsRec(int x0, int y0, int w0, int h0, double weight0, Vector v) {
        splitInSlice(x0, y0, w0, h0, v, weight0);
        for (Iterator i$ = v.iterator(); i$.hasNext(); ) {
            TreeMapNode node = (TreeMapNode) i$.next();
            if (node.isLeaf()) {
                node.setX(node.getX() + TreeMapNode.getBorder());
                node.setY(node.getY() + TreeMapNode.getBorder());
                node.setHeight(node.getHeight() - TreeMapNode.getBorder());
                node.setWidth(node.getWidth() - TreeMapNode.getBorder());
            } else if (TreeMapNode.getBorder() > 1) {
                TreeMapNode.setBorder(TreeMapNode.getBorder() - 2);
                calculatePositionsRec(node.getX() + 2, node.getY() + 2, node.getWidth() - 2, node.getHeight() - 2, node.getWeight(), node.getChildren());
                TreeMapNode.setBorder(TreeMapNode.getBorder() + 2);
            } else if (TreeMapNode.getBorder() == 1) {
                TreeMapNode.setBorder(0);
                calculatePositionsRec(node.getX() + 1, node.getY() + 1, node.getWidth() - 1, node.getHeight() - 1, node.getWeight(), node.getChildren());
                TreeMapNode.setBorder(1);
            } else {
                calculatePositionsRec(node.getX(), node.getY(), node.getWidth(), node.getHeight(), node.getWeight(), node.getChildren());
            }
        }

    }
}

