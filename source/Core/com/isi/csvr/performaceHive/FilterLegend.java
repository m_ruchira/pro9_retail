package com.isi.csvr.performaceHive;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Aug 13, 2009
 * Time: 5:18:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class FilterLegend extends JPanel {
    private int tabColor[];

    public FilterLegend() {
        super();
        tabColor = new int[25];
        int baseCode = 255;
        for (int i = 24; i >= 0; i--, baseCode -= 5) {
            tabColor[i] = baseCode;
        }
        setPreferredSize(new Dimension(tabColor.length * 4 + 100, 20));
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int xCursor = tabColor.length - 1;
        for (int i = tabColor.length - 1; i > 0; i--) {
            g.setColor(new Color(0, tabColor[i], 0));
            g.fillRect(xCursor * 2, 0, 2, 20);
            xCursor--;
        }
        xCursor = (tabColor.length - 1);
        for (int i = 0, j = 0; i < 25; i++, j += 10) {
            g.setColor(new Color(j, 255, j));
            g.fillRect(xCursor * 2, 0, 2, 20);
            xCursor++;
        }

        for (int i = 0, j = 250; i < 25; i++, j -= 10) {
            g.setColor(new Color(255, j, j));
            g.fillRect(xCursor * 2, 0, 2, 20);
            xCursor++;
        }

        for (int i = tabColor.length - 1; i >= 0; i--) {
            g.setColor(new Color(tabColor[i], 0, 0));
            g.fillRect(xCursor * 2, 0, 2, 20);
            xCursor++;
        }
    }
}
