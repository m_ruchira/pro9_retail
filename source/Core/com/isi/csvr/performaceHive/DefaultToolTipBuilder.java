package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:49:48 AM
 * To change this template use File | Settings | File Templates.
 */

import javax.swing.*;
import java.io.Serializable;

public class DefaultToolTipBuilder
        implements IToolTipBuilder, Serializable {
    private static JToolTip instance = null;
    private JTreeMap jTreeMap;
    private String weightPrefix;
    private String valuePrefix;
    private boolean showWeight;

    public DefaultToolTipBuilder(JTreeMap jTreeMap, String weightPrefix, String valuePrefix, boolean showWeight) {
        this.jTreeMap = jTreeMap;
        this.weightPrefix = weightPrefix;
        this.valuePrefix = valuePrefix;
        this.showWeight = showWeight;
    }

    public JToolTip getToolTip() {
        if (instance == null)
            instance = new DefaultToolTip(jTreeMap, weightPrefix, valuePrefix, showWeight);
        return instance;
    }
}
