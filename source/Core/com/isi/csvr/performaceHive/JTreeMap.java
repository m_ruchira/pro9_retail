package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:41:00 AM
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWFont;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.Serializable;
import java.util.Enumeration;

public class JTreeMap extends JComponent {

    private JTree treeView;
    private TreeMapNode activeLeaf;
    private ColorProvider colorProvider;
    private TreeMapNode displayedRoot;
    private TreeMapNode root;
    private SplitStrategy strategy;
    private IToolTipBuilder toolTipBuilder;
    private Zoom zoom;

    public JTreeMap(TreeMapNode root) {
        this(root, ((new SplitSquarified())), null, null, false);
    }

    public JTreeMap(TreeMapNode root, JTree treeView) {
        this(root, ((new SplitSquarified())), null, null, false);
        this.treeView = treeView;
    }

    public JTreeMap(TreeMapNode root, SplitStrategy strategy, JTree treeView, String weightPrefix, String valuePrefix, boolean showWeight) {
        this(root, strategy, weightPrefix, valuePrefix, showWeight);
        this.treeView = treeView;
    }


    public JTreeMap(TreeMapNode root, SplitStrategy strategy, String weightPrefix, String valuePrefix, boolean showWeight) {
        activeLeaf = null;
        colorProvider = null;
        displayedRoot = null;
        this.root = null;
        this.strategy = null;
        ToolTipManager ttm = ToolTipManager.sharedInstance();
        ttm.setInitialDelay(0);
        ttm.setReshowDelay(0);
        ttm.setDismissDelay(0x186a0);
        ttm.setEnabled(true);
        ttm.setLightWeightPopupEnabled(true);
        setToolTipText("");
        toolTipBuilder = new DefaultToolTipBuilder(this, weightPrefix, valuePrefix, showWeight);
        zoom = new Zoom();
        setRoot(root);
        setStrategy(strategy);
        setColorProvider(new UniqueColorProvider());
        addMouseMotionListener(new HandleMouseMotion());
        addMouseListener(new HandleMouseClick());
    }

    public void calculatePositions() {
        if (getStrategy() != null && displayedRoot != null)
            getStrategy().calculatePositions(displayedRoot);
    }

    public JToolTip createToolTip() {
        return toolTipBuilder.getToolTip();
    }

    protected void draw(Graphics g, TreeMapNode item) {
        if (item.isLeaf() && item.getValue() != null) {
            g.setColor(colorProvider.getColor(item.getValue()));
            g.fillRect(item.getX(), item.getY(), item.getWidth() + 2, item.getHeight() + 2);
            Font fontSymbol = new TWFont("Arial", TWFont.BOLD, 11);
            FontMetrics fmSymbol = getFontMetrics(fontSymbol);
            int currentY = 20 + fmSymbol.getHeight();
            if ((currentY < item.getHeight()) && (fmSymbol.stringWidth(item.getSymbol()) < (item.getWidth() + 2))) {
                g.setFont(fontSymbol);
                g.setColor(Color.black);
                g.drawString(item.getSymbol(), item.getX() + 5, item.getY() + 20);

                Font fontLast = new TWFont("Arial", TWFont.PLAIN, 10);
                FontMetrics fmLast = getFontMetrics(fontLast);
                currentY = 20 + fmSymbol.getHeight() + 2 + fmLast.getHeight();
                String lastString = Language.getString("LAST") + "   " + item.getLastPrice();
                if ((currentY < item.getHeight()) && (fmLast.stringWidth(lastString) < (item.getWidth() + 2))) {
                    g.setFont(fontLast);
                    g.setColor(Color.black);
                    g.drawString(lastString, item.getX() + 5, item.getY() + 20 + fmSymbol.getHeight() + 2);

                    Font fontPercentChange = new TWFont("Arial", TWFont.PLAIN, 10);
                    FontMetrics fmPercentChange = getFontMetrics(fontPercentChange);
                    currentY = 20 + fmSymbol.getHeight() + 2 + fmLast.getHeight() + 2 + fmPercentChange.getHeight();
                    String percentChangeString = Language.getString("PCT_CHANGE") + "   " + item.getPercentChange();
                    if ((currentY < item.getHeight()) && (fmPercentChange.stringWidth(percentChangeString) < (item.getWidth() + 2))) {
                        g.setFont(fontPercentChange);
                        g.setColor(Color.black);
                        g.drawString(percentChangeString, item.getX() + 5, item.getY() + 20 + fmSymbol.getHeight() + 2 + fmLast.getHeight() + 2);

                        Font fontVolume = new TWFont("Arial", TWFont.PLAIN, 10);
                        FontMetrics fmVolume = getFontMetrics(fontVolume);
                        currentY = 20 + fmSymbol.getHeight() + 2 + fmLast.getHeight() + 2 + fmPercentChange.getHeight() + 2 + fmVolume.getHeight();
                        String volumeString = Language.getString("VOLUME") + "   " + item.getVolume();
                        if ((currentY < item.getHeight()) && (fmVolume.stringWidth(volumeString) < (item.getWidth() + 2))) {
                            g.setFont(fontVolume);
                            g.setColor(Color.black);
                            g.drawString(volumeString, item.getX() + 5, item.getY() + 20 + fmSymbol.getHeight() + 2 + fmLast.getHeight() + 2 + fmPercentChange.getHeight() + 2);
                        }
                    }
                }
            }
            g.setColor(Color.GRAY);
            g.drawRect(item.getX(), item.getY(), item.getWidth() + 2, item.getHeight() + 2);
        } else {
            for (Enumeration e = item.children(); e.hasMoreElements(); draw(g, (TreeMapNode) e.nextElement())) ;
//            g.setColor(Color.blue);
//            g.fillRect(item.getX() + 5, item.getY() + 5, item.getWidth() - 4 , 10);
        }
    }

    protected void drawLabel(Graphics g, TreeMapNode item) {
        FontMetrics fm = g.getFontMetrics(g.getFont());
        if (fm.getHeight() < item.getHeight() - 2) {
            String label = item.getLabel();
            int stringWidth = fm.stringWidth(label);
            if (item.getWidth() - 5 <= stringWidth) {
                int nbChar = (label.length() * item.getWidth()) / stringWidth;
                if (nbChar > 3)
                    label = (new StringBuilder()).append(label.substring(0, nbChar - 3)).append("...").toString();
                else
                    label = "";
            }
            g.setColor(Color.blue);
            g.fillRect(item.getX() + 5, item.getY() + 2, item.getWidth() - 2, 10);
            Font labelFont = new TWFont("Ariel", TWFont.BOLD, 10);
            g.setFont(labelFont);
            g.setColor(Color.black);
            g.drawString(label, item.getX() + 7, item.getY() + 10);
        }
    }

    protected void drawLabels(Graphics g, TreeMapNode item) {
        g.setFont(getFont());
        if (displayedRoot.isLeaf()) {
            drawLabel(g, displayedRoot);
        } else {
            for (Enumeration e = displayedRoot.children(); e.hasMoreElements(); drawLabel(g, (TreeMapNode) (TreeMapNode) e.nextElement()))
                ;
        }
    }

    public TreeMapNode getActiveLeaf() {
        return activeLeaf;
    }

    public void setActiveLeaf(TreeMapNode newActiveLeaf) {
        if (newActiveLeaf == null || newActiveLeaf.isLeaf())
            activeLeaf = newActiveLeaf;
    }

    public ColorProvider getColorProvider() {
        return colorProvider;
    }

    public void setColorProvider(ColorProvider newColorProvider) {
        colorProvider = newColorProvider;
    }

    public TreeMapNode getDisplayedRoot() {
        return displayedRoot;
    }

    public void setDisplayedRoot(TreeMapNode newDisplayedRoot) {
        displayedRoot = newDisplayedRoot;
    }

    public TreeMapNode getRoot() {
        return root;
    }

    public void setRoot(TreeMapNode newRoot) {
        root = newRoot;
        Insets insets = getInsets();
        root.setX(insets.left);
        root.setY(insets.top);
        setDisplayedRoot(root);
    }

    public SplitStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(SplitStrategy newStrat) {
        strategy = newStrat;
    }

    public Point getToolTipLocation(MouseEvent event) {
        JToolTip toolTip = createToolTip();
        int xMax = displayedRoot.getX() + displayedRoot.getWidth();
        int yMin = displayedRoot.getY();
        if (activeLeaf != null) {
            int posX;
            int posY;
            if (activeLeaf.getWidth() >= toolTip.getWidth() + 8 && activeLeaf.getHeight() >= toolTip.getHeight() + 8) {
                posX = activeLeaf.getX() + 4;
                posY = activeLeaf.getY() + 4;
            } else {
                posX = activeLeaf.getX() + activeLeaf.getWidth() + 4;
                posY = activeLeaf.getY() - toolTip.getHeight() - 4;
            }
            if (posY < yMin + 4)
                posY = yMin + 4;
            if (posX + toolTip.getWidth() > xMax - 4 && activeLeaf.getX() >= toolTip.getWidth() + 4)
                posX = activeLeaf.getX() - 4 - toolTip.getWidth();
            return new Point(posX, posY);
        } else {
            return null;
        }
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int width = getSize().width;
        int height = getSize().height;
        Insets insets = getInsets();
        int border = TreeMapNode.getBorder();
        root.setDimension(root.getX(), root.getY(), width - border - insets.left - insets.right, height - border - insets.top - insets.bottom);
        if (!root.equals(displayedRoot))
            displayedRoot.setDimension(displayedRoot.getX(), displayedRoot.getY(), width - border - insets.left - insets.right, height - border - insets.top - insets.bottom);
        calculatePositions();
        if (displayedRoot.children().hasMoreElements()) {
            g.setColor(getBackground());
            g.fillRect(displayedRoot.getX(), displayedRoot.getY(), displayedRoot.getWidth() + border, displayedRoot.getHeight() + border);
            draw(g, displayedRoot);
            if (activeLeaf != null)
                reveal(g, activeLeaf);
            drawLabels(g, displayedRoot);
        }
    }

    protected void reveal(Graphics g, TreeMapNode item) {
        if (item.isLeaf()) {
            g.setColor(Color.red);
            g.drawRect(item.getX(), item.getY(), item.getWidth() + 2, item.getHeight() + 2);
            g.drawRect(item.getX() + 1, item.getY() + 1, item.getWidth(), item.getHeight());
        }
    }

    public void setBorder(Border border) {
        Insets insets = getInsets();
        displayedRoot.setDimension(displayedRoot.getX() - insets.left, displayedRoot.getY() - insets.top, displayedRoot.getWidth() + insets.left + insets.right, displayedRoot.getHeight() + insets.top + insets.bottom);
        super.setBorder(border);
        insets = getInsets();
        displayedRoot.setDimension(displayedRoot.getX() + insets.left, displayedRoot.getY() + insets.top, displayedRoot.getWidth() - insets.left - insets.right, displayedRoot.getHeight() - insets.top - insets.bottom);
    }

    public void setToolTipBuilder(IToolTipBuilder toolTipBuilder) {
        this.toolTipBuilder = toolTipBuilder;
    }

    public void setZoomKeepProportion(boolean keepProportion) {
        zoom.setKeepProportion(keepProportion);
    }

    public void unzoom() {
        zoom.undo();
    }

    public void zoom(TreeMapNode dest) {
        unzoom();
        zoom.execute(dest);
    }

    public JTree getTreeView() {
        return treeView;
    }

    public void setTreeView(JTree treeView) {
        this.treeView = treeView;
    }

    private class Zoom
            implements Serializable {

        final JTreeMap this$0;
        private boolean enable;
        private boolean keepProportion;

        public Zoom() {
            super();
            this$0 = JTreeMap.this;
            keepProportion = false;
            enable = true;
        }

        public void execute(TreeMapNode dest) {
            if (enable) {
                setActiveLeaf(null);
                setNewDimension(dest);
                setDisplayedRoot(dest);
                enable = false;
            }
        }

        public boolean isKeepProportion() {
            return keepProportion;
        }

        public void setKeepProportion(boolean keepProportion) {
            this.keepProportion = keepProportion;
        }

        protected void setNewDimension(TreeMapNode dest) {
            dest.setX(getRoot().getX());
            dest.setY(getRoot().getY());
            int rootWidth = getRoot().getWidth();
            int rootHeight = getRoot().getHeight();
            if (isKeepProportion()) {
                int destHeight = dest.getHeight();
                int destWidth = dest.getWidth();
                float divWidth = (float) destWidth / (float) rootWidth;
                float divHeight = (float) destHeight / (float) rootHeight;
                if (divWidth >= divHeight) {
                    dest.setHeight(Math.round((float) destHeight / divWidth));
                    dest.setWidth(rootWidth);
                } else {
                    dest.setHeight(rootHeight);
                    dest.setWidth(Math.round((float) destWidth / divHeight));
                }
            } else {
                dest.setHeight(rootHeight);
                dest.setWidth(rootWidth);
            }
        }

        public void undo() {
            if (!enable) {
                setDisplayedRoot(getRoot());
                enable = true;
            }
        }
    }

    protected class HandleMouseClick extends MouseAdapter {

        final JTreeMap this$0;

        protected HandleMouseClick() {
            super();
            this$0 = JTreeMap.this;
        }

        public void mouseClicked(MouseEvent e) {
            TreeMapNode t = getDisplayedRoot().getActiveLeaf(e.getX(), e.getY());
            if (SwingUtilities.isRightMouseButton(e)) {
                Client.getInstance().updateHivePopup(t.getDescription(), t.getKey());
                GUISettings.showPopup(Client.getInstance().getHivePopup(), PerformaceHiveWindow.getSharedInstance(), e.getX(), e.getY());
            } else {

                Client.getInstance().showDetailQuote(t.getKey());
                repaint();
            }
        }
    }

    protected class HandleMouseMotion extends MouseMotionAdapter {

        final JTreeMap this$0;

        protected HandleMouseMotion() {
            super();
            this$0 = JTreeMap.this;
        }

        public void mouseMoved(MouseEvent e) {
            if (getDisplayedRoot().children().hasMoreElements()) {
                TreeMapNode t = getDisplayedRoot().getActiveLeaf(e.getX(), e.getY());
                if (t != null && !t.equals(getActiveLeaf())) {
                    setActiveLeaf(t);
                    repaint();
                }
            }
        }
    }
}
