package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:53:00 AM
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

public class DefaultToolTip extends JToolTip {
    private JTreeMap jTreeMap;

    public DefaultToolTip(JTreeMap jTreeMap, String weightPrefix, String valuePrefix, boolean showWeight) {
        this.jTreeMap = jTreeMap;
        Dimension size = new Dimension(300, 100);
        setPreferredSize(size);
    }

    public void paint(Graphics g) {
        if (jTreeMap.getActiveLeaf() != null) {
            Graphics g2D = g;
            g2D.setColor(Theme.getColor("DTQ_GENERAL_EVEN_BGCOLOR"));
            g2D.fillRect(0, 0, 300, 20);
            g2D.setColor(Color.black);
            Font nameFont = new TWFont("Ariel", TWFont.BOLD, 12);
            g2D.setFont(nameFont);
            g2D.drawString(jTreeMap.getActiveLeaf().getSymbol() + " - " + jTreeMap.getActiveLeaf().getDescription(), 5, 12);
            g2D.setColor(Theme.getColor("DTQ_GENERAL_BGCOLOR"));
            g2D.fillRect(0, 20, 300, 20);
            g2D.setColor(Color.black);
            Font dataFont = new TWFont("Ariel", TWFont.PLAIN, 11);
            g2D.setFont(dataFont);
            g2D.drawString(Language.getString("HEAT_MAP_LBL_LAST_TRADE"), 5, 32);
            g2D.drawString(jTreeMap.getActiveLeaf().getLastPrice(), getWidth() / 4, 32);
            g2D.drawString(Language.getString("VWAP"), getWidth() / 2, 32);
            g2D.drawString(jTreeMap.getActiveLeaf().getVWap(), 3 * getWidth() / 4, 32);
            g2D.setColor(Theme.getColor("DTQ_GENERAL_EVEN_BGCOLOR"));
            g2D.fillRect(0, 40, 300, 20);
            g2D.setColor(Color.black);
            g2D.drawString(Language.getString("LASTTRADE_QTY"), 5, 52);
            g2D.drawString(jTreeMap.getActiveLeaf().getLastQty(), getWidth() / 4, 52);
            g2D.drawString(Language.getString("VOLUME"), getWidth() / 2, 52);
            g2D.drawString(jTreeMap.getActiveLeaf().getVolume(), 3 * getWidth() / 4, 52);
            g2D.setColor(Theme.getColor("DTQ_GENERAL_BGCOLOR"));
            g2D.fillRect(0, 60, 300, 20);
            g2D.setColor(Color.black);
            g2D.drawString(Language.getString("CHANGE"), 5, 72);
            g2D.drawString(jTreeMap.getActiveLeaf().getChange(), getWidth() / 4, 72);
            g2D.drawString(Language.getString("TURNOVER"), getWidth() / 2, 72);
            g2D.drawString(jTreeMap.getActiveLeaf().getTurnOver(), 3 * getWidth() / 4, 72);
            g2D.setColor(Theme.getColor("DTQ_GENERAL_EVEN_BGCOLOR"));
            g2D.fillRect(0, 80, 300, 20);
            g2D.setColor(Color.black);
            g2D.drawString(Language.getString("HEAT_MAP_CRITERIA_PERC_CHANGE"), 5, 92);
            g2D.drawString(jTreeMap.getActiveLeaf().getPercentChange(), getWidth() / 4, 92);
            g2D.drawString(Language.getString("TRADES"), getWidth() / 2, 92);
            g2D.drawString(jTreeMap.getActiveLeaf().getTrades(), 3 * getWidth() / 4, 92);
            g2D.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
        }
    }
}

