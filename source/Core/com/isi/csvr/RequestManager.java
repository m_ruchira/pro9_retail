package com.isi.csvr;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 27, 2003
 * Time: 10:22:07 AM
 * To change this template use Options | File Templates.
 */
public class RequestManager implements ConnectionListener {
    public static final int ID = 0;
    public static final int REQUEST = 1;
    public static final int COUNTER = 2;
    public static final int PATH = 3;
    private static RequestManager self = null;
    private ArrayList<Object[]> removeStore;
    private ArrayList<Object[]> addStore;
    private Hashtable symbolReferences;

    private RequestManager() {
        removeStore = new ArrayList<Object[]>();
        addStore = new ArrayList<Object[]>();
        symbolReferences = new Hashtable();

        ConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public synchronized static RequestManager getSharedInstance() {
        if (self == null) {
            self = new RequestManager();
        }
        return self;
    }

    public synchronized void addReference(String id, Object ref) {
        symbolReferences.put(id, ref);
    }

    public synchronized Object removeRef(String id) {
        return symbolReferences.remove(id);
    }

    public synchronized void clearReferences(String id) {
        symbolReferences.clear();
    }

    public synchronized void addRemoveRequest(byte path, String id, String request) {
        int index = findRemoveRequest(id);
        Object[] requestRecord;

        System.out.println("Adding remove request " + id);

        if (index >= 0) {
            requestRecord = removeStore.get(index);
            Integer counter = (Integer) requestRecord[COUNTER];
            requestRecord[COUNTER] = new Integer(counter.intValue() + 1);
        } else {
            requestRecord = new Object[4];
            requestRecord[ID] = id;
            requestRecord[REQUEST] = request;
            requestRecord[COUNTER] = new Integer(1);
            requestRecord[PATH] = path;
            removeStore.add(requestRecord);
        }
        requestRecord = null;
    }

    public synchronized void removeRequest(String id) {
        if (id == null) return;

        int index = findRemoveRequest(id);
        Object[] requestRecord;

        if (index >= 0) {
            requestRecord = removeStore.get(index);
            Integer counter = (Integer) requestRecord[COUNTER];
            if (counter > 1) {
                requestRecord[COUNTER] = new Integer(counter.intValue() - 1);
            } else {
//                System.out.println("send remove request = "+id);
                removeStore.remove(index);
                removeAddRequest(id);
                SendQFactory.addData((Byte) requestRecord[PATH], (String) requestRecord[REQUEST]);
            }
        }
        requestRecord = null;
    }

    public synchronized void sendRemoveRequest(String id) {
        if (id == null) return;

        int index = findRemoveRequest(id);
        Object[] requestRecord;

        if (index >= 0) {
//            System.out.println("send remove Market T&S request = "+id);
            requestRecord = removeStore.get(index);
            SendQFactory.addData((Byte) requestRecord[PATH], (String) requestRecord[REQUEST]);
        }
        requestRecord = null;
    }

    private int findRemoveRequest(String id) {
        Object request;
        for (int i = 0; i < removeStore.size(); i++) {
            request = (removeStore.get(i))[ID];
            if (request.equals(id)) {
                return i;
            }
        }
        return -1;
    }


    public synchronized void addAddRequest(byte path, String id, String request) {

        if ((id == null) || (id.equalsIgnoreCase("null"))) return;

        int index = findAddRequest(id);
        Object[] requestRecord;

        if (index == -1) {
            System.out.println("send add request =" + id + " , " + request);
            requestRecord = new Object[4];
            requestRecord[ID] = id;
            requestRecord[REQUEST] = request;
            requestRecord[COUNTER] = new Integer(1);
            requestRecord[PATH] = path;
            addStore.add(requestRecord);
            SendQFactory.addData(path, request);
        }
        requestRecord = null;
    }

    public synchronized void sendAddRequest(byte path, String id, String request) {
        int index = findAddRequest(id);
        Object[] requestRecord;

        if (index == -1) {
//            System.out.println("send add t&S request = "+request);

            requestRecord = new Object[4];
            requestRecord[ID] = id;
            requestRecord[REQUEST] = request;
            requestRecord[COUNTER] = new Integer(1);
            requestRecord[PATH] = path;
            addStore.add(requestRecord);
        }
        SendQFactory.addData(path, request);
        requestRecord = null;
    }

    public synchronized void removeAddRequest(String id) {
        if (id == null) return;

        int index = findAddRequest(id);

        if (index >= 0) {
            addStore.remove(index);
        }
    }

    public int getAddRequestSize() {
        return addStore.size();
    }

    public Object[] getAddRequest(int index) {
        try {
            return addStore.get(index);
        } catch (Exception e) {
            return null;
        }
    }

    private int findAddRequest(String id) {
        Object request;
        for (int i = 0; i < addStore.size(); i++) {
            request = (addStore.get(i))[ID];
            if (request.equals(id)) {
                return i;
            }
        }
        return -1;
    }

    public void twConnected() {

    }

    public void twDisconnected() {
        symbolReferences.clear();
    }
}
