package com.isi.csvr;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Apr 26, 2006
 * Time: 5:53:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarketTimer implements Runnable, ExchangeListener {

    private static MarketTimer self = null;
    private Hashtable<String, LongTime> timer;
    private Thread timerThread;
    private boolean isActive = true;

    private MarketTimer() {
        timer = new Hashtable<String, LongTime>();
        timerThread = new Thread(this, "Market Timer Thread");
        timerThread.start();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public synchronized static MarketTimer getSharedInstance() {
        if (self == null) {
            self = new MarketTimer();
        }
        return self;
    }

    public void setMarketTime(String exchangeCode, long time) {
        synchronized (this) {
            try {
                LongTime oldtime = timer.get(exchangeCode);
                oldtime.setLongTime(time);
                oldtime = null;
            } catch (Exception e) {
                timer.put(exchangeCode, new LongTime(time));
            }
        }
    }

    public long getMarketTime(String exchangeCode) {
        return (timer.get(exchangeCode)).getTime();
    }

    public void run() {
        isActive = true;
        while (isActive) {
            Enumeration exchanges = timer.keys();
            synchronized (this) {
                while (exchanges.hasMoreElements()) {
                    String exchange = (String) exchanges.nextElement();
                    LongTime oldtime = timer.get(exchange);
                    oldtime.setLongTime(oldtime.getTime() + 1000);
                    oldtime = null;
                    exchange = null;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void close() {
        isActive = false;
        try {
            timerThread.interrupt();
        } catch (Exception e) {
        }
        timerThread = null;
    }

    private void resetAllTimers() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                Exchange exchange = (Exchange) exchanges.nextElement();
                timer.put(exchange.getSymbol(), new LongTime(exchange.getMarketTime()));
                exchange = null;
            } catch (Exception e) {
            }
        }
        exchanges = null;
    }

    public void exchangeAdded(Exchange exchange) {
        try {
            LongTime oldtime = timer.get(exchange.getSymbol());
            oldtime.setLongTime(exchange.getMarketTime());
            oldtime = null;
        } catch (Exception e) {
            timer.put(exchange.getSymbol(), new LongTime(exchange.getMarketTime()));
        }
    }

    public void exchangeRemoved(Exchange exchange) {
        try {
            timer.remove(exchange.getSymbol());
        } catch (Exception e) {
        }
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        System.out.println("exchanges added called in market timer");
        timer.clear();
        resetAllTimers();
    }

    public void exchangesLoaded() {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }


    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    class LongTime {
        private long time;

        public LongTime(long time) {
            this.time = time;
        }

        private void setLongTime(long time) {
            this.time = time;
        }

        private long getTime() {
            return time;
        }
    }
}
