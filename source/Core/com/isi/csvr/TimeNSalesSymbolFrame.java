package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.trade.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 21, 2008
 * Time: 2:25:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class TimeNSalesSymbolFrame extends InternalFrame implements LinkedWindowListener {

    TimeNSalesModel model;
    FilterableTradeStore store;
    byte stockDecimalPlaces = 2;
    private ViewSetting oViewSetting;
    private Table oTimeNsales;
    private String selectedSymbol;
    private byte type;
    private boolean isFromWSP;
    private TradeSummaryPanel summaryPanel;
    private boolean isLinked;

    public TimeNSalesSymbolFrame(ThreadGroup oThreadGroup, String sThreadID, WindowDaemon oDaemon, Table oTable, String sSymbol, byte type, boolean fromWSP, byte decimal, boolean isLinked, String linkgroup) {
        super(oThreadGroup, sThreadID, oDaemon, oTable);
        //    this.oSetting = oSetting;
        this.oTimeNsales = oTable;
        selectedSymbol = sSymbol;
        this.type = type;
        isFromWSP = fromWSP;
        stockDecimalPlaces = decimal;
        this.isLinked = isLinked;
        setLinkedGroupID(linkgroup);
        initUI();
    }

    public void initUI() {

        oTimeNsales.setWindowType(ViewSettingsManager.TIMEnSALES_VIEW2);
        oTimeNsales.setAutoResize(false);
        if (type != Constants.FULL_QUOTE_TYPE) {
            oTimeNsales.getPopup().showSetDefaultMenu();
        }
        oTimeNsales.setThreadID(Constants.ThreadTypes.TIME_N_SALES);
        model = new TimeNSalesModel();
        try {
            store = CompanyTradeStore.getSharedInstance().getCompanyTradesStore(selectedSymbol);
            model.setDataStore(store);
            model.setDecimalCount(SharedMethods.getDecimalPlaces(selectedSymbol));
        } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (type != Constants.FULL_QUOTE_TYPE) {
            if (oViewSetting == null && isFromWSP && isLinked) {
                ViewSetting oSetting = Client.getInstance().getViewSettingsManager().getTimenSalesView(ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + LinkStore.linked + "_" + getLinkedGroupID());
                if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedSymbol)) {
                    oViewSetting = oSetting;
                }
            }
            if (oViewSetting == null) {
                oViewSetting = Client.getInstance().getViewSettingsManager().getTimenSalesView(ViewSettingsManager.TIMEnSALES_VIEW2
                        + "|" + selectedSymbol);
            }
        } else {
            //    oViewSetting = Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2);   //sSelectedSymbol + "*"+ type);
            if (oViewSetting == null && isFromWSP && isLinked) {
                ViewSetting oSetting = Client.getInstance().getViewSettingsManager().getFullQuoteView(ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + LinkStore.linked + "_" + getLinkedGroupID());
                if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedSymbol)) {
                    oViewSetting = oSetting;
                }
            }
            if (oViewSetting == null) {
                oViewSetting = Client.getInstance().getViewSettingsManager().getFullQuoteView(ViewSettingsManager.TIMEnSALES_VIEW2
                        + "|" + selectedSymbol);
            }
        }

        if (oViewSetting == null) {
            oViewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("TIME_AND_SALES");
            oViewSetting = oViewSetting.getObject(); // clone the object
            oViewSetting.setDesktopType(type);
            if (type != Constants.FULL_QUOTE_TYPE) {
                //   oViewSetting.setID(selectedSymbol);
                GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("SYMBOL_TIME_N_SALES_COLS"));
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
                oViewSetting.setID(System.currentTimeMillis() + "");
                oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedSymbol);
                oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                oViewSetting.putProperty(ViewConstants.VC_TICK_MODE, model.getTickMode());
                Client.getInstance().getViewSettingsManager().putTimenSalesView(ViewSettingsManager.TIMEnSALES_VIEW2
                        + "|" + selectedSymbol, oViewSetting);

            } else {
                oViewSetting.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                //  oViewSetting.setID(MultiDialogWindow.TABLE_ID);   //sSelectedSymbol+ "*"+type);
                GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("SYMBOL_TIME_N_SALES_COLS"));
                //todo added by Dilum
                //SharedMethods.applyCustomViewSetting(oViewSetting);
                System.out.println("in time and sales frame");
                //   oViewSetting.setSize(MultiDialogWindow.getSharedInstance().getSize());
                oViewSetting.setID(System.currentTimeMillis() + "");
                oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedSymbol);
                oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                Client.getInstance().getViewSettingsManager().putFullQuoteSubView(ViewSettingsManager.TIMEnSALES_VIEW2
                        + "|" + selectedSymbol, oViewSetting);
            }
            if (type != Constants.FULL_QUOTE_TYPE) {
                //  DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
            }

        } else {
            oViewSetting.setDesktopType(type);
            isFromWSP = true;
        }

        model.setViewSettings(oViewSetting);
        oTimeNsales.setModel(model);
        model.setTable(oTimeNsales);
        model.applyColumnSettings();
        if (type != Constants.FULL_QUOTE_TYPE) {
            summaryPanel = new TradeSummaryPanel(selectedSymbol);
            oViewSetting.setParent(this);
            this.getContentPane().add(summaryPanel, BorderLayout.NORTH);
            store.addTradeListener(summaryPanel);
            store.fireNewPanelAdded();

            Thread summThread = new Thread(summaryPanel, "T&S Summary panel");
            summThread.start();
            summaryPanel.setVisible(SharedMethods.booleanValue(oViewSetting.getProperty(Client.VC_TnS_SUMMARY), true));
            oViewSetting.putProperty(Client.VC_TnS_SUMMARY, summaryPanel.isVisible());

            final TWMenuItem menuItem = new TWMenuItem("");
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Client.getInstance().showTimeAndSalesSummary(menuItem, summaryPanel, ViewSettingsManager.TIMEnSALES_VIEW2
                            + "|" + selectedSymbol);
                }
            });
            if (summaryPanel.isVisible()) {
                menuItem.setText(Language.getString("HIDE_TnS_SUMMARY"));
                menuItem.setIconFile("hidesummary.gif");
            } else {
                menuItem.setText(Language.getString("SHOW_SUMMARY"));
                menuItem.setIconFile("showsummary.gif");
            }
            oTimeNsales.getPopup().setMenuItem(menuItem);


            final TWMenuItem filterMenuItem = new TWMenuItem(Language.getString("FILTER"), "filter.gif");
            filterMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Client.getInstance().filterTimeAndSalesSummary(selectedSymbol);
                }
            });
            oTimeNsales.getPopup().setMenuItem(filterMenuItem);
            //__added for tick calculation options   __________________

            ButtonGroup group = new ButtonGroup();
            TWMenu mainMenu = new TWMenu(Language.getString("TICK_CALCULATION"));
            TWRadioButtonMenuItem mnuLastTrade = new TWRadioButtonMenuItem(Language.getString("LAST_TRADE"));
            group.add(mnuLastTrade);
            mnuLastTrade.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    model.setTickMode(Constants.TICK_MODE_LAST_TRADE);
                    oViewSetting.putProperty(ViewConstants.VC_TICK_MODE, model.getTickMode());
                }
            });
            TWRadioButtonMenuItem mnuPrecClosed = new TWRadioButtonMenuItem(Language.getString("PREVIOUS_CLOSED"));
            group.add(mnuPrecClosed);
            mnuPrecClosed.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    model.setTickMode(Constants.TICK_MODE_PREVIOUS_CLOSED);
                    oViewSetting.putProperty(ViewConstants.VC_TICK_MODE, model.getTickMode());
                }
            });
            mainMenu.add(mnuLastTrade);
            mainMenu.add(mnuPrecClosed);
            if (isFromWSP) {
                try {
                    int savedTickMode = SharedMethods.intValue(oViewSetting.getProperty(ViewConstants.VC_TICK_MODE));
                    model.setTickMode(savedTickMode);
                    if (savedTickMode == 0) {
                        mnuLastTrade.setSelected(true);
                    } else if (savedTickMode == 1) {
                        mnuPrecClosed.setSelected(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                mnuLastTrade.setSelected(true);   //calculate the tick based on last trade (default calculation)
            }
            oTimeNsales.getPopup().setMenu(mainMenu);


            //___________________________________________
            oTimeNsales.getScrollPane().addMouseListener(new MouseListener() {

                public void mouseClicked(MouseEvent e) {
                    if (SwingUtilities.isRightMouseButton(e)) {
                        SwingUtilities.updateComponentTreeUI(oTimeNsales.getPopup());
                        if (!oTimeNsales.isDetached()) {
                            if (oTimeNsales.isPopupActive())
                                GUISettings.showPopup(oTimeNsales.getPopup(), oTimeNsales.getTable(), e.getX(), e.getY());
                        }
                    }
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                }

                public void mouseExited(MouseEvent e) {
                }
            });
        } else {

            oViewSetting.setParent(this);
        }

        oTimeNsales.getModel().setDecimalCount(stockDecimalPlaces);
//        table.updateGUI();
        oTimeNsales.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);
        this.setShowServicesMenu(true);
        this.setSize(oViewSetting.getSize());
        this.setLocation(oViewSetting.getLocation());
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.setLinkGroupsEnabled(true);

        if (type != Constants.FULL_QUOTE_TYPE) {
            Client.getInstance().getDesktop().add(this);
            try {
                this.setTitle(Language.getString("TIME_AND_SALES") + " : " +
                        SharedMethods.getDisplayKey(selectedSymbol) + " - " +
                        DataStore.getSharedInstance().getStockObject(selectedSymbol).getLongDescription());
            } catch (Exception e) {
                this.setTitle(Language.getString("TIME_AND_SALES") + " : " +
                        SharedMethods.getDisplayKey(selectedSymbol));
            }
        }

        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        this.getContentPane().add(oTimeNsales);
        oTimeNsales.updateGUI();
        this.applySettings();
        GUISettings.applyOrientation(this);
        GUISettings.applyOrientation(oTimeNsales.getPopup());
        this.show();
        model.adjustRows();
        this.setLayer(GUISettings.TOP_LAYER);
        this.setOrientation();

        if (type != Constants.FULL_QUOTE_TYPE) {
            if (isFromWSP && isLinked) {
                Client.getInstance().getViewSettingsManager().setWindow(ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + LinkStore.linked + "_" + getLinkedGroupID(), this);
                LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
            } else {
                Client.getInstance().getViewSettingsManager().setWindow(ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + selectedSymbol, this);
            }
        }

        if (type != Constants.FULL_QUOTE_TYPE) {
            String symbol = SharedMethods.getSymbolFromKey(selectedSymbol);
            try {
                Stock stock = DataStore.getSharedInstance().getStockObject(selectedSymbol);
                if ((stock != null) && (stock.getMarketCenter() != null)) {
                    if (symbol.endsWith("." + stock.getMarketCenter())) {
                        symbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + SharedMethods.getKey(SharedMethods.getExchangeFromKey(selectedSymbol), symbol,
                    SharedMethods.getInstrumentTypeFromKey(selectedSymbol));
            Client.getInstance().addTradeRequest(requestID, selectedSymbol);
//                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol, Meta.QUOTE);
            this.setDataRequestID(TradeStore.getSharedInstance(), selectedSymbol, requestID);
            if (!isFromWSP) {
                //                frame.setLocationRelativeTo(oTopDesktop);
                //                oViewSetting.setLocation(frame.getLocation());
                if (!oViewSetting.isLocationValid()) {
                    this.setLocationRelativeTo(Client.getInstance().getDesktop());
                    oViewSetting.setLocation(this.getLocation());
                } else {
                    this.setLocation(oViewSetting.getLocation());
                }
            }
        }

        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedSymbol))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedSymbol);
        }


    }

    public void windowClosing() {
        summaryPanel.setInactive();
        super.windowClosing();    //To change body of overridden methods use File | Settings | File Templates.

    }

    public void symbolChanged(String sKey) {


        selectedSymbol = sKey;
        String oldSymbol = oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        String oldReqID = getDataRequestID();
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oldSymbol))) {
            DataStore.getSharedInstance().removeSymbolRequest(oldSymbol);
        }

        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedSymbol))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedSymbol);
        }
        RequestManager.getSharedInstance().removeRequest(oldReqID);
        try {
            store = CompanyTradeStore.getSharedInstance().getCompanyTradesStore(selectedSymbol);
            model.setDataStore(store);
            model.setDecimalCount(SharedMethods.getDecimalPlaces(selectedSymbol));
        } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        summaryPanel.changeSymbol(sKey);

        summaryPanel.updateUI();
        oTimeNsales.getTable().updateUI();

        if (type != Constants.FULL_QUOTE_TYPE) {
            String symbol = SharedMethods.getSymbolFromKey(selectedSymbol);
            try {
                Stock stock = DataStore.getSharedInstance().getStockObject(selectedSymbol);
                if ((stock != null) && (stock.getMarketCenter() != null)) {
                    if (symbol.endsWith("." + stock.getMarketCenter())) {
                        symbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + SharedMethods.getKey(SharedMethods.getExchangeFromKey(selectedSymbol), symbol,
                    SharedMethods.getInstrumentTypeFromKey(selectedSymbol));
            Client.getInstance().addTradeRequest(requestID, selectedSymbol);
//                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol, Meta.QUOTE);
            this.setDataRequestID(TradeStore.getSharedInstance(), selectedSymbol, requestID);
        }
        if (type != Constants.FULL_QUOTE_TYPE) {

            try {
                this.setTitle(Language.getString("TIME_AND_SALES") + " : " +
                        SharedMethods.getDisplayKey(selectedSymbol) + " - " +
                        DataStore.getSharedInstance().getStockObject(selectedSymbol).getLongDescription());
            } catch (Exception e) {
                this.setTitle(Language.getString("TIME_AND_SALES") + " : " +
                        SharedMethods.getDisplayKey(selectedSymbol));
            }
        }

        oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedSymbol);
//        try {
//            this.setIcon(false);
//            this.show();
//            this.setSelected(true);
//            this.requestFocusInWindow();
//            this.getTableComponent().getTable().requestFocus();
//        } catch (PropertyVetoException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }

    }

    public void setSelectedSymbol(String selectedSymbol) {
        this.selectedSymbol = selectedSymbol;
    }
}
