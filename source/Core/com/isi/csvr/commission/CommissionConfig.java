package com.isi.csvr.commission;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.shared.ValueFormatter;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 18, 2004
 * Time: 5:22:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommissionConfig extends JDialog implements ActionListener {
    private JTextField txtMin;
    private JTextField txtPct;
    private JCheckBox checkBox;

    public CommissionConfig() throws HeadlessException {
        super(Client.getInstance().getFrame(), Language.getString("EDIT_COMMISSION"), true);
        createUI();
    }

    public static void main(String[] args) {
        CommissionConfig config = new CommissionConfig();
        config.show();
    }

    public void createUI() {

        String[] heights = {"20", "20"};
        String[] widths = {"232", "100"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights, 4, 5);
        JPanel configPanel = new JPanel(layout);

        JLabel lblMin = new JLabel(Language.getString("MINIMUM_COMMISSION"));
        txtMin = new JTextField("");
        txtMin.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        txtMin.setText("" + CommissionSettings.getSharedInstance().getMinCommission());
        txtMin.setHorizontalAlignment(JTextField.RIGHT);
        JLabel lblPct = new JLabel(Language.getString("PCT_COMMISSION"));
        txtPct = new JTextField("");
        txtPct.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        txtPct.setText("" + CommissionSettings.getSharedInstance().getPctCommission());
        txtPct.setHorizontalAlignment(JTextField.RIGHT);

        configPanel.add(lblMin);
        configPanel.add(txtMin);
        configPanel.add(lblPct);
        configPanel.add(txtPct);

        JPanel checkBoxPanel = new JPanel();
//        BoxLayout boxLayout = new BoxLayout(checkBoxPanel, BoxLayout.LINE_AXIS);
        FlexGridLayout fLayout = new FlexGridLayout(new String[]{"300", "100%", "45"}, new String[]{"20"}, 0, 0);
//        checkBoxPanel.setLayout(boxLayout);
        checkBoxPanel.setPreferredSize(new Dimension(345, 20));
        checkBoxPanel.setLayout(fLayout);

        JLabel lblDisable = new JLabel(Language.getString("DISABLE_COMMISSION"));
//        lblDisable.setHorizontalAlignment(SwingConstants.LEADING);
        checkBox = new JCheckBox();
        checkBox.setSelected(!CommissionSettings.getSharedInstance().isAutoCommissionAllowed());
//        checkBoxPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
//        lblDisable.setBorder(BorderFactory.createLineBorder(Color.BLUE));
//        checkBoxPanel.add(Box.createHorizontalStrut(2));
        checkBoxPanel.add(lblDisable);
        checkBoxPanel.add(Box.createHorizontalGlue());
        checkBoxPanel.add(checkBox);

        JPanel buttonBoxPanel = new JPanel();
        BoxLayout buttonLayout = new BoxLayout(buttonBoxPanel, BoxLayout.LINE_AXIS);
        buttonBoxPanel.setLayout(buttonLayout);
        TWButton okButton = new TWButton(Language.getString("OK"));
        okButton.addActionListener(this);
        okButton.setActionCommand("OK");
        TWButton cancelButton = new TWButton(Language.getString("CANCEL"));
        cancelButton.addActionListener(this);
        cancelButton.setActionCommand("CANCEL");
        buttonBoxPanel.add(Box.createHorizontalGlue());
        buttonBoxPanel.add(okButton);
        buttonBoxPanel.add(cancelButton);
        buttonBoxPanel.add(Box.createHorizontalStrut(4));
        buttonBoxPanel.setPreferredSize(new Dimension(345, 30));

        JLabel message = new JLabel(Language.getString("EDIT_COMMISSION_MESSAGE"));
        JPanel messagePanel = new JPanel(new BorderLayout());
        messagePanel.add(message);

        JPanel mainConfigPanle = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "20", "30"}, 0, 0));
        mainConfigPanle.add(configPanel);
        mainConfigPanle.add(checkBoxPanel);
        mainConfigPanle.add(buttonBoxPanel);
        mainConfigPanle.setPreferredSize(new Dimension(350, 125));

        JPanel configPanelHolder = new JPanel(new FlowLayout(FlowLayout.CENTER));
        configPanelHolder.add(mainConfigPanle);


        BoxLayout mainLayout = new BoxLayout(getContentPane(), BoxLayout.Y_AXIS);
        getContentPane().setLayout(mainLayout);
        getContentPane().add(messagePanel);
        getContentPane().add(configPanelHolder);

        GUISettings.applyOrientation(getContentPane());

        setResizable(true);
        pack();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(Client.getInstance().getFrame());
        show();
    }

    private void saveSettings() {
        try {
            CommissionSettings.getSharedInstance().setPctCommission(Double.parseDouble(txtPct.getText()));
        } catch (Exception e) {
            CommissionSettings.getSharedInstance().setPctCommission(0);
        }
        try {
            CommissionSettings.getSharedInstance().setMinCommission(Double.parseDouble(txtMin.getText()));
        } catch (Exception e) {
            CommissionSettings.getSharedInstance().setMinCommission(0);
        }
        CommissionSettings.getSharedInstance().setAutoCommissionAllowed(!checkBox.isSelected());
        CommissionSettings.getSharedInstance().save();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            saveSettings();
        }
        dispose();
    }

}
