package com.isi.csvr.customindex;

import com.isi.csvr.Client;
import com.isi.csvr.DataDisintegrator;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.downloader.TodaysOHLCBacklogDownloadManager;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.ohlc.TodaysOHLCDownloadListener;
import com.isi.csvr.shared.*;
import com.mubasher.formulagen.TWCompiler;

import javax.swing.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 10, 2006
 * Time: 5:12:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomIndexStore extends Thread implements ApplicationListener, TodaysOHLCDownloadListener {

    public static final String CUSTOM_INDEX = "CUSTOM_INDEX";
    private static CustomIndexStore self = null;
    private ArrayList<String> nameList;
    private Hashtable<String, CustomIndexInterface> store;
    private boolean isOHLCBacklogUpdated = false;
    private ArrayList<String> exchanges;
    private Hashtable<String, Hashtable<String, String>> symbolsMap;
    private ArrayList<CustomIndexListener> listeners;
    private boolean isActive = true;
    private boolean isNewCustomIndexCreated = false;
    private String newIndexes = "";

    private CustomIndexStore() {
        super("Custom Index Store Thread");
        store = new Hashtable<String, CustomIndexInterface>();
        listeners = new ArrayList<CustomIndexListener>();
        nameList = new ArrayList<String>();
        symbolsMap = new Hashtable<String, Hashtable<String, String>>();
        exchanges = new ArrayList<String>();
        Application.getInstance().addApplicationListener(this);
        TodaysOHLCBacklogDownloadManager.getSharedInstance().addTodayOHLCDownloadListener(this);
        this.start();
    }

    public static CustomIndexStore getSharedInstance() {
        if (self == null) {
            self = new CustomIndexStore();
        }
        return self;
    }

    public void addCustomIndexListener(CustomIndexListener listener) {
        listeners.add(listener);
    }

    public void removeCustomIndexListener(CustomIndexListener listener) {
        listeners.remove(listener);
    }

    private void fireCustomIndexAdded(String index) {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).customIndexAdded(index);
        }
    }

    private void fireCustomIndexRemoved(String index) {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).customIndexRemoved(index);
        }
    }

    private void fireCustomIndexEditted(String index) {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).customIndexEditted(index);
        }
    }

    public boolean isNewCustomIndexCreated() {
        return isNewCustomIndexCreated;
    }

    public void setNewCustomIndexCreated(boolean isHistoryUpdated) {
        isNewCustomIndexCreated = isHistoryUpdated;
    }

    public String getNewIndexes() {
        return newIndexes;
    }

    public void clearNewIndexes() {
        newIndexes = "";
    }

    public void initializeStore() {
        try {
            isOHLCBacklogUpdated = false;
            Enumeration<Exchange> exch = ExchangeStore.getSharedInstance().getExchanges();
            Exchange excahnge;
            while (exch.hasMoreElements()) {
                excahnge = exch.nextElement();
                if (excahnge.isDefault()) {
                    exchanges.add(excahnge.getSymbol());
                }
                excahnge = null;
            }
            exch = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void addCustomIndex(CustomIndexInterface object) {
        object.setDataStore(DataStore.getSharedInstance());
        store.put(object.getSymbol(), object);
        nameList.add(object.getSymbol());
        Stock stock = new Stock(object.getSymbol(), CUSTOM_INDEX, Meta.INSTRUMENT_INDEX);
        if (object.getDescription() != null && !object.getDescription().equals("")) {
            stock.setLongDescription(object.getDescription());
        } else {
            stock.setLongDescription(object.getSymbol());
        }
//        DataStore.getSharedInstance().addStockofCustomIndex(object.getSymbol(),stock);
        DataStore.getSharedInstance().addStockofCustomIndex(SharedMethods.getExchangeKey(object.getSymbol(), Meta.INSTRUMENT_INDEX), stock);
        fireCustomIndexAdded(object.getSymbol());
        Client.getInstance().customIndexStoreModified();
        newIndexes = newIndexes + "," + object.getSymbol();
        isNewCustomIndexCreated = true;
        if (isOHLCBacklogUpdated) {
            rebuildIntradayValues(object.getSymbol());
        }
//        HistoryDownloadManager.getSharedInstance().createHistoryForNewIndex(SharedMethods.getKey(CUSTOM_INDEX,object.getSymbol()));    //,object);
//        OHLCStore.getInstance().createOHLCForNewIndex(SharedMethods.getKey(CUSTOM_INDEX,object.getSymbol()));
    }

    public boolean isCustomIndex(String index) {
        return nameList.contains(index);
    }

    public int getIndexCount() {
        return store.size();
    }

    public String getIndexSymbol(int col) {
        return nameList.get(col);
    }

    public void removeCustomeIndex(String index) {
        if (store.containsKey(index))
            store.remove(index);
        if (nameList.contains(index))
            nameList.remove(index);
        DataStore.getSharedInstance().removeStockofCustomIndex(SharedMethods.getExchangeKey(index, Meta.INSTRUMENT_INDEX));
        fireCustomIndexRemoved(index);
        Client.getInstance().customIndexStoreModified();
    }

    public Enumeration getColumnNames() {
        return store.keys();
    }

    public Hashtable getStore() {
        return store;
    }

    public CustomIndexInterface getCustomIndexObject(String index) {
        return store.get(index);
    }

    public boolean isValidIndex(String index) {
        Enumeration<String> e = store.keys();
        while (e.hasMoreElements()) {
            if (e.nextElement().equalsIgnoreCase(index)) {
                return false;
            }
        }

        return true;

//        if(store.containsKey(index)){
//            return false;
//        }else{
//            return true;
//        }
    }

    public void editCustomIndex(CustomIndexInterface object) {
        object.setDataStore(DataStore.getSharedInstance());
        try {
            store.put(object.getSymbol(), object);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        Stock stock = DataStore.getSharedInstance().getStockofCustomIndex(object.getSymbol(), Meta.INSTRUMENT_INDEX);
        stock.setLow(0);
        stock.setHigh(0);
        stock.setLastTradeValue(0);
        stock.setVolume(0);
        stock.setTurnover(0);
        stock.setNoOfTrades(0);
        if (object.getDescription() != null && !object.getDescription().equals("")) {
            stock.setLongDescription(object.getDescription());
        } else {
            stock.setLongDescription(object.getSymbol());
        }

        stock = null;
        fireCustomIndexEditted(object.getSymbol());

        Client.getInstance().customIndexStoreModified();
        if (!newIndexes.contains(object.getSymbol())) {
            newIndexes = newIndexes + "," + object.getSymbol();
        }
        isNewCustomIndexCreated = true;
        if (isOHLCBacklogUpdated) {
            rebuildIntradayValues(object.getSymbol());
        }

    }

    public CustomIndexObject createCustomIndexObject(String totalString) {
        if ((totalString == null) || (totalString.trim().equals(""))) return null;

        StringTokenizer fields = new StringTokenizer(totalString, CustomIndexConstants.CI_RECORD_DELIMETER);
        DataDisintegrator pair = new DataDisintegrator();
        pair.setSeperator(CustomIndexConstants.CI_FIELD_DELIMETER);
        String value;
        double price = 0;
        CustomIndexObject object = new CustomIndexObject();
        while (fields.hasMoreTokens()) {
            try {
                pair.setData(fields.nextToken());
                value = pair.getData();

                switch (SharedMethods.intValue(pair.getTag(), 0)) {
                    case CustomIndexConstants.CI_ID:
                        break;
                    case CustomIndexConstants.CI_CAPTIONS:
                        object.setSymbol(UnicodeUtils.getNativeString(value));
                        break;
                    case CustomIndexConstants.CI_DESCRIPTION:
                        object.setDescription(UnicodeUtils.getNativeString(value));
                        break;
                    case CustomIndexConstants.CI_TYPE:
                        object.setType(Integer.parseInt(value));
                        break;
                    case CustomIndexConstants.CI_DATE:
                        object.setDate(Long.parseLong(value));
                        break;
                    case CustomIndexConstants.CI_VALUE:
                        object.setValue(Double.parseDouble(value));
                        break;
                    case CustomIndexConstants.CI_PRICE:
                        object.setPrice(Double.parseDouble(value));
                        price = (Double.parseDouble(value));
                        break;
                    case CustomIndexConstants.CI_EXCHAGE:
                        object.setExchange((value));
                        break;
                    case CustomIndexConstants.CI_SYMBOLS:
                        System.out.println("Custom index symbols = " + value);
                        String[] symbols = value.split("\\" + CustomIndexConstants.CI_DATA_SEPERATOR);
                        for (int j = 0; j < symbols.length; j++) {
                            if (!symbols[j].trim().equals("")) {
                                String[] values = symbols[j].split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                                IndexSymbol symbol = new IndexSymbol(values[0]);
                                try {
                                    symbol.setNoOfShares(Long.parseLong(values[1]));
                                    symbol.setWeight(values[2]);
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                                values = null;
                                object.addIndexSymbol(symbol);
                                symbol = null;
                            }
                        }
                        symbols = null;
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }

        return object;
    }

    public void save() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/customindex.mdf");
            CustomIndexInterface object = null;
            for (int i = 0; i < store.size(); i++) {
//                StringBuffer tempString = new StringBuffer();
                object = (store.get(nameList.get(i)));
//
//                tempString.append(CustomIndexConstants.CI_ID);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(nameList.get(i));
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_CAPTIONS);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(UnicodeUtils.getUnicodeString(object.getSymbol()));
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_DESCRIPTION);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(UnicodeUtils.getUnicodeString(object.getDescription()));
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_EXCHAGE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getExchanges());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_TYPE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getType());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_DATE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getDate());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_VALUE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getValue());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_PRICE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getPrice());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_SYMBOLS);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                String[] symbols =object.getSymbolsArray();
//                for(int j=0;j<symbols.length; j++){
//                    if(!symbols[j].equals("")){
//                        tempString.append(symbols[j]);
//                        tempString.append(CustomIndexConstants.CI_DATA_SEPERATOR);
//                    }
//                }
//                symbols = null;
                out.write(createSavingString(object, nameList.get(i)).getBytes());
                out.write("\r\n".getBytes());
//                tempString = null;
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
        exitThread();
    }

    public String createSavingString(CustomIndexInterface object, String name) {
        StringBuffer tempString = new StringBuffer();
        tempString.append(CustomIndexConstants.CI_ID);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        tempString.append(name);
        tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);

        tempString.append(CustomIndexConstants.CI_CAPTIONS);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        tempString.append(UnicodeUtils.getUnicodeString(object.getSymbol()));
        tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);

        tempString.append(CustomIndexConstants.CI_DESCRIPTION);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        tempString.append(UnicodeUtils.getUnicodeString(object.getDescription()));
        tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);

        tempString.append(CustomIndexConstants.CI_EXCHAGE);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        tempString.append(object.getExchanges());
        tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);

        tempString.append(CustomIndexConstants.CI_TYPE);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        tempString.append(object.getType());
        tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);

        tempString.append(CustomIndexConstants.CI_DATE);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        tempString.append(object.getDate());
        tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);

        tempString.append(CustomIndexConstants.CI_VALUE);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        tempString.append(object.getValue());
        tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);

        tempString.append(CustomIndexConstants.CI_PRICE);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        tempString.append(object.getPrice());
        tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);

        tempString.append(CustomIndexConstants.CI_SYMBOLS);
        tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
        String[] symbols = object.getSymbolsArray();
        for (int j = 0; j < symbols.length; j++) {
            if (!symbols[j].equals("")) {
                tempString.append(symbols[j]);
                tempString.append(CustomIndexConstants.CI_DATA_SEPERATOR);
            }
        }
        symbols = null;
        return tempString.toString();
    }

    public void createFile(String totalString, byte objecttype) {
        CustomIndexObject object = createCustomIndexObject(totalString);
        if (object != null) {
            if (objecttype == CustomIndexConstants.NEW_INDEX) {
                createFile(object, CustomIndexConstants.NEW_INDEX, null, false, object.getPrice());
            } else {
                createFile(object, objecttype, null, false, object.getPrice());
            }
        } else {
            System.out.println("Object returned null. Cannot Create object from given string");
        }
    }

    public void load() {
        String data;
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(Settings.getAbsolutepath() + "datastore/customindex.mdf"));
            while (true) {
                data = in.readLine();
                if ((data == null) || (data.trim().equals(""))) break;
                StringTokenizer fields = new StringTokenizer(data, CustomIndexConstants.CI_RECORD_DELIMETER);
                DataDisintegrator pair = new DataDisintegrator();
                pair.setSeperator(CustomIndexConstants.CI_FIELD_DELIMETER);
                String value;
                double price = 0D;
                CustomIndexObject object = new CustomIndexObject();
                while (fields.hasMoreTokens()) {
                    try {
                        pair.setData(fields.nextToken());
                        value = pair.getData();

                        switch (SharedMethods.intValue(pair.getTag(), 0)) {
                            case CustomIndexConstants.CI_ID:
                                break;
                            case CustomIndexConstants.CI_CAPTIONS:
                                object.setSymbol(UnicodeUtils.getNativeString(value));
                                break;
                            case CustomIndexConstants.CI_DESCRIPTION:
                                object.setDescription(UnicodeUtils.getNativeString(value));
                                break;
                            case CustomIndexConstants.CI_TYPE:
                                object.setType(Integer.parseInt(value));
                                break;
                            case CustomIndexConstants.CI_DATE:
                                object.setDate(Long.parseLong(value));
                                break;
                            case CustomIndexConstants.CI_VALUE:
                                object.setValue(Double.parseDouble(value));
                                break;
                            case CustomIndexConstants.CI_PRICE:
                                object.setPrice(Double.parseDouble(value));
                                price = (Double.parseDouble(value));
                                break;
                            case CustomIndexConstants.CI_EXCHAGE:
                                object.setExchange((value));
                                break;
                            case CustomIndexConstants.CI_SYMBOLS:
                                String[] symbols = value.split("\\" + CustomIndexConstants.CI_DATA_SEPERATOR);
                                for (int j = 0; j < symbols.length; j++) {
                                    if (!symbols[j].trim().equals("")) {
                                        String[] values = symbols[j].split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                                        IndexSymbol symbol = new IndexSymbol(values[0]);
                                        try {
                                            symbol.setNoOfShares(Long.parseLong(values[1]));
                                            symbol.setWeight(values[2]);
                                        } catch (Exception e) {
                                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                        values = null;
                                        object.addIndexSymbol(symbol);
                                        symbol = null;
                                    }
                                }
                                symbols = null;
                                break;

                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
//                addCustomIndex(object);
                createFile(object, CustomIndexConstants.NEW_INDEX, null, false, price);
                fields = null;
                pair = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Hashtable<String, String> getSymbolMap(String index) {
        return symbolsMap.get(index);
    }

    public boolean createFile(CustomIndexObject object, byte objecttype, String oldIndex, boolean calculatePrice, double initialPrice) {
//        CustomIndexWindow.getSharedInstance();
        String name = "Index_" + System.currentTimeMillis();
        File file = new File(Settings.getAbsolutepath() + "formula/" + name + ".java");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        Hashtable<String, String> symbols = new Hashtable<String, String>();
        Hashtable<String, String> exchanges = new Hashtable<String, String>();
        ArrayList<String> ignorexchanges = new ArrayList<String>();
        for (int k = 0; k < object.getIndexSymbols().size(); k++) {
            IndexSymbol sym = (object.getIndexSymbols().get(k));
            if (!sym.getSymbol().equals("*")) {
//                System.out.println("key = "+sym.getKey());
                symbols.put(sym.getKey(), sym.toString());
            } else {
                if (!ignorexchanges.contains(sym.getExchange())) {
                    if (sym.getSector().equals("*")) {
                        ignorexchanges.add(sym.getExchange());
                        Hashtable<String, Stock> symbolsTable = DataStore.getSharedInstance().getHashtable(sym.getExchange());
                        Enumeration enuexchange = symbolsTable.keys();

                        while (enuexchange.hasMoreElements()) {
                            String data = (String) enuexchange.nextElement();
                            String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                            int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                            symbols.put(SharedMethods.getKey(sym.getExchange(), symbol, instrument), sym.toString());
                        }
                        enuexchange = null;
                        symbolsTable = null;
                    } else {
                        String sectors = "";
                        try {
                            if (exchanges.containsKey(sym.getExchange())) {
                                sectors = exchanges.get(sym.getExchange());
                            }
                        } catch (Exception e) {
                        }
                        sectors = sectors + "," + sym.getSector();
//                        symbols.put(sym.getKey(),sym.getKey());
                        exchanges.put(sym.getExchange(), sectors);
                    }
                }
            }
        }
        ignorexchanges.clear();
        ignorexchanges = null;
        Enumeration<String> enuexchanges = exchanges.keys();

        while (enuexchanges.hasMoreElements()) {
            String exchange = enuexchanges.nextElement();
            String sectorlist = exchanges.get(exchange);
            String key = null;
            Hashtable<String, Stock> symbolsTable = DataStore.getSharedInstance().getHashtable(exchange);
            Enumeration<String> enusymbol = symbolsTable.keys();
            while (enusymbol.hasMoreElements()) {
                String data = enusymbol.nextElement();
                String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                if (sectorlist.indexOf(symbolsTable.get(data).getSectorCode()) >= 0) {
                    key = SharedMethods.getKey(exchange, symbol, instrument);
                    symbols.put(key, key);
                    key = null;
                }
                symbol = null;
            }
            enusymbol = null;
            symbolsTable = null;
            sectorlist = null;
            exchange = null;
            key = null;
        }
        try {
            symbolsMap.put(object.getSymbol(), symbols);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        enuexchanges = null;
        exchanges.clear();
        exchanges = null;
        String symbolList = "";
        String dataList = "";
        double initprice = 0;
        Enumeration<String> enuSymbols = symbols.keys();
        while (enuSymbols.hasMoreElements()) {
            String key = enuSymbols.nextElement();
            symbolList = symbolList + "\"" + key + "\"" + ", ";
            if (object.getType() == CustomIndexConstants.TYPE_MARKET_CAP) {
                String data = symbols.get(key);
                String[] values = data.split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                dataList = dataList + "\"" + values[1] + "\"" + ", ";
            } else if (object.getType() == CustomIndexConstants.TYPE_CUSTOM) {
                String data = symbols.get(key);
                String[] values = data.split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                dataList = dataList + "\"" + values[2] + "\"" + ", ";
            }
            if (calculatePrice) {
                StringTokenizer st = new StringTokenizer(object.getDateString(), "/", false);
                String sDay = st.nextToken();
                String sMonth = st.nextToken();
                String sYear = st.nextToken();
                if (sDay.length() == 1)
                    sDay = "0" + sDay;
                if (sMonth.length() == 1)
                    sMonth = "0" + sMonth;
                st = null;
                try {
                    String sDate = sYear.trim() + sMonth.trim() + sDay.trim();
                    if (SharedMethods.isCurrentDay(SharedMethods.getExchangeFromKey(key), sDate)) {
                        if (object.getType() == CustomIndexConstants.TYPE_MARKET_CAP) {
                            String data = symbols.get(key);
                            String[] values = data.split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                            initprice = initprice + (Long.parseLong(values[1]) * DataStore.getSharedInstance().getStockObject(key).getLastTradeValue());
                        } else if (object.getType() == CustomIndexConstants.TYPE_CUSTOM) {
                            String data = symbols.get(key);
                            String[] values = data.split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                            initprice = initprice + (Float.parseFloat(values[2]) * DataStore.getSharedInstance().getStockObject(key).getLastTradeValue());
                        } else {
                            initprice = initprice + DataStore.getSharedInstance().getStockObject(key).getLastTradeValue();
                        }
                    } else {
                        Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(key), SharedMethods.getExchangeFromKey(key), sDate);
                        if ((vecData != null) && (vecData.size() > 0)) {
                            StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                            oTokenizer.nextToken();         // Date
                            oTokenizer.nextToken();         // open
                            oTokenizer.nextToken();         // high
                            oTokenizer.nextToken();         // low
//                            initprice = initprice +  Float.parseFloat(oTokenizer.nextToken());      //close
                            if (object.getType() == CustomIndexConstants.TYPE_MARKET_CAP) {
                                String data = symbols.get(key);
                                String[] values = data.split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                                initprice = initprice + (Long.parseLong(values[1]) * Float.parseFloat(oTokenizer.nextToken()));
                            } else if (object.getType() == CustomIndexConstants.TYPE_CUSTOM) {
                                String data = symbols.get(key);
                                String[] values = data.split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                                initprice = initprice + (Float.parseFloat(values[2]) * Float.parseFloat(oTokenizer.nextToken()));
                            } else {
                                initprice = initprice + Float.parseFloat(oTokenizer.nextToken());
                            }
                            vecData = null;
                        } else {
                            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                            String lastDate = format.format(new Date(Client.getInstance().getMarketTime()));
                            format = null;
                            if (lastDate.equals(sDate)) {
                                Stock stock = DataStore.getSharedInstance().getStockObject(key);
//                                initprice = initprice +  stock.getLastTradeValue();
                                if (object.getType() == CustomIndexConstants.TYPE_MARKET_CAP) {
                                    String data = symbols.get(key);
                                    String[] values = data.split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                                    initprice = initprice + (Long.parseLong(values[1]) * stock.getLastTradeValue());
                                } else if (object.getType() == CustomIndexConstants.TYPE_CUSTOM) {
                                    String data = symbols.get(key);
                                    String[] values = data.split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
                                    initprice = initprice + (Float.parseFloat(values[2]) * stock.getLastTradeValue());
                                } else {
                                    initprice = initprice + stock.getLastTradeValue();
                                }
                            } else {
                                String message = Language.getString("MSG_CUSTOM_INDEX_INVALID_DATE");
                                message = message.replace("[SYMBOL]", key);
//                                message = message.replace("[ALL]", object.getDateString());
                                SharedMethods.showMessage(message, JOptionPane.ERROR_MESSAGE);
                                return false;
                            }
                        }
                    }
                    sDay = null;
                    sMonth = null;
                    sYear = null;
                } catch (Exception ex) {
                    String message = Language.getString("MSG_CUSTOM_INDEX_INVALID_DATE");
                    message = message.replace("[SYMBOL]", key);
//                                message = message.replace("[ALL]", object.getDateString());
                    SharedMethods.showMessage(message, JOptionPane.ERROR_MESSAGE);
//                    SharedMethods.showMessage("Can't find price of this symbol "+key+" for this date "+object.getDateString(),JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
        }
        symbolList = symbolList + "\" \"";
        dataList = dataList + "\" \"";
        if (!calculatePrice) {
            initprice = initialPrice;
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            out.write(("package com.isi.csvr.customindex ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());
            out.write(("public class " + name + " implements CustomIndexInterface {").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("private String[] symbols = {" + symbolList + "};").getBytes());
            out.write(("\n").getBytes());
            out.write(("private String[] dataList = {" + dataList + "};").getBytes());
            out.write(("\n").getBytes());
            out.write(("private double initialvalue = " + object.getValue() + "D;").getBytes());
            out.write(("\n").getBytes());
            out.write(("private double initialprice = " + initprice + "D;").getBytes());
            out.write(("\n").getBytes());
            out.write(("private double previousClosed = 0.00D;").getBytes());
            out.write(("\n").getBytes());
            out.write(("private long volumn = 0;").getBytes());
            out.write(("\n").getBytes());
            out.write(("private long noOfTrades = 0;").getBytes());
            out.write(("\n").getBytes());
            out.write(("private double turnover = 0D;").getBytes());
            out.write(("\n").getBytes());
            out.write(("private com.isi.csvr.shared.FunctionDataStoreInterface datastore = null ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("private int type =" + object.getType() + " ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public double getLastPrice() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" double price = 0D;").getBytes());
            out.write(("\n").getBytes());
            out.write((" volumn = 0;").getBytes());
            out.write(("\n").getBytes());
            out.write((" turnover = 0;").getBytes());
            out.write(("\n").getBytes());
            out.write((" noOfTrades = 0;").getBytes());
            out.write(("\n").getBytes());
            out.write((" previousClosed = 0D;").getBytes());
            out.write(("\n").getBytes());
            out.write((" for(int i=0; i<symbols.length-1; i++) {").getBytes());
            out.write(("\n").getBytes());
            out.write(("com.isi.csvr.shared.FunctionStockInterface stock = datastore.getStockObject(symbols[i]);").getBytes());
            out.write(("\n").getBytes());
            out.write(("if(stock!=null) {").getBytes());
            out.write(("\n").getBytes());
            out.write(("if(stock.getLastTradeValue()<0) {").getBytes());
            out.write(("\n").getBytes());
            if (object.getType() == CustomIndexConstants.TYPE_MARKET_CAP) {
                out.write(("previousClosed = previousClosed + ( Long.parseLong(dataList[i]) * stock.getPreviousClosed());").getBytes());
                out.write(("\n").getBytes());
            } else if (object.getType() == CustomIndexConstants.TYPE_CUSTOM) {
                out.write(("previousClosed = previousClosed + ( Double.parseDouble(dataList[i]) * stock.getPreviousClosed());").getBytes());
                out.write(("\n").getBytes());
            } else {
                out.write(("previousClosed = previousClosed + stock.getPreviousClosed();").getBytes());
                out.write(("\n").getBytes());
            }
//                            out.write(("previousClosed = previousClosed + stock.getPreviousClosed();").getBytes());
//                            out.write(("\n").getBytes());
            out.write(("volumn = volumn + stock.getVolume();").getBytes());
            out.write(("\n").getBytes());
            out.write(("turnover = turnover + stock.getTurnover();").getBytes());
            out.write(("\n").getBytes());
            out.write(("noOfTrades = noOfTrades + stock.getNoOfTrades();").getBytes());
            out.write(("\n").getBytes());
            out.write(("continue ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("} else {").getBytes());
            out.write(("\n").getBytes());
            if (object.getType() == CustomIndexConstants.TYPE_MARKET_CAP) {
                out.write(("price = price + ( Long.parseLong(dataList[i]) * stock.getLastTradeValue());").getBytes());
                out.write(("\n").getBytes());
                out.write(("previousClosed = previousClosed + ( Long.parseLong(dataList[i]) * stock.getPreviousClosed());").getBytes());
                out.write(("\n").getBytes());
            } else if (object.getType() == CustomIndexConstants.TYPE_CUSTOM) {
                out.write(("price = price + ( Double.parseDouble(dataList[i]) * stock.getLastTradeValue());").getBytes());
                out.write(("\n").getBytes());
                out.write(("previousClosed = previousClosed + ( Double.parseDouble(dataList[i]) * stock.getPreviousClosed());").getBytes());
                out.write(("\n").getBytes());
            } else {
                out.write(("price = price + stock.getLastTradeValue();").getBytes());
                out.write(("\n").getBytes());
                out.write(("previousClosed = previousClosed + stock.getPreviousClosed();").getBytes());
                out.write(("\n").getBytes());
            }
//                            out.write(("previousClosed = previousClosed + stock.getPreviousClosed();").getBytes());
//                            out.write(("\n").getBytes());
            out.write(("volumn = volumn + stock.getVolume();").getBytes());
            out.write(("\n").getBytes());
            out.write(("noOfTrades = noOfTrades + stock.getNoOfTrades();").getBytes());
            out.write(("\n").getBytes());
            out.write(("turnover = turnover + stock.getTurnover();").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
//             out.write(("System.out.println(\"----PreviousClosed\"+ previousClosed);").getBytes());
//             out.write(("\n").getBytes());
//             out.write(("System.out.println(\"----Initail Value\"+ initialvalue);").getBytes());
//             out.write(("\n").getBytes());
//             out.write(("System.out.println(\"----Initail Price\"+ initialprice);").getBytes());
            out.write(("\n").getBytes());
            out.write(("return ((price * initialvalue)/initialprice);").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public double getPreviousClosed() {").getBytes());
            out.write(("\n").getBytes());
            out.write(("return ((previousClosed * initialvalue)/initialprice);").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public double getCalculatedValue(double price) {").getBytes());
            out.write(("\n").getBytes());
            out.write(("return ((price * initialvalue)/initialprice);").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public long getVolumn() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return volumn ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public long getNoOfTrades() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return noOfTrades ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public double getTurnover() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return turnover ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public void setDataStore(com.isi.csvr.shared.FunctionDataStoreInterface datastore ) {").getBytes());
            out.write(("\n").getBytes());
            out.write((" this.datastore = datastore ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public String getSymbol() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return \"" + UnicodeUtils.getUnicodeString(object.getSymbol()) + "\" ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public String getDescription() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return \"" + UnicodeUtils.getUnicodeString(object.getDescription()) + "\" ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public String getExchanges() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return \"" + object.getExchanges() + "\" ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public String getDateString() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return \"" + object.getDateString() + "\" ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public long getDate() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return " + object.getDate() + "L;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public double getValue() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return " + object.getValue() + "D;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public double getPrice() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return " + initprice + "D;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public int getType() {").getBytes());
            out.write(("\n").getBytes());
            out.write((" return " + object.getType() + ";").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("public String[] getSymbolsArray() {").getBytes());
            out.write(("\n").getBytes());
            out.write(("return new String[]{" + object.getSymbolsString() + "} ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("\n").getBytes());

            out.write(("\n").getBytes());
            out.write(("}").getBytes());

            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        try {
            // load the compiler
//            Class x = Class.forName("com.mubasher.formulagen.Compiler");
            //Class x = new com.mubasher.formulagen.TWCompiler().getClass();
            // compile the java file
//            Integer result = (Integer)x.getDeclaredMethod("compile", new Class[]{String.class, String.class}).invoke(null, file.getAbsolutePath(), "./formula");
            Integer result = TWCompiler.compile(file.getAbsolutePath(), Settings.getAbsolutepath() + "formula");
            if ((result != Constants.COMPILER_COMPILE_SUCCESS)) {
                return false;
            } else {
                // load class
                CustomIndexInterface functionObj = (CustomIndexInterface) Class.forName("com.isi.csvr.customindex." + name, false, new FunctionLoader()).newInstance();
                if (objecttype == CustomIndexConstants.NEW_INDEX)
                    addCustomIndex(functionObj);
                else
                    editCustomIndex(functionObj);

                //File file2 = new File("./formula/"+name+".java");
                try {
                    file.delete();
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    file.deleteOnExit();
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return false;
        }
    }

    public void run() {
        isActive = true;
        while (isActive) {
            try {
                calculateValues(false);
            } catch (Exception e) {
            }
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void rebuildIntradayValues(String index) {
        if (index == null) {
            if (nameList.size() > 0) {
                for (int i = 0; i < nameList.size(); i++) {
                    createIntradayValues(nameList.get(i));
                }
            }
        } else {
            createIntradayValues(index);
        }
    }

    private void createIntradayValues(String index) {
        boolean isdone;
        boolean iskeeptmp;
        boolean iskeepsymbol;
        Hashtable<String, String> symboltable = symbolsMap.get(index);
        CustomIndexInterface object = (store.get(index));
        String[] indexSymbols = object.getSymbolsArray();
        double firstSymbolClose = 0D;
        double secondSymbolClose = 0D;
        Hashtable<String, String> indexList = new Hashtable<String, String>();
        for (int k = 0; k < indexSymbols.length; k++) {
            String[] values = indexSymbols[k].split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
            IndexSymbol inSymbol = new IndexSymbol(values[0]);
            if (object.getType() == CustomIndexConstants.TYPE_MARKET_CAP) {
                indexList.put(inSymbol.getKey(), values[1]);
            } else if (object.getType() == CustomIndexConstants.TYPE_CUSTOM) {
                indexList.put(inSymbol.getKey(), values[2]);
            } else {
                indexList.put(inSymbol.getKey(), "" + 1);
            }
            inSymbol = null;
        }
        indexSymbols = null;
        object = null;

        Enumeration<String> symbols = symboltable.keys();
        DynamicArray tmpList = new DynamicArray();
        DynamicArray finalList = new DynamicArray();
        while (symbols.hasMoreElements()) {
            String symbol = symbols.nextElement();

            if (symbol != null && !symbol.equals("")) {
                float multiplyFactor = 0;
                try {
                    String data = indexList.get(symbol);
                    multiplyFactor = Float.parseFloat(data);
                    data = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                DynamicArray list = OHLCStore.getInstance().getIntradayOHLCRecords(symbol);
                secondSymbolClose = (float) DataStore.getSharedInstance().getStockObject(symbol).getPreviousClosed() * multiplyFactor;
                if (list.size() > 0) {
                    if (finalList.size() == 0) {
                        IntraDayOHLC OHLCrecord;
                        for (int k = 0; k < list.size(); k++) {
                            OHLCrecord = (IntraDayOHLC) list.get(k);
                            IntraDayOHLC record = new IntraDayOHLC(index, OHLCrecord.getTime(), (OHLCrecord.getOpen() * multiplyFactor),
                                    (OHLCrecord.getHigh() * multiplyFactor), (OHLCrecord.getLow() * multiplyFactor),
                                    (OHLCrecord.getClose() * multiplyFactor), OHLCrecord.getVolume(), 0, 0); // todo intraday vwap
                            finalList.add(record);
                            record = null;
                        }
                        OHLCrecord = null;
//                        finalList.getList().addAll(list.getList());
                    } else {
                        long tmpTime = 0;
                        long symbolTime = 0;
                        int tmpIndex = 0;
                        int symbolIndex = 0;
                        iskeeptmp = false;
                        iskeepsymbol = false;
                        IntraDayOHLC tmpRecord = null;
                        IntraDayOHLC symbolRecord = null;
                        isdone = false;
                        while (!isdone) {
                            if (!iskeeptmp) {
                                try {
                                    tmpRecord = (IntraDayOHLC) finalList.get(tmpIndex);
                                    tmpTime = tmpRecord.getTime();
                                    tmpIndex = tmpIndex + 1;
                                } catch (Exception e) {
                                    tmpTime = 0;
                                }
                            }
                            if (!iskeepsymbol) {
                                try {
                                    symbolRecord = (IntraDayOHLC) list.get(symbolIndex);
                                    symbolTime = symbolRecord.getTime();
                                    symbolIndex = symbolIndex + 1;
                                } catch (Exception e) {
                                    symbolTime = 0;
                                }
                            }
                            if ((tmpTime == 0) && (symbolTime == 0)) {
                                isdone = true;
                            } else if (tmpTime == 0) {
                                IntraDayOHLC record = new IntraDayOHLC(index, symbolRecord.getTime(), (float) ((symbolRecord.getOpen() * multiplyFactor) + firstSymbolClose), (float) ((symbolRecord.getHigh() * multiplyFactor) + firstSymbolClose),
                                        (float) ((symbolRecord.getLow() * multiplyFactor) + firstSymbolClose), (float) ((symbolRecord.getClose() * multiplyFactor) + firstSymbolClose), symbolRecord.getVolume(), 0, 0); // todo intraday vwap
                                tmpList.add(record);
                                secondSymbolClose = symbolRecord.getClose() * multiplyFactor;
                                iskeepsymbol = false;
//                                iskeeptmp = true;
                            } else if (symbolTime == 0) {
                                IntraDayOHLC record = new IntraDayOHLC(index, tmpRecord.getTime(), (float) (tmpRecord.getOpen() + secondSymbolClose), (float) (tmpRecord.getHigh() + secondSymbolClose),
                                        (float) (tmpRecord.getLow() + secondSymbolClose), (float) (tmpRecord.getClose() + secondSymbolClose), tmpRecord.getVolume(), 0, 0); // todo intraday vwap
                                tmpList.add(record);
                                firstSymbolClose = tmpRecord.getClose();
//                                iskeepsymbol = true;
                                iskeeptmp = false;
                            } else if ((tmpTime == symbolTime)) {
                                IntraDayOHLC record = new IntraDayOHLC(index, tmpRecord.getTime(), (float) (tmpRecord.getOpen() + (symbolRecord.getOpen() * multiplyFactor)),
                                        (float) (tmpRecord.getHigh() + (symbolRecord.getHigh() * multiplyFactor)), (float) (tmpRecord.getLow() + (symbolRecord.getLow() * multiplyFactor)),
                                        (float) (tmpRecord.getClose() + (symbolRecord.getClose() * multiplyFactor)), (tmpRecord.getVolume() + symbolRecord.getVolume()), 0, 0); // todo intraday vwap
                                tmpList.add(record);
                                secondSymbolClose = symbolRecord.getClose() * multiplyFactor;
                                firstSymbolClose = tmpRecord.getClose();
                                iskeepsymbol = false;
                                iskeeptmp = false;
                            } else if ((tmpTime < symbolTime)) {
                                IntraDayOHLC record = new IntraDayOHLC(index, tmpRecord.getTime(), (float) (tmpRecord.getOpen() + secondSymbolClose), (float) (tmpRecord.getHigh() + secondSymbolClose),
                                        (float) (tmpRecord.getLow() + secondSymbolClose), (float) (tmpRecord.getClose() + secondSymbolClose), tmpRecord.getVolume(), 0, 0); // todo intraday vwap
                                tmpList.add(record);
                                firstSymbolClose = tmpRecord.getClose();
                                iskeepsymbol = true;
                                iskeeptmp = false;
                            } else if ((tmpTime > symbolTime)) {
                                IntraDayOHLC record = new IntraDayOHLC(index, symbolRecord.getTime(), (float) ((symbolRecord.getOpen() * multiplyFactor) + firstSymbolClose), (float) ((symbolRecord.getHigh() * multiplyFactor) + firstSymbolClose),
                                        (float) ((symbolRecord.getLow() * multiplyFactor) + firstSymbolClose), (float) ((symbolRecord.getClose() * multiplyFactor) + firstSymbolClose), symbolRecord.getVolume(), 0, 0); // todo intraday vwap
                                tmpList.add(record);
                                secondSymbolClose = symbolRecord.getClose() * multiplyFactor;
                                iskeepsymbol = false;
                                iskeeptmp = true;
                            }
                            if (symbolIndex == (list.size() - 1)) {
                                iskeepsymbol = true;
                                symbolTime = 0;
                            }
                            if (tmpIndex == (finalList.size() - 1)) {
                                iskeeptmp = true;
                                tmpTime = 0;
                            }
                        }
                        finalList.clear();
                        finalList.getList().addAll(tmpList.getList());
                        tmpRecord = null;
                        symbolRecord = null;
                    }
                }
                list.clear();
                list = null;
                firstSymbolClose = (double) DataStore.getSharedInstance().getStockObject(symbol).getPreviousClosed() * multiplyFactor;
            }
            symbol = null;
        }
        tmpList.clear();
        tmpList = null;
        calcalateFromOHLC(index, finalList);
        finalList.clear();
        finalList = null;
    }

    private void calcalateFromOHLC(String index, DynamicArray list) {
        double high = 0D;
        double low = 0D;
        double open = 0D;
        Stock stock = DataStore.getSharedInstance().getStockofCustomIndex(index, Meta.INSTRUMENT_INDEX);
        if (list.size() > 0) {
            for (int j = 0; j < list.size(); j++) {
                IntraDayOHLC intraDayOHLC = (IntraDayOHLC) list.get(j);
                if (j == 0) {
                    open = intraDayOHLC.getOpen();
                    high = intraDayOHLC.getHigh();
                    low = intraDayOHLC.getLow();
                }
                if (intraDayOHLC.getHigh() > high) {
                    high = intraDayOHLC.getHigh();
                }
                if ((intraDayOHLC.getLow() > 0) && (intraDayOHLC.getLow() < low)) {
                    low = intraDayOHLC.getLow();
                }
                intraDayOHLC = null;
            }
        }
        open = store.get(index).getCalculatedValue(open);
        stock.setTodaysOpen(open);
        high = store.get(index).getCalculatedValue(high);
        if (stock.getHigh() < high) {
            stock.setHigh(high);
        }
        low = store.get(index).getCalculatedValue(low);
        if (stock.getLow() > low) {
            stock.setLow(low);
        }
        stock = null;
    }

    private void calculateValues(boolean initialize) {

        if (nameList.size() > 0) {
            for (int i = 0; i < nameList.size(); i++) {
                try {
                    Stock stock = DataStore.getSharedInstance().getStockofCustomIndex(nameList.get(i), Meta.INSTRUMENT_INDEX);
                    if ((stock != null)) {
                        if ((!initialize)) {
                            double value = store.get(nameList.get(i)).getLastPrice();
                            stock.setLastTradeValue(value);
                            stock.setPreviousClosed(store.get(nameList.get(i)).getPreviousClosed());
                            if (value > 0) {
                                if ((stock.getHigh() == 0) || (stock.getHigh() < value)) {
                                    stock.setHigh(value);
                                }
                                if ((stock.getLow() == 0) || (stock.getLow() > value)) {
                                    stock.setLow(value);
                                }
                                if (stock.getTodaysOpen() <= 0) {
                                    stock.setTodaysOpen(value);
                                }
                                if (stock.getPreviousClosed() > 0) {
                                    double change = stock.getLastTradeValue() - stock.getPreviousClosed();
                                    double perChange = (change * 100) / stock.getPreviousClosed();
                                    stock.setChange(change);
                                    stock.setPercentChange(perChange);
                                }
                            }
                            stock.setVolume((store.get(nameList.get(i)).getVolumn()));
                            stock.setTurnover((store.get(nameList.get(i)).getTurnover()));
                            stock.setNoOfTrades((int) (store.get(nameList.get(i)).getNoOfTrades()));
                        } else {
                            stock.setLastTradeValue(0);
                            stock.setPreviousClosed(0);
                            stock.setHigh(0);
                            stock.setLow(0);
                            stock.setTodaysOpen(0);
                            stock.setChange(0);
                            stock.setPercentChange(0);
                            stock.setVolume(0);
                            stock.setTurnover(0);
                            stock.setNoOfTrades(0);
                        }
                        stock.setLongDescription(store.get(nameList.get(i)).getDescription());
                    }
                    stock = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }

    public void exitThread() {
        isActive = false;
    }

    public void applicationLoading(int percentage) {
    }

    public void applicationLoaded() {
    }

    public void applicationReadyForTransactions() {
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationExiting() {
        exitThread();
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void workspaceWillLoad() {
    }

    public void workspaceLoaded() {
    }

    public void workspaceWillSave() {
    }

    public void workspaceSaved() {
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void OHLCBacklogDownloadCompleted(String exchange) {
        exchanges.remove(exchange);
        if (exchanges.size() == 0) {
            rebuildIntradayValues(null);
            isOHLCBacklogUpdated = true;
        }
    }

    public void OHLCBacklogDownloadStarted(String exchange) {

    }

}
