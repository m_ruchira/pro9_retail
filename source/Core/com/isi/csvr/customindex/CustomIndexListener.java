package com.isi.csvr.customindex;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 18, 2006
 * Time: 11:31:59 AM
 * To change this template use File | Settings | File Templates.
 */
public interface CustomIndexListener {

    void customIndexAdded(String index);

    void customIndexRemoved(String index);

    void customIndexEditted(String index);
}
