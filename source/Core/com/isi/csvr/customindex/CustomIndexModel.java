package com.isi.csvr.customindex;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SectorStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 11, 2006
 * Time: 9:18:03 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomIndexModel extends CommonTable implements TableModel, CommonTableInterface {

    private int columnCount = 4;
    //    private int rowCount = 5;
    private ArrayList<IndexSymbol> store;
    private String label;

    public CustomIndexModel() {
        label = Language.getString("CLICK_TO_ADD_A_SYMBOL");
    }

    public int getRowCount() {
        if (store == null) {
            return 1;
        } else {
            return store.size() + 1;
        }
    }

    public void setStore(ArrayList<IndexSymbol> store) {
        this.store = store;
    }

    public void setSymbol(String symbol) {
    }

    public void hideLastColumn() {
        columnCount = 2;
    }

    public void showLastColumn() {
        columnCount = 3;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex == (getRowCount() - 1)) {
            switch (columnIndex) {
                case -1:
                    return null;
                case 0:
                    return "";
                case 1:
                    return label;
                default:
                    return "";
            }
        } else {
            IndexSymbol object = store.get(rowIndex);
            switch (columnIndex) {
                case -1:
                    return object;
                case 0:
//                    return object.getExchange();
                    return ExchangeStore.getSharedInstance().getExchange(object.getExchange().trim()).getDisplayExchange(); //Display Exchange

                case 1:
                    if (!object.getSymbol().equals("*"))
                        return object.getSymbol();
                    else {
                        if (!object.getSector().equals("*")) {
                            return SectorStore.getSharedInstance().getSector(object.getExchange(), object.getSector()).getDescription();
                        } else {
                            return Language.getString("ALL");
                        }
                    }
                case 2:
                    return object.getNoOfShares();
                case 3:
                    return object.getWeight();
                default:
                    return "";
            }
//            return "aaa";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        try {
            return getValueAt(0, iCol).getClass();
        } catch (Exception e) {
            return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        if (((row < (getRowCount() - 1)) && (col == 2)) || ((row < (getRowCount() - 1)) && (col == 3))) {
            return true;
        } else {
            return false;
        }
//        if((row==(getRowCount()-1)) && (col == 2)){
//            return true;
//        }else if((row==(getRowCount()-1)) && (col == 3)){
//            return true;
//        }else{
//            return false;
//        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        if (columnIndex == 2) {
            System.out.println("selected shares=" + value);
            try {
                if (true) {
                    if (Long.parseLong((String) value) >= 0) {
                        if (Long.parseLong((String) value) <= 999999999) {
                            IndexSymbol object = store.get(rowIndex);
                            if (object != null) {
                                object.setNoOfShares(Long.parseLong((String) value));
                            }
                            object = null;
                        } else {
                            SharedMethods.showMessage(Language.getString("NUMBER_TOO_LONG"), JOptionPane.ERROR_MESSAGE);
//                             JOptionPane.showMessageDialog(getTable1(),
//                                     Language.getString("NUMBER_TOO_LONG"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    getTable().updateUI();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            getTable().updateUI();
        } else if (columnIndex == 3) {
            try {
                if (!(value).equals("")) {
                    double weight = Double.parseDouble((String) value);
                    if (weight > 0) {
                        if (weight <= 100) {
                            double val = Math.round((weight * 100));
                            IndexSymbol object = store.get(rowIndex);
                            object.setWeight("" + (val / 100));
                            object = null;
                            getTable().updateUI();
                        } else {
                            SharedMethods.showMessage(Language.getString("PERCENTAGE_VALUE_EXCEED"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
//                 if(!(value).equals("")){
//                     if(Double.parseDouble((String)value)<=100){
//                         IndexSymbol object = store.get(rowIndex);
//                         System.out.println("selected weight = "+value);
//                         object.setCustomWeight(Double.parseDouble((String)value));
//                         object = null;
//                         getTable().updateUI();
//                     }
//                 }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }
//    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
//        super.setValueAt(aValue, rowIndex, columnIndex);    //To change body of overridden methods use File | Settings | File Templates.
//    }
}
