package com.isi.csvr.customindex;

import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 10, 2006
 * Time: 5:18:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndexSymbol {

    String id;
    String[] items;
    long noOfShares = 0;
    String weight = "0";

    public IndexSymbol(String id) {
        this.id = id.trim();
        items = id.split("\\_");
    }

    public String getExchange() {
        return id.split("\\_")[0];
    }

    public String getSymbol() {
        return id.split("\\_")[2];
    }

    public String getSector() {
        return id.split("\\_")[1];
    }

    public int getInstrumentType() {
        try {
            return Integer.parseInt(id.split("\\_")[3]);
        } catch (Exception e) {
            return Meta.INSTRUMENT_INDEX;
        }
    }

    public String getKey() {
        if (!getSector().equals("*")) {
            return SharedMethods.getKey(getExchange(), getSector(), getInstrumentType());
        } else
            return SharedMethods.getKey(getExchange(), getSymbol(), getInstrumentType());
    }

    public String toString() {
        return id + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR + noOfShares +
                CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR + weight;
    }

    public long getNoOfShares() {
        return noOfShares;
    }

    public void setNoOfShares(long noOfShares) {
        this.noOfShares = noOfShares;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        if (!weight.equals("")) {
            this.weight = weight;
        }
    }

    public String getId() {
        return id;
    }
}
