package com.isi.csvr.customindex;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.downloader.HistoryDownloadManager;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.io.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 19, 2006
 * Time: 4:20:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomIndexHistoryCreater extends Thread {

    private static CustomIndexHistoryCreater self = null;
    private boolean isActive = true;

    public CustomIndexHistoryCreater() {

    }

    public static CustomIndexHistoryCreater getSharedInstance() {
        if (self == null) {
            self = new CustomIndexHistoryCreater();
        }
        return self;
    }

    public void run() {
        isActive = true;
        File file = new File(Settings.getAbsolutepath() + "history/Custom_Index/");
        if (!file.exists()) {
            file.mkdirs();
        }
        while (isActive) {
            //todo - changed 27/12/2006 - temparary disabled till history creation of Custom indexes
            if (!isActive) {
                if (HistoryDownloadManager.getSharedInstance().isNewHistoryFileAvailable()) {
                    HistoryDownloadManager.getSharedInstance().setNewHistoryFileAvailable(false);
                    //todo - temparary removed
//                createHistoryForAllIndexes();
                } else if (CustomIndexStore.getSharedInstance().isNewCustomIndexCreated()) {
//                File file = new File("history/Custom_Index/");
//                if(!file.exists()){
//                    file.mkdirs();
//                }
                    CustomIndexStore.getSharedInstance().setNewCustomIndexCreated(false);
                    CustomIndexStore.getSharedInstance().clearNewIndexes();
                    //todo - temparary removed
//                createHistoryForNewIndexes();
                }
            }

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void setInactive() {
        isActive = false;
        try {
            this.interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void createHistoryForNewIndexes() {
        String[] indexes = CustomIndexStore.getSharedInstance().getNewIndexes().split(",");
        for (int i = 0; i < indexes.length; i++) {
            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            Exchange exchange;
            while (exchanges.hasMoreElements()) {
                exchange = exchanges.nextElement();
                String path = Settings.getAbsolutepath() + "history/" + exchange + "/";
                if (indexes[i].contains(exchange.getSymbol())) {
                    File file = new File(path);
                    File[] fileList = file.listFiles();
                    for (int j = 0; j < fileList.length; j++) {
//                        System.out.println("index in history = "+indexes[i]);
                        createHistoryForIndex(indexes[i], (fileList[j]).getPath());
                    }
                    fileList = null;
                    file = null;
                }
                path = null;
                exchange = null;
            }
            exchanges = null;
        }
    }

    public void createHistoryForAllIndexes() {
        Enumeration<String> names = CustomIndexStore.getSharedInstance().getColumnNames();
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            CustomIndexInterface object = CustomIndexStore.getSharedInstance().getCustomIndexObject(name);

            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            Exchange exchange;
            while (exchanges.hasMoreElements()) {
                exchange = exchanges.nextElement();
                String path = Settings.getAbsolutepath() + "history/" + exchange + "/";
                if (object.getExchanges().contains(exchange.getSymbol())) {
                    File file = new File(path);
                    File[] fileList = file.listFiles();
                    for (int i = 0; i < fileList.length; i++) {
//                        System.out.println("index in history = "+object.getSymbol());
                        createHistoryForIndex(object.getSymbol(), (fileList[i]).getPath());
                    }
                    fileList = null;
                    file = null;
                }
                path = null;
                exchange = null;
            }
            exchanges = null;
            object = null;
            name = null;
        }
        names = null;
    }

    private void createHistoryForIndex(String indexName, String path) {
        FileInputStream fileIN;
        FileInputStream tempIN = null;
        FileOutputStream fileOut;
        BufferedWriter out;
        BufferedReader in;
        BufferedReader tmpin = null;
        boolean isdone;
        boolean iskeeptmp;
        boolean iskeepsymbol;
        boolean isFirsttmpLine = true;
        boolean isFirstSymbolLine = true;
        String previousKey = "";
        Hashtable<String, String> symbols = CustomIndexStore.getSharedInstance().getSymbolMap((indexName));
        if (symbols != null) {
            Enumeration<String> keyList = symbols.keys();
            int size = symbols.size();
            int count = 0;
            while (keyList.hasMoreElements()) {
                String key = keyList.nextElement();
                String symbol = SharedMethods.getSymbolFromKey(key);
                String sline = null;
                String templine = null;
                File file = null;
                if (count != (size - 1)) {
                    file = new File(Settings.getAbsolutepath() + "history/Custom_Index" + "/temp_" + count + "_.csv");
                } else {
                    file = new File(Settings.getAbsolutepath() + "history/Custom_Index" + "/" + indexName + ".csv");
                }
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                try {
                    fileIN = new FileInputStream(path + "/" + symbol + ".csv");
                    in = new BufferedReader(new InputStreamReader(fileIN));
                    fileOut = new FileOutputStream(file);
                    out = new BufferedWriter(new OutputStreamWriter(fileOut));
                    try {
                        if (count != 0) {
                            tempIN = new FileInputStream(path + "/temp_" + (count - 1) + "_.csv");
                            tmpin = new BufferedReader(new InputStreamReader(tempIN));
                        }
                    } catch (FileNotFoundException e) {
                        tmpin = null;
                        e.printStackTrace();
                    }
                    isdone = false;
                    iskeeptmp = false;
                    iskeepsymbol = false;
                    Date lineDate = null;
                    Date tempdate = null;
                    String tmpdate = "";
                    isFirsttmpLine = true;
                    isFirstSymbolLine = true;
                    float tmpclosedvalue = 0;
                    float fileclosedvalue = 0;
                    float tmpOpen = 0;
                    float tmpHigh = 0;
                    float tmpLow = 0;
                    float tmpClose = 0;
                    float tmpTurnover = 0;
                    float tmpPrevClosed = 0;
                    float tmpChange = 0;
                    float tmpPerChange = 0;
                    long tmpVolume = 0;
                    int tmpTrades = 0;
                    int symbolLinesRead = 0;
                    int tmpLinesRead = 0;
                    while (!isdone) {
                        try {
                            if (!iskeepsymbol) {
                                sline = in.readLine();
                                symbolLinesRead = symbolLinesRead + 1;
                            }
                            if ((count != 0) && (!iskeeptmp) && (tmpin != null)) {
                                templine = tmpin.readLine();
                                tmpLinesRead = tmpLinesRead + 1;
                            }
                            if (sline != null) {
                                lineDate = HistoryFilesManager.getDateHistoryRecord(sline);
                            }
                            if (symbolLinesRead > 1) {
                                isFirstSymbolLine = false;
                            }
                            if (tmpLinesRead > 1) {
                                isFirsttmpLine = false;
                            }
                            if (templine != null) {
                                tempdate = HistoryFilesManager.getDateHistoryRecord(templine);
                                StringTokenizer fields = new StringTokenizer(templine, Meta.FS);
                                tmpdate = fields.nextToken(); //date
                                tmpOpen = (SharedMethods.floatValue(fields.nextToken()));      // open
                                tmpHigh = (SharedMethods.floatValue(fields.nextToken()));            // high
                                tmpLow = (SharedMethods.floatValue(fields.nextToken()));             // low
                                tmpclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                tmpClose = tmpclosedvalue;
                                tmpPerChange = (SharedMethods.floatValue(fields.nextToken()));  // % chg
                                tmpChange = (SharedMethods.floatValue(fields.nextToken()));      // chg
                                tmpPrevClosed = (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                tmpVolume = (SharedMethods.longValue(fields.nextToken()));           // vol
                                tmpTurnover = (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                tmpTrades = (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                            }
                            if (count == 0) {
                                if (lineDate != null) {
                                    out.write(sline);
                                    out.write("\n");
                                } else {
                                    isdone = true;
                                }
                            } else {
                                if (lineDate != null && tempdate != null) {
                                    if (tempdate.compareTo(lineDate) == 0) {
                                        StringTokenizer fields = new StringTokenizer(sline, Meta.FS);
                                        tmpdate = fields.nextToken(); //date
                                        tmpOpen = tmpOpen + (SharedMethods.floatValue(fields.nextToken()));      // open
                                        tmpHigh = tmpHigh + (SharedMethods.floatValue(fields.nextToken()));            // high
                                        tmpLow = tmpLow + (SharedMethods.floatValue(fields.nextToken()));             // low
                                        fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                        tmpClose = tmpClose + fileclosedvalue;
                                        fields.nextToken();   // % chg
                                        fields.nextToken();          // chg
                                        tmpPrevClosed = tmpPrevClosed + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                        tmpVolume = tmpVolume + (SharedMethods.longValue(fields.nextToken()));           // vol
                                        tmpTurnover = tmpTurnover + (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                        tmpTrades = tmpTrades + (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                                        tmpChange = tmpClose - tmpPrevClosed;
                                        tmpPerChange = (tmpChange / tmpPrevClosed);
                                        String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                                + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                                + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                        out.write(str);
                                        out.write("\n");
                                        iskeepsymbol = false;
                                        iskeeptmp = false;
                                    } else if (tempdate.compareTo(lineDate) < 0) {
                                        if (!isFirstSymbolLine) {
                                            tmpOpen = tmpOpen + fileclosedvalue;      // open
                                            tmpHigh = tmpHigh + fileclosedvalue;            // high
                                            tmpLow = tmpLow + fileclosedvalue;             // low
                                            tmpClose = tmpClose + fileclosedvalue;
                                            tmpPrevClosed = tmpPrevClosed + fileclosedvalue;  // prev close
                                        } else {
                                            Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(key), SharedMethods.getExchangeFromKey(key), tmpdate);
                                            if ((vecData != null) && (vecData.size() > 0) && (vecData.elementAt(0) != null)) {
                                                StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                                oTokenizer.nextToken();         // Date
                                                oTokenizer.nextToken();         // open
                                                oTokenizer.nextToken();         // high
                                                oTokenizer.nextToken();         // low
                                                fileclosedvalue = Float.parseFloat(oTokenizer.nextToken());      //close
                                                tmpOpen = tmpOpen + fileclosedvalue;      // open
                                                tmpHigh = tmpHigh + fileclosedvalue;            // high
                                                tmpLow = tmpLow + fileclosedvalue;             // low
                                                tmpClose = tmpClose + fileclosedvalue;
                                                tmpPrevClosed = tmpPrevClosed + fileclosedvalue;  // prev close
                                                vecData = null;
                                            }
                                        }
                                        tmpChange = tmpClose - tmpPrevClosed;
                                        tmpPerChange = (tmpChange / tmpPrevClosed);
                                        String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                                + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                                + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                        out.write(str);
                                        out.write("\n");
                                        iskeepsymbol = true;
                                        iskeeptmp = false;
                                    } else {
                                        StringTokenizer fields = new StringTokenizer(sline, Meta.FS);
                                        tmpdate = fields.nextToken(); //date
                                        if (!isFirsttmpLine) {
                                            tmpOpen = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));      // open
                                            tmpHigh = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));            // high
                                            tmpLow = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));             // low
                                            fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                            tmpClose = tmpclosedvalue + fileclosedvalue;
                                            fields.nextToken();   // % chg
                                            fields.nextToken();          // chg
                                            tmpPrevClosed = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                        } else {
                                            Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(previousKey), SharedMethods.getExchangeFromKey(previousKey), tmpdate);
                                            if ((vecData != null) && (vecData.size() > 0) && (vecData.elementAt(0) != null)) {
                                                StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                                oTokenizer.nextToken();         // Date
                                                oTokenizer.nextToken();         // open
                                                oTokenizer.nextToken();         // high
                                                oTokenizer.nextToken();         // low
                                                tmpclosedvalue = Float.parseFloat(oTokenizer.nextToken());      //close
                                                tmpOpen = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));      // open
                                                tmpHigh = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));            // high
                                                tmpLow = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));             // low
                                                fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                                tmpClose = tmpclosedvalue + fileclosedvalue;
                                                fields.nextToken();   // % chg
                                                fields.nextToken();          // chg
                                                tmpPrevClosed = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                                vecData = null;
                                            } else {
                                                tmpOpen = (SharedMethods.floatValue(fields.nextToken()));      // open
                                                tmpHigh = (SharedMethods.floatValue(fields.nextToken()));            // high
                                                tmpLow = (SharedMethods.floatValue(fields.nextToken()));             // low
                                                fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                                tmpClose = fileclosedvalue;
                                                fields.nextToken();   // % chg
                                                fields.nextToken();          // chg
                                                tmpPrevClosed = (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                            }
                                        }
                                        tmpVolume = (SharedMethods.longValue(fields.nextToken()));           // vol
                                        tmpTurnover = (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                        tmpTrades = (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                                        tmpChange = tmpClose - tmpPrevClosed;
                                        tmpPerChange = (tmpChange / tmpPrevClosed);
                                        String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                                + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                                + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                        out.write(str);
                                        out.write("\n");
                                        iskeeptmp = true;
                                        iskeepsymbol = false;
                                    }
                                } else if (lineDate != null) {
                                    StringTokenizer fields = new StringTokenizer(sline, Meta.FS);
                                    tmpdate = fields.nextToken(); //date
                                    Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(previousKey), SharedMethods.getExchangeFromKey(previousKey), tmpdate);
                                    if ((vecData != null) && (vecData.size() > 0) && (vecData.elementAt(0) != null)) {
                                        StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                        oTokenizer.nextToken();         // Date
                                        oTokenizer.nextToken();         // open
                                        oTokenizer.nextToken();         // high
                                        oTokenizer.nextToken();         // low
                                        tmpclosedvalue = Float.parseFloat(oTokenizer.nextToken());      //close
                                        vecData = null;
                                    } else {
                                        tmpclosedvalue = 0;
                                    }
                                    tmpOpen = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));      // open
                                    tmpHigh = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));            // high
                                    tmpLow = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));             // low
                                    fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                    tmpClose = tmpclosedvalue + fileclosedvalue;
                                    fields.nextToken();   // % chg
                                    fields.nextToken();          // chg
                                    tmpPrevClosed = tmpPrevClosed + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                    tmpVolume = tmpVolume + (SharedMethods.longValue(fields.nextToken()));           // vol
                                    tmpTurnover = tmpTurnover + (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                    tmpTrades = tmpTrades + (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                                    tmpChange = tmpClose - tmpPrevClosed;
                                    tmpPerChange = (tmpChange / tmpPrevClosed);
                                    String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                            + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                            + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                    out.write(str);
                                    out.write("\n");
//                                    out.write(sline);
//                                    out.write("\n");
                                    iskeepsymbol = false;
                                } else if (tempdate != null) {
                                    StringTokenizer fields = new StringTokenizer(templine, Meta.FS);
                                    tmpdate = fields.nextToken(); //date
                                    Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(key), SharedMethods.getExchangeFromKey(key), tmpdate);
                                    if ((vecData != null) && (vecData.size() > 0) && (vecData.elementAt(0) != null)) {
                                        StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                        oTokenizer.nextToken();         // Date
                                        oTokenizer.nextToken();         // open
                                        oTokenizer.nextToken();         // high
                                        oTokenizer.nextToken();         // low
                                        fileclosedvalue = Float.parseFloat(oTokenizer.nextToken());      //close
                                        vecData = null;
                                    } else {
                                        tmpclosedvalue = 0;
                                    }
                                    tmpOpen = fileclosedvalue + (SharedMethods.floatValue(fields.nextToken()));      // open
                                    tmpHigh = fileclosedvalue + (SharedMethods.floatValue(fields.nextToken()));            // high
                                    tmpLow = fileclosedvalue + (SharedMethods.floatValue(fields.nextToken()));             // low
                                    tmpclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                    tmpClose = tmpclosedvalue + fileclosedvalue;
                                    fields.nextToken();   // % chg
                                    fields.nextToken();          // chg
                                    tmpPrevClosed = tmpPrevClosed + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                    tmpVolume = tmpVolume + (SharedMethods.longValue(fields.nextToken()));           // vol
                                    tmpTurnover = tmpTurnover + (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                    tmpTrades = tmpTrades + (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                                    tmpChange = tmpClose - tmpPrevClosed;
                                    tmpPerChange = (tmpChange / tmpPrevClosed);
                                    String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                            + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                            + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                    out.write(str);
                                    out.write("\n");
//                                    out.write(templine);
//                                    out.write("\n");
                                    iskeeptmp = false;
                                } else {
                                    iskeepsymbol = false;
                                    iskeeptmp = false;
                                    isdone = true;
                                }
                                if (!iskeeptmp) {
                                    templine = null;
                                }
                                if (!iskeepsymbol) {
                                    sline = null;
                                }
                            }
                            tempdate = null;
                            lineDate = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                            isdone = true;
                        }
                    }
                    in.close();
                    fileIN.close();
                    in = null;
                    fileIN = null;
                    try {
                        if (count != 0) {
                            tmpin.close();
                            tempIN.close();
                            tmpin = null;
                            tempIN = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    out.flush();
                    out.close();
                    fileOut.close();
                    out = null;
                    fileOut = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    File fileold = new File(path + "/temp_" + (count - 1) + "_.csv");
                    fileold.delete();
                    fileold = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
                symbol = null;
                previousKey = key;
                key = null;
            }
            keyList = null;
        }
    }

}
