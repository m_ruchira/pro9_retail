package com.isi.csvr.customindex;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 11, 2006
 * Time: 12:28:07 AM
 * To change this template use File | Settings | File Templates.
 */
public interface CustomIndexConstants {

    public static final String CI_FIELD_DELIMETER = "=";
    public static final String CI_RECORD_DELIMETER = ";";
    public static final String CI_DATA_SEPERATOR = "$";
    public static final String CI_DATA_SECOND_LEVEL_SEPERATOR = "|";

    public static final int CI_ID = 1;
    public static final int CI_CAPTIONS = 2;
    public static final int CI_DESCRIPTION = 3;
    public static final int CI_TYPE = 4;
    public static final int CI_DATE = 5;
    public static final int CI_VALUE = 6;
    public static final int CI_PRICE = 7;
    public static final int CI_SYMBOLS = 8;
    public static final int CI_EXCHAGE = 9;

    public static final byte TYPE_PRICE = 0;
    public static final byte TYPE_MARKET_CAP = 1;
    public static final byte TYPE_CUSTOM = 2;
    public static final byte NEW_INDEX = 1;
    public static final byte EDIT_INDEX = 2;
}
