package com.isi.csvr.customindex;

import com.isi.csvr.trading.shared.TradingShared;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 10, 2006
 * Time: 5:22:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomIndexObject implements CustomIndexInterface {

    private String symbol;
    private String description;
    private String exchange;
    private int type;
    private long date;
    private String dateString;
    private double value;
    private double price;
    private String[] symbolsArray;
    private ArrayList<IndexSymbol> symbols = new ArrayList<IndexSymbol>();
    //    private ArrayList<String> exchangeList = new ArrayList<String>();
    private boolean isSymbolsModified = false;

    public CustomIndexObject() {

    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public ArrayList<IndexSymbol> getIndexSymbols() {
        return symbols;
    }

    public void setIndexSymbols(ArrayList<IndexSymbol> symbols) {
        this.symbols = symbols;
    }

    public void addIndexSymbol(IndexSymbol symbol) {
        symbols.add(symbol);
        isSymbolsModified = true;
//        if(!exchangeList.contains(symbol.getExchange()) && symbol.getSymbol().equals("*"))
//            exchangeList.add(symbol.getExchange());
    }

    public void removeIndexSymbol(IndexSymbol symbol) {
        symbols.remove(symbol);
        isSymbolsModified = true;
    }

    public String getIndexSymbolID(int index) {
        return symbols.get(index).getId();
    }


    public boolean isSymbolsModified() {
        return isSymbolsModified;
    }

    public void setSymbolsModified(boolean symbolsModified) {
        isSymbolsModified = symbolsModified;
    }

    public String getDateString() {
        if (dateString == null) {
            try {
                dateString = TradingShared.formatGoodTill(date);
            } catch (Exception e) {
                dateString = "";
            }
        }
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String[] getSymbolsArray() {
        if (symbols.size() > 0) {
            String symbolList = "";
            for (int i = 0; i < symbols.size(); i++) {
                symbolList = symbolList + "\"" + (symbols.get(i)).toString() + "\"";
                if (i != (symbols.size() - 1)) {
                    symbolList = symbolList + ", ";
                }
            }
            return new String[]{symbolList};
        } else {
            return symbolsArray;
        }
    }

    public String getSymbolsString() {
        String symbolList = "";
        for (int i = 0; i < symbols.size(); i++) {
            symbolList = symbolList + "\"" + (symbols.get(i)).toString() + "\"";
            if (i != (symbols.size() - 1)) {
                symbolList = symbolList + ", ";
            }
        }
        return symbolList;
    }

    public void setStringsArray(String[] symbolarray) {
        this.symbolsArray = symbolarray;
        for (int i = 0; i < symbolsArray.length; i++) {
            String[] values = symbolsArray[i].split("\\" + CustomIndexConstants.CI_DATA_SECOND_LEVEL_SEPERATOR);
            IndexSymbol symbol = new IndexSymbol(values[0]);
            try {
                symbol.setNoOfShares(Long.parseLong(values[1]));
                symbol.setWeight(values[2]);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            values = null;
            addIndexSymbol(symbol);
//            symbols.add(symbol);
            symbol = null;
        }
    }

    public double getLastPrice() {
        return 0D;
    }

    public void setDataStore(com.isi.csvr.shared.FunctionDataStoreInterface datastore) {

    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getExchanges() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public long getVolumn() {
        return 0;
    }

    public double getTurnover() {
        return 0D;
    }

    public long getNoOfTrades() {
        return 0;
    }

    public double getPreviousClosed() {
        return 0;
    }

    public double getCalculatedValue(double price) {
        return 0D;
    }

}
