package com.isi.csvr.customindex;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenu;
import com.isi.csvr.TWMenuBar;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.profiles.export.DataSelection;
import com.isi.csvr.profiles.imprt.ImportUI;
import com.isi.csvr.profiles.imprt.OpenZipFile;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.table.LongCellEditor;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 10, 2006
 * Time: 12:11:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomIndexWindow extends InternalFrame implements Themeable, ActionListener, DateSelectedListener, MenuListener,
        ItemListener, KeyListener, ExchangeListener {

    private static CustomIndexWindow self = null;
    CustomIndexModel model;
    private TWTextField txtSymbol;
    private TWTextField txtDescription;
    private TWTextField txtValue;
    private ArrayList<TWComboItem> listType;
    private TWComboBox cmbType;
    private ArrayList<TWComboItem> listExchanges;
    private TWComboBox cmbExchange;
    private TWButton btnOk;
    private TWButton btnClose;
    private JPanel cmbGoodTills;
    private JLabel txtGoodTill;
    private TWButton btnShowDays;
    private DatePicker datePicker;
    private Table table;
    private TWMenuBar menuBar;
    private JMenu formulaMenu;
    private JMenu toolsMenu;
    private JMenu helpMenu;
    private TWMenuItem newMenuItem;
    private TWMenu modifyMenu;
    private TWMenu deleteMenu;
    private TWMenuItem importMenuItem;
    private TWMenuItem exportMenuItem;
    private TWMenuItem helpContent;
    private byte type = 0;
    private boolean isDateChanged;
    private boolean isModified = false;
    private CustomIndexObject object;
    private String edittingindex;
    private long goodTillLong;
    private String goodTillString;
    private String selectedExchanges = "";

    private JPopupMenu popupMenu1 = new JPopupMenu();
    private JPopupMenu popupMenu2 = new JPopupMenu();
    private TWMenuItem deleteItem;
    private TWMenuItem deleteAllItems;
    private TWMenuItem symbolItem;
    private TWMenu exchangeItem;

    private CustomIndexWindow() {
        super(false);
        try {
            setTitle(Language.getString("CUSTOM_INDEX_EDITOR"));
            createUI();
            createPopup();
            this.setJMenuBar(menuBar);
            this.addMouseListener(this);
            Theme.registerComponent(this);
            applyTheme();
            setVisible(false);
            ExchangeStore.getSharedInstance().addExchangeListener(this);
            GUISettings.applyOrientation(popupMenu1);
            GUISettings.applyOrientation(popupMenu2);
            GUISettings.applyOrientation(this);
            this.revalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static CustomIndexWindow getSharedInstance() {
        if (self == null) {
            self = new CustomIndexWindow();
        }
        self.clear();
        self.setIndexType(CustomIndexConstants.NEW_INDEX);
        self.object = new CustomIndexObject();
        self.model.setStore(self.object.getIndexSymbols());
        self.isDateChanged = false;
        self.model.fireTableDataChanged();
        return self;
    }

    private void createPopup() {
        deleteItem = new TWMenuItem(Language.getString("REMOVE"));
        deleteItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                int[] index = table.getTable().getSelectedRows();
                for (int j = 0; j < index.length; j++) {
                    object.getIndexSymbols().remove(table.getTable().getSelectedRow());
                }
                table.getTable().setRowSelectionInterval(0, 0);
                enableTypeCombo();

                model.adjustRows();
            }
        });
        popupMenu2.add(deleteItem);
        deleteAllItems = new TWMenuItem(Language.getString("REMOVE_ALL"));
        deleteAllItems.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                object.getIndexSymbols().clear();
                enableTypeCombo();
                selectedExchanges = "";
                model.adjustRows();
            }
        });
        popupMenu2.add(deleteAllItems);

        symbolItem = new TWMenuItem(Language.getString("SYMBOL"));
        symbolItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchSymbol();
            }
        });
        popupMenu1.add(symbolItem);

        exchangeItem = new TWMenu(Language.getString("EXCHANGE"));
        exchangeItem.addMenuListener(this);
        popupMenu1.add(exchangeItem);
    }

    private void searchSymbol() {
        try {
            Symbols symbols = new Symbols();
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
            oCompanies.setTitle(getTitle());
            oCompanies.setSingleMode(false);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true, true);
            oCompanies = null;

            String key;
            String exchange;
            String symbol;
            int instrument = Meta.INSTRUMENT_QUOTE;
            for (int j = 0; j < symbols.getSymbols().length; j++) {
                key = symbols.getSymbols()[j];
                exchange = SharedMethods.getExchangeFromKey(key);
                symbol = SharedMethods.getSymbolFromKey(key);
                instrument = SharedMethods.getInstrumentTypeFromKey(key);
                object.addIndexSymbol(new IndexSymbol(exchange + "_" + "*" + "_" + symbol + "_" + instrument));
                if (!selectedExchanges.contains(exchange)) {
                    selectedExchanges = selectedExchanges + "," + exchange;
                }
            }
            enableTypeCombo();
            table.repaint();
            table.setAutoResize(true);
            model.adjustRows();
            key = null;
            symbol = null;
            exchange = null;
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createMenu() {
        menuBar = new TWMenuBar();
        menuBar.setLayout(new BoxLayout(menuBar, BoxLayout.LINE_AXIS));

        formulaMenu = new JMenu(Language.getString("CUSTOM_INDEX_MENU"));
        formulaMenu.addMenuListener(this);

        newMenuItem = new TWMenuItem(Language.getString("NEW"), "newindex.gif");
        newMenuItem.addActionListener(this);
        formulaMenu.add(newMenuItem);

        modifyMenu = new TWMenu(Language.getString("MODIFY"), "editindex.gif");
        modifyMenu.addMenuListener(this);
        formulaMenu.add(modifyMenu);

        deleteMenu = new TWMenu(Language.getString("DELETE"), "deleteindex.gif");
        deleteMenu.addMenuListener(this);
        formulaMenu.add(deleteMenu);
        formulaMenu.setOpaque(false);
        menuBar.add(formulaMenu);

        toolsMenu = new JMenu(Language.getString("TOOLS"));
        importMenuItem = new TWMenuItem(Language.getString("IMPORT"), "importformula.gif");
        importMenuItem.addActionListener(this);
        toolsMenu.add(importMenuItem);

        exportMenuItem = new TWMenuItem(Language.getString("EXPORT"), "exportformula.gif");
        exportMenuItem.addActionListener(this);
        toolsMenu.add(exportMenuItem);
        toolsMenu.setOpaque(false);
        menuBar.add(toolsMenu);

        helpMenu = new JMenu(Language.getString("HELP"));

        helpContent = new TWMenuItem(Language.getString("HELP_CONTENTS"));

        helpContent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_CUSTOM_INDEX"));
            }
        });
        helpMenu.add(helpContent);
        helpMenu.setOpaque(false);
        menuBar.add(helpMenu);

    }

    private void clear() {
        txtSymbol.setText("");
        txtDescription.setText("");
        txtValue.setText("");
        txtGoodTill.setText("");
    }

    public void showCustomIndexWindow(boolean isVisible) {
        try {
            self.setVisible(isVisible);
            if (self.isVisible()) {
                if (self.isIcon()) {
                    self.setIcon(false);
                }
            }
            self.setSelected(true);

        } catch (PropertyVetoException e) {
        }

    }

    private JPanel getDateCombo() {
        cmbGoodTills = new JPanel(new FlexGridLayout(new String[]{"90%", "10%"}, new String[]{"100%"}, 0, 0));
        cmbGoodTills.setBorder(BorderFactory.createEtchedBorder());
        cmbGoodTills.setPreferredSize(new Dimension(170, 20));
        txtGoodTill = new JLabel();
        txtGoodTill.setPreferredSize(new Dimension(100, 16));
        txtGoodTill.setOpaque(false);
        cmbGoodTills.add(txtGoodTill);
        DownArrow daw = new DownArrow();

        btnShowDays = new TWButton(new DownArrow());
        btnShowDays.setBorder(null);
        btnShowDays.addActionListener(this);
        btnShowDays.setContentAreaFilled(false);
        btnShowDays.setOpaque(false);
        btnShowDays.setPreferredSize(new Dimension(16, 16));
        cmbGoodTills.add(btnShowDays);
        cmbGoodTills.addMouseListener(this);
        //fromDatePanel.addMouseListener(this);
        GUISettings.applyOrientation(cmbGoodTills);
        return cmbGoodTills;
    }

    private void createUI() {
        object = new CustomIndexObject();
        this.setSize(360, 390);
        this.setFont(new TWFont("Arial", Font.PLAIN, 12));
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"75", "5", "25", "100%", "5", "62", "25"}, 8, 5));
        JLabel lblSymbol = new JLabel(Language.getString("SYMBOL"));
        lblSymbol.setAlignmentX(JLabel.LEADING);
        lblSymbol.setFont(this.getFont().deriveFont(Font.BOLD));
        JLabel lblDescription = new JLabel(Language.getString("CUS_INDEX_DESCRIPTION"));
        lblDescription.setAlignmentX(JLabel.LEADING);
        lblDescription.setFont(this.getFont().deriveFont(Font.BOLD));
        JLabel lblExchange = new JLabel(Language.getString("CUS_INDEX_EXCHANGE"));
        lblExchange.setAlignmentX(JLabel.LEADING);
        lblExchange.setFont(this.getFont().deriveFont(Font.BOLD));
        JLabel lblType = new JLabel(Language.getString("CUS_INDEX_TYPE"));
        lblType.setAlignmentX(JLabel.LEADING);
        lblType.setFont(this.getFont().deriveFont(Font.BOLD));
        JLabel lblValue = new JLabel(Language.getString("INITIAL_VALUE"));
        lblValue.setAlignmentX(JLabel.LEADING);
        lblValue.setFont(this.getFont().deriveFont(Font.BOLD));
        JLabel lblDate = new JLabel(Language.getString("STARTING_DATE"));
        lblDate.setAlignmentX(JLabel.LEADING);
        lblDate.setFont(this.getFont().deriveFont(Font.BOLD));
        txtSymbol = new TWTextField();
        txtSymbol.setPreferredSize(new Dimension(100, 20));
        txtDescription = new TWTextField();
        txtDescription.setPreferredSize(new Dimension(100, 20));

        listExchanges = new ArrayList<TWComboItem>();
        cmbExchange = new TWComboBox(new TWComboModel(listExchanges));
        cmbExchange.addItemListener(this);

        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"22", "22"}, 0, 2));
        topPanel.add(lblSymbol);
        topPanel.add(txtSymbol);
        topPanel.add(lblDescription);
        topPanel.add(txtDescription);

        this.add(topPanel);
        this.add(new JSeparator());

        txtValue = new TWTextField();
        txtValue.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        listType = new ArrayList<TWComboItem>();
        cmbType = new TWComboBox(new TWComboModel(listType));
        cmbType.addItemListener(this);

        JPanel typePanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"22"}, 0, 2));
        typePanel.add(lblType);
        typePanel.add(cmbType);
        this.add(typePanel);

        createTable();
        populateTypes();
        populateExchanges();
        this.add(table);
        this.add(new JSeparator());

        JPanel datePanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"22", "22"}, 0, 2));
        datePanel.add(lblDate);
        datePanel.add(getDateCombo());
        datePanel.add(lblValue);
        datePanel.add(txtValue);
        this.add(datePanel);


        btnOk = new TWButton(Language.getString("OK"));
        btnClose = new TWButton(Language.getString("CANCEL"));
        btnOk.addActionListener(this);
        btnClose.addActionListener(this);
        btnOk.setPreferredSize(new Dimension(80, 22));
        btnClose.setPreferredSize(new Dimension(85, 22));

        JPanel btnPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 0, 0));
        JLabel nullLabel = new JLabel("");
        nullLabel.setPreferredSize(new Dimension(5, 10));
        btnPanel.add(btnOk);
        btnPanel.add(nullLabel);
        btnPanel.add(btnClose);
        this.add(btnPanel);

        createMenu();

        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        Client.getInstance().getDesktop().add(this);
        this.setResizable(true);
        this.setClosable(true);
        this.setIconifiable(true);
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        this.setVisible(false);
        GUISettings.applyOrientation(getContentPane());
        this.setLocationRelativeTo(Client.getInstance().getDesktop());

        txtSymbol.addKeyListener(this);
        txtDescription.addKeyListener(this);
        txtValue.addKeyListener(this);
    }

    private void createTable() {
        table = new Table(false);
        model = new CustomIndexModel();
        model.setDirectionLTR(Language.isLTR());
        model.setStore(object.getIndexSymbols());
        model.setViewSettings(ViewSettingsManager.getSummaryView("CUSTOMINDEX"));
        GUISettings.setColumnSettings(model.getViewSettings(), TWColumnSettings.getItem("CUSTOM_INDEX_COLS"));
        table.setModel(model);
        model.setTable(table);
        table.hideCustomizer();
        table.setAutoResize(true);
        table.getTable().setDefaultEditor(Long.class, new LongCellEditor(new TWTextField()));
        table.getTable().setDefaultEditor(Object.class, new DefaultCellEditor(new TWTextField()));
        table.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                Point location = e.getPoint();
                if ((table.getTable().getSelectedRow() == (model.getRowCount() - 1)) && (table.getTable().convertColumnIndexToModel(table.getTable().getSelectedColumn()) == 1)) {
                    GUISettings.showPopup(popupMenu1, table.getTable(), location.x, location.y);
                } else {
                    if (SwingUtilities.isRightMouseButton(e)) {
                        if (table.getTable().getSelectedRow() == (model.getRowCount() - 1)) {
                            try {
                                table.remove(popupMenu2);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            table.add(popupMenu1);
                            GUISettings.showPopup(popupMenu1, table.getTable(), location.x, location.y);
                        } else {
                            try {
                                table.remove(popupMenu1);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            table.add(popupMenu2);
                            GUISettings.showPopup(popupMenu2, table.getTable(), location.x, location.y);
                        }
                    }
                }
            }
        });
    }

    private void populateTypes() {
        listType.add(new TWComboItem(CustomIndexConstants.TYPE_PRICE, Language.getString("PRICE_WEIGHTED")));
        listType.add(new TWComboItem(CustomIndexConstants.TYPE_MARKET_CAP, Language.getString("MARKET_CAP_WEIGHTED")));
        listType.add(new TWComboItem(CustomIndexConstants.TYPE_CUSTOM, Language.getString("CUSTOM_WEIGHTED")));
        cmbType.setSelectedIndex(0);
        model.adjustColumns(table.getTable(), BigInteger.valueOf(3));
    }

    private void populateExchanges() {
        listExchanges.clear();
        StringBuilder allExchanges = new StringBuilder();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if ((exchange.isDefault())) {
                listExchanges.add(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
                allExchanges.append(",");
                allExchanges.append(exchange.getSymbol());
            }
            exchange = null;
        }
        exchanges = null;
        Collections.sort(listExchanges);
        try {
            cmbExchange.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(popupMenu1);
        SwingUtilities.updateComponentTreeUI(popupMenu2);
        SwingUtilities.updateComponentTreeUI(this);
        loadImages(Theme.getID());
    }

    public void loadImages(String sThemeID) {
        helpContent.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/helpcontents.gif"));
        newMenuItem.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/newindex.gif"));
        modifyMenu.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/editindex.gif"));
        deleteMenu.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/deleteindex.gif"));
    }

    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource() == btnShowDays) {
            datePicker = null;
            if (datePicker == null) {
                datePicker = new DatePicker(Client.getInstance().getFrame(), true);
            }
            datePicker.getCalendar().addDateSelectedListener(this);
            Point location = new Point(txtGoodTill.getX(),
                    (txtGoodTill.getY()) + txtGoodTill.getBounds().height);
            SwingUtilities.convertPointToScreen(location, txtGoodTill.getParent());
            datePicker.setLocation(location);
            datePicker.setVisible(true);
        } else if (e.getSource() == newMenuItem) {
            if (!checkModifyStatus()) {
                setIndexType(CustomIndexConstants.NEW_INDEX);
                clear();
                object = new CustomIndexObject();
                model.setStore(object.getIndexSymbols());
                model.fireTableDataChanged();
            }
        } else if (e.getSource() == importMenuItem) {
            OpenZipFile ImSelect = new OpenZipFile();
            ImportUI UI = null;
            UI = ImSelect.getObject();
            if (UI != null) {
                int[] disable = {0, 1, 2};
                UI.disableTabs(disable);
                UI.setSelectedTab(3);
                UI.setVisible(true);
            }
            ImSelect.clearStaticVectors();
            ImSelect = null;
            UI = null;
        } else if (e.getSource() == exportMenuItem) {
            DataSelection ExSelect = new DataSelection();
            int[] disable = {0, 1, 2};
            ExSelect.disableTabs(disable);
            ExSelect.setSelectedTab(3);
            ExSelect.setVisible(true);
            ExSelect = null;
        } else if (e.getSource() == btnOk) {
            if (txtSymbol.getText().trim().equals("")) {
                SharedMethods.showMessage(Language.getString("MSG_EMPTY_CUSTOM_INDEX_NAME"), JOptionPane.ERROR_MESSAGE);
            } else if (txtDescription.getText().trim().equals("")) {
                SharedMethods.showMessage(Language.getString("MSG_EMPTY_CUSTOM_INDEX_DESCRIPTION"), JOptionPane.ERROR_MESSAGE);
            } else {
                if ((!txtSymbol.getText().equals("")) && (!txtDescription.getText().equals(""))) {
                    boolean isSuccess = false;
                    if (createCustomIndexObject()) {
                        if (Byte.parseByte(((TWComboItem) cmbType.getSelectedItem()).getId()) == CustomIndexConstants.TYPE_CUSTOM) {
                            double total = 0;
                            for (int i = 0; i < object.getIndexSymbols().size(); i++) {
                                total = total + Float.parseFloat(object.getIndexSymbols().get(i).getWeight());
                            }
                            if (total != 100) {
                                SharedMethods.showMessage(Language.getString("ERROR_IN_CUTOM_WEIGHT"), JOptionPane.ERROR_MESSAGE);
                                return;
                            } else {
                                isSuccess = CustomIndexStore.getSharedInstance().createFile(object, type, edittingindex, true, 0);
                            }
                        } else {
                            isSuccess = CustomIndexStore.getSharedInstance().createFile(object, type, edittingindex, true, 0);
                        }
                    } else {
                        return;
                    }
                    if (isSuccess) {
                        this.setVisible(false);
                        if (type == CustomIndexConstants.EDIT_INDEX) {
                            SharedMethods.showMessage(Language.getString("MSG_COMPLETE_EDIT_CUSTOM_INDEX"), JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            SharedMethods.showMessage(Language.getString("MSG_COMPLETE_NEW_CUSTOM_INDEX"), JOptionPane.INFORMATION_MESSAGE);
                        }
                        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "CustomIndex-Create");
                    } else {
                        SharedMethods.showMessage(Language.getString("ERROR_IN_CUSTOM_INDEX"), JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        } else if (e.getSource() == btnClose) {
            this.setVisible(false);
        }

    }

    private boolean createCustomIndexObject() {
        if (object == null) {
            object = new CustomIndexObject();
        }
        if (txtSymbol.getText().equals("")) {
            JOptionPane.showMessageDialog(this, Language.getString("MSG_EMPTY_CUSTOM_INDEX_NAME"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            txtSymbol.requestFocus();
            return false;
        } else if (!(object.getIndexSymbols().size() > 0)) {
            JOptionPane.showMessageDialog(this, Language.getString("MSG_EMPTY_CUSTOM_INDEX_SYMBOLS"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!SharedMethods.isValidName(txtSymbol.getText())) {
            JOptionPane.showMessageDialog(this, Language.getString("MSG_INVALID_CHARS"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            txtSymbol.requestFocus();
            return false;
        } else if (txtGoodTill.getText().equals("")) {
            JOptionPane.showMessageDialog(this, Language.getString("MSG_EMPTY_CUSTOM_INDEX_DATE"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            txtGoodTill.requestFocus();
            return false;
        } else if (txtValue.getText().equals("")) {
            JOptionPane.showMessageDialog(this, Language.getString("MSG_EMPTY_CUSTOM_INDEX_VALUE"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            txtValue.requestFocus();
            return false;
        }
        if (type == CustomIndexConstants.EDIT_INDEX && txtSymbol.getText().equals(object.getSymbol())) {
            object.setDate(goodTillLong);
            object.setDescription(txtDescription.getText());
            object.setSymbol(txtSymbol.getText());
            object.setType(Integer.parseInt(((TWComboItem) cmbType.getSelectedItem()).getId()));
            object.setValue(Float.parseFloat(txtValue.getText()));
            object.setDateString(txtGoodTill.getText());
            object.setExchange(selectedExchanges);
            return true;
        } else {
            if (CustomIndexStore.getSharedInstance().isValidIndex(txtSymbol.getText())) {
                object.setDate(goodTillLong);
                object.setDescription(txtDescription.getText());
                object.setSymbol(txtSymbol.getText());
                object.setType(Integer.parseInt(((TWComboItem) cmbType.getSelectedItem()).getId()));
                object.setValue(Float.parseFloat(txtValue.getText()));
                object.setDateString(txtGoodTill.getText());
                object.setExchange(selectedExchanges);
                return true;
            } else {
                JOptionPane.showMessageDialog(this, Language.getString("MSG_DUPLICATE_CUSTOM_INDEX"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                txtSymbol.requestFocus();
                return false;
            }
        }
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        datePicker.hide();
        Calendar cal = Calendar.getInstance();
        cal.set(iYear, iMonth, iDay);
        String dateStr = TradingShared.formatGoodTill(cal.getTime());
        txtGoodTill.setText(dateStr);
        System.out.println("date = " + dateStr);
        if ((goodTillString != null) && !(goodTillString.equals(dateStr))) {
            isDateChanged = true;
        }
        goodTillString = dateStr;
        goodTillLong = cal.getTime().getTime();
        dateStr = null;
        cal = null;
    }

    public void menuSelected(MenuEvent e) {
        if (e.getSource() == modifyMenu) {
            createIndexList(true);
            GUISettings.applyOrientation(modifyMenu);
        } else if (e.getSource() == deleteMenu) {
            createIndexList(false);
            GUISettings.applyOrientation(deleteMenu);
        } else if (e.getSource() == exchangeItem) {
            createExchangeList();
            GUISettings.applyOrientation(exchangeItem);
        } else if (e.getSource() == formulaMenu) {
            int size = CustomIndexStore.getSharedInstance().getIndexCount();
            if (size < 1) {
                modifyMenu.setVisible(false);
                deleteMenu.setVisible(false);
            } else {
                modifyMenu.setVisible(true);
                deleteMenu.setVisible(true);
            }
        }
    }

    private void createExchangeList() {
        exchangeItem.removeAll();

        Enumeration<Exchange> exchnages = ExchangeStore.getSharedInstance().getExchanges();
        while (exchnages.hasMoreElements()) {
            final Exchange exch = exchnages.nextElement();
            if (!exch.isDefault()) continue; // defaut exchanges only

            String market = null;
            if (exch.hasSubMarkets() && exch.isUserSubMarketBreakdown()) {
                market = exch.getDefaultMarket().getMarketID();
            } else {
                market = null;
            }
            final TWMenu item = new TWMenu(exch.getDescription());
            final String defaultMarket = market;
            item.addMenuListener(new MenuListener() {
                public void menuSelected(MenuEvent e) {
                    item.removeAll();

                    TWMenuItem item2 = new TWMenuItem(Language.getString("ALL"));
                    item2.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            if (!selectedExchanges.contains(exch.getSymbol())) {
                                selectedExchanges = selectedExchanges + "," + exch.getSymbol();
                            }
                            Hashtable<String, Stock> symbolsTable = DataStore.getSharedInstance().getHashtable(exch.getSymbol());
                            if (Byte.parseByte(((TWComboItem) cmbType.getSelectedItem()).getId()) == CustomIndexConstants.TYPE_CUSTOM) {
                                Enumeration<String> enusymbol = symbolsTable.keys();
                                String data = null;
                                while (enusymbol.hasMoreElements()) {
                                    data = enusymbol.nextElement();
                                    String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                                    int instruemnt = SharedMethods.getInstrumentFromExchangeKey(data);
                                    if (defaultMarket != null) {
                                        if (symbolsTable.get(data).getMarketID().equals(defaultMarket)) {
                                            object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                        }
                                    } else {
                                        object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                    }
                                    data = null;
                                }
                                enusymbol = null;
                            } else if (Byte.parseByte(((TWComboItem) cmbType.getSelectedItem()).getId()) == CustomIndexConstants.TYPE_MARKET_CAP) {
                                Enumeration<String> enusymbol = symbolsTable.keys();
                                String data = null;
                                while (enusymbol.hasMoreElements()) {
                                    data = enusymbol.nextElement();
                                    String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                                    int instruemnt = SharedMethods.getInstrumentFromExchangeKey(data);
                                    if (defaultMarket != null) {
                                        if (symbolsTable.get(symbol).getMarketID().equals(defaultMarket)) {
                                            object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                        }
                                    } else {
                                        object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                    }
                                    data = null;
                                }
                                enusymbol = null;
                            } else {
                                if (defaultMarket != null) {
                                    Enumeration<String> enusymbol = symbolsTable.keys();
                                    String data = null;
                                    while (enusymbol.hasMoreElements()) {
                                        data = enusymbol.nextElement();
                                        String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                                        int instruemnt = SharedMethods.getInstrumentFromExchangeKey(data);
                                        if (symbolsTable.get(data).getMarketID().equals(defaultMarket)) {
                                            object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                        }
                                        data = null;
                                    }
                                    enusymbol = null;
                                } else {
                                    object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + "*" + "_" + -1));
                                }
                            }
                            symbolsTable = null;
                            enableTypeCombo();
                            table.repaint();
                            table.setAutoResize(true);
                            model.adjustRows();
                        }
                    });
                    item.add(item2);

                    Enumeration<Sector> sectors = SectorStore.getSharedInstance().getSectors(exch.getSymbol());
                    while (sectors.hasMoreElements()) {
                        final Sector sector = sectors.nextElement();
                        TWMenuItem item3 = new TWMenuItem((sector.getDescription()));
                        item3.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                if (!selectedExchanges.contains(exch.getSymbol())) {
                                    selectedExchanges = selectedExchanges + "," + exch.getSymbol();
                                }
                                Hashtable<String, Stock> symbolsTable = DataStore.getSharedInstance().getHashtable(exch.getSymbol());
                                if (Byte.parseByte(((TWComboItem) cmbType.getSelectedItem()).getId()) == CustomIndexConstants.TYPE_CUSTOM) {
//                                    Hashtable<String,Stock> symbolsTable = DataStore.getSharedInstance().getHashtable(exch.getSymbol());
                                    Enumeration<String> enusymbol = symbolsTable.keys();
                                    String data = null;
                                    while (enusymbol.hasMoreElements()) {
                                        data = enusymbol.nextElement();
                                        String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                                        int instruemnt = SharedMethods.getInstrumentFromExchangeKey(data);
                                        if ((symbolsTable.get(data).getSectorCode()).equals(sector.getId())) {
                                            if (defaultMarket != null) {
                                                if (symbolsTable.get(data).getMarketID().equals(defaultMarket)) {
                                                    object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                                }
                                            } else {
                                                object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                            }
                                        }
                                        data = null;
                                    }
                                    enusymbol = null;
                                } else if (Byte.parseByte(((TWComboItem) cmbType.getSelectedItem()).getId()) == CustomIndexConstants.TYPE_MARKET_CAP) {
                                    Enumeration<String> enusymbol = symbolsTable.keys();
                                    String data = null;
                                    while (enusymbol.hasMoreElements()) {
                                        data = enusymbol.nextElement();
                                        String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                                        int instruemnt = SharedMethods.getInstrumentFromExchangeKey(data);
                                        if ((symbolsTable.get(data).getSectorCode()).equals(sector.getId())) {
                                            if (defaultMarket != null) {
                                                if (symbolsTable.get(data).getMarketID().equals(defaultMarket)) {
//                                                if (symbolsTable.get(symbol).getMarketID().equals(defaultMarket)) {
                                                    object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                                }
                                            } else {
                                                object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                            }
                                        }
                                        data = null;
                                    }
                                    enusymbol = null;
                                } else {
                                    if (defaultMarket != null) {
                                        Enumeration<String> enusymbol = symbolsTable.keys();
                                        String data = null;
                                        while (enusymbol.hasMoreElements()) {
                                            data = enusymbol.nextElement();
                                            try {
                                                String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                                                int instruemnt = SharedMethods.getInstrumentFromExchangeKey(data);
                                                if ((symbolsTable.get(data).getSectorCode()).equals(sector.getId())) {
                                                    if (symbolsTable.get(data).getMarketID().equals(defaultMarket)) {
                                                        object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + "*" + "_" + symbol + "_" + instruemnt));
                                                    }
                                                }
                                                data = null;
                                            } catch (Exception e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                        enusymbol = null;
                                    } else {
                                        object.addIndexSymbol(new IndexSymbol(exch.getSymbol() + "_" + sector.getId() + "_" + "*" + "_" + -1));
                                    }
                                }
                                symbolsTable = null;
                                enableTypeCombo();
                                table.repaint();
                                table.setAutoResize(true);
                                model.adjustRows();
                            }
                        });
                        item.add(item3);
                    }
                    GUISettings.applyOrientation(item);
                }

                public void menuDeselected(MenuEvent e) {
                }

                public void menuCanceled(MenuEvent e) {
                }
            });
            GUISettings.applyOrientation(item);
            exchangeItem.add(item);
        }
    }

    public void setIndexType(byte type) {
        this.type = type;
        isDateChanged = false;
        if (type == CustomIndexConstants.EDIT_INDEX) {
            txtSymbol.setEnabled(false);
            cmbType.setEnabled(false);
            cmbExchange.setEnabled(false);
        } else {
            txtSymbol.setEnabled(true);
            cmbType.setEnabled(true);
            cmbExchange.setEnabled(true);
        }
    }

    private void enableTypeCombo() {
        if (object.getIndexSymbols().size() > 0) {
            cmbType.setEnabled(false);
            cmbExchange.setEnabled(false);
        } else {
            cmbType.setEnabled(true);
            cmbExchange.setEnabled(true);
        }
    }

    private void createIndexList(final boolean isModifyMenu) {
        TWMenu menu = null;
        if (isModifyMenu) {
            menu = modifyMenu;
        } else {
            menu = deleteMenu;
        }
        menu.removeAll();
        int size = CustomIndexStore.getSharedInstance().getIndexCount();
        for (int i = 0; i < size; i++) {
            final TWMenuItem item = new TWMenuItem(CustomIndexStore.getSharedInstance().getIndexSymbol(i));
            item.setText(CustomIndexStore.getSharedInstance().getIndexSymbol(i));
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (isModifyMenu) {
                        try {
                            if (object.getSymbol().equals(item.getText())) {
                                return;
                            }
                        } catch (Exception e1) {
                        }
                        if (!checkModifyStatus()) {
                            isModified = false;
                            object.setSymbolsModified(false);
                            setIndexType(CustomIndexConstants.EDIT_INDEX);
                            setEdittingIndex(item.getText());
                        }
                    } else {
                        String str = Language.getString("CUSTOM_INDEX_DELETE_MESSAGE");
                        str = str.replace("[NAME]", item.getText());
                        int option = SharedMethods.showConfirmMessage(str, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                        if (option == JOptionPane.YES_OPTION) {
                            CustomIndexStore.getSharedInstance().removeCustomeIndex(item.getText());
                            txtDescription.setText("");
                            txtSymbol.setText("");
                            setIndexType(CustomIndexConstants.NEW_INDEX);

                        }
                        str = null;
                    }
                }
            });
            menu.add(item);
        }
    }

    private boolean checkModifyStatus() {
        if (type == CustomIndexConstants.NEW_INDEX) {
            boolean name = !(txtSymbol.getText().equals(""));
            boolean symbols = (object.getIndexSymbols().size() > 0);
            if (name && symbols) {
                int status = SharedMethods.showConfirmMessage(Language.getString("CUSTOM_INDEX_SAVE_MODIFIED_MESSAGE"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                if (status == JOptionPane.YES_OPTION) {
                    if (createCustomIndexObject()) {
                        return CustomIndexStore.getSharedInstance().createFile(object, type, null, true, 0);
                    }
                    return true;
                } else {
                    return false;
                }
            } else
                return false;
        } else {
            if ((isModified || object.isSymbolsModified() || (goodTillLong != object.getDate()))) {
                int status = SharedMethods.showConfirmMessage(Language.getString("CUSTOM_INDEX_SAVE_MODIFIED_MESSAGE"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                if (status == JOptionPane.YES_OPTION) {
                    if (createCustomIndexObject()) {
                        return CustomIndexStore.getSharedInstance().createFile(object, type, null, true, 0);
                    }
                    return true;
                } else {
                    return false;
                }
            } else
                return false;
        }
    }

    public void setSelectedExchange(String exchangeID) {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.getSymbol().equals(exchangeID)) {
                cmbExchange.setSelectedItem(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
                cmbExchange.setEnabled(false);
                exchange = null;
                break;
            }
        }
        exchanges = null;
    }

    public void setEdittingIndex(String index) {
        this.edittingindex = index;
        CustomIndexInterface object2 = CustomIndexStore.getSharedInstance().getCustomIndexObject(index);

        txtSymbol.setText(object2.getSymbol());
        txtDescription.setText(object2.getDescription());
        cmbType.setSelectedIndex(object2.getType());
        txtValue.setText("" + object2.getValue());
        txtGoodTill.setText(TradingShared.formatGoodTill(new Date(object2.getDate())));
        goodTillLong = object2.getDate();
        setSelectedExchange(object2.getExchanges());

        object = new CustomIndexObject();
        object.setSymbol(object2.getSymbol());
        object.setDescription(object2.getDescription());
        object.setType(object2.getType());
        object.setValue(object2.getValue());
        object.setDate(object2.getDate());
        object.setStringsArray(object2.getSymbolsArray());
        object.setExchange(object2.getExchanges());
        object.setPrice(object2.getPrice());

        object.setDateString(object2.getDateString());
        object.setSymbolsModified(false);
        model.setStore(object.getIndexSymbols());
        enableTypeCombo();
        table.repaint();
        model.adjustRows();
    }

    public void menuDeselected(MenuEvent e) {
    }

    public void menuCanceled(MenuEvent e) {
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == cmbType) {
            if (Byte.parseByte(((TWComboItem) cmbType.getSelectedItem()).getId()) == CustomIndexConstants.TYPE_MARKET_CAP) {
                model.adjustColumns(table.getTable(), BigInteger.valueOf(7));
            } else if (Byte.parseByte(((TWComboItem) cmbType.getSelectedItem()).getId()) == CustomIndexConstants.TYPE_CUSTOM) {
                model.adjustColumns(table.getTable(), BigInteger.valueOf(11));
            } else {
                model.adjustColumns(table.getTable(), BigInteger.valueOf(3));
            }
        } else if (e.getSource() == cmbExchange) {
        }
    }

    public void keyTyped(KeyEvent e) {
        isModified = true;
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        populateExchanges();
    }

    public void exchangesLoaded() {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }


    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
    }

    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
    }

}
