package com.isi.csvr;

import com.isi.csvr.communication.tcp.TCPConnector;
import com.isi.csvr.event.Application;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class MessageDialog extends JDialog implements WindowListener, Themeable {
    public static final int STATUS_SHOW = 1;
    public static final int STATUS_CLOSED = 2;
    JPanel buttonPanel = null;
    private TWButton btnOK;
    private TWButton btnCancel;
    private TWButton btnconnect;
    private JLabel lblMessage;
    private String firstMessage;
    private String secondMessage;
    private WorkInProgressIndicator workIn;


    public MessageDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        try {
            firstMessage = Language.getString("MSG_DISCONNECTED_DEFAULT");
            jbInit();
            pack();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        Theme.registerComponent(this);
        setResizable(false);
        JPanel panel1 = new JPanel();
        BorderLayout layout = new BorderLayout(0, 0);
        panel1.setLayout(layout);
        panel1.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
        getContentPane().add(panel1);
        lblMessage = new JLabel(" ");
        btnOK = new TWButton(Language.getString("OK"));
        btnOK.setPreferredSize(new Dimension(70, 22));
        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.setPreferredSize(new Dimension(90, 22));
        btnconnect = new TWButton(Language.getString("BTN_RECONNECT"));
        btnconnect.setPreferredSize(new Dimension(100, 22));

        ActionListener oListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                invalidateComponent(true);
            }
        };

        ActionListener oListener2 = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                invalidateComponent(false);
            }
        };

        KeyAdapter oKeyAdapter = new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    invalidateComponent(false);
                }
            }
        };

        ActionListener oConnectionListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().resetConnectionCounter();
            }
        };

        btnOK.addActionListener(oListener2);
        btnOK.addKeyListener(oKeyAdapter);
        btnCancel.addActionListener(oListener);
        btnconnect.addActionListener(oConnectionListener);
        btnconnect.addKeyListener(oKeyAdapter);
        this.addWindowListener(this);

        panel1.add(lblMessage, BorderLayout.CENTER);
        buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(btnOK);
        panel1.add(buttonPanel, BorderLayout.SOUTH);
        pack();
        setDefaultCloseOperation(HIDE_ON_CLOSE);
    }

    public synchronized void setMessage(String mesg) {
        secondMessage = mesg;
        createMessage();
        this.repaint();
    }

    public void setVisible(boolean show) {
        if (show) {
            if (!TCPConnector.isStopped()) {
                super.setVisible(true);
            }
        } else {
            dispose();
        }
    }

    private void createMessage() {
        try {
            StringBuffer sBuff = new StringBuffer();
//            sBuff.append("<html><b><center><font color=red>");
            sBuff.append("<html><div align= center><b><font color=red>");
            sBuff.append(firstMessage);
            sBuff.append("</font><br>");
            if (secondMessage != null) {
//                sBuff.append("<center>");
                sBuff.append(secondMessage);
            }
//            sBuff.append("</b></center>");
            sBuff.append("</b></div>");

            if (secondMessage.equals(Language.getString("NOT_CONNECTED"))) {
                try {
                    buttonPanel.remove(btnOK);
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                try {
                    buttonPanel.remove(btnCancel);
                    buttonPanel.remove(btnconnect);
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                buttonPanel.add(btnconnect);
                buttonPanel.add(btnCancel);
            } else {
                try {
                    buttonPanel.remove(btnOK);
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                try {
                    buttonPanel.remove(btnCancel);
                    buttonPanel.remove(btnconnect);
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                buttonPanel.add(btnOK);
            }
            pack();
            lblMessage.setText(sBuff.toString());
            lblMessage.setHorizontalAlignment(SwingConstants.CENTER);
            sBuff = null;
            pack();
            setLocationRelativeTo(Client.getInstance().getFrame());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*private void centerUI() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }*/

    public synchronized void setStatus(int status) {
        //  prevStatus = status;
    }

    private void invalidateComponent(boolean stopConnection) {
        System.out.println("invalidate in Mesage Dialog");
        secondMessage = null;
        setVisible(false);
        if (stopConnection) {
            Client.getInstance().stopPriceConnection();

            if (!Settings.isConnectedFirstTime && !Settings.isOfflineDataLoaded) {
                workIn = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_OFFLINE_DATA);
                workIn.setVisible(true);
                Settings.setOfflineMode(true);
                new Thread("Offline Data Thread") {
                    public void run() {
                        Application.getInstance().fireLoadOfflineData();
                        if (workIn != null) {
                            workIn.dispose();
                        }
                        Settings.isOfflineDataLoaded = true;
                    }
                }.start();
            }

        }
        //prevStatus = STATUS_CLOSED;
    }

    /**
     * Invoked the first time a window is made visible.
     */
    public void windowOpened(WindowEvent e) {
    }

    /**
     * Invoked when the user attempts to close the window
     * from the window's system menu.  If the program does not
     * explicitly hide or dispose the window while processing
     * this event, the window close operation will be cancelled.
     */
    public void windowClosing(WindowEvent e) {

        //invalidateComponent();
    }

    /**
     * Invoked when a window has been closed as the result
     * of calling dispose on the window.
     */
    public void windowClosed(WindowEvent e) {
    }

    /**
     * Invoked when a window is changed from a normal to a
     * minimized state. For many platforms, a minimized window
     * is displayed as the icon specified in the window's
     * iconImage property.
     *
     * @see java.awt.Frame#setIconImage
     */
    public void windowIconified(WindowEvent e) {
    }

    /**
     * Invoked when a window is changed from a minimized
     * to a normal state.
     */
    public void windowDeiconified(WindowEvent e) {
    }

    /**
     * Invoked when the Window is set to be the active Window. Only a Frame or
     * a Dialog can be the active Window. The native windowing system may
     * denote the active Window or its children with special decorations, such
     * as a highlighted title bar. The active Window is always either the
     * focused Window, or the first Frame or Dialog that is an owner of the
     * focused Window.
     */
    public void windowActivated(WindowEvent e) {
    }

    /**
     * Invoked when a Window is no longer the active Window. Only a Frame or a
     * Dialog can be the active Window. The native windowing system may denote
     * the active Window or its children with special decorations, such as a
     * highlighted title bar. The active Window is always either the focused
     * Window, or the first Frame or Dialog that is an owner of the focused
     * Window.
     */
    public void windowDeactivated(WindowEvent e) {
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(getContentPane());
    }

}