package com.isi.csvr.news;


import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.UnicodeUtils;

import java.text.ParseException;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 12, 2005
 * Time: 10:04:49 AM
 */
public class News {


    public static final char BATE_NEWS_PROVIDER = 'A';
    public static final char BATE_NEWS_SYMBOL = 'B';
    public static final char BATE_NEWS_SECTOR = 'C';
    public static final char BATE_NEWS_ID = 'D';
    public static final char BATE_NEWS_DATE = 'E';
    public static final char BATE_NEWS_LANG = 'F';
    public static final char BATE_NEWS_HDL = 'G';
    public static final char BATE_NEWS_BODY = 'H';
    public static final char BATE_NEWS_KWRDS = 'I';
    public static final char BATE_NEWS_SOURCE = 'J';
    public static final char BATE_NEWS_EXCHANGE = 'K';
    public static final char BATE_NEWS_PRODUCT_ID = 'N';
    public static final char BATE_NEWS_SERVICE_ID = 'O';
    public static final char BATE_NEWS_ACCESSION_NO = 'U';
    public static final char BATE_NEWS_BRANDING = 'j';
    public static final char BATE_NEWS_SECTION_NAME = 'f';
    public static final char BATE_NEWS_CAPTION = 'a';

//     private static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMddHHmm");
    public static final char BATE_NEWS_COMPANY_NAME = 'c';
    public static final char BATE_NEWS_INDUSTRY_CODE = 'd';
    public static final char BATE_NEWS_INDUSTRY_NAME = 'e';
    public static final char BATE_NEWS_TEMPARARY_STATUS = 'R';
    public static final char BATE_NEWS_HOTNEWS_STATUS = 'T';
    public static final char BATE_NEWS_SEQ_NUM = 'P';
    public static final char BATE_NEWS_GROUP_ID = 'Q';
    public static final char BATE_NEWS_COMPANY_CODE = 'b';
    public static final char BATE_NEWS_STATUS = 'g';
    public static final char BATE_INSTRUMENT_TYPE = 'h';
    public static final char BATE_NEWS_CATEGORY = 'o';
    private final String hotNews = "H";
    private final String normalNews = "N";
    private final int command = 1;
    private final int news = 2;
    private String newsProvider = null;
    private String symbol = null;
    private String exchange = null;
    private String newsID = null;
    private long newsDate = 0;
    private String language = null;
    private String headLine = null;
    private String body = null;
    private String keywords = null;
    private String source = null;
    private int instrumentType = 0;// -1;
    private boolean newMessage = false;
    private boolean tempararyStatus = false;
    private boolean hotNewsStatus = false;
    private String seqNo = null;
    private boolean isCommand = false;
    private String category = null;
    private String temparary = "T";
    private String permenant = "P";

    private String[] data = null;

    public News() {
    }

    public News(String symbol) {
        this.symbol = symbol;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) throws Exception {
        if (data == null)
            return;

        this.data = data;
        char tag;
        String newsD = "";
        for (String aData : data) {
            if (aData == null)
                continue;
            if (aData.length() <= 1)
                continue;
            tag = aData.charAt(0);
            switch (tag) {
                case BATE_NEWS_PROVIDER:
                    newsProvider = aData.substring(1);
                    break;
                case BATE_NEWS_SYMBOL:
                    symbol = UnicodeUtils.getNativeString(aData.substring(1));
                    break;
                case BATE_NEWS_EXCHANGE:
                    exchange = UnicodeUtils.getNativeString(aData.substring(1)).trim();
                    break;
                case BATE_NEWS_ID:
                    newsID = aData.substring(1);
                    break;
                case BATE_NEWS_DATE:
                    try {
//                        newsDate = ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, timeFormat.parse(aData.substring(1)).getTime());
                        newsD = aData.substring(1);
                        newsDate = NewsStore.getSharedInstance().getInputTimeFormat().parse(aData.substring(1)).getTime();
                    } catch (ParseException e) {
                        newsDate = 0;
                    }
                    break;
                case BATE_NEWS_LANG:
                    language = aData.substring(1).trim();
                    break;
                case BATE_NEWS_HDL:
                    headLine = UnicodeUtils.getNativeStringFromCompressed(aData.substring(1));
                    break;
                case BATE_NEWS_BODY:
                    setBody(UnicodeUtils.getNativeStringFromCompressed(aData.substring(1)));
                    break;
                case BATE_NEWS_KWRDS:
                    keywords = UnicodeUtils.getNativeStringFromCompressed(aData.substring(1));
                    break;
//                case BATE_NEWS_PRODUCT_ID:
//                    productID = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
//                case BATE_NEWS_SERVICE_ID:
//                    serviceID = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
//                case BATE_NEWS_BRANDING:
//                    branding = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
//                case BATE_NEWS_SECTION_NAME:
//                    sectionName = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
//                case BATE_NEWS_CAPTION:
//                    caption  = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
//                case BATE_NEWS_COMPANY_NAME:
//                    companyName = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
//                case BATE_NEWS_INDUSTRY_CODE:
//                    industryCode = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
//                case BATE_NEWS_INDUSTRY_NAME:
//                    industryName = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
                case BATE_NEWS_SOURCE:
                    source = UnicodeUtils.getNativeStringFromCompressed(aData.substring(1));
                    break;
//                case BATE_NEWS_ACCESSION_NO:
//                    accessionNo = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
                case BATE_NEWS_TEMPARARY_STATUS:
                    tempararyStatus = temparary.equals(aData.substring(1));
                    break;
                case BATE_NEWS_HOTNEWS_STATUS:
                    hotNewsStatus = hotNews.equals(aData.substring(1));
                    break;
//                case BATE_NEWS_GROUP_ID:
//                    groupID = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
//                case BATE_NEWS_COMPANY_CODE:
//                    companyCode = UnicodeUtils.getNativeStringFromCompressedModified(aData.substring(1));
//                    break;
                case BATE_NEWS_SEQ_NUM:
                    seqNo = aData.substring(1);
                    break;
                case BATE_NEWS_STATUS:
                    isCommand = (command == Integer.parseInt(aData.substring(1)));
                    break;
                case BATE_INSTRUMENT_TYPE:
                    instrumentType = Integer.parseInt(aData.substring(1));
                    break;
                case BATE_NEWS_CATEGORY:
                    category = aData.substring(1);
                    break;

            }
        }
//		System.out.println("news heading =="+headLine);
//		System.out.println("news ID, newsProvider and the date=="+ newsID + " , "+ newsProvider + " and "+newsD);
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setAllData(String[] fields) {
        try {
            String heading, keyword;
            exchange = fields[0];
            //symbol = fields[1];
            newsID = fields[2];
            newsDate = 1195051067;
            try {
                newsDate = NewsStore.getSharedInstance().getInputTimeFormat().parse(fields[3]).getTime();
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            language = fields[4];
            heading = UnicodeUtils.getNativeStringFromCompressed(fields[5]);
            if (heading != null) {
                headLine = heading;
            } else {
                headLine = "";
            }
            keyword = UnicodeUtils.getNativeStringFromCompressed(fields[6]);
            if (keyword != null) {
                keywords = keyword;
            } else {
                keywords = "";
            }
            newsProvider = fields[7];
            source = UnicodeUtils.getNativeStringFromCompressed(fields[8]);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getExchange() {
        if (exchange != null && exchange.trim().equals("")) {
            return null;
        }
        return exchange;
    }

    public boolean isNewMessage() {
        return newMessage;
    }

    public void setNewMessage(boolean newMessage) {
        this.newMessage = newMessage;
    }

    public String getNewsProvider() {
        return newsProvider;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getNewsID() {
        return newsID;
    }

    public long getNewsDate() {
        return newsDate;
    }

    public String getCategory() {
        return category;
    }

    public String getDefaultCategory() {
        try {
            String[] data = category.split(",");
            return data[0];
        } catch (Exception e) {
            return newsProvider;
        }
    }

    public boolean isContainInCategory(String provider) {
        try {
            if (category == null) {
                return (newsProvider.equalsIgnoreCase(provider));
            } else {
                String[] data = category.split(",");
                for (int i = 0; i < data.length; i++) {
                    if (data[i].equalsIgnoreCase(provider)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return false;
    }

    public String getLanguage() {
        return language;
    }

    public String getHeadLine() {
        return headLine;
    }

    public void setHeadLine(String headLine) {
        this.headLine = headLine;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = SharedMethods.removeImageTags(body);
    }

    public String getKeywords() {
        return keywords;
    }

    public String getSource() {
        return source;
    }

    public boolean isTempararyNews() {
        return tempararyStatus;
    }

    public boolean isHotNews() {
        return hotNewsStatus;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public boolean isCommand() {
        return isCommand;
    }

    public String getRealProvider() {
        try {
            return getNewsProvider().split("\\.")[0];
        } catch (Exception e) {
            return getNewsProvider();
        }
    }

    public String getShortDescription() {
        String sKey = SharedMethods.getKey(exchange, symbol, instrumentType);
        if (sKey != null && !sKey.isEmpty()) {
            Stock stk = DataStore.getSharedInstance().getStockObject(sKey);
            if (stk != null) {
                return stk.getShortDescription();
            }
        }
        return "";
    }

    public String toString() {
        return "News{" +
                "newsProvider='" + newsProvider + "'" +
                ", newsID='" + newsID + "'" +
                ", Time='" + newsDate + "'" +
                "}"
                ;
    }
}
