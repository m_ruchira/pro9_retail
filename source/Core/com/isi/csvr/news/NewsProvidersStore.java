package com.isi.csvr.news;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.util.Decompress;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 15, 2005
 * Time: 9:49:52 AM
 */
public class NewsProvidersStore {

    public static NewsProvidersStore self = null;
    public static byte DEFAULT_PATH = 2;
    private static Properties prop;
    private Hashtable<String, NewsProvider> allProviders;
    private Hashtable<String, ArrayList<String>> exchangeProviders;
    private Hashtable<String, String> serverProviders;
    private String authProviders;
    private ArrayList<String> globalProviders;

    private NewsProvidersStore() {
        allProviders = new Hashtable<String, NewsProvider>();
        exchangeProviders = new Hashtable<String, ArrayList<String>>();
        serverProviders = new Hashtable<String, String>();
        globalProviders = new ArrayList<String>();
        getSubscribedNewsProviders();
    }

    public static NewsProvidersStore getSharedInstance() {
        if (self == null) {
            self = new NewsProvidersStore();
        }
        return self;
    }

    public void addUserSubscribedProviders(String providers) {
        this.authProviders = providers;
    }

    public void loadProviders() {
        String selectedSrc = "";
        Hashtable<String, DetailedNewsProvider> sSProviders = new Hashtable<String, DetailedNewsProvider>();
        try {
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/newsproviders_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            String[] providers = authProviders.split("\\|");
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out;
            out = decompress.setFiles(sLangSpecFileName);

            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            boolean isSelected = true;
            if ((prop.getProperty("SELECTED_SOURCES") != null) && (!((prop.getProperty("SELECTED_SOURCES")).equals("")))) {
                System.out.println("SELECTED_SOURCES prop is not null==" + prop.getProperty("SELECTED_SOURCES"));
                isSelected = false;
            } else {
                System.out.println("SELECTED_SOURCES property is null");
            }
            System.out.println("isSelected ==" + isSelected);
            do {
                record = in.readLine();
                if (record != null) {
                    try {
                        String[] fields = record.split(Meta.DS);
                        String[] authFields = null;
                        for (String authRecord : providers) {
                            authFields = authRecord.split(",");
                            if (fields[0].equalsIgnoreCase(authFields[0])) {
                                DetailedNewsProvider detailedwsProvider = new DetailedNewsProvider();
                                detailedwsProvider.setProviderID(fields[0]);
                                detailedwsProvider.setGlobal(Integer.parseInt(fields[1]));
                                detailedwsProvider.setDescription(UnicodeUtils.getNativeString(fields[2]));
                                try {
                                    detailedwsProvider.setIp(authFields[1]);
                                } catch (Exception e) {
                                    detailedwsProvider.setIp(Settings.getActiveIP());
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                                try {
                                    detailedwsProvider.setPath(Byte.parseByte(authFields[2]));
                                } catch (Exception e) {
                                    detailedwsProvider.setPath(Constants.PATH_PRIMARY);
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                                detailedwsProvider.setSelected(isSelected);
                                sSProviders.put(fields[0], detailedwsProvider);
                                NewsProvider provider = new NewsProvider(detailedwsProvider.getProviderID());
                                provider.setContentIp(detailedwsProvider.getIp());
                                provider.setDescription(detailedwsProvider.getDescription());
                                provider.setGlobal(detailedwsProvider.isGlobal());
                                provider.setPath(detailedwsProvider.getPath());
                                provider.setSelected(detailedwsProvider.isSelected());
                                allProviders.put(provider.getID(), provider);
//                                serverProviders.put(fields[0], fields[0]);
                                selectedSrc += fields[0] + ",";
                                //TODO - need to uncomment this change after the Ticker Fix
                                /*if (provider.isGlobal()) {
                                    serverProviders.put(fields[0], "");
                                } else {
                                    serverProviders.put(fields[0], fields[0]);
                                }*/
                                //TODO - need to uncomment this change after the Ticker Fix
                                //TODO - This is a temporary fix. Need to comment this change after the Ticker Fix
                                if ((serverProviders.get(fields[0]) == null)) {
                                    if (provider.isGlobal()) {
                                        serverProviders.put(fields[0], "");
                                    } else {
                                        serverProviders.put(fields[0], fields[0]);
                                    }
                                }
                                //TODO - This is a temporary fix. Need to comment this change after the Ticker Fix
                                if (detailedwsProvider.isGlobal()) {
                                    globalProviders.add(fields[0]);
                                }

                                break;
                            }
                        }
                        fields = null;
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            } while (record != null);
            out = null;
            decompress = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if ((prop.getProperty("SELECTED_SOURCES") != null)) {
            String property = prop.getProperty("SELECTED_SOURCES");
            String[] sources = property.split(",");
            for (int i = 0; i < sources.length; i++) {
                try {
                    sSProviders.get(sources[i]).setSelected(true);
                    getProvider(sources[i]).setSelected(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("selected str ==" + selectedSrc);
            System.out.println("all providers count==" + allProviders.size());
            prop.put("SELECTED_SOURCES", selectedSrc);
            saveSettings();
            System.out.println("settings saved");
        }
        loadNewsProviders(sSProviders);
        System.out.println("all news providers loaded==" + allProviders.size());
    }

    public Enumeration<NewsProvider> getAllProviders() {
        return allProviders.elements();
    }

    public Hashtable<String, String> getProviderMap() {
        return serverProviders;
    }

    public void getSubscribedNewsProviders() {
        prop = new Properties();
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "DataStore/news.dll");
            prop.load(oIn);
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getAllProvidersForExchange(String exchange) {
        try {
            if (exchangeProviders.get(exchange) == null) {
                ArrayList<String> list = new ArrayList<String>();
                exchangeProviders.put(exchange, list);
                return list;
            }
            return exchangeProviders.get(exchange);
        } catch (Exception e) {
            ArrayList<String> list = new ArrayList<String>();
            exchangeProviders.put(exchange, list);
            return list;
        }
    }

    public String getContentIp(String providerList) {
        String[] providers = providerList.split(",");
        for (String id : providers) {
            try {
                NewsProvider provider = getProvider(id);
                if (provider.getContentIp() != null && (!provider.getContentIp().trim().isEmpty())
                        && (!provider.getContentIp().equalsIgnoreCase("null"))) {
                    return provider.getContentIp();
                }
            } catch (Exception e) {
            }
        }
        return Settings.getActiveIP();
    }

    public NewsProvider getProvider(String id) {
        return allProviders.get(id);
    }

    public Enumeration<String> getProviderKeys() {
        return allProviders.keys();
    }

    public void clear() {
        allProviders.clear();
        serverProviders.clear();
        authProviders = "";
    }

    private void loadNewsProviders(Hashtable<String, DetailedNewsProvider> sSProviders) {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                Exchange exchange = (Exchange) exchanges.nextElement();
                loadNewsProvidersFile(exchange.getSymbol(), sSProviders);
                exchange = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        sSProviders.clear();
        exchanges = null;
    }

    private void loadNewsProvidersFile(String exchange, Hashtable<String, DetailedNewsProvider> sSProviders) throws Exception {
        try {
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/newsproviders_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);

            Decompress decompress = new Decompress();
            ByteArrayOutputStream out;
            out = decompress.setFiles(sLangSpecFileName);
            ArrayList<String> list = getAllProvidersForExchange(exchange);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            do {
                record = in.readLine();
                if (record != null) {
                    try {
                        String[] fields = record.split(Meta.DS);
                        NewsProvider newsProvider = new NewsProvider(fields[0], UnicodeUtils.getNativeString(fields[2]), fields[1].equals("1"));
                        if (sSProviders.containsKey(newsProvider.getRealID())) {
                            if (!allProviders.containsKey(newsProvider.getID())) {
                                DetailedNewsProvider dNProvider = sSProviders.get(newsProvider.getRealID());
                                newsProvider.setContentIp(dNProvider.getIp());
                                newsProvider.setPath(dNProvider.getPath());
                                newsProvider.setSelected(dNProvider.isSelected());
                                allProviders.put(newsProvider.getID(), newsProvider);
                                String data = serverProviders.get(newsProvider.getRealID());// + ",";
                                if (!data.equals("")) {
                                    data += ",";
                                    data += newsProvider.getID();// + ",";
                                } else {
                                    data = newsProvider.getID();
                                }
//                                data += newsProvider.getID();// + ",";
                                serverProviders.put(newsProvider.getRealID(), data);
                            }
                            list.add(newsProvider.getID());
                        }
                        fields = null;
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            } while (record != null);
            out = null;
            decompress = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int isSubscribedNewsSouceInclude(String list) {
//        int status=0;
        String[] providers = list.split(";");
//		NewsProvider provider;
        for (String id : providers) {
            if (allProviders.containsKey(id)) {
                return 1;
            }
        }
        return 0;
        /*Enumeration<NewsProvider> providerObjects = getAllProviders().elements();
       while (providerObjects.hasMoreElements()) {
           provider = providerObjects.nextElement();
           if(list.contains(id)){
              status=1;
           }
       }
       return status;*/
    }

    public void saveSettings() {
        try {
            synchronized (prop) {
                FileOutputStream oOut = new FileOutputStream(Settings.getAbsolutepath() + "DataStore/news.dll");
                prop.store(oOut, "");
                oOut.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applySettings() {
        if ((prop.getProperty("SELECTED_SOURCES") != null)) {
            String[] sources = prop.getProperty("SELECTED_SOURCES").split(",");
            if (sources.length == 0) {
                Enumeration<NewsProvider> providers = getAllProviders();
                while (providers.hasMoreElements()) {
                    providers.nextElement().setSelected(false);
                }
            } else {
                Hashtable<String, String> temp = new Hashtable<String, String>();
                for (int i = 0; i < sources.length; i++) {
                    try {
                        temp.put(sources[i], "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Enumeration<NewsProvider> providers = getAllProviders();
                NewsProvider provider;
                while (providers.hasMoreElements()) {
                    provider = providers.nextElement();
                    if (temp.containsKey(provider.getRealID())) {
                        provider.setSelected(true);
                    } else {
                        provider.setSelected(false);
                    }
                }
            }

        }
    }

    public void putProperty(String id, String value) {
        prop.put(id, value);
    }

    public String getProperty(String id) {
        return prop.getProperty(id);
    }

    class DetailedNewsProvider {
        private String providerID;
        private String ip = null;
        private byte path;
        private String Description;
        private boolean isGlobal;
        private boolean isSelected = false;

        public DetailedNewsProvider() {
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getProviderID() {
            return providerID;
        }

        public void setProviderID(String providerID) {
            this.providerID = providerID;
        }

        public boolean isGlobal() {
            return isGlobal;
        }

        public void setGlobal(int global) {
            isGlobal = (global == 1);
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public byte getPath() {
            return path;
        }

        public void setPath(byte path) {
            this.path = path;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

    }

}
