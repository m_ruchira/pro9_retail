// Copyright (c) 2000 Home
package com.isi.csvr.news;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.Client;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Market;
import com.isi.csvr.shared.*;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;

public class NewsStore {
    private static final byte FILTER_TYPE_EXCHANGE = 1;
    private static final byte FILTER_TYPE_PROVIDER = 2;
    private static final byte FILTER_TYPE_EXC_AND_PRO = 3;
    private static final byte FILTER_TYPE_NONE = 0;
    public static byte filterType = FILTER_TYPE_NONE;
    private static final byte FILTER_TYPE_SYMBOLS = 4;
    private static final byte FILTER_TYPE_SOURCES = 5;
    //    private final int exchangeMode=0;
//    private final int providerMode=1;
    public static int selectionCriteria = 0;
    public static String selectionExcgId = "";
    public static String selectionProviderId = "";
    private static NewsStore self = null;
    private final SimpleDateFormat inputTimeFormat;
    public String filterEXCriteria = Language.getString("LBL_ALL");
    public String filterPVCriteria = Language.getString("LBL_ALL");
    public int filterMode = 0;
    private NewsList dataStore;
    private NewsList filteredStore;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat bodyDateFormat = null;
    private Date date = null;
    private String template = null;
    private long lastAlrtTime = 0;
    private Hashtable addedSymbols;
    private Hashtable addedSources;

    /**
     * Constructor
     */
    private NewsStore() {
        inputTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat = new SimpleDateFormat("yyyyMMdd");
        bodyDateFormat = new SimpleDateFormat("dd/MM/yyyy '-' HH:mm");//" dd:MM:yyyy '-' HH:mm:ss ");
        date = new Date();
        dataStore = new NewsList();
        filteredStore = new NewsList();
        loadTemplate(null);
    }

    public static NewsStore getSharedInstance() {
        if (self == null) {
            self = new NewsStore();
        }
        return self;
    }

    public synchronized void clear() {
        dataStore.clear();
        filteredStore.clear();
    }

    public SimpleDateFormat getInputTimeFormat() {
        return inputTimeFormat;
    }

    public void setFilter(String filter) {
        dataStore.setFilter(filter);
    }

    public void setFilterType(byte filterType) {
        dataStore.setFilterType(filterType);
    }

    public void applyFilter() {
        dataStore.applyFilter();
    }

    public void addNews(String frame) {
        try {
            News news = new News();
            news.setData(frame.split(Meta.FD));
            addNews(news.getSymbol(), news);

            Date dt = new Date(news.getNewsDate());
            String newsDate = dateFormat.format(dt);
            String currentDate = "";
            Exchange exchange = null;
            Stock stock = null;
            String key = null;

            try {
                exchange = ExchangeStore.getSharedInstance().getExchange(news.getExchange());
                currentDate = dateFormat.format(exchange.getMarketDate());
            } catch (Exception e) {
            }

            try {
                if ((currentDate.equals(newsDate)) && (ExchangeStore.isValidSystemFinformationType(Meta.IT_News))) {
                    if (news.getSymbol() != null) {
                        String[] symbols = news.getSymbol().split(",");
                        for (String symbol : symbols) {
                            if ((exchange != null) && (exchange.hasSubMarkets())) {
                                Market[] markets = exchange.getSubMarkets();
                                for (Market market : markets) {
                                    key = SharedMethods.getKey(news.getExchange(), symbol + Constants.MARKET_SEPERATOR_CHARACTER + market.getSymbol(), news.getInstrumentType());
                                    stock = DataStore.getSharedInstance().getStockObject(key);
                                    if (stock != null) {
                                        stock.setNewsAvailability(Constants.ITEM_NOT_READ);
                                    }
                                }
                            } else {
                                key = SharedMethods.getKey(news.getExchange(), symbol, news.getInstrumentType());
                                stock = DataStore.getSharedInstance().getStockObject(key);
                                if (stock != null) {
                                    stock.setNewsAvailability(Constants.ITEM_NOT_READ);
                                }
                            }
                            stock = null;
                            key = null;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            news = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void addNews(String symbol, News news) {
        if (!news.isCommand()) {
            dataStore.addNews(symbol, news);
            if (isCorrectFiltering(news)) {
                filteredStore.addNews(symbol, news);
            }
        } else {
            dataStore.setNews(symbol, news);
        }
    }

    public News getNews(String id) {
        for (int i = 0; i < getLastNewss().size(); i++) {
            News news = (News) getLastNewss().get(i);
            if ((news.getNewsID() != null) && (news.getNewsID().equals(id)))
                return news;
            news = null;
        }
        return null;
    }

    public void showNewsAlert(String id) {

        News news;
        String sDescr;
        try {
            news = getNews(id);
            sDescr = news.getBody();
            if ((sDescr == null) || ((sDescr.equals("")))) {
                NewsProvider np = NewsProvidersStore.getSharedInstance().getProvider(news.getDefaultCategory());
                Enumeration<String> providers = NewsProvidersStore.getSharedInstance().getProviderKeys();
                while (providers.hasMoreElements()) {
                    String provider = providers.nextElement();
                    if (news.isContainInCategory(provider)) {
                        np = NewsProvidersStore.getSharedInstance().getProvider(provider);
                        break;
                    }
                }
                Client.getInstance().getBrowserFrame().setTitle(Language.getString("NEWS"));
                if (Settings.isConnected()) {
                    SendQFactory.addNewsBodyRequest(news.getExchange(), id, Language.getLanguageTag(), np.getContentIp(), np.getPath()); /*,
                            NewsProvidersStore.getSharedInstance().getProviderPath(news.getNewsProvider()));  */
                }
                news = null;
            } else {
                NewsStore.getSharedInstance().showNews(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            news = null;
            sDescr = null;
        }
    }

    public String generateTempFile(News news) {
        try {
            loadTemplate(news.getLanguage());
            try {
                /*if(ExchangeStore.getSharedInstance().getSelectedExchange() != null){
                    date.setTime(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(
                        ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), news.getNewsDate()));
                } else {
                    date.setTime( news.getNewsDate());
                }*/
                if (ExchangeStore.getSharedInstance().defaultCount() > 0) {
                    if (ExchangeStore.getSharedInstance().getSelectedExchange() != null) {
                        date.setTime(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(
                                ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), news.getNewsDate()));
                    } else {
                        date.setTime(news.getNewsDate());
                    }
                } else {
                    try {
                        date.setTime(Settings.getLocalTimeFor(news.getNewsDate()));
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    date.setTime(news.getNewsDate());
                }

            } catch (Exception e) {
                date.setTime(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(
                        news.getExchange(), news.getNewsDate()));
            }
            String title = news.getHeadLine();
            String source = news.getSource();
            if (title == null) {
                title = "";
            }
            if (source == null) {
                source = "";
            }
            String body = news.getBody();
            body = body.replaceAll("\n", "<br>"); // replace the line break with HTML line break

            String page = replaceString(template, "[NEWS_TITLE]", title);
            page = replaceString(page, "[ID]", news.getNewsID());

            page = replaceString(page, "[NEWS_DATE]", bodyDateFormat.format(date));

            page = replaceString(page, "[NEWS_SOURCE]", source);

            if (body == null) {
                page = replaceString(page, "[NEWS_BODY]", title);
            } else {
                page = replaceString(page, "[NEWS_BODY]", body);
            }

            page = UnicodeUtils.getNativeString(page);
            return page;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void showNews(String id) {
        News news = getNews(id);
        showNews(news);
    }

    public void showNews(News news) {
        try {
            String body = generateTempFile(news);
            String title = news.getHeadLine();
            Client.getInstance().getBrowserFrame().setContentType("text/html");
            Client.getInstance().getBrowserFrame().setTitle(Language.getString("NEWS"));
            Client.getInstance().getBrowserFrame().setNewsTitle(title);
            Client.getInstance().getBrowserFrame().setBody(body);
            Client.getInstance().getBrowserFrame().setText(body);
            Client.getInstance().getBrowserFrame().show(Constants.DOC_TYPE_NEWS);
            Client.getInstance().getBrowserFrame().bringToFont();
            news = null;
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private String replaceString(String buffer, String tag, String str) {
        int index = buffer.indexOf(tag);
        if (index >= 0) {
            return buffer.substring(0, index) + str + buffer.substring(index + tag.length());
        }
        return buffer;
    }

    private void loadTemplate(String language) {
        StringBuilder buffer = new StringBuilder();
        byte[] temp = new byte[1000];
        int count = 0;
        String lang;
        if (language != null) {
            lang = language.trim();
        } else {
            lang = language;
        }
        try {
            InputStream in = new FileInputStream(".\\Templates\\News_" + lang + ".htm");
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            template = buffer.toString();
            if (language == null) {
                if (Language.isLTR())
                    template = template.replaceFirst("\\[DIR\\]", "LTR");
                else
                    template = template.replaceFirst("\\[DIR\\]", "RTL");
            } else {
                if (language.toUpperCase().equals("EN"))
                    template = template.replaceFirst("\\[DIR\\]", "LTR");
                else
                    template = template.replaceFirst("\\[DIR\\]", "RTL");
            }
            template = template.replaceAll("\\[PATH\\]", ((String) System.getProperties().get("user.dir")).replaceAll("\\\\", "/"));
            System.out.println(template);
            buffer = null;
            in = null;
            temp = null;
        } catch (IOException ex) {
            template = "";
        }
    }

    public synchronized LinkedList getLastNewss() {
        return dataStore.getLastNewss();
    }

    public synchronized DynamicArray getFilteredNewss() {
        return dataStore.getFilteredNewss();
    }

    public synchronized DynamicArray getFilteredNewsStore() {
        return filteredStore.getFilteredNewss();
    }

    public void playSound() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    if ((System.currentTimeMillis() - lastAlrtTime) > 2000) { // gap between 2 consecative sounds
                        lastAlrtTime = System.currentTimeMillis();
                        NativeMethods.play(System.getProperties().get("user.dir") + "\\sounds\\news.wav", 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setFilterMode(int mode) {
        this.filterMode = mode;
    }

    public void setFilter(String exgId, String providerId) {
        if ((exgId.equals("*") && (providerId.equals("*")))) {
            filterType = FILTER_TYPE_NONE;
        } else if ((!exgId.equals("*") && (providerId.equals("*")))) {
            filterType = FILTER_TYPE_EXCHANGE;
            selectionExcgId = exgId;
        } else if ((exgId.equals("*") && (!providerId.equals("*")))) {
            filterType = FILTER_TYPE_PROVIDER;
            selectionProviderId = providerId;
        } else {
            filterType = FILTER_TYPE_EXC_AND_PRO;
            selectionExcgId = exgId;
            selectionProviderId = providerId;
        }
    }

    public void setSymbolFilter(Hashtable symbolhash) {
        filterType = FILTER_TYPE_SYMBOLS;
        this.addedSymbols = symbolhash;
        filteredStore.clear();
        for (int i = 0; i < dataStore.getFilteredNewss().size(); i++) {
            try {
                News news = (News) dataStore.getFilteredNewss().get(i);
                String[] symbolArr = news.getSymbol().split(" ");
                if (symbolArr.length > 0) {
                    for (int j = 0; j < symbolArr.length; j++) {
                        if (symbolhash.contains(symbolArr[j])) {
                            filteredStore.addNews(news.getSymbol(), news);
                        }
                    }
                }

            } catch (Exception e) {
            }
        }
    }

    public void setSourceFilter(Hashtable sourceHash) {
        filterType = FILTER_TYPE_SOURCES;
        this.addedSources = sourceHash;
        filteredStore.clear();
        for (int i = 0; i < dataStore.getFilteredNewss().size(); i++) {
            News news = (News) dataStore.getFilteredNewss().get(i);
            try {
                String[] categoryArr = news.getCategory().split(",");
                for (int k = 0; k < categoryArr.length; k++) {
                    if (sourceHash.containsKey(categoryArr[k].split("\\.")[0])) {
                        filteredStore.addNews(news.getSymbol(), news);
                    }
                }


            } catch (Exception e) {
                filteredStore.addNews(news.getSymbol(), news);
            }
        }
    }

    public void applySourceFilter() {
        filterType = FILTER_TYPE_SOURCES;
        filteredStore.clear();
        for (int i = 0; i < dataStore.getFilteredNewss().size(); i++) {
            News news = (News) dataStore.getFilteredNewss().get(i);
            try {
                String[] categoryArr = news.getCategory().split(",");
                for (int k = 0; k < categoryArr.length; k++) {
                    if (this.addedSources.containsKey(categoryArr[k].split("\\.")[0])) {
                        filteredStore.addNews(news.getSymbol(), news);
                    }
                }


            } catch (Exception e) {
                filteredStore.addNews(news.getSymbol(), news);
            }
        }
    }

    public void applyNewsFilter() {
        filteredStore.clear();
        for (int i = 0; i < dataStore.getFilteredNewss().size(); i++) {
            try {
                News news = (News) dataStore.getFilteredNewss().get(i);
                if (filterType == FILTER_TYPE_NONE) {
                    filteredStore.addNews(news.getSymbol(), news);
                } else if (filterType == FILTER_TYPE_EXCHANGE) {
                    if (news.getExchange().equals(selectionExcgId)) {
                        filteredStore.addNews(news.getSymbol(), news);
                    }
                } else if (filterType == FILTER_TYPE_PROVIDER) {

                    if (!news.isContainInCategory(selectionProviderId)) {
                        if (selectionProviderId.equalsIgnoreCase(NewsProvidersStore.getSharedInstance().getProvider(news.getNewsProvider()).getRealID())) {
                            filteredStore.addNews(news.getSymbol(), news);
                        }
                    } else {
                        filteredStore.addNews(news.getSymbol(), news);
                    }
                } else {
                    if ((selectionProviderId.equalsIgnoreCase(NewsProvidersStore.getSharedInstance().getProvider(news.getNewsProvider()).getRealID()))
                            && (selectionExcgId.equalsIgnoreCase(news.getExchange()))) {
                        filteredStore.addNews(news.getSymbol(), news);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public boolean isCorrectFiltering(News news) {
        boolean isCorrectSort = false;
        try {
            switch (filterType) {
                case FILTER_TYPE_NONE:
                    isCorrectSort = true;
                    break;
                case FILTER_TYPE_EXCHANGE:
                    if (news.getExchange().equals(selectionExcgId)) {
                        isCorrectSort = true;
                    } else {
                        isCorrectSort = false;
                    }
                    break;
                case FILTER_TYPE_PROVIDER:
                    if (!news.isContainInCategory(selectionProviderId)) {
                        if (selectionProviderId.equalsIgnoreCase(NewsProvidersStore.getSharedInstance().getProvider(news.getNewsProvider()).getRealID())) {
                            isCorrectSort = true;
                        } else {
                            isCorrectSort = false;
                        }
                    } else {
                        isCorrectSort = true;
                    }
                    break;
                case FILTER_TYPE_EXC_AND_PRO:
                    if ((news.getExchange().equalsIgnoreCase(selectionExcgId))) {
                        if (!news.isContainInCategory(selectionProviderId)) {
                            if (selectionProviderId.equalsIgnoreCase(NewsProvidersStore.getSharedInstance().getProvider(news.getNewsProvider()).getRealID())) {
                                isCorrectSort = true;
                            } else {
                                isCorrectSort = false;
                            }
                        } else {
                            isCorrectSort = true;
                        }
                    } else {
                        isCorrectSort = false;
                    }
                    break;
                case FILTER_TYPE_SYMBOLS:
                    String[] symbolArr = news.getSymbol().split(" ");
                    if (symbolArr.length > 0) {
                        for (int j = 0; j < symbolArr.length; j++) {
                            if (addedSymbols.contains(symbolArr[j])) {
                                isCorrectSort = true;
                            } else {
                                isCorrectSort = false;
                            }
                        }
                    } else {
                        isCorrectSort = false;
                    }
                    break;
                case FILTER_TYPE_SOURCES:
                    if (news.getCategory() != null) {
                        String[] sourceArr = news.getCategory().split(",");
                        if (sourceArr.length > 0) {
                            for (int j = 0; j < sourceArr.length; j++) {
                                if (addedSources.contains(sourceArr[j].split("\\.")[0])) {
                                    isCorrectSort = true;
                                } else {
                                    isCorrectSort = false;
                                }
                            }
                        } else {
                            isCorrectSort = false;
                        }
                    } else {
                        return isCorrectSort = true;
                    }

                    break;
                default:
                    break;
            }
        } catch (Exception e) {
        }
        return isCorrectSort;
    }

}


