package com.isi.csvr.news;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 12, 2005
 * Time: 10:08:35 AM
 */
public class NewsComparator implements Comparator, Serializable {
    public static int TYPE_SORT = 0;
    private int type = TYPE_SORT;
    public static int TYPE_SEARCH = 1;

    public NewsComparator(int type) {
        this.type = type;
    }

    public int compare(Object o1, Object o2) {
        News announcement1 = (News) o1;
        News announcement2 = (News) o2;

        if (type == TYPE_SORT) {
            if (announcement2.getNewsDate() == announcement1.getNewsDate()) {
                return (announcement2.getNewsID().compareToIgnoreCase(announcement1.getNewsID()));
            } else if (announcement2.getNewsDate() > announcement1.getNewsDate()) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return (announcement2.getNewsID().compareToIgnoreCase(announcement1.getNewsID()));
        }
    }
}
