package com.isi.csvr.news;

import com.isi.csvr.*;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.TimeZoneMap;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;
import com.mubasher.outlook.Reminder;
import com.mubasher.outlook.ReminderUI;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA. User: Chandika Hewage Date: Dec 10, 2007 Time: 12:52:29 PM To change this template use File
 * | Settings | File Templates.
 */
public class NewsWindow extends InternalFrame implements ExchangeListener, ActionListener, Themeable, FocusListener, ApplicationListener,
        ConnectionListener, MouseListener, KeyListener, InternalFrameListener {
    public static NewsWindow self;
    final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
    final ImageIcon iconBusy = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
    final String searching = Language.getString("SEARCHING");
    public TWButton btnHide;
    ArrayList<TWComboItem> exchangeArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> exchangesArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> providerArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> providerSearchArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> predefArray = new ArrayList<TWComboItem>();
    ViewSetting oSetting1;
    ViewSetting oSetting2;
    private JPanel mainPanel;
    private TWTabbedPane newsTabPanel;
    private JPanel realTimePanel;
    private JPanel searchPanel;
    private JPanel realTimeLowerPanel;
    private JPanel realTimeUpperPanel;
    private JPanel searchUpper;
    private JPanel criteriaPanel;
    private JPanel periodPanel;
    private JPanel buttonPanel;
    private JPanel hidePanel;
    private JPanel searchLowerPanel;
    private TWLabel exchangeLabel, symbolLabel, providerLabel, keywordLabel, busyLabel;
    private TWLabel lblexchange;
    private TWTextField keywordField;
    private TWTextField symbolField;
    private TWComboBox cmbExchange;
    private JLabel resultLbl;
    private TWComboBox cmbProvider, cmbExchanges;
    private TWComboBox cmbProvidersR;
    private TWComboBox cmbPredef;
    private TWComboModel exchangeModel;
    private TWComboModel exchangeSModel;
    private TWComboModel providerModel;
    private TWComboModel providerRModel;
    private TWComboModel preDefModel;
    private TWButton btnDownArraow;
    private TWButton btnNext;
    private TWButton btnPrev;
    private TWButton btnNextL;
    private TWButton btnPrevL;
    private TWButton btnSearch;
    private TWButton btnShow;
    private JRadioButton rbPreDef = new JRadioButton(Language.getString("PRE_DEFINED"));
    private JRadioButton rbCustom = new JRadioButton(Language.getString("CUSTOM_NEWS"));
    private ButtonGroup periodGroup = new ButtonGroup();
    private DateCombo fromDateCombo;
    private DateCombo toDateCombo;
    private Table table1;
    private NewsModel model;
    private Table table2;
    private NewsModel model2;
    private boolean isLatest = true;
    private NewsMouseListener selectionListener;
    private int width = 700;
    private int height = 550;
    private boolean isconnected = true;
    private AdvanceSearchWindow advanceframe;
    private boolean isSymbolSearched = false;

    private TWMenu mnuTimeZones;
    private TWMenuItem mnuOutlook;

    private NewsWindow() {
        mainPanel = new JPanel();
        realTimePanel = new JPanel();
        searchPanel = new JPanel();
        newsTabPanel = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "nw");
        createUI();
        createShowHidepanel();

        Theme.registerComponent(this);
    }

    public static NewsWindow getSharedInstance() {
        if (self == null) {
            self = new NewsWindow();
        }
        return self;
    }

    public void createUI() {
        this.setSize(width, height);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        this.add(mainPanel);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting1 = ViewSettingsManager.getSummaryView("NEWS");
        oSetting2 = ViewSettingsManager.getSummaryView("NEWS_SEARCH");
        oSetting1.setParent(this);
        oSetting2.setParent(this);
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        mainPanel.add(newsTabPanel);
        lblexchange = new TWLabel(Language.getString("EXCHANGE"));
        exchangeModel = new TWComboModel(exchangeArray);
        exchangeSModel = new TWComboModel(exchangesArray);
        cmbExchange = new TWComboBox(exchangeModel);
        btnNext = new TWButton(Language.getString("NEXT"));
        btnNext.addActionListener(this);
        btnNext.setActionCommand("NEXT");
        btnPrev = new TWButton(Language.getString("PREVIOUS"));
        btnPrev.addActionListener(this);
        btnPrev.setActionCommand("PREVIOUS");
        btnNext.setEnabled(false);
        btnPrev.setEnabled(false);
        btnNextL = new TWButton(Language.getString("NEXT"));
        btnNextL.addActionListener(this);
        btnNextL.setActionCommand("NEXTL");
        btnPrevL = new TWButton(Language.getString("PREVIOUS"));
        btnPrevL.addActionListener(this);
        btnPrevL.setActionCommand("PREVIOUSL");
        btnNextL.setEnabled(false);
        btnPrevL.setEnabled(false);
        btnSearch = new TWButton(Language.getString("WINDOW_TITLE_SEARCH"));
        btnSearch.addActionListener(this);
        btnSearch.setActionCommand("SEARCH");
        btnHide = new TWButton(Language.getString("ADV_SEARCH"));
        btnHide.addActionListener(this);
        btnHide.setActionCommand("HIDE");
        btnShow = new TWButton(new DownArrow());
        btnShow.addActionListener(this);
        btnShow.setActionCommand("SHOW");
        busyLabel = new TWLabel();
        busyLabel.setOpaque(true);
        btnShow.setBorder(BorderFactory.createEmptyBorder());
        searchUpper = new JPanel();
        criteriaPanel = new JPanel();
        periodPanel = new JPanel();
        buttonPanel = new JPanel();
        hidePanel = new JPanel();
        searchLowerPanel = new JPanel();
        exchangeLabel = new TWLabel(Language.getString("EXCHANGE"));
        exchangeLabel.setOpaque(true);
        symbolLabel = new TWLabel(Language.getString("SYMBOL"));
        symbolLabel.setOpaque(true);
        providerLabel = new TWLabel(Language.getString("PROVIDER"));
        providerLabel.setOpaque(true);
        keywordLabel = new TWLabel(Language.getString("KEYWORD"));
        keywordLabel.setOpaque(true);
        keywordField = new TWTextField();
        periodGroup.add(rbPreDef);
        periodGroup.add(rbCustom);
        rbPreDef.addFocusListener(this);
        rbPreDef.setOpaque(true);
        rbPreDef.setSelected(true);
        rbCustom.addFocusListener(this);
        rbCustom.setOpaque(true);
        rbPreDef.addActionListener(this);
        rbCustom.addActionListener(this);
        providerModel = new TWComboModel(providerSearchArray);
        providerRModel = new TWComboModel(providerArray);
        preDefModel = new TWComboModel(predefArray);
        cmbProvider = new TWComboBox(providerModel);
        cmbExchanges = new TWComboBox(exchangeSModel);
        cmbProvidersR = new TWComboBox(providerRModel);
        cmbPredef = new TWComboBox(preDefModel);
        SearchedNewsStore.getSharedInstance().setButtonReferences(btnNextL, btnPrevL, btnSearch);
        populateExchanges();
        populateProviders();
        populateDuration();
        newsTabPanel.addTab(Language.getString("REALTIME_NEWS"), createRealTimePanel());
        newsTabPanel.addTab(Language.getString("WINDOW_TITLE_SEARCH"), createSearchPanel());
        newsTabPanel.selectTab(0);
        this.setTitle(Language.getString(oSetting1.getCaptionID()));
        GUISettings.applyOrientation(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public void selectRealTimeNews() {
        newsTabPanel.setSelectedIndex(0);
    }

    public void selectNewsSearch() {
        newsTabPanel.setSelectedIndex(1);
    }

    private JPanel createRealTimePanel() {
//        realTimePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%"}, 0, 0));
        realTimePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        realTimeUpperPanel = new JPanel();
        realTimeLowerPanel = new JPanel();
        realTimeUpperPanel.setLayout(new FlexGridLayout(new String[]{"70", "10", "120", "10", "70", "10", "120", "100%"}, new String[]{"100%"}, 5, 5, true, true));
        //adding componets to realTimeUpper
        realTimeUpperPanel.add(lblexchange);
        realTimeUpperPanel.add(new TWLabel());
        realTimeUpperPanel.add(cmbExchange);
        realTimeUpperPanel.add(new TWLabel());
        realTimeUpperPanel.add(new TWLabel(Language.getString("PROVIDER")));
        realTimeUpperPanel.add(new TWLabel());
        realTimeUpperPanel.add(cmbProvidersR);
        realTimeUpperPanel.add(new TWLabel());
        realTimeLowerPanel.setLayout(new FlexGridLayout(new String[]{"80", "100%", "80"}, new String[]{"100%"}, 5, 5, true, true));
        //adding realTimeTable
        table1 = new Table();//{
//            public void paint(Graphics g) {
//                super.paint(g);
//                if(!isLatest){
//                    if (SearchedNewsStore.getSharedInstance().isSearchInProgress()) {
//                        FontMetrics fontMetrics = table1.getFontMetrics(getFont());
//                        g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
//                                (getHeight()) / 2);
//                        g.drawImage(icon.getImage(),
//                                (getWidth() - fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
//                                (getHeight() - icon.getImage().getHeight(this)) / 2, this);
//                        fontMetrics = null;
//                    }
//                }
//            }
//        };
//        table1.setAutoResize(true);
        table1.getPopup().hidePrintMenu();
        this.setTable(table1);
        final TWMenuItem indicesSearch = new TWMenuItem(Language.getString("NEWS_FILTER"), "newsfilter.gif");
        indicesSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new NewsSourceSelectionWindow();
            }
        });
        mnuOutlook = new TWMenuItem(Language.getString("OUTLOOK_MENU"), "outlook.gif");
        mnuOutlook.addActionListener(this);
        mnuOutlook.setActionCommand("OUTLOOK");  /*{
            public void actionPerformed(ActionEvent e) {
                if(Reminder.checkAvailability()){
                    ReminderUI rUI = new ReminderUI(Client.getInstance().getFrame());
                    rUI.showDiaog();
                } else {
                    new ShowMessage(Language.getString("MSG_NO_OUTLOOK_SUPPORT"),"E");
                }
            }
        });*/
        table1.getPopup().setMenuItem(indicesSearch);
        table1.getPopup().setMenuItem(mnuOutlook);
        model = new NewsModel();
        model.setDirectionLTR(Language.isLTR());
        table1.setWindowType(ViewSettingsManager.NEWS_VIEW);
        model.setViewSettings(oSetting1);
        table1.setModel(model);
        table1.setAutoResize(false);
        selectionListener = new NewsMouseListener(table1.getTable(), NEWS_TYPES.ALL);
        table1.getTable().getSelectionModel().addListSelectionListener(selectionListener);
        table1.getTable().addMouseListener(selectionListener);
        table1.getPopup().setAutoWidthAdjustMenuVisible(false);
        table1.setPreferredSize(new Dimension(600, 100));

        model.setDataStore(NewsStore.getSharedInstance().getFilteredNewsStore());            //getFilteredNewss()  //getFilteredNewsStore()
        model.setTable(table1);
        model.applyColumnSettings();

        table1.updateGUI();

        //adding Componets to realTimeLower
        realTimeLowerPanel.add(btnPrev);
        realTimeLowerPanel.add(new TWLabel());
        realTimeLowerPanel.add(btnNext);

        //adding components to realTimePanel
//        realTimePanel.add(realTimeUpperPanel);
        realTimePanel.add(table1);
//        realTimePanel.add(realTimeLowerPanel);
        cmbExchange.addActionListener(this);
        cmbProvidersR.addActionListener(this);
        oSetting1.setTableNumber(1);
        super.applySettings();
        cmbExchange.setEnabled(true);
        cmbProvidersR.setEnabled(true);
        GUISettings.applyOrientation(realTimeUpperPanel);
        GUISettings.applyOrientation(realTimeLowerPanel);
        GUISettings.applyOrientation(realTimePanel);
        return realTimePanel;

    }

    private JPanel createSearchPanel() {
        resultLbl = new JLabel();

        searchLowerPanel.setLayout(new FlexGridLayout(new String[]{"90", "50%", "220", "50%", "90"}, new String[]{"100%"}, 5, 5, true, true));
        searchLowerPanel.add(btnPrevL);
        searchLowerPanel.add(new TWLabel());
        searchLowerPanel.add(resultLbl);
        searchLowerPanel.add(new TWLabel());
        searchLowerPanel.add(btnNextL);

        searchPanel.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);

        searchPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%", "30"}, 0, 0));

//        searchUpper.setLayout(new FlexGridLayout(new String[]{"325", "250", "125","100%"}, new String[]{"100%"}, 0, 0));
//        searchUpper.add(createCriteriaPanel());
//        searchUpper.add(createPeriodPanel());
//        searchUpper.add(createButtonPanel());
//        searchUpper.add(new TWLabel());
//        searchUpper.setLayout(new FlexGridLayout(new String[]{"10", "80", "6", "150", "12", "80", "6", "130", "100%", "80", "10", "130", "10"}, new String[]{"100%"}, 0, 3));
        searchUpper.setLayout(new FlexGridLayout(new String[]{"10", "80", "6", "150", "12", "80", "6", "130", "100%", "100", "10", "150", "10"}, new String[]{"100%"}, 0, 3));
        searchUpper.add(new JLabel());
        searchUpper.add(symbolLabel);
        searchUpper.add(new JLabel());
        searchUpper.add(createSymbolPanel());
        searchUpper.add(new JLabel());
        JPanel tempPanel = new JPanel();
        tempPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 2));
        tempPanel.add(keywordField);
        searchUpper.add(keywordLabel);
        searchUpper.add(new JLabel());
        searchUpper.add(tempPanel);
        searchUpper.add(new JLabel());
        searchUpper.add(btnSearch);
        searchUpper.add(new JLabel());
        searchUpper.add(btnHide);
        searchUpper.add(new JLabel());
        btnSearch.setActionCommand("SEARCH");
        searchUpper.registerKeyboardAction(this, "SEARCH", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        btnSearch.addKeyListener(this);
        //creating searched news table
        table2 = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (!isLatest) {
                    if (SearchedNewsStore.getSharedInstance().isNewsSearchInProgress()) {
                        FontMetrics fontMetrics = table2.getFontMetrics(getFont());
                        g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                                (getHeight()) / 2);
                        g.drawImage(icon.getImage(),
                                (getWidth() - (fontMetrics.stringWidth(searching)) / 2) - (getWidth() / 2),
                                (getHeight() - icon.getImage().getHeight(this)) / 2 + 15, table2.getBackground(), this);
                        // + fontMetrics.stringWidth(searching) + 10
                        fontMetrics = null;
                    }
                }
            }
        };
        //todo:outlook reminder added
        TWMenuItem mnuOutlook2 = new TWMenuItem(Language.getString("OUTLOOK_MENU"), "outlook.gif");
        mnuOutlook2.addActionListener(this);
        mnuOutlook2.setActionCommand("OUTLOOK2");
        table2.getPopup().setMenuItem(mnuOutlook2);
//        this.setTable(table2);
//        table2.setAutoResize(true);
        Table searchTbl[] = new Table[1];
        searchTbl[0] = table2;
        this.setTable2(searchTbl);
        model2 = new NewsModel();
        model2.setDirectionLTR(Language.isLTR());
        table2.setWindowType(ViewSettingsManager.NEWS_VIEW);
        model2.setViewSettings(oSetting2);
        table2.setModel(model2);
        table2.setAutoResize(false);
        selectionListener = new NewsMouseListener(table2.getTable(), NEWS_TYPES.ALL);
        table2.getTable().getSelectionModel().addListSelectionListener(selectionListener);
        table2.getTable().addMouseListener(selectionListener);
        table2.getPopup().setAutoWidthAdjustMenuVisible(false);
        table2.getPopup().hidePrintMenu();
        table2.setPreferredSize(new Dimension(600, 100));
        model2.setDataStore(SearchedNewsStore.getSharedInstance().getStore());
        model2.setTable(table2);
        model2.applyColumnSettings();
        table2.updateGUI();
        searchPanel.add(searchUpper);
        searchPanel.add(table2);
        searchPanel.add(searchLowerPanel);
        oSetting2.setTableNumber(2);
        super.applySettings();
        GUISettings.applyOrientation(searchUpper);
        GUISettings.applyOrientation(searchLowerPanel);
        GUISettings.applyOrientation(searchPanel);
        return searchPanel;
    }

    private JPanel createCriteriaPanel() {
        String[] width = {"40%", "60%"};
        String[] height = {"20", "20", "20", "20"};
        criteriaPanel.setLayout(new FlexGridLayout(width, height, 2, 2));
        criteriaPanel.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CRITERIA"), 0, 0, criteriaPanel.getFont().deriveFont(Font.BOLD), Theme.getColor("BORDER_COLOR")));
//        criteriaPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CRITERIA")));
        criteriaPanel.doLayout();
        criteriaPanel.add(exchangeLabel);
        criteriaPanel.add(cmbExchanges);
        criteriaPanel.add(symbolLabel);
        criteriaPanel.add(createSymbolPanel());
        criteriaPanel.add(keywordLabel);
        criteriaPanel.add(keywordField);
        criteriaPanel.add(providerLabel);
        criteriaPanel.add(cmbProvider);
        cmbExchanges.addActionListener(this);
        GUISettings.applyOrientation(criteriaPanel);
        return criteriaPanel;
    }

    private JPanel createPeriodPanel() {
        String[] dateWidths = {"30%", "70%"};
        String[] dateHeights = {"20"};
        fromDateCombo = new DateCombo(Client.getInstance().getFrame());
        fromDateCombo.addFocusListener(this);
        JPanel fromPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 5));
        fromPanel.add(new TWLabel(Language.getString("FROM")));
        fromPanel.add(fromDateCombo);
        fromPanel.doLayout();
        toDateCombo = new DateCombo(Client.getInstance().getFrame());
        toDateCombo.addFocusListener(this);
        JPanel toPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 5));
        toPanel.add(new TWLabel(Language.getString("TO")));
        toPanel.add(toDateCombo);
        toPanel.doLayout();

        String[] widthdPanel = {"40%", "60%"};
        String[] heightdPanel = {"20", "25", "25"};
        periodPanel.setLayout(new FlexGridLayout(widthdPanel, heightdPanel, 5, 5));
        periodPanel.setBorder(BorderFactory.createTitledBorder(null, Language.getString("PERIOD"), 0, 0, periodPanel.getFont().deriveFont(Font.BOLD), Theme.getColor("BORDER_COLOR")));
//        periodPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("PERIOD")));
        periodPanel.add(rbPreDef);
        periodPanel.add(cmbPredef);
        periodPanel.add(rbCustom);
        periodPanel.add(fromPanel);
        periodPanel.add(new TWLabel());
        periodPanel.add(toPanel);
        GUISettings.applyOrientation(periodPanel);
        return periodPanel;
    }

    private JPanel createButtonPanel() {
        //create busy label
        busyLabel = new TWLabel();
        busyLabel.setOpaque(true);
        buttonPanel.setLayout(new FlexGridLayout(new String[]{"20%", "60%", "20%"}, new String[]{"1", "25", "25", "100%"}, 0, 5));
        buttonPanel.add(new TWLabel());
        buttonPanel.add(new TWLabel());
        buttonPanel.add(new TWLabel());
        buttonPanel.add(new TWLabel());
        buttonPanel.add(btnSearch);
        buttonPanel.add(new TWLabel());
        buttonPanel.add(new TWLabel());
        buttonPanel.add(btnHide);
        buttonPanel.add(new TWLabel());
        buttonPanel.add(new TWLabel());
        buttonPanel.add(busyLabel);
        buttonPanel.add(new TWLabel());
        GUISettings.applyOrientation(buttonPanel);
        return buttonPanel;
    }

    //---- Showbtn has replaced with a Label-----
    private JPanel createShowHidepanel() {
        hidePanel.setLayout(new FlexGridLayout(new String[]{"100%", "0"}, new String[]{"100%"}, 0, 0));
        hidePanel.setPreferredSize(new Dimension(775, 20));
        btnShow.setPreferredSize(new Dimension(20, 20));
        TWLabel lbDummy = new TWLabel();
//        lbDummy.setPreferredSize(new Dimension(755,20));
        lbDummy.setPreferredSize(new Dimension(480, 20));

        TWLabel showLabel = new TWLabel(Language.getString("SEARCHED_OPTION_WINDOW"), JLabel.RIGHT);
        showLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        showLabel.setPreferredSize(new Dimension(275, 20));

        showLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                showPanels();
                updateUI();
            }
        });
//        showLabel.s("SHOW");
//        hidePanel.add(btnShow);
        hidePanel.add(lbDummy);
        hidePanel.add(showLabel);
        GUISettings.applyOrientation(hidePanel);
        return hidePanel;
    }

    private void hidePanels() {
//        searchPanel.setVisible(false);
        searchPanel.removeAll();
        searchPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "100%", "25"}, 0, 0));
//       hidePanel.setVisible(true);
//       searchUpper.setVisible(false);
        searchPanel.add(hidePanel);
        searchPanel.add(table2);
        searchPanel.add(searchLowerPanel);
        searchPanel.revalidate();
        searchPanel.updateUI();
    }

    private void showPanels() {
//       hidePanel.setVisible(false);
//       searchUpper.setVisible(true);
//        searchPanel.setVisible(true);
        searchPanel.removeAll();
        searchPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"130", "100%", "25"}, 0, 0));
        searchUpper.setLayout(new FlexGridLayout(new String[]{"325", "325", "125", "100%"}, new String[]{"100%"}, 0, 0));
        searchPanel.add(searchUpper);
        searchPanel.add(table2);
        searchPanel.add(searchLowerPanel);
        searchPanel.revalidate();
        searchPanel.updateUI();
    }

    private JPanel createSymbolPanel() {
        String[] widths = {"100%", "20"};
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights, 0, 2);

        JPanel panel = new JPanel(layout);
        symbolField = new TWTextField();
        symbolField.addFocusListener(this);
        panel.add(symbolField);
        btnDownArraow = new TWButton("...");
        btnDownArraow.addActionListener(this);
        btnDownArraow.setActionCommand("ARROW");
        symbolField.setEditable(true);
        panel.add(btnDownArraow);
        GUISettings.applyOrientation(panel);
        return panel;
    }

    public void populateExchanges() {
        exchangeArray.clear();
        exchangesArray.clear();
        Enumeration exchangeObjects = ExchangeStore.getSharedInstance().getExchanges();
        while (exchangeObjects.hasMoreElements()) {
            Exchange exchanges = (Exchange) exchangeObjects.nextElement();
            if (!exchanges.isExpired() && !exchanges.isInactive()) {
                exchangeArray.add(new TWComboItem(exchanges.getSymbol(), exchanges.getDescription()));
                exchangesArray.add(new TWComboItem(exchanges.getSymbol(), exchanges.getDescription()));
            }
            exchanges = null;
        }
        exchangeObjects = null;
        Collections.sort(exchangeArray);
        Collections.sort(exchangesArray);
        if (exchangeArray.size() > 1) {
            TWComboItem allItem = new TWComboItem("*", Language.getString("ALL"));
            exchangeArray.add(0, allItem);
        }
        try {
            cmbExchange.setSelectedIndex(0);
            cmbExchanges.setSelectedIndex(0);
        } catch (Exception e) {
        }
    }

    public void populateProviders() {
        providerArray.clear();
        providerSearchArray.clear();
        TWComboItem allItem = new TWComboItem("*", Language.getString("ALL"));
        TWComboItem allItem2 = new TWComboItem("*", Language.getString("ALL"));
        providerArray.add(allItem2);
        providerSearchArray.add(allItem);
        /*Enumeration<String> providerObjects = NewsProvidersStore.getSharedInstance().getNewsProviders();
        while (providerObjects.hasMoreElements()) {
            String id = providerObjects.nextElement();
            DetailedNewsProvider dnp= NewsProvidersStore.getSharedInstance().detailedProviders.get(id);
            if (dnp != null){
                providerArray.add(new TWComboItem(id, dnp.getDescription()));
                providerSearchArray.add(new TWComboItem(id,dnp.getDescription()));
            }
        }*/
        Hashtable<String, String> providers = NewsProvidersStore.getSharedInstance().getProviderMap();
        Enumeration<String> providerObjects = providers.keys();
        while (providerObjects.hasMoreElements()) {
            String id = providerObjects.nextElement();
            NewsProvider dnp = NewsProvidersStore.getSharedInstance().getProvider(id);
            if (dnp != null) {
                providerArray.add(new TWComboItem(providers.get(id), dnp.getDescription()));
                providerSearchArray.add(new TWComboItem(providers.get(id), dnp.getDescription()));
            }
        }
        providerObjects = null;
//        Collections.sort(providerArray);
//        Collections.sort(providerSearchArray);
        if (providerArray.size() > 0) {
//            cmbProvider.setSelectedIndex(0);
            cmbProvidersR.setSelectedIndex(0);
        }
        if (providerSearchArray.size() > 0) {
            cmbProvider.setSelectedIndex(0);
        }
        cmbProvider.updateUI();
        cmbProvidersR.updateUI();
    }

    private void populateDuration() {
        predefArray.clear();
        predefArray.add(new TWComboItem(1, Language.getString("1_DAY")));
        predefArray.add(new TWComboItem(2, Language.getString("1_WEEK")));
        predefArray.add(new TWComboItem(3, Language.getString("1_MONTH")));
        predefArray.add(new TWComboItem(4, Language.getString("1_YEAR")));
        predefArray.add(new TWComboItem("*", Language.getString("LBL_ALL")));

        try {
            cmbPredef.setSelectedIndex(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDefaultValues() {
        cmbExchange.setSelectedIndex(0);
        //cmbExchanges.setSelectedIndex(0);
        TWComboItem item1 = (TWComboItem) cmbExchange.getSelectedItem();
        TWComboItem item2 = (TWComboItem) cmbProvidersR.getSelectedItem();
//        NewsStore.getSharedInstance().setFilterCriteria(item1.getValue().trim(),item2.getValue());

    }

    public void setDataStore(boolean isLatestNews) {
        if (isLatestNews) {
            model.setDataStore(NewsStore.getSharedInstance().getFilteredNewsStore());       //getFilteredNewss()
            table1.repaint();
        } else {
            model2.setDataStore(SearchedNewsStore.getSharedInstance().getStore());
            table2.repaint();
        }
    }

    public void setSearchedDataStore() {
        model2.setDataStore(SearchedNewsStore.getSharedInstance().getStore());
    }

    public void setSelectedExchange(String exchange) {
        for (TWComboItem item : exchangesArray) {
            if (exchange.equals(item.getId())) {
                cmbExchanges.setSelectedItem(item);
                return;
            }
        }
    }

    public void setLatest(boolean isLatest) {
        this.isLatest = isLatest;
    }

    private void searchSymbol() {
        try {
            Symbols symbols = new Symbols();
//            symbols.appendSymbol(SharedMethods.getKey(((TWComboItem) cmbExchanges.getSelectedItem()).getId(), symbolField.getText()));
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
            oCompanies.setTitle(getTitle());
            oCompanies.setSelectedExchange(((TWComboItem) cmbExchanges.getSelectedItem()).getId());
            oCompanies.setSingleMode(true);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true);
            oCompanies = null;

            String key = symbols.getSymbols()[0];
            String exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);
            isSymbolSearched = true;
            symbolField.setText(symbol);

            for (TWComboItem item : exchangesArray) {
                if (item.getId().equals(exchange)) {
                    cmbExchanges.setSelectedItem(item);
                    break;
                }
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void search(long sequenceNo) {
        if ((rbPreDef.isSelected()) || (rbCustom.isSelected())) {
            int exchangeindex = cmbExchanges.getSelectedIndex();
            TWComboItem selExchange = exchangesArray.get(exchangeindex);

            String exchangeID = "*";
            if (isSymbolSearched) {
                exchangeID = selExchange.getId();
            }
            String symbol = symbolField.getText();
            if ((symbol == null) || (symbol.equals(""))) {
                symbol = "*";
            } else if (symbol.contains("`")) {
                try {
                    String symbol2 = symbol.split("`")[0];
                    symbol = symbol2;
                } catch (Exception e) {
                    e.printStackTrace();
                    symbol = "*";
                }
            }

            String keyword = UnicodeUtils.getUnicodeString(keywordField.getText());
            if ((keyword == null) || (keyword.equals(""))) {
                keyword = "*";
            }

            String providerList = getProviderListForExchange(exchangeID);
//            populateProviders();

			/*int providerindex = cmbProvider.getSelectedIndex();
            TWComboItem selProvider = providerSearchArray.get(providerindex);
            String providerID = selProvider.getId();
            if(providerID.equals("*")){
                String providers="";
                Enumeration e = NewsProvidersStore.getSharedInstance().getProviderKeys();
                while (e.hasMoreElements()) {
                    providers = providers + "," + e.nextElement();
                }
                providerID=providers.trim().substring(1);
            } else if (providerID.equals("MUBASHER")) {
                Enumeration e = NewsProvidersStore.getSharedInstance().getProviderKeys();
                String providers = "";
                while (e.hasMoreElements()) {
                    String id = (String) e.nextElement();
                    if ((id).contains(providerID)) {
                        providers = providers + "," + id;

                }
            }
                providerID = providers.trim().substring(1);
            }*/
//            if ((NewsProvidersStore.getSharedInstance().isGlobalProvider(providerID)) && (!providerID.equals("*"))) {
//                Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeID);
//                if ((exchange != null)) {
//                    providerID = providerID + "." + exchange.getCountry();
//                }
//                exchange = null;
//            }
//            if(providerID.equals("*")){
//                String providers="";
//                for(int i=1;i<providerSearchArray.size();i++){
//                   providers=providers+","+((TWComboItem)providerSearchArray.get(i)).getId();
//                }
//                providerID=providers.trim().substring(1);
//            }


            String fromDate = null, toDate = null;
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
            if (rbCustom.isSelected()) {
                fromDate = fromDateCombo.getDateString();
                toDate = toDateCombo.getDateString();
                if ((fromDate != null) && (toDate != null)) {

                } else {
                    SharedMethods.showMessage(Language.getString("MSG_FILL_DATE_ENTRY"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } else {
                //setting pre defined time constants in request message
                cmbPredef.setSelectedIndex(2);
                int index1 = cmbPredef.getSelectedIndex();
                Calendar cal = Calendar.getInstance();
                if (index1 == 1) {
                    cal.add(Calendar.DAY_OF_MONTH, -1);
                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 2) {
                    cal.add(Calendar.WEEK_OF_MONTH, -1);
                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 3) {
                    cal.add(Calendar.MONTH, -1);
                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 4) {
                    cal.add(Calendar.YEAR, -1);
                    fromDate = dateformat.format(cal.getTime());
                } else {
                    fromDate = "19700101";
                }

                toDate = dateformat.format(new Date(System.currentTimeMillis()));
            }

            String searchString = Meta.NEWS_SEARCH_ALL + Meta.DS + exchangeID + Meta.FD + symbol + Meta.FD + providerList + Meta.FD + keyword + Meta.FD + fromDate + Meta.FD + toDate;
            String errString = Meta.NEWS_SEARCH_ALL + Meta.DS + Meta.FD + -1;

            System.out.println(searchString);
            SearchedNewsStore.getSharedInstance().initSearch();
            String contentIP = null;
            try {
                contentIP = NewsProvidersStore.getSharedInstance().getContentIp(providerList);
            } catch (Exception e) {
                contentIP = Settings.getActiveIP();
            }
            if (contentIP.equals("")) {
                contentIP = Settings.getActiveIP();
            }
            SearchedNewsStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), sequenceNo, exchangeID, contentIP, errString); //, path);
            SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
            setBusyMode(true);
            SearchedNewsStore.getSharedInstance().disableButtons();
            setDefaultValues();
            newsTabPanel.selectTab(1);
//            setSelectedExchange(exchangeID);
//            Client.getInstance().showNewsSearchWIndow();
            setLatest(false);
            Theme.unRegisterComponent(this);
        } else {
            SharedMethods.showMessage(Language.getString("MSG_FILL_DATE_ENTRY"), JOptionPane.ERROR_MESSAGE);
        }
        isSymbolSearched = false;
    }

    public String getProviderListForExchange(String exchange) {
        String providers = ((TWComboItem) cmbProvider.getSelectedItem()).getId();
        try {
            if (exchange.equals("*")) {
                if (providers.equals("*")) {
                    Enumeration<String> keys = NewsProvidersStore.getSharedInstance().getProviderKeys();
                    String returnStr = "";
                    while (keys.hasMoreElements()) {
                        returnStr += keys.nextElement() + ",";
                    }
                    return returnStr;
                } else {
                    return providers;
                }
            } else {
                if (providers.equals("*")) {
                    ArrayList<String> keys = NewsProvidersStore.getSharedInstance().getAllProvidersForExchange(exchange);
                    String returnStr = "";
                    for (String key : keys) {
                        returnStr += key + ",";
                    }
                    return returnStr;
                } else {
                    return providers;
                }
            }
        } catch (Exception e) {
            return providers;
        }
    }

    public String getProviderListForExchange(String exchange, String providerList) {
        String providers = ((TWComboItem) cmbProvider.getSelectedItem()).getId();
        try {
            if (exchange.equals("*")) {
                if (providers.equals("*")) {
                    Enumeration<String> keys = NewsProvidersStore.getSharedInstance().getProviderKeys();
                    String returnStr = "";
                    while (keys.hasMoreElements()) {
                        returnStr += keys.nextElement() + ",";
                    }
                    return returnStr;
                } else {
                    return providers;
                }
            } else {
                if (providers.equals("*")) {
                    ArrayList<String> keys = NewsProvidersStore.getSharedInstance().getAllProvidersForExchange(exchange);
                    String returnStr = "";
                    for (String key : keys) {
                        returnStr += key + ",";
                    }
                    return returnStr;
                } else {
                    return providerList;
                }
            }
        } catch (Exception e) {
            return providers;
        }
    }

    public void searchForSymbol(String key, long sequence, String providerID) {
        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = "*";//SharedMethods.getExchangeFromKey(key);
//        String exchange = SharedMethods.getExchangeFromKey(key);
        SimpleDateFormat dateFormatFull = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        String endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
        String startDate = dateFormatFull.format(calendar.getTime());
        String keyword = "*";
        ////////////////////////////////////////////////////////////////////////
        cmbPredef.setSelectedIndex(2);
        String providerList = getProviderListForExchange(exchange);
        String fromDate = null, toDate = null;
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
        if (rbCustom.isSelected()) {
            fromDate = fromDateCombo.getDateString();
            toDate = toDateCombo.getDateString();
            if ((fromDate != null) && (toDate != null)) {

            } else {
                SharedMethods.showMessage(Language.getString("MSG_FILL_DATE_ENTRY"), JOptionPane.ERROR_MESSAGE);
                return;
            }
        } else {
            //setting pre defined time constants in request message
            cmbPredef.setSelectedIndex(2);
            int index1 = cmbPredef.getSelectedIndex();
            Calendar cal = Calendar.getInstance();
            if (index1 == 1) {
                cal.add(Calendar.DAY_OF_MONTH, -1);
                fromDate = dateformat.format(cal.getTime());
            } else if (index1 == 2) {
                cal.add(Calendar.WEEK_OF_MONTH, -1);
                fromDate = dateformat.format(cal.getTime());
            } else if (index1 == 3) {
                cal.add(Calendar.MONTH, -1);
                fromDate = dateformat.format(cal.getTime());
            } else if (index1 == 4) {
                cal.add(Calendar.YEAR, -1);
                fromDate = dateformat.format(cal.getTime());
            } else {
                fromDate = "19700101";
            }

            toDate = dateformat.format(new Date(System.currentTimeMillis()));
        }
//        int index1 = cmbPredef.getSelectedIndex();
//        Calendar cal = Calendar.getInstance();
//        if (index1 == 1) {
//            cal.add(Calendar.DAY_OF_MONTH, -1);
//            startDate  = dateFormatFull.format(cal.getTime());
//        } else if (index1 == 2) {
//            cal.add(Calendar.WEEK_OF_MONTH, -1);
//            startDate  = dateFormatFull.format(cal.getTime());
//        } else if (index1 == 3) {
//            cal.add(Calendar.MONTH, -1);
//            startDate  = dateFormatFull.format(cal.getTime());
//        } else if (index1 == 4) {
//            cal.add(Calendar.YEAR, -1);
//            startDate  = dateFormatFull.format(cal.getTime());
//        } else {
//            startDate  = "19700101";
//        }
//        ////////////////////////////////////////////////////////////////////////

        String searchString = Meta.NEWS_SEARCH_ALL + Meta.DS + exchange + Meta.FD + symbol + Meta.FD + providerList + Meta.FD + keyword + Meta.FD + fromDate + Meta.FD + toDate;
        String errString = Meta.NEWS_SEARCH_ALL + Meta.DS + Meta.FD + -1;

        SearchedNewsStore.getSharedInstance().initSearch();
        String contentIP = null;
        try {
            contentIP = NewsProvidersStore.getSharedInstance().getProvider(providerID).getContentIp();
        } catch (Exception e) {
//            contentIP = Settings.getActiveIP();
            try {
                contentIP = NewsProvidersStore.getSharedInstance().getContentIp(providerList);
            } catch (Exception ex) {
                contentIP = Settings.getActiveIP();
            }
        }
        if (contentIP.equals("") || contentIP.equalsIgnoreCase("null")) {
            contentIP = Settings.getActiveIP();
        }

        SearchedNewsStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), sequence, exchange, contentIP, errString); //, path);
        SearchedNewsStore.getSharedInstance().disableButtons();
        SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
        setBusyMode(true);
        btnSearch.setText(Language.getString("CANCEL"));
        btnSearch.setActionCommand("CANCEL");
        btnHide.setEnabled(false);
        Client.getInstance().showNewsSearchWIndow();
        newsTabPanel.selectTab(1);
        symbolField.setText(symbol);
        keywordField.setText("");
        for (TWComboItem item : exchangesArray) {
            if (item.getId().equals(exchange)) {
                cmbExchanges.setSelectedItem(item);
                break;
            }
        }
        cmbProvider.setSelectedIndex(0);
        searchString = null;
        calendar = null;
        startDate = null;
        endDate = null;

    }

    public void updateSearchedTable() {
        table2.updateUI();
        table2.updateGUI();
    }

    public void setBusyMode(boolean isSearching) {
        if (isSearching) {
            busyLabel.setHorizontalAlignment(JLabel.CENTER);
            busyLabel.setIcon(iconBusy);
        } else {
            busyLabel.setIcon(null);
        }
    }

    public void mouseClicked(MouseEvent e) {
//       if(e.getSource().equals(table1)){
//           if(SwingUtilities.isRightMouseButton(e)){
//               System.out.println("Ela Bunga");
//           }
//       }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("HIDE") && e.getSource() instanceof TWMenuItem) {
            super.actionPerformed(e);
        } else if (e.getActionCommand().equals("HIDE")) {
            showAdvanceSearchFrame();
        } else if (e.getActionCommand().equals("OUTLOOK")) {
            if (Reminder.checkAvailability()) {
                ReminderUI rUI = new ReminderUI(Client.getInstance().getFrame());
                int row = table1.getTable().getSelectedRow();
//				row = ((TableSorter) table1.getTable().getModel()).getUnsortedRowFor(row);
                String title = (String) table1.getTable().getModel().getValueAt(row, -7);
                String body = (String) table1.getTable().getModel().getValueAt(row, -3);
                rUI.showDiaog(title, Language.getString("LOCATION_UNKNOWN"), body);
            } else {
                new ShowMessage(Language.getString("MSG_NO_OUTLOOK_SUPPORT"), "E");
            }
        } else if (e.getActionCommand().equals("OUTLOOK2")) {
            if (Reminder.checkAvailability()) {
                ReminderUI rUI = new ReminderUI(Client.getInstance().getFrame());
                int row = table2.getTable().getSelectedRow();
//				row = ((TableSorter) table1.getTable().getModel()).getUnsortedRowFor(row);
                String title = (String) table2.getTable().getModel().getValueAt(row, -7);
                String body = (String) table2.getTable().getModel().getValueAt(row, -3);
                rUI.showDiaog(title, Language.getString("LOCATION_UNKNOWN"), body);
            } else {
                new ShowMessage(Language.getString("MSG_NO_OUTLOOK_SUPPORT"), "E");
            }
        } else if (e.getActionCommand().equals("SHOW")) {
        } else if (e.getActionCommand().equals("O")) {
            if (isConnected()) {
                setSearchedDataStore();
                search(0);
                btnSearch.setText(Language.getString("CANCEL"));
                btnSearch.setActionCommand("CANCEL");
                btnHide.setEnabled(false);
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
//            }
        } else if (e.getSource() == btnNext) {
            doNextBntAction();
        } else if (e.getSource() == btnPrev) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
            if (isConnected()) {
                SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
                setBusyMode(true);
                btnSearch.setText(Language.getString("CANCEL"));
                btnSearch.setActionCommand("CANCEL");
                btnHide.setEnabled(false);
                SearchedNewsStore.getSharedInstance().disableButtons();
                SearchedNewsStore.getSharedInstance().doPreviuosSearch();
//                this.updateUI();
                btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
                btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
                SearchedNewsStore.getSharedInstance().enableButtons();
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getActionCommand().equals("ARROW")) {
            searchSymbol();
        } else if (e.getActionCommand().equals("SEARCH")) {
//            ReminderUI rUI = new ReminderUI(Client.getInstance().getFrame());
//            rUI.showDiaog();

            if (isConnected()) {
                if ((symbolField.getText().equals("")) && (keywordField.getText().equals(""))) {
                    new ShowMessage(Language.getString("MSG_NO_CRITERIA"), "E");
                } else {
                    setSearchedDataStore();
                    search(0);
                    btnSearch.setText(Language.getString("CANCEL"));
                    btnSearch.setActionCommand("CANCEL");
                    btnHide.setEnabled(false);
                }
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getActionCommand().equals("CANCEL")) {
            SearchedNewsStore.getSharedInstance().setSearchInProgress(false);
            SearchedNewsStore.getSharedInstance().initSearch();
            setBusyMode(false);
            btnHide.setEnabled(true);
            btnSearch.setText(Language.getString("WINDOW_TITLE_SEARCH"));
            Client.getInstance().setNewsSearchTitle(null);
            btnSearch.setActionCommand("SEARCH");
            btnHide.setEnabled(true);
            SearchedNewsStore.getSharedInstance().enableButtons();

        } else if (e.getSource().equals(btnNextL)) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
//            SearchedNewsStore.getSharedInstance().doNextSearch();
//            this.updateUI();
//            btnNextL.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrevL.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
//            SearchedNewsStore.getSharedInstance().enableButtons();
            if (isConnected()) {
                SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
                setBusyMode(true);
                btnSearch.setText(Language.getString("CANCEL"));
                btnSearch.setActionCommand("CANCEL");
                btnHide.setEnabled(false);
                SearchedNewsStore.getSharedInstance().disableButtons();
                SearchedNewsStore.getSharedInstance().doNextSearch();
//                this.updateUI();
                btnNextL.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
                btnPrevL.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
                SearchedNewsStore.getSharedInstance().enableButtons();
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getSource().equals(btnPrevL)) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
//            SearchedNewsStore.getSharedInstance().doPreviuosSearch();
//            this.updateUI();
//            btnNextL.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrevL.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
//            SearchedNewsStore.getSharedInstance().enableButtons();
            if (isConnected()) {
                SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
                setBusyMode(true);
                btnSearch.setText(Language.getString("CANCEL"));
                btnSearch.setActionCommand("CANCEL");
                btnHide.setEnabled(false);
                SearchedNewsStore.getSharedInstance().disableButtons();
                SearchedNewsStore.getSharedInstance().doPreviuosSearch();
                this.updateUI();
                btnNextL.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
                btnPrevL.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
                SearchedNewsStore.getSharedInstance().enableButtons();
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getSource() == cmbExchanges) {
            int index = cmbExchanges.getSelectedIndex();
            if (index == 0) {
                if (exchangesArray.size() > 1) {
                    symbolField.setText("");
                    symbolField.setEditable(false);
                } else {
                    symbolField.setEditable(true);
                }
                populateProviders();
            } else {
                symbolField.setEditable(true);
                TWComboItem selected = (TWComboItem) cmbExchanges.getSelectedItem();
                populateProvidersByExchange(selected.getId());

            }
        } else if (e.getSource() == rbPreDef) {
            if (rbPreDef.isSelected()) {
                rbPreDef.setSelected(true);
                rbCustom.setSelected(false);
                fromDateCombo.clearDate();
                toDateCombo.clearDate();
            } else {
                rbCustom.setSelected(false);
                rbPreDef.setSelected(true);
            }
        } else if (e.getSource() == rbCustom) {
            if (rbCustom.isSelected()) {
                rbCustom.setSelected(true);
                rbPreDef.setSelected(false);
                cmbPredef.setSelectedIndex(1);
            } else {
                rbPreDef.setSelected(false);
                rbCustom.setSelected(true);
                cmbPredef.setSelectedIndex(1);

            }
        } else if (e.getSource().equals(cmbExchange)) {
            try {
                TWComboItem item1 = (TWComboItem) cmbExchange.getSelectedItem();
                TWComboItem item2 = (TWComboItem) cmbProvidersR.getSelectedItem();
                NewsStore.getSharedInstance().setFilter(item1.getId(), item2.getId());
                NewsStore.getSharedInstance().applyNewsFilter();

            } catch (Exception e1) {
                e1.printStackTrace();
            }

        } else if (e.getSource().equals(cmbProvidersR)) {
            try {
                TWComboItem item1 = (TWComboItem) cmbExchange.getSelectedItem();
                TWComboItem item2 = (TWComboItem) cmbProvidersR.getSelectedItem();
                NewsStore.getSharedInstance().setFilter(item1.getId(), item2.getId());
                NewsStore.getSharedInstance().applyNewsFilter();
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

    }

    private void doNextBntAction() {
        if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
        if (isConnected()) {
            SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
            setBusyMode(true);
            btnSearch.setText(Language.getString("CANCEL"));
            btnSearch.setActionCommand("CANCEL");
            btnHide.setEnabled(false);
            SearchedNewsStore.getSharedInstance().disableButtons();
            SearchedNewsStore.getSharedInstance().doNextSearch();
            btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
            btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
            SearchedNewsStore.getSharedInstance().enableButtons();
        } else {
            new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
        }
    }

    private void populateProvidersByExchange(String exchange) {
        try {
            providerSearchArray.clear();
            providerSearchArray.trimToSize();
            TWComboItem allItem2 = new TWComboItem("*", Language.getString("ALL"));
            providerSearchArray.add(allItem2);
            ArrayList<String> exgProviders = NewsProvidersStore.getSharedInstance().getAllProvidersForExchange(exchange);
            NewsProvider np;
            for (String id : exgProviders) {
                try {
                    np = NewsProvidersStore.getSharedInstance().getProvider(id);
                    TWComboItem twci = new TWComboItem(np.getID(), np.getDescription());  //np.getRealID()
                    providerSearchArray.add(twci);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            if (providerSearchArray.size() > 1) {
                cmbProvider.setSelectedIndex(1);
            } else {
                cmbProvider.setSelectedIndex(0);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public boolean isConnected() {
        return isconnected;
    }

    public void exchangeAdded(Exchange exchange) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        populateExchanges();
        populateProviders();
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusGained(FocusEvent e) {
        if (e.getSource() == fromDateCombo) {
            rbCustom.setSelected(true);
            rbPreDef.setSelected(false);
        } else if (e.getSource() == toDateCombo) {
            rbCustom.setSelected(true);
            rbPreDef.setSelected(false);
        } else if (e.getSource() == rbPreDef) {
        } else if (e.getSource() == rbCustom) {
        }
    }

    public void focusLost(FocusEvent e) {
    }

    public void twConnected() {
        isconnected = true;
    }

    public void twDisconnected() {
        isconnected = false;
        SearchedNewsStore.getSharedInstance().setSearchInProgress(false);
        SearchedNewsStore.getSharedInstance().initSearch();
        setBusyMode(false);
        Client.getInstance().setNewsSearchTitle(null);
        btnSearch.setText(Language.getString("WINDOW_TITLE_SEARCH"));
        btnSearch.setActionCommand("SEARCH");
        btnHide.setEnabled(true);
        SearchedNewsStore.getSharedInstance().enableButtons();
        Client.getInstance().getDesktop().repaint();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(realTimeUpperPanel);
        SwingUtilities.updateComponentTreeUI(realTimeLowerPanel);
        SwingUtilities.updateComponentTreeUI(realTimePanel);
        SwingUtilities.updateComponentTreeUI(searchPanel);
        SwingUtilities.updateComponentTreeUI(criteriaPanel);
        SwingUtilities.updateComponentTreeUI(periodPanel);
        SwingUtilities.updateComponentTreeUI(buttonPanel);
        SwingUtilities.updateComponentTreeUI(searchUpper);
        SwingUtilities.updateComponentTreeUI(hidePanel);
        SwingUtilities.updateComponentTreeUI(searchLowerPanel);
        SwingUtilities.updateComponentTreeUI(table2);
        SwingUtilities.updateComponentTreeUI(table1);
        SwingUtilities.updateComponentTreeUI(mainPanel);
        SwingUtilities.updateComponentTreeUI(this);

    }

    public void resetSearchBtn() {
        btnSearch.setText(Language.getString("WINDOW_TITLE_SEARCH"));
        btnSearch.setActionCommand("SEARCH");
        btnHide.setEnabled(true);
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            e.consume();
            System.out.println("TEsting key event");
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
            if (isConnected()) {
                SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
                setBusyMode(true);
                btnSearch.setText(Language.getString("CANCEL"));
                btnSearch.setActionCommand("CANCEL");
                btnHide.setEnabled(false);
                SearchedNewsStore.getSharedInstance().disableButtons();
                SearchedNewsStore.getSharedInstance().doNextSearch();
                btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
                btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
                SearchedNewsStore.getSharedInstance().enableButtons();
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }


        }
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            e.consume();
            System.out.println("TEsting key event");
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
            if (isConnected()) {
                SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
                setBusyMode(true);
                btnSearch.setText(Language.getString("CANCEL"));
                btnSearch.setActionCommand("CANCEL");
                btnHide.setEnabled(false);
                SearchedNewsStore.getSharedInstance().disableButtons();
                SearchedNewsStore.getSharedInstance().doNextSearch();
                btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
                btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
                SearchedNewsStore.getSharedInstance().enableButtons();
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }


        }
    }

    private void showAdvanceSearchFrame() {
        advanceframe = AdvanceSearchWindow.getSharedInstance();
        advanceframe.clearFields();
        advanceframe.setLocationRelativeTo(Client.getInstance().getDesktop());
        GUISettings.applyOrientation(advanceframe);
        advanceframe.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        try {
            advanceframe.setSelected(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        advanceframe.setVisible(true);
    }

    public TWTabbedPane getNewsTab() {
        return newsTabPanel;
    }

    public void setCancelActions() {
        btnSearch.setText(Language.getString("CANCEL"));
        btnSearch.setActionCommand("CANCEL");
        btnHide.setEnabled(false);
    }

    public void setResultLblText(String text) {
        this.resultLbl.setText(text);
    }

    public void setField(String symbol, String keyWord) {
        keywordField.setText(keyWord);
        symbolField.setText(symbol);
    }

    public void createTimeZoneMenu() {
        mnuTimeZones.removeAll();
        String activZone = Settings.getItem("CURRENT_TIME_ZONE");
        if (activZone.equalsIgnoreCase("NULL")) {
            activZone = null;
        }

        ButtonGroup buttonGroup = new ButtonGroup();
        TWRadioButtonMenuItem menuItem = new TWRadioButtonMenuItem(Language.getString("EXCHANGE_TIMEZONE"));
        if (activZone == null) {
            menuItem.setSelected(true);
        }
        menuItem.setActionCommand("TZONE," + "NULL");
        buttonGroup.add(menuItem);
        mnuTimeZones.add(menuItem);
        menuItem.addActionListener(this);

        menuItem = new TWRadioButtonMenuItem(Language.getString("SYSTEM_TIMEZONE"));
        if (activZone == null) {
            menuItem.setSelected(true);
        }
        menuItem.setActionCommand("TZONE," + "NULL");
        buttonGroup.add(menuItem);
        mnuTimeZones.add(menuItem);
        menuItem.addActionListener(this);

        Enumeration zoneIDs = TimeZoneMap.getInstance().getZoneIDs();
        while (zoneIDs.hasMoreElements()) {
            String zoneID = (String) zoneIDs.nextElement();
            menuItem = new TWRadioButtonMenuItem(TimeZoneMap.getInstance().getZoneDescription(zoneID));
            if ((activZone != null) && (activZone.equals(zoneID))) {
                menuItem.setSelected(true);
            }
            menuItem.setActionCommand("TZONE," + zoneID);
            buttonGroup.add(menuItem);
            mnuTimeZones.add(menuItem);
            menuItem.addActionListener(this);
        }
        zoneIDs = null;
        GUISettings.applyOrientation(mnuTimeZones);
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        AdvanceSearchWindow.getSharedInstance().setVisible(false);
    }

    private class TWLabel extends JLabel {
        public TWLabel(String name) {
            super(name);
            this.setOpaque(true);
        }

        private TWLabel(String text, int horizontalAlignment) {
            super(text, horizontalAlignment);
            this.setOpaque(true);
        }

        private TWLabel() {
            super();
            this.setOpaque(true);
        }
    }

}
