package com.isi.csvr.news;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 30-Jul-2008 Time: 18:43:33 To change this template use File | Settings
 * | File Templates.
 */
public class AdvanceSearchWindow extends InternalFrame implements ActionListener, Themeable, FocusListener, ConnectionListener, MouseListener, KeyListener {
    public static AdvanceSearchWindow self;
    ArrayList<TWComboItem> predefArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> providerSearchArray = new ArrayList<TWComboItem>();
    private JPanel mainPanel;
    private JPanel upperPanel;
    private JPanel lowerPanel;
    private JPanel criteriaPanel;
    private JPanel periodPanel;
    private int width = 600;
    private int height = 160;
    private TWButton searchBtn;
    private JRadioButton rbPreDef = new JRadioButton(Language.getString("PRE_DEFINED"));
    private JRadioButton rbCustom = new JRadioButton(Language.getString("CUSTOM_NEWS"));
    private ButtonGroup periodGroup = new ButtonGroup();
    private DateCombo fromDateCombo;
    private DateCombo toDateCombo;
    private TWComboModel preDefModel;
    private TWComboBox cmbPredef;
    private TWLabel symbolLabel, providerLabel, keywordLabel;
    private TWTextField keywordField;
    private TWTextField symbolField;
    private TWComboBox cmbProvider;
    private TWComboModel providerModel;
    private TWButton btnDownArraow;
    private boolean isconnected = true;
    private String exchange = "*";

    private AdvanceSearchWindow() {
        this.setSize(width, height);
        this.setTitle(Language.getString("NEWS_ADV_SEARCH"));
        this.setResizable(false);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        Client.getInstance().getDesktop().add(this);
        mainPanel = new JPanel();
        upperPanel = new JPanel();
        lowerPanel = new JPanel();
        criteriaPanel = new JPanel();
        periodPanel = new JPanel();
        searchBtn = new TWButton(Language.getString("SEARCH"));
        preDefModel = new TWComboModel(predefArray);
        cmbPredef = new TWComboBox(preDefModel);
        searchBtn.addActionListener(this);
        searchBtn.setActionCommand("SEARCH");
        providerModel = new TWComboModel(providerSearchArray);
        cmbProvider = new TWComboBox(providerModel);
        symbolLabel = new TWLabel(Language.getString("SYMBOL"));
        symbolLabel.setOpaque(true);
        providerLabel = new TWLabel(Language.getString("PROVIDER"));
        providerLabel.setOpaque(true);
        keywordLabel = new TWLabel(Language.getString("KEYWORD"));
        keywordLabel.setOpaque(true);
        keywordField = new TWTextField();
        periodGroup.add(rbPreDef);
        periodGroup.add(rbCustom);
        rbPreDef.addFocusListener(this);
        rbPreDef.setOpaque(true);
        rbPreDef.setSelected(true);
        rbCustom.addFocusListener(this);
        rbCustom.setOpaque(true);
        rbPreDef.addActionListener(this);
        rbCustom.addActionListener(this);
        createUI();
        Theme.registerComponent(this);

    }

    public static AdvanceSearchWindow getSharedInstance() {
        if (self == null) {
            self = new AdvanceSearchWindow();
        }
        return self;
    }

    private void createUI() {
        this.setLayout(new BorderLayout());
        this.add(mainPanel);
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "30"}, 0, 0));
        mainPanel.add(upperPanel);
        mainPanel.add(lowerPanel);
        upperPanel.setLayout(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 0, 0));
        upperPanel.add(createCriteriaPanel());
        upperPanel.add(createPeriodPanel());
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"100%", "100"}, new String[]{"100%"}, 3, 3));
        lowerPanel.add(new JLabel());
        lowerPanel.add(searchBtn);
        lowerPanel.registerKeyboardAction(this, "SEARCH", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        searchBtn.addKeyListener(this);
        populateDuration();
        populateProviders();


    }

    private JPanel createCriteriaPanel() {
        criteriaPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CRITERIA")));
//        criteriaPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CRITERIA")));
        String[] width = {"40%", "60%"};
        String[] height = {"33%", "33%", "34%"};
        criteriaPanel.setLayout(new FlexGridLayout(width, height, 2, 3));
        criteriaPanel.doLayout();
        criteriaPanel.add(symbolLabel);
        criteriaPanel.add(createSymbolPanel());
        criteriaPanel.add(keywordLabel);
        criteriaPanel.add(keywordField);
        criteriaPanel.add(providerLabel);
        criteriaPanel.add(cmbProvider);
        GUISettings.applyOrientation(criteriaPanel);
        return criteriaPanel;

    }

    private JPanel createPeriodPanel() {
        periodPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("PERIOD")));
        String[] dateWidths = {"30%", "70%"};
        String[] dateHeights = {"20"};
        fromDateCombo = new DateCombo(Client.getInstance().getFrame());
        fromDateCombo.addFocusListener(this);
        fromDateCombo.setEnable(false);
        JPanel fromPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 2));
        fromPanel.add(new TWLabel(Language.getString("FROM")));
        fromPanel.add(fromDateCombo);
        fromPanel.doLayout();
        toDateCombo = new DateCombo(Client.getInstance().getFrame());
        toDateCombo.addFocusListener(this);
        toDateCombo.setEnable(false);
        JPanel toPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 2));
        toPanel.add(new TWLabel(Language.getString("TO")));
        toPanel.add(toDateCombo);
        toPanel.doLayout();
        periodGroup.add(rbPreDef);
        periodGroup.add(rbCustom);
        String[] widthdPanel = {"40%", "60%"};
        String[] heightdPanel = {"20", "25", "25"};
        periodPanel.setLayout(new FlexGridLayout(widthdPanel, heightdPanel, 5, 2));
        periodPanel.add(rbPreDef);
        periodPanel.add(cmbPredef);
        periodPanel.add(rbCustom);
        periodPanel.add(fromPanel);
        periodPanel.add(new TWLabel());
        periodPanel.add(toPanel);
        GUISettings.applyOrientation(periodPanel);
        return periodPanel;
    }

    public void applyTheme() {

    }

    public void focusGained(FocusEvent e) {
        if (e.getSource() == fromDateCombo) {
            rbCustom.setSelected(true);
            rbPreDef.setSelected(false);
        } else if (e.getSource() == toDateCombo) {
            rbCustom.setSelected(true);
            rbPreDef.setSelected(false);
        } else if (e.getSource() == rbPreDef) {

        } else if (e.getSource() == rbCustom) {

        }
    }

    private void populateDuration() {
        predefArray.clear();
        predefArray.add(new TWComboItem(1, Language.getString("1_DAY")));
        predefArray.add(new TWComboItem(2, Language.getString("1_WEEK")));
        predefArray.add(new TWComboItem(3, Language.getString("1_MONTH")));
        predefArray.add(new TWComboItem(4, Language.getString("1_YEAR")));
        predefArray.add(new TWComboItem("*", Language.getString("LBL_ALL")));
        try {
            cmbPredef.setSelectedIndex(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void populateProviders() {
        providerSearchArray.clear();
        TWComboItem allItem = new TWComboItem("*", Language.getString("ALL"));
//        TWComboItem allItem2 = new TWComboItem("*", Language.getString("ALL"));
        providerSearchArray.add(allItem);
        try {
//            NewsProvidersStore.getSharedInstance().loadCommonNewsProvidersFile();
            Hashtable<String, String> providers = NewsProvidersStore.getSharedInstance().getProviderMap();
            Enumeration<String> providerObjects = providers.keys();
            while (providerObjects.hasMoreElements()) {
                String id = providerObjects.nextElement();
                NewsProvider dnp = NewsProvidersStore.getSharedInstance().getProvider(id);
                if (dnp != null) {
                    providerSearchArray.add(new TWComboItem(providers.get(id), dnp.getDescription()));
                }
            }
        } catch (Exception e) {
        }
//        Enumeration<String> providerObjects = NewsProvidersStore.getSharedInstance().getNewsProviders();
//        while (providerObjects.hasMoreElements()) {
//            String id = providerObjects.nextElement();
//            DetailedNewsProvider dnp = NewsProvidersStore.getSharedInstance().detailedProviders.get(id);
//            if (dnp != null) {
//                providerSearchArray.add(new TWComboItem(id, dnp.getDescription()));
//            }
//        }
//        providerObjects = null;
        Collections.sort(providerSearchArray);

        if (providerSearchArray.size() > 0) {
            cmbProvider.setSelectedIndex(0);
        }
        cmbProvider.updateUI();
    }

    private JPanel createSymbolPanel() {
        String[] widths = {"100%", "20"};
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights, 0, 2);

        JPanel panel = new JPanel(layout);
        symbolField = new TWTextField();
        symbolField.addFocusListener(this);
        panel.add(symbolField);
        btnDownArraow = new TWButton("...");
        btnDownArraow.addActionListener(this);
        btnDownArraow.setActionCommand("ARROW");
        symbolField.setEditable(true);
        panel.add(btnDownArraow);
        GUISettings.applyOrientation(panel);
        return panel;
    }

    public void focusLost(FocusEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(searchBtn)) {
            if (isConnected()) {
                NewsWindow.getSharedInstance().setSearchedDataStore();
                search(0);
                NewsWindow.getSharedInstance().setCancelActions();
                NewsWindow.getSharedInstance().setField(symbolField.getText(), keywordField.getText());
                this.setVisible(false);
                NewsStore.getSharedInstance().applySourceFilter();
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getSource().equals(btnDownArraow)) {
            searchSymbol();
        } else if (e.getActionCommand().equals("SEARCH")) {
            if (isConnected()) {
                NewsWindow.getSharedInstance().setSearchedDataStore();
                search(0);
                NewsWindow.getSharedInstance().setCancelActions();
                NewsWindow.getSharedInstance().setField(symbolField.getText(), keywordField.getText());
                this.setVisible(false);
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getSource().equals(rbPreDef)) {
            if (!cmbPredef.isEnabled()) {
                cmbPredef.setEnabled(true);
            }
            toDateCombo.setEnable(false);
            fromDateCombo.setEnable(false);
            toDateCombo.clearDate();
            fromDateCombo.clearDate();
        } else if (e.getSource().equals(rbCustom)) {
            if (cmbPredef.isEnabled()) {
                cmbPredef.setEnabled(false);
            }
            toDateCombo.setEnable(true);
            fromDateCombo.setEnable(true);
        }
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {

    }

    //---------------------------------------------

    public boolean isConnected() {
        return isconnected;
    }

    public void twConnected() {
        isconnected = true;
    }

    public void twDisconnected() {
        isconnected = false;
        SearchedNewsStore.getSharedInstance().setSearchInProgress(false);
        SearchedNewsStore.getSharedInstance().initSearch();
        Client.getInstance().setNewsSearchTitle(null);
        SearchedNewsStore.getSharedInstance().enableButtons();
        Client.getInstance().getDesktop().repaint();
    }

    private void searchSymbol() {
        try {
            Symbols symbols = new Symbols();
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();
            oCompanies.setTitle(getTitle());
            oCompanies.setSingleMode(true);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true);
            oCompanies = null;

            String key = symbols.getSymbols()[0];
            exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);

            symbolField.setText(symbol);
            symbols = null;
        } catch (Exception e) {
        }
    }

    public void search(long sequenceNo) {
        String providers = ((TWComboItem) cmbProvider.getSelectedItem()).getId();
        if ((rbPreDef.isSelected()) || (rbCustom.isSelected())) {

            String symbol = symbolField.getText();
            if ((symbol == null) || (symbol.equals(""))) {
                symbol = "*";
            } else if (symbol.contains("`")) {
                try {
                    String symbol2 = symbol.split("`")[0];
                    symbol = symbol2;
                } catch (Exception e) {
                    e.printStackTrace();
                    symbol = "*";
                }
            }

            String keyword = UnicodeUtils.getUnicodeString(keywordField.getText());
            if ((keyword == null) || (keyword.equals(""))) {
                keyword = "*";
            }

            String providerList = getProviderListForExchange(exchange);

            String fromDate = null, toDate = null;
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
            if (rbCustom.isSelected()) {
//                fromDate = fromDateCombo.getDateString();
//                toDate = toDateCombo.getDateString();
                ////////////////////////////////////////////////////////////////////////
//                SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMdd HH:MM:SS");
                SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                Date dt1 = new Date(Settings.getLocalTimeFor(fromDateCombo.getDateLong()));
                dt1.setHours(0);
                dt1.setMinutes(0);
                dt1.setSeconds(0);
                Date dt2 = new Date(Settings.getLocalTimeFor(toDateCombo.getDateLong()));
                dt2.setHours(23);
                dt2.setMinutes(59);
                dt2.setSeconds(999);

                String userTZ = System.getProperty("user.timezone");
                TimeZone tz = TimeZone.getTimeZone(userTZ);

//                System.out.println("Time zone Details : ");
//                System.out.println("Zone Name1 : "+tz.getDisplayName());
//                System.out.println("Zone Name2 : "+tz.getID());
//                System.out.println("Zone offset : "+dateformat2.format(tz.getOffset(dt.getTime())));
//                System.out.println("TIME NEWS : "+dt);
//                System.out.println("Test offset1 : "+dateformat2.format(dt.getTime()));
//                System.out.println("Test offset2 : "+dateformat2.format(dt.getTime()-tz.getOffset(dt.getTime())));

                fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                toDate = dateformat2.format(dt2.getTime() - tz.getOffset(dt2.getTime()));

//                Settings.getLocalTimeFor(toDateCombo.getDateLong());
//                Calendar cal = Calendar.getInstance();
                ////////////////////////////////////////////////////////////////////////

                if ((fromDate == null) || (toDate == null)) {
                    SharedMethods.showMessage(Language.getString("MSG_FILL_DATE_ENTRY"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } else {
                //setting pre defined time constants in request message
                int index1 = cmbPredef.getSelectedIndex();
                Calendar cal = Calendar.getInstance();
                if (index1 == 0) {
                    cal.add(Calendar.DAY_OF_MONTH, -1);
                    ///////////////////////////////////////////////////////////////////////

                    SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                    Date dt1 = new Date(Settings.getLocalTimeFor(cal.getTimeInMillis()));
                    dt1.setHours(0);
                    dt1.setMinutes(0);
                    dt1.setSeconds(0);
                    String userTZ = System.getProperty("user.timezone");
                    TimeZone tz = TimeZone.getTimeZone(userTZ);
                    fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                    ////////////////////////////////////////////////////
//                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 1) {
                    cal.add(Calendar.WEEK_OF_MONTH, -1);
                    ///////////////////////////////////////
                    SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                    Date dt1 = new Date(Settings.getLocalTimeFor(cal.getTimeInMillis()));
                    dt1.setHours(0);
                    dt1.setMinutes(0);
                    dt1.setSeconds(0);
                    String userTZ = System.getProperty("user.timezone");
                    TimeZone tz = TimeZone.getTimeZone(userTZ);
                    fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                    ///////////////////////////////////////
//                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 2) {
                    cal.add(Calendar.MONTH, -1);
                    ////////////////////////////////////////////
                    SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                    Date dt1 = new Date(Settings.getLocalTimeFor(cal.getTimeInMillis()));
                    dt1.setHours(0);
                    dt1.setMinutes(0);
                    dt1.setSeconds(0);
                    String userTZ = System.getProperty("user.timezone");
                    TimeZone tz = TimeZone.getTimeZone(userTZ);
                    fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                    ///////////////////////////////////////////
//                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 3) {
                    cal.add(Calendar.YEAR, -1);
                    ////////////////////////////////////////////////////////
                    SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                    Date dt1 = new Date(Settings.getLocalTimeFor(cal.getTimeInMillis()));
                    dt1.setHours(0);
                    dt1.setMinutes(0);
                    dt1.setSeconds(0);
                    String userTZ = System.getProperty("user.timezone");
                    TimeZone tz = TimeZone.getTimeZone(userTZ);
                    fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                    ///////////////////////////////////////////////////////
//                    fromDate = dateformat.format(cal.getTime());
                } else {
                    fromDate = "19700101";
                }

//                fromDate = "19700101";
                toDate = dateformat.format(new Date(System.currentTimeMillis()));
            }
            String searchString = Meta.NEWS_SEARCH_ALL + Meta.DS + exchange + Meta.FD + symbol + Meta.FD + providerList + Meta.FD + keyword + Meta.FD + fromDate + Meta.FD + toDate;
            String errString = Meta.NEWS_SEARCH_ALL + Meta.DS + Meta.FD + -1;
            System.out.println(searchString);
            SearchedNewsStore.getSharedInstance().initSearch();
            String contentIP = null;
            if (providers.equals("*")) {
                contentIP = Settings.getGlobalContentNewsIp();
            } else {

                try {
                    contentIP = NewsProvidersStore.getSharedInstance().getContentIp(providerList);
                } catch (Exception e) {
                    contentIP = Settings.getActiveIP();
                }
            }
            if (contentIP.equals("")) {
                contentIP = Settings.getActiveIP();
            }

            SearchedNewsStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), sequenceNo, exchange, contentIP, errString); //, path);
            SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
            SearchedNewsStore.getSharedInstance().disableButtons();
            NewsWindow.getSharedInstance().setDefaultValues();
            NewsWindow.getSharedInstance().getNewsTab().selectTab(1);
            NewsWindow.getSharedInstance().setLatest(false);
            Theme.unRegisterComponent(this);
        } else {
            SharedMethods.showMessage(Language.getString("MSG_FILL_DATE_ENTRY"), JOptionPane.ERROR_MESSAGE);
        }

    }

    public String getProviderListForExchange(String exchange) {
        String providers = ((TWComboItem) cmbProvider.getSelectedItem()).getId();
        try {
            if (exchange.equals("*")) {
                if (providers.equals("*")) {
                    String returnStr = "";
                    /*Enumeration<String> keys = NewsProvidersStore.getSharedInstance().getProviderKeys();
                    while (keys.hasMoreElements()) {
                        returnStr += keys.nextElement() + ",";
                    }*/
                    Enumeration<String> providerObjects = NewsProvidersStore.getSharedInstance().getProviderMap().elements();
                    while (providerObjects.hasMoreElements()) {
                        returnStr += providerObjects.nextElement() + ",";
                    }
                    return returnStr;
                } else {
                    return providers;
                }
            } else {
                if (providers.equals("*")) {
                    ArrayList<String> keys = NewsProvidersStore.getSharedInstance().getAllProvidersForExchange(exchange);
                    String returnStr = "";
                    for (String key : keys) {
                        returnStr += key + ",";
                    }
                    return returnStr;
                } else {
                    return providers;
                }
            }
        } catch (Exception e) {
            return providers;
        }
    }

    public void clearFields() {
        keywordField.setText("");
        symbolField.setText("");
    }

    private class TWLabel extends JLabel {
        public TWLabel(String name) {
            super(name);
            this.setOpaque(true);
        }

        private TWLabel(String text, int horizontalAlignment) {
            super(text, horizontalAlignment);
            this.setOpaque(true);
        }

        private TWLabel() {
            super();
            this.setOpaque(true);
        }
    }
}
