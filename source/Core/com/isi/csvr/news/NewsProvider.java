package com.isi.csvr.news;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 20-Dec-2007 Time: 10:43:49 To change this template use File | Settings
 * | File Templates.
 */
public class NewsProvider {
    public static byte DEFAULT_PATH = 2;
    private byte path = DEFAULT_PATH;
    private String id;
    private String description;
    private boolean isGlobal;
    private String contentIp = null;
    private boolean isSelected = false;
    private boolean isSelectionChanged = false;

    public NewsProvider(String id, String description, boolean global) {
        this.id = id;
        this.description = description;
        isGlobal = global;
    }

    public NewsProvider(String id) {
        this.id = id;
    }

    public String getID() {
        return this.id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isGlobal() {
        return isGlobal;
    }

    public void setGlobal(boolean global) {
        isGlobal = global;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        setSelectionChanged(this.isSelected != selected);
        isSelected = selected;
    }

    public boolean isSelectionChanged() {
        return isSelectionChanged;
    }

    public void setSelectionChanged(boolean selectionChanged) {
        isSelectionChanged = selectionChanged;
    }

    public byte getPath() {
        return path;
    }

    public void setPath(byte path) {
        this.path = path;
    }

    public String getContentIp() {
        return contentIp;
    }

    public void setContentIp(String contentIp) {
        this.contentIp = contentIp;
    }

    public String getRealID() {
        try {
            if (isGlobal) {
                String[] arr = this.id.split("\\.");
                return arr[0];
            } else {
                return id;
            }
        } catch (Exception e) {
            return this.id;
        }
    }

    public String getCountryCode() {
        try {
            if (isGlobal) {
                String[] arr = this.id.split("\\.");
                return arr[1];
            } else {
                return id;
            }
        } catch (Exception e) {
            return this.id;
        }
    }
}