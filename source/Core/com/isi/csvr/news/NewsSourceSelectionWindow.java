package com.isi.csvr.news;

import com.isi.csvr.Client;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 11-Jul-2008 Time: 10:27:15 To change this template use File | Settings
 * | File Templates.
 */
public class NewsSourceSelectionWindow extends JDialog implements ActionListener, Themeable {
    private static final int ALL_SOURCE = 1;
    private static final int ALL_WATCHLISTS = 2;
    private static final int DEFAULT = 1;
    public static NewsSourceSelectionWindow self;
    private static ImageIcon g_oExpandedIcon = null;
    private static Properties prop;
    private JPanel mainPanel;
    private JPanel lowerPanel;
    private TWButton applyBtn;
    private TWButton cancelBtn;
    private JScrollPane sourceScroll;
    private JScrollPane symbolScroll;
    private Hashtable<String, JCheckBox> checkBoxHash;
    private Hashtable<String, JCheckBox> myStockHash;
    private Hashtable<String, JCheckBox> classicHash;
    private Hashtable<String, JCheckBox> selectedHash;
    private TWTabbedPane tabPane;
    private JPanel sourcePanel;
    private JPanel symbolPanel;
    private JPanel sourceBtnPanel;
    private JPanel symbolBtnPanel;
    private JPanel sourceListPanel;
    private JPanel symbolListPanel;
    private JRadioButton allSource;
    private JRadioButton allWatchlists;
    private TWButton selectAllBtn;
    private TWButton unSelectAllBtn;
    private ButtonGroup symbolGroup;

    public NewsSourceSelectionWindow() {
        super(Client.getInstance().getFrame(), Language.getString("NEWS_FILTER"), true);
        this.setTitle(Language.getString("NEWS_FILTER"));
        this.setSize(350, 260);
        checkBoxHash = new Hashtable<String, JCheckBox>();
        myStockHash = new Hashtable<String, JCheckBox>();
        classicHash = new Hashtable<String, JCheckBox>();
        selectedHash = new Hashtable<String, JCheckBox>();
        createUI();
        init();
        setLocationRelativeTo(Client.getInstance().getFrame());
        Theme.registerComponent(this);
        this.setVisible(true);
    }

    public static NewsSourceSelectionWindow getSharedInstance() {
        if (self == null) {
            self = new NewsSourceSelectionWindow();
        }
        return self;
    }

    private void init() {
        loadSettings();

    }

    public void createUI() {

        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "25"}, 0, 0));
        this.add(createMainPanel());
        this.add(createLowerpanle());
    }

    private JPanel createMainPanel() {
        mainPanel = new JPanel();
        tabPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "nw");
        mainPanel.add(tabPane);
//        tabPane = new TWTabbedPane();
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        tabPane.addTab(Language.getString("SOURCES"), createSourceFilterPanel());
        tabPane.addTab(Language.getString("SYMBOLS"), createSymbolFilterPanel());
        tabPane.selectTab(0);
        return mainPanel;
    }

    private JPanel createSourceFilterPanel() {
        sourcePanel = new JPanel();
        sourcePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "100%"}, 0, 0));
        sourcePanel.add(createSourceBtnPanel());
        sourcePanel.add(createSourceListPanel());
        return sourcePanel;
    }

    private JPanel createSourceBtnPanel() {
        sourceBtnPanel = new JPanel();
        sourceBtnPanel.setLayout(new FlexGridLayout(new String[]{"10", "15", "100", "15", "15", "100", "100%"}, new String[]{"100%"}, 0, 2));
        selectAllBtn = new TWButton();
        unSelectAllBtn = new TWButton();
        selectAllBtn.addActionListener(this);
        unSelectAllBtn.addActionListener(this);
        sourceBtnPanel.add(new JLabel());
        sourceBtnPanel.add(selectAllBtn);
        sourceBtnPanel.add(new JLabel(Language.getString("BASKET_SELECT_ALL")));
        sourceBtnPanel.add(new JLabel());
        sourceBtnPanel.add(unSelectAllBtn);
        sourceBtnPanel.add(new JLabel(Language.getString("BASKET_UNSELECT_ALL")));
        sourceBtnPanel.add(new JLabel());
        return sourceBtnPanel;
    }

    private JScrollPane createSourceListPanel() {
        sourceListPanel = new JPanel();
        sourceScroll = new JScrollPane(sourceListPanel);
        try {
//            NewsProvidersStore.getSharedInstance().loadCommonNewsProvidersFile();
            sourceListPanel.setLayout(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
            /*Enumeration<String> providerObjects = NewsProvidersStore.getSharedInstance().getProviderKeys();
            while (providerObjects.hasMoreElements()) {
                String id = providerObjects.nextElement();
                NewsProvider dnp = NewsProvidersStore.getSharedInstance().getAllProviders().get(id);
                if (dnp != null) {
                    JCheckBox cb = new JCheckBox(dnp.getDescription());
                    sourceListPanel.add(cb);
                    checkBoxHash.put(dnp.getID(), cb);
                }
            }*/
            Hashtable<String, String> providers = NewsProvidersStore.getSharedInstance().getProviderMap();
            Enumeration<String> providerObjects = providers.keys();
            while (providerObjects.hasMoreElements()) {
                String id = providerObjects.nextElement();
                NewsProvider dnp = NewsProvidersStore.getSharedInstance().getProvider(id);
                if (dnp != null) {
                    JCheckBox cb = new JCheckBox(dnp.getDescription());
                    sourceListPanel.add(cb);
                    checkBoxHash.put(dnp.getID(), cb);
                }
            }
            sourceListPanel.updateUI();
            sourceScroll.updateUI();
            return sourceScroll;
        } catch (Exception e) {
            return sourceScroll;
        }
    }

    private JPanel createSymbolFilterPanel() {
        symbolPanel = new JPanel();
        symbolPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"42", "100%"}, 0, 0));
        symbolPanel.add(createSymbolBtnPanel());
        symbolPanel.add(createSymbolListPanel());
        return symbolPanel;
    }

    private JPanel createSymbolBtnPanel() {
        symbolBtnPanel = new JPanel();
        allSource = new JRadioButton(Language.getString("NEWS_FILTER_ALL_SOURCE"));
        allWatchlists = new JRadioButton(Language.getString("NEWS_FILTER_ALL_WATCHLISTS"));
        allSource.addActionListener(this);
        allWatchlists.addActionListener(this);
        symbolGroup = new ButtonGroup();
        symbolGroup.add(allSource);
        symbolGroup.add(allWatchlists);
        allSource.setSelected(true);
        symbolBtnPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20"}, 2, 2));
        symbolBtnPanel.add(allSource);
        symbolBtnPanel.add(allWatchlists);
        return symbolBtnPanel;
    }

    private JScrollPane createSymbolListPanel() {
        symbolListPanel = new JPanel();
        symbolScroll = new JScrollPane(symbolListPanel);
        symbolListPanel.setLayout(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        symbolListPanel.add(createWatchListPanel(new JLabel(Language.getString("MY_STOCKS"))));
        try {
            WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
            for (WatchListStore watchlist : watchlists) {
                JCheckBox item = new JCheckBox(watchlist.getCaption());
                if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                    symbolListPanel.add(createCheckBoxPanel(item));
                    myStockHash.put(watchlist.getId(), item);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
       /* if (isClassicViewsAvailable()) {
            symbolListPanel.add(createWatchListPanel(new JLabel(Language.getString("TREE_MY_MIST"))));
            try {
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist : watchlists) {
                    JCheckBox item = new JCheckBox(watchlist.getCaption());
                    if (watchlist.getListType() == WatchListStore.CLASSIC_LIST_TABLE_TYPE) {
                        symbolListPanel.add(createCheckBoxPanel(item));
                        classicHash.put(watchlist.getId(), item);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }*/

        return symbolScroll;
    }

    private JPanel createWatchListPanel(JLabel label) {
        JPanel panel = new JPanel();
        JLabel iconLbl = new JLabel();
        g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/newsfilterbullet.gif");
        panel.setLayout(new FlexGridLayout(new String[]{"20", "100%"}, new String[]{"20"}, 2, 1));
        iconLbl.setIcon(g_oExpandedIcon);
        panel.add(iconLbl);
        panel.add(label);

        return panel;

    }

    private JPanel createCheckBoxPanel(JCheckBox cBox) {
        JPanel panel = new JPanel();
        panel.setLayout(new FlexGridLayout(new String[]{"30", "100%"}, new String[]{"20"}, 0, 0));
        panel.add(new JLabel());
        panel.add(cBox);

        return panel;

    }


    public JPanel createLowerpanle() {
        lowerPanel = new JPanel();
        applyBtn = new TWButton(Language.getString("OK"));
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        applyBtn.addActionListener(this);
        cancelBtn.addActionListener(this);
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"100%", "75", "5", "75"}, new String[]{"100%"}, 2, 1));
        lowerPanel.add(new JLabel());
        lowerPanel.add(applyBtn);
        lowerPanel.add(new JLabel());
        lowerPanel.add(cancelBtn);

        return lowerPanel;

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(selectAllBtn)) {
            selectAll();
        } else if (e.getSource().equals(unSelectAllBtn)) {
            unSelectAll();
        } else if (e.getSource().equals(cancelBtn)) {
            this.dispose();
        } else if (e.getSource().equals(applyBtn)) {
            saveSettings();
            sendRequests();
//            sendAddRequests();
//            sendRemoveRequests();
            if (allWatchlists.isSelected()) {
                NewsStore.getSharedInstance().setSymbolFilter(getWatchListSymbols());

            } else {
                NewsStore.getSharedInstance().setSourceFilter(getFilteredSourceList());               //getFilteredSourceList()
            }
            this.dispose();
        } else if (e.getSource().equals(allSource)) {
            setAllSourceSelection();

        } else if (e.getSource().equals(allWatchlists)) {
            setAllWatchListSelection();

        }

    }

    private void selectAll() {
        try {
            Enumeration chkEnum = checkBoxHash.elements();
            while (chkEnum.hasMoreElements()) {
                JCheckBox jcb = (JCheckBox) chkEnum.nextElement();
                if (!jcb.isSelected()) {
                    jcb.setSelected(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void unSelectAll() {
        try {
            Enumeration chkEnum = checkBoxHash.elements();
            while (chkEnum.hasMoreElements()) {
                JCheckBox jcb = (JCheckBox) chkEnum.nextElement();
                if (jcb.isSelected()) {
                    jcb.setSelected(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void applyTheme() {
        g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
    }

    public void loadSettings() {
        try {
            setSavedNewsSources();
            setSavedSymbolMode();
            setSavedWatchLists();
            setSavedClassicViews();
            if (allWatchlists.isSelected()) {
                NewsStore.getSharedInstance().setSymbolFilter(getWatchListSymbols());
            } else {
                NewsStore.getSharedInstance().setSourceFilter(getFilteredSourceList());            //getFilteredSourceList()
            }
        } catch (Exception e) {
            System.out.println("Load Default Settings");
            selectAll();
            allSource.setSelected(true);
            NewsStore.getSharedInstance().setSourceFilter(getFilteredSourceList());            //getFilteredSourceList()
            saveSettings();
            sendRequests();
        }
    }

    private void setSavedNewsSources() {
        try {
            String[] sources = NewsProvidersStore.getSharedInstance().getProperty("SELECTED_SOURCES").split("\\,");
//			prop.getProperty("SELECTED_SOURCES").split(",");
            for (int i = 0; i < sources.length; i++) {
                JCheckBox cb = (JCheckBox) checkBoxHash.get(sources[i]);
                selectedHash.put(sources[i], cb);
                cb.setSelected(true);
            }
        } catch (Exception e) {
            selectAll();
            allSource.setSelected(true);
            NewsStore.getSharedInstance().setSourceFilter(getFilteredSourceList());            //getFilteredSourceList()
            saveSettings();
            sendRequests();
        }
    }

    private void setSavedSymbolMode() {
        try {
            String mode = NewsProvidersStore.getSharedInstance().getProperty("SELECTED_MODE");
            if (mode.equals("1")) {
                allSource.setSelected(true);
                setAllSourceSelection();
            } else {
                allWatchlists.setSelected(true);
                setAllWatchListSelection();
            }
        } catch (Exception e) {
            allSource.setSelected(true);
        }

    }

    private void setSavedWatchLists() {
        try {
            String[] sources = NewsProvidersStore.getSharedInstance().getProperty("SELECTED_MYSTOCKS").split(",");
            for (int i = 0; i < sources.length; i++) {
                JCheckBox cb = (JCheckBox) myStockHash.get(sources[i]);
                cb.setSelected(true);
            }

        } catch (Exception e) {

        }
    }

    private void setSavedClassicViews() {
        try {
            String[] sources = NewsProvidersStore.getSharedInstance().getProperty("SELECTED_CLASSIC_VIEWS").split(",");
//            String[] sources = prop.getProperty("SELECTED_CLASSIC_VIEWS").split(",");
            for (int i = 0; i < sources.length; i++) {
                JCheckBox cb = (JCheckBox) classicHash.get(sources[i]);
                cb.setSelected(true);
            }

        } catch (Exception e) {

        }
    }

    public void saveSettings() {
        NewsProvidersStore.getSharedInstance().putProperty("SELECTED_SOURCES", getSourceList());
        NewsProvidersStore.getSharedInstance().putProperty("SELECTED_MODE", getSymbolFilterMode());
        NewsProvidersStore.getSharedInstance().putProperty("SELECTED_MYSTOCKS", getMyStockList());
        NewsProvidersStore.getSharedInstance().putProperty("SELECTED_CLASSIC_VIEWS", getClassicList());
        NewsProvidersStore.getSharedInstance().applySettings();
        NewsProvidersStore.getSharedInstance().saveSettings();
        /*try {
            synchronized (prop) {
                FileOutputStream oOut = new FileOutputStream(Settings.getAbsolutepath()+"DataStore\\news.dll");
                prop.setProperty("SELECTED_SOURCES", getSourceList());
                prop.setProperty("SELECTED_MODE", getSymbolFilterMode());
                prop.setProperty("SELECTED_MYSTOCKS", getMyStockList());
                prop.setProperty("SELECTED_CLASSIC_VIEWS", getClassicList());
                prop.store(oOut, "");
                oOut.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private String getSymbolFilterMode() {
        if (allSource.isSelected()) {
            return "" + ALL_SOURCE;
        } else if (allWatchlists.isSelected()) {
            return "" + ALL_WATCHLISTS;
        } else {
            return "" + DEFAULT;
        }
    }

    private String getSourceList() {
        String list = "";
        try {
            Enumeration cbEnum = checkBoxHash.keys();
            while (cbEnum.hasMoreElements()) {
                String key = (String) cbEnum.nextElement();
                if (((JCheckBox) checkBoxHash.get(key)).isSelected()) {
                    list = list + "," + key;
                }

            }
            return list.substring(1);
        } catch (Exception e) {
            return "";
        }
    }

    private String getMyStockList() {
        String list = "";
        try {
            Enumeration cbEnum = myStockHash.keys();
            while (cbEnum.hasMoreElements()) {
                String key = (String) cbEnum.nextElement();
                if (((JCheckBox) myStockHash.get(key)).isSelected()) {
                    list = list + "," + key;
                }

            }
            return list.substring(1);
        } catch (Exception e) {
            return "";
        }
    }

    private String getClassicList() {
        try {
            String list = "";
            Enumeration cbEnum = classicHash.keys();
            while (cbEnum.hasMoreElements()) {
                String key = (String) cbEnum.nextElement();
                if (((JCheckBox) classicHash.get(key)).isSelected()) {
                    list = list + "," + key;
                }

            }
            return list.substring(1);
        } catch (Exception e) {
            return "";
        }
    }

    private void sendRequests() {
        Enumeration<NewsProvider> providers = NewsProvidersStore.getSharedInstance().getAllProviders();
        NewsProvider provider;
        while (providers.hasMoreElements()) {
            provider = providers.nextElement();
            if (provider.isSelectionChanged()) {
                /*if(provider.isSelected()){
					SendQFactory.addData(provider.getPath(), Meta.NEWS + Meta.DS + provider.getID() + Meta.FD + 1);
				} else {
					SendQFactory.addData(provider.getPath(), Meta.NEWS + Meta.DS + provider.getID() + Meta.FD + 0);
				}*/

                //modified following segment of code according to the request by jagath and jaminda
                //to over come sending news requests for both primary and secondary paths
                byte type = provider.getPath();
                if (provider.isSelected()) {
                    switch (type) {
                        case Constants.PATH_PRIMARY:
                            SendQFactory.addData(Constants.PATH_PRIMARY, Meta.NEWS + Meta.DS + provider.getID() + Meta.FD + 1);
                            break;
                        case Constants.PATH_SECONDARY:
                            SendQFactory.addData(Constants.PATH_SECONDARY, Meta.NEWS + Meta.DS + provider.getID() + Meta.FD + 1);
                            break;
                    }
                } else {
                    switch (type) {
                        case Constants.PATH_PRIMARY:
                            SendQFactory.addData(Constants.PATH_PRIMARY, Meta.NEWS + Meta.DS + provider.getID() + Meta.FD + 0);
                            break;
                        case Constants.PATH_SECONDARY:
                            SendQFactory.addData(Constants.PATH_SECONDARY, Meta.NEWS + Meta.DS + provider.getID() + Meta.FD + 0);
                            break;
                    }
                }
                provider.setSelectionChanged(false);
            }
        }
    }

    private void setAllSourceSelection() {
        Enumeration myStockEnum = myStockHash.elements();
        while (myStockEnum.hasMoreElements()) {
            JCheckBox jcb = (JCheckBox) myStockEnum.nextElement();
            jcb.setEnabled(false);
        }
        Enumeration classicEnum = classicHash.elements();
        while (classicEnum.hasMoreElements()) {
            JCheckBox jcb = (JCheckBox) classicEnum.nextElement();
            jcb.setEnabled(false);
        }
    }

    private void setAllWatchListSelection() {
        Enumeration myStockEnum = myStockHash.elements();
        while (myStockEnum.hasMoreElements()) {
            JCheckBox jcb = (JCheckBox) myStockEnum.nextElement();
            jcb.setEnabled(true);
        }
        Enumeration classicEnum = classicHash.elements();
        while (classicEnum.hasMoreElements()) {
            JCheckBox jcb = (JCheckBox) classicEnum.nextElement();
            jcb.setEnabled(true);
        }
    }

    private Hashtable getWatchListSymbols() {
        Hashtable symbolHash = new Hashtable();

        try {
            //My stocks
            Enumeration myStocks = myStockHash.keys();
            while (myStocks.hasMoreElements()) {
                String key = (String) myStocks.nextElement();
                JCheckBox jcb = (JCheckBox) myStockHash.get(key);
                if (jcb.isSelected()) {
                    WatchListStore store = WatchListManager.getInstance().getStore(key);
                    String[] symbolArr = store.getSymbols();
                    for (int i = 0; i < symbolArr.length; i++) {
                        symbolHash.put(SharedMethods.getSymbolFromKey(symbolArr[i]), SharedMethods.getSymbolFromKey(symbolArr[i]));
                    }
                }
            }
            //Classic Views
            Enumeration classic = classicHash.keys();
            while (classic.hasMoreElements()) {
                String key = (String) classic.nextElement();
                JCheckBox jcb = (JCheckBox) classicHash.get(key);
                if (jcb.isSelected()) {
                    WatchListStore store = WatchListManager.getInstance().getStore(key);
                    String[] symbolArr = store.getSymbols();
                    for (int i = 0; i < symbolArr.length; i++) {
                        symbolHash.put(SharedMethods.getSymbolFromKey(symbolArr[i]), SharedMethods.getSymbolFromKey(symbolArr[i]));
                    }
                }
            }
            return symbolHash;
        } catch (Exception e) {
            return new Hashtable();
        }
    }

    private boolean isClassicViewsAvailable() {
        boolean isClassicViewAvailable = false;
        try {
            WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
            for (WatchListStore watchlist : watchlists) {
                JCheckBox item = new JCheckBox(watchlist.getCaption());
                if (watchlist.getListType() == WatchListStore.CLASSIC_LIST_TABLE_TYPE) {
                    isClassicViewAvailable = true;
                }

            }
        } catch (Exception e) {
            isClassicViewAvailable = false;

        }
        return isClassicViewAvailable;

    }

    private Hashtable getFilteredSourceList() {
        Hashtable souHashtable = new Hashtable();
        try {
            Enumeration sourceEnum = checkBoxHash.keys();
            while (sourceEnum.hasMoreElements()) {
                String key = (String) sourceEnum.nextElement();
                JCheckBox jcb = (JCheckBox) checkBoxHash.get(key);
                if (jcb.isSelected()) {
                    souHashtable.put(key, key);
                }
            }
            return souHashtable;
        } catch (Exception e) {
            return new Hashtable();
        }
    }
}
