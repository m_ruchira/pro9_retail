package com.isi.csvr.news;

import com.isi.csvr.Client;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 12, 2005
 * Time: 10:14:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class NewsMouseListener extends MouseAdapter
        implements ListSelectionListener {


    public static final int ONLINE = 0;
    public static final int SEARCHED = 1;
    public static final int COMPANY = 2;

    private JTable table;
    private String message;
    private long time;
    private String title;
//    private NEWS_TYPES mode;

    public NewsMouseListener(JTable oTable, NEWS_TYPES mode) {
        this.table = oTable;
        //this.mode = mode;
    }

    /*public void setMode(NEWS_TYPES mode) {
        this.mode = mode;
    }*/

    static byte[] charToAscii(char[] c) throws Exception {

        Charset cs = Charset.forName("ASCII");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(bos, cs);
        //
        osw.write(c, 0, c.length);
        osw.flush();
        byte[] b = bos.toByteArray();
        return b;
    }

    /**
     * Called from the mouse listener
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            selected();
        } else if (SwingUtilities.isRightMouseButton(e)) {
//            NewsSourceSelectionWindow.getSharedInstance();
//            new NewsSourceSelectionWindow();
        }

    }

    /**
     * Called from the list selection listener
     */
    public void valueChanged(ListSelectionEvent e) {
        //selected();
    }

    public void selected() {
        int iRow = table.getSelectedRow();

        if (iRow >= 0) {
            // it is assumed that the  url is in the last column of the table
            message = (String) table.getModel().getValueAt(table.getSelectedRow(), -3);
            title = UnicodeUtils.getUnicodeString((String) table.getModel().getValueAt(table.getSelectedRow(), table.getModel().getColumnCount() - 1));

            try {
                time = Long.parseLong((String) table.getModel().getValueAt(table.getSelectedRow(), 0));
            } catch (Exception ex) {
                time = 0;
            }

            Thread thread = new Thread("NewsMouseListener-Select") {
                public void run() {
                    if ((message == null) || ((message.equals("")))) {
                        String id = (String) table.getModel().getValueAt(table.getSelectedRow(), -2);
                        String providerID = (String) table.getModel().getValueAt(table.getSelectedRow(), 2);

                        News news = null;
                        String language = null;
                        /*if (mode == NEWS_TYPES.ALL) {
                            news = NewsStore.getSharedInstance().getNews(id);
                            language = news.getLanguage();
                        } else if (mode == NEWS_TYPES.SEARCHED) {
                            news = SearchedNewsStore.getSharedInstance().getSearchedNews(id);
                            language = news.getLanguage();
                        }*/
                        news = NewsStore.getSharedInstance().getNews(id);
                        if (news == null) {
                            news = SearchedNewsStore.getSharedInstance().getSearchedNews(id);
                        }
                        if (news != null) {
                            language = news.getLanguage();
                            if (Settings.isConnected()) {
                                Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_NEWS);
//                                NewsProvider np = (NewsProvider)NewsProvidersStore.getSharedInstance().getProvider(providerID.toUpperCase());
                                NewsProvider np = NewsProvidersStore.getSharedInstance().getProvider(news.getDefaultCategory());
                                Enumeration<String> providers = NewsProvidersStore.getSharedInstance().getProviderKeys();
                                while (providers.hasMoreElements()) {
                                    String provider = providers.nextElement();
                                    if (news.isContainInCategory(provider)) {
                                        np = NewsProvidersStore.getSharedInstance().getProvider(provider);
                                        break;
                                    }
                                }
                                Client.getInstance().getBrowserFrame().setTitle(Language.getString("NEWS"));
//                                ServerSupportedNewsProvider ssnp = NewsProvidersStore.getSharedInstance().ssnProviders.get(getRealID(providerID));
                                if (np == null) {
                                    SendQFactory.addNewsBodyRequest(news.getExchange(), id, language, null, Constants.CONTENT_PATH_SECONDARY);  //, NewsProvidersStore.getSharedInstance().getProviderPath(news.getNewsProvider()));
                                } else {
                                    SendQFactory.addNewsBodyRequest(news.getExchange(), id, language, np.getContentIp(), np.getPath());  //, NewsProvidersStore.getSharedInstance().getProviderPath(news.getNewsProvider()));
                                }
                            }
                            news = null;
                        }
                    } else {
                        String id = (String) table.getModel().getValueAt(table.getSelectedRow(), -2);
                        News news = NewsStore.getSharedInstance().getNews(id);
                        if (news == null) {
                            news = SearchedNewsStore.getSharedInstance().getSearchedNews(id);
                            SearchedNewsStore.getSharedInstance().showNews(id);
                        } else {
                            NewsStore.getSharedInstance().showNews(id);
                        }
                        /*if (mode == NEWS_TYPES.ALL) {
                            NewsStore.getSharedInstance().showNews(id);
                        } else if (mode == NEWS_TYPES.SEARCHED) {
                            SearchedNewsStore.getSharedInstance().showNews(id);
                        }*/
                    }
                }
            };
            thread.start();
        }
    }

    public String getRealID(String provider) {
        try {
            String[] arr = provider.split("\\.");
            return arr[0];
        } catch (Exception e) {
            return provider;
        }
    }
}
