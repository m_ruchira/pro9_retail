package com.isi.csvr.news;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 14, 2003
 * Time: 11:53:21 AM
 * To change this template use Options | File Templates.
 */
public class SearchedNewsStore {

    private static SearchedNewsStore self = null;
    private DynamicArray store;
    private String selectedLanguage = null;
    private String lastSearchedLanguage;
    private DynamicArray searchIndex;
    private byte currentPage = 0;
    private String contentIP = null;
    private String searchString;
    private String errString;
    private String exchange;
    private TWButton btnNext;
    private TWButton btnPrev;
    private String lastSearchID = "";
    //    private TWButton btnReload;
    private TWButton btnSearch;
    //    private JRadioButton rbEnglish;
//    private JRadioButton rbArabic;
    private long lastSearchedID;
    private boolean haveMoreRecords;
    private boolean searchInProgess = false;
    private boolean newsSearchInProgess;


    private int startPosition = 0;
    private int endPosition = 0;
    private int totNewsCount = 0;


    private SearchedNewsStore() {
        store = new DynamicArray();
        searchIndex = new DynamicArray();
    }

    public static SearchedNewsStore getSharedInstance() {
        if (self == null) {
            self = new SearchedNewsStore();
        }
        return self;
    }

    public void setAllData(String data) {
        try {
            long start = 0;
            long end = 0;
            int record = 0;
            String[] records = data.split(Meta.FD);

            String ID = records[0];

            setSearchInProgress(false);
            setNewsSearchInProgress(false);
            NewsWindow.getSharedInstance().setBusyMode(false);
            NewsWindow.getSharedInstance().resetSearchBtn();
            NewsWindow.getSharedInstance().btnHide.setEnabled(true);

            if (ID.equals("-1")) {
                String err = Language.getString("NEWS_SEARCH_FAILED");
                err = err.replaceAll("\\[ERRCODE\\]", records[1].split(":")[1]);
                new ShowMessage(err, "E");
                return;
            }
            try {
                haveMoreRecords = records[0].split(Meta.ID)[1].equals("1");     //0
            } catch (Exception e) {
                haveMoreRecords = records[0].equals("1");
            }
            Client.getInstance().getDesktop().repaint();
            if (records.length > 1) {             //1
                for (int i = 1; i < records.length; i++) { //1
                    String[] info = records[i].split(Meta.ID);
                    News news = new News(UnicodeUtils.getNativeString(info[1]));
                    news.setAllData(info);
                    store.add(news);
                    if (record == 0) {
                        start = Long.parseLong(news.getNewsID());
                    }
                    end = Long.parseLong(news.getNewsID());
                    record++;
                }
            } else {
                new ShowMessage(Language.getString("NO_RESULTS_FOUND"), "I");
            }

            updateSearchIndex(start, end);
            enableButtons();
            String title = null;
            Client.getInstance().setNewsSearchTitle(title);

            String text = Language.getString("NEWS_COUNT").replaceFirst("\\[start\\]", "[" + startPosition + "]");
            text = text.replaceFirst("\\[end\\]", "[" + endPosition + "]");
            text = text.replaceFirst("\\[tot\\]", "[" + totNewsCount + "]");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public News getSearchedNews(String id) {
        for (int i = 0; i < store.size(); i++) {
            News news = (News) store.get(i);
            if (news.getNewsID().equals(id))
                return news;
            news = null;
        }
        return null;
    }

    public void showNews(String id) {
        News news = getSearchedNews(id);
        if (news != null) {
            NewsStore.getSharedInstance().showNews(news);
        }
    }

    public DynamicArray getStore() {
        return store;
    }

    public void clear() {
        store.clear();
    }

    public String getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public void setLastSearchedLanguage(String lastSearchedLanguage) {
        this.lastSearchedLanguage = lastSearchedLanguage;
    }

    private void updateSearchIndex(long start, long end) {
        if (searchIndex.size() - 1 >= currentPage) {
            return; // index is already there
        }

        SearchIndexRecord searchIndexRecord = new SearchIndexRecord();
        searchIndexRecord.start = start;
        searchIndexRecord.end = end;
        searchIndex.add(searchIndexRecord);
    }

    private long getNextID() {
        SearchIndexRecord searchIndexRecord = (SearchIndexRecord) searchIndex.get(currentPage);
        return searchIndexRecord.end;
    }

    private long getPreviousID() {
        SearchIndexRecord searchIndexRecord = (SearchIndexRecord) searchIndex.get(currentPage - 1);
        return searchIndexRecord.start + 1;
    }

    void doPreviuosSearch() {
        doSearch(searchString, getSelectedLanguage(), getPreviousID(), exchange, contentIP, errString);    //, path);
        currentPage--;
    }

    void doNextSearch() {
        doSearch(searchString, getSelectedLanguage(), getNextID(), exchange, contentIP, errString);    //, path);
        currentPage++;
    }

    public void doSearch(String searchString, String language, long id, String exchange, String contentIP, String errorString) {    // , byte path) {
        disableButtons();
        clear();
        lastSearchID = "NewsSearch:" + System.currentTimeMillis();
        this.contentIP = contentIP;
        this.exchange = exchange;
        this.searchString = searchString;
        this.lastSearchedID = id;
        this.errString = errorString;
        SearchedNewsStore.getSharedInstance().setSearchInProgress(true);
        Client.getInstance().setNewsSearchTitle(Language.getString("SEARCHING"));
//        SendQFactory.addData(Constants.PATH_PRIMARY, searchString + Meta.FD + id);
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, searchString + Meta.FD + id + Meta.FD + lastSearchID + Meta.EOL, exchange, contentIP, errorString);
        setLastSearchedLanguage(language);
    }

    public void initSearch() {
        searchIndex.clear();
        lastSearchID = "";
        currentPage = 0;
    }

    public void setSearchInProgress(boolean searchInProgress) {
        this.searchInProgess = searchInProgress;
    }

    public void setButtonReferences(TWButton btnNext, TWButton btnPrev, TWButton btnSearch) {
        this.btnPrev = btnPrev;
        this.btnNext = btnNext;
        this.btnSearch = btnSearch;
    }

    public void disableButtons() {
        try {
            btnPrev.setEnabled(false);
            btnNext.setEnabled(false);
//            btnSearch.setEnabled(false);
//            btnSearch.setText(Language.getString("CANCEL"));
//            btnSearch.setActionCommand("CANCEL");
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void enableButtons() {
        if (currentPage == 0) {
            btnPrev.setEnabled(false);
        } else {
            btnPrev.setEnabled(true);
        }

        if (haveMoreRecords) {
            btnNext.setEnabled(true);
        } else {
            btnNext.setEnabled(false);
        }
//        btnSearch.setEnabled(true);
//        btnSearch.setText(Language.getString("WINDOW_TITLE_SEARCH"));
//        btnSearch.setActionCommand("SEARCH");
    }

    public boolean enablePrev() {
        if (currentPage == 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean enableNext() {
        if (haveMoreRecords) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isNewsSearchInProgress() {
        return searchInProgess;
    }

    public void setNewsSearchInProgress(boolean searchInProgress) {
        this.newsSearchInProgess = searchInProgress;
    }

    class SearchIndexRecord {
        long start;
        long end;
    }

}
