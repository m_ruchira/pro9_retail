// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.news;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.TimeZone;

public class NewsModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private DynamicArray dataStore;
    private TimeZone currentZone;

    /**
     * Constructor
     */
    public NewsModel() {
        //init();
    }

    public DynamicArray getDataStore() {
        return dataStore;
    }

    public void setDataStore(DynamicArray newDataStore) {
        dataStore = newDataStore;
    }

    public void setTimeZone(TimeZone selectedZone) {
        currentZone = selectedZone;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {

        if (dataStore != null) {
//System.out.println(dataStore. size());
            return dataStore.size();
        } else {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            News news = (News) dataStore.get(iRow);

            switch (iCol) {
                case 0:
                    /* time zone chenged to selected exchanges'. As discussed with Manjula 25/Oct/2005.
                    Discussion on MSN
                    Manjula says:
                        let us take the exchange time displayed on top of TW
                    Manjula says:
                        WHEN THEY CHANGE IT CHANGE THE TIME
                    Manjula says:
                        most poeople will wathc their preferred market on top
                    Manjula says:
                        take the default time of that exchange
                    */
                    if (ExchangeStore.getSharedInstance().defaultCount() > 0) {
                        if (ExchangeStore.getSharedInstance().getSelectedExchange() != null) {
                            return "" + ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(
                                    ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), news.getNewsDate());
                        } else {
                            return "" + Settings.getLocalTimeFor(news.getNewsDate());
                        }
                    } else {
                        try {
                            return "" + Settings.getLocalTimeFor(news.getNewsDate());
                        } catch (Exception e) {

                            return "" + Settings.getLocalTimeFor(news.getNewsDate());
                        }
                    }

//                    return "" + news.getNewsDate();
                case 1:
                    return news.getSymbol();
                case 2:
                    return news.getNewsProvider();
                case 3:
                    return news.getSource();
                case 4:
                    return news.getHeadLine();
                case 5:
                    return news.getCategory();
                case 6:
                    return news.getShortDescription();
                case -7:
                    return news.getHeadLine();
                case -6:
                    return news.isHotNews();
                case -5:
                    return news.isTempararyNews();
                case -3:
                    return news.getBody();
                case -2:
                    return news.getNewsID();
                case -1:
                    return "" + news.isNewMessage();

            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("HOT_NEWS_COLOR"), FIELD_NEWS_HOT_NEWS, null, Theme.getColor("NEWS_HOTNEWS_FGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("TEMPORARY_NEWS_COLOR"), FIELD_NEWS_TEMPORARY_NEWS, Theme.getColor("NEWS_TEMPORARY_NEWS_BGCOLOR"), null);
        return customizerRecords;
    }

    public void windowClosing() {

    }
}