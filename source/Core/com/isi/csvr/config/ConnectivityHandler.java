package com.isi.csvr.config;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Dec 2, 2009
 * Time: 10:27:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConnectivityHandler extends ConnectivityFrame implements ActionListener, KeyListener, Runnable, WindowListener {
    public ConnectivityHandler(Frame owner) {
        super(owner);
        intialConnectionMode = Settings.getCommunicationType();
        setConnectioMode();
        setActionListeners();
        thread = new Thread(this, "Config");
        thread.start();
    }

    public ConnectivityHandler(Frame owner, String title, boolean model) {
        super(owner, title, model);
        intialConnectionMode = Settings.getCommunicationType();
        setConnectioMode();
        setActionListeners();
        thread = new Thread(this, "Config");
        thread.start();
    }

    private void setConnectioMode() {
        setConnectioMode(Settings.getCommunicationType());
    }

    private void setConnectioMode(int connectionMode) {
        if (connectionMode == Settings.COMMUNICATION_TYPE_TCP) {
            /*
              internet
             */
            if ((Settings.getItemFromBulk("DEFAULT_USER") != null) && (!Settings.getItemFromBulk("DEFAULT_USER").equals("")))
                txtUserName.setText(Settings.getItemFromBulk("DEFAULT_USER"));
            if ((Settings.getItemFromBulk("PASSWORD") != null) && (!Settings.getItemFromBulk("PASSWORD").equals(""))) {
                txtPassword.setText(Settings.getItemFromBulk("PASSWORD"));
            }
            if (TWControl.isSingleSingnOn()) {
                txtPassword.setEnabled(false);
                txtUserName.setEnabled(false);
                String strIps = "";
                try {
                    strIps = strIps + com.isi.csvr.trading.IPSettings.getSharedInstance().getSSOIP(0).getIP() + ";";
                } catch (Exception e) {
                }
                try {
                    strIps = strIps + com.isi.csvr.trading.IPSettings.getSharedInstance().getSSOIP(1).getIP();
                } catch (Exception e) {
                }
//                txtBrokerSerevrs.setText(com.isi.csvr.trading.IPSettings.getSharedInstance().getSSOIP(0).getIP()+ ";" +com.isi.csvr.trading.IPSettings.getSharedInstance().getSSOIP(1).getIP());
                if (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct) {
                    txtBrokerSerevrs.setText(strIps);
                }
            } else {
                txtUserName.setEnabled(true);
                txtPassword.setEnabled(true);
            }

            txtMarketServers.setText(IPSettings.getStaticIP(0) + ";" + IPSettings.getStaticIP(1));
            if (Settings.isWebProxyEnabled()) {
                chkWeb.setSelected(true);
                txtWebIP.setEnabled(true);
                txtWebPort.setEnabled(true);
                txtWebIP.setText(Settings.getItem("PROXY_WEB_IP"));
                txtWebPort.setText(Settings.getItem("PROXY_WEB_PORT"));
                Settings.setItem("USE_WEB_PROXY", "1");
                System.setProperty("http.proxyHost", Settings.getItem("PROXY_WEB_IP"));
                System.setProperty("http.proxyPort", Settings.getItem("PROXY_WEB_PORT"));
                System.setProperty("https.proxyHost", Settings.getItem("PROXY_WEB_IP"));
                System.setProperty("https.proxyPort", Settings.getItem("PROXY_WEB_PORT"));
            }
            if (Settings.isSocksProxyEnabled()) {
                chkSocks.setSelected(true);
                txtSocksIP.setEnabled(true);
                txtSocksPort.setEnabled(true);
                txtSocksIP.setText(Settings.getItem("PROXY_SOCKS_IP"));
                txtSocksPort.setText(Settings.getItem("PROXY_SOCKS_PORT"));
                Settings.setItem("USE_SOCKS_PROXY", "1");
                System.setProperty("socksProxyHost", txtSocksIP.getText().trim());
                System.setProperty("socksProxyPort", txtSocksPort.getText().trim());
            }
            Settings.setItem("CONNECTION_MODE", "" + connectionMode);
            Settings.setCommunicationType(Settings.COMMUNICATION_TYPE_TCP);
        }
    }

    private void setActionListeners() {
        btnOk.addActionListener(this);
        btnCancel.addActionListener(this);
        chkSocks.addActionListener(this);
        chkWeb.addActionListener(this);
    }

    private boolean doValidatePort(String sPort, String sMessage) {
        try {
            Integer.parseInt(sPort);
            return true;
        } catch (Exception e) {
            showMessage(sMessage);
            return false;
        }
    }

    private boolean doValidateIP(String sIP, String sMessage) {
        if (isOkPressed()) {
            return true;
        }
        return false;

    }

    public void showMessage(String sMsg) {
        if (isOkPressed()) {
            this.setVisible(false);
            ShowMessage oMsg = new ShowMessage(false, sMsg, "E");
            oMsg = null;
            this.setVisible(true);
        }

    }

    private void okPressed() {
        setOkPressed(true);

        if (Settings.isTCPMode()) {
            IPSettings.clearStaticIPList();
            String[] ips = txtMarketServers.getText().split(";");
            if (ips.length == 1) {
                IPSettings.addIP(ips[0].trim(), IP.STATIC, IP.PRIMARY);
            } else if (ips.length == 2) {
                IPSettings.addIP(ips[0].trim(), IP.STATIC, IP.PRIMARY);
                IPSettings.addIP(ips[1].trim(), IP.STATIC, IP.SECONDARAY);
            }
            Settings.setItemInBulk("DEFAULT_USER", txtUserName.getText().trim());
            Settings.setItemInBulk("PASSWORD", new String(txtPassword.getPassword()).trim());
            Settings.setUserDetails(txtUserName.getText().trim(), new String(txtPassword.getPassword()).trim());

            if (chkSocks.isSelected()) {
                if ((doValidateIP(txtSocksIP.getText().trim(), Language.getString("MSG_INVALID_SOCKS_PROXY_IP"))) && (doValidatePort(txtSocksPort.getText().trim(), Language.getString("MSG_INVALID_SOCKS_PROXY_PORT")))) {
                    Settings.setItem("PROXY_SOCKS_IP", txtSocksIP.getText().trim());
                    Settings.setItem("PROXY_SOCKS_PORT", txtSocksPort.getText().trim());

                    Settings.setItem("USE_SOCKS_PROXY", "1");
                    System.setProperty("socksProxyHost", txtSocksIP.getText().trim());
                    System.setProperty("socksProxyPort", txtSocksPort.getText().trim());
                }

            } else {
                Settings.setItem("USE_SOCKS_PROXY", "0");
                System.setProperty("socksProxyHost", "");
                System.setProperty("socksProxyPort", "");
            }
            if (chkWeb.isSelected()) {
                if ((doValidateIP(txtWebIP.getText().trim(), Language.getString("MSG_INVALID_SOCKS_PROXY_IP"))) && (doValidatePort(txtWebPort.getText().trim(), Language.getString("MSG_INVALID_SOCKS_PROXY_PORT")))) {
                    Settings.setItem("PROXY_WEB_IP", txtWebIP.getText().trim());
                    Settings.setItem("PROXY_WEB_PORT", txtWebPort.getText().trim());

                    Settings.setItem("USE_WEB_PROXY", "1");
                    System.setProperty("http.proxyHost", txtWebIP.getText().trim());
                    System.setProperty("http.proxyPort", txtWebPort.getText().trim());
                    System.setProperty("https.proxyHost", txtWebIP.getText().trim());
                    System.setProperty("https.proxyPort", txtWebPort.getText().trim());
                }
            } else {
                Settings.setItem("USE_WEB_PROXY", "0");
                System.setProperty("http.proxyHost", "");
                System.setProperty("http.proxyPort", "");
                System.setProperty("https.proxyHost", "");
                System.setProperty("https.proxyPort", "");
            }

            /* commented here and added to the previous validations itself
            if (chkSocks.isSelected()) {
                Settings.setItem("USE_SOCKS_PROXY", "1");
                System.setProperty("socksProxyHost", txtSocksIP.getText().trim());
                System.setProperty("socksProxyPort", txtSocksPort.getText().trim());
            } else {
                Settings.setItem("USE_SOCKS_PROXY", "0");
                System.setProperty("socksProxyHost", "");
                System.setProperty("socksProxyPort", "");
            }
            if (chkWeb.isSelected()) {
                Settings.setItem("USE_WEB_PROXY", "1");
                System.setProperty("http.proxyHost", txtWebIP.getText().trim());
                System.setProperty("http.proxyPort", txtWebPort.getText().trim());
                System.setProperty("https.proxyHost", txtWebIP.getText().trim());
                System.setProperty("https.proxyPort", txtWebPort.getText().trim());

            } else {
                Settings.setItem("USE_WEB_PROXY", "0");
                System.setProperty("http.proxyHost", "");
                System.setProperty("http.proxyPort", "");
                System.setProperty("https.proxyHost", "");
                System.setProperty("https.proxyPort", "");
            }*/

            //saving changes to tip.dll file
            com.isi.csvr.trading.IPSettings.getSharedInstance().clearSSOIPs();
            String[] tradeIps = txtBrokerSerevrs.getText().split(";");
            if (tradeIps.length == 1) {
                com.isi.csvr.trading.IPSettings.getSharedInstance().addSSOIP(tradeIps[0].trim(), IP.STATIC, IP.PRIMARY);
            } else if (tradeIps.length == 2) {
                com.isi.csvr.trading.IPSettings.getSharedInstance().addSSOIP(tradeIps[0].trim(), IP.STATIC, IP.PRIMARY);
                com.isi.csvr.trading.IPSettings.getSharedInstance().addSSOIP(tradeIps[1].trim(), IP.STATIC, IP.SECONDARAY);
            }

            com.isi.csvr.trading.IPSettings.getSharedInstance().save();
            setOkPressed(true);
            setVisible(false);
            this.dispose();
        }

        if (intialConnectionMode != Settings.getCommunicationType()) {
            SharedMethods.showMessage(Language.getString("MSG_CONNECTION_MODE_CHANGED"),
                    JOptionPane.WARNING_MESSAGE);
        }

    }

    private void cancelPressed() {
        setOkPressed(false);
        setVisible(false);
        this.dispose();
    }

    public boolean isOkPressed() {
        return okPressed;
    }

    public void setOkPressed(boolean okPressed) {
        this.okPressed = okPressed;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnOk)) {
            okPressed();
        }
        if (e.getSource().equals(chkSocks)) {
            if (chkSocks.isSelected()) {
                txtSocksPort.setEnabled(true);
                txtSocksIP.setEnabled(true);
            } else {
                txtSocksPort.setEnabled(false);
                txtSocksIP.setEnabled(false);
            }
        }
        if (e.getSource().equals(chkWeb)) {
            if (chkWeb.isSelected()) {
                txtWebIP.setEnabled(true);
                txtWebPort.setEnabled(true);
            } else {
                txtWebIP.setEnabled(false);
                txtWebPort.setEnabled(false);
            }
        }
        if (e.getSource().equals(btnCancel)) {
            cancelPressed();
        }

    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        if ((e.getKeyCode() == KeyEvent.VK_ENTER)) {
            if (e.getSource() == btnCancel) {
                cancelPressed();
            } else {
                okPressed();
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void run() {
        if (Settings.isSatelliteMode()) // not allowed in Satellite mode
            return;

        int index = 6;
        while (active) {
            index++;
            if (index > 5) {
                index = 1;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void windowOpened(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowClosed(WindowEvent e) {
        active = false;
    }

    public void windowClosing(WindowEvent e) {
        active = false;
    }

    public void windowIconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeiconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowActivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeactivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}


