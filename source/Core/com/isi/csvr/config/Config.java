package com.isi.csvr.config;

// Copyright (c) 2000 Home

import com.isi.csvr.ShowMessage;
import com.isi.csvr.communication.udp.SatelliteIPSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.Socket;

/**
 * A Swing-based dialog class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class Config extends JDialog
        implements ActionListener, KeyListener, Runnable, WindowListener {

    JTextField txtIP;
    JTextField txtIP2;
    JTextField txtUser;
    TWPasswordField txtPass;
    TWButton btnOk;
    TWButton btnCancel;
    JCheckBox chkSOCKSIP;
    //    JCheckBox chkDisableAcceleration;
    JTextField txtProxySOCKSIP;
    JTextField txtProxySOCKSPort;
    boolean g_bProxyChanged = false;
    boolean okPressed = false;
    boolean active = true;
    boolean connected = false;
    boolean backupPortConnected = false;
    Thread thread;
    JRadioButton leftPopup;
    JRadioButton rightPopup;
    JCheckBox dualScreen;
    JTextField txtTradeIP;
    JTextField txtTradeIP2;
    JTextField txtTradeUser;
    private JLabel primaryBulb;
    private JLabel backupBulb;
    private boolean prevHWAcc;
    private JLabel lblIP;
    private JLabel lblIP2;
    private JLabel lblTradeIP;
    private JLabel lblTradeIP2;
    private JRadioButton rbInternet;
    private JRadioButton rbSatellite;
    private int intialConnectionMode;
    private TWTabbedPane userTab;
    private TWComboBox scrollingCombo;
    private String[] scrollDataArray = {Language.getString("1_SEC"), Language.getString("2_SEC"),
            Language.getString("5_SEC"), Language.getString("AUTO_SCROLL_10_SEC"),
            Language.getString("AUTO_SCROLL_15_SEC")};

    /**
     * Constructs a new instance.
     *
     * @param parent
     * @param title
     * @param modal
     */
    public Config(Frame parent, String title, boolean modal) {
        super(parent, title, modal);
        try {
            intialConnectionMode = Settings.getCommunicationType();
            loadGUI();
            setConnectioMode();
            thread = new Thread(this, "Config");
            thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */
    public Config() {
        this(null, "", false);
    }

    public void loadGUI() {
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER, 0, 0);
        ColumnLayout columnLayout = new ColumnLayout(ColumnLayout.MODE_SAME_SIZE);

        this.getContentPane().setLayout(flowLayout);


        JPanel mainPanel = new JPanel(columnLayout);
        ((JPanel) getContentPane()).setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        this.getContentPane().add(mainPanel);

//        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER,10,10);
//        this.getContentPane().setLayout(flowLayout);
//        this.getContentPane().setLayout(new BorderLayout());

        //ConnectionPanel
        JPanel connectionPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        connectionPanel.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Theme.getColor("COMPONENT_BORDER_COLOR")));
//        connectionPanel.setBorder(BorderFactory.createEtchedBorder(0));
        //change start
        connectionPanel.setPreferredSize(new Dimension(300, 80));
//        this.getContentPane().add(connectionPanel);

        JLabel lblConnectionMode = new JLabel(Language.getString("CONNECTION_MODE"));
        lblConnectionMode.setPreferredSize(new Dimension(200, 20));
        connectionPanel.add(lblConnectionMode);

        rbInternet = new JRadioButton(Language.getString("CONNECTION_MODE_INTERNET"));
        rbInternet.setPreferredSize(new Dimension(200, 20));
        rbInternet.addActionListener(this);
        rbInternet.setActionCommand("INTERNET");
        connectionPanel.add(rbInternet);

        rbSatellite = new JRadioButton(Language.getString("CONNECTION_MODE_SATELLITE"));
        rbSatellite.setPreferredSize(new Dimension(200, 20));
        rbSatellite.addActionListener(this);
        rbSatellite.setActionCommand("SATELLITE");
        connectionPanel.add(rbSatellite);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(rbSatellite);
        buttonGroup.add(rbInternet);

        // User Panel
        // adding new panel to add trading details to config dialog
        userTab = new TWTabbedPane();
        txtTradeIP = new JTextField();
        txtTradeIP2 = new JTextField();
        txtTradeUser = new JTextField();
        /// JPanel userPanelPrice = new JPanel(new FlowLayout(FlowLayout.LEADING)); //new FlexGridLayout(new String[]{"150","120"}, new  String[]{"20","20","20","20","20"},2,2)); //new FlowLayout(FlowLayout.LEADING));
        /// JPanel userPanelTrade = new JPanel(new FlowLayout(FlowLayout.LEADING)); //new FlexGridLayout(new String[]{"150","120"}, new  String[]{"20","20","20","20","20"},2,2)); //new FlowLayout(FlowLayout.LEADING));

        JPanel userPanelPrice = new JPanel(new FlexGridLayout(new String[]{"5", "115", "15", "155", "8"}, new String[]{"20", "20", "20", "20"}, 0, 10)); //new FlexGridLayout(new String[]{"150","120"}, new  String[]{"20","20","20","20","20"},2,2)); //new FlowLayout(FlowLayout.LEADING));
        JPanel userPanelTrade = new JPanel(new FlexGridLayout(new String[]{"5", "115", "15", "155", "8"}, new String[]{"20", "20", "20", "20"}, 0, 10)); //new FlexGridLayout(new String[]{"150","120"}, new  String[]{"20","20","20","20","20"},2,2)); //new FlowLayout(FlowLayout.LEADING));

//        userPanel.setBorder(BorderFactory.createEtchedBorder(0));
        userPanelPrice.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Theme.getColor("COMPONENT_BORDER_COLOR")));
        userPanelTrade.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Theme.getColor("COMPONENT_BORDER_COLOR")));
        //userPanel.setPreferredSize(new Dimension(270, 110));
        //change start
        ///userPanelPrice.setPreferredSize(new Dimension(300, 110));
        ///userPanelTrade.setPreferredSize(new Dimension(300, 110));
        if (TWControl.isSingleSingnOn()) {
            ///this.getContentPane().add(userTab);
            mainPanel.add(userTab);
            userTab.addTab(Language.getString("PRICE_TAB"), userPanelPrice);
            userTab.addTab(Language.getString("TRADE_TAB"), userPanelTrade);
            userTab.setSelectedIndex(0);
            mainPanel.setPreferredSize(new Dimension(300, 426));
        } else {
            ///this.getContentPane().add(userPanelPrice);
            mainPanel.add(userPanelPrice);
            mainPanel.setPreferredSize(new Dimension(300, 402));
        }
//        this.getContentPane().add(userPanel, BorderLayout.NORTH);

        lblIP = new JLabel(Language.getString("IP"));
        lblIP.setPreferredSize(new Dimension(150, 20));
        userPanelPrice.add(new JLabel());
        userPanelPrice.add(lblIP);
        userPanelPrice.add(new JLabel());


        lblTradeIP = new JLabel(Language.getString("IP"));
        lblTradeIP.setPreferredSize(new Dimension(150, 20));
        userPanelTrade.add(new JLabel());
        userPanelTrade.add(lblTradeIP);
        userPanelTrade.add(new JLabel());


        txtIP = new JTextField();
        txtIP.addKeyListener(this);
        try {
            txtIP.setText(IPSettings.getStaticIP(0));
        } catch (Exception e) {
            txtIP.setText("");
        }
        //change start
        ///  txtIP.setPreferredSize(new Dimension(120, 20));

        userPanelPrice.add(txtIP);
        userPanelPrice.add(new JLabel());
        txtTradeIP = new JTextField();
        txtTradeIP.addKeyListener(this);
        try {
            txtTradeIP.setText(com.isi.csvr.trading.IPSettings.getSharedInstance().getSSOIP(0).getIP());
        } catch (Exception e) {
            txtTradeIP.setText("");
        }
        //change start
        txtTradeIP.setPreferredSize(new Dimension(120, 20));
        userPanelTrade.add(txtTradeIP);
        userPanelTrade.add(new JLabel());


        lblIP2 = new JLabel(Language.getString("IP2"));
        /// lblIP2.setPreferredSize(new Dimension(150, 20));
        userPanelPrice.add(new JLabel());
        userPanelPrice.add(lblIP2);
        userPanelPrice.add(new JLabel());

        lblTradeIP2 = new JLabel(Language.getString("IP2"));
        lblTradeIP2.setPreferredSize(new Dimension(150, 20));
        userPanelTrade.add(new JLabel());
        userPanelTrade.add(lblTradeIP2);
        userPanelTrade.add(new JLabel());


        txtIP2 = new JTextField();
        txtIP2.addKeyListener(this);
        try {
            txtIP2.setText(IPSettings.getStaticIP(1));
        } catch (Exception e) {
            txtIP2.setText("");
        }
        //change start
        txtIP2.setPreferredSize(new Dimension(120, 20));

        userPanelPrice.add(txtIP2);
        userPanelPrice.add(new JLabel());
        txtTradeIP2 = new JTextField();
        txtTradeIP2.addKeyListener(this);
        try {
            txtTradeIP2.setText(com.isi.csvr.trading.IPSettings.getSharedInstance().getSSOIP(1).getIP());
        } catch (Exception e) {
            txtTradeIP2.setText("");
        }
        //change start
        txtTradeIP2.setPreferredSize(new Dimension(120, 20));
        userPanelTrade.add(txtTradeIP2);
        userPanelTrade.add(new JLabel());

        JLabel lblUser = new JLabel(Language.getString("DEFAULT_USER"));
        lblUser.setPreferredSize(new Dimension(150, 20));
        userPanelPrice.add(new JLabel());
        userPanelPrice.add(lblUser);
        userPanelPrice.add(new JLabel());


        JLabel lblTradeUser = new JLabel(Language.getString("DEFAULT_USER"));
        lblTradeUser.setPreferredSize(new Dimension(150, 20));
        userPanelTrade.add(new JLabel());
        userPanelTrade.add(lblTradeUser);
        userPanelTrade.add(new JLabel());


        txtUser = new JTextField();
        txtUser.addKeyListener(this);
        //change start
        ///     txtUser.setPreferredSize(new Dimension(120, 20));
        userPanelPrice.add(txtUser);
        userPanelPrice.add(new JLabel());

        txtTradeUser = new JTextField();
        txtTradeUser.addKeyListener(this);
        if (Settings.getItemFromBulk("DEFAULT_USER") != null)
            txtUser.setText(Settings.getItemFromBulk("DEFAULT_USER"));
        if (Settings.getItem("DEFAULT_USER") != null)
            txtTradeUser.setText(Settings.getTradeUser());
        if (Settings.getItemFromBulk("PASSWORD") != null)
            txtPass.setText(Settings.getItemFromBulk("PASSWORD"));
        //change start
        ///     txtTradeUser.setPreferredSize(new Dimension(120, 20));
        userPanelTrade.add(txtTradeUser);
        userPanelTrade.add(new JLabel());

        JLabel lblPass = new JLabel(Language.getString("PASSWORD"));
        ///  lblPass.setPreferredSize(new Dimension(150, 20));
        userPanelPrice.add(new JLabel());
        userPanelPrice.add(lblPass);
        userPanelPrice.add(new JLabel());

        txtPass = new TWPasswordField(this, false, false);
        txtPass.addKeyListener(this);
        txtPass.setUsernameFiled(txtUser);
        //change start
        ///   txtPass.setPreferredSize(new Dimension(120, 20));
        userPanelPrice.add(txtPass);
        userPanelPrice.add(new JLabel());

        // Create the PROXY Panel
        /// JPanel proxyPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        JPanel proxyPanel = new JPanel(new FlexGridLayout(new String[]{"5", "115", "15", "155", "8"}, new String[]{"20", "20", "20", "20"}, 0, 10));
        proxyPanel.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Theme.getColor("COMPONENT_BORDER_COLOR")));
        //proxyPanel.setPreferredSize(new Dimension(270, 120)); //120));
        //change start
        //      proxyPanel.setPreferredSize(new Dimension(300, 120)); //120));
        /// this.getContentPane().add(proxyPanel);
        mainPanel.add(proxyPanel);
//        this.getContentPane().add(proxyPanel, BorderLayout.CENTER);

        JLabel lblProxy = new JLabel(Language.getString("SOCKS_PROXY"));
        lblProxy.setPreferredSize(new Dimension(150, 20));
        proxyPanel.add(new JLabel());
        proxyPanel.add(lblProxy);
        proxyPanel.add(new JLabel());

        chkSOCKSIP = new JCheckBox();
        chkSOCKSIP.addActionListener(this);
        chkSOCKSIP.setSelected(true);
        chkSOCKSIP.setActionCommand("P");
        proxyPanel.add(chkSOCKSIP);
        proxyPanel.add(new JLabel());

        JLabel lblProxyIP = new JLabel(Language.getString("PROXY_SOCKS_IP"));
        lblProxyIP.setPreferredSize(new Dimension(150, 20));
        proxyPanel.add(new JLabel());
        proxyPanel.add(lblProxyIP);
        proxyPanel.add(new JLabel());

        txtProxySOCKSIP = new JTextField();
        txtProxySOCKSIP.addKeyListener(this);
        if (Settings.getItem("PROXY_SOCKS_IP") != null)
            txtProxySOCKSIP.setText(Settings.getItem("PROXY_SOCKS_IP"));
        //change start
        txtProxySOCKSIP.setPreferredSize(new Dimension(120, 20));
        proxyPanel.add(txtProxySOCKSIP);
        proxyPanel.add(new JLabel());

        JLabel lblProxySOCKSPort = new JLabel(Language.getString("PROXY_SOCKS_PORT"));
        lblProxySOCKSPort.setPreferredSize(new Dimension(150, 20));
        proxyPanel.add(new JLabel());
        proxyPanel.add(lblProxySOCKSPort);
        proxyPanel.add(new JLabel());

        txtProxySOCKSPort = new JTextField();
        txtProxySOCKSPort.addKeyListener(this);
        if (Settings.getItem("PROXY_SOCKS_PORT") != null)
            txtProxySOCKSPort.setText(Settings.getItem("PROXY_SOCKS_PORT"));
        //change start
        txtProxySOCKSPort.setPreferredSize(new Dimension(120, 20));
        proxyPanel.add(txtProxySOCKSPort);
        proxyPanel.add(new JLabel());

        JLabel lblNetworkStatus = new JLabel(Language.getString("CONNECTION_STATUS"));
        lblNetworkStatus.setPreferredSize(new Dimension(150, 20));
        proxyPanel.add(new JLabel());
        proxyPanel.add(lblNetworkStatus);
        proxyPanel.add(new JLabel());

        JPanel networkStatusPanel = getBulbPanel();
        networkStatusPanel.setPreferredSize(new Dimension(100, 25));
        proxyPanel.add(networkStatusPanel);
        proxyPanel.add(new JLabel());

        JPanel bottomPanel = new JPanel();
        BoxLayout bottomGrid = new BoxLayout(bottomPanel, BoxLayout.Y_AXIS);
        bottomPanel.setLayout(bottomGrid);

//        this.getContentPane().add(bottomPanel, BorderLayout.SOUTH);

        // CONFIG PANEL
        JPanel configPanel = new JPanel(new FlexGridLayout(new String[]{"5", "115", "15", "155", "8"}, new String[]{"20", "20", "20", "20"}, 0, 10));
//        JPanel configPanel = new JPanel(new FlexGridLayout(new String[]{"5", "115", "15", "155", "8"}, new String[]{"20", "20", "20", "20"}, 0, 10));
//        JPanel configPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 10));
        configPanel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Theme.getColor("COMPONENT_BORDER_COLOR")));
        //configPanel.setPreferredSize(new Dimension(270, 35));
        //change start
        configPanel.setPreferredSize(new Dimension(300, 40));
        bottomPanel.add(configPanel);

        JLabel lblAutoScroll = new JLabel(Language.getString("AUTO_SCROLL_INTERVAL"));
        lblAutoScroll.setPreferredSize(new Dimension(115, 20));     //129
        scrollingCombo = new TWComboBox(scrollDataArray);
        scrollingCombo.setPreferredSize(new Dimension(155, 20));

        configPanel.add(new JLabel());
        configPanel.add(lblAutoScroll);
        configPanel.add(new JLabel());
        configPanel.add(scrollingCombo);
        configPanel.add(new JLabel());
        scrollingCombo.updateUI();
        //Dual Screen mode panel

//        if (Settings.isMultiScreenEnvr()){
        JPanel screenPanel = new JPanel();
        screenPanel.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Theme.getColor("COMPONENT_BORDER_COLOR")));
        screenPanel.setLayout(/*new ColumnLayout(ColumnLayout.MODE_SAME_SIZE)*/new FlexGridLayout(new String[]{"5", "290"}, new String[]{"16", "16", "16"}, 0, 5));
        dualScreen = new JCheckBox(Language.getString("DUAL_SCREEN_MODE"));
        dualScreen.setPreferredSize(new Dimension(260, 16));
        dualScreen.addActionListener(this);
        dualScreen.setActionCommand("DUAL");
        screenPanel.add(new JLabel());
        screenPanel.add(dualScreen);
        leftPopup = new JRadioButton(Language.getString("LEFT_SCREEN_POPUP"));
        leftPopup.addActionListener(this);
        leftPopup.setActionCommand("LEFT");
        leftPopup.setSelected(true);
        leftPopup.setPreferredSize(new Dimension(230, 16));

        screenPanel.add(new JLabel());
        screenPanel.add(leftPopup);
        rightPopup = new JRadioButton(Language.getString("RIGHT_SCREEN_POPUP"));
        rightPopup.setPreferredSize(new Dimension(230, 16));
        rightPopup.addActionListener(this);
        rightPopup.setActionCommand("RIGHT");

        screenPanel.add(new JLabel());
        screenPanel.add(rightPopup);
        ButtonGroup grp = new ButtonGroup();
        grp.add(leftPopup);
        grp.add(rightPopup);
        ///this.getContentPane().add(screenPanel);
        screenPanel.setPreferredSize(new Dimension(300, 70));
        mainPanel.add(screenPanel);

        /*if (Settings.isMultiScreenEnvr()){
    dualScreen.setSelected(Settings.getBooleanItem("DUAL_SCREEN_MODE"));
    rightPopup.setEnabled(dualScreen.isSelected());
    leftPopup.setEnabled(dualScreen.isSelected());
    leftPopup.setSelected(Settings.getBooleanItem("DUAL_SCREEN_MODE_LEFT"));
    rightPopup.setSelected(!Settings.getBooleanItem("DUAL_SCREEN_MODE_LEFT"));
        }else{
            dualScreen.setSelected(false);
            dualScreen.setEnabled(false);
            leftPopup.setEnabled(false);
            rightPopup.setEnabled(false);
        }*/
        if (Settings.isMultiScreenEnvr()) {
            dualScreen.setEnabled(true);
            dualScreen.setSelected(Settings.isDualScreenMode());


            if (Settings.isLeftAlignPopups()) {
                leftPopup.setEnabled(true);
                leftPopup.setSelected(true);
                rightPopup.setEnabled(true);
                rightPopup.setSelected(false);
            } else {
                rightPopup.setEnabled(true);
                rightPopup.setSelected(true);
                leftPopup.setEnabled(true);
                leftPopup.setSelected(false);
            }
        } else {
            dualScreen.setEnabled(false);
            leftPopup.setEnabled(false);
            dualScreen.setSelected(Settings.isDualScreenMode());
            rightPopup.setEnabled(false);
        }


        /// this.getContentPane().add(bottomPanel);
        mainPanel.add(bottomPanel);
        // BUTTON PANEL
        //change start
        JPanel oButtontPanel = new JPanel(new FlexGridLayout(new String[]{"120", "80", "0", "80"}, new String[]{"20"}, 5, 10)); // new FlowLayout(FlowLayout.TRAILING));
        oButtontPanel.setPreferredSize(new Dimension(300, 30));
        bottomPanel.add(oButtontPanel);
        oButtontPanel.add(new JLabel(""));
        //change end


        btnOk = new TWButton(Language.getString("OK"));
        btnOk.addActionListener(this);
        btnOk.addKeyListener(this);
        btnOk.setActionCommand("O");
        btnOk.setPreferredSize(new Dimension(80, 20));
        oButtontPanel.add(btnOk);
        oButtontPanel.add(new JLabel(""));
        btnOk.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);
        btnCancel.addKeyListener(this);
        btnCancel.setActionCommand("C");
        btnCancel.setPreferredSize(new Dimension(80, 20));
        oButtontPanel.add(btnCancel);

        btnCancel.registerKeyboardAction(this, "C", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        //change start
//        GUISettings.setSameSize(btnOk, btnCancel);
        GUISettings.applyOrientation(oButtontPanel);
        scrollingCombo.updateUI();
        setResizable(false);
        pack();
//        setSize(280, 410);

        chkSOCKSIP.setSelected(toBoolean(Settings.getItem("USE_SOCKS_PROXY")));
        toggleProxyFields();

        switch (Settings.autoScrollInterval) {
            case 10:
                scrollingCombo.setSelectedIndex(0);
                break;
            case 20:
                scrollingCombo.setSelectedIndex(1);
                break;
            case 50:
                scrollingCombo.setSelectedIndex(2);
                break;
            case 100:
                scrollingCombo.setSelectedIndex(3);
                break;
            case 150:
                scrollingCombo.setSelectedIndex(4);
                break;
        }

        addWindowListener(this);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        // Center the window on the screen
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }

    public boolean showDialog() {
        this.show();
        return isProxyChanged();
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
    }

    private JPanel getBulbPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        primaryBulb = new JLabel(new ImageIcon("images/theme" + Theme.getID() + "/red_bulb_config.gif"));
        primaryBulb.setToolTipText("" + Settings.SERVER_PORT);
        backupBulb = new JLabel(new ImageIcon("images/theme" + Theme.getID() + "/red_bulb_config.gif"));
        backupBulb.setToolTipText("" + Settings.DOWNLOAD_PORT);
        JLabel dummy = new JLabel();
        dummy.setPreferredSize(new Dimension(20, 15));
        panel.add(primaryBulb);
        panel.add(dummy);
        panel.add(backupBulb);

        return panel;
    }

    private boolean doValidatePort(String sPort, String sMessage) {
        try {
            Integer.parseInt(sPort);
            return true;
        } catch (Exception e) {
            showMessage(sMessage);
            return false;
        }
    }

    private boolean doValidateIP(String sIP, String sMessage) {
        /* validate the IP
           Break the ip by '.'s and validate each part*/
        /*try
        {
            if (sIP.trim().equals("")){
                showMessage(sMessage);
                return false;
            }
        }
        catch(Exception e)
        {
            showMessage(sMessage);
            return false;
        }*/
        return true;
    }

    public void showMessage(String sMsg) {
        this.setVisible(false);
        ShowMessage oMsg = new ShowMessage(false, sMsg, "E");
        oMsg = null;
        dualScreen.setSelected(Settings.isDualScreenMode());
        leftPopup.setSelected(Settings.getBooleanItem("DUAL_SCREEN_MODE_LEFT"));
        rightPopup.setSelected(!Settings.getBooleanItem("DUAL_SCREEN_MODE_LEFT"));
        this.setVisible(true);
    }

    private void okPressed() {
        if (Settings.isTCPMode()) {
            if (doValidateIP(txtIP.getText().trim(), Language.getString("MSG_INVALID_IP"))) {
                IPSettings.clearStaticIPList();
                IPSettings.addIP(txtIP.getText().trim(), IP.STATIC, IP.PRIMARY);
                IPSettings.addIP(txtIP2.getText().trim(), IP.STATIC, IP.SECONDARAY);
                Settings.setItemInBulk("DEFAULT_USER", txtUser.getText().trim());
                Settings.setItemInBulk("PASSWORD", new String(txtPass.getPassword()).trim());
                Settings.setUserDetails(txtUser.getText().trim(), new String(txtPass.getPassword()).trim());

                if ((chkSOCKSIP.isSelected()) &&
                        (doValidateIP(txtProxySOCKSIP.getText().trim(), Language.getString("MSG_INVALID_SOCKS_PROXY_IP"))) &&
                        (doValidatePort(txtProxySOCKSPort.getText().trim(), Language.getString("MSG_INVALID_SOCKS_PROXY_PORT")))) {
                    Settings.setItem("PROXY_SOCKS_IP", txtProxySOCKSIP.getText().trim());
                    Settings.setItem("PROXY_SOCKS_PORT", txtProxySOCKSPort.getText().trim());

                }


                if (chkSOCKSIP.isSelected()) {
                    Settings.setItem("USE_SOCKS_PROXY", "1");
                    System.setProperty("socksProxyHost", txtProxySOCKSIP.getText().trim());
                    System.setProperty("socksProxyPort", txtProxySOCKSPort.getText().trim());
                    System.out.println("Proxy set");
                } else {
                    Settings.setItem("USE_SOCKS_PROXY", "0");
                    System.setProperty("socksProxyHost", "");
                    System.out.println("Proxy unset");
                }
                //saving changes to tip.dll file
                if (doValidateIP(txtTradeIP.getText().trim(), Language.getString("MSG_INVALID_IP"))) {
                    com.isi.csvr.trading.IPSettings.getSharedInstance().clearSSOIPs();
                    com.isi.csvr.trading.IPSettings.getSharedInstance().addSSOIP(txtTradeIP.getText().trim(), IP.STATIC, IP.PRIMARY);
                    com.isi.csvr.trading.IPSettings.getSharedInstance().addSSOIP(txtTradeIP2.getText().trim(), IP.STATIC, IP.SECONDARAY);
//                    com.isi.csvr.trading.IPSettings.getSharedInstance().addIP(txtIP.getText().trim(), IP.STATIC, IP.PRIMARY);
//                    com.isi.csvr.trading.IPSettings.getSharedInstance().addIP(txtIP2.getText().trim(), IP.STATIC, IP.SECONDARAY);
                    com.isi.csvr.trading.IPSettings.getSharedInstance().save();
                }
                saveAutoScroll();
                setOkPressed(true);
                setVisible(false);
                this.dispose();
            }
        } else {
            Settings.SATELLITE_CARD_IP = txtIP.getText();
            Settings.SATELLITE_DATA_IP = txtIP2.getText();
            SatelliteIPSettings.save();
            saveAutoScroll();
            setOkPressed(true);
            setVisible(false);
            this.dispose();
        }

        if (intialConnectionMode != Settings.getCommunicationType()) {
            SharedMethods.showMessage(Language.getString("MSG_CONNECTION_MODE_CHANGED"),
                    JOptionPane.WARNING_MESSAGE);
        }

    }

    private void saveAutoScroll() {
        switch (scrollingCombo.getSelectedIndex()) {
            case 0:
                Settings.autoScrollInterval = 10;
                break;
            case 1:
                Settings.autoScrollInterval = 20;
                break;
            case 2:
                Settings.autoScrollInterval = 50;
                break;
            case 3:
                Settings.autoScrollInterval = 100;
                break;
            case 4:
                Settings.autoScrollInterval = 150;
                break;
        }

        Settings.setItem("AUTO_SCROLL_INTERVAL", Settings.autoScrollInterval + "");
    }

    private void cancelPressed() {
        setVisible(false);
        this.dispose();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("O")) // Ok button
        {
            okPressed();
        } else if (e.getActionCommand().equals("P")) // Proxy settings
        {
            g_bProxyChanged = true;
            toggleProxyFields();
        } else if (e.getActionCommand().equals("C")) { //  Cancel button
            cancelPressed();
        } else if (e.getActionCommand().equals("INTERNET")) { //  Internet Radio button
            setConnectioMode(Settings.COMMUNICATION_TYPE_TCP);
        } else if (e.getActionCommand().equals("SATELLITE")) { //  Satellite Radio button
            setConnectioMode(Settings.COMMUNICATION_TYPE_UDP);
        } else if (e.getActionCommand().equals("DUAL")) {
            leftPopup.setEnabled(dualScreen.isSelected());
            rightPopup.setEnabled(dualScreen.isSelected());
            Settings.setDualScreenMode(dualScreen.isSelected());
            Settings.setLeftAlignPopups(leftPopup.isSelected());
        } else if (e.getActionCommand().equals("LEFT")) {
            Settings.setLeftAlignPopups(true);
        } else if (e.getActionCommand().equals("RIGHT")) {
            Settings.setLeftAlignPopups(false);
        }
    }

    private void setConnectioMode() {
        setConnectioMode(Settings.getCommunicationType());
    }

    private void setConnectioMode(int connectionMode) {
        if (connectionMode == Settings.COMMUNICATION_TYPE_TCP) { //internet
            lblIP.setText(Language.getString("IP"));
            lblIP2.setText(Language.getString("IP2"));
            if (TWControl.isSingleSingnOn()) {     //&& TWControl.getSSOType() == TWControl.SSO_TYPES.Direct
                txtUser.setEnabled(false);
                txtPass.setEnabled(false);
            } else {
                txtUser.setEnabled(true);
                txtPass.setEnabled(true);
            }
            chkSOCKSIP.setEnabled(true);
            if (chkSOCKSIP.isSelected()) {
                txtProxySOCKSIP.setEnabled(true);
                txtProxySOCKSPort.setEnabled(true);
            }
            txtIP.setText(IPSettings.getStaticIP(0));
            txtIP2.setText(IPSettings.getStaticIP(1));
            Settings.setItem("CONNECTION_MODE", "" + connectionMode);
            Settings.setCommunicationType(Settings.COMMUNICATION_TYPE_TCP);
            rbInternet.setSelected(true);
        } else {
            lblIP.setText(Language.getString("SATELLITE_CARD_IP"));
            lblIP2.setText(Language.getString("SATELLITE_DATA_IP"));
            txtUser.setEnabled(false);
            txtPass.setEnabled(false);
            chkSOCKSIP.setEnabled(false);
            txtProxySOCKSIP.setEnabled(false);
            txtProxySOCKSPort.setEnabled(false);
            txtIP.setText(Settings.SATELLITE_CARD_IP);
            txtIP2.setText(Settings.SATELLITE_DATA_IP);
            Settings.setItem("CONNECTION_MODE", "" + connectionMode);
            Settings.setCommunicationType(Settings.COMMUNICATION_TYPE_UDP);
            rbSatellite.setSelected(true);
        }
    }

    private void toggleProxyFields() {
        if (chkSOCKSIP.isSelected()) {
            txtProxySOCKSIP.setEnabled(true);
            txtProxySOCKSPort.setEnabled(true);
        } else {
            txtProxySOCKSIP.setEnabled(false);
            txtProxySOCKSPort.setEnabled(false);
        }

//        if (chkHTTPIP.isSelected()){
//            txtProxyHTTPIP.setEnabled(true);
//            txtProxyHTTPPort.setEnabled(true);
//        }else{
//            txtProxyHTTPIP.setEnabled(false);
//            txtProxyHTTPPort.setEnabled(false);
//        }
    }

    public void keyPressed(KeyEvent e) {
        if ((e.getKeyCode() == KeyEvent.VK_ENTER)) {
            if (e.getSource() == btnCancel) {
                cancelPressed();
            } else {
                okPressed();
            }
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    /**
     * Converts the given string to a bolean value
     */
    private boolean toBoolean(String sValue) {
        try {
            if (Integer.parseInt(sValue) == 1)
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isProxyChanged() {
        return g_bProxyChanged;
    }

    public boolean isOkPressed() {
        return okPressed;
    }

    public void setOkPressed(boolean okPressed) {
        this.okPressed = okPressed;
    }

    private boolean isBackupPathOpen() {

        try {
            String ip = Settings.getRegionalContentProviderIP();
            if (ip == null) {
                ip = Settings.getGlobalContentProviderIP();
                if (ip == null) {
                    ip = Settings.getActiveIP();
                }
            }
            Socket socket = new Socket(ip, Settings.DOWNLOAD_PORT);
            socket.getInputStream();
            Thread.sleep(1000);
            socket.close();
            return true;
            /*String url = Meta.BACKLOG_PATH_TEST + Meta.DS + "0" + Meta.EOL;
            ZipFileDownloader downloader = new ZipFileDownloader(url, "N/A", "TEST");
            File[] files = downloader.downloadFiles();
            downloader = null;
            for (int i = 0; i < files.length; i++) {
                pathOK = true;
                try {
                    files[i].delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void run() {

        if (Settings.isSatelliteMode()) // not allowed in Satellite mode
            return;

        int index = 6;
        while (active) {
            index++;
            if (index > 5) {
                index = 1;
                if (Settings.isConnected()) {
                    primaryBulb.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/green_bulb_config.gif"));
                } else {
                    primaryBulb.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/red_bulb_config.gif"));
                    backupBulb.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/red_bulb_config.gif"));
                    backupPortConnected = false;
                }
                if (!backupPortConnected) {
                    if (Settings.isConnected()) {
                        if (isBackupPathOpen()) {
                            backupBulb.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/green_bulb_config.gif"));
                            backupPortConnected = true;
                        } else {
                            backupBulb.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/red_bulb_config.gif"));
                        }
                    }
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowClosed(WindowEvent e) {
        active = false;
    }

    public void windowClosing(WindowEvent e) {
        active = false;
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
    }
}
