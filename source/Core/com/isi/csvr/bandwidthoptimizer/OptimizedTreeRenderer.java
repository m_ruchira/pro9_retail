package com.isi.csvr.bandwidthoptimizer;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Sep 8, 2008
 * Time: 6:29:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptimizedTreeRenderer implements TreeCellRenderer {

    //------------- To store selected/unselected status------------------
    public static Hashtable<String, Boolean> selectedSymbols = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> selectedSectors = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> selectedSubMarkets = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> selectedExchanges = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> tempselectedSymbols = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> tempselectedSectors = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> tempselectedSubMarkets = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> tempselectedExchanges = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> tempunselectedSymbols = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> tempunselectedSectors = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> tempunselectedSubMarkets = new Hashtable<String, Boolean>();
    public static Hashtable<String, Boolean> tempunselectedExchanges = new Hashtable<String, Boolean>();
    private static DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
    private static ImageIcon g_oClosedIcon = null;
    private static ImageIcon g_oExpandedIcon = null;
    private static ImageIcon g_oLeafIcon_sub = null;
    private static Color backgroundSelectionColor;
    private static Color backgroundNonSelectionColor;
    public TWCustomCheckBox checkBox;
    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");

    public OptimizedTreeRenderer() {
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
            backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
            backgroundSelectionColor = Theme.getColor("SIDEBAR_SELECTED_BGCOLOR");

        } catch (Exception e) {
        }
        reload();
    }

    public OptimizedTreeRenderer(OptimizedSymbolTree tree) {
        //To change body of created methods use File | Settings | File Templates.
    }

    public static void reload() {
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
//            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
            backgroundSelectionColor = Theme.getColor("SIDEBAR_SELECTED_BGCOLOR");

        } catch (Exception e) {

        }
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean selected, boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        Component returnValue = null;
        try {
            if (((OptimizedExchangeNode) value) instanceof OptimizedExchangeNode) {
                OptimizedRendererComponent nonLeafRenderer = new OptimizedRendererComponent();
                if (selected)
                    checkBox = nonLeafRenderer.getCheckBox();

                OptimizedExchangeNode nv = (OptimizedExchangeNode) value;
                if (nv.isExchangeType()) {


                    Enumeration keys = selectedExchanges.keys();
                    while (keys.hasMoreElements()) {
                        String key = (String) keys.nextElement();
                        Boolean stockRet = selectedExchanges.get(key);
                        if (key.equals(nv.getExchangeSymbol())) {
                            nonLeafRenderer.setcheckLableString(stockRet);
                        }
                    }
                    if (expanded) {
                        try {
                            if (nv.getMarketStatusType() == Meta.MARKET_OPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_CLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PREOPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PREOPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PRECLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PRECLOSED_COLOR"));
                            } else {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            }
//                            nonLeafRenderer.setMode(0);
                            nonLeafRenderer.setLeftString(nv.toString());
//                            nonLeafRenderer.setcheckLableString(nv.getName());

                            nonLeafRenderer.setRightString(nv.getStatus());
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        nonLeafRenderer.setImage(g_oExpandedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    } else {
                        try {
                            if (nv.getMarketStatusType() == Meta.MARKET_OPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_CLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PREOPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PREOPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PRECLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PRECLOSED_COLOR"));
                            } else {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            }
//                            nonLeafRenderer.setMode(0);
                            nonLeafRenderer.setLeftString(nv.toString());
//                            nonLeafRenderer.setcheckLableString(nv.getName());

                            nonLeafRenderer.setRightString(nv.getStatus());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        nonLeafRenderer.setImage(g_oClosedIcon);


                        nonLeafRenderer.setToolTipText(null);
                    }
                    nonLeafRenderer.setSelectedColor(backgroundNonSelectionColor);
                    if (selected) {
                        nonLeafRenderer.setSelectedColor(backgroundSelectionColor);


                        while (keys.hasMoreElements()) {
                            String key = (String) keys.nextElement();
                            Boolean stockRet = selectedExchanges.get(key);
                            if (key.equals(nv.getExchangeSymbol())) {
                                nonLeafRenderer.setcheckLableString(stockRet);
                            }

                        }
                    } else {
                        nonLeafRenderer.setUnselected();

                        while (keys.hasMoreElements()) {
                            String key = (String) keys.nextElement();
                            Boolean stockRet = selectedExchanges.get(key);
                            if (key.equals(nv.getExchangeSymbol())) {
                                nonLeafRenderer.setcheckLableString(stockRet);
                            }
                        }
                    }
                    returnValue = nonLeafRenderer;
                } else if (nv.isSectorType()) {
                    OptimizedSectorNode sec = (OptimizedSectorNode) value;

                    Enumeration keys = selectedSectors.keys();
                    while (keys.hasMoreElements()) {
                        String key = (String) keys.nextElement();
                        Boolean stockRet = selectedSectors.get(key);
                        if (key.equals(sec.getDescription() + sec.getExchange())) {
                            nonLeafRenderer.setcheckLableString(stockRet);
                        }
                    }

                    if (expanded) {
                        try {
                            nonLeafRenderer.setLeftString(nv.toString());
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        nonLeafRenderer.setImage(g_oExpandedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    } else {
                        try {
                            nonLeafRenderer.setLeftString(nv.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        nonLeafRenderer.setImage(g_oClosedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    }
                    nonLeafRenderer.setSelectedColor(backgroundNonSelectionColor);
                    if (selected) {
                        nonLeafRenderer.setSelectedColor(backgroundSelectionColor);

                        while (keys.hasMoreElements()) {
                            String key = (String) keys.nextElement();
                            Boolean stockRet = selectedSectors.get(key);
                            if (key.equals(sec.getDescription() + sec.getExchange())) {
                                nonLeafRenderer.setcheckLableString(stockRet);
                            }

                        }
                    } else {
                        nonLeafRenderer.setUnselected();

//                            Enumeration keys = selectedSectors.keys();
                        while (keys.hasMoreElements()) {
                            String key = (String) keys.nextElement();
                            Boolean stockRet = selectedSectors.get(key);
                            if (key.equals(sec.getDescription() + sec.getExchange())) {
                                nonLeafRenderer.setcheckLableString(stockRet);
                            }
                        }
                    }
                    returnValue = nonLeafRenderer;
                } else if (nv.isSubMarketType()) {
                    OptimizedSubMarketNode sec = (OptimizedSubMarketNode) value;

                    Enumeration keys = selectedSubMarkets.keys();
                    while (keys.hasMoreElements()) {
                        String key = (String) keys.nextElement();
                        Boolean stockRet = selectedSubMarkets.get(key);
                        if (key.equals(sec.getSubMarket() + sec.getExchange())) {
                            nonLeafRenderer.setcheckLableString(stockRet);
                        }
                    }
                    if (expanded) {
                        try {
                            nonLeafRenderer.setLeftString(nv.toString());
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        nonLeafRenderer.setImage(g_oExpandedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    } else {
                        try {
                            nonLeafRenderer.setLeftString(nv.toString());
//                            nonLeafRenderer.setRightString(nv.getStatus());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        nonLeafRenderer.setImage(g_oClosedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    }
                    nonLeafRenderer.setSelectedColor(backgroundNonSelectionColor);
                    if (selected) {
                        nonLeafRenderer.setSelectedColor(backgroundSelectionColor);

//                         Enumeration keys = selectedSectors.keys();
                        while (keys.hasMoreElements()) {
                            String key = (String) keys.nextElement();
                            Boolean stockRet = selectedSubMarkets.get(key);
                            if (key.equals(sec.getSubMarket() + sec.getExchange())) {
                                nonLeafRenderer.setcheckLableString(stockRet);
                            }
                        }
                    } else {
                        nonLeafRenderer.setUnselected();

                        while (keys.hasMoreElements()) {
                            String key = (String) keys.nextElement();
                            Boolean stockRet = selectedSubMarkets.get(key);
                            if (key.equals(sec.getSubMarket() + sec.getExchange())) {
                                nonLeafRenderer.setcheckLableString(stockRet);
                            }

                        }
                    }
                    returnValue = nonLeafRenderer;

                } else {
                    OptimizedSymbolNode sto = (OptimizedSymbolNode) value;
                    Stock st = (Stock) DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()));
                    nonLeafRenderer.setImage(g_oLeafIcon_sub);
                    nonLeafRenderer.setMode(1);
                    nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_DESCR_COLOR"), Theme.getColor("SIDEBAR_SYMBOL_COLOR"));

                    Enumeration keys = selectedSymbols.keys();
                    while (keys.hasMoreElements()) {
                        String key = (String) keys.nextElement();
                        Boolean stockRet = selectedSymbols.get(key);
                        if (key.equals(sto.getKey())) {
                            nonLeafRenderer.setcheckLableString(stockRet);
                        }

                    }

                    if (selected) {
                        nonLeafRenderer.setSelectedColor(backgroundSelectionColor);
                        nonLeafRenderer.setLeftString(sto.getShortDescription());

                        Enumeration keys1 = selectedSymbols.keys();
                        while (keys1.hasMoreElements()) {
                            String key = (String) keys1.nextElement();
                            Boolean stockRet = selectedSymbols.get(key);
                            if (key.equals(sto.getKey())) {
                                nonLeafRenderer.setcheckLableString(stockRet);
                            }
                        }
                        nonLeafRenderer.setRightString(sto.getSymbol());
                    } else {
                        nonLeafRenderer.setUnselected();
                        nonLeafRenderer.setLeftString(sto.getShortDescription());
                        nonLeafRenderer.setRightString(sto.getSymbol());
                        Enumeration keys1 = selectedSymbols.keys();
                        while (keys1.hasMoreElements()) {
                            String key = (String) keys1.nextElement();
                            Boolean stockRet = selectedSymbols.get(key);
                            if (key.equals(sto.getKey())) {
                                nonLeafRenderer.setcheckLableString(stockRet);
                            }

                        }

                    }
                    returnValue = nonLeafRenderer;
                }

            } else {
                returnValue = new OptimizedRendererComponent();
            }
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return new JLabel("");
        }
    }

    public boolean isOnHotspot(int x, int y) {
        // TODO: alternativa (ma funge???)
        try {
            if (this.checkBox.getBounds().contains(x, y)) {
                this.checkBox.setSelected(true);
            }
            return (this.checkBox.getBounds().contains(x, y));
        } catch (Exception e) {
            return false;
        }
    }
}
