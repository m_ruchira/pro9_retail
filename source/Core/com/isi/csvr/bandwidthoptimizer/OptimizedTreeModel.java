package com.isi.csvr.bandwidthoptimizer;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Sep 8, 2008
 * Time: 5:44:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptimizedTreeModel extends DefaultTreeSelectionModel
        implements TreeModel {
    private OptimizedExchangeNode map;

    public OptimizedTreeModel(OptimizedExchangeNode map) {

        this.map = map;
    }

    public Object getRoot() {
        return map;
    }


    public Object getChild(Object parent, int index) {
        return ((OptimizedExchangeNode) parent).getChild(index);

    }

    public int getChildCount(Object parent) {
        return ((OptimizedExchangeNode) parent).getChildCount();

    }

    public boolean isLeaf(Object node) {
        if (node instanceof OptimizedExchangeNode) {
            return false;
        } else {
            return true;
        }
    }

    public void valueForPathChanged(TreePath path, Object newValue) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getIndexOfChild(Object parent, Object child) {
        return ((OptimizedExchangeNode) parent).getIndex(child);
    }

    public void addTreeModelListener(TreeModelListener l) {
    }

    public void removeTreeModelListener(TreeModelListener l) {
    }
}
