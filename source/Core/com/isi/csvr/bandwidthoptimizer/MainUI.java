package com.isi.csvr.bandwidthoptimizer;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.communication.tcp.TCPConnector;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.WatchlistListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.sideBar.Constants;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.BadLocationException;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Sep 8, 2008
 * Time: 5:39:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainUI extends InternalFrame implements ActionListener, KeyListener, Themeable, WatchlistListener,
        Runnable, FocusListener, ExchangeListener, InternalFrameListener, PopupMenuListener, ApplicationListener, MouseListener {

    public static MainUI self;
    public static boolean checkNodeMode = false;
    public static Hashtable<String, Object> selectedSymbolHash;
    public static ArrayList addrequestArrayList = new ArrayList();
    public static ArrayList removerequestArrayList = new ArrayList();
    public static Hashtable<String, Boolean> selectedSymbols = new Hashtable<String, Boolean>();
    public static Hashtable<String, String> unselectedSymbols = new Hashtable<String, String>();
    public static Properties prop;
    public static boolean firstTimeLoad = false;
    public static boolean IS_EXCHANGE_SELECT = false;
    //    public static boolean PRESS_OK = false;
    private static boolean havingSectors = false;
    public Hashtable<String, OptimizedSymbolNode> mainStore;
    public Hashtable<String, OptimizedSymbolNode> filteredStore;
    public JRadioButton setAll;
    public JRadioButton setEnabled;
    public JRadioButton setDisabled;
    private JPanel mainPanel;
    private JScrollPane scrollPane;
    private JPanel upperPanel;
    private JPanel selectionPanel;
    private JPanel buttonPanel;
    private TWButton searchButton;
    private TWButton allBtn;
    private TWButton enableBtn;
    private TWButton disableBtn;
    private TWButton okBtn;
    private TWButton cancelBtn;
    private TWTextField searchText;
    private OptimizedExchangeNode rootNode;
    private OptimizedSymbolTree tree;
    private OptimizedTreeModel treeModel;
    private OptimizedTreeRenderer renderer = new OptimizedTreeRenderer();
    private JPopupMenu symbolTreePopup;
    private String selectedSymbol = "";
    private String selectedExchange = "";
    private int selectedSymbolsIntType = -1;
    private ViewSetting oSetting;
    private boolean isReady = false;
    private boolean isActive = false;
    private Hashtable<String, OptimizedExchangeNode> watchListReference;
    private ToolBarButton btnSearch;


    private MainUI() {
        rootNode = new OptimizedExchangeNode("Root", 0, "root");
        treeModel = new OptimizedTreeModel(rootNode);
        mainStore = new Hashtable<String, OptimizedSymbolNode>();
        filteredStore = new Hashtable<String, OptimizedSymbolNode>();
        watchListReference = new Hashtable<String, OptimizedExchangeNode>();
        createUI();
        applyTheme();
        Theme.registerComponent(this);
        isActive = true;
        hideTitleBarMenu();
        Application.getInstance().addApplicationListener(this);
        GUISettings.applyOrientation(this);
    }

    public static MainUI getSharedInstance() {
        if (self == null) {
            self = new MainUI();
        }
        return self;
    }

    /* public void showBAndwidthOpUI() {
        oSetting = ViewSettingsManager.getSummaryView("BANDWIDTH_OPTIMIZER");
        oSetting.setParent(this);
        this.setViewSetting(oSetting);
        rootNode = new OptimizedExchangeNode("Root", 0, "root");
        treeModel = new OptimizedTreeModel(rootNode);
        mainStore = new Hashtable<String, OptimizedSymbolNode>();
        filteredStore = new Hashtable<String, OptimizedSymbolNode>();
        watchListReference = new Hashtable<String, OptimizedExchangeNode>();
        createUI();
        applyTheme();
        Theme.registerComponent(this);
        isActive = true;
        hideTitleBarMenu();
        Application.getInstance().addApplicationListener(this);
    }*/

    public static void firstTimeLoaded() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exg = (Exchange) exchanges.nextElement();
            if (exg.isDefault()) {
                OptimizedTreeRenderer.selectedExchanges.put(exg.getSymbol(), true);
                Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());
                if (sectorsEnum.hasMoreElements()) {
                    while (sectorsEnum.hasMoreElements()) {
                        Sector sector = (Sector) sectorsEnum.nextElement();
                        OptimizedTreeRenderer.selectedSectors.put(sector.getDescription() + exg.getSymbol(), true);
                    }
                }
                Exchange exch = ExchangeStore.getSharedInstance().getExchange(exg.getSymbol());
                for (Market subMarket : exch.getSubMarkets()) {
                    OptimizedTreeRenderer.selectedSubMarkets.put(subMarket.getMarketID() + exg.getSymbol(), true);
                }
                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                if (exchageSymbols != null) {
                    while (exchageSymbols.hasMoreElements()) {
                        String symbol = (String) exchageSymbols.nextElement();
                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol() + "~" + symbol);
                        if (st == null) {
                            st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                        }
                        OptimizedTreeRenderer.selectedSymbols.put(st.getKey(), true);
                        st.setSymbolEnabled(true);
                    }
                }
            }
        }

        firstTimeLoad = true;
    }

    public static void save_BandWidthOptimized_Symbols() { // throws IOException
        try {
            if (Settings.isConnected()) {
                if (OptimizedTreeRenderer.selectedSymbols.size() > 0) {
                    FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/optimizedSymbols.dll");
                    //        load_BandWidthOptimized_Symbols();
                    Enumeration keys = OptimizedTreeRenderer.selectedSymbols.keys();
                    while (keys.hasMoreElements()) {
                        String key = (String) keys.nextElement();
                        boolean workspaceSetting = OptimizedTreeRenderer.selectedSymbols.get(key);
                        oOut.write((key + "\n").getBytes());
                    }
                    oOut.close();
                } else {
                    //donothing
                }
            }
        } catch (IOException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void stockUpdator() {
        Enumeration keys = OptimizedTreeRenderer.selectedSymbols.keys();
        while (keys.hasMoreElements()) {
            String keyval = (String) keys.nextElement();

            Stock st = DataStore.getSharedInstance().getStockObject(keyval);
            if (st != null) {
                st.setSymbolEnabled(true);
            }

        }
    }

    public static void updateStocksFromDataStore() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exg = (Exchange) exchanges.nextElement();
            if (exg.isDefault() && exg.isExchageMode()) {
                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                if (exchageSymbols != null) {
                    while (exchageSymbols.hasMoreElements()) {
                        String symbol = (String) exchageSymbols.nextElement();
                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol() + "~" + symbol);
                        if (st == null) {
                            st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                        }
                        OptimizedTreeRenderer.selectedSymbols.put(st.getKey(), true);
                        st.setSymbolEnabled(true);
                    }
                }
            }
        }
    }

    public static void load_BandWidthOptimized_Symbols() throws IOException {

        FileInputStream fstream = new FileInputStream(Settings.SYSTEM_PATH + "/optimizedSymbols.dll");
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        //Read File Line By Line
        while ((strLine = br.readLine()) != null) {
            String ex = SharedMethods.getExchangeFromKey(strLine);

            boolean isValid = ExchangeStore.getSharedInstance().isValidExchange(ex);

            if (isValid) {
                OptimizedTreeRenderer.selectedSymbols.put(strLine, true);
            }
        }
        in.close();
        treePaintMethod();

    }

    public static void repaintTree() {
        OptimizedTreeRenderer.selectedSubMarkets = new Hashtable<String, Boolean>();
        OptimizedTreeRenderer.selectedSectors = new Hashtable<String, Boolean>();
        OptimizedTreeRenderer.selectedExchanges = new Hashtable<String, Boolean>();

        Enumeration keys = OptimizedTreeRenderer.selectedSymbols.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            boolean workspaceSetting = OptimizedTreeRenderer.selectedSymbols.get(key);

            String ex = SharedMethods.getExchangeFromKey(key);
            boolean isValid = ExchangeStore.getSharedInstance().isValidExchange(ex);
            if (isValid) {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                if (st == null) {
                    st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), SharedMethods.getInstrumentTypeFromKey(key), false);
                }
                if (st != null) {
                    try {

                        OptimizedTreeRenderer.selectedExchanges.put(st.getExchange(), true);
                        Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(st.getExchange());
                        if (sectorsEnum.hasMoreElements()) {
                            while (sectorsEnum.hasMoreElements()) {
                                Sector sector = (Sector) sectorsEnum.nextElement();
                                if (st.getSectorCode().equals(sector.getId())) {
                                    OptimizedTreeRenderer.selectedSectors.put(sector.getDescription() + st.getExchange(), true);
                                }
                            }
                        }

                        Exchange exch = ExchangeStore.getSharedInstance().getExchange(st.getExchange());
                        for (Market subMarket : exch.getSubMarkets()) {
                            if (subMarket.getMarketID().equals(st.getMarketID())) {
                                OptimizedTreeRenderer.selectedSubMarkets.put(subMarket.getMarketID() + st.getExchange(), true);
                            }
                        }

                        st.setSymbolEnabled(true);
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    OptimizedTreeRenderer.selectedSymbols.remove(key);
                }
            } else {

                if (isValid) {
                    Stock st = DataStore.getSharedInstance().getStockObject(key);

                    if (st == null) {
                        st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), SharedMethods.getInstrumentTypeFromKey(key), false);
                    }
                    if (st != null) {
                        OptimizedTreeRenderer.selectedSymbols.remove(key);
                        st.setSymbolEnabled(false);
                    }
                }
            }
        }

//        checkingValidExchange();

    }

    public static void treePaintMethod() {

        Enumeration keys = OptimizedTreeRenderer.selectedSymbols.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            boolean workspaceSetting = OptimizedTreeRenderer.selectedSymbols.get(key);

            String ex = SharedMethods.getExchangeFromKey(key);
            boolean isValid = ExchangeStore.getSharedInstance().isValidExchange(ex);
            if (isValid) {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                if (st == null) {
                    st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), SharedMethods.getInstrumentTypeFromKey(key), false);
                }
                if (st != null) {
                    try {

                        OptimizedTreeRenderer.selectedExchanges.put(st.getExchange(), true);
                        Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(st.getExchange());
                        if (sectorsEnum.hasMoreElements()) {
                            while (sectorsEnum.hasMoreElements()) {
                                Sector sector = (Sector) sectorsEnum.nextElement();
                                if (st.getSectorCode().equals(sector.getId())) {
                                    OptimizedTreeRenderer.selectedSectors.put(sector.getDescription() + st.getExchange(), true);
                                }
                            }
                        }

                        Exchange exch = ExchangeStore.getSharedInstance().getExchange(st.getExchange());
                        for (Market subMarket : exch.getSubMarkets()) {
                            if (subMarket.getMarketID().equals(st.getMarketID())) {
                                OptimizedTreeRenderer.selectedSubMarkets.put(subMarket.getMarketID() + st.getExchange(), true);
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    OptimizedTreeRenderer.selectedSymbols.remove(key);
                }
            } else {

                if (isValid) {
                    Stock st = DataStore.getSharedInstance().getStockObject(key);

                    if (st == null) {
                        st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), SharedMethods.getInstrumentTypeFromKey(key), false);
                    }
                    if (st != null) {
                        OptimizedTreeRenderer.selectedSymbols.remove(key);
                        st.setSymbolEnabled(false);
                    }
                }
            }
        }

        checkingValidExchange();

    }

    public static void checkingValidExchange() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exg = (Exchange) exchanges.nextElement();
            if (exg.isDefault()) {
                if ((OptimizedTreeRenderer.selectedExchanges.containsKey(exg.getSymbol())) || (TCPConnector.filteredExchanges.contains(exg.getSymbol()))) {
//                    System.out.println("------------------------------------------------" + exg);

                } else {
                    OptimizedTreeRenderer.selectedExchanges.put(exg.getSymbol(), true);
                    Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());
                    if (sectorsEnum.hasMoreElements()) {
                        while (sectorsEnum.hasMoreElements()) {
                            Sector sector = (Sector) sectorsEnum.nextElement();
                            OptimizedTreeRenderer.selectedSectors.put(sector.getDescription() + exg.getSymbol(), true);
                        }
                    }
                    Exchange exch = ExchangeStore.getSharedInstance().getExchange(exg.getSymbol());
                    for (Market subMarket : exch.getSubMarkets()) {
                        OptimizedTreeRenderer.selectedSubMarkets.put(subMarket.getMarketID() + exg.getSymbol(), true);
                    }
                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                    if (exchageSymbols != null) {
                        while (exchageSymbols.hasMoreElements()) {
                            String symbol = (String) exchageSymbols.nextElement();
                            Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol() + "~" + symbol);
                            if (st == null) {
                                st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                            }
                            if (st.getInstrumentType() != 7) {
                                OptimizedTreeRenderer.selectedSymbols.put(st.getKey(), true);
                            }
                            st.setSymbolEnabled(true);
                        }
                    }
                }
            }
        }
    }

    public static void loadSymbols() {
        prop = new Properties();

        try {
            File file = new File(Settings.SYSTEM_PATH + "/propReq.dll");
            if (file.exists()) {
                FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH + "/propReq.dll");
                prop.load(oIn);
                oIn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // -----------------prop savings
    public static void saveSettings() {
        if (prop != null) {
            try {
                synchronized (prop) {
                    FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/propReq.dll");
                    prop.store(oOut, "");
                    oOut.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(btnSearch)) {
            searchTree(searchText.getText());
        }
    }

    public TreeModel getDefaultTreeModel() {
        rootNode = new OptimizedExchangeNode("Root", 0, "root");
        mainStore = new Hashtable<String, OptimizedSymbolNode>();
        filteredStore = new Hashtable<String, OptimizedSymbolNode>();
        watchListReference = new Hashtable<String, OptimizedExchangeNode>();
        loadTree();
        return treeModel;
    }

    public void createUI() {
        mainPanel = new JPanel();
        upperPanel = new JPanel();
        buttonPanel = new JPanel();
        selectionPanel = new JPanel();
        allBtn = new TWButton(Language.getString("SHOW_ALL"));
        allBtn.addActionListener(this);
        allBtn.setActionCommand("ALL");
        enableBtn = new TWButton(Language.getString("SHOW_ENABLE"));
        enableBtn.addActionListener(this);
        enableBtn.setActionCommand("ENABLE");
        disableBtn = new TWButton(Language.getString("SHOW_DISABLE"));
        disableBtn.addActionListener(this);
        disableBtn.setActionCommand("DISABLE");
        okBtn = new TWButton(Language.getString("OK"));
        okBtn.addActionListener(this);
        okBtn.setActionCommand("OK");
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        cancelBtn.addActionListener(this);
        cancelBtn.setActionCommand("CANCEL");
        setAll = new JRadioButton(Language.getString("SHOW_ALL"));
        setAll.setSelected(true);
        setAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (setAll.isSelected()) {
                    searchDefaultTree();
                    setEnabled.setSelected(false);
                    setDisabled.setSelected(false);
                } else if (!setAll.isSelected()) {
                    setAll.setSelected(true);
                }
            }
        });
        setEnabled = new JRadioButton(Language.getString("SHOW_ENABLE"));
        setEnabled.setSelected(false);
        setEnabled.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (setEnabled.isSelected()) {
                    searchEnabledTree();
                    setAll.setSelected(false);
                    setDisabled.setSelected(false);
                } else if (!setEnabled.isSelected()) {
                    setEnabled.setSelected(true);
                }
            }
        });
        setDisabled = new JRadioButton(Language.getString("SHOW_DISABLE"));
        setDisabled.setSelected(false);
        setDisabled.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (setDisabled.isSelected()) {
                    searchDisabledTree();
                    setEnabled.setSelected(false);
                    setAll.setSelected(false);
                } else if (!setDisabled.isSelected()) {
                    setDisabled.setSelected(true);
                }
            }
        });
        tree = new OptimizedSymbolTree(treeModel);
        tree.setOpaque(false);
        tree.addKeyListener(this);
        tree.putClientProperty("JTree.lineStyle", "Vertical");
        tree.setToggleClickCount(1);
        tree.setCellRenderer(renderer);
        tree.setRootVisible(false);
        scrollPane = new JScrollPane(tree);
        searchButton = new TWButton(">>");
        searchButton.addActionListener(this);
        searchText = new TWTextField();
        searchText.addKeyListener(this);
        searchText.addFocusListener(this);
        searchText.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchText.requestFocus(false);
        btnSearch = new ToolBarButton();
        btnSearch.setPreferredSize(new Dimension(20, 19));
        btnSearch.setToolTipText(Language.getString("SEARCH"));
        btnSearch.setEnabled(true);
        btnSearch.setVisible(true);
        btnSearch.setIcon(getIconFromString("search"));
        btnSearch.setRollOverIcon(getIconFromString("search-mouseover"));
        btnSearch.addMouseListener(this);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        this.add(mainPanel);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle(Language.getString("BANDWIDTH_OPTIMIZER"));
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "100%", "25", "25"}, 3, 3));
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%", "18"}, new String[]{"100%"}, 3, 3));
        selectionPanel.setLayout(new FlexGridLayout(new String[]{"33%", "33%", "34%"}, new String[]{"100%"}, 5, 2));
        buttonPanel.setLayout(new FlexGridLayout(new String[]{"100%", "70", "70"}, new String[]{"100%"}, 5, 2));

        upperPanel.add(searchText);
        upperPanel.add(btnSearch);

        selectionPanel.add(setAll);
        selectionPanel.add(setEnabled);
        selectionPanel.add(setDisabled);
        buttonPanel.add(new JLabel(Language.getString("BANDWIDTH_ADVICE")));
        buttonPanel.add(okBtn);
        buttonPanel.add(cancelBtn);
        GUISettings.applyOrientation(this);
        setSize(550, 400);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        Client.getInstance().getDesktop().add(this);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        loadTree();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        this.hideTitleBarMenu();
        mainPanel.add(upperPanel);
        mainPanel.add(scrollPane);
        mainPanel.add(selectionPanel);
        mainPanel.add(buttonPanel);
    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath(iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    public void loadTree() {
        OptimizedTreeRenderer.tempselectedSymbols = new Hashtable<String, Boolean>();
        OptimizedTreeRenderer.tempselectedSectors = new Hashtable<String, Boolean>();
        OptimizedTreeRenderer.tempselectedSubMarkets = new Hashtable<String, Boolean>();
        OptimizedTreeRenderer.tempselectedExchanges = new Hashtable<String, Boolean>();

        OptimizedTreeRenderer.tempunselectedSymbols = new Hashtable<String, Boolean>();
        OptimizedTreeRenderer.tempunselectedSectors = new Hashtable<String, Boolean>();
        OptimizedTreeRenderer.tempunselectedSubMarkets = new Hashtable<String, Boolean>();
        OptimizedTreeRenderer.tempunselectedExchanges = new Hashtable<String, Boolean>();
        setAll.setSelected(true);
        setEnabled.setSelected(false);
        setDisabled.setSelected(false);
        Enumeration keys = OptimizedTreeRenderer.selectedExchanges.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            Boolean stockRet = OptimizedTreeRenderer.selectedExchanges.get(key);
            OptimizedTreeRenderer.tempunselectedExchanges.put(key, true);
        }
        Enumeration sec = OptimizedTreeRenderer.selectedSectors.keys();
        while (sec.hasMoreElements()) {
            String key = (String) sec.nextElement();
            Boolean stockRet = OptimizedTreeRenderer.selectedSectors.get(key);
            OptimizedTreeRenderer.tempunselectedSectors.put(key, true);
        }
        Enumeration sub = OptimizedTreeRenderer.selectedSubMarkets.keys();
        while (sub.hasMoreElements()) {
            String key = (String) sub.nextElement();
            Boolean stockRet = OptimizedTreeRenderer.selectedSubMarkets.get(key);
            OptimizedTreeRenderer.tempunselectedSubMarkets.put(key, true);
        }
        Enumeration sym = OptimizedTreeRenderer.selectedSymbols.keys();
        while (sym.hasMoreElements()) {
            String key = (String) sym.nextElement();
            Boolean stockRet = OptimizedTreeRenderer.selectedSymbols.get(key);
            OptimizedTreeRenderer.tempunselectedSymbols.put(key, true);
        }
        repaintTree();
        rootNode.clear();

        mainStore.clear();
        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exg = (Exchange) exchanges.nextElement();
                if (exg.isDefault()) {
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        Hashtable<String, OptimizedSubMarketNode> subMarketHash = new Hashtable<String, OptimizedSubMarketNode>();
                        for (Market subMarket : exg.getSubMarkets()) {
                            String subName = subMarket.getDescription();
                            //------ for submarkets---
                            OptimizedSubMarketNode subNode = new OptimizedSubMarketNode(subName, exg.getSymbol());
                            subNode.setExchange(exg.getSymbol());
                            subNode.setName(subName);
                            subNode.setSubMarket(subMarket.getMarketID());
                            Hashtable<String, OptimizedSymbolNode> marketHash = new Hashtable<String, OptimizedSymbolNode>();
                            Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                            String[] symbolList = subSymbols.getSymbols();
                            for (int i = 0; i < symbolList.length; i++) {
                                OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                sto.setKey(symbolList[i]);
                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(subName);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                sto.setType(Constants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    sto.setSelected(sto.isSelected());
//                                    sto.setSelected(st.isSelectedSymbol());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                            }
                            subNode.setChildrenStore(marketHash);
                            subMarketHash.put(subMarket.getMarketID(), subNode);
                            subMarket = null;
                        }
                        OptimizedExchangeNode tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());

                        tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        tempvector.setSubChildrenStore(subMarketHash);
                        rootNode.addNode(tempvector);

                    } else {

                        OptimizedExchangeNode tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        tempExgVector.setName(exg.getSymbol());
                        Hashtable<String, OptimizedSectorNode> sectorHash = new Hashtable<String, OptimizedSectorNode>();
                        try {
//                            Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());
                            Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());

                            if (sectorsEnum.hasMoreElements()) {
                                while (sectorsEnum.hasMoreElements()) {
                                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                    Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                    Sector sector = (Sector) sectorsEnum.nextElement();

                                    OptimizedSectorNode sn = new OptimizedSectorNode(sector.getDescription(), sector.getId());
                                    sn.setExchangeSymbol(sector.getId());
                                    sn.setExchange(exg.getSymbol());

                                    while (exchageSymbols.hasMoreElements()) {
                                        String symbol = (String) exchageSymbols.nextElement();
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));

                                        OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                        sto.setKey(exg.getSymbol() + "~" + symbol);
                                        sto.setExchange(exg.getSymbol());
                                        sto.setMarket(null);
                                        sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                        sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        sto.setType(Constants.symbolType);
                                        try {
                                            sto.setShortDescription(st.getShortDescription());
                                            sto.setOpenVal(st.getTodaysOpen());
                                            sto.setCloseVal(st.getTodaysClose());
                                            sto.setHighVal(st.getHigh());
                                            sto.setLowVal(st.getLow());
                                            sto.setDescription(st.getLongDescription());
//                                        st = null;
                                        } catch (Exception e) {
                                            sto.setShortDescription("");
                                            sto.setOpenVal(0.0);
                                            sto.setCloseVal(0.0);
                                            sto.setHighVal(0.0);
                                            sto.setLowVal(0.0);
                                            sto.setDescription("");
                                            sto.setSelected(sto.isSelected());
                                        }

                                        /* if (firstTimeLoad) {
                                            Stock stocks = DataStore.getSharedInstance().getStockObject(symbol);
                                            if (stocks != null) {
                                                OptimizedTreeRenderer.selectedSymbols.put(symbol, true);
                                                stocks.setSymbolEnabled(true);
                                            }
//                                        stocks.setSymbolEnabled(true);
                                        }*/
                                        try {
                                            if (sector.getId().equals(st.getSectorCode())) {
                                                exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                            }
                                        } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                        mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                    }

                                    sn.setChildrenStore(exgHash);
                                    sectorHash.put(sector.getId(), sn);
                                    sector = null;
                                }

                                /*  Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                Sector sector = (Sector) sectorsEnum.nextElement();

                                OptimizedSectorNode sn = new OptimizedSectorNode(sector.getDescription(), sector.getId());
                                sn.setExchangeSymbol(sector.getId());
                                sn.setExchange(exg.getSymbol());*/


                                tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                tempExgVector.setChildrenStore(sectorHash);
                            } else if (!sectorsEnum.hasMoreElements()) {
                                Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                while (exchageSymbols.hasMoreElements()) {
                                    String symbol = (String) exchageSymbols.nextElement();
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                    sto.setKey(exg.getSymbol() + "~" + symbol);
                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(null);
                                    sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                    sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    sto.setType(Constants.symbolType);
                                    try {
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
//                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                        sto.setSelected(sto.isSelected());
                                    }

                                    /*   if (firstTimeLoad) {
                                        Stock stocks = DataStore.getSharedInstance().getStockObject(exg.getSymbol() + "~" + symbol);
//                                        if (stocks.getInstrumentType() != 7) {
                                        OptimizedTreeRenderer.selectedSymbols.put(exg.getSymbol() + "~" + symbol, true);
//                                        }
                                        stocks.setSymbolEnabled(true);

                                    }*/
                                    try {
                                        exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);

                                    } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    }
                                    mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                }
                                tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                tempExgVector.setLeafChildrenStore(exgHash);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        rootNode.addNode(tempExgVector);
                    }
                }

            }
            try {
                Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
            } catch (Exception e) {
            }
            ToolTipManager.sharedInstance().registerComponent(tree);
            searchText.requestFocus(false);
            GUISettings.applyOrientation(mainPanel);
            GUISettings.applyOrientation(tree);
            GUISettings.applyOrientation(this);
            tree.updateUI();
            tree.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshTree() {
        refreshTree(rootNode.getSymbolStore());
        tree.updateUI();
    }

    private void refreshTree(Vector vector) {
        for (int i = 0; i < vector.size(); i++) {
            Object record = vector.get(i);
            if (record instanceof OptimizedSymbolNode) {
                OptimizedSymbolNode symbolNode = (OptimizedSymbolNode) record;
                Stock st = DataStore.getSharedInstance().getStockObject(symbolNode.getExchange(), symbolNode.getSymbol(), symbolNode.getInstrumentType());
                if (st != null) {
                    symbolNode.setShortDescription(st.getShortDescription());
                    symbolNode.setDescription(st.getLongDescription());
                }
            } else if (record instanceof OptimizedExchangeNode) {
                refreshTree(((OptimizedExchangeNode) record).getSymbolStore());
            }
        }
    }

    public void searchDefaultTree() {
        rootNode.clear();
        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exg = (Exchange) exchanges.nextElement();
                if (exg.isDefault()) {
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        Hashtable<String, OptimizedSubMarketNode> subMarketHash = new Hashtable<String, OptimizedSubMarketNode>();
                        for (Market subMarket : exg.getSubMarkets()) {
                            String subName = subMarket.getDescription();
                            //------ for submarkets---
                            OptimizedSubMarketNode subNode = new OptimizedSubMarketNode(subName, exg.getSymbol());
                            subNode.setExchange(exg.getSymbol());
                            subNode.setName(subName);
                            subNode.setSubMarket(subMarket.getMarketID());
                            Hashtable<String, OptimizedSymbolNode> marketHash = new Hashtable<String, OptimizedSymbolNode>();
                            Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                            String[] symbolList = subSymbols.getSymbols();
                            for (int i = 0; i < symbolList.length; i++) {
                                OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                sto.setKey(symbolList[i]);
                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(subName);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                sto.setType(Constants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    sto.setSelected(sto.isSelected());
//                                    sto.setSelected(st.isSelectedSymbol());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                            }
                            subNode.setChildrenStore(marketHash);
                            subMarketHash.put(subMarket.getMarketID(), subNode);
                            subMarket = null;
                        }
                        OptimizedExchangeNode tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());

                        tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        tempvector.setSubChildrenStore(subMarketHash);
                        rootNode.addNode(tempvector);

                    } else {

                        OptimizedExchangeNode tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        tempExgVector.setName(exg.getSymbol());
                        Hashtable<String, OptimizedSectorNode> sectorHash = new Hashtable<String, OptimizedSectorNode>();
                        try {
                            Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());
                            if (sectorsEnum.hasMoreElements()) {
                                while (sectorsEnum.hasMoreElements()) {
                                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                    Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                    Sector sector = (Sector) sectorsEnum.nextElement();

                                    OptimizedSectorNode sn = new OptimizedSectorNode(sector.getDescription(), sector.getId());
                                    sn.setExchangeSymbol(sector.getId());
                                    sn.setExchange(exg.getSymbol());

                                    while (exchageSymbols.hasMoreElements()) {
                                        String symbol = (String) exchageSymbols.nextElement();
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));

                                        OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                        sto.setKey(exg.getSymbol() + "~" + symbol);
                                        sto.setExchange(exg.getSymbol());
                                        sto.setMarket(null);
                                        sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                        sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        sto.setType(Constants.symbolType);
                                        try {
                                            sto.setShortDescription(st.getShortDescription());
                                            sto.setOpenVal(st.getTodaysOpen());
                                            sto.setCloseVal(st.getTodaysClose());
                                            sto.setHighVal(st.getHigh());
                                            sto.setLowVal(st.getLow());
                                            sto.setDescription(st.getLongDescription());
//                                        st = null;
                                        } catch (Exception e) {
                                            sto.setShortDescription("");
                                            sto.setOpenVal(0.0);
                                            sto.setCloseVal(0.0);
                                            sto.setHighVal(0.0);
                                            sto.setLowVal(0.0);
                                            sto.setDescription("");
                                            sto.setSelected(sto.isSelected());
                                        }
                                        /* if (firstTimeLoad) {
                                            Stock stocks = DataStore.getSharedInstance().getStockObject(symbol);
                                            if (stocks != null) {
                                                OptimizedTreeRenderer.selectedSymbols.put(symbol, true);
                                                stocks.setSymbolEnabled(true);
                                            }
//                                        stocks.setSymbolEnabled(true);
                                        }*/
                                        try {
                                            if (sector.getId().equals(st.getSectorCode())) {
                                                exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                            }
                                        } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                        mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                    }
                                    sn.setChildrenStore(exgHash);
                                    sectorHash.put(sector.getId(), sn);
                                    sector = null;
                                }
                                tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                tempExgVector.setChildrenStore(sectorHash);
                            } else if (!sectorsEnum.hasMoreElements()) {
                                Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                while (exchageSymbols.hasMoreElements()) {
                                    String symbol = (String) exchageSymbols.nextElement();
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                    sto.setKey(exg.getSymbol() + "~" + symbol);
                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(null);
                                    sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                    sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    sto.setType(Constants.symbolType);
                                    try {
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
//                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                        sto.setSelected(sto.isSelected());
                                    }

                                    /* if (firstTimeLoad) {
                                        Stock stocks = DataStore.getSharedInstance().getStockObject(exg.getSymbol() + "~" + symbol);
//                                        if (stocks.getInstrumentType() != 7) {
                                        OptimizedTreeRenderer.selectedSymbols.put(exg.getSymbol() + "~" + symbol, true);
//                                        }
                                        stocks.setSymbolEnabled(true);

                                    }*/
                                    try {
                                        exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);

                                    } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    }
                                    mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                }
                                tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                tempExgVector.setLeafChildrenStore(exgHash);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        rootNode.addNode(tempExgVector);
                    }
                }

            }
            try {
                Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
            } catch (Exception e) {
            }
            ToolTipManager.sharedInstance().registerComponent(tree);
            searchText.requestFocus(false);
            GUISettings.applyOrientation(mainPanel);
            GUISettings.applyOrientation(tree);
            tree.updateUI();
            tree.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchEnabledTree() {

        rootNode.clear();
        try {

            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exg = (Exchange) exchanges.nextElement();
                boolean ismatched = false;
                if (exg.isDefault()) {
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        OptimizedExchangeNode exgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        Hashtable<String, OptimizedSubMarketNode> subMarketHash = new Hashtable<String, OptimizedSubMarketNode>();
                        for (Market subMarket : exg.getSubMarkets()) {
                            String subName = subMarket.getDescription();
                            //------ for submarkets---
                            OptimizedSubMarketNode subNode = new OptimizedSubMarketNode(subName, exg.getSymbol());
                            subNode.setExchange(exg.getSymbol());
                            subNode.setName(subName);
                            subNode.setSubMarket(subMarket.getMarketID());
                            Hashtable<String, OptimizedSymbolNode> marketHash = new Hashtable<String, OptimizedSymbolNode>();
                            Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                            String[] symbolList = subSymbols.getSymbols();
                            boolean isSubMarketMatch = false;
                            for (int i = 0; i < symbolList.length; i++) {
                                OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                sto.setKey(symbolList[i]);

                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(subName);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbolList[i]));
                                sto.setType(Constants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbolList[i]), SharedMethods.getInstrumentFromExchangeKey(symbolList[i]));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    sto.setSelected(sto.isSelected());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }

                                if (OptimizedTreeRenderer.selectedSymbols.containsKey(sto.getKey())) {
                                    isSubMarketMatch = true;
                                    marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                }
                            }
                            if (isSubMarketMatch) {
                                ismatched = true;
                                subNode.setChildrenStore(marketHash);
                                subMarketHash.put(subMarket.getMarketID(), subNode);
                            }
                        }
                        if (ismatched) {
                            OptimizedExchangeNode tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());

                            exgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                            exgVector.setSubChildrenStore(subMarketHash);
                            rootNode.addNode(exgVector);
                        }

                    } else {
                        OptimizedExchangeNode tempExgVector = new OptimizedExchangeNode();
                        tempExgVector.setName(exg.getSymbol());

                        Hashtable<String, OptimizedSectorNode> sectorHash = new Hashtable<String, OptimizedSectorNode>();
                        try {
                            Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());
                            if (sectorsEnum.hasMoreElements()) {
                                while (sectorsEnum.hasMoreElements()) {
                                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                    Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                    Sector sector = (Sector) sectorsEnum.nextElement();
                                    OptimizedSectorNode sn = new OptimizedSectorNode(sector.getDescription(), sector.getId());
                                    sn.setExchangeSymbol(sector.getId());
                                    sn.setExchange(exg.getSymbol());
                                    boolean isSectorMatch = false;
                                    while (exchageSymbols.hasMoreElements()) {
                                        String symbol = (String) exchageSymbols.nextElement();
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                        sto.setKey(exg.getSymbol() + "~" + symbol);

                                        sto.setExchange(exg.getSymbol());
                                        sto.setMarket(null);
                                        sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                        sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        sto.setType(Constants.symbolType);
                                        try {
                                            sto.setShortDescription(st.getShortDescription());
                                            sto.setOpenVal(st.getTodaysOpen());
                                            sto.setCloseVal(st.getTodaysClose());
                                            sto.setHighVal(st.getHigh());
                                            sto.setLowVal(st.getLow());
                                            sto.setDescription(st.getLongDescription());
//                                        st = null;
                                        } catch (Exception e) {
                                            sto.setShortDescription("");
                                            sto.setOpenVal(0.0);
                                            sto.setCloseVal(0.0);
                                            sto.setHighVal(0.0);
                                            sto.setLowVal(0.0);
                                            sto.setDescription("");
                                            sto.setSelected(sto.isSelected());
                                        }
                                        try {
                                            if (OptimizedTreeRenderer.selectedSymbols.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                                if (sector.getId().equals(st.getSectorCode())) {
                                                    isSectorMatch = true;
                                                    exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                                }
                                            }
                                        } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                    }
                                    if (isSectorMatch) {
                                        ismatched = true;
                                        sn.setChildrenStore(exgHash);
                                        sectorHash.put(sector.getId(), sn);
                                    }
                                }
                                if (ismatched) {
                                    tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                    tempExgVector.setChildrenStore(sectorHash);
                                }
                            } else if (!sectorsEnum.hasMoreElements()) {
                                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();

                                while (exchageSymbols.hasMoreElements()) {
                                    String symbol = (String) exchageSymbols.nextElement();
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                    sto.setKey(exg.getSymbol() + "~" + symbol);
                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(null);
                                    sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                    sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    sto.setType(Constants.symbolType);
                                    try {
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
//                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                        sto.setSelected(sto.isSelected());
                                    }
                                    try {
                                        if (OptimizedTreeRenderer.selectedSymbols.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                            ismatched = true;
                                            exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                        }
                                    } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    }
                                }
                                if (ismatched) {
                                    tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                    tempExgVector.setLeafChildrenStore(exgHash);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        if (ismatched) {
                            rootNode.addNode(tempExgVector);
                        }
                    }
                }
            }
            try {
                Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {

            } catch (Exception e) {

            }
            tree.repaint();
            tree.updateUI();
//            for (int i = 0; i < tree.getRowCount(); i++) {
//                tree.expandRow(i);
//            }
            ToolTipManager.sharedInstance().registerComponent(tree);
            GUISettings.applyOrientation(mainPanel);
//                searchText.requestFocus(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchDisabledTree() {
        rootNode.clear();
        try {

            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exg = (Exchange) exchanges.nextElement();
                boolean ismatched = false;
                if (exg.isDefault()) {
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        OptimizedExchangeNode exgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        Hashtable<String, OptimizedSubMarketNode> subMarketHash = new Hashtable<String, OptimizedSubMarketNode>();
                        for (Market subMarket : exg.getSubMarkets()) {
                            String subName = subMarket.getDescription();
                            //------ for submarkets---
                            OptimizedSubMarketNode subNode = new OptimizedSubMarketNode(subName, exg.getSymbol());
                            subNode.setExchange(exg.getSymbol());
                            subNode.setName(subName);
                            subNode.setSubMarket(subMarket.getMarketID());
                            Hashtable<String, OptimizedSymbolNode> marketHash = new Hashtable<String, OptimizedSymbolNode>();
                            Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                            String[] symbolList = subSymbols.getSymbols();
                            boolean isSubMarketMatch = false;
                            for (int i = 0; i < symbolList.length; i++) {
                                OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                sto.setKey(symbolList[i]);

                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(subName);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbolList[i]));
                                sto.setType(Constants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbolList[i]), SharedMethods.getInstrumentFromExchangeKey(symbolList[i]));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    sto.setSelected(sto.isSelected());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }

                                if (!(OptimizedTreeRenderer.selectedSymbols.containsKey(sto.getKey()))) {
                                    isSubMarketMatch = true;
                                    marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                }
                            }
                            if (isSubMarketMatch) {
                                ismatched = true;
                                subNode.setChildrenStore(marketHash);
                                subMarketHash.put(subMarket.getMarketID(), subNode);
                            }
                        }
                        if (ismatched) {
                            OptimizedExchangeNode tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());

                            exgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                            exgVector.setSubChildrenStore(subMarketHash);
                            rootNode.addNode(exgVector);
                        }

                    } else {
                        OptimizedExchangeNode tempExgVector = new OptimizedExchangeNode();
                        tempExgVector.setName(exg.getSymbol());

                        Hashtable<String, OptimizedSectorNode> sectorHash = new Hashtable<String, OptimizedSectorNode>();
                        try {
                            Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());
                            if (sectorsEnum.hasMoreElements()) {
                                while (sectorsEnum.hasMoreElements()) {
                                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                    Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                    Sector sector = (Sector) sectorsEnum.nextElement();
                                    OptimizedSectorNode sn = new OptimizedSectorNode(sector.getDescription(), sector.getId());
                                    sn.setExchangeSymbol(sector.getId());
                                    sn.setExchange(exg.getSymbol());
                                    boolean isSectorMatch = false;
                                    while (exchageSymbols.hasMoreElements()) {
                                        String symbol = (String) exchageSymbols.nextElement();
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                        sto.setKey(exg.getSymbol() + "~" + symbol);

                                        sto.setExchange(exg.getSymbol());
                                        sto.setMarket(null);
                                        sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                        sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        sto.setType(Constants.symbolType);
                                        try {
                                            sto.setShortDescription(st.getShortDescription());
                                            sto.setOpenVal(st.getTodaysOpen());
                                            sto.setCloseVal(st.getTodaysClose());
                                            sto.setHighVal(st.getHigh());
                                            sto.setLowVal(st.getLow());
                                            sto.setDescription(st.getLongDescription());
//                                        st = null;
                                        } catch (Exception e) {
                                            sto.setShortDescription("");
                                            sto.setOpenVal(0.0);
                                            sto.setCloseVal(0.0);
                                            sto.setHighVal(0.0);
                                            sto.setLowVal(0.0);
                                            sto.setDescription("");
                                            sto.setSelected(sto.isSelected());
                                        }
                                        try {
                                            if (!(OptimizedTreeRenderer.selectedSymbols.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType())))) {
                                                if (sector.getId().equals(st.getSectorCode())) {
                                                    isSectorMatch = true;
                                                    exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                                }
                                            }
                                        } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                    }
                                    if (isSectorMatch) {
                                        ismatched = true;
                                        sn.setChildrenStore(exgHash);
                                        sectorHash.put(sector.getId(), sn);
                                    }
                                }
                                if (ismatched) {
                                    tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                    tempExgVector.setChildrenStore(sectorHash);
                                }
                            } else if (!sectorsEnum.hasMoreElements()) {
                                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();

                                while (exchageSymbols.hasMoreElements()) {
                                    String symbol = (String) exchageSymbols.nextElement();
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                    sto.setKey(exg.getSymbol() + "~" + symbol);
                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(null);
                                    sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                    sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    sto.setType(Constants.symbolType);
                                    try {
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
//                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                        sto.setSelected(sto.isSelected());
                                    }
                                    try {
                                        if (!(OptimizedTreeRenderer.selectedSymbols.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType())))) {
                                            ismatched = true;
                                            exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                        }
                                    } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    }
                                }
                                if (ismatched) {
                                    tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                    tempExgVector.setLeafChildrenStore(exgHash);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        if (ismatched) {
                            rootNode.addNode(tempExgVector);
                        }
                    }
                }
            }
            try {
                Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {

            } catch (Exception e) {

            }
            tree.repaint();
            tree.updateUI();
//            for (int i = 0; i < tree.getRowCount(); i++) {
//                tree.expandRow(i);
//            }
            ToolTipManager.sharedInstance().registerComponent(tree);
            GUISettings.applyOrientation(mainPanel);
//                searchText.requestFocus(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchTree(String criteria) {
        if ((!criteria.equals(""))) {
            Enumeration mainStoreenum = mainStore.elements();
            filteredStore.clear();
            while (mainStoreenum.hasMoreElements()) {
                OptimizedSymbolNode sto = (OptimizedSymbolNode) mainStoreenum.nextElement();
                if ((sto.getSymbol().contains(criteria.toUpperCase())) || (sto.getDescription().contains(criteria.substring(0, 1).toUpperCase() + criteria.substring(1))) || (sto.getDescription().contains(criteria))) {
                    filteredStore.put(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()), sto);
                }
            }
            rootNode.clear();
            try {

                Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
                while (exchanges.hasMoreElements()) {
                    Exchange exg = (Exchange) exchanges.nextElement();
                    boolean ismatched = false;
                    if (exg.isDefault()) {
                        if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                            OptimizedExchangeNode exgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                            Hashtable<String, OptimizedSubMarketNode> subMarketHash = new Hashtable<String, OptimizedSubMarketNode>();
                            for (Market subMarket : exg.getSubMarkets()) {
                                String subName = subMarket.getDescription();
                                //------ for submarkets---
                                OptimizedSubMarketNode subNode = new OptimizedSubMarketNode(subName, exg.getSymbol());
                                subNode.setExchange(exg.getSymbol());
                                subNode.setName(subName);
                                subNode.setSubMarket(subMarket.getMarketID());
                                Hashtable<String, OptimizedSymbolNode> marketHash = new Hashtable<String, OptimizedSymbolNode>();
                                Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                                String[] symbolList = subSymbols.getSymbols();
                                boolean isSubMarketMatch = false;
                                for (int i = 0; i < symbolList.length; i++) {
                                    OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                    sto.setKey(symbolList[i]);

                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(subName);
                                    sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                    sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbolList[i]));
                                    sto.setType(Constants.symbolType);
                                    try {
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbolList[i]), SharedMethods.getInstrumentFromExchangeKey(symbolList[i]));
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
                                        sto.setSelected(sto.isSelected());
                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                    }

                                    if (filteredStore.containsKey(sto.getKey())) {
                                        isSubMarketMatch = true;
                                        marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                    }
                                }
                                if (isSubMarketMatch) {
                                    ismatched = true;
                                    subNode.setChildrenStore(marketHash);
                                    subMarketHash.put(subMarket.getMarketID(), subNode);
                                }
                            }
                            if (ismatched) {
                                OptimizedExchangeNode tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());

                                exgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                exgVector.setSubChildrenStore(subMarketHash);
                                rootNode.addNode(exgVector);
                            }

                        } else {
                            OptimizedExchangeNode tempExgVector = new OptimizedExchangeNode();
                            tempExgVector.setName(exg.getSymbol());

                            Hashtable<String, OptimizedSectorNode> sectorHash = new Hashtable<String, OptimizedSectorNode>();
                            try {
                                Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());
                                if (sectorsEnum.hasMoreElements()) {
                                    while (sectorsEnum.hasMoreElements()) {
                                        Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                        Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                        Sector sector = (Sector) sectorsEnum.nextElement();
                                        OptimizedSectorNode sn = new OptimizedSectorNode(sector.getDescription(), sector.getId());
                                        sn.setExchangeSymbol(sector.getId());
                                        sn.setExchange(exg.getSymbol());
                                        boolean isSectorMatch = false;
                                        while (exchageSymbols.hasMoreElements()) {
                                            String symbol = (String) exchageSymbols.nextElement();
                                            Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                            OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                            sto.setKey(exg.getSymbol() + "~" + symbol);

                                            sto.setExchange(exg.getSymbol());
                                            sto.setMarket(null);
                                            sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                            sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                            sto.setType(Constants.symbolType);
                                            try {
                                                sto.setShortDescription(st.getShortDescription());
                                                sto.setOpenVal(st.getTodaysOpen());
                                                sto.setCloseVal(st.getTodaysClose());
                                                sto.setHighVal(st.getHigh());
                                                sto.setLowVal(st.getLow());
                                                sto.setDescription(st.getLongDescription());
//                                        st = null;
                                            } catch (Exception e) {
                                                sto.setShortDescription("");
                                                sto.setOpenVal(0.0);
                                                sto.setCloseVal(0.0);
                                                sto.setHighVal(0.0);
                                                sto.setLowVal(0.0);
                                                sto.setDescription("");
                                                sto.setSelected(sto.isSelected());
                                            }
                                            try {
                                                if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                                    if (sector.getId().equals(st.getSectorCode())) {
                                                        isSectorMatch = true;
                                                        exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                                    }
                                                }
                                            } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                            }
                                        }
                                        if (isSectorMatch) {
                                            ismatched = true;
                                            sn.setChildrenStore(exgHash);
                                            sectorHash.put(sector.getId(), sn);
                                        }
                                    }
                                    if (ismatched) {
                                        tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                        tempExgVector.setChildrenStore(sectorHash);
                                    }
                                } else if (!sectorsEnum.hasMoreElements()) {
                                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                    Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();

                                    while (exchageSymbols.hasMoreElements()) {
                                        String symbol = (String) exchageSymbols.nextElement();
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                        sto.setKey(exg.getSymbol() + "~" + symbol);
                                        sto.setExchange(exg.getSymbol());
                                        sto.setMarket(null);
                                        sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                        sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        sto.setType(Constants.symbolType);
                                        try {
                                            sto.setShortDescription(st.getShortDescription());
                                            sto.setOpenVal(st.getTodaysOpen());
                                            sto.setCloseVal(st.getTodaysClose());
                                            sto.setHighVal(st.getHigh());
                                            sto.setLowVal(st.getLow());
                                            sto.setDescription(st.getLongDescription());
//                                        st = null;
                                        } catch (Exception e) {
                                            sto.setShortDescription("");
                                            sto.setOpenVal(0.0);
                                            sto.setCloseVal(0.0);
                                            sto.setHighVal(0.0);
                                            sto.setLowVal(0.0);
                                            sto.setDescription("");
                                            sto.setSelected(sto.isSelected());
                                        }
                                        try {
                                            if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                                ismatched = true;
                                                exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                            }
                                        } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                    }
                                    if (ismatched) {
                                        tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                        tempExgVector.setLeafChildrenStore(exgHash);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                            if (ismatched) {
                                rootNode.addNode(tempExgVector);
                            }
                        }
                    }
                }
                try {
                    Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                try {

                } catch (Exception e) {

                }
                tree.repaint();
                tree.updateUI();
                for (int i = 0; i < tree.getRowCount(); i++) {
                    tree.expandRow(i);
                }
                ToolTipManager.sharedInstance().registerComponent(tree);
                GUISettings.applyOrientation(mainPanel);
                searchText.requestFocus(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rootNode.clear();
            loadTree();
            tree.updateUI();
            tree.repaint();
            scrollPane.updateUI();
            mainPanel.updateUI();
            this.updateUI();
            searchText.requestFocus(false);
        }
    }

    public void enabledTree() {

        mainStore.clear();
        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exg = (Exchange) exchanges.nextElement();
                if (exg.isDefault()) {
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        Hashtable<String, OptimizedSubMarketNode> subMarketHash = new Hashtable<String, OptimizedSubMarketNode>();
                        for (Market subMarket : exg.getSubMarkets()) {
                            String subName = subMarket.getDescription();
                            //------ for submarkets---
                            OptimizedSubMarketNode subNode = new OptimizedSubMarketNode(subName, exg.getSymbol());
                            subNode.setExchange(exg.getSymbol());
                            subNode.setName(subName);
                            subNode.setSubMarket(subMarket.getMarketID());
                            Hashtable<String, OptimizedSymbolNode> marketHash = new Hashtable<String, OptimizedSymbolNode>();
                            Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                            String[] symbolList = subSymbols.getSymbols();
                            for (int i = 0; i < symbolList.length; i++) {
                                OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                sto.setKey(symbolList[i]);
                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(subName);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                sto.setType(Constants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    sto.setSelected(sto.isSelected());
//                                    sto.setSelected(st.isSelectedSymbol());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                if (OptimizedTreeRenderer.selectedSymbols.containsKey(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])))) {
                                    marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                }
                            }
                            if (marketHash.size() != 0) {
                                subNode.setChildrenStore(marketHash);
                            }
                            if (subNode.getChildCount() != 0) {
                                subMarketHash.put(subMarket.getMarketID(), subNode);
                            }

                            subMarket = null;
                        }
                        OptimizedExchangeNode tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());

                        tempvector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        if (subMarketHash.size() != 0) {
                            tempvector.setSubChildrenStore(subMarketHash);
                        }
                        if (tempvector.getChildCount() != 0) {
                            rootNode.addNode(tempvector);
                        }

                    } else {

                        OptimizedExchangeNode tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        tempExgVector.setName(exg.getSymbol());
                        Hashtable<String, OptimizedSectorNode> sectorHash = new Hashtable<String, OptimizedSectorNode>();
                        try {
                            Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exg.getSymbol());
                            if (sectorsEnum.hasMoreElements()) {
                                while (sectorsEnum.hasMoreElements()) {
                                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                    Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                    Sector sector = (Sector) sectorsEnum.nextElement();

                                    OptimizedSectorNode sn = new OptimizedSectorNode(sector.getDescription(), sector.getId());
                                    sn.setExchangeSymbol(sector.getId());
                                    sn.setExchange(exg.getSymbol());

                                    while (exchageSymbols.hasMoreElements()) {
                                        String symbol = (String) exchageSymbols.nextElement();
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));

                                        OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                        sto.setKey(exg.getSymbol() + "~" + symbol);
                                        sto.setExchange(exg.getSymbol());
                                        sto.setMarket(null);
                                        sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                        sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        sto.setType(Constants.symbolType);
                                        try {
                                            sto.setShortDescription(st.getShortDescription());
                                            sto.setOpenVal(st.getTodaysOpen());
                                            sto.setCloseVal(st.getTodaysClose());
                                            sto.setHighVal(st.getHigh());
                                            sto.setLowVal(st.getLow());
                                            sto.setDescription(st.getLongDescription());
//                                        st = null;
                                        } catch (Exception e) {
                                            sto.setShortDescription("");
                                            sto.setOpenVal(0.0);
                                            sto.setCloseVal(0.0);
                                            sto.setHighVal(0.0);
                                            sto.setLowVal(0.0);
                                            sto.setDescription("");
                                            sto.setSelected(sto.isSelected());
                                        }
                                        try {
                                            if (sector.getId().equals(st.getSectorCode())) {
                                                if (OptimizedTreeRenderer.selectedSymbols.containsKey(st.getKey())) {
                                                    exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                                }
                                            }
                                        } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                        mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                    }
                                    if (exgHash.size() != 0) {
                                        sn.setChildrenStore(exgHash);
                                        sectorHash.put(sector.getId(), sn);
                                    }
                                    sector = null;
                                }
                                tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                if (sectorHash.size() != 0) {
                                    tempExgVector.setChildrenStore(sectorHash);
                                }
                            } else if (!sectorsEnum.hasMoreElements()) {
                                Hashtable<String, OptimizedSymbolNode> exgHash = new Hashtable<String, OptimizedSymbolNode>();
                                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                                while (exchageSymbols.hasMoreElements()) {
                                    String symbol = (String) exchageSymbols.nextElement();
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    OptimizedSymbolNode sto = new OptimizedSymbolNode();
                                    sto.setKey(exg.getSymbol() + "~" + symbol);
                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(null);
                                    sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                                    sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    sto.setType(Constants.symbolType);
                                    try {
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
//                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                        sto.setSelected(sto.isSelected());
                                    }

                                    /*if (firstTimeLoad) {
                                        OptimizedTreeRenderer.selectedSymbols.put(exg.getSymbol() + "~" + symbol, true);
                                        Stock stocks = DataStore.getSharedInstance().getStockObject(exg.getSymbol() + "~" + symbol);
                                        stocks.setSymbolEnabled(true);
                                    }*/
                                    try {
                                        if (OptimizedTreeRenderer.selectedSymbols.containsKey(st.getKey())) {

                                            exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                        }

                                    } catch (Exception e) {
//                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    }
                                    mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                }
                                tempExgVector = new OptimizedExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                if (exgHash.size() != 0) {
                                    tempExgVector.setLeafChildrenStore(exgHash);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        if (tempExgVector.getChildCount() != 0) {
                            rootNode.addNode(tempExgVector);
                        }
                    }
                }

            }
            try {
                Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
            } catch (Exception e) {
            }
            ToolTipManager.sharedInstance().registerComponent(tree);
            searchText.requestFocus(false);
            GUISettings.applyOrientation(mainPanel);
            GUISettings.applyOrientation(tree);
            tree.updateUI();
            tree.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireSymbolAddedEvent(String key, String id) {
        try {
            OptimizedSymbolNode sto = new OptimizedSymbolNode();
            sto.setExchange(SharedMethods.getExchangeFromKey(key));
            sto.setMarket(null);
            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
            sto.setType(Constants.symbolType);
            try {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                sto.setKey(key);
                sto.setShortDescription(st.getShortDescription());
                sto.setOpenVal(st.getTodaysOpen());
                sto.setCloseVal(st.getTodaysClose());
                sto.setHighVal(st.getHigh());
                sto.setLowVal(st.getLow());
                sto.setDescription(st.getLongDescription());
                sto.setSelected(sto.isSelected());
                st = null;
            } catch (Exception e) {
                sto.setShortDescription("");
                sto.setOpenVal(0.0);
                sto.setCloseVal(0.0);
                sto.setHighVal(0.0);
                sto.setLowVal(0.0);
                sto.setDescription("");
            }
            mainStore.put(key, sto);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    tree.updateUI();
                    tree.repaint();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void fireSymbolRemovedEvent(String key, String id) {
        try {
            OptimizedSymbolNode sto = new OptimizedSymbolNode();
            sto.setKey(key);
            sto.setExchange(SharedMethods.getExchangeFromKey(key));
            sto.setMarket(null);
            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
            sto.setType(Constants.symbolType);
            try {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                sto.setShortDescription(st.getShortDescription());
                sto.setOpenVal(st.getTodaysOpen());
                sto.setCloseVal(st.getTodaysClose());
                sto.setHighVal(st.getHigh());
                sto.setLowVal(st.getLow());
                sto.setDescription(st.getLongDescription());
                sto.setSelected(sto.isSelected());

                st = null;
            } catch (Exception e) {
                sto.setShortDescription("");
                sto.setOpenVal(0.0);
                sto.setCloseVal(0.0);
                sto.setHighVal(0.0);
                sto.setLowVal(0.0);
                sto.setDescription("");
            }
            mainStore.put(key, sto);
            (watchListReference.get(id)).removeNode(sto);
            tree.updateUI();
            tree.repaint();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(searchButton)) {
            searchTree(searchText.getText());
        } else if (e.getActionCommand().equals("HIDE")) {
            // super.actionPerformed(e);
        } else if (e.getActionCommand().equals("OK")) {
            this.setVisible(false);
            firstTimeLoad = false;
            searchText.setText("");
            requestBuilder();
        } else if (e.getActionCommand().equals("ENABLE")) {
            searchEnabledTree();
        } else if (e.getActionCommand().equals("DISABLE")) {
            searchDisabledTree();
        } else if (e.getActionCommand().equals("CANCEL")) {
            this.setVisible(false);

            if (!Client.IS_BANDOP_VISIBLE) {
                Enumeration bulkKey = OptimizedTreeRenderer.tempunselectedSymbols.keys();
                while (bulkKey.hasMoreElements()) {
                    String symbol = (String) bulkKey.nextElement();
                    Boolean state = OptimizedTreeRenderer.tempunselectedSymbols.get(bulkKey);
                    if (!OptimizedTreeRenderer.selectedSymbols.containsKey(symbol)) {
                        OptimizedTreeRenderer.selectedSymbols.put(symbol, true);
                        Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                        st.setSymbolEnabled(true);
                    }
                }
                Enumeration sym = OptimizedTreeRenderer.selectedSymbols.keys();
                while (sym.hasMoreElements()) {
                    String symbol = (String) sym.nextElement();
                    Boolean state = OptimizedTreeRenderer.selectedSymbols.get(bulkKey);
                    if (!OptimizedTreeRenderer.tempunselectedSymbols.containsKey(symbol)) {
                        OptimizedTreeRenderer.selectedSymbols.remove(symbol);
                        Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                        st.setSymbolEnabled(false);
                    }
                }

                //-------------- For symbols ------------

                Enumeration bulksec = OptimizedTreeRenderer.tempunselectedSubMarkets.keys();
                while (bulksec.hasMoreElements()) {
                    String symbol = (String) bulksec.nextElement();
                    Boolean state = OptimizedTreeRenderer.tempunselectedSubMarkets.get(bulkKey);
                    if (!OptimizedTreeRenderer.selectedSubMarkets.containsKey(symbol)) {
                        OptimizedTreeRenderer.selectedSubMarkets.put(symbol, true);
                        Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                        st.setSymbolEnabled(true);
                    }
                }
                Enumeration symsec = OptimizedTreeRenderer.selectedSubMarkets.keys();
                while (symsec.hasMoreElements()) {
                    String symbol = (String) symsec.nextElement();
                    Boolean state = OptimizedTreeRenderer.selectedSubMarkets.get(bulkKey);
                    if (!OptimizedTreeRenderer.tempunselectedSubMarkets.containsKey(symbol)) {
                        OptimizedTreeRenderer.selectedSubMarkets.remove(symbol);
                        Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                        st.setSymbolEnabled(false);
                    }
                }

                //------- For subMarkets ------------
                Enumeration bulksub = OptimizedTreeRenderer.tempunselectedSectors.keys();
                while (bulksub.hasMoreElements()) {
                    String symbol = (String) bulksub.nextElement();
                    Boolean state = OptimizedTreeRenderer.tempunselectedSectors.get(bulkKey);
                    if (!OptimizedTreeRenderer.selectedSectors.containsKey(symbol)) {
                        OptimizedTreeRenderer.selectedSectors.put(symbol, true);
                        Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                        try {
                            if (st != null) {
                                st.setSymbolEnabled(true);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                }
                Enumeration symsub = OptimizedTreeRenderer.selectedSectors.keys();
                while (symsub.hasMoreElements()) {
                    String symbol = (String) symsub.nextElement();
                    Boolean state = OptimizedTreeRenderer.selectedSectors.get(bulkKey);
                    if (!OptimizedTreeRenderer.tempunselectedSectors.containsKey(symbol)) {
                        OptimizedTreeRenderer.selectedSectors.remove(symbol);
                    }

                }

                //---------- For Sectors --------

                Enumeration bulkex = OptimizedTreeRenderer.tempunselectedExchanges.keys();
                while (bulkex.hasMoreElements()) {
                    String symbol = (String) bulkex.nextElement();
                    Boolean state = OptimizedTreeRenderer.tempunselectedExchanges.get(bulkKey);
                    if (!OptimizedTreeRenderer.selectedExchanges.containsKey(symbol)) {
                        OptimizedTreeRenderer.selectedExchanges.put(symbol, true);
                        Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                        st.setSymbolEnabled(true);
                    }
                }
                Enumeration symex = OptimizedTreeRenderer.selectedExchanges.keys();
                while (symex.hasMoreElements()) {
                    String symbol = (String) symex.nextElement();
                    Boolean state = OptimizedTreeRenderer.selectedExchanges.get(bulkKey);
                    if (!OptimizedTreeRenderer.tempunselectedExchanges.containsKey(symbol)) {
                        OptimizedTreeRenderer.selectedExchanges.remove(symbol);
                        Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                        st.setSymbolEnabled(false);
                    }

                }

                // ---------- For Exchanges -----------------
            }
//            self = null;
            Client.IS_BANDOP_VISIBLE = false;
        }

    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(searchText)) {
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                if (!searchText.getText().equals("")) {
                    searchTree(searchText.getText());
                    e.consume();
                    searchText.requestFocus(true);
                } else {
                    rootNode.clear();
                    e.consume();
                    setInitialText();
                }

            } else if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                if (searchText.getText().equals("")) {
                    rootNode.clear();
                    loadTree();
                    e.consume();
                    setInitialText();
                }

            } else if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
                rootNode.clear();
                loadTree();
                e.consume();
                setInitialText();
            }
        } else {
            searchText.requestFocus();
            try {
                if (!Character.isWhitespace(e.getKeyChar()) && (!(e.getKeyChar() == KeyEvent.VK_ESCAPE))) {
                    searchText.getDocument().insertString(searchText.getCaretPosition(), "" + e.getKeyChar(), null);
                }
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void setInitialText() {
        searchText.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchText.setSelectionStart(0);
        searchText.setSelectionEnd(searchText.getText().length());
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.


    }

    private JPopupMenu getPopupMenu1() {
        if (symbolTreePopup == null) {
            symbolTreePopup = Client.getInstance().getTablePopup();
            symbolTreePopup.addPopupMenuListener(this);
        }
        return symbolTreePopup;
    }

    private void validatePopup() {

    }

    public void internalFrameClosing(InternalFrameEvent e) {
        this.setVisible(false);
//        this.dispose();
//        self = null;
//        self = null;
        if (!Client.IS_BANDOP_VISIBLE) {
            Enumeration bulkKey = OptimizedTreeRenderer.tempunselectedSymbols.keys();
            while (bulkKey.hasMoreElements()) {
                String symbol = (String) bulkKey.nextElement();
                Boolean state = OptimizedTreeRenderer.tempunselectedSymbols.get(bulkKey);
                if (!OptimizedTreeRenderer.selectedSymbols.containsKey(symbol)) {
                    OptimizedTreeRenderer.selectedSymbols.put(symbol, true);
                    Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                    st.setSymbolEnabled(true);
                }
            }
            Enumeration sym = OptimizedTreeRenderer.selectedSymbols.keys();
            while (sym.hasMoreElements()) {
                String symbol = (String) sym.nextElement();
                Boolean state = OptimizedTreeRenderer.selectedSymbols.get(bulkKey);
                if (!OptimizedTreeRenderer.tempunselectedSymbols.containsKey(symbol)) {
                    OptimizedTreeRenderer.selectedSymbols.remove(symbol);
                    Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                    st.setSymbolEnabled(false);
                }
            }

            //-------------- For symbols ------------

            Enumeration bulksec = OptimizedTreeRenderer.tempunselectedSubMarkets.keys();
            while (bulksec.hasMoreElements()) {
                String symbol = (String) bulksec.nextElement();
                Boolean state = OptimizedTreeRenderer.tempunselectedSubMarkets.get(bulkKey);
                if (!OptimizedTreeRenderer.selectedSubMarkets.containsKey(symbol)) {
                    OptimizedTreeRenderer.selectedSubMarkets.put(symbol, true);
                    Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                    st.setSymbolEnabled(true);
                }
            }
            Enumeration symsec = OptimizedTreeRenderer.selectedSubMarkets.keys();
            while (symsec.hasMoreElements()) {
                String symbol = (String) symsec.nextElement();
                Boolean state = OptimizedTreeRenderer.selectedSubMarkets.get(bulkKey);
                if (!OptimizedTreeRenderer.tempunselectedSubMarkets.containsKey(symbol)) {
                    OptimizedTreeRenderer.selectedSubMarkets.remove(symbol);
                    Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                    st.setSymbolEnabled(false);
                }
            }

            //------- For subMarkets ------------
            Enumeration bulksub = OptimizedTreeRenderer.tempunselectedSectors.keys();
            while (bulksub.hasMoreElements()) {
                String symbol = (String) bulksub.nextElement();
                Boolean state = OptimizedTreeRenderer.tempunselectedSectors.get(bulkKey);
                if (!OptimizedTreeRenderer.selectedSectors.containsKey(symbol)) {
                    OptimizedTreeRenderer.selectedSectors.put(symbol, true);
                    Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                    st.setSymbolEnabled(true);
                }
            }
            Enumeration symsub = OptimizedTreeRenderer.selectedSectors.keys();
            while (symsub.hasMoreElements()) {
                String symbol = (String) symsub.nextElement();
                Boolean state = OptimizedTreeRenderer.selectedSectors.get(bulkKey);
                if (!OptimizedTreeRenderer.tempunselectedSectors.containsKey(symbol)) {
                    OptimizedTreeRenderer.selectedSectors.remove(symbol);
                }

            }

            //---------- For Sectors --------

            Enumeration bulkex = OptimizedTreeRenderer.tempunselectedExchanges.keys();
            while (bulkex.hasMoreElements()) {
                String symbol = (String) bulkex.nextElement();
                Boolean state = OptimizedTreeRenderer.tempunselectedExchanges.get(bulkKey);
                if (!OptimizedTreeRenderer.selectedExchanges.containsKey(symbol)) {
                    OptimizedTreeRenderer.selectedExchanges.put(symbol, true);
                    Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                    st.setSymbolEnabled(true);
                }
            }
            Enumeration symex = OptimizedTreeRenderer.selectedExchanges.keys();
            while (symex.hasMoreElements()) {
                String symbol = (String) symex.nextElement();
                Boolean state = OptimizedTreeRenderer.selectedExchanges.get(bulkKey);
                if (!OptimizedTreeRenderer.tempunselectedExchanges.containsKey(symbol)) {
                    OptimizedTreeRenderer.selectedExchanges.remove(symbol);
                    Stock st = DataStore.getSharedInstance().getStockObject(symbol);
                    st.setSymbolEnabled(false);
                }

            }

            // ---------- For Exchanges -----------------
        }

        Client.IS_BANDOP_VISIBLE = false;
    }

    public void checkingBox(int x, int y) {
        {
            TreePath path = tree.getPathForLocation(x, y);
            if (path != null) {
                tree.setSelectionPath(path);
                try {
                    OptimizedSymbolNode node = (OptimizedSymbolNode) path.getLastPathComponent();
                    OptimizedSymbolNode sto = (OptimizedSymbolNode) node;
                    selectedExchange = sto.getExchange();
                    selectedSymbol = sto.getSymbol();
                    selectedSymbolsIntType = sto.getInstrumentType();
                    Stock stock = DataStore.getSharedInstance().getStockObject(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType());
                    sto.setSelected(!sto.isSelected());
                    if (sto.isSelected()) {
                        JOptionPane.showMessageDialog(tree, Meta.ADD_OPTIMIZED_REQUEST + Meta.DS + selectedSymbol + Meta.FD + selectedExchange + Meta.FD + selectedSymbolsIntType,
                                Language.getString("ERROR"), JOptionPane.OK_OPTION);
                    }

                    OptimizedTreeRenderer.selectedSymbols.put(sto.getKey(), sto.isSelected());
                    Stock st = DataStore.getSharedInstance().getStockObject(sto.getKey());
                    st.setSymbolEnabled(true);
                } catch (Exception e) {

                }

            }
        }
    }

    private void addMouseListener() {
        tree.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                if (path != null) {
                    tree.setSelectionPath(path);
                    try {
                        OptimizedSymbolNode node = (OptimizedSymbolNode) path.getLastPathComponent();
                        OptimizedSymbolNode sto = (OptimizedSymbolNode) node;
                        System.out.println("----------node-----------" + sto);
                        selectedExchange = sto.getExchange();
                        selectedSymbol = sto.getSymbol();
                        selectedSymbolsIntType = sto.getInstrumentType();
                        Stock stock = DataStore.getSharedInstance().getStockObject(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType());
                        sto.setSelected(!sto.isSelected());
                        if (sto.isSelected()) {
                            JOptionPane.showMessageDialog(tree, Meta.ADD_OPTIMIZED_REQUEST + Meta.DS + selectedSymbol + Meta.FD + selectedExchange + Meta.FD + selectedSymbolsIntType,
                                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
                            SendQFactory.addOptimizationRecord(selectedExchange, selectedSymbol, selectedSymbolsIntType);
                        }

                        OptimizedTreeRenderer.selectedSymbols.put(sto.getKey(), sto.isSelected());
                        Stock st = DataStore.getSharedInstance().getStockObject(sto.getKey());
                        st.setSymbolEnabled(true);

                    } catch (Exception e) {

                    }

                }
            }

            public void mouseClicked(MouseEvent ev) {
                TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                if (path == null) {
                    return;
                }
                /*  if (ev.getClickCount() > 1) {
                    OptimizedSymbolNode node = (OptimizedSymbolNode) path.getLastPathComponent();
                    if (node != null && !ev.isPopupTrigger()) {    //&& node.isLeaf()
                        if (node instanceof OptimizedSymbolNode) {
                            Client.getInstance().mnuDetailQuoteSymbol(SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType), false, false, false);
                        }
                    }
                }*/
            }

            public void mouseReleased(MouseEvent ev) {
                try {
                    TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                    if (path == null) {
                        return;
                    }
                    //IF we're on a path, then we can check the node that its holding
                    OptimizedSymbolNode node = (OptimizedSymbolNode) path.getLastPathComponent();
                    if (node != null && ev.isPopupTrigger()) {    //&& node.isLeaf()
                        if (node instanceof OptimizedSymbolNode) {
                            getPopupMenu1().show(tree, ev.getX(), ev.getY());
                        }

                    }
                } catch (Exception e) {

                }
            }
        });
    }

    public String getSelectedKey() {
        return SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);
    }

    public void run() {
        while (isActive) {
            try {
                if (isReadyToFilter()) {
                    searchTree(searchText.getText());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void applyTheme() {
        btnSearch.setIcon(getIconFromString("search"));
        btnSearch.setRollOverIcon(getIconFromString("search-mouseover"));

    }

    private boolean isReadyToFilter() {
        return this.isReady;
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void focusGained(FocusEvent e) {
        if (e.getSource().equals(searchText)) {
            if (searchText.getText().equals(Language.getString("SIDE_BAR_SEARCH"))) {
                searchText.setText("");
            }

        }
    }

    public void focusLost(FocusEvent e) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        rootNode.clear();
        loadTree();
    }

    public void exchangesLoaded() {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {

        try {
            for (int i = 0; i < rootNode.getSymbolStore().size(); i++) {
                OptimizedExchangeNode ex = (OptimizedExchangeNode) rootNode.getSymbolStore().get(i);
                if (ex.getExchangeSymbol().equals(exchange.getSymbol())) {
                    ex.setMarketStatus(exchange.getMarketStatus());
                    updateTree();
                    tree.repaint();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void symbolAdded(String key, String listID) {
        fireSymbolAddedEvent(key, listID);
        tree.repaint();

    }

    public void symbolRemoved(String key, String listID) {
        fireSymbolRemovedEvent(key, listID);
        tree.repaint();
    }

    public void watchlistAdded(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void watchlistRenamed(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void watchlistRemoved(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void setVisible(boolean value) {
        super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        Component[] items = symbolTreePopup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] instanceof TWMenuItem) {
                TWMenuItem item = (TWMenuItem) items[i];
                if (item.getText().equals(Language.getString("ADD_SYMBOLS"))) {
                } else if (item.getText().equals(Language.getString("REMOVE_SYMBOL"))) {
                }
            }
        }
    }

    public void workspaceLoaded() {
//        super.workspaceLoaded();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        Component[] items = symbolTreePopup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] instanceof TWMenuItem) {
                TWMenuItem item = (TWMenuItem) items[i];
                if (item.getText().equals(Language.getString("ADD_SYMBOLS"))) {
//                        item.setVisible(true);
                } else if (item.getText().equals(Language.getString("REMOVE_SYMBOL"))) {
//                        item.setVisible(true);
                }
            }
        }
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
        Component[] items = symbolTreePopup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] instanceof TWMenuItem) {
                TWMenuItem item = (TWMenuItem) items[i];
                if (item.getText().equals(Language.getString("ADD_SYMBOLS"))) {
//                        item.setVisible(true);
                } else if (item.getText().equals(Language.getString("REMOVE_SYMBOL"))) {
//                        item.setVisible(true);
                }
            }
        }
    }

    public void updateTree() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                tree.updateUI();
            }
        });


    }

    public void updateUI() {
        super.updateUI();
//        BasicInternalFrameUI ui = (BasicInternalFrameUI) super.getUI();
//        Component north = ui.getNorthPane();

//        MouseMotionListener[] actions = north.getListeners(MouseMotionListener.class);
//        for (int i = 0; i < actions.length; i++)
//            north.removeMouseMotionListener(actions[i]);
//
//        MouseListener[] actions2 = north.getListeners(MouseListener.class);
//        for (int i = 0; i < actions2.length; i++)
//            north.removeMouseListener(actions2[i]);
    }

    public void applicationReadyForTransactions() {
        super.applicationReadyForTransactions();    //To change body of overridden methods use File | Settings | File Templates.
        System.out.println("------------------");
    }

//  ----------------------- loading to prop

    public void requestBuilder() {
        boolean needToReconnect = false;
        Hashtable<String, ArrayList<Stock>> exchangeRequests = new Hashtable<String, ArrayList<Stock>>();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exg = (Exchange) exchanges.nextElement();
            if (exg.isDefault()) {
                ArrayList<String> totalSymbols = new ArrayList<String>();
                Enumeration eekeys = OptimizedTreeRenderer.selectedSymbols.keys();
                while (eekeys.hasMoreElements()) {
                    String key = (String) eekeys.nextElement();
                    Stock st = DataStore.getSharedInstance().getStockObject(key);
                    if (st.getExchange().equals(exg.getSymbol())) {
                        totalSymbols.add(key);
                    }
                }
                Hashtable<String, ArrayList<Stock>> addedSymbols = new Hashtable<String, ArrayList<Stock>>();
                Hashtable<String, ArrayList<Stock>> removedSymbols = new Hashtable<String, ArrayList<Stock>>();
                Enumeration keys = selectedSymbols.keys();
                while (keys.hasMoreElements()) {
                    String key = (String) keys.nextElement();
                    Boolean stockRet = selectedSymbols.get(key);
                    Stock stock = DataStore.getSharedInstance().getStockObject(key);

                    if (stockRet) {
                        if (addedSymbols.containsKey(stock.getExchange())) {
                            ArrayList<Stock> gettingArrayFromHash = addedSymbols.get(stock.getExchange());
                            gettingArrayFromHash.add(stock);
                        } else {
                            ArrayList<Stock> freshArray = new ArrayList<Stock>();
                            freshArray.add(stock);
                            addedSymbols.put(stock.getExchange(), freshArray);
                        }
                    } else {
                        if (removedSymbols.containsKey(stock.getExchange())) {
                            ArrayList<Stock> gettingArrayFromHash = removedSymbols.get(stock.getExchange());
                            gettingArrayFromHash.add(stock);
                        } else {
                            ArrayList<Stock> freshArray = new ArrayList<Stock>();
                            freshArray.add(stock);
                            removedSymbols.put(stock.getExchange(), freshArray);
                        }
                    }

                } //---------------- end of selecedsymbols put in to needed hash----------

                Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                ArrayList symbolArray = new ArrayList();
                while (exchageSymbols.hasMoreElements()) {
                    symbolArray.add((String) exchageSymbols.nextElement());
                }

                int sizeOfExchangesymbolArry = symbolArray.size();                       //16010DGCXDGO13NOV200874000CA:10
                // String request = Meta.ADD_OPTIMIZED_REQUEST + Meta.DS + key +Meta.FD + exchange + Meta.FD + symbol+ Meta.EOL;
                int sizeOfCollectedSymbolarry = 0;
                if (addedSymbols.containsKey(exg.getSymbol())) {

                    ArrayList<Stock> fromAsHash = addedSymbols.get(exg.getSymbol());
                    sizeOfCollectedSymbolarry = fromAsHash.size();

                    Hashtable<Integer, ArrayList<Stock>> deviderStocks = new Hashtable<Integer, ArrayList<Stock>>();
                    for (int i = 0; i < fromAsHash.size(); i++) {
                        Stock st = fromAsHash.get(i);
                        int insType = st.getInstrumentType();
                        if (deviderStocks.containsKey(insType)) {
                            ArrayList<Stock> gettingArrayFromHash = deviderStocks.get(insType);
                            gettingArrayFromHash.add(st);
                        } else {
                            ArrayList<Stock> freshArray = new ArrayList<Stock>();
                            freshArray.add(st);
                            deviderStocks.put(insType, freshArray);
                        }
                    }

                    Enumeration<Integer> keysx = deviderStocks.keys();
                    while (keysx.hasMoreElements()) {
                        Integer key = keysx.nextElement();
                        ArrayList<Stock> gettingArrayFromHash = deviderStocks.get(key);

                        int arraySize = (gettingArrayFromHash.size() / 100);
                        int reqDevider = arraySize + 1;

                        int position = 0;
                        int max = gettingArrayFromHash.size();

                        for (int j = 1; j <= reqDevider; j++) {
                            ArrayList exchangeSymbols = new ArrayList();

                            int currentPos;
                            if ((j * 100) > max) {
                                currentPos = max;
                            } else {
                                currentPos = (j * 100);
                            }
                            for (int i = position; i < currentPos; i++) {
                                Stock fulKey = gettingArrayFromHash.get(i);
                                int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                                Exchange exx = ExchangeStore.getSharedInstance().getExchange(fulKey.getExchange());
                                if ((exx.isUserSubMarketBreakdown()) && (exx.getSubMarketCount() > 0)) {
                                    exchangeSymbols.add(fulKey.getSymbolCode() + ":" + fulKey.getInstrumentType());
                                } else {
                                    exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                }
                                position = position + 1;
                            }
                            String selectedExchangesSymbols = "";
                            for (int f = 0; f < exchangeSymbols.size(); f++) {
                                selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                            }

                            String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                            SendQFactory.addOptimizationAllRecord(key, exg.getSymbol(), exchangeAddRequest);

                        }
//                        String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                        int messageType = SharedMethods.getSymbolType(key);
                        String valForProp = "req";
                        String keyExchange = exg.getSymbol();

                        if (sizeOfCollectedSymbolarry > 0) {
                            if ((sizeOfExchangesymbolArry == sizeOfCollectedSymbolarry) || (totalSymbols.size() == sizeOfExchangesymbolArry)) {
                                prop.remove(keyExchange);
                                needToReconnect = true;

                            } else if (sizeOfExchangesymbolArry > sizeOfCollectedSymbolarry) {
                                if (prop.containsKey(keyExchange)) {
                                    prop.put(keyExchange, valForProp);
//                                    SendQFactory.addOptimizationAllRecord(key, exg.getSymbol(), exchangeAddRequest);
                                } else {
                                    Enumeration<Object> pkeys = prop.keys();
                                    while (pkeys.hasMoreElements()) {
                                        String keyOnProp = (String) pkeys.nextElement();
                                        if (keyOnProp.equals(exg.getSymbol())) {
//                                            SendQFactory.addOptimizationAllRecord(key, exg.getSymbol(), exchangeAddRequest);
                                        } else {
                                            needToReconnect = true;
                                            prop.put(exg.getSymbol(), valForProp);
                                        }
                                    }
                                    if (prop.size() == 0) {
                                        needToReconnect = true;
                                        prop.put(exg.getSymbol(), valForProp);
                                    }
                                }
                            }
                        }

                    } //--end while
                }   //------------------------- End of Add -----------------------

                //  Begin of Remove -------------------------------------------------
                if (removedSymbols.containsKey(exg.getSymbol())) {
                    ArrayList<Stock> fromAsHash = removedSymbols.get(exg.getSymbol());
                    sizeOfCollectedSymbolarry = fromAsHash.size();

                    Hashtable<Integer, ArrayList<Stock>> deviderStocks = new Hashtable<Integer, ArrayList<Stock>>();
                    for (int i = 0; i < fromAsHash.size(); i++) {
                        Stock st = fromAsHash.get(i);
                        int insType = st.getInstrumentType();
                        if (deviderStocks.containsKey(insType)) {
                            ArrayList<Stock> gettingArrayFromHash = deviderStocks.get(insType);
                            gettingArrayFromHash.add(st);
                        } else {
                            ArrayList<Stock> freshArray = new ArrayList<Stock>();
                            freshArray.add(st);
                            deviderStocks.put(insType, freshArray);
                        }
                    }

                    Enumeration<Integer> keysx = deviderStocks.keys();
                    while (keysx.hasMoreElements()) {
                        Integer key = keysx.nextElement();
                        ArrayList<Stock> gettingArrayFromHash = deviderStocks.get(key);

                        /*  ArrayList exchangeSymbols = new ArrayList();
                        for (int i = 0; i < gettingArrayFromHash.size(); i++) {
                            Stock fulKey = gettingArrayFromHash.get(i);

                            Exchange exx = ExchangeStore.getSharedInstance().getExchange(fulKey.getExchange());
                            if ((exx.isUserSubMarketBreakdown()) && (exx.getSubMarketCount() > 0)) {
                                exchangeSymbols.add(fulKey.getSymbolCode() + ":" + fulKey.getInstrumentType());
                            } else {
                                exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                            }
                        }
                        String selectedExchangesSymbols = "";
                        for (int f = 0; f < exchangeSymbols.size(); f++) {
                            selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                        }*/


                        int arraySize = (gettingArrayFromHash.size() / 100);
                        int reqDevider = arraySize + 1;

                        int position = 0;
                        int max = gettingArrayFromHash.size();

                        for (int j = 1; j <= reqDevider; j++) {
                            ArrayList exchangeSymbols = new ArrayList();

                            int currentPos;
                            if ((j * 100) > max) {
                                currentPos = max;
                            } else {
                                currentPos = (j * 100);
                            }
                            for (int i = position; i < currentPos; i++) {
                                Stock fulKey = gettingArrayFromHash.get(i);
                                int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                                Exchange exx = ExchangeStore.getSharedInstance().getExchange(fulKey.getExchange());
                                if ((exx.isUserSubMarketBreakdown()) && (exx.getSubMarketCount() > 0)) {
                                    exchangeSymbols.add(fulKey.getSymbolCode() + ":" + fulKey.getInstrumentType());
                                } else {
                                    exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                }
                                position = position + 1;
                            }
                            String selectedExchangesSymbols = "";
                            for (int f = 0; f < exchangeSymbols.size(); f++) {
                                selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                            }

                            String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                            Client.WATCHLIST_SELECT = false;
                            SendQFactory.removeOptimizationAllRecord(key, exg.getSymbol(), exchangeAddRequest);
                        }
//                        String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                        int messageType = SharedMethods.getSymbolType(key);
                        String keyForProp = Meta.REMOVE_OPTIMIZED_REQUEST + Meta.DS + messageType + Meta.FD + exg.getSymbol();
                        String valForProp = "161-req";

                        String keyExchange = exg.getSymbol();
                        if (sizeOfCollectedSymbolarry > 0) {
                            if (prop.containsKey(keyExchange)) {
                                prop.put(keyExchange, valForProp);
//                                SendQFactory.removeOptimizationAllRecord(key, exg.getSymbol(), exchangeAddRequest);
                            } else {
                                Enumeration<Object> pkeys = prop.keys();
                                while (pkeys.hasMoreElements()) {
                                    String keyOnProp = (String) pkeys.nextElement();
                                    if (keyOnProp.equals(exg.getSymbol())) {
                                        prop.put(keyOnProp, valForProp);
//                                        SendQFactory.removeOptimizationAllRecord(key, exg.getSymbol(), exchangeAddRequest);
                                    } else {
                                        needToReconnect = true;
                                        prop.put(exg.getSymbol(), valForProp);
                                    }
                                }
                                if (prop.size() == 0) {
                                    needToReconnect = true;
                                    prop.put(exg.getSymbol(), valForProp);
                                }
                            }
                        }
//                        }
                    }
                }   // ----------------------- end of remove---------------
            }
        }

        if (needToReconnect) {
            try {
                save_BandWidthOptimized_Symbols();
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            saveSettings();
            Client.getInstance().disconnectFromServer();
            //  ==================================== DO RECONNECTION ====================================
        } else {
            sendAddRequests();
            sendRemoveRequest();
        }
        selectedSymbols = new Hashtable<String, Boolean>();
    }

    public void putProperty(String id, String value) {
        prop.put(id, value);
    }

    public String getProperty(String id) {
        return prop.getProperty(id);
    }

    public void addrequestCollector(int path, String exchangeSymbol) {
        addrequestArrayList.add(new Check(path, exchangeSymbol));


    }

    public void removerequestCollector(int path, String exchangeSymbol) {
        removerequestArrayList.add(new Check(path, exchangeSymbol));
    }

    public void sendAddRequests() {
        for (int i = 0; i < addrequestArrayList.size(); i++) {
            Check object = (Check) addrequestArrayList.get(i);

            SendQFactory.addData(object.getIndex(), object.getSymbol());
        }
        addrequestArrayList = new ArrayList();
    }

    public void sendRemoveRequest() {
        for (int i = 0; i < removerequestArrayList.size(); i++) {
            Check object = (Check) removerequestArrayList.get(i);
            SendQFactory.addData(object.getIndex(), object.getSymbol());
        }
        removerequestArrayList = new ArrayList();
    }

    class NodeCoparator implements Comparator {
        public int compare(Object o1, Object o2) {
            return ((OptimizedExchangeNode) o1).getName().compareTo(((OptimizedExchangeNode) o2).getName());
        }
    }

    public class Check {

        public int index;
        private String symbol;

        public Check(int index, String symbol) {
            this.symbol = symbol;
            this.index = index;

        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }
    }
}
