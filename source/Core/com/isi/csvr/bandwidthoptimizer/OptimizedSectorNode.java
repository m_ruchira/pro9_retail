package com.isi.csvr.bandwidthoptimizer;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Sep 8, 2008
 * Time: 5:09:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptimizedSectorNode extends OptimizedExchangeNode {
    public String description;
    public String symbol;
    public boolean selected = true;
    public String exchange;
    private String name;
    private String status = null;
    private String exchangeSymbol;
    private Vector symbolStore;

    public OptimizedSectorNode(String name, String exgSymbol) {
        this.name = name;
        this.exchangeSymbol = exgSymbol;
        this.description = name;
        this.symbol = exgSymbol;
        symbolStore = new Vector();
    }

    public OptimizedSectorNode() {

    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExchangeSymbol() {
        return exchangeSymbol;
    }

    public void setExchangeSymbol(String exchangeSymbol) {
        this.exchangeSymbol = exchangeSymbol;
    }

    public Vector getSymbolStore() {
        return symbolStore;
    }

    public void setChildrenStore(Hashtable store) {
        try {
            Enumeration symbols = store.elements();
            while (symbols.hasMoreElements()) {
                symbolStore.add((OptimizedSymbolNode) symbols.nextElement());
            }
            Collections.sort(symbolStore, new LeafCoparator());
        } catch (Exception e) {
        }
    }

    public void addNode(OptimizedExchangeNode node) {
        try {
            symbolStore.add(node);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void removeNode(OptimizedSymbolNode oNode) {
        try {
            for (int i = 0; i < symbolStore.size(); i++) {
                if (((OptimizedSymbolNode) symbolStore.get(i)).getSymbol().equals(oNode.getSymbol())) {
                    symbolStore.remove(i);
                    break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void addNode(OptimizedSymbolNode oNode) {
        try {
            boolean include = false;
            for (int i = 0; i < symbolStore.size(); i++) {
                if (((OptimizedSymbolNode) symbolStore.get(i)).getSymbol().equals(oNode.getSymbol())) {
                    include = true;
                }

            }
            if (!include) {
                symbolStore.add(oNode);
                include = false;
            }
        } catch (Exception e) {
            e.printStackTrace(); //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void clear() {
        symbolStore.clear();
    }

    public Object getChild(int index) {
        return symbolStore.get(index);
    }

    public int getChildCount() {
        try {
            return symbolStore.size();
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isLeaf(Object object) {
        try {
            if (symbolStore.contains((OptimizedExchangeNode) object)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public int getIndex(Object object) {
        return symbolStore.indexOf((OptimizedExchangeNode) object);
    }

    public String toString() {
        return name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public boolean isExchangeType() {
        return false;
    }

    public boolean isSectorType() {
        return true;
    }

    public boolean isSubMarketType() {
        return false;
    }

    class LeafCoparator implements Comparator {
        public int compare(Object o1, Object o2) {
            return ((OptimizedSymbolNode) o1).getDescription().compareTo(((OptimizedSymbolNode) o2).getDescription());
        }
    }
}
