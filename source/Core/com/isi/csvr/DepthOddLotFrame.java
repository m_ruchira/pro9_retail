package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.marketdepth.OddLotModel;
import com.isi.csvr.marketdepth.OddLotStore;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.MarketOddLotRenderer;
import com.isi.csvr.table.Table;
import com.isi.csvr.trading.TradeMethods;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 29, 2008
 * Time: 8:06:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class DepthOddLotFrame extends InternalFrame implements LinkedWindowListener, MouseListener {

    OddLotModel oddlotModel;
    byte stockDecimalPlaces = 2;
    byte type;
    private Table oddLotTable;
    private ViewSetting OddLotModel;
    private boolean loadedFromWorkspace;
    private ViewSetting oddLotiewSetting;
    private String selectedKey;
    private ViewSettingsManager g_oViewSettings;
    private boolean isLinked = false;

    public DepthOddLotFrame(ThreadGroup oThreadGroup, String sThreadID, WindowDaemon oDaemon, Table oTable, boolean loadedFromWorkspace, String selectedKey, byte type, boolean linked, String linkgroup) {
        super(oThreadGroup, sThreadID, oDaemon, oTable);
        oddLotTable = oTable;
        this.loadedFromWorkspace = loadedFromWorkspace;
        this.selectedKey = selectedKey;
        adjustKey(this.selectedKey);
        this.type = type;
        isLinked = linked;
        setLinkedGroupID(linkgroup);
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        initUI();
    }

    public void initUI() {

        oddLotTable.setAutoResize(true);
        oddLotTable.getPopup().showLinkMenus();
        oddLotTable.getPopup().showSetDefaultMenu();
        oddLotTable.setWindowType(ViewSettingsManager.DEPTH_ODDLOT_VIEW);

        oddlotModel = new OddLotModel(selectedKey);

        try {
//                bidModel = new CombinedDepthModel(selectedKey);
            oddlotModel.setDecimalCount(SharedMethods.getDecimalPlaces(selectedKey));
        } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if (oddLotiewSetting == null && loadedFromWorkspace && isLinked) {
            ViewSetting oSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                oddLotiewSetting = oSetting;
            }
        }
        if (oddLotiewSetting == null) {
            oddLotiewSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_ODDLOT_VIEW +
                    "|" + selectedKey);
        }
        if (oddLotiewSetting == null) {
            oddLotiewSetting = g_oViewSettings.getSymbolView("DEPTH_ODDLOT_VIEW");
            oddLotiewSetting = oddLotiewSetting.getObject();
            //oddLotiewSetting.setID(selectedKey);
            oddLotiewSetting.setID(System.currentTimeMillis() + "");
            oddLotiewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
            oddLotiewSetting.putProperty(ViewConstants.VC_LINKED, false);
            oddLotiewSetting.setDesktopType(type);
            g_oViewSettings.putDepthView(ViewSettingsManager.DEPTH_ODDLOT_VIEW +
                    "|" + selectedKey, oddLotiewSetting);
            GUISettings.setColumnSettings(oddLotiewSetting, TWColumnSettings.getItem("MARKET_DEPTH_ODDLOT_COLS"));
            //todo added by Dilum
            //    SharedMethods.applyCustomViewSetting(oddLotiewSetting);
            //    DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oddLotiewSetting.getType(),oddLotiewSetting);
        } else {
            oddLotiewSetting.setDesktopType(type);
            loadedFromWorkspace = true;
        }
        oddlotModel.setViewSettings(oddLotiewSetting);

        oddLotTable.setModel(oddlotModel);
        oddlotModel.setTable(oddLotTable);  // Add the renderer...
        MarketOddLotRenderer renderer = new MarketOddLotRenderer();
        renderer.initRenderer(oddLotiewSetting.getColumnHeadings(), oddlotModel.getRendIDs());
        oddLotTable.getTable().setDefaultRenderer(Object.class, renderer);
        oddLotTable.getTable().setDefaultRenderer(Number.class, renderer);
        oddlotModel.applyColumnSettings();
        //////////////////////////////adding double click events
        oddLotTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {

                    if (oddLotTable.getTable().getSelectedColumn() == 3) {
                        if (((oddlotModel.getValueAt(oddLotTable.getTable().getSelectedRow(), 3))).equals("")) {
                            TradeMethods.getSharedInstance().showBuyScreen(selectedKey);
                        } else {
                            TradeMethods.getSharedInstance().showSellScreen(selectedKey);
                        }
                    } else if (oddLotTable.getTable().getSelectedColumn() == 4) {
                        if (((oddlotModel.getValueAt(oddLotTable.getTable().getSelectedRow(), 4))).equals("")) {
                            TradeMethods.getSharedInstance().showSellScreen(selectedKey);
                        } else {
                            TradeMethods.getSharedInstance().showBuyScreen(selectedKey);
                        }
                    }

                }
            }
        });
        ////////////////////////////////


        oddLotiewSetting.setParent(this);
        oddLotiewSetting.setTableNumber(1);
        this.getContentPane().setLayout(new BorderLayout());
        this.setSize(oddLotiewSetting.getSize());
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setShowServicesMenu(true);
        this.setIconifiable(true);
        this.setLinkGroupsEnabled(true);

        Client.getInstance().oTopDesktop.add(this);
        this.setTitle(Language.getString("MARKET_DEPTH_ODD_LOT") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getCompanyName(selectedKey));


        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);


        GUISettings.applyOrientation(this);
        oddLotTable.updateGUI();
        this.applySettings();
        this.getContentPane().add(oddLotTable);
        this.show();
        oddlotModel.adjustRows();
        this.setLayer(GUISettings.TOP_LAYER);
        if (loadedFromWorkspace && isLinked) {
            g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + selectedKey, this);
        }

        String requestID = Meta.DEPTH_PRICE_ODDLOT + "|" + selectedKey;
        Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.DEPTH_PRICE_ODDLOT);
        this.setDataRequestID(OddLotStore.getSharedInstance(), selectedKey, requestID);

        if (loadedFromWorkspace) {
            //                frame.setLocationRelativeTo(oTopDesktop);
            //                oBidViewSetting.setLocation(frame.getLocation());
            if (!oddLotiewSetting.isLocationValid()) {
                this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                oddLotiewSetting.setLocation(this.getLocation());
            } else {
                this.setLocation(oddLotiewSetting.getLocation());
            }
        }

        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }
    }

    public void setDecimalPlaces() {
        try {
            stockDecimalPlaces = SharedMethods.getDecimalPlaces(selectedKey);
        } catch (Exception e) {
            stockDecimalPlaces = 2;
        }
        oddLotTable.getModel().setDecimalCount(stockDecimalPlaces);
        oddLotTable.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);

    }

    public void symbolChanged(String sKey) {
        selectedKey = sKey;
        adjustKey(selectedKey);
        String oldKey = oddLotiewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oldKey))) {
            DataStore.getSharedInstance().removeSymbolRequest(oldKey);

        }
        String oldReq = getDataRequestID();
        RequestManager.getSharedInstance().removeRequest(oldReq);
        String requestID = Meta.DEPTH_PRICE_ODDLOT + "|" + selectedKey;
        Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.DEPTH_PRICE_ODDLOT);
        this.setDataRequestID(OddLotStore.getSharedInstance(), selectedKey, requestID);
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }


        this.setTitle(Language.getString("MARKET_DEPTH_ODD_LOT") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getCompanyName(selectedKey));

        oddlotModel.setSymbol(selectedKey);
        oddLotTable.getTable().updateUI();
        this.updateUI();
        oddLotiewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);

    }

    private void adjustKey(String sKey) {
        if (!sKey.contains("`O")) {
            String nwsubkey = SharedMethods.getSymbolFromKey(sKey);
            if (nwsubkey.contains("`")) {
                nwsubkey = nwsubkey.split("`")[0];
                nwsubkey = nwsubkey + "`O";
            }
            sKey = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sKey), nwsubkey, SharedMethods.getInstrumentTypeFromKey(sKey));
            this.selectedKey = sKey;
        }

    }

}
