package com.isi.csvr.symbolselector;

import com.isi.csvr.shared.Language;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.Document;
import java.awt.*;

// Copyright (c) 2000 Integrated Systems International (ISI)


public class CompanyTableRenderer extends TWBasicTableRenderer
        implements TableCellRenderer {
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    Document d;
    String text;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iCenterAlign;
    private String[] g_asMarketStatus = new String[4];
    private String g_sNA = "NA";
    private Color foreground, background;
    private DefaultTableCellRenderer lblRenderer;
    private ImageIcon tickImage;
    private ImageIcon unTickImage;
    private boolean boolValue;


    public CompanyTableRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;

        reload();

        try {
            g_asMarketStatus[0] = Language.getString("STATUS_PREOPEN");
            g_asMarketStatus[1] = Language.getString("STATUS_OPEN");
            g_asMarketStatus[2] = Language.getString("STATUS_CLOSE");
            g_asMarketStatus[3] = Language.getString("STATUS_PRECLOSE");
        } catch (Exception e) {
            g_asMarketStatus[0] = g_sNA;
            g_asMarketStatus[1] = g_sNA;
            g_asMarketStatus[2] = g_sNA;
            g_asMarketStatus[3] = g_sNA;
        }

        try {
            tickImage = new ImageIcon("images/Common/tick.gif");
        } catch (Exception e) {
            tickImage = null;
        }
        try {
            unTickImage = new ImageIcon("images/Common/untick.gif");
        } catch (Exception e) {
            unTickImage = null;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {

    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void propertyChanged(int property) {
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (isSelected) {
            foreground = g_oSelectedFG;
            background = g_oSelectedBG;
        } else if (row % 2 == 0) {
            foreground = g_oFG1;
            background = g_oBG1;
        } else {
            foreground = g_oFG2;
            background = g_oBG2;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);


        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    /*if(isSelected){
                        View v = (View) (lblRenderer).getClientProperty(BasicHTML.propertyKey);
                        if (v != null) {
//                            System.out.println("html ");
                            d = v.getDocument();
                            text = d.getText(0, d.getLength()).trim();
                            lblRenderer.setText(text);
                        }
                    }*/

                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;

                case 'T': // TICK
                    boolValue = ((Boolean) value).booleanValue();
                    if (boolValue) {
                        lblRenderer.setIcon(tickImage);
                        lblRenderer.setText("");
                    } else {
                        lblRenderer.setIcon(unTickImage);
                        lblRenderer.setText("");
                    }

                    lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    break;

                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }


        return lblRenderer;
    }

    private boolean toBooleanValue(Object oValue) throws Exception {
        return Boolean.parseBoolean((String) oValue);
    }
}