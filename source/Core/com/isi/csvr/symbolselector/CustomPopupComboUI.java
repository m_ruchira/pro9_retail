package com.isi.csvr.symbolselector;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.util.ExtendedComboInterface;

import javax.swing.*;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 13, 2005
 * Time: 10:20:18 PM
 */
public class CustomPopupComboUI extends MetalComboBoxUI {

    private Component popupItem;
    //private Popup pop;

    public CustomPopupComboUI(Component popupItem) {//CompanyPanel companyPanel) {
        this.popupItem = popupItem;
    }

    protected ComboPopup createPopup() {
        BasicComboPopup popup = new BasicComboPopup(comboBox) {

            public void show() {
                GUISettings.applyOrientation(scroller);
                Dimension popupSize = ((ExtendedComboInterface) comboBox).getPopupSize();
                popupSize.setSize(popupSize.width,
                        getPopupHeightForRowCount(comboBox.getMaximumRowCount()));
                Rectangle popupBounds = computePopupBounds(0,
                        comboBox.getBounds().height,
                        popupSize.width,
                        popupSize.height);

                scroller.setMaximumSize(popupBounds.getSize());
                scroller.setPreferredSize(popupBounds.getSize());
                scroller.setMinimumSize(popupBounds.getSize());
                list.invalidate();
                int selectedIndex = comboBox.getSelectedIndex();
                if (selectedIndex == -1) {
                    list.clearSelection();
                } else {
                    list.setSelectedIndex(selectedIndex);
                }
                list.ensureIndexIsVisible(list.getSelectedIndex());
                setLightWeightPopupEnabled(comboBox.isLightWeightPopupEnabled());
                showPopup(this, popupBounds, 0, comboBox.getHeight());

            }
        };
        popup.getAccessibleContext().setAccessibleParent(comboBox);

        return popup;
    }

    public void showPopup(BasicComboPopup popup, Rectangle popupBounds, int x, int y) {
        if (!Language.isLTR()) {
            x = x - (int) popupBounds.getWidth() + (int) comboBox.getWidth();
        }
        Point point = new Point(x, y);
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = oToolkit.getScreenSize();

        SwingUtilities.convertPointToScreen(point, comboBox);
        if (Language.isLTR()) {
            if ((point.getX() + popupBounds.getWidth()) > screenSize.getWidth()) {
                x = (int) (x - popupBounds.getWidth() + comboBox.getWidth());
            }
        } else {
            if (point.getX() < 0) {
                x = 0;
            }
        }

        if ((point.getY() + popupBounds.getHeight()) > screenSize.getHeight()) {
            y = (int) (y - popupBounds.getHeight() - comboBox.getHeight());
        }
        //pop = PopupFactory.getSharedInstance().getPopup(comboBox, companyPanel,x,y+30);
//        JButton button = new JButton("hhooo");
//        button.setPreferredSize(new Dimension(400,100));
        popup.setLayout(new BorderLayout());
        popup.add(popupItem, BorderLayout.CENTER);
        popup.show(comboBox, x, y);
    }

    public JPopupMenu getPopup() {
        //scroll
        return (JPopupMenu) popup;
    }

//    public void setPopupVisible(JComboBox c, boolean v) {
//        if (v)
//            pop.show();
//        else
//            pop.hide();
//    }


}