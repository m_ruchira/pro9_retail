package com.isi.csvr.symbolselector;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.RepositoryConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.symbolsearch.SymbolSearchListItem;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.theme.Theme;

import javax.swing.table.TableModel;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 12, 2005
 * Time: 10:02:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class CompanyTableModel extends CommonTable
        implements TableModel, Comparator, RepositoryConstants {

    private SymbolSearchListItem[] filteredSymbols;
    private String filterString;
    private String filterExchange;
    private int count;
    private ViewSetting setting;
    private HashMap<String, String> selectedSymbols;

    public CompanyTableModel(ViewSetting setting) {
        this.setting = setting;
        selectedSymbols = new HashMap<String, String>();
        init();
    }

    /*private void loadSymbols() {
        symbols = DataStore.getSharedInstance().getCompanyList(); // take a ref off all filteredSymbols
        try {
            Arrays.sort(symbols,this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        filteredSymbols = new String[symbols.length][];
    }*/

    public void init() {
        filteredSymbols = new SymbolSearchListItem[DataStore.getSharedInstance().getAllSymbolCount()];
    }

    public void filter(String filter, String exchange) {
        try {
            this.filterString = filter.toUpperCase();
        } catch (Exception e) {
            filterString = null;
        }
        try {
            this.filterExchange = exchange.toUpperCase();
        } catch (Exception e) {
            filterExchange = null;
        }
        filter();
    }

    public void refilter() {
        filter();
    }

    private synchronized void filter() {
        try {
            count = 0;
            filteredSymbols = new SymbolSearchListItem[DataStore.getSharedInstance().getAllSymbolCount()];
            String filter;
            String align;
            String width;

            if (Language.isLTR()) {
                align = " align=left ";
            } else {
                align = " align=right ";
            }

            if (Language.isLTR()) {
                width = " style=\"width: 1000\" ";
            } else {
                width = "";
            }

            if (DataStore.getSharedInstance().getAllSymbols() == null) return;
            Enumeration<String> data = DataStore.getSharedInstance().getAllSymbols();
            if (filterString == null) {
                while (data.hasMoreElements()) {
                    String key = data.nextElement().trim();
                    Stock stock = DataStore.getSharedInstance().getStockObject(key);
                    if ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))) {
                        filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                        count++;
                    }
                    stock = null;
                    key = null;
                }
            } else {
                if ((filterString.startsWith("*")) && (filterString.endsWith("*")) && (filterString.length() >= 2)) {

                    String tempFilter = filterString.substring(1, filterString.length() - 1);
                    //String pattern = "(?i)"+tempFilter;
                    filter = filterString.substring(1, filterString.length() - 1);

                    while (data.hasMoreElements()) {
                        String key = data.nextElement().trim();
                        Stock stock = DataStore.getSharedInstance().getStockObject(key);
                        boolean isFound = false;
                        if (((stock.getShortDescription().toUpperCase().indexOf(filter)) >= 0) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange())))) {
                            filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                    "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            if (tempFilter.length() > 0) {
                                int index = 0;
                                if ((index = filteredSymbols[count].getShortDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                    String str = filteredSymbols[count].getShortDescription();
                                    filteredSymbols[count].setShortDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " +
                                            Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" +
                                            str.substring(index + tempFilter.length()) + "</div></html>"));
                                    str = null;
                                }
                            }
                            if (!isFound) {
                                isFound = true;
                            }
                        }
                        if (((stock.getLongDescription().toUpperCase().indexOf(filter)) >= 0) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange())))) {
                            if (isFound) {
                            } else
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            if (tempFilter.length() > 0) {
                                int index = 0;
                                if ((index = filteredSymbols[count].getDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                    String str = filteredSymbols[count].getDescription();
                                    filteredSymbols[count].setDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " +
                                            Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" +
                                            str.substring(index + tempFilter.length()) + "</div></html>"));
                                    str = null;
                                }
                            }
                            if (!isFound) {
                                isFound = true;
                            }
                        }
                        if (((stock.getSymbolCode().toUpperCase().indexOf(filter)) >= 0) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange())))) {
                            if (isFound) {
                            } else
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            if (tempFilter.length() > 0) {
                                int index = 0;
                                if ((index = filteredSymbols[count].getSymbol().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                    String str = filteredSymbols[count].getSymbol();
                                    filteredSymbols[count].setSymbol(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " +
                                            Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" +
                                            str.substring(index + tempFilter.length()) + "</div></html>"));
                                    str = null;
                                }
                            }
                            if (!isFound) {
                                isFound = true;
                            }
                        }
                        if (isFound) {
                            count++;
                        }
                        key = null;
                        stock = null;
                    }
                } else if (filterString.endsWith("*")) {
                    filter = filterString.substring(0, filterString.length() - 1);
                    while (data.hasMoreElements()) {
                        String key = data.nextElement().trim();
                        Stock stock = DataStore.getSharedInstance().getStockObject(key);
                        boolean isFound = false;
                        if (((stock.getLongDescription().toUpperCase().startsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
                        }
                        if (((stock.getShortDescription().toUpperCase().startsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
                        }
                        if (((stock.getSymbolCode().toUpperCase().startsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
                        }
                        if (isFound) {
                            count++;
                        }
                        key = null;
                        stock = null;
                    }

                } else if (filterString.startsWith("*")) {
                    filter = filterString.substring(1);
                    while (data.hasMoreElements()) {
                        String key = data.nextElement().trim();
                        Stock stock = DataStore.getSharedInstance().getStockObject(key);
                        boolean isFound = false;
                        if (((stock.getLongDescription().toUpperCase().endsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
                        }
                        if (((stock.getShortDescription().toUpperCase().endsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
                        }
                        if (((stock.getSymbolCode().toUpperCase().endsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
                        }
                        if (isFound) {
                            count++;
                        }
                        stock = null;
                        key = null;
                    }
                } else {
                    filter = filterString.substring(0, filterString.length());
                    while (data.hasMoreElements()) {
                        String key = data.nextElement().trim();
                        Stock stock = DataStore.getSharedInstance().getStockObject(key);
                        boolean isFound = false;
                        if (((stock.getLongDescription().toUpperCase().startsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
//                            count++;
                        }
                        if (((stock.getShortDescription().toUpperCase().startsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
//                            count++;
                        }
                        if (((stock.getSymbolCode().toUpperCase().startsWith(filter)) &&
                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange()))))) {
                            if (!isFound) {
                                filteredSymbols[count] = new SymbolSearchListItem(stock.getExchange(), stock.getSymbolCode(), stock.getLongDescription(),
                                        "" + stock.getInstrumentType(), null, stock.getShortDescription(), stock.getSymbolCodeWithMarkets());
                            }
                            isFound = true;
//                            count++;
                        }
                        if (isFound) {
                            count++;
                        }
                        stock = null;
                        key = null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* private synchronized void filter() {
        try {
            count = 0;
            filteredSymbols = new Stock[DataStore.getSharedInstance().getAllSymbolCount()];
            String filter;

//            if (SymbolRepository.REPOSITORY == null) return;
            Enumeration<String> data = DataStore.getSharedInstance().getAllSymbols();
            if (filterString == null) {
//                Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
//                while()
                while(data.hasMoreElements()){
                    String key = data.nextElement();
                    Stock stock = DataStore.getSharedInstance().getStockObject(key);
                    if(((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange())))){

                        filteredSymbols[count] = stock;
                        count ++;
                    }
                }

            } else {
                if ((filterString.startsWith("*")) && (filterString.endsWith("*")) && (filterString.length() >= 2)) {

                    String tempFilter = filterString.substring(1, filterString.length()-1);
                    //String pattern = "(?i)"+tempFilter;
                    filter = filterString.substring(1, filterString.length() - 1);
                    while(data.hasMoreElements()){
                        String key = data.nextElement();
                        Stock stock = DataStore.getSharedInstance().getStockObject(key);
                        boolean isFound = false;
                        if(((stock.getShortDescription().toUpperCase().indexOf(filter)) >= 0) &&
                        ((filterExchange == null) || (filterExchange.equalsIgnoreCase(stock.getExchange())))){
                            filteredSymbols[count] = stock.clone();
                            if (tempFilter.length() > 0){
                                int index = 0;
                                if ((index = filteredSymbols[count].getShortDescription().toUpperCase().indexOf(tempFilter.toUpperCase()))>=0){
                                    String  str = filteredSymbols[count][RepositoryConstants.SHORT_DESCRIPTION];
                                    filteredSymbols[count][RepositoryConstants.SHORT_DESCRIPTION] = "<html>" + str.substring(0,index) +  "<span style='background-color: "+ Theme.getHighlightedColor() +"'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length());
                                    str = null;
                                }
                            }
                        }
//                    for (int i = 0; i < SymbolRepository.REPOSITORY.size(); i++) {
//                        boolean isFound = false;
//                        if (((SymbolRepository.REPOSITORY.get(i)[SHORT_DESCRIPTION].toUpperCase().indexOf(filter)) >= 0) &&
//                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(SymbolRepository.REPOSITORY.get(i)[EXCHANGE]))))
//                        {
//                            filteredSymbols[count] = SymbolRepository.REPOSITORY.get(i).clone();
//                            if (tempFilter.length() > 0){
//                                int index = 0;
//                                if ((index = filteredSymbols[count][RepositoryConstants.SHORT_DESCRIPTION].toUpperCase().indexOf(tempFilter.toUpperCase()))>=0){
//                                    String  str = filteredSymbols[count][RepositoryConstants.SHORT_DESCRIPTION];
//                                    filteredSymbols[count][RepositoryConstants.SHORT_DESCRIPTION] = "<html>" + str.substring(0,index) +  "<span style='background-color: "+ Theme.getHighlightedColor() +"'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length());
//                                    str = null;
//                                }
//                                //filteredSymbols[count][RepositoryConstants.DESCRIPTION] = "<html>" + filteredSymbols[count][RepositoryConstants.DESCRIPTION].replaceAll(pattern , "<span style='background-color: #FFFF00'>"+tempFilter+"</span>");
//                            }
//                            if(!isFound){
//                                isFound = true;
////                                count++;
//                            }
//                        }
//                        if (((SymbolRepository.REPOSITORY.get(i)[DESCRIPTION].toUpperCase().indexOf(filter)) >= 0) &&
//                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(SymbolRepository.REPOSITORY.get(i)[EXCHANGE]))))
//                        {
//                            if(isFound){
////                                filteredSymbols[count] = SymbolRepository.REPOSITORY.get(i);
//                            }else
//                                filteredSymbols[count] = SymbolRepository.REPOSITORY.get(i).clone();
//                            if (tempFilter.length() > 0){
//                                int index = 0;
//                                if ((index = filteredSymbols[count][RepositoryConstants.DESCRIPTION].toUpperCase().indexOf(tempFilter.toUpperCase()))>=0){
//                                    String  str = filteredSymbols[count][RepositoryConstants.DESCRIPTION];
//                                    filteredSymbols[count][RepositoryConstants.DESCRIPTION] = "<html>" + str.substring(0,index) +  "<span style='background-color: "+ Theme.getHighlightedColor() +"'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length());
//                                    str = null;
//                                }
//                                //filteredSymbols[count][RepositoryConstants.DESCRIPTION] = "<html>" + filteredSymbols[count][RepositoryConstants.DESCRIPTION].replaceAll(pattern , "<span style='background-color: #FFFF00'>"+tempFilter+"</span>");
//                            }
//                            if(!isFound){
//                                isFound = true;
////                                count++;
//                            }
//                        }
//                        if (((SymbolRepository.REPOSITORY.get(i)[SYMBOL].toUpperCase().indexOf(filter)) >= 0) &&
//                                ((filterExchange == null) || (filterExchange.equalsIgnoreCase(SymbolRepository.REPOSITORY.get(i)[EXCHANGE]))))
//                        {
//                            if(isFound){
////                                filteredSymbols[count] = SymbolRepository.REPOSITORY.get(i);
//                            }else
//                                filteredSymbols[count] = SymbolRepository.REPOSITORY.get(i).clone(); 
//                            if (tempFilter.length() > 0){
//                                int index = 0;
//                                if ((index = filteredSymbols[count][RepositoryConstants.SYMBOL].toUpperCase().indexOf(tempFilter.toUpperCase()))>=0){
//                                    String  str = filteredSymbols[count][RepositoryConstants.SYMBOL];
//                                    filteredSymbols[count][RepositoryConstants.SYMBOL] = "<html>" + str.substring(0,index) +  "<span style='background-color: "+ Theme.getHighlightedColor() +"'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length());
//                                    str = null;
//                                }
//                                //filteredSymbols[count][RepositoryConstants.DESCRIPTION] = "<html>" + filteredSymbols[count][RepositoryConstants.DESCRIPTION].replaceAll(pattern , "<span style='background-color: #FFFF00'>"+tempFilter+"</span>");
//                            }
//                            if(!isFound){
//                                isFound = true;
////                                count++;
//                            }
//                        }
//                        if(isFound){
//                            count++;
//                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void toggleSelected(String key) {
        if (selectedSymbols.containsKey(key)) {
            selectedSymbols.remove(key);
        } else {
            selectedSymbols.put(key, key);
        }
    }

    public void clearSelections() {
        selectedSymbols.clear();
    }

    public Collection<String> getSelectedSymbols() {
        return selectedSymbols.keySet();
    }

    public int getColumnCount() {
        return 5;
    }

    public int getRowCount() {
        try {
            return count;
        } catch (Exception e) {
            return 0;
        }
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            if (columnIndex == 0) {
                if (selectedSymbols.containsKey(filteredSymbols[rowIndex].getKey())) {
                    return true;
                } else {
                    return false;
                }
            } else {
                switch (columnIndex) {
                    case -1:
                        return filteredSymbols[rowIndex].getKey();
                    case 1:
//                        return filteredSymbols[rowIndex].getExchange();//todo:check further
                        return ExchangeStore.getSharedInstance().getExchange(filteredSymbols[rowIndex].getExchange().trim()).getDisplayExchange(); //Display Exchange
                    case 2:
                        return filteredSymbols[rowIndex].getSymbol();
                    case 3:
                        return filteredSymbols[rowIndex].getShortDescription();
                    case 4:
                        return filteredSymbols[rowIndex].getDescription();
                    default:
                        return "";
                }
            }
        } catch (Exception e) {
            return "";
        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
//        System.out.println("=== " + columnIndex + " " + value);
//        Boolean bool = (Boolean)value;
//        selectedSymbols.put(filteredSymbols[rowIndex][EXCHANGE] + filteredSymbols[rowIndex][SYMBOL], bool);
    }

    public String getColumnName(int iCol) {
        return setting.getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return String.class;
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }


    public void resetSymbols() {
        //symbols = null;
    }


    // private class CompanySorter implements Comparator{
    public int compare(Object o1, Object o2) {
        try {
            String[] s1 = (String[]) o1;
            String[] s2 = (String[]) o2;
            return s1[2].compareToIgnoreCase(s2[2]);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setSymbol(String symbol) {

    }

    // }
}

