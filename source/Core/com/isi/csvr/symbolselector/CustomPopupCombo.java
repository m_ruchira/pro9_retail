package com.isi.csvr.symbolselector;

import com.isi.csvr.shared.NonNavigatable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.ExtendedComboInterface;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 13, 2005
 * Time: 10:25:45 PM
 */
public class CustomPopupCombo extends JComboBox implements
        Themeable, NonNavigatable, Cloneable, ExtendedComboInterface {
    protected int popupWidth;
    protected CustomPopupComboUI ui = null;
    //private CompanySelectorModel model = null;
//    private CompanyPanel companyPanel;
    //private JComboBox parent;
    private Component popupItem;

    public CustomPopupCombo(Component popupItem) {
        super();
        this.popupItem = popupItem;
//        companyPanel = new CompanyPanel();
        Theme.registerComponent(this);
        ui = new CustomPopupComboUI(popupItem);//companyPanel);
        setUI(ui);
        popupWidth = 400;
    }

    /*public CompanySelectorCombo(ComboBoxModel aModel) {
        super(aModel);
        companyPanel = new CompanyPanel();
        Theme.registerComponent(this);
        model = (CompanySelectorModel) aModel;
        ui = new CompanySelectorComboUI(companyPanel);
        setUI(ui);
        popupWidth = 0;
        //calculatePopupWidth();
        popupWidth = 400;
    }*/

    public String getSymbol() {
        if (getSelectedItem() instanceof Company) {
            return ((Company) getSelectedItem()).getSymbol();
        } else {
            return (String) getSelectedItem();
        }
    }

    public void setSymbol(String symbol) {
//        for (int i = 0; i < model.getSize(); i++) {
//            if (((Company) model.getElementAt(i)).getSymbol().equals(symbol)) {
//                setSelectedIndex(i);
//                break;
//            }
//        }
    }

/*    public CompanySelector(final Object[] items) {
    super(items);
    ui = new CompanySelectorUI();
    setUI(ui);
    popupWidth = 0;
}

public CompanySelector(Vector items) {
    super(items);
    ui = new CompanySelectorUI();
    setUI(ui);
    popupWidth = 0;
}*/

    public void setPopupWidth(int width) {
        popupWidth = width;
    }

    public Dimension getPopupSize() {
        //Dimension size = getSize();
        //if (popupWidth < 1) popupWidth = size.width;
        return new Dimension(popupWidth, 100);
    }

    /*public void setParent(JComboBox parent){
        this.parent = parent;
    }*/

    public void applyTheme() {

        //calculatePopupWidth();
        super.updateUI();
        setUI(ui);
        SwingUtilities.updateComponentTreeUI(this);
    }

    /*private void calculatePopupWidth() {
        int size = 0;
        int length = 0;
        int maxLength = 0;
        Company company;
        JLabel lbl = new JLabel();
        FontMetrics metrix = lbl.getFontMetrics(lbl.getFont());
        size = model.getSize();
        for (int j = 0; j < size; j++) {
            company = (Company) model.getElementAt(j);
            length = metrix.stringWidth(company.getDescription());
            if (maxLength < length) {
                maxLength = length;
            }
            //System.err.println(">> " + maxLength + " " + length + " " + company.getDescription());
        }
        popupWidth = maxLength + 25;
        lbl = null;
    }*/

    public void updateUI() {
        setUI(ui);
    }

//    public Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }
}