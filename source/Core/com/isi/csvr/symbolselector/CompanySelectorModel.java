package com.isi.csvr.symbolselector;

import javax.swing.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class CompanySelectorModel extends DefaultComboBoxModel {

    private Company[] companies = null;
    //private JComboBox parent;

    public CompanySelectorModel(Company[] companies) {
        this.companies = companies;
        //this.parent = parent;
    }

    public Object getSelectedItem() {
        try {
            return super.getSelectedItem();
        } catch (Exception ex) {
            return super.getSelectedItem();
        }
    }

    public int getSize() {
        return companies.length;
    }

    public Object getElementAt(int index) {
        return companies[index];
    }
}