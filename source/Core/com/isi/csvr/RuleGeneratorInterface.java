package com.isi.csvr;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Sep 13, 2006
 * Time: 6:42:52 PM
 * To change this template use File | Settings | File Templates.
 */
public interface RuleGeneratorInterface {
    void setSymbol(String key);

    void notifyInvalidSymbol(String symbol);
}
