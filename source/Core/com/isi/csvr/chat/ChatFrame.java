package com.isi.csvr.chat;

import com.isi.csvr.Client;
import com.isi.csvr.FontChooser;
import com.isi.csvr.TWFileFilter;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.MinimalHTMLWriter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Dharmendra Attanayak
 * Date: Sep 1, 2006
 * Time: 12:52:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChatFrame extends InternalFrame implements Themeable, ActionListener, KeyListener {

    public static SimpleAttributeSet other = new SimpleAttributeSet();
    public static SimpleAttributeSet self = new SimpleAttributeSet();
    public static SimpleAttributeSet server = new SimpleAttributeSet();
    public static SimpleAttributeSet chattextself = new SimpleAttributeSet();
    public static SimpleAttributeSet chattextother = new SimpleAttributeSet();
    JScrollPane chatTextScroll = new JScrollPane();
    JScrollPane inputTextScroll = new JScrollPane();
    JTextPane chatTextArea = new JTextPane();
    TWTextArea inputTextArea = new TWTextArea();
    JPanel bottomPanel = new JPanel();
    TWButton sendButton = new TWButton();
    TWButton btnFontSelector;
    TWButton btnColorSelector;
    TWButton btnItalic;
    TWButton btnBold;
    TWButton btnSave;
    BasicSplitPaneDivider divider = null;
    CustomerMain customermain;
    Thread custemerthread;
    static {
        StyleConstants.setForeground(other, Color.black);
        StyleConstants.setBold(other, true);
        StyleConstants.setFontFamily(other, "Helvetica");
        StyleConstants.setFontSize(other, 13);

        StyleConstants.setForeground(self, Color.blue);
        StyleConstants.setBold(self, true);
        StyleConstants.setFontFamily(self, "Helvetica");
        StyleConstants.setFontSize(self, 13);

        StyleConstants.setForeground(chattextself, Color.gray);
        StyleConstants.setItalic(chattextself, true);
        StyleConstants.setFontFamily(chattextself, "Helvetica");
        StyleConstants.setFontSize(chattextself, 13);

        StyleConstants.setForeground(chattextother, Color.red);
        StyleConstants.setItalic(chattextother, true);
        StyleConstants.setFontFamily(chattextother, "Helvetica");
        StyleConstants.setFontSize(chattextother, 13);

        StyleConstants.setForeground(server, Color.black);
        StyleConstants.setFontFamily(server, "Helvetica");
        StyleConstants.setFontSize(server, 14);
    }
    private JSplitPane splitPaneV;
    private boolean isCSTConnected = false;
    private boolean isLastMessageShowed = false;
    private FontChooser fontChooser;
    private JColorChooser colorChooser;
    private JDialog colorDialog;

    public ChatFrame() {
        super();
        CustomerMain.isTimeExpired = false;
        CustomerMain.startTime = (new Date()).getTime();
        CustomerMain.connectionActiveTime = 0;
        CustomerMain.RECONNECTIONS = 0;
        CustomerMain.CSTID = "";
        createUI();
        customermain = new CustomerMain(this);
        custemerthread = new Thread(customermain, "Chat customer");
        custemerthread.start();
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        applyTheme();
        divider = ((BasicSplitPaneUI) splitPaneV.getUI()).getDivider();
        setButtons();
    }

    public void createUI() {
        this.setSize(new Dimension(450, 500));
        this.setTitle(Language.getString("CHAT_WINDOW"));
        this.setFont(new TWFont("Arial", Font.PLAIN, 12));
        setBackground(Color.white);
//        setBackground(Theme.getColor("CHAT_WINDOW_BGCOLOR"));
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);
        splitPaneV = new JSplitPane(JSplitPane.VERTICAL_SPLIT) {

            public void updateUI() {
                super.updateUI();    //To change body of overridden methods use File | Settings | File Templates.
//                divider = ((BasicSplitPaneUI) super.getUI()).getDivider();
                if (divider != null)
                    setButtons();
            }

//            public void setContinuousLayout(boolean newContinuousLayout) {
//                super.setContinuousLayout(false);    //To change body of overridden methods use File | Settings | File Templates.
//            }
//
//            public void setDividerSize(int newSize) {
//                super.setDividerSize(26);    //To change body of overridden methods use File | Settings | File Templates.
//            }
//
//            public void setLeftComponent(Component comp) {
//                super.setLeftComponent(chatTextScroll);    //To change body of overridden methods use File | Settings | File Templates.
//            }
//
//            public void setRightComponent(Component comp) {
//                super.setRightComponent(bottomPanel);    //To change body of overridden methods use File | Settings | File Templates.
//            }
//
//            public void setDividerLocation(double proportionalLocation) {
//                super.setDividerLocation(380);    //To change body of overridden methods use File | Settings | File Templates.
//            }
        };
        topPanel.add(splitPaneV, BorderLayout.CENTER);
        creatUpperpanel();
        createLowerpanel();

        splitPaneV.setContinuousLayout(false);
        splitPaneV.setDividerSize(26);
        splitPaneV.setLeftComponent(chatTextScroll);
        splitPaneV.setRightComponent(bottomPanel);
        splitPaneV.setDividerLocation(380);

//        this.setSplitPane(splitPaneV);


        Client.getInstance().getDesktop().add(this);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setResizable(true);
        setClosable(true);
        setIconifiable(true);
        this.setLayer(GUISettings.TOP_LAYER);
        GUISettings.applyOrientation(getContentPane());
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
//        this.addInternalFrameListener(this);
        inputTextArea.requestFocus();
        if (Language.isLTR()) {
            inputTextArea.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        } else {
            inputTextArea.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }
        inputTextArea.setEditable(false);
        sendButton.setEnabled(false);
        this.setShowServicesMenu(false);
        this.hideTitleBarMenu();
    }

//    private void set

    private void setButtons() {
//        if(divider!=null){
//            divider.removeAll();
//        }
//        divider = null;
        divider = ((BasicSplitPaneUI) splitPaneV.getUI()).getDivider();
        if (divider != null) {
            divider.setLayout(new BorderLayout(2, 2));   //new FlowLayout(FlowLayout.LEADING,5,2));
            divider.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 2));  //new FlexGridLayout(new String[]{"0","0","0","0"},new String[]{"20"},2,2 ));
            //        btnFontSelector = new TWButton("Font");
            btnFontSelector = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/chat_font.gif"));
            //        btnFontSelector.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_font.gif"));
            btnFontSelector.addActionListener(this);
            btnFontSelector.setPreferredSize(new Dimension(20, 20));
            btnFontSelector.setCursor(new Cursor(Cursor.HAND_CURSOR));
            panel.add(btnFontSelector);
            btnColorSelector = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/chat_color.gif"));
            //        btnColorSelector.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_color.gif"));
            btnColorSelector.addActionListener(this);
            btnColorSelector.setPreferredSize(new Dimension(20, 20));
            btnColorSelector.setCursor(new Cursor(Cursor.HAND_CURSOR));
            panel.add(btnColorSelector);
            btnItalic = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/chat_italic.gif"));
            //        btnItalic.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_italic.gif"));
            btnItalic.addActionListener(this);
            btnItalic.setPreferredSize(new Dimension(20, 20));
            btnItalic.setCursor(new Cursor(Cursor.HAND_CURSOR));
            panel.add(btnItalic);
            btnBold = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/chat_bold.gif"));
            //        btnBold.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_bold.gif"));
            btnBold.addActionListener(this);
            btnBold.setPreferredSize(new Dimension(20, 20));
            btnBold.setCursor(new Cursor(Cursor.HAND_CURSOR));
            panel.add(btnBold);
            btnSave = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/chat_save.gif"));
            //        btnBold.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_bold.gif"));
            btnSave.addActionListener(this);
            btnSave.setPreferredSize(new Dimension(20, 20));
            btnSave.setCursor(new Cursor(Cursor.HAND_CURSOR));
            panel.add(btnSave);
            divider.add(panel, BorderLayout.CENTER);
            GUISettings.applyOrientation(divider);
        }
    }

    public void creatUpperpanel() {
        chatTextScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        chatTextScroll.setBounds(new Rectangle(0, 0, 390, 300));
        chatTextScroll.getViewport().add(chatTextArea, null);
        chatTextArea.setEditable(false);
        chatTextArea.setBackground(Color.white);
    }

    public void createLowerpanel() {
        inputTextArea.setBorder(BorderFactory.createEmptyBorder());
        inputTextArea.setWrapStyleWord(true);
        inputTextArea.setLineWrap(true);
        bottomPanel.setLayout(new FlexGridLayout(new String[]{"100%", "85"}, new String[]{"100%"}));
        inputTextScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        inputTextScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
//        inputTextArea.setAutoscrolls(false);
        inputTextScroll.setBounds(new Rectangle(0, 0, 390, 150));
        inputTextScroll.getViewport().add(inputTextArea, null);
        bottomPanel.add(inputTextScroll);
        JPanel leftpanel = new JPanel();

        bottomPanel.add(leftpanel);
        leftpanel.setLayout(new FlexGridLayout(new String[]{"81"}, new String[]{"30", "30", "30", "30", "30", "30",}, 4, 4));
        sendButton.setBounds(0, 0, 80, 25);
        sendButton.setEnabled(false);
//        sendButton.setToolTipText("Press To Send");
        sendButton.setText(Language.getString("SEND"));
        leftpanel.add(sendButton);
        sendButton.addActionListener(this);
        inputTextArea.addKeyListener(this);
        bottomPanel.setBackground(Color.white);
        leftpanel.setBackground(Color.white);
        inputTextArea.setBackground(Color.white);
        inputTextScroll.setBorder(BorderFactory.createEmptyBorder());

    }

    protected void insertText(String text, AttributeSet set) {
        try {
            if (Language.isLTR()) {
                chatTextArea.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            } else {
                chatTextArea.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            }
            chatTextArea.getDocument().insertString(chatTextArea.getDocument().getLength(), text, set);
            if (chatTextArea.getDocument().getLength() > 0)
                chatTextArea.setCaretPosition(chatTextArea.getDocument().getLength() - 1);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        setBackground(Color.white);
        if (divider != null) {
            divider = ((BasicSplitPaneUI) splitPaneV.getUI()).getDivider();
//            divider.removeAll();
            setButtons();
//            btnFontSelector.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_font.gif"));
//            btnColorSelector.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_color.gif"));
//            btnItalic.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_italic.gif"));
//            btnBold.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/chat_bold.gif"));
//            divider.repaint();
        }
//        setBackground(Theme.getColor("CHAT_WINDOW_BGCOLOR"));
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_ENTER) {
            e.consume();
            if (isCSTConnected && !inputTextArea.getText().equals("")) {
                customermain.write(MessageFormater.makeTextMessage(UnicodeUtils.getCompressedUnicodeString(inputTextArea.getText())));
//                insertText(Settings.getUserName() + " >>>", self);
//                insertText(inputTextArea.getText() + "\n", chattextself);
                if (Language.isLTR()) {
                    insertText(Settings.getUserName() + " >>> ", self);
                    insertText(inputTextArea.getText() + "\n", chattextself);
                } else {
                    try {
//                        System.out.println("Last charactor == "+UnicodeUtils.getUnicodeString(inputTextArea.getText((inputTextArea.getText().length()-2),1)));
                        int index = 0;
                        if (inputTextArea.getText().length() > 1) {
                            index = (inputTextArea.getText().length() - 2);
                        }
                        if (UnicodeUtils.getUnicodeString(inputTextArea.getText(index, 1)).substring(0, 4).equals("\\u06")) {
                            insertText(Settings.getUserName() + " >>> ", self);
                            insertText(inputTextArea.getText() + "\n", chattextself);
                        } else {
                            insertText(inputTextArea.getText(), chattextself);
                            insertText(" <<< " + Settings.getUserName() + "\n", self);
                        }
                    } catch (BadLocationException e1) {
                        insertText(inputTextArea.getText(), chattextself);
                        insertText(" <<< " + Settings.getUserName() + "\n", self);
                    }
//                    insertText(inputTextArea.getText() , chattextself);
//                    insertText(" <<< "+Settings.getUserName()+ "\n" , self);
                }
                inputTextArea.setText("");
            }

        }
    }

    public void keyReleased(KeyEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        String arg = e.getActionCommand();
        if (e.getSource() == sendButton) {
            if (!inputTextArea.getText().equals("")) {
                customermain.write(MessageFormater.makeTextMessage(UnicodeUtils.getCompressedUnicodeString(inputTextArea.getText())));
                if (Language.isLTR()) {
                    insertText(Settings.getUserName() + " >>> ", self);
                    insertText(inputTextArea.getText() + "\n", chattextself);
                } else {
                    try {
                        int index = 0;
                        if (inputTextArea.getText().length() > 1) {
                            index = (inputTextArea.getText().length() - 2);
                        }
                        if (UnicodeUtils.getUnicodeString(inputTextArea.getText(index, 1)).substring(0, 4).equals("\\u06")) {
                            insertText(Settings.getUserName() + " >>> ", self);
                            insertText(inputTextArea.getText() + "\n", chattextself);
                        } else {
                            insertText(inputTextArea.getText(), chattextself);
                            insertText(" <<< " + Settings.getUserName() + "\n", self);
                        }
//                        insertText(inputTextArea.getText() , chattextself);
//                        insertText(" <<< "+Settings.getUserName()+ "\n" , self);
                    } catch (BadLocationException e1) {
                        insertText(inputTextArea.getText(), chattextself);
                        insertText(" <<< " + Settings.getUserName() + "\n", self);
                    }
                }
                inputTextArea.setText("");
            }
            inputTextArea.requestFocus();
        } else if (e.getSource() == btnFontSelector) {
            fontChooser = new FontChooser(Client.getInstance().getFrame(), true);
            SwingUtilities.updateComponentTreeUI(fontChooser);
            int style;
            if (StyleConstants.isBold(chattextself))
                style = Font.BOLD;
            else if (StyleConstants.isItalic(chattextself))
                style = Font.ITALIC;
            else
                style = Font.PLAIN;
            Font oldFont = new TWFont(StyleConstants.getFontFamily(chattextself), style, StyleConstants.getFontSize(chattextself));
            Font newBodyFont = fontChooser.showDialog(Language.getString("SELECT_FONT"), oldFont);
            if ((newBodyFont != null) && (newBodyFont != oldFont)) {
                StyleConstants.setFontFamily(chattextself, newBodyFont.getFontName());
                StyleConstants.setFontSize(chattextself, newBodyFont.getSize());
                if (newBodyFont.getStyle() == Font.BOLD) {
                    StyleConstants.setBold(chattextself, true);
                    StyleConstants.setItalic(chattextself, false);
                } else if (newBodyFont.getStyle() == Font.ITALIC) {
                    StyleConstants.setItalic(chattextself, true);
                    StyleConstants.setBold(chattextself, false);
                } else {
                    StyleConstants.setBold(chattextself, false);
                    StyleConstants.setItalic(chattextself, false);
                }
            }
//            StyleConstants.setForeground(chattextself, Color.green);
//            StyleConstants.setBold(chattextself, true);
//            insertText("Font button clicked", chattextself);
            inputTextArea.setFont(newBodyFont);
            inputTextArea.requestFocus();
        } else if (e.getSource() == btnColorSelector) {
            Color oldColor = StyleConstants.getForeground(chattextself);
            colorChooser = new JColorChooser(oldColor);
            JLabel previewPane = new JLabel(Language.getString("CLIENT"));
            previewPane.setFont(new TWFont("Arial", Font.BOLD, 20));
            colorChooser.setPreviewPanel(previewPane);
            colorChooser.setColor(oldColor);
            colorDialog = JColorChooser.createDialog(Client.getInstance().getFrame(), Language.getString("SELECT_COLOR"), true, colorChooser, this, this);
            SwingUtilities.updateComponentTreeUI(colorChooser);
            SwingUtilities.updateComponentTreeUI(colorDialog);
            colorDialog.show();
            Color newColor = colorChooser.getColor();
            if (oldColor != newColor) {
                StyleConstants.setForeground(chattextself, newColor);
            }
//            insertText("color button clicked", chattextself);
            inputTextArea.setForeground(newColor);
            inputTextArea.requestFocus();
        } else if (e.getSource() == btnBold) {
            boolean isBold = StyleConstants.isBold(chattextself);
            StyleConstants.setItalic(chattextself, false);
            StyleConstants.setBold(chattextself, !isBold);
            int style = inputTextArea.getFont().getStyle();
            if (style == Font.BOLD) {
                style = Font.PLAIN;
            } else {
                style = Font.BOLD;
            }
            inputTextArea.setFont(inputTextArea.getFont().deriveFont(style));
            inputTextArea.requestFocus();
        } else if (e.getSource() == btnItalic) {
            boolean isItalic = StyleConstants.isItalic(chattextself);
            StyleConstants.setBold(chattextself, false);
            StyleConstants.setItalic(chattextself, !isItalic);
            int style = inputTextArea.getFont().getStyle();
            if (style == Font.ITALIC) {
                style = Font.PLAIN;
            } else {
                style = Font.ITALIC;
            }
            inputTextArea.setFont(inputTextArea.getFont().deriveFont(style));
            inputTextArea.requestFocus();
        } else if (e.getSource() == btnSave) {
            saveDialog();
        }
    }


    public void enableSendOption(boolean status) {
        sendButton.setEnabled(status);
        inputTextArea.setEditable(status);
    }

    private int getOption() {
        int type = -1;
        if (!isLastMessageShowed) {
            type = JOptionPane.showConfirmDialog(this, Language.getString("CHAT_SAVE_BEFORE_EXIT"), Language.getString("WARNING"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
            isLastMessageShowed = true;
        }
        return type;
    }

    private void saveDialog() {
        JFileChooser chooser = new SmartFileChooser(Settings.getAbsolutepath());
        String[] extentions = new String[1];
        extentions[0] = "html";
        TWFileFilter oFilter = new TWFileFilter("HTML", extentions);
        chooser.setFileFilter(oFilter);
        chooser.setAcceptAllFileFilterUsed(false);
//            chooser.s
        if (chooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) {
            chooser.setVisible(false);
        }
        File file = chooser.getSelectedFile();
        if (file == null) {
            chooser.setVisible(false);
        }
        String sFile = file.getAbsolutePath();
        if (!sFile.toLowerCase().endsWith(".html")) {
            sFile += ".html";
        }
        FileWriter writer = null;
        try {
            writer = new FileWriter(sFile);
            MinimalHTMLWriter htmlWriter = new MinimalHTMLWriter(writer, chatTextArea.getStyledDocument());
            htmlWriter.write();
        } catch (IOException ex) {
//                JOptionPane.showMessageDialog(this, "HTML File Not Saved", "ERROR", JOptionPane.ERROR_MESSAGE);
        } catch (BadLocationException ex) {
//                JOptionPane.showMessageDialog(this, "HTML File Corrupt", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
//            finally {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException x) {
            }
        }
//            }
        if (!Language.isLTR()) {
            try {
                BufferedReader in = new BufferedReader(new FileReader(sFile));
                String str;
                String str2;
                StringBuilder builder = new StringBuilder();
                while ((str = in.readLine()) != null) {
                    if (str.trim().toLowerCase().startsWith("<body")) {
                        str2 = str.substring(0, str.indexOf("y") + 1) + " dir=\"rtl\"" + str.substring(str.indexOf("y") + 1);
                        builder.append(str2);
                        str2 = null;
                    } else {
                        builder.append(str);
                    }
                    str = null;
                }
                in.close();
                in = null;
                BufferedWriter out = new BufferedWriter(new FileWriter(sFile));
                out.write(builder.toString());
                out.flush();
                out.close();
                out = null;
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void internalFrameClosing(InternalFrameEvent e) {
//        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        try {
//            SharedMethods.printLine((e.getInternalFrame().getName()),true);
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        int type = getOption();
//        int type = SharedMethods.showConfirmMessage("Do u want to Save before Exit", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
        if (type == JOptionPane.YES_OPTION) {
//            this.setVisible(false);
            saveDialog();
            try {
                inputTextArea.setEditable(false);
                sendButton.setEnabled(false);
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

//            CustomerMain.isServerConnected = false;
            closeWindow();

//            if (!isCSTConnected) {
//                System.out.println("Not connected");
//                closeWindow();
//            } else {
//                if (!CustomerMain.isCSTLogout) {
//                    closeWindow();
//                } else {
//                    System.out.println("Already logged out!!!");
//                    closeWindow();
//                }
//            }
//            chatTextArea.getStyledDocument().
        } else if (type == JOptionPane.NO_OPTION) {
            this.setVisible(false);
            try {
                inputTextArea.setEditable(false);
                sendButton.setEnabled(false);
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

//            CustomerMain.isServerConnected = false;
            closeWindow();

//            if (!isCSTConnected) {
//                System.out.println("Not connected");
//                closeWindow();
//            } else {
//                if (!CustomerMain.isCSTLogout) {
//                    closeWindow();
//                } else {
//                    System.out.println("Already logged out!!!");
//                    closeWindow();
//                }
//            }
        } else {
            isLastMessageShowed = false;
            return;

        }

    }


    public void closeWindow() {
        synchronized (CustomerMain.socketLock) {
            try {
                customermain.write(MessageFormater.makeLogoutMessage());
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            try {
                if (customermain != null) {
                    customermain.closeSockets();
                    customermain = null;
                }
                if (custemerthread != null) {
                    custemerthread.interrupt();
                    custemerthread = null;
                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
            CustomerMain.isTimeExpired = true;
            CustomerMain.isServerConnected = false;
            this.dispose();
        }
    }

    public void timeOutMessage(boolean showMessage) {
        inputTextArea.setEditable(false);
        sendButton.setEnabled(false);
        CustomerMain.isTimeExpired = true;
        isCSTConnected = false;
        if (showMessage && !isLastMessageShowed) {
            JOptionPane.showMessageDialog(Client.getInstance().getFrame(), Language.getString("CHAT_LOG_OUT"), Language.getString("INFORMATION"), JOptionPane.INFORMATION_MESSAGE);
        }
        try {
            if (!CustomerMain.isCSTLogout) {
                customermain.write(MessageFormater.makeLogoutMessage());
            }
            customermain.closeSockets();
            custemerthread.interrupt();
            customermain = null;
            custemerthread = null;

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        CustomerMain.isServerConnected = false;
    }

    public void setCSTConnected(boolean status) {
        sendButton.setEnabled(status);
        isCSTConnected = status;
    }

//    public void setClosable(boolean b) {
//        super.setClosable(isLastMessageShowed);    //To change body of overridden methods use File | Settings | File Templates.
//    }

    public void reconnectMessage(boolean dueToerror) {
        int option = 0;
        if (!isLastMessageShowed) {
            option = JOptionPane.showConfirmDialog(Client.getInstance().getFrame(), Language.getString("CHAT_RECONNECT_MSG"), Language.getString("INFORMATION"),
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
        }
        if (option == JOptionPane.OK_OPTION) {
            customermain.reset(true);
            try {
                CustomerMain.startTime = (new Date()).getTime();
                if (!dueToerror) {
                    if (!custemerthread.isAlive()) {
                        custemerthread.start();
                    } else {
                        custemerthread.interrupt();
                    }
                } else {
                    try {
                        custemerthread = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        custemerthread = new Thread(customermain, "new Customer Main");
                        custemerthread.start();
                    } catch (Exception e) {
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (option == JOptionPane.CANCEL_OPTION) {
            synchronized (CustomerMain.socketLock) {
                timeOutMessage(false);
            }
        }
    }
}
