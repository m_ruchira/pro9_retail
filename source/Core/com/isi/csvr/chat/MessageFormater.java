package com.isi.csvr.chat;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Aug 7, 2006
 * Time: 12:53:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class MessageFormater {

    public static String makeJoinMessage() {
        String message = Meta.MT_CLIENT_AUTHENTICATION + Meta.DS + "A" + Settings.getUserName() + Meta.FD + "C" + Settings.getUserID() + Meta.FD + "D" + Settings.getSessionID() /*"276AFA9F-6094-46EF-929F-006713CEF5FE" */ + Meta.FD +
                "E" + Language.getLanguageTag() + Meta.FD + "F" + Meta.ProductType + Meta.FD + "H" + Settings.TW_VERSION;
        if (!CustomerMain.CSTID.equals("")) {
            message = message + Meta.FD + "G" + CustomerMain.CSTID + Meta.EOL;
        } else {
            message = message + Meta.EOL;
        }
//        System.out.println(message);
        return message;
    }

    public static String makeTextMessage(String text) {
        return Meta.MT_TEXT_MESSAGE + Meta.DS + "A" + Settings.getUserID() + Meta.FD + "B" + text + Meta.EOL;
    }

    public static String makeLogoutMessage() {
        return Meta.MT_CLIENT_LOGOUT + Meta.DS + Meta.EOL;
    }

    public static String makeCustomerPulse() {
        return Meta.MT_CLIENT_PULSE + Meta.DS + Meta.EOL;
    }
}
