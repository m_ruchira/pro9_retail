package com.isi.csvr.chat;

import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Aug 16, 2006
 * Time: 2:15:01 PM
 * To change this template use File | Settings | File Templates.
 */

public class newClientUI extends JFrame {

    //text color details
    static SimpleAttributeSet ITALIC_GRAY = new SimpleAttributeSet();
    static SimpleAttributeSet BOLD_BLACK = new SimpleAttributeSet();
    static SimpleAttributeSet BLACK = new SimpleAttributeSet();

    static {
        StyleConstants.setForeground(ITALIC_GRAY, Color.red);
        StyleConstants.setItalic(ITALIC_GRAY, true);
        StyleConstants.setFontFamily(ITALIC_GRAY, "Helvetica");
        StyleConstants.setFontSize(ITALIC_GRAY, 13);

        StyleConstants.setForeground(BOLD_BLACK, Color.blue);
        StyleConstants.setBold(BOLD_BLACK, true);
        StyleConstants.setFontFamily(BOLD_BLACK, "Helvetica");
        StyleConstants.setFontSize(BOLD_BLACK, 13);

        StyleConstants.setForeground(BLACK, Color.black);
        StyleConstants.setFontFamily(BLACK, "Helvetica");
        StyleConstants.setFontSize(BLACK, 14);
    }
    JScrollPane chatTextScroll = new JScrollPane();
    JScrollPane inputTextScroll = new JScrollPane();
    JTextPane chatTextArea = new JTextPane();
    JTextArea inputTextArea = new JTextArea();
    JPanel bottomPanel = new JPanel();
    JButton sendButton = new JButton();
    JButton clearButton = new JButton();
    JButton joinButton = new JButton();
    JButton quitButton = new JButton();
    JTextField usernameField = new JTextField();
    JTextField userIDField = new JTextField();
    private JSplitPane splitPaneV;

    public newClientUI() {
        this.setSize(new Dimension(350, 500));
        this.setTitle("ClientInterface");
        setBackground(Color.gray);
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BorderLayout());
        getContentPane().add(topPanel);
        splitPaneV = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        topPanel.add(splitPaneV, BorderLayout.CENTER);
        creatUpperpanel();
        createLowerpanel();

        splitPaneV.setContinuousLayout(false);
        splitPaneV.setDividerSize(8);
        splitPaneV.setLeftComponent(chatTextScroll);
        splitPaneV.setRightComponent(bottomPanel);
        splitPaneV.setDividerLocation(250);


    }

    public void creatUpperpanel() {
        chatTextScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        chatTextScroll.setBounds(new Rectangle(0, 0, 390, 300));
        chatTextScroll.getViewport().add(chatTextArea, null);
    }

    public void createLowerpanel() {

        bottomPanel.setLayout(new FlexGridLayout(new String[]{"75%", "25%"}, new String[]{"100%"}));
        //inputTextScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        inputTextScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        inputTextScroll.setBounds(new Rectangle(0, 0, 390, 150));
        inputTextScroll.getViewport().add(inputTextArea, null);
        bottomPanel.add(inputTextScroll);
        JPanel leftpanel = new JPanel();

        bottomPanel.add(leftpanel);
        leftpanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "30", "30", "30", "30", "30",}, 4, 4));
        sendButton.setBounds(0, 0, 80, 25);
        sendButton.setEnabled(true);
        sendButton.setToolTipText("Press To Send");
        sendButton.setText("Send");
        clearButton.setBounds(0, 30, 80, 25);
        clearButton.setEnabled(true);
        clearButton.setToolTipText("Press To Send");
        clearButton.setText("Clear");
        joinButton.setBounds(0, 30, 80, 25);
        joinButton.setEnabled(true);
        joinButton.setToolTipText("Press To Send");
        joinButton.setText("Join");
        quitButton.setBounds(0, 30, 80, 25);
        quitButton.setEnabled(true);
        quitButton.setToolTipText("Press To Send");
        quitButton.setText("Quit");
        usernameField.setToolTipText("Enter your Username");
        usernameField.setBounds(new Rectangle(140, 20, 80, 25));
        userIDField.setToolTipText("Enter your Username");
        userIDField.setBounds(new Rectangle(140, 55, 80, 25));
        leftpanel.add(sendButton);
        leftpanel.add(clearButton);
        leftpanel.add(joinButton);
        leftpanel.add(quitButton);
        leftpanel.add(usernameField);
        leftpanel.add(userIDField);
    }

    protected void insertText(String text, AttributeSet set) {
        try {
            chatTextArea.getDocument().insertString(
                    chatTextArea.getDocument().getLength(), text, set);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    // Needed for inserting icons in the right places
    protected void setEndSelection() {
        chatTextArea.setSelectionStart(chatTextArea.getDocument().getLength());
        chatTextArea.setSelectionEnd(chatTextArea.getDocument().getLength());
    }

}
