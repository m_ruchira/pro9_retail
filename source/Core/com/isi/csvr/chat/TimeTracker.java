package com.isi.csvr.chat;

/**
 * Created by IntelliJ IDEA.
 * User: Dharmendra Attanayak
 * Date: Aug 28, 2006
 * Time: 10:08:02 AM
 * To change this template use File | Settings | File Templates.
 */

import java.util.Date;

/**
 * Simple demo that uses java.util.Timer to schedule a task to execute
 * once 5 seconds have passed.
 */

public class TimeTracker extends Thread {

    private CustomerMain parent;
    private int seconds;
    private boolean isActive = true;

    public TimeTracker(int seconds, CustomerMain parent) {
        super("Timer Thread in Chat");
        this.parent = parent;
        this.seconds = seconds;
        CustomerMain.RECONNECTIONS = 0;
//        System.out.println("seconds="+seconds);
        this.start();
    }

    public void run() {
        isActive = true;
        boolean isTimesUp = false;
        while (isActive) {
            if (CustomerMain.isServerConnected && !CustomerMain.isCSTLogout && !CustomerMain.holdThread) {
                long now = (new Date()).getTime();
                if (((now - CustomerMain.startTime) / 1000) >= seconds) {
                    CustomerMain.startTime = now;
                    showMessage();
                } else {
                    CustomerMain.connectionActiveTime = ((now - CustomerMain.startTime) / 1000);
                }
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void stopThread() {
        try {
            isActive = false;
            this.interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void startThread() {
        try {
            isActive = true;
            this.interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void exitThread() {
        try {
            isActive = false;
            this.interrupt();
            parent = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void showMessage() {
        if (CustomerMain.MAX_RECONNECTIONS <= CustomerMain.RECONNECTIONS) {
            CustomerMain.RECONNECTIONS = 0;
//            CustomerMain.isTimeExpired = true;
            synchronized (CustomerMain.socketLock) {
                parent.getChatFrame().timeOutMessage(true);
            }
        } else {
            CustomerMain.RECONNECTIONS++;
            parent.reconnect();
        }
    }


}
