package com.isi.csvr.chat;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;

import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Aug 7, 2006
 * Time: 11:28:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerMain implements Runnable {

    public static final String socketLock = "SocketLock";
    public static boolean isCSTLogout = false;
    public static String CSTID = "";
    public static Date lastupDate;
    public static boolean notconnected = true;
    public static int RECONNECTIONS = 0;
    public static int MAX_RECONNECTIONS = 1;
    public static boolean isTimeExpired = false;
    public static long startTime = 0;
    public static long connectionActiveTime = 0;
    public static boolean isServerConnected = false;
    public static boolean holdThread;
    public CustomerPulse cp;
    protected InputStream inStream;
    protected OutputStream outStream;
    String[] tokens = null;
    String splitPattern;
    String message;
    String loginTime;
    String CSTloginname;
    String AcceptedTime;
    String AlowableChatTime;
    Socket s = null;
    private TimeTracker timeTracker;
    private boolean isActive = true;
    private boolean dueToError = false;
    private ChatFrame parent;

    public CustomerMain(ChatFrame parent) {
        isServerConnected = false;
        this.parent = parent;
    }

    public ChatFrame getChatFrame() {
        return parent;
    }

    public String readLine(InputStream in) throws Exception {
        int iValue;
        StringBuilder buffer = new StringBuilder();

        while (true) {
            try {
                iValue = in.read();

                if (iValue == -1)
                    throw new Exception("End of Stream");

                if (iValue != '\n') {
                    buffer.append((char) iValue);
                } else {
                    return buffer.toString();
                }
            } catch (Exception e) {
                throw new Exception("Error in input Stream");
            }
        }
    }

    public void run() {
//        if(!isServerConnected ){
//            try {
////                s = new Socket("192.168.0.142", Settings.CHAT_SERVER_PORT);
//                s = new Socket(Settings.CHAT_SERVER_IP, Settings.CHAT_SERVER_PORT);
////                s = new Socket("213.42.22.68", 80);
//                inStream = new DataInputStream(new BufferedInputStream(s.getInputStream()));
//                outStream = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));
//                System.out.println("from the start");
//                write(MessageFormater.makeJoinMessage());
//                if(cp == null)
//                    cp = new CustomerPulse(this, 15);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

        while (isActive) {
            try {
                if (inStream != null)
                    frameAnalyzer(readLine(inStream));
                dueToError = false;
//                System.out.println("end of frame analyser");
            } catch (Exception e) {
                System.out.println("Error Occured!!!");
                dueToError = true;
                if (isServerConnected) {
                    break;
                }
            }

            synchronized (socketLock) {
                if (!isTimeExpired) {
                    if (!isServerConnected) {
                        try {
//                            System.out.println("inside the sync block");
//                            s = new Socket("192.168.0.142", Settings.CHAT_SERVER_PORT);
                            s = new Socket(Settings.CHAT_SERVER_IP, Settings.CHAT_SERVER_PORT);
//                            s = new Socket("213.42.22.68", 80);
                            inStream = new DataInputStream(new BufferedInputStream(s.getInputStream()));
                            outStream = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));
//                            System.out.println("from while loop");
                            write(MessageFormater.makeJoinMessage());
                            if (cp == null)
                                cp = new CustomerPulse(this, 15);
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                            }
                        }
                    }
                } else {
                    break;
                }
            }

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
            }
        }

        try {
            if (isServerConnected && dueToError && !isTimeExpired) {
                parent.enableSendOption(false);
                isServerConnected = false;
//                System.out.println("from run");
                parent.insertText(Language.getString("SERVER_DISCONNECTED") + "\n", ChatFrame.server);
                parent.reconnectMessage(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reset(boolean status) {
        this.isActive = status;
    }

    public void reconnect() {
        parent.reconnectMessage(false);
    }

    public synchronized void write(String data) {
        try {
//            System.out.println("Out data == "+data);
            outStream.write(data.getBytes());
            outStream.flush();
            lastupDate = new Date();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void frameAnalyzer(String frame) {
//        System.out.println("in data="+frame);
        splitPattern = Meta.DS;
        tokens = frame.split(splitPattern);

        if (Integer.parseInt(tokens[0]) == Meta.MT_CLIENT_AUTHENTICATION) {
            displayLoginReply(tokens[1]);
        } else if (Integer.parseInt(tokens[0]) == Meta.MT_TEXT_MESSAGE) {
            displayTextMessage(tokens[1]);
        } else if (Integer.parseInt(tokens[0]) == Meta.MT_CST_LOGOUT) {
            displayCSTLogoutMessage();
        } else if (Integer.parseInt(tokens[0]) == Meta.MT_CST_ACCEPT_CLIENT) {
            displayCSTAcceptMessage(tokens[1]);
        } else if (Integer.parseInt(tokens[0]) == Meta.MT_CST_DISCONNECT_CLIENT) {
            displayDisconectMessage();
        } else if (Integer.parseInt(tokens[0]) == Meta.MT_CUSTOMER_HOLD) {
//            System.out.println("Customer Hold");
            customerHold(tokens[1]);
        } else if (Integer.parseInt(tokens[0]) == Meta.MT_CUSTOMER_TRANSFER) {
//            System.out.println("Transfered");
            displayCustomerTransfer();
        }
    }

    public void displayCustomerTransfer() {
        isCSTLogout = true;
        timeTracker.interrupt();
        parent.insertText(Language.getString("CHAT_HOLD_MESSAGE") + "\n", ChatFrame.server);
    }

    public void customerHold(String frame) {

        int hold = 0;
        splitPattern = Meta.EOL;
        tokens = frame.split(splitPattern);
        try {
            StringTokenizer st = new StringTokenizer(tokens[0], Meta.FD);
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                char tag = token.charAt(0);

                switch (tag) {
                    case 'A':
                        hold = Integer.parseInt(token.substring(1));
                        holdThread = (hold == 1);
                        timeTracker.interrupt();
                        break;
                }
            }
        } catch (Exception e) {
        }
    }

    public void displayLoginReply(String data) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        splitPattern = Meta.EOL;
        tokens = data.split(splitPattern);
        try {
            StringTokenizer st = new StringTokenizer(tokens[0], Meta.FD);
            isServerConnected = true;
            isCSTLogout = true;
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                char tag = token.charAt(0);
                switch (tag) {
                    case 'B':
                        message = UnicodeUtils.getNativeString(token.substring(1));
                        break;
                    case 'D':
                        loginTime = format.format(new Date(Settings.getLocalTimeFor(Long.parseLong(token.substring(1)))));
                        break;
                }
            }

            String str = Language.getString("WELCOME_TO_PRO_SUPPORT");
            parent.insertText(str + "\n", ChatFrame.server);
        } catch (Exception e) {
            System.out.println("Error Occured at receiving loging reply...");
        }
    }

    public void displayTextMessage(String str) {
        splitPattern = Meta.EOL;
        tokens = str.split(splitPattern);
        try {
            StringTokenizer st = new StringTokenizer(tokens[0], Meta.FD);

            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                char tag = token.charAt(0);
                switch (tag) {
                    case 'B':
                        message = UnicodeUtils.getNativeStringFromCompressed(token.substring(1));
                        break;
                }
                token = null;
            }
            st = null;
        } catch (Exception e) {
        }
//        if(Language.isLTR()){
        parent.insertText(Language.getString("CHAT_CST_MESSAGE") + " >>> ", ChatFrame.other);
        parent.insertText(message + "\n", ChatFrame.chattextother);
//        }else{
//            parent.insertText(message , ChatFrame.chattextother);
//            parent.insertText(" <<< "+Language.getString("CHAT_CST_MESSAGE")+ "\n", ChatFrame.other);
//        }
    }

    public void displayDisconectMessage() {
        parent.insertText(Language.getString("CHAT_SERVER_DISCONNECTED") + "\n", ChatFrame.server);
        synchronized (CustomerMain.socketLock) {
            parent.timeOutMessage(false);
        }
    }

    public void displayCSTLogoutMessage() {
        parent.insertText(Language.getString("CHAT_CST_DISCONNECTED") + "\n", ChatFrame.server);
        parent.enableSendOption(false);
        isCSTLogout = true;
    }

    public void displayCSTAcceptMessage(String str) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        int transfered = 0;
        splitPattern = Meta.EOL;
        tokens = str.split(splitPattern);
        String oldCST = CSTID;
        try {
            StringTokenizer st = new StringTokenizer(tokens[0], Meta.FD);
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                char tag = token.charAt(0);

                switch (tag) {
                    case 'A':
                        CSTloginname = token.substring(1);
                        parent.setCSTConnected(true);
                        break;
                    case 'B':
                        AcceptedTime = format.format(new Date(Settings.getLocalTimeFor(Long.parseLong(token.substring(1)))));
                        ;
                        break;
                    case 'C':
                        AlowableChatTime = token.substring(1);
                        break;
                    case 'D':
                        CSTID = token.substring(1);
                        break;
                    case 'E':
                        transfered = Integer.parseInt(token.substring(1));
                        break;
                }
            }
        } catch (Exception e) {
        }
        if (!oldCST.equals("") && oldCST.equals(CSTID)) {
            startTime = (new Date()).getTime() - connectionActiveTime * 1000;
        } else {
            startTime = (new Date()).getTime();
        }
        if (transfered == 1) {
            startTime = (new Date()).getTime();
        }
        isCSTLogout = false;
        startTimer();
        String str2 = Language.getString("CHAT_LOGIN_ACCEPTING_REPLY");
        str2 = str2.replace("[CST_NAME]", CSTloginname);
        str2 = str2.replace("[ACCEPTED_TIME]", AcceptedTime);
        str2 = str2 + "\n";
        parent.insertText(str2, ChatFrame.server);

        parent.enableSendOption(true);
    }

    public void startTimer() {
        if (timeTracker != null) {
            timeTracker.startThread();
        } else {
            timeTracker = new TimeTracker((Integer.parseInt(AlowableChatTime) * 60), this);
        }
    }

    public void closeSockets() {
        isActive = false;
        try {
            inStream.close();
            outStream.close();
            s.close();
            inStream = null;
            outStream = null;
            s = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            cp.exitThread();
            cp.interrupt();
            cp = null;
        } catch (Exception e) {
        }
        try {
            timeTracker.exitThread();
            timeTracker.interrupt();
            timeTracker = null;
        } catch (Exception e) {
        }
    }
}
