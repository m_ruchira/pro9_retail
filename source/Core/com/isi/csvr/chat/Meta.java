package com.isi.csvr.chat;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Jul 13, 2006
 * Time: 6:06:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class Meta {
    public static final int MT_CLIENT_AUTHENTICATION = 10;
    public static final int MT_TEXT_MESSAGE = 11;
    public static final int MT_CLIENT_LOGOUT = 12;
    public static final int MT_CLIENT_PULSE = 13;
    public static final int MT_CST_AUTHENTICATION = 20;
    public static final int MT_CST_ACCEPT_CLIENT = 24;
    public static final int MT_CST_DISCONNECT_CLIENT = 25;
    public static final int MT_CST_LOGOUT = 22;
    public static final int MT_CUSTOMER_INFO = 26;
    public static final int MT_CST_PULSE = 23;
    public static final int MT_CUSTOMER_TRANSFER = 30;
    public static final int MT_CUSTOMER_HOLD = 31;

    public static String DS = "\u0002";
    public static String FD = "" + (char) 25;
    public static String EOL = "\n";

    public static byte ProductType = 1; // MPro = 1
}
