/**
 * This class implements a Jtable in an Internal frame
 * which is used to display market time and sales
 */

package com.isi.csvr;

import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.customformular.CustomFormulaListener;
import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.customformular.FormularEditor;
import com.isi.csvr.datastore.*;
import com.isi.csvr.dnd.DraggableTable;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowController;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolselector.CompanyCellEditor;
import com.isi.csvr.table.UpdateableTable;
import com.isi.csvr.table.updator.TableUpdateManager;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.watchlist.WatchListStore;
import com.smardec.mousegestures.MouseGestures;
import com.smardec.mousegestures.MouseGesturesListener;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Vector;
//import java.lang.reflect.Array;

public class ClientTable extends JInternalFrame
        implements InternalFrameListener,
        WindowWrapper, Themeable, MouseListener,
        DropTargetListener, PropertyChangeListener,
        MouseMotionListener, ActionListener,
        TWDesktopInterface, ClientTableInterface,
        ApplicationListener, TWTitlePaneInterface,
//change start
        UpdateableTable, CustomFormulaListener, LinkedWindowController, KeyListener {
    //------- Public data ----------
    //Vector datavector = new Vector();

    MouseGestures mouseGestures;
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private int windowType;
    private boolean g_bSortable = true;
    private DraggableTable table;
    private DraggableTable fixedTable;
    private AbstractTableModel oDataModel;
    private String g_asHeadings[];
    private int g_aiRendIDs[];
    private String g_sCaption = "";
    private String g_sFilterCaption = "";
    private Symbols symbols = null;
    //DataStore g_oDataStore;
    private Stock g_oStock;
    private int g_iColumn = 0;
    //JScrollPane     g_jSP;
    private TreePath g_oPathOnTree;
    private JTree g_oTree;
    private boolean g_bRunning = true;
    //    TWThread g_oThread;
    private boolean g_bDirLTR = true;
    private int g_iLastSortedCol = -1;
    private int g_iSortOrder = 1;
    private TreePath g_oTreePath;
    private ViewSetting viewSettings;
    private JScrollPane g_oJScrollPane;
    private String g_sNSSymbol;
    //TableSorter     g_oSorter;
    private SortButtonRenderer renderer;
    private boolean g_bSortOrder = false;
    private boolean mouseDragging = false;
    private boolean topStocksType = false;
    private int[] g_aiColWidths;
    private Dimension minimumSize = new Dimension(100, 100);
    //ivate boolean isCellEditable;
    //private int totalWidth;
    private boolean tableLocked;
    private DropTarget dropTarget = null;
    private JPopupMenu titleBarMenu;
    private JPopupMenu headerPopup;
    //private TWMenuItem   fontMenu;
    private TWMenuItem hideShowTitleMenu;
    //    private TWMenuItem hideColimn;
//    private TWMenuItem columnSettings;
    //    private TWMenuItem mnuCustomize;
    private int preferredLayer = GUISettings.DEFAULT_LAYER;
    private boolean sortingInProgress = false;
    private boolean detached;
    //private boolean     telerateType = false;
    //private int m_maxNumPage = 1;

    // Variables used for Printing
    /*private int m_maxNumPage = 1;
    private int maximumWidths[];
    private int colXPositions[];
    private int maxWidth = 110;*/

    //private InternalFrame timeAndSales;
    private Object detachedFrame;
    private long lastUpdatedTime = -1;
    private long newUpdatedTime = 0;
    //private FilterableTradeStore tradeStore;
    //    private boolean autoScrollableTable = true;
    private int desktopItemType = BOARD_TYPE;
    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
    private TWDateFormat g_oDateTimeFormat = new TWDateFormat("HH:mm:ss");
    private TWDateFormat g_oDateFormat = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));
    private int scrollCounter = 0;
    // Variables used for Auto Scrolling
    private int currentHighlightedRow = 0;
    private int viewportBeginRowindex = 0;
    private int viewportEndRowindex = 0;
    private int visibleColumnindex = 0;
    private int noOfRowsInViewport = 0;
    private String currency = null;
    private String typeDescription = null;
    private String TDWLDescription = null;
    private String title = null;
    private boolean headerPopupActive = true;
    private boolean symbolsRegistered = false;
    private ClientTable self;
    private boolean printable;
    private boolean columnResizeable;
    private boolean isLinkGroupsEnabled = false;
    private Point mousePoint;
    private boolean active = true;
    private String marketCode;
    private TWMenuItem editformula;
    private TWMenuItem deleteformula;
    private boolean isLinkControlEnabled = false;
    private JPopupMenu linkGroupMenu;
    private String linkedGroupID = LinkStore.LINK_NONE;

    /**
     * Constructor
     */
    public ClientTable() {
        this.addInternalFrameListener(this);
        headerPopup = new JPopupMenu();
        hideShowTitleMenu = new TWMenuItem();
        headerPopup.add(hideShowTitleMenu);
        hideShowTitleMenu.setActionCommand("HEADER");
        hideShowTitleMenu.addActionListener(this);

        TWMenuItem hideColimn = new TWMenuItem(Language.getString("HIDE_COLUMN"), "hidecolumn.gif");
        headerPopup.add(hideColimn);
        hideColimn.setActionCommand("HIDE_COL");
        hideColimn.addActionListener(this);

        editformula = new TWMenuItem(Language.getString("EDIT_FORMULA"), "editformula.gif");
        headerPopup.add(editformula);
        editformula.setActionCommand("EDIT_FOR");
        editformula.addActionListener(this);

        deleteformula = new TWMenuItem(Language.getString("DELETE_FORMULA"), "deleteformula.gif");
        headerPopup.add(deleteformula);
        deleteformula.setActionCommand("DELETE_FOR");
        deleteformula.addActionListener(this);

        editformula.setVisible(false);
        deleteformula.setVisible(false);
        editformula.setEnabled(false);
        deleteformula.setEnabled(false);

        TWMenuItem columnSettings = new TWMenuItem(Language.getString("COLUMN_SETTINGS"), "colsetting.gif");
        headerPopup.add(columnSettings);
        columnSettings.setActionCommand("COL_SETTINGS");
        columnSettings.addActionListener(this);
        Application.getInstance().addApplicationListener(this);

        TWMenuItem mnuCustomize = new TWMenuItem(Language.getString("CUSTOMIZE"), "customize.gif");
        headerPopup.add(mnuCustomize);
        mnuCustomize.setActionCommand("CUSTOMIZE");
        mnuCustomize.addActionListener(this);
        Application.getInstance().addApplicationListener(this);

        GUISettings.applyOrientation(headerPopup);
        //change start
        CustomFormulaStore.getSharedInstance().addCustomFormulaListener(this);
        createTitlebarManu();
        createLinkGroupMenu();
        setHideShowMenuCaption();
//        addPropertyChangeListener(/*"tableColumnClickedEvent"*,*/ this);

        self = this;
    }

    public void setRootPane(JRootPane root) {
        super.setRootPane(root);
    }

    protected final JRootPane createRootPane() {
        return new DetachableRootPane();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
        viewSettings.setCurrency(currency);
        this.repaint();
    }

    public String getMarketCode() {
        return marketCode;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public boolean isPrintable() {
        return printable;
    }

    public void setPrintable(boolean printable) {
        this.printable = printable;
    }

    public boolean isColumnResizeable() {
        return columnResizeable;
    }

    public void setColumnResizeable(boolean columnResizeable) {
        this.columnResizeable = columnResizeable;
    }

    public boolean isDetachable() {
        return true;
    }

    public Object getDetachedFrame() {
        return detachedFrame;
    }

    public void setDetachedFrame(Object detachedFrame) {
        this.detachedFrame = detachedFrame;
    }

    public boolean isDetached() {
        return detached;
    }

    public void setDetached(boolean detached) {
        this.detached = detached;
    }

    public void adjustHideShowTitleMenu() {
        setHideShowMenuCaption();
    }

    public void windowClosing() {
//        unregisterSymbols();
    }

    private void unregisterSymbols() {

        new Thread() {
            public void run() {
                try {
                    String[] symbols = getSymbols().getSymbols();
                    for (int i = 0; i < symbols.length; i++) {
                        DataStore.getSharedInstance().removeSymbolRequest(symbols[i]);
                        symbols = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }.start();

    }

    private void registerSymbols() {
        String[] symbols = getSymbols().getSymbols();
        //String[] symbols =DataStore.getSharedInstance().addBulkValidationrequests(getSymbols().getSymbols());
        try {
            for (int i = 0; i < symbols.length; i++) {
                try {
                    if (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(symbols[i])) != null) {
                        DataStore.getSharedInstance().addSymbolRequest(SharedMethods.getExchangeFromKey(symbols[i]),
                                SharedMethods.getSymbolFromKey(symbols[i]),
                                SharedMethods.getInstrumentTypeFromKey(symbols[i]), true, this, -1, false, symbols);
                        Stock st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(symbols[i]), SharedMethods.getSymbolFromKey(symbols[i]), SharedMethods.getInstrumentTypeFromKey(symbols[i]));
                        if (st != null) {
                            if (!ExchangeStore.getSharedInstance().getExchange(st.getExchange()).isDefault()) {
                                st.setSymbolEnabled(true);
                            } else {
                                if (OptimizedTreeRenderer.selectedSymbols.containsKey(st.getKey())) {
                                    st.setSymbolEnabled(true);
                                } else if (ExchangeStore.getSharedInstance().getExchange(st.getExchange()).isExchageMode()) {
                                    OptimizedTreeRenderer.selectedSymbols.put(st.getKey(), true);
                                    st.setSymbolEnabled(true);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        symbols = null;
    }

    public boolean isTitleVisible() {
        if (viewSettings != null) {
            return viewSettings.isHeaderVisible();
        } else {
            return true;
        }
    }

    public void setTitleVisible(boolean setVisible) {
        if (setVisible) {
            if (viewSettings != null) {
                viewSettings.setHeaderVisible(true);
            }
            updateUI();
        } else {
            Component titleBar = ((BasicInternalFrameUI) getUI()).getNorthPane();
            if (titleBar != null) {
                //System.out.println("hiding");
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                revalidate();
                if (viewSettings != null) {
                    viewSettings.setHeaderVisible(false);
                }
            }
        }
    }

    private void setHideShowMenuCaption() {
        if (!isTitleVisible()) {
            hideShowTitleMenu.setText(Language.getString("SHOW_TITLEBAR"));
            hideShowTitleMenu.setIconFile("showtitle.gif");
        } else {
            hideShowTitleMenu.setText(Language.getString("HIDE_TITLEBAR"));
            hideShowTitleMenu.setIconFile("hidetitle.gif");
        }
    }


    private void toggleTitleBar() {
        setTitleVisible(!isTitleVisible());
        viewSettings.setHeaderVisible(isTitleVisible());
        setHideShowMenuCaption();
    }

    private void hideColumn() {
        TableColumnModel columnModel = table.getColumnModel();
        int viewportWidth = g_oJScrollPane.getViewport().getWidth();
        int tableWidth = table.getWidth();

        int viewColumn;
        if (!g_bDirLTR) { // this checking is necessary due to a bug in java 1.4
            if (tableWidth > viewportWidth) // take the maximum value
                viewColumn = columnModel.getColumnIndexAtX(tableWidth - (int) mousePoint.getX());
            else
                viewColumn = columnModel.getColumnIndexAtX(viewportWidth - (int) mousePoint.getX());
        } else {
            viewColumn = columnModel.getColumnIndexAtX((int) mousePoint.getX());
        }

        int columnIndex = table.convertColumnIndexToModel(viewColumn);
        TableColumn column = table.getColumn("" + columnIndex);

        if (getColumnCount() == 1) {
            new ShowMessage(false, Language.getString("MSG_LIST_CANNOT_EMPTY"), "E");
            return;
        }

        column.setMaxWidth(0);
        column.setMinWidth(0);
        column.setPreferredWidth(0);
        column.setWidth(0);
        column.setResizable(false);
        column = null;

//        long columnSettings = viewSettings.getColumns();
//        long columnFilter = 1 << columnIndex;
//        columnFilter ^= 0xFFFFFFFFFFFFFFFFL;
//        columnSettings &= columnFilter;
//        viewSettings.setColumns(columnSettings);

        BigInteger columnSettings = viewSettings.getColumns();
        BigInteger columnFilter = BigInteger.valueOf(1).shiftLeft(columnIndex);
        columnFilter = columnFilter.xor(Settings.MAX_BIGINT_VALUE);
        columnSettings = columnSettings.and(columnFilter);
        viewSettings.setColumns(columnSettings);

        table.repaint();
    }

    private int getColumnCount() {
        BigInteger lSelectedPos = (viewSettings.getColumns().or(viewSettings.getFixedColumns())).and(viewSettings.getHiddenColumns().not());
        BigInteger lFixedPos = viewSettings.getFixedColumns();
        BigInteger lHiddenPos = viewSettings.getHiddenColumns();
        Vector g_oSelected = new Vector();


        String[] asColumns = viewSettings.getColumnHeadings();

        for (int i = 0; i < asColumns.length; i++) {
            if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
            {
                if (!((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) {
                    if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) { // not if fixed add to list
                        g_oSelected.add(asColumns[i].trim());
                    }
                }
            }
            lSelectedPos = lSelectedPos.shiftRight(1);
            lFixedPos = lFixedPos.shiftRight(1);
            lHiddenPos = lHiddenPos.shiftRight(1);

        }
        return g_oSelected.size();
    }

    public void applySettings() {
        this.setTitle(viewSettings.getCaption());

        if (table != null) {
            if (viewSettings.getFont() != null) {
                try {
                    table.setFont(viewSettings.getFont());
                    //System.out.println(g_oViewSettings.getID());
                    FontMetrics oMetrices = table.getGraphics().getFontMetrics();
                    table.setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + table.getBodyGap());
                } catch (Exception e) {
                }
            }
            if (isSortable()) {
                if (viewSettings.getHeaderFont() != null) {
                    Font oFont = viewSettings.getHeaderFont();
                    SortButtonRenderer oRend = (SortButtonRenderer) table.getColumnModel().getColumn(0).getHeaderRenderer();
                    oRend.setHeaderFont(oFont);
                    SharedMethods.updateComponent(table.getTableHeader());
                    SharedMethods.updateComponent(table);
                } else {
                    Font oFont = Theme.getDefaultFont(); //Theme.DEFAULT_FONT;
                    SortButtonRenderer oRend = (SortButtonRenderer) table.getColumnModel().getColumn(0).getHeaderRenderer();
                    oRend.setHeaderFont(oFont);
                    SharedMethods.updateComponent(table.getTableHeader());
                    SharedMethods.updateComponent(table);
                }
            } else {
                if (viewSettings.getHeaderFont() != null) {
                    table.getTableHeader().setFont(viewSettings.getHeaderFont());
                    SharedMethods.updateComponent(table);
                } else {
                    table.getTableHeader().setFont(Theme.getDefaultFont()); //Theme.DEFAULT_FONT);
                    SharedMethods.updateComponent(table);
                }
            }
        }
        if (viewSettings.isVisible()) {
            try {
                if (Settings.isPutAllToSameLayer()) {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, viewSettings.getIndex());
                } else {
                    this.getDesktopPane().setLayer(this, getPreferredLayer(), viewSettings.getIndex());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
//            this.setVisible(true);
            this.showWindow();
            try {
                if (viewSettings.getIndex() == 0) {
                    this.getDesktopPane().setRequestFocusEnabled(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (Settings.isPutAllToSameLayer()) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
            } else {
                this.getDesktopPane().setLayer(this, getPreferredLayer(), 0);
            }
        }

        if (viewSettings.getSize() != null) {
            this.setSize(viewSettings.getSize());
        }

        try {
            switch (viewSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    this.setIcon(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (viewSettings.getLocation() != null)
            this.setLocation(viewSettings.getLocation());
        if (viewSettings.isVisible()) {
//            this.setVisible(true);
            this.showWindow();
        } else {
//            this.setVisible(false);
            this.hideWindow();
        }

        int sortCol = viewSettings.getSortColumn();

        if (isSortable() && (sortCol >= 0)) {
            g_bSortOrder = viewSettings.getSortOrder() == SortButtonRenderer.DOWN;

            SortButtonRenderer oRend = (SortButtonRenderer) table.getColumnModel().getColumn(sortCol).getHeaderRenderer();
            oRend.setSelectedColumn(sortCol, viewSettings.getSortOrder());
            oRend = null;
        }
        if (sortCol >= 0) {
            symbols.sortSymbols(viewSettings.getSortColumn(), g_bSortOrder);
        } else {
            symbols.sortSymbols(-1, g_bSortOrder);
        }
        SharedMethods.updateComponent(table.getTableHeader());

        if (!viewSettings.isHeaderVisible()) {
            ((BasicInternalFrameUI) getUI()).setNorthPane(null);
            this.revalidate();
        }

        if ((viewSettings.getCurrency() == null) || (viewSettings.getCurrency().toUpperCase().equals("NULL"))) {
            setCurrency(null);
        } else {
            setCurrency(viewSettings.getCurrency());
        }

        if (symbols != null) {
            if (viewSettings.getFilter() != null) {
                if (viewSettings.getFilterType() == Symbols.FILTER_BY_CURRENCY)
                    setFilterCaption(SymbolMaster.getCurrencyCaption(viewSettings.getFilter()));
                else if (viewSettings.getFilterType() == Symbols.FILTER_BY_MARKET)
                    setFilterCaption(SymbolMaster.getAssetClassCaption(viewSettings.getFilter()));
            }
            updateGUI();
        } else {

            //System.out.println("applying filter - " + g_oViewSettings.getCaption() + " -" + g_oViewSettings.getFilter() + g_oViewSettings.getFilterType());
        }
        setHideShowMenuCaption();
        setDecimalPlaces();
//        updateUI();
    }

    public void printTable() {

    }

    public void printContents() {
        if (isFunctionType()) {
            SharedMethods.printTable(getTable(), PrintManager.TABLE_TYPE_DEFAULT, getTitle());
        } else if (isCustomType())
            SharedMethods.printTable(getTable(), PrintManager.TABLE_TYPE_WATCHLIST, getTitle());
        else
            SharedMethods.printTable(getTable(), PrintManager.TABLE_TYPE_DEFAULT, getTitle());
    }

    public void showServices() {
    }

    public void showLinkGroup() {
        if (isLinkControlEnabled) {

            SwingUtilities.updateComponentTreeUI(linkGroupMenu);
            Point point = MouseInfo.getPointerInfo().getLocation();
            SwingUtilities.convertPointFromScreen(point, this);
            GUISettings.showPopup(linkGroupMenu, this, (int) point.getX(), (int) point.getY());
        }
    }

    public String getLinkGroupIDForTitleBar() {
        return linkedGroupID;
    }

    public boolean isServicesEnabled() {
        return false;
    }

    public boolean isLinkGroupEnabled() {
        return isLinkControlEnabled;
    }


    public int getPreferredLayer() {
        return preferredLayer;
    }

    public void setPreferredLayer(int preferredLayer) {
        this.preferredLayer = preferredLayer;
    }

    private void createTitlebarManu() {
        titleBarMenu = new JPopupMenu();
        TWMenuItem hideTitlemenu = new TWMenuItem(Language.getString("HIDE_TITLEBAR"), "hidetitle.gif");
        hideTitlemenu.setActionCommand("HIDE");
        hideTitlemenu.addActionListener(this);
        titleBarMenu.add(hideTitlemenu);
        GUISettings.applyOrientation(titleBarMenu);
        addTitlebarManu();
    }

    public void revalidateHeader() {
        if (viewSettings != null) {
            if (!viewSettings.isHeaderVisible()) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                this.revalidate();
            }
        }
    }

    public void addTitlebarManu() {
        Component np = ((BasicInternalFrameUI) getUI()).getNorthPane();
        try {
            np.removeMouseListener(this); // if existing
        } catch (Exception e) {
        }
        np.addMouseListener(this);
    }

    //"Calling ClientTable.setVisible(<boolean>) is not allowed\nUse showWindow() method - Uditha";

    public void setVisible(boolean aFlag) {
        setVisible(aFlag, false);
    }

    public void setVisible(boolean status, boolean isDetaching) {
        if (!isDetaching) {
            if (status) {
                showWindow();
            } else {
                hideWindow();
            }
        }
        super.setVisible(status);
        if (!TWDesktop.isInTabbedPanel(this)) {
            if (status && (isCustomType() || isMistType() || isFunctionType())) {
                TWDesktop.addWindowTab(this);
            }
        }
        if (TWDesktop.isInTabbedPanel(this)) {
            if (!status) {
                TWDesktop.removeWindowtab(this);
            }
        }

        if (!TWDesktop.checkInternalFrameAvailability(this)) {
            if (status) {
                TWDesktop.removeWindowtab(this);
            }
        }
        if (TWDesktop.checkInternalFrameAvailability(this) && !TWDesktop.isInTabbedPanel(this)) {
            if (status && (isCustomType() || isMistType() || isFunctionType())) {
                TWDesktop.addWindowTab(this);
            }
        }
    }

    public synchronized void setSelected(boolean selected) throws PropertyVetoException {
        super.setSelected(selected);    //To change body of overridden methods use File | Settings | File Templates.
        if (selected) {
            TWDesktop.setTabForSelectedWindow(this);
            Settings.addToFocusFlowList(this);
        }
    }

    public synchronized void showWindow() {
        try {
            ensureVisibility();
            if (!isVisible()) {
                registerSymbols();
            }
            super.setVisible(true);
            if (!TWDesktop.isInTabbedPanel(this)) {
                if ((isCustomType() || isMistType() || isFunctionType())) {
                    TWDesktop.addWindowTab(this);
                }
            }
            if (!TWDesktop.checkInternalFrameAvailability(this)) {
                TWDesktop.removeWindowtab(this);
            }
            if (TWDesktop.checkInternalFrameAvailability(this) && !TWDesktop.isInTabbedPanel(this)) {
                if ((isCustomType() || isMistType() || isFunctionType())) {
                    TWDesktop.addWindowTab(this);
                }
            }
//            SharedMethods.printLine(getLocation().toString());
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public synchronized void hideWindow() {
        try {

            if (isVisible()) {
//                   if (!isDetached()) {
                unregisterSymbols();
//                   }
            }
            super.setVisible(false);
            Settings.removeFormFocusFlowList(this);
            TWDesktop.setTabForSelectedWindow(Settings.getFormFocusFlowList(Settings.getFocusFlowList().size() - 1));
            if (TWDesktop.isInTabbedPanel(this)) {
                TWDesktop.removeWindowtab(this);
            }

//            SharedMethods.printLine(getLocation().toString());
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }


    public void show() {
        super.show();
        revalidateHeader();
    }

    public void updateUI() {
        super.updateUI();
        addTitlebarManu();
        revalidateHeader();
        setBorder(GUISettings.getTWMainBoardFrameBorder());
    }

    /**
     * Sets the filterd view caption
     */
    public void setFilterCaption(String sCaption) {
        if (sCaption == null) {
            this.setTitle(viewSettings.getCaption());
        } else {
            g_sFilterCaption = " (" + sCaption + ") ";
            this.setTitle(viewSettings.getCaption() + g_sFilterCaption);
        }
    }

    /**
     * Returns the view settings object of this table
     */
    public ViewSetting getViewSettings() {
        return viewSettings;
    }

    /**
     * Sets the view settings object for this table
     */
    public void setViewSettings(ViewSetting oViewSettings) {
        viewSettings = oViewSettings;
        viewSettings.setParent(this);
        try {
            this.viewSettings.setLayer(this.getPreferredLayer());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * Creates the JTable object and a JInternal frame.
     * The JTable is added to the JInternalFrmae and the
     * internal frame is returned.
     */
    public void createTable(boolean sortable) {
        //g_oStock = g_oDataStore.getStockObject("EMOB");
        dropTarget = new DropTarget(this, this);
        setSortable(sortable);
        setPrintable(true);
        setColumnResizeable(true);
        /*if (isTelerateType()){
            System.out.println(getTitle() + "-->> create table");
        }*/
        g_asHeadings = viewSettings.getColumnHeadings();// names;
        g_aiRendIDs = new int[g_asHeadings.length];
        g_sNSSymbol = Language.getString("NA");
        g_aiColWidths = TWColumnSettings.getColWidths();

        for (int i = 0; i < g_asHeadings.length; i++) {
            try {
                g_aiRendIDs[i] = viewSettings.getRendererID(i);
            } catch (Exception e) {
                g_aiRendIDs[i] = 0;
            }
        }

        if (Language.isLTR())
            g_bDirLTR = true;
        else
            g_bDirLTR = false;

        // Create a table model of the data        .
        oDataModel = new AbstractTableModel() {
            public int getColumnCount() {
                //change start
                return g_asHeadings.length + CustomFormulaStore.getSharedInstance().getColumnCount();
            }

            public int getRowCount() {
                try {
                    if (viewSettings.isCutomType() && (!isDetached())) {
                        return (symbols.getFilteredSymbols().length) + 1;
                    } else {
                        return symbols.getFilteredSymbols().length;
                    }
                } catch (Exception w) {
                    return 0;
                }
            }

            public Object getValueAt(int row, int col) {
                try {
                    //System.out.println(getTitle() +  "--> " + (isTelerateType()));
//                    if (!isVisible())
//                        return "0";
                    if (col == -20) {
                        return symbols.getFilteredSymbols()[row];
                    }
                    if (row == symbols.getFilteredSymbols().length) {
                        return "";
                    }
                    //change start
                    g_oStock = DataStore.getSharedInstance().getStockObject(symbols.getFilteredSymbols()[row]);

//                    if ((g_oStock == null)&&(col==0)){
//                        sto=new StringTransferObject();
//                        sto.setValue(symbols.getFilteredSymbols()[row]);
//                        return sto;
//                    }

                    if (col > g_asHeadings.length - 1) {
                        int column = col - g_asHeadings.length;
                        return CustomFormulaStore.getSharedInstance().getColumnValue(g_oStock, column);
                    } else {
                        return StockData.getObjectData(g_oStock, getCurrency(), col);
                    }
                    //change end
                } catch (Exception e) {
                    //e.printStackTrace();
                    return "";
                }
            }

            public String getColumnName(int iCol) {
                //change start
                if (iCol > g_asHeadings.length - 1) {
                    int column = iCol - g_asHeadings.length;
                    return CustomFormulaStore.getSharedInstance().getColumnName(column);
                } else {
                    return g_asHeadings[iCol];
                }
                //change end
            }

            public Class getColumnClass(int c) {
                /*switch(c){
                    case 81:
                        return CompanyCodeSorter.class;

                }*/
                switch (viewSettings.getRendererID(c)) {
                    case 0:
                    case 1:
                    case 2:
                        //case 'B':
                        return String.class;
                    case 'P':
                    case 'Q':
                        return Number[].class;
                    case 3:
                    case 5:
                    case 6:
                    case 4:
                    case 7:
                    case 8:
                    case 'M':
                    case 'S':
                    case 'B':
                        return Number.class;
                    default:
                        return Object.class;
                }
            }

            public boolean isCellEditable(int row, int col) {
                if ((isDetached()) || (!viewSettings.isCutomType())) return false;
                if (table.convertColumnIndexToView(col) == 0) {
                    try {
                        if (row == table.getRowCount() - 1) {
                            return true;
                        } else {
                            return false;
                        }
                    } catch (Exception e) {
                        System.out.println("in client table is cell editable");
                        return false;
                    }
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                String exchange;
                String symbol;
                String key = null;
                int instrument = -1;
                String input = ((String) aValue);

                if (input.equals("")) return;

                //int dotindex = symbol.lastIndexOf(".");

                exchange = SharedMethods.getExchangeFromKey(input); // break the exchange from key
                symbol = SharedMethods.getSymbolFromKey(input);     // break the symbol from key (if not the full key, this becomes null)
                instrument = SharedMethods.getInstrumentTypeFromKey(input); // break the symbol from key (if not the full key, this becomes null)

                if ((ExchangeStore.isNoneDefaultExchangesAvailable() && ((symbol == null) || (instrument < 0)))) {
                    symbol = exchange;
                    TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
//                    String requestID = "MainBoard:" + System.currentTimeMillis();
                    String requestID = DataStore.getSharedInstance().getSequenceID();
                    SendQFactory.addValidateRequest(symbol.toUpperCase(), requestID, null, 0);
                    SymbolsRegistry.getSharedInstance().rememberRequest(requestID, self);
                } else {
                    exchange = SharedMethods.getExchangeFromKey(input); // break the exchange from key
                    symbol = SharedMethods.getSymbolFromKey(input); // remove the symbol part from key
                    instrument = SharedMethods.getInstrumentTypeFromKey(input);

                    if ((symbol == null) || (instrument < 0)) {
                        // may be the symbol only was typed. no exchange. hence symbol value is in exchange var
                        symbol = exchange;
                        //exchange = SymbolMaster.getExchangeForSymbol(symbol, true); // find the exchange for this symbol //Bug ID <#0040> does not work for KSE (8.20.001)
//                        key = SymbolMaster.getExchangeForSymbol(symbol, false); // find the key for this symbol
                        key = DataStore.getSharedInstance().findKeyFromDataStore(symbol);
                        exchange = SharedMethods.getExchangeFromKey(key); // break the exchange from key
                        symbol = SharedMethods.getSymbolFromKey(key);
                        instrument = SharedMethods.getInstrumentTypeFromKey(key);
                    }

                    if (exchange == null) { // bad symbol, not in local files. Must vslidate from the server
                        TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
                        if (ValidatedCompanySelector.CANCELPRESSED == 0)
                            new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");

                    } else {
                        key = SharedMethods.getKey(exchange, symbol, instrument);
//                        exchange = SharedMethods.getExchangeFromKey(key); // break the exchange from key
//                        symbol = SharedMethods.getSymbolFromKey(key); // remove the exchange part from key
                        if ((!viewSettings.getID().equals("8"))) {
                            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrument);
                        }
                        //int instrumentType = SymbolMaster.getInstrumentType(symbol, exchange);

                        //key = exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol;
                        if (symbols.appendSymbol(key)) {
                            fireTableCellUpdated(rowIndex, columnIndex);
                            TradeFeeder.refreshSymbolIndex();
                        } else {
                            int location = symbols.getLocation(symbol);
                            if (location >= 0) {
                                table.setRowSelectionInterval(location, location);
                                table.setColumnSelectionInterval(0, table.getColumnCount() - 1);
                            }
                        }
                        TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
                        updateUI();
                    }
                }
            }
        };

        table = new DraggableTable(this, oDataModel);
        table.getTableHeader().addPropertyChangeListener("tableColumnClickedEvent", this);

        /*{
            public void valueChanged(ListSelectionEvent e) {
                super.valueChanged(e);
                checkSelection(true);
            }
        };

        table = new DraggableTable(this, oDataModel){
            public void valueChanged(ListSelectionEvent e) {
                super.valueChanged(e);
                checkSelection(true);
            }
        };*/
//        table.setColumnModel(new TestColModel());

        g_oJScrollPane = new JScrollPane(table);
        g_oJScrollPane.getHorizontalScrollBar().setRequestFocusEnabled(false);
        g_oJScrollPane.getVerticalScrollBar().setRequestFocusEnabled(false);
        g_oJScrollPane.setRequestFocusEnabled(false);
        g_oJScrollPane.setFocusable(false);

        /*g_oJScrollPane = new JScrollPane(table);
        JViewport viewport = new JViewport();
        viewport.setView(fixedTable);
        viewport.setPreferredSize(fixedTable.getPreferredSize());
        g_oJScrollPane.setRowHeaderView(viewport);
        g_oJScrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, fixedTable
                .getTableHeader());*/

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        this.getContentPane().setLayout(new BorderLayout());

        this.getContentPane().add(g_oJScrollPane, BorderLayout.CENTER);

//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));

        int[][] columnSettings = viewSettings.getColumnSettings();
        int count = CustomFormulaStore.getSharedInstance().getColumnCount();


        String[] cols = viewSettings.getColumnHeadings();
        String[] newCols = new String[(cols.length + count)];
        for (int i = 0; i < cols.length; i++) {
            newCols[i] = cols[i];
        }
        for (int k = 0; k < count; k++) {
            int no = cols.length + k;
            newCols[no] = CustomFormulaStore.getSharedInstance().getColumnName(k);
        }

        viewSettings.setColumnHeadings(newCols);

        if (columnSettings != null) {
            int[][] newColSettings = new int[(columnSettings.length + count)][3];
            for (int i = 0; i < columnSettings.length; i++) {
                for (int j = 0; j < 3; j++) {
                    newColSettings[i][j] = columnSettings[i][j];
                }
            }
            for (int k = 0; k < count; k++) {
                int no = columnSettings.length + k;
                newColSettings[no][0] = no;
                newColSettings[no][1] = 0;
                newColSettings[no][2] = no;
            }
            viewSettings.setColumnSettings(newColSettings);
        }

        this.getTable().createDefaultColumnsFromModel();
        setColumnIdentifiers();

        addMouseListenerToHeaderInTable(table);

        renderer = new SortButtonRenderer(true);
        renderer.setShowToolTip();
        TableColumnModel colmodel = table.getColumnModel();
        int n = viewSettings.getColumnHeadings().length;
        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }

        /*if (isSortable()) {
            renderer = new SortButtonRenderer(true);
            renderer.setShowToolTip();
            TableColumnModel colmodel = table.getColumnModel();
            int n = g_asHeadings.length;

            for (int i = 0; i < n; i++) {
                try {
                    colmodel.getColumn(i).setHeaderRenderer(renderer);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }*/

//        setColumnIdentifiers();
//        adjustColumns();
        this.setRequestFocusEnabled(true);
        table.setRowSelectionAllowed(true);

        table.setCellSelectionEnabled(true);
//        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);       //------------------- Commented by Shanika
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);


        table.setShowVerticalLines(true);
        table.setShowHorizontalLines(true);
        table.setIntercellSpacing(new Dimension(1, 1));

        table.setDefaultRenderer(Object.class, new TableRenderer(g_aiRendIDs));
        table.setDefaultRenderer(Number.class, new TableRenderer(g_aiRendIDs));
        table.getTableHeader().setUpdateTableInRealTime(false); // should not update file dragging
        table.setDefaultEditor(String.class, new CompanyCellEditor(new JComboBox()));
        table.setDefaultEditor(Number.class, new CompanyCellEditor(new JComboBox()));
        table.setDefaultEditor(Object.class, new CompanyCellEditor(new JComboBox()));

        Font oFont = viewSettings.getFont();

        if (oFont != null) {
            FontMetrics oMetrices = this.getFontMetrics(oFont);
            table.setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + table.getBodyGap());
            oMetrices = null;
            table.setFont(oFont);
            oFont = null;
        }

        table.setCellSelectionEnabled(true);
        table.setRowSelectionAllowed(true);
        table.setColumnSelectionAllowed(false);

//        table.updateUI();
        table.addMouseListener(this);
        table.addMouseMotionListener(this);
        table.addKeyListener(this);

        TableUpdateManager.addTable(this);
//        g_oThread = new TWThread(this, viewSettings);
//        g_oThread.start();

        Theme.registerComponent(this);

        if (g_bDirLTR) {
            GUISettings.applyOrientation(this, ComponentOrientation.LEFT_TO_RIGHT);
            GUISettings.applyOrientation(table, ComponentOrientation.LEFT_TO_RIGHT);
        } else {
            GUISettings.applyOrientation(this, ComponentOrientation.RIGHT_TO_LEFT);
            GUISettings.applyOrientation(table, ComponentOrientation.RIGHT_TO_LEFT);
            GUISettings.applyOrientation(table.getTableHeader(), ComponentOrientation.RIGHT_TO_LEFT);
        }
        initMouseGestures();

    }// <--- createTable

    private void checkSelection(boolean isFixedTable) {
        int fixedSelectedIndex = fixedTable.getSelectedRow();
        int selectedIndex = table.getSelectedRow();
        if (fixedSelectedIndex != selectedIndex) {
            if (isFixedTable) {
                table.setRowSelectionInterval(fixedSelectedIndex,
                        fixedSelectedIndex);
            } else {
                fixedTable
                        .setRowSelectionInterval(selectedIndex, selectedIndex);
            }
        }
    }

    private void setColumnIdentifiers() {
        TableColumn oColumn;
        if (CustomFormulaStore.getSharedInstance().getColumnCount() > 0) {
            for (int j = 0; j < CustomFormulaStore.getSharedInstance().getColumnCount(); j++) {
                oColumn = table.getColumn(CustomFormulaStore.getSharedInstance().getColumnName(j));
                int k = j + g_asHeadings.length;
                oColumn.setIdentifier("" + k);
            }
        }

        for (int i = 0; i < g_asHeadings.length; i++) {
            oColumn = table.getColumn(g_asHeadings[i]);
            oColumn.setIdentifier("" + i);
        }
        //change start

        //change end
    }

    public void setNorthPanel(JComponent component) {
        this.getContentPane().add(component, BorderLayout.NORTH);
    }

    public void showInvalidDropMessage() {
        new ShowMessage(Language.getString("MSG_CANNOT_ADD_SYMBOL"), "E");
    }

    /**
     * Added By Bandula
     */
    public JScrollPane getScrollPane() {
        return g_oJScrollPane;
    }

    /**
     * returns the data modal of the table
     */
    public TableModel getModel() {
        return oDataModel;
    }

    /**
     * returns the data modal of the table
     * <p/>
     * public TableSorter getDataIndex()
     * {
     * return g_oSorter;
     * }
     */

    public boolean isCellEditMode() {
        return table.isEditing();
    }

    public void upDateColumnHeadings() {
        g_asHeadings = viewSettings.getColumnHeadings();
    }

    public String[] getColumnHeadings() {
        return g_asHeadings;
    }

    public void adjustColumns() {
        adjustColumns(table, (viewSettings.getColumns().or(viewSettings.getFixedColumns())).and((viewSettings.getHiddenColumns().not())));
//        StringBuilder b = new StringBuilder();
//        for(int i=0;i<table.getColumnCount();i++){
//            if(table.getColumn(""+i).getWidth()>0){
//                b.append(table.getColumn(""+i).getHeaderValue());
//                b.append(" | ");
//            }
//        }
//        System.out.println("**********"+b.toString()+"****************");

//        adjustColumns(table,
//                (viewSettings.getColumns() | viewSettings.getFixedColumns()) & ~viewSettings.getHiddenColumns());
    }

    /**
     * Adjusts column widths depending on the column visibility
     * if a column is to be hidden, the width is set to 0.
     */
    public int adjustColumns(JTable oTable, BigInteger lSettings)//, boolean bFixedCols)
//    public int adjustColumns(JTable oTable, long lSettings)//, boolean bFixedCols)
    {
        BigInteger lPos = lSettings;
//        long lPos = lSettings;
        TableColumn oColumn;
        int iTotalWidth = 0;
        // Added By Bandula
        String[] asHeadings = viewSettings.getColumnHeadings();
        //String[]	colWidths	= g_oViewSettings.getColumnWidths();
        int iColCount;

        iColCount = asHeadings.length;

        // Added By Bandula
        for (int i = 0; i < iColCount; i++) {
            oColumn = oTable.getColumn("" + i);
            if ((lPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
                oColumn.setMaxWidth(0);
                oColumn.setMinWidth(0);
                oColumn.setPreferredWidth(0);
                oColumn.setWidth(0);
                oColumn.setResizable(false);
            } else {
                oColumn.setMaxWidth(1500);
                oColumn.setMinWidth(0);
                oColumn.setResizable(true);
                if (viewSettings.getColumnSettings() != null) {
                    if (i >= viewSettings.getColumnSettings().length) {
                        oColumn.setPreferredWidth(100);
                    } else {
                        if (viewSettings.getColumnSettings()[i][1] != 0) {
                            oColumn.setPreferredWidth(viewSettings.getColumnSettings()[i][1]);
                        } else {
                            setDefaultColWidth(oColumn, i);
                        }
                    }
                    /*TableColumnModel oModel = oTable.getColumnModel();
                    int iViewIndex = oTable.convertColumnIndexToView(oColumn.getModelIndex());
                    try {
                        oModel.moveColumn(iViewIndex, viewSettings.getColumnSettings()[i][2]);
                    } catch (Exception e) {
                        // may fail if column settings for the table is incomplete. Ignore the error
                    }*/
                } else {
                    setDefaultColWidth(oColumn, i);
                }
                iTotalWidth += 100;//oColumn.getWidth();
            }
            lPos = lPos.shiftRight(1);
        }
        if (viewSettings.getColumnSettings() != null) {
            moveColumns(iColCount);
        }
        return iTotalWidth;
    }

    public void moveColumns(int length) {
        try {
            String[] asHeadings = viewSettings.getColumnHeadings();
            //String[]	colWidths	= g_oViewSettings.getColumnWidths();
            int iColCount;
//        BigInteger lPos = (viewSettings.getColumns().or(viewSettings.getFixedColumns())).and((viewSettings.getHiddenColumns().not()));
            TableColumn oColumn;
//        int iTotalWidth = 0;

//        iColCount = asHeadings.length;
            TableColumnModel oModel = table.getColumnModel();
            int[] sorted = new int[length];

//        System.out.println("ViewSetting");
            for (int i = 0; i < asHeadings.length; i++) {
                //            System.out.print(viewSettings.getColumnSettings()[i][0] + "," + viewSettings.getColumnSettings()[i][2] + ",");
                sorted[viewSettings.getColumnSettings()[i][2]] = i;
            }
//        System.out.println("\n");

//        System.out.println("Sorted ");
//        for (int i = 0; i < asHeadings.length; i++) {
//            System.out.print(i + " " + sorted[i] + " , ");
//        }
//        System.out.println("\n");

            for (int i = 0; i < length; i++) {
                //            oColumn = table.getColumn("" + i);
                try {
                    oModel.moveColumn(table.convertColumnIndexToView(sorted[i]), i);
                    //                    System.out.print(sorted[i] + " -> " + i + ", ");
                } catch (Exception e) {
                    // may fail if column settings for the table is incomplete. Ignore the error
                }
                //            try {
                //                Thread.sleep(100);
                //            } catch (InterruptedException e) {
                //                e.printStackTrace();
                //            }
            }

//            System.out.println("View");
//            for (int i = 0; i < asHeadings.length; i++) {
//                System.out.print(i + " " + table.convertColumnIndexToView(i) + " , ");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void repositionColumns() {
        TableColumn oColumn;
        //System.out.println("Repos");
        for (int i = 0; i < viewSettings.getColumnHeadings().length; i++) {
            oColumn = table.getColumn("" + i);

            if (viewSettings.getColumnSettings() != null) {
                repositionColumn(i, table, oColumn);
            } else {
                oColumn.setPreferredWidth(100);
            }
        }
    }

    public void repositionColumn(int iCol, JTable oTable
            , TableColumn oColumn) {
        TableColumnModel oModel = oTable.getColumnModel();
        int iViewIndex = oTable.convertColumnIndexToView(oColumn.getModelIndex());
        if (Language.isLTR())
            oModel.moveColumn(iViewIndex, viewSettings.getColumnSettings()[iCol][2]);
        else
            oModel.moveColumn(iViewIndex, viewSettings.getColumnHeadings().length
                    - viewSettings.getColumnSettings()[iCol][2] - 1);
    }

    public void saveColumnPositions() {
        try {
            if (viewSettings.getColumnSettings() == null) {
                viewSettings.restColumnSettings();
            }
            for (int i = 0; i < viewSettings.getColumnHeadings().length; i++) {
                viewSettings.getColumnSettings()[i][2] =
                        table.convertColumnIndexToView(i);
                TableColumn oColumn = getTable().getColumn("" + i);
                viewSettings.getColumnSettings()[i][1] = oColumn.getWidth();
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    private void setDefaultColWidth(TableColumn oColumn, int iCol) {
        try {
            //change start
//            System.out.println("column ="+iCol);
//            System.out.println("col length ="+g_aiColWidths.length);
            if (iCol < g_aiColWidths.length)
                oColumn.setPreferredWidth(g_aiColWidths[iCol]);
            else
                oColumn.setPreferredWidth(100);
            //change end
        } catch (Exception e) {
            oColumn.setPreferredWidth(100);
        }
    }

    /**
     * Returns the table component
     */
    public JTable getTable() {
        return table;
    }

    /**
     * Returns the position of the column for the
     * given column header
     */
    public int getColumnPsoition(String heading) {
        int position = -1;
        for (int i = 0; i < g_asHeadings.length; i++) {
            if (table.getColumnName(i).equals(heading)) {
                position = i;
                break;
            }
        }
        return position;
    }

    /*private long toLongValue(Object oValue) {
        try {
            return Long.parseLong((String) oValue);
        } catch (Exception e) {
            return 0;
        }
    }

    private double toDoubleValue(Object oValue) {
        try {
            double[] asValue = (double[]) oValue;
            return asValue[0];
        } catch (Exception e) {
            return 0;
        }
    }*/

    /*
     * Added on 19-11-02
     */

    /**
     * Add a mouse listener to the table
     */
    private void addMouseListenerToHeaderInTable(final DraggableTable table) {

        final JTable tableView = table;
        tableView.setColumnSelectionAllowed(false);
        MouseAdapter listMouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    if (isHeaderPopupActive()) {
                        mousePoint = e.getPoint();
                        revalidateColumns();
                        GUISettings.showPopup(headerPopup, tableView.getTableHeader(), e.getX(), e.getY());

                    }
                } else {
                    /*if (isSortable()) {
                        if (!sortingInProgress) {
                            try {
                                sortingInProgress = true;
                                TableColumnModel columnModel = tableView.getColumnModel();
                                int viewportWidth = g_oJScrollPane.getViewport().getWidth();
                                int tableWidth = tableView.getWidth();

                                int viewColumn;
                                if (!g_bDirLTR) { // this checking is necessary due to a bug in java 1.4
                                    if (tableWidth > viewportWidth) // take the maximum value
                                        viewColumn = columnModel.getColumnIndexAtX(tableWidth - e.getX());
                                    else
                                        viewColumn = columnModel.getColumnIndexAtX(viewportWidth - e.getX());
                                } else {
                                    viewColumn = columnModel.getColumnIndexAtX(e.getX());
                                }

                                int column = tableView.convertColumnIndexToModel(viewColumn);

                                *//* Set the sort arrow for the header *//*

                                SortButtonRenderer oRend = (SortButtonRenderer) tableView.getColumnModel().getColumn(column).getHeaderRenderer();
                                int sortOrder = oRend.setSelectedColumn(column);
                                viewSettings.setSortOrder(sortOrder);
                                oRend = null;

                                if (sortOrder == SortButtonRenderer.DOWN)
                                    g_bSortOrder = true;
                                else
                                    g_bSortOrder = false;

                                g_iLastSortedCol = column;
                                symbols.sortSymbols(column, g_bSortOrder);
                                viewSettings.setSortColumn(column);
                                table.repaint();
                                table.getTableHeader().repaint();
                                sortingInProgress = false;
                            } catch (Exception e1) {
                                sortingInProgress = false;
                            }
                        }
                        *//*if (column == g_iLastSortedCol)
                            g_bSortOrder = !g_bSortOrder;
                        else
                            g_bSortOrder	= true;*//*
                        *//*g_iLastSortedCol = column;
                        g_oSymbols.sortSymbols(column,g_bSortOrder);
                        g_oViewSettings.setSortColumn(column);*//*
                    }*/
                }
            }
        };
        JTableHeader th = tableView.getTableHeader();
        th.addMouseListener(listMouseListener);
    }

    public void setAutoResizeOn() {
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    }

    /**
     * Returns the caption to be used by
     * the tree component
     */
    public String toString() {
        return title;
    }

    /*
    * Returns selecetd cols number
    */

    public BigInteger getCols() {
        return viewSettings.getColumns();
    }

    /**
     * Sets column to be displayed on the
     * table. each bit of the int param says
     * whether to enable or dissable the col.
     */
    public void setCols(int iCols) {
        viewSettings.setColumns(BigInteger.valueOf(iCols));
    }

    /**
     * Update the user interface of the table
     */
    public void update() {
        table.updateUI();
    }

    /**
     * Sets the window to the focus
     */
    public void setFocus() {
        this.grabFocus();
    }

    /**
     * Sets symbol list for the table
     */
    public void setSymbols(Symbols oSymbols, boolean addToregister) {
        /* If the symbols object is already registered, remove it.
            This may happen due to a previouse workspace activity */
        if (symbols != null) {// (!isCustomType()))
            SymbolsRegistry.getSharedInstance().unregister(symbols, isCustomType());
        }
        symbols = oSymbols;
        /* register the new symbols object */
//        if ((!isCustomType()) && (addToregister))
        if (addToregister) {
            SymbolsRegistry.getSharedInstance().register(oSymbols, isCustomType());
        }

    }

    /**
     * Returns symbol list for the table
     */
    public Symbols getSymbols() {
        return symbols;
    }

    public void setSymbols(Symbols oSymbols) {
        setSymbols(oSymbols, true);
    }

    /**
     * Checks if the  type is custom for this table
     */
    public boolean isCustomType() {
        return viewSettings.isCutomType();
    }

    public boolean isMistType() {
        return viewSettings.isMISTType();
    }

    /**
     * Checks if the  type is custom for this table
     */
    public boolean isMarketType() {
        return viewSettings.isMarketType();
    }

    public boolean isFunctionType() {
        return viewSettings.isFunctionType();
    }

    public boolean isVolumeWatcherType() throws Exception {
        return viewSettings.isVolumeWatcherType();

    }

    public boolean isCashFlowWatcherType() throws Exception {
        return viewSettings.isCashFlowWatcherType();
    }

    /**
     * Set the path for the table on the tree
     */
    public void setPath(TreePath oTreePath) {
        g_oPathOnTree = oTreePath;
    }

    /**
     * Set areference of the tree
     */
    public void setTree(DefaultMutableTreeNode oTree) {
        g_oTree = new JTree(oTree);
    }

    /**
     * Set the language direction
     */
    public void setLanguageDIR(String sDIR) {
        if (sDIR.equals("0"))
            g_bDirLTR = true;
        else
            g_bDirLTR = false;
    }

    /**
     * Returns the column count
     */
    public int getColCount() {
        return g_asHeadings.length;
    }

    /**
     * Returns the tree path for this view
     */
    public TreePath getTreePath() {
        return g_oTreePath;
    }

    /**
     * Sets the tree path for this view
     */
    public void setTreePath(Object[] oPath) {
        g_oTreePath = new TreePath(oPath);
    }

    /**
     * Update the GUI of the table
     */
    public void updateGUI() {
        this.setTitle(viewSettings.getCaption() + g_sFilterCaption);
//        applyTheme();
    }

    public void applyTheme() {
        table.setBackground(Theme.getColor("BOARD_TABLE_BGCOLOR"));
        table.setGridColor(Theme.getColor("BOARD_TABLE_GRIDCOLOR")); //todo
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
        if (table.isGdidOn()) {
            //table.setGdidOn(true);
        } else {
//            if (Theme.getBoolean("BOARD_TABLE_GRID_ON")){
//                table.setIntercellSpacing(new Dimension(1, 1));
//                table.setGridColor(Theme.getColor("BOARD_TABLE_GRIDCOLOR"));
//            }else {
            table.setIntercellSpacing(new Dimension(0, 0));
//            }
        }
        //change start
        int n = oDataModel.getColumnCount(); // g_asHeadings.length + CustomFormulaStore.getSharedInstance().getIndexCount();
        if (isSortable()) {
            SortButtonRenderer oRend = (SortButtonRenderer) getTable().getColumnModel().getColumn(0).getHeaderRenderer();
            oRend.applyTheme();
//            for (int k = 0; k < n; k++) {
//                SortButtonRenderer oRend = (SortButtonRenderer) table.getColumnModel().getColumn(k).getHeaderRenderer();
//                oRend.applyTheme();
//            }
        } else {
            table.getTableHeader().setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
            table.getTableHeader().setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
            if (table.getTableHeader().getFont() == null)
                table.getTableHeader().setFont(new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.PLAIN, 10));
            SharedMethods.updateComponent(table.getTableHeader());
        }

        try {
            SwingUtilities.updateComponentTreeUI(titleBarMenu);
        } catch (Exception e) {
        }

        try {
            SwingUtilities.updateComponentTreeUI(headerPopup);
        } catch (Exception e) {
        }
    }
    /* public void runThread() {

//        if (true) return;

//        getVisibleColumnIndex();
//        while (active) {
if (((Settings.isConnected() || Settings.isOfflineMode()) && (this.isVisible() || isDetached()) *//*&& (!table.isEditing()) *//* && (!mouseDragging)) ||
                (Settings.isScrollingOn() && this.isVisible() *//*&& (!table.isEditing()) *//* && (!mouseDragging))) {
            newUpdatedTime = UpdateNotifier.getShapshotUpdatedTime(lastUpdatedTime);
            if ((Settings.isScrollingOn()) ||
                    ((!Settings.isScrollingOn()) && (newUpdatedTime > lastUpdatedTime))) {
                lastUpdatedTime = newUpdatedTime;
                try {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
//                                System.out.println("Update " + getTitle());
                            //int[] selectedRows;
                            //int[] selectedCols;
                            if (Settings.isScrollingOn()) {
                                if (scrollCounter >= Settings.autoScrollInterval) {
                                    scrollCounter = 1;
                                    autoScrollingInLinkedWindows();
                                    highlightSelectedRow();
                                } else {
                                    scrollCounter++;
                                }
                            }
                            symbols.sortSymbols();
//                            selectedRows = table.getSelectedRows();
//                            selectedCols = table.getSelectedColumns();
                            table.repaint();
//                            ((AbstractTableModel) table.getModel()).fireTableDataChanged();// repaint();
                           *//* if (selectedRows.length > 0) {
                                table.setRowSelectionInterval(selectedRows[0], selectedRows[selectedRows.length - 1]);
                            }
                            if (selectedCols.length > 0) {
                                table.setColumnSelectionInterval(selectedCols[0], selectedCols[selectedCols.length - 1]);
                            }*//*
//                            selectedRows = null;
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
//            Sleep(1000);
//        }
    }*/

    /**
     * Run meththod of the thread. this updates the table data
     * each 2 seconds
     */

    public void runThread() {
        if (((Settings.isConnected() || Settings.isOfflineMode()) && (this.isVisible() || isDetached()) /*&& (!table.isEditing()) */ && (!mouseDragging)) ||
                (Settings.isScrollingOn() && this.isVisible() /*&& (!table.isEditing()) */ && (!mouseDragging))) {
            newUpdatedTime = UpdateNotifier.getShapshotUpdatedTime(lastUpdatedTime);
            if ((Settings.isScrollingOn()) ||
                    ((!Settings.isScrollingOn()) && (newUpdatedTime > lastUpdatedTime))) {
                lastUpdatedTime = newUpdatedTime;
                try {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            int[] selectedRows;
                            int[] selectedCols;
                            if (Settings.isScrollingOn()) {
                                if (scrollCounter >= Settings.autoScrollInterval) {
                                    scrollCounter = 1;
                                    autoScrollingInLinkedWindows();
                                    highlightSelectedRow();
                                } else {
                                    scrollCounter++;
                                }
                            }
                            symbols.sortSymbols();
                            selectedRows = table.getSelectedRows();
                            selectedCols = table.getSelectedColumns();
                            ((AbstractTableModel) table.getModel()).fireTableDataChanged();// repaint();
                            if (selectedRows.length > 0) {
                                table.setRowSelectionInterval(selectedRows[0], selectedRows[selectedRows.length - 1]);
                            }
                            if (selectedCols.length > 0) {
                                table.setColumnSelectionInterval(selectedCols[0], selectedCols[selectedCols.length - 1]);
                            }
                            selectedRows = null;
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

//  #############################################################################
//      Methods to perform Auto Scrolling
//  #############################################################################

    public void stopUpdating() {
        //TableUpdator.getSharedInstance().unregister(this);
        active = false;
    }

    private void getVisibleColumnIndex() {
        for (int i = 0; i < table.getRowCount(); i++) {
            if ((table.getCellRect(0, i, true)).width > 0) {
                visibleColumnindex = i;
                break;
            }
        }
    }

    public void setScrollingViewPort(int beginRowIndex) {
        noOfRowsInViewport = 0;
        if (((beginRowIndex == (table.getRowCount() - 2)) && isCustomType()) || (beginRowIndex == (table.getRowCount() - 1)))
            viewportBeginRowindex = 0;
        else
            viewportBeginRowindex = beginRowIndex;
        if (table.getHeight() < (g_oJScrollPane.getViewport().getHeight() + table.getRowHeight())) {
            //if (table.getHeight() < (g_oJScrollPane.getViewport().getHeight())) {
//            currentHighlightedRow = 0;
//            viewportBeginRowindex = 0;
//            if (this.isCustomType())
//                noOfRowsInViewport = table.getRowCount() - 1;
//            else
//                noOfRowsInViewport = table.getRowCount();
            //currentHighlightedRow++;
            return;
        } else {
            noOfRowsInViewport = (g_oJScrollPane.getViewport().getHeight() / table.getRowHeight());
            currentHighlightedRow = viewportBeginRowindex;

            if (currentHighlightedRow != 0)
                doVerticalScrolling(noOfRowsInViewport, true);
            else
                doVerticalScrolling(-1, false);
//                doVerticalScrolling(table.getRowCount(), false);
        }
        viewportEndRowindex = viewportBeginRowindex + noOfRowsInViewport;
    }

    /**
     * Sleep the thread for a give time period
     */
    /*private void Sleep(long lDelay) {
        try {
            g_oThread.sleepThread(viewSettings, lDelay);
        } catch (Exception e) {
        }
    }*/
    public void highlightSelectedRow() {
        try {
            getVisibleColumnIndex();
            /*if (table.getHeight() < (g_oJScrollPane.getViewport().getHeight() + table.getRowHeight())) {
                return;
            }*/
            if ((currentHighlightedRow < viewportEndRowindex) &&
                    (viewportEndRowindex != 0)) {
                currentHighlightedRow = table.getSelectedRow() + 1;
                if (currentHighlightedRow >= viewportEndRowindex)
                    currentHighlightedRow = 0;

                Rectangle scrollRect = table.getVisibleRect(); // new Rectangle(xPos, yPos, vwPortWidth, vwPortHeight);
                Rectangle selCellRect = table.getCellRect(currentHighlightedRow, visibleColumnindex, true);

                //if (!SwingUtilities.isRectangleContainingRectangle(scrollRect, selCellRect)) {
                if ((scrollRect.y > selCellRect.y) || ((scrollRect.y + scrollRect.height) < selCellRect.y)) {
                    int row = table.rowAtPoint(new Point(scrollRect.x, scrollRect.y));
                    if (currentHighlightedRow > row) {
                        row = (currentHighlightedRow - row) - 1; // 2
                        doVerticalScrolling(row, true);
                    } else if (currentHighlightedRow < row) {
                        row = (row - currentHighlightedRow) + 1; // 2
                        doVerticalScrolling(row, false);
                    }
                    viewportEndRowindex = currentHighlightedRow + noOfRowsInViewport - 1;
                }
                scrollRect = null;
                selCellRect = null;

                table.setRowSelectionInterval(currentHighlightedRow,
                        currentHighlightedRow);
                table.setColumnSelectionInterval(0, table.getColumnCount() - 1);
                currentHighlightedRow++;
            } else {
                setScrollingViewPort(currentHighlightedRow);
            }
        } catch (Exception ex) {
            setScrollingViewPort(table.getRowCount() - 1);
        }
    }

    public void setSortColumn(int col, boolean ascending) {
        getSymbols().setSortColumn(col);
        getSymbols().setSortOrder(ascending);
        getSymbols().sortSymbols();
        if (isSortable()) {
            SortButtonRenderer oRend = (SortButtonRenderer) table.getColumnModel().getColumn(col).getHeaderRenderer();
            oRend.setSelectedColumn(col, ascending ? SortButtonRenderer.DOWN : SortButtonRenderer.UP);
            oRend = null;
        }
        table.getTableHeader().updateUI();
    }

    private void setDecimalPlaces() {
        String decimalPlaces = viewSettings.getProperty(ViewConstants.VC_DECIMAL_PLACES);
        /*Exchange exchange = null;
        try {
            exchange =ExchangeStore.getSharedInstance().getExchange(viewSettings.getID().split(Meta.FD)[1]);
            table.setDecimalPlaces(exchange.getPriceDecimalPlaces());
        } catch (Exception e) {
            table.setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
        }*/
        if (decimalPlaces != null) {
            try {
                table.setDecimalPlaces(Byte.parseByte(decimalPlaces));
            } catch (Exception ex) {
            }
        }
        decimalPlaces = null;
    }

    /**
     * Checks if the table is sortable
     */
    private boolean isSortable() {
        return g_bSortable;
    }

    /**
     * Sets if the sortable flag to the table
     */
    public void setSortable(boolean bSortable) {
        g_bSortable = bSortable;
    }

    public synchronized boolean isTableLocked() {
        return tableLocked;
    }

    public synchronized void lockTable() {
        tableLocked = true;
    }


    /* Internal frame listener */

    public synchronized void unlockTable() {
        tableLocked = false;
    }

    public void internalFrameOpened(InternalFrameEvent e) {
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        // this method will not be called since doDefaultCloseAction method is overridden
        /*Client.setSelectedTable(null);
        focusNextWindow();*/
    }

    public void internalFrameClosed(InternalFrameEvent e) {
        Application.getInstance().removeApplicationListener(this);
        unregisterSymbols();
        SymbolsRegistry.getSharedInstance().unregister(symbols, isCustomType());
    }

    public void internalFrameIconified(InternalFrameEvent e) {
        try {
            // this.setIcon(true);
            unregisterSymbols();
        } catch (Exception e2) {
            //System.out.println("Not Iconified!");
        }
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
        try {
            //this.setIcon(false);
            registerSymbols();
        } catch (Exception e2) {
            //System.out.println("Not Deiconified!");
        }
    }

    public void internalFrameActivated(InternalFrameEvent e) {
        Client.getInstance().setSelectedTable(this);
    }

    /* Table wrapper methods */

    public void internalFrameDeactivated(InternalFrameEvent e) {
        try {
            if (table.isEditing()) {
                table.getCellEditor().cancelCellEditing();
            }
        } catch (Exception ex) {
        }
    }

    public boolean isWindowVisible() {
        return isVisible();
    }

    public int getWindowStyle() {
        if (isMaximum())
            return ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
    }

    public JTable getTable1() {
        return table;
    }

    public JTable getTable2() {
        return null;
    }

    public JTable getTabFoler(String id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getZOrder() {
        return getDesktopPane().getPosition(this);
    }

    public String getTypeDescription() {
        return typeDescription;
    }
    // for (Data feed by Tadawul) tag in "full market" and "today trade" windows

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getTDWLDescription() {
        return TDWLDescription;
    }

    public void setTDWLDescription(String TDWLDescription) {
        this.TDWLDescription = TDWLDescription;
    }

    /* Mouse Listener Methods */

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof javax.swing.plaf.metal.MetalInternalFrameTitlePane || e.getSource() instanceof TWInternalFrameTitlePane) {
            if (SwingUtilities.isRightMouseButton(e)) {
                GUISettings.showPopup(titleBarMenu, this, e.getX(), e.getY());
            }
        } else if (e.getSource() == table) {
            currentHighlightedRow = table.getSelectedRow();
            if (isLinkControlEnabled && !SwingUtilities.isRightMouseButton(e)) {
                TableModel oModel = table.getModel();
                String sSymbol = ((StringTransferObject) oModel.getValueAt(currentHighlightedRow, -1)).getValue();
                String exchange = ((StringTransferObject) oModel.getValueAt(currentHighlightedRow, -2)).getValue();
                int instrumentType = (int) ((LongTransferObject) oModel.getValueAt(currentHighlightedRow, -11)).getValue();
                fireSymbolChangeInLinkedWindows(getLinkedGroupID(), SharedMethods.getKey(exchange, sSymbol, instrumentType));
            }

        }
    }

    public void mousePressed(MouseEvent e) {

        if (e.getSource() == table) {
            int rowAtPoint = table.rowAtPoint(new Point(e.getX(), e.getY()));
            boolean alreadySelected = false;
            int[] selectedRows = table.getSelectedRows();
            for (int i = 0; i < selectedRows.length; i++) {
                if (rowAtPoint == selectedRows[i]) {
                    alreadySelected = true;
                    break;
                }
            }
            if (!alreadySelected) {
                table.setRowSelectionInterval(rowAtPoint, rowAtPoint);
            }
            selectedRows = null;
            currentHighlightedRow = table.getSelectedRow();
        }
    }

    public void mouseReleased(MouseEvent e) {
        mouseDragging = false;
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        mouseDragging = true;
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("HIDE")) {
            ((BasicInternalFrameUI) getUI()).setNorthPane(null);
            viewSettings.setHeaderVisible(false);
            setHideShowMenuCaption();
            this.revalidate();
        } else if (e.getActionCommand().equals("HEADER")) {
            toggleTitleBar();
        } else if (e.getActionCommand().equals("HIDE_COL")) {
            hideColumn();
        } else if (e.getActionCommand().equals("EDIT_FOR")) {
            editFormula();
        } else if (e.getActionCommand().equals("DELETE_FOR")) {
            deleteFormula();
        } else if (e.getActionCommand().equals("COL_SETTINGS")) {
            Client.getInstance().setSelectedTable(this);
            Client.getInstance().g_mnuTreePopSettings_Selected();
        } else if (e.getActionCommand().equals("CUSTOMIZE")) {
            Client.getInstance().setSelectedTable(this);
            Client.getInstance().showCustomizer();
        }
    }

    private void revalidateColumns() {
        TableColumnModel columnModel = table.getColumnModel();
        int viewportWidth = g_oJScrollPane.getViewport().getWidth();
        int tableWidth = table.getWidth();

        int viewColumn;

        if (!g_bDirLTR) { // this checking is necessary due to a bug in java 1.4
            if (tableWidth > viewportWidth) // take the maximum value
                viewColumn = columnModel.getColumnIndexAtX(tableWidth - (int) mousePoint.getX());
            else
                viewColumn = columnModel.getColumnIndexAtX(viewportWidth - (int) mousePoint.getX());
        } else {
            viewColumn = columnModel.getColumnIndexAtX((int) mousePoint.getX());
        }
        int columnIndex = table.convertColumnIndexToModel(viewColumn);
        String name = table.getColumnName(viewColumn);

        if (CustomFormulaStore.getSharedInstance().isCustomFormularColumn(name)) {
            editformula.setVisible(true);
            editformula.setEnabled(true);
            deleteformula.setVisible(true);
            deleteformula.setEnabled(true);
        } else {
            editformula.setVisible(false);
            editformula.setEnabled(false);
            deleteformula.setVisible(false);
            deleteformula.setEnabled(false);
        }
    }

    private void editFormula() {
        TableColumnModel columnModel = table.getColumnModel();
        int viewportWidth = g_oJScrollPane.getViewport().getWidth();
        int tableWidth = table.getWidth();

        int viewColumn;
        if (!g_bDirLTR) { // this checking is necessary due to a bug in java 1.4
            if (tableWidth > viewportWidth) // take the maximum value
                viewColumn = columnModel.getColumnIndexAtX(tableWidth - (int) mousePoint.getX());
            else
                viewColumn = columnModel.getColumnIndexAtX(viewportWidth - (int) mousePoint.getX());
        } else {
            viewColumn = columnModel.getColumnIndexAtX((int) mousePoint.getX());
        }
        int columnIndex = table.convertColumnIndexToModel(viewColumn);

        String name = table.getColumnName(viewColumn);
        FormularEditor.getSharedInstance().setType(FormularEditor.EDIT_FORMULA);
        FormularEditor.getSharedInstance().setEdittingFormula(name);
        FormularEditor.getSharedInstance().showWindow();
    }

    private void deleteFormula() {
        if (SharedMethods.showConfirmMessage(Language.getString("CONFIRM_DELETE_IN_CUST_FORMULA"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION) {
            TableColumnModel columnModel = table.getColumnModel();
            int viewportWidth = g_oJScrollPane.getViewport().getWidth();
            int tableWidth = table.getWidth();

            int viewColumn;
            if (!g_bDirLTR) { // this checking is necessary due to a bug in java 1.4
                if (tableWidth > viewportWidth) // take the maximum value
                    viewColumn = columnModel.getColumnIndexAtX(tableWidth - (int) mousePoint.getX());
                else
                    viewColumn = columnModel.getColumnIndexAtX(viewportWidth - (int) mousePoint.getX());
            } else {
                viewColumn = columnModel.getColumnIndexAtX((int) mousePoint.getX());
            }
            int columnIndex = table.convertColumnIndexToModel(viewColumn);

            String name = table.getColumnName(viewColumn);
            if (CustomFormulaStore.getSharedInstance().isCustomFormularColumn(name)) {
                System.out.println("customer formular removed =" + name);
                this.saveColumnPositions();
//                BigInteger columns = viewSettings.getColumns();
//                BigInteger columnFilter =  BigInteger.valueOf(1).shiftLeft(columnIndex);
//                columnFilter = columnFilter.xor(BigInteger.valueOf(0xFFFFFFFFFFFFFFFFL));
//                columns = columns.and(columnFilter);
//                viewSettings.setColumns(columns);
                CustomFormulaStore.getSharedInstance().removeCustomeFormula(name);
            }
        }

    }

    /* -Finaliser-*/

    protected void finalize() throws Throwable {
        //System.out.println("Closing view " + getTitle());
        SymbolsRegistry.getSharedInstance().unregister(symbols, isCustomType());
        Theme.unRegisterComponent(this);
    }

    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent event) {

        /* Check if the drag came form prit button */
        try {
            String key = (String) event.getTransferable().getTransferData(DataFlavor.stringFlavor);
            if (key.equals("Print")) {
                event.getDropTargetContext().dropComplete(true);
            } else {
                event.rejectDrop(); // not from the print button. reject drop
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            event.rejectDrop();
            return;
        }
        printTable();
    }

    /**
     * Re-enables double buffering globally.
     */

    /*public static void enableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }*/
    public void cancelSorting() {
        renderer.unsort();//(g_oViewSettings.getSortColumn());
        viewSettings.setSortColumn(-1);
        table.getTableHeader().updateUI();
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void closeWindow() {
        TWDesktop.doCloseWindow(this);
        focusNextWindow();
    }

    public void doDefaultCloseAction() {
//        DataStore.getSharedInstance().unregisterSymbols(getSymbols());
        TWDesktop.doCloseWindow(this);
        focusNextWindow();
    }

    private void focusNextWindow() {
        TWDesktop desktop = (TWDesktop) Client.getInstance().getDesktop();
        desktop.focusNextWindow(desktop.getListPosition(this, true), true);
        desktop = null;
    }

    public int getDesktopItemType() {
        return desktopItemType;
    }

    public void setDesktopItemType(int type) {
        desktopItemType = type;
    }

    public int getWindowType() {
        return windowType;
    }

    public void setWindowType(int type) {
        windowType = type;
    }

    /*public boolean isAutoScrollableTable() {
        return autoScrollableTable;
    }

    public void setAutoScrollableTable(boolean autoScrollableTable) {
        this.autoScrollableTable = autoScrollableTable;
    }*/

    private void doVerticalScrolling(int noOfRows, boolean isScrollDown) {
        if (g_oJScrollPane != null) {
            JScrollBar vBar = g_oJScrollPane.getVerticalScrollBar();
            if (noOfRows == -1) {
                vBar.setValue(0); // go to beginning
            } else {
                if (isScrollDown)
                    vBar.setValue(vBar.getValue() + vBar.getUnitIncrement(1) * noOfRows);
                else
                    vBar.setValue(vBar.getValue() - vBar.getUnitIncrement(-1) * noOfRows);
            }
        }
    }

    public boolean isHeaderPopupActive() {
        return headerPopupActive;
    }

    public void setHeaderPopupActive(boolean headerPopupActive) {
        this.headerPopupActive = headerPopupActive;
    }

    public void setLocationRelativeTo(Component c) {
        Dimension parentSize = c.getSize();

        this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
                (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                this.getWidth(), this.getHeight());
    }

    public String getTitle() {
        try {
            String currentyTitle = "";
            if (currency != null) {
                currentyTitle = " - " + currency;
            }

            if (TDWLDescription == null) {
                if (!isCustomType()) {
                    if (typeDescription != null)
                        return typeDescription + " - " + title + currentyTitle;
                    else
                        return title + currentyTitle;
                } else {
                    if (typeDescription != null)
                        return typeDescription + " - " + ((WatchListStore) viewSettings.getSymbols()).getCaption() + currentyTitle;
                    else
                        return ((WatchListStore) viewSettings.getSymbols()).getCaption() + currentyTitle;
                }
            } else {
                if (!isCustomType()) {
                    if (typeDescription != null)
                        return typeDescription + " - " + title + currentyTitle + TDWLDescription;
                    else
                        return title + currentyTitle;
                } else {
                    if (typeDescription != null)
                        return typeDescription + " - " + ((WatchListStore) viewSettings.getSymbols()).getCaption() + currentyTitle + TDWLDescription;
                    else
                        return ((WatchListStore) viewSettings.getSymbols()).getCaption() + currentyTitle;
                }
            }
        } catch (Exception e) {
            return "";
        }
    }

    public void setTitle(String title) {
        this.title = title;
        super.setTitle(title);
//        if (timeAndSales != null)
//            timeAndSales.setTitle(Language.getString("TIME_AND_SALES") + " - " + title);
    }

    public Constants.ThreadTypes getTableID() {
        return Constants.ThreadTypes.MAIN_BOARDS;
    }

    public boolean isTopStocksType() {
        return topStocksType;
    }

    public void setTopStocksType(boolean topStocksType) {
        this.topStocksType = topStocksType;
    }

    public synchronized boolean isSymbolsRegistered() {
        return symbolsRegistered;
    }

    public synchronized void setSymbolsRegistered(boolean symbolsRegistered) {
        this.symbolsRegistered = symbolsRegistered;
    }

    public void applicationExiting() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void applicationLoaded() {
//        try {
        //if (isShowing()) {
//                DataStore.getSharedInstance().registerSymbols(getSymbols());
        //}
//        } catch (Exception e) {
//
//        }
    }

    public void applicationReadyForTransactions() {
//        try {
//            if (isShowing()) {
//                DataStore.getSharedInstance().registerSymbols(getSymbols());
//            }
//        } catch (Exception e) {
//
//        }
    }

    public void applicationLoading(int percentage) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {

    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    //change start

    public void updateClientTable() {
//        int[][] columnSettings = viewSettings.getColumnSettings();
//        int count = CustomFormulaStore.getSharedInstance().getColumnCount();
//
//
//        String[] cols = viewSettings.getColumnHeadings();
//        String[] newCols = new String[(cols.length+count)];
//        for (int i = 0; i < cols.length; i++) {
//            newCols[i] = cols[i];
//        }
//        for(int k=0 ; k < count; k++){
//            int no = cols.length + k;
//            newCols[no] = CustomFormulaStore.getSharedInstance().getColumnName(k);
//        }
//
//        viewSettings.setColumnHeadings(newCols);
//
//        if(columnSettings!=null){
//            int[][] newColSettings = new int[(columnSettings.length+count)][3];
//            for (int i = 0; i < columnSettings.length; i++) {
//                for(int j =0; j<3; j++){
//                    newColSettings[i][j] = columnSettings[i][j];
//                }
//            }
//            for(int k=0 ; k < count; k++){
//                int no = columnSettings.length + k;
//                newColSettings[no][0] = no;
//                newColSettings[no][1] = 0;
//                newColSettings[no][2] = no;
//            }
//            viewSettings.setColumnSettings(newColSettings);
//        }
//
//        this.getTable().createDefaultColumnsFromModel();
//        setColumnIdentifiers();
//
//        renderer = new SortButtonRenderer(true);
//        renderer.setShowToolTip();
//        TableColumnModel colmodel = table.getColumnModel();
//        int n = viewSettings.getColumnHeadings().length;
//        for (int i = 0; i < n; i++) {
//            colmodel.getColumn(i).setHeaderRenderer(renderer);
//        }

//        table.updateHeaderUI();
        adjustColumns();
//        this.getTable().updateUI();
        this.updateGUI();
//        table.repaint();

    }
    //change end

    public void customFormulaAdded(String columnName) {
        saveColumnPositions();
        String[] cols = new String[0];
        String[] newCols = new String[0];
        try {
            int[][] columnSettings = viewSettings.getColumnSettings();
            int[][] newColSettings = new int[columnSettings.length + 1][3];
            for (int i = 0; i < columnSettings.length; i++) {
                for (int j = 0; j < 3; j++) {
                    newColSettings[i][j] = columnSettings[i][j];
                }
            }
            newColSettings[columnSettings.length][0] = columnSettings.length;
            newColSettings[columnSettings.length][1] = 0;
            newColSettings[columnSettings.length][2] = columnSettings.length;

            cols = viewSettings.getColumnHeadings();
            newCols = new String[cols.length + 1];
            for (int i = 0; i < cols.length; i++) {
                newCols[i] = cols[i];
            }
            newCols[cols.length] = columnName;
            viewSettings.setColumnHeadings(newCols);
            viewSettings.setColumnSettings(newColSettings);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cols = null;
        newCols = null;
        this.getTable().createDefaultColumnsFromModel();
        setColumnIdentifiers();
        renderer = new SortButtonRenderer(true);
        renderer.setShowToolTip();
        TableColumnModel colmodel = table.getColumnModel();
        int n = viewSettings.getColumnHeadings().length;
        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }
        table.updateHeaderUI();
        adjustColumns();
        this.getTable().updateUI();
        table.updateUI();
        this.updateGUI();
        table.repaint();
    }

    public void customFormulaRemoved(String columnName) {

        saveColumnPositions();
        int columnIndex = (g_asHeadings.length + CustomFormulaStore.getSharedInstance().getColumnIdentifier(columnName));
        try {
            BigInteger columns1 = viewSettings.getColumns();    //BigInteger.valueOf(110);   // viewSettings.getColumns();
            BigInteger columns2 = viewSettings.getColumns();    //BigInteger.valueOf(110);   //  viewSettings.getColumns();
            BigInteger left = (columns1.shiftRight(columnIndex + 1)).shiftLeft(columnIndex);
            BigInteger col2u = ((BigInteger.valueOf(1)).shiftLeft(columnIndex)).negate().not();
            BigInteger right = col2u.and(columns2);
            BigInteger columnFilter = left.or(right);
            viewSettings.setColumns(columnFilter);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        String[] cols = viewSettings.getColumnHeadings();
        String[] newCols = new String[cols.length - 1];
        int x = 0;
        for (int i = 0; i < cols.length; i++) {
            if (!cols[i].equals(columnName)) {
                newCols[x] = cols[i];
                x++;
            }
        }
        viewSettings.setColumnHeadings(newCols);
        cols = null;
        newCols = null;

        int[][] columnSettings = viewSettings.getColumnSettings();
        int[][] newColSettings = new int[columnSettings.length - 1][3];
        int colLocation = columnSettings[columnIndex][2];
        int y = 0;
        for (int i = 0; i < columnSettings.length; i++) {
            if (columnSettings[i][0] < columnIndex) {
                for (int j = 0; j < 3; j++) {
                    newColSettings[y][j] = columnSettings[i][j];
                }
                if (columnSettings[i][2] > colLocation) {
                    newColSettings[y][2] = (columnSettings[i][2] - 1);
                }
                y++;
            } else if (columnSettings[i][0] > columnIndex) {
                newColSettings[y][0] = (columnSettings[i][0] - 1);
                newColSettings[y][1] = columnSettings[i][1];
                newColSettings[y][2] = columnSettings[i][2];

                if (columnSettings[i][2] > colLocation) {
                    newColSettings[y][2] = (columnSettings[i][2] - 1);
                }
                y++;
            }
        }
        viewSettings.setColumnSettings(newColSettings);
        this.getTable().createDefaultColumnsFromModel();
        setColumnIdentifiers();

        renderer = new SortButtonRenderer(true);
        renderer.setShowToolTip();
        TableColumnModel colmodel = table.getColumnModel();
        int n = viewSettings.getColumnHeadings().length;
        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }
        table.updateHeaderUI();
        adjustColumns();
        this.getTable().updateUI();
        table.updateUI();
        this.updateGUI();
        table.repaint();

    }

    public void customFormulaEditted(String columnName, String newName) {
        saveColumnPositions();
        try {
            String[] cols = viewSettings.getColumnHeadings();
            String[] newCols = new String[cols.length];
            for (int i = 0; i < cols.length; i++) {
                if (cols[i].equals(columnName)) {
                    newCols[i] = newName;
                } else
                    newCols[i] = cols[i];
            }
            viewSettings.setColumnHeadings(newCols);
        } catch (Exception e) {
        }
        this.getTable().createDefaultColumnsFromModel();
        setColumnIdentifiers();
        renderer = new SortButtonRenderer(true);
        renderer.setShowToolTip();
        TableColumnModel colmodel = table.getColumnModel();
        int n = viewSettings.getColumnHeadings().length;
        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }
        table.updateHeaderUI();
        adjustColumns();
        this.updateGUI();
        table.repaint();
    }

    public void fireColumnChanged() {
        try {
            saveColumnPositions();
            this.getTable().createDefaultColumnsFromModel();
            setColumnIdentifiers();
            renderer = new SortButtonRenderer(true);
            renderer.setShowToolTip();
            TableColumnModel colmodel = table.getColumnModel();
            int n = viewSettings.getColumnHeadings().length;
            for (int i = 0; i < n; i++) {
                colmodel.getColumn(i).setHeaderRenderer(renderer);
            }
            table.updateHeaderUI();
            adjustColumns();
            this.updateGUI();
            table.updateUI();
            table.repaint();
//            this.applySettings();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void notifyColumnRename() {
        try {
            saveColumnPositions();
            this.getTable().createDefaultColumnsFromModel();
            setColumnIdentifiers();
            renderer = new SortButtonRenderer(true);
            renderer.setShowToolTip();
            TableColumnModel colmodel = table.getColumnModel();
            int n = viewSettings.getColumnHeadings().length;
            for (int i = 0; i < n; i++) {
                colmodel.getColumn(i).setHeaderRenderer(renderer);
            }
            table.updateHeaderUI();
            this.updateGUI();
            table.repaint();
//            this.applySettings();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void resetColumns() {
        Vector g_oSelected = new Vector();
        BigInteger lSelectedPos = (viewSettings.getColumns().or(viewSettings.getFixedColumns())).and(viewSettings.getHiddenColumns().not());
        BigInteger lFixedPos = viewSettings.getFixedColumns();
        BigInteger lHiddenPos = viewSettings.getHiddenColumns();

        String[] asColumns = viewSettings.getColumnHeadings();

        for (int i = 0; i < asColumns.length; i++) {
            //change start
            if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
            {
                if ((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
                } else {
                    if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))
                        g_oSelected.add(asColumns[i]);
                }
            }
            //change start
            lSelectedPos = lSelectedPos.shiftRight(1);
            lFixedPos = lFixedPos.shiftRight(1);
            lHiddenPos = lHiddenPos.shiftRight(1);
//            lSelectedPos >>= 1;
//            lFixedPos >>= 1;
//            lHiddenPos >>= 1;
            //change end
        }

        Object[] tempArray = g_oSelected.toArray();
        Arrays.sort(tempArray);
        g_oSelected.clear();
        for (int k = 0; k < tempArray.length; k++) {
            g_oSelected.addElement(tempArray[k]);
        }
        tempArray = null;

        BigInteger lCols = BigInteger.valueOf(0);
        for (int i = 0; i < asColumns.length; i++) {
            if (g_oSelected.indexOf(asColumns[i]) != -1) {
                //change start
                lCols = lCols.add(pwr(i));
            }
        }
        viewSettings.setColumns((lCols));
    }

    private BigInteger pwr(int iPwr) {
        BigInteger i = BigInteger.valueOf(1);
        return (i.shiftLeft(iPwr));
    }

    public void addColumn(String columnName) {
//         SharedMethods.printLine("addColumn ="+columnName , true);
        String[] headings = viewSettings.getColumnHeadings();
        boolean isNewColumn = true;
        int index = 0;
        for (int i = 0; i < headings.length; i++) {
            if (headings[i].equals(columnName)) {
                isNewColumn = false;
                index = i;
                break;
            }
        }
        if (isNewColumn) {
            customFormulaAdded(columnName);

            Vector g_oSelected = new Vector();
            BigInteger lSelectedPos = (viewSettings.getColumns().or(viewSettings.getFixedColumns())).and(viewSettings.getHiddenColumns().not());
            BigInteger lFixedPos = viewSettings.getFixedColumns();
            BigInteger lHiddenPos = viewSettings.getHiddenColumns();

            String[] asColumns = viewSettings.getColumnHeadings();
            for (int i = 0; i < (asColumns.length - 1); i++) {
                if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
                {
                    if ((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
                    } else {
                        if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))
                            g_oSelected.add(asColumns[i]);
                    }
                }
                lSelectedPos = lSelectedPos.shiftRight(1);
                lFixedPos = lFixedPos.shiftRight(1);
                lHiddenPos = lHiddenPos.shiftRight(1);
            }
            g_oSelected.add(asColumns[(asColumns.length - 1)]);
            lSelectedPos = lSelectedPos.shiftRight(1);
            lFixedPos = lFixedPos.shiftRight(1);
            lHiddenPos = lHiddenPos.shiftRight(1);

            Object[] tempArray = g_oSelected.toArray();
            Arrays.sort(tempArray);
            g_oSelected.clear();
            for (int k = 0; k < tempArray.length; k++) {
                g_oSelected.addElement(tempArray[k]);
            }
            tempArray = null;

            BigInteger lCols = BigInteger.valueOf(0);
            for (int i = 0; i < asColumns.length; i++) {
                if (g_oSelected.indexOf(asColumns[i]) != -1) {
                    //change start
                    lCols = lCols.add(pwr(i));
                }
            }
            viewSettings.setColumns((lCols));
//            System.out.println("column Added = "+columnName + " = to table = "+ this.getTitle());
        } else {
            Vector g_oSelected = new Vector();
            BigInteger lSelectedPos = (viewSettings.getColumns().or(viewSettings.getFixedColumns())).and(viewSettings.getHiddenColumns().not());
            BigInteger lFixedPos = viewSettings.getFixedColumns();
            BigInteger lHiddenPos = viewSettings.getHiddenColumns();

            String[] asColumns = viewSettings.getColumnHeadings();
            for (int i = 0; i < (asColumns.length); i++) {
                if (i != index) {
                    if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
                    {
                        if ((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
                        } else {
                            if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))
                                g_oSelected.add(asColumns[i]);
                        }
                    }
                } else {
                    g_oSelected.add(asColumns[i]);
                }
                lSelectedPos = lSelectedPos.shiftRight(1);
                lFixedPos = lFixedPos.shiftRight(1);
                lHiddenPos = lHiddenPos.shiftRight(1);
            }
            g_oSelected.add(asColumns[(asColumns.length - 1)]);
            lSelectedPos = lSelectedPos.shiftRight(1);
            lFixedPos = lFixedPos.shiftRight(1);
            lHiddenPos = lHiddenPos.shiftRight(1);

            Object[] tempArray = g_oSelected.toArray();
            Arrays.sort(tempArray);
            g_oSelected.clear();
            for (int k = 0; k < tempArray.length; k++) {
                g_oSelected.addElement(tempArray[k]);
            }
            tempArray = null;

            BigInteger lCols = BigInteger.valueOf(0);
            for (int i = 0; i < asColumns.length; i++) {
                if (g_oSelected.indexOf(asColumns[i]) != -1) {
                    lCols = lCols.add(pwr(i));
                }
            }
            viewSettings.setColumns((lCols));
//            System.out.println("column Added = "+columnName + " = to table = "+ this.getTitle());

        }
        table.updateHeaderUI();
        adjustColumns();
        this.updateGUI();
        table.repaint();
    }

    public Dimension getMinimumSize() {
        return minimumSize;
    }

    public void setMinimumSize(Dimension minimumSize) {
        this.minimumSize = minimumSize;
        super.setMinimumSize(minimumSize);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        int button = (Integer) evt.getOldValue() / 1000; // why devided by 1000? check TableHeaderUI class for details

        if (((button & InputEvent.BUTTON1_MASK) != 0) && isSortable()) {
            if (!sortingInProgress) {
                try {
                    sortingInProgress = true;
                    /*TableColumnModel columnModel = table.getColumnModel();
                    int viewportWidth = g_oJScrollPane.getViewport().getWidth();
                    int tableWidth = table.getWidth();

                    int viewColumn;*/
                    /*if (!g_bDirLTR) { // this checking is necessary due to a bug in java 1.4
                        if (tableWidth > viewportWidth) // take the maximum value
                            viewColumn = columnModel.getColumnIndexAtX(tableWidth - e.getX());
                        else
                            viewColumn = columnModel.getColumnIndexAtX(viewportWidth - e.getX());
                    } else {
                        viewColumn = columnModel.getColumnIndexAtX(e.getX());
                    }*/

                    int column = table.convertColumnIndexToModel((Integer) evt.getNewValue());

                    /* Set the sort arrow for the header */

                    SortButtonRenderer oRend = (SortButtonRenderer) table.getColumnModel().getColumn(column).getHeaderRenderer();
                    int sortOrder = oRend.setSelectedColumn(column);
                    viewSettings.setSortOrder(sortOrder);
                    oRend = null;

                    if (sortOrder == SortButtonRenderer.DOWN)
                        g_bSortOrder = true;
                    else
                        g_bSortOrder = false;

                    g_iLastSortedCol = column;
                    symbols.sortSymbols(column, g_bSortOrder);
                    viewSettings.setSortColumn(column);
                    table.repaint();
                    table.getTableHeader().repaint();
                    sortingInProgress = false;
                } catch (Exception e1) {
                    sortingInProgress = false;
                }
            }
        }

    }

    private void initMouseGestures() {
        mouseGestures = new MouseGestures(table);
        mouseGestures.setGridSize(10);
        mouseGestures.setMouseButton(MouseEvent.BUTTON3_MASK);
        mouseGestures.addMouseGesturesListener(new MouseGesturesListener() {
            public void gestureMovementRecognized(String currentGesture) {
//                setGestureString(currentGesture);
            }

            public void processGesture(String gesture) {
                try {
                    Thread.sleep(200);
                    Client.getInstance().setGestureString(gesture);
                } catch (InterruptedException e) {
                }
                Client.getInstance().setGestureString("");
            }
        });
//        table.addMouseMotionListener(mouseGestures);
//        mouseGestures.start();
    }

    /* class TestColModel extends DefaultTableColumnModel{
        public TableColumn getColumn(int columnIndex) {
            try {
                return getColumns().nextElement();
            } catch (Exception e) {
                return super.getColumn(columnIndex);
            }
        }
    }*/

//    public void setLinkGroupsEnabled(boolean linkGroupsEnabled) {
//        isLinkControlEnabled = linkGroupsEnabled;
//    }

    public void fireSymbolChangeInLinkedWindows(String group, String sKey) {
        if (linkedGroupID != LinkStore.LINK_NONE) {
            LinkStore.getSharedInstance().fireSymbolChangedForGroup(group, sKey);

        }

    }

    public boolean isLinkWindowControlEnabled() {
        return isLinkControlEnabled;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setLinkWindowControl(boolean isCtrlEnabled) {
        isLinkControlEnabled = isCtrlEnabled;
    }

    private void createLinkGroupMenu() {
        linkGroupMenu = new JPopupMenu();

        TWMenuItem redmenu = new TWMenuItem(Language.getString("RED"), "lnk_red.gif");
        redmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_RED);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_RED);
            }
        });
        linkGroupMenu.add(redmenu);

        TWMenuItem greenmenu = new TWMenuItem(Language.getString("GREEN"), "lnk_green.gif");
        greenmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_GREEN);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_GREEN);
            }
        });
        linkGroupMenu.add(greenmenu);

        TWMenuItem bluemenu = new TWMenuItem(Language.getString("BLUE"), "lnk_blue.gif");
        bluemenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_BLUE);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_BLUE);
            }
        });
        linkGroupMenu.add(bluemenu);

        TWMenuItem yellowmenu = new TWMenuItem(Language.getString("YELLOW"), "lnk_yellow.gif");
        yellowmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_YELLOW);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_YELLOW);
            }
        });
        linkGroupMenu.add(yellowmenu);

        TWMenuItem whitemenu = new TWMenuItem(Language.getString("WHITE"), "lnk_white.gif");
        whitemenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_WHITE);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_WHITE);
            }
        });
        linkGroupMenu.add(whitemenu);

        TWMenuItem notlinkedmenu = new TWMenuItem(Language.getString("NOT_LINK"), "lnk_notlink.gif");
        notlinkedmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_NONE);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_NONE);
            }
        });
        linkGroupMenu.add(notlinkedmenu);

        GUISettings.applyOrientation(linkGroupMenu);

    }

    public String getLinkedGroupID() {
        return linkedGroupID;
    }

    public void setLinkedGroupID(String linkedGroupID) {
        this.linkedGroupID = linkedGroupID;
        try {
            ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(linkedGroupID);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (viewSettings != null) {
            viewSettings.putProperty(ViewConstants.VC_LINKED_GROUP, linkedGroupID);
        }
    }

    public void attach() {
        ((DetachableRootPane) ((JFrame) detachedFrame).getRootPane()).detach();
    }

    private void ensureVisibility() {
        try {
            if (this instanceof NonTabbable) return;

            Dimension parentSize = Toolkit.getDefaultToolkit().getScreenSize();
//            Dimension parentSize = this.getDesktopPane().getSize();
            Dimension mySize = this.getSize();
            Dimension myNewSize = this.getSize();

            Point myLocation = this.getLocation();
            Point myNewLocation = this.getLocation();

            boolean resized = false;
            boolean repositioned = false;

            if (mySize.height > parentSize.height) {
                myNewSize.height = (int) (parentSize.getHeight() * .75);
                resized = true;
            } else {
                myNewSize.height = mySize.height;
            }

            if (mySize.width > parentSize.width) {
                myNewSize.width = (int) (parentSize.getWidth() * .75);
                resized = true;
            } else {
                myNewSize.width = mySize.width;
            }
            if (resized) {
                this.setSize(myNewSize);
                this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                        this.getWidth(), this.getHeight());
            }

            if ((myLocation.y < 0) || (myLocation.y > parentSize.height - 50)) {
                myNewLocation.y = 0;
                repositioned = true;
            } else {
                myNewLocation.y = myLocation.y;
            }

            if ((Language.isLTR() && myLocation.x < 0) || ((!Language.isLTR()) && (myLocation.x + mySize.width < 100)) || (myLocation.x > parentSize.width - 50)) {
//            if ((myLocation.x < 0) || (myLocation.x > parentSize.width - 50)) {
                myNewLocation.x = 0;
                repositioned = true;
            } else {
                myNewLocation.x = myLocation.x;
            }
            if (repositioned) {
                this.setLocation(myNewLocation);
            }


        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public void keyTyped(KeyEvent e) {


    }

    public void keyPressed(KeyEvent e) {

        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        if ((e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN) && e.getSource().equals(table)) {
            currentHighlightedRow = table.getSelectedRow();
            if (isLinkControlEnabled) {
                TableModel oModel = table.getModel();
                String sSymbol = ((StringTransferObject) oModel.getValueAt(currentHighlightedRow, -1)).getValue();
                String exchange = ((StringTransferObject) oModel.getValueAt(currentHighlightedRow, -2)).getValue();
                int instrumentType = (int) ((LongTransferObject) oModel.getValueAt(currentHighlightedRow, -11)).getValue();
                fireSymbolChangeInLinkedWindows(getLinkedGroupID(), SharedMethods.getKey(exchange, sSymbol, instrumentType));
            }

        }
    }

    private void autoScrollingInLinkedWindows() {
        if (isLinkControlEnabled) {
            try {
                TableModel oModel = table.getModel();
                String sSymbol = ((StringTransferObject) oModel.getValueAt(currentHighlightedRow, -1)).getValue();
                String exchange = ((StringTransferObject) oModel.getValueAt(currentHighlightedRow, -2)).getValue();
                int instrumentType = (int) ((LongTransferObject) oModel.getValueAt(currentHighlightedRow, -11)).getValue();
                fireSymbolChangeInLinkedWindows(getLinkedGroupID(), SharedMethods.getKey(exchange, sSymbol, instrumentType));
            } catch (Exception e) {

            }
        }
    }

    public void checkForDetach() {
        if (viewSettings != null && viewSettings.isDetached && Settings.detachOnWspLoad) {
            try {
                ((DetachableRootPane) this.getRootPane()).detach();
                this.setSize(viewSettings.getSize());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setIcon(boolean b) throws PropertyVetoException {
        try {
            if (b) {
                this.setLayer(GUISettings.DESKTOP_ICON_LAYER);
            } else {
                if (Settings.isPutAllToSameLayer()) {
                    this.setLayer(GUISettings.DEFAULT_LAYER);
                    if (viewSettings != null) {
                        this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, viewSettings.getIndex());
                    } else {
                        this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                    }
                } else {
                    this.setLayer(getPreferredLayer());
                    if (viewSettings != null) {
                        this.getDesktopPane().setLayer(this, getPreferredLayer(), viewSettings.getIndex());
                    } else {
                        this.getDesktopPane().setLayer(this, getPreferredLayer(), 0);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        super.setIcon(b);    //To change body of overridden methods use File | Settings | File Templates.
    }
}

