package com.isi.csvr;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: 04-May-2007
 * Time: 09:50:41
 */
public class TreeMenuObject {

    private String menu = null;
    private int watchListType = 0;

    public TreeMenuObject(String name, int type) {
        this.menu = name;
        this.watchListType = type;

    }

    public String toString() {
        return menu;
    }

    public int getType() {
        return watchListType;
    }
}
