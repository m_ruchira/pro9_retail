package com.isi.csvr.netcalculator;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: ISI (c) 2002</p>
 * <p>Company: </p>
 *
 * @author Bandula
 * @version 1.0
 */
public class ChangeRecord implements Serializable {

    private String symbol;
    private long fromDate;
    private long toDate;
    private double startPrice;
    private double endPrice;
    private double netChange;
    private double high;
    private double low;
    private byte valuationType = 0;

    public ChangeRecord() {
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        ois.defaultReadObject();
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
    }

    public double getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(double endPrice) {
        this.endPrice = endPrice;
    }

    public long getFromDate() {
        return fromDate;
    }

    public void setFromDate(long fromDate) {
        this.fromDate = fromDate;
    }

    public double getNetChange() {
        return netChange;
    }

    public void setNetChange(double netChange) {
        this.netChange = netChange;
    }

/*    public String getCompanyName() {
        return compName;
    }

    public void setCompanyName(String compName) {
        this.compName = compName;
    } */

    /*public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    } */

    /*public String getExchange() {
        return exchange;
    } */

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(double startPrice) {
        this.startPrice = startPrice;
    }

    public long getToDate() {
        return toDate;
    }

    public void setToDate(long toDate) {
        this.toDate = toDate;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public byte getValuationType() {
        return valuationType;
    }

    public void setValuationType(byte valuationType) {
        this.valuationType = valuationType;
    }

    public String toString() {
        return getSymbol() + "," + getFromDate() + "," + getStartPrice() + "," + getToDate() + "," + getEndPrice();
    }

}