package com.isi.csvr.netcalculator;

import com.isi.csvr.shared.Settings;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class NetCalcStore extends Object {

    private static Vector store = null;

    public NetCalcStore() {
        loadData();
    }

    public static int size() {
        return store.size();
    }

    public static ChangeRecord getRecord(int i) {
        return (ChangeRecord) store.elementAt(i);
    }

    public static void addRecord(ChangeRecord record) {
        store.addElement(record);
    }

    public static void removeRecord(int index) {
        try {
            store.remove(index);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Save the hash table as an object to the disk
     */
    public static void saveData() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/netChange.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(store);
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Loads the saved hash table from the disk as an object
     */
    public void loadData() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/netChange.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            store = (Vector) oObjIn.readObject();
            oIn.close();
            oIn = null;
        } catch (Exception e) {
            store = new Vector();
            e.printStackTrace();
        }

    }
}