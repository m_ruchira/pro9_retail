// Copyright (c) 2002 Integrated Systems International (ISI)
package com.isi.csvr.netcalculator;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.TWActions;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.TableUpdateListener;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.text.ParseException;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

public class NetCalcModel extends CommonTable
        implements TableModel, CommonTableInterface, TableUpdateListener {
    public static NetCalcModel self;
    private final byte CURRENT_TO_DATE = 1;
    private final byte CURRENT_FROM_DATE = 2;
    private final byte BOTH_CURRENT_DATES = 0;
    private final byte BOTH_HISTORY_DATES = 3;
    private TWDateFormat dateformat;
//    private byte valuationType = Constants.VALUATION_MODE_LAST;

    public NetCalcModel() {
        dateformat = new TWDateFormat("yyyyMMdd");
    }

    /**
     * Constructor
     */
    public static NetCalcModel getSharedInstance() {
        if (self == null) {
            self = new NetCalcModel();
        }
        return self;
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

//    public void setValuationType(byte type){
//        valuationType = type;
//    }
//
//    public byte getValuationType(){
//        return valuationType;
//    }

    public int getRowCount() {
        return NetCalcStore.size() + 1;
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int row, int col) {
        try {
            if (row < (getRowCount() - 1)) { // only transaction rows
                ChangeRecord record = NetCalcStore.getRecord(row);
                if (record == null)
                    return "";
                Stock stock = DataStore.getSharedInstance().getStockObject(record.getSymbol());
                if (stock == null)
                    return "";

                switch (col) {
                    case 0:
                        return stock.getSymbolCode();
                    case 1:
                        return stock.getShortDescription();
                    case 2:
                        if (record.getFromDate() == 0)
                            return new Long(System.currentTimeMillis());
                        else
                            return new Long(record.getFromDate());
                    case 3:
                        return record.getStartPrice() + "";
                    case 4:
                        if (record.getToDate() == 0)
                            return new Long(System.currentTimeMillis());
                        else
                            return new Long(record.getToDate());
                    case 5:
                        return record.getEndPrice() + "";
                    case 6:
                        return record.getNetChange() + "";
                    case 7:
                        return record.getHigh() + "";
                    case 8:
                        return record.getLow() + "";
                    case 9:
//                        return stock.getExchange();
                        return ExchangeStore.getSharedInstance().getExchange(stock.getExchange().trim()).getDisplayExchange(); // To display exchange

                    case 10:
                        try {
//                            if(record.getValuationType()==0){
//                                return getvaluationString(valuationType);
//                            }else{
                            return getvaluationString(record.getValuationType());
//                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                            return getvaluationString(valuationType);
                            return "";
                        }
                    default:
                        return "";
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public String getvaluationString(byte type) {
        switch (type) {
            case Constants.VALUATION_MODE_LAST:
                return Language.getString("LAST");
            case Constants.VALUATION_MODE_VWAP:
                return Language.getString("VWAP");
            default:
                return Language.getString("LAST");
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        try {
            return getValueAt(0, iCol).getClass();
        } catch (Exception e) {
            return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        if ((row == (getRowCount() - 1)) && (col == 0)) {
            return true;
        } else if ((row < (getRowCount() - 1)) && ((col == 2) || (col == 4))) {
            return true;
        }
        return false;
    }

    public void setValueAt(Object value, int rowIndex, int column) {
        if (column == 0) {
            if (value == null) return;
//            String sNewSymbol = ((String) value).toUpperCase();
            String sNewSymbol = ((String) value);
            String exchange;
            String symbol;
            String key = "";
            int instrumentType;

            if ((sNewSymbol != null) && (!sNewSymbol.trim().equals(""))) {
                exchange = SharedMethods.getExchangeFromKey(sNewSymbol); // break the exchange from key
                symbol = SharedMethods.getSymbolFromKey(sNewSymbol); // remove the symbol part from key
                instrumentType = SharedMethods.getInstrumentTypeFromKey(sNewSymbol); // remove the symbol part from key
                if (symbol == null) {
                    symbol = exchange;       // may be the symbol only was typed. no exchange. hence symbol value is in exchange var
                    key = SymbolMaster.getExchangeForSymbol(symbol, false); // find the exchange for this symbol
                    exchange = SharedMethods.getExchangeFromKey(key); // break the exchange from key
                    symbol = SharedMethods.getSymbolFromKey(key);
                    instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
                }
                if (key.equals("-1")) {  // if user canceled mutiple symbol selection window
                    return;
                }
                if (exchange == null) { // bad symbol, not in local files. Must vslidate from the server
                    TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
                    new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");
//                    String requestID = "HistoryAnalyzer:" + System.currentTimeMillis();
//                    SendQFactory.addValidateRequest(sNewSymbol, requestID, null);
//                    SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);

                } else {
                    sNewSymbol = SharedMethods.getKey(exchange, symbol, instrumentType);
                    setNewRecord(sNewSymbol);
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "HistoryAnalyser-Add");
                }
//                if (DataStore.getSharedInstance().isValidSymbol(sNewSymbol)) {
//                    String key = DataStore.getSharedInstance().searchSymbol(sNewSymbol);
//                    setNewRecord(key);
//                } else {
//                    TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
//                    new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");
//                }
            }

        } else if (column == 2) {
            try {
                if (!isEmptyRow(rowIndex)) {
                    ChangeRecord record = NetCalcStore.getRecord(rowIndex);
                    if (record != null) {
                        long marketDate = ExchangeStore.getSharedInstance().getExchange(
                                SharedMethods.getExchangeFromKey(record.getSymbol())).getMarketDate();  //todo:changed
                        if (record.getToDate() < toLongValue(value)) {
                            record.setFromDate(record.getToDate());
                        } else if (toLongValue(value) > marketDate) {
                            record.setFromDate(marketDate);
                        } else {
                            record.setFromDate(toLongValue(value));
                        }

                        if (SharedMethods.isCurrentDay(SharedMethods.getExchangeFromKey(record.getSymbol()), record.getFromDate())) {
                            if (SharedMethods.isCurrentDay(SharedMethods.getExchangeFromKey(record.getSymbol()), record.getToDate())) {
                                getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
                                        dateformat.format(new Date(record.getToDate())), BOTH_CURRENT_DATES);
                            } else {
                                getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
                                        dateformat.format(new Date(record.getToDate())), CURRENT_FROM_DATE);
                            }
                        } else {
                            if (SharedMethods.isCurrentDay(SharedMethods.getExchangeFromKey(record.getSymbol()), record.getToDate())) {
                                getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
                                        dateformat.format(new Date(record.getToDate())), CURRENT_TO_DATE);
                            } else {
                                getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
                                        dateformat.format(new Date(record.getToDate())), BOTH_HISTORY_DATES);
                            }
                        }
//                        getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
//                                dateformat.format(new Date(record.getToDate())));
                        doCalculateNetChange();
                        getTable().updateUI();
                    }
                    record = null;
                    getTable().updateUI();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (column == 4) {
            try {
                if (!isEmptyRow(rowIndex)) {
                    ChangeRecord record = NetCalcStore.getRecord(rowIndex);
                    if (record != null) {
                        long marketDate = ExchangeStore.getSharedInstance().getExchange(
                                SharedMethods.getExchangeFromKey(record.getSymbol())).getMarketDate(); //todo:changed
                        if (record.getFromDate() > toLongValue(value)) {
                            record.setToDate(record.getFromDate());
                        } else if (toLongValue(value) > marketDate) {
                            record.setToDate(marketDate);
                        } else {
                            record.setToDate(toLongValue(value));
                        }

                        if (SharedMethods.isCurrentDay(SharedMethods.getExchangeFromKey(record.getSymbol()), record.getToDate())) {
                            if (SharedMethods.isCurrentDay(SharedMethods.getExchangeFromKey(record.getSymbol()), record.getFromDate())) {
                                getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
                                        dateformat.format(new Date(record.getToDate())), BOTH_CURRENT_DATES);
                            } else {
                                getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
                                        dateformat.format(new Date(record.getToDate())), CURRENT_TO_DATE);
                            }
                        } else {
                            if (SharedMethods.isCurrentDay(SharedMethods.getExchangeFromKey(record.getSymbol()), record.getFromDate())) {
                                getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
                                        dateformat.format(new Date(record.getToDate())), CURRENT_FROM_DATE);
                            } else {
                                getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
                                        dateformat.format(new Date(record.getToDate())), BOTH_HISTORY_DATES);
                            }
                        }
//                        getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(record.getFromDate())),
//                                dateformat.format(new Date(record.getToDate())));
                        doCalculateNetChange();
                        getTable().updateUI();
                    }
                    record = null;
                    getTable().updateUI();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void removeRecord(int index) {
        NetCalcStore.removeRecord(index);
    }

    public void tableUpdating() {
        doCalculateNetChange();
    }

    private void doCalculateNetChange() {
        ChangeRecord record = null;

        for (int i = 0; i < NetCalcStore.size(); i++) {
            record = NetCalcStore.getRecord(i);

            if ((record.getEndPrice() == 0) || (record.getStartPrice() == 0))
                record.setNetChange(0);
            else
                record.setNetChange(record.getEndPrice() - record.getStartPrice());

            record = null;
        }
    }

    public void setNewRecord(String symbol) {
        Stock stock = null;
        try {
            stock = DataStore.getSharedInstance().getStockObject(symbol.toUpperCase()); // need to be Upper case to get From Symbol store
            ChangeRecord record = new ChangeRecord();
            record.setSymbol(symbol.toUpperCase());
            record.setStartPrice(stock.getPreviousClosed());
            record.setEndPrice(stock.getLastTradeValue());
            record.setHigh(stock.getHigh());
            record.setLow(stock.getLow());
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(symbol));
            try {
                record.setFromDate(exchange.getMarketDate()); //todo:changed
                record.setToDate(exchange.getMarketDate()); //todo:changed
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                record.setFromDate(System.currentTimeMillis());
                record.setToDate(System.currentTimeMillis());
            }
            record.setValuationType(Constants.VALUATION_MODE_LAST);
            NetCalcStore.addRecord(record);
            stock = null;
            record = null;
            getTable().updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setStartPrice(String symbol, float startPrice) {
        ChangeRecord record = null;
        double netChange = 0; //endPrice - startPrice;
        for (int i = 0; i < NetCalcStore.size(); i++) {
            record = NetCalcStore.getRecord(i);
            if (record.getSymbol().equals(symbol)) {
                break;
            }
            record = null;
        }
        if (startPrice == 0) {
            record.setStartPrice(0);
            record.setNetChange(0);
        } else {
            netChange = record.getEndPrice() - startPrice;
            record.setStartPrice(startPrice);
            record.setNetChange(netChange);
        }
        record = null;
    }

    public void setEndPrice(String symbol, float endPrice) {
        ChangeRecord record = null;
        double netChange = 0; //endPrice - startPrice;
        for (int i = 0; i < NetCalcStore.size(); i++) {
            record = NetCalcStore.getRecord(i);
            if (record.getSymbol().equals(symbol)) {
                break;
            }
            record = null;
        }
        if (endPrice == 0) {
            record.setStartPrice(0);
            record.setNetChange(0);
        } else {
            netChange = endPrice - record.getStartPrice();
            record.setEndPrice(endPrice);
            record.setNetChange(netChange);
        }
        record = null;
    }

    public String toString() {
        String tempString = "";
        ChangeRecord record = null;

        for (int i = 0; i < NetCalcStore.size(); i++) {
            record = NetCalcStore.getRecord(i);
            tempString += "," + record.toString();
            record = null;
        }
        if (tempString.length() > 0)
            return tempString.substring(1);
        else
            return "";
    }

    public void resetValues(ChangeRecord record) {
        byte dateType = BOTH_HISTORY_DATES;
        long from = record.getFromDate();
        long to = record.getToDate();
        if ((from == to) && (dateformat.format(new Date(from)).compareTo(dateformat.format(new Date(ExchangeStore.getSharedInstance().getExchange(
                SharedMethods.getExchangeFromKey(record.getSymbol())).getMarketDate()))) == 0)) {   //todo:changed
            dateType = BOTH_CURRENT_DATES;
        } else if (dateformat.format(new Date(from)).compareTo(dateformat.format(new Date(ExchangeStore.getSharedInstance().getExchange(
                SharedMethods.getExchangeFromKey(record.getSymbol())).getMarketDate()))) == 0) {  //todo:changed
            dateType = CURRENT_FROM_DATE;
        } else if (dateformat.format(new Date(to)).compareTo(dateformat.format(new Date(ExchangeStore.getSharedInstance().getExchange(
                SharedMethods.getExchangeFromKey(record.getSymbol())).getMarketDate()))) == 0) {   //todo:changed
            dateType = CURRENT_TO_DATE;
        }

        getHistoryValues(record, record.getSymbol(), dateformat.format(new Date(from)), dateformat.format(new Date(to)), dateType);
    }

    private String getCurrentValues(ChangeRecord element, String key) {
        Stock stock = DataStore.getSharedInstance().getStockObject(key);
        String str = "";
        str = System.currentTimeMillis() + Meta.FS + stock.getTodaysOpen() + Meta.FS + stock.getHigh() + Meta.FS + stock.getLow()
                + Meta.FS + stock.getLastTradeValue() + Meta.FS + stock.getAvgTradePrice();
/*        str = System.currentTimeMillis() + Meta.FS + stock.getPreviousClosed() + Meta.FS + stock.getHigh() + Meta.FS + stock.getLow()
                + Meta.FS +stock.getLastTradeValue() + Meta.FS + stock.getAvgTradePrice();*/

        return str;
    }

    private void getHistoryValues(ChangeRecord element, String key, String date1, String date2, byte dateType) {

        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = SharedMethods.getExchangeFromKey(key);
        TWDateFormat formatter = new TWDateFormat("yyyyMMdd");
//        Stock stock = DataStore.getSharedInstance().getStockObject(key);
        Vector vec = null;
        try {
            if (dateType == BOTH_CURRENT_DATES) { //-- Currnt date values getting from stock
                vec = new Vector();
                vec.add(getCurrentValues(element, key));
                String date;
                float high = 0;
                float low = Float.MAX_VALUE;
                float currentHigh = 0;
                float currentLow = Float.MAX_VALUE;
                float price = 0;
                if (element.getValuationType() == Constants.VALUATION_MODE_VWAP) {
                    for (int i = 0; i < vec.size(); i++) {
                        String record = (String) vec.elementAt(i);
                        StringTokenizer fields = new StringTokenizer(record, Meta.FS);
                        date = fields.nextToken(); //date
                        price = SharedMethods.floatValue(fields.nextToken()); // open
                        try {
                            currentHigh = toFloatValue(fields.nextToken()); // high
                            currentLow = toFloatValue(fields.nextToken()); // low

                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
//                    }
                        fields.nextToken(); //close
                        try {
//                        price = SharedMethods.floatValue(fields.nextToken()); // VWAP
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (i == 0) {
                            element.setStartPrice(price);
                        }
                        if (i == (vec.size() - 1)) {
                            price = SharedMethods.floatValue(fields.nextToken()); // VWAP
                            element.setEndPrice(price);
                        }
                        if (currentHigh > high)
                            high = currentHigh;
                        if (currentLow < low)
                            low = currentLow;
                    }

                    doCalculateNetChange();
                    if (low == Float.MAX_VALUE) low = 0;
                    element.setHigh(high);
                    element.setLow(low);
                    vec.clear();
                    vec.trimToSize();
                    vec = null;
                } else {
                    for (int i = 0; i < vec.size(); i++) {
                        String record = (String) vec.elementAt(i);
                        StringTokenizer fields = new StringTokenizer(record, Meta.FS);
                        date = fields.nextToken(); //date
//                    fields.nextToken(); //open
                        price = SharedMethods.floatValue(fields.nextToken()); // open
                        try {
                            currentHigh = toFloatValue(fields.nextToken()); // high
                            currentLow = toFloatValue(fields.nextToken()); // low
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
//                    }
                        try {
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (i == 0) {
                            element.setStartPrice(price);
                        }
                        if (i == (vec.size() - 1)) {
                            price = SharedMethods.floatValue(fields.nextToken()); // close
                            element.setEndPrice(price);
                        }
                        if (currentHigh > high)
                            high = currentHigh;
                        if (currentLow < low)
                            low = currentLow;
                    }

                    doCalculateNetChange();
                    if (low == Float.MAX_VALUE) low = 0;
                    element.setHigh(high);
                    element.setLow(low);
                    vec.clear();
                    vec.trimToSize();
                    vec = null;
                }
            } else {  /*--When current date condition is chaneged -data gather from both history n stock.. New changes are done according to the
                           new history files. Refer OHLCStore for details.. -- Shanika ---2009.07.09 -  */
                try {
                    Date fromDate = formatter.parse(date1);
                    Date toDate = formatter.parse(date2);
                    if (fromDate.compareTo(toDate) == 0) {
                        vec = HistoryFilesManager.readHistoryDataForDate(symbol, exchange, date1);
                    } else {
                        vec = HistoryFilesManager.readHistoryData(symbol, exchange, date1, date2);
                    }

                    if (HistoryFilesManager.getMinimumDate() != null && (fromDate.compareTo(HistoryFilesManager.getMinimumDate()) < 0)) {
                        String msg = Language.getString("DATA_UNAVAILABLE");
                        msg = msg.replaceAll("\\[DATE\\]", new TWDateFormat("dd/MM/yyyy").format(HistoryFilesManager.getMinimumDate()));
                        new ShowMessage(false, msg, "W");
                        element.setFromDate(HistoryFilesManager.getMinimumDate().getTime());
                    }

                } catch (ParseException e) {
                    vec = HistoryFilesManager.readHistoryData(symbol, exchange, date1, date2);
                }
                if (dateType == CURRENT_FROM_DATE) {
                    vec.add(0, getCurrentValues(element, key));
                } else if (dateType == CURRENT_TO_DATE) {
                    vec.add(getCurrentValues(element, key));
                }
                String date;
                float high = 0;
                float low = Float.MAX_VALUE;
                float currentHigh = 0;
                float currentLow = Float.MAX_VALUE;
                float price = 0;

                if (vec.size() == 0) {
                    element.setEndPrice(0);
                    element.setStartPrice(0);
                } else {
                    for (int i = 0; i < vec.size(); i++) {

                        String record = (String) vec.elementAt(i);
                        String[] tokens = new String[0];
                        if (vec.size() - 1 != 0 && i == 0) {
                            tokens = record.split(Meta.FS, 13);
                        } else {
                            tokens = record.split(Meta.FS, 6);
                        }
                        date = tokens[0]; // date
//                    open = SharedMethods.floatValue(tokens[1]);     // open
                        high = SharedMethods.floatValue(tokens[2]);     // high
                        low = SharedMethods.floatValue(tokens[3]);      // low
                        if (element.getValuationType() == Constants.VALUATION_MODE_VWAP) {
                            try {
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (i == 0) {
                                if (dateType == CURRENT_FROM_DATE) {
                                    price = SharedMethods.floatValue(tokens[1]); // open current
                                } else {
                                    price = SharedMethods.floatValue(tokens[7]); // open history
                                }
                                element.setStartPrice(price);
                            }
                            if (i == (vec.size() - 1)) {
                                if (dateType == CURRENT_TO_DATE) {
                                    price = SharedMethods.floatValue(tokens[5]); // VWAP
                                } else {
                                    price = SharedMethods.floatValue(tokens[12]); // --VWAP -history
                                }
                                element.setEndPrice(price);
                            }
                        } else {
                            if (i == 0) {
                                if (dateType == CURRENT_FROM_DATE) {
                                    price = SharedMethods.floatValue(tokens[1]); // VWAP
                                } else {
                                    price = SharedMethods.floatValue(tokens[7]); // VWAP
                                }
                                element.setStartPrice(price);
                            }
                            if (i == (vec.size() - 1)) {
                                if (dateType == CURRENT_TO_DATE) {
                                    price = SharedMethods.floatValue(tokens[4]);
                                } else {
                                    price = SharedMethods.floatValue(tokens[4]); // VWAP
                                }
                                element.setEndPrice(price);
                            }
                        }
                        if (currentHigh > high)
                            high = currentHigh;
                        if (currentLow < low)
                            low = currentLow;
                    }
                    doCalculateNetChange();
                    if (low == Float.MAX_VALUE) low = 0;
                    element.setHigh(high);
                    element.setLow(low);
                    vec.clear();
                    vec.trimToSize();
                    vec = null;
                }
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
            element.setStartPrice(0.00f);
            element.setEndPrice(0.00f);
            element.setHigh(0.00f);
            element.setLow(0.00f);
            element.setNetChange(0.00f);
            //change end
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getSymbol(int row) {
        return (String) getValueAt(row, 0);
    }

    public boolean isEmptyRow(int row) {
        if (getSymbol(row).equals("")) {
            return true;
        }
        return false;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private float toFloatValue(Object oValue) throws Exception {
        return Float.parseFloat((String) oValue);
    }
}

