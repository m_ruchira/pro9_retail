package com.isi.csvr.netcalculator;

import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

public class CalcCellEditor extends DefaultCellEditor implements ActionListener, DateSelectedListener, Themeable {

    private JComboBox comboBox;
    private boolean clearValue = false;
    private long selectedDate = 0;
    private DatePicker dp;
    private TWDateFormat formatter;

    public CalcCellEditor(JFrame parent, JComboBox cb) {
        super(cb);
        comboBox = cb;
        comboBox.addActionListener(this);

        formatter = new TWDateFormat("yyyyMMdd");

        dp = new DatePicker(parent, true);
        dp.getCalendar().addDateSelectedListener(this);
        Theme.registerComponent(this);
    }

    public Object getCellEditorValue() {
        return selectedDate + "";
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {

        selectedDate = ((Long) table.getModel().getValueAt(row, table.convertColumnIndexToModel(column)));
        //prevDate = selectedDate;
        if (clearValue) {
            comboBox.setSelectedIndex(0);
        } else {
            comboBox.setSelectedItem(value);
            if (comboBox.getSelectedIndex() == 1) {
                comboBox.setSelectedIndex(-1);
            }
        }

        return comboBox;
    }

    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent == null) {
            clearValue = true;
        } else {
            clearValue = false;
        }
        return super.isCellEditable(anEvent);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == comboBox) {
            if (comboBox.getSelectedIndex() == 1) {
                Point location = new Point(0, 0);
                SwingUtilities.convertPointToScreen(location, comboBox);
                dp.setLocation((int) location.getX(), (int) (location.getY()) + comboBox.getBounds().height);
                dp.setVisible(true);
            } else
                //if (selectedDate <= 0)
                selectedDate = System.currentTimeMillis();
        }
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        if (source == dp.getCalendar()) {
            try {
                selectedDate = (iYear * 10000) + ((iMonth + 1) * 100) + iDay;
                selectedDate = (formatter.parse(selectedDate + "")).getTime();

                dp.setVisible(false);
            } catch (Exception e) {
            }
        }
    }

    public void applyTheme() {
        dp.applyTheme();
    }
}