package com.isi.csvr.sideBar;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 26-Mar-2008 Time: 14:06:51 To change this template use File | Settings
 * | File Templates.
 */
public class SymbolTreeObject implements Comparable {
    public int type = 0;
    public String exchange;
    public String symbol;
    public String market = null;
    public String shortDescription;
    public String description;
    public boolean isMatched = false;
    public double openVal = 0.00;
    public double closeVal = 0.00;
    public double highVal = 0.00;
    public double lowVal = 0.00;
    public String bestBid = "";
    public String bestOffer = "";

    public SymbolTreeObject() {

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public boolean isMatched() {
        return isMatched;
    }

    public void setMatched(boolean matched) {
        isMatched = matched;
    }


    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public int compareTo(Object o) {
        return getDescription().compareTo(((SymbolTreeObject) o).getDescription());
    }

    public double getOpenVal() {
        return openVal;
    }

    public void setOpenVal(double openVal) {
        this.openVal = openVal;
    }

    public double getCloseVal() {
        return closeVal;
    }

    public void setCloseVal(double closVal) {
        this.closeVal = closeVal;
    }

    public double getHighVal() {
        return highVal;
    }

    public void setHighVal(double highVal) {
        this.highVal = highVal;
    }

    public double getLowVal() {
        return lowVal;
    }

    public void setLowVal(double lowVal) {
        this.lowVal = lowVal;
    }

    public String getBestBid() {
        return bestBid;
    }

    public void setBestBid(String bestBid) {
        this.bestBid = bestBid;
    }

    public String getBestOffer() {
        return bestOffer;
    }

    public void setBestOffer(String bestOffer) {
        this.bestOffer = bestOffer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
