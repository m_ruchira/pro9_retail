package com.isi.csvr.sideBar;

import com.isi.csvr.Client;
import com.isi.csvr.TWFrame;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA. User: Hasika Date: Jun 11, 2008 Time: 10:32:17 AM To change this template use File |
 * Settings | File Templates.
 */
public class SideBar extends JPanel implements Themeable, ComponentListener, ApplicationListener {
    private static SideBar self;
    private JPanel mainPanel;
    private JPanel centerPanel;
    private JButton clickButton;
    private JButton dragButton;
    private ViewSetting viewSettings;
    private JPanel leftSideBarPan;
    private JPanel leftTriggerMainPan;
    private String selectedKey;
    private ViewSetting oSetting;
    private TWFrame window;
    private Dimension oldSize;
    private int previousLength = 0;


    public SideBar() {
        mainPanel = new JPanel(new BorderLayout());
        centerPanel = new JPanel(new BorderLayout());
        clickButton = new JButton();
        dragButton = new JButton();
        oldSize = new Dimension(120, 1);
        Theme.registerComponent(this);
//        createSideBar();
    }

    public static SideBar getSharedInstance() {
        if (self == null) {
            self = new SideBar();
        }
        return self;
    }

    public boolean isNeedsToDisplay() {
        return viewSettings.isVisible();
    }

    public void getWindow() {
        window = Client.getInstance().getWindow();
    }

    public JPanel createSideBar() {
        getWindow();
        oSetting = ViewSettingsManager.getSummaryView("SIDE_BAR");
        oSetting.setParent(SymbolNavigator.getSharedInstance());
        final JButton leftTriggerBtn1 = new JButton();
        final JButton leftTriggerBtn2 = new JButton();
        leftSideBarPan = new JPanel(new BorderLayout());
        leftSideBarPan.add(SymbolNavigator.getSharedInstance(), BorderLayout.CENTER);
        leftTriggerBtn1.setBackground(Color.RED);
        leftTriggerBtn1.setPreferredSize(new Dimension(2, 1));
        leftTriggerBtn2.setPreferredSize(new Dimension(2, 1));
        leftTriggerMainPan = new JPanel(new BorderLayout());
        leftTriggerMainPan.setPreferredSize(new Dimension(2, 1));
        if (!Language.isLTR()) {
            leftTriggerMainPan.addComponentListener(this);
        }
        leftTriggerBtn2.addMouseMotionListener(new MouseMotionListener() {
            public void mouseDragged(MouseEvent e) {
                if (SymbolNavigator.getSharedInstance().isVisible()) {
                    Point point = SwingUtilities.convertPoint(leftTriggerBtn2, e.getX(), e.getY(), leftTriggerBtn1);
                    if (!Language.isLTR()) {
                        point = new Point(Math.abs(point.x), Math.abs(point.y));
                    }
                    if (point.x < 120) {
                        leftTriggerMainPan.setPreferredSize(new Dimension(120, 1));
                        leftTriggerMainPan.doLayout();
                        leftTriggerMainPan.updateUI();
                        leftSideBarPan.doLayout();
                        leftSideBarPan.updateUI();
                        window.getContentPane().doLayout();
                    } else {
                        leftTriggerMainPan.setPreferredSize(new Dimension(point.x, 1));
                        leftTriggerMainPan.doLayout();
                        leftTriggerMainPan.updateUI();
                        leftSideBarPan.doLayout();
                        leftSideBarPan.updateUI();
                        window.getContentPane().doLayout();
                    }
                }
            }

            public void mouseMoved(MouseEvent e) {
                leftTriggerBtn2.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
                leftTriggerBtn2.setBackground(Color.GRAY);
            }
        });
        leftTriggerMainPan.setBorder(BorderFactory.createEmptyBorder());
        leftTriggerBtn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controlSideBar();
            }
        });
        if (Language.isLTR()) {
            leftTriggerMainPan.add(leftTriggerBtn1, BorderLayout.WEST);
            leftTriggerMainPan.add(leftSideBarPan, BorderLayout.CENTER);
            leftTriggerMainPan.add(leftTriggerBtn2, BorderLayout.EAST);
            window.getContentPane().add(leftTriggerMainPan, BorderLayout.WEST);
        } else {
            leftTriggerMainPan.add(leftTriggerBtn1, BorderLayout.EAST);
            leftTriggerMainPan.add(leftSideBarPan, BorderLayout.CENTER);
            leftTriggerMainPan.add(leftTriggerBtn2, BorderLayout.WEST);
            window.getContentPane().add(leftTriggerMainPan, BorderLayout.EAST);
        }

        if (!Language.isLTR()) {
            Application.getInstance().addApplicationListener(this);
        }
        return leftTriggerMainPan;
    }

    public void closeSideBar() {
        oldSize = leftTriggerMainPan.getPreferredSize();
        leftTriggerMainPan.setPreferredSize(new Dimension(2, 1));
        SymbolNavigator.getSharedInstance().setVisible(false);
        window.getContentPane().doLayout();
        oSetting.setSize(oldSize);
        oSetting.putProperty(99, oldSize.width);
    }

    public String getSelectedKey() {
        return selectedKey;
    }

    public void setSideBar() {
        leftTriggerMainPan.setPreferredSize(new Dimension(SymbolNavigator.getSharedInstance().getSavedSize().width + 4, SymbolNavigator.getSharedInstance().getSavedSize().height));
        SymbolNavigator.getSharedInstance().applySettings();
        leftTriggerMainPan.doLayout();
        leftTriggerMainPan.updateUI();
        leftSideBarPan.doLayout();
        leftSideBarPan.updateUI();
        window.getContentPane().doLayout();
    }

    public void controlSideBar() {
        if (SymbolNavigator.getSharedInstance().isVisible() && leftTriggerMainPan.getPreferredSize().width >= 120) {
            closeSideBar();
        } else {
            if (oSetting.getSize().width < 120) {
                if (oldSize.width <= 120) {
                    int width = 4;
                    if ((oSetting.getProperty(99)) != null && !(oSetting.getProperty(99)).equalsIgnoreCase("null")) {
                        width = width + Integer.parseInt(oSetting.getProperty(99));
                    } else {
                        width = width + 120;
                    }
                    leftTriggerMainPan.setPreferredSize(new Dimension(width, 1));
                } else
                    leftTriggerMainPan.setPreferredSize(oldSize);
            } else {
                leftTriggerMainPan.setPreferredSize(oSetting.getSize());
            }

            SymbolNavigator.getSharedInstance().setVisible(true);
            SymbolNavigator.getSharedInstance().updateTree();
            leftTriggerMainPan.doLayout();
            leftTriggerMainPan.updateUI();
            leftSideBarPan.doLayout();
            leftSideBarPan.updateUI();
            window.getContentPane().doLayout();

        }
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void componentResized(ComponentEvent e) {

        if (Application.isReady()) {
            JInternalFrame frames[] = Client.getInstance().getDesktop().getAllFrames();
            int width = e.getComponent().getPreferredSize().width;
            int gap = previousLength - width;
            previousLength = width;
            if (frames != null && frames.length > 0 && Application.isReady()) {
                for (int i = 0; i < frames.length; i++) {
                    JInternalFrame frame = frames[i];
                    if (frame.isVisible()) {
                        frame.setLocation(((int) frame.getLocation().getX()) + gap, ((int) frame.getLocation().getY()));
                    }

                }

            }
        }


    }

    @Override
    public void updateUI() {
        try {
            SymbolNavigator.getSharedInstance().updateTree();
            super.updateUI();
        } catch (Exception e) {
            //To change body of overridden methods use File | Settings | File Templates.
        }
    }

    public void componentMoved(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void componentShown(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void componentHidden(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        JInternalFrame frames[] = Client.getInstance().getDesktop().getAllFrames();
        int width = leftTriggerMainPan.getPreferredSize().width;
        int gap = previousLength - width;
        previousLength = width;
        if (frames != null && frames.length > 0) {
            for (int i = 0; i < frames.length; i++) {
                JInternalFrame frame = frames[i];
                int xpos = frame.getLocation().x;
                int framewidth = frame.getWidth();
                int scrWidth = window.getWidth();
                if (frame.isVisible() && ((xpos + framewidth + previousLength) > scrWidth)) {
                    frame.setLocation(((int) frame.getLocation().getX()) + gap, ((int) frame.getLocation().getY()));
                }

            }

        }
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {

    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
