package com.isi.csvr.sideBar;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 22-Apr-2008 Time: 14:19:47 To change this template use File | Settings
 * | File Templates.
 */

import com.isi.csvr.shared.SharedMethods;

import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 21-Apr-2008 Time: 13:56:14 To change this template use File |
 * Settings | File Templates.
 */
class TransferableTreeNode extends DefaultMutableTreeNode implements
        Transferable {
    final public static DataFlavor DEFAULT_MUTABLE_TREENODE_FLAVOR = new DataFlavor(
            DefaultMutableTreeNode.class, "Default Mutable Tree Node");
    static DataFlavor flavors[] = {DEFAULT_MUTABLE_TREENODE_FLAVOR,
            DataFlavor.stringFlavor, DataFlavor.plainTextFlavor};
    final static int TREE = 0;
    final static int STRING = 1;
    final static int PLAIN_TEXT = 1;
    private SymbolNode data;

    public TransferableTreeNode(SymbolNode data) {
        this.data = data;
    }

    public DataFlavor[] getTransferDataFlavors() {
        return flavors;
    }

    public Object getTransferData(DataFlavor flavor)
            throws UnsupportedFlavorException, IOException {
        Object returnObject;
        if (flavor.equals(flavors[TREE])) {
            Object userObject = data;
            if (userObject == null) {
                returnObject = data;
            } else {
                returnObject = userObject;
            }
        } else if (flavor.equals(flavors[STRING])) {
            Object userObject = data;
            if (userObject == null) {
                returnObject = data.toString();
            } else {
                returnObject = SharedMethods.getKey(((SymbolNode) userObject).getExchange(), ((SymbolNode) userObject).getSymbol(), ((SymbolNode) userObject).getInstrumentType());
            }
        } else if (flavor.equals(flavors[PLAIN_TEXT])) {
            Object userObject = data;
//            Object userObject = data.getUserObject();
            String string;
            if (userObject == null) {
                string = data.toString();
            } else {
                string = userObject.toString();
            }
            returnObject = new ByteArrayInputStream(string.getBytes("Unicode"));
        } else {
            throw new UnsupportedFlavorException(flavor);
        }
        return returnObject;
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) {
        boolean returnValue = false;
        for (int i = 0, n = flavors.length; i < n; i++) {
            if (flavor.equals(flavors[i])) {
                returnValue = true;
                break;
            }
        }
        return returnValue;
    }
}
