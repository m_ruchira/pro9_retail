package com.isi.csvr.sideBar;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;

import java.util.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Apr-2008 Time: 12:07:19 To change this template use File | Settings
 * | File Templates.
 */
public class ExchangeNode {
    private String name;
    private int marketStatus;
    private String status = null;
    private String exchangeSymbol;
    private Vector symbolStore;

    public ExchangeNode(String name, int marketStatus, String exgSymbol) {
        this.name = name;
        this.marketStatus = marketStatus;
        this.exchangeSymbol = exgSymbol;
        symbolStore = new Vector();
        setMarketStatus(marketStatus);
    }

    public ExchangeNode() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMarketStatusType() {
        return marketStatus;
    }

    public void setMarketStatusType(int marketStatus) {
        this.marketStatus = marketStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExchangeSymbol() {
        return exchangeSymbol;
    }

    public void setExchangeSymbol(String exchangeSymbol) {
        this.exchangeSymbol = exchangeSymbol;
    }

    public Vector getSymbolStore() {
        return symbolStore;
    }

    public void setChildrenStore(Hashtable store) {
        try {
            Enumeration symbols = store.elements();
            while (symbols.hasMoreElements()) {
                symbolStore.add((SymbolNode) symbols.nextElement());
            }
            Collections.sort(symbolStore, new LeafCoparator());
        } catch (Exception e) {
        }
    }

    public void addNode(ExchangeNode node) {
        try {
            symbolStore.add(node);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void removeNode(SymbolNode oNode) {
        try {
            for (int i = 0; i < symbolStore.size(); i++) {
                if (((SymbolNode) symbolStore.get(i)).getSymbol().equals(oNode.getSymbol())) {
                    symbolStore.remove(i);
                    break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void addNode(SymbolNode oNode) {
        try {
            boolean include = false;
            for (int i = 0; i < symbolStore.size(); i++) {
                if (((SymbolNode) symbolStore.get(i)).getSymbol().equals(oNode.getSymbol())) {
                    include = true;
                }

            }
            if (!include) {
                symbolStore.add(oNode);
                include = false;
            }
        } catch (Exception e) {
            e.printStackTrace(); //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void clear() {
        symbolStore.clear();
    }

    public Object getChild(int index) {
        return symbolStore.get(index);
    }

    public int getChildCount() {
        try {
            return symbolStore.size();
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isLeaf(Object object) {
        try {
            if (symbolStore.contains((ExchangeNode) object)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public int getIndex(Object object) {
        return symbolStore.indexOf((ExchangeNode) object);
    }

    public String toString() {
        return name;
    }

    public void setMarketStatus(int status) {
        this.marketStatus = status;
        if (status == Meta.MARKET_OPEN) {
            this.status = Language.getString("STATUS_OPEN");
        } else if (status == Meta.MARKET_CLOSE) {
            this.status = Language.getString("STATUS_CLOSE");
        } else if (status == Meta.MARKET_PREOPEN) {
            this.status = Language.getString("STATUS_PREOPEN");
        } else if (status == Meta.MARKET_PRECLOSE) {
            this.status = Language.getString("STATUS_PRECLOSE");
        } else {
            this.status = "";
        }
    }


    public boolean isExchangeType() {
        return true;
    }

    public int compareTo(Object o) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    class LeafCoparator implements Comparator {
        public int compare(Object o1, Object o2) {
            if (SideBarOptionDialog.isSymbolOnly() || SideBarOptionDialog.isSymbolShortOnly()) {
                return ((SymbolNode) o1).getSymbol().compareTo(((SymbolNode) o2).getSymbol());
            }
            if (SideBarOptionDialog.isShortOnly() || SideBarOptionDialog.isShorSymboltOnly()) {
                return ((SymbolNode) o1).getShortDescription().compareTo(((SymbolNode) o2).getShortDescription());
            }
            if (SideBarOptionDialog.isLongOnly() || SideBarOptionDialog.isLongSymbolOnly()) {
                return ((SymbolNode) o1).getDescription().compareTo(((SymbolNode) o2).getDescription());
            }
            return ((SymbolNode) o1).getShortDescription().compareTo(((SymbolNode) o2).getShortDescription());

        }
    }
}
