package com.isi.csvr.sideBar;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 27-Mar-2008 Time: 11:43:15 To change this template use File | Settings
 * | File Templates.
 */
public class NodeVector extends Vector implements Comparable {
    private String name;
    private int marketStatus;
    private String status = null;
    private String exchangeSymbol;

    /*
*public static final short MARKET_PREOPEN    = 1;
public static final short MARKET_OPEN       = 2;
public static final short MARKET_CLOSE      = 3;
public static final short MARKET_PRECLOSE   = 4;
* */
    public NodeVector(String name, int marketStatus, String exgSymbol) {
        this.name = name;
        this.marketStatus = marketStatus;
        this.exchangeSymbol = exgSymbol;
        setMarketStatus(marketStatus);
    }

    public NodeVector(String name, Hashtable hash, int marketStatus, String exgSymbol) {
        this.name = name;
        this.marketStatus = marketStatus;
        this.exchangeSymbol = exgSymbol;
        setMarketStatus(marketStatus);
        Enumeration en = hash.elements();
        while (en.hasMoreElements()) {
            add(en.nextElement());

        }
        Collections.sort(this);
    }

    public NodeVector(String name, Object elements[], int marketStatus) {
        try {
            this.name = name;
            this.marketStatus = marketStatus;
            setMarketStatus(marketStatus);
            Vector temp = new Vector();
            for (int i = 0; i < elements.length; i++) {
                if (((NodeVector) elements[i]).getMarketStatusType() == -1) {
                    temp.add(elements[i]);
                } else {
                    add(elements[i]);
                }
                Collections.sort(this);
            }
            Collections.sort(temp);
            for (int i = 0; i < temp.size(); i++) {
                add(temp.get(i));
            }
        } catch (Exception e) {

        }
    }

    public void setDataStore(Object elements[]) {
        removeAllElements();
        try {
            Vector temp = new Vector();
            for (int i = 0; i < elements.length; i++) {
                if (((NodeVector) elements[i]).getMarketStatusType() == -1) {
                    temp.add(elements[i]);
                } else {
                    add(elements[i]);
                }
                Collections.sort(this);
            }
            Collections.sort(temp);
            for (int i = 0; i < temp.size(); i++) {
                add(temp.get(i));
            }
        } catch (Exception e) {

        }
    }

    public String toString() {
        return name;//+"["+this.status+"]";
    }

    public int compareTo(Object o) {
        return toString().compareTo(o.toString());
    }

    public String getMarketStatus() {
        return this.status;
    }

    public void setMarketStatus(int status) {
        this.marketStatus = status;
        if (status == Meta.MARKET_OPEN) {
            this.status = Language.getString("STATUS_OPEN");
        } else if (status == Meta.MARKET_CLOSE) {
            this.status = Language.getString("STATUS_CLOSE");
        } else if (status == Meta.MARKET_PREOPEN) {
            this.status = Language.getString("STATUS_PREOPEN");
        } else if (status == Meta.MARKET_PRECLOSE) {
            this.status = Language.getString("STATUS_PRECLOSE");
        } else {
            this.status = "";
        }
    }

    public int getMarketStatusType() {
        return this.marketStatus;
    }

    public String getExchangeSymbol() {
        return exchangeSymbol;
    }

    public void setExchangeSymbol(String exchangeSymbol) {
        this.exchangeSymbol = exchangeSymbol;
    }
}
