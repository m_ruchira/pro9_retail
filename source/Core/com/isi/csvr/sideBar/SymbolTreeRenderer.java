package com.isi.csvr.sideBar;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 27-Mar-2008 Time: 14:30:29 To change this template use File | Settings
 * | File Templates.
 */
public class SymbolTreeRenderer implements TreeCellRenderer, Themeable {
    public static Color sideBarSymbolColor = Color.BLUE;
    public static RendererComponent nonLeafRenderer = new RendererComponent();
    //    private JLabel leafRenderer = new JLabel();
    //    private DefaultTreeCellRenderer nonLeafRenderer = new DefaultTreeCellRenderer();
    private static DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
    //    Color selectionBorderColor, selectionForeground, selectionBackground, textForeground, textBackground;
    private static ImageIcon g_oClosedIcon = null;
    private static ImageIcon g_oExpandedIcon = null;
    private static ImageIcon g_oLeafIcon_sub = null;
    //    private static ImageIcon g_oLeafIcon_unSub = null;
    private static Color backgroundSelectionColor;
    private static Color backgroundNonSelectionColor;
    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");

    public SymbolTreeRenderer() {

//        GUISettings.applyOrientation(nonLeafRenderer);
        /*Boolean booleanValue = (Boolean) UIManager.get("Tree.drawsFocusBorderAroundIcon");
        selectionBorderColor = UIManager.getColor("Tree.selectionBorderColor");
        selectionForeground = UIManager.getColor("Tree.selectionForeground");
        selectionBackground = UIManager.getColor("Tree.selectionBackground");
        textForeground = UIManager.getColor("Tree.textForeground");
        textBackground = UIManager.getColor("Tree.textBackground");*/
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
//            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
            backgroundSelectionColor = Theme.getColor("SIDEBAR_SELECTED_BGCOLOR");

        } catch (Exception e) {

        }
//        nonLeafRenderer.setOpaque(true);
        reload();
        Theme.registerComponent(this);
    }

    public static void reload() {
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
//            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
            backgroundSelectionColor = Theme.getColor("SIDEBAR_SELECTED_BGCOLOR");
            nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
            //newly added componets

        } catch (Exception e) {

        }
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean selected, boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        Component returnValue = null;

        try {
            if (((ExchangeNode) value) instanceof ExchangeNode) {
                ExchangeNode nv = (ExchangeNode) value;
                if (nv.isExchangeType()) {
                    if (expanded) {
                        try {
                            if (nv.getMarketStatusType() == Meta.MARKET_OPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_CLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PREOPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PREOPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PRECLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PRECLOSED_COLOR"));
                            } else {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            }
//                            nonLeafRenderer.setMode(0);
                            nonLeafRenderer.setLeftString(nv.toString());
                            nonLeafRenderer.setRightString(nv.getStatus());
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        nonLeafRenderer.setImage(g_oExpandedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    } else {
                        try {
                            if (nv.getMarketStatusType() == Meta.MARKET_OPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_CLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PREOPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PREOPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PRECLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PRECLOSED_COLOR"));
                            } else {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            }
//                            nonLeafRenderer.setMode(0);
                            nonLeafRenderer.setLeftString(nv.toString());
                            nonLeafRenderer.setRightString(nv.getStatus());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        nonLeafRenderer.setImage(g_oClosedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    }
//                    nonLeafRenderer.setSelectedColor(backgroundNonSelectionColor);
                    if (selected) {
                        nonLeafRenderer.setSelectedColor(backgroundSelectionColor);
                    } else {
                        nonLeafRenderer.setUnselected();
//                        nonLeafRenderer.setSelectedColor(backgroundNonSelectionColor);

                    }
                    returnValue = nonLeafRenderer;
                } else {
                    SymbolNode sto = (SymbolNode) value;
                    Stock st = (Stock) DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()));
                    nonLeafRenderer.setImage(g_oLeafIcon_sub);
                    nonLeafRenderer.setMode(1);
//                    nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_DESCR_COLOR"), Theme.getColor("SIDEBAR_SYMBOL_COLOR"));
                    nonLeafRenderer.setColor(0, sideBarSymbolColor, Theme.getColor("SIDEBAR_SYMBOL_COLOR"));

                    /*if (Language.isLTR()) {
                        nonLeafRenderer.setToolTipText("<HTML>" + "<div align=left>" + "" + sto.getShortDescription() + "\n" +
                                "<table>" +
                                "<tr><td>" + Language.getString("TODAYS_OPEN") + "</td><td width=10><td align='right'>" + oPriceFormat.format(st.getTodaysOpen()) + "</td></tr>" +
                                "<tr><td>" + Language.getString("TODAYS_CLOSE") + "</td><td width=10><td align='right'>" + oPriceFormat.format(st.getTodaysClose()) + "</td></tr>" +
                                "<tr><td>" + Language.getString("HIGH") + "</td><td width=10><td align='right'>" + oPriceFormat.format(st.getHigh()) + "</td></tr>" +
                                "<tr><td>" + Language.getString("LOW") + "</td><td width=10><td align='right'>" + oPriceFormat.format(st.getLow()) + "</td></tr></table>");
                    } else {
                        nonLeafRenderer.setToolTipText("<HTML>" + "<div align=right>" + "" + sto.getShortDescription() + "\n" +
                                "<table>" +
                                "<tr><td align='right'>" + oPriceFormat.format(st.getTodaysOpen()) + "</td><td width=10><td align='right'>" + Language.getString("TODAYS_OPEN") + "</td></tr>" +
                                "<tr><td align='right'>" + oPriceFormat.format(st.getTodaysClose()) + "</td><td width=10><td align='right'>" + Language.getString("TODAYS_CLOSE") + "</td></tr>" +
                                "<tr><td align='right'>" + oPriceFormat.format(st.getHigh()) + "</td><td width=10><td align='right'>" + Language.getString("HIGH") + "</td></tr>" +
                                "<tr><td align='right'>" + oPriceFormat.format(st.getLow()) + "</td><td width=10><td align='right'>" + Language.getString("LOW") + "</td></tr></table>");
                    }
                    System.out.println(nonLeafRenderer.getToolTipText());*/

                    if (selected) {
                        nonLeafRenderer.setSelectedColor(backgroundSelectionColor);
                        if (SideBarOptionDialog.isCheckisShortOnly()) {
                            nonLeafRenderer.setLeftString(sto.getShortDescription());
                            if (SideBarOptionDialog.isCheckisShorSymboltOnly()) {
                                nonLeafRenderer.setRightString(sto.getSymbol());
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else if (SideBarOptionDialog.isCheckisLongOnly()) {
                            nonLeafRenderer.setLeftString(sto.getDescription());
                            if (SideBarOptionDialog.isCheckisLongSymbolOnly()) {
                                nonLeafRenderer.setRightString(sto.getSymbol());
                                if (sto.getDescription().equals("")) {
                                    nonLeafRenderer.setLeftString(sto.getShortDescription());
                                }
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else if (SideBarOptionDialog.isCheckisSymbolOnly()) {
                            nonLeafRenderer.setLeftString(sto.getSymbol());

                            if (SideBarOptionDialog.isCheckisSymbolShortOnly()) {
                                nonLeafRenderer.setRightString(sto.getShortDescription());
                                if (sto.getDescription().equals("")) {
                                    nonLeafRenderer.setLeftString(sto.getShortDescription());
                                }
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
//                            nonLeafRenderer.setRightString("");
                        } else {
                            nonLeafRenderer.setLeftString(sto.getShortDescription());
                            nonLeafRenderer.setRightString(sto.getSymbol());
                        }
                    } else {
                        nonLeafRenderer.setUnselected();
                        if (SideBarOptionDialog.isCheckisShortOnly()) {
                            nonLeafRenderer.setLeftString(sto.getShortDescription());
                            if (SideBarOptionDialog.isCheckisShorSymboltOnly()) {
                                nonLeafRenderer.setRightString(sto.getSymbol());
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else if (SideBarOptionDialog.isCheckisLongOnly()) {
                            nonLeafRenderer.setLeftString(sto.getDescription());
                            if (sto.getDescription().equals("")) {
                                nonLeafRenderer.setLeftString(sto.getShortDescription());
                            }
                            if (SideBarOptionDialog.isCheckisLongSymbolOnly()) {
                                nonLeafRenderer.setRightString(sto.getSymbol());
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else if (SideBarOptionDialog.isCheckisSymbolOnly()) {
                            nonLeafRenderer.setLeftString(sto.getSymbol());

                            if (SideBarOptionDialog.isCheckisSymbolShortOnly()) {
                                nonLeafRenderer.setRightString(sto.getShortDescription());
                                if (sto.getDescription().equals("")) {
                                    nonLeafRenderer.setLeftString(sto.getShortDescription());
                                }
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else {
                            nonLeafRenderer.setLeftString(sto.getShortDescription());
                            nonLeafRenderer.setRightString(sto.getSymbol());
                        }
//                        nonLeafRenderer.setText("<HTML><font color=#" + Theme.getSideBarExchangeColor() + ">" + " " + sto.getDescription() + " - " + sto.getSymbol()+ "</font>");

                    }
                    returnValue = nonLeafRenderer;
                }

            } else {
                returnValue = new RendererComponent();
            }
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return new JLabel("");
        }
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
//            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
            backgroundSelectionColor = Theme.getColor("SIDEBAR_SELECTED_BGCOLOR");
            nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
            sideBarSymbolColor = Theme.getColor("SIDEBAR_DESCR_COLOR");

        } catch (Exception e) {

        }
    }
}
