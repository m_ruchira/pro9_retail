package com.isi.csvr.sideBar;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Apr-2008 Time: 19:05:48 To change this template use File | Settings
 * | File Templates.
 */
public class RendererComponent extends JPanel implements Themeable {
    private final static int EXG_MODE = 0;
    private final static int SYM_MODE = 1;
    //    private Font rendersFont = new Font("Helvetica", Font.BOLD, 14);
    public static int FONT_SIZE = 14;
    public static int FONT_STYLE = Font.BOLD;
    public static String FONT_TYPE = "Helvetica";
    private Font rendersFont = new Font(FONT_TYPE, FONT_STYLE, FONT_SIZE);
    private JLabel left;
    private JLabel middle;
    private JLabel right;


    RendererComponent() {
//        rendersFont.getFamily();
        this.setOpaque(false);
        left = new JLabel();
        middle = new JLabel();
        middle.setFont(rendersFont);
        right = new JLabel();
        right.setFont(rendersFont);
        left.setOpaque(false);
        middle.setOpaque(false);
        right.setOpaque(false);
        this.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 0));
        this.add(left);
        this.add(middle);
        this.add(right);
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
    }

    public void setMode(int mode) {
        if (mode == EXG_MODE) {
//           left.setForeground(Color.black);
            middle.setForeground(Color.black);
            right.setForeground(Color.black);
        } else {
//           left.setForeground(Color.blue);
            middle.setForeground(Color.blue);
            right.setForeground(Color.blue);
        }


    }

    public void setLeftString(String s1) {
        middle.setText(s1);
    }

    public void setRightString(String s2) {
        right.setText(s2);
    }

    public void setImage(ImageIcon ico) {
        left.setIcon(ico);
    }

    public void setColor(int mode, Color c1, Color c2) {
        if (mode == EXG_MODE) {
            middle.setForeground(c1);
            right.setForeground(c2);
        } else {
            middle.setForeground(c1);
            right.setForeground(c2);
        }
    }

    public void setSelectedColor(Color c3) {
        this.setOpaque(true);
        this.setBackground(c3);
    }

    public void setUnselected() {
        this.setOpaque(false);
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setFontSttings(Font cuztomizedFont) {
        rendersFont = cuztomizedFont;
        FONT_TYPE = cuztomizedFont.getFamily();
        FONT_SIZE = cuztomizedFont.getSize();
        FONT_STYLE = cuztomizedFont.getStyle();
        middle.setFont(cuztomizedFont);
        right.setFont(cuztomizedFont);
    }

    public Font getFontSetting() {
//       return middle.getFont();
        return rendersFont;
    }
}
