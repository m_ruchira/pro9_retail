package com.isi.csvr.sideBar;

import com.isi.csvr.shared.NonNavigatable;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.dnd.*;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: uditha Date: Apr 9, 2008 Time: 6:31:32 PM
 */
public class SideBarTree extends JTree implements NonNavigatable {
    public SideBarTree(TreeModel newModel) {        //
        super();
        DragSource dragSource = DragSource.getDefaultDragSource();
        dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY_OR_MOVE, new TreeDragGestureListener());
        setModel(newModel);
    }

    protected static TreeModel createTreeModel(Object value) {
        DefaultMutableTreeNode root;

        if ((value instanceof Object[]) || (value instanceof Hashtable) ||
                (value instanceof Vector)) {
            root = new DefaultMutableTreeNode("root");
            DynamicUtilTreeNode.createChildren(root, value);
        } else {
            root = new DynamicUtilTreeNode("root", value);
        }
        return new DefaultTreeModel(root, false);
    }

    public void setModel(Vector<?> value) {
        super.setModel(createTreeModel(value));    //To change body of overridden methods use File | Settings | File Templates.

    }

    public void paint(Graphics g) {
        if ((Theme.SIDEBAR_LIGHT_COLOR != null) && (Theme.SIDEBAR_DARK_COLOR != null)) {
            Rectangle rect = getVisibleRect();
            int width = getWidth();
            int height = (int) rect.getHeight();

            for (int i = 0; i < height; i++) {
                g.setColor(new Color(((Theme.SIDEBAR_LIGHT_COLOR.getRed() * i) + (Theme.SIDEBAR_DARK_COLOR.getRed() * (height - i))) / height,
                        ((Theme.SIDEBAR_LIGHT_COLOR.getRed() * i) + (Theme.SIDEBAR_DARK_COLOR.getGreen() * (height - i))) / height,
                        ((Theme.SIDEBAR_LIGHT_COLOR.getRed() * i) + (Theme.SIDEBAR_DARK_COLOR.getBlue() * (height - i))) / height));

                g.drawLine(rect.x, rect.y + i, width, rect.y + i);
            }
        }
        super.paint(g);
    }

    private static class TreeDragGestureListener implements DragGestureListener {
        public void dragGestureRecognized(DragGestureEvent dragGestureEvent) {
            // Can only drag leafs
            JTree tree = (JTree) dragGestureEvent.getComponent();
            TreePath path = tree.getSelectionPath();
            if (path == null) {
                // Nothing selected, nothing to drag
                System.out.println("Nothing selected - beep");
                tree.getToolkit().beep();
            } else {
//                DefaultMutableTreeNode selection = (DefaultMutableTreeNode) path.getLastPathComponent();
                try {
                    SymbolNode selection = (SymbolNode) path.getLastPathComponent();
                    if (selection.isLeaf()) {
                        TransferableTreeNode node = new TransferableTreeNode(selection);
                        dragGestureEvent.startDrag(DragSource.DefaultCopyDrop, node, new MyDragSourceListener());
                    } else {
                        System.out.println("Not a leaf - beep");
                        tree.getToolkit().beep();
                    }
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }

    private static class MyDragSourceListener implements DragSourceListener {
        public void dragDropEnd(DragSourceDropEvent dragSourceDropEvent) {
            if (dragSourceDropEvent.getDropSuccess()) {
                int dropAction = dragSourceDropEvent.getDropAction();
                if (dropAction == DnDConstants.ACTION_MOVE) {
                    System.out.println("MOVE: remove node");
                }
            }
        }

        public void dragEnter(DragSourceDragEvent dragSourceDragEvent) {
            DragSourceContext context = dragSourceDragEvent
                    .getDragSourceContext();
            int dropAction = dragSourceDragEvent.getDropAction();
            if ((dropAction & DnDConstants.ACTION_COPY) != 0) {
                context.setCursor(DragSource.DefaultCopyDrop);
            } else if ((dropAction & DnDConstants.ACTION_MOVE) != 0) {
                context.setCursor(DragSource.DefaultMoveDrop);
            } else {
                context.setCursor(DragSource.DefaultCopyNoDrop);
            }
        }

        public void dragExit(DragSourceEvent dragSourceEvent) {
        }

        public void dragOver(DragSourceDragEvent dragSourceDragEvent) {
        }

        public void dropActionChanged(DragSourceDragEvent dragSourceDragEvent) {
        }
    }
}
