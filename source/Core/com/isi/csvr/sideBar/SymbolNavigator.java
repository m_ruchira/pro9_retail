package com.isi.csvr.sideBar;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.WatchlistListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowController;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.text.BadLocationException;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 26-Mar-2008 Time: 15:42:44 To change this template use File | Settings
 * | File Templates.
 */
public class SymbolNavigator extends InternalFrame implements ActionListener, KeyListener, WatchlistListener,
        Runnable, FocusListener, NonTabbable, ExchangeListener, InternalFrameListener, PopupMenuListener, MouseListener, LinkedWindowController, Themeable {
    public static SymbolNavigator self;
    public Hashtable<String, SymbolNode> mainStore;
    public Hashtable<String, SymbolNode> filteredStore;
    private JPanel mainPanel;
    private JScrollPane scrollPane;
    private JPanel upperPanel;
    private TWButton searchButton;
    private TWButton naviButton;
    private TWTextField searchText;
    private ExchangeNode rootNode;
    private SideBarTree tree;
    private SearchTreeModel treeModel;
    private SymbolTreeRenderer renderer = new SymbolTreeRenderer();
    private JPopupMenu symbolTreePopup;
    private String selectedSymbol = "";
    private String selectedExchange = "";
    private int selectedSymbolsIntType = -1;
    private ViewSetting oSetting;
    private boolean isReady = false;
    private boolean isActive = false;
    private Hashtable<String, ExchangeNode> watchListReference;
    private ToolBarButton btnSearch;
    private ToolBarButton btnConfig;

    private SymbolNavigator() {
        oSetting = ViewSettingsManager.getSummaryView("SIDE_BAR");
        oSetting.setParent(this);
        this.setViewSetting(oSetting);
        rootNode = new ExchangeNode("Root", 0, "root");
        treeModel = new SearchTreeModel(rootNode);
        mainStore = new Hashtable<String, SymbolNode>();
        filteredStore = new Hashtable<String, SymbolNode>();
        watchListReference = new Hashtable<String, ExchangeNode>();
        createUI();
        applyTheme();
        Theme.registerComponent(this);
        isActive = true;
        hideTitleBarMenu();


    }

    public static SymbolNavigator getSharedInstance() {
        if (self == null) {
            self = new SymbolNavigator();
        }
        return self;
    }

    public boolean isNeedsToDisplay() {
        return oSetting.isVisible();
    }

    public Dimension getSavedSize() {
        return oSetting.getSize();
    }

    public void createUI() {
        mainPanel = new JPanel();
        upperPanel = new JPanel();
        tree = new SideBarTree(treeModel);
        addKeyListener();
        addMouseListener();
        tree.setOpaque(false);
        tree.addKeyListener(this);
        tree.putClientProperty("JTree.lineStyle", "Vertical");
        tree.setToggleClickCount(1);
        tree.setCellRenderer(renderer);
        tree.setRootVisible(false);
        scrollPane = new JScrollPane(tree);


        btnConfig = new ToolBarButton();
        btnConfig.setPreferredSize(new Dimension(20, 19));
        btnConfig.setToolTipText(Language.getString("SIDEBAR_CONF"));
        btnConfig.setEnabled(true);
        btnConfig.setVisible(true);
        btnConfig.setIcon(getIconFromString("configure"));
        btnConfig.setRollOverIcon(getIconFromString("configure-mouseover"));
        btnConfig.addMouseListener(this);
        btnSearch = new ToolBarButton();
        btnSearch.setPreferredSize(new Dimension(20, 19));
        btnSearch.setToolTipText(Language.getString("SEARCH"));
        btnSearch.setEnabled(true);
        btnSearch.setVisible(true);
        btnSearch.setIcon(getIconFromString("search"));
        btnSearch.setRollOverIcon(getIconFromString("search-mouseover"));
        btnSearch.addMouseListener(this);
        searchButton = new TWButton(">>");
        searchButton.addActionListener(this);
        searchText = new TWTextField();
        searchText.addKeyListener(this);
        searchText.addFocusListener(this);
        searchText.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchText.requestFocus(false);
        this.setSize(oSetting.getSize());
        this.setLocation(oSetting.getLocation());
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        this.add(mainPanel);
        this.setResizable(true);
        this.setClosable(true);
        this.setLinkGroupsEnabled(true);
//        this.setMaximizable(true);
//        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle(Language.getString("SIDE_BAR"));
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "100%"}, 3, 3));
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%", "18", "20"}, new String[]{"100%"}, 3, 3));
        upperPanel.add(searchText);
        upperPanel.add(btnSearch);
//        upperPanel.add(naviButton);
        upperPanel.add(btnConfig);
        GUISettings.applyOrientation(this);
        GUISettings.applyOrientation(this);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        Client.getInstance().getDesktop().add(this);
        loadTree();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        WatchListManager.getInstance().addWatchlistListener(this);
        this.hideTitleBarMenu();
        mainPanel.add(upperPanel);
        mainPanel.add(scrollPane);
        setBorder(BorderFactory.createEmptyBorder());
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        SideBar.getSharedInstance().closeSideBar();
    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath(iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    public void loadTree() {
        mainStore.clear();
        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exg = (Exchange) exchanges.nextElement();
                if (exg.isDefault()) {
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        ExchangeNode exgVector = new ExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        for (Market subMarket : exg.getSubMarkets()) {
                            Hashtable<String, SymbolNode> marketHash = new Hashtable<String, SymbolNode>();
                            String subName = subMarket.getDescription();
                            Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                            String[] symbolList = subSymbols.getSymbols();
                            for (int i = 0; i < symbolList.length; i++) {
                                SymbolNode sto = new SymbolNode();
                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(subName);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                sto.setType(Constants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                            }
                            //ExchangeNode tempvector = new ExchangeNode(subName, exg.getMarketStatus(), exg.getSymbol());
                            ExchangeNode tempvector = new ExchangeNode(subName, -1, exg.getSymbol());
                            tempvector.setChildrenStore(marketHash);
                            exgVector.addNode(tempvector);
                        }
                        rootNode.addNode(exgVector);

                    } else {
                        Hashtable<String, SymbolNode> exgHash = new Hashtable<String, SymbolNode>();
                        Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                        while (exchageSymbols.hasMoreElements()) {
                            String symbol = (String) exchageSymbols.nextElement();
                            SymbolNode sto = new SymbolNode();
                            sto.setExchange(exg.getSymbol());
                            sto.setMarket(null);
                            sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                            sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                            sto.setType(Constants.symbolType);
                            try {
                                Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                sto.setShortDescription(st.getShortDescription());
                                sto.setOpenVal(st.getTodaysOpen());
                                sto.setCloseVal(st.getTodaysClose());
                                sto.setHighVal(st.getHigh());
                                sto.setLowVal(st.getLow());
                                sto.setDescription(st.getLongDescription());
                                st = null;
                            } catch (Exception e) {
                                sto.setShortDescription("");
                                sto.setOpenVal(0.0);
                                sto.setCloseVal(0.0);
                                sto.setHighVal(0.0);
                                sto.setLowVal(0.0);
                                sto.setDescription("");
                            }
                            mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                            exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                        }
                        ExchangeNode tempExgVector = new ExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        tempExgVector.setChildrenStore(exgHash);
                        rootNode.addNode(tempExgVector);
                    }
                }

            }
            try {
                Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                watchListReference.clear();
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist : watchlists) {
                    if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                        Hashtable<String, SymbolNode> watchListHash = new Hashtable<String, SymbolNode>();
                        String[] symbolArray = watchlist.getSymbols();
                        for (int i = 0; i < symbolArray.length; i++) {
                            String key = symbolArray[i];
                            SymbolNode sto = new SymbolNode();
                            sto.setExchange(SharedMethods.getExchangeFromKey(key));
                            sto.setMarket(null);
                            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
                            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
                            sto.setType(Constants.symbolType);
                            try {
                                Stock st = DataStore.getSharedInstance().getStockObject(key);
                                boolean isExchangeDefault = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key)).isDefault();
                                if (!isExchangeDefault && st == null) {
                                    String frame = ValidatedSymbols.getSharedInstance().getFrame(key);
                                    String[] arr = frame.split(Meta.ID);
//                                sto.setShortDescription(st.getShortDescription());//st.getShortDescription()
                                    sto.setShortDescription(UnicodeUtils.getNativeString(arr[4].split("\\|")[0]));//st.getShortDescription()
//                                sto.setOpenVal(st.getTodaysOpen());
//                                sto.setCloseVal(st.getTodaysClose());
//                                sto.setHighVal(st.getHigh());
//                                sto.setLowVal(st.getLow());
                                    sto.setDescription(UnicodeUtils.getNativeString(arr[4].split("\\|")[0]));
                                } else {
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setDescription(st.getLongDescription());

                                }
                                st = null;
                            } catch (Exception e) {
//                                e.printStackTrace();
//                                sto.setShortDescription("");
                                sto.setOpenVal(0.0);
                                sto.setCloseVal(0.0);
                                sto.setHighVal(0.0);
                                sto.setLowVal(0.0);
                                sto.setDescription("");
                            }
                            mainStore.put(key, sto);
                            watchListHash.put(key, sto);
                        }
                        ExchangeNode tempWatchListVector = new ExchangeNode(watchlist.getCaption(), -1, "");
                        tempWatchListVector.setChildrenStore(watchListHash);
                        watchListReference.put(watchlist.getId(), tempWatchListVector);
                        rootNode.addNode(tempWatchListVector);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //addMouseListener();
            ToolTipManager.sharedInstance().registerComponent(tree);
            searchText.requestFocus(false);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    //To change body of implemented methods use File | Settings | File Templates.
                    GUISettings.applyOrientation(mainPanel);
                    GUISettings.applyOrientation(tree);
                    tree.updateUI();
                    tree.repaint();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void refreshTree() {
        refreshTree(rootNode.getSymbolStore());
        tree.updateUI();
    }

    private void refreshTree(Vector vector) {
//        System.out.println(rootNode);
//        Vector vector = rootNode.getSymbolStore();
        for (int i = 0; i < vector.size(); i++) {
            Object record = vector.get(i);

            if (record instanceof SymbolNode) {
                SymbolNode symbolNode = (SymbolNode) record;
                Stock st = DataStore.getSharedInstance().getStockObject(symbolNode.getExchange(), symbolNode.getSymbol(), symbolNode.getInstrumentType());
                if (st != null) {
                    symbolNode.setShortDescription(st.getShortDescription());
//                    symbolNode.setOpenVal(st.getTodaysOpen());
//                    symbolNode.setCloseVal(st.getTodaysClose());
//                    symbolNode.setHighVal(st.getHigh());
//                    symbolNode.setLowVal(st.getLow());
                    symbolNode.setDescription(st.getLongDescription());
                }
            } else if (record instanceof ExchangeNode) {
                refreshTree(((ExchangeNode) record).getSymbolStore());
            }
        }
    }

    public void searchTree(String criteria) {
        if ((!criteria.equals(""))) {
            Enumeration mainStoreenum = mainStore.elements();
            filteredStore.clear();
            while (mainStoreenum.hasMoreElements()) {
                SymbolNode sto = (SymbolNode) mainStoreenum.nextElement();
                try {
                    if ((sto.getSymbol().contains(criteria.toUpperCase())) || (sto.getDescription().contains(criteria.substring(0, 1).toUpperCase() + criteria.substring(1))) || (sto.getDescription().contains(criteria))) {
                        filteredStore.put(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()), sto);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            rootNode.clear();
            try {

                Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
                while (exchanges.hasMoreElements()) {
                    Exchange exg = (Exchange) exchanges.nextElement();
                    boolean ismatched = false;
                    if (exg.isDefault()) {
                        if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                            ExchangeNode exgVector = new ExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                            for (Market subMarket : exg.getSubMarkets()) {
                                Hashtable<String, SymbolNode> marketHash = new Hashtable<String, SymbolNode>();
                                String subName = subMarket.getDescription();
                                Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                                String[] symbolList = subSymbols.getSymbols();
                                for (int i = 0; i < symbolList.length; i++) {
                                    if (filteredStore.containsKey(symbolList[i])) {
                                        SymbolNode sto = new SymbolNode();
                                        sto.setExchange(exg.getSymbol());
                                        sto.setMarket(subName);
                                        sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                        sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                        sto.setType(Constants.symbolType);
                                        try {
                                            Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                            sto.setShortDescription(st.getShortDescription());
                                            sto.setOpenVal(st.getTodaysOpen());
                                            sto.setCloseVal(st.getTodaysClose());
                                            sto.setHighVal(st.getHigh());
                                            sto.setLowVal(st.getLow());
                                            sto.setDescription(st.getLongDescription());
                                            st = null;
                                        } catch (Exception e) {
                                            sto.setShortDescription("");
                                            sto.setOpenVal(0.0);
                                            sto.setCloseVal(0.0);
                                            sto.setHighVal(0.0);
                                            sto.setLowVal(0.0);
                                            sto.setDescription("");
                                        }
                                        ismatched = true;
                                        marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                    }
                                }
                                if (ismatched) {
                                    ExchangeNode tempvector = new ExchangeNode(subName, exg.getMarketStatus(), exg.getSymbol());
                                    tempvector.setChildrenStore(marketHash);
                                    exgVector.addNode(tempvector);
                                }
                            }
                            if (ismatched) {
                                rootNode.addNode(exgVector);
                            }

                        } else {
                            Hashtable<String, SymbolNode> exgHash = new Hashtable<String, SymbolNode>();
                            Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                            while (exchageSymbols.hasMoreElements()) {
                                String symbol = (String) exchageSymbols.nextElement();
                                if (filteredStore.containsKey(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)))) {
                                    SymbolNode sto = new SymbolNode();
                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(null);
                                    sto.setSymbol(SharedMethods.getSymbolFromExchangeKey(symbol));
                                    sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    sto.setType(Constants.symbolType);
                                    try {
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                    }
                                    ismatched = true;
                                    exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                }
                            }
                            if (ismatched) {
                                ExchangeNode tempExgVector = new ExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                tempExgVector.setChildrenStore(exgHash);
                                rootNode.addNode(tempExgVector);
                            }
                        }
                    }

                }
                try {
                    Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                try {
                    WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                    for (WatchListStore watchlist : watchlists) {
                        if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                            boolean isWatchListMatched = false;
                            Hashtable<String, SymbolNode> watchListHash = new Hashtable<String, SymbolNode>();
                            String[] symbolArray = watchlist.getSymbols();
                            for (int i = 0; i < symbolArray.length; i++) {
                                String key = symbolArray[i];
                                SymbolNode sto = new SymbolNode();
                                sto.setExchange(SharedMethods.getExchangeFromKey(key));
                                sto.setMarket(null);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(key));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
                                sto.setType(Constants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(key);
                                    boolean isExchangeDefault = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key)).isDefault();
                                    if (!isExchangeDefault) {
                                        String frame = ValidatedSymbols.getSharedInstance().getFrame(key);
                                        String[] arr = frame.split(Meta.ID);
                                        sto.setShortDescription(arr[4].split("\\|")[0]);//st.getShortDescription()
                                        sto.setDescription(arr[4].split("\\|")[0]);
                                    } else {
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setDescription(st.getLongDescription());

                                    }
                                    st = null;
                                } catch (Exception e) {
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                    isWatchListMatched = true;
                                    watchListHash.put(key, sto);
                                }
                            }

                            if (isWatchListMatched) {
                                ExchangeNode tempWatchListVector = new ExchangeNode(watchlist.getCaption(), -1, "");
                                tempWatchListVector.setChildrenStore(watchListHash);
                                rootNode.addNode(tempWatchListVector);
                            }
                        }
                    }
                } catch (Exception e) {

                }
                tree.repaint();
                tree.updateUI();
                for (int i = 0; i < tree.getRowCount(); i++) {
                    tree.expandRow(i);
                }
                ToolTipManager.sharedInstance().registerComponent(tree);
                //addMouseListener();
                GUISettings.applyOrientation(mainPanel);
                searchText.requestFocus(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rootNode.clear();
            loadTree();
            tree.updateUI();
            tree.repaint();
            scrollPane.updateUI();
            mainPanel.updateUI();
            this.updateUI();
            searchText.requestFocus(false);
        }

    }

    public void fireSymbolAddedEvent(String key, String id) {
        try {
            SymbolNode sto = new SymbolNode();
            sto.setExchange(SharedMethods.getExchangeFromKey(key));
            sto.setMarket(null);
            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
            sto.setType(Constants.symbolType);
            try {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                sto.setShortDescription(st.getShortDescription());
                sto.setOpenVal(st.getTodaysOpen());
                sto.setCloseVal(st.getTodaysClose());
                sto.setHighVal(st.getHigh());
                sto.setLowVal(st.getLow());
                sto.setDescription(st.getLongDescription());
                st = null;
            } catch (Exception e) {
                sto.setShortDescription("");
                sto.setOpenVal(0.0);
                sto.setCloseVal(0.0);
                sto.setHighVal(0.0);
                sto.setLowVal(0.0);
                sto.setDescription("");
            }
            mainStore.put(key, sto);
            (watchListReference.get(id)).addNode(sto);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    tree.updateUI();
                    tree.repaint();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void fireSymbolRemovedEvent(String key, String id) {
        try {
            SymbolNode sto = new SymbolNode();
            sto.setExchange(SharedMethods.getExchangeFromKey(key));
            sto.setMarket(null);
            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
            sto.setType(Constants.symbolType);
            try {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                sto.setShortDescription(st.getShortDescription());
                sto.setOpenVal(st.getTodaysOpen());
                sto.setCloseVal(st.getTodaysClose());
                sto.setHighVal(st.getHigh());
                sto.setLowVal(st.getLow());
                sto.setDescription(st.getLongDescription());
                st = null;
            } catch (Exception e) {
                sto.setShortDescription("");
                sto.setOpenVal(0.0);
                sto.setCloseVal(0.0);
                sto.setHighVal(0.0);
                sto.setLowVal(0.0);
                sto.setDescription("");
            }
            mainStore.put(key, sto);
            (watchListReference.get(id)).removeNode(sto);
            SharedMethods.updateComponent(tree);//tree.updateUI();
            tree.repaint();
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(searchButton)) {
            searchTree(searchText.getText());
        } else if (e.getActionCommand().equals("HIDE")) {
            // super.actionPerformed(e);
        } else if (e.getSource().equals(naviButton)) {
            showOptionDialog();
        }
    }

    private void showOptionDialog() {
        SideBarOptionDialog rUI = new SideBarOptionDialog(Client.getInstance().getFrame(), Language.getString("SIDEBAR_OPTIONS_TITLE"), true);
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(searchText)) {
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                if (!searchText.getText().equals("")) {
                    searchTree(searchText.getText());
                    e.consume();
                    searchText.requestFocus(true);
                } else {
                    rootNode.clear();
                    loadTree();
                    e.consume();
                    setInitialText();
                }

            } else if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                if (searchText.getText().equals("")) {
                    rootNode.clear();
                    loadTree();
                    e.consume();
                    setInitialText();
                }

            } else if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
                rootNode.clear();
                loadTree();
                e.consume();
                setInitialText();
            }
        } else {
            searchText.requestFocus();
            try {
                if (!Character.isWhitespace(e.getKeyChar()) && (!(e.getKeyChar() == KeyEvent.VK_ESCAPE))) {
                    searchText.getDocument().insertString(searchText.getCaretPosition(), "" + e.getKeyChar(), null);
                }
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void setInitialText() {
        searchText.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchText.setSelectionStart(0);
        searchText.setSelectionEnd(searchText.getText().length());
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.


    }

    private JPopupMenu getPopupMenu1() {
        if (symbolTreePopup == null) {
            symbolTreePopup = Client.getInstance().getTablePopup();
            symbolTreePopup.addPopupMenuListener(this);
        }
        return symbolTreePopup;
    }

    private void validatePopup() {

    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(btnConfig)) {

            showOptionDialog();
        }
        if (e.getSource().equals(btnSearch)) {
            searchTree(searchText.getText());
        }
    }

    private void addMouseListener() {
        tree.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                if (path != null) {
                    tree.setSelectionPath(path);
                    try {
                        SymbolNode node = (SymbolNode) path.getLastPathComponent();
                        SymbolNode sto = (SymbolNode) node;
                        selectedExchange = sto.getExchange();
                        selectedSymbol = sto.getSymbol();
                        selectedSymbolsIntType = sto.getInstrumentType();
                        Client.getInstance().setSideBarSelected(SymbolNavigator.this);
                    } catch (Exception e) {

                    }

                }
            }

            public void mouseClicked(MouseEvent ev) {
                TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                if (path == null) {
                    return;
                }
                if (ev.getClickCount() > 1) {
                    SymbolNode node = (SymbolNode) path.getLastPathComponent();
                    if (node != null && !ev.isPopupTrigger()) {    //&& node.isLeaf()
                        if (node instanceof SymbolNode) {
                            Stock st = DataStore.getSharedInstance().getStockObject(selectedExchange, selectedSymbol, selectedSymbolsIntType);
                            if (st != null && st.isSymbolEnabled()) {
                                if ((st.getInstrumentType() == Meta.INSTRUMENT_INDEX)) {
                                    Client.getInstance().mnu_DetailQuoteIndex(SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType), false);
                                } else if ((st.getInstrumentType() == Meta.INSTRUMENT_MUTUALFUND)) {
                                    Client.getInstance().mnu_DetailQuoteMutualFund(SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType), false);
                                } else if ((st.getInstrumentType() == Meta.INSTRUMENT_OPTION)) {
                                    Client.getInstance().mnu_DetailQuoteOptions(SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType), false);
                                } else if ((st.getInstrumentType() == Meta.INSTRUMENT_FOREX)) {
                                    Client.getInstance().mnu_DetailQuoteForex(SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType), false);
                                } else {
                                    Client.getInstance().mnuDetailQuoteSymbol(SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType), false, false, false, LinkStore.LINK_NONE);
                                }
                                st = null;
//                                Client.getInstance().mnuDetailQuoteSymbol(SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType), false, false, false, LinkStore.LINK_NONE);
                            }
                        }
                    }
                }
            }

            public void mouseReleased(MouseEvent ev) {
                try {
                    TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                    if (path == null) {
                        return;
                    }

                    //IF we're on a path, then we can check the node that its holding
                    SymbolNode node = (SymbolNode) path.getLastPathComponent();
                    if (node != null && ev.isPopupTrigger()) {    //&& node.isLeaf()
                        if (node instanceof SymbolNode) {
                            getPopupMenu1().show(tree, ev.getX(), ev.getY());
                        }

                    }
                } catch (Exception e) {

                }
            }
        });
    }

    public String getSelectedKey() {
        return SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);
    }

    public void run() {
        while (isActive) {
            try {
                if (isReadyToFilter()) {
                    searchTree(searchText.getText());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void showSideBar() {
        if (Language.isLTR()) {
            showSideBar(true);
            searchText.requestFocus(true);
        } else {
            showSideBar(false);
        }
    }

    public void showSideBar(boolean left) {
        if (isVisible()) {
            setVisible(false);
            return;
        }

        Dimension desktopSize = getDesktopPane().getSize();
        setVisible(true);
        setSize(getWidth(), (int) desktopSize.getHeight());
        if (left) {
            setLocation(0, 0);
        } else {
            setLocation((int) desktopSize.getWidth() - getWidth(), 0);
        }
        searchText.requestFocus(true);
    }

    public void applyTheme() {
//        SymbolTreeRenderer.reload();
////        rootNode.clear();
////        loadTree();
//        SharedMethods.updateComponent(tree);
//        tree.repaint();
        btnSearch.setIcon(getIconFromString("search"));
        btnSearch.setRollOverIcon(getIconFromString("search-mouseover"));
        btnConfig.setIcon(getIconFromString("configure"));
        btnConfig.setRollOverIcon(getIconFromString("configure-mouseover"));

    }

    private boolean isReadyToFilter() {
        return this.isReady;
    }

    public void exchangeAdded(Exchange exchange) {

    }


    public void focusGained(FocusEvent e) {
        if (e.getSource().equals(searchText)) {
            if (searchText.getText().equals(Language.getString("SIDE_BAR_SEARCH"))) {
                searchText.setText("");
            }

        }
    }

    public void focusLost(FocusEvent e) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeMustInitialize(Exchange exchange, long from, long to) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        rootNode.clear();
        loadTree();
    }

    public void exchangesLoaded() {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {

        try {
            for (int i = 0; i < rootNode.getSymbolStore().size(); i++) {
                ExchangeNode ex = (ExchangeNode) rootNode.getSymbolStore().get(i);
                if (ex.getExchangeSymbol().equals(exchange.getSymbol())) {
                    ex.setMarketStatus(exchange.getMarketStatus());
                    updateTree();
                    tree.repaint();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void symbolAdded(String key, String listID) {
        fireSymbolAddedEvent(key, listID);
        rootNode.clear();
        loadTree();
        tree.repaint();
        SharedMethods.updateComponent(tree);

    }

    public void symbolRemoved(String key, String listID) {
        fireSymbolRemovedEvent(key, listID);
        rootNode.clear();
        loadTree();
        tree.repaint();
        SharedMethods.updateComponent(tree);
    }

    public void watchlistAdded(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void watchlistRenamed(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void watchlistRemoved(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void setVisible(boolean value) {
        super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        Component[] items = symbolTreePopup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] instanceof TWMenuItem) {
                TWMenuItem item = (TWMenuItem) items[i];
                if (item.getText().equals(Language.getString("ADD_SYMBOLS"))) {
//                        item.setVisible(false);
                } else if (item.getText().equals(Language.getString("REMOVE_SYMBOL"))) {
//                        item.setVisible(false);
                }
            }
        }
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        Component[] items = symbolTreePopup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] instanceof TWMenuItem) {
                TWMenuItem item = (TWMenuItem) items[i];
                if (item.getText().equals(Language.getString("ADD_SYMBOLS"))) {
//                        item.setVisible(true);
                } else if (item.getText().equals(Language.getString("REMOVE_SYMBOL"))) {
//                        item.setVisible(true);
                }
            }
        }
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
        Component[] items = symbolTreePopup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] instanceof TWMenuItem) {
                TWMenuItem item = (TWMenuItem) items[i];
                if (item.getText().equals(Language.getString("ADD_SYMBOLS"))) {
//                        item.setVisible(true);
                } else if (item.getText().equals(Language.getString("REMOVE_SYMBOL"))) {
//                        item.setVisible(true);
                }
            }
        }
    }

    public void updateTree() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                tree.updateUI();
            }
        });


    }

    public void updateUI() {
        super.updateUI();
        BasicInternalFrameUI ui = (BasicInternalFrameUI) super.getUI();
        Component north = ui.getNorthPane();

        MouseMotionListener[] actions = north.getListeners(MouseMotionListener.class);
        for (int i = 0; i < actions.length; i++)
            north.removeMouseMotionListener(actions[i]);

        MouseListener[] actions2 = north.getListeners(MouseListener.class);
        for (int i = 0; i < actions2.length; i++)
            north.removeMouseListener(actions2[i]);
    }

    public void addKeyListener() {
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                TreePath path = e.getPath();
                SymbolNode node;
                node = (SymbolNode) path.getLastPathComponent();
                SymbolNode sto = (SymbolNode) node;
                selectedExchange = sto.getExchange();
                selectedSymbol = sto.getSymbol();
                selectedSymbolsIntType = sto.getInstrumentType();
                if (selectedSymbol != null && !selectedSymbol.isEmpty()) {
                    fireSymbolChangeInLinkedWindows(getLinkedGroupID(), SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType));
                    //(LinkStore.getSharedInstance()).symbolChanged(SharedMethods.getKey(selectedExchange,selectedSymbol,selectedSymbolsIntType));
                }
            }
        });

    }

    public void fireSymbolChangeInLinkedWindows(String group, String sKey) {
        // (LinkStore.getSharedInstance()).symbolChanged(SharedMethods.getKey(selectedExchange,selectedSymbol,selectedSymbolsIntType));
        LinkStore.getSharedInstance().fireSymbolChangedForGroup(group, sKey);

    }

    public boolean isLinkWindowControlEnabled() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setLinkWindowControl(boolean isCtrlEnabled) {
    }

    class NodeCoparator implements Comparator {
        public int compare(Object o1, Object o2) {
            return ((ExchangeNode) o1).getName().compareTo(((ExchangeNode) o2).getName());
        }
    }
}
