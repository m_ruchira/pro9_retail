package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.marketdepth.CombinedDepthModel;
import com.isi.csvr.marketdepth.DepthPriceModel;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CombinedDepthRenderer;
import com.isi.csvr.table.Table;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 29, 2008
 * Time: 9:46:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class DepthByPriceCombinedFrame extends InternalFrame implements LinkedWindowListener {

    boolean loadedFromWorkspace = false;
    byte stockDecimalPlaces = 2;
    byte type;
    private Table bidTable;
    private Table askTable;
    private ViewSetting oBidViewSetting = null;
    private ViewSetting oAskViewSetting = null;
    private DepthPriceModel askModel;
    private CombinedDepthModel bidModel;
    private String selectedKey;
    private ViewSettingsManager g_oViewSettings;
    private long timeStamp;
    private boolean isLinked = false;

    public DepthByPriceCombinedFrame(ThreadGroup threadGroup, String sThreadID, WindowDaemon oDaemon, Table oTable, String key, boolean fromWSP, byte parentWindowType, boolean isLinked, String linkgroup) {
        super(threadGroup, sThreadID, oDaemon, oTable);
        bidTable = oTable;
        //askTable = oTable2[0];
        loadedFromWorkspace = fromWSP;
        selectedKey = key;
        this.type = parentWindowType;
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        this.isLinked = isLinked;
        setLinkedGroupID(linkgroup);
        initUI();
    }

    public void initUI() {


        bidTable.setAutoResize(true);
        bidTable.getPopup().showLinkMenus();
//        bidTable.getPopup().showSetDefaultMenu();
        bidTable.setWindowType(ViewSettingsManager.COMBINED_DEPTH_VIEW);

        bidModel = new CombinedDepthModel(selectedKey);
        try {
//                bidModel = new CombinedDepthModel(selectedKey);
            bidModel.setDecimalCount(SharedMethods.getDecimalPlaces(selectedKey));
        } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if (type != Constants.FULL_QUOTE_TYPE && oBidViewSetting == null) {
            if (loadedFromWorkspace && isLinked) {
                ViewSetting oSetting = g_oViewSettings.getDepthView(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
                if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                    oBidViewSetting = oSetting;
                }
            }
            if (oBidViewSetting == null) {
                oBidViewSetting = g_oViewSettings.getDepthView(ViewSettingsManager.COMBINED_DEPTH_VIEW +
                        "|" + selectedKey);
            }
        } else if (type == Constants.FULL_QUOTE_TYPE) {
            // oBidViewSetting = g_oViewSettings.getFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW);  // selectedKey + "*"+type);
            if (oBidViewSetting == null && loadedFromWorkspace && isLinked) {
                ViewSetting oSetting = g_oViewSettings.getFullQuoteView(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
                if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                    oBidViewSetting = oSetting;
                }
            }
            if (oBidViewSetting == null) {
                oBidViewSetting = g_oViewSettings.getFullQuoteView(ViewSettingsManager.COMBINED_DEPTH_VIEW +
                        "|" + selectedKey);
            }
        }


        if (oBidViewSetting == null) {
            oBidViewSetting = g_oViewSettings.getSymbolView("COMBINED_DEPTH");
            oBidViewSetting = oBidViewSetting.getObject();
            oBidViewSetting.setDesktopType(type);
            if (type != Constants.FULL_QUOTE_TYPE) {
                GUISettings.setColumnSettings(oBidViewSetting, TWColumnSettings.getItem("COMBINED_ORDER_COLS"));
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oBidViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oBidViewSetting.getType(), oBidViewSetting);
                // oBidViewSetting.setID(selectedKey);
                timeStamp = System.currentTimeMillis();
                oBidViewSetting.setID(timeStamp + "");
                oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
                oBidViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                oBidViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                g_oViewSettings.putDepthView(ViewSettingsManager.COMBINED_DEPTH_VIEW +
                        "|" + selectedKey, oBidViewSetting);
            } else {
                oBidViewSetting.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                GUISettings.setColumnSettings(oBidViewSetting, TWColumnSettings.getItem("COMBINED_ORDER_COLS"));//"|" + sSelectedSymbol+ "*"+Constants.FULL_QUOTE_TYPE
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oBidViewSetting);
                // DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oBidViewSetting.getType(), oBidViewSetting);
                timeStamp = System.currentTimeMillis();
                oBidViewSetting.setID(timeStamp + "");
                oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
                oBidViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                oBidViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                g_oViewSettings.putFullQuoteSubView(ViewSettingsManager.COMBINED_DEPTH_VIEW +
                        "|" + selectedKey, oBidViewSetting);
            }
//                oBidViewSetting.setID(selectedKey);
//                g_oViewSettings.putDepthView(ViewSettingsManager.COMBINED_DEPTH_VIEW +
//                        "|" + selectedKey, oBidViewSetting);

        } else {
            oBidViewSetting.setDesktopType(type);
            loadedFromWorkspace = true;
        }

        bidModel.setViewSettings(oBidViewSetting);
        bidTable.setModel(bidModel);
        bidModel.setTable(bidTable, new CombinedDepthRenderer());  // Add the renderer...
        bidModel.applyColumnSettings();
        TWMenuItem calcMenu1 = new TWMenuItem(Language.getString("PRICE_CALC"), "ordercalc.gif");
        calcMenu1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (type == Constants.FULL_QUOTE_TYPE) {
                    Client.getInstance().mnu_MDepth_Calculator(selectedKey, TWTypes.TradeSides.SELL, false, false, LinkStore.LINK_NONE);
//                        TradeMethods.getSharedInstance().showBuyScreen(MultiDialogWindow.getSharedInstance().getSelectedSymbol());
                } else {
                    Client.getInstance().mnu_MDepth_Calculator(selectedKey, TWTypes.TradeSides.SELL, false, false, LinkStore.LINK_NONE);
                }

            }
        });
        bidTable.getPopup().setMenuItem(calcMenu1);
        final TWMenuItem buymenu = new TWMenuItem(Language.getString("BUY"), "buy.gif");
        buymenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (type == Constants.FULL_QUOTE_TYPE) {
                    TradeMethods.getSharedInstance().showBuyScreen(selectedKey);
                } else {
                    TradeMethods.getSharedInstance().showBuyScreen(selectedKey);
                }
            }
        });
        bidTable.getPopup().setMenuItem(buymenu);
        final TWMenuItem sellmenu = new TWMenuItem(Language.getString("SELL"), "sell.gif");
        sellmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (type == Constants.FULL_QUOTE_TYPE) {
                    TradeMethods.getSharedInstance().showSellScreen(selectedKey);
                } else {
                    TradeMethods.getSharedInstance().showSellScreen(selectedKey);
                }
            }
        });
        bidTable.getPopup().setMenuItem(sellmenu);
        bidTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (type == Constants.FULL_QUOTE_TYPE) {
                        if (((LongTransferObject) (bidModel.getValueAt(bidTable.getTable().getSelectedRow(), -1))).getValue() == 1) {
                            TradeMethods.getSharedInstance().showSellScreen(selectedKey);
                        } else {
                            TradeMethods.getSharedInstance().showBuyScreen(selectedKey);
                        }
                    } else {
                        if (((LongTransferObject) (bidModel.getValueAt(bidTable.getTable().getSelectedRow(), -1))).getValue() == 1) {
                            TradeMethods.getSharedInstance().showSellScreen(selectedKey);
                        } else {
                            TradeMethods.getSharedInstance().showBuyScreen(selectedKey);
                        }
                    }
                }
            }
        });

        bidTable.getPopup().addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                buymenu.setVisible(TradingShared.isReadyForTrading());
                buymenu.setEnabled(TradingShared.isReadyForTrading());
                sellmenu.setVisible(TradingShared.isReadyForTrading());
                sellmenu.setEnabled(TradingShared.isReadyForTrading());
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });
        oBidViewSetting.setParent(this);
        oBidViewSetting.setTableNumber(1);

        this.setShowServicesMenu(true);
        this.getContentPane().setLayout(new BorderLayout());
        this.setSize(oBidViewSetting.getSize());
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setLinkGroupsEnabled(true);
        if (type != Constants.FULL_QUOTE_TYPE) {
            Client.getInstance().oTopDesktop.add(this);
            this.setTitle(Language.getString("MARKET_DEPTH_BY_PRICE") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                    + ") " + DataStore.getSharedInstance().getCompanyName(selectedKey));
        }

        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);


        GUISettings.applyOrientation(this);
        bidTable.updateGUI();
        this.applySettings();
        this.getContentPane().add(bidTable);
        this.show();
        bidModel.adjustRows();
        this.setLayer(GUISettings.TOP_LAYER);
        setDecimalPlaces();
        if (type != Constants.FULL_QUOTE_TYPE) {
            if (loadedFromWorkspace && isLinked) {
                g_oViewSettings.setWindow(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" +
                        LinkStore.linked + "_" + getLinkedGroupID(), this);
                LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
            } else {
                g_oViewSettings.setWindow(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" +
                        selectedKey, this);
            }
//                g_oViewSettings.setWindow(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" +
//                        selectedKey, frame);

            String requestID = Meta.PRICE_DEPTH + "|" + selectedKey;
            Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.PRICE_DEPTH);
            this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
            if (!loadedFromWorkspace) {
                //                frame.setLocationRelativeTo(oTopDesktop);
                //                oBidViewSetting.setLocation(frame.getLocation());
                if (!oBidViewSetting.isLocationValid()) {
                    this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                    oBidViewSetting.setLocation(this.getLocation());
                } else {
                    this.setLocation(oBidViewSetting.getLocation());
                }
            }
        } else {
//                g_oViewSettings.setWindow(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" +
//                        MultiDialogWindow.TABLE_ID, frame);
        }


        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }


    }

    public void setSelectedKey(String selectedKey) {
        this.selectedKey = selectedKey;
    }

    public void symbolChanged(String sKey) {
        selectedKey = sKey;
        String oldKey = oBidViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oldKey))) {
            DataStore.getSharedInstance().removeSymbolRequest(oldKey);

        }
        String oldReqId = getDataRequestID();
        RequestManager.getSharedInstance().removeRequest(oldReqId);
        if (type != Constants.FULL_QUOTE_TYPE) {
            String requestID = Meta.PRICE_DEPTH + "|" + selectedKey;
            Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.PRICE_DEPTH);
            this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
        }
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }
        if (type != Constants.FULL_QUOTE_TYPE) {

            this.setTitle(Language.getString("MARKET_DEPTH_BY_PRICE") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                    + ") " + DataStore.getSharedInstance().getCompanyName(selectedKey));
        }
        bidModel.setSymbol(selectedKey);
        bidTable.getTable().updateUI();
        this.updateUI();
        oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
    }

    public void setDecimalPlaces() {
        try {
            stockDecimalPlaces = SharedMethods.getDecimalPlaces(selectedKey);
        } catch (Exception e) {
            stockDecimalPlaces = 2;
        }
        bidTable.getModel().setDecimalCount(stockDecimalPlaces);
        bidTable.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);

    }
}
