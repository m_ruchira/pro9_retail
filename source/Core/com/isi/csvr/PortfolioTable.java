package com.isi.csvr;

import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.PortfolioStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.print.PrintPreview;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.*;
import java.awt.event.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.Enumeration;

public class PortfolioTable extends JInternalFrame
        implements WindowWrapper, ItemListener, PopupMenuListener, InternalFrameListener,
        ActionListener, MouseListener, DropTargetListener, Printable, TWDesktopInterface, Themeable {

    TWDateFormat g_oDateFormat = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private int windowType;
    private PortfolioStore dataStore;
    private Table table;
    private JComboBox currencyList;
    private Object[] currencyIDs;
    private JLabel lblBaseCurrency;
    private JPopupMenu titleBarMenu;
    private JPopupMenu headerPopup;
    private TWMenuItem fontMenu;
    private TWMenuItem hideShowTitleMenu;
    private DropTarget dropTarget = null;
    private int m_maxNumPage = 1;
    private int maximumWidths[];
    private int colXPositions[];
    private int maxWidth = 110;
    private InternalFrame timeAndSales;


    public PortfolioTable() {
        dropTarget = new DropTarget(this, this);


        createTitlebarManu();
    }

    private void createTitlebarManu() {
        titleBarMenu = new JPopupMenu();
        TWMenuItem hideTitlemenu = new TWMenuItem(Language.getString("HIDE_TITLEBAR"));
        hideTitlemenu.setActionCommand("TITLE_HIDE");
        hideTitlemenu.addActionListener(this);
        titleBarMenu.add(hideTitlemenu);
        addTitlebarManu();
    }

    public void addTitlebarManu() {
        Component np = (Component) ((BasicInternalFrameUI) getUI()).getNorthPane();
        try {
            np.removeMouseListener(this); // if existing
        } catch (Exception e) {
        }
        np.addMouseListener(this);
    }


    private void toggleTitleBar() {
        setTitleVisible(!isTitleVisible());
    }

    public void revalidateHeader() {
        try {
            if (!table.getModel().getViewSettings().isHeaderVisible()) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                this.revalidate();
            }
        } catch (Exception e) {

        }
    }

    public void show() {
        super.show();
        setBaseCurrency();
        revalidateHeader();
    }

    public void updateUI() {
        super.updateUI();
        addTitlebarManu();
        revalidateHeader();
    }

    public void createTable() {
        lblBaseCurrency = new JLabel(Language.getString("DEFAULT_CURRENCY"));

        Enumeration ids = CurrencyStore.getSharedInstance().getCurrencyIDs();
        ArrayList idlist = new ArrayList();
        while (ids.hasMoreElements()) {
            idlist.add(ids.nextElement());
        }
        currencyIDs = idlist.toArray(currencyIDs);
        idlist = null;
        currencyList = new JComboBox(getComboModel());
        currencyList.setPreferredSize(new Dimension(150, 25));

        currencyList.addItemListener(this);
        currencyList.addPopupMenuListener(this);
        try {
            if (currencyList.getItemCount() > 0)
                currencyList.setSelectedIndex(0);
            //else
            //     currencyList.setSelectedIndex(0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JPanel currencyPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        currencyPanel.add(lblBaseCurrency);
        currencyPanel.add(currencyList);

        this.getContentPane().setLayout(new BorderLayout());
        //this.getContentPane().add(currencyPanel,BorderLayout.NORTH);
        Theme.registerComponent(this);
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+ Theme.getID()+"/InternalIcon.gif")));

    }

    private DefaultComboBoxModel getComboModel() {
        DefaultComboBoxModel comboModel = new DefaultComboBoxModel() {
            public Object getElementAt(int index) {
                String caption = SymbolMaster.getCurrencyCaption((String) currencyIDs[index]);
                if (caption.equals(""))
                    return currencyIDs[index];
                else
                    return caption;
            }

            public int getSize() {
                return currencyIDs.length;
            }

        };
        return comboModel;
    }

    public void applyTheme() {
//        setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
    }

    public PortfolioStore getDataStore() {
        return dataStore;
    }

    public void setDataStore(PortfolioStore newDataStore) {
        dataStore = newDataStore;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;

        //getContentPane().add(table);
        table.getTable().setDefaultEditor(Double.class, new DoubleCellEditor(new TWTextField()));
        table.getTable().setDefaultEditor(Long.class, new LongCellEditor(new TWTextField()));
        table.getTable().setDefaultEditor(Object.class, new DefaultTWCellEditor(new TWTextField()));

        this.getContentPane().add(table, BorderLayout.CENTER);
        table.setPopupActive(false);
    }

    public ViewSetting getViewSettings() {
        return table.getModel().getViewSettings();
    }

    public InternalFrame getTimeAndSalesWindow() {
        return timeAndSales;
    }

    public void setTimeAndSalesWindow(InternalFrame frame) {
        this.timeAndSales = frame;
        //this.tradeStore = tradeStore;
    }

    /*public void removeTimeAndSalesWindow(){
         TradeStore.removeFilteredStore(table.getModel().getViewSettings().getType()
             + "|" + table.getModel().getViewSettings().getID());
     }

     public void updateTimeAnsSalesSymbols(boolean reload){
         FilterableTradeStore store = TradeStore.getTradeStore(table.getModel().getViewSettings().getType()
             + "|" + table.getModel().getViewSettings().getID());
         if (store != null){
             store.setSymbols(dataStore.getSymbolIndex());
             if (reload){
                 TradeStore.reloadFilteredStore(store);
             }
         }
         timeAndSales.updateUI();
     }*/

    public void setTitle(String title) {
        super.setTitle(title);
        if (timeAndSales != null)
            timeAndSales.setTitle(Language.getString("MARKET_TIME_N_SALES") + " - " + title);
    }

    /* Table wrapper methods */

    public boolean isWindowVisible() {
        return isVisible();
    }

    public int getWindowStyle() {
        if (isMaximum())
            return ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
    }

    public JTable getTable1() {
        try {
            return table.getTable();
        } catch (Exception e) {
            return null;
        }
    }

    public CommonTable getModel1() {
        try {
            return table.getModel();
        } catch (Exception e) {
            return null;
        }
    }

    public JTable getTable2() {
        return null;
    }

    public JTable getTabFoler(String id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getZOrder() {
        return getDesktopPane().getPosition(this);
    }

    public void applySettings() {
        ViewSetting oSettings = null;
        if (table != null) {
            oSettings = table.getModel().getViewSettings();
            table.getModel().applySettings();
            this.setSize(oSettings.getSize());
        }

        this.setLocation(oSettings.getLocation());

        if (oSettings.isVisible()) {
            if (Settings.isPutAllToSameLayer() && (oSettings.getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSettings.getIndex());
            } else {
                this.getDesktopPane().setLayer(this, this.getLayer(), oSettings.getIndex());
            }
            this.setVisible(true);
            try {
                this.setSelected(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (Settings.isPutAllToSameLayer() && (oSettings.getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
            } else {
                this.getDesktopPane().setLayer(this, this.getLayer(), 0);
            }
        }

        try {
            switch (oSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    this.setIcon(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (oSettings.getLocation() != null)
            this.setLocation(oSettings.getLocation());
        this.setVisible(oSettings.isVisible());
        this.setTitle(oSettings.getCaption());
        revalidateHeader();
        //table.getModel().adjustColumns()
    }

    public boolean isTitleVisible() {
        //if (g_oViewSettings != null){
        return table.getModel().getViewSettings().isHeaderVisible();
        /*}else{
            return true;
        }*/
    }

    public void setTitleVisible(boolean setVisible) {
        ViewSetting settings = table.getModel().getViewSettings();
        if (setVisible) {
            if (settings != null) {
                settings.setHeaderVisible(true);
            }
            updateUI();
        } else {
            Component titleBar = ((BasicInternalFrameUI) getUI()).getNorthPane();
            if (titleBar != null) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                revalidate();
                if (settings != null) {
                    settings.setHeaderVisible(false);
                }
            }
        }
        settings = null;
    }

    public void windowClosing() {

    }

    public Object getDetachedFrame() {
        return null;
    }

    public boolean isDetached() {
        return false;
    }
    /* -- */

    public void updateGUI() {
        this.setTitle(table.getModel().getViewSettings().getCaption());
        table.getModel().updateGUI();
    }

    public String toString() {
        return getTitle();
    }

    public void itemStateChanged(ItemEvent e) {
        if ((e.getStateChange() == e.SELECTED) && (currencyList.getSelectedItem() != null)) {
            try {
                dataStore.setBaseCurrency((String) currencyIDs[currencyList.getSelectedIndex()]);//(String)currencyList.getSelectedItem());
                table.getTable().updateUI();
            } catch (Exception ex) {
            }
            //dataStore.setBaseCurrency((String)currencyList.getSelectedItem());
        }
    }

    public void setBaseCurrency() {
        try {
            if (currencyList.getSelectedIndex() >= 0) {
                dataStore.setBaseCurrency((String) currencyIDs[currencyList.getSelectedIndex()]);//(String)currencyList.getSelectedItem());
                table.getTable().updateUI();
            }
        } catch (Exception ex) {
        }
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        //currencyIDs = CurrencyStoreOld.getSymbols();
        //currencyList.updateUI();
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

    }

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof javax.swing.plaf.metal.MetalInternalFrameTitlePane) {
            if (SwingUtilities.isRightMouseButton(e)) {
                GUISettings.showPopup(titleBarMenu, this, e.getX(), e.getY());
            }
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        /*if (e.getActionCommand().equals("TITLE_HIDE")){
            ((BasicInternalFrameUI)getUI()).setNorthPane(null);
            table.getModel().getViewSettings().setHeaderVisible(false);
            setHideShowMenuCaption();
            this.revalidate();
        }else*/
        if (e.getActionCommand().equals("TITLE_HIDE")) {
            toggleTitleBar();
        }
    }

    // #########################################################################
    // # Print the Portfolio window
    // #########################################################################
    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent event) {
        /* Check if the drag came form prit button */
        try {
            String key = (String) event.getTransferable().getTransferData(DataFlavor.stringFlavor);
            if (key.equals("Print")) {
                event.getDropTargetContext().dropComplete(true);
            } else {
                event.rejectDrop(); // not from the print button. reject drop
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            event.rejectDrop();
            return;
        }
        printTable();
    }

    /**
     * Invoked when a internal frame has been opened.
     *
     * @see javax.swing.JInternalFrame#show
     */
    public void internalFrameOpened(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is in the process of being closed.
     * The close operation can be overridden at this point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {
        focusNextWindow();
    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {
        try {
            if (table.getTable().isEditing()) {
                table.getTable().getCellEditor().cancelCellEditing();
                //System.err.println("Cancelled");
            }
        } catch (Exception ex) {
        }
    }

    public void printTable() {
        createColMaxWidths();
        PrintPreview p = null;
        p = new PrintPreview(this, "Preview");
    }

    private void createColMaxWidths() {
        int maxColWidth = 0;
        int curColWidth = 0;
        TableColumn oColumn = null;
        JLabel label = null;
        TableModel tblModel = null;
        Font font = new TWFont("Arial", Font.PLAIN, 12);
        FontMetrics fMetrics = table.getFontMetrics(font); //table.getFont());

        maximumWidths = new int[table.getTable().getColumnCount()];
        colXPositions = new int[table.getTable().getColumnCount()];

        int startIdx = 0;
        int endIdx = table.getTable().getColumnCount();
        if (!Language.isLTR()) {
            startIdx = -table.getTable().getColumnCount() + 1;
            endIdx = 1;
        }

        tblModel = table.getTable().getModel();
        for (int column = startIdx; column < endIdx; column++) {
            int col = this.table.getTable().convertColumnIndexToModel(Math.abs(column)); // oColumn.getModelIndex();
            oColumn = this.table.getTable().getColumn(col + ""); // Math.abs(column)+"");
            if (oColumn.getWidth() > 0) {
                maxColWidth = fMetrics.stringWidth((String) oColumn.getHeaderValue());
                for (int row = 0; row < table.getTable().getRowCount(); row++) {
                    curColWidth = 0;
                    label = (JLabel) table.getTable().getCellRenderer(row,
                            col).getTableCellRendererComponent(table.getTable(),
                            tblModel.getValueAt(row, col), false, false, row, col);

                    curColWidth = fMetrics.stringWidth(label.getText());
                    if (maxWidth < curColWidth)
                        curColWidth = maxWidth;
                    if (maxColWidth < curColWidth)
                        maxColWidth = curColWidth;
                    label = null;
                }
                maximumWidths[Math.abs(column)] = maxColWidth;
                if (Language.isLTR()) {
                    if (Math.abs(column) < (table.getTable().getColumnCount() - 1)) {
                        colXPositions[Math.abs(column) + 1] = colXPositions[Math.abs(column)] + maxColWidth + 5;
                    }
                } else {
                    if (Math.abs(column) <= (table.getTable().getColumnCount() - 1)) {
                        if ((Math.abs(column) - 1) > -1)
                            colXPositions[Math.abs(column) - 1] = colXPositions[Math.abs(column)] + maxColWidth + 5;
                    }
                }
            } else {
                //maximumWidths[column][0] = i2;
                maximumWidths[Math.abs(column)] = 0;
                if (Language.isLTR()) {
                    if (Math.abs(column) < (table.getTable().getColumnCount() - 1)) {
                        colXPositions[Math.abs(column) + 1] = colXPositions[Math.abs(column)];
                    }
                } else {
                    if (Math.abs(column) <= (table.getTable().getColumnCount() - 1)) {
                        if ((Math.abs(column) - 1) > -1)
                            colXPositions[Math.abs(column) - 1] = colXPositions[Math.abs(column)];
                    }
                }
            }
        }
    }

    public int print(Graphics pg, PageFormat pageFormat, int pageIndex) throws PrinterException {
        String printTitle = null;
        TableModel tblModel = null;
        int iniRow = 0;
        int endRow = 0;
        JLabel label = null;
        String str = null;
        int wPage = 0;
        int hPage = 0;
        int y = 0;
        int drawPos = 0;
        int nColumns = 0;
        String title = null;
        Font fn = null;
        //FontMetrics fm      = null;
        FontMetrics fMetrics = null;
        Font headerFont = null;
        TableColumnModel colModel = null;
        TableColumn tk = null;

        if (pageIndex >= m_maxNumPage)
            return NO_SUCH_PAGE;

        pg.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
        wPage = (int) pageFormat.getImageableWidth();
        hPage = (int) pageFormat.getImageableHeight(); // + 200;

        pg.setFont(new TWFont("Arial", Font.PLAIN, 12)); // table.getFont());
        pg.setColor(Color.black);
        fn = pg.getFont();
        //fm = pg.getFontMetrics();
        fMetrics = pg.getFontMetrics(); //getTable1().getFontMetrics(table.getFont());

        pg.drawLine(0, y, wPage, y);
        y += fMetrics.getAscent() + 3;
        if (Language.isLTR())
            pg.drawString(getTitle(), 0, y);
        else
            pg.drawString(getTitle(), colXPositions[0] + 30, y);
        //pg.drawString(getTitle(), wPage-80, y);

        printTitle = Language.getString("UNIQUOTES");
        if (Language.isLTR()) {
            //printTitle += " : " + g_oDateFormat.format(new Date(TradeStore.getLastTradedTime()));
            pg.drawString(printTitle, wPage - fMetrics.stringWidth(printTitle), y);
        } else {
            //printTitle = g_oDateFormat.format(new Date(TradeStore.getLastTradedTime())) + " : " + printTitle;
            pg.drawString(printTitle, 10, y);
        }

        y += 10; // space between title and table headers

        // Draw the Default Currency Value
        pg.setFont(fn.deriveFont(Font.PLAIN)); // table.getFont());
        fMetrics = pg.getFontMetrics();
        //y += 3;
        pg.drawLine(0, y, wPage, y);
        y += fMetrics.getAscent() + 3;
        if (Language.isLTR()) {
            str = lblBaseCurrency.getText() + " : " + (String) currencyList.getSelectedItem();
            pg.drawString(str, wPage - fMetrics.stringWidth(str) - 35, y);
        } else {
            str = (String) currencyList.getSelectedItem() + " : " + lblBaseCurrency.getText();
            pg.drawString(str, wPage - 115, y);
        }
        y += fMetrics.getDescent() + 8;

        pg.fillRect(0, y - 5, wPage, 2);

        headerFont = fn.deriveFont(Font.BOLD); //table.getFont().deriveFont(Font.BOLD);
        pg.setFont(headerFont);
        fMetrics = pg.getFontMetrics();

        colModel = table.getTable().getColumnModel();
        nColumns = colModel.getColumnCount();

        int h = fMetrics.getAscent();
        y += h + 5; // add ascent of header font because of baseline
        // positioning

        pg.drawLine(0, y + 2, wPage, y + 2);

        int nRow, nCol, tableCol;
        for (tableCol = 0; tableCol < nColumns; tableCol++) {
            tk = colModel.getColumn(tableCol);
            nCol = tk.getModelIndex();
            int width = tk.getWidth();

            if ((colXPositions[nCol] + width) > wPage) {
                boolean isForceToContinue = false;
                String sMesg = "";
                ShowMessage oMessage = null;
                if (pageFormat.getOrientation() == pageFormat.PORTRAIT) {
                    sMesg = "<html>" + Language.getString("MSG_TOO_MANY_COLUMNS") + "<br>" +
                            Language.getString("MSG_LANDSCAPE_OR_REDUCE_COLS") + "</html>";
                    oMessage = new ShowMessage(Client.getInstance(), sMesg, "I", false);
                } else {
                    sMesg = "<html>" + Language.getString("MSG_TOO_MANY_COLUMNS") + "<br>" +
                            Language.getString("LBL_PRINT_HINT_REDUCE_COLUMNS") + "</html>";
                    oMessage = new ShowMessage(Client.getInstance(), sMesg, "I", false);
                }
                sMesg = null;
                return NO_SUCH_PAGE;
            }

            title = ((String) tk.getHeaderValue()).trim(); //(String)tk.getIdentifier();
            if (maximumWidths[nCol] > 0) {
                drawPos = colXPositions[nCol] + maximumWidths[nCol] - fMetrics.stringWidth(title) + 15;  // + maximumWidths[nCol]
                pg.drawString(title, drawPos, y - 5);
            }
        }

        pg.setFont(fn.deriveFont(Font.PLAIN)); // table.getFont());
        fMetrics = pg.getFontMetrics();

        int header = y;
        h = fMetrics.getHeight();
        int rowH = Math.max((int) (h * 1.1), 10);
        int rowPerPage = (hPage - header) / rowH;
        m_maxNumPage = Math.max((int) Math.ceil(table.getTable().getRowCount() / (double) rowPerPage), 1);

        tblModel = table.getTable().getModel();
        iniRow = pageIndex * rowPerPage;
        endRow = Math.min(table.getTable().getRowCount(), iniRow + rowPerPage);

        for (nRow = iniRow; nRow < endRow; nRow++) {
            y += h;
            int startIdx = 0;
            int endIdx = nColumns;
            int strWidth = 0;
            if (!Language.isLTR()) {
                startIdx = -nColumns + 1;
                endIdx = 1;
            }
            for (nCol = startIdx; nCol < endIdx; nCol++) {
                int col = table.getTable().convertColumnIndexToModel(Math.abs(nCol)); // .getColumnModel().getColumn(Math.abs(nCol)).getModelIndex();
                if (maximumWidths[Math.abs(nCol)] == 0) continue;

                label = (JLabel) table.getTable().getCellRenderer(nRow,
                        col).getTableCellRendererComponent(table.getTable(),
                        tblModel.getValueAt(nRow, col), false, false, nRow, col);
                str = label.getText();

                if (col == 1) {
                    strWidth = fMetrics.stringWidth(str);
                    if (strWidth > maxWidth) {
                        String holder = "";
                        char[] chars = str.toCharArray();
                        for (int count = 0; count < str.length(); count++) {
                            if ((fMetrics.stringWidth(holder + chars[count] + "") >= (maxWidth - 8))) {
                                holder += "...";
                                break;
                            }
                            holder += chars[count] + "";
                        }
                        str = holder;
                        holder = null;
                        chars = null;
                    }
                }
                if (Language.isLTR() && ((col == 0) || (col == 1)))
                    drawPos = colXPositions[Math.abs(nCol)] + 10;
                else
                    drawPos = colXPositions[Math.abs(nCol)] + maximumWidths[Math.abs(nCol)] - fMetrics.stringWidth(str) + 10;  // + maximumWidths[nCol]

                if (nRow == (tblModel.getRowCount() - 1)) {
                    if ((col == 7) || (col == 8) || (col == 9) || (col == 10)) {
                        pg.drawLine(drawPos - 5, y - fMetrics.getAscent() - 3, drawPos + maximumWidths[Math.abs(nCol)] + 5, y - fMetrics.getAscent() - 3);
                        pg.fillRect(drawPos - 5, y + 5, maximumWidths[Math.abs(nCol)] + 10, 2);
                    }
                }
                pg.drawString(str, drawPos, y);
            }
        }
        //if (pageIndex == (m_maxNumPage-1))
        //    pg.fillRect(0, y, wPage-2, 2);

        // draw page number
        if (Language.isLTR())
            pg.drawString(Language.getString("PAGE") + " : " + (pageIndex + 1), (int) wPage / 2 - 35, (int) (hPage) - fMetrics.getDescent());
        else
            pg.drawString((pageIndex + 1) + " : " + Language.getString("PAGE"), (int) wPage / 2 - 35, (int) (hPage) - fMetrics.getDescent());

        System.gc();
        return PAGE_EXISTS;
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void closeWindow() {
//		 if (getDefaultCloseOperation() == HIDE_ON_CLOSE){
//			 setVisible(false);
//		 }else{
//			 dispose();
//		 }
        TWDesktop.doCloseWindow(this);
        focusNextWindow();
    }

    public void doDefaultCloseAction() {
        TWDesktop.doCloseWindow(this);
        focusNextWindow();
    }

//     public void doCloseWindow(){
//         super.doDefaultCloseAction();
//         focusNextWindow();
//        switch (getDefaultCloseOperation()) {
//             case DO_NOTHING_ON_CLOSE:
//                 break;
//             case HIDE_ON_CLOSE:
//                 setVisible(false);
//                 if (isSelected())
//                     try {
//                         setSelected(false);
//                     }
//                     catch (PropertyVetoException pve) {}
//                 break;
//             case DISPOSE_ON_CLOSE:
//                 dispose();
//             default:
//                 break;
//        }
//
//     }

    private void focusNextWindow() {
        TWDesktop desktop = (TWDesktop) Client.getInstance().getDesktop();
        desktop.focusNextWindow(desktop.getListPosition(this, true), true);
        desktop = null;
    }

    public int getDesktopItemType() {
        return BOARD_TYPE;
    }

    public int getWindowType() {
        return windowType;
    }

    public void setWindowType(int type) {
        windowType = type;
    }

}
/*package com.isi.csvr;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.*;

import com.isi.csvr.datastore.*;
import com.isi.csvr.datastore.PortfolioStore;
import com.isi.csvr.table.*;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.properties.*;
import com.isi.csvr.shared.*;

public class PortfolioTable extends JInternalFrame
    implements WindowWrapper, ItemListener,
    PopupMenuListener, ActionListener, MouseListener {

    private PortfolioStore  dataStore;
    private Table           table;
    private JComboBox       currencyList;
    private Object[]        currencyIDs;
    private JLabel          lblBaseCurrency;
    private CurrencyStore   currencyStore;

    private JPopupMenu  titleBarMenu;
    private JPopupMenu  headerPopup;
    private TWMenuItem   fontMenu;
    private TWMenuItem   hideShowTitleMenu;

    public PortfolioTable(){

        /*headerPopup = new JPopupMenu();
        fontMenu = new TWMenuItem(Language.getString("FONT"));
        headerPopup.add(fontMenu);
        fontMenu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showHeaderFontChooser();
                }
            });
        hideShowTitleMenu = new TWMenuItem();
        headerPopup.add(hideShowTitleMenu);
        hideShowTitleMenu.setActionCommand("HEADER");
        hideShowTitleMenu.addActionListener(this);*

        createTitlebarManu();
    }

    private void createTitlebarManu(){
        titleBarMenu = new JPopupMenu();
        TWMenuItem hideTitlemenu = new TWMenuItem(Language.getString("HIDE_TITLEBAR"));
        hideTitlemenu.setActionCommand("TITLE_HIDE");
        hideTitlemenu.addActionListener(this);
        titleBarMenu.add(hideTitlemenu);
        addTitlebarManu();
    }

    public void addTitlebarManu(){
        Component np =  (Component)((BasicInternalFrameUI)getUI()).getNorthPane();
        try{
            np.removeMouseListener(this); // if existing
        }catch(Exception e){}
        np.addMouseListener(this);
    }

    /*private void showHeaderFontChooser(){
        JTable table = this.table.getTable();
        SortButtonRenderer oRend;
        FontChooser oFontChooser = new FontChooser(Client.getInstance(),true);
        Font oFont = null;
        oRend =	(SortButtonRenderer)table.getColumnModel().getColumn(0).getHeaderRenderer();
        oFont = oRend.getFont();
        oFont = oFontChooser.showDialog(Language.getString("SELECT_FONT"),oFont);
        if (oFont != null){
            this.table.getModel().getViewSettings().setHeaderFont(oFont);
            for (int i = 0; i < table.getIndexCount() ; i++)  {
                oRend =	(SortButtonRenderer)table.getColumnModel().getColumn(i).getHeaderRenderer();
                oRend.setHeaderFont(oFont);
                oRend.updateUI();
                oRend = null;
            }
            table.getTableHeader().updateUI();
            table.updateUI();
        }
    }*/

/*private void setHideShowMenuCaption(){
        if (!isTitleVisible()){
            hideShowTitleMenu.setText(Language.getString("SHOW_TITLEBAR") + "xx");
        }else{
            hideShowTitleMenu.setText(Language.getString("HIDE_TITLEBAR") + "yy");
        }
    }*

    private void toggleTitleBar(){
        setTitleVisible(!isTitleVisible());
        //table.getModel().getViewSettings().setHeaderVisible(isTitleVisible());
        //-setHideShowMenuCaption();
    }

    public void revalidateHeader(){
        try{
            if (!table.getModel().getViewSettings().isHeaderVisible()){
                ((BasicInternalFrameUI)getUI()).setNorthPane(null);
                this.revalidate();
            }
            //-setHideShowMenuCaption();
        }
        catch(Exception e){

        }
    }

    public void show(){
        super.show();
        setBaseCurrency();
        revalidateHeader();
    }

    public void updateUI(){
        super.updateUI();
        addTitlebarManu();
        revalidateHeader();
    }

    public void createTable(){
        this.currencyStore = currencyStore;
        //this.currencyStore.addCurrencyListener(this);
        lblBaseCurrency = new JLabel(Language.getString("DEFAULT_CURRENCY"));

        currencyIDs = CurrencyStore.getSymbols();
        currencyList    = new JComboBox(getComboModel());
        currencyList.setPreferredSize(new Dimension(150,25));

        currencyList.addItemListener(this);
        currencyList.addPopupMenuListener(this);
        try {
            currencyList.setSelectedIndex(0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JPanel currencyPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        currencyPanel.add(lblBaseCurrency);
        currencyPanel.add(currencyList);

        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(currencyPanel,BorderLayout.NORTH);


    }

    private DefaultComboBoxModel getComboModel(){
        DefaultComboBoxModel comboModel = new DefaultComboBoxModel()
        {
            public Object getElementAt(int index)
            {
                //return currencyIDs[index];
                String caption = StockMaster.getCurrencyCaption((String)currencyIDs[index]);
                if (caption.equals(""))
                    return currencyIDs[index];
                else
                    return caption;
            }

            public int getSize(){
                return currencyIDs.length;
            }

            /*public Object getSelectedItem(){
                return currencyIDs[currencyList.getSelectedIndex()];
            }*
        };
        return comboModel;
    }
    public void setDataStore(PortfolioStore newDataStore) {
        dataStore = newDataStore;
    }

    public PortfolioStore getDataStore() {
        return dataStore;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;

        //getContentPane().add(table);
        this.getContentPane().add(table,BorderLayout.CENTER);
        table.setPopupActive(false);
    }

    public ViewSetting getViewSettings(){
        return table.getModel().getViewSettings();
    }

    /* Table wrapper methods *

    public boolean isWindowVisible()
    {
        return isVisible();
    }

    public int getWindowStyle()
    {
        if (isMaximum())
            return  ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
    }

    public JTable getTable1(){
        try{
            return table.getTable();
        }catch(Exception e){
            return null;
        }
    }

    public CommonTable getModel1()
    {
        try{
            return table.getModel();
        }catch(Exception e){
            return null;
        }
    }

    public JTable getTable2()
    {
        return null;
    }

    public int getZOrder()
    {
        return getDesktopPane().getPosition(this);
    }

    public void printTable(){
    }

    public void applySettings()
    {
        ViewSetting oSettings = null;
        if (table != null){
            oSettings = table.getModel().getViewSettings();
            table.getModel().applySettings();
            this.setSize(oSettings.getSize());
        }

        this.setLocation(oSettings.getLocation());

        if (oSettings.isVisible())
        {
            this.getDesktopPane().setLayer(this,this.getLayer(),
                oSettings.getIndex());
            this.setVisible(true);
            try
            {
                this.setSelected(true);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            this.getDesktopPane().setLayer(this,
                this.getLayer(), 0);
        }

        try
        {
            switch (oSettings.getStyle())
            {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    this.setIcon(false);
                    break;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        if (oSettings.getLocation() != null)
            this.setLocation(oSettings.getLocation());
        this.setVisible(oSettings.isVisible());
        this.setTitle(oSettings.getCaption());
        revalidateHeader();
        //table.getModel().adjustColumns()
    }

    public boolean isTitleVisible(){
        //if (g_oViewSettings != null){
            return table.getModel().getViewSettings().isHeaderVisible();
        /*}else{
            return true;
        }*
    }

    public void setTitleVisible(boolean setVisible){
        ViewSetting settings = table.getModel().getViewSettings();
        if (setVisible){
                if (settings != null){
                    settings.setHeaderVisible(true);
                }
            updateUI();
        }else{
            Component titleBar = ((BasicInternalFrameUI)getUI()).getNorthPane();
            if(titleBar != null){
                ((BasicInternalFrameUI)getUI()).setNorthPane(null);
                revalidate();
                if (settings != null){
                    settings.setHeaderVisible(false);
                }
            }
        }
        settings = null;
     }
    /* -- *

    public void updateGUI(){
        this.setTitle(table.getModel().getViewSettings().getCaption());
        table.getModel().updateGUI();
    }
    public String toString(){
        return getTitle();
    }

    public void itemStateChanged(ItemEvent e){

        if ((e.getStateChange() == e.SELECTED) && (currencyList.getSelectedItem()!=null)){
            try{
                dataStore.setBaseCurrency((String)currencyIDs[currencyList.getSelectedIndex()]);//(String)currencyList.getSelectedItem());
                table.getTable().updateUI();
            }catch(Exception ex){
            }
            //dataStore.setBaseCurrency((String)currencyList.getSelectedItem());
        }
    }

    public void setBaseCurrency(){
        try{
            if (currencyList.getSelectedIndex() >=0){
                //dataStore.setBaseCurrency((String)currencyList.getSelectedItem());
                dataStore.setBaseCurrency((String)currencyIDs[currencyList.getSelectedIndex()]);
                table.getTable().updateUI();
            }
        }catch(Exception ex){
        }
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e){
        currencyIDs = CurrencyStore.getSymbols();
        //currencyList.updateUI();
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e){

    }

    public void popupMenuCanceled(PopupMenuEvent e){
    }

    public void mouseClicked(MouseEvent e){
        if (e.getSource() instanceof javax.swing.plaf.metal.MetalInternalFrameTitlePane){
            if (SwingUtilities.isRightMouseButton(e)){
                GUISettings.showPopup(titleBarMenu,this,e.getX(),e.getY());
            }
        }
    }

    public void mousePressed(MouseEvent e){}

    public void mouseReleased(MouseEvent e){}

    public void mouseEntered(MouseEvent e){}

    public void mouseExited(MouseEvent e){}

    public void mouseDragged(MouseEvent e){}

    public void actionPerformed(ActionEvent e){
        /*if (e.getActionCommand().equals("TITLE_HIDE")){
            ((BasicInternalFrameUI)getUI()).setNorthPane(null);
            table.getModel().getViewSettings().setHeaderVisible(false);
            setHideShowMenuCaption();
            this.revalidate();
        }else* if (e.getActionCommand().equals("TITLE_HIDE")){
            toggleTitleBar();
        }
    }
}*/
