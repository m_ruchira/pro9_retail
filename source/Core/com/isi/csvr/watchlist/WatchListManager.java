/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 7, 2003
 * Time: 12:19:06 PM
 * To change this template use Options | File Templates.
 */
package com.isi.csvr.watchlist;

import com.isi.csvr.DataDisintegrator;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.WatchlistListener;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.*;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

public class WatchListManager implements ViewConstants {
    private static WatchListManager ourInstance;
    private ArrayList<WatchListStore> stores = null;
    private ArrayList<WatchlistListener> listeners = null;

    private WatchListManager() {
        stores = new ArrayList<WatchListStore>();
        listeners = new ArrayList<WatchlistListener>();
    }

    public synchronized static WatchListManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new WatchListManager();

        }
        return ourInstance;
    }

    public WatchListStore createNewStore(byte type, String caption) {
        WatchListStore store = new WatchListStore("");
        store.setId("" + SharedMethods.getSerialID());
        store.setListType(type);
        store.setCaptions(caption);
        stores.add(store);
        return store;
    }

    //    public WatchListStore createNewStore(String caption) {
    public WatchListStore createNewStore(String caption, int selectedType) {
        WatchListStore store = new WatchListStore("");
        store.setId("" + SharedMethods.getSerialID());
        store.setCaptions(caption);
        store.setSelectedType(selectedType);
        stores.add(store);
        fireWatchlistAdded(store.getId());
        return store;
    }


    /**
     * Returns the portfolio store according to the given id
     *
     * @param id
     * @return selected store or null if not found
     */
    public WatchListStore getStore(String id) {
        WatchListStore store = null;
        for (int i = 0; i < stores.size(); i++) {
            store = (WatchListStore) stores.get(i);
            if (store.getId().equals(id)) {
                return store;
            }
        }
        return null;
    }

    /**
     * Remove the given store from the list
     *
     * @param store
     */
    public void removeStore(WatchListStore store) {
        stores.remove(store);
        stores.trimToSize();
        store.clear();
        fireWatchlistRemoved(store.getId());
    }


    /**
     * Returns an array of portfolio stores
     *
     * @return array of portfolio stores
     */
    public WatchListStore[] getStores() {
        return stores.toArray(new WatchListStore[0]);
    }

    /**
     * Add the given store to the list
     *
     * @param store
     */
    public void setStore(WatchListStore store) {
        stores.add(store);
    }

    /**
     * Create a new store using saved settings
     *
     * @param record
     */
    public void createStore(String record) {

        StringTokenizer fields = new StringTokenizer(record, VC_RECORD_DELIMETER);
        DataDisintegrator pair = new DataDisintegrator();
        pair.setSeperator(VC_FIELD_DELIMETER);
        WatchListStore store = new WatchListStore("");
        //store.setId(Settings.get);

        String value;
        while (fields.hasMoreTokens()) {
            try {
                pair.setData(fields.nextToken());
                value = pair.getData();

                switch (SharedMethods.intValue(pair.getTag(), 0)) {
                    case VC_TYPE:
                        int newListType = SharedMethods.intValue(value, 0);
                        store.setListType((byte) newListType);
                        break;

                    case VC_ID:
                        store.setId(value);
                        break;

                    case VC_CAPTIONS:
                        store.setCaptions(value);
                        break;

                    case VC_SYMBOLS:
                        store.setSymbols(getSymbolsArray(value));
                        break;

                    case VC_TITLE_LANG_ID:
                        store.setCaptions(Language.getString(value));
                        break;
//                    case VC_TITLE_CAPTION :
//                       store.setCaptionsBylanguage(value);
//                        break;

                    case VC_WATCHLIST_TYPE:
                        int watchListType = SharedMethods.intValue(value, 0);
                        store.setSelectedType(watchListType);
                        break;

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        stores.add(store);
        store = null;
        fields = null;
        pair = null;
    }

    private String[] getSymbolsArray(String symbols) {
        try {
            return symbols.split(",");
        } catch (Exception e) {
            return new String[0];
        }
    }

    public void removeSymbols(String exchange) {

        for (int index = 0; index < stores.size(); index++) {
            WatchListStore symbols = stores.get(index);
            String[] indexArray = symbols.getSymbols().clone();
            for (int i = 0; i < indexArray.length; i++) {
                if (exchange.equals(SharedMethods.getExchangeFromKey(indexArray[i]))) {
                    symbols.removeSymbol(indexArray[i]);
                }
            }
            indexArray = null;
            symbols = null;
        }
    }


    public void removeSymbolsIfNotValidExchange() {  /*To remove symbols which are not in both auth and saved list. Issue in Egypt V-9 - GDR-Symbols
                                                                                                2009-07-14 -- Shanika*/
        for (int index = 0; index < stores.size(); index++) {
            WatchListStore symbols = stores.get(index);
//            symbols.reFilter();
            String[] indexArray = symbols.getSymbols().clone();
            for (int i = 0; i < indexArray.length; i++) {
                if (!ExchangeStore.getSharedInstance().isValidExchange(SharedMethods.getExchangeFromKey(indexArray[i]))) {
                    symbols.removeSymbol(indexArray[i]);
                }
            }
            indexArray = null;
            symbols = null;
        }
    }

    public void save() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/watchlist.mdf");
            WatchListStore store = null;
            for (int i = 0; i < stores.size(); i++) {
                store = (WatchListStore) stores.get(i);
                out.write(store.toString().getBytes());
                out.write("\r\n".getBytes());
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTempWatchList() {
        BufferedReader in = null;
        BufferedReader intemp = null;
        File tempFile = new File(Settings.getAbsolutepath() + "temp/watchList.mdf");
        try {
            if (tempFile.exists()) {

                File newFile = new File(Settings.getAbsolutepath() + "datastore/watchlist.mdf");
                if (!newFile.exists()) {
                    newFile.createNewFile();
                }
                intemp = new BufferedReader(new FileReader(tempFile));
                in = new BufferedReader(new FileReader(newFile));
                Vector watch = new Vector();
                Vector watchtemp = new Vector();
                String strWatch, strTemp;
                while ((strWatch = in.readLine()) != null) {
                    watch.add(strWatch);
                }
                in.close();
                while ((strTemp = intemp.readLine()) != null) {
                    watchtemp.add(strTemp);
                }
                intemp.close();
                BufferedWriter outBuffer = new BufferedWriter(new FileWriter(newFile));
                for (int i = 0; i < watch.size(); i++) {
                    //watchBuff.add(string + "\n");
                    String string = (String) watch.elementAt(i);
                    String type = string.substring(2, 3);
                    String[] str1 = string.split("10=");
                    int int1 = str1[1].indexOf(";");
                    String str2 = str1[1].substring(0, int1);
                    int index = 0;
                    String sub;
                    for (int j = 0; j < watchtemp.size(); j++) {
                        sub = (String) watchtemp.elementAt(j);
                        String iType = sub.substring(2, 3);
                        String[] sub1 = sub.split("10=");
                        int int2 = sub1[1].indexOf(";");
                        String sub2 = sub1[1].substring(0, int2);

                        if ((sub2.equals(str2)) && (type.equals(iType))) {
                            break;
                        } else {
                            index = index + 1;
                        }
                    }
                    if (index == watchtemp.size()) {
                        //System.out.println("str2");
                        watchtemp.add(string);
                    }
                }
                for (int k = 0; k < watchtemp.size(); k++) {
                    String outputString = adjustWatchListStringForInstrumentType((String) (watchtemp.elementAt(k)));
                    outBuffer.write(outputString + " \n");
                }
                outBuffer.close();
                tempFile.delete();
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //loading conditional watchlist from Temp
        File tempConFile = new File(Settings.getAbsolutepath() + "temp/functionlist.mdf");
        try {
            if (tempConFile.exists()) {

                File newConFile = new File(Settings.getAbsolutepath() + "datastore/functionlist.mdf");
                if (!newConFile.exists()) {
                    newConFile.createNewFile();
                }
                intemp = new BufferedReader(new FileReader(tempConFile));
                in = new BufferedReader(new FileReader(newConFile));
                Vector watch = new Vector();
                Vector watchtemp = new Vector();
                String strWatch, strTemp;
                while ((strWatch = in.readLine()) != null) {
                    watch.add(strWatch);
                }
                in.close();
                while ((strTemp = intemp.readLine()) != null) {
                    watchtemp.add(strTemp);
                }
                intemp.close();
                BufferedWriter outBuffer = new BufferedWriter(new FileWriter(newConFile));
                for (int i = 0; i < watch.size(); i++) {
                    String string = (String) watch.elementAt(i);
                    String[] str1 = string.split("10=");
                    int int1 = str1[1].indexOf(";");
                    String str2 = str1[1].substring(0, int1);
                    int index = 0;
                    String sub;
                    for (int j = 0; j < watchtemp.size(); j++) {
                        sub = (String) watchtemp.elementAt(j);
                        String[] sub1 = sub.split("10=");
                        int int2 = sub1[1].indexOf(";");
                        String sub2 = sub1[1].substring(0, int2);

                        if (sub2.equals(str2)) {
                            break;
                        } else {
                            index = index + 1;
                        }
                    }
                    if (index == watchtemp.size()) {
                        watchtemp.add(string);
                    }
                }
                for (int k = 0; k < watchtemp.size(); k++) {

                    outBuffer.write((String) (watchtemp.elementAt(k)) + " \n");

                }
                outBuffer.close();
                tempConFile.delete();
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void load() {
        String data;
        try {
            loadTempWatchList();
            DataInputStream in = new DataInputStream(new FileInputStream(Settings.getAbsolutepath() + "datastore/watchlist.mdf"));
            while (true) {
                data = in.readLine();
                if ((data == null) || (data.trim().equals(""))) break;
                createStore(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addWatchlistListener(WatchlistListener source) {
        listeners.add(source);
    }

    public void removeWatchlistListener(WatchlistListener source) {
        listeners.remove(source);
    }

    public void fireSymbolAdded(String key, String listID) {
        for (WatchlistListener listener : listeners) {
            try {
                listener.symbolAdded(key, listID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void fireSymbolRemoved(String key, String listID) {
        for (WatchlistListener listener : listeners) {
            try {
                listener.symbolRemoved(key, listID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void fireWatchlistAdded(String listID) {
        for (WatchlistListener listener : listeners) {
            try {
                listener.watchlistAdded(listID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void fireWatchlistRemoved(String listID) {
        for (WatchlistListener listener : listeners) {
            try {
                listener.watchlistRemoved(listID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void fireWatchlistRenamed(String listID) {
        for (WatchlistListener listener : listeners) {
            try {
                listener.watchlistRenamed(listID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private String adjustWatchListStringForInstrumentType(String wlString) {
        String retString = "";
        if (wlString != null && !wlString.isEmpty()) {
            try {
                String[] main1 = wlString.split("10=");
                String[] main2 = main1[1].split(";");
                String[] symbols = main2[1].split(",");
                String type = main2[2];
//                String[] type = main2[2].split(";");
                if (symbols != null && symbols.length > 0) {
                    String symbolString = "";
                    for (int i = 0; i < symbols.length; i++) {
                        String key = symbols[i];
                        if (!SharedMethods.isFullKey(key)) {
                            key = key.trim();
                            key = key + Constants.KEY_SEPERATOR_CHARACTER + Meta.INSTRUMENT_EQUITY;

                        }
                        if (i < symbols.length - 1) {
                            symbolString = symbolString + key + ",";
                        } else {
                            symbolString = symbolString + key;
                        }
                    }
                    System.out.println("Adjusted string = " + symbolString);
                    retString = main1[0] + "10=" + main2[0] + ";" + symbolString + ";" + type;
                    return retString;
                } else {
                    return wlString;
                }
            } catch (Exception e) {
                return wlString;
            }


        } else {
            return wlString;
        }

    }
}

