package com.isi.csvr.TradeStation;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Oct 19, 2010
 * Time: 10:47:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class TradeStationConstants {
    public static final String ATTRIB_FILE_LINE_1 = "Symbol,Description,Date Format,Category,Exchange,Session 1 Start Time," +
            "Session 1 End Time,Session 1 Days,Price Scale,Minimum Movement,Big Point Value,Daily Limit\n";
    public static final String ATTRIB_FILE_LINE_2 = "<SYMBOL>,<DESC>,YYYYMMDD,Stock,<EXCHANGE>,0300,1700,UMTWRFS,<PRICE_SCALE>,1,65000,10000\n";
    public static final String ATTRIB_FILE_NAME = "attributes.INI";
}
