package com.isi.csvr.TradeStation;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.metastock.MetaStockHistoryUpdator;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Oct 19, 2010
 * Time: 11:03:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class TradeStationManager {
    public static ArrayList<String> autoUpdateEnableMarkets;
    private static Hashtable<String, TradeStationIntrdayUpdator> updators = new Hashtable<String, TradeStationIntrdayUpdator>();
    private static TradeStationManager ourInstance = new TradeStationManager();

    private TradeStationManager() {
        autoUpdateEnableMarkets = Settings.autoUpdateEnableMarketsTS;
    }

    public static synchronized TradeStationManager getInstance() {
        return ourInstance;
    }

    public synchronized static void startIntrdayUpdator(String exchange) {

        if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION))
            return;

        if (autoUpdateEnableMarkets.contains(exchange)) {
            TradeStationIntrdayUpdator tsIntrdayUpdator = updators.get(exchange);

            if (tsIntrdayUpdator == null) {
                tsIntrdayUpdator = new TradeStationIntrdayUpdator(exchange);
                updators.put(exchange, tsIntrdayUpdator);
                tsIntrdayUpdator.start();
            } else {
                tsIntrdayUpdator.clear();
                tsIntrdayUpdator.setActive(true);
            }
        }
    }

    public void updateHistory(String exchange) {
        TradeStationHistoryUpdator tsStock2HistoryUpdator = new TradeStationHistoryUpdator(MetaStockHistoryUpdator.HISTORY,
                exchange, MetaStockHistoryUpdator.AUTO, true);
        tsStock2HistoryUpdator.update();
        tsStock2HistoryUpdator = null;
    }

    public void updateIntrdayHistory(String exchange) {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION)) {
            TradeStationIntrdayUpdator tsIntrdayUpdator = updators.get(exchange);

            if (tsIntrdayUpdator != null) {
                tsIntrdayUpdator.updateHistory();
                tsIntrdayUpdator.saveProperties();
            }

        }
    }

    public void addIntrdayRecord(IntraDayOHLC record) {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION)) {
            String exchange = SharedMethods.getExchangeFromKey(record.getSymbol());
            TradeStationIntrdayUpdator tsIntrdayUpdator = updators.get(exchange);
            if (tsIntrdayUpdator != null) {
                tsIntrdayUpdator.addRecord(record);
            }
        }
    }

    public void updateIntrdayHistory(String exchange, String date) {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION)) {
            TradeStationIntrdayUpdator tsIntrdayUpdator = updators.get(exchange);

            if (tsIntrdayUpdator != null) {
                tsIntrdayUpdator.updateHistory(date);
            }
        }
    }
}
