package com.isi.csvr.TradeStation;

import com.isi.csvr.SymbolMaster;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Oct 19, 2010
 * Time: 11:05:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class TradeStationIntrdayUpdator extends Thread {
    public static final String TS_LOCK = "TS_LOCK";
    private static Properties symbolinfo;
    private String exchange;
    private Hashtable<String, List> store;
    private boolean active;
    private boolean exiting = false;
    private String path, historyPath;
    private ArrayList<String> TSSymbolList;

    public TradeStationIntrdayUpdator(String exchange) {
        this.exchange = exchange;
        store = new Hashtable<String, List>();
        path = Settings.TRADE_SATION_DB_PATH + "\\Intraday\\" + exchange;
        historyPath = Settings.TRADE_SATION_DB_PATH + "\\History\\" + exchange;

        TSSymbolList = new ArrayList<String>();
        createFolder();
        loadTSFiles();
        initProperties();

        active = true;
        addExitThread();
    }

    private static String getTSIntradayRecord(TradeStationOHLCRecord ohlc, String exchange) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");

        Date dateTime = new Date();
        dateTime.setTime(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, ohlc.getTime()));
        return dateFormatter.format(dateTime) + "," + timeFormatter.format(dateTime) + "," + ohlc.getOpen() + "," +
                ohlc.getHigh() + "," + ohlc.getLow() + "," + ohlc.getClose() + "," + ohlc.getVolume() + "\n";

    }

    private void loadTSFiles() {
        File folder = new File(path);
        for (File file : folder.listFiles()) {
            TSSymbolList.add(file.getName().split(".txt")[0]);
        }

    }

    public void saveProperties() {
        try {
            File file = new File(Settings.TRADE_SATION_DB_PATH + "\\Intraday\\Settings\\" + exchange + ".dll");
            if (!file.exists())
                file.createNewFile();

            FileOutputStream oOut = new FileOutputStream(file);
            symbolinfo.store(oOut, "");
            oOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initProperties() {
        symbolinfo = new Properties();
        try {
            File file = new File(Settings.TRADE_SATION_DB_PATH + "\\Intraday\\Settings\\" + exchange + ".dll");
            FileInputStream oIn = new FileInputStream(file);
            symbolinfo.load(oIn);
            oIn.close();

            //Delete file after reading
            file.delete();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void createFolder() {
        try {
            File folder = new File(path);
            path = folder.getCanonicalPath();
            folder.mkdirs();
            folder = null;

            File historyFolder = new File(historyPath);
            historyPath = historyFolder.getCanonicalPath();
            historyFolder.mkdirs();
            historyFolder = null;

            File settingsfolder = new File(Settings.TRADE_SATION_DB_PATH + "\\Intraday\\Settings");
            settingsfolder.mkdirs();
            settingsfolder = null;

            TradeStationSharedMethods.writeAttribFile(path, exchange);
            TradeStationSharedMethods.writeAttribFile(historyPath, exchange);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateHistory() {
        System.out.println("TradeStation Intrday History manual update Started ");
        File root = new File(Settings.getAbsolutepath() + "ohlc/" + exchange);
        File[] folders = root.listFiles();
        for (File folder : folders) {
            if (folder.isDirectory()) {
                updateHistory(folder.getName());
            }
        }
        processOHLC();
        System.out.println("TradeStation Intrday History manual update Completed ");
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List getRecordsFor(String symbol) {
        return store.get(symbol);
    }

    public void run() {
        System.out.println("Meta Updator Active " + exchange);
        while (true) {
            try {
                Thread.sleep(15000); // update every 15 sec
            } catch (InterruptedException e) {
            }

            if ((Settings.isConnected()) && (ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION))) {
                synchronized (TS_LOCK) {
                    processOHLC();
                }
            }
        }
    }

    private synchronized void processOHLC() {
        try {
            if (!isActive()) return;

            Enumeration<String> keys = store.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                try {
                    if (exiting) { // client is closing
                        System.out.println("TS Exit");
                        break;
                    }

                    final String symbol = SharedMethods.getSymbolFromKey(key);
                    List records = getRecordsFor(key);
                    if ((records != null) && (records.size() > 0)) {

                        String fileName = TradeStationSharedMethods.getTSFileName(symbol, getCompanyName(key));
                        String filePath = path + "\\" + fileName;
                        if (!TSSymbolList.contains(symbol)) {
                            TradeStationSharedMethods.TSaddSecurity(filePath, true);
                            TSSymbolList.add(symbol);
                        }
                        if (TSSymbolList.contains(symbol)) {
                            long time = addTSIntraRecords(fileName, records, key);
                            symbolinfo.put(fileName, String.valueOf(time));
                        }

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }
            }


        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }

    }

    public long addTSIntraRecords(String fileName, List records, String key) {

        List<TradeStationOHLCRecord> TSrecordList = new ArrayList<TradeStationOHLCRecord>();
        try {
            while (true) {
                IntraDayOHLC ohlc = (IntraDayOHLC) records.remove(0);
                TradeStationOHLCRecord record = new TradeStationOHLCRecord(ohlc.getTime() * 60000L, ohlc.getOpen(),
                        ohlc.getHigh(), ohlc.getLow(), ohlc.getClose(), ohlc.getVolume());
                TSrecordList.add(record);

            }
        } catch (IndexOutOfBoundsException e) {
            // indicates no more records
        }

        //sort by time
        Collections.sort(TSrecordList);

        long firstTimeInRecordList = TSrecordList.get(0).getTime();
        boolean sorted = false;
        if (symbolinfo.get(fileName) != null) {
            long lastTimeInFile = Long.parseLong(symbolinfo.get(fileName).toString());
            sorted = lastTimeInFile < firstTimeInRecordList;
        }

        if (sorted) { //sorted so we can write
            return writeSortedOHLCrecords(path + "\\" + fileName, TSrecordList);
        } else {    //unsorted
            return writeUnsortedRecords(path + "\\" + fileName, key);
        }

    }

    private long writeUnsortedRecords(String filePath, String key) {
        DynamicArray arr = OHLCStore.getInstance().getFullIntradayHistory(key);
        List<TradeStationOHLCRecord> TSrecordList = new ArrayList<TradeStationOHLCRecord>();
        for (int i = 0; i < arr.size(); i++) {
            IntraDayOHLC ohlc = (IntraDayOHLC) arr.get(i);
            TradeStationOHLCRecord record = new TradeStationOHLCRecord(ohlc.getTime() * 60000L, ohlc.getOpen(),
                    ohlc.getHigh(), ohlc.getLow(), ohlc.getClose(), ohlc.getVolume());
            TSrecordList.add(record);
            ohlc = null;
        }
        //Collections.sort(TSrecordList);
        File file = new File(filePath);
        file.delete();
        TradeStationSharedMethods.TSaddSecurity(filePath, true);
        return writeSortedOHLCrecords(filePath, TSrecordList);

    }

    private long writeSortedOHLCrecords(String path, List<TradeStationOHLCRecord> TSrecordList) {
        long lastDateTime = 0;
        File file = new File(path);
        if (file.exists()) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(file, true));
                for (TradeStationOHLCRecord record : TSrecordList) {
                    String TSrecord = getTSIntradayRecord(record, exchange);
                    writer.append(TSrecord);
                    writer.flush();
                    lastDateTime = record.getTime();
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return lastDateTime;
    }

    public String getCompanyName(String key) {
        try {
            if (DataStore.getSharedInstance().isValidSymbol(key)) {
                String companyName = SymbolMaster.getEnglishCompanyName(key);
                if (companyName != null) {
                    return companyName;
                } else {
                    return SharedMethods.getSymbolFromKey(key);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public synchronized void updateHistory(String date) {

        System.out.println("------- UPDATING HISTORY ------->>>>>>>>>>>> " + date);
        String symbol;
        String data;
        int instrument = -1;
        Enumeration symbols = DataStore.getSharedInstance().getSymbols(exchange);
        while (symbols.hasMoreElements()) {
            data = (String) symbols.nextElement();
            symbol = SharedMethods.getSymbolFromExchangeKey(data);
            instrument = SharedMethods.getInstrumentFromExchangeKey(data);
            try {
                updateHistryForSymbol(exchange, symbol, instrument, date);
            } catch (Exception ex) {
            }
        }

    }

    public void clear() {
        try {
            store.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addRecord(IntraDayOHLC record) {
        List list = store.get(record.getSymbol());
        if (list == null) {
            list = Collections.synchronizedList(new LinkedList());
            store.put(record.getSymbol(), list);
        }
        list.add(record);
    }

    private synchronized void updateHistryForSymbol(String exchange, String symbol, int instrument, String date) {
        try {
            String data;
            String[] ohlcData;

            File source = new File(Settings.getAbsolutepath() + "ohlc/" + exchange + "/" + date + "/" + symbol + ".csv");
            String key = SharedMethods.getKey(exchange, symbol, instrument);

            BufferedReader in = new BufferedReader(new FileReader(source));
            while (true) {
                data = in.readLine();
                if (data == null) break;
                ohlcData = data.split(Meta.FS);
                IntraDayOHLC record = new IntraDayOHLC(key, Long.parseLong(ohlcData[0]),
                        Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
                        Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
                        Long.parseLong(ohlcData[5]), 0, 0);
                addRecord(record);
                record = null;
            }
            in.close();
            in = null;
            source = null;
            Thread.sleep(1);
        } catch (Exception e) {
        }
    }

    public void addExitThread() {

        final TradeStationIntrdayUpdator self = this;
        Thread t = new Thread("TradeStationIntrdayUpdator") {
            public void run() {
                try {
                    exiting = true;
                    System.out.println("TS Exit state");
                    saveProperties();
                    synchronized (self) {
                        if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION)) return;
                        active = false;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        Runtime.getRuntime().addShutdownHook(t);
    }

}
