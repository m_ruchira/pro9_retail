package com.isi.csvr.TradeStation;

import com.isi.csvr.Client;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.history.ExportListener;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Oct 18, 2010
 * Time: 3:43:48 PM
 * To change this template use File | Settings | File Templates.
 */

public class TradeStationHistoryUpdator implements Runnable {
    public static final short HISTORY = 0;

    public static final int AUTO = 0;
    public static final int MANUAL = 1;
    public static final String TRADE_LOCK = "TradeLock";
    WorkInProgressIndicator workIn = null;
    private int processType;
    private String start;
    private String end;
    private String destination;
    private ExportListener exportListener;
    private short type;
    private String exchange;
    private boolean deleteOldData;
    private ArrayList<String> TSSymbolList;
    private String path;

    public TradeStationHistoryUpdator(short type, final String exchange, final int processType, boolean deleteOldData) {
        this(type, exchange, processType, "0", "99999999", null, deleteOldData, null);
    }

    public TradeStationHistoryUpdator(short type, final String exchange, final int processType,
                                      final String start, final String end,
                                      final String destination, boolean deleteOldData, final ExportListener exportListener) {
        this.type = type;
        this.start = start;
        this.end = end;
        this.exchange = exchange;
        this.processType = processType;
        this.destination = destination;
        this.exportListener = exportListener;
        this.deleteOldData = deleteOldData;

        path = Settings.TRADE_SATION_DB_PATH + "\\History\\" + exchange;
        TSSymbolList = new ArrayList<String>();
        createFolder();
        loadTSFiles();
    }

    public void run() {
        update();
    }

    private void createFolder() {
        try {
            File folder = new File(path);
            path = folder.getCanonicalPath();
            folder.mkdirs();
            folder = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTSFiles() {
        File folder = new File(path);
        for (File file : folder.listFiles()) {
            TSSymbolList.add(file.getName());
        }

    }

    public void update() {

        if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION))
            return;

        long startTime = System.currentTimeMillis();
        synchronized (TRADE_LOCK) {
            try {
                System.out.println("[Updating TradeStation " + exchange + "]" + SharedMethods.getMemoryDetails());
                Client.getInstance().getMenus().setTradeStationUpdateInProgress(true);
                System.out.println("Starting TradeStation update for " + exchange);

                if (deleteOldData) {
                    TradeStationSharedMethods.TSdeleteOldData(path, exchange);
                    TSSymbolList.clear();
                }

                Enumeration symbols = DataStore.getSharedInstance().getSymbols(exchange);
                while (symbols.hasMoreElements()) {
                    String data = (String) symbols.nextElement();
                    String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                    int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                    String key = SharedMethods.getKey(exchange, symbol, instrument);
                    try {

                        boolean result = false;
                        String fileName = TradeStationSharedMethods.getTSFileName(symbol, getCompanyName(key));
                        String filePath = path + "\\" + fileName;
                        if (!TSSymbolList.contains(symbol)) {
                            result = TradeStationSharedMethods.TSaddSecurity(filePath, false);
                            TSSymbolList.add(symbol);
                        }

                        if (result) {
                            if (type == HISTORY) {
                                File[] paths = HistoryFilesManager.getHistoryPaths(exchange);
                                for (int j = 0; j < paths.length; j++) {
                                    TradeStationSharedMethods.addTSRecords(paths[j].getAbsolutePath() + "\\" + symbol + ".csv", filePath);
                                }
                                paths = null;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                    }
                }

                if (exportListener != null) {
                    exportListener.exportCompleted(null);

                }

                /*if ((processType == MANUAL) && (exportListener == null) && (type == HISTORY)) {
                    if (SystemTrayManager.isSupported()) {
                        SystemTrayManager.showMessage(Language.getString("METASTOCK_HISTORY_DB_UPDATED"));
                    } else {
                        new ShowMessage(Language.getString("METASTOCK_HISTORY_DB_UPDATED"), "I");
                    }
                }*/

            } catch (Exception e) {
                e.printStackTrace();
            }

            Client.setDownloadStatusProgress(0);
            Client.getInstance().getMenus().setTradeStationUpdateInProgress(false);
        }

        start = null;
        end = null;
        destination = null;
        exportListener = null;

        System.out.println("[Updating TradeStation completed " + exchange + "]" + SharedMethods.getMemoryDetails());
        System.out.println("--------->>>>>TradeStation Calc time = " + (System.currentTimeMillis() - startTime));

    }

    public String getCompanyName(String key) {
        try {
            if (DataStore.getSharedInstance().isValidSymbol(key)) {
                String companyName = SymbolMaster.getEnglishCompanyName(key);
                if (companyName != null) {
                    return companyName;
                } else {
                    return SharedMethods.getSymbolFromKey(key);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
