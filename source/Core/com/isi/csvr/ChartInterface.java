package com.isi.csvr;

import com.isi.csvr.cashFlowWatch.CashFlowHistoryDetailUI;
import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.ObjectBidAskTag;
import com.isi.csvr.chart.chartobjects.ObjectOrderLine;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.ChartTable;
import com.isi.csvr.table.ChartTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;
import java.util.TimeZone;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class ChartInterface {
    public static boolean isFirstTimeChartLoading = true;
    private static TimeZone prevTimeZone;
    private static int maximumChartCount = 1000;

    public ChartInterface() {
    }

    public static int getMaximumChartCount() {
        return maximumChartCount;
    }

    public static void setMaximumChartCount(int val) {
        maximumChartCount = val;
    }

    public static String validateSymbol(boolean isUpdateStores, byte windowType,
                                        byte type, String symbol, Object caller, boolean isFromTemplate) {
        String key = "";
        if (ExchangeStore.isNoneDefaultExchangesAvailable()) {
            String requestID = "Chart_New_Symbol:" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol, requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, caller);
        } else {
            //String exchange = SymbolMaster.getExchangeForSymbol(symbol); // find the exchange for this symbol //bug id <#????>
            key = SymbolMaster.getExchangeForSymbol(symbol, false); // find the key for this symbol
            //String key = DataStore.getSharedInstance().searchSymbol(symbol, false); //Bug ID <#0016>
            if (key != null) {
                ((GraphFrame) caller).procesSymbolResponse(key, isFromTemplate);
            } else {
                ((GraphFrame) caller).procesSymbolResponse(null, isFromTemplate);
                new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");
                return null;
            }
        }
        return key;
    }

    public static void showChart(String symbol) throws Exception {

        if (symbol.equals("")) {
            symbol = Client.getInstance().getSelectedSymbol();

            if (!symbol.equals(""))
                Client.getInstance().showChart(symbol, null, null);
            symbol = null;
        } else if (ExchangeStore.isNoneDefaultExchangesAvailable()) {
            String requestID = "Chart_Navigator:" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol, requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, "N/A");
        } else { //Bug ID <#0016>
            String key = SymbolMaster.getExchangeForSymbol(symbol, false); // find the exchange for this symbol
            if (key == null) {
                JOptionPane.showMessageDialog(Client.getInstance().getFrame(), Language.getString("INVALID_SYMBOL") +
                        symbol, Language.getString("ERROR"), JOptionPane.OK_OPTION);
            } else {
                Client.getInstance().showChart(key, null, null);
            }
        }
        /*} else if (!DataStore.getSharedInstance().isValidSymbol(key)) {
            JOptionPane.showMessageDialog(Client.getInstance().getFrame(), Language.getString("INVALID_SYMBOL") +
                    key, Language.getString("ERROR"), JOptionPane.OK_OPTION);
        } else {
            key = DataStore.getSharedInstance().searchSymbol(key, false);
            Client.getInstance().showProChart(key, null, null);
        }*/
    }

//    public ArrayList< > getCashFlowHistoryDetail(){
//        ArrayList<>   histDetail = CashFlowHistoryDetailUI.getSharedInstance().getHistoryDetailArray();
//        return
//    }

    public static String getTitleStr(String symbol, boolean currentMode,
                                     long interval, int graphStyle,
                                     long beginTime, long endTime) {
        String s1, s3, s5, s6, s7;
        String s0 = DataStore.getSharedInstance().getCompanyName(symbol) + " ";
        String s2 = " " + Language.getString("FROM") + " ";
        String s4 = " " + Language.getString("TO") + " ";
        //String sE = ".";
        Date dd;
        TWDateFormat formatter;
        if (currentMode) {
            formatter = new TWDateFormat("dd MMM HH:mm");
            s1 = Language.getString("INTRADAY") + " ";
        } else {
            formatter = new TWDateFormat("dd MMM yyyy");
            s1 = Language.getString("HISTORY") + " ";
        }

        dd = new Date(beginTime);
        s3 = formatter.format(dd);
        dd = new Date(endTime);
        s5 = formatter.format(dd);
        s6 = "  (" + getGraphStyleString(graphStyle);
        s7 = ")  " + getIntervalString(interval);

        return s0 + s1 + s2 + s3 + s4 + s5 + s6 + s7;
    }

    public static String getTitleStr(String symbol, boolean currentMode,
                                     long interval, int graphStyle,
                                     long beginTime, long endTime, boolean isMode, boolean isPeriod, boolean isInterval) {
        String s1 = "", s3 = "", s5 = "", s6 = "", s7 = "";
        String s0 = DataStore.getSharedInstance().getCompanyName(symbol) + " ";
        String s2 = " " + Language.getString("FROM") + " ";
        String s4 = " " + Language.getString("TO") + " ";
        //String sE = ".";
        Date dd;
        TWDateFormat formatter = null;
        if (currentMode && isMode) {
            formatter = new TWDateFormat("dd MMM HH:mm");
            s1 = Language.getString("INTRADAY") + " ";
        } else if (isMode) {
            formatter = new TWDateFormat("dd MMM yyyy");
            s1 = Language.getString("HISTORY") + " ";
        } else {
            formatter = new TWDateFormat("dd MMM yyyy");
        }

        if (isPeriod) {
            dd = new Date(beginTime);
            s3 = formatter.format(dd);
            dd = new Date(endTime);
            s5 = formatter.format(dd);
        } else {
            s2 = "";
            s4 = "";
        }

        s6 = " (" + getGraphStyleString(graphStyle) + ") ";

        if (isInterval) {
            s7 = getIntervalString(interval);
        }
        return s0 + s1 + s2 + s3 + s4 + s5 + s6 + s7;
    }

    public static String getGraphStyleString(int graphStyle) {
        switch (graphStyle) {
            case StockGraph.INT_GRAPH_STYLE_LINE:
                return Language.getString("LINE");
            case StockGraph.INT_GRAPH_STYLE_BAR:
                return Language.getString("BAR");
            case StockGraph.INT_GRAPH_STYLE_HLC:
                return Language.getString("HLC");
            case StockGraph.INT_GRAPH_STYLE_OHLC:
                return Language.getString("OHLC");
            case StockGraph.INT_GRAPH_STYLE_CANDLE:
                return Language.getString("CANDLE");
        }
        return "";
    }

    public static String getIntervalString(long interval) {
        switch ((int) interval) {
            case (int) StockGraph.YEARLY:
                return Language.getString("1_YEAY");
            case (int) StockGraph.QUARTERLY:
                return Language.getString("3_MONTH");
            case (int) StockGraph.MONTHLY:
                return Language.getString("MONTHLY");
            case (int) StockGraph.WEEKLY:
                return Language.getString("WEEKLY");
            case (int) StockGraph.DAILY:
                return Language.getString("DAILY");
            case (int) StockGraph.EVERYMINUTE:
                return Language.getString("EVERY_MINUTE");
            case (int) StockGraph.EVERY10MINUTES:
                return Language.getString("10_MIN");
            case (int) StockGraph.EVERY5MINUTES:
                return Language.getString("5_MIN");
            case (int) StockGraph.EVERY30MINUTES:
                return Language.getString("30_MIN");
            case (int) StockGraph.EVERY15MINUTES:
                return Language.getString("15_MIN");
            case (int) StockGraph.EVERY60MINUTES:
                return Language.getString("60_MIN");
        }
        return "";
    }

    public static Stock getStockObject(String key) {
        return DataStore.getSharedInstance().getStockObject(key);
    }

    public static long getIndexTime() {
        return Client.getInstance().getMarketTime();
    }

//    public static Index getIndexObject(String key) {
//        if (IndexStore.isValidIndex(key))
//            return IndexStore.getIndexObject(key);
//        else
//            return null;
//    }

    public static String getCompanyName(String symbol) {
        if (DataStore.getSharedInstance().isValidSymbol(symbol)) {
            return DataStore.getSharedInstance().getStockObject(symbol).getLongDescription();
        }

        return "";
    }

    public static boolean isOfflineDataAvailable() {
        return true;
    }

    public static boolean isHistoryEnabled() {
        return ExchangeStore.isValidSystemFinformationType(Meta.IT_HistoryCharting) ||
                ExchangeStore.isValidSystemFinformationType(Meta.IT_ProfessionalCharting);
    }

    public static boolean isIndicatorsEnabled() {
//        return ExchangeStore.isValidSystemFinformationType(Meta.IT_ProfessionalCharting);
        return true;
    }

    public static String[] searchSymbols() {
//		Symbols symbols = new Symbols();
//		Companies oCompanies = new  Companies(false);
//		oCompanies.setTitle("");
//		oCompanies.setSingleMode(true);
//		oCompanies.setSymbols(symbols);
//		oCompanies.showDialog();
//		String[] result = symbols.getSymbols();
//		oCompanies = null;
//		symbols = null;
//		return result;

        return SharedMethods.searchSymbols(Language.getString("UNICHARTS"), true, true);

        /* Symbols symbols = new Symbols();
       CompanyTable.getSharedInstance().setSymbols(symbols);
       CompanyTable.getSharedInstance().show(CompanyTable.SINGLE_SELECTION, true, CompanyTable.WINDOW_FLOATING);
       String[] result = symbols.getSymbols();
       return result;*/
    }

    public static String getFormattedPrintingTime() {
        TWDateFormat df = new TWDateFormat("dd MM yyyy - HH:mm:ss");
        df.setTimeZone(Settings.getCurrentZone());
        long timeMillis = System.currentTimeMillis(); //Client.getInstance().getMarketTime();
        return df.format(new Date(timeMillis));
    }

    public static void removeOHLCRequest(boolean isCurrentMode, String[] keys) {
        if (keys != null) {
            for (int i = 0; i < keys.length; i++) {
                removeOHLCRequest(isCurrentMode, keys[i]);
            }
        }
    }

    public static void removeOHLCRequest(boolean isCurrentMode, String key) {
        if (isCurrentMode) {
            OHLCStore.getInstance().removeIntradayRequest(key);
        } else {
            OHLCStore.getInstance().removeHistoryRequest(key);
        }
    }

    public static long getTimeZoneAdjustment(String symbol, long time, boolean isCurrentMode) {

        if (!isCurrentMode) {
            return 0;
        } else {
            try {
//            System.out.println("Base " + symbol);
                String exchangeCode = SharedMethods.getExchangeFromKey(symbol);
                Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
                if (exchange != null)
                    return exchange.getZoneAdjustment(time);
                else
                    return ExchangeStore.getSharedInstance().getSelectedExchange().getZoneAdjustment(time);
            } catch (Exception e) {
                return 0;
            }

        }

    }

    public static void setPrevTimeZone(TimeZone prevTimeZone) {
        ChartInterface.prevTimeZone = prevTimeZone;
    }

    public synchronized static long getPreviousTimeZoneAdjustment(long time) {
        try {
            return prevTimeZone.getOffset(time);
        } catch (Exception e) {
            return 0;
        }
    }

    public static String getColumnSettings(String id) {
        return TWColumnSettings.getItem(id);
    }

    public static void showAnnouncements(String symbol, long fromDate, long toDate) {
        Client.getInstance().searchAnnouncements(symbol, fromDate, toDate);
    }

    public static ChartTableInterface createChartTable() {
        ChartTable chartTable = new ChartTable();
        return chartTable;
    }

    public static ObjectBidAskTag[] getBidTagAndAskTag(ChartProperties cp) {
        ObjectBidAskTag[] tags = new ObjectBidAskTag[2];
        Stock stock = DataStore.getSharedInstance().getStockObject(cp.getSymbol());

        double[] yArrBid = {stock.getBestBidPrice()};
        ObjectBidAskTag objectBidTag = new ObjectBidAskTag(null, yArrBid, cp, cp.getRect());
        objectBidTag.setColor(Theme.getColor("CHART_BID_TAG_COLOR"));
        objectBidTag.setDescript(Language.getString("BEST_BID"));
        tags[0] = objectBidTag;
        double[] yArrAsk = {stock.getBestAskPrice()};
        ObjectBidAskTag objectAskTag = new ObjectBidAskTag(null, yArrAsk, cp, cp.getRect());
        objectAskTag.setColor(Theme.getColor("CHART_OFFER_TAG_COLOR"));
        objectAskTag.setDescript(Language.getString("BEST_ASK"));
        tags[1] = objectAskTag;

        return tags;
    }

    public static double getPreviousCloseValue(ChartProperties cp) {
        Stock stock = DataStore.getSharedInstance().getStockObject(cp.getSymbol());
        return stock.getPreviousClosed();
    }

    /**
     * Returns and ArrayList containing objects of type ObjectOrderLine
     * Object order lines pending/filled buy/sell orders
     *
     * @param cp
     * @return ArrayList
     */
    public static ArrayList<ObjectOrderLine> getOrderLines(ChartProperties cp) {
        ArrayList<ObjectOrderLine> lines = new ArrayList<ObjectOrderLine>();

        ListIterator<Transaction> transactions = OrderStore.getSharedInstance().getTransactions();
        while (transactions.hasNext()) {
            Transaction trx = transactions.next();
            String key = cp.getSymbol();
            if (!key.equals(SharedMethods.getKey(trx.getExchange(), trx.getSymbol(), trx.getSecurityType()))) {
                continue;
            }
            if (trx.getSide() == TradeMeta.BUY) {
                double sl = trx.getStopLossPrice();
                double tp = trx.getTakeProfitPrice();
                String orderID = (trx.getMubasherOrderNumber() != null) ? "#" + trx.getMubasherOrderNumber() : "";
                if (sl > 0) {
                    double[] yArr = new double[1];
                    yArr[0] = sl;
                    ObjectOrderLine slLine = new ObjectOrderLine(null, yArr, cp, cp.getRect());
                    slLine.setPenStyle((byte) PropertyDialogFactory.PEN_DASH);
                    slLine.setId(trx.getMubasherOrderNumber());
                    slLine.setColor(Theme.getColor("CHART_STOP_LOSS_LINE_COLOR"));
                    slLine.setFontColor(Theme.getColor("CHART_STOP_LOSS_LINE_COLOR"));
                    slLine.setDescript(orderID + " " + Language.getString("STOP_LOSS"));
                    if (trx.getStatus() == TradeMeta.T39_Expired ||
                            trx.getStatus() == TradeMeta.T39_Canceled || trx.getStatus() == TradeMeta.T39_Rejected) {
                        slLine.setRemove(true);
                    }
                    lines.add(slLine);
                }
                if (tp > 0) {
                    double[] yArr = new double[1];
                    yArr[0] = tp;
                    ObjectOrderLine tpLine = new ObjectOrderLine(null, yArr, cp, cp.getRect());
                    tpLine.setPenStyle((byte) PropertyDialogFactory.PEN_DASH);
                    tpLine.setId(trx.getMubasherOrderNumber());
                    tpLine.setColor(Theme.getColor("CHART_TAKE_PROFIT_LINE_COLOR"));
                    tpLine.setFontColor(Theme.getColor("CHART_TAKE_PROFIT_LINE_COLOR"));
                    tpLine.setDescript(orderID + " " + Language.getString("TAKE_PROFIT"));
                    if (trx.getStatus() == TradeMeta.T39_Expired ||
                            trx.getStatus() == TradeMeta.T39_Canceled || trx.getStatus() == TradeMeta.T39_Rejected) {
                        tpLine.setRemove(true);
                    }
                    lines.add(tpLine);
                }
            }
            if ((trx.getSide() == TradeMeta.BUY || trx.getSide() == TradeMeta.SELL) && trx.getPrice() > 0) {
                double[] yArr = new double[1];
                yArr[0] = trx.getPrice();
                String orderID = (trx.getMubasherOrderNumber() != null) ? "#" + trx.getMubasherOrderNumber() : "";
                ObjectOrderLine pendingOrderLine = new ObjectOrderLine(null, yArr, cp, cp.getRect());
                pendingOrderLine.setPenStyle((byte) PropertyDialogFactory.PEN_DASH);
                pendingOrderLine.setId(trx.getMubasherOrderNumber());
                if (trx.getStatus() == TradeMeta.T39_Filled) {
                    if (trx.getSide() == TradeMeta.BUY) {
                        pendingOrderLine.setColor(Theme.getColor("CHART_BUY_FILLED_LINE_COLOR"));
                        pendingOrderLine.setFontColor(Theme.getColor("CHART_BUY_FILLED_LINE_COLOR"));
                        pendingOrderLine.setDescript(orderID + " " + Language.getString("BUY_SETTLE"));
                    } else {
                        pendingOrderLine.setColor(Theme.getColor("CHART_SELL_FILLED_LINE_COLOR"));
                        pendingOrderLine.setFontColor(Theme.getColor("CHART_SELL_FILLED_LINE_COLOR"));
                        pendingOrderLine.setDescript(orderID + " " + Language.getString("SELL_SETTLE"));
                    }
                } else if (trx.getStatus() != TradeMeta.T39_Expired &&
                        trx.getStatus() != TradeMeta.T39_Canceled && trx.getStatus() != TradeMeta.T39_Rejected) {
                    if (trx.getSide() == TradeMeta.BUY) {
                        pendingOrderLine.setColor(Theme.getColor("CHART_BUY_PENDING_LINE_COLOR"));
                        pendingOrderLine.setFontColor(Theme.getColor("CHART_BUY_PENDING_LINE_COLOR"));
                        pendingOrderLine.setDescript(orderID + " " + Language.getString("BUY_PENDING"));
                    } else {
                        pendingOrderLine.setColor(Theme.getColor("CHART_SELL_PENDING_LINE_COLOR"));
                        pendingOrderLine.setFontColor(Theme.getColor("CHART_SELL_PENDING_LINE_COLOR"));
                        pendingOrderLine.setDescript(orderID + " " + Language.getString("SELL_PENDING"));
                    }
                } else {
                    pendingOrderLine.setRemove(true);
                }
                ObjectOrderLine existingLine = getExistingLine(lines, trx.getPrice());
                if (existingLine != null) {
                    String description = existingLine.getDescript();
                    existingLine.setDescript(description + ", " + pendingOrderLine.getDescript());
                } else {
                    lines.add(pendingOrderLine); //removes if exists
                }
            }
        }

        return lines;
    }

    private static ObjectOrderLine getExistingLine(ArrayList<ObjectOrderLine> lines, double yVal) {
        for (ObjectOrderLine objectOrderLine : lines) {
            if (objectOrderLine.getYArr()[0] == yVal) {
                return objectOrderLine;
            }
        }
        return null;
    }

    public static boolean isTradingConnected() {
        return TradingShared.isConnected();
    }

    public static boolean isTradingConnected(String exchange) {
        return (TradingShared.isConnected() && TradingShared.isTradableExchange(exchange));
        //return TradingShared.isConnected();  //Changed by charithn
    }

    public static ArrayList getCashFlowHistoryDetailArr() {
        return CashFlowHistoryDetailUI.getSharedInstance().getHistoryRecords();
    }

    /**
     * @param mode   ToolWindow.MODE_INTRADAY or ToolWindow.MODE_HISTORY
     * @param date   date/time in long format
     * @param open
     * @param high
     * @param low
     * @param close
     * @param volume f
     *               <p/>
     *               /*public static void setTollData(int mode, long date, float open, float high, float low, float close, long volume){
     *               if ((toolWindow != null) && (toolWindow.isDisplayable())){
     *               toolWindow.setData(mode, date,  open,  high,  low,  close, volume);
     *               }
     *               }
     *               <p/>
     *               /**
     *               call to close the tool window programatically
     *               <p/>
     *               public static void hideToolWindow(){
     *               toolWindow.dispose();
     *               toolWindow = null;
     *               }
     */

    public static JFrame getChartFrameParentComponent() {
        if (ChartFrame.getSharedInstance().isDetached()) {
            return (JFrame) ChartFrame.getSharedInstance().getDetachedFrame();
        } else {
            return Client.getInstance().getFrame();
        }
    }

    /*private static ToolWindow toolWindow;

    /**
     * Call to open tie tool window in chartframe
     *
    public static void showToolWindow(){
        if ((toolWindow == null) || (!toolWindow.isDisplayable())){
            toolWindow = new ToolWindow();
            Client.getInstance().getDesktop().add(toolWindow);
            Client.getInstance().getDesktop().setLayer(toolWindow, 1000);
        }
        toolWindow.setVisible(true);
    }*/

    public static void fireESCActionForGraphFrame() {

        if (ChartFrame.getSharedInstance().getActiveGraph() != null) {
            ChartFrame.getSharedInstance().getActiveGraph().fireEscapeAction();
        }
    }

    public static boolean isSplitsEnabled() {
        return true;
    }

    public static boolean isAnnoucmentsEnabled() {
        return true;
    }

    public static boolean isSplitAdujstmentsEnabled() {
        return true;
    }

    public static boolean isBuySellEnabled() {
        return true;
    }

    public static boolean isMinMaxEnabled() {
        return true;
    }

    public static boolean isVWAPEnabled() {
        return true;
    }

    public static boolean isFirstTimeChartLoading() {
        return isFirstTimeChartLoading;
    }

    public static void setFirstTimeChartLoading(boolean firstTimeChartLoading) {
        isFirstTimeChartLoading = firstTimeChartLoading;
    }

    public static boolean isExchangeOpenedOrPreClosed(String key) {

        Exchange exg = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key));
        return (exg.getMarketStatus() == Meta.MARKET_OPEN || exg.getMarketStatus() == Meta.MARKET_PRECLOSE);
    }

    //TODO: actually this shoud get from the server side.
    public static double getTurnoverDivisionFactor() {
        return 1.0;
    }

    public static boolean isSystemStrategiesEnabled() {
        return ExchangeStore.isValidSystemFinformationType(Meta.IT_CHART_STRATEGIES);
    }

    public static boolean isSystemPatternsEnabled() {
        return ExchangeStore.isValidSystemFinformationType(Meta.IT_CHART_PATTERNS);
    }

    private String getLegendString(String symb, int legendType) {
        String result;
        switch (legendType) {
            case StockGraph.INT_LEGEND_SYMBOL:
                return symb;
            case StockGraph.INT_LEGEND_SHORT_DESCRIPTION:
                result = DataStore.getSharedInstance().getCompanyName(symb);
//                if (result.equals(""))
//                    result = IndexStore.getDescription(symb);
                return result;
            default:
                result = DataStore.getSharedInstance().getCompanyName(symb);
//                if (result.equals(""))
//                    result = IndexStore.getDescription(symb);
                return result;
        }
    }
}