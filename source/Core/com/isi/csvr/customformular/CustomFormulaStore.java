package com.isi.csvr.customformular;

import com.isi.csvr.DataDisintegrator;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.*;
import com.mubasher.formulagen.TWCompiler;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Aug 22, 2006
 * Time: 1:45:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomFormulaStore {

    private static CustomFormulaStore self = null;
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    String[] cols = null;
    int columnCount;
    int columnIdentifier = 0;
    private Hashtable store;
    private ArrayList<CustomFormulaListener> listeners = new ArrayList<CustomFormulaListener>();
    private ArrayList<String> columnNames = new ArrayList<String>();

    private CustomFormulaStore() {
        store = new Hashtable();
    }

    public static CustomFormulaStore getSharedInstance() {
        if (self == null) {
            self = new CustomFormulaStore();
        }
        return self;
    }

    public void addCustomFormulaListener(CustomFormulaListener listener) {
        listeners.add(listener);
    }

    public void removeCustomFormulaListener(CustomFormulaListener listener) {
        listeners.remove(listener);
    }

    private void fireCustomFormulaAdded(String columnName) {
        CustomFormulaListener listener;
        for (int i = 0; i < listeners.size(); i++) {
            try {
                listener = listeners.get(i);
                listener.customFormulaAdded(columnName);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        listener = null;
    }

    private void fireCustomFormulaRemoved(String columnName) {
        CustomFormulaListener listener = null;
        for (int i = 0; i < listeners.size(); i++) {
            try {
                listener = listeners.get(i);
                listener.customFormulaRemoved(columnName);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
//            listener = null;
        }
        listener = null;
    }

    private void fireCustomFormulaEditted(String columnName, String newName) {
        CustomFormulaListener listener;
        for (int i = 0; i < listeners.size(); i++) {
            try {
                listener = listeners.get(i);
                listener.customFormulaEditted(columnName, newName);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        listener = null;
    }

    public void addCustomFormula(FormularEditorInterface object) {
        store.put(filterCaption(object.getColumnName()), object);       //.split(",")[0]
        columnNames.add(filterCaption(object.getColumnName()));
        fireCustomFormulaAdded(filterCaption(object.getColumnName()));
    }

    public void editCustomFormula(String columnName, FormularEditorInterface object) {
        try {
            store.remove(columnName);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        store.put(filterCaption(object.getColumnName()), object);       //.split(",")[0]
        int columnIndex = columnNames.indexOf(columnName);
        try {
            columnNames.remove(columnIndex);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        columnNames.add(columnIndex, filterCaption(object.getColumnName()));
        fireCustomFormulaEditted(columnName, filterCaption(object.getColumnName()));
        save();
    }

    public void removeCustomeFormula(String columnName) {
        if (store.containsKey(columnName))
            store.remove(columnName);
        columnIdentifier = columnNames.indexOf(columnName);
        if (columnNames.contains(columnName))
            columnNames.remove(columnName);
        fireCustomFormulaRemoved(columnName);
        save();
    }

    public Enumeration getColumnNames() {
        return store.keys();
    }

    public Hashtable getStore() {
        return store;
    }

    public FormularEditorInterface getFormulaObject(String columnName) {
        return (FormularEditorInterface) store.get(columnName);
    }

    public boolean isValidColumnName(String newName) {
        String name = filterCaption(newName);
        cols = Language.getList("TABLE_COLUMNS");
        for (int i = 0; i < cols.length; i++) {
            String col = cols[i];
            if (col.equals(name)) {
                return false;
            }
        }
        if (store.containsKey(name)) {
            return false;
        } else {
            return true;
        }

//        return !(store.contains(name));
    }

    public boolean isCustomFormularColumn(String columnName) {
        return columnNames.contains(columnName);
    }

    public int getColumnCount() {
        return store.size();
    }

    public String getColumnName(int col) {
        return columnNames.get(col);
    }

    public int getColumnIdentifier(String columnName) {
        return columnIdentifier;
    }

    public DoubleTransferObject getColumnValue(FunctionStockInterface stock, int col) {
        try {
            FormularEditorInterface object = (FormularEditorInterface) store.get(columnNames.get(col));
            return doubleTransferObject.setValue(object.getValue(stock));
        } catch (Exception e) {
            return doubleTransferObject.setValue(0);
        }
    }

    public boolean createFile(byte type, String columnName, String FunctionString, String oldName,
                              boolean checkForValidation, boolean initialLoading) {
        FormularEditorInterface functionObj;
        String name = "Formular_" + System.currentTimeMillis();
        File file = new File(Settings.getAbsolutepath() + "formula/" + name + ".java");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            columnName = UnicodeUtils.getNativeString(columnName);
            FileOutputStream out = new FileOutputStream(file);
            out.write(("package com.isi.csvr.customformular;").getBytes());
            out.write(("\n").getBytes());
            out.write(("public class " + name + " implements FormularEditorInterface {").getBytes());
            out.write(("\n").getBytes());
            out.write(("public double getValue( com.isi.csvr.shared.FunctionStockInterface stock) {").getBytes());
            out.write(("\n").getBytes());
            out.write(("return " + FormularEditor.getSharedInstance().getMethod(FunctionString) + " ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("public String getColumnName() {").getBytes());
            out.write(("\n").getBytes());
            out.write(("return \"" + UnicodeUtils.getUnicodeString(columnName) + "\" ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("public String getFunctionString() {").getBytes());
            out.write(("\n").getBytes());
            out.write(("return \"" + FunctionString + "\" ;").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.write(("\n").getBytes());
            out.write(("}").getBytes());
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Integer object = TWCompiler.compile(file.getAbsolutePath(), Settings.getAbsolutepath() + "formula");

            if (checkForValidation && (object != Constants.COMPILER_COMPILE_SUCCESS)) {
                return false;
            } else {
                // load class
                System.out.println("Loading class");
                functionObj = (FormularEditorInterface) Class.forName("com.isi.csvr.customformular." + name, false, new FunctionLoader()).newInstance();
                if (type == FormularEditor.NEW_FORMULA) {
                    addCustomFormula(functionObj);
                    if (!initialLoading) {
                        save();
                    }
                } else {
                    editCustomFormula(oldName, functionObj);
                }

                try {
                    file.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void save() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/customformula.mdf");
//            store = null;
            FormularEditorInterface object = null;
            for (int i = 0; i < store.size(); i++) {
                StringBuffer tempString = new StringBuffer();
                //PortfolioRecord record = null;
                object = ((FormularEditorInterface) store.get(columnNames.get(i)));
                tempString.append(ViewConstants.VC_ID);
                tempString.append(ViewConstants.VC_FIELD_DELIMETER);
                tempString.append(UnicodeUtils.getUnicodeString(object.getColumnName()));
                tempString.append(ViewConstants.VC_RECORD_DELIMETER);

                tempString.append(ViewConstants.VC_CAPTIONS);
                tempString.append(ViewConstants.VC_FIELD_DELIMETER);
                tempString.append(UnicodeUtils.getUnicodeString(object.getColumnName()));
                tempString.append(ViewConstants.VC_RECORD_DELIMETER);

                tempString.append(ViewConstants.VC_DEPTH_CALC_VALUE);
                tempString.append(ViewConstants.VC_FIELD_DELIMETER);
                tempString.append(object.getFunctionString());
                out.write(tempString.toString().getBytes());
                out.write("\r\n".getBytes());
                tempString = null;
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }

    public void load() {
        String data;
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(Settings.getAbsolutepath() + "datastore/customformula.mdf"));
            while (true) {
                data = in.readLine();
                if ((data == null) || (data.trim().equals(""))) break;
                StringTokenizer fields = new StringTokenizer(data, ViewConstants.VC_RECORD_DELIMETER);
                DataDisintegrator pair = new DataDisintegrator();
                pair.setSeperator(ViewConstants.VC_FIELD_DELIMETER);
                //store.setId(Settings.get);
                String columnName = null;
                String functionStr = null;
                String value;
                while (fields.hasMoreTokens()) {
                    try {
                        pair.setData(fields.nextToken());
                        value = pair.getData();

                        switch (SharedMethods.intValue(pair.getTag(), 0)) {
                            case ViewConstants.VC_ID:
                                break;

                            case ViewConstants.VC_CAPTIONS:
                                columnName = UnicodeUtils.getNativeString(value);
                                break;

                            case ViewConstants.VC_DEPTH_CALC_VALUE:
                                functionStr = value;
                                break;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                createFile(FormularEditor.NEW_FORMULA, columnName, functionStr, null, false, true);
                fields = null;
                pair = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String filterCaption(String caption) {
        try {
            String[] captions = caption.split("\\,");
            if (captions.length > 1) {
                return captions[Language.getLanguageID()];
            } else
                return caption;
        } catch (Exception e) {
            return caption;
        }
    }

    public int compare(Object o1, Object o2, int sortColumn, int sortOrder) {
        double val1 = getColumnValue((FunctionStockInterface) o1, sortColumn).getValue();
        double val2 = getColumnValue((FunctionStockInterface) o2, sortColumn).getValue();
        return compareValues(val1, val2, sortOrder);
    }

    private int compareValues(double val1, double val2, int sortOrder) {
        if ((Double.isNaN(val1)) && (Double.isNaN(val2))) {
            return 0;
        } else if (Double.isNaN(val1)) {
            return 1;
        } else if (Double.isNaN(val2)) {
            return -1;
        }
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

}
