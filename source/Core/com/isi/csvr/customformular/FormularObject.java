package com.isi.csvr.customformular;

import javax.swing.text.Style;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Aug 21, 2006
 * Time: 5:42:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class FormularObject {

    public String name;
    public String ID;
    public Style style;
    public String methodName;
    private boolean isOperator;

    public FormularObject(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public boolean isOperator() {
        return isOperator;
    }

    public void setOperator(boolean operator) {
        isOperator = operator;
    }
}
