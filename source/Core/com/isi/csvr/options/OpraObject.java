package com.isi.csvr.options;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 11, 2003
 * Time: 9:36:30 AM
 * To change this template use Options | File Templates.
 */
public class OpraObject {
    private String putSymbol;
    private String callSymbol;
    private int instrumentType;
    private double strikePrice;
    private boolean nearTheMoney = false;

    public OpraObject() {
    }

    public OpraObject(double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public String getPutSymbol() {
        return putSymbol;
    }

    public void setPutSymbol(String putSymbol) {
        this.putSymbol = putSymbol;
    }

    public String getCallSymbol() {
        return callSymbol;
    }

    public void setCallSymbol(String callSymbol) {
        this.callSymbol = callSymbol;
    }

    public double getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public String toString() {
        return "" + strikePrice;    //To change body of overridden methods use File | Settings | File Templates.
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    public boolean isNearTheMoney() {
        return nearTheMoney;
    }

    public void setNearTheMoney(boolean nearTheMoney) {
        this.nearTheMoney = nearTheMoney;
    }
}
