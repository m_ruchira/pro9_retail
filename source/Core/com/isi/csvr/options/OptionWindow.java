package com.isi.csvr.options;

import com.isi.csvr.*;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.OptionStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

public class OptionWindow extends JPanel implements Themeable, ItemListener, ActionListener, DropTargetListener,
        Runnable, OptionListener, PropertyChangeListener, MouseListener, PopupMenuListener {

    private static int population = 0;
    private final int PANEL_WIDTH = 800; //400; //350;
    private final int PANEL_HEIGHT = 45; //250; //160;
    DecimalFormat oPriceFormat = new DecimalFormat(" ###,##0.00 ");
    DecimalFormat oQuantityFormat = new DecimalFormat(" ###,##0 ");
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyMM");
    Calendar calendar;
    Date date;
    JPanel configPanel;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private boolean isActive = true;
    private Thread g_oThread;
    private String sKey = null;
    private String baseSymbol = null;
    private ViewSettingsManager vsManager;
    private ViewSetting oCallsViewSetting;
    private OptionsModel callsModel;
    private InternalFrame parent;
    private OptionChainRenderer renderer1;
    private TWComboBox comboMonths;
    private TWComboBox comboTypes;
    private ArrayList<TWComboItem> months = new ArrayList<TWComboItem>();
    private ArrayList<TWComboItem> types = new ArrayList<TWComboItem>();
    private ArrayList<String> monthlist = new ArrayList<String>();
    //    private ExpireationDateComboModel   comboModel;
    private JPanel quotePanel;
    private JPanel northLowerPanel;
    private JPanel northPanel;
    private JPanel southPanel;
    private JLabel expireDaysLabel;
    private JLabel legandLabel;
    private JLabel lblTrade;
    private JLabel lblChange;
    private JLabel lblBid;
    private JLabel lblAsk;
    private JLabel lblVolume;
    private JComboBox nearSelector;
    private JLabel lblCalls;
    private JLabel lblPuts;
    private TWMenu workSpacePopup1 = new TWMenu();         // PopupMenu for the Workspaces
    private TWMenuItem mnuMDepthPrice;
    private TWMenuItem mnuRegionalQuotes;
    private TWMenuItem mnuDQuote;
    private TWMenuItem mnuTimeNSales;
    private TWMenuItem mnuSnapQuote;
    private TWMenuItem mnuBuy;
    private TWMenuItem mnuSell;
    private Table callTable;
    private Color bgColor;
    private Color fgColor;
    private Color upColor;
    private Color downColor;
    private double prevTrade = 0;
    private double currentTrade = 0;
    private double prevAsk = 0;
    private double prevBid = 0;
    private long prevVolume = 0;
    private String selectedBaseSymbol = null;
    private String selectedMonth = null;
    private String symbol = null;
    private byte selectedType = 0;
    private byte chainType = 0;
    private JLabel dummyLabel;

    public OptionWindow(String sKey, ViewSettingsManager vsManagerIn, boolean removeBaseOnExit, byte chainType) {
        vsManager = vsManagerIn;
        Theme.registerComponent(this);
        OptionStore.getSharedInstance().resetDefaultValues();
        this.sKey = sKey;
        this.chainType = chainType;
        baseSymbol = sKey;
        calendar = Calendar.getInstance();
//		comboModel = new ExpireationDateComboModel();
        this.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));

        createLayout();
        callTable.getTable().addMouseListener(this);
        selectedMonth = oCallsViewSetting.getProperty(ViewConstants.VC_OPTION_MONTH);
        if (selectedMonth != null && selectedMonth.trim().equals("")) {
            selectedMonth = null;
        }
        String month = oCallsViewSetting.getProperty(ViewConstants.VC_OPTION_MONTH);
        if (month == null) {
            SimpleDateFormat format = new SimpleDateFormat("MMM yyyy");
            Calendar cal = Calendar.getInstance();
            month = format.format(cal.getTime()).toUpperCase();
            cal = null;
            format = null;
        }
        OpraMap map = OptionStore.getSharedInstance().createOpraMap(sKey, removeBaseOnExit, month);
        String typeStr = oCallsViewSetting.getProperty(ViewConstants.VC_OPTION_TYPE);
        if ((typeStr != null) && (!typeStr.equals(""))) {
            selectedType = Byte.parseByte(typeStr);
        }
        selectedMonth = null;
        String nearTheMoney = oCallsViewSetting.getProperty(ViewConstants.VC_OPTION_NEAR_THE_MONEY);
        nearSelector.addItemListener(this);
        if ((nearTheMoney == null) || (nearTheMoney.equals("1"))) { // near the money has been selected
            map.setNearTheMoney(true);
            nearSelector.setSelectedIndex(0);
            OptionStore.getSharedInstance().removeOutOfMoneySymbols(sKey, chainType);
            OptionStore.getSharedInstance().getOpraMap(sKey, chainType).setNearTheMoney(true);
            oCallsViewSetting.putProperty(ViewConstants.VC_OPTION_NEAR_THE_MONEY, 1);

        } else {
            map.setNearTheMoney(true);
            nearSelector.setSelectedIndex(1);

        }
        drawOptions();
        callTable.getSmartTable().adjustColumnWidthsToFit(10);
        OptionStore.getSharedInstance().addOpraQuoteObject(sKey, selectedMonth, selectedType, chainType, this);
        OptionWindow.incPopulation();
    }

    public synchronized static int getPopulation() {
        return population;
    }

    public synchronized static void incPopulation() {
        OptionWindow.population++;
    }

    public synchronized static void decPopulation() {
        OptionWindow.population--;
    }

    public Table getTable() {
        return callTable;
    }

    public JPanel getNorthPanel() {
        return northPanel;
    }

    private void createLayout() {
        this.setLayout(new BorderLayout());
        this.setBackground(bgColor);

        // #####################################################################
        //      Create North Panel
        // #####################################################################
        northPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"40%", "60%"}, 1, 1));            //new GridLayout(2, 1)
        northPanel.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        configPanel = new JPanel();
        String[] configPanelwidths = {"0", "0", "100%"};
        String[] configPanelheights = {"20"};
        configPanel.setLayout(new FlexGridLayout(configPanelwidths, configPanelheights));//new FlowLayout(SwingConstants.CENTER , 5, 2)); //null);

        ComboBoxModel monthModel = new TWComboModel(months);
        comboMonths = new TWComboBox(monthModel);
//        comboMonths = new TWComboBox(comboModel);
        ComboBoxModel typesModel = new TWComboModel(types);
        comboTypes = new TWComboBox(typesModel);
        comboTypes.setVisible(false);
        //comboMonths.addItemListener(this);
        dummyLabel = new JLabel();
//        quotePanel = new JPanel(new FlowLayout(SwingConstants.CENTER , 5, 0));
        quotePanel = new JPanel(new FlexGridLayout(new String[]{"10%", "10%", "10%", "10%", "10%", "10%", "10%", "10%", "10%", "10%"}, new String[]{"100%"}, 0, 0));
        quotePanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));

        JLabel lblTradeName = new JLabel(Language.getString("OPTION_LAST_TRADE"));
        lblTrade = new JLabel("0.000");
        JLabel lblChangeName = new JLabel(Language.getString("DQ_CHANGE"));
        lblChange = new JLabel("0.000");
        JLabel lblBidName = new JLabel(Language.getString("DQ_BID"));
        lblBid = new JLabel("0.000");
        JLabel lblAskName = new JLabel(Language.getString("DQ_ASK"));
        lblAsk = new JLabel("0.000");
        JLabel lblVolumeName = new JLabel(Language.getString("DQ_VOLUME"));
        lblVolume = new JLabel("0.000");

        lblTradeName.setHorizontalAlignment(SwingConstants.CENTER);
        lblTrade.setHorizontalAlignment(SwingConstants.LEADING);
        lblChangeName.setHorizontalAlignment(SwingConstants.CENTER);
        lblChange.setHorizontalAlignment(SwingConstants.LEADING);
        lblBidName.setHorizontalAlignment(SwingConstants.CENTER);
        lblBid.setHorizontalAlignment(SwingConstants.LEADING);
        lblAskName.setHorizontalAlignment(SwingConstants.CENTER);
        lblAsk.setHorizontalAlignment(SwingConstants.LEADING);
        lblVolumeName.setHorizontalAlignment(SwingConstants.CENTER);
        lblVolume.setHorizontalAlignment(SwingConstants.LEADING);

        lblTradeName.setPreferredSize(new Dimension(60, 16));
        lblTrade.setPreferredSize(new Dimension(60, 16));
        lblChangeName.setPreferredSize(new Dimension(60, 16));
        lblChange.setPreferredSize(new Dimension(60, 16));
        lblBidName.setPreferredSize(new Dimension(60, 16));
        lblBid.setPreferredSize(new Dimension(60, 16));
        lblAskName.setPreferredSize(new Dimension(60, 16));
        lblAsk.setPreferredSize(new Dimension(60, 16));
        lblVolumeName.setPreferredSize(new Dimension(60, 16));
        lblVolume.setPreferredSize(new Dimension(80, 16));

        lblTradeName.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblTrade.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblChangeName.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblChange.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblBidName.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblBid.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblAskName.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblAsk.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblVolumeName.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));
        lblVolume.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));

        quotePanel.add(lblTradeName);
        quotePanel.add(lblTrade);
        quotePanel.add(lblChangeName);
        quotePanel.add(lblChange);
        quotePanel.add(lblBidName);
        quotePanel.add(lblBid);
        quotePanel.add(lblAskName);
        quotePanel.add(lblAsk);
        quotePanel.add(lblVolumeName);
        quotePanel.add(lblVolume);

        comboMonths.setPreferredSize(new Dimension(125, 18));
        comboTypes.setPreferredSize(new Dimension(125, 18));
        dummyLabel.setPreferredSize(new Dimension(1, 18));

        configPanel.add(comboMonths);
        configPanel.add(dummyLabel);
        configPanel.add(quotePanel);

        String[] northLowerPanelWidths = {"50%", "0", "50%"};
        String[] northLowerPanelHeights = {"100%"};
        northLowerPanel = new JPanel();
        northLowerPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
        northLowerPanel.setLayout(new FlexGridLayout(northLowerPanelWidths, northLowerPanelHeights, 0, 0, false, true));// new FlowLayout(SwingConstants.CENTER ,5,0));
        lblCalls = new JLabel();
        lblCalls.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblCalls.setText(Language.getString("OC_CALLS"));
        lblCalls.setHorizontalAlignment(JLabel.CENTER);

        lblPuts = new JLabel();
        lblPuts.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblPuts.setText(Language.getString("OC_PUTS"));
        lblPuts.setHorizontalAlignment(JLabel.CENTER);
        nearSelector = new JComboBox();
        nearSelector.setBorder(BorderFactory.createEmptyBorder());
        nearSelector.setPreferredSize(new Dimension(120, 18));
        nearSelector.addItem(Language.getString("LBL_NEAR_THE_MONEY"));
        nearSelector.addItem(Language.getString("LBL_ALL"));
        northLowerPanel.add(lblCalls);
        northLowerPanel.add(nearSelector);
        northLowerPanel.add(lblPuts);

        northPanel.add(configPanel);
        northPanel.add(northLowerPanel);
//        northPanel.add(northLowerPanel);

        // #####################################################################
        //      Create the Center Panel
        // #####################################################################

        callTable = new Table(new int[]{7});  //strike as fixed column
        callTable.setAutoResize(false);

        renderer1 = new OptionChainRenderer(sKey);

        try {
            // Create the Calls Model & VeiwSetting
            callsModel = new OptionsModel(chainType);
            callsModel.setDirectionLTR(Language.isLTR());
            if (chainType == OptionStore.CHAIN_TYPE_OPTION) {
                oCallsViewSetting = vsManager.getOptionChainView(ViewSettingsManager.OPTION_CALLS_VIEW + "|" + baseSymbol);
                if (oCallsViewSetting == null) {
                    oCallsViewSetting = vsManager.getSymbolView("OPTIONS");
                    oCallsViewSetting = oCallsViewSetting.getObject();
                    oCallsViewSetting.setID(baseSymbol);
                    vsManager.putOptionChainView(ViewSettingsManager.OPTION_CALLS_VIEW + "|" +
                            baseSymbol, oCallsViewSetting);
                    GUISettings.setColumnSettings(oCallsViewSetting, TWColumnSettings.getItem("OPTION_CHAIN_COLS"));
                    SharedMethods.applyCustomViewSetting(oCallsViewSetting);
                    DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oCallsViewSetting.getType(), oCallsViewSetting);
                }
            } else if (chainType == OptionStore.CHAIN_TYPE_FUTURES) {
                oCallsViewSetting = vsManager.getOptionChainView(ViewSettingsManager.OPTION_CALLS_VIEW + "|" + baseSymbol);
                if (oCallsViewSetting == null) {
                    oCallsViewSetting = vsManager.getSymbolView("FUTURES");
                    oCallsViewSetting = oCallsViewSetting.getObject();
                    oCallsViewSetting.setID(baseSymbol);
                    vsManager.putOptionChainView(ViewSettingsManager.FUTURES_CALLS_VIEW + "|" +
                            baseSymbol, oCallsViewSetting);
                    GUISettings.setColumnSettings(oCallsViewSetting, TWColumnSettings.getItem("OPTION_CHAIN_COLS"));
                    SharedMethods.applyCustomViewSetting(oCallsViewSetting);
                    DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oCallsViewSetting.getType(), oCallsViewSetting);
                }
            }
            oCallsViewSetting.setParent(parent);
            callsModel.setViewSettings(oCallsViewSetting);
            callTable.setModel(callsModel);
            callsModel.setTable(callTable, renderer1);  // Add the renderer...
            callTable.setAutoResize(true);
//            GUISettings.setColumnSettings(oCallsViewSetting, TWColumnSettings.getItem("OPTION_CHAIN_COLS"));
            //todo added by Dilum
            SharedMethods.applyCustomViewSetting(oCallsViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oCallsViewSetting.getType(), oCallsViewSetting);
            callsModel.applyColumnSettings();

        } catch (Exception e) {
            e.printStackTrace();
        }

        callTable.getTable().setBackground(bgColor);
//        callTable.addMouseListener(this);
//        callTable.getTable().addMouseListener(this);
//		callTable.getTable().getTableHeader(). addPropertyChangeListener (this);

        /* ------------------------------------------------------------------------
           Create the south panel
           ------------------------------------------------------------------------ */
        String[] southWidhs = {"0", "100%"};
        String[] southHeights = {"0"};
        expireDaysLabel = new JLabel("");
        expireDaysLabel.setPreferredSize(new Dimension(100, 20));
        expireDaysLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        southPanel = new JPanel(new FlexGridLayout(southWidhs, southHeights, 3, 0, true, false));
        legandLabel = new JLabel(Language.getString("IN_THE_MONEY"), new OpraLegand(), SwingConstants.LEADING);
        legandLabel.setPreferredSize(new Dimension(100, 20));
        southPanel.add(legandLabel);

        southPanel.add(expireDaysLabel);
//        mainPanel.add(southPanel, BorderLayout.SOUTH);
        callTable.setSouthPanel(southPanel);
        this.setVisible(false);
        try {
            applyTheme();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!Language.isLTR()) {
            workSpacePopup1.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }
    }

    public void setMonths(String[] monthList) {

        String[] monthNames = Language.getString("MONTH_NAMES").split(",");
        try {
            monthlist.clear();
            for (int k = 0; k < monthList.length; k++) {
                monthlist.add(monthList[k]);
            }
            Collections.sort(monthlist);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        comboMonths.removeItemListener(this);
        months.clear();
        int index = 0;
        for (int i = 0; i < monthlist.size(); i++) {
            try {
                months.add(new TWComboItem(monthlist.get(i), monthlist.get(i).substring(0, 4) + "  " + monthNames[Integer.parseInt(monthlist.get(i).substring(4)) - 1]));
                if (selectedMonth.equals(monthlist.get(i))) {
                    index = months.size() - 1;
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        comboMonths.updateUI();
        comboMonths.setSelectedIndex(index);
        comboMonths.addItemListener(this);

        /*comboMonths.removeItemListener(this);
        months.clear();
        int index = 0;
        for(int i=0;i<monthList.length;i++){
            months.add(new TWComboItem(monthList[i],monthList[i]));
//            months.add(new TWComboItem(monthList[i],monthNames[Integer.parseInt(monthList[i].substring(4)) -1]));
            if(selectedMonth.equals(monthList[i])){
                index = months.size() -1;
            }
        }
        comboMonths.updateUI();
        comboMonths.setSelectedIndex(index);
        comboMonths.addItemListener(this);*/
    }

    public void setTypes(String[] typesList) {
        comboTypes.removeItemListener(this);
        types.clear();
        int index = 0;
        for (int i = 0; i < typesList.length; i++) {
            if (typesList[i].trim().equals("0")) {
                types.add(new TWComboItem(0, Language.getString("OPTION_TYPE_AMERICAN")));
                if (selectedType == 0) {
                    index = types.size() - 1;
                }
            } else if (typesList[i].trim().equals("1")) {
                types.add(new TWComboItem(1, Language.getString("OPTION_TYPE_EUROPEAN")));
                if (selectedType == 1) {
                    index = types.size() - 1;
                }
            }
        }
        if (typesList.length > 1) {
            comboTypes.setVisible(true);
            configPanel.remove(dummyLabel);
            configPanel.add(comboTypes, 1);
            configPanel.repaint();
            configPanel.updateUI();

        }
        comboTypes.updateUI();
        comboTypes.setSelectedIndex(index);
        comboTypes.addItemListener(this);
    }

    public void optionDataReceived(OpraMap map) {
        try {
            /*boolean justLoaded = false;
            if( comboModel.getSize() ==0){
                justLoaded = true;
            }*/
            selectedMonth = map.getCurrentMonth();
            selectedType = map.getCurrentType();
            selectedBaseSymbol = SharedMethods.getKey(map.getExchange(), map.getBaseSymbol(), map.getInstrumentType());

            setMonths(map.getMonths());
            setTypes(map.getTypes());
//            if (justLoaded){ // no item selected. must select thecurrent month
//                 comboMonths.removeItemListener(this);
//                 comboMonths.removeAllItems();
//                 for(int i=0; i<comboModel.getSize();i++){
//                     if (comboModel.getItemAt(i).getCaption().equals(map.getCurrentMonth())){
//                         comboMonths.setSelectedIndex(i);
//                         break;
//                     }
//                 }
//                 comboMonths.addItemListener(this);
//             }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        callTable.updateGUI();
        callTable.updateUI();
        parent.setTitle(Language.getString(oCallsViewSetting.getCaptionID()) + " - " + DataStore.getSharedInstance().getCompanyName(sKey));
        //componentResized(null);
    }

    private void createWSPopUp() {
        try {
            String wsName = null;
            TWMenuItem wsItemMenu1 = null;
            TWMenuItem wsItemMenu2 = null;
            Object[] windowList = ((TWDesktop) Client.getInstance().getDesktop()).getWindows();
            if (windowList == null)
                return;
            ClientTable nodeObject = null;
            ViewSetting oSettings = null;

            workSpacePopup1.setText(Language.getString("POPUP_ADD_TO_MAINBOARD"));


            for (int i = 0; i < windowList.length; i++) {
                try {
                    JInternalFrame frame = (JInternalFrame) windowList[i];
                    if (!(frame instanceof ClientTable)) {
                        continue;
                    }
                    wsName = frame.getTitle();
                    oSettings = ((ClientTable) frame).getViewSettings();
                    if (oSettings.isCutomType()) {
                        wsItemMenu1 = new TWMenuItem(wsName);
                        wsItemMenu1.addActionListener(this);
                        wsItemMenu1.setActionCommand(oSettings.getID());

                        wsItemMenu2 = new TWMenuItem(wsName);
                        wsItemMenu2.addActionListener(this);
                        wsItemMenu2.setActionCommand(oSettings.getID());

                        if (!Language.isLTR()) {
                            wsItemMenu1.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
                        }

                        workSpacePopup1.add(wsItemMenu1);
                    }
                    frame = null;

                } catch (Exception ex) {
                }
            }
            windowList = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTableFont(Font oFont) {
        callTable.getTable().setFont(oFont);
        callTable.getModel().getViewSettings().setFont(oFont);
        FontMetrics oMetrices = callTable.getTable().getGraphics().getFontMetrics();
        callTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent());
    }

    public void setTableHeaderFont(Font oFont) {
        callTable.getTable().getTableHeader().setFont(oFont);
        callTable.getModel().getViewSettings().setHeaderFont(oFont);
        callTable.getTable().getTableHeader().updateUI();
    }

    /* Necessary to resize headers upon chanege of font type and size */
    public void propertyChange(PropertyChangeEvent evt) {
    }

    /**
     * Returns the view setting object for this table
     */
    public ViewSetting getViewSettings() {
        return oCallsViewSetting;
    }

    /**
     * Returns the Window Type for the window
     */
    public byte getWindowType() {
        return ViewSettingsManager.OPTION_CALLS_VIEW;
    }

    public void setParent(InternalFrame parentIn) {
        parent = parentIn;
        oCallsViewSetting.setParent(parent);
        parent.setTitle(Language.getString(oCallsViewSetting.getCaptionID()) + " : " + DataStore.getSharedInstance().getCompanyName(sKey)); // + Language.getString("GDR_MESSAGE"));
//        parent.addMouseListener(this);
        createWSPopUp();
        createRegionalQuoteMenu();
        createSnapQuoteMenu();
        createTradeMenus();
        callTable.getPopup().setMenu(workSpacePopup1);
        if (Client.getInstance().isValidInfoType(symbol, Meta.IT_RegionalQuotes, true)) {
            callTable.getPopup().setMenuItem(mnuRegionalQuotes);
        }
//        callTable.getPopup().setMenuItem(mnuTnSCall);
        callTable.getPopup().setMenuItem(mnuDQuote);
        if (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).isDefault()) {
            callTable.getPopup().setMenuItem(mnuMDepthPrice);
            callTable.getPopup().setMenuItem(mnuSnapQuote);
        }
        callTable.getPopup().setMenuItem(mnuBuy);
        callTable.getPopup().setMenuItem(mnuSell);
        callTable.getPopup().addPopupMenuListener(this);
    }

    private void createRegionalQuoteMenu() {
        mnuRegionalQuotes = new TWMenuItem(Language.getString("POPUP_REGIONAL_QUOTES"), "regionalquotes.gif");
        mnuRegionalQuotes.addActionListener(this);
        mnuRegionalQuotes.setActionCommand("REGIONAL_CALL");
        mnuMDepthPrice = new TWMenuItem(Language.getString("MARKET_DEPTH_BY_PRICE"), "mktby_price.gif");
        mnuMDepthPrice.addActionListener(this);
        mnuMDepthPrice.setActionCommand("MDEPTH_PRICE");
        mnuTimeNSales = new TWMenuItem(Language.getString("TIME_AND_SALES"), "time&sales.gif");
        mnuTimeNSales.addActionListener(this);
        mnuTimeNSales.setActionCommand("TNS_CALL");
        mnuDQuote = new TWMenuItem(Language.getString("DETAIL_QUOTE"), "detailquote.gif");
        mnuDQuote.addActionListener(this);
        mnuDQuote.setActionCommand("DETAIL_QUOTE");
    }

    private void createSnapQuoteMenu() {
        mnuSnapQuote = new TWMenuItem(Language.getString("SNAP_QUOTE"), "snapquote.gif");
        mnuSnapQuote.setActionCommand("SNAP_CALL");
        mnuSnapQuote.addActionListener(this);
    }

    private void createTradeMenus() {
        mnuBuy = new TWMenuItem(Language.getString("BUY"), "buy.gif");
        mnuBuy.setActionCommand("BUY");
        mnuBuy.addActionListener(this);
        mnuSell = new TWMenuItem(Language.getString("SELL"), "sell.gif");
        mnuSell.setActionCommand("SELL");
        mnuSell.addActionListener(this);
    }

    private void updateQuoteDetails() {
        if (sKey == null)
            return;
        String baseKey = sKey;
        Stock stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(sKey), SharedMethods.getSymbolFromKey(sKey), SharedMethods.getInstrumentTypeFromKey(sKey));
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            String symbol = SharedMethods.getSymbolFromKey(sKey);
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                baseKey = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sKey), symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter())), SharedMethods.getInstrumentTypeFromKey(sKey));
            }
        }
        stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(baseKey), SharedMethods.getSymbolFromKey(baseKey), SharedMethods.getInstrumentTypeFromKey(baseKey));
        if (stock == null) {
            return;
        }

        if (stock.getLastTradeValue() > 0)
            currentTrade = stock.getLastTradeValue();
        else
            currentTrade = stock.getCurrentPrice();
        lblTrade.setText(oPriceFormat.format(currentTrade));
        if (currentTrade > prevTrade) {
            lblTrade.setForeground(upColor);
        } else if (currentTrade < prevTrade) {
            lblTrade.setForeground(downColor);
        } else {
            lblTrade.setBackground(this.getBackground());
            lblTrade.setForeground(fgColor);
        }
        prevTrade = currentTrade;

        lblBid.setText(oPriceFormat.format(stock.getBestBidPrice()));
        if (stock.getBestBidPrice() > prevBid) {
            lblBid.setForeground(upColor);
        } else if (stock.getBestBidPrice() < prevBid) {
            lblBid.setForeground(downColor);
        } else {
            lblBid.setBackground(this.getBackground());
            lblBid.setForeground(fgColor);
        }
        prevBid = stock.getBestBidPrice();

        lblAsk.setText(oPriceFormat.format(stock.getBestAskPrice()));
        if (stock.getBestAskPrice() > prevAsk) {
            lblAsk.setForeground(upColor);
        } else if (stock.getBestAskPrice() < prevAsk) {
            lblAsk.setForeground(downColor);
        } else {
            lblAsk.setBackground(this.getBackground());
            lblAsk.setForeground(fgColor);
        }
        prevAsk = stock.getBestAskPrice();

        if (stock.getChange() == Constants.DEFAULT_DOUBLE_VALUE) {
            lblChange.setText(Constants.DEFAULT_STRING);
            lblChange.setHorizontalAlignment(JLabel.TRAILING);
        } else {
            lblChange.setText(oPriceFormat.format(stock.getChange()));
            if (stock.getChange() > 0) {
                lblChange.setForeground(upColor);
            } else if (stock.getChange() < 0) {
                lblChange.setForeground(downColor);
            } else {
                lblChange.setForeground(this.getForeground());
            }
        }

        lblVolume.setText(oQuantityFormat.format(stock.getVolume()));
        if (stock.getVolume() > prevVolume) {
            lblVolume.setForeground(upColor);
        } else if (stock.getVolume() < prevVolume) {
            lblVolume.setForeground(downColor);
        } else {
            lblVolume.setBackground(this.getBackground());
            lblVolume.setForeground(fgColor);
        }
        prevVolume = stock.getVolume();
        stock = null;

        try {
            OpraMap opraMap;
            opraMap = (OpraMap) OptionStore.getSharedInstance().getOpraMap(sKey, chainType);
            String dateString = opraMap.getCurrentMonth() + opraMap.getExpirationDate();
            Date date = dateFormat.parse(dateString);
//                stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(sKey) ,SharedMethods.getSymbolFromKey(sKey),SharedMethods.getInstrumentTypeFromKey(sKey));
            if ((opraMap != null) && (dateString != null)) {
                String count = "" + SharedMethods.getDateDiff(new Date(System.currentTimeMillis()), date);
                String msg = Language.getString("OPTIONS_EXPIRE_IN");
                msg = msg.replaceAll("\\[COUNT\\]", count);
                expireDaysLabel.setText(msg);
                msg = null;
                count = null;
                selectedBaseSymbol = null;
            }

        } catch (Exception e) {
            selectedBaseSymbol = null;
        }
        stock = null;
        updateUI();
    }

    public void run() {
        while (isActive) {
            updateQuoteDetails();
            sleepMe(1000);
        }
    }

    public void killThread() {
        isActive = false;
        Theme.unRegisterComponent(this);
    }

    public void activate() {
        g_oThread = new Thread(this, "TW Option WIndow");
        g_oThread.start();
    }

    /**
     * Sleep the thread for a give time period
     */
    private void sleepMe(long lDelay) {
        try {
            g_oThread.sleep(lDelay);
        } catch (Exception e) {
        }
    }

    public void applyTheme() {
        bgColor = Theme.getColor("BACKGROUND_COLOR");
        loadThemeColors();

        try {
            updateQuoteDetails();
            renderer1.reload();
            expireDaysLabel.setFont(expireDaysLabel.getFont().deriveFont(Font.BOLD));
            legandLabel.setFont(legandLabel.getFont().deriveFont(Font.BOLD));
        } catch (Exception e) {
        }
        updateUI();
    }

    ;

    private void loadThemeColors() {
        upColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
        downColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
        fgColor = Theme.getColor("LABEL_FGCOLOR");
    }

    public String getCurrentSymbol() {
        return sKey;
    }

    public void connectionEstablished() {
    }

   /* public void addSymbolToWatchList(String symbol, String command, int istrumentType) {
        Object[] windowList = ((TWDesktop)Client.getInstance().getDesktop()).getWindows();
            if (windowList == null)
                return;
            ViewSetting oSettings   = null;

            for (int i = 0; i < windowList.length; i++) {
                try {
                    JInternalFrame frame = (JInternalFrame) windowList[i];
                    if (!(frame instanceof ClientTable)) {
                        continue;
                    }
                    oSettings = ((ClientTable) frame).getViewSettings();
                    if(oSettings.isCutomType()){
                        if(oSettings.getID().equals(command)){
                            String requestID = "MainBoard:"+ System.currentTimeMillis();
                            SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(symbol), requestID, SharedMethods.getExchangeFromKey(symbol));
                            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, frame);
//                            ((ClientTable)frame).getSymbols().appendSymbol(symbol);
//                            oSettings.getSymbols().appendSymbol(symbol);
//                            ((ClientTable)frame).getSymbols().reFilter();
//                            TradeFeeder.refreshSymbolIndex();
//                            ((ClientTable)frame).updateUI();
                            System.out.println("symbol added to watchlist");
                            break;
                        }
                    }
                    frame = null;

                } catch (Exception ex) {
                }
            }
            windowList = null;
    }*/

    public void setSymbol(String sKeyIn) {
    }

    private void drawOptions() {
        try {
            OptionStore.getSharedInstance().getOpraMap(sKey, chainType).addListener(this);
            callsModel.setDataRegister(OptionStore.getSharedInstance().getOpraMap(sKey, chainType));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (this.symbol == null) {
            return;
        }
//        System.out.println("symbol get==="+symbol);
        if (e.getActionCommand().equals("REGIONAL_CALL")) {
            Client.getInstance().mnu_RegionalQuoteSymbol(symbol, false);
        } else if (e.getActionCommand().equals("REGIONAL_PUT")) {
            Client.getInstance().mnu_RegionalQuoteSymbol(symbol, false);
        } else if (e.getActionCommand().equals("SNAP_CALL")) {
            Client.getInstance().showSnapQuote(symbol, true, false, false, LinkStore.LINK_NONE);
        } else if (e.getActionCommand().equals("SNAP_PUT")) {
            Client.getInstance().showSnapQuote(symbol, true, false, false, LinkStore.LINK_NONE);
        } else if (e.getActionCommand().equals("TNS_CALL")) {
            Client.getInstance().mnu_TimeNSalesSymbol(symbol, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
        } else if (e.getActionCommand().equals("TNS_PUT")) {
            Client.getInstance().mnu_TimeNSalesSymbol(symbol, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
        } else if (e.getActionCommand().equals("MDEPTH_PRICE")) {
            Client.getInstance().depthByPrice(symbol, false, Constants.MAINBOARD_TYPE);
        } else if (e.getActionCommand().equals("DETAIL_QUOTE")) {
            Client.getInstance().showDetailQuote(symbol, true, false, false, LinkStore.LINK_NONE);
        } else if (e.getActionCommand().equals("BUY")) {
            String requestID = "OptionWindow" + System.currentTimeMillis();
            DataStore.getSharedInstance().checkSymbolAvailability(SharedMethods.getSymbolFromKey(symbol), requestID, SharedMethods.getExchangeFromKey(symbol), SharedMethods.getInstrumentTypeFromKey(symbol));

//            SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(symbol), requestID, SharedMethods.getExchangeFromKey(symbol), SharedMethods.getInstrumentTypeFromKey(symbol));
            Client.getInstance().doTransaction(TradeMeta.BUY, symbol, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
        } else if (e.getActionCommand().equals("SELL")) {
            String requestID = "OptionWindow" + System.currentTimeMillis();
            DataStore.getSharedInstance().checkSymbolAvailability(SharedMethods.getSymbolFromKey(symbol), requestID, SharedMethods.getExchangeFromKey(symbol), SharedMethods.getInstrumentTypeFromKey(symbol));

//            SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(symbol), requestID, SharedMethods.getExchangeFromKey(symbol), SharedMethods.getInstrumentTypeFromKey(symbol));
            Client.getInstance().doTransaction(TradeMeta.SELL, symbol, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
        } else {
            Client.getInstance().addSymbolToWatchList(symbol, command, Meta.INSTRUMENT_OPTION);
        }
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            if (e.getSource().equals(comboMonths)) {
                try {
                    OptionStore.getSharedInstance().removeOpraQuoteObject(sKey, chainType);
                    selectedMonth = ((TWComboItem) comboMonths.getSelectedItem()).getId();
                    OptionStore.getSharedInstance().getOpraMap(sKey, chainType).clear();
                    OptionStore.getSharedInstance().addOpraQuoteObject(sKey, selectedMonth, selectedType, chainType);
                    oCallsViewSetting.putProperty(ViewConstants.VC_OPTION_MONTH, selectedMonth);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use Options | File Templates.
                }
            } else if (e.getSource().equals(nearSelector)) {
                JComboBox source = (JComboBox) e.getSource();
                if (source.getSelectedIndex() == 0) { // near the money
//                    OptionStore.getSharedInstance().removeOutOfMoneySymbols(sKey,chainType);
                    OptionStore.getSharedInstance().getOpraMap(sKey, chainType).setNearTheMoney(true);
                    oCallsViewSetting.putProperty(ViewConstants.VC_OPTION_NEAR_THE_MONEY, 1);
                    //OptionStore.getSharedInstance().registerOutOfMoneySymbols(OptionStore.getSharedInstance().getOpraMap(sKey),false);
                } else {
                    OptionStore.getSharedInstance().getOpraMap(sKey, chainType).setNearTheMoney(false);
                    oCallsViewSetting.putProperty(ViewConstants.VC_OPTION_NEAR_THE_MONEY, 0);
//                    OptionStore.getSharedInstance().registerOutOfMoneySymbols(OptionStore.getSharedInstance().getOpraMap(sKey,chainType),true);
                }
                drawOptions();
            } else { // near the money selector
                try {
                    OptionStore.getSharedInstance().removeOpraQuoteObject(sKey, chainType);
                    selectedType = Byte.parseByte(((TWComboItem) comboTypes.getSelectedItem()).getId());
                    OptionStore.getSharedInstance().getOpraMap(sKey, chainType).clear();
                    OptionStore.getSharedInstance().addOpraQuoteObject(sKey, selectedMonth, selectedType, chainType);
                    oCallsViewSetting.putProperty(ViewConstants.VC_OPTION_TYPE, selectedType);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use Options | File Templates.
                }
            }
        }
    }

    // Drag and Drop Listeners
    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent event) {
        String key = null;
        // Check if the drag came form prit button
        try {
            key = (String) event.getTransferable().getTransferData(DataFlavor.stringFlavor);
            if (!key.equals("Print")) {
                //System.out.println("Dropped object accepted! " + key);
                event.getDropTargetContext().dropComplete(true);
                if (key != null)
                    this.setSymbol(key);
            } else {
                event.rejectDrop(); // not from the print button. reject drop
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            event.rejectDrop();
            return;
        }
        key = null;
    }

    /**
     * Invoked when the mouse button has been clicked (pressed and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            Point location = e.getPoint();
            int column = callTable.getTable().columnAtPoint(location);
//            int row = callTable.getTable().rowAtPoint(location);
            if ((column == OptionsModel.CALL_STOCK_BID_PRICE_COLUMN) || (column == OptionsModel.PUT_STOCK_BID_PRICE_COLUMN)) {
//                Client.getInstance().doTransaction(TradeMeta.SELL, symbol, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                if (TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(SharedMethods.getExchangeFromKey(symbol))) {
                    Client.getInstance().doTransaction(TradeMeta.SELL, symbol, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                } else {
                    if (ExchangeStore.getSharedInstance().isMarketDepthByPriceAvailable(SharedMethods.getExchangeFromKey(symbol))) {
                        Client.getInstance().depthByPrice(symbol, false, Constants.MAINBOARD_TYPE);
                    } else {
                        Client.getInstance().showDetailQuote(symbol, true, false, false, LinkStore.LINK_NONE);
                    }
                }
            } else if ((column == OptionsModel.CALL_STOCK_ASK_PRICE_COLUMN) || (column == OptionsModel.PUT_STOCK_ASK_PRICE_COLUMN)) {
//                Client.getInstance().doTransaction(TradeMeta.BUY, symbol, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                if (TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(SharedMethods.getExchangeFromKey(symbol))) {
                    Client.getInstance().doTransaction(TradeMeta.BUY, symbol, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                } else {
                    if (ExchangeStore.getSharedInstance().isMarketDepthByPriceAvailable(SharedMethods.getExchangeFromKey(symbol))) {
                        Client.getInstance().depthByPrice(symbol, false, Constants.MAINBOARD_TYPE);
                    } else {
                        Client.getInstance().showDetailQuote(symbol, true, false, false, LinkStore.LINK_NONE);
                    }
                }
            } else {
                Client.getInstance().showDetailQuote(symbol, true, false, false, LinkStore.LINK_NONE);
            }


        }
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
        Point location = e.getPoint();
//        System.out.println("location=="+ location.getX() + " , "+ location.getY());
        int column = callTable.getTable().columnAtPoint(location);
        int row = callTable.getTable().rowAtPoint(location);
//        System.out.println("Column =="+column);
//        System.out.println("row =="+row);
        symbol = null;
        if (column < 7) {
            symbol = (String) callTable.getTable().getModel().getValueAt(row, -2);
        } else if (column > 7) {
            symbol = (String) callTable.getTable().getModel().getValueAt(row, -1);
        } else if (column == 7) {
            symbol = sKey;
        } else {
            symbol = null;
        }
//        System.out.println("symbol set==="+symbol);
    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
    }


    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        mnuRegionalQuotes.setVisible(Client.getInstance().isValidInfoType(symbol, Meta.IT_RegionalQuotes, true));
        mnuTimeNSales.setVisible(Client.getInstance().isValidInfoType(symbol, Meta.IT_SymbolTimeAndSales, true));
        mnuMDepthPrice.setVisible(Client.getInstance().isValidInfoType(symbol, Meta.IT_MarketDepthByPrice, true));
        mnuSnapQuote.setVisible(Client.getInstance().isValidInfoType(symbol, Meta.IT_SnapQuote, true));
        if (TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(SharedMethods.getExchangeFromKey(symbol))) {
            mnuBuy.setVisible(true);
            mnuSell.setVisible(true);
        } else {
            mnuBuy.setVisible(false);
            mnuSell.setVisible(false);
        }
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
