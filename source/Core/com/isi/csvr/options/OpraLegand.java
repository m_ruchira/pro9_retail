package com.isi.csvr.options;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 22, 2003
 * Time: 1:41:34 PM
 * To change this template use Options | File Templates.
 */
public class OpraLegand implements Icon {
    private final int WIDTH = 10;
    private final int HIGHT = 10;
    Color color1;
    Color color2;

    public OpraLegand() {
//        Theme.registerComponent(this);
    }

    public int getIconHeight() {
        return HIGHT;
    }

    public int getIconWidth() {
        return WIDTH;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2d = (Graphics2D) g;
        color1 = new Color(16645064);
//        color1 = Theme.getColor("OPTION_IN_MONEY_BG_COLOR");
        color2 = Theme.getColor("OPTION_IN_MONEY_FG_COLOR");
        g2d.setColor(color1);
        g2d.fillRect(x, y, WIDTH, HIGHT);
        g2d.setColor(color2);
        g2d.drawRect(x, y, WIDTH, HIGHT);
    }
}
