/*
 * (C) Copyright 2009-2010 Direct FN Technologies Limited. All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * Direct FN Technologies and constitute a TRADE SECRET of Direct FN Technologies Limited.
 *
 * Direct FN Technologies Limited retains all title to and intellectual property rights
 * in these materials.
 */
package com.isi.csvr.options;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

/**
 * com.isi.csvr.options.OptionStrategyStore
 */
public class OptionStrategyStore extends Object {

    private String key = null;
    private String optionExchange;
    private int optionInstrument;
    private String[] types;
    private String[] months;
    private byte selectedType;
    private Hashtable<String, ArrayList<Double>> strikePriceList;
    private Hashtable<String, String> expiryDates;
    private Hashtable<String, Hashtable<Double, OpraObject>> symbols;

    public OptionStrategyStore(String key) {
        this.key = key;
        strikePriceList = new Hashtable<String, ArrayList<Double>>();
        symbols = new Hashtable<String, Hashtable<Double, OpraObject>>();
        expiryDates = new Hashtable<String, String>();
    }

    public String getUnderlineKey() {
        return key;
    }

    public String[] getMonths() {
        return months;
    }

    public void setMonths(String[] months) {
        this.months = months;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    public void setExpirationDate(String selectedMonth, String expDate) {
        expiryDates.put(selectedMonth, expDate);
    }

    public String getExpirationDate(String selectedMonth) {
        return expiryDates.get(selectedMonth);
    }

    public String getOptionExchange() {
        return optionExchange;
    }

    public void setOptionExchange(String optionExchange) {
        this.optionExchange = optionExchange;
    }

    public int getOptionInstrument() {
        return optionInstrument;
    }

    public void setOptionInstrument(int optionInstrument) {
        this.optionInstrument = optionInstrument;
    }

    public void setSymbols(String selectedMonth, String[] data) {
        try {
            String record = null;
            String[] price_symbol = null;
            double strikePrice = 0;
            ArrayList<Double> list = new ArrayList<Double>();
            Hashtable<Double, OpraObject> opraSymbols = new Hashtable<Double, OpraObject>();
            for (int i = 0; i < data.length; i++) {
                try {
                    record = data[i];
                    if (!record.trim().equals("")) {
                        price_symbol = record.split("\\|", -3);
                        OpraObject opraObject = new OpraObject();
                        opraObject.setInstrumentType(optionInstrument);
                        strikePrice = Double.parseDouble(price_symbol[0]);
                        opraObject.setStrikePrice(strikePrice);
                        list.add(strikePrice);
                        try {
                            opraObject.setPutSymbol(price_symbol[1]);
                        } catch (Exception e) {
                        }
                        try {
                            opraObject.setCallSymbol(price_symbol[2]);
                        } catch (Exception e) {
                        }
                        opraSymbols.put(strikePrice, opraObject);
                    }
                } catch (Exception e) {
                    e.printStackTrace();//To change body of catch statement use File | Settings | File Templates.
                }
            }
            symbols.put(selectedMonth, opraSymbols);
            Collections.sort(list);
            strikePriceList.put(selectedMonth, list);
        } catch (Exception ex) {

        }

    }

    public byte getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(byte selectedType) {
        this.selectedType = selectedType;
    }

    public ArrayList<Double> getStrikePriceList(String month) {
        return strikePriceList.get(month);
    }

}
