package com.isi.csvr.options;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Jun 7, 2009
 * Time: 6:52:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface OptionStrategyListener {

    public void dataReceived(String key, String selectedMonth);
}
