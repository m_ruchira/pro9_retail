package com.isi.csvr.options;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 11, 2003
 * Time: 10:06:51 AM
 * To change this template use Options | File Templates.
 */
public class OpraMap {
    private String baseSymbol;
    private String exchange;
    private String currentMonth;
    private String expirationDate;
    private int instrumentType;
    private byte currentType;
    private String[] types;
    private String[] months;
    private String[] callSymbols;
    private String[] putSymbols;
    //    private ArrayList symbols;
    private ArrayList<OpraObject> nearTheMoneySymbols;
    private ArrayList outOfTheMoneySymbols;
    private OptionListener listener;
    private boolean removeBaseOnExit = false;
    private boolean nearTheMoney = false;
    private ArrayList<OpraObject> symbols;


    public OpraMap() {
        symbols = new ArrayList<OpraObject>();
        nearTheMoneySymbols = new ArrayList<OpraObject>();
    }

    public OpraMap(boolean removeBaseOnExit) {
        this.removeBaseOnExit = removeBaseOnExit;
        symbols = new ArrayList<OpraObject>();
        nearTheMoneySymbols = new ArrayList<OpraObject>();
    }

    public String getBaseSymbol() {
        return baseSymbol;
    }

    public void setBaseSymbol(String baseSymbol) {
        this.baseSymbol = baseSymbol;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(String currentMonth) {
        this.currentMonth = currentMonth;
    }

    public String[] getMonths() {
        return months;
    }

    public void setMonths(String[] months) {
        this.months = months;
    }

    public ArrayList getAllSymbols() {
        return symbols;
    }

    public ArrayList<OpraObject> getSymbols() {
        if (isNearTheMoney()) {
            return nearTheMoneySymbols;
        } else {
            return symbols;
        }
    }

    public void setSymbols(ArrayList symbols) {
        this.symbols = symbols;
    }

    public boolean isRemoveBaseOnExit() {
        return removeBaseOnExit;
    }

    public void removeListener() {
        listener = null;
    }

    public void addListener(OptionListener listener) {
        this.listener = listener;
    }

    public void fileOptionDataCompleted() {
        if (listener != null) {
            listener.optionDataReceived(this);
        }
    }

    public void clear() {
        try {
            months = null;
            symbols.clear();
            symbols.trimToSize();
            putSymbols = null;
            callSymbols = null;
            nearTheMoneySymbols.clear();
            nearTheMoneySymbols.trimToSize();
            nearTheMoneySymbols.clear();
            outOfTheMoneySymbols.clear();
            outOfTheMoneySymbols.trimToSize();
            outOfTheMoneySymbols = null;
        } catch (Exception e) {
        }
    }

    public boolean isNearTheMoney() {
        return nearTheMoney;
    }

    public void setNearTheMoney(boolean nearTheMoney) {
        this.nearTheMoney = nearTheMoney;
    }

    public ArrayList getNearTheMoneySymbols() {
        return nearTheMoneySymbols;
    }

    public void setNearTheMoneySymbols(ArrayList nearTheMoneySymbols) {
        this.nearTheMoneySymbols = nearTheMoneySymbols;
    }

    public ArrayList getOutOfTheMoneySymbols() {
        return outOfTheMoneySymbols;
    }

    public void setOutOfTheMoneySymbols(ArrayList outOfTheMoneySymbols) {
        this.outOfTheMoneySymbols = outOfTheMoneySymbols;
    }

    public byte getCurrentType() {
        return currentType;
    }

    public void setCurrentType(byte currentType) {
        this.currentType = currentType;
    }

    public String[] getTypes() {
        return types;
    }

    public void setTypes(String[] types) {
        this.types = types;
    }

    public String[] getCallSymbols() {
        return callSymbols;
    }

    public void setCallSymbols(String[] callSymbols) {
        this.callSymbols = callSymbols;
    }

    public String[] getPutSymbols() {
        return putSymbols;
    }

    public void setPutSymbols(String[] putSymbols) {
        this.putSymbols = putSymbols;
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(String expDate) {
        this.expirationDate = expDate;
    }
}
