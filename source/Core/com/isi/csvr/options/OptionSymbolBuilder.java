package com.isi.csvr.options;

import com.isi.csvr.Client;
import com.isi.csvr.ClientTable;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWDesktop;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.OptionStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class OptionSymbolBuilder
        extends JDialog implements Themeable {

    private static TWComboBox cmbStockLists;
    private static String baseExchange;
    private ArrayList<TWComboItem> stockLists = new ArrayList<TWComboItem>();
    private JPanel mainPanel = new JPanel();
    private JPanel buttonPanel;
    private TWTextField txtBaseSymbol = new TWTextField();
    private TWComboBox cmbExpMonth;
    private TWComboBox cmbExpStrike;
    private TWTextField txtPrice;
    private JRadioButton callButton;
    private JRadioButton putButton;
    private JButton btnSearch;
    private JButton btnAdd;
    private JButton btnClose;

    private Symbols symbolObject;
    private String baseSymbol;
    private boolean isActivatedFromMainMenu;
    private String[][] arrExpMonthsCodes;
    private String[][] arrStrikePrices;
    private String selectedPriceCode = null;
    private String priceFormat = "###.00";
    private DecimalFormat decimalFoarmat;

    public OptionSymbolBuilder(Frame parent, String title, boolean modal) {
        super(parent, title, modal);
        try {
            jbInit();
            Theme.registerComponent(this);
            pack();
            this.setResizable(false);
            decimalFoarmat = new DecimalFormat(priceFormat);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void setOptionSymbol(String sData) {
        String optSymbol = null;
        String optExchange = null;
        int optInstrument = Meta.INSTRUMENT_OPTION;
        String sKey = null;

        StringTokenizer st = null;
        st = new StringTokenizer(sData, Meta.FD, false);
        if (st.countTokens() <= 5) {
            new ShowMessage(Client.getInstance(), Language.getString("OPT_INVALID_OPTION_SYMBOL"), "I", false);
            return;
        }
        st.nextToken();     //Exchange
        st.nextToken();     //Symbol
        st.nextToken();     //Month
        st.nextToken();     //Strike Price
        st.nextToken();     //Type

        try {
            optExchange = st.nextToken().trim(); // Option Exchange
            optSymbol = st.nextToken().trim(); // Option Symbol
            optInstrument = Integer.parseInt(st.nextToken().trim()); // Option Instrument
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        sKey = SharedMethods.getKey(optExchange, optSymbol, optInstrument);
        String selWSName = ((TWComboItem) cmbStockLists.getSelectedItem()).getId();
        Client.getInstance().addSymbolToWatchList(sKey, selWSName, optInstrument);

    }

    private void jbInit() throws Exception {
        String[] arrWidths = {"40%", "60%"};
        String[] arrHeights = {"25", "25", "25", "25", "25", "25"};
        mainPanel.setLayout(new FlexGridLayout(arrWidths, arrHeights, 5, 2, true, true));

        JLabel lblBaseSymbol = new JLabel(Language.getString("OPT_BASE_SYMBOL")); // "Base Symbol");
        JLabel lblExpMonth = new JLabel(Language.getString("OPT_MONTH")); // "Month");
        JLabel lblPutCall = new JLabel("");
        JLabel lblStrikePrice = new JLabel(Language.getString("OPT_STRIKE_PRICE")); // "Strike Price");
        JLabel lblStockList = new JLabel(Language.getString("OPT_STOCK_LIST")); // "Stock List");

        cmbExpMonth = new TWComboBox();
        cmbExpStrike = new TWComboBox();
        cmbStockLists = new TWComboBox(new TWComboModel(stockLists));
        txtPrice = new TWTextField();

        txtPrice.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));

        callButton = new JRadioButton(Language.getString("OPT_CALL")); // "Call");
        callButton.setSelected(true);
        callButton.setOpaque(true);
        putButton = new JRadioButton(Language.getString("OPT_PUT")); // "Put");
        putButton.setOpaque(true);
        //Group the radio buttons.
        ButtonGroup group = new ButtonGroup();
        group.add(callButton);
        group.add(putButton);

        arrHeights = null;
        arrHeights = new String[1];
        arrHeights[0] = "20";
        JPanel callPutPanel = new JPanel(new FlexGridLayout(arrWidths, arrHeights, 5, 2, true, true));
        callPutPanel.add(callButton);
        callPutPanel.add(putButton);

        btnAdd = new JButton(Language.getString("BTN_ADD")); // "Add");
        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (txtBaseSymbol.getText().trim().equals("")) {
                    new ShowMessage(Language.getString("MSG_BASE_SYMBOL_CANOT_EMPTY"), "E");
                } else if (validateStrike(txtPrice.getText().trim())) {
                    generateOptionSymbol();
                    closeDialog();
                } else {
                    new ShowMessage(Language.getString("MSG_INVALID_STRIKE_PRICE"), "E");
                }

            }
        });
        btnClose = new JButton(Language.getString("BTN_CLOSE")); // "Close");
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
        });

        btnSearch = new JButton("..."); //Language.getString("BTN_SEARCH")); // "Search");
        btnSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchSymbol();
            }
        });

        txtBaseSymbol.setBorder(BorderFactory.createLineBorder(UIManager.getColor("controlShadow")));

        arrWidths = null;
        arrWidths = new String[2];
        arrWidths[0] = "80";
        arrWidths[1] = "20";
        JPanel symbolPanel = new JPanel(new FlexGridLayout(arrWidths, arrHeights, 5, 2, false, true)); //new FlowLayout(FlowLayout.LEADING, 5, 2));
        symbolPanel.add(txtBaseSymbol);
        symbolPanel.add(btnSearch);

        arrWidths = null;
        arrWidths = new String[2];
        arrWidths[0] = "50%";
        arrWidths[1] = "50%";
        buttonPanel = new JPanel(new FlexGridLayout(arrWidths, arrHeights, 5, 2, false, true));
        buttonPanel.add(btnAdd);
        buttonPanel.add(btnClose);

        JLabel nullLabel = new JLabel(" ");

        mainPanel.add(lblBaseSymbol);
        mainPanel.add(symbolPanel);
        mainPanel.add(lblPutCall);
        mainPanel.add(callPutPanel);
        mainPanel.add(lblExpMonth);
        mainPanel.add(cmbExpMonth);
        mainPanel.add(lblStrikePrice);
        mainPanel.add(txtPrice);//cmbExpStrike);
        mainPanel.add(lblStockList);
        mainPanel.add(cmbStockLists);
        mainPanel.add(nullLabel);
        mainPanel.add(buttonPanel);

        getContentPane().add(mainPanel);

        initializComponents();
        this.setSize(new Dimension(300, 195)); //200)); // 400
        this.setPreferredSize(new Dimension(300, 195)); //200)); // 400
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        GUISettings.applyOrientation(this);
    }

    private void restore() {
        txtPrice.setText("");
        cmbStockLists.setSelectedIndex(0);
        cmbExpMonth.setSelectedIndex(0);
    }

    public void setInvokeMethod(boolean isFromMenu) {
        isActivatedFromMainMenu = isFromMenu;
    }

    public void initializComponents() {
        readFromDataFiles();
        populateCombos();
    }

    public void populateWindowList() {
        try {
            String wsName = null;
            stockLists.clear();
            Object[] windowList = ((TWDesktop) Client.getInstance().getDesktop()).getWindows();
            if (windowList == null)
                return;

            JInternalFrame selectedframe = Client.getInstance().getDesktop().getSelectedFrame();
            for (int i = 0; i < windowList.length; i++) {
                try {
                    JInternalFrame frame = (JInternalFrame) windowList[i];
                    if (!(frame instanceof ClientTable)) {
                        continue;
                    }
                    wsName = frame.getTitle();
                    ViewSetting oSettings = ((ClientTable) frame).getViewSettings();
                    if (oSettings.isCutomType()) {
                        stockLists.add(new TWComboItem(oSettings.getID(), wsName));
                        try {
                            if (frame == selectedframe) {
                                cmbStockLists.setSelectedIndex(stockLists.size() - 1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                    frame = null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (cmbStockLists.getSelectedItem() == null) {
                cmbStockLists.setSelectedIndex(0);
            }
            wsName = null;
            windowList = null;
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        cmbStockLists.updateUI();
    }

    public void clearPreviousData() {

    }

    private void populateCombos() {
        float initValue = 0;
        float increment = 0;
        StringBuffer sData = null;

        cmbExpMonth.removeAllItems();
        cmbExpStrike.removeAllItems();

        SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cmbExpMonth.addItem(format.format(new Date()));
        Date date;
        for (int i = 1; i < 12; i++) {
            cal.add(Calendar.MONTH, 1);
            date = cal.getTime();
            cmbExpMonth.addItem(format.format(date));
        }
    }

    private boolean validateStrike(String value) {
        try {
            return (Float.parseFloat(value) > 0);
        } catch (Exception ex) {
            return false;
        }
    }

    private void readFromDataFiles() {
        String sParam = null;
        StringTokenizer st = null;

        try {
            baseExchange = OptionStore.getSharedInstance().getOptionBuilderProperty("OPRA_EXCHANGE");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        int counter = 0;
        try {
            sParam = OptionStore.getSharedInstance().getOptionBuilderProperty("EXP_MONTH_CODES");
            arrExpMonthsCodes = new String[12][2];
            st = new StringTokenizer(sParam, "|", false);
            while (st.hasMoreTokens()) {
                arrExpMonthsCodes[counter++] = st.nextToken().split(",");
            }
            st = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        counter = 0;
        try {
            sParam = OptionStore.getSharedInstance().getOptionBuilderProperty("STR_PRICE_CODES");
            st = new StringTokenizer(sParam, "|", false);
            arrStrikePrices = new String[st.countTokens()][3];
            while (st.hasMoreTokens()) {
                arrStrikePrices[counter++] = st.nextToken().split(",");
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        st = null;
        sParam = null;
    }

    private void searchSymbol() {
        this.setVisible(false);
        symbolObject = null;
        symbolObject = new Symbols((byte) Meta.INSTRUMENT_QUOTE);
        SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
        oCompanies.setTitle(Language.getString("WINDOW_TITLE_OPTION_SEARCH"));
        oCompanies.setSingleMode(true);
        oCompanies.setIndexSearchMode(false);
        oCompanies.init();
        oCompanies.setSymbols(symbolObject); //g_oTempTable.getSymbols());
        oCompanies.showDialog(true);
        try {
            this.setBaseSymbol(symbolObject.getSymbols()[0]);
        } catch (Exception ex) {
        }
        symbolObject = null;
        oCompanies = null;
        this.setVisible(true);
    }

    public void setBaseSymbol(String sKey) {
        if (sKey == null) {
            baseSymbol = "";
            txtBaseSymbol.setText("");
        } else {
            baseSymbol = sKey;
            try {
                txtBaseSymbol.setText(SharedMethods.getSymbolFromKey(sKey));
            } catch (Exception ex) {
                txtBaseSymbol.setText("");
            }
        }
        txtPrice.setText("");
    }

    public void showSymbolBuilder() {
        this.setVisible(true);
    }

    private void closeDialog() {
        this.setVisible(false);
    }

    private void generateOptionSymbol() {
        String sMonthCode = "";
        String reqString = "";
        byte type;
        float price;
        String symbol = txtBaseSymbol.getText();
        try {
            if (!symbol.equals(SharedMethods.getSymbolFromKey(baseSymbol))) {
                baseSymbol = DataStore.getSharedInstance().findKey(symbol);
            }
            if (callButton.isSelected()) {
                type = Meta.OPTION_SYMBOL_BUILDER_CALL_TYPE;
            } else {
                type = Meta.OPTION_SYMBOL_BUILDER_PUT_TYPE;
            }
            price = Float.parseFloat(txtPrice.getText().trim());

            sMonthCode = ((String) cmbExpMonth.getSelectedItem());
            reqString = Meta.OPTION_SYMBOL_SEARCH + Meta.DS + SharedMethods.getExchangeFromKey(baseSymbol) + Meta.FD + SharedMethods.getSymbolFromKey(baseSymbol) + Meta.FD +
                    sMonthCode + Meta.FD + decimalFoarmat.format(price) + Meta.FD + type + Meta.EOL;
            if (Settings.isConnected()) {
                SendQFactory.addContentData(Constants.CONTENT_PATH_SECONDARY, reqString, SharedMethods.getExchangeFromKey(baseSymbol));
            }
            sMonthCode = null;
            reqString = null;
        } catch (Exception e) {
        }
    }

    public void applyTheme() {
        txtBaseSymbol.updateUI();
        SwingUtilities.updateComponentTreeUI(mainPanel);
        SwingUtilities.updateComponentTreeUI(buttonPanel);
        SwingUtilities.updateComponentTreeUI(this);
    }
}