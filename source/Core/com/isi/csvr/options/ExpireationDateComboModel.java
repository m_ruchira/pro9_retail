package com.isi.csvr.options;

import com.isi.csvr.shared.DynamicArray;

import javax.swing.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class ExpireationDateComboModel extends DefaultComboBoxModel {

    DynamicArray array = null;

    public ExpireationDateComboModel() {
        array = new DynamicArray();
        array.setComparator(new ExpirationDateComparator());
    }

    public boolean addItem(Object item) {
        if (!array.contains(item)) {
            array.insert(item);
            return true;
        }
        return false;
    }

    public void clear() {
        array.clear();
    }

    public Object getElementAt(int index) {
        return ((ExpirationDate) array.get(index)).getCaption();
    }

    public int getSize() {
        return array.size();
    }

    public ExpirationDate getItemAt(int index) {
        return (ExpirationDate) array.get(index);
    }
}