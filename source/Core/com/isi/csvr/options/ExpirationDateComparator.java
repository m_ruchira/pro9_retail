package com.isi.csvr.options;

import java.util.Comparator;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class ExpirationDateComparator implements Comparator {

    public ExpirationDateComparator() {
    }

    public int compare(Object o1, Object o2) {
        ExpirationDate date1 = (ExpirationDate) o1;
        ExpirationDate date2 = (ExpirationDate) o2;

        return (date1.getCode().compareTo(date2.getCode()));
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}