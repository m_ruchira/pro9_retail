package com.isi.csvr.options;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.OptionStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class OptionsModel extends CommonTable implements TableModel, CommonTableInterface {

    public static final int BID_FLAG = 0;
    public static final int ASK_FLAG = 1;
    public static final int STRIKE_PRICE_PRICE_COLUMN = 7;
    public static final int CALL_STOCK_BID_PRICE_COLUMN = 3;
    public static final int CALL_STOCK_ASK_PRICE_COLUMN = 4;
    public static final int PUT_STOCK_BID_PRICE_COLUMN = 11;
    public static final int PUT_STOCK_ASK_PRICE_COLUMN = 12;
    private OpraMap optionTable;
    private DoubleTransferObject doubleObject;
    private LongTransferObject longObject;
    private StringTransferObject strObject;
    private byte chainType = 0;

    /**
     * Constructor
     */
    public OptionsModel(byte chainType) {
        this.chainType = chainType;
        doubleObject = new DoubleTransferObject();
        longObject = new LongTransferObject();
        strObject = new StringTransferObject();
        //this.key        = key;
    }

    public void setDataRegister(OpraMap hashOptions) {
        optionTable = hashOptions;
    }

    /* --- Table Modal's methods start from here --- */
    public int getColumnCount() {
        return getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if ((optionTable == null) || (optionTable.getSymbols() == null))
            return 0;
        else {
            return optionTable.getSymbols().size();
        }
    }

    public double getStrikePrice(int row) {
        OpraObject opraObject = optionTable.getSymbols().get(row);
        return opraObject.getStrikePrice();
    }

    public Object getValueAt(int iRow, int iCol) {

        String symbol = "N/A";
        Stock putStock = null;
        Stock callStock = null;

        if (optionTable == null) {
            return null;
        }

        try {
            OpraObject opraObject = optionTable.getSymbols().get(iRow); // (OpraQuote)optionTable.get(((String)keyArray[iRow]));
            putStock = DataStore.getSharedInstance().getStockObject(optionTable.getExchange(), opraObject.getPutSymbol(), opraObject.getInstrumentType());
            callStock = DataStore.getSharedInstance().getStockObject(optionTable.getExchange(), opraObject.getCallSymbol(), opraObject.getInstrumentType());
            switch (iCol) {
                case -1:
                    return putStock.getKey();
                case -2:
                    return callStock.getKey();
                case -3:
                    return optionTable.getExchange();
                case -6:
                    return chainType;
                case 0:
                    return strObject.setValue(opraObject.getCallSymbol());
                case 1:
                    doubleObject.setFlag(callStock.getLastTradeFlag());
                    return doubleObject.setValue(callStock.getLastTradeValue());
                case 2:
                    if (callStock.getChange() == (Constants.DEFAULT_DOUBLE_VALUE + 1)) {
                        return doubleObject.setValue(0D);
                    } else {
                        return doubleObject.setValue(callStock.getChange());
                    }
                case CALL_STOCK_BID_PRICE_COLUMN:
                    doubleObject.setFlag(BID_FLAG);
                    return doubleObject.setValue(callStock.getBestBidPrice());
                case CALL_STOCK_ASK_PRICE_COLUMN:
                    doubleObject.setFlag(ASK_FLAG);
                    return doubleObject.setValue(callStock.getBestAskPrice());
                case 5:
                    if (callStock.getVolume() == -1) {
                        return longObject.setValue(0);
                    } else {
                        return longObject.setValue(callStock.getVolume());
                    }
                case 6:
                    if (callStock.getOpenInterest() == -1) {
                        return doubleObject.setValue(0D);
                    } else {
                        return doubleObject.setValue(callStock.getOpenInterest());
                    }
                case STRIKE_PRICE_PRICE_COLUMN:
                    return doubleObject.setValue(opraObject.getStrikePrice());
                case 8:
                    return strObject.setValue(opraObject.getPutSymbol());
                case 9:
                    doubleObject.setFlag(putStock.getLastTradeFlag());
                    return doubleObject.setValue(putStock.getLastTradeValue());
                case 10:
                    if (putStock.getChange() == (Constants.DEFAULT_DOUBLE_VALUE + 1)) {
                        return doubleObject.setValue(0D);
                    } else {
                        return doubleObject.setValue(putStock.getChange());
                    }
                case PUT_STOCK_BID_PRICE_COLUMN:
                    doubleObject.setFlag(BID_FLAG);
                    return doubleObject.setValue(putStock.getBestBidPrice());
                case PUT_STOCK_ASK_PRICE_COLUMN:
                    doubleObject.setFlag(ASK_FLAG);
                    return doubleObject.setValue(putStock.getBestAskPrice());
                case 13:
                    if (putStock.getVolume() == -1) {
                        return longObject.setValue(0);
                    } else {
                        return longObject.setValue(putStock.getVolume());
                    }
                case 14:
                    if (putStock.getOpenInterest() == -1) {
                        return doubleObject.setValue(0D);
                    } else {
                        return doubleObject.setValue(putStock.getOpenInterest());
                    }
                default:
                    return "";
            }
        } catch (Exception e) {
            return null;
        }
    }


    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return Object.class;// getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[10];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));   //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("OPTION_TABLE_SELECTED_BGCOLOR"), Theme.getColor("OPTION_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_VALUE_UP_ROW, null, Theme.getColor("OPTION_TABLE_UP_FGCOLOR"));   //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[3] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_VALUE_DOWN_ROW, null, Theme.getColor("OPTION_TABLE_DOWN_FGCOLOR"));
        if (chainType == OptionStore.CHAIN_TYPE_FUTURES) {
            customizerRecords[4] = new CustomizerRecord(Language.getString("OPTION_CHAIN_STRIKE_ROW1"), FIELD_OPTION_CHAIN_STRIKE_ROW1, Theme.getColor("OPTION_TABLE_STRIKE_BGCOLOR1"), Theme.getColor("OPTION_TABLE_STRIKE_FGCOLOR1"));
            customizerRecords[5] = new CustomizerRecord(Language.getString("OPTION_CHAIN_STRIKE_ROW2"), FIELD_OPTION_CHAIN_STRIKE_ROW2, Theme.getColor("OPTION_TABLE_STRIKE_BGCOLOR2"), Theme.getColor("OPTION_TABLE_STRIKE_FGCOLOR2"));
            customizerRecords[6] = new CustomizerRecord(Language.getString("OPTION_CHAIN_PUTS_ROW1"), FIELD_OPTION_CHAIN_PUTS_ROW1, Theme.getColor("OPTION_TABLE_PUTS_BGCOLOR1"), Theme.getColor("OPTION_TABLE_PUTS_FGCOLOR1"));
            customizerRecords[7] = new CustomizerRecord(Language.getString("OPTION_CHAIN_PUTS_ROW2"), FIELD_OPTION_CHAIN_PUTS_ROW2, Theme.getColor("OPTION_TABLE_PUTS_BGCOLOR2"), Theme.getColor("OPTION_TABLE_PUTS_FGCOLOR2"));
            customizerRecords[8] = new CustomizerRecord(Language.getString("OPTION_CHAIN_CALLS_ROW1"), FIELD_OPTION_CHAIN_CALLS_ROW1, Theme.getColor("OPTION_TABLE_CALLS_BGCOLOR1"), Theme.getColor("OPTION_TABLE_CALLS_FGCOLOR1"));
            customizerRecords[9] = new CustomizerRecord(Language.getString("OPTION_CHAIN_CALLS_ROW2"), FIELD_OPTION_CHAIN_CALLS_ROW2, Theme.getColor("OPTION_TABLE_CALLS_BGCOLOR2"), Theme.getColor("OPTION_TABLE_CALLS_FGCOLOR2"));
        } else {
            customizerRecords[4] = new CustomizerRecord(Language.getString("FUTURES_CHAIN_STRIKE_ROW1"), FIELD_OPTION_CHAIN_STRIKE_ROW1, Theme.getColor("OPTION_TABLE_STRIKE_BGCOLOR1"), Theme.getColor("OPTION_TABLE_STRIKE_FGCOLOR1"));
            customizerRecords[5] = new CustomizerRecord(Language.getString("FUTURES_CHAIN_STRIKE_ROW2"), FIELD_OPTION_CHAIN_STRIKE_ROW2, Theme.getColor("OPTION_TABLE_STRIKE_BGCOLOR2"), Theme.getColor("OPTION_TABLE_STRIKE_FGCOLOR2"));
            customizerRecords[6] = new CustomizerRecord(Language.getString("FUTURES_CHAIN_PUTS_ROW1"), FIELD_OPTION_CHAIN_PUTS_ROW1, Theme.getColor("OPTION_TABLE_PUTS_BGCOLOR1"), Theme.getColor("OPTION_TABLE_PUTS_FGCOLOR1"));
            customizerRecords[7] = new CustomizerRecord(Language.getString("FUTURES_CHAIN_PUTS_ROW2"), FIELD_OPTION_CHAIN_PUTS_ROW2, Theme.getColor("OPTION_TABLE_PUTS_BGCOLOR2"), Theme.getColor("OPTION_TABLE_PUTS_FGCOLOR2"));
            customizerRecords[8] = new CustomizerRecord(Language.getString("FUTURES_CHAIN_CALLS_ROW1"), FIELD_OPTION_CHAIN_CALLS_ROW1, Theme.getColor("OPTION_TABLE_CALLS_BGCOLOR1"), Theme.getColor("OPTION_TABLE_CALLS_FGCOLOR1"));
            customizerRecords[9] = new CustomizerRecord(Language.getString("FUTURES_CHAIN_CALLS_ROW2"), FIELD_OPTION_CHAIN_CALLS_ROW2, Theme.getColor("OPTION_TABLE_CALLS_BGCOLOR2"), Theme.getColor("OPTION_TABLE_CALLS_FGCOLOR2"));
        }
        return customizerRecords;
    }

    /* --- Table Modal's metods end here --- */

}