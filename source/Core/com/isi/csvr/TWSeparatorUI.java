package com.isi.csvr;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.TWGradientImage;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSeparatorUI;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 11, 2005
 * Time: 10:02:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class TWSeparatorUI extends BasicSeparatorUI {
    static Rectangle textRect = new Rectangle();
    static Rectangle zeroRect = new Rectangle(0, 0, 0, 0);
    static Rectangle viewRect = new Rectangle(Short.MAX_VALUE, Short.MAX_VALUE);
    private static TWGradientImage gradientImage;
    protected Color shadow;
    protected Color highlight;
    protected JSeparator menuItem = null;
    private String caption;
    private FontMetrics fm;
    private int fontHeight;
    private int fontWidth;
    private int fontBase;


    public TWSeparatorUI(String caption) {
        if ((caption != null) && (caption.length() > 0))
            this.caption = " " + caption + " ";
        else
            this.caption = "";
    }

    public static ComponentUI createUI(JComponent c) {
        return new BasicSeparatorUI();
    }

    public void installUI(JComponent c) {
        installDefaults((JSeparator) c);
        installListeners((JSeparator) c);
        setBGColor(c);

        menuItem = (JSeparator) c;
        gradientImage = new TWGradientImage(Constants.OUTLOOK_MENU_COLUMN_WIDTH, menuItem.getHeight(), TWGradientImage.MENU);

        try {
            fm = ((JSeparator) c).getFontMetrics(Theme.getDefaultFont());
            if (caption.length() > 0) {
                fontWidth = SwingUtilities.computeStringWidth(fm, caption);
                fontHeight = fm.getMaxAscent() + fm.getMaxDescent();
            } else {
                fontWidth = 0;
                fontHeight = 5;
            }
            fontBase = fm.getAscent() + 2;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setBGColor(JComponent item) {
        Color backgroud = Theme.getOptionalColor("MENU_BGCOLOR");
        if (backgroud != null) {
            item.setOpaque(true);
            item.setBackground(backgroud);
        }
    }

    public void uninstallUI(JComponent c) {
        uninstallDefaults((JSeparator) c);
        uninstallListeners((JSeparator) c);
    }

    protected void installDefaults(JSeparator s) {
        LookAndFeel.installColors(s, "Separator.background", "Separator.foreground");
    }

    protected void uninstallDefaults(JSeparator s) {
    }

    protected void installListeners(JSeparator s) {
    }

    protected void uninstallListeners(JSeparator s) {
    }

    public void paint(Graphics g, JComponent c) {
        Dimension s = c.getSize();

        g.setColor(c.getForeground());
        g.drawLine(0, fontHeight / 2, (c.getWidth() - fontWidth) / 2, fontHeight / 2);
        g.drawLine((c.getWidth() - fontWidth) / 2 + fontWidth, fontHeight / 2, s.width, fontHeight / 2);

        g.setColor(c.getBackground());
        g.drawLine(0, fontHeight / 2 + 1, (c.getWidth() - fontWidth) / 2, fontHeight / 2 + 1);
        g.drawLine((c.getWidth() - fontWidth) / 2 + fontWidth, fontHeight / 2 + 1, s.width, fontHeight / 2 + 1);

        g.setColor(Theme.getMenuFontColor());
        if (caption.length() > 0)
            g.drawString(caption, (c.getWidth() - fontWidth) / 2, fontBase);

        g.setColor(Theme.MENU_SELECTION_COLOR);
        /*if (c.getComponentOrientation() == ComponentOrientation.LEFT_TO_RIGHT)
            g.fillRect(0, 0, Constants.OUTLOOK_MENU_COLUMN_WIDTH, c.getHeight());
        else
            g.fillRect(c.getWidth() - Constants.OUTLOOK_MENU_COLUMN_WIDTH, 0, Constants.OUTLOOK_MENU_COLUMN_WIDTH, c.getHeight());*/

        if (Constants.OUTLOOK_MENU_SIDE > 0) { // ltr
            gradientImage.setHeight(c.getHeight());
            gradientImage.paintIcon(c, g, 0, 0);
        } else {
            gradientImage.setHeight(c.getHeight());
            gradientImage.paintIcon(c, g, menuItem.getWidth() - Constants.OUTLOOK_MENU_COLUMN_WIDTH, 0);
        }

    }

    public Dimension getPreferredSize(JComponent c) {
        return new Dimension(0, fontHeight + 5);
    }

    public Dimension getMinimumSize(JComponent c) {
        return null;
    }

    public Dimension getMaximumSize(JComponent c) {
        return null;
    }
}
