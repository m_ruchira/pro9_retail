package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SnapQuoteModel;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Nov 3, 2008
 * Time: 4:40:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class SnapQuoteFrame extends InternalFrame implements LinkedWindowListener {

    String selectedKey = null;
    private boolean loadedFromWorkspace = false;
    private Table oDetailQuote;
    private ViewSetting oViewSetting;
    private ViewSettingsManager g_oViewSettings;
    private boolean isLinked;
    private SnapQuoteModel dqModel;
    private int fontSize = 0;
    private Dimension dim;
    private boolean symbolRegistered;

    public SnapQuoteFrame(Table oTable, WindowDaemon oDaemon, String selectedKey, boolean loadedFromWorkspace, boolean linked, boolean symbolRegistered, String linkgroup) {
        super(oTable, oDaemon);
        this.selectedKey = selectedKey;
        this.loadedFromWorkspace = loadedFromWorkspace;
        this.oDetailQuote = oTable;
        isLinked = linked;
        this.symbolRegistered = symbolRegistered;
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        setLinkedGroupID(linkgroup);
        initUI();

    }

    public void initUI() {

        oDetailQuote.hideHeader();
        oDetailQuote.getPopup().hideCopywHeadMenu();
        oDetailQuote.setSortingDisabled();
        oDetailQuote.setDQTableType();
        oDetailQuote.hideCustomizer();
//        oDetailQuote.getPopup().showSetDefaultMenu();
        oDetailQuote.setWindowType(ViewSettingsManager.SNAP_QUOTE);

        if (oViewSetting == null && loadedFromWorkspace && isLinked) {
            ViewSetting oSetting = g_oViewSettings.getSnapView(ViewSettingsManager.SNAP_QUOTE + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                oViewSetting = oSetting;
                fontSize = oViewSetting.getFont().getSize();
            }
        }
        if (oViewSetting == null) {
            oViewSetting = g_oViewSettings.getSnapView(ViewSettingsManager.SNAP_QUOTE
                    + "|" + selectedKey);
            if (oViewSetting != null) {
                fontSize = oViewSetting.getFont().getSize();
            }
        }
        fontSize = 0;
        if (oViewSetting == null) {
            oViewSetting = g_oViewSettings.getSymbolView("SNAP_QUOTE");
            oViewSetting = oViewSetting.getObject(); // clone the object
            //oViewSetting.setID(selectedKey);
            GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("SNAP_QUOTE_COLS"));
            SharedMethods.applyCustomViewSetting(oViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);

            oViewSetting.setID(System.currentTimeMillis() + "");
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            g_oViewSettings.putSnapView(ViewSettingsManager.SNAP_QUOTE
                    + "|" + selectedKey, oViewSetting);
            //todo added by Dilum
            fontSize = oViewSetting.getFont().getSize();
        } else {
            loadedFromWorkspace = true;
        }

        dqModel = new SnapQuoteModel();
        dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(selectedKey));
        dqModel.setSymbol(selectedKey);
        dqModel.setViewSettings(oViewSetting);
        oDetailQuote.setGroupableModel(dqModel);
        oDetailQuote.setUseSameFontForHeader(true);
        dqModel.setTable(oDetailQuote);
        oDetailQuote.getTable().getTableHeader().setReorderingAllowed(false);
        dqModel.applyColumnSettings();

        oDetailQuote.updateGUI();

        oDetailQuote.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        oDetailQuote.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        oDetailQuote.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        if (fontSize == 0) {
            fontSize = 18;
        }
        oViewSetting.setParent(this);
        this.setShowServicesMenu(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.setSize(oViewSetting.getSize());
//            frame.setSize(oViewSetting.getSize());
        this.setLocation(oViewSetting.getLocation());
        this.getContentPane().add(oDetailQuote);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setLinkGroupsEnabled(true);
        this.addComponentListener(this);        //L 4743 after the inner class
        //L 4781 inside  if
        Client.getInstance().oTopDesktop.add(this);
        this.setTitle(Language.getString("SNAP_QUOTE") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getStockObject(selectedKey).getLongDescription());
        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        this.updateUI();

        if (!symbolRegistered) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        } else if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }

        dqModel.updateGUI();

        this.setLayer(GUISettings.TOP_LAYER);
        this.applySettings();
        this.setOrientation();
        if (loadedFromWorkspace && isLinked) {
            g_oViewSettings.setWindow(ViewSettingsManager.SNAP_QUOTE + "|" +
                    LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            g_oViewSettings.setWindow(ViewSettingsManager.SNAP_QUOTE + "|" +
                    selectedKey, this);
        }
        //((SmartTable) oDetailQuote.getTable()).adjustColumnWidthsToFit(40);
        this.updateUI();
        oDetailQuote.packFrame();

        if (!loadedFromWorkspace) {

//                frame.setLocationRelativeTo(oTopDesktop);
//                oViewSetting.setLocation(frame.getLocation());
            if (!oViewSetting.isLocationValid()) {
                this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                oViewSetting.setLocation(this.getLocation());
            } else {
                this.setLocation(oViewSetting.getLocation());
            }
        }

        if (!loadedFromWorkspace) {
            this.setOrigDimention(this.getSize());
        } else {
            this.setOrigDimention(oViewSetting.getSize());
        }
        String requestID = Meta.PRICE_DEPTH + "|" + selectedKey;
        Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.PRICE_DEPTH);
        this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
        this.show();
        this.setisLoadedFromWsp(false);

    }

    public void checkForDetach() {
        if (oViewSetting != null && oViewSetting.isDetached && Settings.detachOnWspLoad) {
            try {
                this.setSize(oViewSetting.getSize());
                this.setLocation(oViewSetting.getLocation());
                Rectangle b = this.getBounds();
                b.setBounds((int) b.getX(), (int) b.getY(), (int) b.getWidth(), (int) oViewSetting.getSize().getHeight());
                this.setBounds(b);
                ((DetachableRootPane) this.getRootPane()).detach();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void componentResized(ComponentEvent e) {

        if (this.getOrigDimension().height > 0 && this.getOrigDimension().width > 0) {
            if (!oDetailQuote.isFontResized()) {
                dim = reSizeSpecial(this.getSize(), this.getOldSize());
                if (!this.getSize().equals(dim)) {
                    resizeRows(dim, this.getOrigDimension().height);
                }
                this.setSize(dim);
            } else {
                Dimension olddim = new Dimension(this.getOldSize().width, this.getSize().height);
                dim = resizeToFont(this.getSize(), olddim);
                this.setSize(dim);
                oDetailQuote.setFontResized(false);
            }
        }
        if (dim == null) {
            System.out.println("Dim null default size activated");
            Dimension dim2 = oViewSetting.getSize();
            this.setSize(dim2);
            this.setisLoadedFromWsp(true);
            this.setOldSize(dim2);
            this.setOrigDimention(new Dimension(620, 489));
            resizeRows(dim2, this.getOrigDimension().height);
        }
//                    tabRef2.getSmartTable().adjustColumnWidthsToFit(10);
    }

    public void resizeRows(Dimension dim, int origHeight) {
        Font oFont = oViewSetting.getFont();

        if (oFont == null) {
            oFont = oDetailQuote.getTable().getFont();
        }
//                    int defaultsize = oFont.getSize();

        int titlteHeight = this.getTitlebarHeight();
        float ratio = (((float) fontSize) / origHeight);

        int newSize = (int) (ratio * dim.height);


        if (oFont != null) {

            Font oFont2 = new TWFont(oFont.getFamily(), oFont.getStyle(), newSize);

            oDetailQuote.getTable().setFont(oFont2);
            oViewSetting.setFont(oFont2);
            FontMetrics oMetrices = oDetailQuote.getFontMetrics(oFont2);
            int rowCount = oDetailQuote.getTable().getRowCount();
            int emptySpace = ((dim.height - titlteHeight) - 2 - ((oMetrices.getMaxAscent() + oMetrices.getMaxDescent()) * rowCount));
            int newGap = (int) Math.floor((emptySpace / rowCount));

            oDetailQuote.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
            int finspace = (dim.height - titlteHeight) - 4 - (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap) * rowCount;
            if (finspace > 0) {
                int hight = this.dim.height - finspace;
                int width = this.dim.width;
                this.dim.setSize(width, hight);
                this.setOldSize(this.dim);
            }
            oMetrices = null;
            oFont = null;
            oFont2 = null;

        }
    }

    public void symbolChanged(String sKey) {


        if (sKey != null && !sKey.isEmpty()) {

            String oldsKey = oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
            DataStore.getSharedInstance().removeSymbolRequest(oldsKey);
            String oldreqid = super.getDataRequestID();
            RequestManager.getSharedInstance().removeRequest(oldreqid);

            dqModel.setSymbol(sKey);
            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sKey))) {
                DataStore.getSharedInstance().addSymbolRequest(sKey);
            }


            String requestID = Meta.PRICE_DEPTH + "|" + sKey;
            Client.getInstance().addDepthRequest(requestID, sKey, Meta.PRICE_DEPTH);
            this.setDataRequestID(DepthStore.getInstance(), sKey, requestID);
            this.getViewSetting().putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sKey);
            this.setTitle(Language.getString("SNAP_QUOTE") + " : (" + SharedMethods.getSymbolFromKey(sKey)
                    + ") " + DataStore.getSharedInstance().getStockObject(sKey).getLongDescription());
            this.updateUI();
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);

        }

        //To change body of implemented methods use File | Settings | File Templates.
    }
}
