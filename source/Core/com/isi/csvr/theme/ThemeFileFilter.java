package com.isi.csvr.theme;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class ThemeFileFilter implements FileFilter {
    byte[] name;
    String tag;
    int i = 0;

    public ThemeFileFilter() {
        name = new byte[2];
        tag = "ID";
    }

    public boolean accept(File pathname) {
//        byte[] name = new byte[2];
        if (pathname.getName().endsWith(".properties")) {
            try {
                FileInputStream in = new FileInputStream(pathname);
                i = 0;
                i = in.read(name);
                in.close();
                in = null;
                if (tag.equals(new String(name))) {
                    return true;
                } else {
                    return false;
                }
            } catch (IOException ex) {
                return false;
            }
        } else {
            return false;
        }
    }
}