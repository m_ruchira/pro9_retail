package com.isi.csvr.theme;

import javax.swing.border.Border;
import java.awt.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public interface TableThemeSettings {

    public static int GRID_THEME = -1;
    public static int GRID_NONE = 0;
    public static int GRID_HORIZONTAL = 1;
    public static int GRID_VERTICAL = 2;
    public static int GRID_BOTH = 3;

    public java.awt.Font getTableFont();

    public void setTableFont(java.awt.Font tableFont);

    public java.awt.Font getSelectedTableFont();

    public void setSelectedTableFont(java.awt.Font tableFont);

    public java.awt.Color getUpFGColor();

    public void setUpFGColor(java.awt.Color upFGColor);

    public java.awt.Color getUpBGColor();

    public void setUpBGColor(java.awt.Color upBGColor);

    public java.awt.Color getDownFGColor();

    public void setDownFGColor(java.awt.Color downFGColor);

    public java.awt.Color getDownBGColor();

    public void setDownBGColor(java.awt.Color downBGColor);
    //public void setNormalFGColor(java.awt.Color normalFGColor);
    //public java.awt.Color getNormalFGColor();

    public java.awt.Color getRow1BGColor();

    public void setRow1BGColor(java.awt.Color row1BGColor);

    public java.awt.Color getRow1FGColor();

    public void setRow1FGColor(java.awt.Color row1FGColor);

    public java.awt.Color getRow2BGColor();

    public void setRow2BGColor(java.awt.Color row2BGColor);

    public java.awt.Color getRow2FGColor();

    public void setRow2FGColor(java.awt.Color row2FGColor);

    public java.awt.Color getHighlighterFGColor();

    public void setHighlighterFGColor(java.awt.Color highlighterFGColor);

    public java.awt.Color getHighlighterBGColor();

    public void setHighlighterBGColor(java.awt.Color highlighterBGColor);

    public java.awt.Color getTradeFGColor1();

    public void setTradeFGColor1(java.awt.Color TradeFGColor);

    public java.awt.Color getTradeBGColor1();

    public void setTradeBGColor1(java.awt.Color TradeBGColor);

    public java.awt.Color getTradeFGColor2();

    public void setTradeFGColor2(java.awt.Color TradeFGColor);

    public java.awt.Color getTradeBGColor2();

    public void setTradeBGColor2(java.awt.Color TradeBGColor);

    public java.awt.Color getBidFGColor1();

    public void setBidFGColor1(java.awt.Color BidFGColor);

    public java.awt.Color getBidBGColor1();

    public void setBidBGColor1(java.awt.Color BidBGColor);

    public java.awt.Color getBidFGColor2();

    public void setBidFGColor2(java.awt.Color BidFGColor);

    public java.awt.Color getBidBGColor2();

    public void setBidBGColor2(java.awt.Color BidBGColor);

    public java.awt.Color getAskFGColor1();

    public void setAskFGColor1(java.awt.Color AskFGColor);

    public java.awt.Color getAskBGColor1();

    public void setAskBGColor1(java.awt.Color AskBGColor);

    public java.awt.Color getAskFGColor2();

    public void setAskFGColor2(java.awt.Color AskFGColor);

    public java.awt.Color getAskBGColor2();

    public void setAskBGColor2(java.awt.Color AskBGColor);

    public int getGdidType();

    public boolean isGdidOn();

    public void setGdidOn(int gdidOn);

    public Border getCellBorder();

    public java.awt.Color getTableGridColor();

    public void setTableGridColor(java.awt.Color gridColor);

    public java.awt.Color getHeadingFGColor();

    public void setHeadingFGColor(java.awt.Color headFGColor);

    public java.awt.Color getHeadingBGColor();

    public void setHeadingBGColor(java.awt.Color headingBGColor);

    public java.awt.Color getChangeUPFGColor();

    public void setChangeUPFGColor(java.awt.Color color);

    public java.awt.Color getChangeDownFGColor();

    public void setChangeDownFGColor(java.awt.Color color);

    public boolean isCuatomThemeEnabled();

    public void setCustomThemeEnabled(boolean status);

    public void updateUI();

    public Font getBodyFont();

    public void setBodyFont(Font font);

    public Font getHeadingFont();

    public void setHeadingFont(Font font);

    public void updateHeaderUI();

    public int getBodyGap();

    public void setBodyGap(int gap);

    //--------- added by Shanika----

    public java.awt.Color getMinBGColor();

    public void setMinBGColor(java.awt.Color minBGColor);

    public java.awt.Color getMinFGColor();

    public void setMinFGColor(java.awt.Color minFGColor);

    public java.awt.Color getMaxBGColor();

    public void setMaxBGColor(java.awt.Color maxBGColor);

    public java.awt.Color getMaxFGColor();

    public void setMaxFGColor(java.awt.Color maxFGColor);

    public java.awt.Color getWeek52HighBGColor();

    public void setWeek52HighBGColor(java.awt.Color week52HighBGColor);

    public java.awt.Color getWeek52HighFGColor();

    public void setWeek52HighFGColor(java.awt.Color week52HighFGColor);

    public java.awt.Color getWeek52LowBGColor();

    public void setWeek52LowBGColor(java.awt.Color week52LowBGColor);

    public java.awt.Color getWeek52LowFGColor();

    public void setWeek52LowFGColor(java.awt.Color week52LowFGColor);

    //-- Added by Shanika -- To enable/disable advanced table cuztomizer settings..

    public boolean isMinEnabled();

    public void setMinEnabled(boolean status);

    public boolean isMaxEnabled();

    public void setMaxEnabled(boolean status);

    public boolean isWk52LowEnabled();

    public void setWk52LowEnabled(boolean status);

    public boolean isWk52HighEnabled();

    public void setWk52HighEnabled(boolean status);


}