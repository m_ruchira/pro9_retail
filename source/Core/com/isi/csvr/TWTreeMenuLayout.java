package com.isi.csvr;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWGradientImage;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 7, 2005
 * Time: 4:30:54 PM
 */
public class TWTreeMenuLayout implements LayoutManager, Themeable {
    private JLabel outlookColumn;
    private TWGradientImage gradientImage;
    private boolean columnAdded = false;

    public TWTreeMenuLayout() {
        gradientImage = new TWGradientImage(Constants.OUTLOOK_MENU_COLUMN_WIDTH, 0, TWGradientImage.MENU);
        outlookColumn = new JLabel(gradientImage);
        outlookColumn.setOpaque(true);
        outlookColumn.setBackground(Theme.MENU_SELECTION_COLOR);
        Theme.registerComponent(this);
    }

    public void addLayoutComponent(String name, Component comp) {

    }

    public void layoutContainer(Container parent) {
        if (!columnAdded) {
            parent.add(outlookColumn);
            columnAdded = true;
        }

        Component scrollpane = parent.getComponent(0);
        Component tree = ((JScrollPane) parent.getComponent(0)).getViewport().getComponent(0);
        Dimension size = tree.getPreferredSize();
        //int height = Math.min((int)tree.getPreferredSize().getHeight(), 200);

        int height = getMaxPopupHeight(tree);
        height = Math.min((int) tree.getPreferredSize().getHeight(), height);
        //System.out.println("layoutContainer " + height);
        if (Language.isLTR()) {
            outlookColumn.setBounds(1, 1, Constants.OUTLOOK_MENU_COLUMN_WIDTH, height);
            gradientImage.setHeight(height);
            scrollpane.setBounds(Constants.OUTLOOK_MENU_COLUMN_WIDTH + 1, 1, size.width + 20, height);
        } else {
            outlookColumn.setBounds(size.width + 20, 1, Constants.OUTLOOK_MENU_COLUMN_WIDTH, height);
            gradientImage.setHeight(height);
            //scrollpane.setBounds(1, 1, size.width, size.height);
            scrollpane.setBounds(1, 1, size.width + 20, height);
        }
    }

    public Dimension minimumLayoutSize(Container parent) {
        return null;
    }

    public Dimension preferredLayoutSize(Container parent) {
        // add 2 pix for the border
        /*return new Dimension(parent.getComponent(0).getPreferredSize().width + 22 + Constants.OUTLOOK_MENU_COLUMN_WIDTH,
                parent.getComponent(0).getPreferredSize().height + 2);*/
        int height = getMaxPopupHeight(((JScrollPane) parent.getComponent(0)).getViewport().getComponent(0));
        return new Dimension(((JScrollPane) parent.getComponent(0)).getViewport().getComponent(0).getPreferredSize().width + 22 + Constants.OUTLOOK_MENU_COLUMN_WIDTH,
                (int) Math.min(((JScrollPane) parent.getComponent(0)).getViewport().getComponent(0).getPreferredSize().getHeight(), height) + 2);
    }

    public void removeLayoutComponent(Component comp) {

    }

    public void applyTheme() {
        outlookColumn.setBackground(Theme.MENU_SELECTION_COLOR);
    }

    private int getMaxPopupHeight(Component tree) {
        //Point treeLocation = tree.getLocation();
        //SwingUtilities.convertPointToScreen(treeLocation, tree.getParent());
        //int treeTop = (int)treeLocation.getY();
        int screenHeight = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        return screenHeight - 100;
    }
}
