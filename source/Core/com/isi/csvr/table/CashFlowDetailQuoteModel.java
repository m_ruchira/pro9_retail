package com.isi.csvr.table;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Aug 2, 2007
 * Time: 4:43:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowDetailQuoteModel extends CommonTable implements TableModel, CommonTableInterface {

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private static VariationImageTransferObject variationImageTransferObject = new VariationImageTransferObject();
    private String symbol;
    //private DepthObject depth;
    //private BidAsk bid;
    //private BidAsk ask;

    /**
     * Constructor
     */
    public CashFlowDetailQuoteModel() {
    }

    public void setSymbol(String symbolIn) {
        symbol = symbolIn;
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 3;
    }

    public int getRowCount() {
        return 6;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            if (symbol == null)
                return "";
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            //depth = DepthStore.getInstance().getDepthFor(symbol);
            //ask = (BidAsk)depth.getOrderList(DepthObject.ASK).get(iRow);
            //bid = (BidAsk)depth.getOrderList(DepthObject.BID).get(iRow);

            if (stock == null)
                return "";

            switch (iRow) {
                case 0:
                    switch (iCol) {
                        case 0:
                            return "";
                        case 1:
                            return Language.getString("CASH_IN");   // "Cash In";
                        case 2:
                            return Language.getString("CASH_OUT");   //"Cash Out";
                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CASH_DQ_ORDERS");   //"Orders";
                        case 1:
                            return longTrasferObject.setValue(stock.getCashInOrders());
                        case 2:
                            return longTrasferObject.setValue(stock.getCashOutOrders());
                    }
                case 2:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CASH_DQ_VOLUME");   //"Volume";
                        case 1:
                            return longTrasferObject.setValue(stock.getCashInVolume());
                        case 2:
                            return longTrasferObject.setValue(stock.getCashOutVolume());
                    }
                case 3:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CASH_DQ_TURNOVER");   //"Turnover";
                        case 1:
                            return doubleTransferObject.setValue(stock.getCashInTurnover());
                        case 2:
                            return doubleTransferObject.setValue(stock.getCashOutTurnover());
                    }
                case 4:
                    switch (iCol) {
                        case 0:
                            return "";   //"Cash Net";
                        case 1:
                            return Language.getString("CASH_DQ_NET");
                        case 2:
                            return doubleTransferObject.setValue(stock.getCashFlowNet());
                    }
                case 5:
                    switch (iCol) {
                        case 0:
                            return "";   //"Cash Map";
                        case 1:
                            return Language.getString("CASH_DQ_MAP");
                        case 2:
//                            doubleTransferObject.setValue(stock.getCashFlowPct());
                            variationImageTransferObject.setExchange(stock.getExchange());
                            return variationImageTransferObject.setValue(stock.getCashFlowPct());
                    }
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

//    public CustomizerRecord[] getCustomizerRecords() {
//        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
//        CustomizerRecord[] customizerRecords = new CustomizerRecord[8];
//        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
//        customizerRecords[4] = new CustomizerRecord(Language.getString("BID_ROW_1"), FIELD_DEP_BY_ORD_BID_ROW1, Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR1"), Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR1"));
//        customizerRecords[5] = new CustomizerRecord(Language.getString("BID_ROW_2"), FIELD_DEP_BY_ORD_BID_ROW2, Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR2"), Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR2"));
//        customizerRecords[6] = new CustomizerRecord(Language.getString("OFFER_ROW_1"), FIELD_DEP_BY_ORD_ASK_ROW1, Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR1"), Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR1"));
//        customizerRecords[7] = new CustomizerRecord(Language.getString("OFFER_ROW_2"), FIELD_DEP_BY_ORD_ASK_ROW2, Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR2"), Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR2"));
//        return customizerRecords;
//    }

}


