package com.isi.csvr.table;

import com.isi.csvr.datastore.ExchangeStore;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 16, 2005
 * Time: 2:18:57 PM
 */
public class TimeZoneModel extends CommonTable
        implements TableModel, CommonTableInterface {


    /**
     * Constructor
     */
    public TimeZoneModel() {
        //init();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return ExchangeStore.getSharedInstance().count();

    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int iRow, int iCol) {
        return "";
        /*try {

            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(iRow);

            switch (iCol) {
                case 0:
                    return exchange.getSymbol();
                case 1:
                    return exchange.getDescription();
                    //case 2:
                    //    return DataStore.getCompanyName(announcement.getSymbol());
                case 2:
                    return exchange.get
            }
            return "";
        } catch (Exception e) {
            return "";
        }*/
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        /*try{
            return getValueAt(0, iCol).getClass();
        }catch(Exception e){
            return String.class;
        }*/
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }
}
