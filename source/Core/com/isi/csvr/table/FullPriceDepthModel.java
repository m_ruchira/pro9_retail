// Copyright (c) 2000 Home

package com.isi.csvr.table;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Vector;

public class FullPriceDepthModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private static boolean ASK = true;
    private Vector g_oStore;


    /**
     * Constructor
     */
    public FullPriceDepthModel(Vector oStore) {
        g_oStore = oStore;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return g_oStore.size();
    }

    public Object getValueAt(int iRow, int iCol) {
        // Symbol,Description,Index,Today's initial,Net change,
        // % Change,Volume,Turnover,# Trades, # Ups,# Downs,#No Change,
        // # No Trade,52 Wk High,52 Wk High Date,52 Wk Low,52 Wk Low Date

        //if (!Language.isLTR()) {
        //	iCol = getIndexCount() -1 - iCol;
        //}
        //BidAsk oAsk = g_oStock.getAsk(iRow);
        //AssetClass oAssetClass = AssetStore.getAssetClassObject(sID);
        String[] asRow = (String[]) g_oStore.elementAt(iRow);
        //if (isLTR()) {
        switch (iCol) {
            case 0:
                return asRow[0];
            case 1:
                return asRow[1];
            case 2:
                return asRow[2];
            case 3:
                return asRow[3];
            default:
                return "";
        }
        /*}
        else
        {
            switch (iCol) {
                case 3:
                    return asRow[0];
                case 2:
                    return asRow[1];
                case 1:
                    return asRow[2];
                case 0:
                    return asRow[3];
                default:
                    return "";
            }
        }*/
    }

    public String getColumnName(int iCol) {
        //if (isLTR()) {
        return super.getViewSettings().getColumnHeadings()[iCol];
        //} else {
        //	return super.getViewSettings().getColumnHeadings()[super.getViewSettings().getColumnHeadings().length - 1 - iCol ];
        //}
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public void setSymbol(String symbol) {

    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

}