// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.announcement.AnnouncementStore;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


public class CompanyAnnounceModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private String symbol;
    //private AnnouncementStore  dataStore;

    /**
     * Constructor
     */
    public CompanyAnnounceModel(String symbol) {
        this.symbol = symbol;
//this.dataStore = dataStore;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return AnnouncementStore.getSharedInstance().getAnnouncementCount(symbol);
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int iRow, int iCol) {
        Announcement announcement = (Announcement) AnnouncementStore.getSharedInstance().getSymbolAnnouncements(symbol).get(iRow);

        switch (iCol) {
            case 0:
                return "" + announcement.getAnnouncementTime();
            case 1:
                return announcement.getHeadLine();
            case 2:
                return announcement.getMessage();
            case -4:
                return symbol;
            case -3:
                return announcement.getMessage();
            case -2:
                return announcement.getAnnouncementNo();
            case -1:
                return "" + announcement.getNewMessage();
            default:
                return "";
        }
    }

    public String getColumnName(int iCol) {
        //if (isLTR())
        //{
        return super.getViewSettings().getColumnHeadings()[iCol];
        //}
        //else
        //{
        //	return super.getViewSettings().getColumnHeadings()[super.getViewSettings().getColumnHeadings().length - 1 - iCol ];
        //}
    }

    public Class getColumnClass(int iCol) {
        //return getValueAt(0, iCol).getClass();
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */


}

