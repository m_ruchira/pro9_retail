package com.isi.csvr.table;

import com.isi.csvr.TWFileFilter;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.util.PageSetup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigInteger;

public class TWFileChooser extends JDialog
        implements ActionListener {

    public static final int COMPANY = 0;
    public static final int ALL = 1;

    public static final int STATUS_APPLY = 0;
    public static final int STATUS_CANCEL = 1;
    private int status = STATUS_CANCEL;
    public int pageSetupVal = 0;
    private SmartFileChooser fileChooser;
    private int type = 0;
    //change start
    private BigInteger columns = BigInteger.valueOf(0xFFFFFFFF);
    private AbstractButton saveButton;
    private AbstractButton cancelButton;
    private String path = null;

    public TWFileChooser(JFrame parent, String defaultPath) {
        super(parent, true);
        fileChooser = new SmartFileChooser(defaultPath);
        fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        fileChooser.setPreferredSize(new Dimension(550, 300));
        //fileChooser.addActionListener(this);

        //System.out.println(fileChooser.getSelectedFile());
        fileChooser.setControlButtonsAreShown(true);
        //fileChooser.addKeyListener(this);
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(fileChooser, BorderLayout.CENTER);
        this.getContentPane().add(getButtonPanel(), BorderLayout.SOUTH);
        saveButton = fileChooser.getButton(Language.getString("SAVE"));
        //System.out.println("Save button " + (saveButton == null));
        cancelButton = fileChooser.getButton(Language.getString("CANCEL"));
        fileChooser.hideContainerOf(Language.getString("SAVE"));

        this.pack();
    }

    private JPanel getButtonPanel() {
        JPanel buttonPanel = new JPanel();

        buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING, 10, 10));

        TWButton btnCustomize = new TWButton(Language.getString("CUSTOMIZE"));
        btnCustomize.setActionCommand("CUSTOM");
        btnCustomize.addActionListener(this);
        buttonPanel.add(btnCustomize);

        TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.addActionListener(this);
        buttonPanel.add(btnCancel);

        TWButton btnApply = new TWButton(Language.getString("EXPORT"));
        btnApply.setActionCommand("APPLY");
        btnApply.addActionListener(this);
        buttonPanel.add(btnApply);

        buttonPanel.registerKeyboardAction(this, "APPLY", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        buttonPanel.registerKeyboardAction(this, "CANCEL", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        //fileChooser.registerKeyboardAction(this,"CANCEL",KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0),JComponent.WHEN_IN_FOCUSED_WINDOW);

        return buttonPanel;
    }

    public int showDialog() {
        this.show();
        return status;
    }

    public JFileChooser getFileChooserComponent() {
        return fileChooser;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("APPLY")) {
            saveButton.doClick();
            if (fileChooser.getSelectedFile() != null) {
                path = fileChooser.getCurrentDirectory() + "\\" + fileChooser.getSelectedFile().getName();
                //System.out.println(fileChooser.getSelectedFile().getAbsolutePath());
                //fileChooser.getSelectedFile().getd

                if ((((TWFileFilter) fileChooser.getFileFilter()).getExtension()[0]).equals("doc")) {
                    PageSetup setup = new PageSetup(this);
                    setup.setLocationRelativeTo(this);
                    pageSetupVal = setup.showDialog();
                }
                status = STATUS_APPLY;
                this.hide();
            }
        } else if (e.getActionCommand().equals("CANCEL")) {
            cancelButton.doClick();
            status = STATUS_CANCEL;
            this.hide();
        } else if (e.getActionCommand().equals("CUSTOM")) {
            showSelectColumns();
        }
    }

    public void setTradeType(int type) {
        this.type = type;
    }

    private void showSelectColumns() {

        ViewSetting settings = new ViewSetting();

        switch (type) {
            case COMPANY:
                settings.setColumnHeadings(Language.getList("EXPORT_COMPANY_TRADES_COLS"));
                break;
            case ALL:
                settings.setColumnHeadings(Language.getList("EXPORT_ALL_TRADES_COLS"));
                break;
        }

        //change start
        settings.setColumns(columns); //select all columnd
        Customizer c = new Customizer(settings);
        c.showDialog();

        columns = settings.getColumns();
        settings = null;
        c = null;
    }


    public void setColumns(BigInteger columns) {
        this.columns = columns;
    }//change start

    public BigInteger getSelectedColumns() {
        return columns;
    }

    public String getPath() {
        return path;
    }

    public void setDialogTitle(String title) {
        this.setTitle(title);
    }

    public void setApplyButtonText(String text) {
        saveButton.setText(text);
    }

    /*public void keyTyped(KeyEvent e){
        System.out.println("types");
    }

    public void keyPressed(KeyEvent e){
        System.out.println("press");
    }

    public void keyReleased(KeyEvent e){
    }*/

}