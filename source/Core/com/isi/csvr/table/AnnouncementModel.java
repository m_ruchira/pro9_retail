// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class AnnouncementModel extends CommonTable
        implements TableModel {

    private DynamicArray dataStore;

    /**
     * Constructor
     */
    public AnnouncementModel() {
        //init();
    }

    public DynamicArray getDataStore() {
        return dataStore;
    }

    public void setDataStore(DynamicArray newDataStore) {
        dataStore = newDataStore;
    }

    public void setSymbol(String symbol) {

    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {

        if (dataStore != null) {
//System.out.println(dataStore. size());
            return dataStore.size();
        } else {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            Announcement announcement = (Announcement) dataStore.get(iRow);

            switch (iCol) {
                case 0:
                    return "" + announcement.getAnnouncementTime();
                case 1:
                    if (announcement.getSymbol() == null || announcement.getSymbol().equalsIgnoreCase("null"))
                        return "";
                    else
                        return announcement.getSymbol();
                case 2:
                    return UnicodeUtils.getNativeString(DataStore.getSharedInstance().getShortDescription(announcement.getKey()));
                case 3:
                    return UnicodeUtils.getNativeString(announcement.getHeadLine());
                case 4:
//                    return announcement.getExchange();
                    return ExchangeStore.getSharedInstance().getExchange(announcement.getExchange().trim()).getDisplayExchange(); //Display Exchange
                case 5:
                    return announcement.getShortDescription();
                case -3:
                    return announcement.getMessage();
                case -2:
                    return announcement.getAnnouncementNo();
                case -1:
                    return "" + announcement.getNewMessage();

            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[5];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEW_ANNOUNCEMENTS"), FIELD_NEW_ANNOUNCEMENT, null, Theme.getColor("ANNOUNCEMENT_NEW_LINE_FGCOLOR"));
        return customizerRecords;
    }

    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }
}