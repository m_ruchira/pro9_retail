// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

import com.isi.csvr.*;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.*;
import java.math.BigInteger;
import java.util.*;

/**
 * A Swing-based top level window class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class Customizer extends JDialog implements ActionListener,
        KeyListener, MouseListener, ItemListener, MouseMotionListener {
    private final int EXPORT_MODE = 0;
    private final int TABLE_MODE = 1;
    private int mode = TABLE_MODE;
    private final int CLIENT_TABLE_MODE = 2;
    BorderLayout borderLayout1 = new BorderLayout();
    JPanel containerPanel = new JPanel();
    Vector g_oUnselected = new Vector();
    /// JTextArea txtSelectedColumn = new JTextArea(); ///
    JList lstUnselectedList = new JList(g_oUnselected);
    JScrollPane scpScrollUnselected = new JScrollPane(lstUnselectedList);
    JPanel pnlUnselected = new JPanel();
    JLabel lblUnselectedHead = new JLabel();
    JLabel lblSelectedColumn = new JLabel();
    JLabel txtSelectedColumn = new JLabel();
    Vector g_oSelected = new Vector();
    JList lstSelectedList = new JList(g_oSelected);
    JScrollPane scpScrollSelected = new JScrollPane(lstSelectedList);
    JPanel pnlSelected = new JPanel();
    JLabel lblSelectedHead = new JLabel();
    FlowLayout flowLayout1 = new FlowLayout();
    FlowLayout flowLayout2 = new FlowLayout(FlowLayout.LEADING);
    JPanel buttonPanel = new JPanel();
    TWButton btnAdd = new TWButton();
    TWButton btnRemove = new TWButton();
    TWButton btnApply = new TWButton();
    TWButton btnCancel = new TWButton();
    //private boolean         g_bNormalMode = true;
    JPanel mainButtonPanel = new JPanel();
    JPanel infoPannel = new JPanel(new FlowLayout());
    private Table g_oSourceTable;
    private ViewSetting g_oViewSetting;
    private ClientTable g_oCTable;
    private Comparator comparator;
    private boolean showColumnTemplate = false;
    private ArrayList<TWComboItem> columnTemplate = new ArrayList<TWComboItem>();
    private TWComboBox cmbColumns;
    private JRadioButton thisWindowOnly;
    private JRadioButton allOpenedWindows;
    private JRadioButton allWindows;
    private int selectedColumn;
    private Hashtable indexTable = new Hashtable();

    /**
     * Constructs a new instance. for the use of history exporter
     */
    public Customizer(ViewSetting viewSetting) {
        super(Client.getInstance().getFrame(), true);
        showColumnTemplate = false;
        this.setTitle(Language.getString("TITLE_SELECT_COLUMNS"));
//g_oSourceTable = oTable;
        g_oViewSetting = viewSetting;
        comparator = new ListComparator();
        mode = EXPORT_MODE;
        try {
            jbInit();
            init();
            initLists();
            GUISettings.applyOrientation(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a new instance. for the use of Table
     */
    public Customizer(Table oTable) {

        super((Window) SwingUtilities.getAncestorOfClass(Window.class, oTable), Dialog.ModalityType.APPLICATION_MODAL);
        showColumnTemplate = false;
//        super(Client.getInstance().getFrame(), true);
//        Object c = SwingUtilities.getAncestorOfClass(JFrame.class,oTable);
        this.setTitle(Language.getString("TITLE_SELECT_COLUMNS"));
        g_oSourceTable = oTable;
        g_oViewSetting = g_oSourceTable.getModel().getViewSettings();
        mode = TABLE_MODE;
        comparator = new ListComparator();
        g_oSourceTable.saveColumnPositions(g_oViewSetting);
        try {
            jbInit();
            init();
            initLists();
            if (!Language.isLTR()) {
                GUISettings.applyOrientation(this, ComponentOrientation.RIGHT_TO_LEFT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a new instance. for the use of ClientTable
     */
    public Customizer(ClientTable oTable, ViewSetting oSettings) {
        super(Client.getInstance().getFrame(), true);
        showColumnTemplate = true;
        this.setTitle(Language.getString("TITLE_SELECT_COLUMNS"));
        g_oCTable = oTable;
        g_oViewSetting = oSettings;
        mode = CLIENT_TABLE_MODE;
        comparator = new ListComparator();
        try {
            jbInit();
            init();
            initLists();
            if (!Language.isLTR()) {
                GUISettings.applyOrientation(this, ComponentOrientation.RIGHT_TO_LEFT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a new instance. for the use of ClientTable
     */
    public Customizer(ClientTable oTable, ViewSetting oSettings, boolean showColumnTemplate) {
        super(Client.getInstance().getFrame(), true);
        this.showColumnTemplate = showColumnTemplate;
        this.setTitle(Language.getString("TITLE_SELECT_COLUMNS"));
        g_oCTable = oTable;
        g_oViewSetting = oSettings;
        mode = CLIENT_TABLE_MODE;
        comparator = new ListComparator();
        try {
            jbInit();
            init();
            initLists();
            if (!Language.isLTR()) {
                GUISettings.applyOrientation(this, ComponentOrientation.RIGHT_TO_LEFT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        lblSelectedHead.setText(Language.getString("SELECTED_ITEMS"));
        pnlSelected.setLayout(new BorderLayout());

/*if (!Language.isLTR()) {
            Client.applyOrientation(this,ComponentOrientation.RIGHT_TO_LEFT);
            //scpScrollSelected.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            //scpScrollUnselected.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            //--scpScrollSelected.getVerticalScrollBar().setMaximum(100); //scpScrollUnselected.getHorizontalScrollBar().getMaximum());
            //--scpScrollUnselected.getHorizontalScrollBar().setMaximum(100); //scpScrollUnselected.getHorizontalScrollBar().getMaximum());
        }*/

        pnlSelected.add(lblSelectedHead, BorderLayout.NORTH);
        pnlSelected.add(scpScrollSelected, BorderLayout.CENTER);

        lblUnselectedHead.setText(Language.getString("UNSELECTED_ITEMS"));
        pnlUnselected.setLayout(new BorderLayout());
        pnlUnselected.add(lblUnselectedHead, BorderLayout.NORTH);
        pnlUnselected.add(scpScrollUnselected, BorderLayout.CENTER);
        lblSelectedColumn.setText("");


        this.getContentPane().setLayout(borderLayout1);
        this.setResizable(false);
        this.setSize(new Dimension(467, 390));

        //scpScrollUnselected.setPreferredSize(new Dimension(190, 280));
        //scpScrollSelected.setPreferredSize(new Dimension(190, 280));
        pnlUnselected.setPreferredSize(new Dimension(190, 280));

        scpScrollUnselected.setPreferredSize(new Dimension(190, 272));
        scpScrollSelected.setPreferredSize(new Dimension(190, 200));

        buttonPanel.setPreferredSize(new Dimension(60, 280));
        btnAdd.setText(">");
        btnAdd.setBounds(new Rectangle(2, 90, 58, 30));
        btnAdd.addActionListener(this);
        btnAdd.addKeyListener(this);
        btnRemove.setText("<");
        btnRemove.setBounds(new Rectangle(2, 141, 58, 30));
        btnRemove.addActionListener(this);
        btnRemove.addKeyListener(this);
        btnApply.setPreferredSize(new Dimension(90, 30));
        btnApply.setText(Language.getString("APPLY"));
        btnApply.addActionListener(this);
        btnApply.addKeyListener(this);
        btnCancel.setPreferredSize(new Dimension(90, 30));
        btnCancel.setText(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);
        btnCancel.addKeyListener(this);
        mainButtonPanel.setPreferredSize(new Dimension(10, 40));
        buttonPanel.setLayout(null);

        lstUnselectedList.addMouseListener(this);
        lstUnselectedList.addMouseMotionListener(this);
        lstSelectedList.addMouseListener(this);
        lstSelectedList.addMouseMotionListener(this);
        lstUnselectedList.addKeyListener(this);
        lstSelectedList.addKeyListener(this);
        containerPanel.setLayout(flowLayout1);
        this.getContentPane().add(containerPanel, BorderLayout.CENTER);

        TWComboModel columnModel = new TWComboModel(columnTemplate);
        cmbColumns = new TWComboBox(columnModel);
        cmbColumns.setPreferredSize(new Dimension(100, 22));
        populateColumns();
        cmbColumns.addItemListener(this);
        JPanel newUnselectedPanel = new JPanel(new FlexGridLayout(new String[]{"0"}, new String[]{"0", "0"}, 0, 0));


        if (showColumnTemplate) {
            newUnselectedPanel.add(cmbColumns);//,BorderLayout.NORTH);
        } else {
            newUnselectedPanel.add(new JLabel());
        }
        newUnselectedPanel.add(pnlUnselected);//,BorderLayout.CENTER);
//        newUnselectedPanel.add(createSaveModePanel());//,BorderLayout.SOUTH);
        JPanel newSelectedPanel = new JPanel(new FlexGridLayout(new String[]{"0"}, new String[]{"0", "0", "0"}, 0, 0));
        JLabel dummy = new JLabel();

        if (showColumnTemplate) {
            dummy.setPreferredSize(new Dimension(100, 22));
            newSelectedPanel.add(dummy);
            newSelectedPanel.add(pnlSelected);
            newSelectedPanel.add(createSaveModePanel());
        } else {
//            dummy.setPreferredSize(new Dimension(100,22));
            newSelectedPanel.add(dummy);
            newSelectedPanel.add(pnlSelected);
            scpScrollSelected.setPreferredSize(new Dimension(190, 272));
            newSelectedPanel.add(new JLabel());
        }
//flowLayout2.setAlignment(SwingConstants.CENTER);
        if (Language.isLTR()) {
            btnAdd.setText(">");
            btnRemove.setText("<");
            buttonPanel.add(btnAdd, null);
            buttonPanel.add(btnRemove, null);
//containerPanel.add(pnlUnselected, null);
//containerPanel.add(buttonPanel, null);
//containerPanel.add(pnlSelected, null);
        } else {
            btnAdd.setText("<");
            btnRemove.setText(">");
            buttonPanel.add(btnRemove, null);
            buttonPanel.add(btnAdd, null);
            //containerPanel.add(pnlSelected, null);
//containerPanel.add(buttonPanel, null);
//containerPanel.add(pnlUnselected, null);
        }
        containerPanel.add(newUnselectedPanel, null);
        containerPanel.add(buttonPanel, null);
        containerPanel.add(newSelectedPanel, null);


        this.getContentPane().add(mainButtonPanel, BorderLayout.SOUTH);

        mainButtonPanel.setLayout(flowLayout2);
        lblSelectedColumn.setPreferredSize(new Dimension(255, 32));
        mainButtonPanel.add(lblSelectedColumn);
        btnApply.setPreferredSize(new Dimension(91, 25));
        btnCancel.setPreferredSize(new Dimension(91, 25));
        mainButtonPanel.add(btnApply);
        mainButtonPanel.add(btnCancel);

//btnCancel.registerKeyboardAction(this,"C",KeyStroke.getKeyStroke
//    (KeyEvent.VK_ESCAPE,0),JComponent.WHEN_IN_FOCUSED_WINDOW);
        // infoPannel.add(lblSelectedColumn);

        String[] Cols = Language.getList("TABLE_COLUMNS");

        for (int i = 0; i < Cols.length; i++) {
            indexTable.put(Cols[i], i);
        }

    }

    /*public void setTable(Table oTable)
    {
    	g_oSourceTable = oTable;
        init
    }*

    public static void main (String[] args)
    {
    	(new Customizer()).show();
    }*/
    public JPanel createSaveModePanel() {   //radio button Panel
        thisWindowOnly = new JRadioButton(Language.getString("THIS_WINDOW_ONLY"));
        allOpenedWindows = new JRadioButton(Language.getString("ALL_OPENED"));
        allWindows = new JRadioButton(Language.getString("ALL_WINDOWS"));
        thisWindowOnly.setSelected(true);
        ButtonGroup savingMode = new ButtonGroup();
        savingMode.add(thisWindowOnly);
        savingMode.add(allOpenedWindows);
        savingMode.add(allWindows);
        JPanel radioBtnPanel = new JPanel();
        radioBtnPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), Language.getString("APPLY_TO")));
        radioBtnPanel.setLayout(new FlexGridLayout(new String[]{"3%", "94%", "3%"}, new String[]{"33%", "33%", "34%"}, 0, 0));
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(thisWindowOnly);
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(allOpenedWindows);
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(allWindows);
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.setPreferredSize(new Dimension(new Dimension(190, 72)));
        GUISettings.applyOrientation(radioBtnPanel);
        return radioBtnPanel;
    }

    private void populateColumns() {
        columnTemplate.clear();
        columnTemplate.add(new TWComboItem("*", Language.getString("ALL_COLUMNS")));
        Enumeration<ColumnTemplate> templates = ColumnTemplateStore.getSharedInstance().getTemplates();
        ColumnTemplate template;
        while (templates.hasMoreElements()) {
            template = templates.nextElement();
            columnTemplate.add(new TWComboItem(template.getId(), template.getDescription()));
        }
        try {
            cmbColumns.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void init() {
        //g_oViewSetting = g_oSourceTable.getModel().getViewSettings();

        String[] asCols = g_oViewSetting.getColumnHeadings();

        for (int i = 0; i < asCols.length; i++) {
            g_oUnselected.addElement(asCols[i]);
        }

        lstSelectedList.updateUI();
    }

    public void keyPressed(KeyEvent e) {
        Object oSource = e.getSource();

        if (oSource == btnAdd) {
            addColumns();
            updateSelectedLabel();
        } else if (oSource == btnRemove) {
            removeColumns();
            updateSelectedLabel();
        } else if (oSource == btnApply) {
            applySettings();
        } else if (oSource == btnCancel) {
            cancelSettings();
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
            if (e.getSource() == lstUnselectedList) {
                Object selected = lstUnselectedList.getSelectedValue();
                Integer selectedIndex = (Integer) indexTable.get(selected);
                if (selectedIndex != null) {
                    //    String toolTip = getToolTipText(selectedIndex.intValue());
                    lblSelectedColumn.setText(getToolTipText(selectedIndex.intValue()));
                } else {
                    lblSelectedColumn.setText("");
                }


            } else if (e.getSource() == lstSelectedList) {
                Integer selectedIndex = (Integer) indexTable.get(lstSelectedList.getSelectedValue());
                if (selectedIndex != null) {
                    //    String toolTip = getToolTipText(selectedIndex.intValue());
                    lblSelectedColumn.setText(getToolTipText(selectedIndex.intValue()));
                } else {
                    lblSelectedColumn.setText("");
                }

            }
            String text = null;
            View v = (View) lblSelectedColumn.getClientProperty(BasicHTML.propertyKey);
            if (v != null) {

                Document d = v.getDocument();
//                                System.out.println("new"+d);
                try {
                    text = d.getText(0, d.getLength()).trim();
                } catch (BadLocationException e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                //sContent = (editorPane.getDocument()).getText(0,(editorPane.getDocument()).getLength());
            } else {
                text = lblSelectedColumn.getText();
            }


            lblSelectedColumn.setText(text);
        }
    }

    public void keyTyped(KeyEvent e) {
    }


    public void actionPerformed(ActionEvent e) {
        Object oSource = e.getSource();

        if (oSource == btnAdd) {
            addColumns();
            updateSelectedLabel();
        } else if (oSource == btnRemove) {
            removeColumns();
            updateSelectedLabel();
        } else if (oSource == btnApply) {
            if (showColumnTemplate) {
                if (thisWindowOnly.isSelected()) {
                    applySettings();
                } else if (allOpenedWindows.isSelected()) {
                    applySettingsForAllOpen();
                } else if (allWindows.isSelected()) {
                    applySettingsForAll();
                }
            } else {
                applySettings();
            }
//            applySettings();
        } else if (oSource == btnCancel) {
            cancelSettings();
        }
    }

    private void addColumns() {
        int[] aiSelected = null;

        aiSelected = lstUnselectedList.getSelectedIndices();
        if (aiSelected.length >= 0) {
            for (int i = 0; i < aiSelected.length; i++) {
                g_oSelected.addElement(g_oUnselected.elementAt(aiSelected[i]));
            }
            for (int i = 0; i < aiSelected.length; i++) {
                g_oUnselected.removeElementAt(aiSelected[i] - i);
            }
        }

        lstSelectedList.updateUI();
        lstUnselectedList.updateUI();
        lstUnselectedList.setSelectedIndices(new int[0]);
    }

    private void removeColumns() {
        int[] aiSelected = null;
        String sElement;
        aiSelected = lstSelectedList.getSelectedIndices();
        if ((aiSelected.length > 0) && (!lstSelectedList.isSelectionEmpty())) {
            for (int i = 0; i < aiSelected.length; i++) {
                sElement = (String) g_oSelected.elementAt(aiSelected[i]);
                if (sElement.endsWith(Language.getString("FIXED")))
                    aiSelected[i] = -1;
                else
                    g_oUnselected.addElement(sElement);

            }
            int j = 0;
            for (int i = 0; i < aiSelected.length; i++) {
                if (aiSelected[i] == -1) {
                    continue;
                } else {
                    g_oSelected.removeElementAt(aiSelected[i] - j);
                    j++;
                }
            }
            lstSelectedList.setSelectedIndices(new int[0]);
        }

        lstSelectedList.updateUI();
        lstUnselectedList.updateUI();
    }

    public void applySettings() {
        //change start
        BigInteger lCols = BigInteger.valueOf(0);
        String[] asColumns = g_oViewSetting.getColumnHeadings();
        Vector selectedCols = new Vector();

        if (g_oSelected.size() == 0) {
            new ShowMessage(false, Language.getString("MSG_LIST_CANNOT_EMPTY"), "E");
            return;
        }
        for (int i = 0; i < g_oSelected.size(); i++) {
            selectedCols.add(SharedMethods.getTextFromHTML((String) g_oSelected.get(i)));
        }
        for (int i = 0; i < asColumns.length; i++) {
            if (selectedCols.indexOf(asColumns[i]) != -1) {
                //change start
                lCols = lCols.add(pwr(i));
//                lCols += pwr(i);
            }
        }
        //change start
        g_oViewSetting.setColumns((lCols));
        switch (mode) {
            case TABLE_MODE:
                //g_oSourceTable.getModel().updateGUI();
                g_oSourceTable.getTable().getTableHeader().updateUI();
                g_oSourceTable.getModel().updateGUI();
                break;
            case CLIENT_TABLE_MODE:
                SortButtonRenderer oRend = (SortButtonRenderer) g_oCTable.getTable().getColumnModel().getColumn(0).getHeaderRenderer();
                oRend.setHeaderFont(g_oViewSetting.getHeaderFont());
                g_oCTable.adjustColumns();
                g_oCTable.updateGUI();

                break;
            case EXPORT_MODE:
                // do nothing
                break;
        }
        this.dispose();
    }

    public void applySettingsForAllOpen() {
        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i].isVisible()) {
                if (frames[i] instanceof ClientTable) {
                    ClientTable table = (ClientTable) frames[i];
                    ////////////////////////////////////////////////////
                    BigInteger lCols = BigInteger.valueOf(0);
                    String[] asColumns = table.getViewSettings().getColumnHeadings();
                    Vector selectedCols = new Vector();
                    if (g_oSelected.size() == 0) {
                        new ShowMessage(false, Language.getString("MSG_LIST_CANNOT_EMPTY"), "E");
                        return;
                    }
                    for (int j = 0; j < g_oSelected.size(); j++) {
                        selectedCols.add(SharedMethods.getTextFromHTML((String) g_oSelected.get(j)));
                    }
                    for (int k = 0; k < asColumns.length; k++) {
                        if (selectedCols.indexOf(asColumns[k]) != -1) {
                            lCols = lCols.add(pwr(k));
                        }
                    }
                    ////////////////////////////////////////////////////
                    table.getViewSettings().setColumns((lCols));
                    table.adjustColumns();
                    table.updateGUI();
                    SortButtonRenderer oRend = (SortButtonRenderer) table.getTable().getColumnModel().getColumn(0).getHeaderRenderer();
                    oRend.setHeaderFont(table.getViewSettings().getHeaderFont());
                    table = null;
                }
            }
        }
        frames = null;
        this.dispose();
    }

    public void applySettingsForAll() {

        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i] instanceof ClientTable) {
                ClientTable table = (ClientTable) frames[i];
                ////////////////////////////////////////////////////
                BigInteger lCols = BigInteger.valueOf(0);
                String[] asColumns = table.getViewSettings().getColumnHeadings();
                Vector selectedCols = new Vector();
                if (g_oSelected.size() == 0) {
                    new ShowMessage(false, Language.getString("MSG_LIST_CANNOT_EMPTY"), "E");
                    return;
                }
                for (int j = 0; j < g_oSelected.size(); j++) {
                    selectedCols.add(SharedMethods.getTextFromHTML((String) g_oSelected.get(j)));
                }
                for (int k = 0; k < asColumns.length; k++) {
                    if (selectedCols.indexOf(asColumns[k]) != -1) {
                        lCols = lCols.add(pwr(k));
                    }
                }
                ////////////////////////////////////////////////////
                table.getViewSettings().setColumns((lCols));
                table.adjustColumns();
                table.updateGUI();
                SortButtonRenderer oRend = (SortButtonRenderer) table.getTable().getColumnModel().getColumn(0).getHeaderRenderer();
                oRend.setHeaderFont(table.getViewSettings().getHeaderFont());
                table = null;
            }
        }
        frames = null;
        this.dispose();
    }

    /**
     * Loads lists from column names
     */
//    private void initLists() {
////        String[] asSelected;
////        String[] asUnSelected;
//
//        g_oSelected.removeAllElements();
//        g_oUnselected.removeAllElements();
//        lstUnselectedList.updateUI();
//        lstSelectedList.updateUI();
//
//        //change start
//        BigInteger lSelectedPos = (g_oViewSetting.getColumns().or(g_oViewSetting.getFixedColumns())).and(g_oViewSetting.getHiddenColumns().not());
//        BigInteger lFixedPos = g_oViewSetting.getFixedColumns();
//        BigInteger lHiddenPos = g_oViewSetting.getHiddenColumns();
//
////        long lSelectedPos = (g_oViewSetting.getColumns() | g_oViewSetting.getFixedColumns()) & ~g_oViewSetting.getHiddenColumns();
////        long lFixedPos = g_oViewSetting.getFixedColumns();
////        long lHiddenPos = g_oViewSetting.getHiddenColumns();
//        //change end
//        String[] asColumns = g_oViewSetting.getColumnHeadings();
//
//        for (int i = 0; i < asColumns.length; i++) {
//            //change start
//            if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
////            if (((lHiddenPos & 1) == 0)) // make sure the col is not hidden
//            {
//                //change start
//                if ((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
////                if ((lSelectedPos & 1) == 0) {
//                    g_oUnselected.add(asColumns[i].trim());
//                } else {
//                    //change start
//                    if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))
////                    if ((lFixedPos & 1) == 0)
//                        g_oSelected.add(asColumns[i].trim());
//                }
//            }
//            //change start
//            lSelectedPos = lSelectedPos.shiftRight(1);
//            lFixedPos = lFixedPos.shiftRight(1);
//            lHiddenPos = lHiddenPos.shiftRight(1);
////            lSelectedPos >>= 1;
////            lFixedPos >>= 1;
////            lHiddenPos >>= 1;
//            //change end
//        }
//        sortLists();
////        Object[] tempArray = g_oSelected.toArray();
////        Arrays.sort(tempArray);
////        g_oSelected.clear();
////        for (int k = 0; k < tempArray.length; k++) {
////            g_oSelected.addElement(tempArray[k]);
////        }
////        tempArray = null;
////
////        tempArray = g_oUnselected.toArray();
////        Arrays.sort(tempArray);
////        g_oUnselected.clear();
////        for (int k = 0; k < tempArray.length; k++) {
////            g_oUnselected.addElement(tempArray[k]);
////        }
////        tempArray = null;
////
////        lstUnselectedList.updateUI();
////        lstSelectedList.updateUI();
////
//    }
    public void sortLists() {
        Object[] tempArray = g_oSelected.toArray();
        Arrays.sort(tempArray, comparator);
        g_oSelected.clear();
        for (int k = 0; k < tempArray.length; k++) {
            g_oSelected.addElement(tempArray[k]);
        }
        tempArray = null;

        tempArray = g_oUnselected.toArray();
        Arrays.sort(tempArray, comparator);
        g_oUnselected.clear();
        for (int k = 0; k < tempArray.length; k++) {
            g_oUnselected.addElement(tempArray[k]);
        }
        tempArray = null;

        lstUnselectedList.updateUI();
        lstSelectedList.updateUI();
    }

    public void cancelSettings() {
        this.dispose();
    }

    /**
     * Calculate the 2 to the power fo the given number
     */
//    private long pwr(long iPwr) {
//        long i = 1;
//        return (i << iPwr);
//    }

    //change start

    /**
     * Calculate the 2 to the power fo the given number
     */
    private BigInteger pwr(int iPwr) {
        BigInteger i = BigInteger.valueOf(1);
        return (i.shiftLeft(iPwr));
    }
    //change end

    public void showDialog() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
        init();
        initLists();
        show();
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            if (e.getSource() == lstUnselectedList) {
                addColumns();
                updateSelectedLabel();
            } else if (e.getSource() == lstSelectedList) {
                removeColumns();
                updateSelectedLabel();
            }
        } else if (e.getClickCount() == 1) {

            if (e.getSource() == lstUnselectedList) {
                Integer selectedIndex = (Integer) indexTable.get(lstUnselectedList.getSelectedValue());
                if (selectedIndex != null) {
                    //    String toolTip = getToolTipText(selectedIndex.intValue());
                    lblSelectedColumn.setText(getToolTipText(selectedIndex.intValue()));
                } else {
                    lblSelectedColumn.setText("");
                }


            } else if (e.getSource() == lstSelectedList) {
                Integer selectedIndex = (Integer) indexTable.get(lstSelectedList.getSelectedValue());
                if (selectedIndex != null) {
                    //    String toolTip = getToolTipText(selectedIndex.intValue());
                    lblSelectedColumn.setText(getToolTipText(selectedIndex.intValue()));
                } else {
                    lblSelectedColumn.setText("");
                }


            } else {
                lblSelectedColumn.setText("");
            }

            String text = null;
            View v = (View) lblSelectedColumn.getClientProperty(BasicHTML.propertyKey);
            if (v != null) {

                Document d = v.getDocument();
                try {
                    text = d.getText(0, d.getLength()).trim();
                } catch (BadLocationException e1) {
                    e1.printStackTrace();
                }

            } else {
                text = lblSelectedColumn.getText();
            }
            lblSelectedColumn.setText(text);

        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }


    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == cmbColumns) {
            if ((e.getStateChange() == e.SELECTED)) {
                try {
                    ColumnTemplate template = ColumnTemplateStore.getSharedInstance().getColumnTemplate(Integer.parseInt(((TWComboItem) cmbColumns.getSelectedItem()).getId()));
                    initUnselectedList(template.getAllColumns(), false);
                } catch (NumberFormatException e1) {
                    initUnselectedList(g_oViewSetting.getColumns(), true);
                }
            }
        }
    }

    /**
     * Loads lists from column names
     */
    private void initUnselectedList(BigInteger columns, boolean allColumns) {
//        String[] asSelected;
//        String[] asUnSelected;

//        g_oSelected.removeAllElements();
        g_oUnselected.removeAllElements();


        lstUnselectedList.updateUI();
//        lstSelectedList.updateUI();

        //change start
        BigInteger lSelectedPos;
        if (!allColumns) {
            lSelectedPos = columns;
        } else {
            lSelectedPos = (g_oViewSetting.getColumns().or(g_oViewSetting.getFixedColumns())).and(g_oViewSetting.getHiddenColumns().not());
        }
//        BigInteger lSelectedPos = (g_oViewSetting.getColumns().or(g_oViewSetting.getFixedColumns())).and(g_oViewSetting.getHiddenColumns().not());
//        BigInteger lFixedPos = g_oViewSetting.getFixedColumns();
        BigInteger lHiddenPos = g_oViewSetting.getHiddenColumns();
        Vector selectedCols = new Vector();

        for (int i = 0; i < g_oSelected.size(); i++) {
            selectedCols.add(SharedMethods.getTextFromHTML((String) g_oSelected.get(i)));
        }
        g_oSelected.clear();
//        long lSelectedPos = (g_oViewSetting.getColumns() | g_oViewSetting.getFixedColumns()) & ~g_oViewSetting.getHiddenColumns();
//        long lFixedPos = g_oViewSetting.getFixedColumns();
//        long lHiddenPos = g_oViewSetting.getHiddenColumns();
        //change end
        String[] asColumns = g_oViewSetting.getColumnHeadings();

        for (int i = 0; i < asColumns.length; i++) {
            //change start
            if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
//            if (((lHiddenPos & 1) == 0)) // make sure the col is not hidden
            {
                //change start
                boolean condition = true;
                if (!allColumns) {
                    condition = (lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1));
                } else {
                    condition = (lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0));
                }
                if (condition && !selectedCols.contains(asColumns[i])) {
//                if ((lSelectedPos & 1) == 0) {
                    g_oUnselected.add(asColumns[i]);
                } else if (!condition && !selectedCols.contains(asColumns[i])) {
                    if (allColumns)
                        g_oUnselected.add(asColumns[i]);
                } else if (!condition && selectedCols.contains(asColumns[i])) {
                    if (!allColumns) {
                        selectedCols.remove(asColumns[i]);
                        selectedCols.add("<html><font color = red>" + asColumns[i]);
                    }
                }
            }
            //change start
            lSelectedPos = lSelectedPos.shiftRight(1);
//            lFixedPos = lFixedPos.shiftRight(1);
            lHiddenPos = lHiddenPos.shiftRight(1);
//            columns = columns.shiftRight(1);
//            lSelectedPos >>= 1;
//            lFixedPos >>= 1;
//            lHiddenPos >>= 1;
            //change end
        }
        for (int i = 0; i < selectedCols.size(); i++) {
            g_oSelected.add((selectedCols.get(i)));
        }
        sortLists();
//        Object[] tempArray = g_oSelected.toArray();
//        Arrays.sort(tempArray);
//        g_oSelected.clear();
//        for (int k = 0; k < tempArray.length; k++) {
//            g_oSelected.addElement(tempArray[k]);
//        }
//        tempArray = null;
//
//        tempArray = g_oUnselected.toArray();
//        Arrays.sort(tempArray);
//        g_oUnselected.clear();
//        for (int k = 0; k < tempArray.length; k++) {
//            g_oUnselected.addElement(tempArray[k]);
//        }
//        tempArray = null;
//
//        lstUnselectedList.updateUI();
//        lstSelectedList.updateUI();
//
    }

    public void mouseDragged(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseMoved(MouseEvent e) {
        if (e.getSource() == lstUnselectedList) {

            int index = lstUnselectedList.locationToIndex(e.getPoint());
            Object selectedObject = lstUnselectedList.getModel().getElementAt(index);
            Integer selectedIndex = (Integer) indexTable.get(selectedObject);
            if (selectedIndex != null) {
                //    String toolTip = getToolTipText(selectedIndex.intValue());
                lstUnselectedList.setToolTipText(getToolTipText(selectedIndex.intValue()));
            } else {
                lstUnselectedList.setToolTipText("");
            }


        } else if (e.getSource() == lstSelectedList) {
            int index = lstSelectedList.locationToIndex(e.getPoint());
            Object selectedObject = lstSelectedList.getModel().getElementAt(index);
            Integer selectedIndex = (Integer) indexTable.get(selectedObject);
            if (selectedIndex != null) {
                //    String toolTip = getToolTipText(selectedIndex.intValue());
                lstSelectedList.setToolTipText(getToolTipText(selectedIndex.intValue()));
            } else {
                lstSelectedList.setToolTipText("");
            }


        }

    }

    public String getToolTipText(int selectedColumn) {
        try {

            return Language.getList("TABLE_COLUMN_TOOLTIPS")[selectedColumn];

        } catch (Exception e) {
            return null;
        }
    }

    private void updateSelectedLabel() {

        if (lstSelectedList.getSelectedValue() == null && lstUnselectedList.getSelectedValue() == null) {
            lblSelectedColumn.setText("");
        } else if (lstUnselectedList.getSelectedValue() != null) {
            Integer selectedIndex = (Integer) indexTable.get(lstUnselectedList.getSelectedValue());
            lblSelectedColumn.setText(getToolTipText(selectedIndex.intValue()));
        } else if (lstSelectedList.getSelectedValue() != null) {
            Integer selectedIndex = (Integer) indexTable.get(lstSelectedList.getSelectedValue());
            lblSelectedColumn.setText(getToolTipText(selectedIndex.intValue()));
        }

        String text = null;
        View v = (View) lblSelectedColumn.getClientProperty(BasicHTML.propertyKey);
        if (v != null) {

            Document d = v.getDocument();
            try {
                text = d.getText(0, d.getLength()).trim();
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }

        } else {
            text = lblSelectedColumn.getText();
        }
        lblSelectedColumn.setText(text);
    }

    private void initLists() {
        g_oSelected.removeAllElements();
        g_oUnselected.removeAllElements();
        lstUnselectedList.updateUI();
        lstSelectedList.updateUI();

        BigInteger lSelectedPos = (g_oViewSetting.getColumns().or(g_oViewSetting.getFixedColumns())).and(g_oViewSetting.getHiddenColumns().not());
        BigInteger lFixedPos = g_oViewSetting.getFixedColumns();
        BigInteger lHiddenPos = g_oViewSetting.getHiddenColumns();

        String[] asColumns = g_oViewSetting.getColumnHeadings();

        for (int i = 0; i < asColumns.length; i++) {
            if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
            {
                if ((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {  // unselected column
                    if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) { // not if fixed add to list
                        g_oUnselected.add(asColumns[i].trim());
                    }
                } else {
                    if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) { // not if fixed add to list
                        if (asColumns[i].trim() != "")
                            g_oSelected.add(asColumns[i].trim());
                    }
                }
            }
            lSelectedPos = lSelectedPos.shiftRight(1);
            lFixedPos = lFixedPos.shiftRight(1);
            lHiddenPos = lHiddenPos.shiftRight(1);
        }
        sortLists();
    }

    class ListComparator implements Comparator {

        public int compare(Object o1, Object o2) {
            return (SharedMethods.getTextFromHTML((String) o1)).compareToIgnoreCase(SharedMethods.getTextFromHTML((String) o2));
        }

    }
}



