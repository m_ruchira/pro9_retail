// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

import com.isi.csvr.ImageBorder;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.variationmap.VariationImage;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;


public class ExchangeTableRenderer extends TWBasicTableRenderer {
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static ImageIcon collapseImage;
    private static ImageIcon expandImage;
    private static ImageIcon nochildImage;
    private static ImageBorder imageBorder;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private String[] g_asMarketStatus = new String[7];
    private String g_sNA = "NA";
    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private DefaultTableCellRenderer lblRenderer;
    private double doubleValue;
    private VariationImage variationImage;
    private boolean isexDefault = true;
    private TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");


    public ExchangeTableRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        variationImage = new VariationImage();
        reload();

        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");

        try {
            g_asMarketStatus[0] = Language.getString("STATUS_PREOPEN");
            g_asMarketStatus[1] = Language.getString("STATUS_OPEN");
            g_asMarketStatus[2] = Language.getString("STATUS_CLOSE");
            g_asMarketStatus[3] = Language.getString("STATUS_PRECLOSE");
            g_asMarketStatus[4] = Language.getString("STATUS_PRECLOSE");
            g_asMarketStatus[5] = Language.getString("STATUS_PRECLOSE");
            g_asMarketStatus[6] = Language.getString("MKT_STATUS_TRADE_AT_LAST");
        } catch (Exception e) {
            g_asMarketStatus[0] = g_sNA;
            g_asMarketStatus[1] = g_sNA;
            g_asMarketStatus[2] = g_sNA;
            g_asMarketStatus[3] = g_sNA;
            g_asMarketStatus[4] = g_sNA;
            g_asMarketStatus[5] = g_sNA;
            g_asMarketStatus[6] = g_sNA;
        }

        try {
            collapseImage = new ImageIcon("images/Theme" + Theme.getID() + "/collapse.gif");
        } catch (Exception e) {
            collapseImage = null;
        }
        try {
            expandImage = new ImageIcon("images/Theme" + Theme.getID() + "/expand.gif");
        } catch (Exception e) {
            expandImage = null;
        }
        try {
            nochildImage = new ImageIcon("images/Theme" + Theme.getID() + "/nochild.gif");
        } catch (Exception e) {
            expandImage = null;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            try {
                collapseImage = new ImageIcon("images/Theme" + Theme.getID() + "/collapse.gif");
            } catch (Exception e) {
                collapseImage = null;
            }
            try {
                expandImage = new ImageIcon("images/Theme" + Theme.getID() + "/expand.gif");
            } catch (Exception e) {
                expandImage = null;
            }
            try {
                nochildImage = new ImageIcon("images/Theme" + Theme.getID() + "/nochild.gif");
            } catch (Exception e) {
                expandImage = null;
            }
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void propertyChanged(int property) {
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);
        variationImage.setHeight(table.getRowHeight());


        try {
            isexDefault = (Boolean) table.getModel().getValueAt(row, -2);
        } catch (Exception e) {
            isexDefault = false;
        }

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE

                    doubleValue = toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
                case 5: // NetCash %
                    if (isexDefault) {
                        doubleValue = toDoubleValue(value);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                                (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
//                            lblRenderer.setText(g_sNA);
                            lblRenderer.setText("");
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue) + "%"));
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    } else {
                        lblRenderer.setText("");
                    }
                    break;
                case 6: // Chnange
                    doubleValue = toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
                case 7: // % Change
                    doubleValue = toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue) + "%"));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;

                case 8:
                    try {
                        doubleValue = toDoubleValue(value);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                                (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue)));
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                case 'h': // Cash Map
                    if (isexDefault) {
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        variationImage.setType(VariationImage.TYPE_CASH_MAP);
                        variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        lblRenderer.setHorizontalTextPosition(g_iCenterAlign);
                        lblRenderer.setForeground(Color.black);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            variationImage.setValue(0);
                            lblRenderer.setText("");
                        } else {
                            variationImage.setValue(doubleValue);
                            lblRenderer.setIcon(variationImage);
                            lblRenderer.setText(oPChangeFormat.format(doubleValue * 100));
                        }
//                        lblRenderer.setBackground(Color.BLACK);
                    } else {
                        lblRenderer.setText("");
                    }
                    /* if (isSelected) {
                        lblRenderer.setBorder(imageBorder);
                    } else {
                        lblRenderer.setBorder(null);
                    }*/
                    break;
                case 'M': // Market Status
                    try {
                        lblRenderer.setText(getMarketStatus(toIntValue(value)));
                        //lblRenderer.setText(g_asMarketStatus[toIntValue(value) - 1]);
                    } catch (Exception e) {
                        lblRenderer.setText(Language.getString(""));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 'E': // Expander Image
                    switch (toIntValue(value)) {
                        case Constants.EXPANSION_STATUS_COLLAPSED:
                            lblRenderer.setIcon(expandImage);
                            break;
                        case Constants.EXPANSION_STATUS_EXPANDED:
                            lblRenderer.setIcon(collapseImage);
                            break;
                        default:
                            lblRenderer.setIcon(null);
                            break;
                    }
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(SwingConstants.CENTER);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    private String getMarketStatus(int value) {    //added by madhawie for MORMUBMKT-1129
        switch (value) {
            case Constants.PREOPEN:
                return (Language.getString("MKT_STATUS_PRE_OPEN"));
            case Constants.OPEN:
                return (Language.getString("MKT_STATUS_OPEN"));
            case Constants.CLOSED:
                return (Language.getString("MKT_STATUS_CLOSE"));
            case Constants.PRECLOSED:
                return (Language.getString("MKT_STATUS_PRE_CLOSE"));
            case Constants.PRECLOSE_ADJUSTMENT:
                return (Language.getString("MKT_STATUS_PRE_CLOSE_ADJUSTMENT"));
            case Constants.PRECLOSE_WITH_TRADES:
                return (Language.getString("MKT_STATUS_PRE_CLOSE"));
            case Constants.CLOSE_WITH_TRADES:
                return (Language.getString("MKT_STATUS_PRE_CLOSE"));
            case Constants.TRADING_AT_LAST:
                return (Language.getString("MKT_STATUS_TRADE_AT_LAST"));
            case Constants.CLOSING_AUCTION:
                return (Language.getString("MKT_CLOSING_AUCTION"));
            case Constants.CLOSING_AUCTION_NEW:
                return (Language.getString("MKT_CLOSING_AUCTION"));
            default:
                return g_sNA;
        }
    }
}