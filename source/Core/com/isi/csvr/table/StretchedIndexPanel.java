package com.isi.csvr.table;

import com.isi.csvr.customindex.CustomIndexListener;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.updator.TableUpdateManager;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.TimeZone;

public class StretchedIndexPanel extends JPanel
        implements Themeable, MouseListener, ActionListener, ExchangeListener, ApplicationListener, CustomIndexListener, UpdateableTable {

    private static final int ROW_2_HEIGHT = 18;
    public static int adjustWidth;
    public static String symbol;
    public static String key;
    public static boolean isCustomindex = false;
    private static StretchedIndexPanel self = null;
    //-----  added by shanika----
    public JLabel lblMarketName = new JLabel();
    FontMetrics fontMetrics;
    Font boldFnt;
    Font indexIDFnt;
    private ImageIcon downImage;
    private ImageIcon upImage;
    private Color upColor;
    private Color downColor;
    private Color normalColor;
    private ImageIcon imageicon;
    private ImageIcon imageiconL;
    private ImageIcon imageiconR;
    private ImageIcon imageGraphBG;
    //    private ImageIcon imageTile;
    private int imagewidth;
    private Color indexGraphFGColor;
    private ImageIcon graphImage;
    private JLabel lblGraphImage = new JLabel("");
    private JLabel lblIndexName = new JLabel("");
    private JLabel lblIndexValue = new JLabel("");
    private JLabel lblIndexChange = new JLabel("");
    private JLabel lblIndexImage = new JLabel("");
    private JLabel lblIndexPChange = new JLabel("");
    private JLabel lblNoOfTrades = new JLabel("");
    private JLabel lblVolumn = new JLabel("");
    private JLabel lblGap1 = new JLabel();
    private JLabel lblGap2 = new JLabel();
    private JLabel lblGraphGap = new JLabel();
    private JLabel lblGap5 = new JLabel();
    private Thread thread;
    private TWDecimalFormat decimalFormat;
    private TWDecimalFormat numberFormat;
    private String prevKey;
    private String prevMarket;
    private int height = 34;
    private ImageIcon tile;
    private int imageWidth;
    private TWFont graphFont;
    private TWDecimalFormat highLowFormat;

    private StretchedIndexPanel() {
        setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
        Font fnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.BOLD, 20);
        highLowFormat = SharedMethods.getDecimalFormat(0);
        graphFont = new TWFont("arial", Font.PLAIN, 8);
        /*  Do not make the font bold in Arabic mode.
            It may cause displaying of unknown unicode symbols */
        if (Language.isLTR()) {
            boldFnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.BOLD, 20);
            indexIDFnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.BOLD, 20);
        } else {
            boldFnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.PLAIN, 20);
            indexIDFnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.PLAIN, 20);
        }
        fontMetrics = getFontMetrics(boldFnt);

        lblIndexName.setHorizontalAlignment(SwingConstants.CENTER);
        lblIndexName.setOpaque(false);
        lblIndexName.setFont(indexIDFnt);
        //lblIndexName.setBackground(stretchedIndexPanelBG);

        lblIndexValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblIndexValue.setFont(boldFnt);
        //lblIndexValue.setBackground(stretchedIndexPanelBG);
        lblIndexValue.setOpaque(false);

        lblIndexChange.setHorizontalAlignment(SwingConstants.RIGHT);
        lblIndexChange.setFont(boldFnt);
        // lblIndexChange.setBackground(stretchedIndexPanelBG);
        lblIndexChange.setOpaque(false);

        //------ Added by Shanika
        lblGraphImage.setHorizontalAlignment(SwingConstants.RIGHT);
        lblGraphImage.setOpaque(false);


        lblIndexImage.setHorizontalAlignment(SwingConstants.CENTER);
        lblIndexImage.setOpaque(false);

        lblIndexPChange.setHorizontalAlignment(SwingConstants.RIGHT);
        lblIndexPChange.setFont(boldFnt);
        lblIndexPChange.setOpaque(false);
        //lblIndexPChange.setBackground(stretchedIndexPanelBG);

        lblMarketName.setHorizontalAlignment(SwingConstants.CENTER);
        lblMarketName.setOpaque(false);
        lblMarketName.setFont(boldFnt);

        lblVolumn.setHorizontalAlignment(SwingConstants.RIGHT);
        lblVolumn.setFont(boldFnt);
        lblVolumn.setOpaque(false);

        lblNoOfTrades.setHorizontalAlignment(SwingConstants.CENTER);
        lblNoOfTrades.setFont(boldFnt);
        lblNoOfTrades.setOpaque(false);


        if (Language.isLTR()) {
            lblGap5.setPreferredSize(new Dimension(5, ROW_2_HEIGHT));
        } else {
            lblGap5.setPreferredSize(new Dimension(15, ROW_2_HEIGHT));
        }

        this.setPreferredSize(new Dimension(1020, height));
        this.add(lblMarketName);
        this.add(lblGap1);
        this.add(lblIndexName);


        this.add(lblGap2);


        this.add(lblIndexValue);
        this.add(lblIndexChange);
        this.add(lblIndexImage);
        this.add(lblIndexPChange);
        this.add(lblVolumn);
        this.add(lblNoOfTrades);
        this.add(lblGraphGap);

        numberFormat = new TWDecimalFormat("###,##0");
        decimalFormat = new TWDecimalFormat("###,##0.00");

        Theme.registerComponent(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);

        setOpaque(false);
        applyTheme();
        Application.getInstance().addApplicationListener(this);
        GUISettings.applyOrientation(this);
        CustomIndexStore.getSharedInstance().addCustomIndexListener(this);
        TableUpdateManager.addTable(this);

    }


    public static StretchedIndexPanel getInstance() {
        if (self == null) {
            self = new StretchedIndexPanel();
        }
        return self;
    }

    /*public void activate() {
        if (thread == null) {
            thread = new Thread(this, "Streatched Index Panel");
            thread.start();
        }
    }*/

    public static String getIndexID() {
        return key;
    }

    public static void setIndexID(String id) {
        key = id;
        symbol = SharedMethods.getSymbolFromKey(key);
    }

    public void setMarketName(String name) {
        lblMarketName.setText(name);
    }

    public void applyTheme() {
        upColor = Theme.getColor("STRETCHED_INDEX_UP_FGCOLOR");
        downColor = Theme.getColor("STRETCHED_INDEX_DOWN_FGCOLOR");
        normalColor = Theme.getColor("STRETCHED_INDEX_NORMAL_FGCOLOR");
        lblIndexName.setForeground(Theme.getColor("STRETCHED_INDEX_INDEX_NAME_FGCOLOR"));
        lblMarketName.setForeground(Theme.getColor("STRETCHED_INDEX_MARKET_NAME_FGCOLOR"));
        lblIndexValue.setForeground(Theme.getColor("STRETCHED_INDEX_VALUE_FGCOLOR"));
        lblVolumn.setForeground(Theme.getColor("STRETCHED_INDEX_VALUE_FGCOLOR"));
        lblNoOfTrades.setForeground(Theme.getColor("STRETCHED_INDEX_VALUE_FGCOLOR"));


        graphImage = new ImageIcon(Theme.getTheamedImagePath("indexGraphBG"));

        try {
            imageicon = null;
            //imageTile = null;
            imageGraphBG = null;
            imageicon = new ImageIcon(Theme.getTheamedImagePath("indextitletile"));
            imageiconL = new ImageIcon(Theme.getTheamedImagePath("indextitletileL"));
            imageiconR = new ImageIcon(Theme.getTheamedImagePath("indextitletileR"));
            //imageTile = new ImageIcon("images/theme" + Theme.getID()+"/indexpaneltile.png");
            imageGraphBG = new ImageIcon(Theme.getTheamedImagePath("indexGraphBG"));
            imagewidth = imageicon.getIconWidth();
//            imageTilewidth = imageTile.getIconWidth();
        } catch (Exception e) {
            imageicon = null;
            imagewidth = 0;
        }

        indexGraphFGColor = Theme.getColor("INDEXGRAPH_FG");

        try {
            upImage = new ImageIcon("images/theme" + Theme.getID() + "/bigup.gif");
        } catch (Exception e) {
            upImage = null;
        }

        try {
            downImage = new ImageIcon("images/theme" + Theme.getID() + "/bigdown.gif");
        } catch (Exception e) {
            downImage = null;
        }
    }

    public Dimension getPreferredSize() {
        return new Dimension(995, height);
    }

    public void run() {
        if (System.getProperty("DisableIndexPanel") != null) {
            return;
        }
        updateGUI();
        SwingUtilities.updateComponentTreeUI(this);
        while (true) {
            try {
                updateGUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1500);
            } catch (Exception e) {
                e.printStackTrace();
            }
            repaint();
        }

    }

    private synchronized void updateGUI() {

        try {
            if (ExchangeStore.getSelectedExchangeID() != null) {
                if (!ExchangeStore.getSharedInstance().isValidExchange(ExchangeStore.getSelectedExchangeID())) { // oops login changed. need to reset exchanges
                    ExchangeStore.setSelectedExchangeID(null);
                    prevMarket = null;
                    return;
                }
                if (prevMarket == null) { // first time. market not yet selected
                } else if (!prevMarket.equals(ExchangeStore.getSelectedExchangeID())) {
                }

                prevMarket = ExchangeStore.getSelectedExchangeID();
//                lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
                lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getDisplayExchange());
            }

            Exchange exchangeObject = ExchangeStore.getSharedInstance().getSelectedExchange();
            if (exchangeObject == null) {
                exchangeObject = ExchangeStore.getSharedInstance().getDefaultExchange();
                if (exchangeObject != null) {
                    ExchangeStore.setSelectedExchangeID(exchangeObject.getSymbol());
                    lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getDisplayExchange());
                }
            }

        } catch (Exception e) {

        }

        try {
            if (!isCustomindex) {
//                Stock index = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), Meta.INSTRUMENT_INDEX);
                Stock index = DataStore.getSharedInstance().getStockObject(ExchangeStore.getSelectedExchangeID(), symbol, Meta.INSTRUMENT_INDEX);

                if (index != null) { // may fail while loading the applicatiion
                    lblIndexValue.setText(formatDecimal(index.getLastTradeValue()));
                    lblIndexChange.setText(formatDecimal(index.getChange()));
                    setFontColor(lblIndexChange, index.getChange());
                    lblIndexPChange.setText(formatDecimal(index.getPercentChange()) + "%");
                    setFontColor(lblIndexPChange, index.getPercentChange());
                    lblIndexImage.setIcon(formatIcon(index.getPercentChange()));
                    lblGraphImage.setIcon(graphImage);
                    if ((index.getLongDescription() == null) || (index.getLongDescription().equals(Language.getString("NA")))) {
                        lblIndexName.setText(index.getSymbol());
                    } else {
//                        lblIndexName.setText(index.getLongDescription());
                        lblIndexName.setText(index.getSymbol());

                    }
//                    lblVolumn.setText(formatNumber(index.getVolume()));
//                    lblNoOfTrades.setText(formatNumber(index.getNoOfTrades()));
                } else if ((symbol != null) && (symbol.equals("*"))) {
                    setIndex("");
                    lblIndexValue.setText("");
                    lblIndexChange.setText("");
                    lblIndexPChange.setText("");
                    lblIndexImage.setIcon(null);
                } else if ((prevKey == null) || (prevKey.equals("null")) || (!prevKey.equals(key))) {              //(prevKey == null) || (!prevKey.equals(key))
                    lblIndexValue.setText("");
                    lblIndexChange.setText("");
                    lblIndexPChange.setText("");
                    lblIndexName.setText("");
                    lblIndexImage.setIcon(null);
                    String descr = DataStore.getSharedInstance().getCompanyName(key);
                    if (descr.equals("")) { // no index selected.
                    } else {
                        setIndex(descr);
                    }
                    prevKey = key;
                    descr = null;
                } else {
                    lblIndexName.setText("");
                    lblIndexValue.setText("");
                    lblIndexChange.setText("");
                    lblIndexPChange.setText("");
                    key = "";
                    lblIndexImage.setIcon(null);
                }
                index = null;
            } else {
                Stock index = DataStore.getSharedInstance().getStockofCustomIndex(symbol, Meta.INSTRUMENT_INDEX);
                if (index != null) {
                    lblIndexValue.setText(formatDecimal(index.getLastTradeValue()));
                    lblIndexChange.setText(formatDecimal(index.getChange()));
                    setFontColor(lblIndexChange, index.getChange());
                    lblIndexPChange.setText(formatDecimal(index.getPercentChange()) + "%");
                    setFontColor(lblIndexPChange, index.getPercentChange());
                    lblIndexImage.setIcon(formatIcon(index.getPercentChange()));
                    lblGraphImage.setIcon(graphImage);
                    if ((index.getLongDescription() == null) || (index.getLongDescription().equals(Language.getString("NA")))) {
                        lblIndexName.setText(index.getSymbol());
                    } else {
//                        lblIndexName.setText(index.getLongDescription());
                        lblIndexName.setText(index.getSymbol());

                    }
//                    lblVolumn.setText(formatNumber(index.getVolume()));
//                    lblNoOfTrades.setText(formatNumber(index.getNoOfTrades()));
                } else if ((prevKey == null) || (!prevKey.equals(key))) {
                    String descr = DataStore.getSharedInstance().getStockofCustomIndex(symbol, Meta.INSTRUMENT_INDEX).getLongDescription();
                    if (descr.equals("")) { // no index selected.
                    } else {
                        setIndex(descr);
                    }
                    prevKey = key;
                    descr = null;
                }
            }
        } catch (Exception e) {
        }
        repaint();

    }

    public void refresh() {
        updateGUI();
    }

    private void setFontColor(JLabel label, double value) {
        if (value > 0)
            label.setForeground(upColor);
        else if (value < 0)
            label.setForeground(downColor);
        else
            label.setForeground(normalColor);
    }

    private String formatDecimal(double value) {
        try {
            return decimalFormat.format(value);
        } catch (Exception e) {
            e.printStackTrace();
            return "0.00";
        }
    }

    private String formatNumber(long value) {
        try {
            return numberFormat.format(value);
        } catch (Exception e) {
            e.printStackTrace();
            return "00";
        }
    }

    private String formatNumber(double value) {
        try {
            return numberFormat.format(value);
        } catch (Exception e) {
            e.printStackTrace();
            return "00";
        }
    }

    private ImageIcon formatIcon(double value) {
        try {
            if (value > 0)
                return upImage;
            else if (value < 0)
                return downImage;
            else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == lblIndexName)
            lblIndexName.setBorder(BorderFactory.createEtchedBorder());
        if (e.getSource() == lblMarketName)
            lblMarketName.setBorder(BorderFactory.createEtchedBorder());
    }

    public void setNoofTrades(int NoOfTrades) {
        lblNoOfTrades.setText(formatNumber(NoOfTrades));
    }

    public void setVolumn(double volumn) {
        lblVolumn.setText(formatNumber(volumn));
    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == lblIndexName)
            lblIndexName.setBorder(BorderFactory.createLineBorder(Theme.getColor("MARKET_TITLE_BORDER_COLOR"), 1));
        if (e.getSource() == lblMarketName)
            lblMarketName.setBorder(BorderFactory.createLineBorder(Theme.getColor("MARKET_TITLE_BORDER_COLOR"), 1));
    }

    /**
     * Invoked when an action occurs.
     */
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().startsWith("I")) {
            key = e.getActionCommand().substring(1);
            symbol = SharedMethods.getSymbolFromKey(key);
            setIndex(DataStore.getSharedInstance().getCompanyName(key));

        } else {
            ExchangeStore.setSelectedExchangeID(e.getActionCommand().substring(1));
            lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getDisplayExchange());

        }
    }

    public void setIndex(String descr) {
        lblIndexName.setText(descr);
        lblIndexName.updateUI();
    }

    public void adjustLabelWiths() {
        int total_string_width = 980 - 20 - (fontMetrics.stringWidth(lblIndexName.getText()) + fontMetrics.stringWidth(lblMarketName.getText()) + fontMetrics.stringWidth(lblIndexPChange.getText()) + fontMetrics.stringWidth(lblIndexChange.getText()) +
                fontMetrics.stringWidth(lblIndexValue.getText()) + fontMetrics.stringWidth(lblVolumn.getText()) + fontMetrics.stringWidth(lblNoOfTrades.getText()));
        if (total_string_width >= 0) {
//            int adjust = total_string_width / 7;
            int adjust = total_string_width / 12;

            lblIndexPChange.setPreferredSize(new Dimension(fontMetrics.stringWidth(lblIndexPChange.getText()) + adjust, height));
            lblIndexImage.setPreferredSize(new Dimension(20, height));

            lblGraphImage.setPreferredSize(new Dimension(10, height));
            lblGraphGap.setPreferredSize(new Dimension(270, height));
            lblIndexChange.setPreferredSize(new Dimension(fontMetrics.stringWidth(lblIndexChange.getText()) + adjust, height));
            lblIndexName.setPreferredSize(new Dimension(fontMetrics.stringWidth(lblIndexName.getText()) + adjust, height));

//            lblMarketName.setPreferredSize(new Dimension(fontMetrics.stringWidth(lblMarketName.getText())+adjust, height));
            lblMarketName.setPreferredSize(new Dimension(fontMetrics.stringWidth(lblMarketName.getText()), height));


            lblIndexValue.setPreferredSize(new Dimension(fontMetrics.stringWidth(lblIndexValue.getText()) + adjust, height));
            lblVolumn.setPreferredSize(new Dimension(fontMetrics.stringWidth(lblVolumn.getText()) + adjust, height));
            lblNoOfTrades.setPreferredSize(new Dimension(fontMetrics.stringWidth(lblNoOfTrades.getText()) + adjust, height));

            adjustWidth = fontMetrics.stringWidth(lblIndexPChange.getText()) + adjust +
                    fontMetrics.stringWidth(lblIndexChange.getText()) + adjust +
                    fontMetrics.stringWidth(lblIndexName.getText()) + adjust +
                    fontMetrics.stringWidth(lblMarketName.getText()) +
                    fontMetrics.stringWidth(lblIndexValue.getText()) + adjust +
                    fontMetrics.stringWidth(lblVolumn.getText()) + adjust +
                    fontMetrics.stringWidth(lblNoOfTrades.getText()) + adjust;
//            adjustWidth = fontMetrics.stringWidth(lblIndexName.getText())+adjust+fontMetrics.stringWidth(lblMarketName.getText());
//            adjustWidth = total_string_width+(7*adjust);
//            System.out.println("=========================adjustWidth"+adjustWidth);
        }
    }


    public void paint(Graphics g) {

        super.paint(g);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        try {
//                g.setColor(indexGraphBGColor);
            if (Language.isLTR()) {
//                    g.translate(458, 0);

                g.translate(adjustWidth + 5, 0);
//                    g.translate(150, 0);

            } else {
//                    g.translate(8, 0);
//                    g.translate(0, 0);
                g.translate(-20, 0);

            }

            if (Language.isLTR()) {
//                    g.fillRect(0, 0, 290, 100);
                g.drawImage(imageGraphBG.getImage(), 0, 0, this);
            } else {
//                    g.fillRect(0, 0, 240, 100);
                g.drawImage(imageGraphBG.getImage(), 0, 0, this);
            }
            Graphics2D g2D = (Graphics2D) g;
            g2D.scale(240d / 240d, 1);
            g.setColor(indexGraphFGColor);
            int h = 31;
            int w = 240;
            double[] arr;
            if (key.equals("")) {
                return;
            }
            DynamicArray ohlc = OHLCStore.getInstance().getOHLCForIndex(key);

            if (ohlc.size() <= 240) {
                arr = new double[ohlc.size() + 2];
                arr[0] = 1000000;
                for (int j = 0; j < ohlc.size(); j++) {
                    IntraDayOHLC intradayohlc = (IntraDayOHLC) ohlc.get(j);
                    arr[j + 2] = intradayohlc.getClose();
                    arr[0] = Math.min(arr[0], intradayohlc.getClose());
                    arr[1] = Math.max(arr[1], intradayohlc.getClose());

                }
            } else {
                int constant = ohlc.size() - 240;
                arr = new double[242];
                arr[0] = 1000000;
                for (int j = ohlc.size() - 240; j < ohlc.size(); j++) {
                    IntraDayOHLC intradayohlc = (IntraDayOHLC) ohlc.get(j);
                    arr[j + 2 - constant] = intradayohlc.getClose();
                    arr[0] = Math.min(arr[0], intradayohlc.getClose());
                    arr[1] = Math.max(arr[1], intradayohlc.getClose());

                }
            }


            try {
                g.setFont(graphFont);
                if (arr[0] != 1000000) {
                    if (Language.isLTR()) {
                        g.drawString(highLowFormat.format(arr[1]) + "", 2, 8);
                        g.drawString(highLowFormat.format(arr[0]) + "", 2, 29);
                    } else {
                        g.drawString(highLowFormat.format(arr[1]) + "", 24, 8);
                        g.drawString(highLowFormat.format(arr[0]) + "", 24, 29);
                    }
                }
                if ((arr != null) && (arr.length > 0) && (arr[1] != Double.MIN_VALUE)) {
                    g.setColor(indexGraphFGColor);
                    double[] rangearray = new double[arr.length];
                    for (int i = 0; i < arr.length; i++) {
                        rangearray[i] = (arr[i] - arr[0]);
                    }
                    double yratio = ((h - 2) / rangearray[1]);
                    double xgap = (w) / (double) w;
                    int[] xcordinates = new int[arr.length];
                    for (int i = 0; i < arr.length; i++) {
                        if (Language.isLTR()) {
                            xcordinates[i] = i + 25;
                        } else {
                            xcordinates[i] = i + 45;
                        }
                    }

                    for (int i = 0; i < arr.length - 3; i++) {
                        if (rangearray[i + 3] >= 0) {
                            g.drawLine(xcordinates[i], (int) ((h) - (rangearray[i + 2]) * yratio) + 1, xcordinates[i + 1], (int) ((h) - (rangearray[i + 3]) * yratio) + 1);
                        } else {
                            break;
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//                g.setColor(indexGraphFG_Grid_Color);
////                Graphics2D g2D = (Graphics2D) g;
////                g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
//                g.drawRect(0, 0, 240, 33);
//                for (int i = 0; i < 25; i++) {
//                    g.drawLine((i * 10), 0, (i * 10), 33);
//                }
//                for (int i = 0; i < 4; i++) {
//                    g.drawLine(0, (i * 11), 240, (i * 11));
//                }
            ohlc = null;

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        }else{
//            super.paint(g);
//        }
    }


    /* Workspace Listener */
    public void applicationExiting() {

    }

    public void applicationLoaded() {

    }

    public void applicationLoading(int percentage) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    public void workspaceLoaded() {
//        activate();
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void customIndexAdded(String index) {

    }

    public void customIndexRemoved(String index) {

    }

    public void customIndexEditted(String index) {

    }

    public void runThread() {
        //To change body of implemented methods use File | Settings | File Templates.
        if (System.getProperty("DisableIndexPanel") != null) {
            return;
        }
        updateGUI();
        SwingUtilities.updateComponentTreeUI(this);

        try {
            updateGUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        repaint();


    }

    public String getTitle() {
        return "stretched index panel updator";  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Constants.ThreadTypes getTableID() {
        return Constants.ThreadTypes.INDEX_PANEL;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
