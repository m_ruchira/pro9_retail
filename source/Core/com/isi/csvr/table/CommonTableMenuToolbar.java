package com.isi.csvr.table;

import com.isi.csvr.*;
import com.isi.csvr.customizer.CommonTableCustomizerDialog;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.mist.MISTTableCustomizerDialog;
import com.isi.csvr.mist.MISTTableSettings;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.plaf.metal.MetalComboBoxButton;
import javax.swing.plaf.metal.MetalScrollButton;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: udayaa
 * Date: May 21, 2008
 * Time: 11:05:56 AM
 */


public class CommonTableMenuToolbar extends Window implements AWTEventListener, MouseListener, ItemListener, ActionListener {
    private static CommonTableMenuToolbar popWindow;
    String[] sizeList = new String[]
            {"2", "4", "6", "8", "10", "12", "14", "16", "18", "20", "22", "24", "30", "36", "48", "72"};
    private CommonPopup parentpopup;
    private int xp = 0;
    private int yp = 0;
    private int width = 218;
    private int height = 44;
    private JPanel wPannel;
    private JPanel topPannel;
    private JPanel btmPannel;
    private Font oldFont;
    private Font newFont;
    private boolean copyWithheader = false;
    private Table g_oTable;
    private CommonTableSettings target;
    private MISTTableSettings misttarget;
    private boolean mistViewSelected = false;
    private JPopupMenu mnuCopy;
    private JPopupMenu mnuLinkExcel;
    private TWEXComboBox fontSelect;
    private ComboRenderer fontRenderer;
    private TWComboBox textSizeSelect;
    private Color bgColor;
    private Color fgColor;
    private Color comboBorderColor;
    private Color borderColor;
    private int borderThickness;
    private ToolBarButton tbtnResetToDefault;
    private ToolBarButton tbtnResize;
    private ToolBarButton tbtnCopy;
    private ToolBarButton tbtnLinkExcell;
    private ToolBarButton tbtnSetAsDefault;
    private ToolBarButton tbtnColSettings;
    private ToolBarButton tbtnCellselection;
    private ToolBarButton tbtnCustomize;
    private ToolBarButton tbtnPrint;
    private ToolBarButton tbtnBoald;
    private ToolBarButton tbtnItalic;
    private ToolBarButton tbtnTxtSzUp;
    private ToolBarButton tbtnTxtSzDn;
    private ToolBarButton tbtnhideHeader;
    private JPopupMenu mnuSellSelect;

    private CommonTableMenuToolbar(Frame owner) {
        super(owner);
        bgColor = Theme.getColor("MENU_TOOLBAR_BGCOLOR");
        fgColor = Theme.getColor("MENU_TOOLBAR_FGCOLOR");
        comboBorderColor = Theme.getColor("MENU_TOOLBAR_COMBO_BORDER_COLOR");
        borderColor = Theme.getColor("MENU_TOOLBAR_BORDER_COLOR");
        borderThickness = Theme.getInt("MENU_TOOLBAR_BORDER_THICKNESS", 1);
        initPopupWindowUI();
    }

    public static synchronized CommonTableMenuToolbar getSharedInstance() {
        if (popWindow == null) {
            popWindow = new CommonTableMenuToolbar(Client.getInstance().getFrame());
        }
        return popWindow;
    }

    public void setParentpopup(CommonPopup parentpopup) {
        this.parentpopup = parentpopup;
    }

    public void initPopupWindowUI() {
        wPannel = new JPanel(new BorderLayout());
        wPannel.setBackground(bgColor);

        topPannel = new JPanel(new FlowLayout(FlowLayout.LEFT, 1, 0));
        topPannel.setPreferredSize(new Dimension(150, 20));
        topPannel.setOpaque(false);
        fillTopPanel();

        btmPannel = new JPanel(new FlowLayout(FlowLayout.LEFT, 1, 0));
        btmPannel.setPreferredSize(new Dimension(width, 20));
        btmPannel.setOpaque(false);
        fillBtmPanel();

        wPannel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(borderColor, borderThickness), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
        wPannel.add(topPannel, BorderLayout.NORTH);
        wPannel.add(btmPannel, BorderLayout.SOUTH);
        add(wPannel);
        width = Constants.COMMON_TABLE_MENU_TOOLBAR_WIDTH;
    }

    public void fillTopPanel() {
        fontRenderer = new ComboRenderer();
        Integer p[] = {1, 2, 3, 4,};
        fontSelect = new TWEXComboBox(getFontList());
        fontSelect.setExtendedMode(false);
        fontSelect.setPreferredSize(new Dimension(80, 19));
        fontSelect.setExtendedMode(true);
        fontSelect.setToolTipText(Language.getString("FONT"));

        fontSelect.addItemListener(this);
        fontSelect.setMaximumRowCount(15);
        fontSelect.setRenderer(fontRenderer);
        fontSelect.setFocusable(false);
        fontSelect.setBackground(bgColor);
        fontSelect.setForeground(fgColor);
        fontSelect.setArrowButtonIconColor(fontSelect.getForeground());
        fontSelect.setBorder(BorderFactory.createLineBorder(comboBorderColor));
        GUISettings.clearBorders(fontSelect);
        SwingUtilities.updateComponentTreeUI(fontSelect);

        textSizeSelect = new TWComboBox(sizeList);
        textSizeSelect.setPreferredSize(new Dimension(44, 19));
        textSizeSelect.setToolTipText(Language.getString("SIZE"));
        textSizeSelect.addItemListener(this);
        textSizeSelect.setMaximumRowCount(16);
        textSizeSelect.setFocusable(false);
        textSizeSelect.setBackground(bgColor);
        textSizeSelect.setForeground(fgColor);
        textSizeSelect.setArrowButtonIconColor(textSizeSelect.getForeground());
        textSizeSelect.setBorder(BorderFactory.createLineBorder(comboBorderColor));
        GUISettings.clearBorders(textSizeSelect);

        tbtnTxtSzUp = new ToolBarButton();
        tbtnTxtSzUp.setPreferredSize(new Dimension(20, 19));
        tbtnTxtSzUp.setToolTipText(Language.getString("INCREASE_FONT"));
        tbtnTxtSzUp.addMouseListener(this);
        tbtnTxtSzUp.setIcon(getIconFromString("in-font"));
        tbtnTxtSzUp.setRollOverIcon(getIconFromString("in-font-mouseover"));

        tbtnTxtSzDn = new ToolBarButton();
        tbtnTxtSzDn.setPreferredSize(new Dimension(20, 19));
        tbtnTxtSzDn.setToolTipText(Language.getString("DECREASE_FONT"));
        tbtnTxtSzDn.addMouseListener(this);
        tbtnTxtSzDn.setIcon(getIconFromString("de-font"));
        tbtnTxtSzDn.setRollOverIcon(getIconFromString("de-font-mouseover"));

        tbtnBoald = new ToolBarButton();
        tbtnBoald.setPreferredSize(new Dimension(20, 19));
        tbtnBoald.setToolTipText(Language.getString("BOLD"));
        tbtnBoald.setIcon(getIconFromString("bold"));
        tbtnBoald.setRollOverIcon(getIconFromString("bold-mouseover"));
        tbtnBoald.setToggledIcon(getIconFromString("bold-mouseover"));
        tbtnBoald.setToggleEnabled(true);
        tbtnBoald.addMouseListener(this);

        tbtnItalic = new ToolBarButton();
        tbtnItalic.setPreferredSize(new Dimension(20, 19));
        tbtnItalic.setToolTipText(Language.getString("ITALIC"));
        tbtnItalic.addMouseListener(this);
        tbtnItalic.setIcon(getIconFromString("italic"));
        tbtnItalic.setRollOverIcon(getIconFromString("italic-mouseover"));
        tbtnItalic.setToggledIcon(getIconFromString("italic-mouseover"));
        tbtnItalic.setToggleEnabled(true);
        if (!Language.isLTR()) {
            tbtnItalic.setEnabled(false);
            tbtnItalic.removeMouseListener(this);
        }

        topPannel.add(fontSelect);
        topPannel.add(textSizeSelect);
        topPannel.add(tbtnTxtSzUp);
        topPannel.add(tbtnTxtSzDn);
        topPannel.add(tbtnBoald);
        topPannel.add(tbtnItalic);
    }

    public void fillBtmPanel() {
        tbtnCopy = new ToolBarButton();
        tbtnCopy.setPreferredSize(new Dimension(20, 19));
        tbtnCopy.setToolTipText(Language.getString("COPY"));
        tbtnCopy.addMouseListener(this);
        tbtnCopy.setIcon(getIconFromString("copy"));
        tbtnCopy.setRollOverIcon(getIconFromString("copy-mouseover"));

        tbtnLinkExcell = new ToolBarButton();
        tbtnLinkExcell.setPreferredSize(new Dimension(20, 19));
        tbtnLinkExcell.setToolTipText(Language.getString("LINK_TO_XL"));
        tbtnLinkExcell.addMouseListener(this);
        tbtnLinkExcell.setIcon(getIconFromString("link-excel"));
        tbtnLinkExcell.setRollOverIcon(getIconFromString("link-excel-mouseover"));

        tbtnSetAsDefault = new ToolBarButton();
        tbtnSetAsDefault.setPreferredSize(new Dimension(20, 19));
        tbtnSetAsDefault.setToolTipText(Language.getString("SET_AS_DEFAULT"));
        tbtnSetAsDefault.setEnabled(false);
        tbtnSetAsDefault.setIcon(getIconFromString("set-as-default"));
        tbtnSetAsDefault.setRollOverIcon(getIconFromString("set-as-default-mouseover"));

        tbtnResetToDefault = new ToolBarButton();
        tbtnResetToDefault.setPreferredSize(new Dimension(20, 19));
        tbtnResetToDefault.setToolTipText(Language.getString("RESET_TO_DEFAULT"));
        tbtnResetToDefault.setEnabled(false);
        tbtnResetToDefault.setIcon(getIconFromString("reset-default"));
        tbtnResetToDefault.setRollOverIcon(getIconFromString("reset-default-mouseover"));

        tbtnPrint = new ToolBarButton();
        tbtnPrint.setPreferredSize(new Dimension(20, 19));
        tbtnPrint.setToolTipText(Language.getString("PRINT"));
        tbtnPrint.addMouseListener(this);
        tbtnPrint.setIcon(getIconFromString("print"));
        tbtnPrint.setRollOverIcon(getIconFromString("print-mouseover"));

        tbtnCustomize = new ToolBarButton();
        tbtnCustomize.setPreferredSize(new Dimension(20, 19));
        tbtnCustomize.setToolTipText(Language.getString("CUSTOMIZE"));
        tbtnCustomize.addMouseListener(this);
        tbtnCustomize.setIcon(getIconFromString("customize"));
        tbtnCustomize.setRollOverIcon(getIconFromString("customize-mouseover"));

        tbtnColSettings = new ToolBarButton();
        tbtnColSettings.setPreferredSize(new Dimension(20, 19));
        tbtnColSettings.setToolTipText(Language.getString("COLUMN_SETTINGS"));
        tbtnColSettings.addMouseListener(this);
        tbtnColSettings.setIcon(getIconFromString("column"));
        tbtnColSettings.setRollOverIcon(getIconFromString("column-mouseover"));

        tbtnCellselection = new ToolBarButton();
        tbtnCellselection.setPreferredSize(new Dimension(20, 19));
        tbtnCellselection.setToolTipText(Language.getString("SELECTION_MODE"));
        tbtnCellselection.addMouseListener(this);
        tbtnCellselection.setIcon(getIconFromString("cell-selection"));
        tbtnCellselection.setRollOverIcon(getIconFromString("cell-selection-mouseover"));

        tbtnResize = new ToolBarButton();
        tbtnResize.setPreferredSize(new Dimension(20, 19));
        tbtnResize.setToolTipText(Language.getString("ADJUST_WIDTHS"));
        tbtnResize.addMouseListener(this);
        tbtnResize.setIcon(getIconFromString("resize"));
        tbtnResize.setRollOverIcon(getIconFromString("resize-mouseover"));


        tbtnhideHeader = new ToolBarButton();
        tbtnhideHeader.setPreferredSize(new Dimension(20, 19));
        tbtnhideHeader.setToolTipText(Language.getString("SHOWHIDE_TITLEBAR"));
        tbtnhideHeader.setIcon(getIconFromString("hide-title"));
        tbtnhideHeader.setRollOverIcon(getIconFromString("hide-title-mouseover"));
        tbtnhideHeader.setToggledIcon(getIconFromString("hide-title-mouseover"));
        tbtnhideHeader.setToggleEnabled(true);
        tbtnhideHeader.addMouseListener(this);

        btmPannel.add(tbtnCopy);
        btmPannel.add(tbtnLinkExcell);
        btmPannel.add(tbtnResize);
        btmPannel.add(tbtnColSettings);
        btmPannel.add(tbtnCustomize);
        btmPannel.add(tbtnSetAsDefault);
        btmPannel.add(tbtnResetToDefault);
        btmPannel.add(tbtnPrint);

        btmPannel.add(tbtnCellselection);
        btmPannel.add(tbtnhideHeader);

        GUISettings.applyOrientation(this);
    }

    public Vector getFontList() {
        String[] fontList = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        Vector comboList = new Vector();
        String test = Language.getString("LANG");
        for (int i = 0; i < fontList.length; i++) {
            Font fnt = new Font(fontList[i], Font.PLAIN, 12);
            if (fnt.canDisplayUpTo(test) == -1) {
                comboList.add(fontList[i]);
            }
        }
        return comboList;
    }

    public void setG_oTable(Table g_oTable) {
        this.g_oTable = g_oTable;
        SmartTable t = g_oTable.getSmartTable();

        if (g_oTable.getModel().getViewSettings().getMainType() == ViewSettingsManager.MIST_VIEW) {
            misttarget = (MISTTableSettings) t.getTableSettings();
            mistViewSelected = true;
            oldFont = misttarget.getBodyFont();
        } else {
            target = (CommonTableSettings) t.getTableSettings();
            mistViewSelected = false;
        }

        try {
            oldFont = ((CommonTable) t.getModel()).getViewSettings().getFont();
        } catch (Exception ex) {
            try {
                oldFont = g_oTable.getModel().getViewSettings().getFont();
            } catch (Exception ex1) {
            }
        }
        if (oldFont == null) {
            oldFont = target.getBodyFont();
        }

        if (mistViewSelected) {
            misttarget.setBodyFont(oldFont);
        } else {
            target.setBodyFont(oldFont);
        }

    }

    public void setComboBoxButtonIconColor(Color clr) {
        fontSelect.setArrowButtonIconColor(clr);
        textSizeSelect.setArrowButtonIconColor(clr);
    }

    public void eventDispatched(AWTEvent event) {
        Object obj = event.getSource();
        if (this.isVisible()) {

            if (event.getID() == 502 && (obj instanceof JMenuItem)) {       // to hide window when main popup exits
                this.setVisible(false);
                removeListeners();
            } else if (event.getID() == 500 && !(obj instanceof JComboBox || obj instanceof TWButton || obj == this || obj instanceof MetalComboBoxButton || obj instanceof ToolBarButton || obj instanceof MetalScrollButton)) {     // mouse pressed not inside the window

                this.setVisible(false);
                removeListeners();

            } else if (event.getID() == 501 && ((obj instanceof JButton || obj instanceof TWInternalFrameTitlePane) && !(obj instanceof MetalComboBoxButton || obj instanceof TWButton || obj instanceof MetalScrollButton))) {        // internal frame minimized,maximized
                this.setVisible(false);
                removeListeners();
            } else if (event.getID() == 206 && obj instanceof TWFrame) {                  // main window minimized
                this.setVisible(false);
                removeListeners();
            } else if (event.getID() == 100 && (obj.equals(Client.getInstance().getFrame()) || obj instanceof InternalFrame)) {
                System.out.println("Close by 5");
                this.setVisible(false);
                removeListeners();
            }
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(tbtnCopy)) {

            TWMenuItem men1 = parentpopup.getCopyMenu();
            TWMenuItem men2 = parentpopup.getCopyWithHeadMenu();
            men1.updateUI();
            men2.updateUI();
            mnuCopy = new JPopupMenu();
            mnuCopy.add(men1);
            if (copyWithheader) {
                mnuCopy.add(men2);
            }

            GUISettings.applyOrientation(mnuCopy);
            mnuCopy.show(tbtnCopy, 0, 22);
        } else if (e.getSource().equals(tbtnLinkExcell)) {
            TWMenuItem men1 = parentpopup.getLinkMenu();
            TWMenuItem men2 = parentpopup.getLinkMenuWithHeaders();
            men1.setVisible(true);
            men2.setVisible(true);
            men1.setEnabled(true);
            men2.setEnabled(true);
            men1.updateUI();
            men2.updateUI();
            mnuLinkExcel = new JPopupMenu();
            mnuLinkExcel.add(men1);
            mnuLinkExcel.add(men2);
            GUISettings.applyOrientation(mnuLinkExcel);
            mnuLinkExcel.show(tbtnLinkExcell, 0, 22);

        } else if (e.getSource().equals(tbtnCustomize)) {
            if (g_oTable.getModel().getViewSettings().getMainType() == ViewSettingsManager.MIST_VIEW) {
                MISTTableCustomizerDialog.getSharedInstance().show(g_oTable);
            } else {
                CommonTableCustomizerDialog.getSharedInstance(g_oTable).show(g_oTable);
            }
        } else if (e.getSource().equals(tbtnColSettings)) {
            g_oTable.getModel().saveColumnPositions();
            (new Customizer(g_oTable)).showDialog();
        } else if (e.getSource().equals(tbtnResize)) {
            ((SmartTable) g_oTable.getTable()).adjustColumnWidthsToFit(40, 0, g_oTable.getTable().getColumnCount(), 100);
            this.setVisible(false);
        } else if (e.getSource().equals(tbtnSetAsDefault)) {
            parentpopup.setAsDefaultClicked();
            this.setVisible(false);

        } else if (e.getSource().equals(tbtnResetToDefault)) {
            parentpopup.resetToDefaultClicked();
            this.setVisible(false);
        } else if (e.getSource().equals(tbtnPrint)) {
            parentpopup.printMenuClicked();
        } else if (e.getSource().equals(tbtnCellselection)) {

            if (mnuSellSelect == null) {
                TWMenu men = createSelectionPopup();//Client.getInstance().getCellSelectionPopup();
                mnuSellSelect = new JPopupMenu();
                //    JPopupMenu mnuSellSelect = createSelectionPopup();
                int c = men.getItemCount();
                for (int i = 0; i < c; i++) {
                    TWMenuItem itm = (TWMenuItem) men.getItem(0);
                    itm.updateUI();
                    mnuSellSelect.add(itm);
                }
            }
            GUISettings.applyOrientation(mnuSellSelect);
            mnuSellSelect.show(tbtnCellselection, 0, 22);
        } else if (e.getSource().equals(tbtnTxtSzUp)) {
            if (mistViewSelected) {
                int size1 = misttarget.getBodyFont().getSize();
                int index = textSizeSelect.getSelectedIndex();

                if (size1 < 72) {
                    int nwsize = Integer.parseInt((textSizeSelect.getItemAt(index + 1)).toString());
                    System.out.println("{} " + nwsize);
                    newFont = new Font(oldFont.getFamily(), oldFont.getStyle(), nwsize);
                    misttarget.setBodyFont(newFont);
                    g_oTable.getSmartTable().setBodyFont();
                    g_oTable.getModel().getViewSettings().setFont(newFont);
                    oldFont = newFont;
                    setSelectedIndexes();
                    g_oTable.getSmartTable().updateUI();
                }

            } else {
                int size1 = target.getBodyFont().getSize();
                int index = textSizeSelect.getSelectedIndex();

                if (size1 < 72) {
                    int nwsize = Integer.parseInt((textSizeSelect.getItemAt(index + 1)).toString());
                    System.out.println("{} " + nwsize);
                    newFont = new Font(oldFont.getFamily(), oldFont.getStyle(), nwsize);
                    SharedMethods.changeTableFont(oldFont, newFont, g_oTable, g_oTable.getSmartTable(), target);
                    oldFont = newFont;
                    setSelectedIndexes();
                    final WorkInProgressIndicator btnInc = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_SETTINGS);
                    btnInc.setVisible(true);
                    Thread threadup = new Thread("CommonTable_popup_btn_increase_ActionPerformed") {
                        public void run() {
                            ((SmartTable) g_oTable.getTable()).adjustColumnWidthsToFit(40, 0, g_oTable.getTable().getColumnCount(), 10);
                            btnInc.dispose();
                        }
                    };
                    threadup.start();
                }
            }

        } else if (e.getSource().equals(tbtnTxtSzDn)) {

            if (mistViewSelected) {
                int size1 = misttarget.getBodyFont().getSize();
                int index = textSizeSelect.getSelectedIndex();
                if (size1 > 2) {
                    int nwsize = Integer.parseInt((textSizeSelect.getItemAt(index - 1)).toString());
                    newFont = new Font(oldFont.getFamily(), oldFont.getStyle(), nwsize);
                    misttarget.setBodyFont(newFont);
                    g_oTable.getSmartTable().setBodyFont();
                    g_oTable.getModel().getViewSettings().setFont(newFont);
                    oldFont = newFont;
                    setSelectedIndexes();
                    g_oTable.getSmartTable().updateUI();
                }
            } else {

                int size1 = target.getBodyFont().getSize();
                int index = textSizeSelect.getSelectedIndex();

                if (size1 > 2) {
                    int nwsize = Integer.parseInt((textSizeSelect.getItemAt(index - 1)).toString());
                    newFont = new Font(oldFont.getFamily(), oldFont.getStyle(), nwsize);
                    SharedMethods.changeTableFont(oldFont, newFont, g_oTable, g_oTable.getSmartTable(), target);
                    oldFont = newFont;
                    setSelectedIndexes();
                    final WorkInProgressIndicator btnDec = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_SETTINGS);
                    btnDec.setVisible(true);
                    Thread threaddown = new Thread("CommonTable_popup_btn_decrease_ActionPerformed") {
                        public void run() {
                            ((SmartTable) g_oTable.getTable()).adjustColumnWidthsToFit(40, 0, g_oTable.getTable().getColumnCount(), 100);
                            btnDec.dispose();
                        }
                    };
                    threaddown.start();
                }
            }

        } else if (e.getSource().equals(tbtnBoald)) {
            if (mistViewSelected) {
                oldFont = misttarget.getBodyFont();
            } else {
                oldFont = target.getBodyFont();
            }
            if (oldFont.isBold() && oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.ITALIC, oldFont.getSize());
            } else if (oldFont.isBold() && !oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.PLAIN, oldFont.getSize());
            } else if (!oldFont.isBold() && oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.BOLD + Font.ITALIC, oldFont.getSize());
            } else if (!oldFont.isBold() && !oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.BOLD, oldFont.getSize());
            }
            if (mistViewSelected) {
                misttarget.setBodyFont(newFont);
                g_oTable.getSmartTable().setBodyFont();
                g_oTable.getModel().getViewSettings().setFont(newFont);
                g_oTable.getSmartTable().updateUI();

            } else {
                SharedMethods.changeTableFont(oldFont, newFont, g_oTable, g_oTable.getSmartTable(), target);
            }
            oldFont = newFont;
            g_oTable.updateGUI();

        } else if (e.getSource().equals(tbtnItalic)) {

            if (mistViewSelected) {
                oldFont = misttarget.getBodyFont();
            } else {
                oldFont = target.getBodyFont();
            }
            if (oldFont.isBold() && oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.BOLD, oldFont.getSize());
            } else if (oldFont.isBold() && !oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.BOLD + Font.ITALIC, oldFont.getSize());
            } else if (!oldFont.isBold() && oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.PLAIN, oldFont.getSize());
            } else if (!oldFont.isBold() && !oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.ITALIC, oldFont.getSize());
            }
            if (mistViewSelected) {
                misttarget.setBodyFont(newFont);
                g_oTable.getSmartTable().setBodyFont();
                g_oTable.getModel().getViewSettings().setFont(newFont);
                g_oTable.getSmartTable().updateUI();

            } else {
                SharedMethods.changeTableFont(oldFont, newFont, g_oTable, g_oTable.getSmartTable(), target);
            }
            oldFont = newFont;
            g_oTable.updateGUI();
        } else if (e.getSource().equals(tbtnhideHeader)) {
            parentpopup.hideShowTitleBar();
            this.setVisible(false);
        }
    }


    public void setSelectedIndexes() {
        Font fnt;
        if (mistViewSelected) {
            fnt = misttarget.getBodyFont();
        } else {
            fnt = target.getBodyFont();
        }
        String family = fnt.getFamily();
        int sz = fnt.getSize();

        SmartTable smartTable = g_oTable.getSmartTable();

        try {
            fnt = ((CommonTable) smartTable.getModel()).getViewSettings().getFont();
        } catch (Exception ex) {
            try {
                fnt = g_oTable.getModel().getViewSettings().getFont();
            } catch (Exception ex1) {
            }
        }

        if (fnt != null) {

            family = fnt.getFamily();
            sz = fnt.getSize();
            if (fnt.isBold()) {
                tbtnBoald.setSwitchOn(true);
            } else {
                tbtnBoald.setSwitchOn(false);
            }
            if (Language.isLTR()) {
                if (fnt.isItalic()) {
                    tbtnItalic.setSwitchOn(true);

                } else {
                    tbtnItalic.setSwitchOn(false);
                }
            }
        }

        textSizeSelect.setSelectedItem("" + sz);
        fontSelect.setSelectedItem(family);

        try {
            WindowWrapper frame = (WindowWrapper) g_oTable.getModel().getViewSettings().getParent();
            if (!frame.isTitleVisible()) {
                tbtnhideHeader.setSwitchOn(true);
                tbtnhideHeader.setToolTipText(Language.getString("SHOW_TITLEBAR"));
            } else {
                tbtnhideHeader.setSwitchOn(false);
                tbtnhideHeader.setToolTipText(Language.getString("HIDE_TITLEBAR"));
            }

            frame = null;
        } catch (Exception e) {
        }

    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == textSizeSelect) {
            if (e.getStateChange() == e.SELECTED) {
                int size = Integer.parseInt(textSizeSelect.getSelectedItem().toString());
                if (size != oldFont.getSize()) {
                    newFont = new Font(oldFont.getName(), oldFont.getStyle(), size);
                    if (mistViewSelected) {
                        misttarget.setBodyFont(newFont);
                        g_oTable.getSmartTable().setBodyFont();
                        g_oTable.getModel().getViewSettings().setFont(newFont);
                        g_oTable.getSmartTable().updateUI();
                    } else {
                        SharedMethods.changeTableFont(oldFont, newFont, g_oTable, g_oTable.getSmartTable(), target);
                        g_oTable.getModel().getViewSettings().setFont(newFont);
                    }
                    oldFont = newFont;
                }
            }
        } else if (e.getSource() == fontSelect) {

            if (e.getStateChange() == e.SELECTED) {
                String type = fontSelect.getSelectedItem().toString();
                if (!oldFont.getFamily().equals(type)) {
                    newFont = new Font(type, oldFont.getStyle(), oldFont.getSize());
                    if (!(newFont.canDisplayUpTo("TEST") == -1)) {
                        JOptionPane.showMessageDialog(this, Language.getString("FONT_WARNING_MSG"), Language.getString("WARNING"), JOptionPane.OK_OPTION);
                    }
                    if (mistViewSelected) {
                        misttarget.setBodyFont(newFont);
                        g_oTable.getSmartTable().setBodyFont();
                        g_oTable.getModel().getViewSettings().setFont(newFont);
                        g_oTable.getSmartTable().updateUI();
                    } else {
                        SharedMethods.changeTableFont(oldFont, newFont, g_oTable, g_oTable.getSmartTable(), target);
                        g_oTable.getModel().getViewSettings().setFont(newFont);
                    }
                    oldFont = newFont;
                }
            }
        }
    }

    public void disableLinkToExcell() {
        tbtnLinkExcell.removeMouseListener(this);
        tbtnLinkExcell.setEnabled(false);
        tbtnLinkExcell.setVisible(false);
    }

    public void enableLinkToExcell() {
        tbtnLinkExcell.setEnabled(true);
        tbtnLinkExcell.setVisible(true);
        tbtnLinkExcell.removeMouseListener(this);
        tbtnLinkExcell.addMouseListener(this);
    }

    public void disableSetAsDefault() {
        tbtnSetAsDefault.setEnabled(false);
        tbtnSetAsDefault.setVisible(false);
        tbtnSetAsDefault.removeMouseListener(this);
        tbtnResetToDefault.setEnabled(false);
        tbtnResetToDefault.setVisible(false);
        tbtnResetToDefault.removeMouseListener(this);
    }

    public void enableSetAsDefault() {
        tbtnSetAsDefault.removeMouseListener(this);

        tbtnSetAsDefault.addMouseListener(this);
        tbtnSetAsDefault.setEnabled(true);
        tbtnSetAsDefault.setVisible(true);

        tbtnResetToDefault.removeMouseListener(this);
        tbtnResetToDefault.addMouseListener(this);
        tbtnResetToDefault.setEnabled(true);
        tbtnResetToDefault.setVisible(true);
    }

    public void enablePrint() {
        tbtnPrint.removeMouseListener(this);
        tbtnPrint.addMouseListener(this);
        tbtnPrint.setEnabled(true);
        tbtnPrint.setVisible(true);
    }

    public void disablePrint() {
        tbtnPrint.removeMouseListener(this);
        tbtnPrint.setEnabled(false);
        tbtnPrint.setVisible(false);
    }

    public void disableColSettings() {
        tbtnColSettings.removeMouseListener(this);
        tbtnColSettings.setEnabled(false);
        tbtnColSettings.setVisible(false);
    }

    public void enableColSettings() {

        tbtnColSettings.removeMouseListener(this);
        tbtnColSettings.addMouseListener(this);
        tbtnColSettings.setEnabled(true);
        tbtnColSettings.setVisible(true);
    }


    public void disableResize() {
        tbtnResize.removeMouseListener(this);
        tbtnResize.setEnabled(false);
        tbtnResize.setVisible(false);
    }

    public void enableResize() {

        tbtnResize.removeMouseListener(this);
        tbtnResize.addMouseListener(this);
        tbtnResize.setEnabled(true);
        tbtnResize.setVisible(true);
    }

    public void disableFontSettings() {
        tbtnCustomize.removeMouseListener(this);
        tbtnCustomize.setEnabled(false);
        tbtnCustomize.setVisible(false);
    }

    public void enableFontSettings() {
        tbtnCustomize.removeMouseListener(this);
        tbtnCustomize.addMouseListener(this);
        tbtnCustomize.setEnabled(true);
        tbtnCustomize.setVisible(true);
    }

    public void disableBoldItalic() {
        tbtnBoald.setEnabled(false);
        tbtnBoald.setVisible(false);
        tbtnItalic.setEnabled(false);
        tbtnItalic.setVisible(false);
    }

    public void hideItalic() {
        if (!Language.isLTR()) {
            tbtnItalic.setVisible(false);
        }
    }

    public void enableBoldItalic() {
        tbtnBoald.setEnabled(true);
        tbtnBoald.setVisible(true);
        tbtnItalic.setEnabled(true);
        tbtnItalic.setVisible(true);
    }


    private TWMenu createSelectionPopup() {
        TWMenu selectionPopup = new TWMenu(Language.getString("SELECTION_MODE"), "selectionmode.gif");

        TWMenuItem mnuCell = new TWMenuItem(Language.getString("CELL"), "cellselect.gif");
        mnuCell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_oTable.getTable().setCellSelectionEnabled(true);
                g_oTable.getTable().setRowSelectionAllowed(true);
                g_oTable.getTable().setColumnSelectionAllowed(true);
            }
        });
        selectionPopup.add(mnuCell);

        TWMenuItem mnuLine = new TWMenuItem(Language.getString("ROW"), "rowselect.gif");
        mnuLine.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_oTable.getTable().setCellSelectionEnabled(true);
                g_oTable.getTable().setRowSelectionAllowed(true);
                g_oTable.getTable().setColumnSelectionAllowed(false);
            }
        });
        selectionPopup.add(mnuLine);
        return selectionPopup;
    }

    public void setButtonStatus(boolean copyh, boolean excel, boolean setAsD, boolean reserToD, boolean print, boolean customize, boolean colSet, boolean sellSele, boolean resize) {
        copyWithheader = copyh;
        if (!resize) {
            disableResize();
        } else {
            enableResize();
        }
        if (!excel) {
            disableLinkToExcell();
        } else {
            enableLinkToExcell();
        }

        if (!setAsD) {
            disableSetAsDefault();
        } else {
            enableSetAsDefault();
        }

        if (!print) {
            disablePrint();
        } else {
            enablePrint();
        }

        if (!customize) {
            disableFontSettings();
        } else {
            enableFontSettings();
        }

        if (!colSet) {
            disableColSettings();
        } else {
            enableColSettings();
        }
    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath("/menu/popupwindow/" + iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    public void actionPerformed(ActionEvent e) {
    }

    public void showWindow(Component invoker, int x, int y) {
        checkTitleBar();
        checkBoldItalic();
        addListeners();
        Dimension popupSize = this.getPreferredSize();
        Point point = new Point(x, y);
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = oToolkit.getScreenSize();
        Point p = parentpopup.getLocation();

        SwingUtilities.convertPointToScreen(p, parentpopup);

        SwingUtilities.convertPointToScreen(point, invoker);
        if ((point.getX() + popupSize.getWidth()) > screenSize.getWidth()) {
            if (parentpopup.isAnyItemVisable()) {
                point.x = p.x;
            } else {
                point.x = (int) (screenSize.getWidth() - popupSize.getWidth());
            }

        }
        if ((point.getY() + popupSize.getHeight()) > screenSize.getHeight()) {
            point.y = (int) (screenSize.height - (popupSize.getHeight()));
        }
        setSize(width, height);
        setLocation(point.x, point.y - (height + 3));
        setSelectedIndexes();
        GUISettings.applyOrientation(this);
        super.setVisible(true);
    }

    public void destroy() {
        popWindow.dispose();
        popWindow = null;
    }

    public void dispose() {
        super.dispose();
        popWindow = null;
        try {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkBoldItalic() {
        if (g_oTable != null) {
            if ((g_oTable.getWindowType() == ViewSettingsManager.ANNOUNCEMENT_VIEW) ||
                    (g_oTable.getWindowType() == ViewSettingsManager.CORPORATE_ACTION_VIEW) ||
                    (g_oTable.getWindowType() == ViewSettingsManager.NEWS_VIEW) ||
                    (g_oTable.getWindowType() == ViewSettingsManager.BROADCAST_MESSAGE_VIEW)) {
                disableBoldItalic();
            } else if (g_oTable.getWindowType() == ViewSettingsManager.RADAR_WATCH_LIST) {
                hideItalic();
            } else {
                enableBoldItalic();
            }
        }
    }

    private void checkTitleBar() {
        if ((g_oTable != null) && (g_oTable.getWindowType() == ViewSettingsManager.CHART_WINDOW_VIEW) || (g_oTable.getModel().getViewSettings().getDesktopType() == Constants.FULL_QUOTE_TYPE)) {
            tbtnhideHeader.setVisible(false);
            tbtnhideHeader.setEnabled(false);
        } else {
            tbtnhideHeader.setVisible(true);
            tbtnhideHeader.setEnabled(true);
        }
    }

    private void removeListeners() {
        try {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        } catch (Exception e) {
        }

    }

    private void addListeners() {
        Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.MOUSE_EVENT_MASK);
        Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.WINDOW_EVENT_MASK);
    }

    public class ComboRenderer extends JLabel implements ListCellRenderer {
        public ComboRenderer() {
            setOpaque(true);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String item = (String) value;
            setText(item);
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setFont(new TWFont(item, Font.PLAIN, 20, item));

            return this;
        }
    }

    public class TWEXComboBox extends TWComboBox {
        int height = 4;

        public TWEXComboBox(Vector<?> items) {
            super(items);
        }

        public Dimension getPopupSize() {
            int rows = getModel().getSize();
            int width = 0;
            for (int i = 0; i < rows; i++) {
                String item = getModel().getElementAt(i).toString();
                Font fnt = new Font(item, Font.PLAIN, 20);

                width = Math.max(width, super.getFontMetrics(fnt).stringWidth(item));
                height = Math.max(height, super.getFontMetrics(fnt).getHeight());
                item = null;
            }

            if (width < super.getSize().getWidth()) {
                return new Dimension((int) super.getSize().getWidth(), (int) super.getSize().getHeight());
            } else {
                return new Dimension(width + 20, (int) super.getSize().getHeight());
            }
        }

        public void calculateMaxNoOfRowsToDisplay() {

            int h = 25;
            Toolkit oToolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = oToolkit.getScreenSize();

            int scrh = screenSize.height;
            int avhight = scrh - (yp - 3);
            int displayablecount = avhight / height;

            int rows = getModel().getSize();
            if (displayablecount > rows) {

                setMaximumRowCount(rows);
            } else {
                setMaximumRowCount(displayablecount);
            }
        }
    }
}
