// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.datastore.*;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.io.IOException;
import java.util.*;
//change start

public class MarketModel extends CommonTable
        implements TableModel, CommonTableInterface, ClipboardOwner, DDELinkInterface, ExchangeListener {

    private static final DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    public static Hashtable<String, StringKeys> nonDefaultExchanges = new Hashtable<String, StringKeys>();
    private Clipboard clip;
    private ArrayList<ExchangeInterface> store;

    //change end

    /**
     * Constructor
     */
    public MarketModel() {
        store = new ArrayList<ExchangeInterface>();
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        init();
        //change start
        ExchangeStore.getSharedInstance().addExchangeListener(this);

    }

    public static void removeSymbolRequests() {
        Enumeration keys = nonDefaultExchanges.keys();
        while (keys.hasMoreElements()) {
            String exchangeval = (String) keys.nextElement();
            StringKeys stockRet = nonDefaultExchanges.get(exchangeval);

            if (stockRet != null) {
                DataStore.getSharedInstance().removeSymbolRequest(stockRet.getKey());
            }
        }
    }

    //change start

    private void init() {
    }
    //change end
    /* --- Table Modal's metods start from here --- */

    public void rebuildStore() {
//        store = null;
//        store = new ArrayList<ExchangeInterface>(); //[ExchangeStore.getSharedInstance().getAllMarkets().size()];
        store.clear();
        store.trimToSize();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        ArrayList<Exchange> list = new ArrayList<Exchange>();
        Exchange exchange;
        while (exchanges.hasMoreElements()) {
            exchange = (Exchange) exchanges.nextElement();
//            if(exchange.isDefault()){
            if (ExchangeStore.getSharedInstance().ismarketSummaryAvailable(exchange.getSymbol())) {
                list.add(exchange);
            }
//            }
            exchange = null;
        }
        exchanges = null;
        Collections.sort(list);
        Market[] markets;
        for (int i = 0; i < list.size(); i++) {
            exchange = list.get(i);
            markets = exchange.getSubMarkets();
            store.add(exchange);
            if ((exchange.getExpansionStatus() == Constants.EXPANSION_STATUS_EXPANDED) && (exchange.getSubMarkets().length > 0)) {
                for (int j = 0; j < markets.length; j++) {
                    store.add(markets[j]);
                }
            }
            markets = null;
            exchange = null;
        }
        getTable().updateUI();
        list = null;
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        //return ExchangeStore.getSharedInstance().getAllMarkets().size();
        return store.size();
    }

    public Object getValueAt(int iRow, int iCol) {

        try {
//            ExchangeInterface exchange = ExchangeStore.getSharedInstance().getAllMarkets().get(iRow);
            ExchangeInterface exchange = store.get(iRow);
            Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange.getSymbol());

            switch (iCol) {
                case -2:
                    return ex.isDefault();
                case -1:
                    return exchange.getSymbol();
                case 0:
                    return "" + exchange.getExpansionStatus();
                case 1:
                    return exchange.getDescription();
                case 2:
                    return "" + exchange.getVolume();
                case 3:
                    return "" + exchange.getTurnover();
                case 4:
                    return "" + exchange.getNoOfTrades();
                case 5:
                    return "" + exchange.getNoOfUps();
                case 6:
                    return "" + exchange.getNoOfDown();
                case 7:
                    return "" + exchange.getNoOfNoChange();
                case 8:
                    return "" + exchange.getSymbolsTraded();
                case 9:
                    return "" + exchange.getMarketStatus();
                case 10:
                    return "" + ex.getNetCashMapPercentage();
                case 11:
                    return doubleTransferObject.setValue(ex.getCashMapPrecentage());
                case 12:
                    if (ex.isDefault()) {
                        if (ex.getMainIndexForExchange().equals("")) {
                            return "";
                        } else {
                            return ex.getIndexName(ex.getMainIndexForExchange());
                        }
                    } else {
                        return getIndexName(nonDefaultExchanges.get(ex.getSymbol()).getKey());
                    }
                case 13:
                    if (ex.isDefault()) {
                        if (ex.getMainIndexForExchange().equals("")) {
                            return "";
                        } else {
                            return "" + ex.getIndexValue(ex.getMainIndexForExchange());
                        }
                    } else {
                        return "" + getIndexValue(nonDefaultExchanges.get(ex.getSymbol()).getKey());
                    }
                case 14:
//                    try {
                    if (ex.isDefault()) {
                        if (ex.getMainIndexForExchange().equals("")) {
                            return "";
                        } else {
                            return "" + ex.getIndexChange(ex.getMainIndexForExchange());
                        }
                    } else {
                        return "" + getIndexChange(nonDefaultExchanges.get(ex.getSymbol()).getKey());
                    }
                    /* } catch (Exception e) {
                        System.out.println("==market model==" + ex);
                    }*/
                case 15:
                    if (ex.isDefault()) {
                        if (ex.getMainIndexForExchange().equals("")) {
                            return "";
                        } else {
                            return "" + ex.getIndexPerChange(ex.getMainIndexForExchange());
                        }
                    } else {
                        return "" + getIndexPerChange(nonDefaultExchanges.get(ex.getSymbol()).getKey());
                    }
                default:
                    return "0";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        //if (isLTR())
        //{
        return super.getViewSettings().getColumnHeadings()[iCol];
        //}
        /*else
        {
   return super.getViewSettings().getColumnHeadings()[super.getViewSettings().getColumnHeadings().length - 1 - iCol ];
        }*/
    }

    public Class getColumnClass(int iCol) {
        //return getValueAt(0, iCol).getClass();
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void addTableModelListener(TableModelListener l) {
    }

    public void getDDEString(Table table, boolean withHeadings) {

        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (withHeadings)
            buffer.append(copyHeaders(table));
        for (int r = row; r < (rows + row); r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    if (c == 0) {
                        buffer.append(getValueAt(r, 0));
                    } else {
                        buffer.append("=MRegionalDdeServer|'");
                        buffer.append("AS");
                        buffer.append(getValueAt(r, -1));
                        buffer.append("'!'");
                        buffer.append(modelIndex);
                        buffer.append("'");
                        if (c > 0)
                            buffer.append("*1");
                    }

                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        for (int c = 0; c < cols; c++) {
            int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }

    public String getDDEString(String id, int col) {
        for (int i = 0; i < getRowCount(); i++) {
            Exchange exchange = ExchangeStore.getSharedInstance().getDefaultExchanges(i);
            if (!exchange.isExpired() && !exchange.isInactive() && exchange.getSymbol().equals(id)) {
                return (String) getValueAt(i, col);
            }
        }
        return "0";
    }

    //change start

    public void windowClosing() {

    }

    public void exchangeAdded(Exchange exchange) {
        rebuildStore();
    }

    public void exchangeRemoved(Exchange exchange) {
        rebuildStore();
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        rebuildStore();
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void setDataForNonDefaults() {
        try {
            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = exchanges.nextElement();
                if (!exchange.isDefault() && ExchangeStore.getSharedInstance().isValidExchange(exchange.getSymbol())) {
                    NonDefaultIndexList nonDefaultExchange = new NonDefaultIndexList(exchange.getSymbol());
                    if (nonDefaultExchange.getMainIndex() != null) {
                        if (nonDefaultExchange.getMainIndex().getKey() != null) {
                            DataStore.getSharedInstance().addSymbolRequest(nonDefaultExchange.getMainIndex().getKey());
                        }
                        nonDefaultExchanges.put(exchange.getSymbol(), nonDefaultExchange.getMainIndex());
                    }
                }
                //            exchngeHash.put(exchange.getDescription(), exchange.getSymbol());
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getIndexName(String key) {
        return DataStore.getSharedInstance().getCompanyName(key);
    }

    public double getIndexValue(String key) {
        Stock index = DataStore.getSharedInstance().getStockObject(key);
        return index.getLastTradeValue();
    }

    public double getIndexChange(String key) {
        Stock index = DataStore.getSharedInstance().getStockObject(key);
        return index.getChange();
    }

    public double getIndexPerChange(String key) {
        Stock index = DataStore.getSharedInstance().getStockObject(key);
        return index.getPercentChange();
    }

    class StringKeys implements Comparable {
        String key;
        String name = "";

        StringKeys(String key1, String name1) {
            key = key1;
            name = name1;
        }

        public String toString() {
            return name;
        }

        public String getKey() {
            return key;
        }

        public int compareTo(Object o) {
            return toString().compareTo(o.toString());
        }

    }

    class NonDefaultIndexList {
        String exchange;
        StringKeys mainIndex = null;
        private ArrayList<StringKeys> enabledIndexes;

        NonDefaultIndexList(String exchange) {
            enabledIndexes = new ArrayList<StringKeys>();
            loadIndexFile(exchange);
            this.exchange = exchange;
        }

        public void loadIndexFile(String exchange) {
            try {
                SmartProperties exgIndexes = new SmartProperties(Meta.DS);
                Vector keyVector = new Vector();
                String key = null;
                String value = null;
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/index_" + Language.getSelectedLanguage() + ".msf";
                exgIndexes.loadCompressed(sLangSpecFileName);

                //Searching indexes
                if (exgIndexes.size() > 0) {
                    Enumeration indexEnum = exgIndexes.keys();
                    while (indexEnum.hasMoreElements()) {
                        String skey = (String) indexEnum.nextElement();
                        String data = (String) exgIndexes.get(skey);
                        try {
                            if (Integer.parseInt(data.split(Meta.RS)[1]) == 1) {
                                String[] field = data.split(Meta.FS);
                                String dataRec = field[5] + Meta.ID + exchange + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                                String keyR = SharedMethods.getKey(exchange, skey, Meta.INSTRUMENT_INDEX);
                                StringKeys index = new StringKeys(keyR, field[2]);
                                //todo check for duplicates
                                enabledIndexes.add(index);
                                if (field[1].equals("IM")) {  //--To Solve the conflict in MSM -Shanika
//                            if (dataRec.indexOf("M") >= 0) {
                                    mainIndex = index;
                                }
                                index = null;
                                ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                            } else {
                                //do nothing
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                }
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_COM_INDEX)) {
                    sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/cust_index_" + Language.getSelectedLanguage() + ".msf";
                    exgIndexes = new SmartProperties(Meta.DS);
                    File f = new File(sLangSpecFileName);
                    boolean bIsLangSpec = f.exists();
                    if (bIsLangSpec) {
                        exgIndexes.loadCompressed(sLangSpecFileName);
                        Enumeration indexEnum = exgIndexes.keys();
                        while (indexEnum.hasMoreElements()) {
                            try {
                                String skey = (String) indexEnum.nextElement();
                                String data = (String) exgIndexes.get(skey);
                                if (data.split(Meta.RS)[1].equals(Settings.getInstitutionCode())) {
                                    String[] field = data.split(Meta.FS);
                                    String dataRec = field[5] + Meta.ID + exchange + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                                    String keyR = SharedMethods.getKey(exchange, skey, Meta.INSTRUMENT_INDEX);
                                    StringKeys index = new StringKeys(keyR, field[2]);
                                    enabledIndexes.add(index);
                                    ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        }
                    }
                }
                Collections.sort(enabledIndexes);
                if (mainIndex == null && !enabledIndexes.isEmpty()) {
                    mainIndex = enabledIndexes.get(0);
                }
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }


        }

        public String getExchange() {
            return exchange;
        }

        public void setExchange(String exchange) {
            this.exchange = exchange;
        }

        public ArrayList<StringKeys> getEnabledIndexes() {
            return enabledIndexes;
        }

        public void setEnabledIndexes(ArrayList<StringKeys> enabledIndexes) {
            this.enabledIndexes = enabledIndexes;
        }

        public StringKeys getMainIndex() {
            return mainIndex;
        }

        public void setMainIndex(StringKeys mainIndex) {
            this.mainIndex = mainIndex;
        }
    }
    //change end
}