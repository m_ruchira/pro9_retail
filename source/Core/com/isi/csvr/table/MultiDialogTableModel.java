package com.isi.csvr.table;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 2, 2006
 * Time: 10:56:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class MultiDialogTableModel extends CommonTable
        implements DetailQuote, TableModel, CommonTableInterface {

    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private String symbol;

    public int getRowCount() {
        return 2;
    }

    public int getColumnCount() {
        return 4;
    }

    public void setSymbol(String symbolIn) {
        symbol = symbolIn;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            if (symbol == null)
                return "";
            Stock index = DataStore.getSharedInstance().getStockObject(symbol);
            if (index == null)
                return "";

            switch (iRow) {
                case 0:
                    switch (iCol) {
                        case 0:
                            return Language.getString("LAST_MD");
                        case 1:
                            return doubleTransferObject.setValue(index.getLastTradeValue());
                        case 2:
                            return Language.getString("CHANGE_MD");
                        case 3:
                            return doubleTransferObject.setValue(index.getChange());
                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("HIGH_MD");
                        case 1:
                            return doubleTransferObject.setValue(index.getHigh());
                        case 2:
                            return Language.getString("LOW_MD");
                        case 3:
                            return doubleTransferObject.setValue(index.getLow());
                    }
            }
            return "";
        } catch (Exception e) {
            return "";
        }

    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'P';
                    case 2:
                        return 2;
                    case 3:
                        return 5;
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
        }
        return 0;
    }

    public long getTimeOffset() {
        return 0;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
}
