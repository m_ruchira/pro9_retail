package com.isi.csvr.table;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 9, 2006
 * Time: 9:41:58 PM
 */
public class CombinedDepthRenderer extends TWBasicTableRenderer implements TableCellRenderer {

    static Color bidask_BidColor;
    static Color bidask_AskColor;
    static Color g_oBGBid1;
    static Color g_oBGBid2;
    static Color g_oBGAsk1;
    static Color g_oBGAsk2;
    static Color g_oFGBid1;
    static Color g_oFGBid2;
    static Color g_oFGAsk1;
    static Color g_oFGAsk2;
    static int modelColumn;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Border unselectedBorder;
    private String[] g_asColumns;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private TWDateFormat g_oTimeFormat = new TWDateFormat(" hh:mm:ss ");
    private String g_sNA = "NA";
    private String priceMarketOrder = "NA";
    private String priceMarketOnOpening = "NA";
    private long longValue;
    private double doubleValue;
    private int rowCount;
    private boolean isLastRow;
    private Color foreground, background;
    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0  ");

    public CombinedDepthRenderer() {
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
        g_oBGBid1 = Color.white;
        g_oBGBid2 = Color.white;
        g_oBGAsk1 = Color.white;
        g_oBGAsk2 = Color.white;
        g_oFGBid1 = Color.black;
        g_oFGBid2 = Color.black;
        g_oFGAsk1 = Color.black;
        g_oFGAsk2 = Color.black;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oFG1 = Theme.getColor("COMBINED_DEPTH_TABLE_PRICE_FGCOLOR1");
            g_oBG1 = Theme.getColor("COMBINED_DEPTH_TABLE_PRICE_BGCOLOR1");
            g_oFG2 = Theme.getColor("COMBINED_DEPTH_TABLE_PRICE_FGCOLOR2");
            g_oBG2 = Theme.getColor("COMBINED_DEPTH_TABLE_PRICE_BGCOLOR2");
            unselectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oFG1);

            g_oSelectedFG = Theme.getColor("COMBINED_DEPTH_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("COMBINED_DEPTH_TABLE_SELECTED_BGCOLOR");

            g_oBGBid1 = Theme.getColor("COMBINED_DEPTH_TABLE_BID_BGCOLOR1");
            g_oBGBid2 = Theme.getColor("COMBINED_DEPTH_TABLE_BID_BGCOLOR2");
            g_oBGAsk1 = Theme.getColor("COMBINED_DEPTH_TABLE_ASK_BGCOLOR1");
            g_oBGAsk2 = Theme.getColor("COMBINED_DEPTH_TABLE_ASK_BGCOLOR2");

            g_oFGBid1 = Theme.getColor("COMBINED_DEPTH_TABLE_BID_FGCOLOR1");
            g_oFGBid2 = Theme.getColor("COMBINED_DEPTH_TABLE_BID_FGCOLOR2");
            g_oFGAsk1 = Theme.getColor("COMBINED_DEPTH_TABLE_ASK_FGCOLOR1");
            g_oFGAsk2 = Theme.getColor("COMBINED_DEPTH_TABLE_ASK_FGCOLOR2");
        } catch (Exception e) {
            e.printStackTrace();
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            g_oBGBid1 = Color.white;
            g_oBGBid2 = Color.white;
            g_oBGAsk1 = Color.white;
            g_oBGAsk2 = Color.white;
            g_oFGBid1 = Color.black;
            g_oFGBid2 = Color.black;
            g_oFGAsk1 = Color.black;
            g_oFGAsk2 = Color.black;
        }
    }

    public void propertyChanged(int property) {

    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {

        g_asColumns = asColumns;
        g_asRendIDs = asRendIDs;
        g_sNA = Language.getString("NA");
        priceMarketOrder = Language.getString("DEPTH_PRICE_MARKET_ORDER");
        priceMarketOnOpening = Language.getString("DEPTH_PRICE_MARKET_ON_OPENING");
        reload();
        g_iStringAlign = JLabel.LEADING;
        g_iNumberAlign = JLabel.RIGHT;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        modelColumn = table.convertColumnIndexToModel(column);

        rowCount = table.getModel().getRowCount();
        isLastRow = (rowCount == (row + 1));
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getCombinedSelectedColorFG();
                background = sett.getCombinedSelectedColorBG();
            } else {
                if (row % 2 == 0) {
                    if ((modelColumn == 0) || (modelColumn == 1)) {
                        foreground = sett.getCombinedBidColor1FG();
                        background = sett.getCombinedBidColor1BG();
                    } else if ((modelColumn == 3) || (modelColumn == 4)) {
                        foreground = sett.getCombinedAskColor1FG();
                        background = sett.getCombinedAskColor1BG();
                    } else {
                        foreground = sett.getCombinedPriceColor1FG();
                        background = sett.getCombinedPriceColor1BG();
                    }
                } else {
                    if ((modelColumn == 0) || (modelColumn == 1)) {
                        foreground = sett.getCombinedBidColor2FG();
                        background = sett.getCombinedBidColor2BG();
                    } else if ((modelColumn == 3) || (modelColumn == 4)) {
                        foreground = sett.getCombinedAskColor2FG();
                        background = sett.getCombinedAskColor2BG();
                    } else {
                        foreground = sett.getCombinedPriceColor2FG();
                        background = sett.getCombinedPriceColor2BG();
                    }
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                if ((modelColumn == 0) || (modelColumn == 1)) {
                    foreground = g_oFGBid1;
                    background = g_oBGBid1;
                } else if ((modelColumn == 3) || (modelColumn == 4)) {
                    foreground = g_oFGAsk1;
                    background = g_oBGAsk1;
                } else {
                    foreground = g_oFG1;
                    background = g_oBG1;
                }
            } else {
                if ((modelColumn == 0) || (modelColumn == 1)) {
                    foreground = g_oFGBid2;
                    background = g_oBGBid2;
                } else if ((modelColumn == 3) || (modelColumn == 4)) {
                    foreground = g_oFGAsk2;
                    background = g_oBGAsk2;
                } else {
                    foreground = g_oFG2;
                    background = g_oBG2;
                }
            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        try {
            oPriceFormat = ((ExchangeFormatInterface) table.getModel()).getDecimalFormat();
        } catch (Exception e) {
            e.printStackTrace();
        }


        int iRendID = 0;

        iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 3: // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    if (doubleValue == Constants.DEPTH_PRICE_MARKET_ORDER_VALUE) {
                        lblRenderer.setText(priceMarketOrder);
                    } else if (doubleValue == Constants.DEPTH_PRICE_MARKET_ON_OPENING_VALUE) {
                        lblRenderer.setText(priceMarketOnOpening);
                    } else if (doubleValue > 0) {
                        lblRenderer.setText(oPriceFormat.format(doubleValue));
                    } else {
                        lblRenderer.setText(Constants.NULL_STRING);
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    longValue = ((LongTransferObject) (value)).getValue();
                    if (longValue > 0) {
                        lblRenderer.setText(oQuantityFormat.format(longValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    } else {
                        lblRenderer.setText(Constants.NULL_STRING);
                    }
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }

        if (isLastRow) {
            lblRenderer.setBorder(unselectedBorder);
        } else {
            lblRenderer.setBorder(cellBorder);
        }

        return lblRenderer;
    }
}


