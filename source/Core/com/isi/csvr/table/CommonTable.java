// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.announcement.AnnouncementRenderer;
import com.isi.csvr.brokers.BrokerTableRenderer;
import com.isi.csvr.chart.analysistechniques.AnalysTechRenderer;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.customizer.Customizeable;
import com.isi.csvr.customizer.CustomizerConstats;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.marketdepth.DepthCalculatorRenderer;
import com.isi.csvr.portfolio.AdvanceSellTableRenderer;
import com.isi.csvr.portfolio.PortfolioImportRenderer;
import com.isi.csvr.portfolio.SplitAdjustmentRenderer;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.scanner.Results.ScannerRenderer;
import com.isi.csvr.sectormap.SectorMapRenderer;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.symbolselector.CompanyCellEditor;
import com.isi.csvr.symbolselector.CompanyTableRenderer;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trade.TimeAndSalesRenderer;
import com.isi.csvr.trading.portfolio.TradingPfDCRenderer;
import com.isi.csvr.trading.portfolio.TradingPortfolioRenderer;
import com.isi.csvr.trading.ui.FundTransferTableRenderer;
import com.isi.csvr.trading.ui.TradeTableRenderer;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.math.BigInteger;
import java.util.Arrays;

public abstract class CommonTable extends AbstractTableModel
        implements WindowWrapper, Customizeable, CustomizerConstats, ExchangeFormatInterface, CommonTableInterface {

    boolean g_bLTR = true;
    private ViewSetting g_oViewSettings;
    private Table table;
    private String[] g_asHeadings;
    private int[] g_aiRendIDs;
    /*public static final TWDecimalFormat[] decimals  = {  new TWDecimalFormat(" ###,##0 "),
                                                                 new TWDecimalFormat(" ###,##0.0 "),
                                                                 new TWDecimalFormat(" ###,##0.00 "),
                                                                 new TWDecimalFormat(" ###,##0.000 "),
                                                                 new TWDecimalFormat(" ###,##0.0000 "),
                                                                 new TWDecimalFormat(" ###,##0.00000 "),
                                                                 new TWDecimalFormat(" ###,##0.000000 "),
                                                                 new TWDecimalFormat(" ###,##0.0000000 "),
                                                                 new TWDecimalFormat(" ###,##0.00000000 "),
                                                                 new TWDecimalFormat(" ###,##0.000000000 "),
                                                                 new TWDecimalFormat(" ###,##0.0000000000 ")};

    public static final TWDecimalFormat[] decimalNoZero  = {  new TWDecimalFormat(" ###,##0 "),
                                                                 new TWDecimalFormat(" ###,##0.# "),
                                                                 new TWDecimalFormat(" ###,##0.## "),
                                                                 new TWDecimalFormat(" ###,##0.### "),
                                                                 new TWDecimalFormat(" ###,##0.#### "),
                                                                 new TWDecimalFormat(" ###,##0.##### "),
                                                                 new TWDecimalFormat(" ###,##0.###### "),
                                                                 new TWDecimalFormat(" ###,##0.####### "),
                                                                 new TWDecimalFormat(" ###,##0.######## "),
                                                                 new TWDecimalFormat(" ###,##0.######### "),
                                                                 new TWDecimalFormat(" ###,##0.########## ")};*/
    private byte decimalCount = 2;

    /**
     * Constructor
     */
    public CommonTable() {
        //threeDecimalNoZero.flagged = true;
    }

    /**
     * this metod is invoked when the wondow is being closing
     */
    public void invalidateTable() {
        g_oViewSettings = null;
    }

    public JTable getTable() {
        return table.getTable();
    }

    /**
     * Set the table for this modal
     */
    public void setTable(Table oTable) {
        table = oTable;
        if ((table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE) ||
                (table.getWindowType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) ||
                (table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_MFUND) ||
                (table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_FOREX)) {
            table.getTable().setDefaultRenderer(Object.class, new DQTableRenderer());
            table.getTable().setDefaultRenderer(Number.class, new DQTableRenderer());
        } else if (table.getWindowType() == ViewSettingsManager.ACCOUNT_SUMMARY_VIEW) {
            table.getTable().setDefaultRenderer(Object.class, new AccountSummaryTableRenderer());
            table.getTable().setDefaultRenderer(Number.class, new AccountSummaryTableRenderer());
        } else if (table.getWindowType() == ViewSettingsManager.CURRENCY_VIEW) {
            table.getTable().setDefaultRenderer(Object.class, new CurrencyRenderer());
            table.getTable().setDefaultRenderer(Number.class, new CurrencyRenderer());
        } else if (table.getWindowType() == ViewSettingsManager.SNAP_QUOTE) {
            table.getTable().setDefaultRenderer(Object.class, new SnapQuoteRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new DQTableRenderer());
        } else if ((table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW)) {
            table.getTable().setDefaultRenderer(Object.class, new CashFlowDetailQuoteRenderer());
            table.getTable().setDefaultRenderer(Number.class, new DQTableRenderer());
        } else if (table.getWindowType() == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
            table.getTable().setDefaultRenderer(Object.class, new DepthCalculatorRenderer());
            table.getTable().setDefaultRenderer(Number.class, new DepthCalculatorRenderer());
        } else if ((table.getWindowType() == ViewSettingsManager.TIMEnSALES_VIEW2) ||
                (table.getWindowType() == ViewSettingsManager.FULL_TIMEnSALES_VIEW)) {
            table.getTable().setDefaultRenderer(Object.class, new TimeAndSalesRenderer(g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new TimeAndSalesRenderer(g_aiRendIDs));
        } else if (table.getWindowType() == ViewSettingsManager.MARKET_TIMEnSALES_VIEW) {
            table.getTable().setDefaultRenderer(Object.class, new TimeAndSalesRenderer(g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new TimeAndSalesRenderer(g_aiRendIDs));
        } else if ((table.getWindowType() == ViewSettingsManager.ANNOUNCEMENT_VIEW) ||
                (table.getWindowType() == ViewSettingsManager.CORPORATE_ACTION_VIEW) ||
                (table.getWindowType() == ViewSettingsManager.NEWS_VIEW) ||
                (table.getWindowType() == ViewSettingsManager.BROADCAST_MESSAGE_VIEW)) {
            table.getTable().setDefaultRenderer(Object.class, new AnnouncementRenderer(g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new AnnouncementRenderer(g_aiRendIDs));
        } else if ((table.getWindowType() == ViewSettingsManager.FUND_TRANSFER_FRAME)) {
            table.getTable().setDefaultRenderer(Object.class, new FundTransferTableRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new FundTransferTableRenderer(g_asHeadings, g_aiRendIDs));
        } else if ((table.getWindowType() == ViewSettingsManager.BROKERS_VIEW)) {
            table.getTable().setDefaultRenderer(Object.class, new BrokerTableRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new BrokerTableRenderer(g_asHeadings, g_aiRendIDs));
        } else if (table.getWindowType() == ViewSettingsManager.EXCHANGE_VIEW) {
            table.getTable().setDefaultRenderer(Object.class, new ExchangeTableRenderer(g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new ExchangeTableRenderer(g_aiRendIDs));
        } else if ((table.getWindowType() == ViewSettingsManager.TRADE_QUEUED_TRADES) ||
                (table.getWindowType() == ViewSettingsManager.TRADE_PROGRAMED_ORDERS) ||
                (table.getWindowType() == ViewSettingsManager.ORDER_SEARCH) ||
                (table.getWindowType() == ViewSettingsManager.TRADE_PENDING_ORDERS)) {
            JCheckBox checkBox = new JCheckBox();
            checkBox.setHorizontalAlignment(JLabel.CENTER);
            DefaultCellEditor checkBoxEditor = new DefaultCellEditor(checkBox);
            table.getTable().getColumn("1").setCellEditor(checkBoxEditor);
            table.getTable().setDefaultRenderer(Object.class, new TradeTableRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new TradeTableRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Boolean.class, new TradeTableRenderer(g_asHeadings, g_aiRendIDs));
        } else if (table.getWindowType() == ViewSettingsManager.COMPANY_TABLE_VIEW) {
            table.getTable().setDefaultRenderer(Object.class, new CompanyTableRenderer(g_aiRendIDs));
        } else if (table.getWindowType() == ViewSettingsManager.SECTORMAP_VIEW) {
            table.getTable().setDefaultRenderer(Object.class, new SectorMapRenderer(g_aiRendIDs));
        } else if (table.getWindowType() == ViewSettingsManager.OPEN_POSITIONS) {
            table.getTable().setDefaultRenderer(Object.class, new OpenPositionsRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new OpenPositionsRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Boolean.class, new OpenPositionsRenderer(g_asHeadings, g_aiRendIDs));
        } else if (table.getWindowType() == ViewSettingsManager.SPLIT_ADJUSTMENT_VIEW) {
            table.getTable().setDefaultRenderer(Object.class, new SplitAdjustmentRenderer(g_aiRendIDs));
            table.getTable().setDefaultRenderer(Long.class, new SplitAdjustmentRenderer(g_aiRendIDs));
            table.getTable().setDefaultRenderer(Double.class, new SplitAdjustmentRenderer(g_aiRendIDs));
            table.getTable().setDefaultRenderer(Boolean.class, new SplitAdjustmentRenderer(g_aiRendIDs));
        } else if (table.getWindowType() == ViewSettingsManager.PORTFOLIO_IMPORT) {
            table.getTable().setDefaultRenderer(Object.class, new PortfolioImportRenderer());
            table.getTable().setDefaultRenderer(Long.class, new PortfolioImportRenderer());
            table.getTable().setDefaultRenderer(Double.class, new PortfolioImportRenderer());
            table.getTable().setDefaultRenderer(Boolean.class, new PortfolioImportRenderer());
        } else {
            table.getTable().setDefaultRenderer(Object.class, new CommonTableRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Number.class, new CommonTableRenderer(g_asHeadings, g_aiRendIDs));
            table.getTable().setDefaultRenderer(Boolean.class, new CommonTableRenderer(g_asHeadings, g_aiRendIDs));
            //  table.getTable().setDefaultRenderer(Boolean.class, new CommonTableRenderer(g_asHeadings, g_aiRendIDs));
        }

        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }

    }

    /*
     *  Used for MarketDepth Table
     */
    public void setTable(Table oTable, MarketAskDepthRenderer rendererIn, boolean isBidType) {
        MarketAskDepthRenderer renderer = rendererIn;
        table = oTable;
        renderer.setBidRenderer(isBidType);
        renderer.initRenderer(g_asHeadings, g_aiRendIDs);
        table.getTable().setDefaultRenderer(Object.class, renderer);
        table.getTable().setDefaultRenderer(Number.class, renderer);
        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
    }

    public void setTable(Table oTable, TWTableRenderer rendererIn) {
        table = oTable;
        rendererIn.initRenderer(g_asHeadings, g_aiRendIDs);
        table.getTable().setDefaultRenderer(Object.class, (TableCellRenderer) rendererIn);
        table.getTable().setDefaultRenderer(String.class, (TableCellRenderer) rendererIn);
        table.getTable().setDefaultRenderer(Long.class, (TableCellRenderer) rendererIn);
        table.getTable().setDefaultRenderer(Float.class, (TableCellRenderer) rendererIn);
        table.getTable().setDefaultRenderer(Double.class, (TableCellRenderer) rendererIn);

        if (g_oViewSettings != null) {
            setFont();
            //g_oViewSettings.setParent(this);
            updateGUI();
        }
    }

    public void setTable(Table oTable, MarketBidDepthRenderer rendererIn, boolean isBidType) {
        MarketBidDepthRenderer renderer = rendererIn;
        table = oTable;
        renderer.setBidRenderer(isBidType);
        renderer.initRenderer(g_asHeadings, g_aiRendIDs);
        table.getTable().setDefaultRenderer(Object.class, renderer);
        table.getTable().setDefaultRenderer(Number.class, renderer);
        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
    }

    public void setTable(Table oTable, CombinedDepthRenderer rendererIn) {
        CombinedDepthRenderer renderer = rendererIn;
        table = oTable;
        renderer.initRenderer(g_asHeadings, g_aiRendIDs);
        table.getTable().setDefaultRenderer(Object.class, renderer);
        table.getTable().setDefaultRenderer(Number.class, renderer);
        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
    }

    /*
     *  Used for MarketDepth by Price Table
     */
//    public void setTable(Table oTable, DepthByPriceRenderer rendererIn, boolean isBidType) {
//        DepthByPriceRenderer renderer = rendererIn;
//    	g_oTable = oTable;
//        renderer.setBidRenderer(isBidType);
//        renderer.initRenderer(g_asHeadings,g_aiRendIDs,g_oViewSettings.getID());
//        g_oTable.getTable().setDefaultRenderer(Object.class, renderer);
//        g_oTable.getTable().setDefaultRenderer(Number.class, renderer);
//        //g_oTable.getTable().setShowGrid(false);
//        //g_oTable.getTable().setIntercellSpacing(new Dimension(0,0));
//
//        if (g_oViewSettings != null)
//        {
//            setFont();
//            //g_oViewSettings.setParent(this);
//            updateGUI();
//        }
//    }

    /*
     *  Used for MarketDepth By Order Table
     */
//    public void setTable(Table oTable, DepthByOrderRenderer rendererIn, boolean isBidType) {
//        DepthByOrderRenderer renderer = rendererIn;
//    	g_oTable = oTable;
//        renderer.setBidRenderer(isBidType);
//        renderer.initRenderer(g_asHeadings, g_aiRendIDs); //, g_oViewSettings.getID());
//        g_oTable.getTable().setDefaultRenderer(Object.class, renderer);
//        g_oTable.getTable().setDefaultRenderer(Number.class, renderer);
//        //g_oTable.getTable().setShowGrid(false);
//        //g_oTable.getTable().setIntercellSpacing(new Dimension(0,0));
//
//        if (g_oViewSettings != null) {
//            setFont();
//            //g_oViewSettings.setParent(this);
//            updateGUI();
//        }
//    }


    /*
     *  Used for MarketDepth Table
     */
    /*public void setTable(Table oTable, DepthSummaryRenderer rendererIn, boolean isBidType) {
        //DepthSummaryRenderer renderer = rendererIn;
        table = oTable;
        rendererIn.setBidRenderer(isBidType);
        rendererIn.initRenderer(g_asHeadings, g_aiRendIDs);
        table.getTable().setDefaultRenderer(Object.class, rendererIn);
        table.getTable().setDefaultRenderer(Number.class, rendererIn);
        //g_oTable.getTable().setShowGrid(false);
        //g_oTable.getTable().setIntercellSpacing(new Dimension(0,0));

        if (g_oViewSettings != null) {
            setFont();
            //g_oViewSettings.setParent(this);
            updateGUI();
        }
    }*/

    /*
     *  Used for Indices Table
     *
    public void setTable(Table oTable, PortfolioRenderer rendererIn) {
        IndicesRenderer renderer = rendererIn;
    	g_oTable = oTable;
        renderer.initRenderer(g_asHeadings,g_aiRendIDs);
        g_oTable.getTable().setDefaultRenderer(Object.class, renderer);

        if (g_oViewSettings != null)
        {
            setFont();
            //g_oViewSettings.setParent(this);
            updateGUI();
        }
    }*/

    public void setAdvTradingPortfolioTable(Table oTable) {

        table = oTable;
        table.getTable().setDefaultRenderer(Object.class, new AdvanceSellTableRenderer(g_aiRendIDs));
        table.getTable().setDefaultRenderer(Number.class, new AdvanceSellTableRenderer(g_aiRendIDs));
        table.getTable().setDefaultRenderer(Boolean.class, new AdvanceSellTableRenderer(g_aiRendIDs));
        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
    }

    public void setTradingPortfolioTable(Table oTable, boolean isValuationType) {
        TradingPortfolioRenderer renderer = new TradingPortfolioRenderer(g_asHeadings, g_aiRendIDs);
        if (isValuationType)
            renderer.setValuationType();
        table = oTable;
        table.getTable().setDefaultRenderer(Double.class, renderer);
        table.getTable().setDefaultRenderer(String.class, renderer);
        table.getTable().setDefaultRenderer(Float.class, renderer); //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Long.class, renderer);   //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Byte.class, renderer);
        table.getTable().setShowGrid(false);
        table.getTable().setIntercellSpacing(new Dimension(0, 0));
        table.getTable().setCellSelectionEnabled(true);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
        renderer = null;
    }

    public void setDefCurTradingPortfolioTable(Table oTable, boolean isValuationType) {
        TradingPfDCRenderer DCRenderer = new TradingPfDCRenderer(g_asHeadings, g_aiRendIDs);
        //  TradingPortfolioRenderer  DCRenderer = new TradingPortfolioRenderer  (g_asHeadings, g_aiRendIDs);
        if (isValuationType)
            DCRenderer.setValuationType();
        table = oTable;
        table.getTable().setDefaultRenderer(Double.class, DCRenderer);
        table.getTable().setDefaultRenderer(String.class, DCRenderer);
        table.getTable().setDefaultRenderer(Float.class, DCRenderer); //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Long.class, DCRenderer);   //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Byte.class, DCRenderer);
        table.getTable().setShowGrid(false);
        table.getTable().setIntercellSpacing(new Dimension(0, 0));
        table.getTable().setCellSelectionEnabled(true);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
        DCRenderer = null;
    }

    public void setBreakOutsTable(Table oTable) {
        ScannerRenderer renderer = new ScannerRenderer(g_aiRendIDs);
        table = oTable;
        table.getTable().setDefaultRenderer(Double.class, renderer);
        table.getTable().setDefaultRenderer(String.class, renderer);
//           table.getTable().setDefaultRenderer(Float.class, renderer); //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
//           table.getTable().setDefaultRenderer(Long.class, renderer);   //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
//           table.getTable().setDefaultRenderer(Byte.class, renderer);
        table.getTable().setShowGrid(false);
        table.getTable().setIntercellSpacing(new Dimension(0, 0));
        table.getTable().setCellSelectionEnabled(true);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
        renderer = null;
    }



    /*public void setVolumeWatcherTable(Table oTable) {
           VolumeWatchRenderer renderer = new VolumeWatchRenderer(g_aiRendIDs);

           table = oTable;
           table.getTable().setDefaultRenderer(Double.class, renderer);
           table.getTable().setDefaultRenderer(String.class, renderer);
           table.getTable().setDefaultRenderer(Float.class, renderer); //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
           table.getTable().setDefaultRenderer(Long.class, renderer);   //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
           table.getTable().setDefaultRenderer(Byte.class, renderer);
           table.getTable().setShowGrid(false);
           table.getTable().setIntercellSpacing(new Dimension(0, 0));
           table.getTable().setCellSelectionEnabled(true);
           table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

           if (g_oViewSettings != null) {
               setFont();
               updateGUI();
           }
           renderer = null;
       }*/

    /**
     * Set the table for this modal
     *
     * @param oTable
     * @param isValuationType
     */
    public void setPortfolioTable(Table oTable, boolean isValuationType) {
        PortfolioRenderer renderer = new PortfolioRenderer(g_asHeadings, g_aiRendIDs);
        if (isValuationType)
            renderer.setValuationType();
        table = oTable;
        table.getTable().setDefaultRenderer(Object.class, renderer);
        table.getTable().setDefaultRenderer(String.class, renderer);
        table.getTable().setDefaultRenderer(Float.class, renderer); //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Long.class, renderer);   //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Double.class, renderer);
        table.getTable().setDefaultRenderer(Byte.class, renderer);
        table.getTable().setShowGrid(false);
        table.getTable().setIntercellSpacing(new Dimension(0, 0));
        table.getTable().setCellSelectionEnabled(true);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
    }

    public void setPortfolioTableAll(Table oTable) {
        PortfolioRendererAll renderer = new PortfolioRendererAll(g_asHeadings, g_aiRendIDs);
        table = oTable;
        table.getTable().setDefaultRenderer(Object.class, renderer);
        table.getTable().setDefaultRenderer(String.class, renderer);
        table.getTable().setDefaultRenderer(Float.class, renderer); //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Long.class, renderer);   //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Double.class, renderer);
        table.getTable().setDefaultRenderer(Byte.class, renderer);
        table.getTable().setShowGrid(false);
        table.getTable().setIntercellSpacing(new Dimension(0, 0));
        table.getTable().setCellSelectionEnabled(true);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
    }

    /**
     * Deprecated Method
     * Set the table for this modal
     */
    public void setPortfolioTable(Table oTable) {
        table = oTable;
        table.getTable().setDefaultRenderer(String.class, new PortfolioRenderer(g_asHeadings, g_aiRendIDs));
        table.getTable().setDefaultRenderer(Object.class, new PortfolioRenderer(g_asHeadings, g_aiRendIDs));
        table.getTable().setDefaultRenderer(Double.class, new PortfolioRenderer(g_asHeadings, g_aiRendIDs));
        table.getTable().setDefaultRenderer(Float.class, new PortfolioRenderer(g_asHeadings, g_aiRendIDs));
        table.getTable().setDefaultRenderer(Long.class, new PortfolioRenderer(g_asHeadings, g_aiRendIDs));
        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
    }

    public void setMISTTable(Table oTable, com.isi.csvr.mist.MISTRenderer renderer) {
        table = oTable;
        table.getTable().setDefaultRenderer(Object.class, renderer); //new MISTRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setDefaultRenderer(Number.class, renderer); //new MISTRenderer(g_asHeadings,g_aiRendIDs));
        table.getTable().setShowGrid(false);
        table.getTable().setIntercellSpacing(new Dimension(0, 0));
        //mist cell edit
        //table.getTable().setDefaultEditor(String.class,new CompanyCellEditor(Client.getInstance().companySelector));
        table.getTable().setDefaultEditor(String.class, new CompanyCellEditor(new JComboBox()));

        //g_oTable.getTable().setCellSelectionEnabled(true);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        //g_oTable.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
    }

    private void setFont() {
        if (g_oViewSettings.getFont() != null) {
            table.getTable().setFont(g_oViewSettings.getFont());
            //FontMetrics oMetrices = g_oTable.getTable().getGraphics().getFontMetrics();
            //g_oTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent());
        }

        if (g_oViewSettings.getHeaderFont() != null) {
            table.getTable().getTableHeader().setFont(g_oViewSettings.getHeaderFont());
            //if (table.isSortable()) {
            for (int i = 0; i < table.getTable().getColumnModel().getColumnCount(); i++) {
                SortButtonRenderer oRend = (SortButtonRenderer) table.getTable().getColumnModel().getColumn(i).getHeaderRenderer();
                if (oRend != null) {
                    oRend.setHeaderFont(g_oViewSettings.getHeaderFont());
                    oRend.updateUI();
                }
                oRend = null;
            }
            table.getTable().getTableHeader().updateUI();
            //}
            //FontMetrics oMetrices = g_oTable.getTable().getGraphics().getFontMetrics();
            //g_oTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent());
        }
        try {
            table.getTable().updateUI();
        } catch (Exception e) {
        }
    }

    public void adjustRows() {
        try {
            //FontMetrics oMetrices = g_oTable.getTable().getGraphics().getFontMetrics();
            //g_oTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent());
            table.getTable().updateUI();
        } catch (Exception e) {
            // avoid null pointer exceptions
        }
    }

    /**
     * Returns the view setting object for this table
     */
    public ViewSetting getViewSettings() {
        return g_oViewSettings;
    }

    /**
     * Set the view setting object for this table
     */
    public void setViewSettings(ViewSetting oViewSettings) {
        g_oViewSettings = oViewSettings;
        g_asHeadings = g_oViewSettings.getColumnHeadings();
        /*try {
            System.out.println(oViewSettings.getCaption() + ":" + oViewSettings.getSymbols().toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        } */

        //g_aiRendIDs = new int[g_asHeadings.length];

        if (g_oViewSettings.getRenderingIDs().length() > g_asHeadings.length) {
            g_aiRendIDs = new int[g_oViewSettings.getRenderingIDs().length()];
            Arrays.fill(g_aiRendIDs, 0);
        } else {
            g_aiRendIDs = new int[g_asHeadings.length];
        }

        for (int i = 0; i < g_aiRendIDs.length; i++) {
            try {
                g_aiRendIDs[i] = g_oViewSettings.getRendererID(i);
            } catch (Exception e) {
                g_aiRendIDs[i] = 0;
            }
        }


        if (table != null) {
            if (g_oViewSettings.getFont() != null) {
                setFont();
            }
            //g_oTable.applyInitialSorting();
            updateGUI();
        }
    }

    public boolean isParenFrametVisible() {
        try {
            return (((InternalFrame) g_oViewSettings.getParent()).isShowing() ||
                    ((InternalFrame) g_oViewSettings.getParent()).isDetached() ||
                    (g_oViewSettings.getDesktopType() == Constants.FULL_QUOTE_TYPE));
        } catch (Exception e) {
            try {
                return ((JInternalFrame) g_oViewSettings.getParent()).isShowing();
            } catch (Exception e1) {
                return false;
            }
        }
    }


    /**
     * Sets the orientation to LTR ot nor
     */
    public void setDirectionLTR(boolean bLTR) {
        g_bLTR = bLTR;
        //if (!g_bLTR)
        //applyRTL();

    }

    public boolean isLTR() {
        return g_bLTR;
    }

    /**
     * Applies table column renderers for each column of the table,
     * and there column headers.
     */
    public void applyColumnSettings() {
    }

    public void applySettings() {
        setFont();
        updateGUI();
        try {
            ((CommonTableSettings) table.getSmartTable().getTableSettings()).init();
            ((CommonTableSettings) table.getSmartTable().getTableSettings()).setWorkspaceString(g_oViewSettings.getProperty(ViewConstants.CS_TABLE_SETTINGS));
            table.getSmartTable().setBodyGap(((CommonTableSettings) table.getSmartTable().getTableSettings()).getBodyGap());
//            table.getSmartTable().setGdidOn(((CommonTableSettings) table.getSmartTable().getTableSettings()).isGridOn());
            table.getSmartTable().setCustomThemeEnabled(getViewSettings().getProperty(ViewConstants.CS_CUSTOMIZED).equalsIgnoreCase("true"));
        } catch (Exception e) {

        }
        table.applyInitialSorting();
        //change start
        adjustColumns(table.getTable(), (g_oViewSettings.getColumns().or(g_oViewSettings.getFixedColumns())).and(g_oViewSettings.getHiddenColumns().not()));
//        adjustColumns(table.getTable(),
//                (g_oViewSettings.getColumns() | g_oViewSettings.getFixedColumns()) & ~g_oViewSettings.getHiddenColumns());
    }

    //change start
    public void adjustColumns(final JTable oTable, BigInteger lSettings) {
        //change start
        BigInteger lPos = lSettings;
//        long lPos = lSettings;// g_oViewSettings.getColumns();
        String[] asHeadings = g_oViewSettings.getColumnHeadings();

        TableColumn oColumn;
        int visibleCount = 0;

        try {
            for (int i = 0; i < asHeadings.length; i++) {
                oColumn = oTable.getColumn("" + i);

                //change start
                if ((lPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
//                if ((lPos & 1) == 0) {
                    oColumn.setMaxWidth(0);
                    oColumn.setMinWidth(0);
                    oColumn.setPreferredWidth(0);
                    oColumn.setWidth(0);
                    oColumn.setResizable(false);
                    //System.out.println("Hiding " + i);

                } else {
                    oColumn.setMaxWidth(2000);
                    oColumn.setMinWidth(20);
                    oColumn.setResizable(false);
                    visibleCount++;
                    //oColumn.setResizable(true);

                    if (g_oViewSettings.getColumnSettings() != null) {
                        try {
                            if (g_oViewSettings.getColumnSettings()[i][1] >= 5) { // if not almost hidden
                                oColumn.setPreferredWidth(g_oViewSettings.getColumnSettings()[i][1]);
                            } else {
                                oColumn.setPreferredWidth(100);
                            }
                            TableColumnModel oModel = oTable.getColumnModel();
                            int iViewIndex = oTable.convertColumnIndexToView(oColumn.getModelIndex());
                            oModel.moveColumn(iViewIndex, g_oViewSettings.getColumnSettings()[i][2]);
                        } catch (Exception e) {
                            oColumn.setPreferredWidth(100);
                        }


                    } else {
                        oColumn.setPreferredWidth(100);
                    }

                }
                //change start
                lPos = lPos.shiftRight(1);
//                lPos >>= 1;
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        try {
            if (visibleCount > 1) {
                for (int i = 0; i < asHeadings.length; i++) {
                    oColumn = oTable.getColumn("" + i);
                    if (oColumn.getWidth() > 0) {
                        oColumn.setResizable(true);
                    }
                }
            }
        } catch (Exception e) {

        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
//                    oTable.getTableHeader().updateUI();
                    oTable.updateUI();
                } catch (Exception ex) {
                }
            }
        });

    }

    public void repositionColumns() {
        TableColumn oColumn;
        //System.out.println("Repos");
        for (int i = 0; i < g_oViewSettings.getColumnHeadings().length; i++) {
            oColumn = table.getTable().getColumn("" + i);

            if (g_oViewSettings.getColumnSettings() != null) {
                repositionColumn(i, table.getTable(), oColumn);
            } else {
                oColumn.setPreferredWidth(100);
            }
        }
    }

    public void repositionColumn(int iCol, JTable oTable
            , TableColumn oColumn) {
        TableColumnModel oModel = oTable.getColumnModel();
        int iViewIndex = oTable.convertColumnIndexToView(oColumn.getModelIndex());
        //if (isLTR())
        oModel.moveColumn(iViewIndex, g_oViewSettings.getColumnSettings()[iCol][2]);
        /*else
            oModel.moveColumn(iViewIndex,g_oViewSettings.getColumnHeadings().length
                - g_oViewSettings.getColumnSettings()[iCol][2]-1);*/
    }

    public void saveColumnPositions() {
        try {
            if (g_oViewSettings.getColumnSettings() == null) {
                g_oViewSettings.restColumnSettings();
            }

            for (int i = 0; i < g_oViewSettings.getColumnHeadings().length; i++) {
                g_oViewSettings.getColumnSettings()[i][2] =
                        table.getTable().convertColumnIndexToView(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long transposePattern(long lPattern) {
        int lTranspose = 0;

        for (int i = 0; i < 64; i++) {
            if ((lPattern & 1) == 1) {
                lTranspose <<= 1;
                lTranspose |= 1;
            } else {
                lTranspose <<= 0;
            }
            lPattern >>>= 1;
        }
        return lTranspose;
    }

    public void updateGUI() {
        //change start
        adjustColumns(table.getTable(), (g_oViewSettings.getColumns().or(g_oViewSettings.getFixedColumns())).and(g_oViewSettings.getHiddenColumns().not()));
//        adjustColumns(table.getTable(),
//                (g_oViewSettings.getColumns() | g_oViewSettings.getFixedColumns()) & ~g_oViewSettings.getHiddenColumns());
        Font oFont = g_oViewSettings.getFont();

        if (oFont == null) {
            oFont = table.getTable().getFont();
        }

        if (oFont != null) {
            //if (!oFont.equals(table.getTable().getFont())){
            table.getTable().setFont(oFont);
            FontMetrics oMetrices = table.getFontMetrics(oFont);
            table.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + table.getSmartTable().getBodyGap());
            oMetrices = null;
            oFont = null;
            //}
        }
    }

    public int[] getRendIDs() {
        return g_aiRendIDs;
    }

    public String[] reverseOrder(String[] asValues) {
        String[] asNewValues = new String[asValues.length];
        int i = 0;

        for (i = 0; i < asValues.length; i++) {
            asNewValues[asValues.length - 1 - i] = asValues[i];
        }
        return asNewValues;
    }

    public TWDecimalFormat getDecimalFormat() {
        return SharedMethods.getDecimalFormat(decimalCount);
        /*switch(decimalCount){
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                return decimals[decimalCount];
            case -1:
            case -2:
            case -3:
            case -4:
            case -5:
            case -6:
            case -7:
            case -8:
            case -9:
            case -10:
                return decimalNoZero[decimalCount];
            default:
                return decimals[2];
        }*/
    }


    public byte getDecimalCount() {
        return decimalCount;
    }

    public void setDecimalCount(byte count) {
        decimalCount = count;
    }

    /* Table wrapper methods */
    public Point getLocation() {
        return new Point(0, 0);
    }

    public Dimension getSize() {
        return new Dimension(0, 0);
    }

    public boolean isWindowVisible() {
        return true;
    }

    public int getWindowStyle() {
        return 0;
    }

    public JTable getTable1() {
        return table.getTable();
    }

    public JTable getTable2() {
        return null;
    }

    public JTable getTabFoler(String id) {
        return null;
    }

    public int getZOrder() {
        return 0;
    }

    public void printTable() {
    }

    public boolean isTitleVisible() {
        return true;
    }

    public void setTitleVisible(boolean status) {
    }

    public void windowClosing() {

    }

    public Object getDetachedFrame() {
        return null;
    }

    public boolean isDetached() {
        return false;
    }

    /* -- */

    public Table getParentTable() {
        return table;
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[4];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));      //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("ROW_COLOR_1"), FIELD_BASIC_ROW1, Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("ROW_COLOR_2"), FIELD_BASIC_ROW2, Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2"), Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));

        return customizerRecords;
    }

    public void setInsertAnalysTable(Table oTable) {
        AnalysTechRenderer renderer = new AnalysTechRenderer(g_aiRendIDs);
        table = oTable;
        //table.getTable().setDefaultRenderer(Double.class, renderer);
        table.getTable().setDefaultRenderer(String.class, renderer);
        JCheckBox checkBox = new JCheckBox();
        checkBox.setHorizontalAlignment(JLabel.CENTER);
        DefaultCellEditor checkBoxEditor = new DefaultCellEditor(checkBox);
        table.getTable().getColumn("3").setCellEditor(checkBoxEditor);
        table.getTable().setDefaultRenderer(Object.class, renderer);
        table.getTable().setDefaultRenderer(Boolean.class, renderer);
//           table.getTable().setDefaultRenderer(Float.class, renderer); //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
//           table.getTable().setDefaultRenderer(Long.class, renderer);   //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
//           table.getTable().setDefaultRenderer(Byte.class, renderer);
        table.getTable().setShowGrid(false);
        table.getTable().setIntercellSpacing(new Dimension(0, 0));
        table.getTable().setCellSelectionEnabled(true);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        if (g_oViewSettings != null) {
            setFont();
            updateGUI();
        }
        renderer = null;
    }

}
