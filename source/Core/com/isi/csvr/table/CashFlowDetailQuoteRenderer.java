package com.isi.csvr.table;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.variationmap.VariationImage;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Aug 2, 2007
 * Time: 5:15:22 PM
 */
public class CashFlowDetailQuoteRenderer extends TWBasicTableRenderer implements TableCellRenderer {
    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();
    private static Color g_oUpFGColor;
    private static Color g_oDownFGColor;
    private static Color g_oUpBGColor;
    private static Color g_oDownBGColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oDownColor;
    private static Color g_oUpColor;
    private static Color g_oHeaderBG1Color;
    private static Color g_oHeaderBG2Color;
    private static Color g_oHeaderFGColor;
    private String exchange;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private Color headerFG;
    private Color headerBG;
    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private TWDecimalFormat oPChangeFormat;
    private double doubleValue;
    private long longValue;
    private long updateDirection;
    private long now;
    private VariationImage variationImage;
    private String g_sNA = Language.getString("NA");

    public CashFlowDetailQuoteRenderer() {
        reload();

        variationImage = new VariationImage();
        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oPChangeFormat = new TWDecimalFormat(" ###,##0.00 ");

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;

        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oUpFGColor = Color.black;
        g_oDownFGColor = Color.black;
        g_oUpBGColor = Color.white;
        g_oDownBGColor = Color.white;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.white;
        g_oBG2 = Color.white;
        g_oDownColor = Color.black;
        g_oUpColor = Color.black;
        g_oHeaderBG1Color = Color.white;
        g_oHeaderBG2Color = Color.white;
        g_oHeaderFGColor = Color.black;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oDownColor = g_oDownBGColor;
            g_oUpColor = g_oUpBGColor;
            g_oHeaderBG1Color = Theme.getColor("CASH_FLOW_DQ_HEADER_BGCOLOR1");
            g_oHeaderBG2Color = Theme.getColor("CASH_FLOW_DQ_HEADER_BGCOLOR2");
            g_oHeaderFGColor = Theme.getColor("CASH_FLOW_DQ_HEADER_FGCOLOR");
        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oDownColor = Color.red;
            g_oUpColor = Color.green;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void propertyChanged(int property) {
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {


        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
                if (row % 2 == 0) {
                    headerBG = g_oHeaderBG1Color;
                } else {
                    headerBG = g_oHeaderBG2Color;
                }
            } else if (row % 2 == 0) {
                foreground = sett.getRowColor1FG();
                background = sett.getRowColor1BG();
                headerBG = g_oHeaderBG1Color;
            } else {
                foreground = sett.getRowColor2FG();
                background = sett.getRowColor2BG();
                headerBG = g_oHeaderBG2Color;
            }
            headerFG = sett.getHeaderColorFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
                if (row % 2 == 0) {
                    headerBG = g_oHeaderBG1Color;
                } else {
                    headerBG = g_oHeaderBG2Color;
                }
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
                headerBG = g_oHeaderBG1Color;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
                headerBG = g_oHeaderBG2Color;
            }
            headerFG = g_oHeaderFGColor;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);
        variationImage.setHeight(table.getRowHeight());
        try {
            oPriceFormat = ((ExchangeFormatInterface) table.getModel()).getDecimalFormat();// = ((SmartTable)table).getDecimalFormat();
//            oChangeFormat = ((ExchangeFormatInterface)table.getModel()).getDecimalFormat();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int iRendID = getRenderingID(row, column);

        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setBackground(headerBG);
                    lblRenderer.setForeground(headerFG);
                    lblRenderer.setText(" " + value + " ");
                    lblRenderer.setBorder(null);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setBackground(headerBG);
                    lblRenderer.setForeground(headerFG);
                    lblRenderer.setBorder(null); //UIManager.getBorder("TableHeader.cellBorder"));
                    lblRenderer.setText(" " + value + " ");
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    longValue = ((LongTransferObject) (value)).getValue();
                    if (longValue >= 0)
                        lblRenderer.setText(oQuantityFormat.format(longValue));
                    else
                        lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                /*case 5: // CHANGE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(g_oUpColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(g_oDownColor);
                    break;
                case 6: // % CHANGE
                    //adPrice = (double[])value;
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(g_oUpColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(g_oDownColor);
                    break;
                case 7: // DATE
                    longValue = ((LongTransferObject) (value)).getValue();
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringOppositeAlign);
                    break;
                case 8: {// DATE TIME
                    longArray = (long[]) value;
                    if (longArray[0] == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        //Date dDateTime = new Date(lDateTime[0]);
                        date.setTime(longArray[0]);
                        lblRenderer.setText(g_oDateTimeFormat.format(date));
                    }
                    if (longArray[0] > longArray[1]) {
                        lblRenderer.setBackground(g_oUpBGColor);
                        lblRenderer.setForeground(g_oUpFGColor);
                    } else if (longArray[0] < longArray[1]) {
                        lblRenderer.setForeground(g_oDownFGColor);
                        lblRenderer.setBackground(g_oDownBGColor);
                    }
                    longArray[1] = longArray[0];
                    lblRenderer.setHorizontalAlignment(g_iStringOppositeAlign);
                    break;
                }*/
                case 'h': // Cash Map
                    doubleValue = ((VariationImageTransferObject) (value)).getValue();
                    variationImage.setType(VariationImage.TYPE_CASH_MAP);
                    variationImage.setExchange(exchange);
                    variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setHorizontalTextPosition(g_iCenterAlign);
                    lblRenderer.setForeground(Color.black);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        variationImage.setValue(0);
                        lblRenderer.setText("");
                    } else {
                        variationImage.setValue(doubleValue);
                        lblRenderer.setIcon(variationImage);
                        lblRenderer.setText(oPChangeFormat.format(doubleValue * 100));
                    }
                    break;
                case 'm': // Cash Flow Map
                    doubleValue = ((VariationImageTransferObject) (value)).getValue();
                    exchange = ((VariationImageTransferObject) (value)).getExchange();
                    variationImage.setType(VariationImage.TYPE_CASH_FLOW);
                    variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                    variationImage.setExchange(exchange);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == -1)) {
                        //lblRenderer.setBackground(Color.black);
                        variationImage.setValue(0);
                        lblRenderer.setText("");
                    } else if ((doubleValue == Double.MAX_VALUE) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setForeground(g_oUpFGColor);
                        lblRenderer.setText(g_sNA);
                    } else if ((doubleValue == 0)) {
                        lblRenderer.setForeground(g_oDownFGColor);
                        lblRenderer.setText(g_sNA);
                    } else {
                        variationImage.setValue(doubleValue);
                        lblRenderer.setIcon(variationImage);
                        lblRenderer.setText("");
                    }
                    lblRenderer.setBackground(Color.BLACK);
                    break;
                case 'P': // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    updateDirection = ((DoubleTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(g_oDownFGColor);
                            lblRenderer.setBackground(g_oDownBGColor);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'Q': // Quantity with coloured bg
                    longValue = ((LongTransferObject) (value)).getValue();
                    updateDirection = ((LongTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    lblRenderer.setText(oQuantityFormat.format(longValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(g_oDownFGColor);
                            lblRenderer.setBackground(g_oDownBGColor);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'p': // PRICE with no decimals
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    updateDirection = ((DoubleTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    lblRenderer.setText(oQuantityFormat.format(doubleValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(g_oDownFGColor);
                            lblRenderer.setBackground(g_oDownBGColor);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }

        return lblRenderer;
    }

    private int getRenderingID(int iRow, int iCol) {

        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 1;
                    case 2:
                        return 1;
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 4;
                    case 2:
                        return 4;
                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 4;
                    case 2:
                        return 4;
                }
            case 3:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 3;
                }
            case 4:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 2;
                    case 2:
                        return 3;
                }
            case 5:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 2;
                    case 2:
                        return 'h';
                }
        }
        return 0;
    }


}
