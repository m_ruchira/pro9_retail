// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.DetachableRootPane;
import com.isi.csvr.HeaderComboBoxRenderer;
import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.TableSorter;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.mist.MISTTableSettings;
import com.isi.csvr.mist.MultiSpanMISTCellTableUI;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.Constants.ThreadTypes;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.updator.TableUpdateManager;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.portfolio.DCValuationModel;
import com.isi.csvr.util.GroupableTableHeader;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.plaf.TableUI;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.table.*;
import javax.swing.text.Document;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.math.BigInteger;
import java.util.Arrays;

public class Table extends JPanel implements MouseListener, MouseMotionListener, Themeable, UpdateableTable {
//    public static final byte PORTFOLIO_VALUATION = 11;
//    public static final byte PORTFOLIO_TXN_HISTORY = 12;

    private SmartTable table;
    private CommonTable commonTableModel;
    //private TWThread g_oThread;
    private JScrollPane scrollPane;
    private long g_lLastGuiUpdate = -1;
    private CommonPopup commonPopup;
    private boolean g_bAutoResize = false;
    private boolean isActive = true;
    private boolean popupActive = true;
    private boolean isSortingDisabled = true;      // Used to hide the Sortable Header
    private int[] numFixedColumns;

    private TableSorter g_oSorter;
    private int g_iLastSortedCol = -1;
    private boolean g_bSortOrder = false;
    private SortButtonRenderer renderer;
    private byte windowType = 0;
    private long lastSortExitTime = 0;
    private Point mousePoint;

    private boolean isHeaderHide;           // Used to hide the Header
    private boolean isDQuoteType;           // Used to hide the Header

    private TableUpdateListener model = null;
    private boolean headerPopupActive = true;
    private boolean mouseDragging = false;

    private ThreadTypes tableID = ThreadTypes.DEFAULT;
    private int selcetedRow = 0;
    private boolean isFontResized = false;
    private boolean isOptionTable = false;

    public Table() {
        this(new int[]{-1});
    }

    public Table(int[] numFixedColumns) {
        this.numFixedColumns = numFixedColumns;
        commonPopup = new CommonPopup();
        commonPopup.AddCustomizeMenu(this);
        Theme.registerComponent(this);
        setLayout(new BorderLayout());
    }

    public Table(boolean enableCommonPopup) {
        this(enableCommonPopup, new int[]{-1});
    }

    public Table(boolean enableCommonPopup, int[] numFixedColumns) {
        this.numFixedColumns = numFixedColumns;
        setPopupActive(enableCommonPopup);
        commonPopup = new CommonPopup();
        commonPopup.AddCustomizeMenu(this);
        Theme.registerComponent(this);
    }

    /**
     * Sleep the thread for a give time period
     */
    /*private void Sleep(long lDelay) {
        try {
            g_oThread.sleepThread(commonTableModel.getViewSettings(), lDelay);
        } catch (Exception e) {
        }
    }*/
    public static void applyOrientation(Component c, ComponentOrientation o) {
        c.setComponentOrientation(o);

        if (c instanceof JMenu) {
            JMenu menu = (JMenu) c;
            int ncomponents = menu.getMenuComponentCount();
            for (int i = 0; i < ncomponents; ++i) {
                applyOrientation(menu.getMenuComponent(i), o);
            }
        } else if (c instanceof java.awt.Container) {
            java.awt.Container container = (java.awt.Container) c;
            int ncomponents = container.getComponentCount();
            for (int i = 0; i < ncomponents; ++i) {
                applyOrientation(container.getComponent(i), o);
            }
        }
    }

    public void addTableUpdateListener(TableUpdateListener model) {
        this.model = model;
    }

    public void setAutoResize(boolean bAutoResize) {
        if (table != null) {
            if (bAutoResize)
                table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
            else
                table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        }
        g_bAutoResize = bAutoResize;

    }

    public int getTableWidth() {
        if (scrollPane != null)
            return scrollPane.getViewport().getWidth();
        else
            return 0;
    }

    public void packFrame() {
        Dimension tableSize = getPreferredSize();
        ((JInternalFrame) commonTableModel.getViewSettings().getParent()).setSize(new Dimension((int) tableSize.getWidth() + 10, (int) tableSize.getHeight() + getTable().getTableHeader().getHeight() + 30));
    }

    /**
     * Set the view setting object for this table
     * <p/>
     * public void setViewSettings(ViewSetting oViewSettings)
     * {
     * g_oViewSettings = oViewSettings;
     * //applyColumnSettings();
     * }
     */

    public void hideHeader() {
        isHeaderHide = true;
    }

    public void hideCustomizer() {
        getPopup().hideCustomizer();
    }

    public void setDQTableType() {
        isDQuoteType = true;
    }

    public boolean isDQuoteType() {
        return isDQuoteType;
    }

    public void setCalcModel(CommonTable oModel) {
        TableModel oTableModel = oModel;
        commonTableModel = oModel;


        table = new SmartTable(oTableModel) {

            public Rectangle getCellRect(int row, int column, boolean includeSpacing) {
                Rectangle sRect = super.getCellRect(row, column, includeSpacing);
                if ((row < 0) || (column < 0) ||
                        (getRowCount() <= row) || (getColumnCount() <= column)) {
                    return sRect;
                } else {
                    if ((row == 0) || (row == 5) || (row == 11)) {
                        if ((column == 0)) {
                            if (Language.isLTR())
                                return combine(super.getCellRect(row, column, includeSpacing),
                                        super.getCellRect(row, column + 1, includeSpacing));
                            else
                                return combine(super.getCellRect(row, column + 1, includeSpacing),
                                        super.getCellRect(row, column, includeSpacing));
                        } else {
                            return new Rectangle((int) sRect.getX(), (int) sRect.getY(), 0, 0);
                        }
                    } else {
                        return sRect;
                    }
                }
            }

            public void columnSelectionChanged(ListSelectionEvent e) {
                repaint();
            }


            public TableUI getUI() {
                return new CalculatorTableUI(false);
            }

            private Rectangle combine(Rectangle sRect1, Rectangle sRect2) {
                Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                        (int) sRect1.getWidth() + (int) sRect2.getWidth(), (int) sRect1.getHeight());
                return sRect;
            }

            private Rectangle combine(Rectangle sRect1, Rectangle sRect2, Rectangle sRect3, Rectangle sRect4) {
                Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                        (int) sRect1.getWidth() + (int) sRect2.getWidth() + (int) sRect3.getWidth() + (int) sRect4.getWidth(),
                        (int) sRect1.getHeight());
                return sRect;
            }

            public void updateUI() {
                super.updateUI();
                setUI(new CalculatorTableUI(false));
            }
        };
        table.addMouseMotionListener(this);
        table.setUI(new CalculatorTableUI(false));
        table.addMouseListener(this);
        table.getTableHeader().addMouseListener(this);
        CommonTableSettings tableSett = new CommonTableSettings(Language.isLTR());
        tableSett.setWorkspaceString(oModel.getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS));
        table.setTableSettings(tableSett);
        table.setBodyGap(tableSett.getBodyGap());
        setColumnIdentifiers();
        if (!isHeaderHide)
            this.add(table.getTableHeader(), BorderLayout.NORTH);

        scrollPane = new JScrollPane(table) {
            public void setColumnHeaderView(Component view) {
            }
        };
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        this.add(scrollPane, BorderLayout.CENTER);

        updateGUI();
        TableUpdateManager.addTable(this);
//        g_oThread = new TWThread(this, commonTableModel.getViewSettings());
//        g_oThread.start();
    }

    public void setGroupableModel(CommonTable oModel) {
        TableModel oTableModel = (TableModel) oModel;
        commonTableModel = oModel;


        table = new SmartTable(oTableModel) {

            public void columnSelectionChanged(ListSelectionEvent e) {
                repaint();
            }


            public TableUI getUI() {
                return new MultiSpanCellTableUI(false);
            }

            public void updateUI() {
                super.updateUI();
                setUI(new MultiSpanCellTableUI(false));
            }


        };
        table.addMouseMotionListener(this);
        table.setUI(new MultiSpanCellTableUI(false));

        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.setCellSelectionEnabled(true);
        table.getTableHeader().setUpdateTableInRealTime(false); // should not update file dragging*/
        setColumnIdentifiers();
        if (g_bAutoResize)
            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        if (isHeaderHide) {
            scrollPane = new JScrollPane(table) {
                public void setColumnHeaderView(Component view) {
                }
            };
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        } else {
            scrollPane = new JScrollPane(table);
        }
        scrollPane.setMinimumSize(new Dimension(0, 0));

        this.add(scrollPane, BorderLayout.CENTER);

        table.addMouseListener(this);
        table.getTableHeader().addMouseListener(this);

        CommonTableSettings tableSett = new CommonTableSettings(Language.isLTR());
        tableSett.setWorkspaceString(oModel.getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS));
        table.setTableSettings(tableSett);
        table.setBodyGap(tableSett.getBodyGap());
        updateGUI();
        scrollPane.updateUI();
        TableUpdateManager.addTable(this);
//        g_oThread = new TWThread(this, commonTableModel.getViewSettings());
//        g_oThread.start();
    }

    public void setGroupableCashFlowModel(CommonTable oModel) {
        TableModel oTableModel = (TableModel) oModel;
        commonTableModel = oModel;

        table = new SmartTable(oTableModel) {

            public void columnSelectionChanged(ListSelectionEvent e) {
                repaint();
            }

            public TableUI getUI() {
                return new MultiSpanCellCashFlowTableUI(false);
            }

            public void updateUI() {
                super.updateUI();
                setUI(new MultiSpanCellCashFlowTableUI(false));
            }

        };
        table.addMouseMotionListener(this);
        table.setUI(new MultiSpanCellCashFlowTableUI(false));

        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.setCellSelectionEnabled(true);
        table.getTableHeader().setUpdateTableInRealTime(false); // should not update file dragging*/
        setColumnIdentifiers();
        if (g_bAutoResize)
            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        if (isHeaderHide) {
            scrollPane = new JScrollPane(table) {
                public void setColumnHeaderView(Component view) {
                }
            };
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        } else {
            scrollPane = new JScrollPane(table);
        }
        scrollPane.setMinimumSize(new Dimension(0, 0));

        this.add(scrollPane, BorderLayout.CENTER);

        table.addMouseListener(this);
        table.getTableHeader().addMouseListener(this);

        CommonTableSettings tableSett = new CommonTableSettings(Language.isLTR());
        tableSett.setWorkspaceString(oModel.getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS));
        table.setTableSettings(tableSett);
        table.setBodyGap(tableSett.getBodyGap());
        updateGUI();
        scrollPane.updateUI();
        TableUpdateManager.addTable(this);
//        g_oThread = new TWThread(this, commonTableModel.getViewSettings());
//        g_oThread.start();
    }

    public void setGroupablePortfolioModel(CommonTable oModel) {

        TableModel oTableModel = (TableModel) oModel;
        commonTableModel = oModel;

        table = new SmartTable(oTableModel) {

            public void columnSelectionChanged(ListSelectionEvent e) {
                repaint();
            }

            public TableUI getUI() {
                return new TradingPortFolioTableUI(false);
            }

            public void updateUI() {
                super.updateUI();
                setUI(new TradingPortFolioTableUI(false));
            }

            public int adjustColumnWidthsToFit(int minWidth) {
                int[] colWidths = new int[this.getColumnCount()];
                int model;
                String text;
                Document d;

                Arrays.fill(colWidths, minWidth);
                int spacing = (int) getIntercellSpacing().getWidth();


                JLabel comp = (JLabel) getCellRenderer(0, 0).getTableCellRendererComponent(this, getModel().getValueAt(0, 0), false, false, 0, 0);
                Insets insets = comp.getInsets();
                int gap = insets.left + insets.right;
                //insets = null;

                FontMetrics fMetrics = null;
                try {
                    //JLabel headerRenderer = getCo
                    //(JLabel)getColumnModel().getColumn(0).getHeaderRenderer();
                    fMetrics = getFontMetrics(getTableHeader().getFont());
                    gap += 15;
                    //(headerRenderer.getBorder().getBorderInsets(headerRenderer).left*2);

                    for (int column = 0; column < colWidths.length; column++) {
                        //model = this.convertColumnIndexToModel(column);

                        try {
                            if ((fMetrics.stringWidth(getColumnName(column)) + gap) > colWidths[column]) {
                                colWidths[column] = fMetrics.stringWidth(getColumnName(column)) + gap;
                            }
                        } catch (Exception ex) {
                        }
                    }
                    //headerRenderer = null;
                } catch (Exception ex) {
                    // this
                }

                fMetrics = getFontMetrics(getFont());
                gap = insets.left + insets.right + 2;

                //        String text;
                for (int row = 0; row < getRowCount(); row++) {
                    String rowType = (String) getModel().getValueAt(row, -12);
                    if (rowType.equals("combine")) {
                        try {

//                                if (getColumnModel().getColumn(model).getWidth() > 0) {
//                                    comp = (JLabel) getCellRenderer(row, 0).getTableCellRendererComponent(this,
//                                            getModel().getValueAt(row, model), false, false, row, 0);
//
//                                    View v = (View) comp.getClientProperty(BasicHTML.propertyKey);
//                                    if (v != null) {
//                                        d = v.getDocument();
//                                        text = d.getText(0, d.getLength()).trim();
//                                    } else {
//                                        text = comp.getText();
//                                    }
                            text = (String) getModel().getValueAt(row, 0);
                            int totalWidth = 0;
//                                    for (int column = 0; column < colWidths.length; column++) {
                            ViewSetting g_oViewSetting = ((DCValuationModel) getModel()).getViewSetting();
                            BigInteger lSelectedPos = (g_oViewSetting.getColumns().or(g_oViewSetting.getFixedColumns())).and(g_oViewSetting.getHiddenColumns().not());
                            BigInteger lFixedPos = g_oViewSetting.getFixedColumns();
                            BigInteger lHiddenPos = g_oViewSetting.getHiddenColumns();
                            String[] asColumns = g_oViewSetting.getColumnHeadings();
                            int firstcolumn = 0;
                            for (int i = 0; i < asColumns.length; i++) {
                                if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
                                {
                                    if ((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
                                    } else {
                                        if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
//                                                System.out.println("selected columns =="+ asColumns[i]);
//                                                System.out.println("selected columns ID =="+ i);
                                            totalWidth = totalWidth + colWidths[i];
//                                                this.convertColumnIndexToView(i);
//                                                System.out.println( "conversion clumn index to view: "+(this).convertColumnIndexToModel(i) +  " "+ this.convertColumnIndexToView(i));
                                            if ((firstcolumn == 0) || this.convertColumnIndexToView(i) < firstcolumn) {
                                                firstcolumn = i;
//                                                    System.out.println("first column selected ");
                                            }
                                        }
                                    }
                                }
                                lSelectedPos = lSelectedPos.shiftRight(1);
                                lFixedPos = lFixedPos.shiftRight(1);
                                lHiddenPos = lHiddenPos.shiftRight(1);
                            }
//                                        System.out.println("is column visible =="+((DCValuationModel)getModel()).getViewSetting().getColumns().and(new BigInteger(""+column)));
//                                        System.out.println("col settings 0=="+((DCValuationModel)getModel()).getViewSetting().getColumnSettings()[column][0]);
//                                        System.out.println("col settings 1=="+((DCValuationModel)getModel()).getViewSetting().getColumnSettings()[column][1]);
//                                        System.out.println("col settings 2=="+((DCValuationModel)getModel()).getViewSetting().getColumnSettings()[column][2]);
//                                        if(this.isColumnSelected(column))
//                                        totalWidth = totalWidth + colWidths[column];
//                                    }
//                                System.out.println("taoal widith:" + totalWidth);
//                                System.out.println("gap:" + gap );
//                                System.out.println("string widkth:" + fMetrics.stringWidth(text) );
//                                System.out.println("first column :" + firstcolumn );
                            model = this.convertColumnIndexToModel(0);
//                            this.convertColumnIndexToView()
//                             System.out.println("model:" + model );

//                                    int viewCol = this.convertColumnIndexToView(model);
                            if ((fMetrics.stringWidth(text) + gap) > totalWidth) {
                                colWidths[firstcolumn] = (fMetrics.stringWidth(text) + gap - totalWidth + colWidths[firstcolumn]);
//                                        colWidths[0] = (fMetrics.stringWidth(text) + gap - totalWidth + colWidths[0]);
                            }
//                                }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        for (int column = 0; column < colWidths.length; column++) {

                            try {
                                model = this.convertColumnIndexToModel(column);

                                if (getColumnModel().getColumn(column).getWidth() > 0) {
                                    comp = (JLabel) getCellRenderer(row, column).getTableCellRendererComponent(this,
                                            getModel().getValueAt(row, model), false, false, row, column);

                                    View v = (View) comp.getClientProperty(BasicHTML.propertyKey);
                                    if (v != null) {
                                        d = v.getDocument();
                                        text = d.getText(0, d.getLength()).trim();
                                    } else {
                                        text = comp.getText();
                                    }

                                    if ((fMetrics.stringWidth(text) + gap) > colWidths[column]) {
                                        colWidths[column] = fMetrics.stringWidth(text) + gap;
                                    }
                                }
                            } catch (Exception ex) {
                            }
                        }
                    }
                }

                for (int column = 0; column < colWidths.length; column++) {
                    if (this.getColumnModel().getColumn(column).getPreferredWidth() > 0)
                        this.getColumnModel().getColumn(column).setPreferredWidth(colWidths[column]);
                }

                updateUI();
                return (int) getTableHeader().getPreferredSize().getWidth();
            }
        };

        table.addMouseMotionListener(this);
        table.setUI(new TradingPortFolioTableUI(false));

        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.setCellSelectionEnabled(true);
        table.getTableHeader().setUpdateTableInRealTime(false); // should not update file dragging*/
        setColumnIdentifiers();
        if (g_bAutoResize)
            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        if (isHeaderHide) {
            scrollPane = new JScrollPane(table) {
                public void setColumnHeaderView(Component view) {
                }
            };
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        } else {
            scrollPane = new JScrollPane(table);
        }
        scrollPane.setMinimumSize(new Dimension(0, 0));

        this.add(scrollPane, BorderLayout.CENTER);

        table.addMouseListener(this);
        table.getTableHeader().addMouseListener(this);

        CommonTableSettings tableSett = new CommonTableSettings(Language.isLTR());
        tableSett.setWorkspaceString(oModel.getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS));
        renderer = new SortButtonRenderer(true);
        renderer.applyTheme();
        try {
            String asHeadings[] = oModel.getViewSettings().getColumnHeadings();
            TableColumnModel colmodel = table.getColumnModel();
            int n = asHeadings.length;

            for (int i = 0; i < n; i++) {
                colmodel.getColumn(i).setHeaderRenderer(renderer);
            }
            colmodel = null;
            asHeadings = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        table.setTableSettings(tableSett);
        table.setBodyGap(tableSett.getBodyGap());
        updateGUI();
        scrollPane.updateUI();
        TableUpdateManager.addTable(this);

    }

    public void setImportPortfolioEditableHeaderModel(CommonTable oModel, TWComboItem[] combolist) {

        TableModel oTableModel = oModel;
        commonTableModel = oModel;
        table = new SmartTable(oTableModel);
        table.addMouseMotionListener(this);
        String asHeadings[] = commonTableModel.getViewSettings().getColumnHeadings();
        TWComboBox box = new TWComboBox(combolist);
        HeaderComboBoxRenderer rendererN = new HeaderComboBoxRenderer(combolist);

        TableColumnModel columnModel = table.getColumnModel();
        table.setTableHeader(new TWEditableHeader(columnModel));

        TWEditableHeaderTableColumn col;
        for (int i = 1; i < table.getColumnModel().getColumnCount(); i++) {
            col = (TWEditableHeaderTableColumn) table.getColumnModel().getColumn(i);
            col.setHeaderValue(box.getItemAt(0));
            col.setHeaderRenderer(rendererN);
            col.setHeaderEditor(new DefaultCellEditor(box));

        }

        col = (TWEditableHeaderTableColumn) table.getColumnModel().getColumn(0);
        col.setHeaderValue(Language.getString("ROW_NUMBER"));
        renderer = new SortButtonRenderer(false);
        col.setHeaderRenderer(renderer);
        col.setHeaderEditor(null);


        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.getTableHeader().setUpdateTableInRealTime(false); // should not update file dragging*/
        setColumnIdentifiers();
        if (g_bAutoResize)
            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        if (isHeaderHide) {
            scrollPane = new JScrollPane(table) {
                public void setColumnHeaderView(Component view) {
                }
            };
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        } else {

            scrollPane = new JScrollPane(table);
            if (isOptionTable) {
                scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            }
        }
        scrollPane.setMinimumSize(new Dimension(0, 0));

        this.setLayout(new BorderLayout());
        this.add(scrollPane, BorderLayout.CENTER);

        table.addMouseListener(this);
        //  table.getTableHeader().addMouseListener(this);

        CommonTableSettings tableSett = new CommonTableSettings(Language.isLTR());
        tableSett.setWorkspaceString(oModel.getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS));
        if (windowType == ViewSettingsManager.PORTFOLIO_TXN_HISTORY) {
            tableSett.setTransactionHistoryTableType();
        }
        table.setTableSettings(tableSett);
        table.setBodyGap(tableSett.getBodyGap());

        try {
            if (oModel.getViewSettings().getProperty(ViewConstants.CS_CUSTOMIZED).equalsIgnoreCase("true")) {
                table.setCustomThemeEnabled(true);
            } else {
                table.setCustomThemeEnabled(false);
            }
        } catch (Exception e) {
            // if the setting is not saved
        }
        updateGUI();
        try {
            scrollPane.updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        TableUpdateManager.addTable(this);


    }

    public void setGroupablePortfolioValuationModel(CommonTable oModel) {
        TableModel oTableModel = oModel;
        commonTableModel = oModel;

        if (isSortingDisabled) {
            table = new SmartTable(oTableModel) {

                public void columnSelectionChanged(ListSelectionEvent e) {
                    repaint();
                }

                public TableUI getUI() {
                    return new MultiSpanCellComboTableUI();
                }

                public void updateUI() {
                    super.updateUI();
                    setUI(new MultiSpanCellComboTableUI());
                }


            };
            table.setUI(new TradingPortFolioTableUI(false));

        } else {

            g_oSorter = new TableSorter(oTableModel, windowType);

            table = new SmartTable(g_oSorter) {

                protected JTableHeader createDefaultTableHeader() {
                    return new GroupableTableHeader(columnModel);
                }

                public void columnSelectionChanged(ListSelectionEvent e) {
                    repaint();
                }


                public TableUI getUI() {
                    return new MultiSpanCellComboTableUI();
                }

                public void updateUI() {
                    super.updateUI();
                    setUI(new MultiSpanCellComboTableUI());
                }

            };

            g_oSorter.setTable(table);

        }

        table.addMouseMotionListener(this);
        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.setCellSelectionEnabled(true);
        table.getTableHeader().setUpdateTableInRealTime(false); // should not update file dragging*/
        setColumnIdentifiers();
        if (g_bAutoResize)
            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        if (isHeaderHide) {
            scrollPane = new JScrollPane(table) {
                public void setColumnHeaderView(Component view) {
                }
            };
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        } else {
            scrollPane = new JScrollPane(table);
        }
        scrollPane.setMinimumSize(new Dimension(0, 0));

        this.add(scrollPane, BorderLayout.CENTER);

        table.addMouseListener(this);
        table.getTableHeader().addMouseListener(this);

        CommonTableSettings tableSett = new CommonTableSettings(Language.isLTR());
        tableSett.setWorkspaceString(oModel.getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS));
        renderer = new SortButtonRenderer(true);
        renderer.applyTheme();
        try {
            String asHeadings[] = oModel.getViewSettings().getColumnHeadings();
            TableColumnModel colmodel = table.getColumnModel();
            int n = asHeadings.length;

            for (int i = 0; i < n; i++) {
                colmodel.getColumn(i).setHeaderRenderer(renderer);
            }
            colmodel = null;
            asHeadings = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        table.setTableSettings(tableSett);
        table.setBodyGap(tableSett.getBodyGap());
        updateGUI();
        scrollPane.updateUI();
        TableUpdateManager.addTable(this);


    }

    public void createGroupableTable() {
        scrollPane = new JScrollPane(table);
        this.add(scrollPane, BorderLayout.CENTER);
    }

    public void setOptionTableType() {
        isOptionTable = true;
    }

    public boolean isOptionTableType() {
        return isOptionTable;
    }

    public void setGroupableMISTModel(CommonTable oModel) {
        TableModel oTableModel = (TableModel) oModel;
        commonTableModel = oModel;

        table = new SmartTable(oTableModel) {
            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }

            public Rectangle getCellRect(int row, int column, boolean includeSpacing) {
                Rectangle sRect = super.getCellRect(row, column, includeSpacing);
                if ((row < 0) || (column < 0) ||
                        (getRowCount() <= row) || (getColumnCount() <= column)) {
                    return sRect;
                } else {
                    //System.out.println("Col " + column);
                    if ((row % 2) == 0) {
                        if ((column == 0)) {
                            // System.out.println("col 0");
                            if (Language.isLTR())
                                return combine(super.getCellRect(row, column, includeSpacing),
                                        super.getCellRect(row, column + 1, includeSpacing));
                            else
                                return combine(super.getCellRect(row, column + 1, includeSpacing),
                                        super.getCellRect(row, column, includeSpacing));
                            /*}else  if (column == 1)
                                return new Rectangle((int)sRect.getX(),(int)sRect.getY(),0,0);*/
                        } else if (column == 1) {
                            // System.out.println("col 1");
                            //return combine(super.getCellRect(row,column-1,includeSpacing),super.getCellRect(row,column,includeSpacing));
                            return new Rectangle((int) sRect.getX(), (int) sRect.getY(), 0, 0);
                        } else {
                            return sRect;
                        }
                    } else {
                        return sRect;
                    }
                }
            }

            public TableCellRenderer getCellRenderer(int row, int column) {
                if (column == 1)
                    return super.getCellRenderer(row, 0);
                else
                    return super.getCellRenderer(row, column);
            }

            public int columnAtPoint(Point point) {
                if ((super.rowAtPoint(point) % 2) == 0) {
                    if ((super.columnAtPoint(point) == 1))
                        return 0;
                    else
                        return super.columnAtPoint(point);
                } else {
                    return super.columnAtPoint(point);
                }
            }

            public void columnSelectionChanged(ListSelectionEvent e) {
                repaint();
            }

            public TableUI getUI() {
//                System.out.println("request ui >>>>>>>>>>>>>>>>>>>>");
                return new MultiSpanCellTableUI(false);
            }

            private Rectangle combine(Rectangle sRect1, Rectangle sRect2) {
                Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                        (int) sRect1.getWidth() + (int) sRect2.getWidth(),
                        (int) sRect1.getHeight());
                return sRect;
            }

            public void updateUI() {
                super.updateUI();
                setUI(new MultiSpanMISTCellTableUI(false));
            }
///
        };

        table.setTableSettings(new MISTTableSettings());
        table.setDefaultEditor(String.class, new DefaultCellEditor(new TWTextField()));
//        g_oTable.setDefaultEditor(String.class, new CompanyCellEditor(Client.getInstance().companySelector));
        table.setUI(new MultiSpanMISTCellTableUI(false));
        table.addMouseListener(this);
        table.addMouseMotionListener(this);
        table.getTableHeader().addMouseListener(this);

        setColumnIdentifiers();

        TableUpdateManager.addTable(this);
//        g_oThread = new TWThread(this, commonTableModel.getViewSettings());
//        g_oThread.start();
    }

    public void setGroupableHeaderModel(CommonTable oModel) {
        TableModel oTableModel = (TableModel) oModel;
        commonTableModel = oModel;

        if (!isSortingDisabled) {
            g_oSorter = new TableSorter(oTableModel, windowType);
            //table = new SmartTable(g_oSorter, numFixedColumns);
            table = new SmartTable(g_oSorter, numFixedColumns) {
                protected JTableHeader createDefaultTableHeader() {
                    return new GroupableTableHeader(columnModel);
                }

            };
            table.addMouseMotionListener(this);
            commonPopup.enableUnsort(true);
            g_oSorter.setTable(table);
        } else {
            table = new SmartTable(oTableModel) {
                protected JTableHeader createDefaultTableHeader() {
                    return new GroupableTableHeader(columnModel);
                }

            };
        }

        String asHeadings[] = commonTableModel.getViewSettings().getColumnHeadings();
        renderer = new SortButtonRenderer(true);
        renderer.applyTheme();

//        TableColumnModel colmodel = table.getColumnModel();
//        int n = asHeadings.length;
//
//        for (int i = 0; i < n; i++) {
//            colmodel.getColumn(i).setHeaderRenderer(renderer);
//        }
//        colmodel = null;
        asHeadings = null;

        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.getTableHeader().setUpdateTableInRealTime(false); // should not update file dragging*/
        setColumnIdentifiers();
        if (g_bAutoResize)
            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        if (isHeaderHide) {
            scrollPane = new JScrollPane(table) {
                public void setColumnHeaderView(Component view) {
                }
            };
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        } else {

            scrollPane = new JScrollPane(table);
            if (isOptionTable) {
                scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            }
        }
        scrollPane.setMinimumSize(new Dimension(0, 0));

        this.setLayout(new BorderLayout());
        this.add(scrollPane, BorderLayout.CENTER);

        table.addMouseListener(this);
        table.getTableHeader().addMouseListener(this);

        CommonTableSettings tableSett = new CommonTableSettings(Language.isLTR());
        tableSett.setWorkspaceString(oModel.getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS));
        if (windowType == ViewSettingsManager.PORTFOLIO_TXN_HISTORY) {
            tableSett.setTransactionHistoryTableType();
        }
        table.setTableSettings(tableSett);
        table.setBodyGap(tableSett.getBodyGap());

        try {
            if (oModel.getViewSettings().getProperty(ViewConstants.CS_CUSTOMIZED).equalsIgnoreCase("true")) {
                table.setCustomThemeEnabled(true);
            } else {
                table.setCustomThemeEnabled(false);
            }
        } catch (Exception e) {
            // if the setting is not saved
        }
        updateGUI();
        try {
            scrollPane.updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        TableUpdateManager.addTable(this);
//        g_oThread = new TWThread(this, commonTableModel.getViewSettings());
//        g_oThread.start();

    }

    public void setColumnIdentifiers() {
        TableColumn oColumn;

        String[] asHeadings = commonTableModel.getViewSettings().getColumnHeadings();
        for (int i = 0; i < table.getColumnCount(); i++) {
            oColumn = table.getColumn(asHeadings[i]);
            oColumn.setIdentifier("" + i);
        }
    }

    public boolean isSortable() {
        return (!isSortingDisabled);
    }

    /*
    * Added By Bandula
    */

    public Dimension getPreferredSize() {
        return table.getPreferredSize();
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    /**
     * Set the table modal for the class
     */
    public CommonTable getModel() {
        return commonTableModel;
    }

    public void setModel(CommonTable oModel) {
        TableModel oTableModel = oModel;
        commonTableModel = oModel;

        if (!isSortingDisabled) {
            g_oSorter = new TableSorter(oTableModel, windowType);
            table = new SmartTable(g_oSorter, numFixedColumns);
            table.addMouseMotionListener(this);
            commonPopup.enableUnsort(true);
            g_oSorter.setTable(table);

            String asHeadings[] = commonTableModel.getViewSettings().getColumnHeadings();
            renderer = new SortButtonRenderer(true);
            renderer.applyTheme();

            TableColumnModel colmodel = table.getColumnModel();
            int n = asHeadings.length;

            for (int i = 0; i < n; i++) {
                colmodel.getColumn(i).setHeaderRenderer(renderer);
            }
            colmodel = null;
            asHeadings = null;
        } else {
            /*table = new SmartTable(oTableModel, numFixedColumns);
            table.addMouseMotionListener(this);*/
            table = new SmartTable(oTableModel, numFixedColumns);
            table.addMouseMotionListener(this);

            String asHeadings[] = commonTableModel.getViewSettings().getColumnHeadings();
            renderer = new SortButtonRenderer(true);
            renderer.applyTheme();

            TableColumnModel colmodel = table.getColumnModel();
            int n = asHeadings.length;

            for (int i = 0; i < n; i++) {
                colmodel.getColumn(i).setHeaderRenderer(renderer);
            }
            colmodel = null;
            asHeadings = null;
        }

        table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        table.getTableHeader().setUpdateTableInRealTime(false); // should not update file dragging*/
        setColumnIdentifiers();
        if (g_bAutoResize)
            table.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
//            table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        else
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        if (isHeaderHide) {
            scrollPane = new JScrollPane(table) {
                public void setColumnHeaderView(Component view) {
                }
            };
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        } else {

            scrollPane = new JScrollPane(table);
            if (isOptionTable) {
                scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            }
        }
        scrollPane.setMinimumSize(new Dimension(0, 0));

        this.setLayout(new BorderLayout());
        this.add(scrollPane, BorderLayout.CENTER);

        table.addMouseListener(this);
        table.getTableHeader().addMouseListener(this);

        CommonTableSettings tableSett = new CommonTableSettings(Language.isLTR());
        tableSett.setWorkspaceString(oModel.getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS));
        if (windowType == ViewSettingsManager.PORTFOLIO_TXN_HISTORY) {
            tableSett.setTransactionHistoryTableType();
        }
        table.setTableSettings(tableSett);
        table.setBodyGap(tableSett.getBodyGap());

        try {
            if (oModel.getViewSettings().getProperty(ViewConstants.CS_CUSTOMIZED).equalsIgnoreCase("true")) {
                table.setCustomThemeEnabled(true);
            } else {
                table.setCustomThemeEnabled(false);
            }
        } catch (Exception e) {
            // if the setting is not saved
        }
        updateGUI();
        try {
            scrollPane.updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        TableUpdateManager.addTable(this);
//        g_oThread = new TWThread(this, commonTableModel.getViewSettings());
//        g_oThread.start();
    }

    /**
     * Return the JTable object of this calass
     */
    public JTable getTable() {
        return table;
    }

    public int getUnsortedRow(int row) {
        return g_oSorter.getUnsortedRowFor(row);
    }

    public int getSelectedRow() {
        return selcetedRow;
    }

    public void applyTheme() {
        updateGUI();
        SwingUtilities.updateComponentTreeUI(commonPopup);
    }

    public void updateGUI() {
        //if (Theme.getLasetUpdatedTime() != g_lLastGuiUpdate) { //Bug ID <#0009>
        if (table != null) {
            if (table.isCuatomThemeEnabled()) {
                table.getTableHeader().setBackground(((CommonTableSettings) table.getTableSettings()).getHeaderColorBG());
                table.getTableHeader().setForeground(((CommonTableSettings) table.getTableSettings()).getHeaderColorFG());
                // table.updateHeaderUI();
            } else {
                table.getTableHeader().setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
                table.getTableHeader().setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
//                table.updateHeaderUI();
            }
        } else {
//            System.out.println("++++++++++++++++++ null table = "+this.getTitle());
        }
        //  table.getTableHeader().setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")); //Bug ID <#0009>
        //  table.getTableHeader().setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR")); //Bug ID <#0009>
        try {
            renderer.applyTheme();
        } catch (Exception e) {
            // if renderer not initialized
        }
        g_lLastGuiUpdate = Theme.getLasetUpdatedTime();
        //}
    }

    public JComponent getNorthPanel() {
        try {
            return (JComponent) ((BorderLayout) this.getLayout()).getLayoutComponent(BorderLayout.NORTH);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setNorthPanel(JComponent component) {
        this.add(component, BorderLayout.NORTH);
    }

    public void runThread() {
        //while (isActive) {
        if (!isActive) return;

        if ((!mouseDragging) && (commonTableModel.isParenFrametVisible())) {
            if (!table.isEditing()) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        // notify model table is about to be updated
                        if (model != null) {
                            model.tableUpdating();
                        }
                        resort();
                        try {
                            table.updateUI();
                        } catch (Exception e) {

                        }
                    }
                });

            }
        }
        //Sleep(1000);
        // }
        //Theme.unRegisterComponent(this);
    }

    public String getTitle() {
        try {
            return ((JInternalFrame) commonTableModel.getViewSettings().getParent()).getTitle();
        } catch (Exception e) {
            return "Unknown";
        }
    }

    /*public void run() {
        while (isActive) {
            if ((!mouseDragging) && (commonTableModel.isParenFrametVisible())) {
                if (!table.isEditing()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            // notify model table is about to be updated
                            if (model != null) {
                                model.tableUpdating();
                            }

                            try {
                                table.updateUI();
                            } catch (Exception e) {
                            }
                        }
                    });

                }
            }
            Sleep(1000);
        }
        Theme.unRegisterComponent(this);
    }*/

    public void setThreadID(ThreadTypes tableID) {
        this.tableID = tableID;
    }

    public ThreadTypes getTableID() {
        return tableID;
    }

    public void killThread() {
        isActive = false;
//        TableUpdator.getSharedInstance().unregister(this);
        TableUpdateManager.removeTable(this);
    }

    public CommonPopup getPopup() {
        return commonPopup;
    }

    public void setSouthPanel(JComponent component) {
        this.add(component, BorderLayout.SOUTH);
    }

    public void sortData(final SmartTable tableView, final MouseEvent e) {
        if (!isSortable()) return;

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    if ((System.currentTimeMillis() - lastSortExitTime) > 500) { // keep a distence of 1 sec between sorts
                        lastSortExitTime = System.currentTimeMillis();
                        TableColumnModel columnModel = tableView.getColumnModel();
                        int viewportWidth = scrollPane.getViewport().getWidth();
                        int tableWidth = tableView.getWidth();

                        int viewColumn;
                        if (!Language.isLTR()) { // this checking is necessary due to a bug in java 1.4
                            if (tableWidth > viewportWidth) // take the maximum value
                                viewColumn = columnModel.getColumnIndexAtX(tableWidth - e.getX());
                            else
                                viewColumn = columnModel.getColumnIndexAtX(viewportWidth - e.getX());
                        } else {
                            viewColumn = columnModel.getColumnIndexAtX(e.getX());
                        }

                        int column = tableView.convertColumnIndexToModel(viewColumn);
                        if (column == g_iLastSortedCol)
                            g_bSortOrder ^= true;
//                            g_bSortOrder = !g_bSortOrder;
                        else
                            g_bSortOrder = true;
                        g_iLastSortedCol = column;
                        getModel().getViewSettings().setSortColumn(column);

                        g_oSorter.sortByColumn(column, g_bSortOrder);

                        /* Set the sort arrow for the header */
                        SortButtonRenderer oRend = (SortButtonRenderer) tableView.getColumnModel().getColumn(column).getHeaderRenderer();
                        int sortOrder = oRend.setSelectedColumn(column);
                        getModel().getViewSettings().setSortOrder(sortOrder);
                        table.getTableHeader().repaint();
                        oRend = null;
                        lastSortExitTime = System.currentTimeMillis();
                    }
                } catch (Exception e1) {
                    lastSortExitTime = System.currentTimeMillis();
                    ;
                    e1.printStackTrace();
                }
            }

            ;
        });
    }

    public void resort() {
        try {
            if (g_iLastSortedCol >= 0) {
                //  int column = table.convertColumnIndexToModel(g_iLastSortedCol);     //todo changed by udaya
                int column = g_iLastSortedCol;
                getModel().getViewSettings().setSortColumn(column);


                int[] selectedRows = table.getSelectedRows();
                int[] selectedCols = table.getSelectedColumns();
                g_oSorter.sortByColumn(column, g_bSortOrder);
                if (selectedRows.length > 0) {
                    table.setRowSelectionInterval(selectedRows[0], selectedRows[selectedRows.length - 1]);
                }
                if (selectedCols.length > 0) {
                    table.setColumnSelectionInterval(selectedCols[0], selectedCols[selectedCols.length - 1]);
                }

                /* Set the sort arrow for the header */
//                SortButtonRenderer oRend = (SortButtonRenderer) table.getColumnModel().getColumn(column).getHeaderRenderer();
//                int sortOrder = oRend.setSelectedColumn(column);
//                getModel().getViewSettings().setSortOrder(sortOrder);
//                table.getTableHeader().repaint();
//                oRend = null;
            }
        } catch (Exception e) {

        }
    }

    /* Events for the Mouse listener */

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof JTableHeader) {
//            table.setColumnSelectionAllowed(false);
            if ((!isDetached()) && isHeaderPopupActive() && SwingUtilities.isRightMouseButton(e)) {
                SwingUtilities.updateComponentTreeUI(commonPopup);
                GUISettings.showPopup(commonPopup, table.getTableHeader(), e.getX(), e.getY());
                mousePoint = e.getPoint();
            } else {
                sortData(table, e);
            }

        } else {
            if (SwingUtilities.isRightMouseButton(e)) {
                int rowAtPoint = table.rowAtPoint(new Point(e.getX(), e.getY()));
                int columnAtPoint = table.columnAtPoint(new Point(e.getX(), e.getY()));
//                int rowAtPoint = table.rowAtPoint(new Point(e.getX(), e.getY()));
                boolean alreadySelected = false;
                int[] selectedRows = table.getSelectedRows();
                for (int i = 0; i < selectedRows.length; i++) {
                    if (rowAtPoint == selectedRows[i]) {
                        alreadySelected = true;
                        break;
                    }
                }
                if (!alreadySelected) {
                    this.getTable().setRowSelectionInterval(rowAtPoint, rowAtPoint);
                }
                this.getTable().setColumnSelectionInterval(columnAtPoint, columnAtPoint);
                selectedRows = null;
                SwingUtilities.updateComponentTreeUI(commonPopup);
                if (!isDetached()) {
                    if (isPopupActive())
                        GUISettings.showPopup(commonPopup, table, e.getX(), e.getY());
                }
            }
        }
    }

    public boolean isDetached() {
        try {
            DetachableRootPane rootPane = (DetachableRootPane) (SwingUtilities.getAncestorOfClass(DetachableRootPane.class, table));
            if (rootPane != null) {
                return rootPane.isDetached();
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
        mouseDragging = false;
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public boolean isPopupActive() {
        return popupActive;
    }

    public void setPopupActive(boolean newPopupActive) {
        popupActive = newPopupActive;
    }

    public boolean isUseSameFontForHeader() {
        return table.isUseSameFontForHeader();
    }

    public void setUseSameFontForHeader(boolean newUseSameFontForHeader) {
        table.setUseSameFontForHeader(newUseSameFontForHeader);
    }

    public void unsort() {
        try {
            g_oSorter.unsort();
            SortButtonRenderer oRend = (SortButtonRenderer) table.getColumnModel().getColumn(getModel().getViewSettings().getSortColumn()).getHeaderRenderer();
            getModel().getViewSettings().setSortColumn(-1);
            g_iLastSortedCol = -1;
            oRend.unsort();
            oRend = null;
            table.getTableHeader().updateUI();
            table.updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applyInitialSorting() {

        if (isSortingDisabled) return;

        int sortCol = commonTableModel.getViewSettings().getSortColumn();

        if (sortCol >= 0) {
            if (commonTableModel.getViewSettings().getSortOrder() == SortButtonRenderer.DOWN) {
                g_bSortOrder = true;
            } else
                g_bSortOrder = false;
            g_oSorter.sortByColumn(commonTableModel.getViewSettings().getSortColumn(), g_bSortOrder);
            g_iLastSortedCol = commonTableModel.getViewSettings().getSortColumn();
            int viewcol = getTable().convertColumnIndexToView(sortCol);
            SortButtonRenderer oRend = (SortButtonRenderer) getTable().getColumnModel().getColumn(sortCol).getHeaderRenderer();
            oRend.setSelectedColumn(sortCol, commonTableModel.getViewSettings().getSortOrder());
            oRend = null;
        }
        getTable().getTableHeader().updateUI();
    }

    private void hideColumn() {
        TableColumnModel columnModel = table.getColumnModel();
        int viewportWidth = scrollPane.getViewport().getWidth();
        int tableWidth = table.getWidth();

        int viewColumn;
        if (!Language.isLTR()) { // this checking is necessary due to a bug in java 1.4
            if (tableWidth > viewportWidth) // take the maximum value
                viewColumn = columnModel.getColumnIndexAtX(tableWidth - (int) mousePoint.getX());
            else
                viewColumn = columnModel.getColumnIndexAtX(viewportWidth - (int) mousePoint.getX());
        } else {
            viewColumn = columnModel.getColumnIndexAtX((int) mousePoint.getX());
        }
        //change start
        int columnIndex = table.convertColumnIndexToModel(viewColumn);
        TableColumn column = table.getColumn("" + columnIndex);

        column.setMaxWidth(0);
        column.setMinWidth(0);
        column.setPreferredWidth(0);
        column.setWidth(0);
        column.setResizable(false);
        column = null;

        //change start

//        long columnSettings = commonTableModel.getViewSettings().getColumns();
//        long columnFilter = 1 << columnIndex;
//        columnFilter ^= 0xFFFFFFFFFFFFFFFFL;
//        columnSettings &= columnFilter;
//        commonTableModel.getViewSettings().setColumns(columnSettings);

        BigInteger columnSettings = commonTableModel.getViewSettings().getColumns();
        BigInteger columnFilter = BigInteger.valueOf(1).shiftLeft(columnIndex);
        columnFilter = columnFilter.xor(Settings.MAX_BIGINT_VALUE);
        columnSettings = columnSettings.and(columnFilter);
        commonTableModel.getViewSettings().setColumns(columnSettings);
        //change end

        table.repaint();
    }

    private void hidePreperedColumn(int Column) {

        TableColumnModel columnModel = table.getColumnModel();
        int viewportWidth = scrollPane.getViewport().getWidth();
        int tableWidth = table.getWidth();

        int viewColumn;
        if (!Language.isLTR()) { // this checking is necessary due to a bug in java 1.4
            if (tableWidth > viewportWidth) // take the maximum value
                viewColumn = columnModel.getColumnIndexAtX(tableWidth - (int) mousePoint.getX());
            else
                viewColumn = columnModel.getColumnIndexAtX(viewportWidth - (int) mousePoint.getX());
        } else {
            viewColumn = columnModel.getColumnIndexAtX((int) mousePoint.getX());
        }
        //change start
        int columnIndex = table.convertColumnIndexToModel(viewColumn);
        TableColumn column = table.getColumn("" + columnIndex);

        column.setMaxWidth(0);
        column.setMinWidth(0);
        column.setPreferredWidth(0);
        column.setWidth(0);
        column.setResizable(false);
        column = null;

        //change start

//        long columnSettings = commonTableModel.getViewSettings().getColumns()h;
//        long columnFilter = 1 << columnIndex;
//        columnFilter ^= 0xFFFFFFFFFFFFFFFFL;
//        columnSettings &= columnFilter;
//        commonTableModel.getViewSettings().setColumns(columnSettings);

        BigInteger columnSettings = commonTableModel.getViewSettings().getColumns();
        BigInteger columnFilter = BigInteger.valueOf(1).shiftLeft(columnIndex);
        columnFilter = columnFilter.xor(Settings.MAX_BIGINT_VALUE);
        columnSettings = columnSettings.and(columnFilter);
        commonTableModel.getViewSettings().setColumns(columnSettings);
        //change end

        table.repaint();
    }

    public byte getWindowType() {
        return windowType;
    }

    public void setWindowType(byte type) {
        windowType = type;
    }

    public void setSortingEnabled() {
        isSortingDisabled = false;
    }

    public boolean isSortingEnabled() {
        return !isSortingDisabled; //isSortingEnabled;
    }

    // Deprecated Methods
    public void setSortingDisabled() {
        isSortingDisabled = true;
    }

    // Deprecated Methods
    public boolean isSortingDisabled() {
        return isSortingDisabled;
    }

    public boolean isHeaderPopupActive() {
        return headerPopupActive;
    }

    public void setHeaderPopupActive(boolean headerPopupActive) {
        this.headerPopupActive = headerPopupActive;
    }

    /**
     * Invoked when a mouse button is pressed on a component and then
     * dragged.  <code>MOUSE_DRAGGED</code> events will continue to be
     * delivered to the component where the drag originated until the
     * mouse button is released (regardless of whether the mouse position
     * is within the bounds of the component).
     * <p/>
     * Due to platform-dependent Drag&Drop implementations,
     * <code>MOUSE_DRAGGED</code> events may not be delivered during a native
     * Drag&Drop operation.
     */
    public void mouseDragged(MouseEvent e) {
        mouseDragging = true;
    }

    /**
     * Invoked when the mouse cursor has been moved onto a component
     * but no buttons have been pushed.
     */
    public void mouseMoved(MouseEvent e) {

    }

    public SmartTable getSmartTable() {
        return table;
    }

    public void saveColumnPositions(ViewSetting viewSettings) {
        try {
            if (viewSettings.getColumnSettings() == null) {
                viewSettings.restColumnSettings();
            }
            for (int i = 0; i < viewSettings.getColumnHeadings().length; i++) {
                viewSettings.getColumnSettings()[i][2] =
                        table.convertColumnIndexToView(i);
                TableColumn oColumn = getTable().getColumn("" + i);
                viewSettings.getColumnSettings()[i][1] = oColumn.getWidth();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isFontResized() {
        return isFontResized;
    }

    public void setFontResized(boolean fontResized) {
        isFontResized = fontResized;
    }

    public void setG_bSortOrder(boolean g_bSortOrder) {
        this.g_bSortOrder = g_bSortOrder;
    }

    public int getG_iLastSortedCol() {
        return g_iLastSortedCol;
    }

    public void setG_iLastSortedCol(int g_iLastSortedCol) {
        this.g_iLastSortedCol = g_iLastSortedCol;
    }
}

