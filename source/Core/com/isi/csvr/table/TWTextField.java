package com.isi.csvr.table;

import javax.swing.*;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TWTextField extends JTextField implements ComboBoxEditor {

    public TWTextField() {
    }

    public TWTextField(Document doc, String text, int columns) {
        super(doc, text, columns);
    }

    public TWTextField(int columns) {
        super(columns);
    }

    public TWTextField(String text, int columns) {
        super(text, columns);
    }

    public void addActionListener(ActionListener l) {
        super.addActionListener(l);
    }

    public void addKeyListener(KeyListener k) {
        super.addKeyListener(k);
    }

    public void removeActionListener(ActionListener l) {
        super.removeActionListener(l);
    }

    public void selectAll() {
        super.selectAll();
    }

    public Object getItem() {
        return getText().trim();
    }

    public void setItem(Object anObject) {
        if (anObject != null)
            setText(anObject.toString());
        else
            setText("");
    }

    public Component getEditorComponent() {
        return this;
    }
}