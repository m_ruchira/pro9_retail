// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.BidAsk;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Stock;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


public class MarketDepthModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private String g_sSymbol;
    private Stock g_oStock;

    /**
     * Constructor
     */
    public MarketDepthModel(String sSymbol) {
        g_sSymbol = sSymbol;
        g_oStock = DataStore.getSharedInstance().getStockObject(sSymbol);
        //init();

    }

    /*private void init()
    {
         = DataStore.
    }*/

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        return Constants.MARKET_DEPTH_UPPER_LIMIT;
    }

    public Object getValueAt(int iRow, int iCol) {
        // Symbol,Description,Index,Today's initial,Net change,
        // % Change,Volume,Turnover,# Trades, # Ups,# Downs,#No Change,
        // # No Trade,52 Wk High,52 Wk High Date,52 Wk Low,52 Wk Low Date

        //--BidAsk oBid = g_oStock.getBid(iRow+1);
        //--BidAsk oAsk = g_oStock.getAsk(iRow+1);
        BidAsk oBid = null;//g_oStock.getBid(iRow);
        BidAsk oAsk = null;//g_oStock.getAsk(iRow);
        //AssetClass oAssetClass = AssetStore.getAssetClassObject(sID);
        //if (isLTR())
        //{
        //System.out.println("*** Reading LTR " + iCol);
        switch (iCol) {
            case 0:
                return "" + (iRow + 1);
            case 1:
                return "" + oBid.getPrice();
            case 2:
                return "" + oBid.getQuantity();
                /*case 3:
                    return "" + oBid.getTransactionTime();
                case 4:
                    return "" + oBid.getSession();
                case 5:
                    return "" + oBid.getOrderCondition();
                case 6:
                    return "" + oBid.getTick();
                case 7:
                    return "" + oBid.getBroker();
                case 8:
                    return "" + oAsk.getPrice()[0];
                case 9:
                    return "" + oAsk.getQuantity();
                case 10:
                    return "" + oAsk.getTransactionTime();
                case 11:
                    return "" + oAsk.getSession();
                case 12:
                    return "" + oAsk.getOrderCondition();
                case 13:
                    return "" + oAsk.getTick();
                case 14:
                    return "" + oAsk.getBroker();*/
            default:
                return "0";

        }
        /*}
        else
        {
            switch (iCol)
            {
                case 14:
                    return "" + (iRow + 1);
                case 13:
                    return "" + oBid.getPrice()[0];
                case 12:
                    return "" + oBid.getQuantity();
                case 11:
                    return "" + oBid.getTransactionTime();
                case 10:
                    return "" + oBid.getSession();
                case 9:
                    return "" + oBid.getOrderCondition();
                case 8:
                    return "" + oBid.getTick();
                case 7:
                    return "" + oBid.getBroker();

                case 6:
                    return "" + oAsk.getPrice()[0];
                case 5:
                    return "" + oAsk.getQuantity();
                case 4:
                    return "" + oAsk.getTransactionTime();
                case 3:
                    return "" + oAsk.getSession();
                case 2:
                    return "" + oAsk.getOrderCondition();
                case 1:
                    return "" + oAsk.getTick();
                case 0:
                    return "" + oAsk.getBroker();
				default:
                	return "0";
            }
        }*/
    }

    public String getColumnName(int iCol) {
        //if (isLTR())
        //{
        return super.getViewSettings().getColumnHeadings()[iCol];
        //}
        //else
        //{
        //	return super.getViewSettings().getColumnHeadings()[super.getViewSettings().getColumnHeadings().length - 1 - iCol ];
        //}
    }

    public Class getColumnClass(int iCol) {
        //return getValueAt(0, iCol).getClass();
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */


}

