package com.isi.csvr.table;

import com.isi.csvr.shared.Language;

import javax.swing.*;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableCellRenderer;
import java.awt.*;


/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Dec 19, 2007
 * Time: 9:53:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class TradingPortFolioTableUI extends BasicTableUI {

    public TradingPortFolioTableUI(boolean rtl) {
        super();
    }


    public void paint(Graphics g, JComponent c) {


        Rectangle oldClipBounds = g.getClipBounds();
        Rectangle clipBounds = new Rectangle(oldClipBounds);
        int tableWidth = table.getColumnModel().getTotalColumnWidth();
        clipBounds.width = Math.min(clipBounds.width, tableWidth);
        g.setClip(clipBounds);
        int firstIndex = table.rowAtPoint(new Point(0, clipBounds.y));
        int lastIndex = table.getRowCount() - 1;
        Rectangle rowRect = new Rectangle(0, 0,
                tableWidth, table.getRowHeight() + table.getRowMargin());
        rowRect.y = firstIndex * rowRect.height;

        for (int index = firstIndex; index <= lastIndex; index++) {
            if (rowRect.intersects(clipBounds)) {
                paintRow(g, index);
            }
            rowRect.y += rowRect.height;
        }
        g.setClip(oldClipBounds);
    }


    private void paintRow(Graphics g, int row) {

        //  System.out.println("______________________sathPaintRow");
        Rectangle rect = g.getClipBounds();
        // boolean drawn  = false;
        int numColumns = table.getColumnCount();
        String isCombine = (String) table.getModel().getValueAt(row, -12);
        //System.out.println("sathexcode : row  " +row+ ":"+ isCombine);
        Rectangle firstCell = null;
        for (int column = 0; column < numColumns; column++) {
            Rectangle cellRect = table.getCellRect(row, column, true);

//            if ((row == 1)|| (row == 2) || (row == 3)) {
//                if (Language.isLTR())
//                    cellRect = combine(table.getCellRect(row, 1, true), table.getCellRect(row, 2, true));
//                else
//                    cellRect = combine(table.getCellRect(row, 2, true), table.getCellRect(row, 1, true));
//            }


            //if ((row == 0) || (row == 1) || (row == 2)  ) {
            if (isCombine.equals("combine")) {// || (row == 1)){

                if (Language.isLTR()) {
                    cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
                    //for (int i = 2 ; i < numColumns;i++ ){
                    for (int i = 2; i < numColumns; i++) {
                        cellRect = combine(cellRect, table.getCellRect(row, i, true));
                    }
                    // cellRect = combine(cellRect,table.getCellRect(row, 3, true));


                } else {

                    cellRect = combine(table.getCellRect(row, numColumns - 1, true), table.getCellRect(row, numColumns - 2, true));
                    //cellRect = combine(table.getCellRect(row, numColumns - 1, true), table.getCellRect(row, numColumns - 2, true));
                    for (int i = numColumns - 3; i >= 0; i--) {
                        cellRect = combine(cellRect, table.getCellRect(row, i, true));
                    }

                }

                //} else {
                //  cellRect = table.getCellRect(row, column, true);
                //}
            }

            //combine all the cells
            //  if  (isCombine.equals("combine")){
//          if (row == 1){
//               if (column == 0){
//                   firstCell = table.getCellRect(row, 0,true);
//               }
//            cellRect =  combine(firstCell,table.getCellRect(row,column,true) );
//            firstCell = cellRect;
//
//          }

//              if  (isCombine.equals("combine")){
//                //if ((row == 0) || (row == 1)){
////                if ((row == 0) ){//|| (row == 1)){
//                if ((column == 0) || (column == 1)) {
//                    if (Language.isLTR())
//                        cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
//                    else
//                        cellRect = combine(table.getCellRect(row, 1, true), table.getCellRect(row, 0, true));
//                } else {
//                    cellRect = table.getCellRect(row, column, true);
//                }
//            }


            if (cellRect.intersects(rect)) {
                //drawn = true;
                paintCell(g, cellRect, row, column);
            } //else {
            //   if (drawn) break;
            //}

        }
    }

    private void paintCell(Graphics g, Rectangle cellRect, int row, int column) {
        int spacingHeight = table.getRowMargin();
        int spacingWidth = table.getColumnModel().getColumnMargin();

        try {
            cellRect.setBounds(cellRect.x + spacingWidth / 2,
                    cellRect.y + spacingHeight / 2,
                    cellRect.width - spacingWidth, cellRect.height - spacingHeight);

            if (table.isEditing() && table.getEditingRow() == row &&
                    table.getEditingColumn() == column) {
                Component component = table.getEditorComponent();
                component.setBounds(cellRect);
                component.validate();
            } else {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component component = table.prepareRenderer(renderer, row, column);

                if (component.getParent() == null) {
                    rendererPane.add(component);
                }
                rendererPane.paintComponent(g, component, table, cellRect.x, cellRect.y,
                        cellRect.width, cellRect.height, true);
                if (table.getShowHorizontalLines()) {
                    g.setColor(table.getGridColor());
                    g.drawLine(cellRect.x, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                    if (table.getRowCount() == row + 1) {
                        g.drawLine(cellRect.x, cellRect.y + cellRect.height, cellRect.x + cellRect.width, cellRect.y + cellRect.height);
                    }
                }
                if (table.getShowVerticalLines()) {
                    g.setColor(table.getGridColor());
                    g.drawLine(cellRect.x, cellRect.y, cellRect.x, cellRect.y + cellRect.height);
                    if (table.getColumnCount() == column + 1) {
                        g.drawLine(cellRect.x + cellRect.width, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                    }
                }
            }
        } catch (Exception e) {

        }
    }


    private Rectangle combine(Rectangle sRect1, Rectangle sRect2) {
        Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                (int) sRect1.getWidth() + (int) sRect2.getWidth(), (int) sRect1.getHeight());
        return sRect;
    }

}
