package com.isi.csvr.table;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 10, 2008
 * Time: 8:34:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class TWEditableHeaderTableColumn extends TableColumn {

    protected TableCellEditor headerEditor;

    protected boolean isHeaderEditable;

    public TWEditableHeaderTableColumn() {
        setHeaderEditor(createDefaultHeaderEditor());
        isHeaderEditable = true;
    }

    public TableCellEditor getHeaderEditor() {
        return headerEditor;
    }

    public void setHeaderEditor(TableCellEditor headerEditor) {
        this.headerEditor = headerEditor;
    }

    public boolean isHeaderEditable() {
        return isHeaderEditable;
    }

    public void setHeaderEditable(boolean isEditable) {
        isHeaderEditable = isEditable;
    }

    public void copyValues(TableColumn base) {
        modelIndex = base.getModelIndex();
        identifier = base.getIdentifier();
        width = base.getWidth();
        minWidth = base.getMinWidth();
        setPreferredWidth(base.getPreferredWidth());
        maxWidth = base.getMaxWidth();
        headerRenderer = base.getHeaderRenderer();
        headerValue = base.getHeaderValue();
        cellRenderer = base.getCellRenderer();
        cellEditor = base.getCellEditor();
        isResizable = base.getResizable();
    }

    protected TableCellEditor createDefaultHeaderEditor() {
        return new DefaultCellEditor(new JTextField());
    }

}
