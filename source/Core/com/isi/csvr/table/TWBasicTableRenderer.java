package com.isi.csvr.table;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: May 14, 2008
 * Time: 9:40:54 AM
 */
public abstract class TWBasicTableRenderer implements TWTableRenderer, TableCellRenderer {

    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();
    protected static Border cellBorder;
    protected static Border themeCellBorder;

    protected static void reloadRenderer() {
        Color gridColor = Theme.getOptionalColor("BOARD_TABLE_GRID_COLOR");
        int gridStyle;
        if (gridColor != null) {
            gridStyle = Theme.getInt("BOARD_TABLE_GRID_STYLE", TableThemeSettings.GRID_NONE);
        } else {
            gridStyle = TableThemeSettings.GRID_NONE;
        }

        switch (gridStyle) {
            case TableThemeSettings.GRID_NONE:
                themeCellBorder = null;
                break;
            case TableThemeSettings.GRID_HORIZONTAL:
                themeCellBorder = BorderFactory.createMatteBorder(0, 0, 1, 0, gridColor);
                break;
            case TableThemeSettings.GRID_VERTICAL:
                if (Language.isLTR()) {
                    themeCellBorder = BorderFactory.createMatteBorder(0, 0, 0, 1, gridColor);
                } else {
                    themeCellBorder = BorderFactory.createMatteBorder(0, 1, 0, 0, gridColor);
                }
                break;
            case TableThemeSettings.GRID_BOTH:
                if (Language.isLTR()) {
                    themeCellBorder = BorderFactory.createMatteBorder(0, 0, 1, 1, gridColor);
                } else {
                    themeCellBorder = BorderFactory.createMatteBorder(0, 1, 1, 0, gridColor);
                }
                break;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();

        Component renderer = DEFAULT_RENDERER.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        try {
            if (isCustomThemeEnabled) {
                cellBorder = ((CommonTableSettings) ((SmartTable) table).getTableSettings()).getCellBorder();
            } else {
                cellBorder = themeCellBorder;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((JLabel) renderer).setBorder(cellBorder);

        return renderer;
    }
}
