package com.isi.csvr.table;

import javax.swing.*;

public class CombinedCell extends JPanel {

    public CombinedCell() {
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    }

    public void setData(String[] data) {
        add(new JLabel(data[0]));
        add(new JLabel(data[1]));
        add(new JLabel(data[2]));
    }
}