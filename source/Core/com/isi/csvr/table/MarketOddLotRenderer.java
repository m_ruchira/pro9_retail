package com.isi.csvr.table;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.marketdepth.OddLot;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 24, 2008
 * Time: 12:12:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarketOddLotRenderer extends TWBasicTableRenderer {

    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1_Bid;
    private static Color g_oBG1_Bid;
    private static Color g_oFG2_Bid;
    private static Color g_oBG2_Bid;
    private static Color g_oFG1_Ask;
    private static Color g_oBG1_Ask;
    private static Color g_oFG2_Ask;
    private static Color g_oBG2_Ask;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private String g_sNA = "NA";

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1_Bid = Color.black;
        g_oBG1_Bid = Color.white;
        g_oFG2_Bid = Color.black;
        g_oBG2_Bid = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("DEPTH_BY_ORDER_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("DEPTH_BY_ORDER_SELECTED_BGCOLOR");
            g_oFG1_Ask = Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR1");
            g_oBG1_Ask = Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR1");
            g_oFG2_Ask = Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR2");
            g_oBG2_Ask = Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR2");

            g_oFG1_Bid = Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR1");
            g_oBG1_Bid = Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR1");
            g_oFG2_Bid = Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR2");
            g_oBG2_Bid = Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR2");
            //}
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1_Ask = Color.white;
            g_oBG1_Ask = Color.black;
            g_oFG2_Ask = Color.white;
            g_oBG2_Ask = Color.black;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {

        g_asRendIDs = asRendIDs;
        reload();

        g_iStringAlign = JLabel.LEADING;
        g_iNumberAlign = JLabel.RIGHT;
        oQuantityFormat = new TWDecimalFormat(" ###,##0  ");


    }

    public void propertyChanged(int property) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        Color foreground = null, background = null;
        oPriceFormat = (((ExchangeFormatInterface) table.getModel()).getDecimalFormat());

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getDepthOrderSelectedColorFG();
                background = sett.getDepthOrderSelectedColorBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getDepthOrderAskColor1FG();
                    background = sett.getDepthOrderAskColor1BG();
                } else {
                    foreground = sett.getDepthOrderAskColor2FG();
                    background = sett.getDepthOrderAskColor2BG();
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            }
//            else if (row % 2 == 0) {
//                if(Byte.parseByte((String)table.getModel().getValueAt(row,-1))== OddLot.BID) {
//                foreground = g_oFG1_Bid;
//                background = g_oBG1_Bid;
//                }
//                else{
//                background = g_oBG1_Ask;
//                foreground = g_oFG1_Ask;
//                }
//            } 
            else {

                if (Byte.parseByte((String) table.getModel().getValueAt(row, -1)) == OddLot.BID) {
                    foreground = g_oFG2_Bid;
                    background = g_oBG2_Bid;
                } else {
                    background = g_oBG2_Ask;
                    foreground = g_oFG2_Ask;
                }
            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        try {
            oPriceFormat = ((ExchangeFormatInterface) table.getModel()).getDecimalFormat();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int iRendID = 0;
        iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                case 1: // SYMBOL
                case 2: // DESCRIPTION
                    String text = (String) value;
                    if (text.contains("`")) {

                        text = text.substring(0, text.indexOf("`"));
                    }

                    lblRenderer.setText(text);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE

                    if (value != "") {
                        lblRenderer.setText(oPriceFormat.format(toDoubleValue(value)));
                    } else {
                        lblRenderer.setText("");
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;

                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }

        return lblRenderer;
    }

    private double toDoubleValue(Object oValue) {
        try {
            return Double.parseDouble((String) oValue);
        } catch (Exception e) {
            return 0;
        }
    }

    private long toLongValue(Object oValue) {
        try {
            return Long.parseLong((String) oValue);
        } catch (Exception e) {
            return 0;
        }
    }
}
