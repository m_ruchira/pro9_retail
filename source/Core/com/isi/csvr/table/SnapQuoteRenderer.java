// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

public class SnapQuoteRenderer extends TWBasicTableRenderer {
    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();
    private static Color g_oUpFGColor;
    private static Color g_oDownFGColor;
    private static Color g_oUpBGColor;
    private static Color g_oDownBGColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oDownColor;
    private static Color g_oUpColor;
    private static Color g_oHeaderBGColor;
    private static Color g_oHeaderFGColor;
    private static Color g_oBidFG1;
    private static Color g_oBidBG1;
    private static Color g_oBidFG2;
    private static Color g_oBidBG2;
    private static Color g_oAskFG1;
    private static Color g_oAskBG1;
    private static Color g_oAskFG2;
    private static Color g_oAskBG2;
    private static Border selectedBorder;
    private static Border unselectedBorder;
    private static Border headerBorder;
    private int g_iStringAlign;
    private int g_iStringOppositeAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private TWDateFormat g_oDateFormat;
    private TWDateFormat g_oDateTimeFormat = new TWDateFormat("HH:mm:ss");
    private String g_sNA = "NA";
    private Color headerFG;
    private Color headerBG;
    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private long[] longArray;
    private Date date = new Date();
    private double doubleValue;
    private long longValue;
    private long updateDirection;
    private long now;

    public SnapQuoteRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;

        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
        g_iStringOppositeAlign = JLabel.RIGHT;
    }

    public static void reloadForPrinting() {
        g_oUpFGColor = Color.black;
        g_oDownFGColor = Color.black;
        g_oUpBGColor = Color.white;
        g_oDownBGColor = Color.white;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.white;
        g_oBG2 = Color.white;
        g_oDownColor = Color.black;
        g_oUpColor = Color.black;
        g_oHeaderBGColor = Color.white;
        g_oHeaderFGColor = Color.black;
        g_oBidFG1 = Color.white;
        g_oBidBG1 = Color.white;
        g_oBidFG2 = Color.white;
        g_oBidBG2 = Color.white;
        g_oAskFG1 = Color.white;
        g_oAskBG1 = Color.white;
        g_oAskFG2 = Color.white;
        g_oAskBG2 = Color.white;
    }

    public static void reload() {
        try {
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oDownColor = g_oDownBGColor;
            g_oUpColor = g_oUpBGColor;
            g_oHeaderBGColor = Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR");
            g_oHeaderFGColor = Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR");
            selectedBorder = BorderFactory.createMatteBorder(0, 0, 2, 0, g_oSelectedFG);
            unselectedBorder = BorderFactory.createMatteBorder(0, 0, 2, 0, g_oFG1);
            headerBorder = UIManager.getBorder("TableHeader.cellBorder");

            g_oBidFG1 = Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR1");
            g_oBidBG1 = Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR1");
            g_oBidFG2 = Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR1");
            g_oBidBG2 = Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR2");
            g_oAskFG1 = Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR1");
            g_oAskBG1 = Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR1");
            g_oAskFG2 = Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR1");
            g_oAskBG2 = Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR2");
        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oDownColor = Color.red;
            g_oUpColor = Color.green;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            g_oBidFG1 = Color.white;
            g_oBidBG1 = Color.white;
            g_oBidFG2 = Color.white;
            g_oBidBG2 = Color.white;
            g_oAskFG1 = Color.white;
            g_oAskBG1 = Color.white;
            g_oAskFG2 = Color.white;
            g_oAskBG2 = Color.white;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void propertyChanged(int property) {
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else if (row > 10) {
                if (column < 2) {
                    if (row % 2 == 0) {
                        foreground = sett.getDepthOrderBidColor1FG();
                        background = sett.getDepthOrderBidColor1BG();
                    } else {
                        foreground = sett.getDepthOrderBidColor2FG();
                        background = sett.getDepthOrderBidColor2BG();
                    }
                } else {
                    if (row % 2 == 0) {
                        foreground = sett.getDepthOrderAskColor1FG();
                        background = sett.getDepthOrderAskColor1BG();
                    } else {
                        foreground = sett.getDepthOrderAskColor2FG();
                        background = sett.getDepthOrderAskColor2BG();
                    }
                }
            } else if (row % 2 == 0) {
                foreground = sett.getRowColor1FG();
                background = sett.getRowColor1BG();
            } else {
                foreground = sett.getRowColor2FG();
                background = sett.getRowColor2BG();
            }
            headerBG = sett.getHeaderColorBG();
            headerFG = sett.getHeaderColorFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row > 10) {
                if (column < 2) {
                    if (row % 2 == 0) {
                        foreground = g_oBidFG1;
                        background = g_oBidBG1;
                    } else {
                        foreground = g_oBidFG2;
                        background = g_oBidBG2;
                    }
                } else {
                    if (row % 2 == 0) {
                        foreground = g_oAskFG1;
                        background = g_oAskBG1;
                    } else {
                        foreground = g_oAskFG2;
                        background = g_oAskBG2;
                    }
                }
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            headerBG = g_oHeaderBGColor;
            headerFG = g_oHeaderFGColor;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        try {
            oPriceFormat = ((ExchangeFormatInterface) table.getModel()).getDecimalFormat();// = ((SmartTable)table).getDecimalFormat();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int iRendID = getRenderingID(row, column);

        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setBackground(headerBG);
                    lblRenderer.setForeground(headerFG);
                    lblRenderer.setBorder(headerBorder);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setBackground(background); //table.getTableHeader().getBackground());
                    lblRenderer.setForeground(foreground);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    if (doubleValue >= 0)
                        lblRenderer.setText(oPriceFormat.format(doubleValue));
                    else
                        lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    longValue = ((LongTransferObject) (value)).getValue();
                    if (longValue >= 0)
                        lblRenderer.setText(oQuantityFormat.format(longValue));
                    else
                        lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 5: // CHANGE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(g_oUpColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(g_oDownColor);
                    break;
                case 6: // % CHANGE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(g_oUpColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(g_oDownColor);
                    break;
                case 7: // DATE
                    longValue = ((LongTransferObject) (value)).getValue();
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringOppositeAlign);
                    break;
                case 8: {// DATE TIME
                    longArray = (long[]) value;
                    if (longArray[0] == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longArray[0]);
                        lblRenderer.setText(g_oDateTimeFormat.format(date));
                    }
                    if (longArray[0] > longArray[1]) {
                        lblRenderer.setBackground(g_oUpBGColor);
                        lblRenderer.setForeground(g_oUpFGColor);
                    } else if (longArray[0] < longArray[1]) {
                        lblRenderer.setForeground(g_oDownFGColor);
                        lblRenderer.setBackground(g_oDownBGColor);
                    }
                    longArray[1] = longArray[0];
                    lblRenderer.setHorizontalAlignment(g_iStringOppositeAlign);
                    break;
                }
                case 'P': // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    updateDirection = ((DoubleTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(g_oDownFGColor);
                            lblRenderer.setBackground(g_oDownBGColor);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'Q': // Quantity with coloured bg
                    longValue = ((LongTransferObject) (value)).getValue();
                    updateDirection = ((LongTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    lblRenderer.setText(oQuantityFormat.format(longValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(g_oDownFGColor);
                            lblRenderer.setBackground(g_oDownBGColor);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'p': // PRICE with no decimals
                    //adPrice = (double[]) value;
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    updateDirection = ((DoubleTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    lblRenderer.setText(oQuantityFormat.format(doubleValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(g_oDownFGColor);
                            lblRenderer.setBackground(g_oDownBGColor);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }

        return lblRenderer;
    }

    private int getRenderingID(int iRow, int iCol) {

        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'P';
                    case 2:
                        return 2;
                    case 3:
                        return 'P';
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'Q';
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 5;
                    case 2:
                        return 2;
                    case 3:
                        return 'P';
                }
            case 3:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 6;
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 4:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 5:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'Q';
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 6:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'p';
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 7:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 4;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 8:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 9:
            case 10:
                return 1;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                switch (iCol) {
                    case 0:
                        return 3;
                    case 1:
                        return 4;
                    case 2:
                        return 3;
                    case 3:
                        return 4;
                }
        }
        return 0;
    }


}
