package com.isi.csvr.table;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;

import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Nov 20, 2008
 * Time: 4:23:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class OtherSymbolEventModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private String symbol;
    private ArrayList<Stock.SymbolEvent> dataStore = new ArrayList();

    public OtherSymbolEventModel(String symbol) {
        this.symbol = symbol;
        loadDataStore();
    }

    public int getRowCount() {
        return dataStore.size();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            Stock.SymbolEvent ev = dataStore.get(rowIndex);
            switch (columnIndex) {

                case 0:
                    return getDescriptionString(ev.type);

                case 1:
                    return "";
                default:
                    return "";
            }
        } catch (Exception e) {
            return "";
        }


    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (iCol) {

            case 0:
            case 1:
                return String.class;
            default:
                return Object.class;

        }


    }

    private void loadDataStore() {
        Stock stk = DataStore.getSharedInstance().getStockObject(symbol);

        if (stk != null) {

            Hashtable<Integer, Stock.SymbolEvent> ht = stk.getSymbolEventHashTable();
            if (ht.containsKey(Constants.EVENT_FINANCIAL_CALENDAR)) {
                dataStore.add(ht.get(Constants.EVENT_FINANCIAL_CALENDAR));
            }
            if (ht.containsKey(Constants.EVENT_INSIDER_TRADES)) {
                dataStore.add(ht.get(Constants.EVENT_INSIDER_TRADES));
            }
            if (ht.containsKey(Constants.EVENT_RESEARCH_MATERIAL)) {
                dataStore.add(ht.get(Constants.EVENT_RESEARCH_MATERIAL));
            }


        }

    }

    public String getDescriptionString(int evType) {
        String description = "";
        switch (evType) {
            case Constants.EVENT_FINANCIAL_CALENDAR:
                description = Language.getString("FINANCIAL_CALENDAR");
                break;
            case Constants.EVENT_INSIDER_TRADES:
                description = Language.getString("INSIDER_TRADES");
                break;
            case Constants.EVENT_RESEARCH_MATERIAL:
                description = Language.getString("RESEARCH_MATERIAL");
                break;
            default:
                description = "";
        }
        return description;
    }
}
