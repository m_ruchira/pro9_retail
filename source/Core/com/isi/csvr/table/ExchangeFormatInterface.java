package com.isi.csvr.table;

import com.isi.csvr.shared.TWDecimalFormat;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 5, 2006
 * Time: 3:28:54 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ExchangeFormatInterface {

    public TWDecimalFormat getDecimalFormat();

    public byte getDecimalCount();

    public void setDecimalCount(byte count);
}
