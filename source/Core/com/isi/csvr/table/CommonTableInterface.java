// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.properties.ViewSetting;

public interface CommonTableInterface {

    /**
     * Set the table for this modal
     */
    public void setTable(Table oTable);

    /**
     * Returns the view setting object for this table
     */
    public ViewSetting getViewSettings();

    /**
     * Set the view setting object for this table
     */
    public void setViewSettings(ViewSetting oViewSettings);

    /**
     * Set the column array for this class
     *
     public void setColumns(String[] asCols);*/

    /**
     * Sets the orientation to LTR ot nor
     */
    public void setDirectionLTR(boolean bLTR);

    /**
     * Change the column list direction to RTL
     *
     public void applyRTL();*/

    /**
     * Applies table column renderers for each column of the table,
     * and there column headers.
     */
    public void applyColumnSettings();

    /**
     * Update the GUI of the table component
     */
    public void updateGUI();


    public void setSymbol(String symbol);
}

 