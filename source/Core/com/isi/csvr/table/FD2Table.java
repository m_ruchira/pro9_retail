package com.isi.csvr.table;

// Copyright (c) 2000 Home

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.FDTable;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.ColumnLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FD2Table extends InternalFrame implements Runnable, Themeable, DropTargetListener, MouseListener, InternalFrameListener { //, Scrollable {

    private final String SPACE = "  ";
    private final int PANEL_WIDTH = 480;
    private final int PANEL_HEIGHT = 330; //400; //380; //530; //380; //410; //
    private final int FIELD_HEIGHT = 15;
    private final int[] FIELD_WIDTH; // = {PANEL_WIDTH,130,95,130,95,225};
    DecimalFormat oPriceFormat = new DecimalFormat(" ###,##0.00 ");
    DecimalFormat oQuantityFormat = new DecimalFormat(" ###,##0 ");
    DateFormat dateFormatter1 = new SimpleDateFormat("yyyyMMdd");
    DateFormat dateFormatter2 = new SimpleDateFormat("dd/MM/yyyy");
    private boolean isFirstTime = false;
    private boolean isActive = true;
    private Thread g_oThread;
    private String sKey = null;
    private ViewSetting g_oViewSettings;
    private Color headerColor = null;
    private JInternalFrame parent;
    //    private boolean     loadedFromTheme = false;
    private Color upColor;
    private Color downColor;
    private Color upfgColor;
    private Color downfgColor;
    private Color fgColor;
    private Color fd1Color;
    private double prevTrade = 0;
    private double prevAsk = 0;
    private double prevBid = 0;
    //private final int BOTTOM_HEIGHT = 290; //275; //117;
    private long prevVolume = 0;
    private JTextField reference = new JTextField();
    //private final int[] FIELD_WIDTH = {PANEL_WIDTH,70,90,110,89,90+110};
    //private JTextField  txtSymbol   = new JTextField();
    private JLabel lblDescr = new JLabel();
    private JPanel mainPanel = new JPanel();

    private JLabel l1;
    private JLabel l2;
    private JLabel l3;
    private JLabel l4;
    private JLabel lblHeader11;
    private JLabel lblHeader12;
    private JLabel lblHeader13;
    private JLabel lblHeader14;
    private JLabel lblHeader15;

    private JLabel lblHeader16;
    private JLabel lblHeader17;
    private JLabel lblHeader18;
    private JLabel lblHeader19;
    private JLabel lblHeader110;

    private JLabel lblHeader111;
    private JLabel lblHeader112;
    private JLabel lblHeader113;
    private JLabel lblHeader114;
    private JLabel lblHeader115;

    private JLabel lblHeader21;
    private JLabel lblHeader22;
    private JLabel lblHeader23;
    private JLabel lblHeader24;
    private JLabel lblHeader25;

    // section 1
    private JLabel lblRefYearHighLow;
    private JLabel lblRefYearDividends;
    private JLabel lblRefYearEPS;
    private JTextArea lblRefYearURL = new JTextArea();
    private JLabel lblHigh1;
    private JLabel lblHigh2;
    private JLabel lblHigh3;
    //    private JLabel lblHigh4;
    private JLabel lblLow1;
    private JLabel lblLow2;
    private JLabel lblLow3;
    //    private JLabel lblLow4;
    private JLabel lblDividend1;
    private JLabel lblDividend2;
    private JLabel lblDividend3;
    private JLabel lblDividend4;
    private JLabel lblEPS1;
    private JLabel lblEPS2;
    private JLabel lblEPS3;
    //    private JLabel lblEPS4;
    private JLabel lblEquityEPS1;
    private JLabel lblEquityEPS2;
    private JLabel lblEquityEPS3;
    //    private JLabel lblEquityEPS4;
    private JLabel lblRevenue1;
    private JLabel lblRevenue2;
    private JLabel lblRevenue3;
    //    private JLabel lblRevenue4;
    private JLabel lblNetIncome1;
    private JLabel lblNetIncome2;
    private JLabel lblNetIncome3;
    //    private JLabel lblNetIncome4;
    private JLabel lblSplitFactor1;
    private JLabel lblSplitFactor2;
    private JLabel lblSplitFactor3;
    private JLabel lblSplitFactor4;
    private JLabel lblSplitFactorDate1;
    private JLabel lblSplitFactorDate2;
    private JLabel lblSplitFactorDate3;
    private JLabel lblSplitFactorDate4;

    private DropTarget dropTarget1 = null;
    private DropTarget dropTarget2 = null;

    private JPanel contentPanel;

    /**
     * Constructor
     */
    public FD2Table(ViewSetting viewSetting, String sSymbol) { //String sKey) {
        setViewSetting(viewSetting);
//        g_oViewSettings = viewSetting;
        setSymbol(sSymbol);

        Theme.registerComponent(this);

        contentPanel = new JPanel(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));

        this.setLayout(new FlowLayout(SwingUtilities.CENTER, 5, 5));
//        viewSetting.setSize(new Dimension(this.PANEL_WIDTH + 30, this.PANEL_HEIGHT + 10));
//        setPreferredSize(new Dimension(this.PANEL_WIDTH + 10, this.PANEL_HEIGHT + 10));
        dropTarget1 = new DropTarget(this, this);

        // Calculate the widths
        FIELD_WIDTH = new int[6];
        FIELD_WIDTH[0] = this.PANEL_WIDTH;
        int width = this.PANEL_WIDTH / 4;
        FIELD_WIDTH[1] = width;
        width = (this.PANEL_WIDTH - width) / 4;
        FIELD_WIDTH[2] = width;
        FIELD_WIDTH[3] = width * 2;
        FIELD_WIDTH[4] = width / 2 + 15;
        FIELD_WIDTH[5] = FIELD_WIDTH[0] - FIELD_WIDTH[1];

        // #####################################################################
        //      Create North Panel
        // #####################################################################
        JPanel configPanel = new JPanel();
        configPanel.setLayout(new FlowLayout(SwingConstants.CENTER, 5, 2));
        configPanel.setBorder(null);
        /*txtSymbol.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
            	if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    validateSymbol(txtSymbol.getText().toUpperCase().trim());
					//System.out.println(" Quote Symbol : " + txtSymbol.getText());
                }
            }
        });
        dropTarget2 = new DropTarget(txtSymbol, this);*/


        lblDescr.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
//        lblDescr.setPreferredSize(new Dimension(PANEL_WIDTH, 20));

        lblRefYearURL.setBorder(BorderFactory.createEmptyBorder());
        lblRefYearURL.setBackground(contentPanel.getBackground());
        lblRefYearURL.getSize().width = PANEL_WIDTH - 25;
        lblRefYearURL.setEditable(false);
        lblRefYearURL.setLineWrap(true);
        lblRefYearURL.setWrapStyleWord(true);


        // #####################################################################
        //      Create the Center Panel
        // #####################################################################

//        mainPanel.setLayout(new FlowLayout(SwingConstants.CENTER, 0, 0));
        mainPanel.setLayout(new FlowLayout(SwingConstants.CENTER, 0, 5));
        mainPanel.setPreferredSize(new Dimension(this.PANEL_WIDTH + 15, this.PANEL_HEIGHT - 100));

        loadThemeColors();
        headerColor = Theme.getDTQHeadColor();

        lblHeader11 = createHeaderField("", 1, SwingConstants.TRAILING);
        lblHeader12 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader13 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader14 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader15 = createHeaderField("", 2, SwingConstants.TRAILING);

        lblHeader12.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
//        lblHeader13.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblHeader14.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
//        lblHeader15.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));

        l1 = createField(SPACE + "Div. Amount", 1, SwingConstants.LEADING);
        lblHigh1 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblHigh2 = createField(SPACE + "Div. Pay Date", 1, SwingConstants.LEADING);
        lblHigh3 = createField("  ", 2, SwingConstants.TRAILING);
//        lblHigh4 = createField(" 0.00 ",2,SwingConstants.TRAILING);

        l2 = createField(SPACE + "Div. Rate", 1, SwingConstants.LEADING);
        lblLow1 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblLow2 = createField(SPACE + "Ex Divident Date", 1, SwingConstants.LEADING);
        lblLow3 = createField("  ", 2, SwingConstants.TRAILING);
//        lblLow4 = createField(" 0.00 ",2,SwingConstants.TRAILING);

        lblHeader16 = createHeaderField("", 1, SwingConstants.TRAILING);
        lblHeader17 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader18 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader19 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader110 = createHeaderField("", 2, SwingConstants.TRAILING);

        lblHeader17.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblHeader18.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblHeader19.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblHeader110.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));

        createField(SPACE + Language.getString("FD2_DIVIDEND"), 1, SwingConstants.LEADING);
        lblDividend1 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblDividend2 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblDividend3 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblDividend4 = createField(" 0.00 ", 2, SwingConstants.TRAILING);

        lblHeader111 = createHeaderField("", 1, SwingConstants.TRAILING);
        lblHeader112 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader113 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader114 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader115 = createHeaderField("", 2, SwingConstants.TRAILING);

        lblHeader112.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
//        lblHeader113.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblHeader114.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
//        lblHeader115.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));

        l3 = createField(SPACE + "Cal Year High", 1, SwingConstants.LEADING);
        lblEPS1 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblEPS2 = createField(SPACE + "Cal Year High Date", 1, SwingConstants.LEADING);
        lblEPS3 = createField("  ", 2, SwingConstants.TRAILING);
//        lblEPS4 = createField(" 0.00 ",2,SwingConstants.TRAILING);

        l4 = createField(SPACE + "Cal Year Low", 1, SwingConstants.LEADING);
        lblEquityEPS1 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblEquityEPS2 = createField(SPACE + "Cal Year Low Date", 1, SwingConstants.LEADING);
        lblEquityEPS3 = createField("  ", 2, SwingConstants.TRAILING);
//        lblEquityEPS4 = createField(" 0.00 ",2,SwingConstants.TRAILING);

        createField(SPACE + "52 week high", 1, SwingConstants.LEADING);
        lblRevenue1 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblRevenue2 = createField(SPACE + "52 week high Date", 1, SwingConstants.LEADING);
        lblRevenue3 = createField(" ", 2, SwingConstants.TRAILING);
//        lblRevenue4 = createField(" 0.00 ",2,SwingConstants.TRAILING);

        createField(SPACE + "52 Week low", 1, SwingConstants.LEADING);
        lblNetIncome1 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblNetIncome2 = createField(SPACE + "52 Week low date", 1, SwingConstants.LEADING);
        lblNetIncome3 = createField("  ", 2, SwingConstants.TRAILING);
//        lblNetIncome4 = createField(" 0.00 ",2,SwingConstants.TRAILING);

        lblHeader21 = createHeaderField("", 1, SwingConstants.TRAILING);
        lblHeader22 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader23 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader24 = createHeaderField("", 2, SwingConstants.TRAILING);
        lblHeader25 = createHeaderField("", 2, SwingConstants.TRAILING);

        createField(SPACE + Language.getString("FD2_SPLIT_FACTOR_DATE"), 1, SwingConstants.LEADING);
        lblSplitFactorDate1 = createField("     ", 2, SwingConstants.TRAILING);
        lblSplitFactorDate2 = createField("     ", 2, SwingConstants.TRAILING);
        lblSplitFactorDate3 = createField("     ", 2, SwingConstants.TRAILING);
        lblSplitFactorDate4 = createField("     ", 2, SwingConstants.TRAILING);
        //lblHigh1.setForeground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR");

        createField(SPACE + Language.getString("FD2_SPLIT_FACTOR"), 1, SwingConstants.LEADING);
        lblSplitFactor1 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblSplitFactor2 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblSplitFactor3 = createField(" 0.00 ", 2, SwingConstants.TRAILING);
        lblSplitFactor4 = createField("     ", 2, SwingConstants.TRAILING);

        contentPanel.add(lblDescr);
        contentPanel.add(lblRefYearURL);
        contentPanel.add(mainPanel);
        this.add(contentPanel);
        pack();
//        this.setBounds(new Rectangle(this.PANEL_WIDTH, this.PANEL_HEIGHT));

        applyTheme();
        updateData();
        applySettings();
        g_oThread = new Thread(this, "TW FD2 Table");

        isFirstTime = true;
        g_oThread.start();
    }

    public void setParent(JInternalFrame parent) {
        this.parent = parent;
        //System.out.println("Set Parent null " + (this.parent == null));
        //hidePanel();
    }

    private JLabel createField(String caption, int column, int align) {
        JLabel field = new JLabel(caption);
        field.setPreferredSize(new Dimension(FIELD_WIDTH[column], FIELD_HEIGHT));
        field.setHorizontalAlignment(align);

        mainPanel.add(field);

        return field;
    }

    private JLabel createHeaderField(String caption, int column, int align) {
        JLabel field = new JLabel(caption);
        field.setOpaque(true);
        field.setBackground(headerColor);
        field.setPreferredSize(new Dimension(FIELD_WIDTH[column], FIELD_HEIGHT));
        field.setHorizontalAlignment(align);
        mainPanel.add(field);

        return field;
    }

    private JLabel createHeaderField(String caption, int column, int align,
                                     int width, int height) {
        JLabel field = new JLabel(caption);
        field.setOpaque(true);
        field.setBackground(headerColor);
        field.setPreferredSize(new Dimension(width, height));
        field.setHorizontalAlignment(align);
        mainPanel.add(field);

        return field;
    }

    /**
     * Returns the view setting object for this table
     */
    public ViewSetting getViewSettings() {
        return g_oViewSettings;
    }

    /**
     * Set the view setting object for this table
     */
    public void setViewSettings(ViewSetting oViewSettings) {

        g_oViewSettings = oViewSettings;
        g_oViewSettings.setSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
    }

    public void connectionEstablished() {
    }

    ;

    /**
     * Returns the Window Type for the window
     */
    public int getWindowType() {
        return ViewSettingsManager.FUNDAMENTAL_DATA_VIEW;
    }

    public void setSymbol(String sKeyIn) {
        String symbol = null;
        int instrument = -1;
        sKey = sKeyIn;
        Stock stock = DataStore.getSharedInstance().getStockObject(sKeyIn);
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            symbol = SharedMethods.getSymbolFromKey(sKeyIn);
            instrument = SharedMethods.getInstrumentTypeFromKey(sKeyIn);
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                symbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
                sKeyIn = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sKeyIn), symbol, instrument);
            }
        }

        String companyName = null;
        try {
            companyName = DataStore.getSharedInstance().getCompanyName(sKey);
        } catch (Exception e) {
        }
        if ((companyName != null) && (!companyName.equals(""))) {
            lblDescr.setText(companyName);
        } else {
            lblDescr.setText(symbol);
        }

        String sym = SharedMethods.getSymbolFromKey(sKey);
        String ex = SharedMethods.getExchangeFromKey(sKey);
        instrument = SharedMethods.getInstrumentTypeFromKey(sKey);

        if (parent != null)
            parent.setTitle(Language.getString("WINDOW_TITLE_FD2") + " " + SharedMethods.getDisplayKey(sKey));

        DataStore.getSharedInstance().addSymbolRequest(ex, sym, instrument, Meta.FUNDAMENTALDATATABLE);
        lblDescr.updateUI();
        symbol = null;
        updateData();
        getViewSetting().setSize(getWindowSize());
        applySettings();
    }

    /**
     * Run meththod of the thread. this updates the table data
     * each 1 seconds
     */
    public void run() {
        // update for the first time. called wwice to avoid up/down color change
        updateData();
        updateData();
        getViewSetting().setSize(getWindowSize());
        applySettings();
//        setLocationRelativeTo(Client.getInstance().getFrame());
//        setVisible(true);
        while (isActive) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    if (Settings.isConnected()) {
                        updateData();
                    }
                }
            });
            if (!isVisible())
                setVisible(true);
//            getViewSetting().setSize(getWindowSize());
//            applySettings();
            Sleep(2000);
        }
    }

    private void updateData() {

        try {
            int yearValue = 0;
            String sRefYear = null;

            if (sKey == null) return;
            String symbol;
            String sKeyIn = sKey;
            int instrument = Meta.INSTRUMENT_COMMON_STOCK;
            Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
            if ((stock != null) && (stock.getMarketCenter() != null)) {
                symbol = SharedMethods.getSymbolFromKey(sKey);
                instrument = SharedMethods.getInstrumentTypeFromKey(sKey);
                if (symbol.endsWith("." + stock.getMarketCenter())) {
                    symbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
                    sKeyIn = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sKey), symbol, instrument);
                }
            }
            FDTable fdTable = null;
            try {
                fdTable = DataStore.getSharedInstance().getFDTable(sKeyIn);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }


            if (fdTable == null)
                return;
            if (parent != null) {

                String companyName = null;
                try {
                    companyName = DataStore.getSharedInstance().getCompanyName(sKey);
                } catch (Exception e) {
                }
                if ((companyName != null) && (!companyName.equals(""))) {
                    lblDescr.setText(companyName);
                } else {
                    lblDescr.setText(sKey);
                }
            }
            String text = fdTable.getBusinessDescription();

            // reset the window size in the first time the description is set
            lblRefYearURL.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            if (isFirstTime && (text != null || !text.equals(""))) {
                lblRefYearURL.setText(text);
                getViewSetting().setSize(getWindowSize());
                isFirstTime = false;
                applySettings();
                show();
            } else if (isFirstTime) {
                lblRefYearURL.setText("");
                getViewSetting().setSize(getWindowSize());
                applySettings();
                show();
            }
            if ((fdTable.getYearForDividends() != null) && (fdTable.getYearForDividends().length() > 0)) {
                try {
                    yearValue = Integer.parseInt(fdTable.getYearForDividends());
                    //                if (yearValue > 50)
                    //                    sRefYear = "19" + fdTable.getYearForDividends();
                    //                else
                    //                    sRefYear = "20" + fdTable.getYearForDividends();
//                    yearValue = Integer.parseInt(sRefYear);

                    lblHeader17.setText(" " + yearValue-- + " ");
                    lblHeader18.setText(" " + yearValue-- + " ");
                    lblHeader19.setText(" " + yearValue-- + " ");
                    lblHeader110.setText(" " + yearValue-- + " ");
                } catch (Exception e) {
                    lblHeader17.setText(" -- ");
                    lblHeader18.setText(" -- ");
                    lblHeader19.setText(" -- ");
                    lblHeader110.setText(" -- ");
                }
            }

//        lblRefYearURL.setText(fdTable.getSURL());

            lblHigh1.setText(oPriceFormat.format(fdTable.getDivAmount()));

            try {
                Date payDiv = dateFormatter1.parse((fdTable.getDivPayDate()));
                lblHigh3.setText(dateFormatter2.format(payDiv));
            } catch (ParseException e) {
            }

            lblLow1.setText(oPriceFormat.format(fdTable.getDivRate()));

            try {
                Date exDiv = dateFormatter1.parse((fdTable.getExDivDate()));
                lblLow3.setText(dateFormatter2.format(exDiv));
            } catch (ParseException e) {
            }

            float[] dividends = fdTable.getDividends();
            lblDividend1.setText(oPriceFormat.format(dividends[0]));
            lblDividend2.setText(oPriceFormat.format(dividends[1]));
            lblDividend3.setText(oPriceFormat.format(dividends[2]));
            lblDividend4.setText(oPriceFormat.format(dividends[3]));

            lblEPS1.setText(oPriceFormat.format(fdTable.getCalYearHigh()));
            Date dateEPS3 = dateFormatter1.parse((fdTable.getCalYearHighDate()));
            lblEPS3.setText(dateFormatter2.format(dateEPS3));

            lblEquityEPS1.setText(oPriceFormat.format(fdTable.getCalYearLow()));
            Date dateEquityEPS3 = dateFormatter1.parse((fdTable.getCalYearLowDate()));
            lblEquityEPS3.setText(dateFormatter2.format(dateEquityEPS3));

            lblRevenue1.setText(oPriceFormat.format(fdTable.getFd52wkHigh()));
            Date dateRevenue3 = dateFormatter1.parse((fdTable.getFd52wkHighDate()));//new Date(Long.parseLong((fdTable.getFd52wkHighDate())));
            lblRevenue3.setText(dateFormatter2.format(dateRevenue3));

            lblNetIncome1.setText(oPriceFormat.format(fdTable.getFd52wkLow()));
            Date dateNetIncome3 = dateFormatter1.parse((fdTable.getFd52wkLowDate()));
            lblNetIncome3.setText(dateFormatter2.format(dateNetIncome3));
            float[] splitFactors = fdTable.getSplitFactors();
            lblSplitFactor1.setText(oPriceFormat.format(splitFactors[0]));
            lblSplitFactor2.setText(oPriceFormat.format(splitFactors[1]));
            lblSplitFactor3.setText(oPriceFormat.format(splitFactors[2]));
            String[] splitFactorDates = fdTable.getSplitFactorDates();
            if ((splitFactorDates[0] != null) && !splitFactorDates[0].equals("null")) {
                Date SplitFactorDate1 = dateFormatter1.parse(splitFactorDates[0]);
                lblSplitFactorDate1.setText(" " + dateFormatter2.format(SplitFactorDate1) + " ");
            } else
                lblSplitFactorDate1.setText(" -- ");
            if ((splitFactorDates[1] != null) && !splitFactorDates[1].equals("null")) {
                Date SplitFactorDate2 = dateFormatter1.parse(splitFactorDates[1]);
                lblSplitFactorDate2.setText(" " + dateFormatter2.format(SplitFactorDate2) + " ");
            } else
                lblSplitFactorDate2.setText(" -- ");
            if ((splitFactorDates[2] != null) && !splitFactorDates[2].equals("null")) {
                Date SplitFactorDate3 = dateFormatter1.parse(splitFactorDates[1]);
                lblSplitFactorDate3.setText(" " + dateFormatter2.format(SplitFactorDate3) + " ");
            } else
                lblSplitFactorDate3.setText(" -- ");

//            updateUI();
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

//        applySettings();

    }

    public void killThread() {
        isActive = false;
    }

    /**
     * Sleep the thread for a give time period
     */
    private void Sleep(long lDelay) {
        try {
            g_oThread.sleep(lDelay);
        } catch (Exception e) {
        }
    }

    private void loadThemeColors() {
        upColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
        upfgColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
        downColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
        downfgColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
        fgColor = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1"); //LABEL_FGCOLOR");
        fd1Color = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
    }

    public void applyTheme() {
        loadThemeColors();

        headerColor = Theme.getDTQHeadColor();
        lblHeader11.setBackground(headerColor);
        lblHeader12.setBackground(headerColor);
        lblHeader13.setBackground(headerColor);
        lblHeader14.setBackground(headerColor);
        lblHeader15.setBackground(headerColor);

        lblHeader16.setBackground(headerColor);
        lblHeader17.setBackground(headerColor);
        lblHeader18.setBackground(headerColor);
        lblHeader19.setBackground(headerColor);
        lblHeader110.setBackground(headerColor);

        lblHeader111.setBackground(headerColor);
        lblHeader112.setBackground(headerColor);
        lblHeader113.setBackground(headerColor);
        lblHeader114.setBackground(headerColor);
        lblHeader115.setBackground(headerColor);

        lblHeader21.setBackground(headerColor);
        lblHeader22.setBackground(headerColor);
        lblHeader23.setBackground(headerColor);
        lblHeader24.setBackground(headerColor);
        lblHeader25.setBackground(headerColor);

        l1.setForeground(fgColor);
        l2.setForeground(fgColor);
        lblHigh1.setForeground(fgColor);
        lblHigh2.setForeground(fgColor);
        lblHigh3.setForeground(fgColor);
        lblLow1.setForeground(fgColor);
        lblLow2.setForeground(fgColor);
        lblLow3.setForeground(fgColor);
        l3.setForeground(fgColor);
        l4.setForeground(fgColor);
        lblEPS1.setForeground(fgColor);
        lblEPS2.setForeground(fgColor);
        lblEPS3.setForeground(fgColor);
        lblEquityEPS1.setForeground(fgColor);
        lblEquityEPS2.setForeground(fgColor);
        lblEquityEPS3.setForeground(fgColor);

        updateData();
        updateUI();
    }

    public void setMaximum(boolean max) {
    }

    public boolean isMaximim() {
        return true;
    }


    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent event) {
        String key = null;
        try {
            key = (String) event.getTransferable().getTransferData(DataFlavor.stringFlavor);
            if (!key.equals("Print")) {
                event.getDropTargetContext().dropComplete(true);
                if (key != null)
                    this.setSymbol(key);
            } else {
                event.rejectDrop(); // not from the print button. reject drop
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            event.rejectDrop();
            return;
        }
        key = null;
    }

    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    private Dimension getWindowSize() {
        int h = lblDescr.getPreferredSize().height + 5 +
                lblRefYearURL.getPreferredSize().height +
                mainPanel.getPreferredSize().height + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT;
//        System.out.println(lblRefYearURL.getText());
        int w = 0;
        if (Language.isLTR()) {
            w = mainPanel.getPreferredSize().width + 5;
        } else {
            w = mainPanel.getPreferredSize().width + 13;
        }
        return new Dimension(w, h);
    }

    public void internalFrameClosed(InternalFrameEvent e) {
        killThread();
        super.internalFrameClosed(e);
    }

    public void closeWindow() {
        killThread();
        super.closeWindow();
    }
}