package com.isi.csvr.table;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 21, 2007
 * Time: 10:01:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class OptionDetailQuoteModel extends CommonTable
        implements DetailQuote, TableModel, CommonTableInterface {

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private String index;

    /**
     * Constructor
     */
    public OptionDetailQuoteModel() {
    }

    public void setSymbol(String index) {
        this.index = index;
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 4;
    }

    public int getRowCount() {
        return 11;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            if (index == null)
                return "";
            Stock stock = DataStore.getSharedInstance().getStockObject(index);
            if (stock == null)
                return "";

            switch (iRow) {
                case -1:
                    return stock.getInstrumentType();
                case 0:
                    switch (iCol) {
                        case 0:
                            return Language.getString("LASTTRADE_TIME");
                        case 1:
                            return longTrasferObject.setValue(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), stock.getLastTradeTime()));
                        case 2:
                            return Language.getString("BEST_BID");
                        case 3:
                            doubleTransferObject.setFlag(stock.getBestBidPriceFlag());
                            return doubleTransferObject.setValue(stock.getBestBidPrice());
                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("LAST_TRADE");
                        case 1:
                            doubleTransferObject.setFlag(stock.getLastTradeFlag());
                            return doubleTransferObject.setValue(stock.getLastTradeValue());
                        case 2:
                            return Language.getString("BEST_BID_QUANTITY");
                        case 3:
                            return longTrasferObject.setValue(stock.getBestBidQuantity());
                    }
                case 2:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CHANGE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getChange());
                        case 2:
                            return Language.getString("BEST_ASK");
                        case 3:
                            doubleTransferObject.setFlag(stock.getBestAskPriceFlag());
                            return doubleTransferObject.setValue(stock.getBestAskPrice());
                    }
                case 3:
                    switch (iCol) {
                        case 0:
                            return Language.getString("PCT_CHANGE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getPercentChange());
                        case 2:
                            return Language.getString("BEST_ASK_QUANTITY");
                        case 3:
                            return longTrasferObject.setValue(stock.getBestAskQuantity());
                    }
                case 4:
                    switch (iCol) {
                        case 0:
                            return Language.getString("VWAP");
                        case 1:
                            return doubleTransferObject.setValue(stock.getAvgTradePrice());
                        case 2:
                            return Language.getString("VOLUME");
                        case 3:
                            longTrasferObject.setFlag(stock.getVolumeFlag());
                            return longTrasferObject.setValue(stock.getVolume());
                    }
                case 5:
                    switch (iCol) {
                        case 0:
                            return Language.getString("SPREAD");
                        case 1:
                            return doubleTransferObject.setValue(stock.getSpread());
                        case 2:
                            return Language.getString("RANGE");
                        case 3:
                            return doubleTransferObject.setValue(stock.getRange());
                    }
                case 6:
                    switch (iCol) {
                        case 0:
                            return Language.getString("PCT_SPREAD");
                        case 1:
                            return doubleTransferObject.setValue(stock.getPctSpread());
                        case 2:
                            return Language.getString("PCT_RANGE");
                        case 3:
                            return doubleTransferObject.setValue(stock.getPctRange());
                    }
                case 7:
                    switch (iCol) {
                        case 0:
                            return Language.getString("STRIKE_PRICE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getStrikePrice());
                        case 2:
                            return Language.getString("EXPIRATION_DATE");
                        case 3:
                            return longTrasferObject.setValue(stock.getExpirationDate());
                    }
                case 8:
                    switch (iCol) {
                        case 0:
                            return Language.getString("OPEN_INTEREST");
                        case 1:
                            return longTrasferObject.setValue(stock.getOpenInterest());
                        case 2:
                            return Language.getString("CONTRACT_HIGH");
                        case 3:
                            return doubleTransferObject.setValue(stock.getContractHigh());
                    }
                case 9:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CHANGE_OPEN_INTEREST");
                        case 1:
                            return doubleTransferObject.setValue(stock.getChgInOpenInt());
                        case 2:
                            return Language.getString("CONTRACT_LOW");
                        case 3:
                            return doubleTransferObject.setValue(stock.getContractLow());
                    }
                case 10:
                    switch (iCol) {
                        case 0:
                            return Language.getString("LOT_SIZE");
                        case 1:
                            return longTrasferObject.setValue(stock.getLotSize());
                        case 2:
                            return Language.getString("CURRENCY");
                        case 3:
                            return stock.getCurrencyCode();
                    }


            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 8;
                    case 2:
                        return 2;
                    case 3:
                        return 'P';
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'P';
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 5;
                    case 2:
                        return 2;
                    case 3:
                        return 'P';
                }
            case 3:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 6;
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 4:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 'Q';
                }
            case 5:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'S';
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 6:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'S';
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 7:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 7;
                }
            case 8:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 4;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 9:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 5;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 10:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 4;
                    case 2:
                        return 2;
                    case 3:
                        return 1;
                }

        }
        return 0;
    }

    public long getTimeOffset() {
        return 0;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 3);
        customizerRecords[3] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_VALUE_UP_ROW, Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_VALUE_DOWN_ROW, Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }
}