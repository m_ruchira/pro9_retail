// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWMenu;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.customizer.CommonTableCustomizerDialog;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.mist.MISTTableCustomizerDialog;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class CommonPopup extends JPopupMenu
        implements ActionListener, PopupMenuListener {
    private TWMenuItem mnuColSettings;
    private TWMenuItem mnuResizeCols;
    private TWMenuItem mnuCustomize;
    private TWMenuItem mnuTitle;
    private TWMenuItem mnuUnsort;
    private TWMenuItem mnuPrint;
    private TWMenuItem mnuCopy;
    private TWMenuItem mnuCopyH;
    private TWMenuItem mnuLink;
    private TWMenuItem mnuLinkH;
    private TWMenuItem mnuSetDefault;
    private TWMenuItem mnuResetDefault;
    private TWMenuItem mnuSaveSettings;

    private boolean copyWithH;
    private boolean linkToEx;
    private boolean setAsDe;
    private boolean print;
    private boolean resize = true;
    private boolean colSetings;
    private boolean setCuztomizer = true;
    private boolean isTMTavailable = true;

//    private CommonTableMenuToolbar tableMenuToolbar;

    private Table g_oTable;
    private int screenWidth;

    /**
     * Constructor
     */
    public CommonPopup() {
        screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
    }

    public void AddCustomizeMenu(Table oTable) {
        this.addPopupMenuListener(this);

        g_oTable = oTable;
        copyWithH = true;
        linkToEx = false;
        setAsDe = false;
        print = true;
        colSetings = true;

        mnuUnsort = new TWMenuItem(Language.getString("UNSORT"), "unsort.gif");
        mnuUnsort.addActionListener(this);
        mnuUnsort.setActionCommand("U");
        mnuUnsort.setVisible(false);
        mnuUnsort.setEnabled(false);
        this.add(mnuUnsort);

        mnuSaveSettings = new TWMenuItem("<html><font color=red><B><U>Save Settings", "resetdefault.gif");
        mnuSaveSettings.addActionListener(this);
        mnuSaveSettings.setActionCommand("S");
        mnuSaveSettings.setVisible(Settings.isCustomViewSettingsEnabled());
        mnuSaveSettings.setEnabled(Settings.isCustomViewSettingsEnabled());
        this.add(mnuSaveSettings);


        GUISettings.applyOrientation(this);
    }

    private TWMenu createSelectionPopup() {
        TWMenu selectionPopup = new TWMenu(Language.getString("SELECTION_MODE"), "selectionmode.gif");

        TWMenuItem mnuCell = new TWMenuItem(Language.getString("CELL"), "cellselect.gif");
        mnuCell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_oTable.getTable().setCellSelectionEnabled(true);
                g_oTable.getTable().setRowSelectionAllowed(true);
                g_oTable.getTable().setColumnSelectionAllowed(true);
            }
        });
        selectionPopup.add(mnuCell);

        TWMenuItem mnuLine = new TWMenuItem(Language.getString("ROW"), "rowselect.gif");
        mnuLine.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_oTable.getTable().setCellSelectionEnabled(true);
                g_oTable.getTable().setRowSelectionAllowed(true);
                g_oTable.getTable().setColumnSelectionAllowed(false);
            }
        });
        selectionPopup.add(mnuLine);

        return selectionPopup;
    }

    public void hideSetDefaultMenu() {
        setAsDe = false;
    }

    public void showSetDefaultMenu() {
        setAsDe = true;
    }


    public void hideCustomizerMenu() {
        setCuztomizer = false;
    }

    public void showCustomizerMenu() {
        setCuztomizer = true;
    }

    public void hideTitleBarMenu() {
    }

    public void hidePrintMenu() {
        print = false;
    }

    public void hideResizeColumns() {
        resize = false;
    }

    public void hideExcelLinkMenus() {
        linkToEx = false;
    }

    public boolean isTMTavailable() {
        return isTMTavailable;
    }

    public void setTMTavailable(boolean TMTavailable) {
        isTMTavailable = TMTavailable;
    }

    private void setHideShowMenuCaption() {
        try {
            WindowWrapper frame = (WindowWrapper) g_oTable.getModel().getViewSettings().getParent();
            if (!frame.isTitleVisible()) {
                mnuTitle.setText(Language.getString("SHOW_TITLEBAR"));
                mnuTitle.setIconFile("showtitle.gif");
            } else {
                mnuTitle.setText(Language.getString("HIDE_TITLEBAR"));
                mnuTitle.setIconFile("hidetitle.gif");
            }
            mnuTitle.updateUI();
            frame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("C")) {
            g_oTable.getModel().saveColumnPositions();
            (new Customizer(g_oTable)).showDialog();
        } else if (e.getActionCommand().equals("A")) {
            ((SmartTable) g_oTable.getTable()).adjustColumnWidthsToFit(40);
        } else if (e.getActionCommand().equals("U")) {
            g_oTable.unsort();
        } else if (e.getActionCommand().equals("T")) {
            hideShowTitleBar();
        } else if (e.getActionCommand().equals("D")) {
//            if(g_oTable.getModel().getViewSettings().getParent() instanceof InternalFrame){
//                Table table1 = ((InternalFrame)g_oTable.getModel().getViewSettings().getParent()).getTableComponent();
//                Table table2 = ((InternalFrame)g_oTable.getModel().getViewSettings().getParent()).getSubTableComponent();
//                table1.getModel().saveColumnPositions();
//                table1.getModel().getViewSettings().setFont(table1.getTable().getFont());
//                DefaultSettingsManager.getSharedInstance().addDefaultSetting(""+table1.getModel().getViewSettings().getType(),
//                        table1.getModel().getViewSettings().toWorkspaceString(false));
//                try {
//                    if (table2 != null) {
//                        table2.getModel().saveColumnPositions();
//                        table2.getModel().getViewSettings().setFont(table2.getTable().getFont());
//                        DefaultSettingsManager.getSharedInstance().addDefaultSetting(""+table2.getModel().getViewSettings().getType(),
//                            table2.getModel().getViewSettings().toWorkspaceString(false));
//                    }
//                } catch (Exception e1) {
//                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                }
//            } else{
//                g_oTable.getModel().saveColumnPositions();
//                g_oTable.getModel().getViewSettings().setFont(g_oTable.getTable().getFont());
//                DefaultSettingsManager.getSharedInstance().addDefaultSetting(""+g_oTable.getModel().getViewSettings().getType(),
//                        g_oTable.getModel().getViewSettings().toWorkspaceString(false));
//            }
        } else if (e.getActionCommand().equals("S")) {
            if (g_oTable.getModel().getViewSettings().getParent() instanceof InternalFrame) {
                Table table1 = ((InternalFrame) g_oTable.getModel().getViewSettings().getParent()).getTableComponent();
                Table table2 = ((InternalFrame) g_oTable.getModel().getViewSettings().getParent()).getSubTableComponent();
                table1.getModel().saveColumnPositions();
                table1.getModel().getViewSettings().setFont(table1.getTable().getFont());
                SharedMethods.saveCustomViewSetting(table1.getModel().getViewSettings());
                try {
                    if (table2 != null) {
                        table2.getModel().saveColumnPositions();
                        table2.getModel().getViewSettings().setFont(table2.getTable().getFont());
                        SharedMethods.saveCustomViewSetting(table2.getModel().getViewSettings());
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            } else {
                g_oTable.getModel().saveColumnPositions();
                g_oTable.getModel().getViewSettings().setFont(g_oTable.getTable().getFont());
                SharedMethods.saveCustomViewSetting(g_oTable.getModel().getViewSettings());
            }
        } else if (e.getActionCommand().equals("R")) {
            /* if(g_oTable.getModel().getViewSettings().getParent() instanceof InternalFrame){
                Table table1 = ((InternalFrame)g_oTable.getModel().getViewSettings().getParent()).getTableComponent();
                Table table2 = ((InternalFrame)g_oTable.getModel().getViewSettings().getParent()).getSubTableComponent();
                DefaultSettingsManager.getSharedInstance().removeDefaultSetting(""+table1.getModel().getViewSettings().getType());
                try {
                    if (table2 != null) {
                        DefaultSettingsManager.getSharedInstance().removeDefaultSetting(""+table2.getModel().getViewSettings().getType());
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            } else {
            DefaultSettingsManager.getSharedInstance().removeDefaultSetting(""+g_oTable.getModel().getViewSettings().getType());
            }*/

        } else if (e.getActionCommand().equals("F")) {
            if (g_oTable.getModel().getViewSettings().getMainType() == ViewSettingsManager.MIST_VIEW) {
                MISTTableCustomizerDialog.getSharedInstance().show(g_oTable);
            } else {
                CommonTableCustomizerDialog.getSharedInstance(g_oTable).show(g_oTable);
            }
//            CommonTableCustomizerDialog.getSharedInstance().show(g_oTable);
        } else {
            if ((g_oTable.getWindowType() == ViewSettingsManager.DETAIL_QUOTE) ||
                    (g_oTable.getWindowType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) ||
                    (g_oTable.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_MFUND))
                printTable(PrintManager.TABLE_TYPE_DEFAULT_NO_HEAD);
            else if ((g_oTable.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_ORDER) ||
                    (g_oTable.getWindowType() == ViewSettingsManager.FUND_TRANSFER_FRAME))
                printTableWithPanel(PrintManager.TABLE_TYPE_DEFAULT);
            else if ((g_oTable.getWindowType() == ViewSettingsManager.ACCOUNT_SUMMARY_VIEW))
                printTableWithPanel(PrintManager.TABLE_TYPE_DEFAULT_NO_HEAD);
            else if (g_oTable.getModel().getViewSettings().getID().equals("HISTORY_ANALYZER"))
                printTable(PrintManager.TABLE_TYPE_WATCHLIST); // ignore last col
            else if ((g_oTable.getWindowType() == ViewSettingsManager.SNAP_QUOTE) ||
                    (g_oTable.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW))
                printTable(PrintManager.TABLE_TYPE_SNAP_QUOTE);
            else if ((g_oTable.getWindowType() == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW))
                printTableWithPanel(PrintManager.TABLE_TYPE_DEPTH_CALC);
            else if (g_oTable.getModel().getViewSettings().getMainType() == ViewSettingsManager.MIST_VIEW)
                SharedMethods.printTable(g_oTable.getTable(), PrintManager.TABLE_TYPE_MIST_VIEW, g_oTable.getTitle());
            else
                printTable(PrintManager.TABLE_TYPE_DEFAULT);
        }
    }

    public void enableCustomViewSettings() {
        mnuSaveSettings.setVisible(true);
        mnuSaveSettings.setEnabled(true);
    }

    public void setMenu(TWMenu oMenu) {
        this.add(oMenu);
        //oMenu.addActionListener(this);
    }

    public void setMenuItem(TWMenuItem oMenuItem) {
        this.add(oMenuItem);
        GUISettings.applyOrientation(oMenuItem);
        //oMenuItem.addActionListener(this);
    }

    public void hideCustomizer() {
//        mnuColSettings.setVisible(false);
//        mnuColSettings.setEnabled(false);
        colSetings = false;
        // tableMenuToolbar.disableColSettings();
    }

    public void enableUnsort(boolean statud) {
        mnuUnsort.setVisible(statud);
        mnuUnsort.setEnabled(statud);
    }

    public void hideFontMenu() {
//        mnuCustomize.setVisible(false);
//        mnuCustomize.setEnabled(false);

        //  tableMenuToolbar.disableFontSettings();
    }

    public void hideCopywHeadMenu() {
//        mnuCopyH.setVisible(false);
//        mnuCopyH.setEnabled(false);
        copyWithH = false;
    }

    public void showLinkMenus() {
//        mnuLink.setVisible(true);
//        mnuLink.setEnabled(true);
//        mnuLinkH.setVisible(true);
//        mnuLinkH.setEnabled(true);
        linkToEx = true;
        // tableMenuToolbar.enableLinkToExcell();
    }

    public void setAutoWidthAdjustMenuVisible(boolean status) {
//        mnuResizeCols.setVisible(status);
//        mnuResizeCols.setEnabled(status);

    }

    public void hideShowTitleBar() {
        WindowWrapper frame = (WindowWrapper) g_oTable.getModel().getViewSettings().getParent();
        frame.setTitleVisible(!frame.isTitleVisible());
        setHideShowMenuCaption();
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        setHideShowMenuCaption();
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public TWMenuItem getCopyMenu() {
        mnuCopy = new TWMenuItem(Language.getString("COPY"), "copy.gif");
        mnuCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
        mnuCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ((SmartTable) g_oTable.getTable()).copyCells(false);
            }
        });
        return mnuCopy;
    }

    public TWMenuItem getCopyWithHeadMenu() {
        mnuCopyH = new TWMenuItem(Language.getString("COPY_WITH_HEAD"), "copyWH.gif");
        mnuCopyH.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        mnuCopyH.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ((SmartTable) g_oTable.getTable()).copyCells(true);
            }
        });
        return mnuCopyH;
    }


    public TWMenuItem getLinkMenu() {
        mnuLink = new TWMenuItem(Language.getString("LINK_TO_XL"), "link.gif");
        mnuLink.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK));
        mnuLink.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) { //DDE
                if (!Settings.isConnected()) {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                } else if (ExchangeStore.isValidSystemFinformationType(Meta.IT_DDE_Link)) {
                    ((DDELinkInterface) g_oTable.getModel()).getDDEString(g_oTable, false);
                } else {
                    SharedMethods.showNonBlockingMessage(Language.getString("DDE_NOT_SUBSCRIBED"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        mnuLink.setVisible(false);
        mnuLink.setEnabled(false);
        return mnuLink;
    }

    public TWMenuItem getLinkMenuWithHeaders() {
        mnuLinkH = new TWMenuItem(Language.getString("LINK_WITH_HEAD"), "linkWH.gif");
        mnuLinkH.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.ALT_DOWN_MASK));
        mnuLinkH.addActionListener(new ActionListener() { //DDE

            public void actionPerformed(ActionEvent e) {
                if (!Settings.isConnected()) {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                } else if (ExchangeStore.isValidSystemFinformationType(Meta.IT_DDE_Link)) {
                    ((DDELinkInterface) g_oTable.getModel()).getDDEString(g_oTable, true);
                } else {
                    SharedMethods.showNonBlockingMessage(Language.getString("DDE_NOT_SUBSCRIBED"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        mnuLinkH.setVisible(false);
        mnuLinkH.setEnabled(false);
        return mnuLinkH;
    }

    public void printMenuClicked() {

        if ((g_oTable.getWindowType() == ViewSettingsManager.DETAIL_QUOTE) ||
                (g_oTable.getWindowType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) ||
                (g_oTable.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_MFUND))
            printTable(PrintManager.TABLE_TYPE_DEFAULT_NO_HEAD);
        else if ((g_oTable.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_ORDER) ||
                (g_oTable.getWindowType() == ViewSettingsManager.FUND_TRANSFER_FRAME))
            printTableWithPanel(PrintManager.TABLE_TYPE_DEFAULT);
        else if ((g_oTable.getWindowType() == ViewSettingsManager.ACCOUNT_SUMMARY_VIEW))
            printTableWithPanel(PrintManager.TABLE_TYPE_DEFAULT_NO_HEAD);
        else if (g_oTable.getModel().getViewSettings().getID().equals("HISTORY_ANALYZER"))
            printTable(PrintManager.TABLE_TYPE_WATCHLIST); // ignore last col
        else if ((g_oTable.getWindowType() == ViewSettingsManager.SNAP_QUOTE) ||
                (g_oTable.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW))
            printTable(PrintManager.TABLE_TYPE_SNAP_QUOTE);
        else if ((g_oTable.getWindowType() == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW))
            printTableWithPanel(PrintManager.TABLE_TYPE_DEPTH_CALC);
        else if (g_oTable.getModel().getViewSettings().getMainType() == ViewSettingsManager.MIST_VIEW)
            SharedMethods.printTable(g_oTable.getTable(), PrintManager.TABLE_TYPE_MIST_VIEW, g_oTable.getTitle());
        else if (g_oTable.getWindowType() == ViewSettingsManager.CHART_WINDOW_VIEW)
            SharedMethods.printTable(g_oTable.getTable(), PrintManager.TABLE_TYPE_DEFAULT, g_oTable.getParent().getName());
        else if (g_oTable.getWindowType() == ViewSettingsManager.PORTFOLIO_VALUATION) {
            printPFTable(PrintManager.TABLE_TYPE_DEFAULT);
        } else if (g_oTable.getWindowType() == ViewSettingsManager.PORTFOLIO_TXN_HISTORY) {
            printPFTable(PrintManager.TABLE_TYPE_DEFAULT);
        } else
            printTable(PrintManager.TABLE_TYPE_DEFAULT);


    }

    public void setAsDefaultClicked() {
        if (g_oTable.getModel().getViewSettings().getParent() instanceof InternalFrame) {
            Table table1 = ((InternalFrame) g_oTable.getModel().getViewSettings().getParent()).getTableComponent();
            Table table2 = ((InternalFrame) g_oTable.getModel().getViewSettings().getParent()).getSubTableComponent();
            table1.getModel().saveColumnPositions();
            table1.getModel().getViewSettings().setFont(table1.getTable().getFont());
            DefaultSettingsManager.getSharedInstance().addDefaultSetting("" + table1.getModel().getViewSettings().getType(),
                    table1.getModel().getViewSettings().toWorkspaceString(false));
            try {
                if (table2 != null) {
                    table2.getModel().saveColumnPositions();
                    table2.getModel().getViewSettings().setFont(table2.getTable().getFont());
                    DefaultSettingsManager.getSharedInstance().addDefaultSetting("" + table2.getModel().getViewSettings().getType(),
                            table2.getModel().getViewSettings().toWorkspaceString(false));
                }
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            g_oTable.getModel().saveColumnPositions();
            g_oTable.getModel().getViewSettings().setFont(g_oTable.getTable().getFont());
            DefaultSettingsManager.getSharedInstance().addDefaultSetting("" + g_oTable.getModel().getViewSettings().getType(),
                    g_oTable.getModel().getViewSettings().toWorkspaceString(false));
        }
    }

    public void resetToDefaultClicked() {
        if (g_oTable.getModel().getViewSettings().getParent() instanceof InternalFrame) {
            Table table1 = ((InternalFrame) g_oTable.getModel().getViewSettings().getParent()).getTableComponent();
            Table table2 = ((InternalFrame) g_oTable.getModel().getViewSettings().getParent()).getSubTableComponent();
            DefaultSettingsManager.getSharedInstance().removeDefaultSetting("" + table1.getModel().getViewSettings().getType());
            try {
                if (table2 != null) {
                    DefaultSettingsManager.getSharedInstance().removeDefaultSetting("" + table2.getModel().getViewSettings().getType());
                }
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            DefaultSettingsManager.getSharedInstance().removeDefaultSetting("" + g_oTable.getModel().getViewSettings().getType());
        }

    }

    public void printTable(int type) {
        try {
            String title = ((JInternalFrame) g_oTable.getModel().getViewSettings().getParent()).getTitle();
            if (g_oTable.getModel().getViewSettings().getParent() instanceof InternalFrame) {
                JTable table1 = ((InternalFrame) (g_oTable.getModel().getViewSettings().getParent())).getTable1();
                JTable table2 = ((InternalFrame) (g_oTable.getModel().getViewSettings().getParent())).getTable2();
                if (table2 != null) {
                    Object[] tables = new Object[2];
                    tables[0] = table1;
                    tables[1] = table2;
                    SharedMethods.printTable(tables, type, title);
                    tables = null;
                    table2 = null;
                } else {
                    SharedMethods.printTable(g_oTable.getTable(), type, title);
                }

            } else {
                SharedMethods.printTable(g_oTable.getTable(), type, title);
            }
            title = null;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void printPFTable(int type) {
        try {
            String title = ((JInternalFrame) g_oTable.getModel().getViewSettings().getParent()).getTitle();
            if (g_oTable.getModel().getViewSettings().getParent() instanceof InternalFrame) {
                JTable table1 = ((InternalFrame) (g_oTable.getModel().getViewSettings().getParent())).getTable1();
                JTable table2 = ((InternalFrame) (g_oTable.getModel().getViewSettings().getParent())).getTable2();
                if (table2 != null) {
                    Object[] tables = new Object[2];
                    tables[0] = table1;
                    tables[1] = table2;
                    SharedMethods.printPFTable(tables, type, title);
                    tables = null;
                    table2 = null;
                } else {
                    SharedMethods.printTable(g_oTable.getTable(), type, title);
                }

            } else {
                SharedMethods.printTable(g_oTable.getTable(), type, title);
            }
            title = null;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void printTableWithPanel(int type) {
        try {
            String title = ((JInternalFrame) g_oTable.getModel().getViewSettings().getParent()).getTitle();
            if (g_oTable.getModel().getViewSettings().getParent() instanceof InternalFrame) {
                JTable table1 = ((InternalFrame) (g_oTable.getModel().getViewSettings().getParent())).getTable1();
                JTable table2 = ((InternalFrame) (g_oTable.getModel().getViewSettings().getParent())).getTable2();
                String[][] topStr = ((InternalFrame) (g_oTable.getModel().getViewSettings().getParent())).getPanelString();
                if (table2 != null) {
                    Object[] tables = new Object[2];
                    tables[0] = table1;
                    tables[1] = table2;
                    if (topStr != null)
                        SharedMethods.printTable(tables, type, title, topStr);
                    else
                        SharedMethods.printTable(tables, type, title);
                    tables = null;
                    table2 = null;
                } else {
                    Object[] tables = new Object[1];
                    tables[0] = g_oTable.getTable();
                    if (topStr != null)
                        SharedMethods.printTable(g_oTable.getTable(), type, title, topStr);
                    else
                        SharedMethods.printTable(g_oTable.getTable(), type, title);
                }

            } else {
                SharedMethods.printTable(g_oTable.getTable(), type, title);
            }
            title = null;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void setPrintTarget(Object target, int style) {

    }

    public void show(final Component invoker, final int x, final int y) {
        if (isTMTavailable) {
            int xPos = x;
            Point p = new Point(x, y);
            SwingUtilities.convertPointToScreen(p, invoker);
            if (p.x + Constants.COMMON_TABLE_MENU_TOOLBAR_WIDTH > screenWidth) {
                p.x = screenWidth - Constants.COMMON_TABLE_MENU_TOOLBAR_WIDTH;
                SwingUtilities.convertPointFromScreen(p, invoker);
                xPos = p.x;
            }
            final int adjX = xPos;
            new Thread() {
                public void run() {
                    CommonTableMenuToolbar.getSharedInstance().setG_oTable(g_oTable);
                    CommonTableMenuToolbar.getSharedInstance().setParentpopup(CommonPopup.this);
                    CommonTableMenuToolbar.getSharedInstance().setButtonStatus(copyWithH, linkToEx, setAsDe, setAsDe, print, setCuztomizer, colSetings, true, resize);

                    CommonTableMenuToolbar.getSharedInstance().showWindow(invoker, adjX, y);
                }
            }.start();
            boolean isanyVisable = isAnyItemVisable();
            if (isanyVisable) {
                setCuztomizer = true; //To enable customizer
                super.show(invoker, xPos, y);
            }
        }
    }

    public boolean isAnyItemVisable() {
        Component cmp[] = this.getComponents();

        if (cmp != null && cmp.length > 0) {
            for (int i = 0; i < cmp.length; i++) {
                if (cmp[i].isVisible() && cmp[i].isEnabled()) {
                    return true;
                }
            }
        }
        return false;
    }
}

