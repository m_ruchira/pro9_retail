package com.isi.csvr.table;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 17-Jun-2008 Time: 17:59:58 To change this template use File | Settings
 * | File Templates.
 */
public class ForexDetailQuoteModel extends CommonTable
        implements DetailQuote, TableModel, CommonTableInterface {

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private String symbol;
    private long timeOffset;

    /**
     * Constructor
     */
    public ForexDetailQuoteModel() {
    }

    public void setSymbol(String symbolIn) {
        symbol = symbolIn;
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 4;
    }

    public int getRowCount() {
        return 4;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            if (symbol == null)
                return "";
            Stock index = DataStore.getSharedInstance().getStockObject(symbol);
            timeOffset = index.getTimeOffset();
            if (index == null)
                return "";

            switch (iRow) {
                case -1:
                    return index.getInstrumentType();
                case 0:
                    switch (iCol) {
                        case 0:
                            return Language.getString("SYMBOL");
                        case 1:
                            return index.getSymbol();
                        case 2:
                            return Language.getString("SPREAD");
                        case 3:
                            return doubleTransferObject.setValue(calSpread(index));
                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("HEAT_MAP_LBL_LAST_TRADE");
                        case 1:
                            return doubleTransferObject.setValue(index.getLastTradeValue());
                        case 2:
                            return Language.getString("RANGE");
                        case 3:
                            return doubleTransferObject.setValue(index.getRange());
                    }
                case 2:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BEST_BID");
                        case 1:

                            return doubleTransferObject.setValue(index.getBestBidPrice());
                        case 2:
                            return Language.getString("HIGH");
                        case 3:
                            return doubleTransferObject.setValue(index.getHigh());
                    }
                case 3:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BEST_ASK");
                        case 1:
                            return doubleTransferObject.setValue(index.getBestAskPrice());
                        case 2:
                            return Language.getString("LOW");
                        case 3:
                            return doubleTransferObject.setValue(index.getLow());
                    }

            }


            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 1;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 5;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 3:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 5;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
//           case 4:
//                switch (iCol) {
//                    case 0:
//                        return 2;
//                    case 1:
//                        return 4;
//                    case 2:
//                        return 2;
//                    case 3:
//                        return 3;
//                }
        }
//        switch (iRow) {
//            case 0:
//                switch (iCol) {
//                    case 0:
//                        return 2;
//                    case 1:
//                        return 3;
//                    case 2:
//                        return 2;
//                    case 3:
//                        return 3;
//                }
//            case 1:
//                switch (iCol) {
//                    case 0:
//                        return 2;
//                    case 1:
//                        return 5;
//                    case 2:
//                        return 2;
//                    case 3:
//                        return 3;
//                }
//            case 2:
//                switch (iCol) {
//                    case 0:
//                        return 2;
//                    case 1:
//                        return 6;
//                    case 2:
//                        return 2;
//                    case 3:
//                        return 3;
//                }
//            case 3:
//                switch (iCol) {
//                    case 0:
//                        return 2;
//                    case 1:
//                        return 4;
//                    case 2:
//                        return 2;
//                    case 3:
//                        return 3;
//                }
//        }
        return 0;
    }

    public long getTimeOffset() {
        return timeOffset;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 3);
        customizerRecords[3] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_VALUE_UP_ROW, Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_VALUE_DOWN_ROW, Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    public double calSpread(Stock stock) {
        if ((stock.getBestAskPrice() == 0) && (stock.getBestBidPrice() == 0)) {
//            return 0;
            return Double.NaN;
        } else if (stock.getBestAskPrice() == 0) {
//            return 0;
            return Double.POSITIVE_INFINITY;
        } else if (stock.getBestBidPrice() == 0) {
//            return 0;
            return Double.NEGATIVE_INFINITY;
        } else {
            return (stock.getBestAskPrice() - stock.getBestBidPrice());
        }
    }
}
