// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Bandula
 */

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class MutualFundDetailQuoteModel extends CommonTable
        implements DetailQuote, TableModel, CommonTableInterface {

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private String index;

    /**
     * Constructor
     */
    public MutualFundDetailQuoteModel() {
    }

    public void setSymbol(String index) {
        this.index = index;
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 4;
    }

    public int getRowCount() {
        return 5;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            Stock fund = DataStore.getSharedInstance().getStockObject(index);
            if (fund == null)
                return "";

            switch (iRow) {
                case -1:
                    return fund.getInstrumentType();
                case 0:
                    switch (iCol) {
                        case 0:
                            return Language.getString("NAV");
                        case 1:
                            return doubleTransferObject.setValue(fund.getNetAssetValue());
                        case 2:
                            return Language.getString("CURRENCY");
                        case 3:
                            return fund.getCurrencyCode();
                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("SIZE");
                        case 1:
                            return doubleTransferObject.setValue(fund.getMarketCap());
                        case 2:
                            return Language.getString("MANAGEMENT_FEE");
                        case 3:
                            return doubleTransferObject.setValue(fund.getMgtFee());
                    }
                case 2:
                    switch (iCol) {
                        case 0:
                            return Language.getString("YTD");
                        case 1:
                            return doubleTransferObject.setValue(fund.getPerformanceYTD());
                        case 2:
                            return Language.getString("MONTHS_12");
                        case 3:
                            return doubleTransferObject.setValue(fund.getPerformance12M());
                    }
                case 3:
                    switch (iCol) {
                        case 0:
                            return Language.getString("3_YEAR");
                        case 1:
                            return doubleTransferObject.setValue(fund.getPerformance3Y());
                        case 2:
                            return Language.getString("5_YEAR");
                        case 3:
                            return doubleTransferObject.setValue(fund.getPerformance5Y());
                    }
                case 4:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BENCHMARK");
                        case 1:
                            return fund.getBenchmark();
                        case 2:
                            return Language.getString("DEALING_DEADLINE");
                        case 3:
                            return longTrasferObject.setValue(fund.getDeadline());
                    }
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                    case 2:
                        return 2;
                    case 1:
                        return 3;
                    case 3:
                        return 1;
                }
            case 1:
                switch (iCol) {
                    case 0:
                    case 2:
                        return 2;
                    case 1:
                    case 3:
                        return 3;
                }
            case 2:
                switch (iCol) {
                    case 0:
                    case 2:
                        return 2;
                    case 1:
                    case 3:
                        return 3;
                }
            case 3:
                switch (iCol) {
                    case 0:
                    case 2:
                        return 2;
                    case 1:
                    case 3:
                        return 3;
                }
            case 4:
                switch (iCol) {
                    case 0:
                    case 2:
                        return 2;
                    case 1:
                        return 1;
                    case 3:
                        return 'A';
                }
        }
        return 0;
    }

    public long getTimeOffset() {
        return 0;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 3);
        customizerRecords[3] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_VALUE_UP_ROW, Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_VALUE_DOWN_ROW, Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }
}

