/*
 * (swing1.1beta3)
 *
 */

package com.isi.csvr.table;

import com.isi.csvr.shared.Language;

import javax.swing.*;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;


/**
 * @version 1.0 11/26/98
 */

public class MultiSpanCellTableUI extends BasicTableUI {

    //private boolean RTL = false;

    public MultiSpanCellTableUI(boolean rtl) {
        super();
        //RTL = rtl;
    }

    private void paintGrid(Graphics g, int rMin, int rMax, int cMin, int cMax) {
        g.setColor(table.getGridColor());

        Rectangle minCell = table.getCellRect(rMin, cMin, true);
        Rectangle maxCell = table.getCellRect(rMax, cMax, true);
        Rectangle damagedArea = minCell.union(maxCell);

        if (table.getShowHorizontalLines()) {
            int tableWidth = damagedArea.x + damagedArea.width;
            int y = damagedArea.y;
            for (int row = rMin; row <= rMax; row++) {
                y += table.getRowHeight(row);
                g.drawLine(damagedArea.x, y - 1, tableWidth - 1, y - 1);
            }
        }
        if (table.getShowVerticalLines()) {
            TableColumnModel cm = table.getColumnModel();
            int tableHeight = damagedArea.y + damagedArea.height;
            int x;
            if (table.getComponentOrientation().isLeftToRight()) {
                x = damagedArea.x;
                for (int column = cMin; column <= cMax; column++) {
                    int w = cm.getColumn(column).getWidth();
                    x += w;
                    g.drawLine(x - 1, 0, x - 1, tableHeight - 1);
                }
            } else {
                x = damagedArea.x + damagedArea.width;
                for (int column = cMin; column < cMax; column++) {
                    int w = cm.getColumn(column).getWidth();
                    x -= w;
                    g.drawLine(x - 1, 0, x - 1, tableHeight - 1);
                }
                x -= cm.getColumn(cMax).getWidth();
                g.drawLine(x, 0, x, tableHeight - 1);
            }
        }
    }

    public void paint(Graphics g, JComponent c) {
        Rectangle oldClipBounds = g.getClipBounds();
        Rectangle clipBounds = new Rectangle(oldClipBounds);
        int tableWidth = table.getColumnModel().getTotalColumnWidth();
        clipBounds.width = Math.min(clipBounds.width, tableWidth);
        g.setClip(clipBounds);

        int firstIndex = table.rowAtPoint(new Point(0, clipBounds.y));
        int lastIndex = table.getRowCount() - 1;

        Rectangle rowRect = new Rectangle(0, 0,
                tableWidth, table.getRowHeight() + table.getRowMargin());
        rowRect.y = firstIndex * rowRect.height;

        for (int index = firstIndex; index <= lastIndex; index++) {
            if (rowRect.intersects(clipBounds)) {
                paintRow(g, index);
            }
            rowRect.y += rowRect.height;
        }
        g.setClip(oldClipBounds);
    }

    private void paintRow(Graphics g, int row) {
        Rectangle rect = g.getClipBounds();
        // boolean drawn  = false;


        int numColumns = table.getColumnCount();

        for (int column = 0; column < numColumns; column++) {
            Rectangle cellRect = table.getCellRect(row, column, true);

            if (row == 9) {
                if (Language.isLTR())
                    cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true),
                            table.getCellRect(row, 2, true), table.getCellRect(row, 3, true));
                else
                    cellRect = combine(table.getCellRect(row, 3, true), table.getCellRect(row, 2, true),
                            table.getCellRect(row, 1, true), table.getCellRect(row, 0, true));
            }
            if (row == 10) {
                if (((column == 0) || (column == 1))) {
                    if (Language.isLTR())
                        cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
                    else
                        cellRect = combine(table.getCellRect(row, 1, true), table.getCellRect(row, 0, true));
                } else if (((column == 2) || (column == 3))) {
                    if (Language.isLTR())
                        cellRect = combine(table.getCellRect(row, 2, true), table.getCellRect(row, 3, true));
                    else
                        cellRect = combine(table.getCellRect(row, 3, true), table.getCellRect(row, 2, true));
                }
            }
            if (cellRect.intersects(rect)) {
                //drawn = true;
                paintCell(g, cellRect, row, column);
            } //else {
            //   if (drawn) break;
            //}

        }
    }

    private void paintCell(Graphics g, Rectangle cellRect, int row, int column) {
        int spacingHeight = table.getRowMargin();
        int spacingWidth = table.getColumnModel().getColumnMargin();

        try {
            cellRect.setBounds(cellRect.x + spacingWidth / 2,
                    cellRect.y + spacingHeight / 2,
                    cellRect.width - spacingWidth, cellRect.height - spacingHeight);

            if (table.isEditing() && table.getEditingRow() == row &&
                    table.getEditingColumn() == column) {
                Component component = table.getEditorComponent();
                component.setBounds(cellRect);
                component.validate();
            } else {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component component = table.prepareRenderer(renderer, row, column);

                if (component.getParent() == null) {
                    rendererPane.add(component);
                }
                rendererPane.paintComponent(g, component, table, cellRect.x, cellRect.y,
                        cellRect.width, cellRect.height, true);
                if (table.getShowHorizontalLines()) {
                    g.setColor(table.getGridColor());
                    g.drawLine(cellRect.x, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                    if (table.getRowCount() == row + 1) {
                        g.drawLine(cellRect.x, cellRect.y + cellRect.height, cellRect.x + cellRect.width, cellRect.y + cellRect.height);
                    }
                }
                if (table.getShowVerticalLines()) {
                    g.setColor(table.getGridColor());
                    g.drawLine(cellRect.x, cellRect.y, cellRect.x, cellRect.y + cellRect.height);
                    if (table.getColumnCount() == column + 1) {
                        g.drawLine(cellRect.x + cellRect.width, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private Rectangle combine(Rectangle sRect1, Rectangle sRect2) {
        Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                (int) sRect1.getWidth() + (int) sRect2.getWidth(), (int) sRect1.getHeight());
        return sRect;
    }

    private Rectangle combine(Rectangle sRect1, Rectangle sRect2, Rectangle sRect3, Rectangle sRect4) {
        Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                (int) sRect1.getWidth() + (int) sRect2.getWidth() +
                        (int) sRect3.getWidth() + (int) sRect4.getWidth(), (int) sRect1.getHeight());
        return sRect;
    }
}

