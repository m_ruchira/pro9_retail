package com.isi.csvr.table;

import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import java.util.Vector;

/**
 * GroupableTableHeader
 *
 * @author Nobuo Tamemasa
 * @version 1.0 10/20/98
 */

public class TWTableHeader extends JTableHeader {
    private static final String uiClassID = "TWTableHeaderUI";
    protected Vector columnGroups = null;


    public TWTableHeader(TableColumnModel model) {
        super(model);
        setUI(new TWTableHeaderUI());
    }

    public void updateUI() {
        try { //Bug ID <#0015> included the try/catch
            super.updateUI();
            setUI(new TWTableHeaderUI());
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

}

