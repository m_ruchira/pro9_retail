package com.isi.csvr.table.updator;

import com.isi.csvr.shared.GUISettings;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA. User: admin Date: 09-Oct-2007 Time: 13:14:53 To change this template use File | Settings |
 * File Templates.
 */
public class SymbolPanel extends JPanel implements MouseListener, ActionListener {
    private static final int ROW_1_HEIGHT = 15;
    private static final int ROW_2_HEIGHT = 18;
    private static final int PANEL_WIDTH = 1000;
    public static SymbolPanel self;

    public SymbolPanel() {
//        this.setPreferredSize(new Dimension(1010,35));
        this.setLayout(new FlexGridLayout(new String[]{"20%", "20%", "20%", "20%", "20%"}, new String[]{"100%"}, 1, 1));
        for (int i = 0; i < 5; i++) {
            SymbolPanelComponent spComp = new SymbolPanelComponent(i);
//            (new Thread(spComp,"Symbol Panel " + i)).start();
            this.add(spComp);
        }
        GUISettings.applyOrientation(this);
    }

    public static SymbolPanel getInstance() {
        if (self == null) {
            self = new SymbolPanel();
        }
        return self;
    }

    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
