package com.isi.csvr.table.updator;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.table.UpdateableTable;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 29, 2005
 * Time: 10:17:10 AM
 */
public class TableUpdator extends Thread {

    private List<UpdateableTable> tables;
    private boolean active = true;
    private Constants.ThreadTypes id;
    private long sleepTime = 1000;

    public TableUpdator(Constants.ThreadTypes id, long sleepTime) {
        super("Table Updator : " + id);
        this.id = id;
        this.sleepTime = sleepTime;
        tables = Collections.synchronizedList(new LinkedList<UpdateableTable>());
        start();
    }

    public synchronized void register(UpdateableTable table) {
        tables.add(table);
    }

    public synchronized void unregister(UpdateableTable table) {
        tables.remove(table);
    }

    public Constants.ThreadTypes getUpdatorId() {
        return id;
    }

    public void run() {

        while (active) {
            try { // make sure for loop does not exit
                for (UpdateableTable table : tables) { // refresh all tables
                    try { // ignore any exceptions while refreshing a table
                        table.runThread();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try { // no interruptions please
                Thread.sleep(sleepTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
