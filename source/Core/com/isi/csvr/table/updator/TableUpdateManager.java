package com.isi.csvr.table.updator;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.table.UpdateableTable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 7, 2005
 * Time: 9:01:39 AM
 * To change this template use File | Settings | File Templates.
 */
public class TableUpdateManager {
    private static List<TableUpdator> updators = null;

    public synchronized static void addTable(UpdateableTable table) {
        if (updators == null) {
            updators = Collections.synchronizedList(new ArrayList<TableUpdator>());
        }

        TableUpdator updator = getUpdator(table.getTableID());
        updator.register(table);
    }

    public synchronized static void removeTable(UpdateableTable table) {
        TableUpdator updator = getUpdator(table.getTableID());
        updator.unregister(table);
    }

    private synchronized static TableUpdator getUpdator(Constants.ThreadTypes id) {
        for (TableUpdator updator : updators) {
            if (updator.getUpdatorId().equals(id)) {
                return updator;
            }
        }
        long sleepTime = 0;
        if (id.equals(Constants.ThreadTypes.TIME_N_SALES)) {
            sleepTime = 500;
        } else if (id.equals(Constants.ThreadTypes.MAIN_BOARDS)) {
            sleepTime = 100;
        } else if (id.equals(Constants.ThreadTypes.SYMBOL_PANEL)) {
            sleepTime = 500;
        } else if (id.equals(Constants.ThreadTypes.INDEX_PANEL)) {
            sleepTime = 1500;
        } else {
            sleepTime = 1000;
        }
//        System.out.println("table ID = "+id +" ,sleepTime = "+ sleepTime);
        TableUpdator newUpdator = new TableUpdator(id, sleepTime);
        updators.add(newUpdator);
        return newUpdator;
    }

}
