package com.isi.csvr.table;

// Copyright (c) 2000 Home

import com.isi.csvr.shared.Settings;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.Vector;

public class StaticTradeStore extends Object {
    Vector g_oTrades;

    /**
     * Constructor
     */
    public StaticTradeStore(String sSymbol) throws Exception {
        g_oTrades = new Vector(1000, 500);
        loadData(sSymbol);
        g_oTrades.trimToSize();
        //System.out.println("---------------> "+size());
    }

    public String getItem(int iRow, int iCol) {
        return ((String[]) g_oTrades.elementAt(iRow))[iCol];
    }

    /**
     * This reads the flat files in the \ FlatDatbase\Current
     * First line has all the information for the stock object
     * Other lines has the trade information ( time , price , volume )
     */
    private void loadData(String sSymbol) throws Exception {
        String sLine = null;
        String[] asLine;
        FileInputStream oFileIn = new FileInputStream(Settings.CURRENT_FLAT_DB_PATH + "\\" + sSymbol + ".csv");
        BufferedReader oIn = new BufferedReader(new InputStreamReader(oFileIn));

        while (true) {
            sLine = oIn.readLine();
            //System.out.println(sLine);
            asLine = getArray(sLine);
            if (asLine != null)
                g_oTrades.add(asLine);
            else
                break;
        }
        oIn.close();
        oFileIn.close();
    }

    /**
     * Breake the record into fields and store in an array
     */
    private String[] getArray(String sLine) {
        String[] asFileds = new String[8];


        try {
            StringTokenizer oTokens = new StringTokenizer(sLine, ",");

            //for (int i=0;i<8;i++)
            //    asFileds[i] = oTokens.nextToken();
            int i = 0;
            while (oTokens.hasMoreTokens()) {
                asFileds[i] = oTokens.nextToken();
                i++;
            }
            //asFileds[5] = ""; //session
            //asFileds[6] = ""; // broker
            //asFileds[7] = ""; // broker
            return asFileds;
        } catch (Exception e) {
            return null;
        }
    }

    public int size() {
        return g_oTrades.size();
    }
}