package com.isi.csvr.table;

import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.*;

public class SmartFileChooser extends JFileChooser {

    public SmartFileChooser(String path) {
        super(path);
        localizeHomeButton(this);
    }

    public AbstractButton getButton(String caption) {
        return enumerate(caption, this);
    }

    private AbstractButton enumerate(String caption, Container cont) {

        if (!(cont instanceof Container)) {
            return null;
        }

        int n = cont.getComponentCount();
        AbstractButton btn = null;

        for (int i = 0; i < n; i++) {
            Component comp = cont.getComponent(i);
            if (comp instanceof AbstractButton) {
                btn = (AbstractButton) comp;
                if ((btn.getText() != null) && (btn.getText().equals(caption))) {
                    return btn;
                }
            } else if (comp instanceof Container) {
                btn = enumerate(caption, (Container) comp);
                if (btn != null)
                    return btn;
            }
            //break;
        } // end of for
        return null;
    }

    public void hideContainerOf(String caption) {
        hidePanel(0, caption, this);
    }

    public void hideContainerOf(int level, String caption) {
        hidePanel(level, caption, this);
    }

    private void localizeHomeButton(Container cont) {
        //System.out.println("localizing");
        if (!(cont instanceof Container)) {
            return;
        }
        int n = cont.getComponentCount();
        AbstractButton btn = null;

        for (int i = 0; i < n; i++) {
            Component comp = cont.getComponent(i);
            if (comp instanceof AbstractButton) {
                btn = (AbstractButton) comp;
                if ((btn.getToolTipText() != null) && (btn.getToolTipText().equals("Desktop"))) {
                    btn.setToolTipText(Language.getString("MY_DOCUMENTS"));
                    return;
                }
            } else if (comp instanceof Container) {
                localizeHomeButton((Container) comp);
            }
        }
    }

    private void hidePanel(int level, String caption, Container cont) {

        if (!(cont instanceof Container)) {
            return;
        }
        int n = cont.getComponentCount();
        AbstractButton btn = null;
        JLabel lbl;

        for (int i = 0; i < n; i++) {
            Component comp = cont.getComponent(i);
            if (comp instanceof AbstractButton) {
                btn = (AbstractButton) comp;
                if ((btn.getText() != null) && (btn.getText().equals(caption))) {
                    //cont.setVisible(false);
                    hideObject(level, cont);
                    return;
                }
            } else if (comp instanceof JLabel) {
                lbl = (JLabel) comp;
                if ((lbl.getText() != null) && (lbl.getText().equals(caption))) {
                    hideObject(level, cont);
                    //cont.setVisible(false);
                    return;
                }
            } else if (comp instanceof Container) {
                hidePanel(level, caption, (Container) comp);
            }
        }
    }

    private void hideObject(int level, Component source) {
        Component object = source;
        try {
            for (int i = 0; i < level; i++) {
                object = (Component) object.getParent();
            }
            object.setVisible(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}