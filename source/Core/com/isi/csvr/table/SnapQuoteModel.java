// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Bandula
 */

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.marketdepth.DepthObject;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class SnapQuoteModel extends CommonTable implements TableModel, CommonTableInterface {

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private String symbol;
    //private DepthObject depth;
    //private BidAsk bid;
    //private BidAsk ask;

    /**
     * Constructor
     */
    public SnapQuoteModel() {
    }

    public void setSymbol(String symbolIn) {
        symbol = symbolIn;
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 4;
    }

    public int getRowCount() {
        return 16;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            if (symbol == null)
                return "";
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            //depth = DepthStore.getInstance().getDepthFor(symbol);
            //ask = (BidAsk)depth.getOrderList(DepthObject.ASK).get(iRow);
            //bid = (BidAsk)depth.getOrderList(DepthObject.BID).get(iRow);

            if (stock == null)
                return "";

            switch (iRow) {
                case 0:
                    switch (iCol) {
                        case 0:
                            return Language.getString("LAST_TRADE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getLastTradeValue());
                        case 2:
                            return Language.getString("BEST_BID");
                        case 3:
                            return doubleTransferObject.setValue(stock.getBestBidPrice());
                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("LASTTRADE_QTY");
                        case 1:
                            return longTrasferObject.setValue(stock.getTradeQuantity());
                        case 2:
                            return Language.getString("BID_QTY");
                        case 3:
                            return longTrasferObject.setValue(stock.getBestBidQuantity());
                    }
                case 2:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CHANGE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getChange());
                        case 2:
                            return Language.getString("BEST_ASK");
                        case 3:
                            return doubleTransferObject.setValue(stock.getBestAskPrice());
                    }
                case 3:
                    switch (iCol) {
                        case 0:
                            return Language.getString("PCT_CHANGE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getPercentChange());
                        case 2:
                            return Language.getString("OFFER_QTY");
                        case 3:
                            return longTrasferObject.setValue(stock.getBestAskQuantity());
                    }
                case 4:
                    switch (iCol) {
                        case 0:
                            return Language.getString("VWAP");
                        case 1:
                            return doubleTransferObject.setValue(stock.getAvgTradePrice());
                        case 2:
                            return Language.getString("OPEN");
                        case 3:
                            return doubleTransferObject.setValue(stock.getTodaysOpen());
                    }
                case 5:
                    switch (iCol) {
                        case 0:
                            return Language.getString("VOLUME");
                        case 1:
                            return longTrasferObject.setValue(stock.getVolume());
                        case 2:
                            return Language.getString("HIGH");
                        case 3:
                            return doubleTransferObject.setValue(stock.getHigh());
                    }
                case 6:
                    switch (iCol) {
                        case 0:
                            return Language.getString("TURNOVER");
                        case 1:
                            return doubleTransferObject.setValue(stock.getTurnover());
                        case 2:
                            return Language.getString("LOW");
                        case 3:
                            return doubleTransferObject.setValue(stock.getLow());
                    }
                case 7:
                    switch (iCol) {
                        case 0:
                            return Language.getString("TRADES");
                        case 1:
                            return longTrasferObject.setValue(stock.getNoOfTrades());
                        case 2:
                            return Language.getString("52WK_HIGH");
                        case 3:
                            return doubleTransferObject.setValue(stock.getHighPriceOf52Weeks());
                    }
                case 8:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CLOSE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getTodaysClose());
                        case 2:
                            return Language.getString("52WK_LOW");
                        case 3:
                            return doubleTransferObject.setValue(stock.getLowPriceOf52Weeks());
                    }

                case 9:
                    switch (iCol) {
                        case 0:
                            return "";
                        case 1:
                            return "";
                        case 2:
                            return "";
                        case 3:
                            return Language.getString("MARKET_DEPTH_BY_PRICE");
                    }
                case 10:
                    switch (iCol) {
                        case 0:
                            return "";//Language.getString("LBL_BID");
                        case 1:
                            return Language.getString("BID");
                        case 2:
                            return "";//Language.getString("OFFER");;
                        case 3:
                            return Language.getString("OFFER");
                    }

                case 11:
                    switch (iCol) {
                        case 0:
                            return doubleTransferObject.setValue(getPrice(DepthObject.BID, 0));
                        case 1:
                            return longTrasferObject.setValue(getQuantity(DepthObject.BID, 0));
                        case 2:
                            return doubleTransferObject.setValue(getPrice(DepthObject.ASK, 0));
                        case 3:
                            return longTrasferObject.setValue(getQuantity(DepthObject.ASK, 0));
                    }
                case 12:
                    switch (iCol) {
                        case 0:
                            return doubleTransferObject.setValue(getPrice(DepthObject.BID, 1));
                        case 1:
                            return longTrasferObject.setValue(getQuantity(DepthObject.BID, 1));
                        case 2:
                            return doubleTransferObject.setValue(getPrice(DepthObject.ASK, 1));
                        case 3:
                            return longTrasferObject.setValue(getQuantity(DepthObject.ASK, 1));
                    }
                case 13:
                    switch (iCol) {
                        case 0:
                            return doubleTransferObject.setValue(getPrice(DepthObject.BID, 2));
                        case 1:
                            return longTrasferObject.setValue(getQuantity(DepthObject.BID, 2));
                        case 2:
                            return doubleTransferObject.setValue(getPrice(DepthObject.ASK, 2));
                        case 3:
                            return longTrasferObject.setValue(getQuantity(DepthObject.ASK, 2));
                    }
                case 14:
                    switch (iCol) {
                        case 0:
                            return doubleTransferObject.setValue(getPrice(DepthObject.BID, 3));
                        case 1:
                            return longTrasferObject.setValue(getQuantity(DepthObject.BID, 3));
                        case 2:
                            return doubleTransferObject.setValue(getPrice(DepthObject.ASK, 3));
                        case 3:
                            return longTrasferObject.setValue(getQuantity(DepthObject.ASK, 3));
                    }
                case 15:
                    switch (iCol) {
                        case 0:
                            return doubleTransferObject.setValue(getPrice(DepthObject.BID, 4));
                        case 1:
                            return longTrasferObject.setValue(getQuantity(DepthObject.BID, 4));
                        case 2:
                            return doubleTransferObject.setValue(getPrice(DepthObject.ASK, 4));
                        case 3:
                            return longTrasferObject.setValue(getQuantity(DepthObject.ASK, 4));
                    }
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[8];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("BID_ROW_1"), FIELD_DEP_BY_ORD_BID_ROW1, Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR1"), Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR1"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("BID_ROW_2"), FIELD_DEP_BY_ORD_BID_ROW2, Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR2"), Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR2"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("OFFER_ROW_1"), FIELD_DEP_BY_ORD_ASK_ROW1, Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR1"), Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR1"));
        customizerRecords[7] = new CustomizerRecord(Language.getString("OFFER_ROW_2"), FIELD_DEP_BY_ORD_ASK_ROW2, Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR2"), Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR2"));
        return customizerRecords;
    }

    private double getPrice(byte type, int row) {
        try {
            if (isWithinValidDepth(type, row))
                return ((BidAsk) DepthStore.getInstance().getDepthFor(symbol).getPriceList(type).get(row)).getPrice();
            else
                return -1;
        } catch (Exception ex) {
            return -1;
        }
    }

    private long getQuantity(byte type, int row) {
        try {
            if (isWithinValidDepth(type, row))
                return ((BidAsk) DepthStore.getInstance().getDepthFor(symbol).getPriceList(type).get(row)).getQuantity();
            else
                return -1;
        } catch (Exception ex) {
            return -1;
        }
    }

    private boolean isWithinValidDepth(byte type, int row) {
        try {
            DepthObject depth = DepthStore.getInstance().getDepthFor(symbol);
            int size = 0;
            for (int i = 0; i < depth.getPriceList(type).size(); i++) {
                if (((BidAsk) depth.getPriceList(type).get(i)).getQuantity() == 0)
                    break;
                else
                    size++;
            }
            return row < size;
        } catch (Exception ex) {
            return false;
        }
    }

}


