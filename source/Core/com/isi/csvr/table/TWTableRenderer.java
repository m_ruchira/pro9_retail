package com.isi.csvr.table;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 8, 2003
 * Time: 10:52:15 AM
 * To change this template use Options | File Templates.
 */
public interface TWTableRenderer {
    public static final int PROPERTY_FONT_CHANGED = 1;
    public static final int PROPERTY_TABLE_COLORS_CHANGED = 2;

    public void initRenderer(String[] asColumns, int[] asRendIDs);

    public void propertyChanged(int property);

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column);
}
