package com.isi.csvr.table;

import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Jul 1, 2008
 * Time: 3:05:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class TWTableColumnModel extends DefaultTableColumnModel {


    public TWTableColumnModel() {
        super();
    }

    public void addColumn(TableColumn aColumn) {
//        System.out.println("tableColumns addColumn count before"+tableColumns.size());
//        synchronized (LOCK) {
        super.addColumn(aColumn);
//        }
//        System.out.println("tableColumns addColumn count after"+tableColumns.size());
    }

    public void removeColumn(TableColumn column) {
//        synchronized (LOCK) {
//            System.out.println("tableColumns removeColumn count before and ccolumn"+tableColumns.size() + " , "+column.getHeaderValue());
//            System.out.println("tableColumns removeColumn ccolumn"+column.getHeaderValue());
        super.removeColumn(column);    //To change body of overridden methods use File | Settings | File Templates.
//        }
//        System.out.println("tableColumns removeColumn count after"+tableColumns.size());
    }

    public void moveColumn(int columnIndex, int newIndex) {
//        System.out.println("tableColumns moveColumn count "+ tableColumns.size());
//        System.out.println("tableColumns moveColumn columnIndex and newIndex"+ columnIndex + " , " + newIndex);
//        synchronized (LOCK) {
        super.moveColumn(columnIndex, newIndex);    //To change body of overridden methods use File | Settings | File Templates.
//        }
    }

    public int getColumnCount() {
//        System.out.println("tableColumns getColumnCount count "+tableColumns.size());
        return super.getColumnCount();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public Enumeration<TableColumn> getColumns() {
//        System.out.println("tableColumns getColumns count "+tableColumns.size());
        return super.getColumns();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public int getColumnIndex(Object identifier) {
//        if(tableColumns.size() < 119){
//            System.out.println("tableColumns getColumnIndex count "+tableColumns.size());
//            System.out.println("tableColumns getColumnIndex identifier"+identifier);
//            System.out.println("tableColumns getColumnIndex identifier and index "+identifier + " , "+ super.getColumnIndex(identifier));
//        }
//        synchronized (LOCK) {
        return super.getColumnIndex(identifier);    //To change body of overridden methods use File | Settings | File Templates.
//        }
    }

    public TableColumn getColumn(int columnIndex) {
//        if (tableColumns.size() < 119) {
//            System.out.println("tableColumns getColumn count "+tableColumns.size());
//            System.out.println("tableColumns getColumn columnIndex and column "+columnIndex + " , "+ super.getColumn(columnIndex).getHeaderValue());
//        }
//        synchronized (LOCK) {
        try {
            return super.getColumn(columnIndex);    //To change body of overridden methods use File | Settings | File Templates.
        } catch (Exception e) {
            System.out.println("tableColumns getColumn count and columnIndex" + tableColumns.size() + " , " + columnIndex);
//                System.out.println("tableColumns getColumn columnIndex "+columnIndex);
            System.out.println("tableColumns getColumn columnIndex and column " + columnIndex + " , " + super.getColumn(columnIndex).getHeaderValue());
        }
        return super.getColumn(columnIndex);
//        }
    }
}
