package com.isi.csvr;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 10, 2007
 * Time: 5:30:12 PM
 */

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.mist.MISTTable;
import com.isi.csvr.shared.Language;
import com.isi.csvr.ticker.GlobalSettings;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.advanced.HolderPanel;
import com.isi.csvr.ticker.custom.Ticker;
import com.isi.csvr.ticker.custom.TickerFrame;
import com.isi.csvr.ticker.custom.TradeTicker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

//import com.isi.csvr.ticker.TradeTicker;

public class DetachableRootPane extends JRootPane {

    /**
     * The name of a {@linkplain JComponent#getClientProperty(Object) client
     * property} that indicates that the {@link JInternalFrame} this {@link
     * DetachableRootPane} is "leaving" was resizable.
     */
    private static final String INTERNAL_FRAME_RESIZABLE = "internalFrameResizable";

    /**
     * The name of a {@linkplain JComponent#getClientProperty(Object) client
     * property} that indicates that the {@link JInternalFrame} this {@link
     * DetachableRootPane} is "leaving" was closable.
     */
    private static final String INTERNAL_FRAME_CLOSABLE = "internalFrameClosable";

    /**
     * The name of a {@linkplain JComponent#getClientProperty(Object) client
     * property} that indicates that the {@link JInternalFrame} this {@link
     * DetachableRootPane} is "leaving" was maximizable.
     */
    private static final String INTERNAL_FRAME_MAXIMIZABLE = "internalFrameMaximizable";

    /**
     * The name of a {@linkplain JComponent#getClientProperty(Object) client
     * property} that indicates that the {@link JInternalFrame} this {@link
     * DetachableRootPane} is "leaving" was iconifiable.
     */
    private static final String INTERNAL_FRAME_ICONIFIABLE = "internalFrameIconifiable";
    public static DetachableRootPane self;
    public static int initialPaneWidth;
    public static int resizedPaneWidth;
    public static boolean FIRST_TIME_DETACHED = true;
    public static boolean IS_DETACHED = false;
    public static JInternalFrame j;
    public static boolean FONT_SELECTED = false;
    public static int originalHeight;
    public static int resizedHeight;
    public static JFrame f;
    public static int DETACH_TICKER_WIDTH = 1362;//1276;
    public static int FRAME_GAP;
    final Ticker tt = new Ticker();
    /**
     * Detaches this {@link DetachableRootPane} from its current parent
     * frame&mdash;provided that parent is either a {@link JInternalFrame} or a
     * {@link JFrame}&mdash;and installs it in a new frame of the appropriate
     * type.
     * <p/>
     * <p>If the current parent of this {@link DetachableRootPane} is a
     * non-iconified {@link JInternalFrame}, then the following visual behavior
     * occurs:
     * <ul>
     * <p/>
     * <li>The {@link DetachableRootPane} {@linkplain
     * JComponent#putClientProperty(Object, Object) sets a client property} named
     * "<code>desktopPane</code>" on itself, set to the value of the {@link
     * JInternalFrame}'s associated {@link JDesktopPane}</li>
     * <p/>
     * <li>The {@link JInternalFrame} is closed and disposed</li>
     * <p/>
     * <li>A new {@link JFrame} with the same title appears in its place,
     * retaining its size, but not its maximized status</li>
     * <p/>
     * <li>If that {@link JFrame} is closed, then a new {@link JInternalFrame} is
     * created directly under it, where possible, on the {@link JDesktopPane}
     * previously installed as a {@linkplain JComponent#getClientProperty(Object)
     * client property}</li>
     * <p/>
     * </ul></p>
     * <p/>
     * <p>If the current parent of this {@link DetachableRootPane} is a
     * non-iconified {@link JFrame}, then the following visual behavior occurs:
     * <ul>
     * <li>The {@link JFrame} is closed and disposed</li>
     * <p/>
     * <li>If the "<tt>desktopPane</tt>" {@linkplain
     * JComponent#getClientProperty(Object) client property} is set and is equal
     * to a non-<code>null</code> {@link JDesktopPane} instance, then a new {@link
     * JInternalFrame} is created, added to that {@link JDesktopPane} in the
     * location that the outgoing {@link JFrame} last had (see {@link
     * SwingUtilities#convertPointFromScreen(Point, Component)}); otherwise
     * nothing further happens</li>
     * </ul>
     */
    JInternalFrame internalFrame;
//    public static boolean IS_FRAME_RESIZED = false;
    /**
     * A {@link PropertyChangeListener} that monitors a {@link JInternalFrame} for
     * changes in its structural resizability and sets the {@link
     * #parentIsResizable} field appropriately.  This field is assigned a
     * non-<code>null</code> value in the {@link #addNotify()} method, and is
     * reset to <code>null</code> in most cases by the {@link #removeNotify()}
     * method.
     */
    private PropertyChangeListener pcl;
    /**
     * Indicates whether the "outgoing" parent of this {@link
     * DetachableRootPane}&mdash;usually a {@link JInternalFrame}&mdash;was
     * structurally resizable.
     */
    private boolean parentIsResizable;

    //    private TWRootPaneUI rootPaneUI;
    private boolean detached;

    /**
     * Creates a {@link DetachableRootPane}.  Typically this will be called in an
     * overridden {@link JInternalFrame#createRootPane()} method.  As a side
     * effect, installs an {@link AbstractAction} named "<code>detach</code>" in
     * the {@linkplain JComponent#getActionMap() associated
     * <code>ActionMap</code>} and associates it with the keystroke
     * "<code>F2</code>" (in the default {@link InputMap}).
     */
    public DetachableRootPane() {
        super();
        /*final ActionMap actionMap = this.getActionMap();
        assert actionMap != null;
        actionMap.put("detach", new AbstractAction("Detach") {
            public final void actionPerformed(final ActionEvent event) {
                detach();
            }
        });
        final InputMap inputMap = this.getInputMap();
        assert inputMap != null;
        inputMap.put(KeyStroke.getKeyStroke("F2"), "detach");*/

        self = this;
    }

    public static DetachableRootPane getInstance() {
//       if(self==null){
//           self=new DetachableRootPane();
//       }
        return self;
    }

    /**
     * Convenience method that creates a {@link JFrame} with a {@link
     * DetachableRootPane} instance as {@linkplain JFrame#getRootPane() its root
     * pane}.  This method never returns <code>null</code>.
     *
     * @return a new {@link JFrame}; never <code>null</code>
     */
    public static JFrame createJFrame() {
        return new JFrame() {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Convenience method that creates a {@link JFrame} with a {@link
     * DetachableRootPane} instance as {@linkplain JFrame#getRootPane() its root
     * pane}.  This method never returns <code>null</code>.
     *
     * @param configuration the supplied {@link GraphicsConfiguration}; supplied to the
     *                      new {@link JFrame}'s {@linkplain
     *                      JFrame#JFrame(GraphicsConfiguration) constructor}; may be
     *                      <code>null</code>
     * @return a new {@link JFrame}; never <code>null</code>
     */
    public static JFrame createJFrame(final GraphicsConfiguration configuration) {
        return new JFrame(configuration) {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Convenience method that creates a {@link JFrame} with a {@link
     * DetachableRootPane} instance as {@linkplain JFrame#getRootPane() its root
     * pane}.  This method never returns <code>null</code>.
     *
     * @param title the supplied {@linkplain JFrame#getTitle() title}; supplied
     *              to the new {@link JFrame}'s {@linkplain JFrame#JFrame(String)
     *              constructor}; may be <code>null</code>
     * @return a new {@link JFrame}; never <code>null</code>
     */
    public static JFrame createJFrame(final String title) {
        return new JFrame(title) {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Convenience method that creates a {@link JFrame} with a {@link
     * DetachableRootPane} instance as {@linkplain JFrame#getRootPane() its root
     * pane}.  This method never returns <code>null</code>.
     *
     * @param title         the supplied {@linkplain JFrame#getTitle() title}; supplied
     *                      to the new {@link JFrame}'s {@linkplain JFrame#JFrame(String,
     *                      GraphicsConfiguration) constructor}; may be <code>null</code>
     * @param configuration the supplied {@link GraphicsConfiguration}; supplied to the
     *                      new {@link JFrame}'s {@linkplain JFrame#JFrame(String,
     *                      GraphicsConfiguration) constructor}; may be <code>null</code>
     * @return a new {@link JFrame}; never <code>null</code>
     */
    public static JFrame createJFrame(final String title, final GraphicsConfiguration configuration) {
        return new JFrame(title, configuration) {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }


 /*   public void tickerDetached(int internalFrame, int frame, JInternalFrame jInternalFrame, JFrame jFrame) {
        j = jInternalFrame;
        originalHeight = internalFrame;
        resizedHeight = frame;
        f = jFrame;
        if (j.getTitle().equals(Language.getString("TICKER"))) {
            if (j.getHeight() + 5 != f.getHeight()) {
                f.setSize(new Dimension(1276, j.getHeight() + 5));
            }
            if (DETACH_TICKER_WIDTH != f.getWidth()) {
                FRAME_GAP = DETACH_TICKER_WIDTH - f.getWidth();

                GlobalSettings.adjustTickerSize(f.getWidth());
                TradeTicker.getInstance().adjustControls();
                // Redraw the according to the new dimnsions
                TradeTicker.getInstance().redrawTicker();
            }

            DETACH_TICKER_WIDTH = f.getWidth();
            FIRST_TIME_DETACHED = false;
        }

    }*/

    /**
     * Creates a new {@link JInternalFrame} with a {@link DetachableRootPane}
     * instance as {@linkplain JFrame#getRootPane() its root pane}.  This method
     * never returns <code>null</code>.
     *
     * @return a new {@link JInternalFrame}; never <code>null</code>
     */
    public static JInternalFrame createJInternalFrame() {
        return new JInternalFrame() {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Creates a new {@link JInternalFrame} with a {@link DetachableRootPane}
     * instance as {@linkplain JFrame#getRootPane() its root pane}.  This method
     * never returns <code>null</code>.
     *
     * @param title the supplied {@linkplain JInternalFrame#getTitle() title};
     *              supplied to the new {@link JInternalFrame}'s {@linkplain
     *              JInternalFrame#JInternalFrame(String) constructor}; may be
     *              <code>null</code>
     * @return a new {@link JInternalFrame}; never <code>null</code>
     */
    public static JInternalFrame createJInternalFrame(final String title) {
        return new JInternalFrame(title) {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Creates a new {@link JInternalFrame} with a {@link DetachableRootPane}
     * instance as {@linkplain JFrame#getRootPane() its root pane}.  This method
     * never returns <code>null</code>.
     *
     * @param title     the supplied {@linkplain JInternalFrame#getTitle() title};
     *                  supplied to the new {@link JInternalFrame}'s {@linkplain
     *                  JInternalFrame#JInternalFrame(String) constructor}; may be
     *                  <code>null</code>
     * @param resizable whether or not the new {@link JInternalFrame} is supposed to
     *                  be {@linkplain JInternalFrame#isResizable() structurally
     *                  resizable}
     * @return a new {@link JInternalFrame}; never <code>null</code>
     */
    public static JInternalFrame createJInternalFrame(final String title, final boolean resizable) {
        return new JInternalFrame(title, resizable) {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Creates a new {@link JInternalFrame} with a {@link DetachableRootPane}
     * instance as {@linkplain JFrame#getRootPane() its root pane}.  This method
     * never returns <code>null</code>.
     *
     * @param title     the supplied {@linkplain JInternalFrame#getTitle() title};
     *                  supplied to the new {@link JInternalFrame}'s {@linkplain
     *                  JInternalFrame#JInternalFrame(String) constructor}; may be
     *                  <code>null</code>
     * @param resizable whether or not the new {@link JInternalFrame} is supposed to
     *                  be {@linkplain JInternalFrame#isResizable() structurally
     *                  resizable}
     * @param closable  whether or not the new {@link JInternalFrame} is supposed to
     *                  be {@linkplain JInternalFrame#isClosable() closable}
     * @return a new {@link JInternalFrame}; never <code>null</code>
     */
    public static JInternalFrame createJInternalFrame(final String title, final boolean resizable, final boolean closable) {
        return new JInternalFrame(title, resizable, closable) {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Creates a new {@link JInternalFrame} with a {@link DetachableRootPane}
     * instance as {@linkplain JFrame#getRootPane() its root pane}.  This method
     * never returns <code>null</code>.
     *
     * @param title       the supplied {@linkplain JInternalFrame#getTitle() title};
     *                    supplied to the new {@link JInternalFrame}'s {@linkplain
     *                    JInternalFrame#JInternalFrame(String) constructor}; may be
     *                    <code>null</code>
     * @param resizable   whether or not the new {@link JInternalFrame} is supposed to
     *                    be {@linkplain JInternalFrame#isResizable() structurally
     *                    resizable}
     * @param closable    whether or not the new {@link JInternalFrame} is supposed to
     *                    be {@linkplain JInternalFrame#isClosable() closable}
     * @param maximizable whether or not the new {@link JInternalFrame} is supposed to
     *                    be {@linkplain JInternalFrame#isMaximizable() maximizable}
     * @return a new {@link JInternalFrame}; never <code>null</code>
     */
    public static JInternalFrame createJInternalFrame(final String title, final boolean resizable, final boolean closable, final boolean maximizable) {
        return new JInternalFrame(title, resizable, closable, maximizable) {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Creates a new {@link JInternalFrame} with a {@link DetachableRootPane}
     * instance as {@linkplain JFrame#getRootPane() its root pane}.  This method
     * never returns <code>null</code>.
     *
     * @param title       the supplied {@linkplain JInternalFrame#getTitle() title};
     *                    supplied to the new {@link JInternalFrame}'s {@linkplain
     *                    JInternalFrame#JInternalFrame(String) constructor}; may be
     *                    <code>null</code>
     * @param resizable   whether or not the new {@link javax.swing.JInternalFrame} is supposed to
     *                    be {@linkplain javax.swing.JInternalFrame#isResizable() structurally
     *                    resizable}
     * @param closable    whether or not the new {@link JInternalFrame} is supposed to
     *                    be {@linkplain JInternalFrame#isClosable() closable}
     * @param maximizable whether or not the new {@link JInternalFrame} is supposed to
     *                    be {@linkplain JInternalFrame#isMaximizable() maximizable}
     * @param iconifiable whether or not the new {@link JInternalFrame} is supposed to
     *                    be {@linkplain JInternalFrame#isIconifiable() iconifiable}
     * @return a new {@link JInternalFrame}; never <code>null</code>
     */
    public static JInternalFrame createJInternalFrame(final String title, final boolean resizable, final boolean closable, final boolean maximizable, final boolean iconifiable) {
        return new JInternalFrame(title, resizable, closable, maximizable, iconifiable) {
            protected final JRootPane createRootPane() {
                return new DetachableRootPane();
            }
        };
    }

    /**
     * Calls the {@linkplain Component#addNotify() superclass implementation} and
     * then ensures that if the parent this {@link DetachableRootPane} was just
     * added to is either a {@link JInternalFrame} or a {@link JFrame} its
     * resizability is monitored via a {@link PropertyChangeListener}.
     *
     * @see #removeNotify()
     */
    public void addNotify() {
        super.addNotify();

        final Container parent = this.getParent();

        boolean addPropertyChangeListener = true;

        if (parent instanceof JInternalFrame) {
            this.parentIsResizable = ((JInternalFrame) parent).isResizable();
        } else if (parent instanceof JFrame) {
            this.parentIsResizable = ((JFrame) parent).isResizable();
        } else {
            addPropertyChangeListener = false;
        }

        if (addPropertyChangeListener) {
            this.pcl = new PropertyChangeListener() {
                public final void propertyChange(final PropertyChangeEvent event) {

                    // Monitor the "resizable" property of the parent for changes.
                    if (event != null && "resizable".equals(event.getPropertyName())) {
                        final Boolean value = (Boolean) event.getNewValue();
                        parentIsResizable = value != null && value.booleanValue();
                    }

                }
            };
            parent.addPropertyChangeListener("resizable", this.pcl);
        }
    }

    /**
     * Ensures that the {@link PropertyChangeListener} that might have been
     * installed on the {@linkplain Component#getParent() parent of this
     * <code>DetachableRootPane</code>} is removed.
     */
    public void removeNotify() {
        if (this.pcl != null) {
            final Container parent = this.getParent();
            assert parent != null;
            parent.removePropertyChangeListener("resizable", this.pcl);
        }
        super.removeNotify();
    }

    public void detach() {

        final Component parent = this.getParent();
        if (parent instanceof JInternalFrame) {
            // Leaving JInternalFrame; going into JFrame

            internalFrame = (JInternalFrame) parent;

            final Rectangle bounds = internalFrame.getBounds();
            assert bounds != null;
            final Point point;
            if (internalFrame.isShowing()) {
                point = internalFrame.getLocationOnScreen();
            } else {
                point = internalFrame.getLocation();
            }
            assert point != null;

            bounds.x = point.x;
            bounds.y = point.y;

            final JDesktopPane pane = internalFrame.getDesktopPane();
            if (pane != null) {
                //pane.remove(internalFrame);
                this.putClientProperty("desktop", pane);
            }

            // Store the structural state of the "outgoing" JInternalFrame.  This is
            // so that we can create another JInternalFrame, if necessary, that will
            // look and behave like the one that we are currently detaching from.
            this.putClientProperty(INTERNAL_FRAME_RESIZABLE, this.parentIsResizable);
            this.putClientProperty(INTERNAL_FRAME_CLOSABLE, internalFrame.isClosable());
            this.putClientProperty(INTERNAL_FRAME_MAXIMIZABLE, internalFrame.isMaximizable());
            this.putClientProperty(INTERNAL_FRAME_ICONIFIABLE, internalFrame.isIconifiable());

            if (internalFrame instanceof ClientTable) {
                ((ClientTable) internalFrame).setVisible(false, true);
            } else if (internalFrame instanceof InternalFrame) {
                ((InternalFrame) internalFrame).setDetached(true);
                internalFrame.setVisible(false);
            } else {
                internalFrame.setVisible(false);
            }

            // Create a new JFrame that has us as its root pane, and that calls this
            // method (detach()) when closed.
            final JFrame frame = new JFrame(internalFrame.getTitle()) {
                protected final void frameInit() {
                    super.frameInit();
                    final JFrame selfref = this;
                    // Ensure that however the window is closed, it actually causes this
                    // detach() method to be fired instead.
                    this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

                    this.addWindowListener(new WindowAdapter() {
                        public final void windowClosing(final WindowEvent event) {
//                            SharedMethods.printLine("Detaching ====== 1 ..." + internalFrame.getTitle(), true);
                            detach();
                        }
                    });

                    this.addComponentListener(new ComponentListener() {
                        public void componentResized(ComponentEvent e) {
                            if (internalFrame.getTitle().equals(Language.getString("TICKER"))) {
                                Ticker.IS_FRAME_DATA_COME = true;
                                synchronized (TickerFrame.SYN_RESIZED) {

                                    TradeTicker.IS_FRAME_RESIZED = true;
                                    IS_DETACHED = true;
                                    initialPaneWidth = internalFrame.getWidth();
                                }
                                DetachableRootPane.getInstance().tickerDetached(internalFrame.getHeight(), getHeight(), internalFrame, selfref);
                            }
                        }

                        public void componentMoved(ComponentEvent e) {
                            //To change body of implemented methods use File | Settings | File Templates.
                        }

                        public void componentShown(ComponentEvent e) {
                            //To change body of implemented methods use File | Settings | File Templates.
                        }

                        public void componentHidden(ComponentEvent e) {
                            //To change body of implemented methods use File | Settings | File Templates.
                        }
                    });
                }

                /*public void paintComponent(Graphics g) {
                    Point pos = this.getLocationOnScreen();
                    Point offset = new Point(-pos.x, -pos.y);
                    updateBackground();
                    g.drawImage(background, offset.x, offset.y, null);
                }*/

                protected final JRootPane createRootPane() {
                    // Ensure that this DetachableRootPane instance is the root pane for
                    // any JFrames it creates.
                    return DetachableRootPane.this;
                }
            };
            /*frame.setUndecorated(true);
            frame.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);*/
            //frame.setAlwaysOnTop(false);
            frame.setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));

            if (internalFrame instanceof InternalFrame) {
                ((InternalFrame) internalFrame).setRootPane(new JRootPane());
//                ((InternalFrame) internalFrame).setDetached(true);
                ((InternalFrame) internalFrame).setDetachedFrame(frame);
                if (internalFrame instanceof TickerFrame) {
                    frame.setAlwaysOnTop(true);
                }
                bounds.setSize((int) bounds.getWidth() + 5, (int) bounds.getHeight() + 5);
            } else if (internalFrame instanceof ClientTable) {
                ((ClientTable) internalFrame).setRootPane(new JRootPane());
                ((ClientTable) internalFrame).setDetached(true);
                ((ClientTable) internalFrame).setDetachedFrame(frame);
                bounds.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
            } else if (internalFrame instanceof MISTTable) {
                ((MISTTable) internalFrame).setRootPane(new JRootPane());
                ((MISTTable) internalFrame).setDetached(true);
                ((MISTTable) internalFrame).setDetachedFrame(frame);
                bounds.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
            }
//            bounds.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
            frame.setBounds(bounds);
            frame.setVisible(true);
            detached = true;

        } else if (parent instanceof JFrame) {
            // Leaving JFrame; going into JInternalFrame

            final JFrame frame = (JFrame) parent;

            final JDesktopPane pane = (JDesktopPane) this.getClientProperty("desktop");
            final Rectangle bounds = frame.getBounds();
            final Point point = new Point(bounds.x, bounds.y);
            if (internalFrame instanceof TickerFrame) {
                IS_DETACHED = false;
                if (Client.getInstance().isTickerFixed()) {
                    setTickerFrameFixed((InternalFrame) internalFrame);
                    internalFrame.setVisible(true);
                } else {
                    internalFrame.setVisible(true);
                }
            } else {
                try {
                    SwingUtilities.convertPointFromScreen(point, pane);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (frame.getExtendedState() == JFrame.MAXIMIZED_BOTH) {
                    internalFrame.setSize(internalFrame.getDesktopPane().getSize());
                } else {
                    if (point.x < 0) {
                        point.x = 0;
                    } else if (point.x > pane.getWidth()) {
                        point.x = Math.max(pane.getWidth() - bounds.width, 0);
                    }
                    if (point.y < 0) {
                        point.y = 0;
                    } else if (point.y > pane.getHeight()) {
                        point.y = Math.max(pane.getHeight() - bounds.height, 0);
                    }
                    bounds.x = point.x;
                    bounds.y = point.y;
                    bounds.setSize((int) bounds.getWidth(), (int) bounds.getHeight());
                    internalFrame.setBounds(bounds);
                }
                //internalFrame.setVisible(true);
                frame.setVisible(false);
            }

            if (internalFrame instanceof InternalFrame) {
                ((InternalFrame) internalFrame).setRootPane(this);
                frame.dispose();
                ((InternalFrame) internalFrame).setDetached(false);
                ((InternalFrame) internalFrame).setDetachedFrame(null);
            } else if (internalFrame instanceof ClientTable) {
                ((ClientTable) internalFrame).setRootPane(this);
                frame.dispose();
                ((ClientTable) internalFrame).setDetached(false);
                ((ClientTable) internalFrame).setDetachedFrame(null);
            }
//            detached = false;
//            internalFrame.doDefaultCloseAction();
            internalFrame.setVisible(true);
            detached = false;
            if (internalFrame.getDefaultCloseOperation() == JInternalFrame.DISPOSE_ON_CLOSE) {
                internalFrame.doDefaultCloseAction();
            }
        }
    }

    public void tickerDetached(int internalFrame, int frame, JInternalFrame jInternalFrame, JFrame jFrame) {
        j = jInternalFrame;
        originalHeight = internalFrame;
        resizedHeight = frame;
        f = jFrame;
        if (j.getTitle().equals(Language.getString("TICKER"))) {
            if (j.getHeight() + 5 != f.getHeight()) {
                f.setSize(new Dimension(f.getWidth(), j.getHeight() + 5));
            }
            if (initialPaneWidth != f.getWidth()) {
                FRAME_GAP = initialPaneWidth - f.getWidth();

                GlobalSettings.adjustTickerSize(f.getWidth());
                TradeTicker.getInstance().adjustControls();
                // Redraw the according to the new dimnsions
                TradeTicker.getInstance().redrawTicker();
            }

            initialPaneWidth = f.getWidth();
            FIRST_TIME_DETACHED = false;
        }

    }

    public void fontResized() {
        if (SharedSettings.IS_CUSTOM_SELECTED) {
            if (f.getHeight() != GlobalSettings.STOCK_TICKER_HEIGHT) {
                f.setSize(new Dimension(j.getWidth(), GlobalSettings.STOCK_TICKER_HEIGHT + 30));
            }
        } else {
            if (f.getHeight() != GlobalSettings.STOCK_TICKER_HEIGHT) {
                f.setSize(new Dimension(j.getWidth(), HolderPanel.ADV_TICKER_HEIGHT + 30));
            }
        }

    }

    public void detachedFrameResized() {
        if (SharedSettings.IS_CUSTOM_SELECTED) {
            if (f.getHeight() != GlobalSettings.STOCK_TICKER_HEIGHT) {
                f.setSize(new Dimension(j.getWidth(), GlobalSettings.STOCK_TICKER_HEIGHT + 30));
            }
        } else {
            if (f.getHeight() != GlobalSettings.STOCK_TICKER_HEIGHT) {
                f.setSize(new Dimension(j.getWidth(), HolderPanel.ADV_TICKER_HEIGHT + 30));
            }
        }

    }

    private void setTickerFrameFixed(InternalFrame frame) {
        if (Ticker.tickerFixedToTopPanel) {
            Client.getInstance().tickerPanel.add(frame);
            Client.getInstance().tickerPanel.updateUI();
        } else {
            Client.getInstance().bottomtickerPanel.add(frame);
            Client.getInstance().bottomtickerPanel.updateUI();
        }
        Client.getInstance().window.validate();
        Client.getInstance().setTickerFixed(true);
        System.out.println("Inside setticker fixed...");
        Client.getInstance().floatMenu.setText(Language.getString("FLOAT_TICKER"));
    }

    public boolean isDetached() {
        return detached;
    }

    /* private Image background;

    public void updateBackground() {
        try {
            Robot rbt = new Robot();
            Toolkit tk = Toolkit.getDefaultToolkit();
            Dimension dim = tk.getScreenSize();
            background = rbt.createScreenCapture(
                    new Rectangle(0, 0, (int) dim.getWidth(),
                            (int) dim.getHeight()));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/

    /*public RootPaneUI getUI() {
        if (rootPaneUI == null){
            rootPaneUI = new TWRootPaneUI();
        }
        return rootPaneUI;
    }


    public void updateUI() {
        if (rootPaneUI == null){
            rootPaneUI = new TWRootPaneUI();
        }
        setUI(rootPaneUI);
    }*/
}
