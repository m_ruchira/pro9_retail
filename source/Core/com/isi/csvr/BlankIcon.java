/* (swing1.1) */

//package jp.gr.java_conf.tame.swing.icon;
package com.isi.csvr;

import javax.swing.*;
import java.awt.*;

/**
 * Creates a blank icon to use when the column is not sorted
 *
 * @version 1.0 02/26/99
 * @Author unknown (This is a downloaded class from the internet)
 */
public class BlankIcon implements Icon {
    private Color fillColor;
    private Color oColor1;
    private Color oColor2;
    private int size;

    private int[] aiX = {6, 9, 2, 6};
    private int[] aiY = {0, 9, 9, 0};

    public BlankIcon() {
        this(null, 0);
    }

    /**
     * Constructor
     */
    public BlankIcon(Color color, int size) {
        fillColor = color;
        this.size = size;

        oColor1 = UIManager.getColor("controlLtHighlight");
        oColor2 = UIManager.getColor("controlDkShadow");
    }

    /**
     * Paint the icon
     */
    public void paintIcon(Component c, Graphics g, int x, int y) {
        if (fillColor != null) {
            g.setColor(Color.green);
            g.drawRect(x, y, size - 1, size - 1);
        }
        //g.setColor(oColor2);
        //g.drawLine(0,0,10,10);// drawPolygon(aiX,aiY,4);
    }

    /**
     * Returns the width of the icon
     */
    public int getIconWidth() {
        return size;
    }

    /**
     * Returns the height of the icon
     */
    public int getIconHeight() {
        return size;
    }
}
