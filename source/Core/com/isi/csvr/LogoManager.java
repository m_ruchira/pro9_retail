/* Copyright (c) 2000 Integrated Systems International (ISI)
    Changed to Internet Explorer mode 6 / Jan / 2009
        Shanika Liyanage, Uditha Nagahawatta
*/
package com.isi.csvr;

import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.ContextMenuProvider;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * A Swing-based dialog class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class LogoManager extends JWindow {

    private static Browser splashScreen;

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */
    public LogoManager() {
        super();
        try {
            jbInit();

            Dimension oDim = Toolkit.getDefaultToolkit().getScreenSize();

            this.setBounds((int) ((oDim.getWidth() - this.getWidth()) / 2),
                    (int) ((oDim.getHeight() - this.getHeight()) / 2) - 10,
                    this.getWidth(), this.getHeight());
            this.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the value for the progress bar
     */
    public static void setPtogress(int iProgress) {
        splashScreen.executeScript("IncreaseSliderWidth(" + iProgress + ");");
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        this.getContentPane().setLayout(new BorderLayout(0, 0));
        splashScreen = new Browser();
        splashScreen.setEventHandler(null);
        splashScreen.getProperties().setAllowContextMenu(false);
        splashScreen.getProperties().setAllowRunActiveX(true);
        splashScreen.getProperties().setAllowScripts(true);
        splashScreen.setSilent(true);

        getContentPane().add(splashScreen, BorderLayout.CENTER);
        String path = (System.getProperties().get("user.dir") + "\\Templates\\Splash.html");
        File tamplate = new File(path);
        splashScreen.navigate(tamplate.getAbsolutePath());
        Dimension dim = null;
        try {
            int width = Integer.parseInt(splashScreen.getDocument().getElementById("size").getAttribute("width"));
            int height = Integer.parseInt(splashScreen.getDocument().getElementById("size").getAttribute("height"));
            dim = new Dimension(width, height);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            dim = new Dimension(0, 0);
        }
//        Dimension dim = splashScreen.getDocument().getBody().getSize();
        setSize(new Dimension(dim.width, dim.height));
        splashScreen.waitReady();
        ContextMenuProvider provider = new ContextMenuProvider() {
            public JPopupMenu getPopupMenu(Element contextElement) {
                // don't show any menu
                return null;
            }
        };
        splashScreen.setContextMenuProvider(provider);
        splashScreen.setEnabled(false);


    }

}

