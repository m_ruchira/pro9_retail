package com.isi.csvr;

import com.isi.csvr.shared.Meta;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Sep 24, 2007
 * Time: 10:03:59 AM
 */
public class TWTableCellRendererComponent extends DefaultTableCellRenderer {

//    private int column;


    private int status;
    private boolean selected;
    private Color selectedColor;
    private int flagValue;
//    private Font fractionalFont;
//    private int fraction;
//    private int base;


    public TWTableCellRendererComponent() {
        //setIcon(this);    
    }

    public void setSelected(boolean selected, Color selectedColor) {
        this.selected = selected;
        this.selectedColor = selectedColor;
    }

    // implements javax.swing.table.TableCellRenderer

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
//        this.column = table.convertColumnIndexToModel(column);
//        this.setIcon(this);
        //this.setIconTextGap(0);
//        fraction = 1;
        //fractionalFont = getFont().deriveFont(getFont().getSize() / 2);
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setFlagValue(int flagValue) {
        this.flagValue = flagValue;
    }

    public void paint(Graphics g) {
        super.paint(g);
        if ((status == Meta.SYMBOL_STATE_EXPIRED) ||
                (status == Meta.SYMBOL_STATE_DELISTED) ||
                (status == Meta.SYMBOL_STATE_SUSPENDED) ||
                (status == Meta.SYMBOL_STATE_HATLED) ||
                (status == Meta.SYMBOL_STATE_DELAYED)) {
            Graphics2D g2D = (Graphics2D) g;
            g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
            g.drawLine(0, (int) g.getClipBounds().getHeight() / 2, 500, (int) g.getClipBounds().getHeight() / 2);
        }

        /*if (selected && (Theme.mainBoardAlphaLevel > 0)){
            Graphics2D g2D = (Graphics2D) g;
            g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,  Theme.mainBoardAlphaLevel));
            g.setColor(selectedColor);
            g.fillRect((int)g.getClipBounds().getX(), (int)g.getClipBounds().getY(), (int)g.getClipBounds().getWidth(), (int)g.getClipBounds().getHeight());
        }*/

    }

    /*public int getIconHeight() {
        return getHeight() / 2;
    }

    public int getIconWidth() {
        return getHeight() / 2;
    }*/

    /*public void paintIcon(Component c, Graphics g, int x, int y) {
        *//*g.setColor(Color.blue);
        g.drawLine(x,y, x+getIconWidth(), y);
        g.drawLine(x,y+getIconHeight(),x+getIconWidth(), y+getIconHeight());
        g.drawLine(x,y, x,y+getIconHeight());
        g.drawLine(x+getIconWidth()-1,y, x+getIconWidth()-1,y+getIconHeight());*//*
        //Map<TextAttribute, Object> map = getFont().getAttributes();
        fractionalFont = getFont().deriveFont(AffineTransform.getScaleInstance(.5, .5));
        Font f = getFont();
        g.setFont(fractionalFont);
        g.setColor(Color.BLACK);
        /y = y+getIconHeight() / 2;
        g.drawString(""+fraction, x, y);
        x--;
        x = x+ g.getFontMetrics().stringWidth(""+fraction);
        y = y+getIconHeight() / 4;
        g.drawString("/",  x, y);
        x = x+ g.getFontMetrics().stringWidth("/");
        y = y+g.getFontMetrics().getMaxDescent();
        g.drawString("4", x, y);*
        g.setFont(f);
    }*/

    /*protected void paintComponent(Graphics g) {
//        Graphics2D g2D = (Graphics2D) g;
//            g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.2f));
        super.paintComponent(g);    //To change body of overridden methods use File | Settings | File Templates.
    }*/
}
