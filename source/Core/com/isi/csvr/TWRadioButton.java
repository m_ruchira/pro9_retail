package com.isi.csvr;

import com.isi.csvr.shared.TWRadioButtonIcon;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Sep 23, 2008
 * Time: 4:14:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class TWRadioButton extends JRadioButton {

    public TWRadioButton(String text) {
        super(text, new TWRadioButtonIcon());
    }

    public TWRadioButton(String text, boolean selected) {
        super(text, new TWRadioButtonIcon(), selected);
    }
}
