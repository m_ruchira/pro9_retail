// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.dnd.DraggableTable;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.CustomThemeInterface;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.variationmap.VariationImage;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Date;

public class TableRenderer implements TableCellRenderer, CustomThemeInterface {
    private static final TWTableCellRendererComponent DEFAULT_RENDERER =
            new TWTableCellRendererComponent();
    private static Color g_oUpFGColor;
    private static Color g_oDownFGColor;
    private static Color g_oUpBGColor;
    private static Color g_oDownBGColor;
    private static Color g_oChangeUpFG;
    private static Color g_oChangeDownFG;
    private static Color upFGColor;
    private static Color downFGColor;
    private static Color upBGColor;
    private static Color downBGColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color changeUpFG;
    private static Color changeDownFG;
    private static Color maxBG;
    private static Color maxFG;
    private static Color minBG;
    private static Color minFG;
    private static Color week52HighBG;
    private static Color week52HighFG;
    private static Color week52LowBG;
    private static Color week52LowFG;
    private static Color volumeFG;
    private static Color bidask_BidColor;
    private static Color bidask_AskColor;
    private static Color g_oBGBid1;
    private static Color g_oBGBid2;
    private static Color g_oBGAsk1;
    private static Color g_oBGAsk2;
    private static Color g_oBGTrade1;
    private static Color g_oBGTrade2;
    private static Color g_oFGBid1;
    private static Color g_oFGBid2;
    private static Color g_oFGAsk1;
    private static Color g_oFGAsk2;
    private static Color g_oFGTrade1;
    private static Color g_oFGTrade2;
    private static Color gridColor;
    private static Color disabled_SymbolBG;
    private static Date date;
    private static int modelColumn;
    private static Border dropBorder;
    private static Border cellBorder;
    private static ImageBorder imageBorder;
    private static Border themeCellBorder;
    private final int[] g_asRendIDs;
    private final int g_iStringAlign;
    private final int g_iNumberAlign;
    private final int g_iCenterAlign;
    private final TWDateFormat g_oDateFormat;// = new SimpleDateFormat ("yyyy/MM/dd");
    private final TWDateFormat g_oDateTimeFormat;// = new SimpleDateFormat ("HH:mm:ss");
    private final String g_sNA = Language.getString("NA");
    private final TWDecimalFormat oVariableDecimal = new TWDecimalFormat(" ###,###.###  ");
    private final TWSymbolImage eventBulb;
    private final VariationImage variationImage;
    private String exchange;
    private String g_sBID = Language.getString("NA");
    private String g_sASK = Language.getString("NA");
    private String g_Sline = "-";
    private String priceMarketOrder = "NA";
    private String priceMarketOnOpening = "NA";
    private long[] longArray;
    private long longValue;
    private TWTableCellRendererComponent lblRenderer;// = new JLabel();
    private double doubleValue;
    private long updateDirection;
    private boolean blankCell;
    private TWDecimalFormat oCumReturnFormat = new TWDecimalFormat(" ###,##0.000  ");
    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oCurrencyPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
    private TWDecimalFormat oChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    private TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    private ImageIcon g_oUPImage = null;
    private ImageIcon g_oDownImage = null;
    private ImageIcon positiveEqualImage = null;
    private ImageIcon negEqualImage = null;
    private int tempInt;
    private long tempZone;
    private long now;
    private double[] ohlcValues;
    private int selectedDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;
    private int tableDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;
    private int stockDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;
    private int currencyDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;
    private int symbolStatus = 0;
    private boolean isSymbolEnable = true;
    private Color foreground, background;
    private int iRendID = 0;

    public TableRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;

        //DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        //dfs.setZeroDigit('\u0660');
        oCumReturnFormat = new TWDecimalFormat(" ###,##0.000  ");
        //oCumReturnFormat.setDecimalFormatSymbols(dfs);
        oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
        //oPriceFormat.setDecimalFormatSymbols(dfs);
        oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
        //oQuantityFormat.setDecimalFormatSymbols(dfs);
        oChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
        //oChangeFormat.setDecimalFormatSymbols(dfs);
        oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
        //oPChangeFormat.setDecimalFormatSymbols(dfs);

        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));
        g_oDateTimeFormat = new TWDateFormat(Language.getString("BOARD_TIME_FORMAT"));
        date = new Date();
        variationImage = new VariationImage();

        try {
            g_oUPImage = new ImageIcon("images/Common/up.gif");
        } catch (Exception e) {
            g_oUPImage = null;
        }
        try {
            g_oDownImage = new ImageIcon("images/Common/down.gif");
        } catch (Exception e) {
            g_oDownImage = null;
        }
        try {
            positiveEqualImage = new ImageIcon("images/Common/equal_green.gif");
        } catch (Exception e) {
            positiveEqualImage = null;
        }
        try {
            negEqualImage = new ImageIcon("images/Common/equal_red.gif");
        } catch (Exception e) {
            negEqualImage = null;
        }

        eventBulb = new TWSymbolImage();

        try {
            g_sBID = " " + Language.getString("BID") + " ";
        } catch (Exception e) {
            g_sBID = " Bid ";
        }

        try {
            g_sASK = " " + Language.getString("OFFER") + " ";
        } catch (Exception e) {
            g_sASK = " Ask ";
        }

        reload();

        if (Language.isLTR()) {
            g_iStringAlign = JLabel.LEFT;
        } else {
            g_iStringAlign = JLabel.RIGHT;
        }

        priceMarketOrder = Language.getString("DEPTH_PRICE_MARKET_ORDER");
        priceMarketOnOpening = Language.getString("DEPTH_PRICE_MARKET_ON_OPENING");


        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reload() {
        try {
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oChangeUpFG = Theme.getColor("BOARD_TABLE_CELL_CHANGE_UP_FGCOLOR");
            g_oChangeDownFG = Theme.getColor("BOARD_TABLE_CELL_CHANGE_DOWN_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");

            g_oBGBid1 = Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1");
            g_oBGBid2 = Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR2");
            g_oBGAsk1 = Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1");
            g_oBGAsk2 = Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR2");
            g_oBGTrade1 = Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1");
            g_oBGTrade2 = Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR2");

            g_oFGBid1 = Theme.getColor("BOARD_TABLE_CELL_BID_FGCOLOR1");
            g_oFGBid2 = Theme.getColor("BOARD_TABLE_CELL_BID_FGCOLOR2");
            g_oFGAsk1 = Theme.getColor("BOARD_TABLE_CELL_ASK_FGCOLOR1");
            g_oFGAsk2 = Theme.getColor("BOARD_TABLE_CELL_ASK_FGCOLOR2");
            g_oFGTrade1 = Theme.getColor("BOARD_TABLE_CELL_TRADE_FGCOLOR1");
            g_oFGTrade2 = Theme.getColor("BOARD_TABLE_CELL_TRADE_FGCOLOR2");

            maxBG = Theme.getColor("BOARD_TABLE_MAX_BGCOLOR");
            maxFG = Theme.getColor("BOARD_TABLE_MAX_FGCOLOR");
            minBG = Theme.getColor("BOARD_TABLE_MIN_BGCOLOR");
            minFG = Theme.getColor("BOARD_TABLE_MIN_FGCOLOR");
            week52HighBG = Theme.getColor("BOARD_TABLE_52_HIGH_BGCOLOR");
            week52HighFG = Theme.getColor("BOARD_TABLE_52_HIGH_FGCOLOR");
            week52LowBG = Theme.getColor("BOARD_TABLE_52_LOW_BGCOLOR");
            week52LowFG = Theme.getColor("BOARD_TABLE_52_LOW_FGCOLOR");
            volumeFG = Theme.getColor("SMALL_TRADE_FG_COLOR");
            bidask_BidColor = Theme.getColor("BIDASK_MAP_BID_COLOR");
            bidask_AskColor = Theme.getColor("BIDASK_MAP_ASK_COLOR");

//     --- Added by shanika-- For disabled symbols----
            disabled_SymbolBG = Theme.getColor("DISABLED_SYMBOL_BG_COLOR");

//            Color gridColor = Theme.getOptionalColor("BOARD_TABLE_GRID_COLOR");
            gridColor = Theme.getOptionalColor("BOARD_TABLE_GRID_COLOR");

            int gridStyle;
            if (gridColor != null) {
                gridStyle = Theme.getInt("BOARD_TABLE_GRID_STYLE", TableThemeSettings.GRID_NONE);
            } else {
                gridStyle = TableThemeSettings.GRID_NONE;
            }

            switch (gridStyle) {
                case TableThemeSettings.GRID_NONE:
                    themeCellBorder = null;
                    break;
                case TableThemeSettings.GRID_HORIZONTAL:
                    themeCellBorder = BorderFactory.createMatteBorder(0, 0, 1, 0, gridColor);
                    break;
                case TableThemeSettings.GRID_VERTICAL:
                    if (Language.isLTR()) {
                        themeCellBorder = BorderFactory.createMatteBorder(0, 0, 0, 1, gridColor);
                    } else {
                        themeCellBorder = BorderFactory.createMatteBorder(0, 1, 0, 0, gridColor);
                    }
                    break;
                case TableThemeSettings.GRID_BOTH:
                    if (Language.isLTR()) {
                        themeCellBorder = BorderFactory.createMatteBorder(0, 0, 1, 1, gridColor);
                    } else {
                        themeCellBorder = BorderFactory.createMatteBorder(0, 1, 1, 0, gridColor);
                    }
                    break;
            }

            imageBorder = new ImageBorder(1, 0, 1, 0, g_oSelectedBG);

//            dragBorder = BorderFactory.createMatteBorder(1, 0, 1, 0, g_oSelectedFG);
            dropBorder = BorderFactory.createMatteBorder(2, 0, 0, 0, g_oDownBGColor);
        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            changeDownFG = Color.red;
            changeUpFG = Color.green;
            volumeFG = Color.blue;
            bidask_BidColor = Color.green;
            bidask_AskColor = Color.red;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        //renderer = DEFAULT_RENDERER.getTableCellRendererComponent(table, value,
        //        isSelected, hasFocus, row, column);


        lblRenderer = (TWTableCellRendererComponent) DEFAULT_RENDERER.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

//        lblRenderer.setBorder(BorderFactory.createLineBorder(Color.red));

//        modelColumn = column;
        modelColumn = table.convertColumnIndexToModel(column);

        if (((TableThemeSettings) table).isCuatomThemeEnabled()) {
            downFGColor = ((TableThemeSettings) table).getDownFGColor();
            downBGColor = ((TableThemeSettings) table).getDownBGColor();
            upFGColor = ((TableThemeSettings) table).getUpFGColor();
            upBGColor = ((TableThemeSettings) table).getUpBGColor();
            changeUpFG = ((TableThemeSettings) table).getChangeUPFGColor();
            changeDownFG = ((TableThemeSettings) table).getChangeDownFGColor();
            //if (((TableThemeSettings) table).isGdidOn()){
            cellBorder = ((TableThemeSettings) table).getCellBorder();
            //}
            if (isSelected) {
                foreground = ((TableThemeSettings) table).getHighlighterFGColor();
                background = ((TableThemeSettings) table).getHighlighterBGColor();
                downFGColor = foreground;
                upFGColor = foreground;
                downBGColor = background;
                upBGColor = background;
                imageBorder.setColor(background);
                lblRenderer.setFont(((TableThemeSettings) table).getSelectedTableFont());
            } else if (row % 2 == 0) {
                if ((modelColumn == 4) || (modelColumn == 5)) {
                    background = ((TableThemeSettings) table).getTradeBGColor1();
                    foreground = ((TableThemeSettings) table).getTradeFGColor1();
                } else if ((modelColumn == 20) || (modelColumn == 21)) {
                    background = ((TableThemeSettings) table).getBidBGColor1();
                    foreground = ((TableThemeSettings) table).getBidFGColor1();
                } else if ((modelColumn == 22) || (modelColumn == 23)) {
                    background = ((TableThemeSettings) table).getAskBGColor1();
                    foreground = ((TableThemeSettings) table).getAskFGColor1();
                } else if (modelColumn == 36) {
                    try {
                        if (((DoubleTransferObject) (value)).getValue() > 1) {
                            background = ((TableThemeSettings) table).getAskBGColor1();
                            foreground = ((TableThemeSettings) table).getAskFGColor1();
                        } else if (((DoubleTransferObject) (value)).getValue() < 1) {
                            background = ((TableThemeSettings) table).getBidBGColor1();
                            foreground = ((TableThemeSettings) table).getBidFGColor1();

                        } else {
                            background = ((TableThemeSettings) table).getRow1BGColor();
                            foreground = ((TableThemeSettings) table).getRow1FGColor();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        background = ((TableThemeSettings) table).getRow1BGColor();
                        foreground = ((TableThemeSettings) table).getRow1FGColor();
                    }
                } else {
                    background = ((TableThemeSettings) table).getRow1BGColor();
                    foreground = ((TableThemeSettings) table).getRow1FGColor();
                }
                //imageBorder = BorderFactory.createMatteBorder(1, 0, 1, 0, background);
            } else {
                if ((modelColumn == 4) || (modelColumn == 5)) {
                    background = ((TableThemeSettings) table).getTradeBGColor2();
                    foreground = ((TableThemeSettings) table).getTradeFGColor2();
                } else if ((modelColumn == 20) || (modelColumn == 21)) {
                    background = ((TableThemeSettings) table).getBidBGColor2();
                    foreground = ((TableThemeSettings) table).getBidFGColor2();
                } else if ((modelColumn == 22) || (modelColumn == 23)) {
                    background = ((TableThemeSettings) table).getAskBGColor2();
                    foreground = ((TableThemeSettings) table).getAskFGColor2();
                } else if (modelColumn == 36) {
                    try {
                        if (((DoubleTransferObject) (value)).getValue() > 1) {
                            background = ((TableThemeSettings) table).getAskBGColor2();
                            foreground = ((TableThemeSettings) table).getAskFGColor2();
                        } else if (((DoubleTransferObject) (value)).getValue() < 1) {
                            background = ((TableThemeSettings) table).getBidBGColor2();
                            foreground = ((TableThemeSettings) table).getBidFGColor2();

                        } else {
                            background = ((TableThemeSettings) table).getRow1BGColor();
                            foreground = ((TableThemeSettings) table).getRow1FGColor();
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                        background = ((TableThemeSettings) table).getRow1BGColor();
                        foreground = ((TableThemeSettings) table).getRow1FGColor();
                    }
                } else {
                    background = ((TableThemeSettings) table).getRow2BGColor();
                    foreground = ((TableThemeSettings) table).getRow2FGColor();
                }
                //imageBorder = BorderFactory.createMatteBorder(1, 0, 1, 0, background);
            }
        } else {
            downFGColor = g_oDownFGColor;
            downBGColor = g_oDownBGColor;
            upFGColor = g_oUpFGColor;
            upBGColor = g_oUpBGColor;
            changeUpFG = g_oChangeUpFG;
            changeDownFG = g_oChangeDownFG;
            cellBorder = themeCellBorder;
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
                downFGColor = foreground;
                upFGColor = foreground;
                downBGColor = background;
                upBGColor = background;
                imageBorder.setColor(background);

                //renderer.setFont(((TableThemeSettings) table).getSelectedTableFont());
            } else if (row % 2 == 0) {
                if ((modelColumn == 4) || (modelColumn == 5)) {
                    background = g_oBGTrade1;
                    foreground = g_oFGTrade1;
                } else if ((modelColumn == 20) || (modelColumn == 21)) {
                    background = g_oBGBid1;
                    foreground = g_oFGBid1;
                } else if ((modelColumn == 22) || (modelColumn == 23)) {
                    background = g_oBGAsk1;
                    foreground = g_oFGAsk1;
                } else if (modelColumn == 36) {
                    try {
                        if (((DoubleTransferObject) (value)).getValue() > 1) {
                            background = g_oBGBid1;
                            foreground = g_oFGBid1;
                        } else if ((Math.abs(((DoubleTransferObject) (value)).getValue()) < 1)) {
                            background = g_oBGAsk1;
                            foreground = g_oFGAsk1;
                        } else {
                            background = g_oBG1;
                            foreground = g_oFG1;
                        }
                    } catch (Exception e) {
                        background = g_oBG1;
                        foreground = g_oFG1;
                    }
                } else {
                    background = g_oBG1;
                    foreground = g_oFG1;
                }
//                foreground = g_oFG1;
            } else {
                if ((modelColumn == 4) || (modelColumn == 5)) {
                    background = g_oBGTrade2;
                    foreground = g_oFGTrade2;
                } else if ((modelColumn == 20) || (modelColumn == 21)) {
                    background = g_oBGBid2;
                    foreground = g_oFGBid2;
                } else if ((modelColumn == 22) || (modelColumn == 23)) {
                    background = g_oBGAsk2;
                    foreground = g_oFGAsk2;
                } else if (modelColumn == 36) {
                    try {
                        if (((DoubleTransferObject) (value)).getValue() > 1) {
                            background = g_oBGBid2;
                            foreground = g_oFGBid2;
                        } else if ((Math.abs(((DoubleTransferObject) (value)).getValue()) < 1)) {
                            background = g_oBGAsk2;
                            foreground = g_oFGAsk2;
                        } else {
                            background = g_oBG2;
                            foreground = g_oFG2;
                        }
                    } catch (Exception e) {
                        background = g_oBG1;
                        foreground = g_oFG1;
                    }
                } else {
                    background = g_oBG2;
                    foreground = g_oFG2;
                }
//                foreground = g_oFG2;
            }
        }

        tableDecimals = ((DraggableTable) table).getDecimalPlaces();
        try {
            stockDecimals = (Byte) table.getModel().getValueAt(row, -4);
        } catch (Exception e) {
            stockDecimals = 2;
        }
        try {
            currencyDecimals = (Integer) table.getModel().getValueAt(row, -14);
        } catch (Exception e) {
            currencyDecimals = 2;
        }
        applyCurremcyDecimalPlaces(currencyDecimals);
        if (tableDecimals == Constants.UNASSIGNED_DECIMAL_PLACES) { // do not use table's decimal places
            if (selectedDecimals != stockDecimals) {
                applyDecimalPlaces(stockDecimals);
            }
        } else { // use decimals from table
            if (selectedDecimals != tableDecimals) {
                applyDecimalPlaces(tableDecimals);
            }
        }

        //-------------- Added by Shanika ------

        try {
            isSymbolEnable = (Boolean) table.getModel().getValueAt(row, -13);
        } catch (Exception e) {
            isSymbolEnable = true;
        }

        try {
            symbolStatus = (Integer) table.getModel().getValueAt(row, -5);
        } catch (Exception e) {
            symbolStatus = 0;
        }
        lblRenderer.setStatus(symbolStatus);


        if ((((DraggableTable) table).isDragging()) && (row == ((DraggableTable) table).getMouseRow())) {
            lblRenderer.setBorder(dropBorder);
        } else {
            lblRenderer.setBorder(cellBorder);
        }

        try {
            iRendID = g_asRendIDs[modelColumn];
        } catch (Exception e) {
            iRendID = 3;
        }

        try {
            if (((TableThemeSettings) table).isCuatomThemeEnabled()) { //Bug ID <#0018> row selection not visible if MIN / MAX color is selected
                if (isSelected) {

                } else {
                    tempInt = ((IntTransferObject) table.getModel().getValueAt(row, 47)).getValue();
                    if (Menus.isShowMinMax && Menus.isMinMaxHighlightRows) {
                        if (tempInt == Stock.MAX) {
                            if (((TableThemeSettings) table).isMaxEnabled()) {
                                background = ((TableThemeSettings) table).getMaxBGColor();
                                foreground = ((TableThemeSettings) table).getMaxFGColor();
                            }
                        } else if (tempInt == Stock.MIN) {
                            if (((TableThemeSettings) table).isMinEnabled()) {
                                background = ((TableThemeSettings) table).getMinBGColor();
                                foreground = ((TableThemeSettings) table).getMinFGColor();
                            }
                        }
                    }
                    tempInt = ((IntTransferObject) table.getModel().getValueAt(row, -10)).getValue();
                    if (Menus.isShow52WkhighLow && Menus.is52WeekHighlightRows) {
                        if (tempInt == Stock.BEYOND52WEEKHIGH) {
                            if (((TableThemeSettings) table).isWk52HighEnabled()) {
                                background = ((TableThemeSettings) table).getWeek52HighBGColor();
                                foreground = ((TableThemeSettings) table).getWeek52HighFGColor();
                            }
                        } else if (tempInt == Stock.BEYOND52WEEKLOW) {
                            if (((TableThemeSettings) table).isWk52LowEnabled()) {
                                background = ((TableThemeSettings) table).getWeek52LowBGColor();
                                foreground = ((TableThemeSettings) table).getWeek52LowFGColor();
                            }
                        }
                    }
                }

            }
            //------ Added by Shanika------
            else { //Bug ID <#0018> row selection not visible if MIN / MAX color is selected
                if (isSelected) {

                } else {
                    tempInt = ((IntTransferObject) table.getModel().getValueAt(row, 47)).getValue();
                    if (Menus.isShowMinMax && Menus.isMinMaxHighlightRows) {
                        if (tempInt == Stock.MAX) {
                            if (((TableThemeSettings) table).isMaxEnabled()) {
                                background = maxBG;
                                foreground = maxFG;
                            }
                        } else if (tempInt == Stock.MIN) {
                            if (((TableThemeSettings) table).isMinEnabled()) {
                                background = minBG;
                                foreground = minFG;
                            }
                        }
                    }
                    tempInt = ((IntTransferObject) table.getModel().getValueAt(row, -10)).getValue();
                    if (Menus.isShow52WkhighLow && Menus.is52WeekHighlightRows) {
                        if (tempInt == Stock.BEYOND52WEEKHIGH) {
                            if (((TableThemeSettings) table).isWk52HighEnabled()) {
                                background = week52HighBG;
                                foreground = week52HighFG;
                            }
                        } else if (tempInt == Stock.BEYOND52WEEKLOW) {
                            if (((TableThemeSettings) table).isWk52LowEnabled()) {
                                background = week52LowBG;
                                foreground = week52LowFG;
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            // ignore
        }

        try {
            tempZone = ((LongTransferObject) table.getModel().getValueAt(row, 49)).getValue();
        } catch (Exception e) {
            tempZone = 0;
        }

        //--------------------------------- Added by Shanika-----
        /* if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
            String sArray = (String) table.getModel().getValueAt(row, -20);
            if ((sArray != null) && (!sArray.isEmpty())) {
                if (OptimizedTreeRenderer.selectedSymbols.containsKey(sArray)) {
                    if (OptimizedTreeRenderer.selectedSymbols.get(sArray).equals(true)) {
                        lblRenderer.setBackground(background);
                    } else {
                        lblRenderer.setBackground(disabled_SymbolBG);
                    }
                } else {
                    lblRenderer.setBackground(disabled_SymbolBG);
                    //            MainUI.PRESS_OK = false;
                }
            } else {
                lblRenderer.setBackground(background);
            }
        } else {
            lblRenderer.setBackground(background);
        }*/

        if (Exchange.isBandwidthOpEnabled()) {
            if (isSymbolEnable) {
                lblRenderer.setBackground(background);
            } else {
                lblRenderer.setBackground(disabled_SymbolBG);
            }
        } else {
            lblRenderer.setBackground(background);
        }


        lblRenderer.setForeground(foreground);
        if (isSelected) {
            if (((TableThemeSettings) table).isCuatomThemeEnabled()) {
                background = ((TableThemeSettings) table).getHighlighterBGColor();
            } else {
                background = g_oSelectedBG;
            }
            lblRenderer.setBackground(background);
        }
        try {
            lblRenderer.setIcon(null);
            lblRenderer.setOpaque(true);
            variationImage.setHeight(table.getRowHeight());

            try {
                blankCell = ((TransferObject) value).isBlank();
            } catch (Exception e) {
                blankCell = false;
            }

            if (blankCell) {
                lblRenderer.setText("");
                return lblRenderer;
            }

            if (isSymbolEnable) {
                switch (iRendID) {
                    case 0: // DEFAULT
                        lblRenderer.setText(" " + ((StringTransferObject) value).getValue());
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        break;
                    case 1: // String align  with news / announcement bulbs
                        if (Settings.isShowArabicNumbers())
                            lblRenderer.setText(" " + GUISettings.arabize(((StringTransferObject) value).getValue()));
                        else
                            lblRenderer.setText(" " + ((StringTransferObject) value).getValue());
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        lblRenderer.setHorizontalTextPosition(g_iStringAlign);
                        try {
                            eventBulb.setAnnouncementStatus((Integer) table.getModel().getValueAt(row, -6));
                            eventBulb.setNewsStatus((Integer) table.getModel().getValueAt(row, -7));
                            /*eventBulb.setEventStatus((Integer) table.getModel().getValueAt(row, -8));*/

                            int temp = ((IntTransferObject) table.getModel().getValueAt(row, -10)).getValue();
                            if (Menus.isShow52WkhighLow && !Menus.is52WeekHighlightRows) {
                                eventBulb.set52WeekStatus(temp);
                            }
                            int minMax = ((IntTransferObject) table.getModel().getValueAt(row, 47)).getValue();
                            if (Menus.isShowMinMax && !Menus.isMinMaxHighlightRows) {
                                eventBulb.setMinMaxStatus(minMax);
                            }

                            lblRenderer.setIcon(eventBulb);

                        } catch (Exception e) {
                            // ignore
                        }
                        break;
                    case 2: //
                        lblRenderer.setText(((StringTransferObject) value).getValue());
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        break;
                    case 'R': //
                        lblRenderer.setText((String) value);
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 3: // DECIMAL
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            lblRenderer.setText(g_sNA);
                        } else if ((modelColumn == 105 && doubleValue == 0)) {
                            lblRenderer.setText("  " + g_sNA + "  ");
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue)));
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'n': // MIN-MAX
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            lblRenderer.setText(g_sNA);
                        } else if (doubleValue <= 0) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue)));
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'v': // DECIMAL
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        lblRenderer.setText((oVariableDecimal.format(doubleValue)));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'd':  // DECIMAL WITH '-' FOR MINUS VALUES
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue)) || (doubleValue == -1)) {
                            lblRenderer.setText(g_Sline);
                            lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        } else {
                            doubleValue = ((DoubleTransferObject) (value)).getValue();
                            lblRenderer.setText((oVariableDecimal.format(doubleValue)));
                            lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                        }
                        break;
                    case 'B': // DECIMAL
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue)) || (doubleValue == -1)) {
                            lblRenderer.setText(g_sNA);
                        } else if ((doubleValue == Double.MAX_VALUE) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            lblRenderer.setText(g_sBID);
                        } else if ((doubleValue == 0)) {
                            lblRenderer.setText(g_sASK);
                        } else {
                            lblRenderer.setText(oPriceFormat.format(doubleValue));
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        //System.out.println(doubleValue);
                        break;
                    case 'S': // Spread
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue))) {
                            lblRenderer.setText(g_sNA);
                        } else if (doubleValue == Double.POSITIVE_INFINITY) {
                            lblRenderer.setText(g_sBID);
                        } else if (doubleValue == Double.NEGATIVE_INFINITY) {
                            lblRenderer.setText(g_sASK);
                        } else {
                            lblRenderer.setText(oPriceFormat.format(doubleValue));
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'b': // Bid Ask Map
                        doubleValue = ((VariationImageTransferObject) (value)).getValue();
                        exchange = ((VariationImageTransferObject) (value)).getExchange();
                        variationImage.setType(VariationImage.TYPE_BID_ASK);
                        variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                        variationImage.setExchange(exchange);
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == -1)) {
                            variationImage.setValue(0);
                            lblRenderer.setText("");
                        } else if ((doubleValue == Double.MAX_VALUE) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            lblRenderer.setForeground(bidask_BidColor);
                            lblRenderer.setText(g_sBID);
                        } else if ((doubleValue == 0)) {
                            lblRenderer.setForeground(bidask_AskColor);
                            lblRenderer.setText(g_sASK);
                        } else {
                            variationImage.setValue(doubleValue);
                            lblRenderer.setIcon(variationImage);
                            lblRenderer.setText("");
                        }
                        lblRenderer.setBackground(Color.BLACK);
                        if (isSelected) {
                            lblRenderer.setBorder(imageBorder);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                        break;
                    case 'o':
                        ohlcValues = ((VariationImageTransferObject) (value)).getValueArray();
                        variationImage.setType(VariationImage.TYPE_OHLC_MAP);
                        variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        variationImage.setValues(ohlcValues);
                        lblRenderer.setIcon(variationImage);
                        lblRenderer.setText("");
                        lblRenderer.setBackground(Color.BLACK);
                        if (isSelected) {
                            lblRenderer.setBorder(imageBorder);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                        break;
                    case 's': // Spread Map
                        doubleValue = ((VariationImageTransferObject) (value)).getValue();
                        exchange = ((VariationImageTransferObject) (value)).getExchange();
                        variationImage.setType(VariationImage.TYPE_SPREAD);
                        variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                        variationImage.setExchange(exchange);
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        if ((Double.isNaN(doubleValue))) {
                            lblRenderer.setText("");
                            lblRenderer.setForeground(Color.white);
                            variationImage.setValue(0);
                        } else if (doubleValue == Double.POSITIVE_INFINITY) {
                            lblRenderer.setText(g_sBID);
                            lblRenderer.setForeground(Color.green);
                            variationImage.setValue(0);
                        } else if (doubleValue == Double.NEGATIVE_INFINITY) {
                            lblRenderer.setText(g_sASK);
                            lblRenderer.setForeground(Color.red);
                            variationImage.setValue(0);
                        } else {
                            variationImage.setValue(doubleValue);
                            lblRenderer.setIcon(variationImage);
                            lblRenderer.setText("");
                        }
                        lblRenderer.setBackground(Color.BLACK);
                        if (isSelected) {
                            lblRenderer.setBorder(imageBorder);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                        break;
                    case 'r': // Range Map
                        doubleValue = ((VariationImageTransferObject) (value)).getValue();
                        exchange = ((VariationImageTransferObject) (value)).getExchange();
                        variationImage.setType(VariationImage.TYPE_RANGE);
                        variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                        variationImage.setExchange(exchange);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            variationImage.setValue(0);
                            lblRenderer.setText("");
                        } else {
                            variationImage.setValue(doubleValue);
                            lblRenderer.setIcon(variationImage);
                            lblRenderer.setText("");
                        }
                        lblRenderer.setBackground(Color.BLACK);
                        if (isSelected) {
                            lblRenderer.setBorder(imageBorder);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                        break;
                    case 'c': // Change Map
                        doubleValue = ((VariationImageTransferObject) (value)).getValue();
                        exchange = ((VariationImageTransferObject) (value)).getExchange();
                        variationImage.setType(VariationImage.TYPE_CHANGE);
                        variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                        variationImage.setExchange(exchange);
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            variationImage.setValue(0);
                            lblRenderer.setText("");
                        } else {
                            variationImage.setValue(doubleValue);
                            lblRenderer.setIcon(variationImage);
                            lblRenderer.setText("");
                        }
                        lblRenderer.setBackground(Color.BLACK);
                        if (isSelected) {
                            lblRenderer.setBorder(imageBorder);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                        break;
                    case 'h': // Cash Map
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        variationImage.setType(VariationImage.TYPE_CASH_MAP);
                        variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        lblRenderer.setHorizontalTextPosition(g_iCenterAlign);
                        lblRenderer.setForeground(Color.black);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            variationImage.setValue(0);
                            lblRenderer.setText("");
                        } else {
                            variationImage.setValue(doubleValue);
                            lblRenderer.setIcon(variationImage);
                            lblRenderer.setText(oPChangeFormat.format(doubleValue * 100));
                        }
                        lblRenderer.setBackground(Color.BLACK);
                        if (isSelected) {
                            lblRenderer.setBorder(imageBorder);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                        break;
                    case 't': // Tick Map
                        longValue = ((LongTransferObject) (value)).getValue();
                        variationImage.setType(VariationImage.TYPE_TICK);
                        variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        variationImage.setValue((int) longValue);
                        lblRenderer.setIcon(variationImage);
                        //renderer.setBackground(Color.BLACK);
                        lblRenderer.setText("");
                        if (isSelected) {
                            lblRenderer.setBorder(imageBorder);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                        break;
                    case 'F': // Price with background highlight
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        updateDirection = ((DoubleTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        //floatArray = (float[]) value;
                        if (doubleValue == Constants.DEPTH_PRICE_MARKET_ORDER_VALUE) {
                            lblRenderer.setText(priceMarketOrder + "  ");
                        } else if (doubleValue == Constants.DEPTH_PRICE_MARKET_ON_OPENING_VALUE) {
                            lblRenderer.setText(priceMarketOnOpening + "  ");
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue)));
                        }

//                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                        if (Math.abs(now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(upBGColor);
                                lblRenderer.setForeground(upFGColor);
                            } else if (updateDirection < 0) {
                                lblRenderer.setForeground(downFGColor);
                                lblRenderer.setBackground(downBGColor);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'D': // double value with background highlight
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue))) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            updateDirection = ((DoubleTransferObject) (value)).getFlag();
                            now = System.currentTimeMillis();
                            lblRenderer.setText((oQuantityFormat.format(doubleValue)));
                            if (Math.abs(now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                                if (updateDirection > 0) {
                                    lblRenderer.setBackground(upBGColor);
                                    lblRenderer.setForeground(upFGColor);
                                } else if (updateDirection < 0) {
                                    lblRenderer.setForeground(downFGColor);
                                    lblRenderer.setBackground(downBGColor);
                                }
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'Q': // Quantity with coloured bg
                        longValue = ((LongTransferObject) (value)).getValue();
                        updateDirection = ((LongTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        lblRenderer.setText((oQuantityFormat.format(longValue)));
                        if (Math.abs(now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(upBGColor);
                                lblRenderer.setForeground(upFGColor);
                            } else if (updateDirection > 0) {
                                lblRenderer.setForeground(downFGColor);
                                lblRenderer.setBackground(downBGColor);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'V': // Quantity (Volume) with coloured Fg
                        longValue = ((LongTransferObject) (value)).getValue();
                        updateDirection = ((LongTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        lblRenderer.setText((oQuantityFormat.format(longValue)));
                        if (Math.abs(now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setForeground(volumeFG);
                            } else if (updateDirection > 0) {
                                lblRenderer.setForeground(changeDownFG);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'l':
                        longValue = ((LongTransferObject) (value)).getValue();
                        if (longValue == -1) {
                            lblRenderer.setText(g_Sline);
                            lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        } else {
                            lblRenderer.setText((oQuantityFormat.format(longValue)));
                            lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                        }

                        break;
                    case 4: // QUANTITY
                        lblRenderer.setText((oQuantityFormat.format(((LongTransferObject) (value)).getValue())));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 5: // CHANGE
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue))) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            lblRenderer.setText((oChangeFormat.format(doubleValue)));
                            if (doubleValue > 0) {
                                lblRenderer.setForeground(changeUpFG);
                            } else if (doubleValue < 0) {
                                lblRenderer.setForeground(changeDownFG);
                            } else
                                lblRenderer.setForeground(foreground);
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;

                    case 'p': // CHANGE
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue))) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            lblRenderer.setText((oChangeFormat.format(doubleValue)));
                            lblRenderer.setForeground(foreground);
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 6: // % CHANGE
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        lblRenderer.setText((oPChangeFormat.format(doubleValue)));
                        if (doubleValue > 0) {
                            lblRenderer.setForeground(changeUpFG);
                        } else if (doubleValue < 0) {
                            lblRenderer.setForeground(changeDownFG);
                        } else
                            lblRenderer.setForeground(foreground);
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 7: // DATE
                        longArray = (long[]) value;
                        if (longArray[0] <= 86400000)
                            lblRenderer.setText(g_sNA);
                        else {
                            date.setTime(longArray[0] + tempZone);
                            lblRenderer.setText((g_oDateFormat.format(date)));
                        }
                        if (longArray[0] > longArray[1]) {
                            lblRenderer.setBackground(upBGColor);
                            lblRenderer.setForeground(upFGColor);
                        } else if (longArray[0] < longArray[1]) {
                            lblRenderer.setForeground(downFGColor);
                            lblRenderer.setBackground(downBGColor);
                        }
                        longArray[1] = longArray[0];
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 8: // TIME
                        longValue = ((LongTransferObject) (value)).getValue();
                        if (longValue <= 86400000)
                            lblRenderer.setText(g_sNA);
                        else {
                            date.setTime(longValue + tempZone);
                            lblRenderer.setText((g_oDateTimeFormat.format(date)));
                        }
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        break;
                    case 9: // TICK
                        longValue = ((LongTransferObject) (value)).getValue();
                        switch ((int) longValue) {
                            case Settings.TICK_UP:
                                lblRenderer.setIcon(g_oUPImage);
                                lblRenderer.setText("");
                                break;
                            case Settings.TICK_DOWN:
                                lblRenderer.setIcon(g_oDownImage);
                                lblRenderer.setText("");
                                break;
                            case Settings.TICK_POSITIVE_EQUAL:
                                lblRenderer.setIcon(positiveEqualImage);
                                lblRenderer.setText("");
                                break;
                            case Settings.TICK_NEGATIVE_EQUAL:
                                lblRenderer.setIcon(negEqualImage);
                                lblRenderer.setText("");
                                break;
                            default:
                                lblRenderer.setIcon(null);
                                lblRenderer.setText("");
                        }
                        lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                        break;
                    case 'L': // DATE
                        longValue = ((LongTransferObject) (value)).getValue();
                        if (longValue == 0)
                            lblRenderer.setText(g_sNA);
                        else {
                            date.setTime(longValue + tempZone);
                            lblRenderer.setText(g_oDateFormat.format(date));
                            lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        }
                        break;
                    case 'q': // OPEN
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue)));
                            if ((Integer) table.getModel().getValueAt(row, -9) == Constants.STOCK_OPEN_ABOVE_PREV_CLOSED) {
                                lblRenderer.setForeground(changeUpFG);
                            } else if ((Integer) table.getModel().getValueAt(row, -9) == Constants.STOCK_OPEN_BELOW_PREV_CLOSED) {
                                lblRenderer.setForeground(changeDownFG);
                            } else {
                                //donothing
                                lblRenderer.setForeground(foreground);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'w': // Turnover with currency decimals
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        updateDirection = ((DoubleTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        //floatArray = (float[]) value;
                        lblRenderer.setText((oCurrencyPriceFormat.format(doubleValue)));
                        if (Math.abs(now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(upBGColor);
                                lblRenderer.setForeground(upFGColor);
                            } else if (updateDirection < 0) {
                                lblRenderer.setForeground(downFGColor);
                                lblRenderer.setBackground(downBGColor);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    /*case 'i': //double for Intrinsic Value - NA for instrument types other than 66 -- modified by shanaka
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if (doubleValue == -0) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue)));
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;*/
                    case 'A':
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            lblRenderer.setText(g_sNA);
                        } else if ((modelColumn == 105 && doubleValue == 0)) {
                            lblRenderer.setText("  " + g_sNA + "  ");
                        } else {
                            lblRenderer.setText("");
                        }
                        if (doubleValue == 1) {
                            lblRenderer.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/yellow_flag.png"));
                            lblRenderer.setFlagValue(1);
                        } else if (doubleValue == 2) {
                            lblRenderer.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/orange_flag.png"));
                            lblRenderer.setFlagValue(2);
                        } else if (doubleValue == 3) {
                            lblRenderer.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/red_flag.png"));
                            lblRenderer.setFlagValue(3);
                        }
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        lblRenderer.setHorizontalTextPosition(g_iCenterAlign);
                        break;

                    default:
                        lblRenderer.setText("");

                }
            } else {
                switch (iRendID) {
                    case 0: // DEFAULT
                        lblRenderer.setText(" " + ((StringTransferObject) value).getValue());
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        break;

                    case 1: // String align  with news / announcement bulbs
                        if (Settings.isShowArabicNumbers())
                            lblRenderer.setText(" " + GUISettings.arabize(((StringTransferObject) value).getValue()));
                        else
                            lblRenderer.setText(" " + ((StringTransferObject) value).getValue());
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        lblRenderer.setHorizontalTextPosition(g_iStringAlign);
                        try {
                            eventBulb.setAnnouncementStatus((Integer) table.getModel().getValueAt(row, -6));
                            eventBulb.setNewsStatus((Integer) table.getModel().getValueAt(row, -7));
//                            eventBulb.setEventStatus((Integer) table.getModel().getValueAt(row, -8));
//                        eventBulb.setNewsEventStatus((Integer)table.getModel().getValueAt(row, -12));                                       //(Integer)table.getModel().getValueAt(row, -12)

                            lblRenderer.setIcon(eventBulb);
                        } catch (Exception e) {
                            // ignore
                        }

                        break;
                    default:
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);

                        lblRenderer.setText("�");
                }
            }


        } catch (Exception e) {
            lblRenderer.setText("");
        }
        return lblRenderer;
    }

    private void applyDecimalPlaces(int decimalPlaces) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                break;
            case Constants.ONE_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case Constants.TWO_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case Constants.THREE_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
            case Constants.FIVE_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                break;
            case Constants.SIX_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                break;
            case Constants.SEVEN_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL);
                break;
            case Constants.EIGHT_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL);
                break;

            case Constants.ONE_DECIMAL_PLACES_NOZERO:
                oCumReturnFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL_NOZERO);
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL_NOZERO);
                oChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL_NOZERO);
                break;
            case Constants.TWO_DECIMAL_PLACES_NOZERO:
                oCumReturnFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL_NOZERO);
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL_NOZERO);
                oChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL_NOZERO);
                break;
            case Constants.THREE_DECIMAL_PLACES_NOZERO:
                oCumReturnFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL_NOZERO);
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL_NOZERO);
                oChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL_NOZERO);
                break;
            case Constants.FOUR_DECIMAL_PLACES_NOZERO:
                oCumReturnFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL_NOZERO);
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL_NOZERO);
                oChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL_NOZERO);
                break;
            case Constants.FIVE_DECIMAL_PLACES_NOZERO:
                oCumReturnFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL_NOZERO);
                oPriceFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL_NOZERO);
                oChangeFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL_NOZERO);
                break;
            case Constants.SIX_DECIMAL_PLACES_NOZERO:
                oCumReturnFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL_NOZERO);
                oPriceFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL_NOZERO);
                oChangeFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL_NOZERO);
                break;
            case Constants.SEVEN_DECIMAL_PLACES_NOZERO:
                oCumReturnFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL_NOZERO);
                oPriceFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL_NOZERO);
                oChangeFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL_NOZERO);
                break;
            case Constants.EIGHT_DECIMAL_PLACES_NOZERO:
                oCumReturnFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL_NOZERO);
                oPriceFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL_NOZERO);
                oChangeFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL_NOZERO);
                break;
            default:
                oCumReturnFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
        }
        this.selectedDecimals = decimalPlaces;
    }

    private void applyCurremcyDecimalPlaces(int decimalPlaces) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                break;
            case Constants.ONE_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case Constants.TWO_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case Constants.THREE_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
            case Constants.FIVE_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                break;
            case Constants.SIX_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                break;
            case Constants.SEVEN_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL);
                break;
            case Constants.EIGHT_DECIMAL_PLACES:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL);
                break;

            case Constants.ONE_DECIMAL_PLACES_NOZERO:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL_NOZERO);
                break;
            case Constants.TWO_DECIMAL_PLACES_NOZERO:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL_NOZERO);
                break;
            case Constants.THREE_DECIMAL_PLACES_NOZERO:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL_NOZERO);
                break;
            case Constants.FOUR_DECIMAL_PLACES_NOZERO:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL_NOZERO);
                break;
            case Constants.FIVE_DECIMAL_PLACES_NOZERO:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL_NOZERO);
                break;
            case Constants.SIX_DECIMAL_PLACES_NOZERO:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL_NOZERO);
                break;
            case Constants.SEVEN_DECIMAL_PLACES_NOZERO:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL_NOZERO);
                break;
            case Constants.EIGHT_DECIMAL_PLACES_NOZERO:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL_NOZERO);
                break;
            default:
                oCurrencyPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
        }
    }

    public void getCurrentData(TableThemeSettings target) {
        if (target.isCuatomThemeEnabled()) return;
        target.setUpBGColor(g_oUpBGColor);
        target.setUpFGColor(g_oUpFGColor);
        target.setDownBGColor(g_oDownBGColor);
        target.setDownFGColor(g_oDownFGColor);

        target.setRow1BGColor(g_oBG1);
        target.setRow1FGColor(g_oFG1);
        target.setRow2BGColor(g_oBG2);
        target.setRow2FGColor(g_oFG2);

        target.setHighlighterBGColor(g_oSelectedBG);
        target.setHighlighterFGColor(g_oSelectedFG);

        target.setTradeBGColor1(g_oBGTrade1);
        target.setTradeFGColor1(g_oFG1);
        target.setTradeBGColor2(g_oBGTrade2);
        target.setTradeFGColor2(g_oFG2);

        target.setBidBGColor1(g_oBGBid1);
        target.setBidFGColor1(g_oFG1);
        target.setBidBGColor2(g_oBGBid2);
        target.setBidFGColor2(g_oFG2);

        target.setAskBGColor1(g_oBGAsk1);
        target.setAskFGColor1(g_oFG1);
        target.setAskBGColor2(g_oBGAsk2);
        target.setAskFGColor2(g_oFG2);

        target.setChangeDownFGColor(changeDownFG);
        target.setChangeUPFGColor(changeUpFG);

        target.setMinBGColor(minBG);
        target.setMinFGColor(minFG);

        target.setMaxBGColor(maxBG);
        target.setMaxFGColor(maxFG);

        target.setWeek52LowBGColor(week52LowBG);
        target.setWeek52LowFGColor(week52LowFG);

        target.setWeek52HighBGColor(week52HighBG);
        target.setWeek52HighFGColor(week52HighFG);

        target.setTableGridColor(gridColor);


    }
}
