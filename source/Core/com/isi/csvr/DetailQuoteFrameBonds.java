package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.BondDetailQuoteModel;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Jul 28, 2009
 * Time: 10:06:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class DetailQuoteFrameBonds extends InternalFrame implements LinkedWindowListener {
    String selectedKey;
    boolean loadedFromWorkspace;
    ViewSetting oViewSetting;
    Table table;
    BondDetailQuoteModel dqModel;
    ViewSettingsManager g_oViewSettings;
    Dimension dim;
    int fontsize = 0;
    private boolean symbolRegistered;
    private boolean isLinked;
    private boolean isSymbolDTQ;


    public DetailQuoteFrameBonds(Table oTable, WindowDaemon oDaemon, String selectedKey, boolean loadedFromWorkspace, boolean symbolRegistered, boolean isLinked, String linkGroup) {
        super(oTable, oDaemon);
        this.selectedKey = selectedKey;
        this.loadedFromWorkspace = loadedFromWorkspace;
        table = oTable;
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        this.symbolRegistered = symbolRegistered;
        this.isLinked = isLinked;
        setLinkedGroupID(linkGroup);

        int insType = SharedMethods.getInstrumentTypeFromKey(this.selectedKey);
        if ((insType == 0) || (insType == 68)) {
            isSymbolDTQ = true;
        } else {
            isSymbolDTQ = false;
        }
        initUI();
    }

    public void initUI() {
        table.hideHeader();
        table.getPopup().hideCopywHeadMenu();
        table.setSortingDisabled();
        table.setDQTableType();
        table.hideCustomizer();
        table.getPopup().showSetDefaultMenu();
        if (isSymbolDTQ) {
            table.getPopup().hideCustomizerMenu();
        } else {
            table.getPopup().showCustomizerMenu();
        }
        table.setWindowType(ViewSettingsManager.DETAIL_QUOTE);

        if (oViewSetting == null && loadedFromWorkspace && isLinked) {
            ViewSetting oSetting = g_oViewSettings.getDTQView(ViewSettingsManager.DETAIL_QUOTE + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                oViewSetting = oSetting;
            }
        }
        if (oViewSetting == null) {
            oViewSetting = g_oViewSettings.getDTQView(ViewSettingsManager.DETAIL_QUOTE
                    + "|" + selectedKey);
        }
        if (oViewSetting == null) {
            oViewSetting = g_oViewSettings.getSymbolView("DETAIL_QUOTE");
            oViewSetting = oViewSetting.getObject(); // clone the object
            SharedMethods.applyCustomViewSetting(oViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
            // oViewSetting.setID(selectedKey);
            oViewSetting.setID(System.currentTimeMillis() + "");
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("DETAIL_QUOTE_STOCK_COLS"));
            g_oViewSettings.putDTQView(ViewSettingsManager.DETAIL_QUOTE
                    + "|" + selectedKey, oViewSetting);
            //todo added by Dilum
            fontsize = oViewSetting.getFont().getSize();
        } else {
            loadedFromWorkspace = true;
        }

        dqModel = new BondDetailQuoteModel();
        dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(selectedKey));
        dqModel.setSymbol(selectedKey);
        dqModel.setViewSettings(oViewSetting);
        table.setModel(dqModel);
        dqModel.setTable(table);
        table.setUseSameFontForHeader(true);
        table.getTable().getTableHeader().setReorderingAllowed(false);
        dqModel.applyColumnSettings();

        table.updateGUI();
        table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        table.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        if (fontsize == 0) {
            fontsize = 18;
        }

        oViewSetting.setParent(this);
        this.setShowServicesMenu(true);
        this.setDetachable(true);
        this.setPrintable(true);
//            frame.setSize(oViewSetting.getSize());
        this.setLocation(oViewSetting.getLocation());
        this.getContentPane().add(table);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setLinkGroupsEnabled(true);
        this.addComponentListener(this);
        Client.getInstance().oTopDesktop.add(this);
        if (!symbolRegistered) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        } else if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }
        this.setTitle(Language.getString("DETAIL_QUOTE") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getStockObject(selectedKey).getLongDescription());
        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        this.updateUI();
        this.applySettings();

        dqModel.updateGUI();


        this.setLayer(GUISettings.TOP_LAYER);
        this.setOrientation();
        if (isLinked && loadedFromWorkspace) {
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE + "|" +
                    LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE + "|" +
                    selectedKey, this);
        }
        //((SmartTable) table.getTable()).adjustColumnWidthsToFit(40);
        this.updateUI();
        table.packFrame();

        if (!loadedFromWorkspace) {
//                frame.setLocationRelativeTo(oTopDesktop);
//                oViewSetting.setLocation(frame.getLocation());
            if (!oViewSetting.isLocationValid()) {
                this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                oViewSetting.setLocation(this.getLocation());
            } else {
                this.setLocation(oViewSetting.getLocation());
            }
        }
        this.setOrigDimention(this.getSize());
        this.show();
        this.setisLoadedFromWsp(false);

    }

    public void componentResized(ComponentEvent e) {
        if (this.getOrigDimension().height > 0 && this.getOrigDimension().width > 0) {
            if (!table.isFontResized()) {
                dim = reSizeSpecial(this.getSize(), this.getOldSize());
                if (!this.getSize().equals(dim)) {
                    resizeRows(dim, this.getOrigDimension().height);
                }
                this.setSize(dim);
            } else {
                Dimension olddim = new Dimension(this.getOldSize().width, this.getSize().height);
                dim = resizeToFont(this.getSize(), olddim);
                this.setSize(dim);
                table.setFontResized(false);
            }

        }
        if (dim == null) {
            System.out.println("Dim null default size activated");
            Dimension dim2 = table.getSize();
            this.setSize(dim2);
            this.setisLoadedFromWsp(true);
            this.setOldSize(dim2);
            this.setOrigDimention(new Dimension(620, 489));
            dim = dim2;
            resizeRows(dim2, this.getOrigDimension().height);

        }
        table.getSmartTable().adjustColumnWidthsToFit(10);
    }

    public void resizeRows(Dimension dim, int origHeight) {
        Font oFont = table.getFont();
//                    System.out.println("body font size == "+oFont.getSize());
        if (oFont == null) {
            oFont = table.getTable().getFont();
        }
//                    int defaultsize = oFont.getSize();

        int titlteHeight = this.getTitlebarHeight();
        float ratio = (((float) fontsize) / origHeight);
//                    System.out.println("ratio =="+ratio);
        int newSize = (int) (ratio * dim.height);
//                    System.out.println("new Font size =="+newSize);

        if (oFont != null) {

            Font oFont2 = new TWFont(oFont.getFamily(), oFont.getStyle(), newSize);
//                            viewRef2.setFont(oFont2);
            table.getTable().setFont(oFont2);
            FontMetrics oMetrices = table.getFontMetrics(oFont2);
            int rowCount = table.getTable().getRowCount();
            int emptySpace = ((dim.height - titlteHeight) - 2 - ((oMetrices.getMaxAscent() + oMetrices.getMaxDescent()) * rowCount));
            int newGap = (int) Math.floor((emptySpace / rowCount));

            table.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
            int finspace = (dim.height - titlteHeight) - 4 - (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap) * rowCount;
            if (finspace > 0) {
                int hight = this.dim.height - finspace;
                int width = this.dim.width;
                this.dim.setSize(width, hight);
                this.setOldSize(this.dim);
            }
            oMetrices = null;
            oFont = null;
            oFont2 = null;

        }
    }

    public void symbolChanged(String sKey) {
        if (sKey != null && !sKey.isEmpty()) {
            String oldKey = oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
            if (oldKey != null) {
                DataStore.getSharedInstance().removeSymbolRequest(oldKey);
            }
            selectedKey = sKey;
            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
                DataStore.getSharedInstance().addSymbolRequest(selectedKey);
            }

            dqModel.setSymbol(sKey);
            dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(selectedKey));
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sKey);
            this.setTitle(Language.getString("DETAIL_QUOTE") + " : (" + SharedMethods.getSymbolFromKey(sKey)
                    + ") " + DataStore.getSharedInstance().getStockObject(sKey).getLongDescription());
            this.updateUI();
            this.show();
            // this.updateLinkGroupIconInTitleBar();
        }
    }
}
