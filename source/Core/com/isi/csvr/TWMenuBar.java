package com.isi.csvr;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 5, 2006
 * Time: 10:03:11 PM
 */
public class TWMenuBar extends JMenuBar implements Themeable {

    private ImageIcon tile;
    private int imageWidth;

    public TWMenuBar() {
        Theme.registerComponent(this);
        applyTheme();
    }

    public void applyTheme() {
        tile = new ImageIcon("images/theme" + Theme.getID() + "/menubartile.png");
        imageWidth = tile.getIconWidth();
    }

    public void paint(Graphics g) {
        if (imageWidth > 0) {
            int width = getWidth();
            for (int i = 0; i <= width; i += imageWidth) {
                g.drawImage(tile.getImage(), i, 0, this);
            }

            super.paintChildren(g);
        } else {
            super.paint(g);
        }
    }

    public JMenu add(JMenu c) {
        c.setOpaque(false); // menu must have the same BG as menubar
        return super.add(c);
    }
}
