package com.isi.csvr.indices;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.table.IndicesModal;
import com.isi.csvr.table.Table;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 26-Aug-2008 Time: 09:54:25 To change this template use File | Settings
 * | File Templates.
 */
public class IndicesFrame extends InternalFrame implements ExchangeListener, ApplicationListener, ActionListener {
    private static final int lastSelectedExg = 251;
    public static IndicesFrame self;
    ViewSetting oSetting;
    private Table indicestable;
    private IndicesModal model;
    private JPanel exchangePanel;
    private ArrayList exchanges;
    private TWComboBox exchangeCombo;
    private String lastSelectedExchange = null;
    private TWButton addBtn;
    private String lastExg;

    private IndicesFrame() {
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Application.getInstance().addApplicationListener(this);
        createUI();


    }

    public static IndicesFrame getSharedInstance() {
        if (self == null) {
            self = new IndicesFrame();
        }
        return self;
    }

    public void createUI() {
        indicestable = new Table();
        indicestable.setSortingEnabled();
        final TWMenuItem mnuIndexGraph = new TWMenuItem(Language.getString("GRAPH"), "graph.gif");
        mnuIndexGraph.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().show_indexGraph(indicestable);
            }
        });
        final TWMenuItem removeSymbol = new TWMenuItem(Language.getString("REMOVE_SYMBOL"), "rmsymbol.gif");
        removeSymbol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeSymbol();
            }
        });
        indicestable.getPopup().setMenuItem(mnuIndexGraph);
        indicestable.getPopup().setMenuItem(removeSymbol);
        indicestable.setPreferredSize(new Dimension(500, 100));
        model = new IndicesModal(null);
        oSetting = ViewSettingsManager.getSummaryView("BTW_SECTORS");
        model.setViewSettings(oSetting);
        indicestable.setModel(model);
        model.setTable(indicestable);
        model.applyColumnSettings();
        model.updateGUI();
        this.setTable(indicestable);
        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        oSetting.setParent(this);
//        this.setViewSetting(oSetting);
        this.getContentPane().add(indicestable);
        Client.getInstance().oTopDesktop.add(this);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        this.updateUI();
        this.applySettings();
        this.setLayer(GUISettings.TOP_LAYER);
        this.setVisible(false);
        indicestable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    String symbol = (String) indicestable.getTable().getModel().getValueAt(indicestable.getTable().getSelectedRow(), -3);
                    Client.getInstance().mnu_DetailQuoteIndex(symbol, false);
                    symbol = null;
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    if (((TWComboItem) exchangeCombo.getSelectedItem()).getId().equals("CUSTOM_INDEX")) {
                        mnuIndexGraph.setVisible(false);
                    } else {
                        mnuIndexGraph.setVisible(true);
                    }
                    try {
//                        if (ExchangeStore.getSharedInstance().getExchange(((TWComboItem) exchangeCombo.getSelectedItem()).getId()).isDefault()) {
//                            removeSymbol.setVisible(false);
//                        } else {
                        if (((TWComboItem) exchangeCombo.getSelectedItem()).getId().equals("CUSTOM_INDEX")) {
                            removeSymbol.setVisible(false);
                        } else {
                            if (ExchangeStore.getSharedInstance().getExchange(((TWComboItem) exchangeCombo.getSelectedItem()).getId()).isDefault()) {
                                removeSymbol.setVisible(false);
                            } else {
                                removeSymbol.setVisible(true);
                            }
                        }
//                        }
                    } catch (Exception e1) {
                        removeSymbol.setVisible(true);
                        e1.printStackTrace();
                    }
                    try {
                        indicestable.getPopup().setPopupSize(new Dimension(150, getVisibleMenuItemCount() * 25));
                    } catch (Exception e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    indicestable.getPopup().updateUI();
                    indicestable.getPopup().repaint();
                }
            }
        });

        exchangePanel = getExchangePanel();
        if (TWControl.isLinktoExcelInMarketIndicesEnable()) {
            indicestable.getPopup().showLinkMenus();
        }
        indicestable.setNorthPanel(exchangePanel);
        GUISettings.applyOrientation(this);

    }

    private int getVisibleMenuItemCount() {
        int count = 0;
        int length = indicestable.getPopup().getComponentCount();
        for (int i = 0; i < length; i++) {
            if (indicestable.getPopup().getComponent(i).isVisible()) {
                count++;
            }
        }
        return count;
    }

    private JPanel getExchangePanel() {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"110", "200", "100%", "0"}, new String[]{"22"}, 6, 6, true, true));
        JLabel label = new JLabel(Language.getString("EXCHANGE"));
        addBtn = new TWButton(Language.getString("ADD_GLOBAL_INDEX"));
        addBtn.addActionListener(this);
        panel.add(label);
        exchanges = new ArrayList();
        exchangeCombo = new TWComboBox(new TWComboModel(exchanges));
        popuplateExchanges();
        panel.add(exchangeCombo);
        panel.add(new JLabel());
        panel.add(addBtn);
        panel.doLayout();
        String s = oSetting.getProperty(ViewConstants.VC_MARKETINDICES_EXCHANGE);
        return panel;

    }

    public void popuplateExchanges() {
        exchangeCombo.removeActionListener(this);
        exchanges.clear();
        try {
            for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
                try {
                    Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
                    TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                    if (ExchangeStore.getSharedInstance().isMarketIndicesAvailable(exchange.getSymbol())) {
                        exchanges.add(comboItem);
                    }
                    exchange = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            Collections.sort(exchanges);
            if (ExchangeStore.getSharedInstance().defaultCount() > 0) {
                exchanges.add(new TWComboItem("CUSTOM_INDEX", Language.getString("CUSTOM_INDEX")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            exchangeCombo.updateUI();
//            exchangeCombo.setSelectedIndex(0);
            if (exchangePanel != null)
                exchangePanel.doLayout();
        } catch (Exception e) {
        }
        exchangeCombo.addActionListener(this);
    }

    public void showWindow() {
        setVisible(true);
        try {
            TWComboItem comboItem = (TWComboItem) exchangeCombo.getSelectedItem();
            model.setExchange(comboItem.getId());
            final String exchange = comboItem.getId();
            if ((!exchange.equalsIgnoreCase("CUSTOM_INDEX"))) {
                if ((!ExchangeStore.getSharedInstance().getExchange(exchange).isDefault())) {
                    Vector symbolList = (Vector) model.getIndexTable().get(exchange);
                    for (int i = 0; i < symbolList.size(); i++) {
                        String symbol = (String) symbolList.get(i);
                        if (((ArrayList) model.getComIndexTable().get(exchange)).contains(symbol)) {
                            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX, Meta.CUST_INDEX);
                        } else {
                            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX);
                        }
                        //                    DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX);
                    }
                    symbolList = null;
                } else {
                    Vector symbolList = (Vector) model.getIndexTable().get(exchange);
                    for (int i = 0; i < symbolList.size(); i++) {
                        String symbol = (String) symbolList.get(i);
                        if (((ArrayList) model.getComIndexTable().get(exchange)).contains(symbol)) {
                            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX, Meta.CUST_INDEX);
                        }
                    }
                    symbolList = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void setVisible(boolean value) {
        super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
        if (value) {
            if (Settings.isConnected()) {
                TWComboItem comboItem = (TWComboItem) exchangeCombo.getSelectedItem();
                model.setExchange(comboItem.getId());
                final String exchange = comboItem.getId();
                new Thread() {
                    public void run() {
                        sendIndexRequests(exchange);
                    }
                }.start();

            }
        } else {
            if (Settings.isConnected()) {
                new Thread() {
                    public void run() {
                        removeIndexRequests(lastSelectedExchange);
                    }
                }.start();
                model.savedIndexesForExchanges();

            }
        }
    }*/

    private void sendIndexRequests(String exchange) {
        try {
            if ((lastSelectedExchange != null) && (!lastSelectedExchange.equalsIgnoreCase("CUSTOM_INDEX"))) {
                Vector symbolList = (Vector) model.getIndexTable().get(lastSelectedExchange);
                for (int i = 0; i < symbolList.size(); i++) {
                    String symbol = (String) symbolList.get(i);
                    if (((ArrayList) model.getComIndexTable().get(lastSelectedExchange)).contains(symbol)) {
                        DataStore.getSharedInstance().removeSymbolRequest(lastSelectedExchange, symbol, Meta.INSTRUMENT_INDEX, Meta.CUST_INDEX);
                    } else {
                        DataStore.getSharedInstance().removeSymbolRequest(lastSelectedExchange, symbol, Meta.INSTRUMENT_INDEX);
                    }
                }
                symbolList = null;
            }
            if ((!exchange.equalsIgnoreCase("CUSTOM_INDEX"))) {
                if ((!ExchangeStore.getSharedInstance().getExchange(exchange).isDefault())) {
                    Vector symbolList = (Vector) model.getIndexTable().get(exchange);
                    for (int i = 0; i < symbolList.size(); i++) {
                        String symbol = (String) symbolList.get(i);
                        if (((ArrayList) model.getComIndexTable().get(exchange)).contains(symbol)) {
                            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX, Meta.CUST_INDEX);
                        } else {
                            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX);
                        }
                        //                    DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX);
                    }
                    symbolList = null;
                } else {
                    try {
                        Vector symbolList = (Vector) model.getIndexTable().get(exchange);
                        for (int i = 0; i < symbolList.size(); i++) {
                            String symbol = (String) symbolList.get(i);
                            if (((ArrayList) model.getComIndexTable().get(exchange)).contains(symbol)) {
                                DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX, Meta.CUST_INDEX);
                            }
                        }
                        symbolList = null;
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }

            lastSelectedExchange = exchange;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void removeIndexRequests(String exchange) {
        try {
            if ((exchange != null) && (!exchange.equalsIgnoreCase("CUSTOM_INDEX"))) {
                Vector symbolList = (Vector) model.getIndexTable().get(exchange);
                for (int i = 0; i < symbolList.size(); i++) {
                    String symbol = (String) symbolList.get(i);
                    DataStore.getSharedInstance().removeSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX);
                }
                symbolList = null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == exchangeCombo) {
                TWComboItem comboItem = (TWComboItem) exchangeCombo.getSelectedItem();
                model.setExchange(comboItem.getId());
                String exchange = comboItem.getId();
                sendIndexRequests(exchange);
                try {
                    if ((!ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()) &&
                            (!ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()) &&
                            (!ExchangeStore.getSharedInstance().getExchange(exchange).isExpired())) {
                        addBtn.setEnabled(true);
                        addBtn.setVisible(true);
                    } else {
                        addBtn.setEnabled(false);
                        addBtn.setVisible(false);
                    }
                } catch (Exception e1) {
                    addBtn.setEnabled(false);
                    addBtn.setVisible(false);
//                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                oSetting.putProperty(ViewConstants.VC_MARKETINDICES_EXCHANGE, exchange);
                exchange = null;
            } else if (e.getSource().equals(addBtn)) {
                Symbols symbols = new Symbols();

                SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
                oCompanies.setSingleMode(false);
                oCompanies.setTitle(Language.getString("MARKET_INDICES"));
                TWComboItem comboItem = (TWComboItem) exchangeCombo.getSelectedItem();
                String exchange = comboItem.getId();
                oCompanies.setSelectedExchange(exchange);
                oCompanies.setIndexSearchMode(true);
                oCompanies.init();
                Vector symbolVector = (Vector) model.getIndexTable().get(exchange);
                for (int i = 0; i < symbolVector.size(); i++) {
                    symbols.appendSymbol(SharedMethods.getKey(exchange, (String) symbolVector.get(i), Meta.INSTRUMENT_INDEX));

                }
                oCompanies.setSymbols(symbols);
                oCompanies.setShowDefaultExchangesOnly(false);
                oCompanies.showDialog(true);
                oCompanies = null;
                model.clearStore();
                String[] symbolList = symbols.getSymbols();
                for (int i = 0; i < symbolList.length; i++) {
                    model.addIndex(symbolList[i]);
                }
                symbols = null;
            }
            super.actionPerformed(e);

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void setlastSelectedExchange(String exg) {
        lastExg = exg;
    }

    private void removeSymbol() {
        String symbol = (String) indicestable.getTable().getModel().getValueAt(indicestable.getTable().getSelectedRow(), 0);
        model.removeIndex(symbol);

    }

    private void setInitialParams() {
        try {
            if ((lastExg == null) || (lastExg.equals(""))) {
                exchangeCombo.setSelectedIndex(0);
            } else {
                for (int i = 0; i < exchanges.size(); i++) {
                    TWComboItem item = (TWComboItem) exchanges.get(i);
                    if (item.getId().equals(lastExg)) {
                        exchangeCombo.setSelectedIndex(i);
                        break;

                    } else {
                        exchangeCombo.setSelectedIndex(0);
                    }

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (exchanges.size() > 0) {
                exchangeCombo.setSelectedIndex(0);
            }
        }
        try {
            Exchange exg = ExchangeStore.getSharedInstance().getExchange(((TWComboItem) exchangeCombo.getSelectedItem()).getId());
            if (exg.isDefault()) {
                addBtn.setVisible(false);
            } else {
                addBtn.setVisible(true);
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void save() {
        model.savedIndexesForExchanges();
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        super.internalFrameClosing(e);
        removeIndexRequests(lastSelectedExchange);
        model.savedIndexesForExchanges();
    }

    public void exchangeAdded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesAdded(boolean offlineMode) {
        //To change body of implemented methods use File | Settings | File Templates.
        popuplateExchanges();
        setInitialParams();
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
        popuplateExchanges();
        setInitialParams();
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        setlastSelectedExchange(oSetting.getProperty(ViewConstants.VC_MARKETINDICES_EXCHANGE));
    }

    public void applicationReadyForTransactions() {

    }
}
