package com.isi.csvr.trading.datastore;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 17, 2006
 * Time: 6:18:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockTransferObjectStore {

    private static StockTransferObjectStore self = null;
    private ArrayList<StockTransferObject> store;

    private StockTransferObjectStore() {
        store = new ArrayList<StockTransferObject>();
    }

    public static StockTransferObjectStore getSharedInstance() {
        if (self == null) {
            self = new StockTransferObjectStore();
        }
        return self;
    }

    public void addStockTransObject(StockTransferObject object) {
        store.add(object);
    }

    public StockTransferObject getStockTransObject(int index) {
        return store.get(index);
    }

    public StockTransferObject getStockTransObject(String ID) {
        StockTransferObject obj = null;
        for (int i = 0; i < store.size(); i++) {
            obj = store.get(i);
            if (obj.getID().equals(ID.trim())) {
                return obj;
            }
        }
        return null;
    }

    public int getsize() {
        return store.size();
    }

    public void clear() {
        store.clear();
        store.trimToSize();
    }

}
