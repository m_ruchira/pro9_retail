package com.isi.csvr.trading.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 21, 2007
 * Time: 6:50:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class FutureBaseAttributes {
    private double margin;
    private int maxContracts;
    private int contractSize;
    private double priceCorrectionFactor;

    public FutureBaseAttributes(int contractSize, double margin, int maxContracts, double priceCorrectionFactor) {
        this.contractSize = contractSize;
        this.margin = margin;
        this.maxContracts = maxContracts;
        this.priceCorrectionFactor = priceCorrectionFactor;
    }

    public double getMargin() {
        return margin;
    }

    public int getMaxContracts() {
        return maxContracts;
    }


    public int getContractSize() {
        return contractSize;
    }

    public double getPriceCorrectionFactor() {
        return priceCorrectionFactor;
    }
}

