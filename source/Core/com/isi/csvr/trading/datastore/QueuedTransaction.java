package com.isi.csvr.trading.datastore;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 1, 2004
 * Time: 4:04:07 PM
 */
public class QueuedTransaction extends Transaction implements Serializable {

    private boolean selected;
    private long QID;

    public QueuedTransaction(long qID) {
        this.QID = qID;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public long getQID() {
        return QID;
    }

    public void setQID(int QID) {
        this.QID = QID;
    }
}
