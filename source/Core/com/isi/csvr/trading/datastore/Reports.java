package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.*;
import com.isi.csvr.util.Decompress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 22, 2005
 * Time: 1:54:36 PM
 */
public class Reports {
    private String broker;
    private ArrayList<TWComboItem> reports;

    public Reports(String broker) {
        this.broker = broker;
        reports = new ArrayList<TWComboItem>();
        load();
    }

    private void load() {
        try {
            Decompress decompress = new Decompress();
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/reports_" + Language.getSelectedLanguage().trim() + ".msf";
            ByteArrayOutputStream out;
            out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;

            while (true) {
                record = in.readLine();
                if (record == null) break;
                String[] data = record.split(Meta.DS);
                if (data[0].equals(broker)) {
                    String[] fields = data[1].split(Meta.FS);
                    TWComboItem reportItem = new TWComboItem(fields[1], UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[0])));
                    reports.add(reportItem);
                }
            }
            out.close();
            out = null;
            decompress = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<TWComboItem> getReports() {
        return reports;
    }
}
