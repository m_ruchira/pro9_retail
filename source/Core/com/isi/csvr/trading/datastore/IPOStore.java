package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.TWComboItem;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 24-Jun-2008 Time: 16:48:57 To change this template use File | Settings
 * | File Templates.
 */
public class IPOStore {
    public static IPOStore self;
    public ArrayList<IPORecord> comboList;
    public ArrayList<TWComboItem> comboItemList;
    public DynamicArray<IPOTableRecord> tableRecord;

    private IPOStore() {
        comboList = new ArrayList<IPORecord>();
        comboItemList = new ArrayList<TWComboItem>();
        tableRecord = new DynamicArray<IPOTableRecord>();
    }

    public static IPOStore getSharedInstance() {
        if (self == null) {
            self = new IPOStore();
        }
        return self;
    }

    public ArrayList<IPORecord> getComboList() {
        return comboList;
    }

    public ArrayList<TWComboItem> getComboItemList() {
        return comboItemList;
    }

    public IPOTableRecord getTableRecord(int index) {
        return tableRecord.get(index);
    }

    public void addToIPOCombo(String record) {
        try {
            TradeMessage message = new TradeMessage(record);
            String[] ipoData = message.getMessageData().split(TradeMeta.RS);
            for (int i = 0; i < ipoData.length; i++) {
                try {
                    String[] ipoRecorddata = ipoData[i].split(TradeMeta.FD);
                    try {
                        IPORecord ipo = new IPORecord();
                        ipo.setId(ipoRecorddata[0]);
                        ipo.setDiscriptionEn(ipoRecorddata[1]);
                        ipo.setDiscriptionAr(ipoRecorddata[2]);
                        ipo.setSymbol(ipoRecorddata[3]);
                        ipo.setValue(Double.parseDouble(ipoRecorddata[4]));
                        ipo.setCurrency(ipoRecorddata[5]);
                        ipo.setExchange(ipoRecorddata[6]);
                        comboItemList.add(new TWComboItem(ipo.getId(), ipo.getSymbol()));
                        comboList.add(ipo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addToIPOTableRecords(String record) {
        try {
            TradeMessage message = new TradeMessage(record);
            String[] ipoData = message.getMessageData().split(TradeMeta.RS);
            for (int i = 0; i < ipoData.length; i++) {
                try {
                    String[] ipoRecorddata = ipoData[i].split(TradeMeta.FD);
                    try {
                        IPOTableRecord ipo = new IPOTableRecord();
                        ipo.setDescriptionEn(ipoRecorddata[0]);
                        ipo.setDescriptionAr(ipoRecorddata[1]);
                        ipo.setSymbol(ipoRecorddata[2]);
                        ipo.setQuntity(Long.parseLong(ipoRecorddata[3]));
                        ipo.setTotalValue(Double.parseDouble(ipoRecorddata[4]));
                        ipo.setDate(ipoRecorddata[5]);
                        ipo.setExchange(ipoRecorddata[6]);
                        ipo.setStatus(Integer.parseInt(ipoRecorddata[7]));
                        ipo.setTransactID(ipoRecorddata[8]);
                        ipo.setCurrency(ipoRecorddata[9]);
                        tableRecord.add(ipo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public DynamicArray getIPOTableStore() {
        return tableRecord;
    }

    public IPORecord getIPORecord(String id) {

        IPORecord ipor;
        for (int i = 0; i < comboList.size(); i++) {
            ipor = (IPORecord) comboList.get(i);
            if ((ipor.getId().equals(id)) && (!ipor.getId().equals("One"))) ;
            return ipor;
        }
        return null;

    }

    public void processCancelRequests(String record) {
        try {
            IPOTableRecord ipot;
            TradeMessage message = new TradeMessage(record);
            String[] ipoData = message.getMessageData().split(TradeMeta.RS);
            String successID = ipoData[0];
            if (!successID.equals(-1)) {
                ipot = (IPOTableRecord) getIPOIpoTableRecord(successID);
                ipot.setStatus(2);//cancel State
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private IPOTableRecord getIPOIpoTableRecord(String id) {
        IPOTableRecord ipor;
        for (int i = 0; i < tableRecord.size(); i++) {
            ipor = (IPOTableRecord) tableRecord.get(i);
            if ((ipor.getTransactID().equals(id))) {
                return ipor;
            }
        }
        return null;
    }

}
