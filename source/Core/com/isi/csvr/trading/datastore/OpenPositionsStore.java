package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.ExpandableRecord;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trading.shared.TradeMeta;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Nov 17, 2007
 * Time: 5:27:18 PM
 */
public class OpenPositionsStore {
    private static OpenPositionsStore self;
    private List<ExpandableRecord<OpenPositionRecord>> store;
    private List<OpenPositionRecord> visibleItems;

    public OpenPositionsStore() {
        store = Collections.synchronizedList(new ArrayList<ExpandableRecord<OpenPositionRecord>>());
        visibleItems = Collections.synchronizedList(new ArrayList<OpenPositionRecord>());
    }

    public static synchronized OpenPositionsStore getSharedInstance() {
        if (self == null) {
            self = new OpenPositionsStore();
        }
        return self;
    }

    public synchronized void reconstructVisibleItems() {
        visibleItems.clear();
        for (ExpandableRecord<OpenPositionRecord> record : store) {
            if (record.isCollapsed()) {
                visibleItems.add(record.getBaseRecord());
            } else { // expanded
                Iterator<OpenPositionRecord> items = record.getRecords();
                visibleItems.add(record.getBaseRecord());
                while (items.hasNext()) {
                    visibleItems.add(items.next());
                }
            }
        }
        Collections.sort(visibleItems, new Sorter());
    }

    public void addRecord(OpenPositionRecord newRecord) {
        boolean blockFound = false;
        boolean locationFound = false;
        String key = SharedMethods.getKey(newRecord.getExchange(), newRecord.getSymbol(), newRecord.getInstrument());
        for (ExpandableRecord<OpenPositionRecord> block : store) {
            if (block.getId().equals(key)) { // found the block
                blockFound = true;
                Iterator<OpenPositionRecord> records = block.getRecords();
                while (records.hasNext()) {
                    OpenPositionRecord record = records.next();
                    if (record.getOrderID().equals(newRecord.getOrderID())) {
                        record.setOpenQuantity(newRecord.getOpenQuantity());
                        record.setClosedQuantity(newRecord.getClosedQuantity());
                        record.setDate(newRecord.getDate());
                        record.setOpenPrice(newRecord.getOpenPrice());
                        record.setStatus(newRecord.getStatus());
                        locationFound = true;
                    }
                }
                if (!locationFound) {
                    block.addRecord(newRecord);
                }
                break;
            }
        }

        if (!blockFound) { // location not found. this may be a new symbol not in the list. create a new block
            OpenPositionRecord baseRecord = new OpenPositionRecord(true, newRecord.getExchange(), newRecord.getSymbol(), "", "", newRecord.getInstrument());
            ExpandableRecord<OpenPositionRecord> block = new ExpandableRecord<OpenPositionRecord>(key, baseRecord);
            block.setExpanded();
            block.addRecord(newRecord);
            store.add(block);
        }
        updateBaserecord(newRecord.getKey());
        reconstructVisibleItems();
    }

    private void updateBaserecord(String id) {
        for (ExpandableRecord<OpenPositionRecord> block : store) {
            if (block.getId().equals(id)) { // found the block
                OpenPositionRecord baseRecord = block.getBaseRecord();
                baseRecord.setOpenQuantity(0);
                baseRecord.setClosedQuantity(0);
                Iterator<OpenPositionRecord> records = block.getRecords();
                int totalBuy = 0;
                int totalSell = 0;
                while (records.hasNext()) {
                    OpenPositionRecord record = records.next();
                    if (record.getSide() == TradeMeta.BUY) {
                        baseRecord.setOpenQuantity(baseRecord.getOpenQuantity() + (record.getOpenQuantity() - record.getClosedQuantity()));
                        totalBuy += record.getOpenQuantity();
                    } else {
                        baseRecord.setOpenQuantity(baseRecord.getOpenQuantity() - (record.getOpenQuantity() - record.getClosedQuantity()));
                        totalSell += record.getOpenQuantity();
                    }
                    baseRecord.setClosedQuantity(baseRecord.getClosedQuantity() + record.getClosedQuantity());
                }
                if (baseRecord.getOpenQuantity() == 0) {
                    baseRecord.setSide(-1);
                    baseRecord.setStatus(TradeMeta.SQUARED);
                } else {
                    baseRecord.setStatus(-1);
                    if (totalBuy > totalSell) {
                        baseRecord.setSide(TradeMeta.LONG);
                    } else if (totalBuy < totalSell) {
                        baseRecord.setSide(TradeMeta.SHORT);
                    }
                }
            }
        }
    }

    //(openbuyr-closebuy)>totalSell
    public ExpandableRecord<OpenPositionRecord> getExpandable(String id) {
        for (ExpandableRecord<OpenPositionRecord> record : store) {
            if (record.getId().equals(id)) {
                return record;
            }
        }
        return null;
    }

    public List<OpenPositionRecord> getRecords() {
        return visibleItems;
    }

    public ArrayList<OpenPositionRecord> getSecondaryRecords() {
        try {
            ArrayList<OpenPositionRecord> aList = new ArrayList<OpenPositionRecord>();
            List<OpenPositionRecord> list = getRecords();
            for (int i = 0; i < list.size(); i++) {
                OpenPositionRecord opr = list.get(i);
                if (!opr.isBaseRecord()) {
                    aList.add(opr);
                }
            }
            return aList;
        } catch (Exception e) {
            return new ArrayList<OpenPositionRecord>();
        }

    }

    public int getSize() {
        return visibleItems.size();
    }

    public void clear() {
        visibleItems.clear();
        store.clear();
    }

    class Sorter implements Comparator<OpenPositionRecord> {
        public int compare(OpenPositionRecord o1, OpenPositionRecord o2) {
            if (o1.getKey().compareTo(o2.getKey()) == 0) {
                if ((o1.getDate() - o2.getDate()) >= 1) {
                    return 1;
                } else if ((o1.getDate() - o2.getDate()) <= -1) {
                    return -1;
                } else return 0;
            } else {
                return o1.getKey().compareTo(o2.getKey());
            }
        }
    }
}
