package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Settings;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 1:44:46 PM
 */
public class ProgramedOrderStore implements Serializable {
    public static ProgramedOrderStore self = null;
    private DynamicArray store;

    private ProgramedOrderStore() {
        try {
            loadData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized ProgramedOrderStore getSharedInstance() {
        if (self == null) {
            self = new ProgramedOrderStore();
        }
        return self;
    }

    public void addTransaction(ProgrammedTrade transaction) {
        store.add(transaction);
    }

    public ProgrammedTrade getTransaction(int index) {
        return (ProgrammedTrade) store.get(index);
    }

    public ProgrammedTrade searhTransaction(String id) {
        ProgrammedTrade transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (ProgrammedTrade) store.get(i);
            if (transaction.getMubasherOrderNumber().equals(id))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public void removeTransaction(long id) {
        ProgrammedTrade transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (ProgrammedTrade) store.get(i);
            if ((transaction.getTransactionID() != null) && (transaction.getTransactionID().equals(id)))
                store.remove(i);
            transaction = null;
        }
    }

    public int size() {
        return store.size();
    }

    public void saveData() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/progorders.dat");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(store);
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadData() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/progorders.dat");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            store = (DynamicArray) oObjIn.readObject();
            oIn.close();
            oIn = null;
        } catch (Exception e) {
            store = new DynamicArray();
        }

    }
}
