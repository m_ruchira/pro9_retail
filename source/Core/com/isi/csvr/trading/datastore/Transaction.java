package com.isi.csvr.trading.datastore;

import com.dfn.mtr.mix.beans.ConditionalBehaviour;
import com.dfn.mtr.mix.beans.Order;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.shared.TRSOrder;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;

import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 1:27:13 PM
 */
public class Transaction implements Serializable, Cloneable {
    public long ESISOrderNo; //
    public String portfolioNo;
    public double limitPrice; //
    public int filledQuantity; //
    public long lastQuantity; //
    public int TIFType; //
    public String expireDate = null; //
    public int discloseQuantity; //
    public String reasonCode = null; //
    public long userID = 0;
    int tPlus = 2;
    private long messageID;
    private String orderID;
    private String clOrderID;
    private String origClOrderID;
    private String transactionID;
    private String mubasherOrderNumber;
    private String exchange; //
    private String symbol;
    private String marketCode;
    private int side;
    private int securityType = 0;
    private char type;
    private double price;
    private double stopLossPrice;
    private double trailStopLossPrice;
    private int stopLossType = -1;
    private double takeProfitPrice;
    private long orderQuantity;
    private long minQuantity;
    private double lastPrice;
    private String currency;
    private long transactTime; // Bug ID <#0029> Changed to long from string
    private long orderDate;
    private double averageCost;
    private boolean isDayOrder = false;
    private boolean isMarkForDelivery = false;
    private String rejectReason;
    private int status = TradeMeta.T39_DEFAULT;
    private double avgPrice;
    private int manualRequestStatus;
    private long sequenceNumber;
    private double commission;
    private double ordValue;
    private double ordNetValue;
    private int channel;
    private String twOrderID = null;
    //option data
    private int putOrCall;                    // 201 0 = Put, 1 = Call
    private double strikePrice = 0;              // 202
    private String maturityMonthYear = null;     // 200 Maturity Month or Year - YYYYMM
    private String baseSymbol = null;            // Option Base symbol
    private double cum_commission = 0; //808
    private double cum_ordValue = 0; //809
    private double cum_ordNetValue = 0; //810
    private String settleCurrency = null; //820
    private double settleRate = 1; //821
    private double avgCost = -1;  //819
    //  private String condition;
    private ConditionalBehaviour condition;
    private ConditionalBehaviour bracketCondition;
    private String conditionStartTime;
    private String conditionExpireTime;
    private int ruleStatus;
    private int ruleType;
    private int sliceExecType = -1;         //830
    private long sliceTimeInterval = -1;    //831
    private int sliceBlockSIze = -1;        //832
    private long sliceOrderID = -1;
    private int sliceOrderStatus = -1;
    //Adding Reference ID :Requested By Ruwan, Done by Chandika
    private String refID = "";
    private String bookKeeper = "N/A";
    private Order mixOrderObject;
    private int orderCategory;
    private String sKey;

    private TWDecimalFormat valueFormatter;


    //private static SimpleDateFormat txnTimeFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS"); //20040913-08:30:41.846


    public Transaction() {
        mixOrderObject = new Order();

    }

    public void setOrderData(TRSOrder trsOrdera) {
        if (trsOrdera.getMubasherOrderNumber() != null)
            mubasherOrderNumber = trsOrdera.getMubasherOrderNumber();
        if (trsOrdera.getOrderID() != null)
            orderID = trsOrdera.getOrderID();
        if (trsOrdera.getClOrdID() != null)
            clOrderID = trsOrdera.getClOrdID();
        if (trsOrdera.getOrigClOrdID() != null)
            origClOrderID = trsOrdera.getOrigClOrdID();
        if (trsOrdera.getESISNumber() >= 0)
            ESISOrderNo = trsOrdera.getESISNumber();
        if (trsOrdera.getExecID() != null)
            transactionID = trsOrdera.getExecID();
        if (trsOrdera.getPortfolioNo() != null)
            portfolioNo = trsOrdera.getPortfolioNo();
        if (trsOrdera.getSecurityExchange() != null)
            exchange = trsOrdera.getSecurityExchange();
        if (trsOrdera.getSymbol() != null)
            symbol = trsOrdera.getSymbol();
        if (trsOrdera.getOrdStatus() != TradeMeta.T39_DEFAULT) {
            status = trsOrdera.getOrdStatus();
        }
        if (trsOrdera.getSide() >= 0)
            side = trsOrdera.getSide();   // Buy/Sell
        if (trsOrdera.getSecurityType() >= 0)
            securityType = trsOrdera.getSecurityType();   // Buy/Sell
        if (trsOrdera.getOrdType() > (char) 0)
            type = trsOrdera.getOrdType();  // Market/Limit
        if (trsOrdera.getPrice() >= 0)
            price = trsOrdera.getPrice();
        if (trsOrdera.getOrderQty() >= 0)
            orderQuantity = trsOrdera.getOrderQty();
        if (trsOrdera.getPrice() >= 0)
            limitPrice = trsOrdera.getPrice();
        if (trsOrdera.getMinQty() > 0)
            minQuantity = trsOrdera.getMinQty();
        if (trsOrdera.getCumQty() >= 0)
            filledQuantity = trsOrdera.getCumQty();
        if (trsOrdera.getLastShares() >= 0)
            lastQuantity = trsOrdera.getLastShares();
        if (trsOrdera.getLastPx() >= 0)
            lastPrice = trsOrdera.getLastPx();
        if (trsOrdera.getAvgPx() >= 0)
            avgPrice = trsOrdera.getAvgPx();
        if (trsOrdera.getTimeInForce() >= 0) {
            TIFType = trsOrdera.getTimeInForce();
        }
        if (trsOrdera.getCreatedDate() != null)
            try {
                orderDate = TradingShared.formatFIXTimeToLong(trsOrdera.getCreatedDate());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
//            orderDate = TradingShared.formatFIXTimeToTW(trsOrdera.getCreatedDate());
        if (trsOrdera.getMaxFloor() >= 0)
            discloseQuantity = trsOrdera.getMaxFloor();
        if (trsOrdera.getOrdRejReason() >= 0)
            reasonCode = "" + trsOrdera.getOrdRejReason();
        if (trsOrdera.getCommission() >= 0)
            commission = trsOrdera.getCommission();
        if (trsOrdera.getChannel() >= 0)
            channel = trsOrdera.getChannel();
        if (trsOrdera.getExpireTime() != null)
            try {
                expireDate = TradingShared.formatGoodTill(trsOrdera.getExpireTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (trsOrdera.getTransactTime() != null)
            try {
                transactTime = TradingShared.formatFIXTimeToLong(trsOrdera.getTransactTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (trsOrdera.getOrdValue() >= 0)
            ordValue = trsOrdera.getOrdValue();
        if (trsOrdera.getOrdNetValue() >= 0)
            ordNetValue = trsOrdera.getOrdNetValue();
        if (trsOrdera.getCum_commission() > 0)
            cum_commission = trsOrdera.getCum_commission();
        if (trsOrdera.getCum_ordValue() >= 0)
            cum_ordValue = trsOrdera.getCum_ordValue();
        if (trsOrdera.getCum_ordNetValue() >= 0)
            cum_ordNetValue = trsOrdera.getCum_ordNetValue();
        if (trsOrdera.getSequenceNumber() > 0)
            sequenceNumber = trsOrdera.getSequenceNumber();
        if (trsOrdera.getText() != null)
            rejectReason = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(trsOrdera.getText()));
        if (trsOrdera.getTwOrderID() != null)
            twOrderID = trsOrdera.getTwOrderID();
        if (trsOrdera.getCurrency() != null)
            currency = trsOrdera.getCurrency();
        if (trsOrdera.getStrikePrice() >= 0)
            strikePrice = trsOrdera.getStrikePrice();
        if (trsOrdera.getPutOrCall() > 0)
            putOrCall = trsOrdera.getPutOrCall();
        if (trsOrdera.getMaturityMonthYear() != null)
            maturityMonthYear = trsOrdera.getMaturityMonthYear();
        if (trsOrdera.getStopLossPrice() > 0)
            stopLossPrice = trsOrdera.getStopLossPrice();
        if (trsOrdera.getTrailStopLossPrice() > 0)
            trailStopLossPrice = trsOrdera.getTrailStopLossPrice();
        if (trsOrdera.getTakeProfit() > 0)
            takeProfitPrice = trsOrdera.getTakeProfit();
        if (trsOrdera.getBaseSymbol() != null)
            baseSymbol = trsOrdera.getBaseSymbol();
        if (trsOrdera.getAvgCost() > 0)
            averageCost = trsOrdera.getAvgCost();
        if (trsOrdera.getSettleCurrency() != null)
            settleCurrency = trsOrdera.getSettleCurrency();
        if (trsOrdera.getSettleRate() > 0)
            settleRate = trsOrdera.getSettleRate();
        if (trsOrdera.getCondition() != null)
            //   condition = trsOrdera.getCondition();
            if (trsOrdera.getConditionStartTime() != null)
                conditionStartTime = trsOrdera.getConditionStartTime();
        if (trsOrdera.getConditionExpireTime() != null)
            conditionExpireTime = trsOrdera.getConditionExpireTime();
        if (trsOrdera.getConditionStatus() != -1)
            ruleStatus = trsOrdera.getConditionStatus();
        if (trsOrdera.getConditionType() != -1)
            ruleType = trsOrdera.getConditionType();
        if (trsOrdera.getSliceBlockSIze() != -1)
            sliceBlockSIze = trsOrdera.getSliceBlockSIze();
        if (trsOrdera.getSliceOrderID() != -1)
            sliceOrderID = trsOrdera.getSliceOrderID();
        if (trsOrdera.getSliceExecType() != -1)
            sliceExecType = trsOrdera.getSliceExecType();
        if (trsOrdera.getSliceOrderStatus() != -1)
            sliceOrderStatus = trsOrdera.getSliceOrderStatus();
        if (trsOrdera.getSliceTimeInterval() != -1)
            sliceTimeInterval = trsOrdera.getSliceTimeInterval();
        if (trsOrdera.getStopLossType() != -1)
            stopLossType = trsOrdera.getStopLossType();
        isDayOrder = trsOrdera.isDayOrder();
        isMarkForDelivery = trsOrdera.isMarkedForDelivery();
        refID = trsOrdera.getRefID();
        bookKeeper = trsOrdera.getBookKeeper();
        tPlus = trsOrdera.getTPlus();
    }

    public Order getMixOrderObject() {
        return mixOrderObject;
    }

    public void setMixOrderObject(Order mixOrderObject) {
        this.mixOrderObject = mixOrderObject;
        if (mixOrderObject.getMubasherOrderNumber() > 0)
            mubasherOrderNumber = mixOrderObject.getMubasherOrderNumber() + "";   //todo check for muborderNo
        if (mixOrderObject.getOrderID() != null)
            orderID = mixOrderObject.getOrderID();
        if (mixOrderObject.getClOrdID() != null)
            clOrderID = mixOrderObject.getClOrdID();        //todo var mismatch
        if (mixOrderObject.getOrigClOrdID() != null)
            origClOrderID = mixOrderObject.getOrigClOrdID();  //todo var mismatch
        if (mixOrderObject.getESISOrdNumber() >= 0)
            ESISOrderNo = mixOrderObject.getESISOrdNumber();
        if (mixOrderObject.getExecutionID() != null)
            transactionID = mixOrderObject.getExecutionID();
        if (mixOrderObject.getPortfolioNo() != null)
            portfolioNo = mixOrderObject.getPortfolioNo();
        if (mixOrderObject.getSecurityExchange() != null)
            exchange = mixOrderObject.getSecurityExchange();
        if (mixOrderObject.getSymbol() != null)
            symbol = mixOrderObject.getSymbol();
        if (mixOrderObject.getMarketCode() != null)
            marketCode = mixOrderObject.getMarketCode();
        if (mixOrderObject.getOrdStatus() != TradeMeta.T39_DEFAULT) {
            status = mixOrderObject.getOrdStatus();
        }
        if (mixOrderObject.getSide() >= 0)
            side = mixOrderObject.getSide();   // Buy/Sell
        //  if (mixOrderObject.getSecurityType() != null)
        if (mixOrderObject.getMubasherSecurityType() > -1)
//            securityType = Integer.parseInt(mixOrderObject.getSecurityType()); // Buy/Sell      //todo instrument type change
            securityType = (mixOrderObject.getMubasherSecurityType()); // Buy/Sell      //todo instrument type change
        if (mixOrderObject.getOrdType() > (char) 0)
            type = mixOrderObject.getOrdType();  // Market/Limit
        if (mixOrderObject.getPrice() >= 0)
            price = mixOrderObject.getPrice();
        if (mixOrderObject.getOrderQty() >= 0)
            orderQuantity = mixOrderObject.getOrderQty();
        if (mixOrderObject.getPrice() >= 0)
            limitPrice = mixOrderObject.getPrice();
        if (mixOrderObject.getMinimumQty() > 0)
            minQuantity = mixOrderObject.getMinimumQty();
        if (mixOrderObject.getCumulativeQty() >= 0)
            filledQuantity = (int) mixOrderObject.getCumulativeQty();   //todo var mismatch
        if (mixOrderObject.getLastShares() >= 0)
            lastQuantity = mixOrderObject.getLastShares();
        if (mixOrderObject.getLastPrice() >= 0)
            lastPrice = mixOrderObject.getLastPrice();
        if (mixOrderObject.getAveragePrice() >= 0)
            avgPrice = mixOrderObject.getAveragePrice();
        if (mixOrderObject.getTimeInForce() >= 0) {
            TIFType = mixOrderObject.getTimeInForce();
        }
        if (mixOrderObject.getCreatedDate() != null)
            try {
                orderDate = TradingShared.formatFIXTimeToLong(mixOrderObject.getCreatedDate());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
//            orderDate = TradingShared.formatFIXTimeToTW(trsOrdera.getCreatedDate());
        if (mixOrderObject.getMaxFloor() > 0) // todo : remove equelity
            discloseQuantity = (int) mixOrderObject.getMaxFloor();  //todo var mismatch
        if (mixOrderObject.getOrdRejectReason() >= 0)
            reasonCode = "" + mixOrderObject.getOrdRejectReason();
        if (mixOrderObject.getCommission() >= 0)
            commission = mixOrderObject.getCommission();
        if (mixOrderObject.getChannel() >= 0)
            channel = mixOrderObject.getChannel();
        if (mixOrderObject.getExpireTime() != null)
            try {
                expireDate = TradingShared.formatGoodTill(mixOrderObject.getExpireTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (mixOrderObject.getTransactionTime() != null)
            try {
                transactTime = TradingShared.formatFIXTimeToLong(mixOrderObject.getTransactionTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (mixOrderObject.getOrdValue() >= 0)
            ordValue = mixOrderObject.getOrdValue();
        if (mixOrderObject.getOrdValue() >= 0)
            ordNetValue = mixOrderObject.getOrderNetValue();    //todo check for correct mapping  -missing
        if (mixOrderObject.getCommission() > 0)
            cum_commission = mixOrderObject.getCommission();  //todo check for correct mapping -missing
        if (mixOrderObject.getCumOrderValue() >= 0)
            cum_ordValue = mixOrderObject.getCumOrderValue();   //todo check for correct mapping -missing
        if (mixOrderObject.getCumOrderNetValue() >= 0)
            cum_ordNetValue = mixOrderObject.getCumOrderNetValue();  //todo check for correct mapping -missing
        if (mixOrderObject.getSequenceNumber() > 0)
            sequenceNumber = mixOrderObject.getSequenceNumber();
        if (mixOrderObject.getText() != null)
            rejectReason = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(mixOrderObject.getText()));
        if (mixOrderObject.getTwOrderID() != null)
            twOrderID = mixOrderObject.getTwOrderID();
        if (mixOrderObject.getCurrency() != null)
            currency = mixOrderObject.getCurrency();
        if (mixOrderObject.getStrikePrice() >= 0)
            strikePrice = mixOrderObject.getStrikePrice();
        if (mixOrderObject.getPutOrCall() > 0)
            putOrCall = mixOrderObject.getPutOrCall();
        if (mixOrderObject.getMaturityMonthYear() != null)
            maturityMonthYear = mixOrderObject.getMaturityMonthYear();
        if (mixOrderObject.getStopPrice() > 0)
            stopLossPrice = mixOrderObject.getStopPrice();
        if (mixOrderObject.getStopLossTrailPrice() > 0)
            trailStopLossPrice = mixOrderObject.getStopLossTrailPrice();   //todo check for correct mapping -missing
        if (mixOrderObject.getTakeProfit() > 0)
            takeProfitPrice = mixOrderObject.getTakeProfit();   //todo check for correct mapping -missing
//        if (mixOrderObject.getBaseSymbol() != null)
//            baseSymbol = trsOrdera.getBaseSymbol();   //todo check for correct mapping -missing
        if (mixOrderObject.getOrderAvgCost() > 0)
            averageCost = mixOrderObject.getOrderAvgCost();   //todo check for correct mapping -missing
        if (mixOrderObject.getSettleCurrency() != null)
            settleCurrency = mixOrderObject.getSettleCurrency();
//        if (mixOrderObject.getSettleRate() > 0)
//            settleRate = trsOrdera.getSettleRate();  //todo check for correct mapping -missing
        if (getCondtionalBehaviourObject(MIXConstants.CONDITION_CATEGORY_PRE_CONDITION, MIXConstants.CONDITION_TYPE_NORMAL) != null) {
            condition = getCondtionalBehaviourObject(MIXConstants.CONDITION_CATEGORY_PRE_CONDITION, MIXConstants.CONDITION_TYPE_NORMAL);
            conditionStartTime = condition.getConditionStartTime();
            conditionExpireTime = condition.getExpireTime();
            ruleStatus = condition.getConditionStatus();
            ruleType = condition.getConditionType();
        }
//        if (mixOrderObject.getBehaviourList() != null && mixOrderObject.getBehaviourList().size()>0)
//            conditionStartTime = mixOrderObject.getBehaviourList().get(0).getConditionStartTime();
//        if (mixOrderObject.getBehaviourList() != null && mixOrderObject.getBehaviourList().size()>0)
//            conditionExpireTime = mixOrderObject.getBehaviourList().get(0).getExpireTime();   //todo check for correct mapping -missing
//        if (mixOrderObject.getBehaviourList() != null && mixOrderObject.getBehaviourList().size()>0)
//
//                ruleStatus =(mixOrderObject.getBehaviourList().get(0).getConditionStatus());
//
//        if (mixOrderObject.getBehaviourList() != null && mixOrderObject.getBehaviourList().size()>0)
//            ruleType = mixOrderObject.getBehaviourList().get(0).getConditionType();

        if (mixOrderObject.getAlgoCondition() != null && mixOrderObject.getAlgoCondition().getSliceOrderBlock() != null) {
            try {
                sliceBlockSIze = Integer.parseInt(mixOrderObject.getAlgoCondition().getSliceOrderBlock()); //todo check for correct mapping -missing
            } catch (NumberFormatException e) {
                sliceBlockSIze = -1;
            }
        }
        if (mixOrderObject.getAlgoCondition() != null && mixOrderObject.getAlgoCondition().getSliceOrderID() != null)
            try {
                sliceOrderID = Long.parseLong(mixOrderObject.getAlgoCondition().getSliceOrderID());   //todo check for correct mapping -missing
            } catch (NumberFormatException e) {
                sliceOrderID = -1;
            }
        if (mixOrderObject.getAlgoCondition() != null && mixOrderObject.getAlgoCondition().getAlgoType() > -1)
            try {
                sliceExecType = mixOrderObject.getAlgoCondition().getAlgoType();   //todo check for correct mapping -missing
            } catch (NumberFormatException e) {
                sliceExecType = -1;
            }
        if (mixOrderObject.getAlgoCondition() != null && mixOrderObject.getAlgoCondition().getSliceOrderStatus() != null) {
            try {
                sliceOrderStatus = Integer.parseInt(mixOrderObject.getAlgoCondition().getSliceOrderStatus());//todo check for correct mapping -missing
            } catch (NumberFormatException e) {
                sliceOrderStatus = -1;
            }
        }
        if (mixOrderObject.getAlgoCondition() != null && mixOrderObject.getAlgoCondition().getSliceOrderInterval() != null) {
            try {
                sliceTimeInterval = Long.parseLong(mixOrderObject.getAlgoCondition().getSliceOrderInterval()); //todo check for correct mapping -missing
            } catch (Exception e) {
                sliceTimeInterval = -1;
            }
        }
        if (mixOrderObject.getStopPxType() != -1) {
            stopLossType = mixOrderObject.getStopPxType();     //todo check for correct mapping -missing
        }
        isDayOrder = mixOrderObject.isDayOrder() == 1;
//        isMarkForDelivery = mixOrderObject.isMarkedForDelivery();    //todo check for correct mapping -missing
        //      refID=mixOrderObject.getRefID();    //todo check for correct mapping -missing
        bookKeeper = mixOrderObject.getBookKeeperID() + "";
        bracketCondition = getCondtionalBehaviourObject(MIXConstants.CONDITION_CATEGORY_POST_CONDITION, MIXConstants.CONDITION_TYPE_BRACKET);
        orderCategory = mixOrderObject.getOrderCategory();
    }

    public ConditionalBehaviour getCondtionalBehaviourObject(int condtionCatagory, int condtionType) {
        if (mixOrderObject.getBehaviourList() != null && mixOrderObject.getBehaviourList().size() > 0) {
            List<ConditionalBehaviour> list = mixOrderObject.getBehaviourList();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getConditionCategory() == condtionCatagory && list.get(i).getConditionType() == condtionType) {
                    return list.get(i);
                }
            }
            return null;
        } else {
            return null;
        }
    }


    public long getMessageID() {
        return messageID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public int getDiscloseQuantity() {
        return discloseQuantity;
    }

    public long getESISOrderNo() {
        return ESISOrderNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public double getAvgPrice() {
        return avgPrice;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public long getTransactTime() {
        return transactTime;
    }

    public int getFilledQuantity() {
        return filledQuantity;
    }

    public double getLimitPrice() {
        return limitPrice;
    }

    public void setLimitPrice(double limitPrice) {
        this.limitPrice = limitPrice;
    }

    public int getManualRequestStatus() {
        return manualRequestStatus;
    }

    public long getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(long orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getPortfolioNo() {
        return portfolioNo;
    }

    public void setPortfolioNo(String portfolioNo) {
        this.portfolioNo = portfolioNo;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public int getStatus() {
        return status;
    }

    public int getTIFType() {
        return TIFType;
    }

//    public void setPrice(double price) {
//        this.price = price;
//    }

    public void setTIFType(int TIFType) {
        this.TIFType = TIFType;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public long getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(long minQuantity) {
        this.minQuantity = minQuantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public long getLastQuantity() {
        return lastQuantity;
    }

    public void setQuantity(long quantity) {
        this.lastQuantity = quantity;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbolWithMarketCode() {
        if (marketCode != null)
            return symbol + Constants.MARKET_SEPERATOR_CHARACTER + marketCode;
        else
            return symbol;
    }

    public int getSecurityType() {
        return securityType;
    }

    public long getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(long orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public int getChannel() {
        return channel;
    }

    public double getCommission() {
        return commission;
    }

    public String getClOrderID() {
        return clOrderID;
    }

    public String getOrigClOrderID() {
        return origClOrderID;
    }

    public String getMubasherOrderNumber() {
        return mubasherOrderNumber;
    }

    public double getOrdValue() {
        return ordValue;
    }

    public double getOrdNetValue() {
        return ordNetValue;
    }

    public double getCum_commission() {
        return cum_commission;
    }

    public double getCum_ordValue() {
        return cum_ordValue;
    }

    public double getCum_ordNetValue() {
        return cum_ordNetValue;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public String getTwOrderID() {
        return twOrderID;
    }

    public void setTwOrderID(String twOrderID) {
        this.twOrderID = twOrderID;
    }

    public String getMaturityMonthYear() {
        return maturityMonthYear;
    }

    public int getPutOrCall() {
        return putOrCall;
    }

    public double getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public double getStopLossPrice() {
        return stopLossPrice;
    }

    public double getTrailStopLossPrice() {
        return trailStopLossPrice;
    }

    public double getTakeProfitPrice() {
        return takeProfitPrice;
    }

    public String getBaseSymbol() {
        return baseSymbol;
    }

    public long getPendingQuantity() {

        if (status == TradeMeta.T39_Canceled) {
            return 0;
        } else if (status == TradeMeta.T39_Rejected) {
            return 0;
        } else if (status == TradeMeta.T39_Suspended) {
            return 0;
        } else if (status == TradeMeta.T39_Expired) {
            return 0;
        } else {
            return orderQuantity - filledQuantity;
        }
    }

    public double getAverageCost() {
        return averageCost;
    }

    public String getSettleCurrency() {
        return settleCurrency;
    }

    public double getSettleRate() {
        return settleRate;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public ConditionalBehaviour getRule() {
        return condition;
    }

    public void setRule(ConditionalBehaviour condition) {
        this.condition = condition;
    }

    public String getRuleExpireTime() {
        return conditionExpireTime;
    }

    public void setRuleExpireTime(String conditionExpireTime) {
        this.conditionExpireTime = conditionExpireTime;
    }

    public String getRuleStartTime() {
        return conditionStartTime;
    }

    public void setRuleStartTime(String conditionStartTime) {
        this.conditionStartTime = conditionStartTime;
    }

    public int getRuleStatus() {
        return ruleStatus;
    }

    public void setRuleStatus(int ruleStatus) {
        this.ruleStatus = ruleStatus;
    }

    public int getRuleType() {
        return ruleType;
    }

    public int getSliceExecType() {
        return sliceExecType;
    }

    public void setSliceExecType(int sliceExecType) {
        this.sliceExecType = sliceExecType;
    }

    public long getSliceTimeInterval() {
        return sliceTimeInterval;
    }

    public void setSliceTimeInterval(long sliceTimeInterval) {
        this.sliceTimeInterval = sliceTimeInterval;
    }

    public int getSliceBlockSIze() {
        return sliceBlockSIze;
    }

    public void setSliceBlockSIze(int sliceBlockSIze) {
        this.sliceBlockSIze = sliceBlockSIze;
    }

    public long getSliceOrderID() {
        return sliceOrderID;
    }

    public void setSliceOrderID(long sliceOrderID) {
        this.sliceOrderID = sliceOrderID;
    }

    public int getSliceOrderStatus() {
        return sliceOrderStatus;
    }

    public void setSliceOrderStatus(int sliceOrderStatus) {
        this.sliceOrderStatus = sliceOrderStatus;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


    public String toString() {
        return "Transaction{" +
                "orderID=" + orderID +
                ", mubasherOrderNumber='" + mubasherOrderNumber + "'" +
                ", price=" + price +
                "}";
    }

    public String getXML() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<Order>");
        buffer.append("<OrderID value=\"").append(getMubasherOrderNumber()).append("\"/>");
//        buffer.append("<ExchangeOrderID value=\"").append(getClOrderID()).append("\"/>");
        buffer.append("<Portfolio value=\"").append(getPortfolioNo()).append("\"/>");
        buffer.append("<Exchange value=\"").append(getExchange()).append("\"/>");
        buffer.append("<Symbol value=\"").append(getSymbol()).append("\"/>");
        buffer.append("<InstrumentType value=\"").append(getSecurityType()).append("\"/>");
        buffer.append("<Side value=\"").append(getSide() == TradeMeta.BUY ? "B" : getSide() == TradeMeta.SELL ? "S" : getSide()).append("\"/>");
        buffer.append("<Type value=\"").append(getType() == TradeMeta.ORDER_TYPE_LIMIT ? "L" : getType() == TradeMeta.ORDER_TYPE_MARKET ? "M" : getType()).append("\"/>");
        buffer.append("<Price value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getPrice())).append("\"/>");
        buffer.append("<Quantity value=\"").append(getOrderQuantity()).append("\"/>");
        buffer.append("<AverageCost value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getAverageCost())).append("\"/>");
        buffer.append("<FilledQuantity value=\"").append(getFilledQuantity()).append("\"/>");
        buffer.append("<LastPrice value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getLastPrice())).append("\"/>");
        buffer.append("<LastQuantity value=\"").append(getLastQuantity()).append("\"/>");
        buffer.append("<Status value=\"").append((char) getStatus()).append("\"/>");
        buffer.append("<Commission value=\"").append(getCommission()).append("\"/>");
        buffer.append("<UserOrderID value=\"").append(getTwOrderID()).append("\"/>");
        buffer.append("<Currency value=\"").append(getCurrency()).append("\"/>");
        buffer.append("<ExpiryDate value=\"").append(TradingShared.formatTWToyyyyMMdd(getExpireDate())).append("\"/>");
        buffer.append("<OrderDate value=\"").append(TradingShared.formatLongToFIXTime(getOrderDate())).append("\"/>");
        buffer.append("<RejectReson value=\"").append(getRejectReason() == null ? "" : getRejectReason()).append("\"/>");
        buffer.append("</Order>\n");

        return buffer.toString();
    }


    public int getStopLossType() {
        return stopLossType;
    }

    public void setStopLossType(int stopLossType) {
        this.stopLossType = stopLossType;
    }

    public boolean isDayOrder() {
        return isDayOrder;
    }

    public void setDayOrder(boolean dayOrder) {
        isDayOrder = dayOrder;
    }

    public boolean isMarkForDelivery() {
        return isMarkForDelivery;
    }

    public void setMarkForDelivery(boolean markForDelivery) {
        isMarkForDelivery = markForDelivery;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getBookKeeper() {
        return bookKeeper;
    }

    public void setBookKeeper(String bookKeeper) {
        this.bookKeeper = bookKeeper;
    }

    public int getTPlus() {
        return tPlus;
    }

    public void setTPlus(int tPlus) {
        this.tPlus = tPlus;
    }

    public ConditionalBehaviour getBracketCondition() {
        return bracketCondition;
    }

    public void setBracketCondition(ConditionalBehaviour bracketCondition) {
        this.bracketCondition = bracketCondition;
    }

    public int getOrderCategory() {
        return orderCategory;
    }

    public String getSKey() {
        if (sKey == null) {
            sKey = SharedMethods.getKey(exchange, symbol, securityType);
        }
        return sKey;
    }

    public String getSKeyWithMarketCode() {
        if (sKey == null) {
            sKey = SharedMethods.getKey(exchange, this.getSymbolWithMarketCode(), securityType);
        }
        return sKey;
    }
}

