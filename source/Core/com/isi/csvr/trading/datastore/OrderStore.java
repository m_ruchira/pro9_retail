package com.isi.csvr.trading.datastore;

import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.shared.OrderStatusListener;
import com.isi.csvr.trading.shared.TradeMeta;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ListIterator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 1:44:46 PM
 */
public class OrderStore implements Serializable {
    public static OrderStore self = null;
    public ArrayList<OrderStatusListener> listeners;
    private ArrayList<Transaction> store;
    private ArrayList<Transaction> filteredStore;
    private String filterSide = "*";
    private String filterStatus = "*";
    private String filterExchange = "*";
    private String filterPortfolio = "*";
    private TransactionComparator comparator;

    private OrderStore() {
        try {
//            loadData();
            comparator = new TransactionComparator();
            store = new ArrayList<Transaction>();
            filteredStore = new ArrayList<Transaction>();
            listeners = new ArrayList<OrderStatusListener>();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized OrderStore getSharedInstance() {
        if (self == null) {
            self = new OrderStore();
        }
        return self;
    }

    public void addOrderStatusListener(OrderStatusListener listener) {
        listeners.add(listener);
    }

    public void removeOrderStatusListener(OrderStatusListener listener) {
        listeners.remove(listener);
    }

    public void fireOrderStatusChanged(String mubasherOrderNo) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                listeners.get(i).orderStatusChanged(mubasherOrderNo);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void fireNewOrderAdded(String mubasherOrderNo) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
//                listeners.get(i).newOrderAdded(mubasherOrderNo);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void addTransaction(Transaction transaction) {
        store.add(transaction);
        applyFilter();
    }

    public Transaction getFilteredTransaction(int index) {
        return filteredStore.get(index);
    }

    public Transaction getTransaction(int index) {
        return store.get(index);
    }

    public ListIterator<Transaction> getTransactions() {
        return store.listIterator();
    }

    public Transaction searhTransactionByMubasherOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if ((transaction.getMubasherOrderNumber() != null) && (transaction.getMubasherOrderNumber().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public Transaction searhTransactionByTWOrderID(String id, boolean isSending) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if ((transaction.getTwOrderID() != null) && (transaction.getTwOrderID().equals(id))) {
                if (!isSending) {
                    return transaction;
                } else {
                    if (transaction.getStatus() == TradeMeta.T39_SENDING) {
                        return transaction;
                    }

                }
            }
            transaction = null;
        }
        return null;
    }


    public void removeTransactionByMubasherOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if ((transaction.getMubasherOrderNumber() != null) && (transaction.getMubasherOrderNumber().equals(id)))
                store.remove(i);
            transaction = null;
        }
        applyFilter();
    }

    public void replaceTransactionByMubasherOrderID(Transaction newTransaction) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if ((transaction.getMubasherOrderNumber() != null) &&
                    (transaction.getMubasherOrderNumber().equals(newTransaction.getMubasherOrderNumber()))) {
                //if (newTransaction.getSequenceNumber() > transaction.getSequenceNumber()){
                store.set(i, newTransaction);
                break;
                //}
            }
            transaction = null;
        }
    }

    public Transaction searhTransactionByOrigClOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if ((transaction.getOrigClOrderID() != null) && (transaction.getOrigClOrderID().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public Transaction searhTransactionByClOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if ((transaction.getClOrderID() != null) && (transaction.getClOrderID().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public void removeTransactionByMessageID(long id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if (transaction.getMessageID() == id)
                store.remove(i);
            transaction = null;
        }
        applyFilter();
    }

    public void removeTransactionByClOrdID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if ((transaction.getClOrderID() != null) && (transaction.getClOrderID().equals(id))) {
                store.remove(i);
                System.out.println("Txn Removed ++++++++++++++++++++  clord= " + id);
            }

            transaction = null;
        }
        applyFilter();
    }

    public void replaceTransactionByMessageID(Transaction newTransaction) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            if (transaction.getMessageID() == newTransaction.getMessageID()) {
                store.set(i, newTransaction);
                break;
            }
            transaction = null;
        }
    }

    /*public boolean isShowOnlyOpenOrders() {
        return showOnlyOpenOrders;
    }

    public void setShowOnlyOpenOrders(boolean showOnlyOpenOrders) {
        this.showOnlyOpenOrders = showOnlyOpenOrders;
        applyFilter();
    }*/

    public void setFilter(String side, String status, String exchange, String portfolio) {
        filterSide = side;
        filterStatus = status;
        filterExchange = exchange;
        filterPortfolio = portfolio;
        applyFilter();
    }

    public void applyFilter() {
        boolean sideOK = false;
        boolean statusOK = false;
        boolean exchangeOK = false;
        boolean portfolioOK = false;

        Transaction transaction;
        filteredStore.clear();
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            sideOK = false;
            statusOK = false;
            exchangeOK = false;
            portfolioOK = false;

            if ((filterSide.equals("*")) || (filterSide.equals("" + transaction.getSide()))) {
                sideOK = true;
            }

            if ((filterPortfolio.equals("*")) || (filterPortfolio.equals(transaction.getPortfolioNo()))) {
                portfolioOK = true;
            }

            String[] statuses = filterStatus.trim().split(",");
            if (filterStatus.equals("*")) {
                statusOK = true;
            } else if ((filterStatus.equals("OPEN")) &&
                    ((transaction.getStatus() != TradeMeta.T39_Filled) && (transaction.getStatus() != TradeMeta.T39_Expired)
                            && (transaction.getStatus() != TradeMeta.T39_Done_For_Day) && (transaction.getStatus() != TradeMeta.T39_NEW_DELETED)
                            && (transaction.getStatus() != TradeMeta.T39_Executed) && (transaction.getStatus() != TradeMeta.T39_APPROVAL_REJECTED)
                            && (transaction.getStatus() != TradeMeta.T39_INVALIDATED_BY_CHANAGE) && (transaction.getStatus() != TradeMeta.T39_CANELED_BY_EXCHANGE)
                            && (transaction.getStatus() != TradeMeta.T39_INVALIDATED_BY_CANCEL) && (transaction.getStatus() != TradeMeta.T39_PROCESSED)
                            && (transaction.getStatus() != TradeMeta.T39_PF_EXPIRED) && (transaction.getStatus() != TradeMeta.T39_PF_CANCELLED)
                            && (transaction.getStatus() != TradeMeta.T39_COMPLETED) && (transaction.getStatus() != TradeMeta.T39_Completed)
                            && (transaction.getStatus() != TradeMeta.T39_Canceled) && (transaction.getStatus() != TradeMeta.T39_Rejected))) {
                statusOK = true;
            } else if (statuses != null) {
                for (String status : statuses) {
                    if (status.charAt(0) == transaction.getStatus()) {
                        statusOK = true;
                        break;
                    }
                }
            }

            if ((filterExchange.equals("*")) || (filterExchange.equals(transaction.getExchange()))) {
                exchangeOK = true;
            }

            /*if ((transaction.getStatus() == TradeMeta.T39_Filled) || (transaction.getStatus() == TradeMeta.T39_Expired)
                || (transaction.getStatus() == TradeMeta.T39_Canceled) || (transaction.getStatus() == TradeMeta.T39_Rejected)){
                if (!isShowOnlyOpenOrders()){
                    filteredStore.add(0,transaction);
                }
            }else{
                filteredStore.add(0,transaction);
            }*/
            if (sideOK && statusOK && exchangeOK && portfolioOK) {
                filteredStore.add(0, transaction);
            }
            transaction = null;
        }
        Collections.sort(filteredStore, comparator);
        TradeMethods.getSharedInstance().sortBlotter();
    }

    public ArrayList<Transaction> getFilteredList(String side, String status, String exchange, String portfolio) {
        boolean sideOK = false;
        boolean statusOK = false;
        boolean exchangeOK = false;
        boolean portfolioOK = false;
        ArrayList<Transaction> filteredList = new ArrayList<Transaction>();
        Transaction transaction;
        //  filteredStore.clear();
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            sideOK = false;
            statusOK = false;
            exchangeOK = false;
            portfolioOK = false;

            if ((side.equals("*")) || (side.equals("" + transaction.getSide()))) {
                sideOK = true;
            }

            if ((portfolio.equals("*")) || (portfolio.equals(transaction.getPortfolioNo()))) {
                portfolioOK = true;
            }

            if (status.equals("*")) {
                statusOK = true;
            } else if ((status.equals("OPEN")) &&
                    ((transaction.getStatus() != TradeMeta.T39_Filled) && (transaction.getStatus() != TradeMeta.T39_Expired)
                            && (transaction.getStatus() != TradeMeta.T39_Canceled) && (transaction.getStatus() != TradeMeta.T39_Rejected))
                    && (transaction.getStatus() != TradeMeta.T39_WAITING_FOR_APPROVAL) && (transaction.getStatus() != TradeMeta.T39_SENDING)
                    && (transaction.getStatus() != TradeMeta.T39_APPROVAL_VALIDATION_REJECTED) && (transaction.getStatus() != TradeMeta.T39_APPROVAL_REJECTED)) {
                statusOK = true;
            } else if (status.toCharArray()[0] == transaction.getStatus()) {
                statusOK = true;
            }

            if ((exchange.equals("*")) || (exchange.equals(transaction.getExchange()))) {
                exchangeOK = true;
            }

            /*if ((transaction.getStatus() == TradeMeta.T39_Filled) || (transaction.getStatus() == TradeMeta.T39_Expired)
                || (transaction.getStatus() == TradeMeta.T39_Canceled) || (transaction.getStatus() == TradeMeta.T39_Rejected)){
                if (!isShowOnlyOpenOrders()){
                    filteredStore.add(0,transaction);
                }
            }else{
                filteredStore.add(0,transaction);
            }*/
            if (sideOK && statusOK && exchangeOK && portfolioOK) {
                filteredList.add(0, transaction);
            }
            transaction = null;
        }
        //Collections.sort(filteredStore, comparator);
        return filteredList;
    }

    public int filteredSize() {
        return filteredStore.size();
    }

    public int size() {
        return store.size();
    }

    public int getPendingConditionCount() {
        int count = 0;
        for (Transaction transaction : store) {
            if (transaction.getStatus() == TradeMeta.T39_PENDING_TRIGGER) {
                count++;
            }
        }
        return count;
    }

    public ArrayList<Transaction> getSendingTransactions() {
        ArrayList<Transaction> trans = new ArrayList<Transaction>();
        for (Transaction transaction : store) {
            if (transaction.getStatus() == TradeMeta.T39_SENDING) {
                trans.add(transaction);
            }
        }
        return trans;
    }

    public void clear() {
        store.clear();
        store.trimToSize();
        filteredStore.clear();
        filteredStore.trimToSize();
    }

    public String getOrdersXML() {
        StringBuilder buffer = new StringBuilder();
        for (Transaction transaction : store) {
            buffer.append(transaction.getXML());
        }
        return buffer.toString();
    }

    class TransactionComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            Transaction i1 = (Transaction) o1;
            Transaction i2 = (Transaction) o2;
            if (i1.getOrderDate() > i2.getOrderDate()) {
                return -1;
            } else if (i1.getOrderDate() < i2.getOrderDate()) {
                return 1;
            } else {
                return (i2.getClOrderID().compareTo(i1.getClOrderID()));
            }
        }

    }
}
