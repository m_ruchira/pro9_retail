package com.isi.csvr.trading.datastore;

import com.isi.csvr.trading.CashTransaction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 4, 2005
 * Time: 2:37:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashTransactionSearchStore implements Serializable {
    private static CashTransactionSearchStore self = null;
    private ArrayList store;
    private Hashtable pageStore;
    private String tableName;
    private boolean canGoNext;
    private boolean canGoPrev;
    private int currentPage = 0;
    private long pageBottom = 0;

    private CashTransactionSearchStore() {
        store = new ArrayList();
        pageStore = new Hashtable();

        //CashTransaction cs = new CashTransaction(1, 200, 121, 20032020, "seeter");
        //store.add(cs);

    }

    public static synchronized CashTransactionSearchStore getSharedInstance() {
        if (self == null) {
            self = new CashTransactionSearchStore();
        }
        return self;
    }

    public void addTransaction(CashTransaction transaction) {
        store.add(transaction);
        //setPageBottom(transaction.getReference());
//        applyFilter();
    }

    public long getCurrentPageBottom() {
        return pageBottom;
    }

    public void setCurrentPageBottom(long id) {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            pageRecord = new PageRecord();
            pageStore.put("" + currentPage, pageRecord);
        }
        if (pageRecord.bottom == -1)
            pageRecord.bottom = id;
        System.out.println("Bottom " + pageRecord.bottom);
    }

    public CashTransaction getTransaction(int index) {
        return (CashTransaction) store.get(index);
    }

    public void setTransactionSummary() {
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    public void setNewPage() {
        currentPage++;
        if (currentPage <= 1) {
            setCanGoPrev(false);
        } else {
            setCanGoPrev(true);
        }
    }

    public void goPageBack() {
        currentPage--;
        currentPage--;
        if (currentPage <= 1) {
            setCanGoPrev(false);
        } else {
            setCanGoPrev(true);
        }
    }

    public long getPageTop() {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            return 0;
        } else {
            return pageRecord.top;
        }
    }

    public long getPageBottom() {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            return 0;
        } else {
            return pageRecord.bottom;
        }
    }

    public void setPageBottom(long id) {
        pageBottom = id;
    }

    public void setCurrentPageTop(long id) {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            pageRecord = new PageRecord();
            pageStore.put("" + currentPage, pageRecord);
        }
        if (pageRecord.top == -1)
            pageRecord.top = id;
    }

    public int size() {
        return store.size();
    }

    public void clear() {
        store.clear();
        store.trimToSize();
    }

    public void init() {
        store.clear();
        store.trimToSize();
        pageStore.clear();
        currentPage = 0;
        tableName = "*";
    }

    public boolean canGoNext() {
        return canGoNext;
    }

    public void setCanGoNext(boolean canGoNext) {
        this.canGoNext = canGoNext;
    }

    public boolean canGoPrev() {
        return canGoPrev;
    }

    public void setCanGoPrev(boolean canGoPrev) {
        this.canGoPrev = canGoPrev;
    }

    private class PageRecord {
        public long top = -1;
        public long bottom = -1;
    }

}
