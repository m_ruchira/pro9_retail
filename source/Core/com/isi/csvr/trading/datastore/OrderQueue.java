package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;

import java.io.*;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 1:44:46 PM
 */
public class OrderQueue implements Serializable {
    public static OrderQueue self = null;
    private ArrayList<QueuedTransaction> list;
    private ArrayList<QueuedTransaction> selectedList;
    private ArrayList<QComponent> names;
    private long selectedQID = 0;

    private OrderQueue() {
        try {
            selectedList = new ArrayList<QueuedTransaction>();
            loadData();
            filterTransactions();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized OrderQueue getSharedInstance() {
        if (self == null) {
            self = new OrderQueue();
        }
        return self;
    }

    public void addTransaction(QueuedTransaction transaction) {
        list.add(transaction);
        filterTransactions();
    }

    public QueuedTransaction getTransaction(int index) {
        return selectedList.get(index);
    }

    public QueuedTransaction searhTransaction(String id) {
        QueuedTransaction transaction;
        for (QueuedTransaction aList : selectedList) {
            transaction = aList;
            if (transaction.getMubasherOrderNumber().equals(id))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public void removeQueue(long qID) {
        try {
            for (int i = list.size() - 1; i >= 0; i--) {
                QueuedTransaction transaction = list.get(i);
                if (transaction.getQID() == qID) {
                    selectedList.remove(transaction);
                    list.remove(transaction);
                }
            }
            deleteComponent(qID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeTransaction(String id) {
        QueuedTransaction transaction;
        for (int i = 0; i < selectedList.size(); i++) {
            transaction = selectedList.get(i);
            if ((transaction.getTransactionID() != null) && (transaction.getTransactionID().equals(id)))
                selectedList.remove(i);
            transaction = null;
        }

        for (int i = 0; i < list.size(); i++) {
            transaction = list.get(i);
            if ((transaction.getTransactionID() != null) && (transaction.getTransactionID().equals(id)))
                list.remove(i);
            transaction = null;
        }
    }

    public void removeTransaction(QueuedTransaction toBeRemoved) {
        QueuedTransaction transaction;
        for (int i = 0; i < selectedList.size(); i++) {
            transaction = selectedList.get(i);
            if ((transaction.equals(toBeRemoved))) {
                selectedList.remove(i);
            }
            transaction = null;
        }
        for (int i = 0; i < list.size(); i++) {
            transaction = list.get(i);
            if ((transaction.equals(toBeRemoved))) {
                list.remove(i);
            }
            transaction = null;
        }
    }

    public void removeTransaction(int index) {
        try {
            QueuedTransaction transaction = selectedList.remove(index);
            list.remove(transaction);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int size() {
        return selectedList.size();
    }

    public int fullSize() {
        return list.size();
    }

    public void saveData() {
        try {
            Hashtable<String, Object> store = new Hashtable<String, Object>();
            store.put("NAMES", names);
            store.put("LIST", list);
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/orderq.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(store);
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadData() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/orderq.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            Hashtable store = (Hashtable) oObjIn.readObject();
            names = (ArrayList<QComponent>) store.get("NAMES");
            if (names == null) {
                names = new ArrayList<QComponent>();
            }
            list = (ArrayList<QueuedTransaction>) store.get("LIST");
            if (list == null) {
                list = new ArrayList<QueuedTransaction>();
            }
            store = null;
            oIn.close();
            oIn = null;
        } catch (Exception e) {
            names = new ArrayList<QComponent>();
            list = new ArrayList<QueuedTransaction>();
//            QComponent component = createComponent(Language.getString("DEFAULT_BASKET"));
            createComponent(Language.getString("DEFAULT_BASKET"));
        }
    }

    public long getSelectedQID() {
        return selectedQID;
    }

    public void setSelectedQID(long selectedQID) {
        this.selectedQID = selectedQID;
        filterTransactions();
    }

    private void filterTransactions() {
        selectedList.clear();
        for (QueuedTransaction transaction : list) {
            if (transaction.getQID() == this.selectedQID) {
                selectedList.add(transaction);
            }
        }
    }

    public QComponent createComponent(String caption) {
        QComponent qComponent = new QComponent(System.currentTimeMillis(), caption);
        names.add(qComponent);
        return qComponent;
    }

    public void deleteComponent(int index) {
        names.remove(index);
    }

    public void deleteComponent(long id) {
        for (int i = 0; i < names.size(); i++) {
            QComponent component = names.get(i);
            if (component.getId().equals("" + id)) {
                names.remove(i);
                return;
            }
        }
    }

    public QComponent getQConmonent(int index) {
        return names.get(index);
    }

    public QComponent getQComponent(long id) {
        for (int i = 0; i < names.size(); i++) {
            QComponent component = names.get(i);
            if (component.getId().equals("" + id)) {
                return names.get(i);
            }
        }
        return null;
    }

    public int getQCount() {
        return names.size();
    }
}