package com.isi.csvr.trading.datastore;

import com.dfn.mtr.mix.beans.ExchangeRates;
import com.dfn.mtr.mix.beans.ExchangeRatesResponce;
import com.dfn.mtr.mix.beans.MIXObject;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.util.Decompress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 31, 2006
 * Time: 9:35:04 PM
 */
public class CurrencyStore implements TradingConnectionListener {

    private static CurrencyStore self = null;
    private static HashMap<String, HashMap<String, Currency>> store;
    private static HashMap<String, HashMap<String, Currency>> primaryStore;
    private static HashMap<String, HashMap<String, Currency>> secondaryStore;
    private static Hashtable<String, String> descriptions;
    private static ArrayList<CurrencyListener> listeners;
    private static ArrayList<String> currencyIndex;

    private CurrencyStore() {
        store = new HashMap<String, HashMap<String, Currency>>();
        primaryStore = new HashMap<String, HashMap<String, Currency>>();
        secondaryStore = new HashMap<String, HashMap<String, Currency>>();
        descriptions = new Hashtable<String, String>();
        listeners = new ArrayList<CurrencyListener>();
        currencyIndex = new ArrayList<String>();
        loadCurrencyDetails();
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static synchronized CurrencyStore getSharedInstance() {
        if (self == null) {
            self = new CurrencyStore();
        }
        return self;
    }

    private static void updateCurrencyIndex(String currency) {
        if (!currencyIndex.contains(currency)) {
            currencyIndex.add(currency);
        }
    }

    public static ArrayList<String> getCurrencyIndex() {
        return currencyIndex;
    }

    private static synchronized Currency getCurrencyObject(String base, String target, byte path) {
        HashMap<String, Currency> map = null;
        if (path == Constants.PATH_SECONDARY) {
            map = secondaryStore.get(base);
            if (map == null) {
                map = new HashMap<String, Currency>();
                secondaryStore.put(base, map);
                updateCurrencyIndex(base);
            }
        } else {
            map = primaryStore.get(base);
            if (map == null) {
                map = new HashMap<String, Currency>();
                primaryStore.put(base, map);
                updateCurrencyIndex(base);
            }
        }
//        HashMap<String, Currency> map = store.get(base);
       /* if (map == null) {
            map = new HashMap<String, Currency>();
            store.put(base, map);
            updateCurrencyIndex(base);
        }*/
        Currency currency = map.get(target);
        if (currency == null) {
            currency = new Currency(0, 0);
            map.put(target, currency);
            updateCurrencyIndex(target);
        }
        return currency;
    }

    /**
     * returns the cross currencsy rate for given symbols
     *
     * @param base
     * @param target
     * @return buyrate or 0 if error
     */
    public static double getBuyRate(String base, String target, byte path) {
        try {
            if (base.equals(target)) {
                return 1;
            } else {
                return getCurrencyObject(base, target, path).buyrate;
            }
        } catch (Exception e) {
            return 0d;
        }
    }

    /**
     * returns the cross currencsy rate for given symbols
     *
     * @param base
     * @param target
     * @return sellrate or 0 if error
     */
    public static double getSellRate(String base, String target, byte path) {
        try {
            if (base.equals(target)) {
                return 1;
            } else {
                return getCurrencyObject(base, target, path).sellrate;
            }
        } catch (Exception e) {
            return 0D;
        }
    }

    public static void loadCurrencyDetails() {

        try {
            String record;
            Decompress decompress = new Decompress();
            boolean bIsLangSpec = false;
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/currencies_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            bIsLangSpec = f.exists();
            ByteArrayOutputStream out;
            //if (bIsLangSpec)
            out = decompress.setFiles(sLangSpecFileName);
            //else
            //    out = decompress.setFiles("system/currencies.msf");

            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            do {
                record = SharedMethods.readLine(in);
                if (record != null) {
                    record = record.trim();
                    String[] fields = record.split(Meta.DS);
                    String key = fields[0];
                    descriptions.put(key, Language.getLanguageSpecificString(UnicodeUtils.getNativeString(fields[1])));
                }
            } while (record != null);
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*try {
            SmartProperties properties = new SmartProperties(Meta.DS);
            properties.loadCompressed("system/currency.msf");
            Enumeration keys = properties.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String description = Language.getLanguageSpecificString(properties.getProperty(key));
                descriptions.put(key, UnicodeUtils.getNativeString(description));
            }
            properties = null;
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public static double getAdjustedPriceForBuy(String base, String ref, double value, String portfolioID) {
        try {
//            double rate = getBuyRate(base, ref, TradingShared.getTrader().getPath(portfolioID));
            double rate = getSellRate(base, ref, TradingShared.getTrader().getPath(portfolioID));// changed bu udaya all pro conversions should use sell rate
            return value * rate;
        } catch (Exception e) {
            return value;
        }
    }

    public static float getAdjustedPriceForBuy(String base, String ref, float value, String portfolioID) {
        try {
            double rate = getBuyRate(base, ref, TradingShared.getTrader().getPath(portfolioID));
            return (float) (value * rate);
        } catch (Exception e) {
            return value;
        }
    }

    public static double getAdjustedPriceForBuy(String base, String ref, double value, byte path) {
        try {
//            double rate = getBuyRate(base, ref, path);
            double rate = getSellRate(base, ref, path);
            return value * rate;
        } catch (Exception e) {
            return value;
        }
    }

    public static float getAdjustedPriceForBuy(String base, String ref, float value, byte path) {
        try {
            double rate = getBuyRate(base, ref, path);
            return (float) (value * rate);
        } catch (Exception e) {
            return value;
        }
    }

    public static double getAdjustedPriceForSell(String base, String ref, double value, String portfolioID) {
        try {
            double rate = getSellRate(base, ref, TradingShared.getTrader().getPath(portfolioID));
            return value * rate;
        } catch (Exception e) {
            return value;
        }
    }

    public static float getAdjustedPriceForSell(String base, String ref, float value, String portfolioID) {
        try {
            double rate = getSellRate(base, ref, TradingShared.getTrader().getPath(portfolioID));
            return (float) (value * rate);
        } catch (Exception e) {
            return value;
        }
    }

    public static double getAdjustedPriceForSell(String base, String ref, double value, byte path) {
        try {
            double rate = getSellRate(base, ref, path);
            return value * rate;
        } catch (Exception e) {
            return value;
        }
    }

    public static float getAdjustedPriceForSell(String base, String ref, float value, byte path) {
        try {
            double rate = getSellRate(base, ref, path);
            return (float) (value * rate);
        } catch (Exception e) {
            return value;
        }
    }

    /**
     * Set the currency record specified in the string to the store
     *
     * @param currencyData
     */
    public void addCurrency(String currencyData, byte path) {
        try {
            System.out.println("Currency " + currencyData);
            TradeMessage message = new TradeMessage(currencyData);
            String[] records = message.getMessageData().split(TradeMeta.DS);
//            String[] records = currencyData.split("\\|");
            for (int i = 0; i < records.length; i++) {
                String[] fields = records[i].split(TradeMeta.FD);
                addCurrency(fields[0], fields[1], Double.parseDouble(fields[2]), Double.parseDouble(fields[3]), path);
                fields = null;
            }
            records = null;
            fireCurrencyAdded();
        } catch (Exception e) {
            // do nothing
            e.printStackTrace();
        }
    }

    public void addCurrency(MIXObject currencyData, byte path) {
        try {

            com.dfn.mtr.mix.beans.TransferObject[] currnecy = currencyData.getTransferObjects();
            if (currnecy == null || currnecy.length == 0) {
                return;
            }
            ExchangeRatesResponce rates = (ExchangeRatesResponce) currnecy[0];
            List exchangerates = rates.getExchRates();
            if (exchangerates == null) {
                return;
            }
            for (int i = 0; i < exchangerates.size(); i++) {
                ExchangeRates rate = (ExchangeRates) exchangerates.get(i);
                addCurrency(rate.getBaseCurrency(), rate.getTargetCurrency(), rate.getBuyRate(), rate.getSellRate(), path);
            }
            fireCurrencyAdded();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    /**
     * Add/set currency data to the store
     *
     * @param base     currency
     * @param target   currency
     * @param buyrate
     * @param sellrate
     */
    private void addCurrency(String base, String target, double buyrate, double sellrate, byte path) {
        Currency currency = getCurrencyObject(base, target, path);

        currency.buyrate = buyrate;
        currency.sellrate = sellrate;
        currency = null;
    }

    public void clearStore() {
        primaryStore.clear();
        secondaryStore.clear();
        store.clear();
        currencyIndex.clear();
        currencyIndex.trimToSize();
    }

    public ArrayList<String> getCurrencyIDs() {
        return currencyIndex;
    }

    public String getCurrencyDescription(String id) {
        return descriptions.get(id);
    }

    public void addCurrencyListener(CurrencyListener listener) {
        listeners.add(listener);
    }

    public void removeCurrencyListener(CurrencyListener listener) {
        listeners.remove(listener);
    }

    private void fireCurrencyAdded() {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                CurrencyListener listener = listeners.get(i);
                listener.currencyAdded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tradeServerConnected() {

    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
//        store.clear();
    }
}
