package com.isi.csvr.trading.datastore;

import com.dfn.mtr.mix.beans.MIXObject;
import com.dfn.mtr.mix.beans.MarginableSymbolResponse;
import com.dfn.mtr.mix.beans.Symbol;
import com.dfn.mtr.mix.beans.TransferObject;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trading.marginTrading.MarginSymbolRecord;
import com.isi.csvr.trading.shared.TradeKey;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Oct 6, 2008
 * Time: 9:53:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarginSymbolStore {
    public static MarginSymbolStore self;
    private Hashtable<String, MarginSymbolRecord> marginStore;
    //  private DynamicArray<MarginSymbolRecord> marginableStore;

    private MarginSymbolStore() {
        marginStore = new Hashtable<String, MarginSymbolRecord>();
        //  marginableStore = new DynamicArray<MarginSymbolRecord>();

    }

    public static MarginSymbolStore getSharedInstance() {
        if (self == null) {
            self = new MarginSymbolStore();
        }
        return self;
    }

    public void setData(MIXObject frame) {
        TransferObject[] resp = frame.getTransferObjects();
        if (resp == null || resp.length < 1) {
            return;
        }
        MarginableSymbolResponse response = (MarginableSymbolResponse) resp[0];
        if (response.getSymbolList() == null) {
            return;
        }
        // marginStore.clear();
        //  marginableStore.clear();
        List<Symbol> symbols = response.getSymbolList();
        for (int i = 0; i < symbols.size(); i++) {
            Symbol sym = symbols.get(i);
            MarginSymbolRecord mr = new MarginSymbolRecord();
            mr.setExchange(sym.getExchange());
            mr.setInstrumentType(sym.getInstrumentType());
            mr.setSymbol(sym.getSymbol());
            mr.setMarginBuypct(Double.parseDouble(sym.getSymbolMarginablePercentage()));
            marginStore.put(mr.getSKey().getTradeKey(), mr);
            // marginableStore.add(mr);
        }
    }

    public void setData(String frame) {
        try {
            TradeMessage message = new TradeMessage(frame);
            String[] marginSymbolData = message.getMessageData().split(TradeMeta.RS);
            String[] data = marginSymbolData[0].split(TradeMeta.DS);
            for (int i = 0; i < data.length; i++) {
                String[] marginRecords = data[i].split(TradeMeta.FD);
                try {
                    String exchange = null;
                    String symbol = null;
                    int instrumentType = 0;
                    double marginBuypct = 0d;
                    double marginDaybuypct = 0d;
                    char tag;
                    for (int k = 0; k < marginRecords.length; k++) {
                        tag = marginRecords[k].charAt(0);
                        switch (tag) {
                            case 'A':
                                exchange = (marginRecords[k].substring(1));
                                break;
                            case 'B':
                                symbol = (marginRecords[k].substring(1));
                                break;
                            case 'C':
                                instrumentType = Integer.parseInt(marginRecords[k].substring(1));
                                break;
                            case 'D':
                                marginBuypct = Double.parseDouble(marginRecords[k].substring(1));
                                break;
                            case 'E':
                                marginDaybuypct = Double.parseDouble(marginRecords[k].substring(1));
                                break;
                        }
                    }
                    symbol = SharedMethods.getSymbolFromKey(TradingShared.getPriceSymbol(exchange, symbol, instrumentType, null));
                    MarginSymbolRecord mr = new MarginSymbolRecord();
                    mr.setExchange(exchange);
                    mr.setSymbol(symbol);
                    mr.setInstrumentType(instrumentType);
                    mr.setMarginBuypct(marginBuypct);
                    mr.setMarginDayBuypct(marginDaybuypct);
                    marginStore.put(SharedMethods.getKey(exchange, symbol, instrumentType), mr);
                    //  marginableStore.add(mr);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public boolean isMarginSymbolAvailable(TradeKey key) {
        if (marginStore.containsKey(key.getTradeKey())) {
            return true;
        } else {
            return false;
        }

    }

    public MarginSymbolRecord getMarginSymbolRecord(TradeKey key) {
        return marginStore.get(key.getTradeKey());

    }

    public Hashtable getMarginStore() {
        return marginStore;

    }

//    public DynamicArray getMarginbleStore() {
//        return marginableStore;
//    }
//     public DynamicArray getMarginbleStore(String exchange){
//         try {
//             return getMarginbleStore(exchange);
//         } catch (Exception e) {
//             return new DynamicArray();
//         }

    //     }
    public MarginSymbolRecord getTableRecord(String exg, int index) {
        try {
            return (MarginSymbolRecord) (getMarginbleStore(exg)).get(index);
        } catch (Exception e) {
            return new MarginSymbolRecord();
        }
    }

    public DynamicArray getMarginbleStore(String exchange) {
        DynamicArray temp = new DynamicArray();
        if (!exchange.equalsIgnoreCase(Language.getString("ALL"))) {
            Enumeration<String> enu = marginStore.keys();
            if (enu != null) {
                while (enu.hasMoreElements()) {
                    String key = enu.nextElement();
                    MarginSymbolRecord mr = (MarginSymbolRecord) marginStore.get(key);
                    if (mr.getExchange().equals(exchange)) {
                        temp.add(mr);

                    }
                }
            }

            return temp;
        } else {
            Enumeration<String> enu = marginStore.keys();
            if (enu != null) {
                while (enu.hasMoreElements()) {
                    String key = enu.nextElement();
                    MarginSymbolRecord mr = (MarginSymbolRecord) marginStore.get(key);
//                    if (mr.getExchange().equals(exchange)) {
                    temp.add(mr);


                }
            }
            return temp;
        }

    }

    public void clearAllstores() {
//        if (marginableStore != null) {
//            marginableStore.clear();
//        }
        if (marginStore != null) {
            marginStore.clear();
        }
    }


}
