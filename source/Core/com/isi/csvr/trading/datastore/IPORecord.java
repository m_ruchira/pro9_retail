package com.isi.csvr.trading.datastore;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 24-Jun-2008 Time: 16:49:27 To change this template use File | Settings
 * | File Templates.
 */
public class IPORecord {
    String symbol;
    String discriptionEn;
    String discriptionAr;
    String id;
    String exchange;
    String currency;
    double value = 0;

    public IPORecord() {

    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDiscriptionEn() {
        return discriptionEn;
    }

    public void setDiscriptionEn(String discription) {
        this.discriptionEn = discription;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getDiscriptionAr() {
        return discriptionAr;
    }

    public void setDiscriptionAr(String discriptionAr) {
        this.discriptionAr = discriptionAr;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
