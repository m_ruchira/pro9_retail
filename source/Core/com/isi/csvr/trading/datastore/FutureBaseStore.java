package com.isi.csvr.trading.datastore;

import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 21, 2007
 * Time: 6:49:35 PM
 */
public class FutureBaseStore {

    private static FutureBaseStore self;
    private Hashtable<String, FutureBaseAttributes> store;

    private FutureBaseStore() {
        store = new Hashtable<String, FutureBaseAttributes>();
    }

    public static synchronized FutureBaseStore getSharedInstance() {
        if (self == null) {
            self = new FutureBaseStore();
        }
        return self;
    }

    public void addAttribute(String key, FutureBaseAttributes attributes) {
        store.put(key, attributes);
    }

    public FutureBaseAttributes getAttribute(String key) {
        return store.get(key);
    }
}
