package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.Language;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 28-Jun-2008 Time: 16:05:04 To change this template use File | Settings
 * | File Templates.
 */
public class IPOTableRecord {
    String DescriptionEn = null;
    String DescriptionAr = null;
    String Symbol = null;
    long quntity;
    double totalValue;
    String date;
    String exchange;
    int status;
    String currency;
    String transactID;

    public IPOTableRecord() {

    }

    public String getDescriptionEn() {
        if (Language.isLTR()) {
            return DescriptionEn;
        } else {
            return DescriptionAr;
        }
    }

    public void setDescriptionEn(String descriptionEn) {
        DescriptionEn = descriptionEn;
    }

    public String getDescriptionAr() {
        return DescriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        DescriptionAr = descriptionAr;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String symbol) {
        Symbol = symbol;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(double totalValue) {
        this.totalValue = totalValue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getQuntity() {
        return quntity;
    }

    public void setQuntity(long quntity) {
        this.quntity = quntity;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransactID() {
        return transactID;
    }

    public void setTransactID(String transactID) {
        this.transactID = transactID;
    }
}
