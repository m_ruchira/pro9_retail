package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.DynamicArray;

import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 31, 2004
 * Time: 10:16:57 AM
 */
public class OrderDepthStore implements Comparator {
    public static OrderDepthStore self = null;
    private Hashtable store;

    private OrderDepthStore() {
        store = new Hashtable();
    }

    public static synchronized OrderDepthStore getSharedInstance() {
        if (self == null) {
            self = new OrderDepthStore();
        }
        return self;
    }

    public void addRecord(Transaction transaction) {
        DynamicArray transactions = (DynamicArray) store.get(transaction.getMubasherOrderNumber());
        if (transactions == null) {
            transactions = new DynamicArray();
            store.put(transaction.getMubasherOrderNumber(), transactions);
        }
        if (Collections.binarySearch(transactions.getList(), transaction, this) < 0) { // check if this txn is already in
            transactions.add(transaction);
            Collections.sort(transactions.getList(), this);
        }
        transactions = null;
    }

    public void updateRecord(Transaction transaction) {
        if (transaction.getMubasherOrderNumber() == null) return;

        DynamicArray transactions = (DynamicArray) store.get(transaction.getMubasherOrderNumber());
        if (transactions != null) {
            if (transaction.getLastPrice() == 0) {
                transactions = null;
                return;
            }
            transactions.add(transaction);
            Collections.sort(transactions.getList(), this);
            transactions = null;
        }
    }

    public DynamicArray getTransactions(String orderID) {
        return (DynamicArray) store.get(orderID);
    }

    public void unRegister(String orderID) {
        store.remove(orderID);
    }

    public void register(String orderID) {
        DynamicArray transactions = new DynamicArray();
        store.put(orderID, transactions);
        transactions = null;
    }

    public void clear() {
        store.clear();
    }

    public int compare(Object obj1, Object obj2) {
        try {
            return ((Transaction) obj1).getTransactionID().compareTo(((Transaction) obj2).getTransactionID());
//            return (int)(((Transaction)obj1).getTransactionID() - ((Transaction)obj2).getTransactionID());
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
