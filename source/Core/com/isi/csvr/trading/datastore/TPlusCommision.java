package com.isi.csvr.trading.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Dec 9, 2008
 * Time: 5:22:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class TPlusCommision {
    private String flat;
    private String min;
    private String max;
    private String pct;
    private String currency;
    private String tplus;

    public TPlusCommision() {

    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getPct() {
        return pct;
    }

    public void setPct(String pct) {
        this.pct = pct;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTplus() {
        return tplus;
    }

    public void setTplus(String tplus) {
        this.tplus = tplus;
    }
}
