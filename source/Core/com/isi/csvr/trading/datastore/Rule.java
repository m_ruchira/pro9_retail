package com.isi.csvr.trading.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 10, 2005
 * Time: 3:22:00 PM
 */
public class Rule implements Cloneable {
    private String id;
    private String exchange;
    private String portfolio;
    private String category;
    private String rule;
    private String message;

    public Rule(String id, String exchange, String category, String portfolio, String rule, String message) {
        this.category = category;
        this.id = id;
        this.exchange = exchange;
        this.portfolio = portfolio;
        this.message = message;
        this.rule = rule;
    }

    public String getCategory() {
        return category;
    }

    public String getId() {
        return id;
    }

    public String getExchange() {
        return exchange;
    }

    public String getPortfolio() {
        return portfolio;
    }

    public String getMessage() {
        return message;
    }

    public String getRule() {
        return rule;
    }


    public String toString() {
        return "Rule{" +
                "category='" + category + "'" +
                ", id='" + id + "'" +
                ", exchange='" + exchange + "'" +
                ", rule='" + rule + "'" +
                ", message='" + message + "'" +
                "}";
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
