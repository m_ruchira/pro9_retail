package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.trading.TradeMethods;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 1:44:46 PM
 */
public class OrderSearchStore implements Serializable {
    public static OrderSearchStore self = null;
    private DynamicArray store;
    //    private DynamicArray filteredStore;
    private boolean includeExecuted = false;
    private Hashtable pageStore;
    private int currentPage;
    private boolean canGoNext;
    private boolean canGoPrev;
    private String tableName;

    private OrderSearchStore() {
        try {
//            loadData();
            store = new DynamicArray();
            pageStore = new Hashtable();
//            filteredStore = new DynamicArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized OrderSearchStore getSharedInstance() {
        if (self == null) {
            self = new OrderSearchStore();
        }
        return self;
    }

    public void addTransaction(Transaction transaction) {
        store.add(transaction);
        TradeMethods.getSharedInstance().getOrderSearchWindow().enableButtons();
//        applyFilter();
    }

    public Transaction getTransaction(int index) {
        return (Transaction) store.get(index);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Transaction searhTransactionByOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getMubasherOrderNumber() != null) && (transaction.getMubasherOrderNumber().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public Transaction searhTransactionByClOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getClOrderID() != null) && (transaction.getClOrderID().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public Transaction searhTransactionByMubasherOrderID(String id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getMubasherOrderNumber() != null) && (transaction.getMubasherOrderNumber().equals(id)))
                return transaction;
            transaction = null;
        }
        return null;
    }

    public void removeTransactionByOrderID(long id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getOrderID() != null) && (transaction.getOrderID().equals(id))) {
                store.remove(i);
            }
            transaction = null;
        }
//        applyFilter();
    }

    public void replaceTransactionByOrderID(Transaction newTransaction) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if ((transaction.getOrderID() != null) && (transaction.getOrderID().equals(newTransaction.getOrderID()))) {
                //if (newTransaction.getSequenceNumber() > transaction.getSequenceNumber()){
                store.set(i, newTransaction);
                break;
                //}
            }
            transaction = null;
        }
    }

    public Transaction searhTransactionByMessageID(long id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if (transaction.getMessageID() == id)
                return transaction;
            transaction = null;
        }
        return null;
    }

    public void removeTransactionByMessageID(long id) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if (transaction.getMessageID() == id)
                store.remove(i);
            transaction = null;
        }
//        applyFilter();
    }

    public void replaceTransactionByMessageID(Transaction newTransaction) {
        Transaction transaction;
        for (int i = 0; i < store.size(); i++) {
            transaction = (Transaction) store.get(i);
            if (transaction.getMessageID() == newTransaction.getMessageID()) {
                store.set(i, newTransaction);
                break;
            }
            transaction = null;
        }
    }

    public boolean isIncludeExecuted() {
        return includeExecuted;
    }

//    public void setIncludeExecuted(boolean includeExecuted) {
//        this.includeExecuted = includeExecuted;
////        applyFilter();
//    }

//    public void applyFilter(){
//        Transaction transaction;
//        filteredStore.clear();
//        for (int i = 0; i < store.size(); i++) {
//            transaction = (Transaction)store.get(i);
//            if ((transaction.getStatus() == TradeMeta.T39_Filled)){
//                if (isIncludeExecuted()){
//                    filteredStore.add(transaction);
//                }
//            }else{
//                filteredStore.add(transaction);
//            }
//            transaction = null;
//        }
//    }

    public void setNewPage() {
        currentPage++;
        if (currentPage <= 1) {
            setCanGoPrev(false);
        } else {
            setCanGoPrev(true);
        }
    }

    public void goPageBack() {
        currentPage--;
        currentPage--;
        if (currentPage <= 1) {
            setCanGoPrev(false);
        } else {
            setCanGoPrev(true);
        }
    }

    public long getPageTop() {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            return Integer.MAX_VALUE;
        } else {
            return pageRecord.top;
        }
    }

    public long getPageBottom() {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            return Long.MAX_VALUE;
        } else {
            return pageRecord.bottom;
        }
    }

    public void setCurrentPageTop(long id) {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            pageRecord = new PageRecord();
            pageStore.put("" + currentPage, pageRecord);
        }
        if (pageRecord.top == -1)
            pageRecord.top = id;
    }

    public void setCurrentPageBottom(long id) {
        PageRecord pageRecord = (PageRecord) pageStore.get("" + currentPage);
        if (pageRecord == null) {
            pageRecord = new PageRecord();
            pageStore.put("" + currentPage, pageRecord);
        }
        if (pageRecord.bottom == -1)
            pageRecord.bottom = id;
    }

    public int size() {
        return store.size();
    }

    public void clear() {
        store.clear();
        store.trimToSize();
    }

    public void init() {
        store.clear();
        store.trimToSize();
        pageStore.clear();
        currentPage = 0;
        tableName = "*";
    }

    public boolean canGoNext() {
        return canGoNext;
    }

    public void setCanGoNext(boolean canGoNext) {
        this.canGoNext = canGoNext;
    }

    public boolean canGoPrev() {
        return canGoPrev;
    }

    public void setCanGoPrev(boolean canGoPrev) {
        this.canGoPrev = canGoPrev;
    }

    private class PageRecord {
        public long top = -1;
        public long bottom = -1;
    }
}
