package com.isi.csvr.trading.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 31, 2006
 * Time: 9:41:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class Currency {
    double buyrate;
    double sellrate;

    public Currency(double buyrate, double sellrate) {
        this.buyrate = buyrate;
        this.sellrate = sellrate;
    }

}
