package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.trading.shared.BookKeeper;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Dec 2, 2008
 * Time: 2:58:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class TPlusStore {
    public static TPlusStore self;
    private Hashtable<String, TPlusRecord> tStore;
    private DynamicArray<TPlusRecord> tRecordStore;
    private Hashtable<String, BookKeeper> bookKeeperStore;
    private Hashtable<String, Hashtable> tPlusStore;
    private Hashtable<String, Hashtable> exchangeTPlusStore;
    private ArrayList<TPlusCommision> commisionDataStore;
    private boolean isTPlusRecordReceived = false;

    private TPlusStore() {
        tStore = new Hashtable<String, TPlusRecord>();
        tRecordStore = new DynamicArray<TPlusRecord>();
        bookKeeperStore = new Hashtable<String, BookKeeper>();
        tPlusStore = new Hashtable<String, Hashtable>();
        exchangeTPlusStore = new Hashtable<String, Hashtable>();
        commisionDataStore = new ArrayList<TPlusCommision>();


    }

    public static TPlusStore getSharedInstance() {
        if (self == null) {
            self = new TPlusStore();
        }
        return self;
    }

    public void setData(String frame, int x) {
        try {
            tPlusStore.clear();
            TradeMessage message = new TradeMessage(frame);
            String[] tPlusData = message.getMessageData().split(TradeMeta.RS);
            String tValue = tPlusData[0];
            String[] tPlusSymbols = tPlusData[1].split(TradeMeta.DS);
            for (int i = 0; i < tPlusSymbols.length; i++) {
                try {
                    TPlusRecord tRecord = new TPlusRecord();
                    tRecord.setSymbol(tPlusSymbols[i]);
                    tRecordStore.add(tRecord);
                    tStore.put(tPlusSymbols[i], tRecord);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            tPlusStore.put(tValue, tStore);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void setData(String frame) {
        try {
//            exchangeTPlusStore.clear();
            TradeMessage message = new TradeMessage(frame);
            String[] tPlusData = message.getMessageData().split(TradeMeta.RS);
            String exchange = tPlusData[0];
            Hashtable<String, Hashtable> tPlusHash;
            tPlusHash = exchangeTPlusStore.get(exchange);
            if (tPlusHash == null) {
                tPlusHash = new Hashtable<String, Hashtable>();
            }
            String tValue = tPlusData[1];
            Hashtable<String, TPlusRecord> tPlusRecordHash;
            tPlusRecordHash = tPlusHash.get(tValue);
            if (tPlusRecordHash == null) {
                tPlusRecordHash = new Hashtable<String, TPlusRecord>();
            }
            String[] tPlusSymbols = tPlusData[2].split(TradeMeta.DS);
            for (int i = 0; i < tPlusSymbols.length; i++) {
                try {
                    TPlusRecord tRecord = new TPlusRecord();
                    tRecord.setSymbol(tPlusSymbols[i]);
//                    tRecordStore.add(tRecord);
                    tPlusRecordHash.put(tPlusSymbols[i], tRecord);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            tPlusHash.put(tValue, tPlusRecordHash);
            exchangeTPlusStore.put(exchange, tPlusHash);
            isTPlusRecordReceived = true;

        } catch (Exception e) {
            //T+0 data not set
            e.printStackTrace();
        }


    }

    public void setTPlusCommisionData(String frame) {

        try {
            commisionDataStore.clear();
            TradeMessage message = new TradeMessage(frame);
            String[] tPlusCommisionData = message.getMessageData().split(TradeMeta.DS);
            for (int i = 0; i < tPlusCommisionData.length; i++) {
                try {
                    String[] data = tPlusCommisionData[i].split(TradeMeta.FD);
                    TPlusCommision tpc = new TPlusCommision();
                    tpc.setPct(data[1]);
                    tpc.setFlat(data[2]);
                    tpc.setMin(data[3]);
                    tpc.setMax(data[4]);
                    tpc.setCurrency(data[5]);
                    tpc.setTplus(data[6]);
                    commisionDataStore.add(tpc);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getTPlusCommisionObject(String currency, String tPlus) {
        try {
            ArrayList al = new ArrayList();
            Vector av = new Vector();
            String list = "";
            for (int i = 0; i < commisionDataStore.size(); i++) {
                TPlusCommision tcp = (TPlusCommision) commisionDataStore.get(i);
                if ((tcp.getCurrency().equals(currency)) && (tcp.getTplus().equals(tPlus))) {
                    list = list + "~" + tcp.getMax() + "," + tcp.getMin() + "," + tcp.getFlat() + "," + tcp.getPct();
                }

            }

            return list.substring(1);
        } catch (Exception e) {
            return "";
        }

    }

    public void setBookKeepers(String frame) {
        try {
            String[] records = frame.split(":");
            for (int i = 0; i < records.length; i++) {
                String[] data = records[i].split("~");
                BookKeeper bKeeper = new BookKeeper();
                bKeeper.setId(data[0]);
                bKeeper.setCode(data[1]);
                bKeeper.setName(UnicodeUtils.getNativeString(data[2]));
                bKeeper.setDefault(data[3] == "1");
                addBookKeeper(data[1], bKeeper);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    public void addBookKeeper(String key, BookKeeper bk) {
        bookKeeperStore.put(key, bk);

    }

    public BookKeeper getBookKeeper(String key) {
        try {
            return bookKeeperStore.get(key);
        } catch (Exception e) {
            return new BookKeeper();
        }
    }

    public DynamicArray<TPlusRecord> getTplusRecodrsStore() {
        return tRecordStore;
    }

    public Hashtable<String, Hashtable> getMainTPlusStore() {
        return exchangeTPlusStore;
    }

    public Hashtable<String, TPlusRecord> getTPlusRecords() {
        return tStore;
    }

    public boolean isTPlusSupportedSymbol(String symbol) {
        try {
            if (tStore.containsKey(symbol)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isTPlusSupportedSymbol(String key, String tvalue) {
        try {
            String exchange = SharedMethods.getExchangeFromKey(key);
            Hashtable<String, Hashtable> tPlusHash = exchangeTPlusStore.get(exchange);
            Hashtable<String, Hashtable> tPlusRecordHash = tPlusHash.get(tvalue);
            String searchKey = SharedMethods.getExchangeKey(key);
            try {
                if (tPlusRecordHash.containsKey(searchKey)) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isTPlusRecordReceived() {
        return isTPlusRecordReceived;
    }
}
