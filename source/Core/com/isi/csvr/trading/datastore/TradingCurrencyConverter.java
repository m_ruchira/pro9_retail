package com.isi.csvr.trading.datastore;

import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;

import java.io.FileInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Jul 27, 2009
 * Time: 6:14:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class TradingCurrencyConverter {

    private static TradingCurrencyConverter self = null;
    private static SmartProperties g_oAssetClasses;

    private TradingCurrencyConverter() {
        load();
    }

    public static TradingCurrencyConverter getSharedInstance() {
        if (self == null) {
            self = new TradingCurrencyConverter();
        }
        return self;
    }

    public String getDisplayCurrency(String baseCurrecy) {
        try {
            String sValue = null;
            sValue = getCurrency((String) g_oAssetClasses.get(baseCurrecy));
            return sValue;
        } catch (Exception e) {
            return baseCurrecy;
        }
    }

    public double getCurrencyMultiflyFactor(String baseCurrecy) {
        double sValue = 1D;
        try {
            sValue = getFactor((String) g_oAssetClasses.get(baseCurrecy));
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return sValue;
    }

    private String getCurrency(String data) {
        return data.split(Meta.FS)[0];
    }

    private double getFactor(String data) {
        try {
            return Double.parseDouble(data.split(Meta.FS)[1]);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return 1D;
    }

    private void load() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/Trade_currencies.dll");
            g_oAssetClasses = new SmartProperties(Meta.DS);
            g_oAssetClasses.load(oIn);
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
