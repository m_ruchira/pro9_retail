package com.isi.csvr.trading.datastore;

import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.PageCounter;
import com.isi.csvr.trading.SendQueue;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.shared.*;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Jan 9, 2008
 * Time: 3:29:56 PM
 */
public class CashLogSearchStore implements Serializable {
    public static CashLogSearchStore self = null;
    private DynamicArray store;
    private boolean canGoNext;

    private String portfolio;
    private String type;
    private String account;
    private String fromDate;
    private String toDate;
    private PageCounter pageCounter;

    private CashLogSearchStore() {
        try {
            store = new DynamicArray();
            pageCounter = new PageCounter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized CashLogSearchStore getSharedInstance() {
        if (self == null) {
            self = new CashLogSearchStore();
        }
        return self;
    }

    public void addCashLogRecord(CashLogRecord cashLog) {
        store.add(cashLog);
    }

    public CashLogRecord getCashLogRecord(int index) {
        return (CashLogRecord) store.get(index);
    }

    public void sendNewRequest(String portfolio, String type, String fromDate, String toDate) {
        sendRequest(portfolio, type, fromDate, toDate, null, "0");
    }

    private void sendRequest(String portfolio, String type, String fromDate, String toDate, String account, String startIndex) {
        try {
            this.portfolio = portfolio;
            this.type = type;
            this.fromDate = fromDate;
            this.toDate = toDate;
            if (account == null) {
                account = TradingShared.getTrader().getAccoutOfPortfolio(portfolio).getAccountNumber();
            }
            this.account = account;
            MIXObject cashlogserch = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_INQUIRY, MIXConstants.REQUEST_TYPE_CASH_LOG_SEARCH, TradingShared.getTrader().getPath(portfolio));
            InquiryObject cashserch = new InquiryObject();
            cashserch.setPortfolioID(portfolio);
            cashserch.setCashLogType(type);
            cashserch.setSequenceNo(startIndex);
            cashserch.setStartDate(fromDate);
            cashserch.setEndDate(toDate);
//          cashserch.set               todo check for account

            TradeMessage message = new TradeMessage(TradeMeta.MT_CASH_LOG_SEARCH);
            message.addData("A" + portfolio);
            message.addData("B" + type);
            message.addData("C" + account);
            message.addData("D" + startIndex);
            message.addData("E" + fromDate);
            message.addData("F" + toDate);

            Pagination page = new Pagination();
            page.setStartingSequenceNumber(startIndex);
            page.setPageWidth(25);
            cashserch.setPages(page);
            cashlogserch.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{cashserch});
            if (TradingShared.isConnected()) {
                //SendQueue.getSharedInstance().addData(message.toString(), TradingShared.getTrader().getPath(portfolio));
                SendQueue.getSharedInstance().addData(cashlogserch.getMIXString(), TradingShared.getTrader().getPath(portfolio));
            }
            message = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendNextRequest() {
        long lastID = Long.parseLong(pageCounter.getNext()) + 1;

        sendRequest(portfolio, type, fromDate, toDate, account, "" + lastID);
    }

    public void sendPreviousRequest() {
        String lastID = pageCounter.getPrevious();
        sendRequest(portfolio, type, fromDate, toDate, account, lastID);
    }

    public void setData(String data) {
        try {
            String[] datas = data.split(Meta.DS);
            String startID = datas[1];
            String endID = datas[2];
            canGoNext = ("1".equals(datas[3].trim()));
            String[] records = datas[4].split(Meta.RS);
            CashLogRecord record;
            for (int i = 0; i < records.length; i++) {
                try {
                    record = new CashLogRecord();
                    record.setData(records[i]);
                    store.add(record);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            pageCounter.addPage(startID, endID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        TradeMethods.getSharedInstance().getCashLogSearchWindow().enableButtons();
    }

    public void setData(MIXObject response) {
        try {
            CashLogSearchResponse resp = (CashLogSearchResponse) response.getTransferObjects()[0];
            ArrayList<CashLogSearchRecord> cashLogRecords = resp.getCashLogSearchRecords();

            //    String[] datas = data.split(Meta.DS);
            String startID = resp.getPages().getStartingSequenceNumber();
            String endID = "";
            canGoNext = (resp.getPages().getIsNextPageAvailable() == 1);
            CashLogRecord record;
            int recordCount = cashLogRecords.size();
            for (int i = 0; i < cashLogRecords.size(); i++) {
                try {
                    record = new CashLogRecord();
                    record.setRecord(cashLogRecords.get(i));
                    store.add(record);
                    if (i == recordCount - 1) {
                        endID = record.getId();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            pageCounter.addPage(startID, endID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        TradeMethods.getSharedInstance().getCashLogSearchWindow().enableButtons();
    }


    public int size() {
        return store.size();
    }

    public void clear() {
        store.clear();
        store.trimToSize();
    }

    public void init() {
        store.clear();
        store.trimToSize();
        pageCounter.init();
    }

    public boolean canGoNext() {
        return canGoNext;
    }

    public boolean canGoPrev() {
        return pageCounter.hasPreviuos();
    }
}
