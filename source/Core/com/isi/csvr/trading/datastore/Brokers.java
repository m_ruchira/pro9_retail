package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.Settings;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 8, 2004
 * Time: 2:07:11 PM
 */
public class Brokers {
    public static Brokers self = null;
    private final short MW_DIRECT_LOGIN = 1;
    private Hashtable brokers;
    private String selectedBrokerID;

    private Brokers() {
        brokers = new Hashtable();
    }

    public static synchronized Brokers getSharedInstance() {
        if (self == null) {
            self = new Brokers();
        }
        return self;
    }

    //    public void addBroker(String id, String description, String[] ips) {
//        Broker broker = new Broker(id, description);
//        for (int i = 0; i < ips.length; i++) {
//            try {
//                broker.addIP(com.isi.csvr.trading.IPSettings.getSharedInstance().getSSOIP(0).getIP());
//            } catch (Exception e) {
//                broker.addIP("");
//            }
////            broker.addIP(ips[i]);
//        }
//        brokers.put(id, broker);
//        broker = null;
//    }
    public void addBroker(String id, String description, ArrayList<String> ipList, short type) {
        Broker broker = new Broker(id, description, type);
        if (ipList.size() != 0) {
            if (ipList.get(0).startsWith("http://") || ipList.get(0).startsWith("https://")) {
                broker.setType(MW_DIRECT_LOGIN);
            }
        }
        for (int i = 0; i < ipList.size(); i++) {
            try {
                if (Settings.isSingleSignOnMode()) {
                    broker.addIP(com.isi.csvr.trading.IPSettings.getSharedInstance().getSSOIP(0).getIP());
                } else {
                    broker.addIP(ipList.get(i));
                }
            } catch (Exception e) {
                broker.addIP(ipList.get(i));
            }
//            broker.addIP(ips[i]);
        }
        brokers.put(id, broker);
        broker = null;
    }

    public int getBrokerCount() {
        return brokers.size();
    }

    public Broker getFirstBroker() {
        try {
            Enumeration elements = brokers.elements();
            return (Broker) elements.nextElement();
        } catch (Exception e) {
            return null;
        }
    }

    public Broker getBroker(String id) {
        return (Broker) brokers.get(id);
    }

    public String getBrokerName(String id) {
        try {
            return ((Broker) brokers.get(id)).getDecription();
        } catch (Exception e) {
            return "NA";
        }
    }

    public String getSelectedBrokerID() {
        return selectedBrokerID;
    }

    public Broker getSelectedBroker() {
        return getBroker(selectedBrokerID);
    }

    public void setSelectedBroker(String selectedBroker) {
        this.selectedBrokerID = selectedBroker;
    }

    public Enumeration getBrokers() {
        return brokers.elements();
    }
}
