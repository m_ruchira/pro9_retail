package com.isi.csvr.trading.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Oct 6, 2008
 * Time: 9:55:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarginSymbolRecord {
    private String exchange;
    private String symbol;
    private int instrumentType = 0;
    private double marginBuypct = 0;
    private double marginDayBuypct = 0;


    public MarginSymbolRecord() {

    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    public double getMarginBuypct() {
        if (marginBuypct == 0) {
            return 1;
        } else {
            return marginBuypct;
        }
    }

    public void setMarginBuypct(double marginBuypct) {
        this.marginBuypct = marginBuypct / 100;
    }

    public double getMarginDayBuypct() {
        if (marginDayBuypct == 0) {
            return 1;
        } else {

            return marginDayBuypct;
        }
    }

    public void setMarginDayBuypct(double marginDayBuypct) {
        this.marginDayBuypct = marginDayBuypct / 100;
    }
}
