package com.isi.csvr.trading.connection;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.trading.BrokerConfig;
import com.isi.csvr.trading.shared.TradingShared;
import com.mubasher.win32.Win32IdleTime;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 3, 2004
 * Time: 2:08:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class TradingConnectionNotifier {

    private static TradingConnectionNotifier self;
    public Win32IdleTime win32IdleTime;
    private Vector listeners;

    private TradingConnectionNotifier() {
        listeners = new Vector(10, 5);
    }

    public synchronized static TradingConnectionNotifier getInstance() {
        if (self == null) {
            self = new TradingConnectionNotifier();
        }
        return self;
    }

    public synchronized void addConnectionListener(Object listener) {
        listeners.addElement(listener);
    }

    public synchronized void removeConnectionListener(Object listener) {
        listeners.remove(listener);
    }

    public synchronized void fireTWConnected() {

        try {
            if (BrokerConfig.getSharedInstance().isLoggedInUserShown()) {
                TradingShared.TRADE_USER_STATUS = Language.getString("TRADEUSER_MESSAGE").replaceAll("\\[TRADE_USER\\]", TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < listeners.size(); i++) {
            try {
                TradingConnectionListener listener = (TradingConnectionListener) listeners.get(i);
                listener.tradeServerConnected();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Settings.getTradingTimeoutMode() != 0) {    //0=never disconet
            win32IdleTime = new Win32IdleTime();
            win32IdleTime.addWin32IdleTimeListener(Client.getInstance());
            win32IdleTime.start(Settings.getTradingTimeoutMode());
        }
    }

    public synchronized void fireSecondaryPathConnected() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                TradingConnectionListener listener = (TradingConnectionListener) listeners.get(i);
                listener.tradeSecondaryPathConnected();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void fireTWDisconnected() {

        TradingShared.TRADE_USER_STATUS = "";
        for (int i = 0; i < listeners.size(); i++) {
            try {
                TradingConnectionListener listener = (TradingConnectionListener) listeners.get(i);
                listener.tradeServerDisconnected();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            win32IdleTime.stop();
            win32IdleTime = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}


