package com.isi.csvr.trading.ui;


import com.isi.csvr.Client;
import com.isi.csvr.RuleGeneratorInterface;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Sep 13, 2006
 * Time: 12:12:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class RuleGenerator extends InternalFrame implements KeyListener, ActionListener, FocusListener,
        Themeable, RuleGeneratorInterface, DateSelectedListener, MouseListener {

    public static int DEFAULT = 0;
    public static int LAST = 1;
    public static int DOUBLE_LAST = 2;
    public static int BID_ASK = 3;
    public static int DOUBLE_BID_ASK = 4;
    public static int LOST_OR_BID_ASK = 5;
    public static int MID_POINT = 6;
    public static int GREATER_THAN_OR_EQ = 1;
    public static int LESS_THAN_OR_EQ = 2;
    public static int EQUAL = 0;
    public static int EXPIRY_DAY = 0;
    public static int EXPIRY_WEEK = 1;
    public static int EXPIRY_MONTH = 2;
    public static int EXPIRY_DATE = 3;
    public final int TYPE_NONE = 0;
    public final int TYPE_VALUE = 1;
    public final int TYPE_TRAILER = 2;
    JPanel topPanel;
    JPanel middlePanel;
    JPanel bottomPanel;
    JPanel btnPanel;
    TWButton btnClose;
    TWButton btnOk;
    TWTextField txtmiddleValue;
    TWTextField txtbottomValue;
    TWTextField txtbottomTrailer;
    JLabel txtDescription;
    TWComboBox cmbMethod;
    TWComboBox cmbOperations;
    TWComboBox cmbExpirary;
    TWTextField txtSymbol;
    TWButton btnSelectSymbol;
    TWMenuItem mnuDay;
    TWMenuItem mnuWeek;
    TWMenuItem mnuMonth;
    TWMenuItem mnuGTD;
    String symbol = "";
    int instrument = -1;
    String exchange;
    Stock selectedStock;
    private JPopupMenu datePopup;
    private ArrayList<TWComboItem> methods;
    private ArrayList<TWComboItem> operations;
    private ArrayList<TWComboItem> expirys;
    private boolean keyTyped;
    private ArrayList<String> validatedSymbols;
    private TransactionDialog parent;
    private JPanel cmbGoodTills;
    private JLabel txtGoodTill;
    private TWButton btnShowDays;
    private DatePicker datePicker;
    private String rule;
    private String expiryDate;

    public RuleGenerator(TransactionDialog parent, String symbol, String exchange, int instrument) {
        super();
        this.symbol = symbol;
        this.instrument = instrument;
        this.exchange = exchange;
        this.parent = parent;
        this.setTitle(Language.getString("RULE_GENERATOR"));
        createUI();
        Theme.registerComponent(this);
        Client.getInstance().getDesktop().add(this);
        setModel(parent);
//        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setResizable(true);
//        setClosable(true);
        setIconifiable(false);
        doInternalvalidate(SharedMethods.getKey(exchange, symbol, instrument));
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        GUISettings.applyOrientation(getContentPane());

    }

    public void dispose() {
        System.out.println("dispose in Rule Gen");
        this.setVisible(false);
        setModelStatus(false);
        parent = null;
        super.dispose();
    }

    private void showCalendar() {
        Point location = new Point(0, 0);
        System.out.println("show calender");

        datePopup.setLightWeightPopupEnabled(false);
        datePopup.setOpaque(true);
        //datePopup.show(cmbGoodTills, 200,200);


        datePopup.show(cmbGoodTills, (int) location.getX(), (int) (location.getY()) + cmbGoodTills.getBounds().height);
        //System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjjjj"+(int) location.getX()+"jjfjdfjd"+((int) (location.getY()) + cmbGoodTills.getBounds().height));
        //System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkk"+datePopup.isVisible());
    }

    public void createUI() {
        this.setSize(350, 400);
        this.setPreferredSize(new Dimension(350, 400));
//        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"80", "110", "110", "30"}, 7, 7));
        Border border = BorderFactory.createEtchedBorder(); // Theme.getColor("RULE_BORDER_COLOR"));
        Border topTitileborder = BorderFactory.createTitledBorder(border, Language.getString("CONTACT_INFO"));
        Border midTitileborder = BorderFactory.createTitledBorder(border, Language.getString("TRIGGER"));
        Border botTitileborder = BorderFactory.createTitledBorder(border, Language.getString("STOP_LOSS"));

        validatedSymbols = new ArrayList<String>();

        topPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"25", "25"}));
        topPanel.setBorder(topTitileborder);
        JLabel lblUnderlying = new JLabel(Language.getString("UNDERLYING"));
        JLabel lblDescription = new JLabel(Language.getString("DESCRIPTION"));
        txtDescription = new JLabel("");
        topPanel.add(lblUnderlying);
        topPanel.add(getSymbolPanel());
        topPanel.add(lblDescription);
        topPanel.add(txtDescription);

        middlePanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"20", "22", "20"}, 2, 2));
        middlePanel.setBorder(midTitileborder);
        JLabel lblMethod = new JLabel(Language.getString("METHOD"));
        JLabel lblmidValue = new JLabel(Language.getString("WHEN_VALUE"));
        JLabel lblExpiry = new JLabel(Language.getString("EXPIRY_DATE"));
        methods = new ArrayList<TWComboItem>();
        cmbMethod = new TWComboBox(new TWComboModel(methods));
        cmbMethod.setLightWeightPopupEnabled(false);
        cmbMethod.addMouseListener(this);

        populateMethods();

        operations = new ArrayList<TWComboItem>();
        cmbOperations = new TWComboBox(new TWComboModel(operations));
        cmbOperations.setPreferredSize(new Dimension(50, 20));
        cmbOperations.setLightWeightPopupEnabled(false);
        populateValues();
        ValueFormatter priceFormatter = new ValueFormatter(ValueFormatter.DECIMAL);
//        priceFormatter.addDocumentListener(this);
        txtmiddleValue = new TWTextField();
        txtmiddleValue.setPreferredSize(new Dimension(100, 20));
        txtmiddleValue.setHorizontalAlignment(SwingConstants.RIGHT);
        txtmiddleValue.setDocument(priceFormatter);
        txtmiddleValue.addKeyListener(this);
        JPanel valuePanel = new JPanel(new FlexGridLayout(new String[]{"60", "10", "100%"}, new String[]{"100%"}));
        valuePanel.add(cmbOperations);
        valuePanel.add(new JLabel(""));
        valuePanel.add(txtmiddleValue);

        expirys = new ArrayList<TWComboItem>();
        cmbExpirary = new TWComboBox(new TWComboModel(expirys));
        populateExpiryDates();

        middlePanel.add(lblMethod);
        middlePanel.add(cmbMethod);
        middlePanel.add(lblmidValue);
        middlePanel.add(valuePanel);
        middlePanel.add(lblExpiry);
        middlePanel.add(getDateCombo());

        bottomPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"21", "21", "21"}, 2, 2));
        bottomPanel.setBorder(botTitileborder);
        ButtonGroup buttonGroup = new ButtonGroup();
        JPanel panel8 = new JPanel();
        panel8.setLayout(new FlowLayout());
        // panel8.setBackground(Color.GRAY);

        JRadioButton chkNone = new JRadioButton(Language.getString("NONE"), true);
        chkNone.addActionListener(this);
        chkNone.setActionCommand("NONE");
        chkNone.setPreferredSize(new Dimension(150, 20));
        // chkNone.setBackground(Theme.MENU_GRADIENT1_COLOR);
        panel8.add(chkNone);
        buttonGroup.add(chkNone);
        bottomPanel.add(panel8);
        bottomPanel.add(new JLabel(""));

        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout());
        JRadioButton chkValue = new JRadioButton(Language.getString("VALUE"), false);
        chkValue.addActionListener(this);
        chkValue.setActionCommand("VALUE");
        chkValue.setPreferredSize(new Dimension(150, 20));
        panel2.add(chkValue);
        buttonGroup.add(chkValue);
        bottomPanel.add(panel2);
        ValueFormatter priceFormatter2 = new ValueFormatter(ValueFormatter.DECIMAL);
//        priceFormatter2.addDocumentListener(this);
        txtbottomValue = new TWTextField();
        txtbottomValue.setPreferredSize(new Dimension(150, 20));
        txtbottomValue.setHorizontalAlignment(SwingConstants.RIGHT);
        txtbottomValue.setDocument(priceFormatter2);
        txtbottomValue.addKeyListener(this);
        bottomPanel.add(txtbottomValue);

        JPanel panel3 = new JPanel();
        panel3.setLayout(new FlowLayout());
        JRadioButton chkTrailer = new JRadioButton(Language.getString("TRAILER"), false);
        chkTrailer.addActionListener(this);
        chkTrailer.setActionCommand("TRAILER");
        chkTrailer.setPreferredSize(new Dimension(150, 20));
        panel3.add(chkTrailer);
        buttonGroup.add(chkTrailer);
        bottomPanel.add(panel3);
        ValueFormatter priceFormatter3 = new ValueFormatter(ValueFormatter.DECIMAL);
//        priceFormatter3.addDocumentListener(this);
        txtbottomTrailer = new TWTextField();
        txtbottomTrailer.setPreferredSize(new Dimension(150, 20));
        txtbottomTrailer.setHorizontalAlignment(SwingConstants.RIGHT);
        txtbottomTrailer.setDocument(priceFormatter3);
        txtbottomTrailer.addKeyListener(this);
        bottomPanel.add(txtbottomTrailer);
        revalidateBottomPanel(TYPE_NONE);

        btnPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 5, 5));

        btnOk = new TWButton(Language.getString("OK"));
        btnOk.setPreferredSize(new Dimension(80, 25));
        btnOk.addActionListener(this);

        btnClose = new TWButton(Language.getString("CLOSE"));
        btnClose.setPreferredSize(new Dimension(80, 25));
        btnClose.addActionListener(this);
        btnPanel.add(btnOk);
        btnPanel.add(btnClose);

        datePopup = new JPopupMenu();
        mnuDay = new TWMenuItem(Language.getString("DAY"));
        mnuDay.setActionCommand("DAY");
        mnuDay.addActionListener(this);
        datePopup.add(mnuDay);
        mnuWeek = new TWMenuItem(Language.getString("WEEK"));
        mnuWeek.setActionCommand("WEEK");
        mnuWeek.addActionListener(this);
        datePopup.add(mnuWeek);
        mnuMonth = new TWMenuItem(Language.getString("MONTH"));
        mnuMonth.setActionCommand("MONTH");
        mnuMonth.addActionListener(this);
        datePopup.add(mnuMonth);
        mnuGTD = new TWMenuItem(Language.getString("GTD"));
        mnuGTD.setActionCommand("GTD");
        mnuGTD.addActionListener(this);
        datePopup.add(mnuGTD);


        this.add(topPanel);
        this.add(middlePanel);
        this.add(bottomPanel);
        this.add(btnPanel);
        applyTheme();
    }

    private JPanel getDateCombo() {
        //cmbGoodTills = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        cmbGoodTills = new JPanel(new BorderLayout());
        cmbGoodTills.setBorder(BorderFactory.createEtchedBorder());
        cmbGoodTills.setPreferredSize(new Dimension(170, 20));
        txtGoodTill = new JLabel();
        txtGoodTill.setPreferredSize(new Dimension(100, 16));
        txtGoodTill.setOpaque(false);
        cmbGoodTills.add(txtGoodTill, BorderLayout.WEST);
        DownArrow daw = new DownArrow();

        btnShowDays = new TWButton(new DownArrow());
        btnShowDays.setBorder(null);
        btnShowDays.addActionListener(this);
        btnShowDays.setContentAreaFilled(false);
        btnShowDays.setOpaque(false);
        btnShowDays.setPreferredSize(new Dimension(16, 16));
        cmbGoodTills.add(btnShowDays, BorderLayout.EAST);
        cmbGoodTills.addMouseListener(this);
        //fromDatePanel.addMouseListener(this);

        return cmbGoodTills;
    }


    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }

    private void populateMethods() {
        methods.add(new TWComboItem(DEFAULT, Language.getString("DEFAULT")));
        methods.add(new TWComboItem(LAST, Language.getString("LAST")));
        methods.add(new TWComboItem(DOUBLE_LAST, Language.getString("DOUBLE_LAST")));
//        methods.add(new TWComboItem(BID_ASK, Language.getString("BID_ASK")));
//        methods.add(new TWComboItem(DOUBLE_BID_ASK, Language.getString("DOUBLE_BID_ASK")));
//        methods.add(new TWComboItem(LOST_OR_BID_ASK, Language.getString("LOST_OR_BID_ASK")));
//        methods.add(new TWComboItem(MID_POINT, Language.getString("MID_POINT")));
        cmbMethod.setSelectedIndex(0);
        cmbMethod.updateUI();
    }

    private void populateValues() {
        operations.add(new TWComboItem(GREATER_THAN_OR_EQ, " >= "));
        operations.add(new TWComboItem(LESS_THAN_OR_EQ, " <= "));
        operations.add(new TWComboItem(EQUAL, " = "));
        cmbOperations.setSelectedIndex(0);
        cmbOperations.updateUI();
    }

    public void populateExpiryDates() {
        expirys.add(new TWComboItem(EXPIRY_DAY, Language.getString("EXPIRY_DAY")));
        expirys.add(new TWComboItem(EXPIRY_WEEK, Language.getString("EXPIRY_WEEK")));
        expirys.add(new TWComboItem(EXPIRY_MONTH, Language.getString("EXPIRY_MONTH")));
        cmbExpirary.setSelectedIndex(0);
        cmbExpirary.updateUI();
    }

    public JPanel getSymbolPanel() {
        JPanel symbolsPanel = new JPanel(new BorderLayout());
        //symbolsPanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        //symbolsPanel.setPreferredSize(new Dimension(175, 25));
        symbolsPanel.addMouseListener(this);
        txtSymbol = new TWTextField();
        txtSymbol.setText(symbol);
        txtSymbol.setDocument(new ValueFormatter(ValueFormatter.UPPERCASE, 20L));
        txtSymbol.setText(symbol);
        txtSymbol.setBorder(null);
        txtSymbol.addKeyListener(this);
        txtSymbol.addFocusListener(this);
        symbolsPanel.add(txtSymbol, BorderLayout.CENTER);
//        lblSelectedSymbols.setPreferredSize(new Dimension(150, 20));
//        symbolsPanel.add(lblSelectedSymbols);
        btnSelectSymbol = new TWButton(new DownArrow());
        btnSelectSymbol.setBorder(null);
        symbolsPanel.add(btnSelectSymbol, BorderLayout.EAST);
        btnSelectSymbol.setPreferredSize(new Dimension(20, 20));
        btnSelectSymbol.addActionListener(this);
        btnSelectSymbol.setActionCommand("SS");
        symbolsPanel.setBorder(BorderFactory.createEtchedBorder());
        return symbolsPanel;
    }

    private boolean doInternalvalidate(String symbol) {
        String key = SymbolMaster.getExchangeForSymbol(symbol, false);
        if (key != null) {
            this.exchange = SharedMethods.getExchangeFromKey(key);
            this.symbol = SharedMethods.getSymbolFromKey(key);
            this.instrument = SharedMethods.getInstrumentTypeFromKey(key);
            selectedStock = DataStore.getSharedInstance().getStockObject(key);
            txtSymbol.setText(symbol);
            txtDescription.setText(selectedStock.getLongDescription());
            return true;
        } else {
            selectedStock = null;
            return false;
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        if (e.getSource() == txtSymbol) {
            invalidateStock();
            if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
                doInternalvalidate(txtSymbol.getText());
            } else {
                keyTyped = true;
                txtDescription.setText("");
                System.out.println("keyTyped");
            }
        }
    }

    public void focusGained(FocusEvent e) {
//        System.out.println("focus gained in Rule Gen");
    }

    public void focusLost(FocusEvent e) {
//        System.out.println("focus lost in Rule Gen");
        if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
            if (keyTyped) {
                boolean validated = doInternalvalidate(txtSymbol.getText());
                if (!validated) {
                    // show mwssage
                }
            }
        } else {
            if (keyTyped) {
                invalidateStock();
                validateSymbol(txtSymbol.getText());
            }
        }
    }

    private void validateSymbol(String symbol) {
        String requestID = "RuleGenerator" + ":" + System.currentTimeMillis();
        SendQFactory.addValidateRequest(symbol, requestID, null, 0);
        SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
        System.out.println("Validate request sent");
    }

    private void removeValidatedSymbols() {
        for (String key : validatedSymbols) {
            DataStore.getSharedInstance().removeSymbolRequest(key);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("SS")) {
            searchSymbol();
        } else if (e.getActionCommand().equals("NONE")) {
            revalidateBottomPanel(TYPE_NONE);
        } else if (e.getActionCommand().equals("VALUE")) {
            revalidateBottomPanel(TYPE_VALUE);
        } else if (e.getActionCommand().equals("TRAILER")) {
            revalidateBottomPanel(TYPE_TRAILER);
        } else if (e.getSource() == btnClose) {
            this.setVisible(false);
            setModelStatus(false);
            this.dispose();
        } else if (e.getSource() == btnOk) {
            generateRule();
            this.setVisible(false);
            setModelStatus(false);
        } else if (e.getSource() == cmbGoodTills) {
            showCalendar();
        } else if (e.getSource() == btnShowDays) {
            showCalendar();
        } else if (e.getActionCommand().equals("GTD")) {
            if (datePicker == null) {
                datePicker = new DatePicker(Client.getInstance().getFrame(), true);
            }
            datePicker.getCalendar().addDateSelectedListener(this);
            Point location = new Point(txtGoodTill.getX(),
                    (txtGoodTill.getY()) + txtGoodTill.getBounds().height);
            SwingUtilities.convertPointToScreen(location, txtGoodTill.getParent());
            datePicker.setLocation(location);

            datePicker.setVisible(true);
        } else if (e.getActionCommand().equals("DAY")) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 1);
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            txtGoodTill.setText(TradingShared.formatGoodTill(cal.getTime()));
            cal = null;
        } else if (e.getActionCommand().equals("WEEK")) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 7);
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            txtGoodTill.setText(TradingShared.formatGoodTill(cal.getTime()));
            cal = null;
        } else if (e.getActionCommand().equals("MONTH")) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, 1);
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
            txtGoodTill.setText(TradingShared.formatGoodTill(cal.getTime()));
            cal = null;

        }
    }

    /*public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("SS")) {
            searchSymbol();
        } else if(e.getSource() == btnClose){
            this.setVisible(false);
            this.dispose();
        } else if(e.getSource() == btnOk){
            generateRule();
            dispose();
        }
    }*/

    private void generateRule() {
        StringBuilder ruleBuffer = new StringBuilder();

        if (txtSymbol.getText().trim().equals("")) {
            rule = null;
        } else {
            ruleBuffer.append(txtSymbol.getText().trim().toUpperCase());
            ruleBuffer.append(Meta.DD);
            ruleBuffer.append("DFM");
            ruleBuffer.append(Meta.DD);
            ruleBuffer.append(methods.get(cmbMethod.getSelectedIndex()).getId());
            ruleBuffer.append(Meta.DD);
            ruleBuffer.append(operations.get(cmbOperations.getSelectedIndex()).getId());
            ruleBuffer.append(Meta.DD);
            ruleBuffer.append(txtmiddleValue.getText().trim());
            ruleBuffer.append(Meta.DD);
            ruleBuffer.append("0");

            int days = Integer.parseInt(expirys.get(cmbExpirary.getSelectedIndex()).getId());
            Date date = TradeMethods.getDateFromDays(days);
            expiryDate = TradingShared.formatExpireDate(date.getTime());
            rule = ruleBuffer.toString();
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == cmbMethod) {
            super.mouseClicked(e);
            System.out.println("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
        }
        //To change body of overridden methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == cmbMethod) {
            System.out.println("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");

        }

        //To change body of overridden methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of overridden methods use File | Settings | File Templates.
    }

    private void revalidateBottomPanel(int type) {
        switch (type) {
            case TYPE_NONE:
                txtbottomValue.setEnabled(false);
                txtbottomTrailer.setEnabled(false);
                break;
            case TYPE_VALUE:
                txtbottomValue.setEnabled(true);
                txtbottomTrailer.setEnabled(false);
                break;
            case TYPE_TRAILER:
                txtbottomTrailer.setEnabled(true);
                txtbottomValue.setEnabled(false);
                break;
        }
    }

    private void searchSymbol() {
        try {

            String[] symbols = SharedMethods.searchSymbols(getTitle(), true, true);
            if ((symbols != null) && (!symbols[0].equals(null))) {
                keyTyped = false;
                symbol = SharedMethods.getSymbolFromKey(symbols[0]);
                exchange = SharedMethods.getExchangeFromKey(symbols[0]);
                instrument = SharedMethods.getInstrumentTypeFromKey(symbols[0]);
                selectedStock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
//                lockInputs(false);
                txtSymbol.setText(symbol);
                txtDescription.setText(selectedStock.getLongDescription());
                this.requestFocus();
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void internalFrameClosed(InternalFrameEvent e) {
//        System.out.println("--------------Closing window in Rule Gen");
        this.setVisible(false);
        setModelStatus(false);
        removeValidatedSymbols();
        finalizeWindow();
        System.out.println("--------------Window closed in Rule Gen");
    }

    public void setSymbol(String key) {
        System.out.println("symbol set");
        txtSymbol.setEnabled(true);
        btnSelectSymbol.setEnabled(true);
        symbol = SharedMethods.getSymbolFromKey(key);
        exchange = SharedMethods.getExchangeFromKey(key);
        instrument = SharedMethods.getInstrumentTypeFromKey(key);
        keyTyped = false;
        txtSymbol.setText(symbol);
        txtDescription.setText(DataStore.getSharedInstance().getStockObject(key).getLongDescription());
        validatedSymbols.add(key);
    }

    public void notifyInvalidSymbol(String symbol) {
        invalidateStock();
        txtSymbol.setText("");
        txtDescription.setText("");
        keyTyped = false;
    }

    private void invalidateStock() {
        selectedStock = null;
        symbol = null;
        instrument = -1;
        exchange = null;
        txtDescription.setText("");
    }


    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        datePicker.hide();
        Calendar cal = Calendar.getInstance();
        cal.set(iYear, iMonth, iDay);
//        goodTillStr = TradingShared.formatGoodTill(cal.getTime());
        txtGoodTill.setText(TradingShared.formatGoodTill(cal.getTime()));
        cal = null;
    }

    public String getRule() {
        return rule;
    }

    public String getExpiryDate() {
        return expiryDate;
    }
}
