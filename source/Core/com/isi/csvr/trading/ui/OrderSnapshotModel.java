package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.datastore.ProgramedOrderStore;
import com.isi.csvr.trading.datastore.ProgrammedTrade;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 31, 2004
 * Time: 11:49:54 AM
 */
public class OrderSnapshotModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private String mubasherOrderID;

    public OrderSnapshotModel(String mubasherOrderID) {
        this.mubasherOrderID = mubasherOrderID;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return 4;//super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return 4;
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int iRow, int iCol) {
        Transaction transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderID);

        switch (iCol) {
            case 0:
                switch (iRow) {
                    case 0:
                        return Language.getString("ORDER_NUMBER");
                    case 1:
                        return Language.getString("ESIS_NUMBER");
                    case 2:
                        return Language.getString("ACTION");
                    case 3:
                        return Language.getString("GOOD_TILL");
                }
            case 1:
                switch (iRow) {
                    case 0:
                        return "" + transaction.getOrderID();
                    case 1:
                        return "" + transaction.getESISOrderNo();
                    case 2:
                        return TradingShared.getActionString(transaction.getSide());
                    case 3:
                        return transaction.getExpireDate();
                }
            case 2:
                switch (iRow) {
                    case 0:
                        return Language.getString("ORDER_TYPE");
                    case 1:
                        return Language.getString("PRICE");
                    case 2:
                        return Language.getString("QUANTITY");
                    case 3:
                        return Language.getString("MIN_FILL");
                }
            case 3:
                switch (iRow) {
                    case 0:
                        return TradingShared.getTypeString(transaction.getType());
                    case 1:
                        return "" + transaction.getPrice();
                    case 2:
                        return "" + transaction.getOrderQuantity();
                    case 3:
                        return "" + transaction.getMinQuantity();
                }
        }
        return "DEF";
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        /* switch(super.getViewSettings().getRendererID(iCol))
        {
            case 0:
            case 1:
            case 2:
            case 11:
                return String.class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
                return Number.class;
            case 'B':
                return Boolean.class;
            default:
                return Object.class;
        }*/
        return Object.class;
    }

    public boolean isCellEditable(int row, int col) {
        switch (col) {
            case 11:
                return true;
            default:
                return false;
        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        ProgrammedTrade transaction = ProgramedOrderStore.getSharedInstance().getTransaction(rowIndex);
        switch (columnIndex) {
            case 1:
                transaction.setSelected(((Boolean) value).booleanValue());
                break;
        }
        transaction = null;
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[4];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));   //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("BUY_COLOR"), FIELD_BID_ROW1, Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_BID_FGCOLOR1"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELL_COLOR"), FIELD_ASK_ROW2, Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_ASK_FGCOLOR1"));
        return customizerRecords;
    }
}
