package com.isi.csvr.trading.ui;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Nov 19, 2007
 * Time: 5:20:44 PM
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Nov 19, 2007
 * Time: 5:02:56 PM
 * To change this template use File | Settings | File Templates.
 */

public class CustomizableLable extends JLabel implements Themeable {

    private String name;
    private FontMetrics logingDetailFont;

    public CustomizableLable(String name) {
        super(name);
        this.name = name;
        Theme.registerComponent(this);
    }

    public void applyTheme() {
        this.setBackground(Theme.getColor("LOGGINUSERBACKGROUND_BGCOLOR"));
        this.setForeground(Theme.getColor("LOGINGUSERDETAILS_FGCOLOR"));
    }

}