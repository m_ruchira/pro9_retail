package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.MarginSymbolStore;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Oct 29, 2008
 * Time: 5:49:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarginableSymbolWindow extends InternalFrame implements TradingConnectionListener, ActionListener {
    public static MarginableSymbolWindow self;
    private JLabel exgLabel;
    private TWComboBox exgCombo;
    private TWComboModel exgCmbModel;
    private Table marginableTable;
    private ViewSetting oSetting;
    private JPanel upperPanel;
    private ArrayList<TWComboItem> exgArray;
    private MarginableSymbolModel model;
    private String SelectedExchange;


    private MarginableSymbolWindow() {
        oSetting = ViewSettingsManager.getSummaryView("MARGINABLE_SYMBOLS");
        exgLabel = new JLabel(Language.getString("EXCHANGE"));
        exgArray = new ArrayList<TWComboItem>();
        exgCmbModel = new TWComboModel(exgArray);
        exgCombo = new TWComboBox(exgCmbModel);
        marginableTable = new Table();
        upperPanel = new JPanel();
        createUI();
        populateExchangeCombo();


    }

    public static MarginableSymbolWindow getSharedInstance() {
        if (self == null) {
            self = new MarginableSymbolWindow();
        }
        return self;
    }

    public void createUI() {
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%"}, 0, 0));
        this.add(createUpperPanel());
        this.add(createTablePanel());
        setClosable(true);
        setSize(650, 350);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        setTitle(oSetting.getCaption());
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        GUISettings.applyOrientation(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        setLocationRelativeTo(Client.getInstance().getDesktop());

    }

    private JPanel createUpperPanel() {
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100", "150", "100%"}, new String[]{"100%"}, 5, 5));
        upperPanel.add(exgLabel);
        upperPanel.add(exgCombo);
        upperPanel.add(new JLabel());
        exgCombo.addActionListener(this);
        return upperPanel;

    }

    private Table createTablePanel() {
        marginableTable.setPreferredSize(new Dimension(650, 290));
        model = new MarginableSymbolModel();
        marginableTable.setWindowType(ViewSettingsManager.IPO_SUBSCRIPTION);
        model.setViewSettings(oSetting);
        marginableTable.setModel(model);
        model.setTable(marginableTable);
        ((SmartTable) marginableTable.getTable()).setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
        model.applySettings();
        model.updateGUI();
        oSetting.setParent(this);
        setLayer(GUISettings.TOP_LAYER);
        this.setTable(marginableTable);
        super.setTable(marginableTable);

        return marginableTable;
    }

    private void populateExchangeCombo() {
        SharedMethods.populateExchangesForTWCombo(exgArray, true);

        Enumeration enu = TradingShared.getTrader().getExchanges();
//        while (enu.hasMoreElements()) {
//            String exchange = (String) enu.nextElement();
//            Exchange exg = ExchangeStore.getSharedInstance().getExchange(exchange);
//            TWComboItem item = new TWComboItem(exg.getSymbol(), exg.getDescription());
//            exgArray.add(item);
//
//        }
        if (exgArray.size() > 1) {
            exgArray.add(0, new TWComboItem(Language.getString("ALL"), Language.getString("ALL")));
        }
        exgCombo.setSelectedIndex(0);
        exgCombo.updateUI();
    }

    public void tradeServerConnected() {
        exgArray.clear();
        populateExchangeCombo();
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        MarginSymbolStore.getSharedInstance().clearAllstores();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(exgCombo)) {
            SelectedExchange = ((TWComboItem) exgCombo.getSelectedItem()).getId();
            model.setExchange(SelectedExchange);

        }

    }
}
