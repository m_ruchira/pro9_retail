package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.CashLogSearchStore;
import com.isi.csvr.trading.datastore.OrderSearchStore;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TraderProfileDataListener;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Jan 9, 2008
 * Time: 2:41:08 PM
 */
public class CashLogSearchWindow extends InternalFrame implements TradingConnectionListener, ActionListener,
        Themeable, TraderProfileDataListener {

    private static boolean searchInProgress;
    private static CashLogSearchWindow self = null;
    private TWComboBox cmbPortfolios;
    private TWComboBox cmbTypes;
    private DateCombo fromDateCombo;
    private DateCombo toDateCombo;
    private TWButton btnClose;
    private TWButton btnOK;
    private JButton btnNext;
    private JButton btnPrev;
    private ViewSetting oSetting;
    private Table blotterTable;
    private CashLogModel blotterModel;

    public CashLogSearchWindow(ViewSetting oSetting) {
        super(oSetting.getCaption(), oSetting);
        this.oSetting = oSetting;
        oSetting.setParent(this);
        createUI();
        self = this;
    }

    public static boolean isSearchInProgress() {
        return searchInProgress;
    }

    public static void setSearchInProgress(boolean status) {
        searchInProgress = status;
        if (!searchInProgress) {
            self.btnPrev.setEnabled(CashLogSearchStore.getSharedInstance().canGoPrev());
            self.btnNext.setEnabled(CashLogSearchStore.getSharedInstance().canGoNext());
        }
    }

    private void createUI() {
        JPanel configPanel = new JPanel();
        String[] configWidths = {"70", "150", "30", "70", "150", "10", "80"};
        String[] configHeights = {"20", "20"};
        FlexGridLayout configLayout = new FlexGridLayout(configWidths, configHeights, 5, 5);
        configPanel.setLayout(configLayout);
        btnOK = new TWButton(Language.getString("SEARCH"));
        btnOK.addActionListener(this);
        btnOK.setActionCommand("OK");
        btnClose = new TWButton(Language.getString("CLOSE"));
        btnClose.addActionListener(this);
        btnClose.setActionCommand("CANCEL");

        JLabel lblPortfolios = new JLabel(Language.getString("PORTFOLIO"));
        cmbPortfolios = new TWComboBox();
        configPanel.add(lblPortfolios);
        configPanel.add(cmbPortfolios);
        configPanel.add(new JLabel(""));

        JLabel lblTypes = new JLabel(Language.getString("CASHLOG_TYPE"));
        cmbTypes = new TWComboBox();
        configPanel.add(lblTypes);
        configPanel.add(cmbTypes);
        configPanel.add(new JLabel(""));
        configPanel.add(btnOK);

        JLabel lblFromDate = new JLabel(Language.getString("FROM"));
        fromDateCombo = new DateCombo(Client.getInstance().getFrame());
        configPanel.add(lblFromDate);
        configPanel.add(fromDateCombo);
        configPanel.add(new JLabel(""));

        JLabel lblToDate = new JLabel(Language.getString("TO"));
        toDateCombo = new DateCombo(Client.getInstance().getFrame());
        configPanel.add(lblToDate);
        configPanel.add(toDateCombo);
        configPanel.add(new JLabel(""));
        configPanel.add(btnClose);

//        configPanel.add(new JLabel(""));
//        configPanel.add(new JLabel(""));
//        configPanel.add(new JLabel(""));
//
//        configPanel.add(new JLabel(""));
//
//        JPanel buttonPanel = new JPanel();
//        BoxLayout buttonLayout = new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS);
//        buttonPanel.setLayout(buttonLayout);
//
//        buttonPanel.add(btnOK);
//        buttonPanel.add(Box.createHorizontalGlue());
//        buttonPanel.add(btnClose);
//        configPanel.add(buttonPanel);

        createTable();
        blotterTable.setNorthPanel(configPanel);
        populatePortfolios();
        populateTypes();
        createButtonPanel();
        //setSize(600, 400);
        Client.getInstance().getDesktop().add(this);
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        Calendar cal = Calendar.getInstance();
        fromDateCombo.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DATE));
        toDateCombo.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DATE));
        //setLocationRelativeTo(Client.getInstance().getDesktop());
        setResizable(true);
        setClosable(true);
        setIconifiable(true);
        hideTitleBarMenu();
        super.applySettings();
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        setVisible(oSetting.isVisible());
    }

    public void createButtonPanel() {
        JPanel buttonPanel = new JPanel(new BorderLayout(5, 5));
        btnPrev = new JButton("  " + Language.getString("PREVIOUS") + "  ");
        btnPrev.setBorder(BorderFactory.createEmptyBorder());
        btnNext = new JButton("  " + Language.getString("NEXT") + "  ");
        btnNext.setBorder(BorderFactory.createEmptyBorder());
        buttonPanel.add(btnPrev, BorderLayout.WEST);
        buttonPanel.add(btnNext, BorderLayout.EAST);
        btnPrev.addActionListener(this);
        btnNext.addActionListener(this);
        btnPrev.setEnabled(false);
        btnNext.setEnabled(false);
        //getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        blotterTable.setSouthPanel(buttonPanel);
    }

    private void createTable() {
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");

        blotterTable = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (isSearchInProgress()) {
                    FontMetrics fontMetrics = blotterTable.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                            (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
                            (getHeight() - icon.getImage().getHeight(this)) / 2, this);
                    fontMetrics = null;
                }
            }
        };
        blotterTable.setThreadID(Constants.ThreadTypes.TRADING);
        //final FontMetrics fontMetrics = blotterTable.getFontMetrics(getFont());
        blotterTable.setPreferredSize(new Dimension(500, 100));
        blotterModel = new CashLogModel();
        try {
            blotterTable.getPopup().setMenuItem(getCopyMenu());
            blotterTable.getPopup().setMenuItem(getCopyWithHeadMenu());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        blotterTable.setWindowType(ViewSettingsManager.CASHLOG_SEARCH);
        blotterModel.setViewSettings(oSetting);
        blotterTable.setModel(blotterModel);
        blotterModel.setTable(blotterTable);
        ((SmartTable) blotterTable.getTable()).setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
        blotterModel.applyColumnSettings();
        blotterModel.updateGUI();

        blotterTable.getPopup().hideTitleBarMenu();

        oSetting.setParent(this);
        //super.applySettings();
        setLayer(GUISettings.TOP_LAYER);
        //getContentPane().add(blotterTable, BorderLayout.CENTER);
        getContentPane().add(blotterTable);
        super.setTable(blotterTable);

        blotterTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    int row = blotterTable.getTable().getSelectedRow();
                    String clOrdId = (String) blotterTable.getTable().getModel().getValueAt(row, 3); // get the clOrdId
                    Transaction order = OrderSearchStore.getSharedInstance().searhTransactionByClOrderID(clOrdId);
                    TradeMethods.getSharedInstance().showDetailQuoteOrder(order);
                    order = null;
                } else {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        int row = blotterTable.getTable().getSelectedRow();
                        int col = blotterTable.getTable().convertColumnIndexToModel(blotterTable.getTable().getSelectedColumn());
                        if (col == 6) {
                            String orderID = (String) blotterTable.getTable().getModel().getValueAt(row, 3);
                            String mubasherOrderID = (String) blotterTable.getTable().getModel().getValueAt(row, 16);
                            Transaction transaction = OrderSearchStore.getSharedInstance().searhTransactionByClOrderID(orderID);
                            if (transaction == null) {
                                transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderID);
                            }
                            //Transaction transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(orderID);
                            if ((transaction != null) && (transaction.getStatus() == TradeMeta.T39_Rejected)) {
                                TradeMethods.getSharedInstance().showRejectReasonFull(transaction);
                                transaction = null;
                            }
                        }
                    }
                }
            }
        });
    }

    private synchronized void populatePortfolios() {
        try {
            if (TradingShared.getTrader() != null) {
                try {
                    cmbPortfolios.removeAllItems();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    try {
                        record = portfolios.get(i);
                        TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());
                        cmbPortfolios.addItem(item);
                        item = null;
                        record = null;
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                cmbPortfolios.updateUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateTypes() {
        try {
            cmbTypes.removeAllItems();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<TWComboItem> items = new ArrayList<TWComboItem>();

        cmbTypes.addItem(new TWComboItem("*", Language.getString("LBL_ALL")));

        items.add(new TWComboItem(TradeMeta.CT_CONCLS, TradingShared.CASHLOG_TYPE_CONCLS));
        items.add(new TWComboItem(TradeMeta.CT_CONOPN, TradingShared.CASHLOG_TYPE_CONOPN));
        items.add(new TWComboItem(TradeMeta.CT_COUFEE, TradingShared.CASHLOG_TYPE_COUFEE));
        items.add(new TWComboItem(TradeMeta.CT_CRPFEE, TradingShared.CASHLOG_TYPE_CRPFEE));
        items.add(new TWComboItem(TradeMeta.CT_DEPOST, TradingShared.CASHLOG_TYPE_DEPOST));
        items.add(new TWComboItem(TradeMeta.CT_MDAFEE, TradingShared.CASHLOG_TYPE_MDAFEE));
        items.add(new TWComboItem(TradeMeta.CT_MISFEE, TradingShared.CASHLOG_TYPE_MISFEE));
        items.add(new TWComboItem(TradeMeta.CT_MKTMKT, TradingShared.CASHLOG_TYPE_MKTMKT));
        items.add(new TWComboItem(TradeMeta.CT_REFUND, TradingShared.CASHLOG_TYPE_REFUND));
        items.add(new TWComboItem(TradeMeta.CT_REVBUY, TradingShared.CASHLOG_TYPE_REVBUY));
        items.add(new TWComboItem(TradeMeta.CT_REVSEL, TradingShared.CASHLOG_TYPE_REVSEL));
        items.add(new TWComboItem(TradeMeta.CT_STLBUY, TradingShared.CASHLOG_TYPE_STLBUY));
        items.add(new TWComboItem(TradeMeta.CT_STLSEL, TradingShared.CASHLOG_TYPE_STLSEL));
        items.add(new TWComboItem(TradeMeta.CT_TRFFEE, TradingShared.CASHLOG_TYPE_TRFFEE));
        items.add(new TWComboItem(TradeMeta.CT_TRNSFR, TradingShared.CASHLOG_TYPE_TRNSFR));
        items.add(new TWComboItem(TradeMeta.CT_WIPFEE, TradingShared.CASHLOG_TYPE_WIPFEE));
        items.add(new TWComboItem(TradeMeta.CT_WITHDR, TradingShared.CASHLOG_TYPE_WITHDR));

        Collections.sort(items);
        for (TWComboItem item : items) {
            cmbTypes.addItem(item);
        }
        items = null;
    }

    private void initSearch() {
        CashLogSearchStore.getSharedInstance().init();

    }

    private void sendRequest() {
        try {
            String portfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
            String type = ((TWComboItem) cmbTypes.getSelectedItem()).getId();
            String from = fromDateCombo.getDateString();
            String to = toDateCombo.getDateString();
            if (Integer.parseInt(to) < Integer.parseInt(from)) {
                new ShowMessage(Language.getString("INVALID_DATE_RANGE"), "E");
                return;
            }
            setSearchInProgress(true);
            CashLogSearchStore.getSharedInstance().sendNewRequest(portfolio, type, from, to);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void enableButtons() {
        btnOK.setEnabled(true);
        setSearchInProgress(false);
        blotterTable.getTable().updateUI();
    }

    public void tradeServerConnected() {
        populatePortfolios();
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        setSearchInProgress(false);
        CashLogSearchStore.getSharedInstance().clear();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        toDateCombo.applyTheme();
        fromDateCombo.applyTheme();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnOK) {
            if (TradingShared.isReadyForTrading()) {
                new Thread("Order Search Disable Thread") {
                    public void run() {
                        initSearch();
                        sendRequest();
                        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "OrderSearch");
                        btnOK.setEnabled(false);
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                        btnOK.setEnabled(true);
                    }
                }.start();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "CashLogSearch");
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } else if (e.getSource() == btnClose) {
            enableButtons();
            setVisible(false);
        } else if (e.getSource() == cmbPortfolios) {

        } else if (e.getSource() == btnPrev) {
            if (TradingShared.isReadyForTrading()) {
                CashLogSearchStore.getSharedInstance().clear();
                CashLogSearchStore.getSharedInstance().sendPreviousRequest();
                setSearchInProgress(true);
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } else if (e.getSource() == btnNext) {
            if (TradingShared.isReadyForTrading()) {
                CashLogSearchStore.getSharedInstance().clear();
                CashLogSearchStore.getSharedInstance().sendNextRequest();
                setSearchInProgress(true);
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void accountDataChanged(String accountID) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios();
    }

    public TWMenuItem getCopyMenu() {
        TWMenuItem mnuCopy = new TWMenuItem(Language.getString("COPY"), "copy.gif");
        mnuCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
        mnuCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    blotterModel.copyCells(blotterTable, false);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCopy;
    }

    public TWMenuItem getCopyWithHeadMenu() {
        TWMenuItem mnuCopyH = new TWMenuItem(Language.getString("COPY_WITH_HEAD"), "copywh.gif");
        mnuCopyH.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        mnuCopyH.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    blotterModel.copyCells(blotterTable, true);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCopyH;
    }
}
