package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.NonEscapable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 9, 2004
 * Time: 2:19:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class Level2AuthWait extends JDialog implements WindowListener, TradingConnectionListener, NonEscapable {

    public static boolean ACTIVE = false;
    public static Level2AuthWait self = null;
    private JLabel message;
    private JLabel icon;

    public Level2AuthWait() {
        super(Client.getInstance().getFrame(), true);
        super.setBackground(Color.WHITE);

        message = new JLabel(Language.getString("AUTHENTICATING"));
        icon = new JLabel();
        message.setHorizontalAlignment(SwingConstants.CENTER);
        icon.setHorizontalAlignment(SwingConstants.CENTER);
        message.setVerticalAlignment(SwingConstants.CENTER);
        icon.setVerticalAlignment(SwingConstants.CENTER);
        icon.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/busy.gif"));
        message.setOpaque(true);
        icon.setOpaque(true);
        message.setBackground(Color.WHITE);
        icon.setBackground(Color.WHITE);
//        setBorder(BorderFactory.createLineBorder(Color.black));
        JLabel l1 = new JLabel();
        l1.setOpaque(true);
        JLabel l2 = new JLabel();
        l2.setOpaque(true);
        l1.setBackground(Color.WHITE);
        l2.setBackground(Color.WHITE);
        getContentPane().setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"15%", "35%", "35%", "15%"}, 0, 0));
        getContentPane().add(l1);
        getContentPane().add(icon);
        getContentPane().add(message);
        getContentPane().add(l2);
        addWindowListener(this);
        setSize(200, 100);
        setLocationRelativeTo(Client.getInstance().getFrame());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);
        setUndecorated(true);
        ACTIVE = true;
    }

    public static synchronized Level2AuthWait getSharedInstance() {
        if (self == null) {
            self = new Level2AuthWait();
        }
        return self;
    }

    public void show() {
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        super.show();
    }

    public void showWithBlocking() {
        show();
    }

    public void showWithNoneBlocking() {
        new Thread("level2 Authentication") {
            public void run() {
                show();
            }
        }.start();
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
        ACTIVE = false;
        TradingConnectionNotifier.getInstance().removeConnectionListener(this);
    }

    public void windowClosing(WindowEvent e) {
        ACTIVE = false;
    }

    public void windowDeactivated(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowOpened(WindowEvent e) {

    }

    public void tradeServerConnected() {
        ACTIVE = false;
        dispose();
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        ACTIVE = false;
        dispose();
    }
}
