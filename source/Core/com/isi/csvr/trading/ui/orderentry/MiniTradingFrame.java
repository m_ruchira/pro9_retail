package com.isi.csvr.trading.ui.orderentry;

import com.isi.csvr.Client;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWComboItem;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Dec 2, 2009
 * Time: 1:50:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class MiniTradingFrame extends InternalFrame implements ApplicationListener, InternalFrameListener {
    ViewSetting oViewSetting;
    ViewSettingsManager g_oViewSettings;
    private MiniTradingCorePanel tradePanel;
    private String selectedKey;
    private String selectedTimeStamp;
    private String miniTradeParams;

    private String portfolio;
    private String custodian;
    private String exchange;
    private String symbol;
    private String side;
    private char type;
    private String price;
    private String quantity;

    private String existingKey;


    public MiniTradingFrame(String key, String timeStamp) {
        String name = SharedMethods.getSymbolFromKey(key);
        if (name != null && (!name.trim().equalsIgnoreCase("null"))) {
            name = " - " + name;
        } else {
            name = "";
        }
        setTitle(Language.getString("MINI_TRADE") + name);
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        this.selectedKey = key;
        this.selectedTimeStamp = timeStamp;
        this.existingKey = key;


        createUI();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Application.getInstance().addApplicationListener(this);

    }

    private void createUI() {
        if (oViewSetting == null) {
            oViewSetting = g_oViewSettings.getMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
                    + "|" + selectedKey + ":" + selectedTimeStamp);
            if (oViewSetting != null) {
                this.miniTradeParams = oViewSetting.getProperty(ViewConstants.MINI_TRADE_PARAM);
            }
        }
        if (oViewSetting == null) {
            oViewSetting = g_oViewSettings.getSymbolView("MINI_TRADE_VIEW");
            oViewSetting = oViewSetting.getObject(); // clone the object
            SharedMethods.applyCustomViewSetting(oViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
            oViewSetting.setID(System.currentTimeMillis() + "");
            selectedTimeStamp = System.currentTimeMillis() + "";
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey + ":" + selectedTimeStamp);
            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            g_oViewSettings.putMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
                    + "|" + selectedKey + ":" + selectedTimeStamp, oViewSetting);

        }

        tradePanel = new MiniTradingCorePanel(selectedKey, miniTradeParams, this);
        this.setResizable(false);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setDetachable(true);
        this.setIconifiable(true);
        this.setLocation(oViewSetting.getLocation());
        if (!TradingShared.isBookKeepersAvailable()) {
            this.setSize(300, 175);
        } else {
            this.setSize(300, 200);
        }
        this.setLayer(GUISettings.TOP_LAYER);
//        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 5, 5, true, true));
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));

        this.getContentPane().add(tradePanel);
        Client.getInstance().oTopDesktop.add(this);
        this.hideTitleBarMenu();
//        setVisible(true);

//        Client.getInstance().getDesktop().add(this);
    }

    public String getMiniTradeParams() {
        try {
            this.portfolio = ((TWComboItem) tradePanel.getComboPorfolio().getSelectedItem()).getId();
        } catch (Exception e) {
            this.portfolio = "";
        }

        try {
            this.custodian = ((TWComboItem) tradePanel.getComboCustodian().getSelectedItem()).getId();
        } catch (Exception e) {
            this.custodian = "";
        }

        try {
//            this.exchange = ((TWComboItem) tradePanel.getComboExchange().getSelectedItem()).getId();
            this.exchange = tradePanel.getComboExchange().getText();

        } catch (Exception e) {
            this.exchange = "";
        }

        try {
            this.symbol = tradePanel.getTextSymbol().getText();
        } catch (Exception e) {
            this.symbol = "";
        }

        try {
            this.side = ((TWComboItem) tradePanel.getComboSide().getSelectedItem()).getId();
        } catch (Exception e) {
            this.side = "";
        }

        try {
            this.type = ((TWComboItem) tradePanel.getComboType().getSelectedItem()).getId().charAt(0);
        } catch (Exception e) {
            this.type = '1';
        }

        try {
            this.price = tradePanel.getTextPrice().getText();
        } catch (Exception e) {
            this.price = "";
        }

        try {
            this.quantity = tradePanel.getTextQty().getText();
        } catch (Exception e) {
            this.quantity = "";
        }

        this.miniTradeParams = portfolio + ":" +
                custodian + ":" +
                exchange + ":" +
                symbol + ":" +
                side + ":" +
                type + ":" +
                price + ":" +
                quantity + ":" + "0";

        return miniTradeParams;
    }

    public void setMiniTradeParams(String miniTradeParams) {

    }

    public void workspaceWillSave() {
        try {
            oViewSetting.putProperty(ViewConstants.MINI_TRADE_PARAM, getMiniTradeParams());
        } catch (Exception e) {
            oViewSetting.putProperty(ViewConstants.MINI_TRADE_PARAM, "");
        }

        try {
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey + ":" + selectedTimeStamp);
        } catch (Exception e) {
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
        }
        oViewSetting.setParent(this);

//        oViewSetting = g_oViewSettings.getMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
//                    + "|" + existingKey + ":" + selectedTimeStamp);

//        g_oViewSettings.removeMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
//                + "|" + existingKey + ":" + selectedTimeStamp);
        if (!existingKey.equals(selectedKey)) {
            g_oViewSettings.removeMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
                    + "|" + existingKey + ":" + selectedTimeStamp);
            g_oViewSettings.putMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
                    + "|" + selectedKey + ":" + selectedTimeStamp, oViewSetting);
        }
    }

    public void internalFrameClosing(InternalFrameEvent e) {
//         g_oViewSettings.removeMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
//                + "|" + selectedKey + ":" + selectedTimeStamp);
        g_oViewSettings.removeMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
                + "|" + existingKey + ":" + selectedTimeStamp);
        this.dispose();
    }


    public void setFocus() {
        tradePanel.setFocus();
    }

    public ViewSetting getViewSetting() {
        return oViewSetting;
    }

    public void resetSymbolDetails(String key) {
        this.selectedKey = key;
        setTitle(Language.getString("MINI_TRADE") + "-" + SharedMethods.getSymbolFromKey(selectedKey));
        tradePanel.resetSymbolDetails(key);
    }

    public void removeExistingFrame() {

    }


}
