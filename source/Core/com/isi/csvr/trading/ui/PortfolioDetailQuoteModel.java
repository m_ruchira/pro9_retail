package com.isi.csvr.trading.ui;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.DetailQuote;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.ValuationModel;
import com.isi.csvr.trading.portfolio.ValuationRecord;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 8, 2005
 * Time: 12:34:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioDetailQuoteModel extends CommonTable implements DetailQuote, TableModel, CommonTableInterface {

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    //private String symbol;
    private long timeOffset;
    private TradePortfolios tradePortfolios;
    private ArrayList dataStore;
    private ValuationModel valuationModel;
    private int selectedRow;

    /**
     * Constructor
     */
    public PortfolioDetailQuoteModel(int selectedRow, TradePortfolios tradeportfolios) {

        tradePortfolios = tradeportfolios;
        this.dataStore = tradeportfolios.getValuationList();
        //this.valuationModel =valuationModel;
        this.selectedRow = selectedRow;
    }

    /*public void setSymbol(String symbolIn) {
        symbol = symbolIn;
    }*/

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 4;
    }

    public int getRowCount() {
        return 8;
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            ValuationRecord record = (ValuationRecord) dataStore.get(selectedRow);
            if (record == null)
                return ""; //null;
            Stock stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
            if (stock == null)
                return "";

            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(stock.getExchange());

            switch (iRow) {
                case -1:
                    return SharedMethods.getDecimalPlaces(stock.getExchange(), stock.getSymbolCode(), stock.getInstrumentType());
//                    return SharedMethods.getCurrencyDecimalPlaces(stock.getExchange(), stock.getSymbolCode(), stock.getInstrumentType());
                case 0:
                    switch (iCol) {
                        case 0:
                            return Language.getString("SYMBOL");
                        case 1:
                            return PortfolioInterface.getSymbol(stock);
                        case 2:
                            return Language.getString("HOLDING");
                        case 3:
                            return longTrasferObject.setValue(record.getOwned(stock.getInstrumentType()));
                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CURRENCY");
                        case 1:
                            if (PortfolioInterface.getCurrency(stock) == null)
                                return "";
                            else
                                return "" + PortfolioInterface.getCurrency(stock);
                        case 2:
                            return Language.getString("SELL_PENDING");
                        case 3:
                            return longTrasferObject.setValue(record.getSellPending());
                    }
                case 2:
                    switch (iCol) {
                        case 0:
                            return Language.getString("AVG_COST");
                        case 1:
                            return doubleTransferObject.setValue(convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), false, record.getAvgCost(), record.getPortfolioID()));
                        case 2:
                            return Language.getString("BUY_PENDING");
                        case 3:
                            return longTrasferObject.setValue(record.getBuyPending());
                    }
                case 3:
                    switch (iCol) {
                        case 0:
                            return Language.getString("MKT_PRICE");
                        case 1:
                            if (stock.getLastTradeValue() > 0)
                                return doubleTransferObject.setValue(convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getLastTrade(stock), record.getPortfolioID()));
                            else
                                return doubleTransferObject.setValue(convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getPreviousClosed(stock), record.getPortfolioID()));
                        case 2:
                            return Language.getString("PLEDGED");
                        case 3:
                            return longTrasferObject.setValue(record.getPledged());
                    }
                case 4:
                    switch (iCol) {
                        case 0:
                            return Language.getString("COST_BASICS");
                        case 1:
                            return doubleTransferObject.setValue(convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), false, record.getCumCost(), record.getPortfolioID()));
                        case 2:
                            return Language.getString("BALANCE");
                        case 3:
                            return longTrasferObject.setValue(record.getBalance());
                    }
                case 5:
                    switch (iCol) {
                        case 0:
                            return Language.getString("VALUATION");
                        case 1:
                            if (stock.getLastTradeValue() > 0)
                                return doubleTransferObject.setValue(convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                        record.getOwned(stock.getInstrumentType()) * PortfolioInterface.getLastTrade(stock) +
                                                record.getCashDividends(), record.getPortfolioID()));
                            else
                                return doubleTransferObject.setValue(convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                        record.getOwned(stock.getInstrumentType()) * PortfolioInterface.getPreviousClosed(stock) +
                                                record.getCashDividends(), record.getPortfolioID()));
                        case 2:
                            return Language.getString("HOLDING_PRE");
                        case 3:
                            return doubleTransferObject.setValue(getPercOfPortfolio(selectedRow));
                    }
                case 6:
                    switch (iCol) {
                        case 0:
                            return Language.getString("GAIN_LOSS");
                        case 1:
                            double marketValue = 0f;
                            if (stock.getLastTradeValue() > 0)
                                marketValue = applyPriceModification(exchange, ((record.getOwned(stock.getInstrumentType()) * PortfolioInterface.getLastTrade(stock))));
                            else
                                marketValue = applyPriceModification(exchange, (record.getOwned(stock.getInstrumentType()) * PortfolioInterface.getPreviousClosed(stock)));
                            marketValue += record.getCashDividends();
                            return doubleTransferObject.setValue(convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), false, (marketValue - record.getCumCost()), record.getPortfolioID()));
                        case 2:
                            return Language.getString("GAIN_LOSS_PRE");
                        case 3:
                            double marketValue1 = 0f;
                            if (stock.getLastTradeValue() > 0)
                                marketValue1 = applyPriceModification(exchange, (record.getOwned(stock.getInstrumentType()) *
                                        PortfolioInterface.getLastTrade(stock)));
                            else
                                marketValue1 = applyPriceModification(exchange, (record.getOwned(stock.getInstrumentType()) *
                                        PortfolioInterface.getPreviousClosed(stock)));
                            marketValue1 += record.getCashDividends();
                            return doubleTransferObject.setValue(((marketValue1 - record.getCumCost()) / record.getCumCost()) * 100);
                    }
                case 7:
                    switch (iCol) {
                        case 0:
                            return Language.getString("GAIN_LOSS_TODAY");
                        case 1:
                            return doubleTransferObject.setValue(convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), true, (record.getOwned(stock.getInstrumentType()) * PortfolioInterface.getChange(stock)[0]), record.getPortfolioID()));
                        case 2:
                            return Language.getString("GAIN_LOSS_TODAY_PRE");
                        case 3:
                            return doubleTransferObject.setValue(applyPriceModification(exchange, ((record.getOwned(stock.getInstrumentType()) * PortfolioInterface.getChange(stock)[0]) / record.getCumCost()) * 100));
                    }
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    /*
     public Object getValueAt(int iRow, int iCol) {
             try {

                 switch(iRow) {
                     case 0:
                         switch(iCol) {
                             case 0:
                                 return Language.getString("SYMBOL");
                             case 1:
                                 return valuationModel.getValueAt(selectedRow,0);
                             case 2:
                                 return Language.getString("HOLDING");
                             case 3:
                                 return valuationModel.getValueAt(selectedRow,5);
                         }
                     case 1:
                         switch(iCol) {
                             case 0:
                                 return Language.getString("CURRENCY");
                             case 1:
                                 return valuationModel.getValueAt(selectedRow,3);
                             case 2:
                                 return Language.getString("SELL_PENDING");
                             case 3:
                                 return valuationModel.getValueAt(selectedRow,19);
                         }
                     case 2:
                         switch(iCol) {
                             case 0:
                                 return Language.getString("AVG_COST");
                             case 1:
                                 return valuationModel.getValueAt(selectedRow,8);
                             case 2:
                                 return Language.getString("BUY_PENDING");
                             case 3:
                                 return valuationModel.getValueAt(selectedRow,18);
                         }
                     case 3:
                         switch(iCol) {
                             case 0:
                                 return Language.getString("MKT_PRICE");
                             case 1:
                                 return valuationModel.getValueAt(selectedRow,9);
                             case 2:
                                 return Language.getString("PLEDGED");
                             case 3:
                                 return valuationModel.getValueAt(selectedRow,6);
                         }
                     case 4:
                         switch(iCol) {
                             case 0:
                                 return Language.getString("COST_BASICS");
                             case 1:
                                 return valuationModel.getValueAt(selectedRow,10);
                             case 2:
                                 return Language.getString("BALANCE");
                             case 3:
                                 return valuationModel.getValueAt(selectedRow,7);
                         }
                     case 5:
                         switch(iCol) {
                             case 0:
                                 return Language.getString("VALUATION");
                             case 1:
                                 return valuationModel.getValueAt(selectedRow,11);
                             case 2:
                                 return Language.getString("HOLDING_PRE");
                             case 3:
                                 return valuationModel.getValueAt(selectedRow,16);
                         }
                     case 6:
                         switch(iCol) {
                             case 0:
                                 return Language.getString("GAIN_LOSS");
                             case 1:
                                 return valuationModel.getValueAt(selectedRow,14);
                             case 2:
                                 return Language.getString("GAIN_LOSS_PRE");
                             case 3:
                                 return valuationModel.getValueAt(selectedRow,15);
                         }
                     case 7:
                         switch(iCol) {
                             case 0:
                                 return Language.getString("GAIN_LOSS_TODAY");
                             case 1:
                                 return valuationModel.getValueAt(selectedRow,12);
                             case 2:
                                 return Language.getString("GAIN_LOSS_TODAY_PRE");
                             case 3:
                                 return valuationModel.getValueAt(selectedRow,13);
                         }
                 }
                 return "";
             } catch(Exception e) {
                 return "";
             }
         }


    */
    public int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 1;
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 1;
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'V';
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 3:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'C';
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 4:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'C';
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 5:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'C';
                    case 2:
                        return 2;
                    case 3:
                        return 6;
                }
            case 6:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'C';
                    case 2:
                        return 2;
                    case 3:
                        return 6;
                }
            case 7:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'C';
                    case 2:
                        return 2;
                    case 3:
                        return 6;
                }
        }
        return 0;
    }

    public long getTimeOffset() {
        return timeOffset;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 3);
        customizerRecords[3] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"), null);
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"), null);
        customizerRecords[5] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_VALUE_UP_ROW, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_VALUE_DOWN_ROW, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR"));
        return customizerRecords;
    }

    /*private float convertToSelectedCurrency(String sCurrency, float value) {
        // trad
//        return value * CurrencyStore.getCurrency(sCurrency + "|" + tradePortfolios.getBaseCurrency());//old
        // reg
        return value * (float) CurrencyStore.getRate(sCurrency, tradePortfolios.getBaseCurrency());
    }*/

    private double convertToSelectedCurrency(Exchange exchange, String sCurrency, boolean applyPriceModificationFactor, double value, String portfolio) {
        if (applyPriceModificationFactor) {
            if (exchange != null)
                return value * (double) com.isi.csvr.trading.datastore.CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
            else
                return value * (double) com.isi.csvr.trading.datastore.CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio));
        } else {
            return value * (double) com.isi.csvr.trading.datastore.CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio));
        }
    }

    private double applyPriceModification(Exchange exchange, double value) {
        if (exchange != null)
            return value / exchange.getPriceModificationFactor();
        else
            return value;
    }

    private double convertToSelectedCurrency(Exchange exchange, String sCurrency, double value, String portfolio) {
        return convertToSelectedCurrency(exchange, sCurrency, true, value, portfolio);
    }

    public double getPercOfPortfolio(int row) {
        double mValue = 0f;
        double selMValue = 0f;
        double totMValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (stock.getLastTradeValue() > 0)
                    mValue = record.getOwned(stock.getInstrumentType()) * convertToSelectedCurrency(ExchangeStore.getSharedInstance().getExchange(stock.getExchange()),
                            PortfolioInterface.getCurrency(stock),
                            PortfolioInterface.getLastTrade(stock) +
                                    record.getCashDividends(), record.getPortfolioID());
                else
                    mValue = record.getOwned(stock.getInstrumentType()) * convertToSelectedCurrency(ExchangeStore.getSharedInstance().getExchange(stock.getExchange()),
                            PortfolioInterface.getCurrency(stock),
                            PortfolioInterface.getPreviousClosed(stock) +
                                    record.getCashDividends(), record.getPortfolioID());
                if (row == i)
                    selMValue = mValue;
                totMValue += mValue;
                record = null;
                stock = null;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        /*if (isCashIncluded) {
            if (row == (getRowCount() - 2))
                selMValue = pfWindow.getCashBalanceForSelectedPFs();
            totMValue += pfWindow.getCashBalanceForSelectedPFs();
        }*/
        if (totMValue == 0)
            return 0f;
        else
            return (selMValue / totMValue) * 100;
    }
}
