package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.BooleanTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.ProgramedOrderStore;
import com.isi.csvr.trading.datastore.ProgrammedTrade;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 2:10:12 PM
 */


public class ProgramedOrderModel extends CommonTable
        implements TableModel, CommonTableInterface {
    //    IntTransferObject intTrasferObject;
    BooleanTransferObject booleanObject;

    /**
     * Constructor
     */
    public ProgramedOrderModel() {
        init();
    }

    private void init() {
//        intTrasferObject = new IntTransferObject();
        booleanObject = new BooleanTransferObject();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        return ProgramedOrderStore.getSharedInstance().size();
    }

    public Object getValueAt(int iRow, int iCol) {
        ProgrammedTrade transaction = ProgramedOrderStore.getSharedInstance().getTransaction(iRow);

        switch (iCol) {
            case -1:
                return booleanObject.setValue(false);
            case 0:
//                return intTrasferObject.setValue(transaction.getSide());
                return "" + (transaction.getSide());
            case 1:
                return "" + transaction.getOrderDate();
            case 2:
                return "" + transaction.getSymbol();
            case 3:
                return "" + TradingShared.getActionString(transaction.getSide());
            case 4:
                return "" + TradingShared.getTypeString(transaction.getType());
            case 5:
                if (transaction.getPrice() == 0)
                    return "" + Double.POSITIVE_INFINITY;
                else
                    return "" + transaction.getPrice();
            case 6:
                return "" + transaction.getOrderQuantity();
            case 7:
                return "TODO"; //"" + transaction.getGoodTillStr();
            case 8:
                return transaction.getField().getDescription();
            case 9:
                return transaction.getCondition().getDescription();
            case 10:
                return transaction.getValue();
            case 11:
                return "TODO"; //transaction.getStatusStr();
            default:
                return "0";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 11:
                return String.class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
                return Number.class;
//            case 'B':
//            case -1:
//                return Boolean.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        switch (col) {
            case 11:
                return true;
            default:
                return false;
        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        ProgrammedTrade transaction = ProgramedOrderStore.getSharedInstance().getTransaction(rowIndex);
        switch (columnIndex) {
            case 1:
                transaction.setSelected(((Boolean) value).booleanValue());
                break;
        }
        transaction = null;
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }


}


