package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.SendQueue;
import com.isi.csvr.trading.datastore.CashTransactionSearchStore;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 4, 2005
 * Time: 10:27:50 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashTransactionUI extends InternalFrame implements ActionListener, Themeable {

    private static CashTransactionUI self = null;
    private ViewSetting oSetting;
    private JPanel mainPanel;
    private JPanel headingPanel;
    private JPanel summaryPanel;
    private JPanel container;
    private Container con;
    private CashLabel headingLabel, headingNameLabel, headingNoLabel;
    private CashLabel headingNameValue, headingNoValue;
    private CashLabel summaryLabel, summOpenLabl, summBuyLabel, summSellLabel, summDepositeLabel, summWithdLabel, summOtherLabel, summCloseLabel;
    private CashLabel sumOpenVal, sumCloseVal, sumBuyVal, sumSellVal, sumDepositeVal, sumWithdVal, sumOtherVal;
    private JButton btnPrev, btnNext;
    private TWButton btnSearch;
    private Table blotterTable;
    private String type;
    private String fromDate;
    private String toDate;
    private String startDate, endDate;
    private float openVal, closeVal, buyVal, sellVal, depositeVal, withdrawVal, chargeVal;
    private long accountNo;
    private long nextlong = 0;

    private CashTransactionUI(ViewSetting oSetting) {
        super(oSetting.getCaption(), oSetting);
        this.oSetting = oSetting;
        oSetting.setParent(this);
        self = this;
        createUI();
    }

    public static CashTransactionUI getSharedInstance() {
        if (self == null) {
            ViewSetting vSetting = ViewSettingsManager.getSummaryView("CASH_TRANSACTION_SEARCH");
            //GUISettings.setColumnSettings(vSetting, TWColumnSettings.getItem("CASH_TRANSACTION_COLS"));
            self = new CashTransactionUI(vSetting);
        }
        return self;
    }

    public void createUI() {

        try {
            //this.setSize(620,400);
            this.setLocationRelativeTo(Client.getInstance().getFrame());
            con = this.getContentPane();
            //this.setVisible(true);
            container = new JPanel();
            String[] mainWidth = {"100%"};
            String[] mainHeight = {"20", "40", "20", "140"};
            container.setLayout(new FlexGridLayout(mainWidth, mainHeight, 5, 3));
            container.setBackground(Color.white);
            //container.setForeground(Color.BLACK);

            mainPanel = new JPanel();
            String[] mainWidth1 = {"100%"};
            String[] mainHeight1 = {"18"};
            mainPanel.setLayout(new FlexGridLayout(mainWidth1, mainHeight1));
            headingLabel = new CashLabel();
            headingLabel.setHorizontalAlignment(SwingUtilities.CENTER);
            mainPanel.add(headingLabel);
            mainPanel.setBackground(Color.white);
            container.add(mainPanel);

            headingPanel = new JPanel();
            String[] headingWidth = {"50%", "50%"};
            String[] headingHeight = {"18", "18"};
            headingPanel.setLayout(new FlexGridLayout(headingWidth, headingHeight, 5, 2));
            headingNameLabel = new CashLabel(Language.getString("HEADING_NAME"));
            headingNoLabel = new CashLabel(Language.getString("HEADING_NO"));
            headingNameValue = new CashLabel();
            headingNameValue.setText(TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY));
            headingNameValue.setHorizontalAlignment(SwingUtilities.TRAILING);
            headingNoValue = new CashLabel();
            headingNoValue.setText("" + accountNo);
            headingNoValue.setHorizontalAlignment(SwingUtilities.TRAILING);
            //headingPanel.add(headingLabel);
            headingPanel.add(headingNameLabel);
            headingPanel.add(headingNameValue);
            headingPanel.add(headingNoLabel);
            headingPanel.add(headingNoValue);
            headingPanel.setBackground(Color.white);
            container.add(headingPanel);

            JPanel summaryHeadPanel = new JPanel();
            String[] mainWidth2 = {"100%"};
            String[] mainHeight2 = {"18"};
            summaryHeadPanel.setLayout(new FlexGridLayout(mainWidth2, mainHeight2));
            summaryLabel = new CashLabel(Language.getString("SUMMARY_HEADING"));
            summaryLabel.setHorizontalAlignment(SwingUtilities.CENTER);
            summaryHeadPanel.add(summaryLabel);
            summaryHeadPanel.setBackground(Color.white);
            container.add(summaryHeadPanel);

            summaryPanel = new JPanel();
            String[] summaryWidth = {"50%", "50%"};
            String[] summaryHeight = {"18", "18", "18", "18", "18", "18", "18"};
            summaryPanel.setLayout(new FlexGridLayout(summaryWidth, summaryHeight, 5, 2));
            summOpenLabl = new CashLabel(Language.getString("OPENING_BALANCE"));
            summBuyLabel = new CashLabel(Language.getString("BUY_SETTLE"));
            summSellLabel = new CashLabel(Language.getString("SELL_SETTLE"));
            summDepositeLabel = new CashLabel(Language.getString("TOT_DEPOSITE"));
            summWithdLabel = new CashLabel(Language.getString("TOT_WIDTHDRAWAL"));
            summOtherLabel = new CashLabel(Language.getString("OTHER_CHARGES"));
            summCloseLabel = new CashLabel(Language.getString("CLOSING_BALANCE"));
            sumOpenVal = new CashLabel();

            sumBuyVal = new CashLabel();
            sumSellVal = new CashLabel();
            sumDepositeVal = new CashLabel();
            sumWithdVal = new CashLabel();
            sumOtherVal = new CashLabel();
            sumCloseVal = new CashLabel();

            summaryPanel.add(summOpenLabl);
            summaryPanel.add(sumOpenVal);
            summaryPanel.add(summBuyLabel);
            summaryPanel.add(sumBuyVal);
            summaryPanel.add(summSellLabel);
            summaryPanel.add(sumSellVal);
            summaryPanel.add(summDepositeLabel);
            summaryPanel.add(sumDepositeVal);
            summaryPanel.add(summWithdLabel);
            summaryPanel.add(sumWithdVal);
            summaryPanel.add(summOtherLabel);
            summaryPanel.add(sumOtherVal);
            summaryPanel.add(summCloseLabel);
            summaryPanel.add(sumCloseVal);
            summaryPanel.setBackground(Color.white);
            container.add(summaryPanel);

            createTable();
            blotterTable.setNorthPanel(container);
            createButtonPanel();

            Client.getInstance().getDesktop().add(this);
            setDefaultCloseOperation(HIDE_ON_CLOSE);
            setResizable(true);
            setClosable(true);
            setIconifiable(true);
            super.applySettings();
            GUISettings.applyOrientation(getContentPane());
            Theme.registerComponent(this);

            //TradingConnectionNotifier.getInstance().addConnectionListener(this);
            //JPanel btnPanel = createButtonPanel();
            //container.add(btnPanel);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void setSummaryDetail(String startDate, String endDate, long accuntNo, float open, float buy, float sell, float deposite, float withdraw, float otherCharge, float close) {
        try {
            SimpleDateFormat simple = new SimpleDateFormat("yyyyMMdd");
            Date start = simple.parse(startDate);
            Date end = simple.parse(endDate);
            SimpleDateFormat simple2 = new SimpleDateFormat("dd/MM/yyyy");
            this.startDate = simple2.format(start);
            this.endDate = simple2.format(end);
            this.accountNo = accuntNo;
            this.openVal = open;
            this.buyVal = buy;
            this.sellVal = sell;
            this.depositeVal = deposite;
            this.withdrawVal = withdraw;
            this.chargeVal = otherCharge;
            this.closeVal = close;
            sumOpenVal.setText("" + openVal);
            sumOpenVal.setHorizontalAlignment(SwingUtilities.TRAILING);
            sumBuyVal.setText("" + buyVal);
            sumBuyVal.setHorizontalAlignment(SwingUtilities.TRAILING);
            sumSellVal.setText("" + sellVal);
            sumSellVal.setHorizontalAlignment(SwingUtilities.TRAILING);
            sumDepositeVal.setText("" + depositeVal);
            sumDepositeVal.setHorizontalAlignment(SwingUtilities.TRAILING);
            sumWithdVal.setText("" + withdrawVal);
            sumWithdVal.setHorizontalAlignment(SwingUtilities.TRAILING);
            sumOtherVal.setText("" + chargeVal);
            sumOtherVal.setHorizontalAlignment(SwingUtilities.TRAILING);
            sumCloseVal.setText("" + closeVal);
            sumCloseVal.setHorizontalAlignment(SwingUtilities.TRAILING);
            String str = Language.getString("HEADING");
            str = str.replaceAll("\\[start\\]", (this.startDate));
            str = str.replaceAll("\\[end\\]", (this.endDate));
            headingLabel.setText(str);
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void enableNext(int index) {
        if (index == 0) {
            btnNext.setEnabled(false);
        } else {
            btnNext.setEnabled(true);
        }
    }

    public void createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        String[] width = {"100", "100%", "80", "80"};
        String[] height = {"25"};
        FlexGridLayout flex = new FlexGridLayout(width, height);
        buttonPanel.setLayout(flex);
        btnPrev = new JButton(Language.getString("PREVIOUS"));
        btnPrev.setBorder(BorderFactory.createEmptyBorder());
        btnNext = new JButton(Language.getString("NEXT"));
        btnNext.setBorder(BorderFactory.createEmptyBorder());
        btnSearch = new TWButton(Language.getString("SEARCH"));
        buttonPanel.add(btnPrev);
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(btnSearch);
        buttonPanel.add(btnNext);
        btnPrev.addActionListener(this);
        btnNext.addActionListener(this);
        btnSearch.addActionListener(this);
        btnPrev.setEnabled(false);
        btnNext.setEnabled(false);
        btnSearch.setEnabled(true);
        //getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        blotterTable.setSouthPanel(buttonPanel);
    }

    private void createTable() {

        blotterTable = new Table();
        blotterTable.setThreadID(Constants.ThreadTypes.TRADING);
        //final FontMetrics fontMetrics = blotterTable.getFontMetrics(getFont());
        //blotterTable.setPreferredSize(new Dimension(500, 100));
        CashTransactionModel blotterModel = new CashTransactionModel();
        blotterModel.setDataStore(CashTransactionSearchStore.getSharedInstance());
        //blotterTable.setWindowType(ViewSettingsManager.ORDER_SEARCH);
        blotterModel.setViewSettings(oSetting);
        blotterTable.setModel(blotterModel);
        blotterModel.setTable(blotterTable);
        blotterModel.applyColumnSettings();
        blotterModel.updateGUI();

        oSetting.setParent(this);
        //super.applySettings();
        setLayer(GUISettings.TOP_LAYER);
        //getContentPane().add(blotterTable, BorderLayout.CENTER);
        getContentPane().add(blotterTable);
        super.setTable(blotterTable);
    }

    private void initSearch() {
        CashTransactionSearchStore.getSharedInstance().init();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnPrev) {
            if (TradingShared.isReadyForTrading()) {
                CashTransactionSearchStore.getSharedInstance().clear();
                CashTransactionSearchStore.getSharedInstance().goPageBack();
                sendDetailRequest(CashTransactionSearchStore.getSharedInstance().getPageBottom(), fromDate, toDate, type);
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } else if (e.getSource() == btnNext) {
            if (TradingShared.isReadyForTrading()) {
                CashTransactionSearchStore.getSharedInstance().clear();
                //CashTransactionSearchStore.getSharedInstance().setNewPage();
                //nextlong =CashTransactionSearchStore.getSharedInstance().getCurrentPageBottom();
                sendDetailRequest(CashTransactionSearchStore.getSharedInstance().getPageBottom(), fromDate, toDate, type);
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } else if (e.getSource() == btnSearch) {
            CashTransactionSearch search = new CashTransactionSearch();
            search.setVisible(true);
        }
    }

    public void sendSummaryRequest(String fromDate, String toDate) {
        try {
            if (TradingShared.isConnected()) {
                TradeMessage messageHeader = new TradeMessage(TradeMeta.MT_CASH_SUMMARY);
                messageHeader.addData(fromDate);
                messageHeader.addData(toDate);
                messageHeader.addData(TradingShared.getTrader().getMubasherID(Constants.PATH_PRIMARY));
                SendQueue.getSharedInstance().addData(messageHeader.toString(), Constants.PATH_PRIMARY);
                messageHeader = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendDetailRequest(long startID, String fromDate, String toDate, String type) {
        this.type = type;
        this.fromDate = fromDate;
        this.toDate = toDate;
        CashTransactionSearchStore.getSharedInstance().clear();
        if (startID == 0) {
            btnPrev.setEnabled(false);
        } else {
            btnPrev.setEnabled(true);
        }
        try {
            if (TradingShared.isConnected()) {
                TradeMessage messageHeader = new TradeMessage(TradeMeta.MT_CASH_DETAIL);
                messageHeader.addData(fromDate);
                messageHeader.addData(toDate);
                messageHeader.addData(type);
                messageHeader.addData(startID);
                messageHeader.addData(TradingShared.getTrader().getMubasherID(Constants.PATH_PRIMARY));
                SendQueue.getSharedInstance().addData(messageHeader.toString(), Constants.PATH_PRIMARY);
                messageHeader = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* public void sendRequest(long startID){
        String symbol;

        try {
            if (TradingShared.isConnected()){
                 TradeMessage messageHeader = new TradeMessage(TradeMeta.MT_CASH_SUMMARY);
                //messageHeader.addData();
                messageHeader.addData(TradingShared.getTradeUserName());

                SendQueue.getSharedInstance().addData(messageHeader.toString());
                messageHeader = null;

                TradeMessage message = new TradeMessage(TradeMeta.MT_CASH_DETAIL);
                //message.addData();
                message.addData(TradingShared.getTradeUserName());

                SendQueue.getSharedInstance().addData(message.toString());
                message = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void internalFrameActivated(InternalFrameEvent e) {
        super.internalFrameActivated(e);
        //trade
        //cmbSymbols = Client.getInstance().getCompanySelector();
        //reg
        //cmbSymbols = new JComboBox();
        //symbolPanel.add(cmbSymbols);
        super.show();
    }


    class CashLabel extends JLabel {

        public CashLabel(String Text) {
            super.setText(Text);
            setForeground(Color.black);
            setHorizontalAlignment(SwingUtilities.LEADING);
//            setFont( new TWFont(getFont().getFontName(),Font.BOLD,14));
            setFont(getFont().deriveFont((float) getFont().getSize() + 1));
            setFont(getFont().deriveFont(Font.BOLD));
            //setFont(getFont().deriveFont(Font.BOLD));

        }

        public CashLabel() {
            //super.setText("");
            setForeground(Color.black);
            setHorizontalAlignment(SwingUtilities.LEADING);
//            setFont( new TWFont(getFont().getFontName(),Font.BOLD,14));
            setFont(getFont().deriveFont((float) getFont().getSize() + 1));
            setFont(getFont().deriveFont(Font.BOLD));
            //setFont(getFont().deriveFont(Font.BOLD));
        }

    }
}
