package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.datastore.Reports;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;
import com.isi.util.TypeConvert;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 22, 2005
 * Time: 2:17:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerStatements implements ActionListener, ProgressListener,
        Themeable, Runnable, InternalFrameListener, MouseListener, ItemListener {

    public static CustomerStatements self = null;
    private InternalFrame frame;
    private TWButton submit;
    private TWButton close;
    private ArrayList<TWComboItem> reportList;
    private ArrayList<TWComboItem> monthList;
    private JComboBox statements;
    private JComboBox portfolios;
    private JComboBox months;
    private com.isi.csvr.util.SimpleProgressBar progressBar;
    private JLabel lblProgress;
    private String reportType;
    private String reportYear;
    private String reportMonth;
    private String reportPortfolio;
    private boolean active = true;

    public CustomerStatements() {
        createUI();
    }

    public static CustomerStatements getSharedInstance() {
        return self;
    }

    private void createUI() {
        frame = new InternalFrame(false);
        frame.setTitle(Language.getString("TRADING_REPORTS"));
        //frame.getContentPane().setLayout(new BorderLayout());
        frame.setClosable(true);

        String[] widths = {"100", "180"};
        String[] heights = {"22"};
//        String[] heights = {"25", "25", "20"};
//        JPanel centerPanel = new JPanel(new FlexGridLayout(widths, heights, 5, 5, true, true));

        JPanel portfolioPanel = new JPanel(new FlexGridLayout(widths, heights, 5, 2, true, true));
        portfolioPanel.add(new JLabel(Language.getString("PORTFOLIO")));
        ArrayList<TWComboItem> portfoliolist = new ArrayList<TWComboItem>();
        for (TradingPortfolioRecord record : TradingShared.getTrader().getPortfolios()) {
            portfoliolist.add(new TWComboItem(record.getPortfolioID(), record.getName()));
        }
        TWComboModel portfolioModel = new TWComboModel(portfoliolist);
        portfolios = new JComboBox(portfolioModel);
        if (portfolioModel.getSize() > 0) {
            portfolios.setSelectedIndex(0);
            reportPortfolio = ((TWComboItem) portfolios.getSelectedItem()).getId();
        }
        portfolioPanel.add(portfolios);
        portfolios.addItemListener(this);

        JPanel typePanel = new JPanel(new FlexGridLayout(widths, heights, 5, 2, true, true));
        typePanel.add(new JLabel(Language.getString("TYPE")));
        reportList = new ArrayList<TWComboItem>();
        try {
            Reports reports = new Reports(TradingShared.getTrader().getBrokerID(reportPortfolio));
//            reportList = reports.getReports();
            for (int i = 0; i < reports.getReports().size(); i++) {
                reportList.add(reports.getReports().get(i));
            }
//            Arrays.copyOf(reportList,reports.getReports());
        } catch (Exception e) {
        }
        TWComboModel reportModel = new TWComboModel(reportList);
        statements = new JComboBox(reportModel);
        if (reportModel.getSize() > 0) {
            statements.setSelectedIndex(0);
        }
        typePanel.add(statements);
        statements.addItemListener(this);

        JPanel monthPanel = new JPanel(new FlexGridLayout(widths, heights, 5, 2, true, true));
        monthPanel.add(new JLabel(Language.getString("MONTH")));
        monthList = new ArrayList<TWComboItem>();
        TWComboModel monthModel = new TWComboModel(monthList);
        getMonths();
        months = new JComboBox(monthModel);
        monthPanel.add(months);
        if (monthModel.getSize() > 0) {
            months.setSelectedIndex(0);
        }
        months.addItemListener(this);

        JPanel progressPanel = new JPanel(new FlexGridLayout(widths, heights, 5, 2, true, true));
        lblProgress = new JLabel(Language.getString("PROGRESS"));
        progressPanel.add(lblProgress);
        progressBar = new com.isi.csvr.util.SimpleProgressBar();
        progressBar.setHorizontalAlignment(SwingConstants.CENTER);
        progressPanel.add(progressBar);

        JPanel messagePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel msg = new JLabel(Language.getString("MSG_ACROBAT_REQUIRED"));
        messagePanel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        messagePanel.addMouseListener(this);
        msg.addMouseListener(this);
        messagePanel.add(msg);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        submit = new TWButton(Language.getString("SUBMIT"));
        submit.setPreferredSize(new Dimension(80, 25));
        submit.addActionListener(this);
        buttonPanel.add(submit);
        close = new TWButton(Language.getString("CLOSE"));
        close.setPreferredSize(new Dimension(80, 25));
        close.addActionListener(this);
        buttonPanel.add(close);


        //String[] mainWidths = {"100%"};
        //String[] mainHeights = {"100%", "0", "0"};

//        JPanel centerPanel = new JPanel(new ColumnLayout());
//        centerPanel.add(typePanel);
//        centerPanel.add(monthPanel);
//        centerPanel.add(progressPanel);

        frame.getContentPane().setLayout(new ColumnLayout(ColumnLayout.MODE_SAME_SIZE));
//        if (TradingShared.getTrader().numberOfAccounts() > 1){
        frame.getContentPane().add(portfolioPanel);
//        }
        frame.getContentPane().add(typePanel);
        frame.getContentPane().add(monthPanel);
        frame.getContentPane().add(progressPanel);
        frame.getContentPane().add(messagePanel);
        frame.getContentPane().add(buttonPanel);
        GUISettings.applyOrientation(frame.getContentPane());

//        frame.setSize(300, 225);
        frame.pack();
        GUISettings.setLocationRelativeTo(frame, Client.getInstance().getDesktop());
        Client.getInstance().getDesktop().add(frame);
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.addInternalFrameListener(this);

        frame.setOrientation();
        Theme.registerComponent(this);
        applyTheme();
        self = this;
    }

    private void getMonths() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
//        ArrayList<TWComboItem> list = new ArrayList<TWComboItem>();
        monthList.clear();
        String currentDay = TradingShared.getTrader().getCurrentDay(reportPortfolio);
        int currentYear = Integer.parseInt(currentDay.substring(0, 4));
        int currentMonth = Integer.parseInt(currentDay.substring(4, 6));
        String[] months = Language.getList("MONTH_NAMES");
        int reportDepth = TradingShared.getTrader().getReportDepth(reportPortfolio);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, currentYear);
        cal.set(Calendar.MONTH, currentMonth - 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);

        for (int i = 0; i < reportDepth; i++) {
            TWComboItem item = new TWComboItem(dateFormat.format(cal.getTime()),
                    cal.get(Calendar.YEAR) + " - " + months[cal.get(Calendar.MONTH)]);
            monthList.add(item);
            cal.add(Calendar.MONTH, -1);
        }

//        return list;

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == submit) {
            try {
                submit.setEnabled(false);
                months.setEnabled(false);
                statements.setEnabled(false);
                reportType = ((TWComboItem) statements.getSelectedItem()).getId();
                reportYear = ((TWComboItem) months.getSelectedItem()).getId().substring(0, 4);
                reportMonth = ((TWComboItem) months.getSelectedItem()).getId().substring(4, 6);
                reportPortfolio = ((TWComboItem) portfolios.getSelectedItem()).getId();
                Thread thread = new Thread(this, "CustomerStatement");
                thread.start();

                progressBar.setValue(0);
            } catch (Exception e1) {
                submit.setEnabled(true);
                months.setEnabled(true);
                statements.setEnabled(true);
            }
        } else if (e.getSource() == close) {
            if (active) {
                frame.setVisible(false);
            } else {
                frame.dispose();
            }
        }
    }

    public void show() {
        frame.setVisible(true);
        if (!active) {
            progressBar.setValue(0);
        }
    }

    public void run() {
        try {
            byte[] buffer = new byte[1000];
            byte[] fileLen = new byte[4];
            int totalRead = 0;
            int bytesread;
            StringBuilder url = new StringBuilder();

            url.append(TradingShared.getTrader().getReportURL(reportPortfolio));
            url.append("?sid=");
            url.append(TradingShared.getTrader().getSessionID(reportPortfolio));
            url.append("&lan=");
            url.append(Language.getLanguageTag());
            url.append("&type=");
            url.append(reportType);
            url.append("&year=");
            url.append(reportYear);
            url.append("&month=");
            url.append(reportMonth);
            url.append("&PF=");
            url.append(reportPortfolio);

            System.out.println(url);
            active = true;
            while (active) {
                try {
                    progressBar.setText(Language.getString("CONNECTING"));
                    URL connection = new URL(url.toString());
                    InputStream in = connection.openStream();
                    File file = new File(Settings.getAbsolutepath() + "temp/downloads/pdf" + System.currentTimeMillis() + ".pdf");
                    FileOutputStream out = new FileOutputStream(file);
                    file.deleteOnExit();
                    totalRead = 0;
                    progressBar.setText(Language.getString("DOWNLOADING"));

                    for (int i = 0; i < fileLen.length; i++) {
                        fileLen[i] = (byte) in.read();
                    }

                    int size = TypeConvert.byteArrayToInt(fileLen, 0, 4);

                    if (size <= 0) { // no data available
                        active = false;
                        setProcessCompleted(null, false);
                        break;
                    }

                    setMax(null, size);

                    while (true) {
                        bytesread = in.read(buffer);
                        if (bytesread == -1) {
                            out.close();
                            break;
                        }
                        totalRead += bytesread;
                        setProgress(null, totalRead);
                        out.write(buffer, 0, bytesread);
                    }
                    setProcessCompleted(null, true);
                    openReport(file.getAbsolutePath());
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "CustomerStatements");
                    active = false;
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    active = false;
                    setProcessCompleted(null, false);
                } finally {
                    progressBar.setText("");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("[Statement Download] Completed ");
    }

    public void openReport(String file) {
        try {
            Process process = Runtime.getRuntime().exec("START \"" + file + "\"");

            process.waitFor();
            if (process.exitValue() != 0) {
                new ShowMessage(Language.getString("MSG_PDF_NOT_INSTALLED"), "E");
            }
            process = null;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setMax(String id, int max) {
        progressBar.setMaximum(max);
    }

    public void setProcessCompleted(String id, boolean sucess) {
        submit.setEnabled(true);
        months.setEnabled(true);
        statements.setEnabled(true);
        if (!sucess) {
            SharedMethods.showMessage(Language.getString("NO_STATEMENTS_AVAILABLE"), JOptionPane.ERROR_MESSAGE);
        }
    }

    public void setProcessStarted(String id) {
        progressBar.setValue(0);
    }

    public void setProgress(String id, int progress) {
        progressBar.setValue(progress);
        progressBar.repaint();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(frame);
        progressBar.setBorder(UIManager.getDefaults().getBorder("TextField.border"));
    }

    public void internalFrameActivated(InternalFrameEvent e) {
    }

    public void internalFrameClosed(InternalFrameEvent e) {
        self = null;
        System.out.println("Disposed ...");
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        if (!active) {
            Theme.unRegisterComponent(this);
            frame.dispose();
        }
    }

    public void itemStateChanged(ItemEvent e) {
        progressBar.setValue(0);
        if (e.getSource() == portfolios) {
            statements.removeItemListener(this);
            months.removeItemListener(this);
            reportList.clear();
            monthList.clear();
            try {
                reportPortfolio = ((TWComboItem) portfolios.getSelectedItem()).getId();
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                String brokerID = TradingShared.getTrader().getBrokerID(reportPortfolio);
                Reports reports = new Reports(brokerID);
//                reportList = reports.getReports();
//                Collections.copy(reportList,reports.getReports());
//                statements.removeAllItems();
                for (int i = 0; i < reports.getReports().size(); i++) {
                    reportList.add(reports.getReports().get(i));
                }
                statements.updateUI();
                statements.setSelectedIndex(0);
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                getMonths();
                months.updateUI();
                months.setSelectedIndex(0);
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            statements.addItemListener(this);
            months.addItemListener(this);
        }
    }

    public void mouseClicked(MouseEvent e) {
        try {
            Runtime.getRuntime().exec("START http://www.adobe.com/");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void internalFrameDeactivated(InternalFrameEvent e) {
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    public void internalFrameIconified(InternalFrameEvent e) {
    }

    public void internalFrameOpened(InternalFrameEvent e) {
    }
}