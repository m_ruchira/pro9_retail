package com.isi.csvr.trading.ui;

import bsh.Interpreter;
import com.dfn.mtr.mix.beans.Condition;
import com.dfn.mtr.mix.beans.ConditionalBehaviour;
import com.dfn.mtr.mix.beans.Order;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.*;
import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.chrriis.dj.swingsuite.JNumberEntryField;
import com.isi.csvr.chrriis.dj.swingsuite.JTextEntryField;
import com.isi.csvr.chrriis.dj.swingsuite.TextEntryFormatter;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.BrokerConfig;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.marginTrading.CoverageLabel;
import com.isi.csvr.trading.marginTrading.MarginCalculator;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.TransactRecord;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.util.Decompress;
import com.isi.csvr.variationmap.VariationImage;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.plaf.metal.MetalScrollBarUI;
import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 25, 2004
 * Time: 3:17:45 PM
 */
public class TransactionDialog extends InternalFrame
        implements Runnable, ActionListener, Themeable, KeyListener,
        FocusListener, MouseListener, TradeMeta, TraderProfileDataListener,
        DateSelectedListener, TransactionDialogInterface, DocumentListener,
        AdjustmentListener, ItemListener, TradingConnectionListener {//, FocusTraversalPolicy {

    private JLabel lblBuyingPower;
    private JPanel powerPanel;

    private TWComboBox cmbPortfolioNos;
    private InfoLabel txtOrderNo;
    //    private TWComboBox bookKeepersCombo;
    //    private ArrayList<TWComboItem> bookKeepers;
    //    private InfoLabel bookKeepersLbl;
    private JPanel portfolioDataPanel;

    private JLabel companyName;
    private JPanel namePanel;

    private TWTextField txtSymbol;
    private TWButton btnSelectSymbol;
    private JPanel symbolsPanel;
    private InfoLabel lblSymbol;
    private InfoLabel lblLast;
    private InfoLabel txtLast;
    private InfoLabel lblChange;
    private InfoLabel txtChange;
    private InfoLabel lblBidOffer;
    private InfoLabel txtBidOffer;
    private InfoLabel lblHighLow;
    private InfoLabel txtHighLOw;
    private InfoLabel lblMinMax;
    private InfoLabel txtMinMax;
    private InfoLabel lblExpiryDate;
    private InfoLabel txtExpiryDate;
    private InfoLabel lblLotSize;
    private InfoLabel txtLotSize;
    private JPanel infoPanel;

    // private JComboBox cmbAction;
    private TWComboBox cmbAction;
    private JLabel lblTPrice;
    private TWTextField txtTPrice;
    private JScrollBar priceSpinner;
    private JLabel lblTQty;
    private JNumberEntryField txtQty;
    private JScrollBar quantitySpinner;
    private JNumberEntryField txtMinFill;
    private TWComboBox cmbType;
    private JCheckBox txtAllOrNone;
    private JPanel cmbGoodTills;
    private JLabel txtGoodTill;
    private TWTextField txtDisclosed;
    private TWComboBox cmbStopPrice;
    private TWTextField txtStopPrice;

    private TWTextField txtRemaining;
    private TWTextField txtFilled;

    private InfoLabel lblHoldongs;
    private InfoLabel txtHoldongs;
    private InfoLabel lblBuyPending;
    private InfoLabel txtBuyPending;
    private InfoLabel lblSellPending;
    private InfoLabel txtSellPending;
    private InfoLabel lblOrderValue;
    private InfoLabel txtOrderValue;
    private InfoLabel lblCommission;
    private InfoLabel txtCommission;
    private InfoLabel lblNetValue;
    private InfoLabel txtNetValue;
    private InfoLabel lblInitMargin;
    private InfoLabel txtInitMargin;
    private InfoLabel lblMainMargin;
    private InfoLabel txtMainMargin;
    private JPanel accountStatusPanel;

    private TWButton btnBuy;
    private TWButton btnQ;
    private TWButton btnSell;
    private TWButton btnAmend;
    private TWButton btnCancel;
    private TWButton btnClose;

    private JButton btnOrderBook;
    private JButton btnOrderCal;
    private JButton btnShowDays;

    private boolean active = true;
    private DecimalFormat quantityFormat;
    private DecimalFormat changeFormat;
    private Color upColor;
    private Color downColor;
    private Color normalColor;
    private JLabel lblPending;

    private JPopupMenu datePopup;
    private DatePicker datePicker;
    //    private DateCombo datePicker;
    private TWTextField refID;

    private double minPrice;
    private double maxPrice;
    private double openPrice;
    private double highPrice;
    private double lowPrice;
    //    private double marketPrice;
    private double refPrice;
    private double bid;
    private double offer;
    private int side;
    private char type = TradeMeta.ORDER_TYPE_LIMIT;
    private short goodTill = 1;
    private short tiff = -1;
    private long goodTillLong;
    private String marketCode;
    private String goodTillStr;
    private String currentOrderID;
    private String currency;
    private String exchange;
    private String symbol;
    private int instrument;
    private boolean keyTyped;
    private long remainingQty;
    private long totalQty;

    TWMenuItem mnuFillOrKill;
    TWMenuItem mnuDay;
    TWMenuItem mnuWeek;
    TWMenuItem mnuMonth;
    TWMenuItem mnuGTD;
    TWMenuItem mnuGTC;
    TWMenuItem mnuAOP;
    TWMenuItem mnuIOC;
    TWMenuItem mnuGTX;

    private boolean queued;
    private int executionMode = TradeMeta.MODE_NORMAL;
    private int windowMode = NEW;

    //    private InfoLabel lblOrderValue;
    //    private InfoLabel txtHoldongs;
    //    private InfoLabel txtOrderValue;
    //    private InfoLabel txtBuyPending;
    //    private InfoLabel txtCommission;
    //    private InfoLabel txtSellPending;
    //    private InfoLabel txtNetValue;
    private JLabel lblPower;

    private long pendingBuyQty = 0;
    private long pendingSellQty = 0;
    private long holding = 0;
    private long dayHolding = 0;
    private double netValue = 0;

    private ArrayList<TWComboItem> orderTypesList;
    private ArrayList<TWComboItem> stopPriceTypesList;
    private ArrayList<String> validatedSymbols;

    private Transaction currentTransaction;
    private long selectedQID = 0;

    public static boolean blocked = false;
    private Stock selectedStock;
    private FutureBaseAttributes futureBaseAttributes = null;
    private int quantityScrollValue = 0;
    private int priceScrollValue = 0;
    private String currentExchange = "NULL";
    private Hashtable<String, String> ruleTable;

    final String[] widths = {"22%", "28%", "22%", "28%"};
    final String[] dataPanelWidths = {"50%", "50%"};
    final String[] widthsForDataPanel = {"44%", "56%"};
    final String[] widths2 = {"22%", "28%", "50%"};
    final String[] conditionWidths = {"24%", "31%", "16%", "0", "29%"};

    private final int WIDTH = 490;
    private int HEIGHT = 395;//390;       //365
    private String selectedPortfolio;
    private String rule;
    private ConditionalBehaviour behaviourRule;
    private ConditionalBehaviour bracketRule;

    private JPanel conditionPanel;
    private JPanel cmbConditionExpiry;
    private TWComboBox cmbConditionMethods;
    private ArrayList<TWComboItem> conditionMethods;
    private TWComboBox cmbConditionOperators;
    private ArrayList<TWComboItem> conditionOperators;
    private TWTextField txtConditionValue;
    private JLabel txtConditionExpiry;
    private int conditionalMode = TradeMeta.CONDITION_TYPE_NONE;
    private JButton btnShowConditionDays;

    private TWTabbedPane mainTabbedPane;
    //   private JPanel advancedPanel;

    private JPanel normalTabPanel;
    private JPanel advanceTabPanel;

    private JPanel cardPanel;
    private JPanel dataPanel;
    private JPanel amendDataPanel;
    private JPanel buttonPanel;

    private JPanel bracketPanel;
    private JCheckBox bracketCheck;
    private TWTextField bracketTakeProf;
    private TWTextField bracketStopLoss;
    private TWTextField bracketTrailStopLoss;
    private TWComboBox bracketcmbSL;
    private ArrayList<TWComboItem> bracketSLTypes;
    private JLabel bracketlblSL;
    private JLabel bracketlblTP;

    private JPanel stradlePanel;
    private TWTextField stradleTakeProf;
    private JLabel stradlelblTP;

    private TWDataPanel actionPanel;
    private TWDataPanel typePanel;
    private TWDataPanel lPricePanel;
    private TWDataPanel aonPanel;
    private TWDataPanel qtyPanel;
    private TWDataPanel gtdPanel;
    private TWDataPanel filledQtyPanel;
    private TWDataPanel remQtyPanel;
    private TWDataPanel minFillPanel;
    private TWDataPanel disclosedPanel;
    private TWDataPanel t0SellPanel;
    private TWDataPanel t0HoldingsPanel;
    private TWDataPanel refIDPanel;
    private TWDataPanel dayOrderCheckPanel;

    private JPanel stopPricePanel;

    private ArrayList<TWComboItem> execTypeMethods;
    private TWComboBox cmbexecTypeOperators;
    private TWTextField txtexecTimeValue;
    private TWTextField txtexecBlockValue;
    private JPanel executionPanel;
    private boolean enabledSliceOrders = false;

    private CardLayout cardLayout;

    private TWTabbedPane conditionTabbedPane;
    ConditionPanelComponent preConComponent;
    ConditionPanelComponent ammConComponent;
    ConditionPanelComponent canConComponent;

    private JPanel blankCardPanel;
    private JCheckBox dayOrder;
    private JPanel stopLossCardPanel;
    private TWTextField stopLossCardField;

    private JPanel loginDetailPanel;
    CustomizableLable lblLogingDetail;

    public static final byte SIMPLE_WINDOW_TYPE = 0;
    public static final byte ADVANCED_WINDOW_TYPE = 1;

    private boolean isEnableBracketOrderPanel = false;
    private boolean hasStopLossOrders = false;
    private boolean isEnabledDayOrder = false;
    private boolean isEnabledAdvacedTabPanel = false;
    private boolean isTPlusEnable = false;
    private boolean isEnableSliceOrders = true;
    private boolean isConditionalOrdersEnabled = true;
    private boolean isEnableSliceOrdersInterval = true;

    private final String cardPanelBracket = "bracket";
    private final String cardPanelStraddle = "straddle";
    private final String cardPanelBlank = "blank";
    private final String cardPanelStopLoss = "stopLoss";

    //egypt changes :- custodial acounts
    private TWComboBox bookKeepersCombo;
    private TWComboModel bookKeepersModel;
    private ArrayList<TWComboItem> bookKeepers;
    InfoLabel bookKeepersLbl;
    private JLabel t0SellLbl;
    private JLabel t0Holdings;
    private JCheckBox t0SellChkBox;
    private TWTextField t0HoldingsFdl;
    private ConfirmationFrame confirmation;

    private static final String NA = Language.getString("NA");

    private JPanel pnlMarginViews = new JPanel();
    private JLabel imglbl1 = new JLabel();
    private CoverageLabel imglbl2;
    TWDecimalFormat formatter = new TWDecimalFormat("#0.00");
    VariationImage coverage;
    VariationImage marginUtilized;
    TradingFocusTraversalPolicy focuspolicy;
    JLabel coveragelbl;
    private int coverageDecimals = 2;
    private int coverageDecimalsRoundMtd = 1;
    double commission = 0;

    public TransactionDialog(String portfolio, String symbol, long quantity, int side, boolean queued, long qID) throws HeadlessException {
        this(portfolio, symbol, quantity, side, queued, qID, TradeMeta.MODE_NORMAL, null);
    }

    public TransactionDialog(String portfolio, String key, long quantity, int side, boolean queued, long qID, int executionMode, String bookKeeper) throws HeadlessException {


        this.selectedPortfolio = portfolio;
        this.side = side;
        this.queued = queued;
        this.selectedQID = qID;
        this.executionMode = executionMode;

        boolean symboilValidated;
        ruleTable = new Hashtable<String, String>();

        if (SharedMethods.isFullKey(key)) {
//            this.symbol = SharedMethods.getSymbolFromKey(key);
//            this.exchange = SharedMethods.getExchangeFromKey(key);
//            this.instrument = SharedMethods.getInstrumentTypeFromKey(key);
//            selectedStock = DataStore.getSharedInstance().getStockObject(key);
            setSelectedStock(key);
            if ((instrument == Meta.INSTRUMENT_FUTURE) && (selectedStock != null)) {
                futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, selectedStock.getOptionBaseSymbol(), instrument));
            }
            symboilValidated = true;
        } else {
            symboilValidated = doInternalvalidate(key);
        }

        if (executionMode == TradeMeta.MODE_PROGRAMMED) {
            conditionalMode = CONDITION_TYPE_NORMAL;
        }

        if (TradingShared.isBracketOrderEnabled(Constants.PATH_PRIMARY)) {
            isEnableBracketOrderPanel = true;
            HEIGHT = HEIGHT + 80;
        } else if (TradingShared.SECONDARY_LOGIN_SUCCESS && TradingShared.isBracketOrderEnabled(Constants.PATH_SECONDARY)) {
            isEnableBracketOrderPanel = true;
            HEIGHT = HEIGHT + 80;
        } else {
            isEnableBracketOrderPanel = false;
        }

        if (TradingShared.isDayOrdersEnabled(Constants.PATH_PRIMARY)) {
            isEnabledDayOrder = true;
            // HEIGHT = HEIGHT + 30;
            HEIGHT = HEIGHT;
        } else if (TradingShared.SECONDARY_LOGIN_SUCCESS && TradingShared.isDayOrdersEnabled(Constants.PATH_SECONDARY)) {
            isEnabledDayOrder = true;
            // HEIGHT = HEIGHT + 30;
            HEIGHT = HEIGHT;
        } else {
            isEnabledDayOrder = false;
        }

        if (TradingShared.isConditionalOrdersEnabled(Constants.PATH_PRIMARY) || TradingShared.isSlicedOrderEnabled(Constants.PATH_PRIMARY)) {
            isEnabledAdvacedTabPanel = true;
            HEIGHT = HEIGHT + 20;
        } else if (TradingShared.SECONDARY_LOGIN_SUCCESS && (TradingShared.isConditionalOrdersEnabled(Constants.PATH_PRIMARY) || TradingShared.isSlicedOrderEnabled(Constants.PATH_PRIMARY))) {
            isEnabledAdvacedTabPanel = true;
            HEIGHT = HEIGHT + 20;
        } else {
            isEnabledAdvacedTabPanel = false;
        }

        if ((TWControl.isT0OrdersEnabled()) && (TradingShared.isT0ordersEnable())) {
            isTPlusEnable = true;
            HEIGHT = HEIGHT + 50;
        } else {
            isTPlusEnable = false;
        }

        if (TradingShared.isBookKeepersAvailable()) {
            HEIGHT = HEIGHT + 25;
        }

//        isEnableSliceOrdersInterval = BrokerConfig.getSharedInstance().isSliceOrderTimeInrevalAvailable();  // to enable/disable time interval in slice orders

        /*if (TradingShared.isT0ordersEnable(exchange)) {
            isTPlusEnable = true;
            HEIGHT = HEIGHT + 50;
        }
        if (TradingShared.isBracketOrderEnabled()) {
            isEnableBracketOrderPanel = true;
            HEIGHT = HEIGHT + 80;
        }
        if (TradingShared.isDayOrdersEnabled()) {
            isEnabledDayOrder = true;
            HEIGHT = HEIGHT + 22 + 20;
        }

        isEnableSliceOrdersInterval = BrokerConfig.getSharedInstance().isSliceOrderTimeInrevalAvailable();  // to enable/disable time interval in slice orders
        if (TradingShared.isAdvacedOrderEnabled()) {
            isEnabledAdvacedTabPanel = true;
            HEIGHT = HEIGHT + 20;
        } else if (TradingShared.isConditionalTradableExchange()) {
            HEIGHT = HEIGHT + 20;
            if (isEnabledAdvacedTabPanel) {
                isEnableSliceOrders = true;
            } else {
                isEnabledAdvacedTabPanel = true;
                isEnableSliceOrders = false;
                isEnableSliceOrdersInterval = false;
            }
        }*/

        //todo ---
        // egypt changes :

        bookKeepers = new ArrayList<TWComboItem>();
        bookKeepersModel = new TWComboModel(bookKeepers);
        bookKeepersCombo = new TWComboBox(bookKeepersModel);
        bookKeepersLbl = new InfoLabel(Language.getString("BOOKKEEPER"));
        t0SellLbl = new JLabel(Language.getString("SELL_T+0"));
        t0Holdings = new JLabel(Language.getString("HOLDINGS_T+0"));
        t0SellChkBox = new JCheckBox();
        t0SellChkBox.addActionListener(this);
        t0HoldingsFdl = new TWTextField();
        imglbl2 = new CoverageLabel(BrokerConfig.getSharedInstance().getCoverageDecimalCount(), BrokerConfig.getSharedInstance().getCoverageDecimalRoundingMode());
        coverageDecimals = BrokerConfig.getSharedInstance().getCoverageDecimalCount();
        coverageDecimalsRoundMtd = BrokerConfig.getSharedInstance().getCoverageDecimalRoundingMode();
        createUI();
        setPanelLabels();

        try {
            if ((selectedPortfolio == null) || (selectedPortfolio.equals(""))) {
                TradingPortfolioRecord portflio = TradingShared.getTrader().getPortfolio(selectedPortfolio);
                if (portflio == null) {
                    portflio = TradeMethods.getPreferredProtfolio(exchange);
                    selectedPortfolio = portflio.getPortfolioID();
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        populatePortfolios();
        TradeMethods.getSharedInstance().populateBookKeepers(selectedPortfolio, bookKeepers);
        try {
            if (bookKeeper != null) {
                bookKeepersCombo.setSelectedIndex(TradeMethods.getSharedInstance().getBookKeeper(selectedPortfolio, bookKeepers, bookKeeper));
            } else {
                bookKeepersCombo.setSelectedIndex(TradeMethods.getSharedInstance().getDefaultBookKeeper(selectedPortfolio, bookKeepers));
            }
        } catch (Exception e) {
            bookKeepersCombo.setEnabled(false);

        }
        validateTIFFTypes(); // revalidate TIFF type for the new symbol/exchange
        lockInputs(!symboilValidated);
        setPowerValues();
        if (side == TradeMeta.BUY) {
            cmbAction.setSelectedItem(Language.getString("BUY"));
        } else if (side == TradeMeta.SELL) {
            cmbAction.setSelectedItem(Language.getString("SELL"));
        } else {
            cmbAction.setEnabled(false);
        }
        Theme.registerComponent(this);
        //currencyFormat = new DecimalFormat("#,##0.00");
        //priceFormat = new DecimalFormat("#,##0.00");
        changeFormat = new DecimalFormat("#,##0.00");
        quantityFormat = new DecimalFormat("#,##0");
        upColor = Color.green.darker();
        downColor = Color.red.darker();
        normalColor = Color.black;

        try {
            TradingShared.getTrader().addAccountListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setTitle(Language.getString("NEW_ORDER"));

        setInitialPrice();
        if (quantity > 0) {

            txtQty.setText(String.valueOf(quantity));
        }
        if (side == TradeMeta.SELL) {
            try {
                if (TradingShared.isT0ordersEnable(exchange)) {
                    if (bookKeeper != null) {
                        //donothing
                    } else {
                        bookKeeper = getSelectedBookKeeper();
                    }
                    TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecord(SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper), selectedPortfolio);
                    if (t0SellChkBox.isSelected()) {
                        try {
                            txtQty.setText(String.valueOf((transactRecord.getTPlusDayNetHolding() - transactRecord.getTPlusDaySellPending())));

                        } catch (Exception e) {
                            txtQty.setText("");
                        }
                    } else {
                        txtQty.setText(String.valueOf((transactRecord.getQuantity() - transactRecord.getPledged() - transactRecord.getPendingSell() - transactRecord.getTPlusPendingStock() - transactRecord.getTPlusDayNetHolding())));

                    }
                }
            } catch (Exception e) {
                //do nothing on very first time
            }
        }

        if (selectedStock != null) {
            boolean isSymbolTypeOption = isSymbolTypeOption(selectedStock.getInstrumentType());
            populateAccountStatusPanel(isFuturesSymbol(selectedStock.getInstrumentType()));
            populateInfoPanel(isSymbolTypeOption);
            populateOrderDataPanel(isSymbolTypeOption);
            setTitle(selectedStock.getInstrumentType());
        }
        updateMarginPanel();

        this.setDetachable(true);

        GUISettings.applyOrientation(this);
//        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
//        selectedOrderType = ((TWComboItem) cmbType.getSelectedItem()).getId().toCharArray()[0];
        Thread thread = new Thread(this, "TransactionDialog");
        thread.start();
        currentTransaction = null;
    }

    private boolean isFuturesSymbol(int instruemntType) {
        switch (instruemntType) {
            case Meta.INSTRUMENT_FUTURE:
            case Meta.INSTRUMENT_FUTURE_SPREAD:
                return true;
            default:
                return false;
        }
    }

    private boolean isSymbolTypeOption(int instruemntType) {
        switch (instruemntType) {
            case Meta.INSTRUMENT_FUTURE:
            case Meta.INSTRUMENT_FUTURE_SPREAD:
            case Meta.INSTRUMENT_OPTION:
            case Meta.INSTRUMENT_EQUITY_OPTION:
            case Meta.INSTRUMENT_INDEX_OPTION:
            case Meta.INSTRUMENT_FUTURE_OPTION:
                return true;
            default:
                return false;
        }
    }

    private void setTitle(int instrument) {
        switch (instrument) {
            case Meta.INSTRUMENT_FUTURE:
            case Meta.INSTRUMENT_FUTURE_SPREAD:
                this.setTitle(Language.getString("ORDER_ENTRY_TITLE_FUTURES"));
                break;
            case Meta.INSTRUMENT_OPTION:
            case Meta.INSTRUMENT_EQUITY_OPTION:
            case Meta.INSTRUMENT_INDEX_OPTION:
            case Meta.INSTRUMENT_FUTURE_OPTION:
                this.setTitle(Language.getString("ORDER_ENTRY_TITLE_OPTIONS"));
                break;
            default:
                this.setTitle(Language.getString("ORDER_ENTRY_TITLE_EQUITY"));
                break;
        }
    }


    public void setVisible(boolean visible) {
//        HEIGHT = this.getPreferredSize().height;
        if (windowMode == CANCEL) {
//            HEIGHT = HEIGHT + 22;
            this.setSize(WIDTH, HEIGHT + 22);
        } else if (windowMode == AMEND) {
//            HEIGHT = HEIGHT + 22;
            this.setSize(WIDTH, HEIGHT + 22);
        } else {
            this.setSize(WIDTH, HEIGHT);
        }

        try {
            txtQty.requestFocus();
        } catch (Exception e) {
        }
        this.requestFocus();
        super.setVisible(visible);
        try {
            txtQty.requestFocus();
        } catch (Exception e) {
        }
        blocked = visible;
    }

    private void setStopLossTypes(ArrayList list) {
        list.clear();
        list.add(new TWComboItem(TradingShared.STOP_PRICE_TYPE_LIMIT, TradingShared.STOP_PRICE_TYPE_LIMIT_STR));
        list.add(new TWComboItem(TradingShared.STOP_PRICE_TYPE_TRAILING, TradingShared.STOP_PRICE_TYPE_TRAILING_STR));
        list.add(new TWComboItem(TradingShared.STOP_PRICE_TYPE_PER_TRAILING, TradingShared.STOP_PRICE_TYPE_PER_TRAILING_STR));
    }

    private void createUI() {
        setResizable(false);
        setIconifiable(false);
        setMaximizable(false);
        setClosable(true);
        setLayer(GUISettings.TOP_LAYER);

        cardLayout = new CardLayout(0, 0);
        cardPanel = new JPanel(cardLayout);
        cardPanel.add(cardPanelBracket, getBracketCardPanel());
        cardPanel.add(cardPanelStraddle, getStradleCardPanel());
        cardPanel.add(cardPanelStopLoss, getStopLossCardPanel());
        cardPanel.add(cardPanelBlank, getBlankCardPanel());
        cardPanel.setVisible(false);
        cardPanel.setOpaque(true);
        cardPanel.setPreferredSize(new Dimension(WIDTH, 80));

        normalTabPanel = new JPanel();
        normalTabPanel.setLayout(new ColumnLayout());
        normalTabPanel.setSize(WIDTH, 150);
        normalTabPanel.add(getOrderDataPanel());
        normalTabPanel.add(cardPanel);
        normalTabPanel.add(getAmendDataPanel());
        normalTabPanel.add(getAccountStatusPanel());

        advanceTabPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"55", "72"}));
        advanceTabPanel.add(getConditionPanel());
        advanceTabPanel.add(getSliceOrderPanel());
        advanceTabPanel.setSize(normalTabPanel.getSize());

        mainTabbedPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "td");
        mainTabbedPane.addTab(Language.getString("TAB_NORMAL"), normalTabPanel);
        mainTabbedPane.addTab(Language.getString("TAB_ADVANCE"), advanceTabPanel);
        mainTabbedPane.selectTab(0);

        ColumnLayout mainLayout = new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE);
        getContentPane().setLayout(mainLayout);
        getContentPane().add(getLoginDetailsPanel());
        getContentPane().add(getBuyingPowerPanel());
        getContentPane().add(getPortfolioPanel());
        getContentPane().add(getNamePanel());
        getContentPane().add(getInfoPanel());
        getContentPane().add(mainTabbedPane);
        getContentPane().add(getButtonPanel());

        validateButtons(side);
        validateStopLoss(side);

        setResizable(false);

        pack();


        setInitialGoodTill();
        createDatePopup();
        validateTIFFTypes();
        validatePriceInputs();
        validateTradingWindowTypes();
        focuspolicy = new TradingFocusTraversalPolicy();
        this.setFocusTraversalPolicy(focuspolicy);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    private void createDatePopup() {
        datePopup = new JPopupMenu();

        mnuDay = new TWMenuItem(TradingShared.TIFF_DAY);
        mnuDay.setActionCommand("DAY");
        mnuDay.addActionListener(this);
        mnuDay.setVisible(true);
        datePopup.add(mnuDay);

        mnuIOC = new TWMenuItem(TradingShared.TIFF_IOC);
        mnuIOC.setActionCommand("IOC");
        mnuIOC.addActionListener(this);
        mnuIOC.setVisible(true);
        datePopup.add(mnuIOC);

        mnuFillOrKill = new TWMenuItem(TradingShared.TIFF_FOK);
        mnuFillOrKill.setActionCommand("FOQ");
        mnuFillOrKill.addActionListener(this);
        mnuFillOrKill.setVisible(true);
        datePopup.add(mnuFillOrKill);

        mnuAOP = new TWMenuItem(TradingShared.TIFF_AOP);
        mnuAOP.setActionCommand("AOP");
        mnuAOP.addActionListener(this);
        mnuAOP.setVisible(true);
        datePopup.add(mnuAOP);

        mnuGTD = new TWMenuItem(TradingShared.TIFF_GTD);
        mnuGTD.setActionCommand("GTD");
        mnuGTD.addActionListener(this);
        mnuGTD.setVisible(true);
        datePopup.add(mnuGTD);

        mnuWeek = new TWMenuItem(TradingShared.TIFF_WEEK);
        mnuWeek.setActionCommand("WEEK");
        mnuWeek.addActionListener(this);
        mnuWeek.setVisible(true);
        datePopup.add(mnuWeek);

        mnuMonth = new TWMenuItem(TradingShared.TIFF_MONTH);
        mnuMonth.setActionCommand("MONTH");
        mnuMonth.addActionListener(this);
        mnuMonth.setVisible(true);
        datePopup.add(mnuMonth);

        mnuGTC = new TWMenuItem(TradingShared.TIFF_GTC);
        mnuGTC.setActionCommand("GTC");
        mnuGTC.addActionListener(this);
        mnuGTC.setVisible(true);
        datePopup.add(mnuGTC);

        mnuGTX = new TWMenuItem(TradingShared.TIFF_GTX);
        mnuGTX.setActionCommand("GTX");
        mnuGTX.addActionListener(this);
        mnuGTX.setVisible(true);
        datePopup.add(mnuGTX);

        GUISettings.applyOrientation(datePopup);
    }

    private void validateTradingWindowTypes() {
        if (isEnableBracketOrderPanel || hasStopLossOrders) {
            cardPanel.setVisible(true);
        } else {
            cardPanel.setVisible(false);
        }
        if (isEnabledAdvacedTabPanel) {
            mainTabbedPane.setTabLineVisible(true);
        } else {
            mainTabbedPane.setTabLineVisible(false);
        }
    }

    public JPanel createConditionTab() {

        JPanel conditionPanel = new JPanel();
        conditionPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));
        preConComponent = new ConditionPanelComponent(this);
        ammConComponent = new ConditionPanelComponent(this);
        canConComponent = new ConditionPanelComponent(this);
        conditionTabbedPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "td");
//        conditionTabbedPane.setTabPlacement(JTabbedPane.TOP);
        try {
            conditionTabbedPane.addTab(Language.getString("PRE_CONDITION"), preConComponent);
            conditionTabbedPane.addTab(Language.getString("AMMEND_CONDITION"), ammConComponent);
            conditionTabbedPane.addTab(Language.getString("CANCEL_CONDITION"), canConComponent);
            conditionTabbedPane.showTab(0);
            conditionPanel.add(conditionTabbedPane);
        } catch (Exception e) {
            System.out.println("UI Exception.............");
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return conditionPanel;
    }

    private void populateSliceOrderStatuses() {
        execTypeMethods.clear();
        execTypeMethods.add(new TWComboItem("*", Language.getString("SLICE_TYPE_NONE")));
        execTypeMethods.add(new TWComboItem(TradeMeta.SLICE_ORD_TYPE_ALL, Language.getString("SLICE_TYPE_ALL")));
        execTypeMethods.add(new TWComboItem(TradeMeta.SLICE_ORD_TYPE_ICEBURG, Language.getString("SLICE_TYPE_ICEBURG")));
        if (BrokerConfig.getSharedInstance().isSliceOrderTimeInrevalAvailable(TradingShared.getTrader().getPath(selectedPortfolio))) {     // enable disable time interval
            execTypeMethods.add(new TWComboItem(TradeMeta.SLICE_ORD_TYPE_TIME_INTERVAL, Language.getString("SLICE_TYPE_TIME_INTERVAL")));
        }
    }

    private void setPanelLabels() {
        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
        if (stock != null) {
            if (stock.getInstrumentType() == Meta.INSTRUMENT_MUTUALFUND) {
                lblLast.setText(Language.getString("YTD"));
                lblTPrice.setText(Language.getString("NAV"));
                lblChange.setText(Language.getString("3_YEAR"));
                lblBidOffer.setText(Language.getString("CURRENCY"));
                lblHighLow.setText(Language.getString("5_YEAR"));
                lblMinMax.setText("");
                selectOrderType(TradeMeta.ORDER_TYPE_MARKET);
                cmbType.setEnabled(false);
            } else if (SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.SYMBOL_TYPE_OPTIONS) {
                lblLast.setText(Language.getString("LAST"));
                lblTPrice.setText(Language.getString("PRICE"));
                lblChange.setText(Language.getString("CHANGE") + " ( % )");
                lblBidOffer.setText(Language.getString("BID_TRADE") + " / " + Language.getString("OFFER_TRADE"));
                lblHighLow.setText(Language.getString("LOW_TRADE") + " / " + Language.getString("HIGH_TRADE"));
                lblMinMax.setText(Language.getString("MIN_PRICE_SHORT") + " / " + Language.getString("MAX_PRICE_SHORT"));
                cmbType.setEnabled(true);
            } else {
                lblLast.setText(Language.getString("LAST"));
                lblTPrice.setText(Language.getString("PRICE"));
                lblChange.setText(Language.getString("CHANGE") + " ( % )");
                lblBidOffer.setText(Language.getString("BID_TRADE") + " / " + Language.getString("OFFER_TRADE"));
                lblHighLow.setText(Language.getString("LOW_TRADE") + " / " + Language.getString("HIGH_TRADE"));
                lblMinMax.setText(Language.getString("MIN_PRICE_SHORT") + " / " + Language.getString("MAX_PRICE_SHORT"));
                cmbType.setEnabled(true);
            }
        }
    }

    private void populateMethods() {
        conditionMethods.add(new TWComboItem(0, Language.getString("SLICE_TYPE_NONE")));
        conditionMethods.add(new TWComboItem(TradeMeta.CONDITION_TRIGGER_LAST, Language.getString("CONDITION_LAST")));
        conditionMethods.add(new TWComboItem(TradeMeta.CONDITION_TRIGGER_BID, Language.getString("CONDITION_BID")));
        conditionMethods.add(new TWComboItem(TradeMeta.CONDITION_TRIGGER_ASK, Language.getString("CONDITION_OFFER")));
        conditionMethods.add(new TWComboItem(TradeMeta.CONDITION_TRIGGER_MIN, Language.getString("CONDITION_MIN")));
        conditionMethods.add(new TWComboItem(TradeMeta.CONDITION_TRIGGER_MAX, Language.getString("CONDITION_MAX")));
        cmbConditionMethods.setSelectedIndex(0);
        cmbConditionMethods.updateUI();
    }

    private void populateValues() {
        conditionOperators.add(new TWComboItem(TradeMeta.CONDITION_OPERATOR_GREATER_THAN_OR_EQ, Language.getString("GREATER_THAN_OR_EQUAL")));
        conditionOperators.add(new TWComboItem(TradeMeta.CONDITION_OPERATOR_LESS_THAN_OR_EQ, Language.getString("LESS_THAN_OR_EQUAL")));
        conditionOperators.add(new TWComboItem(TradeMeta.CONDITION_OPERATOR_EQUAL, Language.getString("EQUAL")));

        if (executionMode == MODE_PROGRAMMED) {
            if (side == BUY) {
                cmbConditionOperators.setSelectedIndex(1);
            } else {
                cmbConditionOperators.setSelectedIndex(0);
            }
        }
        cmbConditionOperators.updateUI();
    }

    private void validateTIFFTypes() {
        try {

            String[] tifs = null;
            String tiff = null;
            Interpreter interpreter = loadInterpriter(-1);
            Rule rule = RuleManager.getSharedInstance().getRule("TIF_WITH_TYPE", exchange, "TIF");
            if (rule != null) {
                try {
                    tiff = (String) interpreter.eval(rule.getRule());
                    tifs = tiff.split(",");
                } catch (Exception evalError) {
                    evalError.printStackTrace();
                    tifs = null;
                }
            } else {
                rule = RuleManager.getSharedInstance().getRule("TIF", exchange, "TIF");
                try {
                    if (rule == null) {
                        tifs = null;
                    } else {
                        tifs = rule.getRule().split(",");
                    }
                } catch (Exception e) {
                    tifs = null;
                }
            }
            datePopup.removeAll();
            if (tifs == null) {
                datePopup.add(mnuDay);
                datePopup.add(mnuIOC);
                datePopup.add(mnuFillOrKill);
                datePopup.add(mnuAOP);
                datePopup.add(mnuGTD);
                datePopup.add(mnuWeek);
                datePopup.add(mnuMonth);
                datePopup.add(mnuGTC);
                datePopup.add(mnuGTX);
            } else {
                for (int i = 0; i < tifs.length; i++) {
                    try {
                        int tifType = Integer.parseInt(tifs[i]);
                        switch (tifType) {
                            case TradeMeta.TIF_DAY:
                                datePopup.add(mnuDay);
                                break;
                            case TradeMeta.TIF_GTC:
                                datePopup.add(mnuGTC);
                                break;
                            case TradeMeta.TIF_AOP:
                                datePopup.add(mnuAOP);
                                break;
                            case TradeMeta.TIF_IOC:
                                datePopup.add(mnuIOC);
                                break;
                            case TradeMeta.TIF_FOK:
                                datePopup.add(mnuFillOrKill);
                                break;
                            case TradeMeta.TIF_GTD:
                                datePopup.add(mnuGTD);
                                break;
                            case TradeMeta.TIF_GTX:
                                datePopup.add(mnuGTX);
                                break;
                            case TradeMeta.TIF_WEEK:
                                datePopup.add(mnuWeek);
                                break;
                            case TradeMeta.TIF_MONTH:
                                datePopup.add(mnuMonth);
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
            setInitialGoodTill();
        } catch (Exception e) {
        }
    }

    private void setPowerValues() {
        try {

            String portfolioId = ((TWComboItem) cmbPortfolioNos.getSelectedItem()).getId();
            Account account = TradingShared.getTrader().findAccountByPortfolio(portfolioId);
            if (account != null) {
                if (side == BUY) {
                    DecimalFormat format = new DecimalFormat("#,###.###");
                    double buyingpower = 0.00;
                    boolean marginBuyingpower = false;
                    double symbolMargin = TradeMethods.getMarginForSymbol(exchange, symbol, instrument, selectedPortfolio,
                            dayOrder.isSelected(), account.getMarginLimit(), account.getDayMarginLimit());
                    String stockCurr = TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(selectedStock.getCurrencyCode());
                    if (isMarginApplicableForPortfolio(selectedPortfolio, selectedStock.getKey()) && isMarginMapsValidForSymbol(new TradeKey(SharedMethods.getExchangeFromKey(selectedStock.getKey()), SharedMethods.getSymbolFromKey(selectedStock.getKey()), SharedMethods.getInstrumentTypeFromKey(selectedStock.getKey())))) {
                        marginBuyingpower = true;
                        buyingpower = MarginCalculator.getSharedInstance().getBuyingPowerForNewOrder(portfolioId, selectedStock.getKey(), getCommisionVal());
                        if (buyingpower < 0) {
                            buyingpower = 0;
                        }
                    } else {
                        buyingpower = account.getBuyingPower();
                    }
                    lblBuyingPower.setText(Language.getString("BUYING_POWER") + " " + stockCurr + " " +
                            format.format(convertToSelectedCurrency(stockCurr, account.getCurrency(), buyingpower)));
//                    if (symbolMargin > 0) {
//                        TradingPortfolioRecord portfolio = TradingShared.getTrader().getPortfolio(selectedPortfolio);
//                        if (dayOrder.isSelected()) {
//                            if (MarginSymbolStore.getSharedInstance().isMarginSymbolAvailable(SharedMethods.getKey(exchange, symbol, instrument))) {
//                                double haircut = 0;
//                                try {
//                                    haircut = (MarginSymbolStore.getSharedInstance().getMarginSymbolRecord(SharedMethods.getKey(exchange, symbol, instrument))).getMarginDayBuypct();
//                                } catch (Exception e) {
//                                    haircut = 1;
//                                }
//                                double adjustedBuyingpower = account.getBuyingPower() + account.getDayMarginLimit() + (portfolio.getDayMarginPct() * haircut); //- portfolio.getMarginBlocked() - portfolio.getMarginDue();
//                                lblBuyingPower.setText(Language.getString("BUYING_POWER") + " " + stockCurr + " " +
//                                        format.format(convertToSelectedCurrency(stockCurr, account.getCurrency(), adjustedBuyingpower)));
//                            } else {
//                                lblBuyingPower.setText(Language.getString("BUYING_POWER") + " " + stockCurr + " " +
//                                        format.format(convertToSelectedCurrency(stockCurr, account.getCurrency(), buyingpower)));
//                            }
//                        } else {
//
//                            lblBuyingPower.setText(Language.getString("BUYING_POWER") + " " + stockCurr + " " +
//                                    format.format(convertToSelectedCurrency(stockCurr, account.getCurrency(), buyingpower)));
//                        }
//                    } else {
//                        TradingPortfolioRecord portfolio = TradingShared.getTrader().getPortfolio(selectedPortfolio);
//                        if (dayOrder.isSelected()) {
//                            if (MarginSymbolStore.getSharedInstance().isMarginSymbolAvailable(SharedMethods.getKey(exchange, symbol, instrument))) {
//                                double haircut = 0;
//                                try {
//                                    haircut = (MarginSymbolStore.getSharedInstance().getMarginSymbolRecord(SharedMethods.getKey(exchange, symbol, instrument))).getMarginDayBuypct();
//                                } catch (Exception e) {
//                                    haircut = 1;
//                                }
//                                double adjustedBuyingpower = account.getBuyingPower() + account.getDayMarginLimit() + (portfolio.getDayMarginPct() * haircut);// - portfolio.getMarginBlocked() - portfolio.getMarginDue();
//                                lblBuyingPower.setText(Language.getString("BUYING_POWER") + " " +
//                                        stockCurr + " " + format.format(convertToSelectedCurrency(stockCurr, account.getCurrency(), adjustedBuyingpower)));
//                            } else {
//                                lblBuyingPower.setText(Language.getString("BUYING_POWER") + " " +
//                                        stockCurr + " " + format.format(convertToSelectedCurrency(stockCurr, account.getCurrency(), account.getBuyingPower())));
//                            }
//                        } else {
//                            lblBuyingPower.setText(Language.getString("BUYING_POWER") + " " +
//                                    stockCurr + " " + format.format(convertToSelectedCurrency(stockCurr, account.getCurrency(), account.getBuyingPower())));
//                        }
//                    }
                    format = null;
                    //  if (netValue > 0) {
                    updateMarginPanel();
                    //  }
                } else {
                    DecimalFormat format = new DecimalFormat("#,###");
                    if (instrument == Meta.INSTRUMENT_FUTURE) {
                        if (side == BUY) {
                            lblBuyingPower.setText(Language.getString("OPEN_SELL_COUNT") + " : " +
                                    format.format(TradePortfolios.getInstance().getOpenSellCount(selectedPortfolio, SharedMethods.getKey(exchange, symbol, instrument))));
                        } else {
                            lblBuyingPower.setText(Language.getString("OPEN_BUY_COUNT") + " : " +
                                    format.format(TradePortfolios.getInstance().getOpenBuyCount(selectedPortfolio, SharedMethods.getKey(exchange, symbol, instrument))));
                        }
                    } else {
                        if (TradingShared.isT0ordersEnable(exchange)) {
                            String bookKeeper = getSelectedBookKeeper();
                            if (bookKeeper != null) {
                                //donothing
                            } else {
                                bookKeeper = "";
                            }
                            TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecord(SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper), selectedPortfolio);
                            if (t0SellChkBox.isSelected()) {
                                try {
                                    lblBuyingPower.setText(Language.getString("AVAILABLE_QUANTITY") + " : " +
                                            format.format(transactRecord.getTPlusDayNetHolding() - transactRecord.getTPlusDaySellPending()));
                                } catch (Exception e) {
                                    lblBuyingPower.setText(Language.getString("AVAILABLE_QUANTITY") + " : " +
                                            format.format(0));
                                }
                            } else {
                                lblBuyingPower.setText(Language.getString("AVAILABLE_QUANTITY") + " : " +
                                        format.format(transactRecord.getQuantity() - transactRecord.getPledged() - transactRecord.getPendingSell() - transactRecord.getTPlusPendingStock() - transactRecord.getTPlusDayNetHolding()));
                            }
                        } else {
                            lblBuyingPower.setText(Language.getString("AVAILABLE_QUANTITY") + " : " +
                                    format.format(holding - pendingSellQty));
                        }
                    }
                    format = null;
                }
                account = null;
            }
        } catch (Exception e) {
            ///e.printStackTrace();
        }
    }

    private synchronized void populatePortfolios() {
        try {
            cmbPortfolioNos.removeItemListener(this);
            if (TradingShared.getTrader() != null) {
                cmbPortfolioNos.removeAllItems();
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    Account account = TradingShared.getTrader().findAccountByPortfolio(record.getPortfolioID());
//                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName() + " - " + account.getCurrency());
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());
                    cmbPortfolioNos.addItem(item);
                    if ((selectedPortfolio != null) && (selectedPortfolio.equals(record.getPortfolioID()))) {
                        cmbPortfolioNos.setSelectedIndex(i);
                    }
                    account = null;
                    item = null;
                    record = null;
                }
                cmbPortfolioNos.updateUI();
                if (selectedPortfolio == null) {
                    selectedPortfolio = ((TWComboItem) cmbPortfolioNos.getSelectedItem()).getId();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbPortfolioNos.addItemListener(this);
    }

    private JPanel getBuyingPowerPanel() {
        createMarginViewsPannel();
        powerPanel = new JPanel();
        powerPanel.setOpaque(true);
        powerPanel.setBackground(Color.white);
//        String[] powerWidths = {"100%", "195"};
        String[] powerWidths = {"100%", "220"};
        String[] powerHeights = {"20"};
        FlexGridLayout powerPanelLayout = new FlexGridLayout(powerWidths, powerHeights, 10, 3);
        powerPanel.setLayout(powerPanelLayout);
        lblBuyingPower = new JLabel("", JLabel.LEADING);
        lblBuyingPower.setForeground(Color.green.darker());
        if (side == TradeMeta.BUY) {
            lblBuyingPower.setText(Language.getString("BUYING_POWER"));
        } else {
            lblBuyingPower.setText(Language.getString("AVAILABLE_QUANTITY"));
        }
        lblBuyingPower.setFont(lblBuyingPower.getFont().deriveFont(Font.BOLD, lblBuyingPower.getFont().getSize() + 3));
        powerPanel.add(lblBuyingPower);
        powerPanel.add(pnlMarginViews);
        pnlMarginViews.setVisible(false);
        return powerPanel;
    }

    private JPanel getLoginDetailsPanel() {
        loginDetailPanel = new JPanel();
        String sTradeUserStatus = TradingShared.TRADE_USER_STATUS;
        lblLogingDetail = new CustomizableLable(sTradeUserStatus);

        ImageIcon imgTraderUser = null;
        try {
            imgTraderUser = new ImageIcon("images/Common/icon-user.gif");
        } catch (Exception e) {
            imgTraderUser = null;
            System.out.println("icon is null");
        }

        JLabel lblUserIcon = new JLabel("");
        lblUserIcon.setIcon(imgTraderUser);
        // lblLogingDetail .setPreferredSize(new Dimension(80, 20));
        loginDetailPanel.setLayout(new BoxLayout(loginDetailPanel, BoxLayout.LINE_AXIS));
        loginDetailPanel.add(Box.createHorizontalGlue());
        if ((TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY) != null) && (!TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY).equals(""))) {
            loginDetailPanel.add(lblLogingDetail);
        } else {
            loginDetailPanel.add(new CustomizableLable(""));
        }
        loginDetailPanel.add(lblUserIcon);
        loginDetailPanel.add(Box.createHorizontalGlue());

        loginDetailPanel.setVisible(false);
        if (TradingShared.isConnected()) {
            loginDetailPanel.setVisible(true);
        }
        return loginDetailPanel;
    }

    private JPanel getNamePanel() {
        // Company Name
        namePanel = new JPanel();
        namePanel.setOpaque(true);
        namePanel.setBackground(Color.white);

        String[] nameWidths = {"100%"};
        String[] nameHeights = {"20"};
        FlexGridLayout namePanelLayout = new FlexGridLayout(nameWidths, nameHeights, 10, 3);
        namePanel.setLayout(namePanelLayout);
        companyName = new JLabel();
        companyName.setOpaque(true);
        companyName.setBackground(Color.white);
        companyName.setForeground(Color.blue);
        companyName.setFont(companyName.getFont().deriveFont(Font.BOLD, companyName.getFont().getSize() + 4));
        companyName.setHorizontalAlignment(SwingConstants.LEADING);
        namePanel.add(companyName);
        namePanel.setPreferredSize(new Dimension(WIDTH, 30));
        return namePanel;
    }

    private JPanel getPortfolioPanel() {
        portfolioDataPanel = new JPanel();
        portfolioDataPanel.setOpaque(true);
        FlexGridLayout portfolioPanelLayout = null;
        if (TradingShared.isBookKeepersAvailable()) {
            portfolioDataPanel.setPreferredSize(new Dimension(WIDTH, 50));
            String[] heights = {"20", "20"};
            String[] newWidths = {"22%", "28%", "50%"};
            portfolioPanelLayout = new FlexGridLayout(newWidths, heights, 10, 3);
        } else {
            portfolioDataPanel.setPreferredSize(new Dimension(WIDTH, 26));
            String[] heights = {"20"};
            String[] newWidths = {"22%", "28%", "50%"};
            portfolioPanelLayout = new FlexGridLayout(newWidths, heights, 10, 3);
        }
//        portfolioDataPanel.setBackground(new Color(0xE9E9E9));
        portfolioDataPanel.setLayout(portfolioPanelLayout);

        InfoLabel lblPortfolioNo = new InfoLabel(Language.getString("PORTFOLIO_NUMBER"));
        cmbPortfolioNos = new TWComboBox();
        cmbPortfolioNos.setOpaque(true);
//        cmbPortfolioNos.setBackground(Color.white);

        InfoLabel lblOrderNo = new InfoLabel(Language.getString("ORDER_NUMBER"));
        txtOrderNo = new InfoLabel();
        txtOrderNo.setBorder(BorderFactory.createEtchedBorder());
        txtOrderNo.setOpaque(true);
        txtOrderNo.setBackground(Color.white);
        txtOrderNo.setHorizontalAlignment(SwingConstants.RIGHT);

        bookKeepersLbl = new InfoLabel(Language.getString("BOOKKEEPER"));
        bookKeepers = new ArrayList<TWComboItem>();
        TWComboModel bookKeepersModel = new TWComboModel(bookKeepers);
        bookKeepersCombo = new TWComboBox(bookKeepersModel);

        portfolioDataPanel.add(lblPortfolioNo);
        portfolioDataPanel.add(cmbPortfolioNos);
        portfolioDataPanel.add(new LableValePairOrdered(lblOrderNo, txtOrderNo));
        portfolioDataPanel.add(bookKeepersLbl);
        portfolioDataPanel.add(bookKeepersCombo);

        return portfolioDataPanel;
    }

    private JPanel getBlankCardPanel() {
        blankCardPanel = new JPanel();
        blankCardPanel.setOpaque(false);
        if (hasStopLossOrders) {
            blankCardPanel.setPreferredSize(new Dimension(WIDTH, 24));
        } else {
            blankCardPanel.setPreferredSize(new Dimension(WIDTH, 80));
        }
        return blankCardPanel;
    }

    private JPanel getStopLossCardPanel() {
        stopLossCardPanel = new JPanel(new FlexGridLayout(new String[]{"20%", "29%", "51%"}, new String[]{"20"}, 11, 0));
        stopLossCardPanel.setOpaque(false);
        stopLossCardPanel.setPreferredSize(new Dimension(WIDTH, 24));
        stopLossCardField = new TWTextField();
        stopLossCardPanel.add(new JLabel(Language.getString("STOP_LOSS")));
        stopLossCardPanel.add(stopLossCardField);
        return stopLossCardPanel;
    }

    private JPanel getStradleCardPanel() {
        stradlePanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"23", "23"}, 9, 1, true, true));
        stradlePanel.setOpaque(true);

        stopPriceTypesList = new ArrayList<TWComboItem>();
        setStopLossTypes(stopPriceTypesList);
        cmbStopPrice = new TWComboBox(new TWComboModel(stopPriceTypesList));
        cmbStopPrice.setSelectedIndex(0);
        JLabel lblStopPrice = new JLabel(Language.getString("STOP_PRICE"));
        txtStopPrice = new TWTextField();
        txtStopPrice.setHorizontalAlignment(SwingConstants.RIGHT);
        txtStopPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));

        stopPricePanel = new JPanel(new FlexGridLayout(new String[]{"22%", "14%", "13%", "50%"}, new String[]{"20"}));
        stopPricePanel.setOpaque(false);
        stopPricePanel.add(lblStopPrice);
        stopPricePanel.add(cmbStopPrice);
        stopPricePanel.add(txtStopPrice);

        JPanel internalStradle = new JPanel(new FlexGridLayout(new String[]{"22%", "27%", "50%"}, new String[]{"22"}));
        internalStradle.setOpaque(false);
        stradlelblTP = new JLabel(Language.getString("TAKE_PROFIT"));
        stradleTakeProf = new TWTextField();
        stradleTakeProf.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        stradleTakeProf.setEnabled(true);
        internalStradle.add(stradlelblTP);
        internalStradle.add(stradleTakeProf);
        internalStradle.add(new JLabel(""));

        stradlePanel.add(stopPricePanel);
        stradlePanel.add(internalStradle);

        stradlePanel.setPreferredSize(new Dimension(WIDTH, 80));
        stradleTakeProf.setPreferredSize(new Dimension(100, 20));

        return stradlePanel;
    }

    private JPanel getBracketCardPanel() {
        bracketPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"23", "23", "23"}, 9, 1, true, true));
        bracketPanel.setOpaque(true);

        JPanel internalBracketSL = new JPanel(new FlexGridLayout(new String[]{"22%", "14%", "13%", "50%"}, new String[]{"20"}));
        internalBracketSL.setOpaque(false);
        JPanel internalBracketTP = new JPanel(new FlexGridLayout(new String[]{"22%", "27%", "50%"}, new String[]{"20"}));
        internalBracketTP.setOpaque(false);

        bracketCheck = new JCheckBox(Language.getString("SELL_BRACKET"));
        bracketCheck.setOpaque(false);
        bracketCheck.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                setBrackerOrderPanelStatus(bracketCheck.isSelected());
            }
        });
        bracketCheck.setActionCommand("BRACKET");

        bracketlblSL = new JLabel(Language.getString("STOP_LOSS"));
        bracketStopLoss = new TWTextField();
        bracketTrailStopLoss = new TWTextField();
        bracketStopLoss.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        bracketTrailStopLoss.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        bracketTrailStopLoss.setText("0");
        bracketlblTP = new JLabel(Language.getString("TAKE_PROFIT"));
        bracketTakeProf = new TWTextField();
        bracketTakeProf.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        bracketSLTypes = new ArrayList<TWComboItem>();
        setStopLossTypes(bracketSLTypes);
        bracketcmbSL = new TWComboBox(new TWComboModel(bracketSLTypes));
        internalBracketSL.add(bracketlblSL);
        internalBracketSL.add(bracketcmbSL);
        internalBracketSL.add(bracketStopLoss);
        internalBracketTP.add(bracketlblTP);
        internalBracketTP.add(bracketTakeProf);
        internalBracketTP.add(new JLabel(""));

        JPanel brackectCheckPanel = new JPanel(new FlexGridLayout(new String[]{"0", "100%"}, new String[]{"20"}));
        brackectCheckPanel.setOpaque(false);
        brackectCheckPanel.add(bracketCheck);
        brackectCheckPanel.add(new JLabel(""));

        bracketPanel.add(brackectCheckPanel);
        bracketPanel.add(internalBracketSL);
        bracketPanel.add(internalBracketTP);
        bracketPanel.setPreferredSize(new Dimension(WIDTH, 80));
        bracketcmbSL.setSelectedIndex(0);
        bracketcmbSL.setOpaque(false);
        setBrackerOrderPanelStatus(false);
        bracketPanel.addKeyListener(this);

        return bracketPanel;
    }

    private JPanel getInfoPanel() {
        // Info panel
        infoPanel = new JPanel();
        infoPanel.setOpaque(true);
        infoPanel.setBackground(Color.white);

//        JPanel symbolDataPanel = new JPanel();
//        symbolDataPanel.setOpaque(true);
//        symbolDataPanel.setBackground(Color.white);

        String[] heights2 = {"20", "20", "20", "20"};
        FlexGridLayout symbolPanelLayout = new FlexGridLayout(widths2, heights2, 10, 3);
        infoPanel.setLayout(symbolPanelLayout);

        lblSymbol = new InfoLabel(Language.getString("SYMBOL"));
        lblLast = new InfoLabel(Language.getString("LAST"));
        txtLast = new InfoLabel();
        txtLast.setHorizontalAlignment(SwingConstants.RIGHT);
        lblChange = new InfoLabel(Language.getString("CHANGE") + " ( % )");
        txtChange = new InfoLabel();
        txtChange.setHorizontalAlignment(SwingConstants.RIGHT);
        lblBidOffer = new InfoLabel(Language.getString("BID_TRADE") + " /" + Language.getString("OFFER_TRADE"));
        txtBidOffer = new InfoLabel();
        txtBidOffer.addMouseListener(this);
        txtBidOffer.setHorizontalAlignment(SwingConstants.RIGHT);
        lblHighLow = new InfoLabel(Language.getString("LOW_TRADE") + " / " + Language.getString("HIGH_TRADE"));
        txtHighLOw = new InfoLabel();
        txtHighLOw.setHorizontalAlignment(SwingConstants.RIGHT);
        lblMinMax = new InfoLabel(Language.getString("MIN_PRICE_SHORT") + " / " + Language.getString("MAX_PRICE_SHORT"));
        txtMinMax = new InfoLabel();
        txtMinMax.setHorizontalAlignment(SwingConstants.RIGHT);
        txtMinMax.addMouseListener(this);

        lblExpiryDate = new InfoLabel(Language.getString("EXPIRY_DATE"));
        txtExpiryDate = new InfoLabel();
        txtExpiryDate.setHorizontalAlignment(SwingConstants.RIGHT);
        txtExpiryDate.addMouseListener(this);

        lblLotSize = new InfoLabel(Language.getString("LOT_SIZE"));
        txtLotSize = new InfoLabel();
        txtLotSize.setHorizontalAlignment(SwingConstants.RIGHT);
        txtLotSize.addMouseListener(this);

        infoPanel.add(lblSymbol);
        infoPanel.add(getSymbolPanel());
        infoPanel.add(new LableValePair(lblBidOffer, txtBidOffer));

        infoPanel.add(lblLast);
        infoPanel.add(txtLast);
        infoPanel.add(new LableValePair(lblHighLow, txtHighLOw));

        infoPanel.add(lblChange);
        infoPanel.add(txtChange);
        infoPanel.add(new LableValePair(lblMinMax, txtMinMax));

        infoPanel.add(lblExpiryDate);
        infoPanel.add(txtExpiryDate);
        infoPanel.add(new LableValePair(lblLotSize, txtLotSize));

        infoPanel.setPreferredSize(new Dimension(WIDTH, 70));

        return infoPanel;
    }

    private JPanel getConditionPanel() {
        String[] conditionHeights = {"20"};
        FlexGridLayout conditionLayout = new FlexGridLayout(conditionWidths, conditionHeights, 5, 3);
        conditionPanel = new JPanel(conditionLayout);
        conditionPanel.setOpaque(true);
        conditionMethods = new ArrayList<TWComboItem>();
        cmbConditionMethods = new TWComboBox(new TWComboModel(conditionMethods));
        conditionPanel.add(cmbConditionMethods);
        populateMethods();
        cmbConditionMethods.addItemListener(this);

        conditionOperators = new ArrayList<TWComboItem>();
        cmbConditionOperators = new TWComboBox(new TWComboModel(conditionOperators));
        conditionPanel.add(cmbConditionOperators);
        cmbConditionOperators.addItemListener(this);
        populateValues();

        ValueFormatter conditionPriceFormatter = new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL);
        txtConditionValue = new TWTextField();
        txtConditionValue.setHorizontalAlignment(JLabel.RIGHT);
        txtConditionValue.setDocument(conditionPriceFormatter);
        conditionPanel.add(txtConditionValue);

        JLabel lblExpiry = new JLabel(Language.getString("EXPIRY"));
        conditionPanel.add(lblExpiry);

        conditionPanel.add(getConditionExpiryCombo());

        cmbConditionOperators.setEnabled(false);
        txtConditionValue.setEnabled(false);
        btnShowConditionDays.setEnabled(false);

        Font fnt = new Font("Arial", 1, 12);
        conditionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Theme.getColor("BUYSELL_BORDER_COLOR")), Language.getString("CONDITION"), 0, 0, fnt, Theme.getColor("BUYSELL_BORDER_TITLE_COLOR")));
        conditionPanel.setOpaque(true);

        /*if (executionMode != TradeMeta.MODE_PROGRAMMED) {
            conditionPanel.setVisible(false);
        }*/
        conditionPanel.setVisible(true);
        cmbConditionOperators.setEnabled(false);
        txtConditionValue.setEnabled(false);
        btnShowConditionDays.setEnabled(false);

        return conditionPanel;
    }

    private JPanel getSliceOrderPanel() {
        Font fnt = new Font("Arial", 1, 12);
        executionPanel = new JPanel(new FlexGridLayout(widths, new String[]{"20", "20"}, 2, 2));
        executionPanel.setSize(WIDTH, 45);
        executionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Theme.getColor("BUYSELL_BORDER_COLOR")), Language.getString("SLICE_ORDERS_PANEL"), 0, 0, fnt, Theme.getColor("BUYSELL_BORDER_TITLE_COLOR")));
        executionPanel.setOpaque(true);
        txtexecBlockValue = new TWTextField();
        txtexecBlockValue.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        txtexecBlockValue.setSize(100, 20);
        txtexecTimeValue = new TWTextField();
        txtexecTimeValue.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        txtexecTimeValue.setSize(100, 20);
        execTypeMethods = new ArrayList<TWComboItem>();
        cmbexecTypeOperators = new TWComboBox(new TWComboModel(execTypeMethods));
        cmbexecTypeOperators.setSize(100, 20);
        cmbexecTypeOperators.setEnabled(false);
        populateSliceOrderStatuses();
        cmbexecTypeOperators.addItemListener(this);
        cmbexecTypeOperators.setSelectedIndex(0);
        JLabel execLabel = new JLabel(Language.getString("SLICE_ORD_EXEC_TYPE"));
        execLabel.setOpaque(false);
        executionPanel.add(execLabel);
        executionPanel.add(cmbexecTypeOperators);
        if (isEnableSliceOrders) {
            cmbexecTypeOperators.setEnabled(true);
        } else {
            cmbexecTypeOperators.setEnabled(false);
        }

        if (isEnableSliceOrdersInterval) {         //
            JLabel timeLabel = new JLabel(Language.getString("SLICE_ORD_TIME_INTERVAL"));
            timeLabel.setOpaque(false);
            executionPanel.add(timeLabel);
            executionPanel.add(txtexecTimeValue);
        } else {
            executionPanel.add(new JLabel());
            executionPanel.add(new JLabel());
        }
        JLabel blockLabel = new JLabel(Language.getString("SLICE_ORD_BLOCK_SIZE"));
        blockLabel.setOpaque(false);
        executionPanel.add(blockLabel);
        executionPanel.add(txtexecBlockValue);
        executionPanel.add(new JLabel(""));
        executionPanel.add(new JLabel(""));

        txtexecBlockValue.setEnabled(false);
        txtexecTimeValue.setEnabled(false);


        return executionPanel;
    }

    private JPanel getAmendDataPanel() {
        JLabel lblFilled = new JLabel(Language.getString("FILLED_QTY"));
        txtFilled = new TWTextField();
        txtFilled.setHorizontalAlignment(SwingConstants.RIGHT);
        txtFilled.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        txtFilled.setEnabled(false);

        lblPending = new JLabel(Language.getString("PENDING_QTY"));
        txtRemaining = new TWTextField();
        txtRemaining.setHorizontalAlignment(SwingConstants.RIGHT);
        txtRemaining.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        txtRemaining.setEnabled(false);

        filledQtyPanel = new TWDataPanel(widthsForDataPanel, 20);
        filledQtyPanel.setOpaque(false);
        filledQtyPanel.add(lblFilled);
        filledQtyPanel.add(txtFilled);

        remQtyPanel = new TWDataPanel(widthsForDataPanel, 20);
        remQtyPanel.setOpaque(false);
        remQtyPanel.add(lblPending);
        remQtyPanel.add(txtRemaining);

        amendDataPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"20"}, 10, 1));

        amendDataPanel.setVisible(false);
        amendDataPanel.setOpaque(false);

        amendDataPanel.add(filledQtyPanel);
        amendDataPanel.add(remQtyPanel);

        amendDataPanel.setPreferredSize(new Dimension(WIDTH, 22));

        return amendDataPanel;
    }

    private JPanel getOrderDataPanel() {
        dataPanel = new JPanel();
        dataPanel.setOpaque(true);

        FlexGridLayout dataPanelLayout;
        if (isEnabledDayOrder) {
            dataPanelLayout = new FlexGridLayout(dataPanelWidths, new String[]{"20", "20", "20", "20", "22", "20"}, 10, 3);
            /*if (isTPlusEnable) {
                if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 149));
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 140));//95
                }
            } else {
                dataPanel.setPreferredSize(new Dimension(WIDTH, 140));//95
            }*/
        } else {
            dataPanelLayout = new FlexGridLayout(dataPanelWidths, new String[]{"20", "20", "20", "20", "20", "20"}, 10, 3);
            /*if (isTPlusEnable) {
                if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 118));
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
                }
            } else {
                dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
            }*/
        }
        dataPanel.setLayout(dataPanelLayout);

        JLabel lblAction = new JLabel(Language.getString("ACTION"));
        String[] actions = {Language.getString("BUY"), Language.getString("SELL")};
        //  cmbAction = new JComboBox(actions);
        cmbAction = new TWComboBox(actions);
        cmbAction.addActionListener(this);
        JLabel lblType = new JLabel(Language.getString("ORDER_TYPE"));

        orderTypesList = new ArrayList<TWComboItem>();
        validatedSymbols = new ArrayList<String>();
        TradeMethods.getSharedInstance().populateOrderTypes(exchange, orderTypesList, TradingShared.getTrader().getPath(selectedPortfolio));
        TWComboModel typesModel = new TWComboModel(orderTypesList);
        cmbType = new TWComboBox(typesModel);
        try {
            cmbType.setSelectedIndex(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbType.addActionListener(this);
        lblTPrice = new JLabel(Language.getString("PRICE"));
        txtTPrice = new TWTextField();
        txtTPrice.setHorizontalAlignment(SwingConstants.RIGHT);
        ValueFormatter priceFormatter = new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL);
        priceFormatter.addDocumentListener(this);
        txtTPrice.setDocument(priceFormatter);
//        txtTPrice.addKeyListener(this);

        txtTPrice.registerKeyboardAction(this, "ENTER", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
//        txtTPrice.registerKeyboardAction(this, "ENTER", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
        dataPanel.registerKeyboardAction(this, "ENTER", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
//        dataPanel.registerKeyboardAction(this, "ENTER", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
        priceSpinner = new JScrollBar(JScrollBar.VERTICAL) {
            public void setUI(ScrollBarUI ui) {
                super.setUI(new MetalScrollBarUI());    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        priceSpinner.setRequestFocusEnabled(false);
        priceSpinner.addAdjustmentListener(this);
        priceSpinner.setValue(0);
        priceSpinner.setMaximum(Integer.MAX_VALUE);
        priceSpinner.setMinimum(Integer.MIN_VALUE);
        JPanel pricePanel = new JPanel();
        String[] pricePanelWidths = {"100%", "0"};
        String[] pricePanelHeights = {"20"};
        pricePanel.setLayout(new FlexGridLayout(pricePanelWidths, pricePanelHeights));
        pricePanel.add(txtTPrice);
        pricePanel.add(priceSpinner);

        lblTQty = new JLabel(Language.getString("QUANTITY"));
        ValueFormatter qtyFormatter = new ValueFormatter(ValueFormatter.POSITIVE_INTEGER);
        qtyFormatter.addDocumentListener(this);
        quantitySpinner = new JScrollBar(JScrollBar.VERTICAL) {
            public void setUI(ScrollBarUI ui) {
                super.setUI(new MetalScrollBarUI());    //To change body of overridden methods use File | Settings | File Templates.
            }
        };

        quantitySpinner.setRequestFocusEnabled(false);
        quantitySpinner.addAdjustmentListener(this);
        quantitySpinner.setValue(0);
        quantitySpinner.setMaximum(Integer.MAX_VALUE);
        quantitySpinner.setMinimum(Integer.MIN_VALUE);
        JPanel quantityPanel = new JPanel();
        String[] quantityPanelWidths = {"100%", "0"};
        String[] quantityPanelHeights = {"20"};
        quantityPanel.setLayout(new FlexGridLayout(quantityPanelWidths, quantityPanelHeights));
        txtQty = createDoubleFieldWithDecimalsAndCustomFormatter();
        txtQty.addFocusListener(this);
        txtQty.addMouseListener(this);
        quantityPanel.add(txtQty);
        quantityPanel.add(quantitySpinner);

        JLabel lblMinFill = new JLabel(Language.getString("MIN_FILL"));
//        txtMinFill = new TWTextField();
//        txtMinFill.setHorizontalAlignment(SwingConstants.RIGHT);
//        txtMinFill.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));

        txtMinFill = createDoubleFieldWithDecimalsAndCustomFormatter();
        txtMinFill.setHorizontalAlignment(SwingConstants.RIGHT);
        txtMinFill.addFocusListener(this);
        txtMinFill.addMouseListener(this);

        JLabel lblAllOrNone = new JLabel(Language.getString("ALL_OR_NONE"));
        txtAllOrNone = new JCheckBox();
        txtAllOrNone.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                txtMinFill.setEditable(!txtAllOrNone.isSelected());
            }
        });

        JLabel lblDgTill = new JLabel(Language.getString("GOOD_TILL"));

        JLabel lblDisclosed = new JLabel(Language.getString("DISCLOSED"));
        txtDisclosed = new TWTextField();
        txtDisclosed.setHorizontalAlignment(SwingConstants.RIGHT);
        txtDisclosed.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        txtDisclosed.addKeyListener(this);

        t0SellLbl = new JLabel(Language.getString("SELL_T+0"));
        t0SellChkBox = new JCheckBox();
        t0SellChkBox.addActionListener(this);

        t0Holdings = new JLabel(Language.getString("HOLDINGS_T+0"));
        t0HoldingsFdl = new TWTextField();

        actionPanel = new TWDataPanel(widthsForDataPanel, 20);
        actionPanel.setOpaque(false);
        actionPanel.add(lblAction);
        actionPanel.add(cmbAction);


        typePanel = new TWDataPanel(widthsForDataPanel, 20);
        typePanel.setOpaque(false);
        typePanel.add(lblType);
        typePanel.add(cmbType);


        lPricePanel = new TWDataPanel(widthsForDataPanel, 20);
        lPricePanel.setOpaque(false);
        lPricePanel.add(lblTPrice);
        lPricePanel.add(pricePanel);


        aonPanel = new TWDataPanel(widthsForDataPanel, 20);
        aonPanel.setOpaque(false);
        aonPanel.add(lblAllOrNone);
        aonPanel.add(txtAllOrNone);


        qtyPanel = new TWDataPanel(widthsForDataPanel, 20);
        qtyPanel.setOpaque(false);
        qtyPanel.add(lblTQty);
        qtyPanel.add(quantityPanel);


        gtdPanel = new TWDataPanel(widthsForDataPanel, 20);
        gtdPanel.setOpaque(false);
        gtdPanel.add(lblDgTill);
        gtdPanel.add(getDateCombo());


        minFillPanel = new TWDataPanel(widthsForDataPanel, 20);
        minFillPanel.setOpaque(false);
        minFillPanel.add(lblMinFill);
        minFillPanel.add(txtMinFill);


        disclosedPanel = new TWDataPanel(widthsForDataPanel, 20);
        disclosedPanel.setOpaque(false);
        disclosedPanel.add(lblDisclosed);
        disclosedPanel.add(txtDisclosed);


        t0SellPanel = new TWDataPanel(widthsForDataPanel, 20);
        t0SellPanel.setOpaque(false);
        t0SellPanel.add(t0SellLbl);
        t0SellPanel.add(t0SellChkBox);


        t0HoldingsPanel = new TWDataPanel(widthsForDataPanel, 20);
        t0HoldingsPanel.setOpaque(false);
        t0HoldingsPanel.add(t0Holdings);
        t0HoldingsPanel.add(t0HoldingsFdl);


        if (side == TradeMeta.BUY) {
            t0SellChkBox.setEnabled(false);
            t0HoldingsFdl.setEditable(false);
        } else {
            t0SellChkBox.setEnabled(true);
            t0HoldingsFdl.setEditable(false);
        }

        //reference ID Added under Ruwan's requiremet, done by Chandika
        refIDPanel = new TWDataPanel(widthsForDataPanel, 20);
        refID = new TWTextField();
        refID.setDocument(new LimitedLengthDocument(12));
        refID.setEditable(true);
        refIDPanel.setOpaque(false);
        refIDPanel.add(new JLabel(Language.getString("REFERENCE_ID")));
        refIDPanel.add(refID);

        dayOrder = new JCheckBox();
        dayOrder.setOpaque(false);
        dayOrder.setSelected(TradingShared.isSelectDayOrder());
        dayOrder.addActionListener(this);
        dayOrderCheckPanel = new TWDataPanel(widthsForDataPanel, 20);
        dayOrderCheckPanel.setOpaque(false);
        dayOrderCheckPanel.add(new JLabel(Language.getString("DAY_ORDER_TYPE")));
        dayOrderCheckPanel.add(dayOrder);

        dataPanel.add(actionPanel);
        dataPanel.add(typePanel);
        dataPanel.add(lPricePanel);
        dataPanel.add(aonPanel);
        dataPanel.add(qtyPanel);
        dataPanel.add(gtdPanel);
        dataPanel.add(minFillPanel);
        dataPanel.add(disclosedPanel);
        dataPanel.add(t0SellPanel);
        dataPanel.add(t0HoldingsPanel);
        dataPanel.add(refIDPanel);
        dataPanel.add(dayOrderCheckPanel);

        if (isEnabledDayOrder) {
            if (isTPlusEnable) {
                if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 149 - 25));
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 140 - 25));//95
                }
            } else {
                dataPanelLayout.setRowHeight(4, "1");
                t0HoldingsPanel.setVisible(false);
                t0SellPanel.setVisible(false);
                dataPanel.setPreferredSize(new Dimension(WIDTH, 120 - 25));//95
                SwingUtilities.updateComponentTreeUI(dataPanel);
            }
        } else {
            if (isTPlusEnable) {
                if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 118));
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
                }
            } else {
                dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
            }
        }

        return dataPanel;
    }


    private JNumberEntryField<Double> createDoubleFieldWithDecimalsAndCustomFormatter() {

        final JNumberEntryField<Double> formattedField = new JNumberEntryField<Double>(Double.NaN, 14, 2);
        formattedField.setFormatter(new TextEntryFormatter() {
            public String getTextForDisplay(JTextEntryField textEntryField, String validText) {
                String value = "";
                if ((((JNumberEntryField) textEntryField).getNumber().doubleValue() == Double.NaN) || (validText.equals("0"))) {
                    validText = "";
                    value = "";
                } else {
                    value = NumberFormat.getNumberInstance().format((Object) ((JNumberEntryField<?>) textEntryField).getNumber());

                }
                return value;
            }
        });
//           txtQty.addFocusListener(this);
//           txtQty.addMouseListener(this);
        formattedField.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                // System.out.println("key event1" + e.getKeyCode());
            }

            public void keyPressed(KeyEvent e) {
                //   System.out.println("key event2" + e.getKeyCode());
                try {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        if (windowMode == AMEND) {
                            executeTransaction(TradeMeta.AMEND, queued, selectedQID);
                        } else if (windowMode == CANCEL) {
                            executeTransaction(TradeMeta.CANCEL, queued, selectedQID);
                        } else {
                            changeType();
                            executeTransaction(side, queued, selectedQID);
                        }
                    }
                } finally {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER)
                        e.consume();
                }
            }

            public void keyReleased(KeyEvent e) {
                try {
//                  System.out.println("222VALUE in Number =="+txtQty.getNumber());
//                   System.out.println("222VALUE in text =="+txtQty.getText());
                    if (e.getSource().equals(txtQty)) {
                        setSpecialConditions();
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                } finally {
//                    e.consume();
                }
                try {
//                  System.out.println("222VALUE in formatter =="+ NumberFormat.getNumberInstance().format(txtQty.getNumber()));
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return formattedField;
    }


    private JPanel getAccountStatusPanel() {
        String[] statusWidths = {"25%", "25%", "50%"};
        String[] statusHeights = {"20", "20", "20"};
        FlexGridLayout statusPanelLayout = new FlexGridLayout(statusWidths, statusHeights, 10, 3);
        accountStatusPanel = new JPanel(statusPanelLayout);
        accountStatusPanel.setOpaque(true);
        accountStatusPanel.setBackground(Color.white);

        lblHoldongs = new InfoLabel(Language.getString("HOLDING"));
        txtHoldongs = new InfoLabel("0");
        txtHoldongs.setHorizontalAlignment(SwingConstants.RIGHT);
        lblOrderValue = new InfoLabel(Language.getString("ORDER_VALUE"));
        txtOrderValue = new InfoLabel("0");
        txtOrderValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblBuyPending = new InfoLabel(Language.getString("BUY_PENDING"));
        txtBuyPending = new InfoLabel("0");
        txtBuyPending.setHorizontalAlignment(SwingConstants.RIGHT);
        lblCommission = new InfoLabel(Language.getString("COMMISSION"));
        txtCommission = new InfoLabel("0");
        txtCommission.setHorizontalAlignment(SwingConstants.RIGHT);
        lblSellPending = new InfoLabel(Language.getString("SELL_PENDING"));
        txtSellPending = new InfoLabel("0");
        txtSellPending.setHorizontalAlignment(SwingConstants.RIGHT);
        lblNetValue = new InfoLabel(Language.getString("NET_VALUE"));
        txtNetValue = new InfoLabel("0");
        txtNetValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblInitMargin = new InfoLabel(Language.getString("INITIAL_MARGIN"));
        txtInitMargin = new InfoLabel("0");
        txtInitMargin.setHorizontalAlignment(SwingConstants.RIGHT);
        lblMainMargin = new InfoLabel(Language.getString("MAINTENANCE_MARGIN"));
        txtMainMargin = new InfoLabel("0");
        txtMainMargin.setHorizontalAlignment(SwingConstants.RIGHT);

        accountStatusPanel.add(lblHoldongs);
        accountStatusPanel.add(txtHoldongs);
        accountStatusPanel.add(new LableValePair(lblOrderValue, txtOrderValue));
//        accountStatusPanel.add(lblOrderValue);
//        accountStatusPanel.add(txtOrderValue);
        accountStatusPanel.add(lblBuyPending);
        accountStatusPanel.add(txtBuyPending);
        accountStatusPanel.add(new LableValePair(lblCommission, txtCommission));
//        accountStatusPanel.add(lblCommission);
//        accountStatusPanel.add(txtCommission);
        accountStatusPanel.add(lblSellPending);
        accountStatusPanel.add(txtSellPending);
        accountStatusPanel.add(new LableValePair(lblNetValue, txtNetValue));
//        accountStatusPanel.add(lblNetValue);
//        accountStatusPanel.add(txtNetValue);

        accountStatusPanel.setPreferredSize(new Dimension(WIDTH, 75));

        return accountStatusPanel;
    }

    private JPanel getButtonPanel() {
        buttonPanel = new JPanel();
        BoxLayout buttonLayout = new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS);
        buttonPanel.setLayout(buttonLayout);
        buttonPanel.setOpaque(true);
        buttonPanel.setPreferredSize(new Dimension(WIDTH, 35));

        btnQ = new TWButton(Language.getString("ADD_TO_BASKET"));
        btnQ.addActionListener(this);
        buttonPanel.add(btnQ);

        btnOrderBook = new JButton("<HTML><U><font color=blue>" + Language.getString("ORDER_BOOK") + "</font></U>");
        btnOrderBook.setVisible(false);
        btnOrderBook.addActionListener(this);
        btnOrderBook.setBorder(BorderFactory.createEmptyBorder());
        btnOrderBook.setContentAreaFilled(false);
        btnOrderBook.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonPanel.add(btnOrderBook);

        btnOrderCal = new JButton("<HTML><U><font color=blue>" + Language.getString("PRICE_CALC") + "</font></U>");
        btnOrderCal.setVisible(false);
        btnOrderCal.addActionListener(this);
        btnOrderCal.setBorder(BorderFactory.createEmptyBorder());
        btnOrderCal.setContentAreaFilled(false);
        btnOrderCal.setCursor(new Cursor(Cursor.HAND_CURSOR));
        buttonPanel.add(btnOrderCal);

        buttonPanel.add(Box.createHorizontalGlue());

        btnBuy = new TWButton(Language.getString("BUY"));
        btnBuy.addActionListener(this);
        buttonPanel.add(btnBuy);
        btnSell = new TWButton(Language.getString("SELL"));
        btnSell.addActionListener(this);
        buttonPanel.add(btnSell);
        btnAmend = new TWButton(Language.getString("AMEND_ORDER"));
        btnAmend.addActionListener(this);
        buttonPanel.add(btnAmend);
        btnCancel = new TWButton(Language.getString("CANCEL_ORDER"));
        btnCancel.addActionListener(this);
        buttonPanel.add(btnCancel);
        btnClose = new TWButton(Language.getString("CLOSE"));
        btnClose.addActionListener(this);
        JLabel space = new JLabel();
        space.setPreferredSize(new Dimension(3, 35));
        buttonPanel.add(space);
        buttonPanel.add(btnClose);

        return buttonPanel;
    }

    private JPanel getSymbolPanel() {
        symbolsPanel = new JPanel(new BorderLayout());
        symbolsPanel.addMouseListener(this);
        txtSymbol = new TWTextField();
        txtSymbol.setDocument(new ValueFormatter(ValueFormatter.UPPERCASE, 20L));
        txtSymbol.setText(symbol);
        txtSymbol.setBorder(null);
        txtSymbol.addKeyListener(this);
        txtSymbol.addFocusListener(this);
        symbolsPanel.add(txtSymbol, BorderLayout.CENTER);
        TradeMethods.getSharedInstance().populateOrderTypes(exchange, orderTypesList, TradingShared.getTrader().getPath(selectedPortfolio));
        btnSelectSymbol = new TWButton(new DownArrow());
        btnSelectSymbol.setBorder(null);
        symbolsPanel.add(btnSelectSymbol, BorderLayout.EAST);
        btnSelectSymbol.setPreferredSize(new Dimension(20, 20));
        btnSelectSymbol.addActionListener(this);
        btnSelectSymbol.setActionCommand("SS");
        symbolsPanel.setBorder(BorderFactory.createEtchedBorder());
        return symbolsPanel;
    }

    private void populateOrderDataPanel(boolean isSymbolTypeOption) {
        dataPanel.removeAll();
        if (!isSymbolTypeOption) {
            if (isEnabledDayOrder) {
                dataPanel.setLayout(new FlexGridLayout(dataPanelWidths, new String[]{"20", "20", "20", "20", "22", "20"}, 10, 3));  //todo changed
                if (isTPlusEnable) {
                    if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                        dataPanel.setPreferredSize(new Dimension(WIDTH, 149 - 25));        //day odrders are no longer used
                    } else {
                        dataPanel.setPreferredSize(new Dimension(WIDTH, 140 - 25));//95
                    }
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 120 - 25));//95
                }
            } else {
                dataPanel.setLayout(new FlexGridLayout(dataPanelWidths, new String[]{"20", "20", "20", "20", "20", "20"}, 10, 3));
                if (isTPlusEnable) {
                    if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                        dataPanel.setPreferredSize(new Dimension(WIDTH, 118));
                    } else {
                        dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
                    }
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
                }
            }
            dataPanel.add(actionPanel);
            dataPanel.add(typePanel);
            dataPanel.add(lPricePanel);
            dataPanel.add(aonPanel);
            dataPanel.add(qtyPanel);
            dataPanel.add(gtdPanel);
            dataPanel.add(minFillPanel);
            dataPanel.add(disclosedPanel);
            dataPanel.add(t0SellPanel);
            dataPanel.add(t0HoldingsPanel);
            dataPanel.add(refIDPanel);
            dataPanel.add(dayOrderCheckPanel);
        } else {
            if (isEnabledDayOrder) {
                dataPanel.setLayout(new FlexGridLayout(dataPanelWidths, new String[]{"20", "20", "20", "22", "20"}, 10, 3));
                if (isTPlusEnable) {
                    if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                        dataPanel.setPreferredSize(new Dimension(WIDTH, 124 - 25));
                    } else {
                        dataPanel.setPreferredSize(new Dimension(WIDTH, 115 - 25));//95
                    }
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 95 - 25));//95
                }
            } else {
                dataPanel.setLayout(new FlexGridLayout(dataPanelWidths, new String[]{"20", "20", "20", "20", "20"}, 10, 3));
                if (isTPlusEnable) {
                    if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                        dataPanel.setPreferredSize(new Dimension(WIDTH, 93));
//                        dataPanel.setPreferredSize(new Dimension(WIDTH, 118));
                    } else {
                        dataPanel.setPreferredSize(new Dimension(WIDTH, 70));
//                        dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
                    }
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 70));
//                    dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
                }
            }
            dataPanel.add(actionPanel);
            dataPanel.add(typePanel);
            dataPanel.add(lPricePanel);
            dataPanel.add(aonPanel);
            dataPanel.add(qtyPanel);
            dataPanel.add(gtdPanel);
            dataPanel.add(t0SellPanel);
            dataPanel.add(t0HoldingsPanel);
            dataPanel.add(refIDPanel);
            dataPanel.add(dayOrderCheckPanel);
        }

        dataPanel.doLayout();
    }

    private void populateAccountStatusPanel(boolean isFuturesSymbol) {
        accountStatusPanel.removeAll();
        String[] statusWidths = {"50%", "50%"};
        String[] statusHeights = {"20", "20", "20"};
        FlexGridLayout statusPanelLayout = new FlexGridLayout(statusWidths, statusHeights, 10, 3);
        accountStatusPanel.setLayout(statusPanelLayout);

        if (!isFuturesSymbol) {
//            accountStatusPanel.add(lblHoldongs);
            accountStatusPanel.add(new LableValePair(lblHoldongs, txtHoldongs));
            accountStatusPanel.add(new LableValePair(lblOrderValue, txtOrderValue));
//            accountStatusPanel.add(lblOrderValue);
//            accountStatusPanel.add(txtOrderValue);

//            accountStatusPanel.add(lblBuyPending);
            accountStatusPanel.add(new LableValePair(lblBuyPending, txtBuyPending));
            accountStatusPanel.add(new LableValePair(lblCommission, txtCommission));
//            accountStatusPanel.add(lblCommission);
//            accountStatusPanel.add(txtCommission);

//            accountStatusPanel.add(lblSellPending);
            accountStatusPanel.add(new LableValePair(lblSellPending, txtSellPending));
            accountStatusPanel.add(new LableValePair(lblNetValue, txtNetValue));
//            accountStatusPanel.add(lblNetValue);
//            accountStatusPanel.add(txtNetValue);
        } else {
//            accountStatusPanel.add(lblOrderValue);
            accountStatusPanel.add(new LableValePair(lblOrderValue, txtOrderValue));
            accountStatusPanel.add(new LableValePair(lblInitMargin, txtInitMargin));
//            accountStatusPanel.add(lblInitMargin);
//            accountStatusPanel.add(txtInitMargin);

//            accountStatusPanel.add(lblCommission);
            accountStatusPanel.add(new LableValePair(lblCommission, txtCommission));
            accountStatusPanel.add(new LableValePair(lblMainMargin, txtMainMargin));
//            accountStatusPanel.add(lblMainMargin);
//            accountStatusPanel.add(txtMainMargin);

//            accountStatusPanel.add(lblNetValue);
            accountStatusPanel.add(new LableValePair(lblNetValue, txtNetValue));
            accountStatusPanel.add(new LableValePair(new JLabel(""), new JLabel("")));
//            accountStatusPanel.add(new JLabel(""));
//            accountStatusPanel.add(new JLabel(""));
        }
        accountStatusPanel.doLayout();
    }

    private void populateInfoPanel(boolean isSymbolTypeOption) {

        infoPanel.removeAll();

        if (!isSymbolTypeOption) {
            String[] heights2 = {"20", "20", "20"};
            FlexGridLayout symbolPanelLayout = new FlexGridLayout(widths2, heights2, 10, 3);
            infoPanel.setLayout(symbolPanelLayout);
            infoPanel.add(lblSymbol);
            infoPanel.add(getSymbolPanel());
            if (focuspolicy != null) {
                focuspolicy.reorderTraversalPolicy();
            }
            infoPanel.add(new LableValePair(lblBidOffer, txtBidOffer));

            infoPanel.add(lblLast);
            infoPanel.add(txtLast);
            infoPanel.add(new LableValePair(lblHighLow, txtHighLOw));

            infoPanel.add(lblChange);
            infoPanel.add(txtChange);
            infoPanel.add(new LableValePair(lblMinMax, txtMinMax));

            infoPanel.setPreferredSize(new Dimension(WIDTH, 70));
        } else {
            String[] heights2 = {"20", "20", "20", "20"};
            FlexGridLayout symbolPanelLayout = new FlexGridLayout(widths2, heights2, 10, 3);
            infoPanel.setLayout(symbolPanelLayout);
            infoPanel.add(lblSymbol);
            infoPanel.add(getSymbolPanel());
            if (focuspolicy != null) {
                focuspolicy.reorderTraversalPolicy();
            }
            infoPanel.add(new LableValePair(lblBidOffer, txtBidOffer));

            infoPanel.add(lblLast);
            infoPanel.add(txtLast);
            infoPanel.add(new LableValePair(lblHighLow, txtHighLOw));

            infoPanel.add(lblChange);
            infoPanel.add(txtChange);
            infoPanel.add(new LableValePair(lblMinMax, txtMinMax));

            infoPanel.add(lblExpiryDate);
            infoPanel.add(txtExpiryDate);
            infoPanel.add(new LableValePair(lblLotSize, txtLotSize));

            infoPanel.setPreferredSize(new Dimension(WIDTH, 95));
        }
        infoPanel.doLayout();
    }

    private void setInitialPrice() {
        if (symbol != null) {
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
            if (stock.getInstrumentType() == Meta.INSTRUMENT_MUTUALFUND) {
                txtTPrice.setText(SharedMethods.getDecimalFormatNoComma(exchange, symbol, instrument).format(stock.getNetAssetValue()));
            } else {
                if (side == TradeMeta.SELL) {
                    if (exchange != null) {
                        if (TWControl.isShowingMinPriceOnLiquidate()) {
                            txtTPrice.setText(SharedMethods.getDecimalFormatNoComma(exchange, symbol, instrument).format(stock.getMinPrice()));
                        } else {
                            txtTPrice.setText(SharedMethods.getDecimalFormatNoComma(exchange, symbol, instrument).format(stock.getBestBidPrice()));
                        }
                    }
                } else if (side == TradeMeta.BUY) {
                    if (exchange != null) {
                        txtTPrice.setText(SharedMethods.getDecimalFormatNoComma(exchange, symbol, instrument).format(stock.getBestAskPrice()));
                    }
                }
            }
            stock = null;
        }
    }

    public void setSquareOrderType(boolean isSquareOrderTrue) {
        /*if(isSquareOrderTrue) {
            try {
                selectOrderType(TradeMeta.ORDER_TYPE_SQUARE_OFF);
                cmbType.setSelectedItem(0);
                cmbType.updateUI();
                txtTQty.setEnabled(false);
                quantitySpinner.setEnabled(false);
                cmbAction.setEnabled(false);

                validatePriceInputs();
                setSpecialConditions();
                cmbType.setEnabled(false);
            } catch(Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }*/
    }

    public void setPrice(double price) {
        try {
            txtTPrice.setText(SharedMethods.getDecimalFormatNoComma(currentExchange, symbol, instrument).format(price));
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void setTransaction(Transaction transaction, int mode) {
        windowMode = mode;
        currentTransaction = transaction;
        txtSymbol.setText(transaction.getSymbol());
        exchange = transaction.getExchange();
        instrument = transaction.getSecurityType();
        symbol = transaction.getSymbol();
        bracketCheck.setSelected(false);
        dayOrder.setSelected(false);
        if (transaction.getSide() == TradeMeta.BUY) {
            cmbAction.setSelectedIndex(0);
        } else {
            cmbAction.setSelectedIndex(1);
        }
        try {
            String portfolio = transaction.getPortfolioNo();
            selectedPortfolio = portfolio;
            for (int i = 0; i < TradingShared.getTrader().getPortfolioCount(); i++) {
                if (portfolio.equals(TradingShared.getTrader().getPortfolioID(i))) {
                    cmbPortfolioNos.setSelectedIndex(i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderTypesList.clear();
//        TradeMethods.getSharedInstance().populateOrderTypes(transaction.getExchange(), orderTypesList);
        TradeMethods.getSharedInstance().populateOrderTypes(transaction.getExchange(), orderTypesList, TradingShared.getTrader().getPath(selectedPortfolio));
        selectOrderType(transaction.getType());
        try {
            TWComboItem item2 = (TWComboItem) cmbType.getSelectedItem();
            type = item2.getId().charAt(0);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        validatePriceInputs();
        setSelectedStock(SharedMethods.getKey(exchange, symbol, instrument));
//        selectedStock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
        if ((instrument == Meta.INSTRUMENT_FUTURE) && (selectedStock != null)) {
            futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, selectedStock.getOptionBaseSymbol(), instrument));
        }
        boolean isSymbolTypeOption = isSymbolTypeOption(selectedStock.getInstrumentType());
        populateAccountStatusPanel(isFuturesSymbol(selectedStock.getInstrumentType()));
        populateInfoPanel(isSymbolTypeOption);
        populateOrderDataPanel(isSymbolTypeOption);
        setTitle(selectedStock.getInstrumentType());

        validatePriceInputs();

        try {

            if (isEnabledDayOrder && transaction.isDayOrder()) {
                dayOrder.setSelected(true);
            } else {
                dayOrder.setSelected(false);
            }

            if (transaction.getStopLossPrice() > 0) {
                bracketStopLoss.setText("" + transaction.getStopLossPrice());
                bracketCheck.setSelected(true);
                bracketStopLoss.setEnabled(true);
                bracketcmbSL.setEnabled(true);
                txtStopPrice.setText("" + transaction.getStopLossPrice());
                txtStopPrice.setEnabled(true);
                cmbStopPrice.setEnabled(true);
            }
            if (transaction.getStopLossType() > -1) {
                TWComboItem item;
                for (int i = 0; i < bracketSLTypes.size(); i++) {
                    item = bracketSLTypes.get(i);
                    if (Integer.parseInt(item.getId()) == transaction.getStopLossType()) {
                        bracketcmbSL.setSelectedItem(item);
                        break;
                    }
                    item = null;
                }
                for (int i = 0; i < stopPriceTypesList.size(); i++) {
                    item = stopPriceTypesList.get(i);
                    if (Integer.parseInt(item.getId()) == transaction.getStopLossType()) {
                        cmbStopPrice.setSelectedItem(item);
                        break;
                    }
                    item = null;
                }
            }
            if (transaction.getTakeProfitPrice() > 0) {
                bracketTakeProf.setText("" + transaction.getTakeProfitPrice());
                bracketCheck.setSelected(true);
                bracketTakeProf.setEnabled(true);
                stradleTakeProf.setText("" + transaction.getTakeProfitPrice());
                stradleTakeProf.setEnabled(true);
            }
            amendDataPanel.setVisible(true);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        txtTPrice.setText(SharedMethods.getDecimalFormatNoComma(exchange, symbol, instrument).format(transaction.getPrice()));
        txtMinFill.setText("" + transaction.getMinQuantity());
        txtDisclosed.setText("" + transaction.getDiscloseQuantity());
        txtFilled.setText("" + transaction.getFilledQuantity());

        if ((windowMode == TradeMeta.AMEND) && (TradingShared.isAmendModeDelta())) {
            /*if (selectedStock.getLotSize() > 0) {
                txtTQty.setText("" + (long)(transaction.getPendingQuantity()/ selectedStock.getLotSize()));
            } else {
                txtTQty.setText("" + transaction.getPendingQuantity());
            }*/
            txtQty.setText(String.valueOf(transaction.getPendingQuantity()));
            txtRemaining.setText("" + transaction.getOrderQuantity());
            remainingQty = transaction.getPendingQuantity();
            totalQty = transaction.getOrderQuantity();
        } else {
            /*if (selectedStock.getLotSize() > 0) {
                txtTQty.setText("" + (long)(transaction.getOrderQuantity()/ selectedStock.getLotSize()));
            } else {
                txtTQty.setText("" + transaction.getOrderQuantity());
            }*/
            txtQty.setText(String.valueOf(transaction.getOrderQuantity()));
            txtRemaining.setText("" + transaction.getPendingQuantity());
        }

        TWComboItem comboItem = new TWComboItem("" + transaction.getPortfolioNo(), TradingShared.getTrader().getPortfolioName("" + transaction.getPortfolioNo()));
        cmbPortfolioNos.setSelectedItem(comboItem);
        selectedPortfolio = transaction.getPortfolioNo();
        cmbPortfolioNos.setEnabled((windowMode != TradeMeta.CANCEL) && (windowMode != TradeMeta.AMEND));
        currentOrderID = transaction.getMubasherOrderNumber();
        txtOrderNo.setText("" + transaction.getClOrderID());

        tiff = (short) transaction.getTIFType();
        goodTillStr = transaction.getExpireDate(); //override the value set by above statement with original
        try {
            goodTillLong = TradingShared.getGoodTillLong(transaction.getExpireDate());
        } catch (Exception e) {
            goodTillLong = 0;
        }
        setGoodTillText(tiff);
        validateButtons(windowMode);

        if (windowMode == CANCEL)
            setTitle(Language.getString("CANCEL_ORDER"));
        else if (windowMode == AMEND)
            setTitle(Language.getString("AMEND_ORDER"));

        if ((transaction.getRule() != null) && ((transaction.getRuleType() == CONDITION_TYPE_NORMAL))) { // this is a conditional order
            conditionalMode = TradeMeta.CONDITION_TYPE_NORMAL;
            cmbConditionOperators.setEnabled(true);
            txtConditionValue.setEnabled(true);
            btnShowConditionDays.setEnabled(true);
            //  String[] fields = transaction.getRule().split(TradeMeta.DD);
            // String[] fields = transaction.getRule().getExpireTime().split(TradeMeta.DD);//todo
            String condtionMethod = transaction.getRule().getConditions().get(0).getConditionParameter();
            String condtionOperator = transaction.getRule().getConditions().get(0).getOperator();
            String value = transaction.getRule().getConditions().get(0).getConditionValue();
            String exp = transaction.getRule().getExpireTime();

            for (TWComboItem item : conditionMethods) {
                //if (item.getId().equals(fields[TradeMeta.CONDITION_FIELD_METHOD])) {
                if (item.getId().equals(condtionMethod)) {
                    cmbConditionMethods.setSelectedItem(item);
                    break;
                }
            }

            for (TWComboItem item : conditionOperators) {
                //  if (item.getId().equals(fields[TradeMeta.CONDITION_FIELD_OPERATOR])) {
                if (item.getId().equals(condtionOperator)) {
                    cmbConditionOperators.setSelectedItem(item);
                    break;
                }
            }
//            txtConditionValue.setText(fields[TradeMeta.CONDITION_FIELD_VALUE1]);
            txtConditionValue.setText(value);
            txtConditionExpiry.setText(TradingShared.formatGoodTill(transaction.getRuleExpireTime()));
        } else if (transaction.getRuleType() == CONDITION_TYPE_STOP_LOSS) {
            conditionalMode = TradeMeta.CONDITION_TYPE_STOP_LOSS;
            cmbConditionOperators.setEnabled(true);
            txtConditionValue.setEnabled(true);
            btnShowConditionDays.setEnabled(true);
        } else if (transaction.getRuleType() == CONDITION_TYPE_BRACKET) {
            conditionalMode = TradeMeta.CONDITION_TYPE_BRACKET;
        } else if (transaction.getRuleType() == CONDITION_TYPE_STRADEL) {
            conditionalMode = TradeMeta.CONDITION_TYPE_STRADEL;
        } else {
            conditionalMode = TradeMeta.CONDITION_TYPE_NONE;
        }
        conditionalMode = transaction.getRuleType();

        conditionPanel.setVisible(true);
        if (conditionalMode == TradeMeta.CONDITION_TYPE_NORMAL) {
            conditionPanel.setEnabled(true);
        } else {
            conditionPanel.setEnabled(false);
        }

        try {
            execTypeMethods.clear();
            if (TradingShared.getSliceOrderType(transaction.getSliceExecType()).equalsIgnoreCase(Language.getString("SLICE_TYPE_NONE"))) {

                execTypeMethods.add(new TWComboItem("*", TradingShared.getSliceOrderType(transaction.getSliceExecType())));
            } else {
                execTypeMethods.add(new TWComboItem("1", TradingShared.getSliceOrderType(transaction.getSliceExecType())));

            }
            cmbexecTypeOperators.setSelectedIndex(0);
            cmbexecTypeOperators.updateUI();
            txtexecBlockValue.setEnabled(false);
            txtexecTimeValue.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (transaction.getSliceBlockSIze() != -1) {
            txtexecBlockValue.setText("" + transaction.getSliceBlockSIze());
            if (windowMode == TradeMeta.AMEND) {
                txtexecBlockValue.setEnabled(true);
            }
        }
        if (transaction.getSliceTimeInterval() != -1) {
            txtexecTimeValue.setText("" + transaction.getSliceTimeInterval());
            if (windowMode == TradeMeta.AMEND) {
                txtexecTimeValue.setEnabled(true);
            }
        }
        if (windowMode == TradeMeta.CANCEL) {
            refID.setEditable(false);
            cmbAction.setEnabled(false);
            cmbType.setEnabled(false);
            txtTPrice.setEnabled(false);
            priceSpinner.setEnabled(false);
            txtQty.setEnabled(false);
            quantitySpinner.setEnabled(false);
            txtMinFill.setEnabled(false);
            txtStopPrice.setEnabled(false);
            cmbStopPrice.setEnabled(false);
            txtDisclosed.setEnabled(false);
            txtAllOrNone.setEnabled(false);
            btnShowDays.setEnabled(false);
            txtSymbol.setEnabled(false);
            btnSelectSymbol.setEnabled(false);
            txtDisclosed.setEnabled(false);
            txtMinFill.setEnabled(false);
            txtAllOrNone.setEnabled(false);
            cmbConditionMethods.setEnabled(false);
            cmbConditionOperators.setEnabled(false);
            txtConditionValue.setEnabled(false);
            btnShowConditionDays.setEnabled(false);
            cmbexecTypeOperators.setEnabled(false);
            txtexecBlockValue.setEnabled(false);
            txtexecTimeValue.setEnabled(false);
            stradleTakeProf.setEnabled(false);
            bracketCheck.setEnabled(false);
            bracketTakeProf.setEnabled(false);
            bracketStopLoss.setEnabled(false);
            bracketTrailStopLoss.setEnabled(false);
            bracketcmbSL.setEnabled(false);
            dayOrder.setEnabled(false);
        } else if (windowMode == TradeMeta.AMEND) {
            refID.setEditable(false);
            txtSymbol.setEnabled(false);
            btnSelectSymbol.setEnabled(false);
            cmbAction.setEnabled(false);
            bookKeepersCombo.setEnabled(false);
            if (transaction.getOrderCategory() == MIXConstants.ORDER_CATEGORY_ALGO) {
                txtQty.setEditable(false);
            }
            if (TradingShared.isAmendModeDelta()) {
                lblTQty.setText(Language.getString("PENDING_QTY"));
                lblPending.setText(Language.getString("QUANTITY"));
            }
            if ((transaction.getSide() == TradeMeta.SELL) && (transaction.getRuleType() == TradeMeta.CONDITION_TYPE_STOP_LOSS)) { // amend a stop loss sell order
                cmbType.setEnabled(false);
                cmbexecTypeOperators.setEnabled(false);
                txtexecBlockValue.setEnabled(false);
                txtexecTimeValue.setEnabled(false);
            } else if (transaction.getRuleType() == TradeMeta.CONDITION_TYPE_BRACKET) {
                cmbType.setEnabled(true);
                txtTPrice.setEnabled(false);
                priceSpinner.setEnabled(false);
                txtQty.setEnabled(false);
                quantitySpinner.setEnabled(false);
                txtDisclosed.setEnabled(false);
                txtAllOrNone.setEnabled(false);
                btnShowDays.setEnabled(false);
                txtDisclosed.setEnabled(false);
                txtMinFill.setEnabled(false);
                txtAllOrNone.setEnabled(false);
                cmbConditionMethods.setEnabled(false);
                cmbConditionOperators.setEnabled(false);
                txtConditionValue.setEnabled(false);
                btnShowConditionDays.setEnabled(false);
                cmbexecTypeOperators.setEnabled(false);
                txtexecBlockValue.setEnabled(false);
                txtexecTimeValue.setEnabled(false);
            } else {
                cmbType.setEnabled(true);
                cmbexecTypeOperators.setEnabled(false);
                txtexecBlockValue.setEnabled(false);
                txtexecTimeValue.setEnabled(false);
            }
        }
        setSpecialConditions();
        updateMarginPanel();
        pack();
    }

    private void selectOrderType(char type) {
        int size = cmbType.getModel().getSize();
        boolean isSelected = false;
        for (int i = 0; i < size; i++) {
            TWComboItem item = (TWComboItem) cmbType.getModel().getElementAt(i);
            if (item.getId().charAt(0) == type) {
                cmbType.setSelectedIndex(i);
                isSelected = true;
                break;
            }
            item = null;
        }
        if (!isSelected) {
            cmbType.removeItemListener(this);
            orderTypesList.clear();
            orderTypesList.add(new TWComboItem("" + type, TradingShared.getTypeString(type)));
            try {
                cmbType.setSelectedIndex(0);
                cmbType.updateUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
            cmbType.addItemListener(this);
        }
    }

    public boolean finalizeWindow() {
        //currentTwOrderID = null;
        Theme.unRegisterComponent(this);
        active = false;
        TradingShared.getTrader().removeAccountListener(this);
        TradingConnectionNotifier.getInstance().removeConnectionListener(this);
        ruleTable.clear();
        return true;
    }

    public void run() {

        while (active) {
            try {
                if ((currentExchange == null) || (!currentExchange.equals(exchange))) {
                    if (exchange != null) {
                        loadRules();
                    }
                    currentExchange = exchange;
                    //SharedMethods.applyDecimalPlaces(priceFormat, ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceDecimalPlaces());
//                    String portfolioId = ((TWComboItem) cmbPortfolioNos.getSelectedItem()).getId();
//                    Account account = TradingShared.getTrader().findAccountByPortfolio(portfolioId);
                    //SharedMethods.applyDecimalPlaces(currencyFormat, (byte)com.isi.csvr.datastore.CurrencyStore.getSharedInstance().getDecimalPlaces(account.getCurrency()));
                }
            } catch (Exception e) {
            }

            if (exchange != null) {
                setSelectedStock(SharedMethods.getKey(exchange, symbol, instrument));
//                selectedStock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
            }

            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPrice)) {
                btnOrderBook.setVisible(true);
            } else {
                btnOrderBook.setVisible(false);
            }

            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_DepthCalculator)) {
                btnOrderCal.setVisible(true);
            } else {
                btnOrderCal.setVisible(false);
            }

            if (selectedStock != null) {
                if (instrument == Meta.INSTRUMENT_FUTURE) {
                    futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, selectedStock.getOptionBaseSymbol(), instrument));
                }
                companyName.setText(selectedStock.getLongDescription() + " : " + symbol);
//                setTitle(Language.getString("NEW_ORDER"));
                currency = selectedStock.getCurrencyCode();
                lblTQty.setText(Language.getString("QUANTITY"));
                lblOrderValue.setText(Language.getString("ORDER_VALUE") + " (" + TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(selectedStock.getCurrencyCode()) + ")");

                if (selectedStock.getInstrumentType() == Meta.INSTRUMENT_MUTUALFUND) {
                    txtBidOffer.setText(selectedStock.getCurrencyCode());
                    txtLast.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getPerformance12M()));
                    txtChange.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getPerformance3Y()));
                    txtHighLOw.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getPerformance5Y()));
                    txtMinMax.setText("");
                } else {
                    if ((SharedMethods.getSymbolType(selectedStock.getInstrumentType()) == Meta.SYMBOL_TYPE_OPTIONS)) {
                        lblTQty.setText(Language.getString("LOTS"));
                        lblOrderValue.setText(Language.getString("CONTRACT_VALUE") + " (" + TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(selectedStock.getCurrencyCode()) + ")");
                    }
                    txtBidOffer.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getBestBidPrice()) + " / " + SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getBestAskPrice()));
                    txtChange.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getChange()) + " (" + changeFormat.format(selectedStock.getPercentChange()) + ")");
                    txtLast.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getLastTradeValue()));
                    txtHighLOw.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getLow()) + " / " + SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getHigh()));
                    if ((selectedStock.getMinPrice() > 0) && (selectedStock.getMaxPrice() > 0)) {
                        txtMinMax.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getMinPrice()) + " / " + SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getMaxPrice()));
                    } else if (selectedStock.getMinPrice() > 0) {
                        txtMinMax.setText(SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getMinPrice()) + " / " + TradingShared.NA);
                    } else if (selectedStock.getMaxPrice() > 0) {
                        txtMinMax.setText(TradingShared.NA + " / " + SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(selectedStock.getMaxPrice()));
                    } else {
                        txtMinMax.setText(TradingShared.NA + " / " + TradingShared.NA);
                    }
                }

                minPrice = selectedStock.getMinPrice();
                maxPrice = selectedStock.getMaxPrice();
                openPrice = selectedStock.getTodaysOpen();
                highPrice = selectedStock.getHigh();
                lowPrice = selectedStock.getLow();
                refPrice = selectedStock.getRefPrice();
                bid = selectedStock.getBestBidPrice();
                offer = selectedStock.getBestAskPrice();
                marketCode = selectedStock.getMarketID();
                if (selectedStock.getChange() > 0) {
                    txtChange.setForeground(upColor);
                } else if (selectedStock.getChange() < 0) {
                    txtChange.setForeground(downColor);
                } else {
                    txtChange.setForeground(normalColor);
                }

                calculateBuySellPending(symbol);
                if (type == TradeMeta.ORDER_TYPE_SQUARE_OFF) {
                    calculateDayOrderPending(symbol);
                    if (dayHolding < 0) {
                        cmbAction.setSelectedIndex(0);
                    } else {
                        cmbAction.setSelectedIndex(1);
                    }
                    txtQty.setText(String.valueOf(Math.abs(dayHolding)));
                }
                setPowerValues();

                try {
                    String bookKeeper = ((TWComboItem) bookKeepersCombo.getSelectedItem()).getId();
                    TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecord(SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper), selectedPortfolio);
                    t0HoldingsFdl.setText("" + transactRecord.getTPlusDayNetHolding());
                } catch (Exception e) {
                    t0HoldingsFdl.setText("");
                }

                if (selectedStock.getExpirationDate() != 0) {
                    txtExpiryDate.setText(SharedMethods.toDisplayDateFormat(selectedStock.getExpirationDate()));
                } else {
                    txtExpiryDate.setText(NA);
                }
                if (selectedStock.getLotSize() > 0) {
                    txtLotSize.setText("" + selectedStock.getLotSize());
                } else {
                    txtLotSize.setText(NA);
                }
            }

            calculateOrderValue();
            //    calculateMarginValues();
            sleep();
        }
    }

    private void setSpecialConditions() {
        try {

            if (windowMode == CANCEL) {
                return;
            }

            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
            try {
                if (stock.getInstrumentType() == Meta.INSTRUMENT_MUTUALFUND) {
                    txtAllOrNone.setSelected(false);
                    txtAllOrNone.setEnabled(false);
                    txtMinFill.setEnabled(false);
                    txtTPrice.setEnabled(false);
                    priceSpinner.setEnabled(false);
                    txtDisclosed.setEnabled(false);
                    txtMinFill.setEnabled(false);
                    return;
                }
            } finally {
                stock = null;
            }

            Interpreter interpreter = loadInterpriter(-1);

            Rule rule = RuleManager.getSharedInstance().getRule("MINFIL_ENABLE", exchange, "NEW_ORDER");
            if (rule != null) {
                try {
                    int result = (Integer) interpreter.eval(rule.getRule());
                    txtMinFill.setEnabled(result == 1);
                    if (result == 0) { //Bug ID <#0022>

                        txtMinFill.setText("0");
                    }
                } catch (Exception evalError) {
                    evalError.printStackTrace();
                    txtMinFill.setEnabled(true);
                }
            } else {
                txtMinFill.setEnabled(true);
            }

            rule = RuleManager.getSharedInstance().getRule("AON_SELECT", exchange, "NEW_ORDER");
            if (rule != null) {
                try {
                    int result = (Integer) interpreter.eval(rule.getRule());
                    txtAllOrNone.setSelected(result == 1);
                } catch (Exception evalError) {
                    evalError.printStackTrace();
                    txtAllOrNone.setSelected(false);
                }
            } else {
                txtAllOrNone.setSelected(false);
            }

            rule = RuleManager.getSharedInstance().getRule("AON_ENABLE", exchange, "NEW_ORDER");
            if (rule != null) {
                try {
                    int result = (Integer) interpreter.eval(rule.getRule());
                    txtAllOrNone.setEnabled(result == 1);
                } catch (Exception evalError) {
                    evalError.printStackTrace();
                    txtAllOrNone.setEnabled(false);
                }
            } else {
                txtAllOrNone.setEnabled(false);
            }

            rule = RuleManager.getSharedInstance().getRule("DISCLOSED_ENABLE", exchange, "NEW_ORDER");
            if (rule != null) {
                try {
                    int result = (Integer) interpreter.eval(rule.getRule());
                    txtDisclosed.setEnabled(result == 1);
                    if (result == 0) { //Bug ID <#0023>
                        txtDisclosed.setText("");
                    }
                } catch (Exception evalError) {
                    evalError.printStackTrace();
                    txtDisclosed.setEnabled(false);
                }
            } else {
                txtDisclosed.setEnabled(false);
            }

        } catch (Exception evalError) {
            //evalError.printStackTrace();
        }
    }

    /*private void setAllOrNoneStatus() {
        try {

            int tradeQty = Integer.parseInt(txtTQty.getText());
            double tradePrice = 0;

            if (exchange == null) return;

            if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                tradePrice = Double.parseDouble(txtTPrice.getText());
            } else {
                if (exchange != null) {
                    Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol);
                    if (stock != null) {
                        if (side == TradeMeta.SELL)
                            tradePrice = stock.getBestBidPrice();
                        else
                            tradePrice = stock.getBestAskPrice();
                    }
                    stock = null;
                }
            }


            Rule rule = RuleManager.getSharedInstance().getRule("AON", exchange, "NEW_ORDER");
            if (rule != null) {
                Interpreter interpreter = new Interpreter();
                interpreter.set("quantity", tradeQty);
                interpreter.set("price", Math.round(tradePrice * 1000) / (double) 1000);
                int result = (Integer) interpreter.eval(rule.getRule());

                boolean aonSelected = (result & 4) == 4; // bit 3
                boolean aonEnabled  = (result & 2) == 2; // bit 2
                boolean minEnabled  = (result & 1) == 1; // bit 1

                txtAllOrNone.setSelected(aonSelected);
                txtAllOrNone.setEnabled(aonEnabled);
                txtMinFill.setEditable(minEnabled);

                if (txtAllOrNone.isSelected()){ // no need to edit the minfill if all or none selected
                    txtMinFill.setEditable(false);
                    txtMinFill.setText("");
                }


                /* Old System. Removed due to limitations 6 mar 2006 Uditha
                    result format
                    AON Selected   MunFill Enabled  ->  result
                       0                0           ->    0
                       0                1           ->    1
                       1                0           ->    2
                       1                1           ->    3

                switch (result) {
                    case 0:
                        txtAllOrNone.setSelected(false);
                        txtAllOrNone.setEnabled(false);
                        txtMinFill.setEditable(true);
                        break;
                    case 1:
                        txtAllOrNone.setSelected(false);
                        txtAllOrNone.setEnabled(true);
                        if (txtAllOrNone.isSelected()){
                            txtMinFill.setEditable(false);
                        }else{
                            txtMinFill.setEditable(true);
                        }
                        break;
                    case 2:
                        txtAllOrNone.setSelected(true);
                        txtAllOrNone.setEnabled(false);
                        txtMinFill.setEditable(false);
                        txtMinFill.setText("");
                        break;
                    case 3:
                        txtAllOrNone.setSelected(true);
                        txtAllOrNone.setEnabled(true);
                        txtMinFill.setEditable(false);
                        txtMinFill.setText("");
                        break;
                    case 4:
                        txtAllOrNone.setSelected(false);
                        txtAllOrNone.setEnabled(false);
                        txtMinFill.setEditable(false);
                        txtMinFill.setText("");
                        break;
                }*
                interpreter = null;
            } else {
                txtAllOrNone.setEnabled(true);
                txtMinFill.setEditable(true);
            }
            rule = null;
        } catch (EvalError evalError) {
            //evalError.printStackTrace();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }*/

    private void resetingValues() {
        txtNetValue.setText("0");
        txtCommission.setText("0");
        txtOrderValue.setText("0");
    }

    private void calculateBuySellPending(String symbol) {
        try {

            String key = SharedMethods.getKey(exchange, symbol, instrument);
            String portfolio = selectedPortfolio;
//            String portfolio = ((TWComboItem) cmbPortfolioNos.getSelectedItem()).getId();
            if (SharedMethods.checkInstrumentType(key, Meta.INSTRUMENT_FUTURE)) {
                if (side == BUY) {
                    holding = TradePortfolios.getInstance().getOpenSellCount(portfolio, key);
                } else {
                    holding = TradePortfolios.getInstance().getOpenBuyCount(portfolio, key);
                }
            } else {
                String bookKeeper = getSelectedBookKeeper();
                if (bookKeeper != null) {
                    //donothing
                } else {
                    bookKeeper = "";
                }
                if (t0SellChkBox.isSelected()) {
                    holding = TradePortfolios.getInstance().getTPlusHoldings(portfolio, SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper));
                } else {
                    holding = TradePortfolios.getInstance().getAvailableQuantity(portfolio, SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper));
                }
            }
            String bookKeeper = getSelectedBookKeeper();
            if (bookKeeper != null) {
                //donothing
            } else {
                bookKeeper = "";
            }
            TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecord(SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper), portfolio);
            pendingBuyQty = TradePortfolios.getInstance().getPendingBuyQuantity(portfolio, SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper));
            if (t0SellChkBox.isSelected()) {
                try {
                    pendingSellQty = transactRecord.getTPlusDaySellPending();
                } catch (Exception e) {
                    pendingSellQty = 0l;
                }
            } else {
                pendingSellQty = TradePortfolios.getInstance().getPendingSellQuantity(portfolio, SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper));
            }

            txtHoldongs.setText(quantityFormat.format(holding));
            txtBuyPending.setText(quantityFormat.format(pendingBuyQty));
            txtSellPending.setText(quantityFormat.format(pendingSellQty));
            portfolio = null;
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private void calculateDayOrderPending(String symbol) {
        try {
            String key = SharedMethods.getKey(exchange, symbol, instrument);
            String portfolio = selectedPortfolio;
//            String portfolio = ((TWComboItem) cmbPortfolioNos.getSelectedItem()).getId();

            dayHolding = TradePortfolios.getInstance().getDayHolding(portfolio, key);
            portfolio = null;
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private void calculateMarginValues() {
        try {
            String portfolio = selectedPortfolio;
//            String portfolio = ((TWComboItem) cmbPortfolioNos.getSelectedItem()).getId();
            long tradeQty = txtQty.getNumber().longValue();
//            long tradeQty = Integer.parseInt(txtTQty.getText());
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
            /*if (stock.getLotSize() > 0) {
                tradeQty  = tradeQty * stock.getLotSize();
            }*/
            Account account = TradingShared.getTrader().findAccountByPortfolio(portfolio);
            String currency = account.getCurrency();
            TWDecimalFormat currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol(), instrument));
            double tradePrice = 0;
            long openBuyCount = 0;
            long openSellCount = 0;
            long prndingBuy = 0;
            long prndingSell = 0;
            double currencyFactor = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(stock.getCurrencyCode());
            if (futureBaseAttributes != null) {
                tradePrice = futureBaseAttributes.getMargin();
                TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecordForFutures(SharedMethods.getKey(exchange, symbol, instrument), selectedPortfolio);
                if (transactRecord != null) {
                    openBuyCount = transactRecord.getOpenBuyCount();
                    openSellCount = transactRecord.getOpenSellCount();
                    prndingBuy = transactRecord.getPendingBuy();
                    prndingSell = transactRecord.getPendingSell();
                }
                if (side == TradeMeta.BUY) {
                    if ((openSellCount - prndingBuy) >= tradeQty) {
                        txtInitMargin.setText("0");
                        txtMainMargin.setText("0");
                    } else {
                        long marginContracts = 0;
                        if (windowMode == NEW) {
                            if (prndingBuy >= openSellCount) {
                                marginContracts = tradeQty;
                            } else {
                                marginContracts = tradeQty - (openSellCount - prndingBuy);
                            }
                        } else if (windowMode == AMEND) {
                            if (tradeQty >= openSellCount) {
                                marginContracts = tradeQty - openSellCount;
                            } else {
                                marginContracts = 0;
                            }
                        }
                        txtInitMargin.setText(currencyFormat.format(marginContracts * tradePrice * currencyFactor));
                        txtMainMargin.setText(currencyFormat.format(marginContracts * tradePrice * currencyFactor));
                    }
                } else {
                    if ((openBuyCount - prndingSell) >= tradeQty) {
                        txtInitMargin.setText("0");
                        txtMainMargin.setText("0");
                    } else {
                        long marginContracts = 0;
                        if (windowMode == NEW) {
                            if (prndingSell >= openBuyCount) {
                                marginContracts = tradeQty;
                            } else {
                                marginContracts = tradeQty - (openBuyCount - prndingSell);
                            }
                        } else if (windowMode == AMEND) {
                            if (tradeQty >= openBuyCount) {
                                marginContracts = tradeQty - openBuyCount;
                            } else {
                                marginContracts = 0;
                            }
                        }
                        txtInitMargin.setText(currencyFormat.format(marginContracts * tradePrice * currencyFactor));
                        txtMainMargin.setText(currencyFormat.format(marginContracts * tradePrice * currencyFactor));
                    }
                }
            } else {
                double initMargin = Double.NaN;
                boolean isDayOrder = false;
                if ((type == TradeMeta.ORDER_TYPE_MARKET) || (type == TradeMeta.ORDER_TYPE_LIMIT)) {
                    isDayOrder = dayOrder.isSelected();
                }
                try {
                    initMargin = TradeMethods.getMarginForSymbol(exchange, symbol, instrument, portfolio, isDayOrder, account.getMarginLimit(), account.getDayMarginLimit());
                } catch (Exception e) {

                }
                if (initMargin != Double.NaN) {
                    txtInitMargin.setText(currencyFormat.format(initMargin * currencyFactor));
                    txtMainMargin.setText(currencyFormat.format(initMargin * currencyFactor));
                } else {
                    txtInitMargin.setText(NA);
                    txtMainMargin.setText(NA);
                }
            }

        } catch (Exception e) {
            txtInitMargin.setText(NA);
            txtMainMargin.setText(NA);
        }
    }

    private void calculateOrderValue() {
        try {
            String portfolio = selectedPortfolio;
            long tradeQty = txtQty.getNumber().longValue();
            long tradeActualQty = txtQty.getNumber().longValue();
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
            if (stock.getLotSize() > 0) {
                tradeQty = tradeQty * stock.getLotSize();
            }
            double tradePrice = 0;
            TWDecimalFormat currencyFormat = null;

            try {
                String currency = TradingShared.getTrader().findAccountByPortfolio(selectedPortfolio).getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            } catch (Exception e) {
                currencyFormat = SharedMethods.getDecimalFormat(2);
            }
//            try {
//                currencyFormat = SharedMethods.getDecimalFormat(exchange, symbol, instrument);
//            } catch (Exception e) {
//                currencyFormat = SharedMethods.getDecimalFormat(2);
//            }

//            if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
//                long openBuyCount = 0;
//                long openSellCount = 0;
//                long prndingBuy = 0;
//                long prndingSell = 0;
//                FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol(), instrument));
//                long effectiveQuantity = 0;
//                if (futureBaseAttributes != null) {
//                    tradePrice = futureBaseAttributes.getMarginPct();
//                    TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecordForFutures(SharedMethods.getKey(exchange, symbol, instrument), selectedPortfolio);
//                    if (transactRecord != null) {
//                        openBuyCount = transactRecord.getOpenBuyCount();
//                        openSellCount = transactRecord.getOpenSellCount();
//                        prndingBuy = transactRecord.getPendingBuy();
//                        prndingSell = transactRecord.getPendingSell();
//                    }
//                    if (side == TradeMeta.BUY) {
//                        if ((openSellCount - prndingBuy) >= tradeQty) {
//                            netValue = 0;
//                            txtNetValue.setText("0");
//                            txtCommission.setText("0");
//                            txtOrderValue.setText("0");
//                        } else {
//                            long marginContracts = 0;
//                            if (windowMode == NEW) {
//                                if (prndingBuy >= openSellCount) {
//                                    marginContracts = tradeQty;
//                                } else {
//                                    marginContracts = tradeQty - (openSellCount - prndingBuy);
//                                }
//                            } else if (windowMode == AMEND) {
//                                if (tradeQty >= openSellCount) {
//                                    marginContracts = tradeQty - openSellCount;
//                                } else {
//                                    marginContracts = 0;
//                                }
//                            }
//                            double commission = 0;
//                            if (marginContracts > 0) {
//                                commission = SharedMethods.getCommission(exchange, 1, marginContracts, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
//                            }
//                            netValue = (marginContracts * tradePrice) + commission;
//                            txtCommission.setText(currencyFormat.format(commission));
//                            txtOrderValue.setText(currencyFormat.format(marginContracts * tradePrice));
//                            txtNetValue.setText(currencyFormat.format(netValue));
//                        }
//                    } else {
//                        if ((openBuyCount - prndingSell) >= tradeQty) {
//                            netValue = 0;
//                            txtNetValue.setText("0");
//                            txtCommission.setText("0");
//                            txtOrderValue.setText("0");
//                        } else {
//                            long marginContracts = 0;
//                            if (windowMode == NEW) {
//                                if (prndingSell >= openBuyCount) {
//                                    marginContracts = tradeQty;
//                                } else {
//                                    marginContracts = tradeQty - (openBuyCount - prndingSell);
//                                }
//                            } else if (windowMode == AMEND) {
//                                if (tradeQty >= openBuyCount) {
//                                    marginContracts = tradeQty - openBuyCount;
//                                } else {
//                                    marginContracts = 0;
//                                }
//                            }
//                            double commission = 0;
//                            if (marginContracts > 0) {
//                                commission = SharedMethods.getCommission(exchange, 1, marginContracts, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
//                            }
//                            netValue = (marginContracts * tradePrice) + commission;
//                            txtCommission.setText(currencyFormat.format(commission));
//                            txtOrderValue.setText(currencyFormat.format(marginContracts * tradePrice));
//                            txtNetValue.setText(currencyFormat.format(netValue));
//                        }
//                    }
//                }  else {
//                    if (type == TradeMeta.ORDER_TYPE_LIMIT) {
//                        tradePrice = Double.parseDouble(txtTPrice.getText());
//                    } else {
//                        if (exchange != null) {
//                            if (stock != null) {
//                                if (side == TradeMeta.SELL)
//                                    tradePrice = stock.getBestBidPrice();
//                                else
//                                    tradePrice = stock.getBestAskPrice();
//                            }
//                        }
//                    }
//                    double commission = 0;
//                    if (side == TradeMeta.BUY) {
//                        if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
//                            commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
//                        } else {
//                            commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
//                        }
//                    } else {
//                        if (t0SellChkBox.isSelected()) {
//                            commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
//                        } else {
//                            commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
//                        }
//                    }
//                    txtOrderValue.setText(currencyFormat.format((tradeQty * tradePrice) / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor()));
//                    if (commission == -1) { // if there is not commission rule, make the commission NA
//                        txtCommission.setText(Language.getString("NA"));
//                        commission = 0;
//                    } else {
//                        txtCommission.setText(currencyFormat.format(commission / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor()));//TradingShared.getCommission(tradeQty * tradePrice)));
//                    }
//                    if (side == TradeMeta.SELL)
//                        netValue = (tradeQty * tradePrice) - commission;// TradingShared.getCommission(tradeQty * tradePrice);
//                    else
//                        netValue = (tradeQty * tradePrice) + commission;//TradingShared.getCommission(tradeQty * tradePrice);
//                    netValue = netValue / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor(); // adjust to base currency
//                    txtNetValue.setText(currencyFormat.format(netValue));
//                }
//            } else {
            if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                tradePrice = Double.parseDouble(txtTPrice.getText());
            } else {
                if (instrument == Meta.INSTRUMENT_MUTUALFUND) {
                    tradePrice = Double.parseDouble(txtTPrice.getText());
                } else {
                    if (exchange != null) {
                        //                        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol);
                        if (stock != null) {
                            if (side == TradeMeta.SELL)
                                tradePrice = stock.getBestBidPrice();
                            else
                                tradePrice = stock.getBestAskPrice();
                        }
//                        stock = null;
                    }
                }

            }
            commission = 0;
            if (tradeActualQty > 0) {
                if (side == TradeMeta.BUY) {
                    if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    } else {
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    }
                } else {
                    if (t0SellChkBox.isSelected()) {
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    } else {
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, marketCode, portfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    }
                }
            }
            if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
                commission = -1;
            }
            double value = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(stock.getCurrencyCode());

//                txtOrderValue.setText(currencyFormat.format((tradeQty * tradePrice * value) / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor()));
            txtOrderValue.setText(currencyFormat.format((tradeQty * tradePrice * value) / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor()));
            if (commission == -1) { // if there is not commission rule, make the commission NA
                txtCommission.setText(Language.getString("NA"));
                commission = 0;
            } else {
                txtCommission.setText(currencyFormat.format(commission * value / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor()));//TradingShared.getCommission(tradeQty * tradePrice)));
            }
            if (side == TradeMeta.SELL)
                netValue = ((tradeQty * tradePrice) - commission) * value;// TradingShared.getCommission(tradeQty * tradePrice);
            else
                netValue = ((tradeQty * tradePrice) + commission) * value;//TradingShared.getCommission(tradeQty * tradePrice);
            netValue = netValue / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor(); // adjust to base currency
            txtNetValue.setText(currencyFormat.format(netValue));
//            }

//            double commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, marketCode, portfolio);
//            txtOrderValue.setText(currencyFormat.format((tradeQty * tradePrice) / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor()));
//            if (commission == -1){ // if there is not commission rule, make the commission NA
//                txtCommission.setText(Language.getString("NA"));
//                commission = 0;
//            }else {
//                txtCommission.setText(currencyFormat.format(commission / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor()));//TradingShared.getCommission(tradeQty * tradePrice)));
//            }
//            if (side == TradeMeta.SELL)
//                netValue = (tradeQty * tradePrice) - commission;// TradingShared.getCommission(tradeQty * tradePrice);
//            else
//                netValue = (tradeQty * tradePrice) + commission;//TradingShared.getCommission(tradeQty * tradePrice);
//            netValue = netValue / ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceModificationFactor(); // adjust to base currency
//            txtNetValue.setText(currencyFormat.format(netValue));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void invalidateStock() {
//        selectedStock = null;
        setSelectedStock(null);
        futureBaseAttributes = null;
//        symbol = null;
//        instrument = -1;
//        exchange = null;
        companyName.setText("");
    }


    public void setSelectedStock(String key) {
        if (key == null) {
            selectedStock = null;
            symbol = null;
            instrument = -1;
            exchange = null;
        } else {
            this.symbol = SharedMethods.getSymbolFromKey(key);
            this.exchange = SharedMethods.getExchangeFromKey(key);
            this.instrument = SharedMethods.getInstrumentTypeFromKey(key);
            selectedStock = DataStore.getSharedInstance().getStockObject(key);
            try {
                if (((selectedStock != null) && selectedStock.isSymbolEnabled())) {
                    btnBuy.setEnabled(true);
                    btnSell.setEnabled(true);
                    btnQ.setEnabled(true);
                } else {
                    btnBuy.setEnabled(false);
                    btnSell.setEnabled(false);
                    btnQ.setEnabled(false);
                }
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }
    }

    /**
     * If all markets are of default type, can do the validation internally
     */
    private boolean doInternalvalidate(String symbol) {
        String key = SymbolMaster.getExchangeForSymbol(symbol, false);
        if (key != null) {
//            this.exchange = SharedMethods.getExchangeFromKey(key);
//            this.symbol = SharedMethods.getSymbolFromKey(key);
//            this.instrument = SharedMethods.getInstrumentTypeFromKey(key);
            setSelectedStock(key);
            if (this.symbol != null) {
                txtSymbol.setText(this.symbol);
            }

//            selectedStock = DataStore.getSharedInstance().getStockObject(key);
            if ((instrument == Meta.INSTRUMENT_FUTURE) && (selectedStock != null)) {
                futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, selectedStock.getOptionBaseSymbol(), instrument));
            }
            setSymbol(key);

            TradeMethods.getSharedInstance().populateOrderTypes(exchange, orderTypesList, TradingShared.getTrader().getPath(selectedPortfolio));
            validateTIFFTypes(); // revalidate TIFF type for the new symbol/exchange
            setPanelLabels();
            updateMarginPanel();
            lockInputs(false);
            return true;
        } else {
            lockInputs(true);
            setSelectedStock(null);
//            selectedStock = null;
            futureBaseAttributes = null;
            return false;
        }
    }

    private void validateSymbol(String symbol) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            String requestID = "TransactionDialog" + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol, requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
            System.out.println("Validate request sent");
        }
    }

    private void removeValidatedSymbols() {
        for (String key : validatedSymbols) {
            DataStore.getSharedInstance().removeSymbolRequest(key);
        }
    }

    private void lockInputs(boolean status) {
        try {
            if ((selectedStock != null) && (selectedStock.isSymbolEnabled())) {
                btnBuy.setEnabled(!status);
                btnSell.setEnabled(!status);
                btnQ.setEnabled(!status);
            } else {
                btnBuy.setEnabled(false);
                btnSell.setEnabled(false);
                btnQ.setEnabled(false);
            }
        } catch (Exception e) {
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cmbAction) {
            validatePriceInputs();
            applyColors();
            changeType();
            validateButtons(side);
            validateStopLoss(side);
            if (side == TradeMeta.SELL) {

                try {
                    if (TradingShared.isT0ordersEnable(exchange)) {
                        String bookKeeper = getSelectedBookKeeper();
                        if (bookKeeper != null) {
                            //donothing
                        } else {
                            bookKeeper = getSelectedBookKeeper();
                        }
                        TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecord(SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper), selectedPortfolio);
                        if (t0SellChkBox.isSelected()) {
                            try {
                                txtQty.setText(String.valueOf((transactRecord.getTPlusDayNetHolding() - transactRecord.getTPlusDaySellPending())));

                            } catch (Exception e2) {
                                txtQty.setText("");
                            }
                        } else {
                            txtQty.setText(String.valueOf((transactRecord.getQuantity() - transactRecord.getPledged() - transactRecord.getPendingSell() - transactRecord.getTPlusPendingStock() - transactRecord.getTPlusDayNetHolding())));

                        }
                    }
                } catch (Exception e1) {
                    //do nothing on very first time
                }
            }
            updateMarginPanel();
        } else if (e.getSource() == dayOrder) {
            applyColors();
        } else if (e.getSource() == cmbGoodTills) {
            showCalendar();
        } else if (e.getSource() == cmbType) {
            validatePriceInputs();
            setSpecialConditions();
            resetingValues();
            focuspolicy.reorderTraversalPolicy();
        } else if (e.getSource() == btnOrderBook) {
            showOrderBook();
        } else if (e.getSource() == btnOrderCal) {
            showDepthCalc();
        } else if (e.getSource() == btnClose) {
            dispose();
        } else if (e.getSource() == btnQ) {
            addTransactionToQueue();
        } else if (e.getSource() == btnBuy) {
            executeTransaction(TradeMeta.BUY, queued, selectedQID);
        } else if (e.getSource() == btnSell) {
            executeTransaction(TradeMeta.SELL, queued, selectedQID);
        } else if (e.getSource() == btnCancel) {
            executeTransaction(TradeMeta.CANCEL, queued, selectedQID);
        } else if (e.getSource() == btnAmend) {
            executeTransaction(TradeMeta.AMEND, queued, selectedQID);
        } else if (e.getSource() == btnShowDays) {
            showCalendar();
        } else if (e.getActionCommand().equals("COND_EXPIRY")) {
            showConditionExpity();
        } else if (e.getActionCommand().startsWith("COND_EXPIRY_DAYS")) {
            setConditionExpity(e.getActionCommand());
        } else if (e.getActionCommand().equals("GTD")) {
            if (datePicker == null) {
                datePicker = new DatePicker(Client.getInstance().getFrame(), true, true);
//                datePicker = new DateCombo(Client.getInstance().getFrame());
            }
            datePicker.getCalendar().addDateSelectedListener(this);
            Point location = new Point(txtGoodTill.getX(),
                    (txtGoodTill.getY()) + txtGoodTill.getBounds().height);
            SwingUtilities.convertPointToScreen(location, txtGoodTill.getParent());
            datePicker.setLocation(location);

            datePicker.showDialog();
        } else if (e.getSource() instanceof TWMenuItem) {
            setGoodTill((TWMenuItem) e.getSource());
        } else if (e.getActionCommand().equals("SS")) {
            searchSymbol();
            /*} else if(e.getSource() == btnruleGen){
          if(ruleGenerator!=null){
              ruleGenerator.dispose();
          }
          ruleGenerator = new RuleGenerator(this, symbol, exchange);
          ruleGenerator.setVisible(true);

          rule = ruleGenerator.getRule();
          ruleExpiryDate = TradingShared.formatFIXTimeToLong(ruleGenerator.getExpiryDate());*/
        } else if (e.getSource().equals(t0SellChkBox)) {
//            if (t0SellChkBox.isSelected()) {
            String bookKeeper = ((TWComboItem) bookKeepersCombo.getSelectedItem()).getId();
            TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecord(SharedMethods.getTradeKey(exchange, symbol, instrument, bookKeeper), selectedPortfolio);
            t0HoldingsFdl.setText("" + transactRecord.getTPlusDayNetHolding());
            if (t0SellChkBox.isSelected()) {
                try {
                    txtQty.setText(String.valueOf((transactRecord.getTPlusDayNetHolding() - transactRecord.getTPlusDaySellPending())));

                } catch (Exception e2) {
                    txtQty.setText("");
                }
            } else {
                txtQty.setText(String.valueOf((transactRecord.getQuantity() - transactRecord.getPledged() - transactRecord.getPendingSell() - transactRecord.getTPlusPendingStock() - transactRecord.getTPlusDayNetHolding())));

            }
//            } else {
//                t0HoldingsFdl.setText("");
//            }

        }
        if (e.getActionCommand().equalsIgnoreCase("ENTER")) {
            if (windowMode == AMEND) {
                executeTransaction(TradeMeta.AMEND, queued, selectedQID);
            } else if (windowMode == CANCEL) {
                executeTransaction(TradeMeta.CANCEL, queued, selectedQID);
            } else {
                changeType();
                executeTransaction(side, queued, selectedQID);
            }
        }
    }

    private void addTransactionToQueue() {
        try {
            //Bug ID <#0051> - removed becaus
//            if (TradingShared.isShariaTestPassed(exchange, symbol)){
            JPopupMenu popupMenu = new JPopupMenu();
            int qCount = OrderQueue.getSharedInstance().getQCount();
            if (qCount > 1) {
                for (int i = 0; i < qCount; i++) {
                    QComponent component = OrderQueue.getSharedInstance().getQConmonent(i);
                    TWMenuItem item = new TWMenuItem(component.getValue());
                    item.setActionCommand("" + component.getId());
                    item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/basketorders.gif"));
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            selectedQID = Long.parseLong(e.getActionCommand());

//                                executeTransaction(TradeMeta.BUY, true, selectedQID);
                            executeTransaction(side, true, selectedQID);
                        }
                    });
                    popupMenu.add(item);
                }
                GUISettings.applyOrientation(popupMenu);
                Point point = btnQ.getLocation();
                GUISettings.showPopup(popupMenu, btnQ.getParent(), (int) point.getX(), (int) point.getY());
            } else {
                selectedQID = Long.parseLong(OrderQueue.getSharedInstance().getQConmonent(0).getId());
                executeTransaction(side, true, selectedQID);
//                    executeTransaction(TradeMeta.BUY, true, selectedQID);
            }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void searchSymbol() {
        try {
            boolean showDefaultMarketOnly = BrokerConfig.getSharedInstance().isRemoveMarketCodeFromSymbolForAllBrokers();

            String[] symbols = SharedMethods.searchTradingSymbols(getTitle(), true, true, false);   // add all sub market symbols to sysmbol serch
            if ((symbols != null) && (!symbols[0].equals(null))) {
                keyTyped = false;
//                symbol = SharedMethods.getSymbolFromKey(symbols[0]);
//                exchange = SharedMethods.getExchangeFromKey(symbols[0]);
//                this.instrument = SharedMethods.getInstrumentTypeFromKey(symbols[0]);
//                selectedStock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                setSelectedStock(symbols[0]);
                if ((instrument == Meta.INSTRUMENT_FUTURE) && (selectedStock != null)) {
                    futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, selectedStock.getOptionBaseSymbol(), instrument));
                }
                validateTIFFTypes(); // revalidate TIFF type for the new symbol/exchange
                setPanelLabels();
                lockInputs(false);
                txtSymbol.setText(symbol);
                // setInitialPrice(); //todo added by udaya
                //todo - added by Dilum
                TradeMethods.getSharedInstance().populateOrderTypes(exchange, orderTypesList, TradingShared.getTrader().getPath(selectedPortfolio));
                if (selectedStock != null) {
                    boolean isSymbolTypeOption = isSymbolTypeOption(selectedStock.getInstrumentType());
                    populateAccountStatusPanel(isFuturesSymbol(selectedStock.getInstrumentType()));
                    populateInfoPanel(isSymbolTypeOption);
                    populateOrderDataPanel(isSymbolTypeOption);
                    setTitle(selectedStock.getInstrumentType());
                }
                updateMarginPanel();
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showOrderBook() {
        Client.getInstance().depthByPrice(symbol, false, Constants.MAINBOARD_TYPE);
    }

    private void showDepthCalc() {
        Client.getInstance().mnu_MDepth_Calculator(symbol, TWTypes.TradeSides.BUY, false, false, LinkStore.LINK_NONE);
    }

    private void setGoodTillText(int tiff) {
        /* if (tiff == 4){
             txtGoodTill.setText(Language.getString("FILL_OR_KILL"));
         }else if (tiff == 0){
             txtGoodTill.setText(Language.getString("DAY"));
         }else if (tiff == 7){
             txtGoodTill.setText(Language.getString("WEEK"));
         }else if (tiff == 8){
             txtGoodTill.setText(Language.getString("MONTH"));
         }else if (tiff == 1){
             txtGoodTill.setText(Language.getString("GTC"));
         }else{
             txtGoodTill.setText(goodTillStr);
         }*/
        txtGoodTill.setText(TradingShared.getTiffString(tiff, goodTillStr));
    }

    private void setGoodTill(TWMenuItem item) {
        if (item.getText().equals(TradingShared.TIFF_FOK)) {
            goodTill = 1;
            tiff = TradeMeta.TIF_FOK;
            txtGoodTill.setText(TradingShared.TIFF_FOK);
            setGoodTillDate(0);
        } else if (item.getText().equals(TradingShared.TIFF_DAY)) {
            goodTill = 1;
            tiff = TradeMeta.TIF_DAY;
            txtGoodTill.setText(TradingShared.TIFF_DAY);
            setGoodTillDate(0);
        } else if (item.getText().equals(TradingShared.TIFF_WEEK)) {
            goodTill = 7;
            tiff = TradeMeta.TIF_WEEK;
            txtGoodTill.setText(TradingShared.TIFF_WEEK);
            setGoodTillDate(goodTill);
        } else if (item.getText().equals(TradingShared.TIFF_MONTH)) {
            goodTill = 29;
            tiff = TradeMeta.TIF_MONTH;
            txtGoodTill.setText(TradingShared.TIFF_MONTH);
            setGoodTillDate(goodTill);
        } else if (item.getText().equals(TradingShared.TIFF_GTC)) {
            goodTill = -1;
            tiff = TradeMeta.TIF_GTC;
            txtGoodTill.setText(TradingShared.TIFF_GTC);
            setGoodTillDate(goodTill);

        } else if (item.getText().equals(TradingShared.TIFF_AOP)) {
            goodTill = -3;
            tiff = TradeMeta.TIF_AOP;
            txtGoodTill.setText(TradingShared.TIFF_AOP);
            setGoodTillDate(goodTill);
        } else if (item.getText().equals(TradingShared.TIFF_IOC)) {
            goodTill = -4;
            tiff = TradeMeta.TIF_IOC;
            txtGoodTill.setText(TradingShared.TIFF_IOC);
            setGoodTillDate(goodTill);
        } else if (item.getText().equals(TradingShared.TIFF_GTX)) {
            goodTill = -5;
            tiff = TradeMeta.TIF_GTX;
            txtGoodTill.setText(TradingShared.TIFF_GTX);
            setGoodTillDate(goodTill);
        } else {
            goodTill = Short.parseShort(item.getText());
            tiff = TradeMeta.TIF_GTD;
            setGoodTillDate(goodTill);
            txtGoodTill.setText(goodTillStr);
        }
    }

    private void setGoodTillFromTiff(int tiffValue) {      //this method is for initinal loading only

        /*if ((type == TradeMeta.ORDER_TYPE_MARKET)) {        
            tiffValue = TradeMeta.TIF_FOK;
        } else if ((type == TradeMeta.ORDER_TYPE_LIMIT)) {
            tiff = TradeMeta.TIF_DAY;
        }*/

        switch (tiffValue) {
            case TradeMeta.TIF_FOK:
                goodTill = 1;
                tiff = TradeMeta.TIF_FOK;
                txtGoodTill.setText(TradingShared.TIFF_FOK);
                setGoodTillDate(0);
                break;
            case TradeMeta.TIF_DAY:
                goodTill = 1;
                tiff = TradeMeta.TIF_DAY;
                txtGoodTill.setText(TradingShared.TIFF_DAY);
                setGoodTillDate(0);
                break;
            case TradeMeta.TIF_WEEK:
                goodTill = 7;
                tiff = TradeMeta.TIF_WEEK;
                txtGoodTill.setText(TradingShared.TIFF_WEEK);
                setGoodTillDate(goodTill);
                break;
            case TradeMeta.TIF_MONTH:
                goodTill = 29;
                tiff = TradeMeta.TIF_MONTH;
                txtGoodTill.setText(TradingShared.TIFF_MONTH);
                setGoodTillDate(goodTill);
                break;
            case TradeMeta.TIF_IOC:
                goodTill = -4;
                tiff = TradeMeta.TIF_IOC;
                txtGoodTill.setText(TradingShared.TIFF_IOC);
                setGoodTillDate(goodTill);
                break;
            case TradeMeta.TIF_GTX:
                goodTill = -5;
                tiff = TradeMeta.TIF_GTX;
                txtGoodTill.setText(TradingShared.TIFF_GTX);
                setGoodTillDate(goodTill);
                break;
            case TradeMeta.TIF_AOP:
                goodTill = -3;
                tiff = TradeMeta.TIF_AOP;
                txtGoodTill.setText(TradingShared.TIFF_AOP);
                setGoodTillDate(goodTill);
                break;
            case TradeMeta.TIF_GTC:
                goodTill = -1;
                tiff = TradeMeta.TIF_GTC;
                txtGoodTill.setText(TradingShared.TIFF_GTC);
                setGoodTillDate(goodTill);
                break;
        }
    }

    private void setInitialGoodTill() {

        Rule rule;
        int result;

        try {
            Interpreter interpreter = loadInterpriter(-1);
            rule = RuleManager.getSharedInstance().getRule("INITIAL", exchange, "TIF");
            result = Integer.parseInt((String) interpreter.eval(rule.getRule()));
            setGoodTillFromTiff(result);
            /*   try {
            rule = RuleManager.getSharedInstance().getRule("INITIAL", exchange, "TIF");
//            result = ((Integer) interpreter.eval(rule.getRule()));
            result = Integer.parseInt((String) interpreter.eval(rule.getRule()));
            setGoodTillFromTiff(result);*/
            setGoodTillDate(0);
        } catch (Exception evalError) {
            goodTill = 1;
            tiff = 0;
            txtGoodTill.setText(Language.getString("DAY"));
            setGoodTillDate(0);
        }
    }

    private void setGoodTillDate(int days) {
        if (days == -1) { // GTC
            goodTillStr = Language.getString("GTC");
            tiff = 1;
            goodTillLong = 0;
        } else if (days == -2) { // Fill Or Kill
            goodTillStr = Language.getString("FILL_OR_KILL");
            tiff = 4;
            goodTillLong = 0;
        } else if (days == -3) { // Fill Or Kill
            goodTillStr = Language.getString("AOP");
            tiff = 2;
            goodTillLong = 0;
        } else if (days == -4) { // Fill Or Kill
            goodTillStr = Language.getString("IOC");
            tiff = 3;
            goodTillLong = 0;
        } else if (days == -5) { // Fill Or Kill
            goodTillStr = Language.getString("GTX");
            tiff = 5;
            goodTillLong = 0;
        } else {
            /*Calendar cal = Calendar.getInstance();
            try {
                cal.set(Integer.parseInt(TradingShared.getCurrentDay().substring(0, 4)),
                        Integer.parseInt(TradingShared.getCurrentDay().substring(4, 6)) - 1,
                        Integer.parseInt(TradingShared.getCurrentDay().substring(6, 8)),
                        23, 59, 59);
            } catch (Exception e) {
                e.printStackTrace();
            }
            cal.add(Calendar.DATE, days);*/
            Date date = TradeMethods.getDateFromDays(days);
            goodTillStr = TradingShared.formatExpireDate(date.getTime());
            goodTillLong = date.getTime();
            date = null;
        }
    }

    private void setGoodTillDate(int year, int month, int day) {
        tiff = 6;
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);
        goodTillStr = TradingShared.formatGoodTill(cal.getTime());
        goodTillLong = cal.getTime().getTime();
        txtGoodTill.setText(goodTillStr);
        cal = null;
    }

    private void executeTransaction(int mode, boolean queued, long qID) {
        boolean result = true;
        boolean sucess = false;
        if (mode != CANCEL) { // nothing to validate in CANCEL orders
            result = validateInputs();
//            if ((mode == TradeMeta.AMEND) && (side == SELL) && (conditionalMode == TradeMeta.CONDITION_TYPE_STOP_LOSS)){
//                result = validateStopLossSell();
//            }else {
//                result = validateInputs(side);
//            }
        }
        if (result) {
            //if (executionMode == TradeMeta.MODE_NORMAL) {
            if (exchange != null && TradeMethods.isSymbolAllowedForProtfolio(selectedPortfolio, exchange, true)) {
                if ((mode == TradeMeta.BUY) || (mode == TradeMeta.SELL)) {
                    // check if the condition limit has exceeded
                    if (((conditionalMode == TradeMeta.CONDITION_TYPE_NORMAL) ||
                            (isStopLosssDataAvailable())) && (OrderStore.getSharedInstance().getPendingConditionCount() >= TradingShared.getMaxCondionCount())) {
                        SharedMethods.showMessage(Language.getString("MSG_MAX_CONDITIONAL_ORDERS"), JOptionPane.ERROR_MESSAGE);
                        sucess = false;
                    } else {
                        sucess = sendNewOrderRequest(queued, mode, qID);
                    }

                } else if (mode == TradeMeta.AMEND) {
                    Transaction transaction;
                    transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(currentOrderID);
                    sucess = amendOrder(queued, transaction);
                    transaction = null;
                } else if (mode == TradeMeta.CANCEL) {
                    Transaction transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(currentOrderID);
                    if (transaction != null) {
                        sucess = cancelOrder(transaction);
                    }
                }
            }
            //}
            if (sucess) {

                if (mode == TradeMeta.AMEND || mode == TradeMeta.CANCEL) {
                    dispose();
                } else {
                    if (confirmation == null) {
                        confirmation = new ConfirmationFrame(this);
                    } else {
                        confirmation.setParent(null);
                        confirmation.dispose();
                        confirmation = new ConfirmationFrame(this);
                    }
                    if (TradingShared.isShowConfigPopup()) {
                        confirmation.setVisible(true);
                    }
                    if (!confirmation.isVisible()) {
                        if (Settings.isKeepOrderWindowOpen()) {
                            clearOrderData();
                        } else {
                            dispose();
                        }
                    }
                }
            }
        }
    }

    private void clearOrderData() {
        txtQty.setText(String.valueOf("0"));
        txtQty.requestFocus();
        cmbConditionMethods.setSelectedIndex(0);
        cmbConditionOperators.setSelectedIndex(0);
        txtConditionValue.setText("");
        conditionalMode = CONDITION_TYPE_NONE;
    }

    private void validatePriceInputs() {
        setSelectedPortfolioData(selectedPortfolio);
        TWComboItem item = (TWComboItem) cmbType.getSelectedItem();
        char type = item.getId().charAt(0);

        switch (type) {
            case TradeMeta.ORDER_TYPE_MARKET:
                txtTPrice.setEnabled(false);
                txtTPrice.setText("");
                priceSpinner.setEnabled(false);
                cmbAction.setEnabled(true);
                txtQty.setEnabled(true);
                if (isEnableBracketOrderPanel) {
                    cardPanel.setVisible(true);
                    blankCardPanel.setVisible(false);
                    bracketPanel.setVisible(true);
                    stradlePanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelBracket);
                } else {
                    setBrackerOrderPanelStatus(false);
                    cardPanel.setVisible(false);
                }
                btnShowDays.setEnabled(true);
                break;
            case TradeMeta.ORDER_TYPE_LIMIT:
                txtTPrice.setEnabled(true);
                priceSpinner.setEnabled(true);
                cmbAction.setEnabled(true);
                txtQty.setEnabled(true);
                if (isEnableBracketOrderPanel) {
                    cardPanel.setVisible(true);
                    blankCardPanel.setVisible(false);
                    bracketPanel.setVisible(true);
                    stradlePanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelBracket);
                } else {
                    setBrackerOrderPanelStatus(false);
                    cardPanel.setVisible(false);
                }
                btnShowDays.setEnabled(true);
                break;
            case TradeMeta.ORDER_TYPE_STOPLOSS_MARKET:
                txtTPrice.setEnabled(false);
                txtTPrice.setText("");
                priceSpinner.setEnabled(false);
                txtStopPrice.setEnabled(true);
                txtStopPrice.setText("");
                cmbStopPrice.setSelectedIndex(0);
                cmbStopPrice.setEnabled(true);
                cmbAction.setEnabled(true);
                txtQty.setEnabled(true);
                dayOrder.setEnabled(false);
                dayOrder.setSelected(false);
                if (isEnableBracketOrderPanel) {
                    cardPanel.setVisible(true);
                    stradlePanel.setVisible(true);
                    bracketPanel.setVisible(false);
                    blankCardPanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelStraddle);
                } else {
                    setBrackerOrderPanelStatus(false);
                    cardPanel.setVisible(true);
                    blankCardPanel.setVisible(true);
                    bracketPanel.setVisible(false);
                    stradlePanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelBlank);
                }
                btnShowDays.setEnabled(true);
                enableSliceOrderPanel(false);
//                enabledSliceOrders = false;
                break;
            case TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT:
                txtTPrice.setEnabled(true);
                priceSpinner.setEnabled(true);
                txtStopPrice.setEnabled(true);
                txtStopPrice.setText("");
                cmbStopPrice.setSelectedIndex(0);
                cmbStopPrice.setEnabled(true);
                cmbAction.setEnabled(true);
                txtQty.setEnabled(true);
                dayOrder.setEnabled(false);
                dayOrder.setSelected(false);
                if (isEnableBracketOrderPanel) {
                    cardPanel.setVisible(true);
                    stradlePanel.setVisible(true);
                    bracketPanel.setVisible(false);
                    blankCardPanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelStraddle);
                } else {
                    setBrackerOrderPanelStatus(false);
                    cardPanel.setVisible(true);
                    blankCardPanel.setVisible(true);
                    bracketPanel.setVisible(false);
                    stradlePanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelBlank);
                }
                btnShowDays.setEnabled(true);
//                enabledSliceOrders = false;
                enableSliceOrderPanel(false);
                break;
            case TradeMeta.ORDER_TYPE_LIMIT_ON_CLOSE:
                txtTPrice.setEnabled(true);
                priceSpinner.setEnabled(true);
                cmbAction.setEnabled(true);
                txtQty.setEnabled(true);
                dayOrder.setEnabled(false);
                dayOrder.setSelected(false);
                if (isEnableBracketOrderPanel || hasStopLossOrders) {
                    cardPanel.setVisible(true);
                    blankCardPanel.setVisible(true);
                    bracketPanel.setVisible(false);
                    stradlePanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelBlank);
                } else {
                    cardPanel.setVisible(false);
                }
                btnShowDays.setEnabled(false);
                this.setSize(new Dimension(WIDTH, HEIGHT));
                cmbexecTypeOperators.setEnabled(false);
                enabledSliceOrders = false;
                break;
            case TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE:
                txtTPrice.setEnabled(false);
                priceSpinner.setEnabled(false);
                cmbAction.setEnabled(true);
                txtQty.setEnabled(true);
                dayOrder.setEnabled(false);
                dayOrder.setSelected(false);
                if (isEnableBracketOrderPanel || hasStopLossOrders) {
                    cardPanel.setVisible(true);
                    blankCardPanel.setVisible(true);
                    bracketPanel.setVisible(false);
                    stradlePanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelBlank);
                } else {
                    cardPanel.setVisible(false);
                }
                btnShowDays.setEnabled(false);
                this.setSize(new Dimension(WIDTH, HEIGHT));
                cmbexecTypeOperators.setEnabled(false);
                enabledSliceOrders = false;
                break;
            case TradeMeta.ORDER_TYPE_SQUARE_OFF:
                txtTPrice.setText("");
                txtTPrice.setEnabled(false);
                txtQty.setEnabled(false);
                cmbAction.setEnabled(false);
                quantitySpinner.setEnabled(false);
                priceSpinner.setEnabled(false);
                dayOrder.setEnabled(false);
                dayOrder.setSelected(false);
                if (isEnableBracketOrderPanel || hasStopLossOrders) {
                    cardPanel.setVisible(true);
                    blankCardPanel.setVisible(true);
                    bracketPanel.setVisible(false);
                    stradlePanel.setVisible(false);
                    cardLayout.show(cardPanel, cardPanelBlank);
                } else {
                    cardPanel.setVisible(false);
                }
                btnShowDays.setEnabled(false);
//                this.setSize(new Dimension(WIDTH, HEIGHT));
//                cmbexecTypeOperators.setEnabled(false);
//                enabledSliceOrders = false;
                enableSliceOrderPanel(false);
                break;
        }
        if (windowMode == AMEND) {
            cmbAction.setEnabled(false);
        }
        this.type = type;
        validateTIFFTypes();
    }

    private void changeType() {
        if (cmbAction.getSelectedItem().equals(Language.getString("BUY"))) {
            this.side = TradeMeta.BUY;
        } else {
            this.side = TradeMeta.SELL;
        }
    }

    private void validateButtons(int action) {
        btnBuy.setVisible(action == TradeMeta.BUY);
        btnSell.setVisible(action == TradeMeta.SELL);
        btnAmend.setVisible(action == TradeMeta.AMEND);
        btnCancel.setVisible(action == TradeMeta.CANCEL);
        btnQ.setVisible((executionMode == TradeMeta.MODE_NORMAL) && ((action == TradeMeta.BUY) || (action == TradeMeta.SELL)));
        t0SellChkBox.setEnabled(action == TradeMeta.SELL);
        if (action == TradeMeta.AMEND) {
            t0SellChkBox.setSelected(true);
        }
        if (action == TradeMeta.BUY) {
            t0SellChkBox.setSelected(false);
        }
    }

    private void validateStopLoss(int action) {
//        if (action == TradeMeta.BUY){
//            txtStopPrice.setEnabled(TradingShared.isConditionalTradableExchange(exchange)); //true if allowed
//            txtTakeProfit.setEnabled(TradingShared.isConditionalTradableExchange(exchange)); //true if allowed
//        }else {
//            txtStopPrice.setEnabled(false);
//            txtStopPrice.setText("");
//            txtTakeProfit.setEnabled(false);
//            txtTakeProfit.setText("");
//        }
    }

    private void showCalendar() {
        Point location = new Point(0, 0);
        datePopup.show(cmbGoodTills, (int) location.getX(), (int) (location.getY()) + cmbGoodTills.getBounds().height);
    }

    private void showConditionExpity() {
        TWMenuItem item;
        Point location = new Point(0, 0);
        JPopupMenu popup = new JPopupMenu();
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_0_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|0");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_1_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|1");    //todo changed by udaya form 0 to 1
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_2_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|2");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_3_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|3");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_WEEK"));
        item.setActionCommand("COND_EXPIRY_DAYS|7");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_2WEEKS"));
        item.setActionCommand("COND_EXPIRY_DAYS|14");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_1MONTH"));
        item.setActionCommand("COND_EXPIRY_DAYS|30");
        item.addActionListener(this);
        popup.add(item);

        GUISettings.applyOrientation(popup);
        popup.show(cmbConditionExpiry, 0, cmbConditionExpiry.getHeight());
    }

    private void setConditionExpity(String command) {
        try {
            int days;
            if (command != null) {
                days = Integer.parseInt(command.split("\\|")[1]);
            } else {
                days = 0;
            }
            Date date = TradeMethods.getDateFromDays(days);
            String goodTillStr = TradingShared.formatGoodTill(date.getTime());
            txtConditionExpiry.setText(goodTillStr);
            System.out.println(date.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean sendNewOrderRequest(boolean queued, int mode, long qID) {
        double buyingPower;
        double buyingPowerWithoutMargins;
        String conditionStartDate = null;
        String conditionExpireDate = null;
        boolean isMarginUsed = false;
        try {
            if (TradingShared.isShariaTestPassed(exchange, symbol, instrument)) {
                if (!isRuleBasedValidationSucess(conditionalMode != TradeMeta.CONDITION_TYPE_NONE)) {
                    return false;
                }
                Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                Long ordQty = txtQty.getNumber().longValue();
                if (stock.getLotSize() > 0) {
                    ordQty = ordQty * stock.getLotSize();
                }
                try {
                    buyingPower = TradeMethods.getEffectiveBuyingPowerForSymbol(exchange, symbol, instrument, selectedPortfolio, dayOrder.isSelected(), getCommisionVal());
                    Account account = TradingShared.getTrader().findAccountByPortfolio(selectedPortfolio);
                    buyingPower = convertToSelectedCurrency(stock.getCurrencyCode(), account.getCurrency(), buyingPower);

                } catch (Exception e) {
                    buyingPower = 0;
                }

                try {
                    Account account = TradingShared.getTrader().findAccountByPortfolio(selectedPortfolio);
                    buyingPowerWithoutMargins = account.getBuyingPower();
                    buyingPowerWithoutMargins = convertToSelectedCurrency(stock.getCurrencyCode(), account.getCurrency(), buyingPowerWithoutMargins);
                } catch (Exception e) {
                    buyingPowerWithoutMargins = 0;
                }

                boolean tradingFutures = instrument == Meta.INSTRUMENT_FUTURE;

                if ((!tradingFutures) && (side == TradeMeta.SELL) && (holding < getQuantity()) && (!t0SellChkBox.isSelected())) {
                    ShowMessage message = new ShowMessage(Language.getString("INSUFFICIENT_HOLDINGS_POPUP"), "W", true);
                    if (message.getReturnValue() != 1) {
                        return false;
                    }
                } else if (side == TradeMeta.BUY) {
                    double orderPrice = -1;
                    if (type == TradeMeta.ORDER_TYPE_MARKET) {
                        orderPrice = 0;
                    } else {
                        try {
                            orderPrice = Double.parseDouble(txtTPrice.getText());
                        } catch (Exception e) {
                        }
                    }

                    if ((!isMargisApplied(selectedPortfolio, selectedStock.getKey())) && (buyingPower < netValue)) {
                        System.out.println(Language.getString("INSUFFICIENT_FUNDS_POPUP"));
                        ShowMessage message = new ShowMessage(Language.getString("INSUFFICIENT_FUNDS_POPUP"), "W", true);
                        if (message.getReturnValue() != 1) {
                            return false;
                        }
                    } else {
                        if ((buyingPowerWithoutMargins < buyingPower) && (buyingPowerWithoutMargins < netValue)) {
//                            ShowMessage message = new ShowMessage(Language.getString("INSUFFICIENT_BUYING_POWER_POPUP"), "W", true);
//                            if (message.getReturnValue() != 1) { // moved inside the Oreder confermation
//                                return false;
//                            }
                        }
                    }

                    if (isMargisApplied(selectedPortfolio, selectedStock.getKey()) && buyingPowerWithoutMargins < netValue) {
                        double margin;
                        if (buyingPowerWithoutMargins <= 0) {
                            margin = netValue;
                        } else {
                            margin = netValue - buyingPowerWithoutMargins;
                        }
                        Account account = TradingShared.getTrader().getAccoutOfPortfolio(selectedPortfolio);
                        double normalmargin = MarginCalculator.getSharedInstance().calculateNormalMargin(new String[]{selectedPortfolio}, account.getCurrency());
                        if (normalmargin > 0) {
                            normalmargin = normalmargin + account.getTotMarginBlock() - account.getTotMarginDue();
                        }
                        normalmargin = convertToSelectedCurrency(stock.getCurrencyCode(), account.getCurrency(), normalmargin);
                        boolean isnormalmargin = true;
                        boolean isdaymargin = false;
                        if (normalmargin <= 0) {
                            isnormalmargin = false;
                            isdaymargin = true;
                        } else if (normalmargin > 0 && (normalmargin - margin) > 0) {
                            isnormalmargin = true;
                            isdaymargin = false;
                        } else {
                            isnormalmargin = true;
                            isdaymargin = true;
                        }
                        Object[] resp = validateTiffForMargin(tiff, isnormalmargin, isdaymargin);

                        if (resp != null && (!(Boolean) resp[0])) {
                            ShowMessage message = new ShowMessage((String) resp[1], "W", true);
                            if (message.getReturnValue() != 1) {
                                return false;
                            }
                        }
                        boolean isCoverageFailwithOutNwOrder = validateCurrentMargin();

                        if (isCoverageFailwithOutNwOrder) {

                        }

//                        if ( validateMargins(margin) && !isCoverageFailwithOutNwOrder) {
                        if (validateMargins(margin)) {
                            if (buyingPower < 0) {
                                buyingPower = 0.00;
                            }
                            double coverage = getAdjustedCoverage(margin);
                            double qty = getAdjustedQuantity(margin, account);
                            long adjQty = (long) qty;// (int) Math.floor(qty);
                            if (adjQty <= 0) {
                                adjQty = 0;
                            }
//                            TWDecimalFormat warningformat = SharedMethods.getDecimalFormat(coverageDecimals);
//                            TWDecimalFormat bpformat = new TWDecimalFormat("#,###");
//                            TWDecimalFormat netvalformat = SharedMethods.getDecimalFormat(exchange, symbol, instrument);
                            TWDecimalFormat qtyformat = new TWDecimalFormat("#,###");
                            String msg = Language.getString("INSUFFICIENT_COVERAGE_ADJUSTMENT");
                            msg = msg.replaceFirst("\\[qty\\]", qtyformat.format(adjQty));
//                            if (!Double.isNaN(coverage)) {
//                                msg = msg.replaceFirst("\\[coverage\\]", warningformat.format(coverage));
//                            } else {
//                                msg = msg.replaceFirst("\\[coverage\\]", TradingShared.NA + "");
//                            }
//                            msg = msg.replaceFirst("\\[netval\\]", netvalformat.format(netValue));
//                            msg = msg.replaceFirst("\\[netval\\]", netvalformat.format(qty));

                            MarginMessage message = new MarginMessage(adjQty, msg);

                            if (message.getReturnVal() == 2 && adjQty > 0) {
                                txtQty.setText(adjQty + "");
                            } else if (message.getReturnVal() == 1) {
                                txtQty.setText(adjQty + "");
                                return false;
                            } else {
                                return false;
                            }
                        }
                        isMarginUsed = true;
                    }
                }

                double price = -1;
                try {
                    price = Double.parseDouble(txtTPrice.getText());
                } catch (Exception e) {
                }

                double stopPrice = -1;
                double trailStopPrice = -1;
                double takeProfit = -1;
                int stopLossType = -1;
                String stopLossTypeStr = "";
                boolean isDayOrder = false;
                boolean isBracketOrder = false;
                if ((type == TradeMeta.ORDER_TYPE_MARKET) || (type == TradeMeta.ORDER_TYPE_LIMIT) /*||
                            (type == TradeMeta.ORDER_TYPE_DAY_LIMIT) || (type == TradeMeta.ORDER_TYPE_DAY_MARKET)*/) {
                    isDayOrder = dayOrder.isSelected();
                    isBracketOrder = bracketCheck.isSelected();
                    if (bracketCheck.isSelected()) {
                        try {
                            stopPrice = Double.parseDouble(bracketStopLoss.getText());
                        } catch (Exception e) {
                            stopPrice = 0;
                        }
                        try {
                            trailStopPrice = Double.parseDouble(bracketTrailStopLoss.getText());
                        } catch (Exception e) {
                            trailStopPrice = 0;
                        }
                        try {
                            takeProfit = Double.parseDouble(bracketTakeProf.getText());
                        } catch (Exception e) {
                            takeProfit = 0;
                        }
                        stopLossType = Integer.parseInt(((TWComboItem) bracketcmbSL.getSelectedItem()).getId());
                        stopLossTypeStr = ((TWComboItem) bracketcmbSL.getSelectedItem()).getValue();
                    }
                } else if ((type == TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT) || (type == TradeMeta.ORDER_TYPE_STOPLOSS_MARKET)) {

                    isBracketOrder = true;
                    try {
                        stopPrice = Double.parseDouble(txtStopPrice.getText());
                    } catch (Exception e) {
                        stopPrice = 0;
                    }
                    try {
                        trailStopPrice = Double.parseDouble(bracketTrailStopLoss.getText());
                    } catch (Exception e) {
                        trailStopPrice = 0;
                    }
                    try {
                        takeProfit = Double.parseDouble(stradleTakeProf.getText());
                    } catch (Exception e) {
                        takeProfit = 0;
                    }
                    stopLossType = Integer.parseInt(((TWComboItem) cmbStopPrice.getSelectedItem()).getId());
                    stopLossTypeStr = ((TWComboItem) cmbStopPrice.getSelectedItem()).getValue();
//                        }
                }
                /*else if ((type == TradeMeta.ORDER_TYPE_STOP_LIMIT) || (type == TradeMeta.ORDER_TYPE_STOP_MARKET)){
                    if(isEnableBracketOrderPanel) {
                        try {
                            stopPrice = Double.parseDouble(txtStopPrice.getText());
                        } catch (Exception e) {
                            stopPrice = 0;
                        }
                        stopLossType = Integer.parseInt(((TWComboItem)cmbStopPrice.getSelectedItem()).getId());
                        stopLossTypeStr = ((TWComboItem)cmbStopPrice.getSelectedItem()).getValue();
                    } else {
                        try {
                            stopPrice = Double.parseDouble(stopLossCardField.getText());
                        } catch (Exception e) {
                            stopPrice = 0;
                        }
                        stopLossType = -1;
                        stopLossTypeStr = "";
                    }
                    if(stradleCheck.isSelected()) {
                        try {
                            takeProfit = Double.parseDouble(stradleTakeProf.getText());
                        } catch (Exception e) {
                            takeProfit = 0;
                        }
                    }
                }*/

                String field;
                try {
                    field = conditionMethods.get(cmbConditionMethods.getSelectedIndex()).getValue();
                } catch (Exception e) {
                    field = null;
                }

                String operator;
                try {
                    operator = conditionOperators.get(cmbConditionOperators.getSelectedIndex()).getValue();
                } catch (Exception e) {
                    operator = null;
                }

                float conditionValue;
                try {
                    conditionValue = Float.parseFloat(txtConditionValue.getText().trim());
                } catch (Exception e) {
                    conditionValue = 0;
                }

                int execType = -1;
                int blocksize = -1;
                long timeinterval = -1;
                if (enabledSliceOrders) {
                    execType = Integer.parseInt(((TWComboItem) cmbexecTypeOperators.getSelectedItem()).getId());
                    try {
                        blocksize = Integer.parseInt(txtexecBlockValue.getText());
                        if (blocksize < 1) {
                            SharedMethods.showMessage(Language.getString("INVALID_SLICED_BLOCK_SIZE"), JOptionPane.ERROR_MESSAGE);
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        SharedMethods.showMessage(Language.getString("INVALID_SLICED_BLOCK_SIZE"), JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                    try {
                        timeinterval = Long.parseLong(txtexecTimeValue.getText());
                        if (timeinterval < 1) {
                            SharedMethods.showMessage(Language.getString("INVALID_SLICED_TIME_INTERVAL"), JOptionPane.ERROR_MESSAGE);
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        if (execType == TradeMeta.SLICE_ORD_TYPE_TIME_INTERVAL) {
                            SharedMethods.showMessage(Language.getString("INVALID_SLICED_TIME_INTERVAL"), JOptionPane.ERROR_MESSAGE);
                            return false;
                        }
                    }
                }
                if ((blocksize > getQuantity()) || ((txtMinFill.getText().trim().length() > 0) && (blocksize > TradingShared.getIntValue(txtMinFill.getText())))) {
                    SharedMethods.showMessage(Language.getString("MSG_INVALID_SLICE_ORDER_QTY"), JOptionPane.ERROR_MESSAGE);
                    return false;
                }

                int decimalCount = 2;
                String currency = null;
                try {
                    currency = TradingShared.getTrader().findAccountByPortfolio(selectedPortfolio).getCurrency();
                    decimalCount = SharedMethods.getCurrencyDecimalPlaces(currency);
                } catch (Exception e) {
                }
                String tiffStr = TradingShared.getTiffString(tiff, goodTillStr);

                if (TradeMethods.isDuplicateSendingOrder(exchange, symbol, stock.getInstrumentType(), selectedPortfolio, getQuantity(), side)) {
                    String message = Language.getString("MSG_DUPLICATE_ORDER");
                    message = message.replaceAll("\\[SIDE\\]", TradingShared.getActionString(mode));
                    int j = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                    if (j == JOptionPane.NO_OPTION) {
                        return false;
                    }
                }


                StringBuilder ruleBuffer = new StringBuilder();
                behaviourRule = null;
                bracketRule = null;
                if (conditionalMode == TradeMeta.CONDITION_TYPE_NORMAL) {
                    ConditionalBehaviour behaviour = new ConditionalBehaviour();
                    behaviour.setConditionCategory(MIXConstants.CONDITION_CATEGORY_PRE_CONDITION);
                    behaviour.setConditionType(MIXConstants.CONDITION_TYPE_NORMAL);

                    Condition condtion = new Condition();
                    condtion.setOperator(conditionOperators.get(cmbConditionOperators.getSelectedIndex()).getId());
                    condtion.setConditionParameter(conditionMethods.get(cmbConditionMethods.getSelectedIndex()).getId());
                    condtion.setConditionValue(txtConditionValue.getText().trim());

                    ruleBuffer.append(exchange);
                    ruleBuffer.append(TradeMeta.DD);
                    ruleBuffer.append(symbol);
                    ruleBuffer.append(TradeMeta.DD);
                    ruleBuffer.append(conditionMethods.get(cmbConditionMethods.getSelectedIndex()).getId());
                    ruleBuffer.append(TradeMeta.DD);
                    ruleBuffer.append(conditionOperators.get(cmbConditionOperators.getSelectedIndex()).getId());
                    ruleBuffer.append(TradeMeta.DD);
                    ruleBuffer.append(txtConditionValue.getText().trim());
                    ruleBuffer.append(TradeMeta.DD);
                    ruleBuffer.append("0"); // no second value

                    conditionExpireDate = TradingShared.formatExpireDate(TradingShared.getGoodTillLong(txtConditionExpiry.getText()));
                    conditionStartDate = TradingShared.formatExpireDate(TradingShared.getGoodTillLongYYYYDDMMHHMMSS(TradingShared.getTrader().getCurrentDay(selectedPortfolio)));
                    behaviour.setConditionStartTime(conditionStartDate);
                    behaviour.setExpireTime(conditionExpireDate);
                    ArrayList<Condition> list = new ArrayList<Condition>();
                    list.add(condtion);
                    behaviour.setConditions(list);
                    behaviour.setConditionStatus(TradeMeta.CONDITION_STATUS_PENDING);
                    behaviourRule = behaviour;
                    rule = ruleBuffer.toString();
                }
                if (isBracketOrder && !((type == TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT) || (type == TradeMeta.ORDER_TYPE_STOPLOSS_MARKET))) {     //todo added By Udaya for MIX implimentation
                    ConditionalBehaviour behaviour = new ConditionalBehaviour();
                    behaviour.setConditionCategory(MIXConstants.CONDITION_CATEGORY_POST_CONDITION);
                    behaviour.setConditionType(MIXConstants.CONDITION_TYPE_BRACKET);
                    conditionExpireDate = TradingShared.formatExpireDate((System.currentTimeMillis() + (60 * 60 * 24 * 1000)));
//                    conditionExpireDate = TradingShared.formatExpireDate();
                    conditionStartDate = TradingShared.formatExpireDate(TradingShared.getGoodTillLongYYYYDDMMHHMMSS(TradingShared.getTrader().getCurrentDay(selectedPortfolio)));
                    behaviour.setConditionStartTime(conditionStartDate);
                    behaviour.setExpireTime(conditionStartDate);
                    behaviour.setConditionStatus(TradeMeta.CONDITION_STATUS_PENDING);
                    bracketRule = behaviour;

                }
                int i = TradeMethods.getNewOrderConfirmMsg(symbol, exchange, instrument, price, decimalCount, stopPrice, takeProfit, getQuantity(),
                        field, operator, conditionValue, type, mode, side, conditionalMode, stopLossTypeStr, trailStopPrice, isDayOrder, tiffStr, selectedPortfolio,
                        txtOrderValue.getText(), txtCommission.getText(), txtNetValue.getText(), txtInitMargin.getText(),
                        txtMainMargin.getText(), null, TradingShared.getIntValue(txtDisclosed.getText()), behaviourRule, isMarginUsed);

                if (i == JOptionPane.OK_OPTION) {
                    /*float stopPrice = -1;
                    float takeProfit = -1;

                    try {
                        stopPrice = Float.parseFloat(txtStopPrice.getText());
                    } catch (Exception e) {
                        stopPrice = 0;
                    }
                    try {
                        takeProfit = Float.parseFloat(txtTakeProfit.getText());
                    } catch (Exception e) {
                        takeProfit = 0;
                    }*/

                    /*int qty;
if ((windowMode == TradeMeta.AMEND) && (TradingShared.isAmendModeDelta())){
qty = (int)totalQty;                                            *//* Adjust the total qty *//*
                            qty += (TradingShared.getIntValue(txtTQty.getText()) - remainingQty);
                        } else {
                            qty = TradingShared.getIntValue(txtTQty.getText());
                        }*/

                    /*
                    boolean isBracketEnabled = false;
                    try {
                        if((type == ORDER_TYPE_STOPLOSS_LIMIT) || (type == ORDER_TYPE_STOPLOSS_MARKET)){
                            if(bracketCheck.isSelected()){
                                isBracketEnabled = true;
                            }
                        }
                    } catch(Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }*/
                    int tPlus = 2;
                    String bookKeeper = null;
                    try {
                        bookKeeper = ((TWComboItem) bookKeepersCombo.getSelectedItem()).getId();
                        if (t0SellChkBox.isSelected()) {
                            tPlus = 0;
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        tPlus = 2;
                    }

                    return TradeMethods.getSharedInstance().sendNewOrderForExec(!queued /* if this is a queued order, there is no need to authenticate.
                                                                                        it will be authenticated later while sending */
                            , queued, qID, ((TWComboItem) cmbPortfolioNos.getSelectedItem()).getId(),
                            exchange, txtSymbol.getText(), side, type, TradingShared.getDoubleValue(txtTPrice.getText()),
                            maxPrice, currency, getQuantity(), tiff, goodTillLong,
                            TradingShared.getIntValue(txtDisclosed.getText()), /*TradingShared.getIntValue(txtMinFill.getText())*/ txtMinFill.getNumber().intValue(),
                            txtAllOrNone.isSelected(), stock.getStrikePrice(),
                            TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(),
                            stopPrice, stock.getOptionBaseSymbol(),
                            TradeMeta.CONDITION_STATUS_PENDING, behaviourRule, conditionStartDate, conditionExpireDate, takeProfit,
                            enabledSliceOrders, execType, timeinterval, blocksize, stopLossType, isBracketOrder, trailStopPrice, dayOrder.isSelected(), null, refID.getText(), bookKeeper, tPlus, bracketRule);
                }
            }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }


    public boolean isStopLosssDataAvailable() {
        double stopPrice = -1;
        double trailStopPrice = -1;
        double takeProfit = -1;

        try {
            stopPrice = Double.parseDouble(bracketStopLoss.getText());
        } catch (Exception e) {
            stopPrice = 0;
        }
        try {
            trailStopPrice = Double.parseDouble(bracketTrailStopLoss.getText());
        } catch (Exception e) {
            trailStopPrice = 0;
        }
        try {
            takeProfit = Double.parseDouble(bracketTakeProf.getText());
        } catch (Exception e) {
            takeProfit = 0;
        }

        return ((stopPrice > 0) || (takeProfit > 0) || (trailStopPrice > 0));

    }

    private boolean cancelOrder(Transaction oldTransaction) {
        try {
            int currencyDecimals = 2;
            try {
                String currency = TradingShared.getTrader().findAccountByPortfolio(selectedPortfolio).getCurrency();
                currencyDecimals = SharedMethods.getCurrencyDecimalPlaces(currency);
            } catch (Exception e) {
            }

            String tiffStr = TradingShared.getTiffString(tiff, goodTillStr);
            int i = TradeMethods.getNewOrderConfirmMsg(oldTransaction.getSymbol(), oldTransaction.getExchange(), oldTransaction.getSecurityType(),
                    TradingShared.getDoubleValue(txtTPrice.getText()), currencyDecimals, oldTransaction.getStopLossPrice(), oldTransaction.getTakeProfitPrice(), oldTransaction.getOrderQuantity(),
                    null, null, 0f, type, TradeMeta.CANCEL, oldTransaction.getSide(), oldTransaction.getRuleType(), ((TWComboItem) bracketcmbSL.getSelectedItem()).getValue(),
                    oldTransaction.getTrailStopLossPrice(), false, tiffStr, oldTransaction.getPortfolioNo(), txtOrderValue.getText(), txtCommission.getText(), txtNetValue.getText(), null, null, null, TradingShared.getIntValue(txtDisclosed.getText()), null, false);

            /*ShowMessage oMessage = new ShowMessage(TradeMethods.getConfirmMessage(oldTransaction.getSymbol(), oldTransaction.getExchange(), oldTransaction.getSecurityType(),
                 TradingShared.getDoubleValue(txtTPrice.getText()), currencyDecimals, oldTransaction.getStopLossPrice(), oldTransaction.getTakeProfitPrice(),
//                    Float.parseFloat(txtTPrice.getText()),
                 Long.parseLong(txtTQty.getText()), null, null, 0f, type, TradeMeta.CANCEL, oldTransaction.getSide(), oldTransaction.getRuleType(),
                 ((TWComboItem) bracketcmbSL.getSelectedItem()).getValue(), oldTransaction.getTrailStopLossPrice(), false), "I", true);
         int i = oMessage.getReturnValue();*/
            if (i == JOptionPane.OK_OPTION) {
                if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                    TradeMethods.getSharedInstance().blockTillValidated();
                    if (TradingShared.level2AuthenticationSucess) {
                        TradeMethods.cancelOrder(oldTransaction);
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean amendOrder(boolean queued, Transaction oldTransaction) {
        TradeMessage tradeMessage2;
        try {
            if (!isRuleBasedValidationSucess(conditionalMode != TradeMeta.CONDITION_TYPE_NONE)) {
                return false;
            }
            Stock stock = DataStore.getSharedInstance().getStockObject(oldTransaction.getExchange(), oldTransaction.getSymbol(), oldTransaction.getSecurityType());
            Long ordQty = txtQty.getNumber().longValue();
            if (stock.getLotSize() > 0) {
                ordQty = ordQty * stock.getLotSize();
            }
            String field;
            try {
                field = conditionMethods.get(cmbConditionMethods.getSelectedIndex()).getValue();
            } catch (Exception e) {
                field = null;
            }

            String operator;
            try {
                operator = conditionOperators.get(cmbConditionOperators.getSelectedIndex()).getValue();
            } catch (Exception e) {
                operator = null;
            }

            double conditionValue;
            try {
                conditionValue = Double.parseDouble(txtConditionValue.getText().trim());
            } catch (Exception e) {
                conditionValue = 0;
            }
            double stopPrice = -1;
            double trailStopPrice = -1;
            double takeProfit = -1;
            int stopLossType = -1;
            String stopLossTypeStr = "";
            boolean isBracketOrder = false;
            boolean isDayOrder = false;

            if ((type == TradeMeta.ORDER_TYPE_MARKET) || (type == TradeMeta.ORDER_TYPE_LIMIT) /*||
                    (type == TradeMeta.ORDER_TYPE_DAY_LIMIT) || (type == TradeMeta.ORDER_TYPE_DAY_MARKET)*/) {
                isDayOrder = dayOrder.isSelected();
                isBracketOrder = bracketCheck.isSelected();
                if (bracketCheck.isSelected()) {
                    try {
                        stopPrice = Double.parseDouble(bracketStopLoss.getText());
                    } catch (Exception e) {
                        stopPrice = 0;
                    }
                    try {
                        trailStopPrice = Double.parseDouble(bracketTrailStopLoss.getText());
                    } catch (Exception e) {
                        trailStopPrice = 0;
                    }
                    try {
                        takeProfit = Double.parseDouble(bracketTakeProf.getText());
                    } catch (Exception e) {
                        takeProfit = 0;
                    }
                    stopLossType = Integer.parseInt(((TWComboItem) bracketcmbSL.getSelectedItem()).getId());
                    stopLossTypeStr = (((TWComboItem) bracketcmbSL.getSelectedItem()).getValue());
                }
            }
            /*else if ((type == TradeMeta.ORDER_TYPE_STOP_LIMIT) || (type == TradeMeta.ORDER_TYPE_STOP_MARKET)){
                if(isEnableBracketOrderPanel) {
                    try {
                        stopPrice = Double.parseDouble(txtStopPrice.getText());
                    } catch (Exception e) {
                        stopPrice = 0;
                    }
                    stopLossType = Integer.parseInt(((TWComboItem)cmbStopPrice.getSelectedItem()).getId());
                    stopLossTypeStr = (((TWComboItem)cmbStopPrice.getSelectedItem()).getValue());
                } else {
                    try {
                        stopPrice = Double.parseDouble(stopLossCardField.getText());
                    } catch (Exception e) {
                        stopPrice = 0;
                    }
                    stopLossType = -1;
                    stopLossTypeStr = "";
                }
                if(stradleCheck.isSelected()) {
                    try {
                        takeProfit = Double.parseDouble(stradleTakeProf.getText());
                    } catch (Exception e) {
                        takeProfit = 0;
                    }
                }
            }*/
            else if ((type == TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT) || (type == TradeMeta.ORDER_TYPE_STOPLOSS_MARKET)) {
                isBracketOrder = true;
                try {
                    stopPrice = Double.parseDouble(txtStopPrice.getText());
                } catch (Exception e) {
                    stopPrice = 0;
                }
                try {
                    trailStopPrice = Double.parseDouble(bracketTrailStopLoss.getText());
                } catch (Exception e) {
                    trailStopPrice = 0;
                }
                try {
                    takeProfit = Double.parseDouble(stradleTakeProf.getText());
                } catch (Exception e) {
                    takeProfit = 0;
                }
                stopLossType = Integer.parseInt(((TWComboItem) cmbStopPrice.getSelectedItem()).getId());
                stopLossTypeStr = (((TWComboItem) cmbStopPrice.getSelectedItem()).getValue());
            }


            long qty;
            if ((TradingShared.isAmendModeDelta())) {
                qty = (int) totalQty;                                            /* Adjust the total qty */
                qty += getQuantity() - remainingQty;
            } else {
                qty = getQuantity();
            }

            int blockSize = -1;
            long timeInterval = 0;

            if (enabledSliceOrders) { // added by udaya
                if (txtexecBlockValue.isEnabled()) {
                    try {
                        blockSize = Integer.parseInt(txtexecBlockValue.getText());
                        if (blockSize < 1) {
                            SharedMethods.showMessage(Language.getString("INVALID_SLICED_BLOCK_SIZE"), JOptionPane.ERROR_MESSAGE);
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        SharedMethods.showMessage(Language.getString("INVALID_SLICED_BLOCK_SIZE"), JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }

                timeInterval = -1;
                if (txtexecTimeValue.isEnabled()) {
                    try {
                        timeInterval = Long.parseLong(txtexecTimeValue.getText());
                        if (timeInterval < 1) {
                            SharedMethods.showMessage(Language.getString("INVALID_SLICED_TIME_INTERVAL"), JOptionPane.ERROR_MESSAGE);
                            return false;
                        }
                    } catch (NumberFormatException e) {
                        SharedMethods.showMessage(Language.getString("INVALID_SLICED_TIME_INTERVAL"), JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }
            }

            if ((blockSize > getQuantity()) || ((txtMinFill.getText().trim().length() > 0) && (blockSize > txtMinFill.getNumber().intValue()/*TradingShared.getIntValue(txtMinFill.getText())*/))) {
                SharedMethods.showMessage(Language.getString("MSG_INVALID_SLICE_ORDER_QTY"), JOptionPane.ERROR_MESSAGE);
                return false;
            }

            int currencyDecimals = 2;
            try {
                String currency = TradingShared.getTrader().findAccountByPortfolio(selectedPortfolio).getCurrency();
                currencyDecimals = SharedMethods.getCurrencyDecimalPlaces(currency);
            } catch (Exception e) {
            }

            String tiffStr = TradingShared.getTiffString(tiff, goodTillStr);
            if (conditionalMode == TradeMeta.CONDITION_TYPE_NORMAL) {
                ConditionalBehaviour behaviour = new ConditionalBehaviour();
                behaviour.setConditionCategory(MIXConstants.CONDITION_CATEGORY_PRE_CONDITION);
                behaviour.setConditionType(TradeMeta.CONDITION_TYPE_NORMAL);

                Condition condtion = new Condition();
                condtion.setOperator(conditionOperators.get(cmbConditionOperators.getSelectedIndex()).getId());
                condtion.setConditionParameter(conditionMethods.get(cmbConditionMethods.getSelectedIndex()).getId());
                condtion.setConditionValue(txtConditionValue.getText().trim());

                String ruleExpiry = TradingShared.formatExpireDate(TradingShared.getGoodTillLong(txtConditionExpiry.getText()));
                String conditionStartDate = TradingShared.formatExpireDate(TradingShared.getGoodTillLong(TradingShared.getTrader().getCurrentDay(selectedPortfolio)));
                behaviour.setConditionStartTime(conditionStartDate);
                behaviour.setExpireTime(ruleExpiry);
                ArrayList<Condition> list = new ArrayList<Condition>();
                list.add(condtion);
                behaviour.setConditions(list);
                behaviour.setConditionStatus(TradeMeta.CONDITION_STATUS_AMENDED);
                behaviourRule = behaviour;

            }

            int i = TradeMethods.getNewOrderConfirmMsg(oldTransaction.getSymbol(), oldTransaction.getExchange(), oldTransaction.getSecurityType(),
                    TradingShared.getDoubleValue(txtTPrice.getText()), currencyDecimals, stopPrice, takeProfit, getQuantity(), field, operator, conditionValue,
                    type, TradeMeta.AMEND, oldTransaction.getSide(), conditionalMode, /*oldTransaction.getRuleType()*/ stopLossTypeStr, trailStopPrice, isDayOrder, tiffStr, oldTransaction.getPortfolioNo(),
                    txtOrderValue.getText(), txtCommission.getText(), txtNetValue.getText(), txtInitMargin.getText(),
                    txtMainMargin.getText(), null, TradingShared.getIntValue(txtDisclosed.getText()), behaviourRule, false);

            if (i == JOptionPane.OK_OPTION) {
                if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                    TradeMethods.getSharedInstance().blockTillValidated();
                    if (TradingShared.level2AuthenticationSucess) {
                        TRSOrder tradeMessage = new TRSOrder();
                        if (oldTransaction.getSliceExecType() != -1) {
                            tradeMessage2 = new TradeMessage(MT_SLICED_ORDER);
                            tradeMessage.setSliceOrderStatus(SLICE_STATUS_AMENDED);
                        } else {
                            if ((conditionalMode == TradeMeta.CONDITION_TYPE_NORMAL) /*||
                                    ((conditionalMode == TradeMeta.CONDITION_TYPE_STOP_LOSS) && (side == TradeMeta.SELL))*/) {
                                tradeMessage2 = new TradeMessage(MT_CONDITIONAL_ORDER);
                            } else {
                                tradeMessage2 = new TradeMessage(MT_AMEND);
                            }
                        }

                        String newrule = null;
                        String ruleExpiry = null;
                        if (conditionalMode == TradeMeta.CONDITION_TYPE_NORMAL) {
                            ConditionalBehaviour behaviour = new ConditionalBehaviour();
                            behaviour.setConditionCategory(MIXConstants.CONDITION_CATEGORY_PRE_CONDITION);
                            behaviour.setConditionType(TradeMeta.CONDITION_TYPE_NORMAL);

                            Condition condtion = new Condition();
                            condtion.setOperator(conditionOperators.get(cmbConditionOperators.getSelectedIndex()).getId());
                            condtion.setConditionParameter(conditionMethods.get(cmbConditionMethods.getSelectedIndex()).getId());
                            condtion.setConditionValue(txtConditionValue.getText().trim());

                            StringBuilder ruleBuffer = new StringBuilder();
                            ruleBuffer.append(exchange);
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(symbol);
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(conditionMethods.get(cmbConditionMethods.getSelectedIndex()).getId());
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(conditionOperators.get(cmbConditionOperators.getSelectedIndex()).getId());
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(txtConditionValue.getText().trim());
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append("0"); // no second value
                            rule = ruleBuffer.toString();
                            newrule = ruleBuffer.toString();
                            ruleExpiry = TradingShared.formatExpireDate(TradingShared.getGoodTillLong(txtConditionExpiry.getText()));
                            String conditionStartDate = TradingShared.formatExpireDate(TradingShared.getGoodTillLong(TradingShared.getTrader().getCurrentDay(selectedPortfolio)));
                            behaviour.setConditionStartTime(conditionStartDate);
                            behaviour.setExpireTime(ruleExpiry);
                            ArrayList<Condition> list = new ArrayList<Condition>();
                            list.add(condtion);
                            behaviour.setConditions(list);
                            behaviour.setConditionStatus(TradeMeta.CONDITION_STATUS_AMENDED);
                            behaviourRule = behaviour;

                        } else if (conditionalMode == TradeMeta.CONDITION_TYPE_STOP_LOSS) {
                            StringBuilder ruleBuffer = new StringBuilder();
                            ruleBuffer.append(exchange);
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(symbol);
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(0); // no method
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(0); // no operator
//                            ruleBuffer.append(TradeMeta.DD);                //todo - commented by Dilum - the change order request came from Shiran
//                            ruleBuffer.append(takeProfit);             //todo - commented by Dilum - the change order request came from Shiran
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(stopPrice);
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(stopLossType);
                            ruleBuffer.append(TradeMeta.DD);
                            ruleBuffer.append(takeProfit);
                            rule = ruleBuffer.toString();
                            newrule = ruleBuffer.toString();
                            ruleExpiry = TradingShared.formatExpireDate(TradingShared.getGoodTillLong(txtConditionExpiry.getText()));
                        }
                        if (isBracketOrder && !((type == TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT) || (type == TradeMeta.ORDER_TYPE_STOPLOSS_MARKET))) {     //todo added By Udaya for MIX implimentation
                            ConditionalBehaviour behaviour = new ConditionalBehaviour();
                            behaviour.setConditionCategory(MIXConstants.CONDITION_CATEGORY_POST_CONDITION);
                            behaviour.setConditionType(MIXConstants.CONDITION_TYPE_BRACKET);
                            String conditionExpireDate = TradingShared.formatExpireDate((System.currentTimeMillis() + (60 * 60 * 24 * 1000)));
//                    conditionExpireDate = TradingShared.formatExpireDate();
                            String conditionStartDate = TradingShared.formatExpireDate(TradingShared.getGoodTillLongYYYYDDMMHHMMSS(TradingShared.getTrader().getCurrentDay(selectedPortfolio)));
                            behaviour.setConditionStartTime(conditionStartDate);
                            behaviour.setExpireTime(conditionStartDate);
                            behaviour.setConditionStatus(TradeMeta.CONDITION_STATUS_AMENDED);
                            bracketRule = behaviour;

                        }

                        btnAmend.setEnabled(false);
                        int tPlus = 2;
                        String bookKeeper = null;
                        try {
                            bookKeeper = ((TWComboItem) bookKeepersCombo.getSelectedItem()).getId();
                            if (t0SellChkBox.isSelected()) {
                                tPlus = 0;
                            }
                        } catch (Exception e) {
                            tPlus = 2;
                        }
                        Order order = TradeMethods.amendOrder(oldTransaction, queued, side, type, TradingShared.getDoubleValue(txtTPrice.getText()),
                                currency, qty, tiff, goodTillLong, txtDisclosed.getText(), getMinFill() + "", stopPrice, behaviourRule, ruleExpiry,
                                takeProfit, timeInterval, blockSize, conditionalMode, stopLossType, isDayOrder, trailStopPrice, false, null, bookKeeper, bracketRule);
                        if (queued) {
                            currentTransaction.setMixOrderObject(order);

                        }
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean validateStopLossSell() {
        return true;
    }

    private void loadRules() {
        ruleTable.clear();
        try {
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out = decompress.setFiles("rules/" + exchange + "/qv.msf");
            decompress.decompress();
            ruleTable.put("QUANTITY_VALIDATION", new String(out.toByteArray()));
        } catch (Exception e) {
        }
        try {
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out = decompress.setFiles("rules/" + exchange + "/pv.msf");
            decompress.decompress();
            ruleTable.put("PRICE_VALIDATION", new String(out.toByteArray()));
        } catch (Exception e) {
        }
    }

    private void adjustQuantityUpDown(int keyCode) {
        long increment = 1;
        int direction = 1;

        if (keyCode == KeyEvent.VK_UP) {
            direction = 1;
            increment = 1;
        } else if (keyCode == KeyEvent.VK_DOWN) {
            direction = -1;
            increment = -1;
        }
        try {
            Interpreter interpreter;// = new Interpreter();
            interpreter = loadInterpriter(direction);
            increment = (Long) interpreter.eval(ruleTable.get("QUANTITY_VALIDATION"));
//            increment = (Long)interpreter.source("rules/" + exchange + "/qv.java");
            txtQty.setText(String.valueOf(increment));
            interpreter = null;
        } catch (Exception e) { // could not eveluate the rule or rule not found
            //e.printStackTrace();
            long currentQty = getQuantity();
            if (direction == 1) {
                txtQty.setText(String.valueOf(currentQty + increment));

            } else if (currentQty > 0) {
                txtQty.setText(String.valueOf(currentQty + increment));

            }
        }
    }

    private double getDefaultIncrement() {
        try {
            int decimalCount = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument).getDecimalCount();
            return Math.pow(10, -decimalCount);
        } catch (Exception e) {
            return 1;
        }
    }

    private void adjustPriceUpDown(int keyCode) {
        double increment = 1;
        int direction = 1;
        int decimalPlaces;

        if (keyCode == KeyEvent.VK_UP) {
            direction = 1;
            increment = getDefaultIncrement();
        } else if (keyCode == KeyEvent.VK_DOWN) {
            direction = -1;
            increment = -getDefaultIncrement();
        }

        try {
            Interpreter interpreter;// = new Interpreter();
            interpreter = loadInterpriter(direction);
            increment = (Double) interpreter.eval(ruleTable.get("PRICE_VALIDATION"));
//            increment = (Double)interpreter.source("rules/" + exchange + "/pv.java");
//            decimalPlaces = ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceDecimalPlaces();
            decimalPlaces = SharedMethods.getDecimalPlaces(exchange, symbol, instrument);
            txtTPrice.setText(SharedMethods.getDecimalFormatNoComma(decimalPlaces).format(increment));
//            txtTPrice.setText(SharedMethods.formatToDecimalPlacesNumeric(decimalPlaces ,increment));
            interpreter = null;
        } catch (Exception e) { // could not eveluate the rule or rule not found
            double currentPrice = getPrice();
            if ((currentPrice + increment) > 0) {
                //decimalPlaces = ExchangeStore.getSharedInstance().getExchange(currentExchange).getPriceDecimalPlaces();
//                txtTPrice.setText(SharedMethods.formatToDecimalPlacesNumeric(decimalPlaces ,currentPrice + increment));
                txtTPrice.setText(SharedMethods.getDecimalFormatNoComma(currentExchange, symbol, instrument).format(currentPrice + increment));
            }

        }

    }

    private Interpreter loadInterpriter(int direction) throws Exception {
        String sectorCode;
        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
        try {
            sectorCode = stock.getSectorCode();
        } catch (Exception e) {
            sectorCode = "";
        }
        int mktStatus = ExchangeStore.getSharedInstance().getExchange(exchange).getMarketStatus();
        Interpreter interpreter = new Interpreter();
        interpreter.set("symbol", symbol);
        interpreter.set("instrumentType", instrument);
        interpreter.set("sector", sectorCode);
        interpreter.set("price", getPrice());
        interpreter.set("quantity", getQuantity());
        interpreter.set("min", minPrice);
        interpreter.set("max", maxPrice);
        interpreter.set("open", openPrice);
        interpreter.set("high", highPrice);
        interpreter.set("low", lowPrice);
        interpreter.set("side", side);
        interpreter.set("tiff", tiff);
        interpreter.set("type", type);
        interpreter.set("disclosed", getDisclosed());
        interpreter.set("minfill", getMinFill());
        interpreter.set("marketCode", marketCode);
        interpreter.set("refPrice", refPrice);
        interpreter.set("bid", bid);
        interpreter.set("offer", offer);
        interpreter.set("direction", direction);
        interpreter.set("mktstatus", mktStatus);
        interpreter.set("lotsize", stock.getLotSize());

        return interpreter;
    }

    public void applyTheme() {
        applyColors();
        btnShowDays.setBorder(null);
    }

    public void applyColors() {
        Color conditionBGColor = null;

        companyName.setForeground(Theme.getColor("ORDER_ENTRY_COMPANY_FGCOLOR"));
        lblBuyingPower.setForeground(Theme.getColor("ORDER_ENTRY_BUYING_POWER_FGCOLOR"));
        //portfolioDataPanel.setBackground(Theme.getColor("ORDER_ENTRY_PORTFOLIO_PANEL_BGCOLOR"));

        if (cmbAction.getSelectedItem().equals(Language.getString("BUY"))) {
            if (dayOrder.isSelected()) {
                dataPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                bracketPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                cardPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                stradlePanel.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                blankCardPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                buttonPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                cmbType.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                cmbAction.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                t0SellChkBox.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                dayOrder.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                cmbGoodTills.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                txtAllOrNone.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                executionPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                cmbexecTypeOperators.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                bracketcmbSL.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                //          advancedPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                advanceTabPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                mainTabbedPane.setBackground(Theme.getColor("DAY_ORDER_ROW_BUY_BGCOLOR"));
                bracketlblSL.setText(Language.getString("STOP_LOSS"));
                bracketCheck.setText(Language.getString("SELL_BRACKET"));
                conditionBGColor = Theme.getOptionalColor("DAY_ORDER_ROW_BUY_BGCOLOR");
            } else {
                dataPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                bracketPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                cardPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                stradlePanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                blankCardPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                buttonPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                cmbType.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                cmbAction.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                t0SellChkBox.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                dayOrder.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                cmbGoodTills.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                txtAllOrNone.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                executionPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                cmbexecTypeOperators.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                bracketcmbSL.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                //       advancedPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                advanceTabPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                mainTabbedPane.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                bracketlblSL.setText(Language.getString("STOP_LOSS"));
                bracketCheck.setText(Language.getString("SELL_BRACKET"));
                conditionBGColor = Theme.getOptionalColor("BOARD_TABLE_CELL_BID_BGCOLOR1");
            }
        } else {
            if (dayOrder.isSelected()) {
                dataPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                bracketPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                cardPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                stradlePanel.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                blankCardPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                buttonPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                cmbType.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                cmbAction.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                t0SellChkBox.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                dayOrder.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                cmbGoodTills.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                txtAllOrNone.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                executionPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                cmbexecTypeOperators.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                bracketcmbSL.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                //     advancedPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                advanceTabPanel.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                mainTabbedPane.setBackground(Theme.getColor("DAY_ORDER_ROW_SELL_BGCOLOR"));
                bracketlblSL.setText(Language.getString("STOP_PRICE"));
                bracketCheck.setText(Language.getString("BUY_BRACKET"));
                conditionBGColor = Theme.getOptionalColor("DAY_ORDER_ROW_SELL_BGCOLOR");
            } else {
                dataPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                bracketPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                cardPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                stradlePanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                blankCardPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                buttonPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                cmbType.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                cmbAction.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                t0SellChkBox.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                dayOrder.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                cmbGoodTills.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                txtAllOrNone.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                executionPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                cmbexecTypeOperators.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                bracketcmbSL.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
                //          advancedPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                advanceTabPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                mainTabbedPane.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
                bracketlblSL.setText(Language.getString("STOP_PRICE"));
                bracketCheck.setText(Language.getString("BUY_BRACKET"));
                conditionBGColor = Theme.getOptionalColor("BOARD_TABLE_CELL_ASK_BGCOLOR1");
            }
        }
        coveragelbl.setForeground(Theme.getColor("NEW_ORDER_COVERAGE_FG"));
        imglbl2.setForeground(Theme.getColor("NEW_ORDER_COVERAGE_VAL_FG"));


        conditionPanel.setBackground(conditionBGColor);
        Font fnt = new Font("Arial", 1, 12);
        conditionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Theme.getColor("BUYSELL_BORDER_COLOR")), Language.getString("CONDITION"), 0, 0, fnt, Theme.getColor("BUYSELL_BORDER_TITLE_COLOR")));
        executionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Theme.getColor("BUYSELL_BORDER_COLOR")), Language.getString("SLICE_ORDERS_PANEL"), 0, 0, fnt, Theme.getColor("BUYSELL_BORDER_TITLE_COLOR")));
        cmbConditionMethods.setBackground(conditionBGColor);
        cmbConditionOperators.setBackground(conditionBGColor);
        cmbConditionExpiry.setBackground(conditionBGColor);
    }

    private void setBrackerOrderPanelStatus(boolean status) {
        bracketCheck.setSelected(status);
        bracketTakeProf.setEnabled(status);
        bracketStopLoss.setEnabled(status);
        bracketTrailStopLoss.setEnabled(status);
        bracketcmbSL.setEnabled(status);
    }

    private JPanel getDateCombo() {
        String[] widths = {"100%", "16"};
        String[] heights = {"15"};
        FlexGridLayout expiryLayout = new FlexGridLayout(widths, heights, 0, 0);
        cmbGoodTills = new JPanel(expiryLayout);
        cmbGoodTills.setBorder(BorderFactory.createEtchedBorder());
        cmbGoodTills.setPreferredSize(new Dimension(100, 20));
        txtGoodTill = new JLabel();
        txtGoodTill.setPreferredSize(new Dimension(80, 16));
        txtGoodTill.setOpaque(false);
        cmbGoodTills.add(txtGoodTill);
        btnShowDays = new JButton(new DownArrow());
        btnShowDays.setBorder(BorderFactory.createEmptyBorder());
        btnShowDays.addActionListener(this);
        btnShowDays.setContentAreaFilled(false);
        btnShowDays.setOpaque(false);
        btnShowDays.setPreferredSize(new Dimension(16, 16));
        cmbGoodTills.add(btnShowDays);
        return cmbGoodTills;
    }

    private JPanel getConditionExpiryCombo() {
        String[] widths = {"100%", "16"};
        String[] heights = {"15"};
        FlexGridLayout expiryLayout = new FlexGridLayout(widths, heights, 0, 0);
        cmbConditionExpiry = new JPanel(expiryLayout);
        cmbConditionExpiry.setBorder(BorderFactory.createEtchedBorder());
        txtConditionExpiry = new JLabel();
        txtConditionExpiry.setOpaque(false);
        cmbConditionExpiry.add(txtConditionExpiry);
        btnShowConditionDays = new JButton(new DownArrow());
        btnShowConditionDays.setActionCommand("COND_EXPIRY");
        btnShowConditionDays.setBorder(BorderFactory.createEmptyBorder());
        btnShowConditionDays.addActionListener(this);
        btnShowConditionDays.setContentAreaFilled(false);
        btnShowConditionDays.setOpaque(false);
        cmbConditionExpiry.add(btnShowConditionDays);
        setConditionExpity(null);  //
        return cmbConditionExpiry;
    }

    private void enableBracketOrderPanel(boolean status) {
        bracketCheck.setEnabled(status);
        bracketTakeProf.setEnabled(status);
        bracketStopLoss.setEnabled(status);
        bracketTrailStopLoss.setEnabled(status);
        bracketcmbSL.setEnabled(status);
    }

    private void enableSliceOrderPanel(boolean status) {
        cmbexecTypeOperators.setEnabled(status);
        // txtexecBlockValue.setEnabled(status);  //will be enabled when the item is selected
        //  txtexecTimeValue.setEnabled(status);
    }

    private void enableDayOrderPanel(boolean status) {
        dayOrder.setEnabled(status);
    }

    private void enableConditionalPanel(boolean status) {
        cmbConditionMethods.setEnabled(status);
        //  cmbConditionOperators.setEnabled(status);
        //   txtConditionValue.setEnabled(status);       //will be enabled when the item is selected
        //   btnShowConditionDays.setEnabled(status);
    }

    private void setSelectedPortfolioData(String portfolio) {
        byte path = TradingShared.getTrader().getPath(portfolio);
        TradeMethods.getSharedInstance().populateOrderTypes(exchange, orderTypesList, path);
        if (TradingShared.isConditionalOrdersEnabled(path)) {
            enableConditionalPanel(true);
        } else {
            enableConditionalPanel(false);
        }
        if (TradingShared.isBracketOrderEnabled(path)) {
            enableBracketOrderPanel(true);
            setBrackerOrderPanelStatus(false);
        } else {
            enableBracketOrderPanel(false);
        }
        if (TradingShared.isSlicedOrderEnabled(path)) {
            enableSliceOrderPanel(true);
        } else {
            enableSliceOrderPanel(false);
        }
        if (TradingShared.isDayOrdersEnabled(path)) {
            enableDayOrderPanel(true);
        } else {
            enableDayOrderPanel(false);
        }
    }

    public void itemStateChanged(ItemEvent e) {
        if ((e.getSource() == cmbPortfolioNos) && (e.getStateChange() == ItemEvent.SELECTED)) {
            selectedPortfolio = ((TWComboItem) cmbPortfolioNos.getSelectedItem()).getId();
            try {
                TradeMethods.getSharedInstance().populateBookKeepers(selectedPortfolio, bookKeepers);
                bookKeepersCombo.updateUI();
                bookKeepersCombo.setSelectedIndex(TradeMethods.getSharedInstance().getDefaultBookKeeper(selectedPortfolio, bookKeepers));
            } catch (Exception e1) {
//                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            setSelectedPortfolioData(selectedPortfolio);
            updateMarginPanel();

        } else if ((e.getSource() == cmbexecTypeOperators) && (e.getStateChange() == ItemEvent.SELECTED)) {
            String id = ((TWComboItem) cmbexecTypeOperators.getSelectedItem()).getId();
            if (id.equals("*")) {
                enabledSliceOrders = false;
                txtexecBlockValue.setEnabled(false);
                txtexecTimeValue.setEnabled(false);
            } else if ((Integer.parseInt(id) == TradeMeta.SLICE_ORD_TYPE_TIME_INTERVAL) || (Integer.parseInt(id) == TradeMeta.SLICE_ORD_TYPE_ICEBURG)) {
                enabledSliceOrders = true;
                txtexecBlockValue.setEnabled(true);
                txtexecTimeValue.setEnabled(true);
            } else {
                enabledSliceOrders = true;
                txtexecBlockValue.setEnabled(true);
                txtexecTimeValue.setEnabled(false);
            }
        } else if ((e.getSource() == cmbConditionMethods) && (e.getStateChange() == ItemEvent.SELECTED)) {
            if (cmbConditionMethods.getSelectedIndex() == 0) {
                conditionalMode = CONDITION_TYPE_NONE;
                cmbConditionOperators.setEnabled(false);
                txtConditionValue.setEnabled(false);
                btnShowConditionDays.setEnabled(false);
            } else {
                conditionalMode = CONDITION_TYPE_NORMAL;
                cmbConditionOperators.setEnabled(true);
                txtConditionValue.setEnabled(true);
                btnShowConditionDays.setEnabled(true);
            }
        }
    }

    public void focusGained(FocusEvent e) {
    }

    public void focusLost(FocusEvent e) {
        if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
            if (keyTyped) {
                boolean validated = doInternalvalidate(txtSymbol.getText());
                if (validated && (selectedStock != null)) {
                    boolean isSymbolTypeOption = isSymbolTypeOption(selectedStock.getInstrumentType());
                    populateAccountStatusPanel(isFuturesSymbol(selectedStock.getInstrumentType()));
                    populateInfoPanel(isSymbolTypeOption);
                    populateOrderDataPanel(isSymbolTypeOption);
                    setTitle(selectedStock.getInstrumentType());
                }
            }
        } else {
            if (keyTyped && e.getSource().equals(txtSymbol)) {
                invalidateStock();
                lockInputs(true);
                validateSymbol(txtSymbol.getText());
            }
        }
    }

    public void windowActivated(WindowEvent e) {

    }

    public void windowClosed(WindowEvent e) {
        //finalizeWindow();
    }

    public void windowClosing(WindowEvent e) {
        //finalizeWindow();
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        if (e.getSource() == txtSymbol) {
            invalidateStock();
            if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
                boolean isValidated = doInternalvalidate(txtSymbol.getText());
                if (isValidated && (selectedStock != null)) {
                    boolean isSymbolTypeOption = isSymbolTypeOption(selectedStock.getInstrumentType());
                    populateAccountStatusPanel(isFuturesSymbol(selectedStock.getInstrumentType()));
                    populateInfoPanel(isSymbolTypeOption);
                    populateOrderDataPanel(isSymbolTypeOption);
                    setTitle(selectedStock.getInstrumentType());
                }
            } else {
                keyTyped = true;
            }
        } else if (e.getSource().equals(txtQty)) {
            if ((e.getKeyCode() == KeyEvent.VK_UP) || (e.getKeyCode() == KeyEvent.VK_DOWN)) {
                adjustQuantityUpDown(e.getKeyCode());
            }
        } else if (e.getSource().equals(txtTPrice)) {
            if ((e.getKeyCode() == KeyEvent.VK_UP) || (e.getKeyCode() == KeyEvent.VK_DOWN)) {
                adjustPriceUpDown(e.getKeyCode());
            }
        }
    }

    public void keyTyped(KeyEvent e) {
        //exchange = null;
        //keyTyped = true;

    }

    public void mouseClicked(MouseEvent e) {
//        InfoLabel field = (InfoLabel)e.getSource();
//        txtTPrice.setText(field.getText());
//        field = null;
    }

    public void mouseEntered(MouseEvent e) {
//        InfoLabel field = (InfoLabel)e.getSource();
//        field.setFont(field.getFont().deriveFont((float)field.getFont().getSize()+1F));
//        field = null;
    }

    public void mouseExited(MouseEvent e) {
//        InfoLabel field = (InfoLabel)e.getSource();
//        field.setFont(field.getFont().deriveFont((float)field.getFont().getSize()-1F));
//        field = null;
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void accountDataChanged(String accountID) {
        setPowerValues();
    }

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios();
    }

    public void internalFrameActivated(InternalFrameEvent e) {
    }

    public void internalFrameClosed(InternalFrameEvent e) {
        System.out.println("--------------Closing window");
        removeValidatedSymbols();
        finalizeWindow();
        super.internalFrameClosed(e);
        System.out.println("--------------Window closed");
    }

/*    public void internalFrameClosing(InternalFrameEvent e) {

    }

    public void internalFrameDeactivated(InternalFrameEvent e) {

    }

    public void internalFrameDeiconified(InternalFrameEvent e) {

    }

    public void internalFrameIconified(InternalFrameEvent e) {

    }*/

    /*public void internalFrameOpened(InternalFrameEvent e) {
        System.out.println("txtSymbol null " + (txtSymbol== null));
        System.out.println("txtTQty null   " + (txtTQty== null));
        if (txtSymbol!= null){
            if (txtSymbol.getText().equals("")){
                txtSymbol.requestFocus();
                return;
            }
        } else if (txtTQty != null){
            txtTQty.requestFocus();
        }
    }*/

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        datePicker.hide();
        setGoodTillDate(iYear, iMonth, iDay);
    }

    public void setSymbol(String key) {
        txtSymbol.setEnabled(true);
        btnSelectSymbol.setEnabled(true);
//        symbol = SharedMethods.getSymbolFromKey(key);
//        exchange = SharedMethods.getExchangeFromKey(key);
//        this.instrument = SharedMethods.getInstrumentTypeFromKey(key);
        setSelectedStock(key);
        TradeMethods.getSharedInstance().populateOrderTypes(exchange, orderTypesList, TradingShared.getTrader().getPath(selectedPortfolio));
        lockInputs(false);
        keyTyped = false;
        validatedSymbols.add(key);
//        selectedStock = DataStore.getSharedInstance().getStockObject(key);
        if ((instrument == Meta.INSTRUMENT_FUTURE) && (selectedStock != null)) {
            futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, selectedStock.getOptionBaseSymbol(), instrument));
        }
        setSpecialConditions();
        setPanelLabels();
        updateMarginPanel();
        /*if (isEnabledDayOrder) {
            if (isTPlusEnable) {
                if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 149));
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 140));  //95
                }
            } else {
                dataPanel.setPreferredSize(new Dimension(WIDTH, 140));   //95
            }
        } else {
            if (isTPlusEnable) {
                if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbol, instrument), "0")) {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 118));
                } else {
                    dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
                }
            } else {
                dataPanel.setPreferredSize(new Dimension(WIDTH, 95));
            }
        }
        dataPanel.doLayout();*/
        if (selectedStock != null) {
            boolean isSymbolTypeOption = isSymbolTypeOption(selectedStock.getInstrumentType());
            populateAccountStatusPanel(isFuturesSymbol(selectedStock.getInstrumentType()));
            populateInfoPanel(isSymbolTypeOption);
            populateOrderDataPanel(isSymbolTypeOption);
            setTitle(selectedStock.getInstrumentType());
        }
        SwingUtilities.updateComponentTreeUI(this);
//        updateUI();
        revalidate();
        Dimension oldSize = this.getSize();
        this.requestFocusInWindow();
        cmbAction.requestFocusInWindow();
        txtSymbol.setCaretPosition(txtSymbol.getText().length());

        /*if(exchange!=null && ExchangeStore.getSharedInstance().getExchange(exchange).isValidTradingIinformationType(TradeMeta.TIT_ENABLE_BRACKET_ORDERS) && !isEnableBracketOrderPanel){
            isEnableBracketOrderPanel = true;
            HEIGHT = HEIGHT + 80;
            this.setSize(oldSize.width, oldSize.height + 80);
        } else if(exchange!=null && !ExchangeStore.getSharedInstance().getExchange(exchange).isValidTradingIinformationType(TradeMeta.TIT_ENABLE_BRACKET_ORDERS) && isEnableBracketOrderPanel){
            isEnableBracketOrderPanel = false;
            HEIGHT = HEIGHT - 80;
            this.setSize(oldSize.width, oldSize.height - 80);
        }
        if(exchange!=null && ExchangeStore.getSharedInstance().getExchange(exchange).isValidTradingIinformationType(TradeMeta.TIT_ENABLE_ADVANCED_ORDERS) && !isEnabledAdvacedTabPanel){
            isEnabledAdvacedTabPanel = true;
            HEIGHT = HEIGHT + 20;
            this.setSize(oldSize.width, oldSize.height + 20);
        } else if(exchange!=null && !ExchangeStore.getSharedInstance().getExchange(exchange).isValidTradingIinformationType(TradeMeta.TIT_ENABLE_ADVANCED_ORDERS) && isEnabledAdvacedTabPanel){
            isEnabledAdvacedTabPanel = false;
            HEIGHT = HEIGHT - 20;
            this.setSize(oldSize.width, oldSize.height - 20);
        }
        validateTradingWindowTypes();
        this.doLayout();*/
    }

    private void selectPortfolio(String portfolio) {
        try {
            selectedPortfolio = portfolio;
            for (int i = 0; i < TradingShared.getTrader().getPortfolioCount(); i++) {
                if (portfolio.equals(TradingShared.getTrader().getPortfolioID(i))) {
                    cmbPortfolioNos.setSelectedIndex(i);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void notifyInvalidSymbol(String symbol) {
        invalidateStock();
        lockInputs(false);
        keyTyped = false;
    }


    public void changedUpdate(DocumentEvent e) {
        setSpecialConditions();
    }

    public void insertUpdate(DocumentEvent e) {
        setSpecialConditions();
    }

    public void removeUpdate(DocumentEvent e) {
        setSpecialConditions();
    }

    public void adjustmentValueChanged(AdjustmentEvent e) {
        try {
            if (e.getSource().equals(quantitySpinner)) {
                if (e.getAdjustmentType() == AdjustmentEvent.TRACK) {
                    if (quantityScrollValue > e.getValue()) {
                        adjustQuantityUpDown(KeyEvent.VK_UP);
                    } else {
                        adjustQuantityUpDown(KeyEvent.VK_DOWN);
                    }
                    quantityScrollValue = e.getValue();
                }
            } else if (e.getSource().equals(priceSpinner)) {
                if (e.getAdjustmentType() == AdjustmentEvent.TRACK) {
                    if (priceScrollValue > e.getValue()) {
                        adjustPriceUpDown(KeyEvent.VK_UP);
                    } else {
                        adjustPriceUpDown(KeyEvent.VK_DOWN);
                    }
                    priceScrollValue = e.getValue();
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private long getQuantity() {
        try {
            return txtQty.getNumber().longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    private double getPrice() {
        try {
            return Double.parseDouble(txtTPrice.getText());
        } catch (Exception e) {
            return 0;
        }
    }

    private double getDisclosed() {
        try {
            return Double.parseDouble(txtDisclosed.getText());
        } catch (Exception e) {
            return 0;
        }
    }

    private long getMinFill() {
        try {
//            return Long.parseLong(txtMinFill.getText());
            return txtMinFill.getNumber().longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public Component getDefaultFocusComponent() {
        if (txtSymbol != null) {
            if (txtSymbol.getText().equals("")) {
                return txtSymbol;
            }
        }
        if (txtQty != null) {
            return txtQty;
        }
        return cmbPortfolioNos;
    }

    class TradingFocusTraversalPolicy extends FocusTraversalPolicy {
        //        private Component[] firstTab = {cmbPortfolioNos, txtSymbol, cmbAction, cmbType, txtTPrice, txtAllOrNone, txtQty, btnShowDays, txtMinFill, txtDisclosed, cmbStopPrice, txtStopPrice,
        //                btnBuy, btnSell, btnAmend, btnCancel, btnClose, btnQ};
        private Component[] firstTab = {cmbPortfolioNos, txtSymbol, cmbAction, cmbType, txtTPrice, txtAllOrNone, txtQty, btnShowDays, txtMinFill, txtDisclosed, bracketcmbSL, bracketStopLoss, bracketTakeProf,
                btnBuy, btnSell, btnAmend, btnCancel, btnClose, btnQ};

        private Component[] secondTab = {cmbPortfolioNos, txtSymbol,
                cmbexecTypeOperators, txtexecTimeValue, txtexecBlockValue,
                btnBuy, btnSell, btnAmend, btnCancel, btnClose, btnQ};

        private Component[] components = firstTab;

        public void reorderTraversalPolicy() {
            String type = ((TWComboItem) cmbType.getSelectedItem()).getId();
            if (type.equals(TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT + "") || type.equals(TradeMeta.ORDER_TYPE_STOPLOSS_MARKET + "")) {
                firstTab = new Component[]{cmbPortfolioNos, txtSymbol, cmbAction, cmbType, txtTPrice, txtAllOrNone, txtQty, btnShowDays, txtMinFill, txtDisclosed, cmbStopPrice, txtStopPrice, stradleTakeProf,
                        btnBuy, btnSell, btnAmend, btnCancel, btnClose, btnQ};
            } else {
                firstTab = new Component[]{cmbPortfolioNos, txtSymbol, cmbAction, cmbType, txtTPrice, txtAllOrNone, txtQty, btnShowDays, txtMinFill, txtDisclosed, bracketcmbSL, bracketStopLoss, bracketTakeProf,
                        btnBuy, btnSell, btnAmend, btnCancel, btnClose, btnQ};
            }

        }

        public Component getComponentAfter(Container aContainer, Component aComponent) {
            if (mainTabbedPane.getSelectedIndex() == 1) {
                components = secondTab;
            } else {
                components = firstTab;
            }
            int currentComponent = findComponent(aComponent);
            return components[findNextComponentIndex(currentComponent)];
        }

        public Component getComponentBefore(Container aContainer, Component aComponent) {
            int currentComponent = findComponent(aComponent);
            return components[findPreviousComponentIndex(currentComponent)];
        }

        public Component getDefaultComponent(Container aContainer) {
            return getDefaultFocusComponent();
        }

        public Component getFirstComponent(Container aContainer) {
            return txtQty;
        }

        public Component getLastComponent(Container aContainer) {
            return txtSymbol;
        }

        private int findComponent(Component aComponent) {
            for (int i = 0; i < components.length; i++) {
                if (aComponent.equals(components[i])) {
                    return i;
                }
            }
            return 0;
        }

        private int findNextComponentIndex(int current) {
            for (int i = current + 1; i < components.length; i++) {
                if (components[i].isEnabled() && components[i].isVisible()) {
                    return i;
                }
            }
            for (int i = 0; i < current; i++) {
                if (components[i].isEnabled() && components[i].isVisible()) {
                    return i;
                }
            }
            return current;
        }

        private int findPreviousComponentIndex(int current) {
            for (int i = current - 1; i >= 0; i--) {
                if (components[i].isEnabled() && components[i].isVisible()) {
                    return i;
                }
            }
            for (int i = components.length - 1; i > current; i--) {
                if (components[i].isEnabled() && components[i].isVisible()) {
                    return i;
                }
            }
            return current;
        }
    }

    private class LableValePair extends JPanel {

        private Component label;
        private Component value;
        private String[] widths = {"0", "100%"};
        private String[] heights = {"100%"};

        public LableValePair(Component label, Component value) {
            this.label = label;
            this.value = value;
            setOpaque(false);
            setLayout(new FlexGridLayout(widths, heights));
            add(label);
            add(value);
        }
    }

    private class LableValePairOrdered extends JPanel {

        private Component label;
        private Component value;
        private String[] widths = {"44%", "56%"};
        private String[] heights = {"100%"};

        public LableValePairOrdered(Component label, Component value) {
            this.label = label;
            this.value = value;
            setOpaque(false);
            setLayout(new FlexGridLayout(widths, heights));
            add(label);
            add(value);
        }
    }

    public String getCurrentSymbol() {
        if (symbol != null) {
            return SharedMethods.getKey(exchange, symbol, instrument);
        } else {
            return "Symbol Not Selected";
        }
    }

    public String getCurrentExchange() {
        if (exchange != null) {
            return exchange;
        } else {
            return "Symbol Not Selected";
        }
    }

    private class TWDataPanel extends JPanel {
        public TWDataPanel(String[] widths, int height) {
            super.setLayout(new FlexGridLayout(widths, new String[]{"" + height}));
        }
    }

    private double convertToSelectedCurrency(String selectedCurrency, String baseCurrency, double value) {
        if (side == SELL) {
            return value * CurrencyStore.getBuyRate(baseCurrency, selectedCurrency, TradingShared.getTrader().getPath(selectedPortfolio));
        } else {
            return value * CurrencyStore.getSellRate(baseCurrency, selectedCurrency, TradingShared.getTrader().getPath(selectedPortfolio));

        }
    }


    private void disableSliceOrders() {
        enabledSliceOrders = false;
        txtexecBlockValue.setEnabled(false);
        txtexecTimeValue.setEnabled(false);
    }

    private String getSelectedBookKeeper() {
        try {
            return ((TWComboItem) bookKeepersCombo.getSelectedItem()).getId();
        } catch (Exception e) {
            return null;
        }
    }

    private boolean isRuleBasedValidationSucess(boolean conditionalMode) {

        long disclosed;
        long minfill;
        long quantity;
        double price;
        try {
            disclosed = Long.parseLong(txtDisclosed.getText());
        } catch (NumberFormatException e) {
            disclosed = 0;
        }
        try {
//            minfill = Long.parseLong(txtMinFill.getText());
            minfill = txtMinFill.getNumber().longValue();
        } catch (NumberFormatException e) {
            minfill = 0;
        }
        try {
            quantity = txtQty.getNumber().longValue();
        } catch (NumberFormatException e) {
            quantity = 0;
        }
        try {
            price = Double.parseDouble(txtTPrice.getText());
        } catch (NumberFormatException e) {
            price = 0;
        }
        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
        if (TradeMethods.ruleBasedValidationSucess(conditionalMode, exchange, type, price, quantity, disclosed, minfill, minPrice, maxPrice, tiff, side, symbol, 0, stock, true)) {
            return isRuleBaseWarningSuccess();
        }
        return false;
    }

    private boolean validateInputs() {
        try {
            long minfill;
            try {
//                minfill = Long.parseLong(txtMinFill.getText().trim());
                minfill = txtMinFill.getNumber().longValue();
            } catch (NumberFormatException e) {
                minfill = 0;
            }
            if ((txtQty.getText().trim().equals("")) || ((txtQty.getNumber().longValue() <= 0))) {
                new ShowMessage(Language.getString("INVALID_QUANTITY"), "E");
                txtQty.requestFocus();
                return false;
            }
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
            /*long ordQty = Long.parseLong(txtTQty.getText());
            if(stock.getLotSize() >0){
                ordQty = ordQty * stock.getLotSize();
            }*/
            if (((type == TradeMeta.ORDER_TYPE_LIMIT) || (type == TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT) || (type == TradeMeta.ORDER_TYPE_DAY_LIMIT)
                    || (type == TradeMeta.ORDER_TYPE_LIMIT_ON_CLOSE)) &&
                    ((txtTPrice.getText().trim().equals("")) || (Double.parseDouble(txtTPrice.getText()) <= 0))) {
                new ShowMessage(Language.getString("INVALID_PRICE"), "E");
                txtTPrice.requestFocus();
                return false;
            }
            if (getQuantity() < minfill) {
                new ShowMessage(Language.getString("MSG_INVALID_MIN_FILL"), "E");
                txtQty.requestFocus();
                return false;
            }
            if (tiff == 6) { // only for GTD bug id <#0027>
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                long selectedDate = Integer.parseInt(dateFormat.format(goodTillLong));
                long today = Integer.parseInt(dateFormat.format(ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDate()));
                if (selectedDate < today) {
                    new ShowMessage(Language.getString("MSG_INVALID_GOOD_TILL_DATE"), "E");
                    return false;
                }
            }

            if (((type == TradeMeta.ORDER_TYPE_MARKET) || (type == TradeMeta.ORDER_TYPE_LIMIT))
                    && (bracketCheck.isSelected())) {

                if (bracketStopLoss.getText().equals("") && bracketTakeProf.getText().equals("")) {
                    new ShowMessage(Language.getString("INVALID_STOP_LOSS_N_TAKE_PROFIT_VALUE_EMPTY"), "E");
                    bracketStopLoss.requestFocus();
                    return false;
                } else if (bracketStopLoss.getText().equals("")) {
                    if (Double.parseDouble(bracketTakeProf.getText()) <= 0) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_VALUE"), "E");
                        bracketTakeProf.requestFocus();
                        return false;
                    }
                } else if (bracketTakeProf.getText().equals("")) {
                    if (Double.parseDouble(bracketStopLoss.getText()) <= 0) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_VALUE"), "E");
                        bracketStopLoss.requestFocus();
                        return false;
                    }
                } else {
                    if (Double.parseDouble(bracketTakeProf.getText()) <= 0) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_VALUE"), "E");
                        bracketTakeProf.requestFocus();
                        return false;
                    }
                    if (Double.parseDouble(bracketStopLoss.getText()) <= 0) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_VALUE"), "E");
                        bracketStopLoss.requestFocus();
                        return false;
                    }
                }
            } else if (((type == TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT) || (type == TradeMeta.ORDER_TYPE_STOPLOSS_MARKET))) {

                if (txtStopPrice.getText().equals("") && stradleTakeProf.getText().equals("")) {
                    new ShowMessage(Language.getString("INVALID_STOP_LOSS_N_TAKE_PROFIT_VALUE_EMPTY"), "E");
                    txtStopPrice.requestFocus();
                    return false;
                } else if (txtStopPrice.getText().equals("")) {
                    if (Double.parseDouble(stradleTakeProf.getText()) <= 0) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_VALUE"), "E");
                        stradleTakeProf.requestFocus();
                        return false;
                    }
                } else if (stradleTakeProf.getText().equals("")) {
                    if (Double.parseDouble(txtStopPrice.getText()) <= 0) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_VALUE"), "E");
                        txtStopPrice.requestFocus();
                        return false;
                    }
                } else {
                    if (Double.parseDouble(stradleTakeProf.getText()) <= 0) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_VALUE"), "E");
                        stradleTakeProf.requestFocus();
                        return false;
                    }
                    if (Double.parseDouble(txtStopPrice.getText()) <= 0) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_VALUE"), "E");
                        txtStopPrice.requestFocus();
                        return false;
                    }
                }
            }

            if (((type == TradeMeta.ORDER_TYPE_LIMIT)) && (bracketCheck.isSelected())) {
                double price = Double.parseDouble(txtTPrice.getText());
                int stopType = Integer.parseInt(((TWComboItem) bracketcmbSL.getSelectedItem()).getId());
                if (side == TradeMeta.BUY) {
                    if (!bracketTakeProf.getText().equals("") && (Double.parseDouble(bracketTakeProf.getText()) <= price)) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_LESSER_LIMITPRICE"), "E");
                        bracketTakeProf.requestFocus();
                        return false;
                    } else if (!bracketStopLoss.getText().equals("") && (stopType == TradingShared.STOP_PRICE_TYPE_LIMIT) && (Double.parseDouble(bracketStopLoss.getText()) >= price)) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_GREATER_LIMITPRICE"), "E");
                        bracketStopLoss.requestFocus();
                        return false;
                    }
                } else if (side == TradeMeta.SELL) {
                    if (!bracketTakeProf.getText().equals("") && (Double.parseDouble(bracketTakeProf.getText()) >= price)) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_GREATER_LIMITPRICE"), "E");
                        bracketTakeProf.requestFocus();
                        return false;
                    } else if (!bracketStopLoss.getText().equals("") && (stopType == TradingShared.STOP_PRICE_TYPE_LIMIT) && (Double.parseDouble(bracketStopLoss.getText()) <= price)) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_LESSER_LIMITPRICE"), "E");
                        bracketStopLoss.requestFocus();
                        return false;
                    }
                }
            } else if (((type == TradeMeta.ORDER_TYPE_MARKET)) && (bracketCheck.isSelected())) {
//                Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                int stopType = Integer.parseInt(((TWComboItem) bracketcmbSL.getSelectedItem()).getId());
                double price = stock.getLastTradeValue();
                if (price <= 0) {
                    price = stock.getPreviousClosed();
                }
                if (side == TradeMeta.BUY) {
                    if (!bracketTakeProf.getText().equals("") && (Double.parseDouble(bracketTakeProf.getText()) <= price)) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_LESSER_PRICE"), "E");
                        bracketTakeProf.requestFocus();
                        return false;
                    } else if (!bracketStopLoss.getText().equals("") && (stopType == TradingShared.STOP_PRICE_TYPE_LIMIT) && (Double.parseDouble(bracketStopLoss.getText()) >= price)) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_GREATER_PRICE"), "E");
                        bracketStopLoss.requestFocus();
                        return false;
                    }
                } else if (side == TradeMeta.SELL) {
                    if (!bracketTakeProf.getText().equals("") && (Double.parseDouble(bracketTakeProf.getText()) >= price)) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_GREATER_PRICE"), "E");
                        bracketTakeProf.requestFocus();
                        return false;
                    } else if (!bracketStopLoss.getText().equals("") && (stopType == TradingShared.STOP_PRICE_TYPE_LIMIT) && (Double.parseDouble(bracketStopLoss.getText()) <= price)) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_LESSER_PRICE"), "E");
                        bracketStopLoss.requestFocus();
                        return false;
                    }
                }
            } else if (((type == TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT))) {
                double price = Double.parseDouble(txtTPrice.getText());
                int stopType = Integer.parseInt(((TWComboItem) cmbStopPrice.getSelectedItem()).getId());
                if (side == TradeMeta.BUY) {
                    if (!stradleTakeProf.getText().equals("") && (Double.parseDouble(stradleTakeProf.getText()) >= price)) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_GREATER_LIMITPRICE"), "E");
                        stradleTakeProf.requestFocus();
                        return false;
                    } else if (!txtStopPrice.getText().equals("") && (stopType == TradingShared.STOP_PRICE_TYPE_LIMIT) && (Double.parseDouble(txtStopPrice.getText()) <= price)) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_LESSER_LIMITPRICE"), "E");
                        txtStopPrice.requestFocus();
                        return false;
                    }
                } else if (side == TradeMeta.SELL) {
                    if (!stradleTakeProf.getText().equals("") && (Double.parseDouble(stradleTakeProf.getText()) <= price)) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_LESSER_LIMITPRICE"), "E");
                        stradleTakeProf.requestFocus();
                        return false;
                    } else if (!txtStopPrice.getText().equals("") && (stopType == TradingShared.STOP_PRICE_TYPE_LIMIT) && (Double.parseDouble(txtStopPrice.getText()) >= price)) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_GREATER_LIMITPRICE"), "E");
                        txtStopPrice.requestFocus();
                        return false;
                    }
                }
            } else if (((type == TradeMeta.ORDER_TYPE_STOPLOSS_MARKET))) {
//                Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                int stopType = Integer.parseInt(((TWComboItem) cmbStopPrice.getSelectedItem()).getId());
                double price = stock.getLastTradeValue();
                if (price <= 0) {
                    price = stock.getPreviousClosed();
                }
                if (side == TradeMeta.BUY) {
                    if (!stradleTakeProf.getText().equals("") && (Double.parseDouble(stradleTakeProf.getText()) >= price)) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_GREATER_PRICE"), "E");
                        stradleTakeProf.requestFocus();
                        return false;
                    } else if (!txtStopPrice.getText().equals("") && (stopType == TradingShared.STOP_PRICE_TYPE_LIMIT) && (Double.parseDouble(txtStopPrice.getText()) <= price)) {
                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_LESSER_PRICE"), "E");
                        txtStopPrice.requestFocus();
                        return false;
                    }
                } else if (side == TradeMeta.SELL) {
                    if (!stradleTakeProf.getText().equals("") && (Double.parseDouble(stradleTakeProf.getText()) <= price)) {
                        new ShowMessage(Language.getString("INVALID_TAKE_PROFIT_LESSER_PRICE"), "E");

                        stradleTakeProf.requestFocus();
                        return false;
                    } else if (!txtStopPrice.getText().equals("") && (stopType == TradingShared.STOP_PRICE_TYPE_LIMIT) && (Double.parseDouble(txtStopPrice.getText()) >= price)) {

                        new ShowMessage(Language.getString("INVALID_STOP_LOSS_GREATER_PRICE"), "E");
                        txtStopPrice.requestFocus();
                        return false;
                    }
                }
            }

            if ((executionMode == MODE_PROGRAMMED) && (txtConditionValue.getText().trim().equals(""))) {
                SharedMethods.showMessage(Language.getString("INVALID_CONDITION_VALUE"), JOptionPane.ERROR_MESSAGE);
                txtConditionValue.requestFocus();
                return false;
            }

            if (!txtDisclosed.getText().trim().equals("")) {
                try {
                    Interpreter interpreter = loadInterpriter(-1);
                    Rule rule = RuleManager.getSharedInstance().getRule("DISCLOSED_VALIDATION", exchange, "NEW_ORDER");
                    int value = (Integer) interpreter.eval(rule.getRule());
                    if (value <= 0) {
                        SharedMethods.showMessage(Language.getString("INVALID_DISCLOSED_QUANTITY"), JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                    interpreter = null;
                } catch (Exception e) { // could not eveluate the rule or rule not found
                    e.printStackTrace();
                }
            }

            try {
                Interpreter interpreter = loadInterpriter(-1);
                Rule rule = RuleManager.getSharedInstance().getRule("MINFILL_VALIDATION", exchange, "NEW_ORDER");
                int value = (Integer) interpreter.eval(rule.getRule());
                if (value <= 0) {
                    SharedMethods.showMessage(Language.getString("INVALID_MINFILL_QUANTITY"), JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                interpreter = null;
            } catch (Exception e) { // could not eveluate the rule or rule not found
                e.printStackTrace();
            }
            return true;
        } catch (NumberFormatException e) {
            new ShowMessage(Language.getString("MSG_INVALID_TRADE_INPUTS"), "E");
            return false;
        }
    }

    private boolean validateMargins(double neworderMargin) {
        //  return MarginCalculator.getSharedInstance().getCoverageWarning(selectedPortfolio, netValue-getCommisionVal(), selectedStock.getKey(), neworderMargin);
        double[] values = MarginCalculator.getSharedInstance().getCoveragevaluesForNewOrder(selectedPortfolio, netValue - getCommisionVal(), neworderMargin, selectedStock.getKey());
        if (values != null && values.length > 0) {
            try {
                BigDecimal benchMark = new BigDecimal(values[0]);
                double BM = (benchMark.setScale(coverageDecimals, coverageDecimalsRoundMtd)).doubleValue();
                BigDecimal coverage = new BigDecimal(values[1]);
                double CAL = (coverage.setScale(coverageDecimals, coverageDecimalsRoundMtd)).doubleValue();
                if (CAL < BM) {
//                if (values[1] < values[0]) {
                    return true;
                } else {
                    return false;
                }
            } catch (NumberFormatException e) {
                return false;
            }

        } else {
            return false;
        }
        //return false;
    }

    private boolean validateCurrentMargin() {
        double[] values = MarginCalculator.getSharedInstance().getCoveragevaluesForNewOrder(selectedPortfolio, 0, 0, selectedStock.getKey());
        if (values != null && values.length > 0) {
            try {
                BigDecimal benchMark = new BigDecimal(values[0]);
                double BM = (benchMark.setScale(coverageDecimals, coverageDecimalsRoundMtd)).doubleValue();
                BigDecimal coverage = new BigDecimal(values[1]);
                double CAL = (coverage.setScale(coverageDecimals, coverageDecimalsRoundMtd)).doubleValue();
                if (CAL < BM) {
//                if (values[1] <= values[0]) {
                    return true;
                } else {
                    return false;
                }
            } catch (NumberFormatException e) {
                return false;
            }

        } else {
            return false;
        }
    }

    private double getAdjustedQuantity(double neworderMargin, Account cashAcc) {

        double cash = Math.max(0, (cashAcc.getBalance() + cashAcc.getBlockedAmount()));
        double loan = cashAcc.getTotMarginDue() + Math.abs(cashAcc.getTotMarginBlock());
//        double coverage =MarginCalculator.getSharedInstance().getCoverageWithNewOrder(selectedPortfolio,0,selectedStock.getKey(), 0);
        double price = getPrice();
        if (price <= 0d || (!txtTPrice.isEnabled())) {
            if (selectedStock != null && selectedStock.getBestAskPrice() > 0) {
                price = selectedStock.getBestAskPrice();
            } else {
                price = selectedStock.getLastTradeValue();
            }
        }
        double coverage = MarginCalculator.getSharedInstance().getCoveragevaluesForNewOrder(selectedPortfolio, netValue - getCommisionVal(), neworderMargin, selectedStock.getKey())[0];
        double maxbp = MarginCalculator.getSharedInstance().getMaxBuyingpower(coverage, cash, loan, getCommisionVal(), price, selectedStock.getKey(), selectedPortfolio);
        maxbp = convertToSelectedCurrency(selectedStock.getCurrencyCode(), cashAcc.getCurrency(), maxbp);
        int maxqty = (int) (maxbp / price);
        double maxCommision = SharedMethods.getCommission(exchange, price, maxqty, marketCode, selectedPortfolio, SharedMethods.getKey(exchange, symbol, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(selectedStock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(selectedStock), PFStore.getBaseCurrency()));
        double adjustedqty = MarginCalculator.getSharedInstance().getAdjustedQty(coverage, cash, loan, maxCommision, price, selectedStock.getKey(), selectedPortfolio);
        return adjustedqty;
    }

    private double getAdjustedCoverage(double neworderMargin) {
        // return MarginCalculator.getSharedInstance().getCoverageWithNewOrder(selectedPortfolio, netValue, selectedStock.getKey(), neworderMargin);
        return MarginCalculator.getSharedInstance().getCoveragePctForNewOrder(selectedPortfolio, netValue - getCommisionVal(), neworderMargin, selectedStock.getKey(), true);
    }

    private Object[] validateTiffForMargin(short tiff, boolean isnormalmargin, boolean isdaymargin) {
        return MarginCalculator.getSharedInstance().validateTIF(tiff, selectedPortfolio, isnormalmargin, isdaymargin);
    }

    private double getCommisionVal() {
        return commission;
//        double com = 0.00;
//        String comstring = txtCommission.getText();
//        if (comstring != null && (!comstring.trim().equals("")) && (!comstring.trim().equalsIgnoreCase(Language.getString("NA")))) {
//            try {
//                com = Double.parseDouble(comstring);
//            } catch (NumberFormatException e) {
//                com = 0.00;
//            }
//            return com;
//        } else {
//            return 0;
//        }

    }

    private boolean isRuleBaseWarningSuccess() {
        try {
            Interpreter interpreter = loadInterpriter(0);
            long value = (Long) interpreter.eval(ruleTable.get("QUANTITY_VALIDATION"));
            //                long value = (Long)interpreter.source("rules/" + exchange + "/qv.java");
            if (value < 0) {
                int result = SharedMethods.showConfirmMessage(Language.getString("MSG_POSSIBLE_INVALID_QUANTITY"),
                        JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                if (result != JOptionPane.OK_OPTION) {
                    return false;
                }
            }
            interpreter = null;
        } catch (Exception e) { // could not eveluate the rule or rule not found
            //e.printStackTrace();
        }
        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
        /*long ordQty = Long.parseLong(txtTQty.getText());
        if(stock.getLotSize() >0){
            ordQty = ordQty * stock.getLotSize();
        }*/
        try {
            if (instrument == Meta.INSTRUMENT_FUTURE) {
//                Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                if (stock != null) {
                    FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol(), instrument));
                    int maxContractSize = futureBaseAttributes.getMaxContracts();
//                    long quantity = ordQty;
                    if (getQuantity() > maxContractSize) {
                        String msg = Language.getString("MSG_MAX_CONTRACT_SIZE_EXCEEDED");
                        msg = msg.replace("[SIZE]", SharedMethods.toIntegerFoamat(maxContractSize));
                        int result = SharedMethods.showConfirmMessage(msg, JOptionPane.WARNING_MESSAGE);
                        if (result != JOptionPane.OK_OPTION) {
                            return false;
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        try {
            if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                if (!((currentTransaction != null) &&
                        (currentTransaction.getRuleType() == TradeMeta.CONDITION_TYPE_STOP_LOSS) &&
                        (currentTransaction.getSide() == TradeMeta.SELL)) ||
                        !((windowMode == TradeMeta.AMEND) &&
                                ((currentTransaction.getRuleType() == TradeMeta.CONDITION_TYPE_BRACKET) || (currentTransaction.getRuleType() == TradeMeta.CONDITION_TYPE_STRADEL)))) {
                    Interpreter interpreter = loadInterpriter(0);
                    double value = (Double) interpreter.eval(ruleTable.get("PRICE_VALIDATION"));
                    //                     double value = (Double)interpreter.source("rules/" + exchange + "/pv.java");
                    if (value < 0) {
                        int result = SharedMethods.showConfirmMessage(Language.getString("MSG_POSSIBLE_INVALID_PRICE"),
                                JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                        if (result != JOptionPane.OK_OPTION) {
                            return false;
                        }
                    }
                    interpreter = null;
                }
            }
        } catch (Exception e) { // could not eveluate the rule or rule not found
            //e.printStackTrace();
        }
        try {
            Interpreter interpreter = loadInterpriter(0);
            Rule rule = RuleManager.getSharedInstance().getRule("MIN_MAX", exchange, "NEW_ORDER");
            boolean value = (Boolean) interpreter.eval(rule.getRule());
            if (value) {
                //   int result = SharedMethods.showConfirmMessage(Language.getString("MSG_POSSIBLE_INVALID_PRICE"),
                int result = SharedMethods.showConfirmMessage(Language.getLanguageSpecificString(rule.getMessage()),
                        JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                if (result != JOptionPane.OK_OPTION) {
                    return false;
                }
            }
            interpreter = null;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Interpreter interpreter = loadInterpriter(0);
                Rule rule = RuleManager.getSharedInstance().getRule("MIN_MAX", exchange, "NEW_ORDER");
                int value = (Integer) interpreter.eval(rule.getRule());
                if (value < 0) {
                    int result = SharedMethods.showConfirmMessage(Language.getString("MSG_POSSIBLE_INVALID_PRICE"),
                            JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                    if (result != JOptionPane.OK_OPTION) {
                        return false;
                    }
                }
                interpreter = null;
            } catch (Exception e1) {

            }

//            e.printStackTrace();
        }
        return true;
    }

    private void createMarginViewsPannel() {
        pnlMarginViews = new JPanel();
        coverage = new VariationImage();
        coverage.setType(VariationImage.TYPE_COVERAGE_MAP);

//        marginUtilized = new VariationImage();
//         marginUtilized.setType(VariationImage.TYPE_CASH_MAP);
//        JPanel utilized = new JPanel();
//        utilized.setLayout(new BorderLayout(1, 0));
//        JLabel marginUtilisedlbl = new JLabel("  " + Language.getString("ACCOUNT_SUMMARY_MARGIN_UTILIZED"));
//        utilized.add(marginUtilisedlbl, BorderLayout.WEST);
//        imglbl1.setVerticalAlignment(JLabel.CENTER);
//        imglbl1.setIcon(marginUtilized);
//        imglbl1.setHorizontalTextPosition(JLabel.CENTER);
//        imglbl1.setPreferredSize(new Dimension(85,20));
//        imglbl1.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
//        marginUtilized.setHeight(20);
//        marginUtilized.setWidth(85);
//        utilized.add(imglbl1, BorderLayout.EAST);

        JPanel coveragepan = new JPanel();
        coveragepan.setLayout(new BorderLayout(1, 0));
        coveragelbl = new JLabel(Language.getString("ACCOUNT_SUMMARY_COVERAGE"));
        coveragepan.add(coveragelbl, BorderLayout.WEST);
        imglbl2.initIcon();
        coverage.setHeight(18);
        coverage.setWidth(85);
        imglbl2.setVerticalAlignment(JLabel.CENTER);
        imglbl2.setHorizontalTextPosition(JLabel.CENTER);
//        imglbl2.setPreferredSize(new Dimension(85, 18));
        imglbl2.setPreferredSize(new Dimension(100, 18));
//        imglbl2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        coveragepan.add(imglbl2, BorderLayout.EAST);
        //  coveragepan.setBorder(BorderFactory.createLineBorder(Color.BLUE));
        pnlMarginViews.setLayout(new BorderLayout());
        pnlMarginViews.add(coveragepan, BorderLayout.NORTH);
        pnlMarginViews.setOpaque(false);
        coveragepan.setOpaque(false);
        //pnlMarginViews.add(coveragelbl, BorderLayout.WEST);


        GUISettings.applyOrientation(pnlMarginViews);

    }

    public void updateMarginPanel() {

        if (side == BUY && selectedStock != null && isMarginApplicableForPortfolio(selectedPortfolio, selectedStock.getKey()) && isMarginMapsValidForSymbol(new TradeKey(SharedMethods.getExchangeFromKey(selectedStock.getKey()), SharedMethods.getSymbolFromKey(selectedStock.getKey()), SharedMethods.getInstrumentTypeFromKey(selectedStock.getKey())))
                && (!(windowMode == AMEND || windowMode == CANCEL))) {
            pnlMarginViews.setVisible(true);
            //  double coverageval = MarginCalculator.getSharedInstance().getCoveragePctForAccSummary(selectedPortfolio);// ((DoubleTransferObject) (accountModel.getValueAt(-6, 0))).getValue();
            String sKey;
            if (selectedStock != null) {
                sKey = selectedStock.getKey();
            } else {
                sKey = "";
            }
//            double coverageval = MarginCalculator.getSharedInstance().getCoveragePctForNewOrder(selectedPortfolio, netValue, calculateNewOrderMargin(), sKey, false);// ((DoubleTransferObject) (accountModel.getValueAt(-6, 0))).getValue();
            double[] coverageval = MarginCalculator.getSharedInstance().getCoveragevaluesForNewOrder(selectedPortfolio, netValue - getCommisionVal(), calculateNewOrderMargin(), sKey);// ((DoubleTransferObject) (accountModel.getValueAt(-6, 0))).getValue();
            //  if (!Double.isInfinite(coverageval) && (!Double.isNaN(coverageval))) {
            try {
                if (coverageval != null && (!Double.isInfinite(coverageval[1]))) {
                    imglbl2.setCoverageValues(coverageval[0], coverageval[1]);
                    //                coverage.setValue(1 - coverageval);
                    //                imglbl2.setText(formatter.format((1 - coverageval) * 100) + "%");
                } else {
                    imglbl2.setEmptyCoverageValues();
                }
            } catch (Exception e) {
                imglbl2.setEmptyCoverageValues();
            }
        } else {
            pnlMarginViews.setVisible(false);
            // coverage.setValue(Double.NaN);
            // coverage.setValue(0.45);
            imglbl2.setEmptyCoverageValues();

        }
        imglbl2.validate();
        imglbl2.repaint();
        powerPanel.doLayout();

    }

    private double calculateNewOrderMargin() {
        if (windowMode == AMEND) {
            long origQiy = currentTransaction.getOrderQuantity();
            long tradeQty = txtQty.getNumber().longValue();

        }
        Account account = null;
        double buyingPowerWithoutMargins = 0;
        double margin = 0;
        try {
            account = TradingShared.getTrader().findAccountByPortfolio(selectedPortfolio);
            buyingPowerWithoutMargins = account.getBuyingPower();
        } catch (Exception e) {
            buyingPowerWithoutMargins = 0;
        }
        buyingPowerWithoutMargins = convertToSelectedCurrency(selectedStock.getCurrencyCode(), account.getCurrency(), buyingPowerWithoutMargins);
        if (buyingPowerWithoutMargins <= 0) {
            margin = netValue;
        } else {
            margin = netValue - buyingPowerWithoutMargins;
        }
        if (margin < 0) {
            margin = 0;
        }
        return margin;
    }

    private boolean isMarginApplicableForPortfolio(String pfid, String sKey) {
        TradingPortfolioRecord record = TradingShared.getTrader().getPortfolio(pfid);
        if (record != null) {
            if ((record.isMarginEnabled() && record.isDayMarginEnabled()) || record.isMarginEnabled()) {
                return true;
            } else if (record.isDayMarginEnabled()) {
                return record.isDayMarginAvailable(SharedMethods.getExchangeFromKey(sKey));
//                if (isMarketClose(sKey)) {
//                    return true;
//                } else {
//                    return !record.isMarginTransisitionPassed();
////                    return true;
//                }
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    private boolean isMarginMapsValidForSymbol(TradeKey sKey) {
        if (sKey != null || sKey.getTradeKey().trim().isEmpty()) {
            return MarginSymbolStore.getSharedInstance().isMarginSymbolAvailable(sKey);
        } else {
            return false;
        }
    }

    private boolean isMarketClose(String Skey) {
        try {
            return ((ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(Skey)).getMarketStatus() == Meta.MARKET_CLOSE));
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isMargisApplied(String pfId, String sKey) {

        return (isMarginApplicableForPortfolio(pfId, sKey) && isMarginMapsValidForSymbol(new TradeKey(SharedMethods.getExchangeFromKey(sKey), SharedMethods.getSymbolFromKey(sKey), SharedMethods.getInstrumentTypeFromKey(sKey))));
    }

    public void tradeServerConnected() {
        lblLogingDetail.setText(TradingShared.TRADE_USER_STATUS);
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    class ConfirmationFrame extends InternalFrame implements ActionListener, InternalFrameListener {

        private JPanel topPanel;
        private JPanel notePanel;
        private JPanel infoPanel;
        private JPanel btnPanel;
        private JPanel tickPanel;
        private JLabel configLbl;
        private JLabel noteLbl;
        private TWButton btnYes;
        private TWButton btnNo;
        private JCheckBox checkRadio;
        private ImageIcon infoIcon;
        private TransactionDialog parent;


        ConfirmationFrame(TransactionDialog frame) {
            parent = frame;
            initUI();
        }

        public void setParent(TransactionDialog parent) {
            this.parent = parent;
        }

        private void initUI() {
            setTitle(Language.getString("INFORMATION"));
            setLayout(new BorderLayout(2, 2));
            setSize(440, 155);
            setClosable(true);
//             setLocationRelativeTo(Client.getInstance().getFrame());
            setLocation(410, 200);
            setLayer(GUISettings.TOP_LAYER);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            topPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"35", "30", "20", "2"}, 2, 5));
            notePanel = new JPanel(new FlexGridLayout(new String[]{"60", "100%", "10"}, new String[]{"20"}));
            infoPanel = new JPanel(new FlexGridLayout(new String[]{"40", "100%", "20"}, new String[]{"40"}, 5, 0));
            btnPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "80", "80", "50%"}, new String[]{"22"}, 10, 2));
            tickPanel = new JPanel(new FlexGridLayout(new String[]{"120", "100%", "10%"}, new String[]{"20"}));
            configLbl = new JLabel(Language.getString("ORDER_WINDOW_CONFIG_MSG"));
            configLbl.setFont(new TWFont("Arial", 1, 12));
            btnYes = new TWButton(Language.getString("YES"));
            btnYes.addActionListener(this);
            btnNo = new TWButton(Language.getString("NO"));
            btnNo.addActionListener(this);
            checkRadio = new JCheckBox(Language.getString("ORDER_WINDOW_POPUP_MSG"));
            checkRadio.setFont(new TWFont("Arial", 0, 11));
            checkRadio.addActionListener(this);
            infoIcon = new ImageIcon(Theme.getTheamedImagePath("information"));
            JLabel bulkLbl = new JLabel();
            bulkLbl.setIcon(infoIcon);
            infoPanel.add(bulkLbl);
            infoPanel.add(configLbl);
            infoPanel.add(new JLabel());
            btnPanel.add(new JLabel());
            btnPanel.add(btnYes);
            btnPanel.add(btnNo);
            btnPanel.add(new JLabel());
            tickPanel.add(new JLabel());
            tickPanel.add(checkRadio);
            tickPanel.add(new JLabel());
            topPanel.add(infoPanel);
            topPanel.add(btnPanel);
            topPanel.add(tickPanel);
            topPanel.add(new JSeparator());

            noteLbl = new JLabel(Language.getString("ORDER_WINDOW_NOTE"));
            noteLbl.setFont(new TWFont("Arial", 0, 10));
            notePanel.add(new JLabel());
            notePanel.add(noteLbl);
            notePanel.add(new JLabel());

            getContentPane().add(topPanel, BorderLayout.NORTH);
            getContentPane().add(notePanel, BorderLayout.SOUTH);

            Client.getInstance().getDesktop().add(this);
            setLayer(GUISettings.TOP_LAYER);

        }

        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(btnYes)) {
//                TradingShared.setKeepOrderWindowOpen(true);
                Settings.setKeepOrderWindowOpen(true);
                dispose();
            } else if (e.getSource().equals(btnNo)) {
                Settings.setKeepOrderWindowOpen(false);
                dispose();
            } else if (e.getSource().equals(checkRadio)) {
                if (checkRadio.isSelected()) {
                    TradingShared.setShowConfigPopup(false);
                } else if (!checkRadio.isSelected()) {
                    TradingShared.setShowConfigPopup(true);
                }
            }
        }

        public void internalFrameClosed(InternalFrameEvent e) {
            if (!confirmation.isVisible()) {
                if (Settings.isKeepOrderWindowOpen()) {
                    clearOrderData();
                } else {

                    if (parent != null) {
                        parent.dispose();
                    }
                }
            }

        }

    }

    public void setLocationRelativeTo(Component c) {
        Dimension parentSize = c.getSize();

        if (Settings.isDualScreenMode()) {
            if (Settings.isLeftAlignPopups()) {
                this.setBounds((int) ((parentSize.getWidth() / 2 - this.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                        this.getWidth(), this.getHeight());
            } else {
                this.setBounds((int) ((parentSize.getWidth() / 2) + (parentSize.getWidth() / 2 - this.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                        this.getWidth(), this.getHeight());
            }
        } else {


            this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
                    (int) (20),
                    this.getWidth(), this.getHeight());
        }
    }


    private class MarginMessage extends JOptionPane {
        private long qty = 0;
        private TWButton send;
        private TWButton ammend;
        private TWButton cancel;
        private String message;
        private int returnVal = 0;

        private MarginMessage(long readjustedQty, String message) {
//            super(Client.getInstance().getFrame());
            qty = readjustedQty;
            if (qty <= 0) {
                qty = 0;
            }
            this.message = message;
            initUI();

        }

        private void initUI() {

            send = getsendButton(this);
            ammend = getAmmendButton(this);
            cancel = getCancelButton(this);
            if (qty <= 0) {
                send.setEnabled(false);
                ammend.setEnabled(false);

            }
            Object[] options = new Object[]{send, ammend, cancel};
            String g_sHeading = Language.getString("WARNING");
            setMessage(message);
            setMessageType(JOptionPane.WARNING_MESSAGE);
            setOptions(options);
            setInitialValue(options[0]);
            setInitialSelectionValue(options[0]);
            selectInitialValue();
            JDialog oDialog = createDialog(Client.getInstance().getFrame(), g_sHeading);
//        oDialog.addFocusListener(this); // see comment in "focusGained()"

            GUISettings.setLocationRelativeTo(oDialog, Client.getInstance().getFrame());
            GUISettings.applyOrientation(oDialog);
            oDialog.show();

        }

        public int getReturnVal() {
            return returnVal;
        }

        private TWButton getsendButton(final JOptionPane oPane) {
//            final TWButton oButton = new TWButton(Language.getString("MARGIN_READJUST_SEND") + " " + qty);
            final TWButton oButton = new TWButton(Language.getString("MARGIN_READJUST_SEND"));

            ActionListener oListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    oPane.setValue(oButton.getText());
                    returnVal = 2;
                }
            };

            KeyAdapter oKeyAdapter = new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        oPane.setValue(oButton.getText());
                        returnVal = 2;
                    }
                }
            };
            oButton.addActionListener(oListener);
            oButton.addKeyListener(oKeyAdapter);
            return oButton;
        }

        private TWButton getAmmendButton(final JOptionPane oPane) {
            final TWButton oButton = new TWButton(Language.getString("MARGIN_READJUST_AMMEND"));

            ActionListener oListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    oPane.setValue(oButton.getText());
                    returnVal = 1;
                }
            };

            KeyAdapter oKeyAdapter = new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        oPane.setValue(oButton.getText());
                        returnVal = 1;
                    }
                }
            };
            oButton.addActionListener(oListener);
            oButton.addKeyListener(oKeyAdapter);
            return oButton;
        }

        private TWButton getCancelButton(final JOptionPane oPane) {
            final TWButton oButton = new TWButton(Language.getString("MARGIN_READJUST_CANCEL"));

            ActionListener oListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    oPane.setValue(oButton.getText());
                    returnVal = 0;
                }
            };

            KeyAdapter oKeyAdapter = new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        oPane.setValue(oButton.getText());
                        returnVal = 0;
                    }
                }
            };
            oButton.addActionListener(oListener);
            oButton.addKeyListener(oKeyAdapter);
            return oButton;
        }
    }

}


