package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.datastore.StockTransferObject;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 17, 2006
 * Time: 5:52:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockTransferWindow extends InternalFrame implements InternalFrameListener, Themeable, ItemListener {

    private JLabel lblCurrency;
    private JLabel valCurrency;
    private JLabel lblsymbol;
    private TWTextField valsymbol;
    private JLabel lblqty;
    private TWTextField valqty;
    private JLabel lblBroker;
    private TWTextField valBroker;
    private JLabel lblTransferDate;
    private JLabel valTransferDate;
    private JLabel lblNewCost;
    private TWTextField valNewCost;
    private TWButton okBtn;
    private TWButton cancelBtn;
    private StockTransferObject object;
    private TWComboBox cmbPortfolio;
    private ArrayList<TWComboItem> portfolioList;
    private TWTextField txtSymbol;
    private TWButton btnSelectSymbol;
    private String symbol = null;
    private String exchange = null;
    private String selPortfolio = null;
    private String broker = null;
    private String tQty = null;
    private String tAvgCost = null;

    public StockTransferWindow(StockTransferObject object) {
        super(false);
        this.object = object;
        try {
            createUI();
        } catch (Exception e) {
        }
        setData();
    }

    public StockTransferWindow() {
        super(false);
        try {
            createUI();
        } catch (Exception e) {
        }
    }

    private void createUI() {
        this.setSize(300, 280);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setTitle(Language.getString("STOCK_TRANSFER_NOTIFICATION"));
        this.setClosable(true);
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        Container container = this.getContentPane();
        container.setLayout(new BorderLayout());
        JPanel dataPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"25", "25", "25", "25", "25", "25"}, 5, 5));

//        container.setLayout(new FlexGridLayout(new String[]{"50%","50%"}, new String[]{"25","25","25","25","25","25","35"}, 5,5));

        JLabel lblPortfolio = new JLabel(Language.getString("PORTFOLIO"));
        portfolioList = new ArrayList<TWComboItem>();
        cmbPortfolio = new TWComboBox(new TWComboModel(portfolioList));
        cmbPortfolio.addItemListener(this);

        lblCurrency = new JLabel(Language.getString("CURRENCY"));
//        lblCurrency.setBorder(BorderFactory.createEtchedBorder());
        lblCurrency.setHorizontalAlignment(JLabel.LEADING);
        valCurrency = new JLabel("");
        valCurrency.setBorder(BorderFactory.createEtchedBorder());
        valCurrency.setHorizontalAlignment(JLabel.TRAILING);
        populatePortfolios();
        lblsymbol = new JLabel(Language.getString("SYMBOL"));
//        lblsymbol.setBorder(BorderFactory.createEtchedBorder());
        lblsymbol.setHorizontalAlignment(JLabel.LEADING);
        lblqty = new JLabel(Language.getString("QUANTITY"));
//        lblqty.setBorder(BorderFactory.createEtchedBorder());
        lblqty.setHorizontalAlignment(JLabel.LEADING);
        lblBroker = new JLabel(Language.getString("BROKER"));
//        lblBroker.setBorder(BorderFactory.createEtchedBorder());
        lblBroker.setHorizontalAlignment(JLabel.LEADING);
        lblTransferDate = new JLabel(Language.getString("TRANSFER_DATE"));
//        lblTransferDate.setBorder(BorderFactory.createEtchedBorder());
        lblTransferDate.setHorizontalAlignment(JLabel.LEADING);
//        lblCurrentCost = new JLabel(Language.getString("CURRENT_AVG_COST"));
        lblNewCost = new JLabel(Language.getString("NEW_AVG_COST"));
//        lblNewCost.setBorder(BorderFactory.createEtchedBorder());
        lblNewCost.setHorizontalAlignment(JLabel.LEADING);

        valsymbol = new TWTextField();
        valqty = new TWTextField();
        valqty.setDocument(new ValueFormatter(ValueFormatter.INTEGER));
        valBroker = new TWTextField();
        valTransferDate = new JLabel("");
        valNewCost = new TWTextField();
        valNewCost.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));

        okBtn = new TWButton(Language.getString("OK"));
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        okBtn.addActionListener(this);
        cancelBtn.addActionListener(this);
        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlexGridLayout(new String[]{"90%", "80", "80", "10%"}, new String[]{"25"}, 10, 5));
        btnPanel.add(new JLabel(""));
        btnPanel.add(okBtn);
        btnPanel.add(cancelBtn);
        btnPanel.add(new JLabel(""));

        dataPanel.add(lblPortfolio);
        dataPanel.add(cmbPortfolio);
        dataPanel.add(lblCurrency);
        dataPanel.add(valCurrency);
        dataPanel.add(lblsymbol);
        dataPanel.add(getSymbolPanel());
        dataPanel.add(lblBroker);
        dataPanel.add(valBroker);
        dataPanel.add(lblqty);
        dataPanel.add(valqty);
        dataPanel.add(lblNewCost);
        dataPanel.add(valNewCost);
        container.add(dataPanel, BorderLayout.CENTER);
        container.add(btnPanel, BorderLayout.SOUTH);
        this.addInternalFrameListener(this);
        this.applyTheme();
        GUISettings.applyOrientation(getContentPane());
    }

    public JPanel getSymbolPanel() {
        JPanel symbolsPanel = new JPanel(new BorderLayout());
        //symbolsPanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        //symbolsPanel.setPreferredSize(new Dimension(175, 25));
        symbolsPanel.addMouseListener(this);
        txtSymbol = new TWTextField();
        txtSymbol.setDocument(new ValueFormatter(ValueFormatter.UPPERCASE, 20L));
        txtSymbol.setText("");
        txtSymbol.setEnabled(false);
        txtSymbol.setEditable(false);
        txtSymbol.setBorder(null);
//        txtSymbol.addKeyListener(this);
//        txtSymbol.addFocusListener(this);
        symbolsPanel.add(txtSymbol, BorderLayout.CENTER);
//        lblSelectedSymbols.setPreferredSize(new Dimension(150, 20));
//        symbolsPanel.add(lblSelectedSymbols);
        btnSelectSymbol = new TWButton(new DownArrow());
        btnSelectSymbol.setBorder(null);
        symbolsPanel.add(btnSelectSymbol, BorderLayout.EAST);
        btnSelectSymbol.setPreferredSize(new Dimension(20, 20));
        btnSelectSymbol.addActionListener(this);
        symbolsPanel.setBorder(BorderFactory.createEtchedBorder());
        return symbolsPanel;
    }

    private void searchSymbol() {
        try {
            String[] symbols = SharedMethods.searchTradingSymbols(getTitle(), true, true, false);
            if ((symbols != null) && (!symbols[0].equals(null))) {
                symbol = SharedMethods.getSymbolFromKey(symbols[0]);
                exchange = SharedMethods.getExchangeFromKey(symbols[0]);
                txtSymbol.setText(symbol);
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populatePortfolios() {
        try {
            if (TradingShared.getTrader() != null) {
                cmbPortfolio.removeItemListener(this);
                portfolioList.clear();
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());
                    portfolioList.add(item);
                    item = null;
                    record = null;
                }
                cmbPortfolio.updateUI();
                cmbPortfolio.setSelectedIndex(0);
                cmbPortfolio.addItemListener(this);
                selPortfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
                String currency = TradingShared.getTrader().getPortfolio(selPortfolio).getCurrencyID();
                if (currency != null) {
                    valCurrency.setText(currency);
                } else {
                    valCurrency.setText("");
                }
                currency = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setStockTransObject(StockTransferObject object) {
        this.object = object;
        setData();
    }

    public void setData() {
        txtSymbol.setText(object.getSymbol());
        valNewCost.setText(object.getAvgCost() + "");
        valqty.setText(object.getQuantity() + "");
        valTransferDate.setText(object.getTransferDate());
        cmbPortfolio.removeItemListener(this);
        cmbPortfolio.removeAllItems();
        portfolioList.clear();
        portfolioList.add(new TWComboItem(object.getPortfolioID(), object.getPortfolioID()));
//        cmbPortfolio.add(object.getPortfolioID())
        cmbPortfolio.setSelectedItem(new TWComboItem(object.getPortfolioID(), object.getPortfolioID()));
        String currency = TradingShared.getTrader().getPortfolio(object.getPortfolioID()).getCurrencyID();
        valCurrency.setText(currency);
        currency = null;
        valBroker.setText(object.getBroker());

        valsymbol.setEnabled(false);
        valNewCost.setEnabled(false);
        valqty.setEnabled(false);
        valTransferDate.setEnabled(false);
        btnSelectSymbol.setEnabled(false);
        cmbPortfolio.setEnabled(false);
        valCurrency.setEnabled(false);
        valBroker.setEnabled(false);
        okBtn.setEnabled(false);
    }

    public void applyTheme() {

    }

    private boolean validateInputs() {
        boolean isValid = true;

        if ((symbol == null) || (symbol.equals(""))) {
            isValid = false;
        }
        broker = valBroker.getText();
        if ((broker == null) || (broker.equals(""))) {
            isValid = false;
        }
        tQty = valqty.getText();
        if ((tQty == null) || (tQty.equals(""))) {
            isValid = false;
        }
        tAvgCost = valNewCost.getText();
        if ((tAvgCost == null) || (tAvgCost.equals(""))) {
            isValid = false;
        }
        return isValid;
    }

    private void setRequestData() {

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == okBtn) {
            if (validateInputs()) {
                TradeMethods.getSharedInstance().sendStockTransferRequest(selPortfolio, symbol, exchange, broker, Long.parseLong(tQty), Double.parseDouble(tAvgCost));
                this.dispose();
            }
        } else if (e.getSource() == cancelBtn) {
            this.dispose();
        } else if (e.getSource() == btnSelectSymbol) {
            searchSymbol();
        }
    }

    /* Printable method */
    public void internalFrameClosed(InternalFrameEvent e) {
        super.internalFrameClosed(e);
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == cmbPortfolio) {
            selPortfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
            String currency = TradingShared.getTrader().getPortfolio(selPortfolio).getCurrencyID();
            valCurrency.setText(currency);
            currency = null;
        }
    }
}
