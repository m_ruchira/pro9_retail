package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.IntTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.CashTransaction;
import com.isi.csvr.trading.datastore.CashTransactionSearchStore;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 4, 2005
 * Time: 2:38:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashTransactionModel extends CommonTable implements TableModel, CommonTableInterface {
    //private int type;
    private CashTransactionSearchStore dataStore;
    private ArrayList dataStore1;
    private IntTransferObject intTrasferObject;

    public CashTransactionModel() {
        //this.type = type;
        intTrasferObject = new IntTransferObject();
    }

    public void setSymbol(String symbol) {

    }

    public CashTransactionSearchStore getDataStore() {
        return dataStore;
    }

    public void setDataStore(CashTransactionSearchStore newDataStore) {
        dataStore = newDataStore;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {

        if (dataStore != null) {
//System.out.println(dataStore. size());
            return dataStore.size();
        } else {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            CashTransaction cashTransaction = (CashTransaction) dataStore.getTransaction(iRow);

            switch (iCol) {
                case 0:
                    return "" + cashTransaction.getDate();
                case 1:
                    return cashTransaction.getTypeName(cashTransaction.getTypeID());
                case 2:
                    return "" + cashTransaction.getReference();
                case 3:
                    return "" + cashTransaction.getAmount();
                case 4:
                    return UnicodeUtils.getNativeString(cashTransaction.getNarration());

            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        /*switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:*/
        return Object.class;
        //}
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[4];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));         //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("BUY_COLOR"), FIELD_BID_ROW1, Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_BID_FGCOLOR1"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELL_COLOR"), FIELD_ASK_ROW2, Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_ASK_FGCOLOR1"));
        return customizerRecords;
    }
}
