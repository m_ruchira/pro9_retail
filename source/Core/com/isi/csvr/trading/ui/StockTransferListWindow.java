package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.TableSorter;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.datastore.StockTransferObject;
import com.isi.csvr.trading.datastore.StockTransferObjectStore;
import com.isi.csvr.trading.shared.TraderProfileDataListener;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 18, 2006
 * Time: 5:05:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockTransferListWindow implements TradingConnectionListener, TraderProfileDataListener {

    private Table stockTransTable;

    public StockTransferListWindow() {

    }

    public InternalFrame createStockTransferFrame() {
        stockTransTable = new Table();
        stockTransTable.setThreadID(Constants.ThreadTypes.TRADING);
        TWMenuItem mnuDelete = new TWMenuItem(Language.getString("CANCEL_STOCK_TRANSFER"), "cancelorder.gif");
        mnuDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    int row = stockTransTable.getTable().getSelectedRow();
                    row = ((TableSorter) stockTransTable.getTable().getModel()).getUnsortedRowFor(row);
                    StockTransferObject transfer = StockTransferObjectStore.getSharedInstance().getStockTransObject(row);
                    TradeMethods.getSharedInstance().cancelStockTransfer(transfer, transfer.getID());
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        stockTransTable.getPopup().setMenuItem(mnuDelete);
        stockTransTable.setSortingEnabled();
        stockTransTable.setPreferredSize(new Dimension(400, 100));
        StockTransferModel stockTransModel = new StockTransferModel();
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("STOCKTRANSFER");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("STOCKTRANSFER_COLS"));
        stockTransTable.setWindowType(ViewSettingsManager.STOCK_TRASFER_VIEW);
        stockTransModel.setViewSettings(oSetting);
        stockTransTable.setModel(stockTransModel);
        stockTransModel.setTable(stockTransTable);
        stockTransModel.applyColumnSettings();
        stockTransTable.getTable().setDefaultEditor(String.class, new DefaultCellEditor(new TWTextField()));
        stockTransTable.getTable().setDefaultEditor(Number.class, new DefaultCellEditor(new TWTextField()));
        stockTransModel.updateGUI();
        stockTransTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = stockTransTable.getTable().getSelectedRow();
                row = ((TableSorter) stockTransTable.getTable().getModel()).getUnsortedRowFor(row);
                StockTransferObject transfer = StockTransferObjectStore.getSharedInstance().getStockTransObject(row);
                if (e.getClickCount() > 1) {
                    StockTransferWindow sw = TradeMethods.getSharedInstance().showStockTransferWindow();
                    sw.setStockTransObject(transfer);
                    sw.setVisible(true);
                }
            }
        });

        InternalFrame frame = new InternalFrame(stockTransTable);
        frame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        oSetting.setParent(frame);
        frame.getContentPane().add(stockTransTable);
        Client.getInstance().getDesktop().add(frame);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        frame.updateUI();
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(true);
        GUISettings.applyOrientation(frame);
        return frame;
    }

    public void tradeServerConnected() {

    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {

    }

    public void accountDataChanged(String accountID) {

    }

    public void portfolioDataChanged(String portfolioID) {

    }
}
