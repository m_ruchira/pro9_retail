package com.isi.csvr.trading.ui;

// Copyright (c) 2000 Integrated Systems International (ISI)

import com.isi.csvr.Client;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingConstants;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexNullLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * This dialog class is used to change the current password
 * of the user.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class ChangeTradingPass extends JDialog implements KeyListener {
    private JPanel jPanel1 = new JPanel();
    private JLabel lblOld = new JLabel();
    private JLabel lblNew1 = new JLabel();
    private JLabel lblNew2 = new JLabel();
    private JLabel lblTradeOld = new JLabel();
    private JLabel lblTradeNew1 = new JLabel();
    private JLabel lblTradeNew2 = new JLabel();
    private JLabel lblMessage = new JLabel();
    /*private VirtualKeyboardTextField txtOld;
    private VirtualKeyboardTextField txtNew1;
    private VirtualKeyboardTextField txtNew2;
    private VirtualKeyboardTextField txtTradeOld;
    private VirtualKeyboardTextField txtTradeNew1;
    private VirtualKeyboardTextField txtTradeNew2;*/
    private TWPasswordField txtOld;
    private TWPasswordField txtNew1;
    private TWPasswordField txtNew2;
    private TWPasswordField txtTradeOld;
    private TWPasswordField txtTradeNew1;
    private TWPasswordField txtTradeNew2;
    private TWButton btnCancel = new TWButton();
    private TWButton btnOk = new TWButton();
    private String newPassword;
    private String oldPassword;
    private String newTradingPassword;
    private String oldTradingPassword;
    private String message;
    private boolean messageType;

    // Server g_oServerConnector;

    /**
     * Constructs a new instance.
     *
     * @param parent
     * @param title
     * @param modal
     */
    public ChangeTradingPass(Frame parent, String title, String message, boolean modal) {
        super(parent, title, modal);
        try {
            this.message = message;
            messageType = (message != null);
            jbInit();
            pack(); // New
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */
    public ChangeTradingPass(JFrame oParent, String title, String message) {
        this(oParent, title, message, true);
//g_oServerConnector = oServerConnector;
    }

    public void keyPressed(KeyEvent ev) {
    }

    public void keyReleased(KeyEvent ev) {
        if (ev.getKeyChar() == '\n') {
            ev.consume();
            btnOk_actionPerformed();
        }
    }

    public void keyTyped(KeyEvent ev) {
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        jPanel1.setLayout(new ColumnLayout(ColumnLayout.MODE_SAME_SIZE));
        this.setBackground(Theme.getColor("BOARD_TABLE_BGCOLOR")); //BACKGROUND_COLOR"));
        this.setResizable(false);

        /*txtOld = new VirtualKeyboardTextField(this);
        txtNew1 = new VirtualKeyboardTextField(this);
        txtNew2 = new VirtualKeyboardTextField(this);

        txtTradeOld = new VirtualKeyboardTextField(this);
        txtTradeNew1 = new VirtualKeyboardTextField(this);
        txtTradeNew2 = new VirtualKeyboardTextField(this);*/

        txtOld = new TWPasswordField(this, TWControl.isUseVirtualKeyBoard(),
                TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));
        txtNew1 = new TWPasswordField(this, TWControl.isUseVirtualKeyBoard(),
                TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));
        txtNew2 = new TWPasswordField(this, TWControl.isUseVirtualKeyBoard(),
                TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));

        txtTradeOld = new TWPasswordField(this, TWControl.isUseVirtualKeyBoard(),
                TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));
        txtTradeNew1 = new TWPasswordField(this, TWControl.isUseVirtualKeyBoard(),
                TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));
        txtTradeNew2 = new TWPasswordField(this, TWControl.isUseVirtualKeyBoard(),
                TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));

        JPanel level1Panel = new JPanel();
        level1Panel.setLayout(new FlexNullLayout());
        level1Panel.setBorder(BorderFactory.createTitledBorder(Language.getString("LEVEL_1_PASSWORD")));
        level1Panel.setPreferredSize(new Dimension(320, 130));

        lblOld.setText(Language.getString("PASSWORD_OLD"));
        lblOld.setBounds(new Rectangle(18, 20, 132, 20));
        lblNew1.setText(Language.getString("PASSWORD_NEW1"));
        lblNew1.setBounds(new Rectangle(18, 55, 132, 20));
        lblNew2.setText(Language.getString("PASSWORD_NEW2"));
        lblNew2.setBounds(new Rectangle(18, 90, 132, 20));

        txtOld.setUsername(TradingShared.getTradeUserName());
        txtNew1.setUsername(TradingShared.getTradeUserName());
        txtNew2.setUsername(TradingShared.getTradeUserName());
        txtTradeOld.setUsername(TradingShared.getTradeUserName());
        txtTradeNew1.setUsername(TradingShared.getTradeUserName());
        txtTradeNew2.setUsername(TradingShared.getTradeUserName());

        txtOld.setBounds(new Rectangle(154, 20, 155, 20));
        txtNew1.setBounds(new Rectangle(154, 55, 155, 20));
        txtNew2.setBounds(new Rectangle(154, 90, 155, 20));

        JPanel level2Panel = new JPanel();
        level2Panel.setLayout(new FlexNullLayout());
        level2Panel.setBorder(BorderFactory.createTitledBorder(Language.getString("LEVEL_2_PASSWORD")));
        level2Panel.setPreferredSize(new Dimension(320, 130));
//        level2Panel.setBounds(5, 140, 320, 130);

        lblTradeOld.setText(Language.getString("PASSWORD_OLD"));
        lblTradeOld.setBounds(new Rectangle(18, 20, 132, 20));
        lblTradeNew1.setText(Language.getString("PASSWORD_NEW1"));
        lblTradeNew1.setBounds(new Rectangle(18, 55, 132, 20));
        lblTradeNew2.setText(Language.getString("PASSWORD_NEW2"));
        lblTradeNew2.setBounds(new Rectangle(18, 90, 132, 20));


        txtTradeOld.setBounds(new Rectangle(154, 20, 155, 20));
        txtTradeNew1.setBounds(new Rectangle(154, 55, 155, 20));
        txtTradeNew2.setBounds(new Rectangle(154, 90, 155, 20));
        txtTradeNew2.addKeyListener(this);

        if ((TradingShared.getLevel2AuthType() != TradeMeta.LEVEL2_AUTH_PW) &&
                (TradingShared.getLevel2AuthType() != TradeMeta.LEVEL2_AUTH_PW_ALLWAYS)) {
            txtTradeOld.setEnabled(false);
            txtTradeNew1.setEnabled(false);
            txtTradeNew2.setEnabled(false);
        }

//        int buttonTopOffset = 0;
        if (messageType) {
//            buttonTopOffset = 100;
            //lblMessage.setPreferredSize(new Dimension(320, 101));
//            lblMessage.setBounds(new Rectangle(18, 275, 300, 101));
            lblMessage.setText(UnicodeUtils.getNativeString(message));
        }

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        btnCancel.setText(Language.getString("CANCEL"));

//        btnCancel.setBounds(new Rectangle(248, 275 + buttonTopOffset, 75, 29));
        btnCancel.setPreferredSize(new Dimension(75, 25));
        btnCancel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnCancel_actionPerformed();
            }
        });
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnCancel_actionPerformed();
            }
        });
        btnOk.setText(Language.getString("OK"));
        buttonPanel.add(btnOk);
        buttonPanel.add(btnCancel);
//        btnOk.setBounds(new Rectangle(159, 275 + buttonTopOffset, 75, 29));
        btnOk.setPreferredSize(new Dimension(75, 25));
        btnOk.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnOk_actionPerformed();
            }
        });
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnOk_actionPerformed();
            }
        });

        getContentPane().add(jPanel1);
        jPanel1.add(level1Panel, null);
        level1Panel.add(lblOld, null);
        level1Panel.add(lblNew1, null);
        level1Panel.add(lblNew2, null);
        level1Panel.add(txtOld, null);
        level1Panel.add(txtNew1, null);
        level1Panel.add(txtNew2, null);

        if ((TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW) ||
                (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW_ALLWAYS)) {
            jPanel1.add(level2Panel, null);
            level2Panel.add(lblTradeOld, null);
            level2Panel.add(lblTradeNew1, null);
            level2Panel.add(lblTradeNew2, null);
            level2Panel.add(txtTradeOld, null);
            level2Panel.add(txtTradeNew1, null);
            level2Panel.add(txtTradeNew2, null);
        }

        jPanel1.add(lblMessage, null);
        jPanel1.add(buttonPanel);

        GUISettings.applyOrientation(this);

        pack();
    }

    /**
     * Displays the dialog
     */
    public void showDialog() {
        /*Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension screen = oToolkit.getScreenSize();
        if (messageType) {
            this.setBounds((int) ((screen.getWidth() - 340) / 2),
                    (int) ((screen.getHeight() - 435) / 2), 340, 435);
        } else {
            this.setBounds((int) ((screen.getWidth() - 340) / 2),
                    (int) ((screen.getHeight() - 335) / 2), 340, 335);
        }*/
        setLocationRelativeTo(Client.getInstance().getFrame());
        newPassword = null;
        oldPassword = null;
        newTradingPassword = null;
        oldTradingPassword = null;
        this.setVisible(true);
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewTradingPassword() {
        return newTradingPassword;
    }

    public String getOldTradingPassword() {
        return oldTradingPassword;
    }

    /**
     * Validates input to make sure the new password is
     * typed correctly
     */
    private boolean validateInput() {
        Object[] options = {Language.getString("OK")};

        if ((new String(txtOld.getPassword()).trim().equals("")) ||
                (new String(txtNew1.getPassword()).trim().equals("")) ||
                (new String(txtNew2.getPassword()).trim().equals("")) ||
                ((TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW_ALLWAYS) ||
                        (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW)) &&
                        ((new String(txtTradeOld.getPassword()).trim().equals("")) ||
                                (new String(txtTradeNew1.getPassword()).trim().equals("")) ||
                                (new String(txtTradeNew2.getPassword()).trim().equals("")))
                ) {
            JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    Language.getString("FILL_ALL_PASSWORDS"),
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
            return false;
        } else if ((new String(txtOld.getPassword()).trim().equals(new String(txtNew1.getPassword()).trim()))) {
            JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    Language.getString("LEVEL1_PW_SAME_AS_OLD_PWD"),
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
            return false;
        } else if (!new String(txtNew1.getPassword()).trim().equals(new String(txtNew2.getPassword()).trim())) {
            JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    Language.getString("LEVEL1_PW_1_2_NOT_MATCH"),
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
            return false;
        } else if ((new String(txtTradeOld.getPassword()).trim().equals(new String(txtTradeNew1.getPassword()).trim())) &&
                ((TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW_ALLWAYS) ||
                        (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW))) {
            JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    Language.getString("LEVEL2_PW_SAME_AS_OLD_PWD"),
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
            return false;
        } else if ((!new String(txtTradeNew1.getPassword()).trim().equals(new String(txtTradeNew2.getPassword()).trim())) &&
                ((TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW_ALLWAYS) ||
                        (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW))) {
            JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    Language.getString("LEVEL2_PW_1_2_NOT_MATCH"),
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
            return false;
//        } else if ((txtNew1.getPassword().length < 4) ||
//                ((txtTradeNew1.getPassword().length < 4) && ((TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW_ALLWAYS) ||
//                        (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW)))) {
//                    JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
//                    Language.getString("MSG_PASSWORD_TOO_SHORT"),
//                    //    "Your Password is too short. Please enter a password that has more than 6 characters.",
//                    Language.getString("ERROR"),
//                    JOptionPane.OK_OPTION,
//                    JOptionPane.ERROR_MESSAGE,
//                    null,
//                    options,
//                    options[0]);
//            return false;
        } else
            return true;
    }

    /**
     * When Ok Button is pressed
     */
    void btnOk_actionPerformed() {
        Object[] options = {Language.getString("OK")};

        if (validateInput() && TradeMethods.getSharedInstance().isPasswordComplexityValid(new String(txtNew1.getPassword()).trim(), TradingConstants.LEVEL1_TRADING_PASSWORD) &&
                TradeMethods.getSharedInstance().isPasswordComplexityValid(new String(txtTradeNew1.getPassword()).trim(), TradingConstants.LEVEL2_TRADING_PASSWORD)) {
//            if ( (new String(txtNew1.getPassword()).trim().equals("")) || (new String(txtNew2.getPassword()).trim().equals(""))) {
//                JOptionPane.showOptionDialog(Client.getInstance(),
//                        Language.getString("MSG_INVALID_PASSWORD"),
//                        Language.getString("ERROR"),
//                        JOptionPane.OK_OPTION,
//                        JOptionPane.ERROR_MESSAGE,
//                        null,
//                        options,
//                        options[0]);
//                return;
//            }
            oldPassword = new String(txtOld.getPassword()).trim();
            newPassword = new String(txtNew1.getPassword()).trim();
            oldTradingPassword = new String(txtTradeOld.getPassword()).trim();
            newTradingPassword = new String(txtTradeNew1.getPassword()).trim();

            if (!TradeMethods.getSharedInstance().isPasswordValid(newPassword, TradingConstants.LEVEL1_TRADING_PASSWORD)) {
                return;
            }
//            if (TradeMethods.getSharedInstance().isPasswordComplexityValid(newPassword, TradingConstants.LEVEL1_TRADING_PASSWORD)) {
//                return;
//            }
            if (!TradeMethods.getSharedInstance().isPasswordValid(newTradingPassword, TradingConstants.LEVEL2_TRADING_PASSWORD)) {
                return;
            }
//            if (TradeMethods.getSharedInstance().isPasswordComplexityValid(newTradingPassword, TradingConstants.LEVEL2_TRADING_PASSWORD)) {
//                return;
//            }

            hide();

//        	try
//            {
//                SendQueue.addData(
//                    Meta.NEWPASSWORD + Meta.DS
//                	+ SharedMethods.encrypt(new String(txtNew1.getPassword()).trim()) + Meta.FS
//                    + Meta.USER_PASSWORD + Meta.DS
//                    + SharedMethods.encrypt(new String(txtOld.getPassword()).trim())  + Meta.FS
//                    + Meta.SESSIONID + Meta.DS + Settings.getSessionID()
//                    );
//                this.dispose();
//            }
//            catch(Exception e2)
//            {
//            	e2.printStackTrace();
//	        	JOptionPane.showOptionDialog(Client.getInstance(),
//                    Language.getString("MSG_COMM_FAIL"),
//                    Language.getString("ERROR"),
//                    JOptionPane.OK_OPTION,
//                    JOptionPane.ERROR_MESSAGE,
//                    null,
//                    options,
//                    options[0]);
//            }
            //this.hide();
        } else {
//        	JOptionPane.showOptionDialog(Client.getInstance(),
//                    Language.getString("MSG_NEW_PASS1_PASS2_ERROR"),
//                    Language.getString("ERROR"),
//                    JOptionPane.OK_OPTION,
//                    JOptionPane.ERROR_MESSAGE,
//                    null,
//                    options,
//                    options[0]);
        }
    }

//    private boolean checkPasswordComplexity(String loginpwd, String tradepwd){
//         if()
//
//
//    }

    /**
     * When cancel button is pressed
     */
    void btnCancel_actionPerformed() {
        //clearData();
        this.hide();
    }

//    public static void clearData()
//    {
//    	txtOld.setText("");
//        txtNew1.setText("");
//        txtNew2.setText("");
//    }
}


