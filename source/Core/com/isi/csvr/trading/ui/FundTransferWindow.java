package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.TableSorter;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.FundTransferStore;
import com.isi.csvr.trading.shared.*;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 9, 2006
 * Time: 4:50:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class FundTransferWindow extends InternalFrame implements ItemListener,
        TraderProfileDataListener, TradingConnectionListener, Themeable {

    private Table fundTransferTable;
    private ArrayList<TWComboItem> typeList;
    private ArrayList<TWComboItem> methodList;
    private ArrayList<TWComboItem> portfolioList;
    private TWComboBox cmbType;
    private TWComboBox cmbMethod;
    private TWComboBox cmbPortfolio;

    private String selectedType = "*";
    private String selectedMethod = "*";
    private String selectedPortfolio = "*";

    private JLabel lblType;
    private JLabel lblStatus;
    private JLabel lblExchange;
    private ViewSetting oSetting;
    private TWMenuItem mnuDelete;

    public FundTransferWindow(Table fundTransferTable) {
        super(fundTransferTable);
        this.fundTransferTable = fundTransferTable;
        createFundTransferFrame();
        Theme.registerComponent(this);
    }

    public void sort() {
        fundTransferTable.resort();
    }

    public ViewSetting getViewSetting() {
        return oSetting;
    }

    public InternalFrame createFundTransferFrame() {
//        fundTransferTable = new Table();
        fundTransferTable.setThreadID(Constants.ThreadTypes.TRADING);
        mnuDelete = new TWMenuItem(Language.getString("CANCEL_FUND_TRANSFER"), "cancelorder.gif");
        mnuDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    int row = fundTransferTable.getTable().getSelectedRow();
                    row = ((TableSorter) fundTransferTable.getTable().getModel()).getUnsortedRowFor(row);
                    FundTransfer transfer = FundTransferStore.getSharedInstance().getFilteredTransaction(row);
                    TradeMethods.getSharedInstance().cancelFundTransfer(transfer, transfer.getType(), transfer.getCashTransactionID());
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        fundTransferTable.getPopup().setMenuItem(mnuDelete);
        fundTransferTable.setSortingEnabled();
        fundTransferTable.setPreferredSize(new Dimension(500, 100));
        FundTransferModel fundTransModel = new FundTransferModel();
        oSetting = ViewSettingsManager.getSummaryView("FUNDTRANSFER");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("FUNDTRANSFER_COLS"));
        fundTransferTable.setWindowType(ViewSettingsManager.FUND_TRANSFER_FRAME);
        fundTransModel.setViewSettings(oSetting);
        fundTransferTable.setModel(fundTransModel);
        fundTransModel.setTable(fundTransferTable);
        fundTransModel.applyColumnSettings();
        fundTransferTable.getTable().setDefaultEditor(String.class, new DefaultCellEditor(new TWTextField()));
        fundTransferTable.getTable().setDefaultEditor(Number.class, new DefaultCellEditor(new TWTextField()));
        ((SmartTable) fundTransferTable.getTable()).setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
        fundTransModel.updateGUI();
        fundTransferTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = fundTransferTable.getTable().getSelectedRow();
                row = ((TableSorter) fundTransferTable.getTable().getModel()).getUnsortedRowFor(row);
                FundTransfer transfer = FundTransferStore.getSharedInstance().getFilteredTransaction(row);
                mnuDelete.setEnabled(TradeMethods.getSharedInstance().getItemValidity("CANCEL", "FUND_TRANSFER_LIST", "*", ("" + transfer.getStatus()).charAt(0)));
                if (e.getClickCount() > 1) {
                    if (transfer.getType() == TradingConstants.TRANSFER_WITHDRAWAL) {
                        WithdrawaRequest wrObject = (WithdrawaRequest) transfer;
                        WithdrawalRequestFrame wr = TradeMethods.getSharedInstance().showWithdrawalRequest(wrObject.getMethod() == 0);
                        wr.setData(wrObject.getDate(), wrObject.getAmount(), wrObject.getMethod(), wrObject.getAccount(),
                                wrObject.getPortfolio(), wrObject.getBank());
                        wr = null;
                        wrObject = null;
                    } else if (transfer.getType() == TradingConstants.TRANSFER_FUND_TRANSFER) {
                        FundTransferRequest frObject = (FundTransferRequest) transfer;
                        FundTransferUI fundUI = TradeMethods.getSharedInstance().showDepositeFrame();
                        fundUI.setData(frObject.getMethod(), frObject.getCurrency(), frObject.getPortfolio(), frObject.getBank(), frObject.getBankAddress(),
                                frObject.getChequeNo(), frObject.getChequeDate(), frObject.getDepositeDate(), frObject.getNote(), frObject.getBranch(),
                                frObject.getBankID(), frObject.getReferenceNo(), frObject.getAmount());
                        frObject = null;
                        fundUI = null;
                    }
                }
            }
        });

        fundTransferTable.setNorthPanel(getFundTransFilter());
//        InternalFrame frame = new InternalFrame(fundTransferTable);
        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        oSetting.setParent(this);
        setSize(oSetting.getSize());
        setLocation(oSetting.getLocation());
        this.getContentPane().add(fundTransferTable);
        Client.getInstance().getDesktop().add(this);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        this.updateUI();
//        this.applySettings();
        this.setLayer(GUISettings.TOP_LAYER);
        this.setVisible(false);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);

        GUISettings.applyOrientation(this);
        this.setOrientation();
        this.applySettings();
        return this;
    }


    private JPanel getFundTransFilter() {
        String[] widths = {"80", "100", "80", "100", "80", "100"};
        String[] heights = {"20"};
        JPanel panel = new JPanel(new FlexGridLayout(widths, heights, 5, 5));

        lblType = new JLabel(Language.getString("TYPE"));
        panel.add(lblType);
        typeList = new ArrayList<TWComboItem>();
        TWComboModel typeComboModel = new TWComboModel(typeList);
        typeList.add(new TWComboItem("*", Language.getString("ALL")));
        typeList.add(new TWComboItem("" + TradingConstants.TRANSFER_FUND_TRANSFER, Language.getString("FUND_TRANSFER_NOTIFICATION")));
        typeList.add(new TWComboItem("" + TradingConstants.TRANSFER_WITHDRAWAL, Language.getString("WITHDRAWAL_REQUEST")));
        cmbType = new TWComboBox(typeComboModel);
        cmbType.setExtendedMode(true);
        panel.add(cmbType);
        try {
            cmbType.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbType.addItemListener(this);

        lblStatus = new JLabel(Language.getString("STATUS"));
        panel.add(lblStatus);
        methodList = new ArrayList<TWComboItem>();
        TWComboModel statusomboModel = new TWComboModel(methodList);
        cmbMethod = new TWComboBox(statusomboModel);
        panel.add(cmbMethod);
        populateMethods();
        cmbMethod.addItemListener(this);


        lblExchange = new JLabel(Language.getString("PORTFOLIO"));
        panel.add(lblExchange);
        portfolioList = new ArrayList<TWComboItem>();
        TWComboModel exchangeComboModel = new TWComboModel(portfolioList);
        cmbPortfolio = new TWComboBox(exchangeComboModel);
        populatePortfolios();
        panel.add(cmbPortfolio);
        cmbPortfolio.addItemListener(this);

        return panel;
    }

    private void populateMethods() {
        methodList.clear();
        methodList.add(new TWComboItem("*", Language.getString("ALL")));
        methodList.add(new TWComboItem(TradingConstants.TRANSFER_STATUS_PENDING, Language.getString("FT_PENDING")));
        methodList.add(new TWComboItem(TradingConstants.TRANSFER_STATUS_CANCELLED, Language.getString("FT_CANCELLED")));
        methodList.add(new TWComboItem(TradingConstants.TRANSFER_STATUS_REJECTED, Language.getString("FT_REJECTED")));
        methodList.add(new TWComboItem(TradingConstants.TRANSFER_STATUS_VALIDATED, Language.getString("FT_VALIDATED")));
        methodList.add(new TWComboItem(TradingConstants.TRANSFER_STATUS_APPROVED, Language.getString("FT_APPROVED")));
        try {
            cmbMethod.setSelectedItem(new TWComboItem("*", Language.getString("ALL")));
        } catch (Exception e) {
            cmbMethod.setSelectedIndex(0);
        }
    }

    private void populatePortfolios() {
        cmbPortfolio.removeItemListener(this);
        try {
            if (TradingShared.getTrader() != null) {
                portfolioList.clear();
                portfolioList.add(new TWComboItem("*", Language.getString("ALL")));
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());
                    portfolioList.add(item);
                    item = null;
                    record = null;
                }
                cmbPortfolio.updateUI();
                try {
                    cmbPortfolio.setSelectedItem(new TWComboItem("*", Language.getString("ALL")));
                } catch (Exception e) {
                    cmbPortfolio.setSelectedIndex(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbPortfolio.addItemListener(this);
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == cmbType) {
            selectedType = (((TWComboItem) cmbType.getSelectedItem()).getId());
            FundTransferStore.getSharedInstance().setFilter(selectedType, selectedMethod, selectedPortfolio);
        } else if (e.getSource() == cmbMethod) {
            selectedMethod = (((TWComboItem) cmbMethod.getSelectedItem()).getId());
            FundTransferStore.getSharedInstance().setFilter(selectedType, selectedMethod, selectedPortfolio);
        } else if (e.getSource() == cmbPortfolio) {
            selectedPortfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
            FundTransferStore.getSharedInstance().setFilter(selectedType, selectedMethod, selectedPortfolio);
        }
    }

    public void tradeServerConnected() {
        TradeMethods.getSharedInstance().pendingFundTransfer(Constants.PATH_PRIMARY);
        //  TradeMethods.getSharedInstance().pendingFundTransfer(Constants.PATH_SECONDARY);
        populatePortfolios();
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
        TradeMethods.getSharedInstance().pendingFundTransfer(Constants.PATH_SECONDARY);
        populatePortfolios();
    }

    public void tradeServerDisconnected() {
    }

    public void accountDataChanged(String accountID) {
    }

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios();
    }

    public String[][] getPanelString() {
        String[][] strArray = new String[3][8];
        strArray[0][0] = "10";
        strArray[0][1] = lblType.getText();
        strArray[0][2] = "4";
        strArray[0][3] = (((TWComboItem) cmbType.getSelectedItem()).getValue());
        strArray[0][4] = "10";
        strArray[0][5] = "";
        strArray[0][6] = "4";
        strArray[0][7] = "";
        strArray[1][0] = "10";
        strArray[1][1] = lblStatus.getText();
        strArray[1][2] = "4";
        strArray[1][3] = (((TWComboItem) cmbMethod.getSelectedItem()).getValue());
        strArray[1][4] = "10";
        strArray[1][5] = "";
        strArray[1][6] = "4";
        strArray[1][7] = "";
        strArray[2][0] = "10";
        strArray[2][1] = lblExchange.getText();
        strArray[2][2] = "4";
        strArray[2][3] = (((TWComboItem) cmbPortfolio.getSelectedItem()).getValue());
        strArray[2][4] = "10";
        strArray[2][5] = "";
        strArray[2][6] = "4";
        strArray[2][7] = "";
        return strArray;
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }
}
