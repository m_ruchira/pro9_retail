package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.calendar.CalCombo;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TraderProfileDataListener;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum
 * Date: Sep 27, 2005
 * Time: 2:48:23 PM
 */
public class FundTransferUI extends InternalFrame implements ActionListener, Themeable, TraderProfileDataListener, TradingConnectionListener {
    private JPanel panel1, panel2, panel3, panel4, panel5, panel6;
    private TWButton okBtn, cancelBtn;
    private TWTextField chequeNumberField;
    private TWTextField bankField;
    private TWTextField originatingBankField;
    private TWTextField referenceField;
    private TWTextField ChequeRefField;
    private TWTextArea bankAddressField;
    private TWTextField branchField;
    private TWTextField refNoField;
    private TWTextArea notesField;
    private CalCombo chequeCombo;
    private CalCombo depositeCombo;
    private CalCombo depDateCombo;
    private JComboBox mainCombo;
    private JComboBox bankCombo;
    private CardLayout card1;
    private JComboBox currencyCombo;
    private TWTextField amountField;
    private boolean isCardOneSelected = true;
    //    private Color color1;
//    private Color color2;
    private int index = 999999;
    private int cardType = 0;
    //private JComboBox cmbExchanges;
    private TWComboModel currencyModel;
    private TWComboModel bankModel;
    private ArrayList<TWComboItem> currencyList;
    private ArrayList<TWComboItem> bankList;
    private ArrayList<TWComboItem> modes;
    private TWComboBox cmbPortfolio;
    private ArrayList<TWComboItem> portfolioList;
    private int mainwidth = 325;

    private String selectedPortfolo = null;

    public FundTransferUI() {
        super(false);
        createUI();
    }

    public void createUI() {

        this.setSize(mainwidth, 475);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setTitle(Language.getString("FUND_TRANSFER_NOTIFICATION"));
        this.setClosable(true);
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        JPanel container = new JPanel();
//        getContentPane().setLayout(new BorderLayout());
        Container container = this.getContentPane();
        container.setLayout(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));

//        String[] width = {""+columnwidth, ""+(columnwidth-5)};
        String[] width = {"50%", "50%"};
        Border eched = BorderFactory.createEtchedBorder();
        Theme.registerComponent(this);

        panel1 = new JPanel();
        String[] height1 = {"25", "25"};
        FlexGridLayout flex1 = new FlexGridLayout(width, height1, 5, 5);
        panel1.setLayout(flex1);
        JLabel lblPortfolio = new JLabel(Language.getString("PORTFOLIO"));
        panel1.add(lblPortfolio);
        portfolioList = new ArrayList<TWComboItem>();
        cmbPortfolio = new TWComboBox(new TWComboModel(portfolioList));
        populatePortfolios();
        panel1.add(cmbPortfolio);
        JLabel lbel1 = new JLabel(Language.getString("PAYMENT_METHOD"));
        panel1.add(lbel1);
        cmbPortfolio.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                selectedPortfolo = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
                populateBank();
                modes.clear();
                TradingShared.getDepositMethods(modes, selectedPortfolo);
            }
        });
//        panel1.setBorder(BorderFactory.createLineBorder(Color.RED));
        currencyList = new ArrayList<TWComboItem>();
        populateCurrency();
        currencyModel = new TWComboModel(currencyList);
        currencyCombo = new JComboBox(currencyModel);
        if (index != 999999) {
            currencyCombo.setSelectedIndex(index);
        } else if (currencyList != null) {
            try {
                currencyCombo.setSelectedIndex(0);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        CurrencyStore.getSharedInstance().getCurrencyIDs();
//        Object[] obj1 = {Language.getString("CHEQUE"), Language.getString("BANK_TRANSFER"), Language.getString("CASH")};
        modes = new ArrayList<TWComboItem>();
        TradingShared.getDepositMethods(modes, selectedPortfolo);
        mainCombo = new JComboBox(new TWComboModel(modes));
        try {
            mainCombo.setSelectedIndex(0);
        } catch (Exception e) {
        }
        panel1.add(mainCombo);
        container.add(panel1);

        panel2 = new JPanel();
        String[] height2 = {"25"};
        FlexGridLayout flex2 = new FlexGridLayout(width, height2, 5, 5);
        panel2.setLayout(flex2);
        JLabel lbel2 = new JLabel(Language.getString("CURRENCY"));
        panel2.add(lbel2);
        panel2.add(currencyCombo);
        container.add(panel2);

        panel3 = new JPanel();
        card1 = new CardLayout();
        panel3.setLayout(card1);
        JPanel chequeInternal = new JPanel();
        String[] heightInter1 = {"25", "25", "25", "25"};
        chequeInternal.setLayout(new FlexGridLayout(width, heightInter1, 5, 5));
        JLabel chequeNumberLabel = new JLabel(Language.getString("CHEQUE_NUMBER"));
        JLabel chequeDateLabel = new JLabel(Language.getString("CHEQUE_DATE"));
        JLabel bankLabel = new JLabel(Language.getString("BANK_S"));
        JLabel depositeDateLabel = new JLabel(Language.getString("DEPOSIT_DATE"));
        //JLabel ChequeRefLabel = new JLabel(Language.getString("REFERENCE_NUMBER"));
        chequeNumberField = new TWTextField();
        bankField = new TWTextField();
        //ChequeRefField = new TWTextField();

        chequeInternal.add(chequeNumberLabel);
        chequeInternal.add(chequeNumberField);
        chequeInternal.add(chequeDateLabel);

        chequeCombo = new CalCombo();
        chequeInternal.add(chequeCombo);

        chequeInternal.add(bankLabel);
        chequeInternal.add(bankField);
        chequeInternal.add(depositeDateLabel);

        depositeCombo = new CalCombo();
        chequeInternal.add(depositeCombo);
        //chequeInternal.add(ChequeRefLabel);
        //chequeInternal.add(ChequeRefField);

        panel3.add("Cheque", chequeInternal);

        JPanel bankInternal = new JPanel();
        String[] heightInter2 = {"25", "100%"};
        bankInternal.setLayout(new FlexGridLayout(width, heightInter2, 5, 5));
        JLabel originatingBankLabel = new JLabel(Language.getString("ORIGINATING_BANK"));
        //JLabel referenceLabel = new JLabel(Language.getString("REFERENCE_NUMBER"));
        JLabel bankAddressLabel = new JLabel(Language.getString("BANK_ADDRESS"));
        originatingBankField = new TWTextField();
        //referenceField = new TWTextField();
        bankAddressField = new TWTextArea();
        bankAddressField.setBorder(null);
        JScrollPane bankPane = new JScrollPane(bankAddressField, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        bankInternal.add(originatingBankLabel);
        bankInternal.add(originatingBankField);
        //bankInternal.add(referenceLabel);
        //bankInternal.add(referenceField);
        bankInternal.add(bankAddressLabel);
        bankInternal.add(bankPane);

        panel3.add("Bank", bankInternal);

        JPanel cashInternal = new JPanel();
        String[] heightInter3 = {"25", "25", "25"};
        cashInternal.setLayout(new FlexGridLayout(width, heightInter3, 5, 5));
        JLabel depDateLabel = new JLabel(Language.getString("DEPOSIT_DATE"));
        JLabel banLabel = new JLabel(Language.getString("BANK_S"));
        JLabel branchLabel = new JLabel(Language.getString("BRANCH_DEPOSITE"));
        //JLabel refNoLabel = new JLabel(Language.getString("REFERENCE_NUMBER"));
        depDateCombo = new CalCombo();
        branchField = new TWTextField();
        //refNoField = new TWTextField();
        bankList = new ArrayList<TWComboItem>();
        bankModel = new TWComboModel(bankList);
        bankCombo = new JComboBox(bankModel);
        populateBank();

        if ((bankList != null) && (bankList.size() > 0)) {
            bankCombo.setSelectedIndex(0);
        }
        cashInternal.add(depDateLabel);
        cashInternal.add(depDateCombo);
        cashInternal.add(banLabel);
        cashInternal.add(bankCombo);
        cashInternal.add(branchLabel);
        cashInternal.add(branchField);
        //cashInternal.add(refNoLabel);
        //cashInternal.add(refNoField);

        panel3.add("Cash", cashInternal);
        container.add(panel3);

        mainCombo.addActionListener(this);
        currencyCombo.addActionListener(this);
        selectPanel();

        panel5 = new JPanel();
        //panel5.setPreferredSize(new Dimension(this.getWidth(), 100));
        String[] height5 = {"25", "25", "50"};
        FlexGridLayout flex5 = new FlexGridLayout(width, height5, 5, 5);
        panel5.setLayout(flex5);

        JLabel refNoLabel = new JLabel(Language.getString("REFERENCE_NUMBER"));
        JLabel amountLabel = new JLabel(Language.getString("AMOUNT_S"));
        JLabel notesLabel = new JLabel(Language.getString("OPTIONAL_NOTES"));
        refNoField = new TWTextField();
        amountField = new TWTextField();
        amountField.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        notesField = new TWTextArea();
        notesField.setBorder(BorderFactory.createEmptyBorder());
//        notesField.setBorder(eched);
        JScrollPane notesPane = new JScrollPane(notesField, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        panel5.add(refNoLabel);
        panel5.add(refNoField);
        panel5.add(amountLabel);
        panel5.add(amountField);
        panel5.add(notesLabel);
        panel5.add(notesPane);

        container.add(panel5);

        panel4 = new JPanel();
        String[] width2 = {"100%"};
        String[] height3 = {"50"};
        panel4.setLayout(new FlexGridLayout(width2, height3, 5, 5));
        JLabel requiredFields = new JLabel(Language.getString("REQUIRED_FIELDS"));
        panel4.add(requiredFields);

        container.add(panel4);

        panel6 = new JPanel();
        panel6.setLayout(new FlowLayout(FlowLayout.TRAILING));
        JPanel btnPanel = new JPanel();
        okBtn = new TWButton(Language.getString("SUBMIT"));
        cancelBtn = new TWButton(Language.getString("CLOSE"));
        okBtn.addActionListener(this);
        cancelBtn.addActionListener(this);
        btnPanel.add(okBtn);
        btnPanel.add(cancelBtn);
        panel6.add(btnPanel);
        container.add(panel6, BorderLayout.SOUTH);

//        getContentPane().add(container, BorderLayout.NORTH);
        this.addInternalFrameListener(this);
        this.applyTheme();
        setOrientation();
        GUISettings.applyOrientation(getContentPane());

    }

    private void populateBank() {
        try {
            bankList.clear();
            ArrayList<String[]> bank = TradingShared.getTrader().getBanks(selectedPortfolo);
            //  ArrayList<String[]> bank = TradingShared.getTrader().getBanks();
            if (bank.size() > 0) {
                for (int i = 0; i < bank.size(); i++) {
                    //  bankList.add(new TWComboItem((bank.get(i)[1]), (bank.get(i)[2])));
                    bankList.add(new TWComboItem((bank.get(i)[0] + ";" + bank.get(i)[1]), (bank.get(i)[2]))); //todo added by udaya
//                bankList.add(new TWComboItem((bank.get(i)[0]), (bank.get(i)[2])));
                }
            } else {
                bankList.add(new TWComboItem(-1, Language.getString("NA")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateCurrency() {
        try {
            currencyList.clear();
            Enumeration currency = CurrencyStore.getSharedInstance().getCurrencyIDs();
            String defaultCurrency = TradingShared.getTrader().getAccount(0).getCurrency();
            int i = 0;
            while (currency.hasMoreElements()) {
                String id = (String) currency.nextElement();
                if (id.equals(defaultCurrency)) {
                    index = i;
                }
                String discription = CurrencyStore.getSharedInstance().getCurrencyDescription(id);
                currencyList.add(new TWComboItem(id, discription));
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void setData(int type, String currency, String portfolio, String bank, String bankAddress, String chequeNo, String chequeDate,
                        String depositeDate, String notes, String branch, String bankID, String referenceNo, double amount) {
        currencyList.clear();
        String discription = CurrencyStore.getSharedInstance().getCurrencyDescription(currency.trim());
        currencyList.add(new TWComboItem(currency, discription));
        currencyCombo.setSelectedItem(new TWComboItem(currency, discription));
        currencyCombo.setEnabled(false);
        selectedPortfolo = portfolio;
        TradingShared.getDepositMethods(modes, selectedPortfolo, true); // reload modes.
        mainCombo.updateUI();
        for (int i = 0; i < mainCombo.getItemCount(); i++) {
            TWComboItem item = (TWComboItem) mainCombo.getItemAt(i);
            if (item.getId().equals("" + type)) {
                mainCombo.setSelectedIndex(i);
                break;
            }
        }
//        mainCombo.setSelectedItem(new TWComboItem(99, Language.getString("BANK_TRANSFER")));
        mainCombo.setEnabled(false);
        cmbPortfolio.removeAllItems();
        portfolioList.clear();
        portfolioList.add(new TWComboItem(portfolio, portfolio));
        cmbPortfolio.setSelectedIndex(0);
        selectedPortfolo = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
        cmbPortfolio.setEnabled(false);
        if (notes != null) {
            notesField.setText(notes);
        }
        notesField.setEnabled(false);
        refNoField.setText(referenceNo);
        refNoField.setEnabled(false);
        amountField.setText(amount + "");
        amountField.setEnabled(false);
        populateBank();
        if (bankList != null && bankList.size() > 0) {
            bankCombo.setSelectedIndex(0);
        }
        bankCombo.updateUI();
        if (type == TradeMeta.TRANSFER_CHEQUE) {
            card1.show(panel3, "Cheque");
            bankField.setText(bank);
            bankField.setEnabled(false);
            chequeNumberField.setText(chequeNo);
            chequeNumberField.setEnabled(false);
            chequeCombo.setDate(chequeDate);
            chequeCombo.setEnabled(false);
            depositeCombo.setDate(depositeDate);
            depositeCombo.setEnabled(false);
        } else if (type == TradeMeta.TRANSFER_BANK) {
            card1.show(panel3, "Bank");
            originatingBankField.setText(bank);
            bankAddressField.setText(bankAddress);
            originatingBankField.setEnabled(false);
            bankAddressField.setEnabled(false);
        } else {
            card1.show(panel3, "Cash");
            depDateCombo.setDate(depositeDate);
            depDateCombo.setEnabled(false);
            branchField.setText(branch);
            branchField.setEnabled(false);
            bankList.clear();
            bankList.add(new TWComboItem(bankID, bankID));
            bankCombo.setSelectedIndex(0);
            bankCombo.setEnabled(false);

        }
        okBtn.setEnabled(false);
        cancelBtn.setEnabled(true);

    }

    public void applyTheme() {
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");
        SwingUtilities.updateComponentTreeUI(this);
        depDateCombo.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        depositeCombo.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        chequeCombo.setBorder(BorderFactory.createEtchedBorder(color1, color2));
    }

    public void actionPerformed(ActionEvent ev) {
        Object obj = ev.getSource();
        if (obj == currencyCombo) {
            populateBank();
            if (bankList != null && bankList.size() > 0) {
                bankCombo.setSelectedIndex(0);
            }
            bankCombo.updateUI();
            //SwingUtilities.updateComponentTreeUI(bankCombo);
        } else if (obj == mainCombo) {
            selectPanel();
            /*if (((TWComboItem)mainCombo.getSelectedItem()).getId().equals("" + TradeMeta.TRANSFER_CHEQUE)) {
                card1.show(panel3, "Cheque");
                cardType = 0;
            } else if (((TWComboItem)mainCombo.getSelectedItem()).getId().equals("" + TradeMeta.TRANSFER_BANK)) {
                card1.show(panel3, "Bank");
                cardType = 1;
            } else {
                card1.show(panel3, "Cash");
                cardType = 2;
            }*/
            /*if (mainCombo.getSelectedItem().equals(Language.getString("CHEQUE"))) {
                card1.show(panel3, "Cheque");
                //isCardOneSelected=true;
                cardType = 0;
            } else if (mainCombo.getSelectedItem().equals(Language.getString("BANK_TRANSFER"))) {
                card1.show(panel3, "Bank");
                //isCardOneSelected =false;
                cardType = 1;
            } else {
                card1.show(panel3, "Cash");
                cardType = 2;
            }*/
        } else if (obj == okBtn) {
            int type = 0;
            if (!validateInputs()) {
                return;
            }
            int curInt = currencyCombo.getSelectedIndex();   //CurrencyStore.getSharedInstance().getC
            String curr = currencyList.get(curInt).getId();
            String amount = amountField.getText();
            String reference = refNoField.getText();
            String portfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
            selectedPortfolo = portfolio;
            String notes = "";
            boolean isServerResponsed = false;
            if (notesField.getText().equals("")) {
            } else {
                notes = notesField.getText();
            }
            if (cardType == 0) {
                type = 0;
                java.util.Calendar chequeDate = chequeCombo.getDate();
                java.util.Calendar depositeDate = depositeCombo.getDate();
                String chequeNo = chequeNumberField.getText();
                String bank = bankField.getText();
                //String Chequereference = ChequeRefField.getText();
                if (bank.equals("") || chequeNo.equals("") || (depositeDate == null) || (chequeDate == null) || amount.equals("") || reference.equals("")) {
                    errorMessage();
                } else {
                    if (confirmMessage()) {
                        isServerResponsed = TradeMethods.getSharedInstance().fundTransferRequest(type, curr, bank, null, chequeNo, chequeDate, amount,
                                depositeDate, reference, notes, null, null, System.currentTimeMillis(), portfolio, null);
                        if (isServerResponsed) {
                            Theme.unRegisterComponent(this);
                            this.dispose();
                        }
                    }
                }
            } else if (cardType == 1) {
                type = 1;
                String originatingBank = originatingBankField.getText();
                //String reference = referenceField.getText();
                String bankAddress = bankAddressField.getText();
                if (bankAddress.equals("") || reference.equals("") || originatingBank.equals("") || amount.equals("")) {
                    errorMessage();
                } else {
                    if (confirmMessage()) {
                        isServerResponsed = TradeMethods.getSharedInstance().fundTransferRequest(type, curr, originatingBank, bankAddress, null, null,
                                amount, null, reference, notes, null, null, System.currentTimeMillis(), portfolio, null);
                        if (isServerResponsed) {
                            Theme.unRegisterComponent(this);
                            this.dispose();
                        }
                    }
                }
            } else {
                type = 2;
                //String reference = refNoField.getText();
                String branch = branchField.getText();
                java.util.Calendar depositeDate = depDateCombo.getDate();
                int bankInt = bankCombo.getSelectedIndex();
                String bank = bankList.get(bankInt).getId().split(";")[0];
                String accNo = bankList.get(bankInt).getId().split(";")[1];
                if (bank == null || reference.equals("") || amount.equals("") || (depositeDate == null))   //|| branch.equals("")
                {
                    errorMessage();
                } else {
                    if (confirmMessage()) {
                        isServerResponsed = TradeMethods.getSharedInstance().fundTransferRequest(type, curr, null, null, null, null,
                                amount, depositeDate, reference, notes, bank, branch, System.currentTimeMillis(), portfolio, accNo);
                        if (isServerResponsed) {
                            Theme.unRegisterComponent(this);
                            this.dispose();
                        }
                    }
                }
            }
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "DepositNotification");
        } else {
            chequeCombo.hidePopup();
            depositeCombo.hidePopup();
            depDateCombo.hidePopup();
            Theme.unRegisterComponent(this);
            this.dispose();
        }
    }

    private boolean validateInputs() {
        try {
//			int curInt = currencyCombo.getSelectedIndex();   //CurrencyStore.getSharedInstance().getC
//            String curr = currencyList.get(curInt).getId();
            String amount = amountField.getText();
            String reference = refNoField.getText();
//            String portfolio = ((TWComboItem)cmbPortfolio.getSelectedItem()).getId();
//            String notes = "";
            if (cardType == 0) {
                java.util.Calendar chequeDate = chequeCombo.getDate();
                java.util.Calendar depositeDate = depositeCombo.getDate();
                String chequeNo = chequeNumberField.getText();
                String bank = bankField.getText();
                if (bank.equals("") || chequeNo.equals("") || (depositeDate == null) || (chequeDate == null) || amount.equals("") || reference.equals("")) {
                    errorMessage();
                    return false;
                }
                if (Integer.parseInt(amount) <= 0) {
                    new ShowMessage(Language.getString("MSG_INVALID_AMOUNT"), "E");
                    return false;
                }
            } else if (cardType == 1) {
                String originatingBank = originatingBankField.getText();
                String bankAddress = bankAddressField.getText();
                if (bankAddress.equals("") || reference.equals("") || originatingBank.equals("") || amount.equals("")) {
                    errorMessage();
                    return false;
                }
                if (Integer.parseInt(amount) <= 0) {
                    new ShowMessage(Language.getString("MSG_INVALID_AMOUNT"), "E");
                    return false;
                }
            } else {
                String branch = branchField.getText();
                java.util.Calendar depositeDate = depDateCombo.getDate();
                int bankInt = bankCombo.getSelectedIndex();
                String bank = bankList.get(bankInt).getId();
//                if (bank == null || reference.equals("") || branch.equals("") || amount.equals("") || (depositeDate == null))
                if (bank == null || reference.equals("") || amount.equals("") || (depositeDate == null)) {
                    errorMessage();
                    return false;
                }
                if (Integer.parseInt(amount) <= 0) {
                    new ShowMessage(Language.getString("MSG_INVALID_AMOUNT"), "E");
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            new ShowMessage(Language.getString("MSG_INVALID_VALUE"), "E");
            return false;
        }
    }

    private void selectPanel() {
        if (((TWComboItem) mainCombo.getSelectedItem()).getId().equals("" + TradeMeta.TRANSFER_CHEQUE)) {
            card1.show(panel3, "Cheque");
            cardType = 0;
        } else if (((TWComboItem) mainCombo.getSelectedItem()).getId().equals("" + TradeMeta.TRANSFER_BANK)) {
            card1.show(panel3, "Bank");
            cardType = 1;
        } else {
            card1.show(panel3, "Cash");
            cardType = 2;
        }
    }

    private boolean confirmMessage() {
        String curr = currencyCombo.getSelectedItem().toString();
        String amount = amountField.getText();
        String str = Language.getString("MSG_TRANSFER_CONFIRMATION");
        str = str.replaceFirst("\\[AMOUNT\\]", amount);
        str = str.replaceFirst("\\[CUR\\]", curr);
        int i = SharedMethods.showConfirmMessage(str, JOptionPane.INFORMATION_MESSAGE);
        if ((i == JOptionPane.OK_OPTION)) {
            return true;
        } else {
            return false;
        }
    }

    private void populatePortfolios() {
        try {
            if (TradingShared.getTrader() != null) {
                portfolioList.clear();
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());
                    portfolioList.add(item);
                    item = null;
                    record = null;
                }
                cmbPortfolio.updateUI();
                cmbPortfolio.setSelectedIndex(0);
                selectedPortfolo = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void errorMessage() {
        SharedMethods.showMessage(Language.getString("MSG_FILL_REQUIRED_ENTRIES"), JOptionPane.ERROR_MESSAGE);
    }

    public void accountDataChanged(String accountID) {

    }

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios();
    }

    public void tradeServerConnected() {
        cancelBtn.setEnabled(true);
        okBtn.setEnabled(true);
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        cancelBtn.setEnabled(false);
        okBtn.setEnabled(false);
    }

    /* Printable method */
    public void internalFrameClosed(InternalFrameEvent e) {
        chequeCombo.hidePopup();
        depositeCombo.hidePopup();
        depDateCombo.hidePopup();
    }

}
