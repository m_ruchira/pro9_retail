package com.isi.csvr.trading.ui;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.shared.DownArrow;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.symbolfilter.FunctionTableRenderer;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: admin Date: 28-Sep-2007 Time: 09:32:58 To change this template use File | Settings |
 * File Templates.
 */
public class ConditionPanelComponent extends JPanel implements ActionListener, Themeable, MouseListener {
    public String componentType = "";
    public JButton addConditionBtn;
    public SmartTable conditionTable;
    public DynamicArray conditionArray;
    public String currentSymbol = "";
    public String currentExchange = "";
    TransactionDialog parent;
    JLabel txtConditionExpiry;
    JButton btnShowConditionDays;
    JPanel cmbConditionExpiry;
    JRadioButton rb1;
    JRadioButton rb2;
    JPanel expanel;
    JPanel lowerPanel;
    JPanel upperPanel;
    //    private ConditionStore conditionStore;
    private ConditionBuilderDialog conditionBuilderDialog;

    public ConditionPanelComponent(TransactionDialog sParent) {
        this.parent = sParent;
        conditionArray = new DynamicArray();
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "100%", "60"}));
        this.add(createUpper());
        this.add(createMiddle());
        this.add(createLower());
        conditionBuilderDialog = new ConditionBuilderDialog(this, currentSymbol, currentExchange);
        this.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        Theme.registerComponent(this);
    }

    public JPanel createUpper() {
        upperPanel = new JPanel();
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%", "130"}, new String[]{"100%"}));
        addConditionBtn = new JButton();
        addConditionBtn = new JButton(Language.getString("ADD_FUNCTION_UNDERLINE"));
        addConditionBtn.setBorder(BorderFactory.createEmptyBorder());
        addConditionBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
        addConditionBtn.setContentAreaFilled(false);
        addConditionBtn.setActionCommand("ADD");
        addConditionBtn.addActionListener(this);
        addConditionBtn.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/condition_add.gif"));
        upperPanel.add(new JLabel());
        upperPanel.add(addConditionBtn);
        upperPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        return upperPanel;
    }

    public JPanel createMiddle() {
        JPanel middlePanel = new JPanel();
        middlePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));
        conditionTable = new SmartTable();
//        conditionStore = ConditionStore.getSharedInstance();
        conditionTable.setModel(new ConditionTableModel(conditionTable, conditionArray, Language.getList("CONDITION_BUILDER_COLUMNS")));
        JScrollPane pane = new JScrollPane(conditionTable);
        pane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.add(pane, BorderLayout.CENTER);
//        setSize(650, 310);
        //setResizable(false);
        conditionTable.setRowHeight(15);
        int[] rendIDs = {3, 4, 2, 2, 2, 1};
        conditionTable.setDefaultRenderer(String.class, new FunctionTableRenderer(rendIDs));
        conditionTable.setDefaultRenderer(Number.class, new FunctionTableRenderer(rendIDs));
        conditionTable.getTableHeader().setResizingAllowed(false);
        conditionTable.getTableHeader().setReorderingAllowed(false);
        conditionTable.setRowSelectionAllowed(false);
        conditionTable.addMouseListener(this);
        conditionTable.setBorder(BorderFactory.createEmptyBorder());
        pane.setBorder(BorderFactory.createEmptyBorder());
        conditionTable.getColumnModel().getColumn(0).setPreferredWidth(20);
        conditionTable.getColumnModel().getColumn(1).setPreferredWidth(20);
        conditionTable.getColumnModel().getColumn(2).setPreferredWidth(230); //80
        conditionTable.getColumnModel().getColumn(3).setPreferredWidth(160);
        conditionTable.getColumnModel().getColumn(4).setPreferredWidth(140); //160
        conditionTable.getColumnModel().getColumn(5).setPreferredWidth(120);
//        conditionTable.getColumnModel().getColumn(6).setPreferredWidth(80); //100
        middlePanel.add(pane);
        return middlePanel;
    }

    public JPanel createLower() {
        lowerPanel = new JPanel();
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"150", "100%"}, new String[]{"17", "15"}));
//        lowerPanel.setBorder (BorderFactory.createEtchedBorder());      //BorderFactory.createTitledBorder (BorderFactory.createEtchedBorder(),Language.getString("MATCHING_CONDITION"))
        lowerPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), Language.getString("MATCHING_CONDITION")));
        ButtonGroup buttonGroup = new ButtonGroup();
        rb1 = new JRadioButton(Language.getString("MATCH_ALL"));
        rb2 = new JRadioButton(Language.getString("MATCH_ANY"));
        rb1.setSelected(true);
        buttonGroup.add(rb1);
        buttonGroup.add(rb2);
        lowerPanel.add(rb1);
        lowerPanel.add(createExpireDatePanel());
        lowerPanel.add(rb2);
        lowerPanel.add(new JLabel());
        rb1.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        rb2.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        lowerPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        return lowerPanel;
    }

    public JPanel createExpireDatePanel() {
        expanel = new JPanel();
        expanel.setLayout(new FlexGridLayout(new String[]{"100%", "60", "100"}, new String[]{"17"}));
        expanel.add(new JLabel());
        expanel.add(new JLabel(Language.getString("EXPIRY")));
        expanel.add(getConditionExpiryCombo());
        expanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        return expanel;
    }

    private JPanel getConditionExpiryCombo() {
        String[] widths = {"100%", "16"};
        String[] heights = {"15"};
        FlexGridLayout expiryLayout = new FlexGridLayout(widths, heights, 0, 0);
        cmbConditionExpiry = new JPanel(expiryLayout);
        cmbConditionExpiry.setBorder(BorderFactory.createEtchedBorder());
        //cmbConditionExpiry.setPreferredSize(new Dimension(145, 20));
        txtConditionExpiry = new JLabel();
        //txtConditionExpiry.setPreferredSize(new Dimension(80, 16));
        txtConditionExpiry.setOpaque(false);
        cmbConditionExpiry.add(txtConditionExpiry);
        btnShowConditionDays = new JButton(new DownArrow());
        btnShowConditionDays.setActionCommand("COND_EXPIRY");
        btnShowConditionDays.setBorder(BorderFactory.createEmptyBorder());
        btnShowConditionDays.addActionListener(this);
        btnShowConditionDays.setContentAreaFilled(false);
        btnShowConditionDays.setOpaque(false);
        btnShowConditionDays.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        //btnShowDays.setPreferredSize(new Dimension(16, 16));
        cmbConditionExpiry.add(btnShowConditionDays);
        cmbConditionExpiry.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        setConditionExpity(null);
        return cmbConditionExpiry;
    }

    private void setConditionExpity(String command) {
        try {
            int days;
            if (command != null) {
                days = Integer.parseInt(command.split("\\|")[1]);
            } else {
                days = 1;
            }
            Date date = TradeMethods.getDateFromDays(days);
            String goodTillStr = TradingShared.formatGoodTill(date.getTime());
            txtConditionExpiry.setText(goodTillStr);
            System.out.println(date.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applyColors() {
        upperPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        lowerPanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        rb1.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        rb2.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        expanel.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        btnShowConditionDays.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        cmbConditionExpiry.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
    }

    public void applyTheme() {
        addConditionBtn.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/function_add.gif"));
        applyColors();
        SwingUtilities.updateComponentTreeUI(this);
//        SwingUtilities.updateComponentTreeUI(table);
//        table.getTableHeader().setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
//        table.getTableHeader().setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
//        table.updateUI();
//        SwingUtilities.updateComponentTreeUI(functionBuilderDialog);
        //SwingUtilities.updateComponentTreeUI(dialog);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("ADD")) {
            conditionBuilderDialog.addNewFunction();
        } else if (e.getActionCommand().equals("COND_EXPIRY")) {
            testConditions();
//            getCondionSet();
            showConditionExpity();
        } else if (e.getActionCommand().startsWith("COND_EXPIRY_DAYS")) {
            setConditionExpity(e.getActionCommand());
        }
    }

    private void showConditionExpity() {
        TWMenuItem item;
        Point location = new Point(0, 0);
        JPopupMenu popup = new JPopupMenu();
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_0_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|0");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_1_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|1");               // changed fro 0 to 1
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_2_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|2");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_3_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|3");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_4_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|4");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_5_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|5");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_6_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|6");
        item.addActionListener(this);
        popup.add(item);
        item = new TWMenuItem(Language.getString("CONDITION_EXPIRY_7_DAY"));
        item.setActionCommand("COND_EXPIRY_DAYS|7");
        item.addActionListener(this);
        popup.add(item);

        GUISettings.applyOrientation(popup);
        popup.show(cmbConditionExpiry, 0, cmbConditionExpiry.getHeight());
    }

    public void addToConditionArray(ConditionObject co) {
        try {
            conditionArray.add(co);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    public Vector getCondionSet() {
        Vector conditionVector = new Vector();
        for (int i = 0; i < conditionArray.size(); i++) {
            ConditionObject cob = (ConditionObject) conditionArray.get(i);
            Object[] conditionData = new Object[4];
            if (cob.getType() == 1) {    //search type
                conditionData[0] = cob.getSymbol();
            } else if (cob.getType() == 2) {   //index type
                conditionData[0] = cob.getSymbol();
            } else if (cob.getType() == 3) {  //full market
//               conditionData[0]=Language.getString("FULL_MARKET");
                conditionData[0] = cob.getSymbol();
            } else if (cob.getType() == 4) {  //symbol type
//               conditionData[0]=Language.getString("CURRENT_SYMBOL");
                conditionData[0] = cob.getSymbol();
            }
            //Setting the field
            if (cob.getField().equals(Language.getString("LAST_TRADE"))) {
                conditionData[1] = 1;
            } else if (cob.getField().equals(Language.getString("CASH_IN"))) {
                conditionData[1] = 2;
            } else if (cob.getField().equals(Language.getString("CASH_OUT"))) {
                conditionData[1] = 3;
            } else if (cob.getField().equals(Language.getString("CONDITION_MIN"))) {
                conditionData[1] = 4;
            } else if (cob.getField().equals(Language.getString("CONDITION_MAX"))) {
                conditionData[1] = 5;
            } else if (cob.getField().equals(Language.getString("CASH_DQ_VOLUME"))) {
                conditionData[1] = 6;
            } else if (cob.getField().equals(Language.getString("CASH_DQ_TURNOVER"))) {
                conditionData[1] = 7;
            } else if (cob.getField().equals(Language.getString("HEAT_MAP_CRITERIA_NUM_TRADES"))) {
                conditionData[1] = 8;
            }
            //Setting the Condition
            if (cob.getCondition().equals(Language.getString("EQUAL"))) {
                conditionData[2] = 1;
            } else if (cob.getCondition().equals(Language.getString("NOT_EQUAL"))) {
                conditionData[2] = 2;
            } else if (cob.getCondition().equals(Language.getString("LESS_THAN_OR_EQUAL"))) {
                conditionData[2] = 3;
            } else if (cob.getCondition().equals(Language.getString("GREATER_THAN_OR_EQUAL"))) {
                conditionData[2] = 4;
            }
            //Setting the Value
            conditionData[3] = cob.getValue();

            conditionVector.add(conditionData);
            conditionData = null;
        }
        return conditionVector;
    }

    public int getMatchingCondition() {
        int matchCondition = 0;
        if (rb1.isSelected()) {
            matchCondition = 1;
        } else if (rb2.isSelected()) {
            matchCondition = 2;
        }
        return matchCondition;
    }

    public String getConditionExpiaryDate() {
        return txtConditionExpiry.getText();
    }

    //    public void setCondition(Object[] arr){
//        if(arr.length>1){
//            ConditionObject cob=new ConditionObject();
//            if(((String)arr[0]).equals(Language.getString("CURRENT_SYMBOL"))){
//                cob.setSymbol(Language.getString("CURRENT_SYMBOL"));
//
//            }else if(((String)arr[0]).equals(Language.getString("FULL_MARKET"))){
//                cob.setSymbol(Language.getString("CURRENT_SYMBOL"));
//            }else{
//                cob.setSymbol((String)arr[0]);
//            }
//
//        }
//
//    }
    public void testConditions() {
        Vector vec = getCondionSet();
        Enumeration e = vec.elements();
        while (e.hasMoreElements()) {
            Object[] ar = (Object[]) e.nextElement();
            System.out.println("symbol " + ar[0]);
            System.out.println("field " + ar[1]);
            System.out.println("condition " + ar[2]);
            System.out.println("value " + ar[3]);
            System.out.println("+++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("Matching condition : " + getMatchingCondition());
        System.out.println("Condition expiary  : " + getConditionExpiaryDate());
    }

    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        int row = conditionTable.rowAtPoint(e.getPoint());
        int col = conditionTable.columnAtPoint(e.getPoint());
        if (col == 0) {
            ConditionObject conditionObject = (ConditionObject) conditionArray.get(row);
            if (conditionObject != null)
                removeItem(row);
        } else if (col == 1) {
            conditionBuilderDialog.editFunction(row);
        }
        conditionTable.updateUI();
    }

    private void removeItem(int index) {
        ShowMessage message = new ShowMessage(Language.getString("MSG_REMOVE_FUNCTION"), "W", true);
        int result = message.getReturnValue();
        if (result == 1) {
            conditionArray.remove(index);
        }
    }

    public ConditionObject getCondition(int index) {
        return (ConditionObject) conditionArray.get(index);
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCurrenSymbol() {
        return parent.getCurrentSymbol();
    }

    public String getCurrentExchange() {
        return parent.getCurrentExchange();
    }
}
