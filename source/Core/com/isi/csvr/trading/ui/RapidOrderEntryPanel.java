package com.isi.csvr.trading.ui;

import bsh.Interpreter;
import com.isi.csvr.Client;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.TransactionDialogInterface;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.TransactRecord;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.util.Decompress;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 7, 2005
 * Time: 9:46:22 AM
 */
public class RapidOrderEntryPanel extends JPanel
        implements TraderProfileDataListener, Themeable,
        ActionListener, TradingConnectionListener, FocusListener, KeyListener, TransactionDialogInterface {

    TitledBorder border;
    private TWComboBox cmbPortfolios;
    private TWComboBox cmbBookKeepers;
    private ArrayList<TWComboItem> portfolioList;
    private ArrayList<TWComboItem> orderTypeList;
    private ArrayList<TWComboItem> bookKeeperList;
    private String exchange;
    private String symbolCode;
    private int instrument = -1;
    private TWComboBox cmbTypes;
    private TWTextField txtSymbol;
    private TWTextField txtQuantity;
    private TWTextField txtPrice;
    private TWButton btnBuy;
    private TWButton btnSell;
    private TWButton btnClear;
    private TWButton btnClose;
    private JLabel lblAccount;
    private JLabel lblBookKeeper;
    private JLabel lblTplusHolding;
    private TWTextField txtTplusHolding;
    private JCheckBox chkSellTPlus;
    private JLabel lblSellTplus;
    private JLabel lblSymbol;
    private JLabel lblQuantity;
    private JLabel lblType;
    private JLabel lblPrice;
    private int side = 0;
    private String orderValue = null;
    private String commission = null;
    private String netValue = null;
    private String initMargin = null;
    private String maintMargin = null;
    private boolean keyTyped = false;
    private String selectedPortfolio;

    private TWPanel pnlBookKeeper;
    private TWPanel pnllblTPlusSymbols;
    private TWPanel pnltxtTPlusSymbols;


    public RapidOrderEntryPanel() {
        createUI();
    }

    public void createUI() {
//        String[] widths = {"100", "100", "100", "120", "100", "5", "60", "60", "15", "75", "75"};
//        String[] widths = {"100","100", "100", "100", "120", "100","80","60", "5", "60", "60", "15", "75", "75"};
//        String[] heights = {"17", "20"};
//        setLayout(new FlexGridLayout(widths, heights, 7, 2));
        setOpaque(true);

        border = new TitledBorder(Language.getString("RAPID_ORDEERS"));
        setBorder(border);

        lblAccount = new JLabel(Language.getString("ACCOUNT"));
//        add(lblAccount);
        lblBookKeeper = new JLabel(Language.getString("BOOKKEEPER"));
//        add(lblBookKeeper);
        lblSymbol = new JLabel(Language.getString("SYMBOL"));
//        add(lblSymbol);
        lblQuantity = new JLabel(Language.getString("QUANTITY"));
//        add(lblQuantity);
        lblType = new JLabel(Language.getString("TYPE"));
//        add(lblType);
        lblPrice = new JLabel(Language.getString("PRICE"));
//        add(lblPrice);
        lblTplusHolding = new JLabel(Language.getString("HOLDINGS_T+0"));
//        add(lblTplusHolding);
        txtTplusHolding = new TWTextField();
        txtTplusHolding.setEnabled(false);
//        add(txtTplusHolding);
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());

        portfolioList = new ArrayList<TWComboItem>();
        TWComboModel portfolioModel = new TWComboModel(portfolioList);
        cmbPortfolios = new TWComboBox(portfolioModel);
        cmbPortfolios.addActionListener(this);
        cmbPortfolios.setEnabled(false);
//        cmbPortfolios.set
        bookKeeperList = new ArrayList<TWComboItem>();
        TWComboModel bookKeeperModel = new TWComboModel(bookKeeperList);
        cmbBookKeepers = new TWComboBox(bookKeeperModel);
        cmbBookKeepers.setEnabled(false);

//        add(cmbPortfolios);
//        add(cmbBookKeepers);
        txtSymbol = new TWTextField();
        // txtSymbol.setEnabled(false);
        txtSymbol.addKeyListener(this);
        txtSymbol.addFocusListener(this);
        txtSymbol.setDocument(new ValueFormatter(ValueFormatter.UPPERCASE, 20L));
//        add(txtSymbol);
        txtQuantity = new TWTextField();
        txtQuantity.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        txtQuantity.setEnabled(false);
//        add(txtQuantity);

        orderTypeList = new ArrayList<TWComboItem>();
        TWComboModel orderTypeModel = new TWComboModel(orderTypeList);
        cmbTypes = new TWComboBox(orderTypeModel);
        cmbTypes.setEnabled(false);
        cmbTypes.addActionListener(this);
        cmbTypes.setActionCommand("TYPES");
//        add(cmbTypes);
        txtPrice = new TWTextField();
        txtPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtPrice.setEnabled(false);
//        add(txtPrice);
        lblSellTplus = new JLabel(Language.getString("SELL_T+0"));
//        add(lblSellTplus);
        chkSellTPlus = new JCheckBox();
        chkSellTPlus.addActionListener(this);
//        add(chkSellTPlus);
//        add(new JLabel());

        btnBuy = new TWButton(Language.getString("BUY"));
        btnBuy.setGradientDark("BOARD_TABLE_CELL_BID_BGCOLOR1");
        btnBuy.setGradientLight("BOARD_TABLE_CELL_BID_BGCOLOR2");
        btnBuy.setActionCommand("BUY");
        btnBuy.addActionListener(this);
        btnBuy.setEnabled(false);
//        add(btnBuy);


        btnSell = new TWButton(Language.getString("SELL"));
        btnSell.setGradientDark("BOARD_TABLE_CELL_ASK_BGCOLOR1");
        btnSell.setGradientLight("BOARD_TABLE_CELL_ASK_BGCOLOR2");
        btnSell.setActionCommand("SELL");
        btnSell.addActionListener(this);
        btnSell.setEnabled(false);
//        add(btnSell);

//        add(new JLabel());

        btnClear = new TWButton(Language.getString("CLEAR"));
        btnClear.setActionCommand("CLEAR");
        btnClear.addActionListener(this);
        btnClear.setEnabled(false);
//        add(btnClear);

        btnClose = new TWButton(Language.getString("CLOSE"));
        btnClose.setActionCommand("CLOSE");
        btnClose.addActionListener(this);
//        add(btnClose);

        pnlBookKeeper = new TWPanel(100, lblBookKeeper, cmbBookKeepers);
        pnllblTPlusSymbols = new TWPanel(80, lblTplusHolding, lblSellTplus, false);
        pnltxtTPlusSymbols = new TWPanel(60, txtTplusHolding, chkSellTPlus, false);
        setLayout(new FlowLayout(FlowLayout.LEADING));
        add(new TWPanel(100, lblAccount, cmbPortfolios));
        add(pnlBookKeeper);
        add(new TWPanel(100, lblSymbol, txtSymbol));
        add(new TWPanel(100, lblQuantity, txtQuantity));
        add(new TWPanel(120, lblType, cmbTypes));
        add(new TWPanel(100, lblPrice, txtPrice));
        add(pnllblTPlusSymbols);
        add(pnltxtTPlusSymbols);
        add(new TWPanel(5, new JLabel(), new JLabel(), false));
        add(new TWPanel(60, new JLabel(), btnBuy));
        add(new TWPanel(60, new JLabel(), btnSell));
        add(new TWPanel(15, new JLabel(), new JLabel(), false));
        add(new TWPanel(75, new JLabel(), btnClear));
        add(new TWPanel(75, new JLabel(), btnClose));

        Theme.registerComponent(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    private synchronized void populatePortfolios() {
//        cmbPortfolios.removeItemListener(this);
        try {
            cmbPortfolios.removeAllItems();
            if (TradingShared.getTrader() != null) {
                portfolioList.clear();
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());
                    portfolioList.add(item);
                    item = null;
                    record = null;
                }
                cmbPortfolios.updateUI();
                if (portfolioList.size() > 0) {
                    cmbPortfolios.setSelectedIndex(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        cmbPortfolios.addItemListener(this);
    }

    private void populateBookKeepers() {
        try {
            TradeMethods.getSharedInstance().populateBookKeepers(((TWComboItem) cmbPortfolios.getSelectedItem()).getId(), bookKeeperList);
            cmbBookKeepers.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void selectPportfolio(String portfolioID) {
        if (portfolioList != null && !portfolioList.isEmpty()) {
            int index = 0;
            if (portfolioID != null && !portfolioID.isEmpty()) {
                for (int i = 0; i < portfolioList.size(); i++) {
                    TWComboItem item = portfolioList.get(i);
                    if (item.getId().equals(portfolioID)) {
                        index = i;
                        break;
                    }
                }
                cmbPortfolios.setSelectedIndex(index);
            } else {
                cmbPortfolios.setSelectedIndex(0);
            }


        }

    }

    /*private synchronized void populateExchanges() {
        try {
            exchangeList.clear();
            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = exchanges.nextElement();
                exchangeList.add(new TWComboItem(exchange.getSymbol(), exchange.getSymbol()));
                exchange = null;
            }
            exchanges = null;
            cmbExchanegs.updateUI();
            cmbExchanegs.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void sendNewOrder(int side) {
        //String symbol = txtSymbol.getText();
        String portfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
        //  System.out.println("Exchange symCode Instrument:  "+ exchange + "  "+ symbolCode + " " + instrument);
        if (TradingShared.isReadyForTrading()) {
            if (TradeMethods.isSymbolAllowedForProtfolio(portfolio, exchange, true)) {
                if (TradingShared.isShariaTestPassed(exchange, symbolCode, instrument)) {
                    char type = orderTypeList.get(cmbTypes.getSelectedIndex()).getId().charAt(0);
                    if (isValidInputs(type) && validateRules(side)) {


                        boolean allOrNone = false;
                        int quantity = Integer.parseInt(txtQuantity.getText());

                        /*if (cmbTypes.getSelectedItem().equals(Language.getString("ORDER_TYPE_MARKET"))) {
                            type = TradeMeta.ORDER_TYPE_MARKET;
                        } else {
                            type = TradeMeta.ORDER_TYPE_LIMIT;
                        }*/


                        double price = 0;
                        try {
                            price = Double.parseDouble(txtPrice.getText());
                        } catch (Exception e) {
                            price = 0; // for market orders
                        }

                        try {
                            Rule rule = RuleManager.getSharedInstance().getRule("AON_SELECT", exchange, "NEW_ORDER");
                            if (rule != null) {
                                Interpreter interpreter = new Interpreter();
                                interpreter.set("instrumentType", instrument);
                                interpreter.set("quantity", quantity);
                                interpreter.set("price", Math.round(price * 1000) / (double) 1000);
                                int result = (Integer) interpreter.eval(rule.getRule());

                                allOrNone = (result == 1); // bit 3
//                                 allOrNone = (result & 4) == 4; // bit 3

                                /* Old System. Removed due to limitations 6 mar 2006 Uditha
                                    result format
                                    AON Selected   ->  result
                                       0           ->    0
                                       0           ->    1
                                       1           ->    2
                                       1           ->    3
                                *

                                switch (result){
                                    case 0:
                                    case 1:
                                        allOrNone = false;
                                        break;
                                    case 2:
                                    case 3:
                                        allOrNone = true;
                                        break;
                                }*/
                            }
                        } catch (Exception evalError) {
                            evalError.printStackTrace();
                        }

                        int currencyDecimals = 2;
                        int tPlus = 2;
                        String bookKeeper = "";
                        try {
                            bookKeeper = ((TWComboItem) cmbBookKeepers.getSelectedItem()).getId();
                            if (chkSellTPlus.isSelected()) {
                                tPlus = 0;
                            }
                        } catch (Exception e) {
                            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            tPlus = 2;
                        }
                        try {
                            String currency = TradingShared.getTrader().findAccountByPortfolio(portfolio).getCurrency();
                            currencyDecimals = SharedMethods.getCurrencyDecimalPlaces(currency);
                            bookKeeper = ((TradingPortfolioRecord) TradingShared.getTrader().getPortfolio(portfolio)).getDefaultBookKeeper();
                        } catch (Exception e) {
                        }
                        calculateOrderValue();
                        calculateMarginValues();
                        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, instrument);
                        boolean validationSucess = TradeMethods.ruleBasedValidationSucess(false, exchange, type, //TradeMeta.ORDER_TYPE_LIMIT,
                                price, quantity, -1, 0, stock.getMinPrice(), stock.getMaxPrice(), (short) 0, side, symbolCode, 0, stock, true);
                        if (!validationSucess) {
                            return;
                        }

                        int tifType = TradingShared.getDefaultTifType(type);
                        String tiffStr = TradingShared.getTiffString(tifType, null);
                        int i = TradeMethods.getNewOrderConfirmMsg(symbolCode, exchange, instrument, price, currencyDecimals, 0, 0,
                                Long.parseLong(txtQuantity.getText()), null, null, 0f, type, side, side, TradeMeta.CONDITION_TYPE_NONE, "", 0, false, tiffStr, portfolio,
                                orderValue, commission, netValue, initMargin, maintMargin, null, 0, null, false);

                        /*ShowMessage oMessage = new ShowMessage(TradeMethods.getConfirmMessage(symbolCode ,exchange, instrument, price, currencyDecimals, 0, 0,
                             Long.parseLong(txtQuantity.getText()),null,null,0f, type, side, side, TradeMeta.CONDITION_TYPE_NONE,"",0,false), "I", true);
                     int i = oMessage.getReturnValue();*/
                        if (i == JOptionPane.OK_OPTION) {
                            // Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, instrument);

//                            boolean validationSucess = TradeMethods.ruleBasedValidationSucess(false, exchange, type, //TradeMeta.ORDER_TYPE_LIMIT,
//                                    price, quantity, -1, 0, stock.getMinPrice(), stock.getMaxPrice(), (short)0, side, symbolCode, 0, stock, true);
                            //  if (validationSucess) {
                            boolean sucess = TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, portfolio, exchange, symbolCode, side, type, price,
                                    0, "*", Long.parseLong(txtQuantity.getText()), tifType, 0, -1, 0, allOrNone, stock.getStrikePrice(),
                                    TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(), -1,
                                    stock.getOptionBaseSymbol(), -1, null, null, null, -1, null, null, null, 0d, bookKeeper, 0);
                            if (sucess) { //Bug ID <#0017> inroduced the if condition to avaoid clearing if authentication failed
                                clear();
                                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "RapidOrder");
                            }
                            // }
                            stock = null;
                        }
                    }
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }

    }

    private void calculateMarginValues() {
        try {
            String portfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
//            TWDecimalFormat currencyFormat = null;
            double initMargin = 0;
            boolean isDayOrder = false;
            /*try {
                Account account = TradingShared.getTrader().findAccountByPortfolio(portfolio);
                String currency = account.getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
                initMargin = TradeMethods.getMarginForSymbol(exchange,  symbolCode, instrument,  portfolio, isDayOrder, account.getMarginLimit(), account.getDayMarginLimit());
            } catch (Exception e) {

            }*/
//            this.initMargin = currencyFormat.format(initMargin);
//            this.maintMargin = currencyFormat.format(initMargin);

            long tradeQty = Integer.parseInt(txtQuantity.getText());
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, instrument);
            /*if (stock.getLotSize() >0) {
                tradeQty  = tradeQty * stock.getLotSize();
            }*/
            Account account = TradingShared.getTrader().findAccountByPortfolio(portfolio);
            String currency = account.getCurrency();
            TWDecimalFormat currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol(), instrument));
            double tradePrice = 0;
            long openBuyCount = 0;
            long openSellCount = 0;
            long prndingBuy = 0;
            long prndingSell = 0;
            double currencyFactor = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(stock.getCurrencyCode());
            if (futureBaseAttributes != null) {
                tradePrice = futureBaseAttributes.getMargin();
                TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecordForFutures(SharedMethods.getKey(exchange, symbolCode, instrument), portfolio);
                if (transactRecord != null) {
                    openBuyCount = transactRecord.getOpenBuyCount();
                    openSellCount = transactRecord.getOpenSellCount();
                    prndingBuy = transactRecord.getPendingBuy();
                    prndingSell = transactRecord.getPendingSell();
                }
                if (side == TradeMeta.BUY) {
                    if ((openSellCount - prndingBuy) >= tradeQty) {
                        this.initMargin = "0";
                        this.maintMargin = "0";
                    } else {
                        long marginContracts = 0;
                        if (prndingBuy >= openSellCount) {
                            marginContracts = tradeQty;
                        } else {
                            marginContracts = tradeQty - (openSellCount - prndingBuy);
                        }
                        this.initMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                        this.maintMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                    }
                } else {
                    if ((openBuyCount - prndingSell) >= tradeQty) {
                        this.initMargin = "0";
                        this.maintMargin = "0";
                    } else {
                        long marginContracts = 0;
                        if (prndingSell >= openBuyCount) {
                            marginContracts = tradeQty;
                        } else {
                            marginContracts = tradeQty - (openBuyCount - prndingSell);
                        }
                        this.initMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                        this.maintMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                    }
                }
            }

        } catch (Exception e) {
        }
    }

    private void calculateOrderValue() {
        try {
            /*String portfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
            int tradeQty = Integer.parseInt(txtQuantity.getText());
            double tradePrice = 0;
            double netValue = 0;
            TWDecimalFormat currencyFormat = null;
            char type = orderTypeList.get(cmbTypes.getSelectedIndex()).getId().charAt(0);
            try {
                String currency = TradingShared.getTrader().findAccountByPortfolio(portfolio).getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            } catch (Exception e) {
            }
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, instrument);
            if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
                long openBuyCount = 0;
                long openSellCount = 0;
                long prndingBuy = 0;
                long prndingSell = 0;
                if (stock != null) {

                    FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol(), instrument));
                    long effectiveQuantity = 0;
                    tradePrice = futureBaseAttributes.getMarginPct();
                    TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecordForFutures(SharedMethods.getKey(exchange, symbolCode, instrument), portfolio);
                    if (transactRecord != null) {
                        openBuyCount = transactRecord.getOpenBuyCount();
                        openSellCount = transactRecord.getOpenSellCount();
                        prndingBuy = transactRecord.getPendingBuy();
                        prndingSell = transactRecord.getPendingSell();
                    }
                    if (side == TradeMeta.BUY) {
                        if ((openSellCount - prndingBuy) >= tradeQty) {
                            netValue = 0;
                            this.netValue = "0";
                            this.commission = "0";
                            this.orderValue = "0";
                        } else {
                            long marginContracts = 0;
                            if (prndingBuy >= openSellCount) {
                                marginContracts = tradeQty;
                            } else {
                                marginContracts = tradeQty - (openSellCount - prndingBuy);
                            }
                            double commission = 0;
                            if (marginContracts > 0) {
                                commission = SharedMethods.getCommission(exchange, 1, marginContracts, null, portfolio, SharedMethods.getKey(exchange, symbolCode, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                            }
                            netValue = (marginContracts * tradePrice) + commission;
                            this.commission = currencyFormat.format(commission);
                            this.orderValue = currencyFormat.format(marginContracts * tradePrice);
                            this.netValue = currencyFormat.format(netValue);
                        }
                    } else {
                        if ((openBuyCount - prndingSell) >= tradeQty) {
                            netValue = 0;
                            this.netValue = "0";
                            this.commission = "0";
                            this.orderValue = "0";
                        } else {
                            long marginContracts = 0;
                            if (prndingSell >= openBuyCount) {
                                marginContracts = tradeQty;
                            } else {
                                marginContracts = tradeQty - (openBuyCount - prndingSell);
                            }
                            double commission = 0;
                            if (marginContracts > 0) {
                                commission = SharedMethods.getCommission(exchange, 1, marginContracts, null, portfolio, SharedMethods.getKey(exchange, symbolCode, instrument),
                                        TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                            }
                            netValue = (marginContracts * tradePrice) + commission;
                            this.commission = currencyFormat.format(commission);
                            this.orderValue = currencyFormat.format(marginContracts * tradePrice);
                            this.netValue = currencyFormat.format(netValue);
                        }
                    }
                }
            } else {
                if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                    tradePrice = Double.parseDouble(txtPrice.getText());
                } else {
                    if (instrument == Meta.INSTRUMENT_MUTUALFUND) {
                        tradePrice = Double.parseDouble(txtPrice.getText());
                    } else {
                        if (exchange != null) {
                            //                        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol);
                            if (stock != null) {
                                if (side == TradeMeta.SELL)
                                    tradePrice = stock.getBestBidPrice();
                                else
                                    tradePrice = stock.getBestAskPrice();
                            }
                            stock = null;
                        }
                    }
                }
                double commission =0;
                if (side == TradeMeta.BUY) {
                    if(TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange,symbolCode,instrument),"0")){
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    }else{
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    }
                } else {
                    commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                }

                this.orderValue = currencyFormat.format((tradeQty * tradePrice) / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());
                if (commission == -1) { // if there is not commission rule, make the commission NA
                    this.commission = Language.getString("NA");
                    commission = 0;
                } else {
                    this.commission = currencyFormat.format(commission / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());
                }
                if (side == TradeMeta.SELL)
                    netValue = (tradeQty * tradePrice) - commission;// TradingShared.getCommission(tradeQty * tradePrice);
                else
                    netValue = (tradeQty * tradePrice) + commission;//TradingShared.getCommission(tradeQty * tradePrice);
                netValue = netValue / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor(); // adjust to base currency
                this.netValue = currencyFormat.format(netValue);
            }*/


            String portfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
            long tradeQty = Integer.parseInt(txtQuantity.getText());
            long tradeActualQty = Integer.parseInt(txtQuantity.getText());
            char type = orderTypeList.get(cmbTypes.getSelectedIndex()).getId().charAt(0);
            double tradePrice = 0;
            double netValue = 0;
            TWDecimalFormat currencyFormat = null;
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, instrument);
            if (stock.getLotSize() > 0) {
                tradeQty = tradeQty * stock.getLotSize();
            }
            try {
                String currency = TradingShared.getTrader().findAccountByPortfolio(portfolio).getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            } catch (Exception e) {
                currencyFormat = SharedMethods.getDecimalFormat(2);
            }

            if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                tradePrice = Double.parseDouble(txtPrice.getText());
            } else {
                if (instrument == Meta.INSTRUMENT_MUTUALFUND) {
                    tradePrice = Double.parseDouble(txtPrice.getText());
                } else {
                    if (exchange != null) {
                        //                        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol);
                        if (stock != null) {
                            if (side == TradeMeta.SELL)
                                tradePrice = stock.getBestBidPrice();
                            else
                                tradePrice = stock.getBestAskPrice();
                        }
                        stock = null;
                    }
                }
            }
            double commission = 0;
            if (side == TradeMeta.BUY) {
                if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbolCode, instrument), "0")) {
                    commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                } else {
                    commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                }
            } else {
                commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, instrument), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
            }

            double value = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(stock.getCurrencyCode());
            if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
                commission = -1;
            }
            this.orderValue = currencyFormat.format((tradeQty * tradePrice * value) / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());
            if (commission == -1) { // if there is not commission rule, make the commission NA
                this.commission = Language.getString("NA");
                commission = 0;
            } else {
                this.commission = currencyFormat.format(commission * value / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());//TradingShared.getCommission(tradeQty * tradePrice)));
            }
            if (side == TradeMeta.SELL)
                netValue = ((tradeQty * tradePrice) - commission) * value;// TradingShared.getCommission(tradeQty * tradePrice);
            else
                netValue = ((tradeQty * tradePrice) + commission) * value;//TradingShared.getCommission(tradeQty * tradePrice);
            netValue = netValue / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor(); // adjust to base currency
            this.netValue = currencyFormat.format(netValue);

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void focusSymbol() {
        txtSymbol.requestFocus();
    }

    public void focusQuantity() {
        txtQuantity.requestFocus();
    }

    private boolean isValidInputs(char type) {
        if (txtSymbol.getText().trim().equals("")) {
            SharedMethods.showMessage(Language.getString("INVALID_SYMBOL"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if ((txtPrice.getText().trim().equals("")) && ((type != TradeMeta.ORDER_TYPE_MARKET) && (type != TradeMeta.ORDER_TYPE_STOPLOSS_MARKET)
                /*&& (type != TradeMeta.ORDER_TYPE_DAY_MARKET)*/ && (type != TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE)  /*&& (type != TradeMeta.ORDER_TYPE_TRAILING_STOP_MARKET)*/
                && (type != TradeMeta.ORDER_TYPE_SQUARE_OFF))) {
            SharedMethods.showMessage(Language.getString("INVALID_PRICE"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if ((txtQuantity.getText().trim().equals("")) || (txtQuantity.getText().trim().equals("0"))) {
            SharedMethods.showMessage(Language.getString("INVALID_QUANTITY"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (txtQuantity.getText().trim() != null) {
            int qty = 0;
            try {
                qty = Integer.parseInt(txtQuantity.getText().trim());
            } catch (NumberFormatException e) {
                qty = 0;
            }
            if (qty <= 0) {
                SharedMethods.showMessage(Language.getString("INVALID_QUANTITY"), JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }


        return true;
    }

    private double getPrice() {
        try {
            return Double.parseDouble(txtPrice.getText());
        } catch (Exception e) {
            return 0;
        }
    }

    private boolean validateRules(int side) {
        Hashtable<String, String> ruleTable = new Hashtable<String, String>();

        try {
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out = decompress.setFiles("rules/" + exchange + "/qv.msf");
            decompress.decompress();
            ruleTable.put("QUANTITY_VALIDATION", new String(out.toByteArray()));

            Interpreter interpreter = loadInterpriter(side);
            long value = (Long) interpreter.eval(ruleTable.get("QUANTITY_VALIDATION"));
            if (value < 0) {
                int result = SharedMethods.showConfirmMessage(Language.getString("MSG_POSSIBLE_INVALID_QUANTITY"),
                        JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                if (result != JOptionPane.OK_OPTION) {
                    return false;
                }
                /*new ShowMessage(Language.getString("INVALID_QUANTITY"), "E");
                return false;*/
            }
            interpreter = null;
        } catch (Exception e) {
            e.printStackTrace();
            //return false;
        }

        try {
            char type = orderTypeList.get(cmbTypes.getSelectedIndex()).getId().charAt(0);
            if ((type != TradeMeta.ORDER_TYPE_MARKET)) {
                Decompress decompress = new Decompress();
                ByteArrayOutputStream out = decompress.setFiles("rules/" + exchange + "/pv.msf");
                decompress.decompress();
                ruleTable.put("PRICE_VALIDATION", new String(out.toByteArray()));

                Interpreter interpreter = loadInterpriter(side);
                double value = (Double) interpreter.eval(ruleTable.get("PRICE_VALIDATION"));
                if (value < 0) {
                    int result = SharedMethods.showConfirmMessage(Language.getString("MSG_POSSIBLE_INVALID_PRICE"),
                            JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                    if (result != JOptionPane.OK_OPTION) {
                        return false;
                    }
                    /*new ShowMessage(Language.getString("INVALID_PRICE"), "E");
                    return false;*/
                }
                interpreter = null;
            }
        } catch (Exception e) { // could not eveluate the rule or rule not found
            //e.printStackTrace();
//            return false;
        }
        return true;
    }

    private Interpreter loadInterpriter(int side) throws Exception {
        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, instrument);

        Interpreter interpreter = new Interpreter();
        interpreter.set("symbol", stock.getSymbolCode());
        interpreter.set("instrumentType", stock.getInstrumentType());
        interpreter.set("sector", stock.getSectorCode());
        interpreter.set("price", getPrice());
        interpreter.set("quantity", Integer.parseInt(txtQuantity.getText()));
        interpreter.set("min", stock.getMinPrice());
        interpreter.set("max", stock.getMaxPrice());
        interpreter.set("open", stock.getTodaysOpen());
        interpreter.set("high", stock.getHigh());
        interpreter.set("low", stock.getLow());
        interpreter.set("side", side);
        interpreter.set("tiff", 0);
        interpreter.set("type", orderTypeList.get(cmbTypes.getSelectedIndex()).getId().charAt(0));
        interpreter.set("disclosed", 0);
        interpreter.set("minfill", 0);
        interpreter.set("marketCode", stock.getMarketID());
        interpreter.set("refPrice", stock.getRefPrice());
        interpreter.set("bid", stock.getBestBidPrice());
        interpreter.set("offer", stock.getBestAskPrice());
        interpreter.set("direction", 0);

        return interpreter;
    }

    public void populateOrder(Stock stock) {
        try {
            /*for (int i = 0; i < exchangeList.size(); i++) {
                TWComboItem item = exchangeList.get(i);
                if (item.getId().equals(stock.getExchange())) {
                    cmbExchanegs.setSelectedIndex(i);
                    break;
                }
            }*/
            keyTyped = false;
            exchange = stock.getExchange();
            txtSymbol.setText(stock.getSymbol());
            symbolCode = stock.getSymbolCode();
            instrument = stock.getInstrumentType();
            txtPrice.setText("" + stock.getLastTradeValue());
            int decimalPlaces = stock.getDecimalCount(); //ExchangeStore.getSharedInstance().getExchange(exchange).getPriceDecimalPlaces();
            txtPrice.setText(SharedMethods.formatToDecimalPlacesNumeric(decimalPlaces, stock.getLastTradeValue()));
            //untick sell T+0 check box and disable it for non T+0 symbols
            if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, stock.getSymbol(), instrument), "0")) {
                chkSellTPlus.setEnabled(true);
            } else {
                txtTplusHolding.setText("");
                chkSellTPlus.setSelected(false);
                chkSellTPlus.setEnabled(false);
            }

            //String[] orderTypes = {"1", "2", "3", "4"};
            /*try {
                orderTypes = RuleManager.getSharedInstance().getRule("ORDER_TYPES", "*", "NEW_ORDER").getRule();
            } catch (Exception e) {
                orderTypes = RuleManager.getSharedInstance().getRule("ORDER_TYPES", exchange, "NEW_ORDER").getRule();
            }*/

            TWComboItem oldOrderType = null;
            if (orderTypeList != null && !orderTypeList.isEmpty()) {
                oldOrderType = ((TWComboItem) cmbTypes.getSelectedItem());
            }
            orderTypeList.clear();
            //TradingShared.populateOrderTypes(orderTypes, orderTypeList);

            TradeMethods.getSharedInstance().populateRapidOrderTypes(exchange, orderTypeList);
            selectOrderType();
            try {
                if (oldOrderType != null && orderTypeList.contains(oldOrderType)) {
                    cmbTypes.setSelectedItem(oldOrderType);

                } else {
                    cmbTypes.setSelectedIndex(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!TradeMethods.isSymbolAllowedForProtfolio(selectedPortfolio, exchange, false)) {
                selectPortfolioForSymbol(exchange);
            }
            stock = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectPortfolioForSymbol(String exchange) {
        TradingPortfolioRecord record = TradeMethods.getPreferredProtfolio(exchange);
        if (record != null) {
            selectPportfolio(record.getPortfolioID());
        } else {
            selectPportfolio(null);
        }
        populateBookKeepers();

    }

    public void accountDataChanged(String accountID) {

    }

    /*public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded() {
        populateExchanges();
    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }*/

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios();
        populateBookKeepers();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("BUY")) {
            chkSellTPlus.setSelected(false);
            side = TradeMeta.BUY;
            sendNewOrder(TradeMeta.BUY);
        }
        if (e.getActionCommand().equals("SELL")) {
            side = TradeMeta.SELL;
            sendNewOrder(TradeMeta.SELL);
        }
        if (e.getActionCommand().equals("CLEAR")) {
            clear();
        }
        if (e.getActionCommand().equals("CLOSE")) {
            Client.getInstance().showRapidOrderPanel(false);
        }
        if (e.getActionCommand().equals("TYPES")) {
            selectOrderType();
        }
        if (e.getSource().equals(cmbPortfolios)) {
            selectedPortfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
            populateBookKeepers();
        }
        if (e.getSource().equals(chkSellTPlus)) {
            try {
                String portfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
                String bookKeeper = ((TWComboItem) cmbBookKeepers.getSelectedItem()).getId();
                TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecord(SharedMethods.getTradeKey(exchange, symbolCode, instrument, bookKeeper), portfolio);
                txtTplusHolding.setText("" + transactRecord.getTPlusDayNetHolding());
                if (chkSellTPlus.isSelected()) {
                    try {
                        txtQuantity.setText("" + (transactRecord.getTPlusDayNetHolding() - transactRecord.getTPlusDaySellPending()));
                    } catch (Exception e2) {
                        txtQuantity.setText("");
                    }
                } else {
                    txtQuantity.setText("" + (transactRecord.getQuantity() - transactRecord.getPledged() - transactRecord.getPendingSell() - transactRecord.getTPlusPendingStock() - transactRecord.getTPlusDayNetHolding()));
                    txtTplusHolding.setText("" + 0);
                }
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    private void selectOrderType() {
        try {
            TWComboItem item = (TWComboItem) cmbTypes.getSelectedItem();
            char chType = item.getId().charAt(0);
            if (((chType) == TradeMeta.ORDER_TYPE_MARKET) || ((chType) == TradeMeta.ORDER_TYPE_STOPLOSS_MARKET) /*|| ((chType) == TradeMeta.ORDER_TYPE_DAY_MARKET)*/
                    || ((chType) == TradeMeta.ORDER_TYPE_SQUARE_OFF) || ((chType) == TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE)/*|| ((chType) == TradeMeta.ORDER_TYPE_TRAILING_STOP_MARKET)*/) {
                txtPrice.setText("");
                txtPrice.setEnabled(false);
            } else {
                txtPrice.setEnabled(true);
            }
        } catch (Exception e) {

        }
    }

    public void clear() {
        txtPrice.setText("");
        txtQuantity.setText("");
//        txtSymbol.setText("");
//        symbolCode = "";
//        instrument = -1;
//        lockInputs(false);
//        invalidateStock();
    }

    public void tradeServerConnected() {
        cmbPortfolios.setEnabled(true);
        cmbBookKeepers.setEnabled(true);
//        cmbExchanegs.setEnabled(true);
//        txtSymbol.setEnabled(true);
        txtQuantity.setEnabled(true);
        cmbTypes.setEnabled(true);
        txtPrice.setEnabled(true);
        btnBuy.setEnabled(true);
        btnSell.setEnabled(true);
        btnClear.setEnabled(true);
        if (TradingShared.isBookKeepersAvailable()) {
            pnlBookKeeper.setVisible(true);
        } else {
            pnlBookKeeper.setVisible(false);
        }
        if ((TWControl.isT0OrdersEnabled()) && (TradingShared.isT0ordersEnable())) {
            pnllblTPlusSymbols.setVisible(true);
            pnltxtTPlusSymbols.setVisible(true);
        } else {
            pnllblTPlusSymbols.setVisible(false);
            pnltxtTPlusSymbols.setVisible(false);
        }
    }

    public void tradeSecondaryPathConnected() {
        if (TradingShared.isBookKeepersAvailable()) {
            pnlBookKeeper.setVisible(true);
        } else {
            pnlBookKeeper.setVisible(false);
        }
        if ((TWControl.isT0OrdersEnabled()) && (TradingShared.isT0ordersEnable())) {
            pnllblTPlusSymbols.setVisible(true);
            pnltxtTPlusSymbols.setVisible(true);
        } else {
            pnllblTPlusSymbols.setVisible(false);
            pnltxtTPlusSymbols.setVisible(false);
        }
    }

    public void tradeServerDisconnected() {
        cmbPortfolios.setEnabled(false);
//        cmbExchanegs.setEnabled(false);
//        txtSymbol.setEnabled(false);
        txtQuantity.setEnabled(false);
        cmbTypes.setEnabled(false);
        txtPrice.setEnabled(false);
        btnBuy.setEnabled(false);
        btnSell.setEnabled(false);
        btnClear.setEnabled(false);
    }

    public void applyTheme() {
        Color background = Theme.getOptionalColor("RAPID_ORDER_PANEL_COLOR");
        Color textColor = Theme.getOptionalColor("RAPID_ORDER_PANEL_LABEL");

        if (background != null) {
            setBackground(background);
        } else {
            setBackground(Theme.getColor("BACKGROUND_COLOR"));
        }

        if (textColor == null) {
            textColor = Theme.getColor("LABEL_FGCOLOR");
        }
        lblAccount.setForeground(textColor);
        lblPrice.setForeground(textColor);
        lblQuantity.setForeground(textColor);
        lblSymbol.setForeground(textColor);
        lblType.setForeground(textColor);

        border.setTitleColor(textColor);
        border.setBorder(BorderFactory.createLineBorder(textColor));
    }

    public void focusGained(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusLost(FocusEvent e) {
        if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
            if (keyTyped) {
                doInternalValidation(txtSymbol.getText());

            }
        } else {
            if (keyTyped) {
                invalidateStock();
                lockInputs(true);
                validateSymbol(txtSymbol.getText());
            }
        }
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        if (e.getSource() == txtSymbol) {
            invalidateStock();
            if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
                doInternalValidation(txtSymbol.getText());
            } else {
                keyTyped = true;
            }
        }
    }

    private void validateSymbol(String symbol) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            lockInputs(true);
            String requestID = "RapidOrder" + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol, requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
        } else {
            doInternalValidation(symbol);
        }
    }

    private void doInternalValidation(String symbol) {
        String key = SymbolMaster.getExchangeForSymbol(symbol, false);
        if (key != null && !key.isEmpty()) {
            Stock stk = DataStore.getSharedInstance().getStockObject(key);
            populateOrder(stk);
        } else {
            invalidateStock();
        }
    }

    private void invalidateStock() {
        exchange = null;
        symbolCode = null;
        instrument = -1;


    }

    private void lockInputs(boolean lock) {
        btnBuy.setEnabled(!lock);
        btnSell.setEnabled(!lock);

    }


    public void setSymbol(String key) {

        lockInputs(false);
        populateOrder(DataStore.getSharedInstance().getStockObject(key));
        keyTyped = false;
    }

    public void notifyInvalidSymbol(String symbol) {
        SharedMethods.showMessage(Language.getString("INVALID_SYMBOL"), JOptionPane.ERROR_MESSAGE);
        txtSymbol.setText("");
        txtPrice.setText("");
        keyTyped = false;
        invalidateStock();
        lockInputs(false);

    }

    class TWPanel extends JPanel {

        public TWPanel(int width, Component component1, Component component2) {
            super();
            setLayout(new FlexGridLayout(new String[]{"" + width}, new String[]{"17", "20"}, 2, 2));
            add(component1);
            add(component2);
            setOpaque(false);
        }

        public TWPanel(int width, Component component1, Component component2, boolean needHrGap) {
            super();
            if (needHrGap) {
                setLayout(new FlexGridLayout(new String[]{"" + width}, new String[]{"17", "20"}, 2, 2));
            } else {
                setLayout(new FlexGridLayout(new String[]{"" + width}, new String[]{"17", "20"}, 0, 2));
            }
            add(component1);
            add(component2);
            setOpaque(false);
        }
    }
}
