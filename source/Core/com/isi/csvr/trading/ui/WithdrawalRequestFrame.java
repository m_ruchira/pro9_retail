package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.print.ImagePrinter;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.util.Decompress;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 1, 2005
 * Time: 3:44:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class WithdrawalRequestFrame extends InternalFrame implements ActionListener, Runnable, Themeable,
        TraderProfileDataListener, TradingConnectionListener, ItemListener {

    TWButton btnClose;
    TWButton btnSubmit;
    JPanel buttons;
    JLabel accountLabel;
    String selectedPortfolio;
    String selectedAccount;
    SimpleDateFormat date;
    private TWTextField txtAmount;
    //private CalCombo datePicker;
    private TWComboBox cmbMethds;
    private TWComboBox bankCombo;
    //    private String date;
    private TWComboBox locationCombo;
    private TWComboBox cmbPortfolio;
    private ArrayList<TWComboItem> portfolioList;
    private ArrayList<TWComboItem> banks;
    private ArrayList<TWComboItem> locations;
    private ArrayList<TWComboItem> modes;
    private boolean active = true;
    private ValueLabel lblDateVal;
    private ValueLabel lblBalVal;
    private ValueLabel lblBlockedVal;
    private ValueLabel lblSellPendingVal;
    private ValueLabel lblBalAvailableVal;
    private ValueLabel lblCurrencyVal;
    private int bankIndex = -1;
    private JPanel contents;
    private boolean isInitmode = true;

    public WithdrawalRequestFrame(boolean isCheque) {
        super(false);
        createUI();
        if (!isCheque)
            setInitialMode();
        isInitmode = false;
    }

    public void setInitialMode() {
        accountLabel.setText(Language.getString("ACCOUNT"));
        contents.remove(bankIndex);
        contents.add(bankCombo);
        if (bankCombo.getItemCount() == 0) {
            printBankInfoForm();
            cmbMethds.setSelectedIndex(0);
//            cmbMethds.setSelectedIndex(1);
        } else {
            bankCombo.setEnabled(true);
            if (bankCombo.getModel().getSize() > 0) {
                try {
                    bankCombo.setSelectedIndex(0);
                } catch (Exception ex) {
                }
            }
        }
        if (locationCombo.getItemCount() != 0)
            locationCombo.setEnabled(true);
        contents.doLayout();
    }

    private void createUI() {

        setLayout(new BorderLayout());

        String[] widths = {"50%", "50%"};
        String[] heights = {"22", "22", "22", "22", "22", "22", "22", "22", "22", "22"};
//        String[] heights = {"22", "22", "22", "22", "22", "22", "22", "22"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights, 5, 5);

        contents = new JPanel(layout);

        JLabel lblPortfolio = new JLabel(Language.getString("PORTFOLIO"));
        contents.add(lblPortfolio);
        portfolioList = new ArrayList<TWComboItem>();
        cmbPortfolio = new TWComboBox(new TWComboModel(portfolioList));
        populatePortfolios();
        contents.add(cmbPortfolio);
        cmbPortfolio.addItemListener(this);

        JLabel lblDate = new JLabel(Language.getString("DATE"));
        contents.add(lblDate);
        Account account = TradingShared.getTrader().findAccount(selectedAccount);
        lblDateVal = new ValueLabel("");
        date = new SimpleDateFormat("dd/MM/yyyy");
        try {
            lblDateVal.setText(date.format(account.getDate()));
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        lblDateVal.setHorizontalAlignment(JLabel.RIGHT);
        lblDateVal.setBorder(BorderFactory.createEtchedBorder());
        contents.add(lblDateVal);

        JLabel lblBal = new JLabel(Language.getString("ACCOUNT_BALANCE"));
        contents.add(lblBal);
        lblBalVal = new ValueLabel("");
        lblBalVal.setHorizontalAlignment(JLabel.RIGHT);
        lblBalVal.setBorder(BorderFactory.createEtchedBorder());
        contents.add(lblBalVal);

        JLabel lblBlocked = new JLabel(Language.getString("BLOCKED_AMOUNT"));
        contents.add(lblBlocked);
        lblBlockedVal = new ValueLabel("");
        lblBlockedVal.setHorizontalAlignment(JLabel.RIGHT);
        lblBlockedVal.setBorder(BorderFactory.createEtchedBorder());
        contents.add(lblBlockedVal);

        JLabel lblSellPending = new JLabel(Language.getString("UNRIALIZED_SALES"));
        contents.add(lblSellPending);
        lblSellPendingVal = new ValueLabel("");
        lblSellPendingVal.setHorizontalAlignment(JLabel.RIGHT);
        lblSellPendingVal.setBorder(BorderFactory.createEtchedBorder());
        contents.add(lblSellPendingVal);

        JLabel lblBalAvailable = new JLabel(Language.getString("MSG_AVAILABLE_CASH_AMOUNT"));
        contents.add(lblBalAvailable);
        lblBalAvailableVal = new ValueLabel("");
        lblBalAvailableVal.setHorizontalAlignment(JLabel.RIGHT);
        lblBalAvailableVal.setBorder(BorderFactory.createEtchedBorder());
        contents.add(lblBalAvailableVal);

        JLabel lblCurrency = new JLabel(Language.getString("CURRENCY"));
        contents.add(lblCurrency);
        lblCurrencyVal = new ValueLabel("");
        lblCurrencyVal.setHorizontalAlignment(JLabel.RIGHT);
        lblCurrencyVal.setBorder(BorderFactory.createEtchedBorder());
        contents.add(lblCurrencyVal);

        modes = new ArrayList<TWComboItem>();
        /*TWComboItem item1 = new TWComboItem(TradeMeta.TRANSFER_CHEQUE, Language.getString("CHEQUE"));
        modes.add(item1);
        TWComboItem item2 = new TWComboItem(TradeMeta.TRANSFER_BANK, Language.getString("BANK_TRANSFER"));
        modes.add(item2);*/
        TradingShared.getWithdrawalMethods(modes, selectedPortfolio);
        JLabel lblMethod = new JLabel(Language.getString("PAYMENT_METHOD"));
        cmbMethds = new TWComboBox(new TWComboModel(modes));
        cmbMethds.setSelectedIndex(0);
        cmbMethds.addActionListener(this);
        contents.add(lblMethod);
        contents.add(cmbMethds);

        JLabel lblAmount = new JLabel(Language.getString("AMOUNT"));
        txtAmount = new TWTextField();
        txtAmount.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL, 20L));
        contents.add(lblAmount);
        contents.add(txtAmount);

        /*JLabel chequeDateLabel = new JLabel(Language.getString("COLLECTION_DATE"));
        contents.add(chequeDateLabel);
        datePicker = new CalCombo();
        contents.add(datePicker);*/

        accountLabel = new JLabel(Language.getString("COLLECTION_LOCATION"));
        contents.add(accountLabel);
        banks = new ArrayList<TWComboItem>();
        TWComboModel bankModel = new TWComboModel(banks);
        bankCombo = new TWComboBox(bankModel);
        bankCombo.setEnabled(false);
        bankCombo.setExtendedMode(true);

        locations = new ArrayList<TWComboItem>();
        TWComboModel locationModel = new TWComboModel(locations);
        locationCombo = new TWComboBox(locationModel);
        locationCombo.setEnabled(false);
        locationCombo.setExtendedMode(true);

        bankIndex = contents.getComponentCount();
        contents.add(locationCombo);
//        contents.add(bankCombo);


        try {
            ArrayList<String[]> bankRecs = TradingShared.getTrader().getBanks(selectedPortfolio);
            //ArrayList<String[]> bankRecs = TradingShared.getTrader().getBanks();
            for (int i = 0; i < bankRecs.size(); i++) {
                banks.add(new TWComboItem(bankRecs.get(i)[0] + "|" + bankRecs.get(i)[1], bankRecs.get(i)[1] + " - " + bankRecs.get(i)[2]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        populateLocations();
        try {
            locationCombo.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        buttons = new JPanel(new FlowLayout(FlowLayout.TRAILING));

        btnClose = new TWButton(Language.getString("CLOSE"));
        btnClose.addActionListener(this);
        btnClose.setActionCommand("CLOSE");
        btnSubmit = new TWButton(Language.getString("SUBMIT"));
        btnSubmit.addActionListener(this);
        btnSubmit.setActionCommand("SUBMIT");

        buttons.add(btnSubmit);
        buttons.add(btnClose);

        add(contents, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);
        setTitle(Language.getString("WITHDRAWAL_REQUEST"));
        setClosable(true);
        setSize(300, 355);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        Thread thread = new Thread(this, "WithdrawalReq");
        thread.start();

        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        applyTheme();
    }

    private void populatePortfolios() {
        try {
            if (TradingShared.getTrader() != null) {
                portfolioList.clear();
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());
                    portfolioList.add(item);
                    item = null;
                    record = null;
                }
                cmbPortfolio.updateUI();
                cmbPortfolio.setSelectedIndex(0);
                selectedPortfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
                selectedAccount = TradingShared.getTrader().getPortfolio(selectedPortfolio).getAccountNumber();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void populateLocations() {
        locations.clear();
        String broker = TradingShared.getTrader().getBrokerID(selectedPortfolio);
        try {
            Decompress decompress = new Decompress();
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/brokerlocations_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            ByteArrayOutputStream out;
            if (f.exists()) {
                out = decompress.setFiles(sLangSpecFileName);
            } else {
                locationCombo.setEnabled(false);
                return;
            }
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            do {
                record = null;
                record = in.readLine();
                if (record != null) {
                    try {
                        String[] data = record.split(Meta.DS);
                        if (data[0].equals(broker)) {
//                            String[] fields = data[1].split(Meta.FS);
                            TWComboItem reportItem = new TWComboItem(data[2], UnicodeUtils.getNativeString(data[1]));
                            locations.add(reportItem);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            } while (record != null);

            out = null;
            decompress = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();

        }
        if (locations.size() < 1) {
            locationCombo.setEnabled(false);
        } else if (locations.size() > 1) {
            TWComboItem reportItem = new TWComboItem("NONE", Language.getString("NONE"));
            locations.add(0, reportItem);
//            locationCombo.setSelectedIndex(0);
        }
    }

    public void setData(String wdate, double amount, int method, String account, String portfolio, String bankID) {
        active = true;
        lblDateVal.setText(wdate);
        cmbPortfolio.removeAllItems();
        cmbPortfolio.removeItemListener(this);
        cmbPortfolio.addItem(portfolio);
        cmbPortfolio.setSelectedItem(portfolio);
        cmbPortfolio.addItemListener(this);
        selectedPortfolio = portfolio;
        selectedAccount = TradingShared.getTrader().getPortfolio(selectedPortfolio).getAccountNumber();
        Account account2 = TradingShared.getTrader().findAccount(selectedAccount);
        lblDateVal.setText(date.format(account2.getDate()));
        txtAmount.setText(amount + "");
        cmbMethds.removeActionListener(this);
        cmbMethds.removeAllItems();
        cmbMethds.addItem(TradingShared.getFundTransferMethod(method));
        cmbMethds.setSelectedItem(TradingShared.getFundTransferMethod(method));
        cmbMethds.addActionListener(this);
        bankCombo.removeActionListener(this);
        if (method == TradeMeta.TRANSFER_BANK) {
            bankCombo.removeAllItems();
            ArrayList<String[]> bankRecs = TradingShared.getTrader().getBanks(selectedPortfolio);
            //  ArrayList<String[]> bankRecs = TradingShared.getTrader().getBanks();
            for (int i = 0; i < bankRecs.size(); i++) {
                if ((bankRecs.get(i)[0].equals(bankID)) && (bankRecs.get(i)[1].equals(account))) {
                    bankCombo.addItem(account + " - " + bankRecs.get(i)[2]);
                }
            }
            bankCombo.setSelectedItem(0);
        } else {
            bankCombo.removeAllItems();
        }
        bankCombo.addActionListener(this);
        cmbMethds.setEnabled(false);
        cmbPortfolio.setEnabled(false);
        bankCombo.setEnabled(false);
        locationCombo.setEnabled(false);
        btnSubmit.setEnabled(false);
        btnClose.setEnabled(true);
        txtAmount.setEnabled(false);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("SUBMIT")) {

            boolean sucess = false;
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            int mode = Integer.parseInt(((TWComboItem) cmbMethds.getSelectedItem()).getId());

            if (txtAmount.getText().equals("")) {
                SharedMethods.showMessage(Language.getString("INVALID_WITHDRAWAL_AMOUNT_MESSAGE"), JOptionPane.ERROR_MESSAGE);
            } else {
                double amount = Double.parseDouble(txtAmount.getText());
//                long amount = Long.parseLong(txtAmount.getText()); //bug id <#0039> amount must be double
                String bankID = null;
                String bankName = null;
                String bankAccount = null;
                String location = null;
                if (mode == TradeMeta.TRANSFER_CHEQUE) {
                    bankID = "-1";
                    bankName = "NA";
                    bankAccount = "NA";
                    try {
                        location = ((TWComboItem) locationCombo.getSelectedItem()).getId();
                        if (location.equals("NONE")) {
                            SharedMethods.showMessage(Language.getString("INVALID_WITHDRAWAL_LOCATION_MESSAGE"), JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    } catch (Exception e1) {
                        location = "NA";
                    }
                } else {
                    location = "NA";
                    try {
                        bankID = ((TWComboItem) bankCombo.getSelectedItem()).getId().split("\\|")[0];
                        bankName = ((TWComboItem) bankCombo.getSelectedItem()).getValue().split("-")[1];
                        bankAccount = ((TWComboItem) bankCombo.getSelectedItem()).getId().split("\\|")[1];
                    } catch (Exception e1) {
                        bankID = "-1";
                        bankName = "NA";
                        bankAccount = "NA";
                    }
                }
                String selPf = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
                Account account;
                if (selPf != null && !selPf.isEmpty()) {
                    account = TradingShared.getTrader().findAccountByPortfolio(selPf);
                } else {
                    account = TradingShared.getTrader().getAccount(0);
                }
                if (amount > account.getBalance()) {
                    int result = SharedMethods.showConfirmMessage(Language.getString("MSG_WITHDRAW_AMOUNT_EXCEEDED"),
                            JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                    if (result == JOptionPane.YES_OPTION) {
                        sucess = sendWithdrawalRequest(mode, amount, bankID, bankName, bankAccount, location);
                    }
                } else {
                    if (mode == TradeMeta.TRANSFER_BANK) {
                        int warning = SharedMethods.showConfirmMessage(Language.getString("MSG_WITHDRAW_AMOUNT_NET_BANK_CHARGES"),
                                JOptionPane.INFORMATION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                        if (warning == JOptionPane.CANCEL_OPTION) {
                            return;
                        }
                    }
                    sucess = sendWithdrawalRequest(mode, amount, bankID, bankName, bankAccount, location);
                }

                if (sucess) {
                    dispose();
                }
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "WithdrawalNotification");
            }
        } else if (e.getActionCommand().equals("CLOSE")) {
            dispose();
        } else if (e.getSource() == cmbMethds) {
            if (cmbMethds.getSelectedIndex() == 1) { // CHEQUE MODE
                accountLabel.setText(Language.getString("COLLECTION_LOCATION"));
                bankCombo.setEnabled(false);
                locationCombo.setEnabled(true);
                try {
                    locationCombo.setSelectedIndex(0);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
//                bankCombo.getModel().setSelectedItem("");
                contents.remove(bankIndex);
                contents.add(locationCombo);
                contents.doLayout();
            } else { // BANK TRANSFER MODE
                accountLabel.setText(Language.getString("ACCOUNT"));
                contents.remove(bankIndex);
                contents.add(bankCombo);
                if (bankCombo.getItemCount() == 0) {
                    if (!isInitmode) {
                        printBankInfoForm();
                    }
                    cmbMethds.setSelectedIndex(0);
                } else {
                    bankCombo.setEnabled(true);
                    if (bankCombo.getModel().getSize() > 0) {
                        try {
                            bankCombo.setSelectedIndex(0);
                        } catch (Exception ex) {
                        }
                    }
                }
                contents.doLayout();
            }
        }
    }

    private void printBankInfoForm() {
        String str = TradingShared.getMessageString("MSG_NO_BANK_DETAILS");
        if (Settings.getBrokerFax() != null) {
            str = str.replaceFirst("\\[BROKER_FAX\\]", Settings.getBrokerFax());
        }
        int result = SharedMethods.showConfirmMessage(str, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);

        if (result == JOptionPane.YES_OPTION) {
            ImagePrinter imagePrinter = new ImagePrinter(this, "printables/Common/Bankinfo_" + Language.getLanguageTag() + ".jpg");
            imagePrinter.print();
            imagePrinter = null;
        }
    }

    private boolean sendWithdrawalRequest(int mode, double amount, String bank, String bankName, String accoutNumber, String location) {

        String str;
        String amounts = txtAmount.getText();
        String curr = lblCurrencyVal.getText();
        String portfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();

        if (mode == TradeMeta.TRANSFER_CHEQUE) {
            str = Language.getString("WITHDRAWAL_CONFIRM_MESSAGE_CHEQUE");
            str = str.replaceFirst("\\[AMOUNT\\]", amounts);
            str = str.replaceFirst("\\[CUR\\]", curr);
            str = str.replaceFirst("\\[NAME\\]", TradingShared.getTrader().getMubasherID(portfolio));
        } else {
            str = Language.getString("WITHDRAWAL_CONFIRM_MESSAGE_BANK");
            str = str.replaceFirst("\\[AMOUNT\\]", amounts);
            str = str.replaceFirst("\\[CUR\\]", curr);
            str = str.replaceFirst("\\[ACCOUNT\\]", accoutNumber);
            str = str.replaceFirst("\\[BANK\\]", bankName);
        }
        int result = SharedMethods.showConfirmMessage(str,
                JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            return TradeMethods.getSharedInstance().sendWinthdrawalRequest(mode, amount, bank, accoutNumber, portfolio, location, curr);
        }
        return false;
    }


    public void run() {
        Account account = null;
        while (active) {
            try {
                account = TradingShared.getTrader().findAccount(selectedAccount);
                lblBalVal.setText(" " + SharedMethods.toDeciamlFoamat(account.getBalance()) + " ");
                lblBlockedVal.setText(" " + SharedMethods.toDeciamlFoamat(account.getBlockedAmount()) + " ");
                //  lblSellPendingVal.setText("0");
                lblSellPendingVal.setText(account.getUnrializedSales() + "");
                lblBalAvailableVal.setText(" " + SharedMethods.toDeciamlFoamat(account.getCashAvailableForWithdrawal()) + " ");
                lblCurrencyVal.setText(" " + account.getCurrency() + " ");
            } catch (Exception e) {
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void applyTheme() {
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");
        //datePicker.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void accountDataChanged(String accountID) {

    }

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios();
//        populateLocations();
    }

    public void tradeServerConnected() {
        btnClose.setEnabled(true);
        btnSubmit.setEnabled(true);
        populateLocations();
//        TradeMethods.requestBankAccountDetails(Constants.PATH_PRIMARY);
    }

    public void tradeSecondaryPathConnected() {
//        TradeMethods.requestBankAccountDetails(Constants.PATH_SECONDARY);
    }

    public void tradeServerDisconnected() {
        btnClose.setEnabled(false);
        btnSubmit.setEnabled(false);
    }

    public void internalFrameClosed(InternalFrameEvent e) {
        active = false;
        Theme.unRegisterComponent(this);
        super.internalFrameClosed(e);
        Client.getInstance().getDesktop().remove(this);
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == cmbPortfolio) {
            selectedPortfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
            selectedAccount = TradingShared.getTrader().getPortfolio(selectedPortfolio).getAccountNumber();
            TradingShared.getWithdrawalMethods(modes, selectedPortfolio);
        }
    }

    class ValueLabel extends JLabel {

        /**
         * Creates a <code>JLabel</code> instance with the specified text.
         * The label is aligned against the leading edge of its display area,
         * and centered vertically.
         *
         * @param text The text to be displayed by the label.
         */
        public ValueLabel(String text) {
            super(text);
            setFont(getFont().deriveFont(Font.BOLD));
            setHorizontalAlignment(JLabel.RIGHT);
        }
    }
}
