package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.StockTransferObject;
import com.isi.csvr.trading.datastore.StockTransferObjectStore;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 18, 2006
 * Time: 4:36:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockTransferModel extends CommonTable implements TableModel, CommonTableInterface {

    public StockTransferModel() {
    }

    public int getRowCount() {
        return StockTransferObjectStore.getSharedInstance().getsize();
    }

    public void setSymbol(String symbol) {

    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        StockTransferObject transaction = StockTransferObjectStore.getSharedInstance().getStockTransObject(rowIndex);
        switch (columnIndex) {
            case 0:
                return (transaction.getSymbol());
            case 1:
//                return  (transaction.getExchange()); //TODO:Need to check further -shanika
                return ExchangeStore.getSharedInstance().getExchange(transaction.getExchange().trim()).getDisplayExchange(); // To display exchange
            case 2:
                return DataStore.getSharedInstance().getShortDescription(SharedMethods.getKey(transaction.getExchange(), transaction.getSymbol(), transaction.getInstrument()));
            case 3:
                return transaction.getAvgCost();
            case 4:
                return transaction.getQuantity();
            case 5:
                return transaction.getBroker();
            case 6:
                return transaction.getTransferDate();
            case 7:
                return transaction.getPortfolioID();
            case 8:
                return TradingShared.getFundTransferStatus(transaction.getStatus());
            case 9:
                return transaction.getID();
            case 10:
                return TradingShared.getFundTransferReason(transaction.getRejectReason());
            default:
                return "0";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 1:
            case 2:
            case 0:
            case 5:
            case 6:
            case 7:
            case 9:
//            case 3:
            case 'B':
                return String.class;
//            case  0:
//            case  3:
            case 'P':
            case 'Q':
                return Number[].class;
//            case  0:
            case 8:
            case 4:
            case 3:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[4];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));     //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("ROW_COLOR_1"), FIELD_BASIC_ROW1, Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("ROW_COLOR_2"), FIELD_BASIC_ROW2, Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2"), Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        return customizerRecords;
    }
}
