package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.BrokerConfig;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.CurrencyListener;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.trading.marginTrading.CoverageLabel;
import com.isi.csvr.trading.portfolio.PortfolioList;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.shared.TraderProfileDataListener;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.variationmap.VariationImage;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 23, 2006
 * Time: 2:58:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class AccountWindow extends InternalFrame implements TraderProfileDataListener, TradingConnectionListener,
        MouseListener, CurrencyListener, ActionListener, PopupMenuListener, Themeable {
    Dimension dim;
    int defaultsize = 18;
    Dimension originalDim;
    JPanel loginDetailPanel;
    CustomizableLable lblLogingDetail;
    //SimplevariationImage coverage;
    SimplevariationImage marginUtilized;
    TWDecimalFormat formatter = new TWDecimalFormat("#0.00");
    private ViewSetting accountSettings;
    private AccountModel accountModel;
    private Table accountTable;
    private JLabel lblAvailablePFs = new JLabel();
    private JLabel btnAvailablePFs = new JLabel();
    private JPanel pnlAvailablePFs = new JPanel(new FlowLayout(FlowLayout.LEFT, 1, 1));
    private JPanel pnlMarginViews = new JPanel();
    private JLabel lblSelectedCurrencyID = new JLabel();
    private JLabel lblCurrencyIDs = new JLabel();
    private JLabel btnCurrencyIDs = new JLabel();
    private JPanel pnlCurrencyIDs = new JPanel(new FlowLayout(FlowLayout.RIGHT, 1, 1));
    private String[] selectedPFIDs = null;
    private String[] currencyIDs;
    private ButtonGroup btnGroupCurrency;
    private JPopupMenu currencyPopup;
    private boolean initialSecondaryPathConnection = true;
    private JLabel imglbl1 = new JLabel();
    private CoverageLabel imglbl2;
    private boolean isPortfolioMarginable;
    private boolean isPortfolioMarginableDay;
    private JPanel compound = new JPanel();
    private boolean initdataFromUpdate = true;

    //JLabel  lblLogingDetail;

    public AccountWindow(Table accountTable, ViewSetting oSetting) {
        super(accountTable);
//        super(oSetting.getCaption(), oSetting);
        this.accountTable = accountTable;
        accountSettings = oSetting;
        originalDim = new Dimension(accountSettings.getSize());
        setOrigDimention(originalDim);
        //oSetting.getSize();
        createAccountWindow();
//        addComponentListener(this);
        //     this.setisLoadedFromWsp(false);
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        //    resizeRowhight();
        //     resizeRows(originalDim, originalDim.height);
    }

    public void componentResized(ComponentEvent e) {
        if (this.getOrigDimension().height > 0 && this.getOrigDimension().width > 0) {
            if (!accountTable.isFontResized()) {
//                Dimension currSize = this.getSize();
                dim = reSizeSpecial(new Dimension(this.getSize().width, this.getSize().height),
                        new Dimension(this.getOldSize().width, this.getOldSize().height));
                if (!this.getSize().equals(dim)) {
                    resizeRows(dim, this.getOrigDimension().height);
                    //  resizeRows(dim,311 );
                } else {
                    this.setOldSize(new Dimension(this.getOldSize().width, this.getOldSize().height));
                }
                this.setSize(new Dimension(dim.width, dim.height));

            } else {
                Dimension olddim = new Dimension(this.getOldSize().width, this.getSize().height);
                dim = resizeToFont(new Dimension(this.getSize().width, this.getSize().height - this.getTitlebarHeight() - 20),
                        new Dimension(olddim.width, olddim.height - this.getTitlebarHeight() - 20));
                System.out.println("size of font resize else ==" + dim.width + " , " + dim.height);
                this.setSize(new Dimension(dim.width, dim.height + this.getTitlebarHeight() + 20));
                accountTable.setFontResized(false);
            }
        }
        if (dim == null) {
            Dimension dim2 = accountSettings.getSize();
            this.setSize(dim2);
            this.setisLoadedFromWsp(false);
            this.setOldSize(dim2);
            this.setOrigDimention(originalDim);
        }
        accountTable.getSmartTable().adjustColumnWidthsToFit(10);
        accountSettings.setSize(dim);
        accountSettings.setFont(accountTable.getTable().getFont());
    }

    public void resizeRows(Dimension dim, int origHeight) {
        Font oFont = accountSettings.getFont();
        if (oFont == null) {
            oFont = accountTable.getTable().getFont();
        }
        int northPanelHight = (accountTable.getNorthPanel()).getSize().height;
        System.out.println("Height ^^^^^" + northPanelHight);
        int titlteHeight = this.getTitlebarHeight();
        float ratio = (((float) defaultsize) / origHeight);
        int newSize = (int) Math.floor(ratio * dim.height);
        if (oFont != null) {
            Font oFont2 = new TWFont(oFont.getFamily(), oFont.getStyle(), newSize);
            accountTable.getTable().setFont(oFont2);
            FontMetrics oMetrices = accountTable.getFontMetrics(oFont2);
            int rowCount = accountTable.getTable().getRowCount();
            int emptySpace = ((dim.height - titlteHeight - northPanelHight) - 2 - ((oMetrices.getMaxAscent() + oMetrices.getMaxDescent()) * rowCount));
            int newGap = (int) Math.floor((emptySpace / rowCount));
            accountTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
            int finspace = (dim.height - titlteHeight - northPanelHight) - 4 - (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap) * rowCount;
            if (finspace > 0) {
                int hight = this.dim.height - finspace;  //+this.getTitlebarHeight() + 20;
                int width = this.dim.width;
                this.dim.setSize(width, hight);
                this.setOldSize(this.dim);
            }
            oMetrices = null;
            oFont = null;
            oFont2 = null;
        }
    }

    /*public void resizeRowhight(){
        try{
        int titlteHeight = 23;
        int northPanelHight =  (accountTable.getNorthPanel()).getSize().height;
        Font oFont2= accountTable.getTable().getFont();
        FontMetrics oMetrices = accountTable.getFontMetrics(oFont2);
        int rowCount = accountTable.getTable().getRowCount();
        int emptySpace = ((this.getSize().height - titlteHeight-northPanelHight)- 2 -((oMetrices.getMaxAscent() + oMetrices.getMaxDescent())*rowCount));
        int newGap = (int)Math.floor((emptySpace/rowCount));

        accountTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
        int assent = oMetrices.getMaxAscent();
      //  accountTable.getTable().updateUI();
        }catch(Exception e){
            e.printStackTrace();
        }

    }*/

    public InternalFrame createAccountWindow() {
        accountTable.setThreadID(Constants.ThreadTypes.TRADING);
        accountTable.hideHeader();
        accountTable.hideCustomizer();
        accountTable.setAutoResize(true);
        accountTable.setWindowType(ViewSettingsManager.ACCOUNT_SUMMARY_VIEW);
        accountTable.getPopup().hideCopywHeadMenu();
        accountTable.getPopup().hideResizeColumns();
        accountModel = new AccountModel(this);
//        ViewSetting oSetting = ViewSettingsManager.getSummaryView("ACCOUNT");
//        accountSettings = oSetting;
        accountModel.setViewSettings(accountSettings);
        accountTable.setModel(accountModel);
        accountModel.setTable(accountTable);
        accountModel.applyColumnSettings();
        accountModel.updateGUI();
        accountTable.setNorthPanel(createPortfolioPanel());

        setDecimalPlaces();

//        GUISettings.setColumnSettings(accountSettings, TWColumnSettings.getItem("ACCOUNT_COLS"));

        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        accountSettings.setParent(this);
        this.getContentPane().add(accountTable);
        Client.getInstance().getDesktop().add(this);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setPrintable(true);
        this.setDetachable(true);
        this.setTitle(Language.getString(accountSettings.getCaptionID()));
        this.updateUI();
        this.applySettings();
        this.setLayer(GUISettings.TOP_LAYER);
        this.setVisible(false);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        CurrencyStore.getSharedInstance().addCurrencyListener(this);
        setTitle();
        /*if(selectedPFIDs==null){
            setInitialData();
        }*/
        accountTable.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        //accountTable.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);   //to show a scroll bar for users with margin trading
        accountTable.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        accountTable.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        if (!getisLoadedFromWsp()) {
            //     this.setOrigDimention(this.getSize());
        }
        return this;
    }

    private void createMarginViewsPannel() {
        pnlMarginViews = new JPanel();
        // coverage = new SimplevariationImage();
        //  coverage.setType(SimplevariationImage.TYPE_COVERAGE);
        marginUtilized = new SimplevariationImage();
        JPanel utilized = new JPanel();
        utilized.setLayout(new BorderLayout(1, 0));
        JLabel marginUtilisedlbl = new JLabel("  " + Language.getString("ACCOUNT_SUMMARY_MARGIN_UTILIZED"));
        utilized.add(marginUtilisedlbl, BorderLayout.WEST);
        imglbl1.setVerticalAlignment(JLabel.CENTER);
        imglbl1.setIcon(marginUtilized);
        imglbl1.setHorizontalTextPosition(JLabel.CENTER);
        imglbl1.setPreferredSize(new Dimension(85, 20));
        imglbl1.setOpaque(false);
        // imglbl1.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        marginUtilized.setHeight(20);
        marginUtilized.setWidth(85);
        utilized.add(imglbl1, BorderLayout.EAST);

        imglbl2 = new CoverageLabel(BrokerConfig.getSharedInstance().getCoverageDecimalCount(), BrokerConfig.getSharedInstance().getCoverageDecimalRoundingMode());
        JPanel coveragepan = new JPanel();
        coveragepan.setLayout(new BorderLayout(1, 0));
        JLabel coveragelbl = new JLabel(Language.getString("ACCOUNT_SUMMARY_COVERAGE"));
        coveragepan.add(coveragelbl, BorderLayout.WEST);
        imglbl2.initIcon(new SimplevariationImage());
//        imglbl2.setPreferredSize(new Dimension(85, 20));
        imglbl2.setPreferredSize(new Dimension(100, 20));
        imglbl2.setVerticalAlignment(JLabel.CENTER);
        imglbl2.setHorizontalTextPosition(JLabel.CENTER);
        // imglbl2.setIcon(coverage);
        imglbl2.setOpaque(false);
        // imglbl2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        coveragepan.add(imglbl2, BorderLayout.EAST);
        //    coverage.setHeight(20);
        //  coverage.setWidth(85);

//        pnlMarginViews.setLayout(new FlexGridLayout(new String[]{"5%", "45%", "5%", "40%", "5%"}, new String[]{"3", "20", "3"}));
//        pnlMarginViews.setLayout(new FlexGridLayout(new String[]{"5", "190", "100%", "195", "5"}, new String[]{"3", "20", "3"}));
        pnlMarginViews.setLayout(new FlexGridLayout(new String[]{"5", "190", "100%", "210", "5"}, new String[]{"3", "20", "3"}));

        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(utilized);
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(coveragepan);
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());
        pnlMarginViews.add(new JLabel());


        GUISettings.applyOrientation(pnlMarginViews);

    }

    public void setInitialData() {
        try {
            ArrayList<TradingPortfolioRecord> records = TradingShared.getTrader().getPortfolios();
            TradingPortfolioRecord record = null;
            selectedPFIDs = new String[records.size()];
            for (int i = 0; i < records.size(); i++) {
                record = records.get(i);
                selectedPFIDs[i] = record.getPortfolioID();
                record = null;
            }
            accountModel.setPotfolios(selectedPFIDs);
            setTitle();
            updateMarginPanel();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        selectedPFIDs = new String[1];
//        String  sel = (TradingShared.getTrader().getPortfolios().get(0)).getPortfolioID();
//        selectedPFIDs[0] = sel;
//        accountModel.setPotfolios(selectedPFIDs);
//        setTitle();
    }

    //public Dimension getMaximumSize() {
    //     return new Dimension(600, (int)accountTable.getPreferredSize().getHeight() + this.getTitlebarHeight() + 45) ;
    //  }

//    public Dimension getMinimumSize() {
//        return new Dimension(10, (int)accountTable.getPreferredSize().getHeight() + this.getTitlebarHeight() + 45) ;
//    }

    public ViewSetting getViewSettings() {
        return accountSettings;
    }

    public void selectAccounts(String[] selPortfolio, String currency) {
        if (selPortfolio != null) {
            this.selectedPFIDs = selPortfolio;
            accountModel.setPotfolios(selPortfolio);

            setTitle();
            updateMarginPanel();
        }
        if (currency != null) {
            lblSelectedCurrencyID.setText(currency);
            accountModel.setBaseCurrency(currency);
        }

        setDecimalPlaces();
        updateMarginPanel();

    }

    public void setTitle() {
        String sTitle = Language.getString("ACCOUNT_SUMMARY") + " - ";
        TradingPortfolioRecord pfRecord = null;
        if (selectedPFIDs != null) {
            for (int i = 0; i < selectedPFIDs.length; i++) {
                pfRecord = TradePortfolios.getInstance().getPortfolio(selectedPFIDs[i]);
                if (pfRecord != null)
                    sTitle += pfRecord.getName();
                if (i != (selectedPFIDs.length - 1))
                    sTitle += ", ";
//                sTitle += selectedPFIDs[i];
//                if (i != (selectedPFIDs.length - 1))
//                    sTitle += ", ";
            }
        }
        if (sTitle.trim().endsWith(",")) {
            sTitle = sTitle.substring(0, sTitle.lastIndexOf(","));
        }
        if (sTitle.trim().endsWith("-")) {
            sTitle = sTitle.substring(0, sTitle.lastIndexOf("-"));
        }

        sTitle = sTitle;
        this.setTitle(sTitle);
        this.repaint();
    }

    private JPanel createPortfolioPanel() {
        createMarginViewsPannel();
        currencyIDs = new String[0];
//        selectedPFIDs = new String[1];
        pnlAvailablePFs.setPreferredSize(new Dimension(90, 24));
        btnAvailablePFs.setPreferredSize(new Dimension(10, 22));
        btnAvailablePFs.setIcon(new ImageIcon("images/Common/graph_down_pop.gif"));

        btnAvailablePFs.addMouseListener(this);
        lblAvailablePFs.addMouseListener(this);

        pnlAvailablePFs.add(lblAvailablePFs);
        pnlAvailablePFs.add(btnAvailablePFs);
        pnlAvailablePFs.setPreferredSize(new Dimension(120, 24));

        // Create Currency List
        pnlCurrencyIDs.setPreferredSize(new Dimension(90, 24));
        pnlCurrencyIDs.setLayout(new BoxLayout(pnlCurrencyIDs, BoxLayout.LINE_AXIS));
        btnCurrencyIDs.setPreferredSize(new Dimension(10, 22));
        btnCurrencyIDs.setIcon(new ImageIcon("images/Common/graph_down_pop.gif"));

        btnCurrencyIDs.addMouseListener(this);
        lblCurrencyIDs.addMouseListener(this);

//        pnlCurrencyIDs.add(Box.createHorizontalGlue());
        pnlCurrencyIDs.add(lblCurrencyIDs);
        pnlCurrencyIDs.add(btnCurrencyIDs);
        lblSelectedCurrencyID.setPreferredSize(new Dimension(80, 20));
        pnlCurrencyIDs.add(lblSelectedCurrencyID);
        lblAvailablePFs.setText("  " + Language.getString("PORTFOLIO_LIST"));
        lblCurrencyIDs.setText(Language.getString("CURRENCY"));
        lblSelectedCurrencyID.setHorizontalTextPosition(JLabel.CENTER);
        currencyAdded();
//        pnlCurrencyIDs.setBorder(BorderFactory.createLineBorder(Color.black));
        JPanel portLoggingDetailPanel = new JPanel();
        /**************display trade user login status****************************/
        loginDetailPanel = new JPanel();
        lblLogingDetail = new CustomizableLable(TradingShared.TRADE_USER_STATUS);
        JLabel lblUserIcon;
        lblUserIcon = new JLabel("");
        ImageIcon imgTraderUser = null;
        try {
            imgTraderUser = new ImageIcon("images/Common/icon-user.gif");
        } catch (Exception e) {
            imgTraderUser = null;
            System.out.println("icon is null");
        }
        lblUserIcon.setIcon(imgTraderUser);
        // lblLogingDetail .setPreferredSize(new Dimension(80, 20));
        loginDetailPanel.setLayout(new BoxLayout(loginDetailPanel, BoxLayout.LINE_AXIS));
        loginDetailPanel.add(Box.createHorizontalGlue());
        if ((TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY) != null) && (!TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY).equals(""))) {
            loginDetailPanel.add(lblLogingDetail);
        } else {
            loginDetailPanel.add(new CustomizableLable(""));
        }
        loginDetailPanel.add(lblUserIcon);
        loginDetailPanel.add(Box.createHorizontalGlue());
        loginDetailPanel.setVisible(false);

        if (TradingShared.isConnected()) {
            loginDetailPanel.setVisible(true);
        }

        portLoggingDetailPanel.setLayout(new BorderLayout());
        JPanel portPanel = new JPanel();
        // portPanel.setLayout(new BoxLayout(portPanel, BoxLayout.LINE_AXIS));
//        portPanel.setLayout(new FlexGridLayout(new String[]{"5", "190", "100%", "195", "5"}, new String[]{ "25"}));
        /*
            Added by chandika for the version 9 KSA mtr solution
         */
        final TWButton btnRefresh = TradeMethods.getAccountRefresh();
        btnRefresh.setVisible(false);
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Thread("Account Summary  Refresh Button") {
                    public void run() {
                        try {
                            TradeMethods.enableRefreshButtons(false);
                            if (TWControl.clearDataBeforeRefresh()) {
                                accountModel.clear();
                            }
                            TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);
                            TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
                            Thread.sleep(5000);
                            TradeMethods.enableRefreshButtons(true);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }.start();
            }
        });
        portPanel.setLayout(new FlexGridLayout(new String[]{"5", "150", "100%", "150", "5", "90"}, new String[]{"25"}));
        portPanel.add(new JLabel());
        portPanel.add(pnlAvailablePFs);//,BorderLayout.WEST);
        portPanel.add(new JLabel());
        portPanel.add(pnlCurrencyIDs);//BorderLayout.EAST);
        portPanel.add(new JLabel());
        portPanel.add(btnRefresh);

//        JPanel compound = new JPanel();
        compound.setLayout(new BorderLayout());
        compound.add(portPanel, BorderLayout.NORTH);

        compound.add(pnlMarginViews, BorderLayout.SOUTH);
        TradeMethods.checkRefreshButton();
        portLoggingDetailPanel.add(loginDetailPanel, BorderLayout.PAGE_START);
        portLoggingDetailPanel.add(compound, BorderLayout.SOUTH);
        return portLoggingDetailPanel;
    }


    public void accountDataChanged(String accountID) {
        if (selectedPFIDs == null) {
            setInitialData();
        } else {
            accountModel.setPotfolios(selectedPFIDs);
            updateMarginPanel();

        }
    }

    public void portfolioDataChanged(String portfolioID) {
        if (selectedPFIDs == null) {
            setInitialData();
        }
        setTitle();
    }

    public void tradeServerConnected() {
//        setInitialData();

        if (this != null) {

            lblLogingDetail.setText(TradingShared.TRADE_USER_STATUS);
            loginDetailPanel.setVisible(true);
            checkMarginAvailability();
        }
        //    if (selectedPFIDs == null) {
        setInitialData();
        accountModel.setMarginReferenceExchange(TradeMethods.getReferenceMarginExchangeForAccSummary());
        if (imglbl2 != null) {
            imglbl2.setDecimals(BrokerConfig.getSharedInstance().getCoverageDecimalCount());
        }

        accountModel.setCashAddToTotPF(BrokerConfig.getSharedInstance().isCashBalAddedToTotPf());
        TradeMethods.checkRefreshButton();

        //  }
    }

    public void tradeSecondaryPathConnected() {
        if (initialSecondaryPathConnection) {
            initialSecondaryPathConnection = false;
            setInitialData();
            updateMarginPanel();
        }
    }

    public void tradeServerDisconnected() {
        if (this != null) {
            loginDetailPanel.setVisible(false);
        }
        TradingShared.clearAccounts();
        updateMarginPanel();
        accountModel.resetMarginValues();
        pnlMarginViews.setVisible(false);
        selectedPFIDs = null;
        TradeMethods.checkRefreshButton();
        setInitdataFromUpdate(true);
    }

    public String[][] getPanelString() {
        String sTitle = "";
        for (int i = 0; i < selectedPFIDs.length; i++) {
            sTitle += selectedPFIDs[i];
            if (i != (selectedPFIDs.length - 1))
                sTitle += ", ";
        }
        String[][] strArray = new String[1][8];
        strArray[0][0] = "10";
        strArray[0][1] = Language.getString("SELECTED_PORTFOLIO");
        strArray[0][2] = "4";
        strArray[0][3] = sTitle;
        strArray[0][4] = "10";
        strArray[0][5] = Language.getString("CURRENCY");
        strArray[0][6] = "4";
        strArray[0][7] = lblSelectedCurrencyID.getText();
        return strArray;
    }

    public void workspaceLoaded() {
        if (this.isShowing()) {
//                this.resizeRowhight();
        }
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private void currencyButtonClicked() {
        TWMenuItem menuItem = null;
        String sItem = null;
        btnGroupCurrency = new ButtonGroup();
        if (currencyPopup != null) {
            currencyPopup.removeAll();
            currencyPopup.removePopupMenuListener(this);
        }
        currencyPopup = new JPopupMenu();
        currencyPopup.setLightWeightPopupEnabled(false);
        currencyPopup.addPopupMenuListener(this);

        ArrayList<String> idlist = TradeMethods.getAccountSummaryCurrensyList();
        currencyIDs = idlist.toArray(currencyIDs);
        String[] currencies = new String[0];
        currencies = idlist.toArray(currencies);

        for (int i = 0; i < currencies.length; i++) {
            sItem = currencies[i];
            menuItem = new TWMenuItem(sItem);
            menuItem.setActionCommand(sItem);
            menuItem.addActionListener(this);
            currencyPopup.add(menuItem);
            btnGroupCurrency.add(menuItem);
            menuItem = null;
        }
        GUISettings.applyOrientation(currencyPopup);

        int popWidth = 0;
        if (Language.isLTR()) {
            popWidth = (int) currencyPopup.getPreferredSize().getWidth();
        }
        SwingUtilities.updateComponentTreeUI(currencyPopup);
        currencyPopup.show(btnCurrencyIDs, btnCurrencyIDs.getWidth() - popWidth, btnCurrencyIDs.getHeight() - 4);

    }

    public void setSelectedPFIDs(String[] selectedPFIDs) {
        this.selectedPFIDs = selectedPFIDs;
        accountModel.setPotfolios(selectedPFIDs);
        setTitle();
        checkMarginAvailability();
        updateMarginPanel();
    }

    public void checkMarginAvailability() {
        if (selectedPFIDs == null)
            setInitialData();
        if (selectedPFIDs != null) {
            for (int i = 0; i < selectedPFIDs.length; i++) {
                TradingPortfolioRecord record = TradingShared.getTrader().getPortfolio(selectedPFIDs[i]);
                if (record.isMarginEnabled()) {
                    isPortfolioMarginable = true;
                }
                if (record.isDayMarginEnabled()) {
                    isPortfolioMarginableDay = true;
                }
            }
            if (isPortfolioMarginable || isPortfolioMarginableDay) {
                pnlMarginViews.setVisible(true);
            } else {
                pnlMarginViews.setVisible(false);
            }

            isPortfolioMarginableDay = false;
            isPortfolioMarginable = false;
            this.doLayout();
        }
    }

    public void setSelectedCurrency(String currency) {
        lblSelectedCurrencyID.setText(currency);
    }

    private void portfolioButtonClicked() {
        if (TradingShared.isConnected()) {
            int popWidth = 0;
            PortfolioList pf = new PortfolioList(this, selectedPFIDs);
            if (Language.isLTR()) {
                popWidth = (int) pf.getPreferredSize().getWidth();
            }
            pf.show(btnAvailablePFs, btnAvailablePFs.getWidth() - popWidth, btnAvailablePFs.getHeight() - 4);
            pf = null;
        }
    }

    private void setCurrency(String currency) {
        String sItem = null;
        for (int i = 0; i < currencyIDs.length; i++) {
            sItem = currencyIDs[i];
            if (currency.equals(sItem)) {
                lblSelectedCurrencyID.setText(sItem);
                accountModel.setBaseCurrency(sItem);
            }
            sItem = null;
        }
        setDecimalPlaces();
    }

    public void show() {

        super.show();
        checkMarginAvailability();
        if (getisLoadedFromWsp()) {

            setisLoadedFromWsp(false);
            dim = accountSettings.getSize();
        } else if (dim == null) {
            dim = this.getSize();
            //resizeRowhight();
        } else {
            //  resizeRowhight();
        }

        // resizeRows(this.getSize(),311);
        //resizeRowhight();
    }

    public void setVisible(boolean value) {
        super.setVisible(value);

        /*if(TradeMethods.getSharedInstance().getIsAccountFrameCreated()  && TradeMethods.getSharedInstance().getAccountWindow().isShowing()){
            TradeMethods.getSharedInstance().getAccountWindow().resizeRowhight();
        }*/
    }


    private void setDecimalPlaces() {
        int decimalPlaces;
        try {
            if (!lblSelectedCurrencyID.getText().equals(""))
                decimalPlaces = com.isi.csvr.datastore.CurrencyStore.getSharedInstance().getDecimalPlaces(lblSelectedCurrencyID.getText());
            else
                decimalPlaces = 2;
        } catch (Exception e) {
//            e.printStackTrace();
            decimalPlaces = 2;
        }
        accountModel.setDecimalCount((byte) decimalPlaces);
    }

    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (e.getSource() == btnAvailablePFs) {
            portfolioButtonClicked();
        } else if (e.getSource() == lblAvailablePFs) {
            portfolioButtonClicked();
        } else if (e.getSource() == lblCurrencyIDs) {
            currencyButtonClicked();
        } else if (e.getSource() == btnCurrencyIDs) {
            currencyButtonClicked();
        }
    }

    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        super.mouseEntered(e);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        super.mouseExited(e);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void currencyAdded() {
        currencyIDs = new String[0];
        currencyPopup = null;
    }

    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        setCurrency(e.getActionCommand());
    }

    public void updateMarginPanel() {
        if (accountModel != null) {
            if (TradingShared.isConnected() && accountModel.isSelectedPortfoliosMarginable() && accountModel.isMarginMapsValid()) {
                double utilized = ((DoubleTransferObject) (accountModel.getValueAt(-5, 0))).getValue();
                double[] coverageval = (double[]) (accountModel.getValueAt(-6, 0));
                if ((!Double.isInfinite(utilized)) && (!Double.isNaN(utilized))) {
                    marginUtilized.setValue(utilized);
                    imglbl1.setText(formatter.format(utilized * 100) + "%");
                } else {
                    marginUtilized.setValue(Double.NaN);
                    imglbl1.setText("N/A");
                }
                if (coverageval != null && (!Double.isInfinite(coverageval[1]))) {

                    imglbl2.setCoverageValues(coverageval[0], coverageval[1]);
                } else {

                    imglbl2.setEmptyCoverageValues();
                }
            } else {
                marginUtilized.setValue(Double.NaN);
                imglbl1.setText("N/A");
                imglbl2.setEmptyCoverageValues();

            }
            this.doLayout();
        }
    }

    public void updateImages(double[] cov, double util) {
        if ((!Double.isInfinite(util)) && (!Double.isNaN(util))) {
            marginUtilized.setValue(util);
            imglbl1.setText(formatter.format(util * 100) + "%");
            imglbl1.repaint();
        } else {
            marginUtilized.setValue(Double.NaN);
            imglbl1.setText("N/A");
        }
        if (cov != null && (!Double.isInfinite(cov[1]))) {

            imglbl2.setCoverageValues(cov[0], cov[1]);
            imglbl2.repaint();
        } else {

            imglbl2.setEmptyCoverageValues();
        }
//         marginUtilized.setValue(util);
//         imglbl1.setText(formatter.format(util * 100) + "%");
//         imglbl1.repaint();
//          coverage.setValue(1-cov);
//         imglbl2.setText(formatter.format((1-cov) * 100) + "%");
//         imglbl2.repaint();
    }

    public Dimension getPreferredSize() {
        if (accountModel != null && accountModel.isSelectedPortfoliosMarginable()) {
            return new Dimension((int) super.getPreferredSize().getWidth(), (int) super.getPreferredSize().getHeight() + 35);
        } else {
            return super.getPreferredSize();
        }
    }

    /* Internal frame listener */ //todo
    /*public void internalFrameOpened(InternalFrameEvent e) {
        super.internalFrameOpened(e);
        if(TradeMethods.getSharedInstance().getIsAccountFrameCreated()  && TradeMethods.getSharedInstance().getAccountWindow().isShowing()){
            System.out.println("account frame opened");
            TradeMethods.getSharedInstance().getAccountWindow().resizeRowhight();
        }
    }*/
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applyTheme() {
        super.applyTheme();    //To change body of overridden methods use File | Settings | File Templates.
        imglbl1.setForeground(Theme.getColor("ACCOUNT_SUMMARY_MARGIN_UTILIZED_FG"));
        imglbl2.setForeground(Theme.getColor("ACCOUNT_SUMMARY_COVERAGE_FG"));
    }

    public boolean isInitdataFromUpdate() {
        return initdataFromUpdate;
    }

    public void setInitdataFromUpdate(boolean initdataFromUpdate) {
        this.initdataFromUpdate = initdataFromUpdate;
    }

    private class SimplevariationImage extends VariationImage {
        public static final int TYPE_COVERAGE = 0;
        public static final int TYPE_SPREAD = 1;
        public static final int TYPE_RANGE = 2;
        public static final int TYPE_CHANGE = 3;
        public static final int TYPE_TICK = 4;
        public static final int TICK_FILTER = 3;
        public static final int TYPE_OHLC_MAP = 5;
        public static final int TYPE_CASH_MAP = 6;
        public static final int TYPE_CASH_FLOW = 7;
//    public static final int TYPE_FULL_MAP=8;

        private int type;
        private int height = 10;
        private int width = 50;
        private int CLEARENCE = 4;
        private int LOW_CLEARENCE = 2;
        private Double value = Double.NaN;
        private int intValue = 0;
        private int imageWidth = 0;
        private String exchange;
        private double[] values;//=new double[13];
        private double[] fullMapValues = new double[7];


        public SimplevariationImage() {
            fullMapValues[0] = 20;
            fullMapValues[6] = 70;
            imageWidth = width - (CLEARENCE * 2);
        }

        private SimplevariationImage(int type) {
            this.type = type;
        }

        public int getIconHeight() {
            return height;
        }

        public int getIconWidth() {
            return imageWidth;
        }

        public void setValue(Double value) {
            if (value > 1) {
                value = 1.00;
            }
            this.value = value;
        }


        public void setWidth(int width) {
            this.width = width;
            imageWidth = width - (CLEARENCE * 2);
        }

        public void setHeight(int height) {
            this.height = height;
            CLEARENCE = height / 4;
        }

        public void setType(int type) {
            this.type = type;
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
            if (!Double.isNaN(value)) {
                try {
                    if (type == TYPE_COVERAGE) {
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(2, 2, (int) (value * (c.getWidth() - 4)), c.getHeight() - 4);
                        g.setColor(Theme.INDEXPANEL_CASHMAP_BORDER_COLOR);
                        g.drawRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
//                        g.drawRect(1, 1, c.getWidth() - 2, c.getHeight() - 3);
                    } else {
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(2, 2, (int) (value * (c.getWidth() - 4)), c.getHeight() - 4);
                        g.setColor(Theme.INDEXPANEL_CASHMAP_BORDER_COLOR);
                        //  g.drawRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
                        g.drawRect(0, 0, c.getWidth() - 2, c.getHeight() - 2);
                    }
                } catch (Exception e) {
                }
            }
        }


    }
}
