package com.isi.csvr.trading.ui;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.DetailQuote;
import com.isi.csvr.table.UpdateableTable;
import com.isi.csvr.table.updator.TableUpdateManager;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.CurrencyListener;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.trading.marginTrading.MarginCalculator;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.TransactRecord;
import com.isi.csvr.trading.shared.Account;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 17, 2004
 * Time: 2:50:44 PM
 */


public class AccountModel extends CommonTable
        implements DetailQuote, TableModel, CommonTableInterface, UpdateableTable, CurrencyListener {

    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private static StringTransferObject stringTransferObject = new StringTransferObject();
    private static boolean showNetSecurityAndPosition = false;
    boolean temp;
    String[] selectedPFs;
    String marginReferenceExchange = "*";
    private String currency = "";
    private double balance;
    private double buyingpower;
    private double adjustedBuyingpower;
    private double adjustedBuyingpowertemp;
    private double portfoliovalue;
    private double portfoliovaluetemp;
    private double blockedamount;
    private double odlimit;
    private double unrealizedSales;
    private double cashForWithdrawal;
    private double marginDue = 0;
    private double marginBlocked = 0;
    private double dayMarginDue = 0;
    private double dayMarginBlocked = 0;
    private double[] coverage = new double[]{0.00, 0.00};
    private double marginUtilized = 0;
    private double normalMargin = 0;
    private double dayMargin = 0;
    private double topUpAmount = 0;
    private double liquidationAmount = 0;
    //added on 6th of octorber 2008
    private double pendingTransfers;
    private double pendingDeposits;
    private double buyingPowerDay;
    private double netPortfolioPosition;
    private double netSecurity;
    private AccountWindow parent;
    private String[] selPortfolios;
    private ArrayList<Account> accounts = new ArrayList<Account>();
    private ArrayList<String> selPortfolioList = new ArrayList<String>();
    private boolean isPortfolioMarginable;
    private boolean isPortfolioMarginableDay;
    private boolean isCashAddToTotPF = false;

    /**
     * Constructor
     */
    public AccountModel(AccountWindow window) {
        parent = window;
        if (Settings.getBooleanItem("SHOW_NET_SECURITY_AND_POSITION"))
            showNetSecurityAndPosition = true;
        TableUpdateManager.addTable(this);
        CurrencyStore.getSharedInstance().addCurrencyListener(this);
    }

    public boolean isSelectedPortfoliosMarginable() {
        //       temp = !temp;
        //       return temp;
        calculateMarginValues();
        return (isPortfolioMarginable || isPortfolioMarginableDay);
    }

    public synchronized void setPotfolios(String[] portfolios) {
        //change start
        try {
            selPortfolios = portfolios;
            if (portfolios != null) {
                selectedPFs = portfolios;
                accounts.clear();
                accounts.trimToSize();
                selPortfolioList.clear();
                selPortfolioList.trimToSize();
                isPortfolioMarginable = false;
                isPortfolioMarginableDay = false;
                dayMargin = 0;
                normalMargin = 0;
                topUpAmount = 0;
                liquidationAmount = 0;
                for (int i = 0; i < portfolios.length; i++) {
                    if (!selPortfolioList.contains((portfolios[i]))) {
                        selPortfolioList.add(portfolios[i]);

                    }
                    TradingPortfolioRecord record = TradingShared.getTrader().getPortfolio(portfolios[i]);
                    if (record.isMarginEnabled()) {
                        isPortfolioMarginable = true;
                    }
                    if (record.isDayMarginEnabled()) {
                        isPortfolioMarginableDay = true;
                    }
                    Account account = TradingShared.getTrader().findAccount(record.getAccountNumber());
                    if (account != null && (!accounts.contains(account))) {
                        accounts.add(account);
                    }
                }

                if ((currency == null) || (currency.equals("") || currency.isEmpty())) {
                    currency = accounts.get(0).getCurrency();
                    parent.setSelectedCurrency(currency);
                    MarginCalculator.getSharedInstance().setCurrency(currency);
                }
                doCalculate();
            }
        } catch (Exception e) {
        }
        //change end
    }

    public synchronized void doCalculate() {
        balance = 0;
        buyingpower = 0;
        adjustedBuyingpower = 0;
        portfoliovalue = 0;
        blockedamount = 0;
        odlimit = 0;
        unrealizedSales = 0;
        cashForWithdrawal = 0;


        pendingTransfers = 0;
        pendingDeposits = 0;
        buyingPowerDay = 0;
        marginDue = 0;
        marginBlocked = 0;
        dayMarginDue = 0;
        dayMarginBlocked = 0;

        for (int i = 0; i < accounts.size(); i++) {
            balance += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getBalance(), accounts.get(i).getPath());
            // buyingpower += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getBuyingPower(), accounts.get(i).getPath());
//            buyingpower += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getBuyingPower(), accounts.get(i).getPath());
            blockedamount += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getBlockedAmount(), accounts.get(i).getPath());
            odlimit += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getODLimit(), accounts.get(i).getPath());
            unrealizedSales += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getUnrializedSales(), accounts.get(i).getPath());
            cashForWithdrawal += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getCashAvailableForWithdrawal(), accounts.get(i).getPath());
            pendingTransfers += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getPendingTransfers(), accounts.get(i).getPath());
            pendingDeposits += CurrencyStore.getAdjustedPriceForBuy(accounts.get(i).getCurrency(), currency, accounts.get(i).getPendingDeposites(), accounts.get(i).getPath());
        }

        if (showNetSecurityAndPosition) {
            netPortfolioPosition = CurrencyStore.getAdjustedPriceForBuy(accounts.get(0).getCurrency(), currency, accounts.get(0).getNetPosition(), accounts.get(0).getPath());
            netSecurity = CurrencyStore.getAdjustedPriceForBuy(accounts.get(0).getCurrency(), currency, accounts.get(0).getNetSecurityValue(), accounts.get(0).getPath());
        }

        double marketvalue = 0d;
        double margin = 0d;
        double marginDue = 0d;
        double dayMarginDue = 0d;
        //  double dayMargin = 0d;
        double marginBlock = 0d;
        for (int j = 0; j < selPortfolioList.size(); j++) {
            try {
                TradingPortfolioRecord portfolio = TradingShared.getTrader().getPortfolio(selPortfolioList.get(j));
                marketvalue += getMarketValue(portfolio.getPortfolioID());
                margin += CurrencyStore.getAdjustedPriceForBuy(portfolio.getCurrencyID(), currency, portfolio.getMarginPct(), portfolio.getPortfolioID());
                //  marginDue += CurrencyStore.getAdjustedPriceForBuy(portfolio.getCurrencyID(), currency, portfolio.getMarginDue(), portfolio.getPortfolioID());
                //  dayMarginDue += CurrencyStore.getAdjustedPriceForBuy(portfolio.getCurrencyID(), currency, portfolio.getDayMarginDue(), portfolio.getPortfolioID());
                //  marginBlock += CurrencyStore.getAdjustedPriceForBuy(portfolio.getCurrencyID(), currency, portfolio.getMarginBlocked(), portfolio.getPortfolioID());
                //   dayMargin += CurrencyStore.getAdjustedPriceForBuy(portfolio.getCurrencyID(), currency, portfolio.getDayMarginPct(), portfolio.getPortfolioID());
                portfolio = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        portfoliovalue = marketvalue;
        portfoliovaluetemp = portfoliovalue;
        double haircut = 0;

        //  adjustedBuyingpower = buyingpower + dayMargin + normalMargin;//; + cashMargin + portfolioMargin - portfolioMarginBlocked - portfolioMarginDue;
        adjustedBuyingpower = calculateBuyingPowers();
        adjustedBuyingpowertemp = adjustedBuyingpower;
//        adjustedBuyingpowerDay = buyingpower + dayCashMargin + portfolioMarginDay - portfolioMarginBlocked - portfolioDayMarginDue;
        if (adjustedBuyingpower < 0) {
            adjustedBuyingpower = 0;
        }

        //cashForWithdrawal -=  marginDue;
        marketvalue = 0d;
        if (isPortfolioMarginable || isPortfolioMarginableDay) {
            calculateMarginValues();
        }

    }

    private void calculateMarginValues() {
        if (selPortfolios != null && selectedPFs.length > 0) {
//            if(isPortfolioMarginable && isPortfolioMarginableDay){
//
//            }else if(isPortfolioMarginable ){
            double[] allocation = MarginCalculator.getSharedInstance().getTotalMarginDueMarginBlock(selectedPFs, currency);
            marginDue = allocation[0];
            marginBlocked = allocation[1];
            dayMarginDue = allocation[2];
            dayMarginBlocked = allocation[3];
            if (Double.compare(-0.00, dayMarginBlocked) == 0) {
                dayMarginBlocked = 0.00;
            }
            if (Double.compare(-0.00, marginBlocked) == 0) {
                marginBlocked = 0.00;
            }
            normalMargin = MarginCalculator.getSharedInstance().calculateNormalMargin(selectedPFs, currency);
            dayMargin = MarginCalculator.getSharedInstance().calculateDayMargin(selectedPFs, currency);
            topUpAmount = MarginCalculator.getSharedInstance().getTopUPamount(selectedPFs);
            liquidationAmount = MarginCalculator.getSharedInstance().getLiquidationAmount(selectedPFs);
            if (isMarginMapsValid() && getMarginMapPortfolio() != null) {
                String pfid = getMarginMapPortfolio();
                coverage = MarginCalculator.getSharedInstance().getCoveragevaluesForAccSummary(pfid, marginReferenceExchange);
                marginUtilized = MarginCalculator.getSharedInstance().getMarginUtilized(pfid);
                parent.updateImages(coverage, marginUtilized);
            }

        }
    }

    public double getMarketValue(String portfolioID) {
        double mValue = 0d;
        try {
            Stock stock = null;
            TransactRecord record = null;
            ArrayList dataStore = TradePortfolios.getInstance().getTransactionList();
            for (int i = 0; i < dataStore.size(); i++) {
                record = (TransactRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getKey().getExchange(), record.getKey().getSymbol());
                if (record.getPfID().equals(portfolioID)) {
                    try {
                        if (stock.getLastTradeValue() > 0) {
                            mValue += Math.abs(record.getQuantity()) * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getLastTrade(stock), record.getPfID());
                        } else {
                            mValue += Math.abs(record.getQuantity()) * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getPreviousClosed(stock), record.getPfID());
                        }
                    } catch (Exception e) {
                        //do nothing  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                record = null;
                stock = null;
            }
            return mValue;
        } catch (Exception e) {
            return mValue;//To change body of catch statement use File | Settings | File Templates.
        }
    }

    private double convertToSelectedCurrency(String exchangeCode, String sCurrency, boolean applyPriceModificationFactor, double value, String portfolio) {
        if (applyPriceModificationFactor) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            if (exchange != null)
//                return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
                return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
            else
//                return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
                return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
        } else {
//            return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
            return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
        }
    }

    private float convertToSelectedCurrency(String sCurrency, float value, String portfolio) {
        return value * (float) CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
    }

    public void setBaseCurrency(String currency) {
        if (currency != null) {
            this.currency = currency;
            MarginCalculator.getSharedInstance().setCurrency(currency);
            doCalculate();
        }
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 2;
    }

    public int getRowCount() {
        if (TradingShared.isReadyForTrading()) {
            if (isPortfolioMarginable && isPortfolioMarginableDay) {
                return 15;
            } else if (isPortfolioMarginable || isPortfolioMarginableDay) {
                return 13;
            } else {
                return 9;
            }
        } else {
            return 0;
        }
        //dayCashMargin + portfolioMarginDay
    }

    public Object getValueAt(int iRow, int iCol) {
        if (isPortfolioMarginableDay && (!isPortfolioMarginable) && iRow >= 11) {
            iRow = iRow + 2;
        }
        try {
            if (TWControl.isAccountSummaryFieldsEnable()) {
                switch (iRow) {
                    case -6:
                        return coverage;

                    case -5:
                        return doubleTransferObject.setValue(marginUtilized);
                    case -4:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                return doubleTransferObject.setValue(balance);
                        }
                    case -3:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                return doubleTransferObject.setValue(portfoliovaluetemp);
                        }
                    case -2:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                // return doubleTransferObject.setValue(dayCashMargin);
                                return doubleTransferObject.setValue(0);
                        }
                    case -1:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                // return doubleTransferObject.setValue(portfolioMarginDay);
                                return doubleTransferObject.setValue(0);
                        }
                    case 0:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                return doubleTransferObject.setValue(cashForWithdrawal);
                        }
                    case 1:
                        switch (iCol) {
                            case 0:
                                return Language.getString("UNRIALIZED_SALES");
                            case 1:
                                return doubleTransferObject.setValue(unrealizedSales);
                        }
                    case 2:
                        switch (iCol) {
//                        case 0:
//                            return Language.getString("ACCOUNT_BALANCE");
//                        case 1:
//                            return doubleTransferObject.setValue(balance);
                            case 0:
                                return Language.getString("BLOCKED_AMOUNT");
                            case 1:
                                return doubleTransferObject.setValue(blockedamount);
                        }
//                case 3:
//                    switch (iCol) {
////                        case 0:
////                            return Language.getString("BLOCKED_AMOUNT");
////                        case 1:
////                            return doubleTransferObject.setValue(blockedamount);
//                        case 0:
//                            return Language.getString("MARGIN_AMOUNT");
//                        case 1:
//                            return doubleTransferObject.setValue(cashMargin + portfolioMargin);
//                    }
                    case 3:
                        switch (iCol) {
//                        case 0:
//                            return Language.getString("MARGIN_AMOUNT");
//                        case 1:
//                            return doubleTransferObject.setValue(cashMargin + portfolioMargin);
                            case 0:
                                return Language.getString("OD_LIMIT");
                            case 1:
                                return doubleTransferObject.setValue(odlimit);
                        }
                    case 4:
                        switch (iCol) {
//                        case 0:
//                            return Language.getString("OD_LIMIT");
//                        case 1:
//                            return doubleTransferObject.setValue(odlimit);
                            case 0:
                                return Language.getString("PENDING_DEPOSITES");
                            case 1:
                                return doubleTransferObject.setValue(pendingDeposits);
                        }
                    case 5:
                        switch (iCol) {
//                        case 0:
//                            return Language.getString("BUYING_POWER");
//                        case 1:
//                            return doubleTransferObject.setValue(adjustedBuyingpower);
                            case 0:
                                return Language.getString("CASH_BALANCE");
                            case 1:
                                return doubleTransferObject.setValue(balance);
                        }
                    case 6:
                        if (showNetSecurityAndPosition) {
                            switch (iCol) {
                                case 0:
                                    return Language.getString("NET_SECURITY");
                                case 1:
                                    return doubleTransferObject.setValue(netSecurity);
                            }
                        } else {
                            switch (iCol) {
//                        case 0:
//                            return Language.getString("PORTFOLIO_VALUATION");
//                        case 1:
//                            return doubleTransferObject.setValue(portfoliovalue);
                                case 0:
                                    return Language.getString("HOLDINGS");
                                case 1:
                                    return doubleTransferObject.setValue(portfoliovaluetemp);
                            }
                        }

                    case 7:
                        if (showNetSecurityAndPosition) {
                            switch (iCol) {
                                case 0:
                                    return Language.getString("NET_PORTFOLIO_POSITION");
                                case 1:
                                    return doubleTransferObject.setValue(netPortfolioPosition);
                            }
                        } else {
                            switch (iCol) {
//                         case 0:
//                            return Language.getString("PENDING_TRANSFERS");
//                         case 1:
//                            return doubleTransferObject.setValue(pendingTransfers);
                                case 0:
                                    return Language.getString("TOTAL_PF");
                                case 1:
                                    if (isCashAddToTotPF) {
                                        return doubleTransferObject.setValue(balance + portfoliovalue);
                                    } else {
                                        return doubleTransferObject.setValue(portfoliovaluetemp);
                                    }
                            }
                        }
                    case 8:
                        switch (iCol) {
//                         case 0:
//                            return Language.getString("PENDING_DEPOSITES");
//                         case 1:
//                            return doubleTransferObject.setValue(pendingDeposits);
                            case 0:
                                return Language.getString("BUYING_POWER");
                            case 1:
                                return doubleTransferObject.setValue(adjustedBuyingpowertemp);
                        }
                    case 9:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_TOP_UP_AMT");
                            case 1:
                                return doubleTransferObject.setValue(topUpAmount);

                        }
                    case 10:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_LIQUIDATION_AMT");
                            case 1:
                                return doubleTransferObject.setValue(liquidationAmount);

                        }

                    case 11:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_MARGIN_DUE");
                            case 1:
                                return doubleTransferObject.setValue(marginDue);

                        }
                    case 12:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_BLOCKED_MARGIN");
                            case 1:
                                return doubleTransferObject.setValue(marginBlocked);

                        }
                    case 13:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_DAY_MARGIN_DUE");
                            case 1:
                                return doubleTransferObject.setValue(dayMarginDue);

                        }
                    case 14:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_BLOCKED_DAY_MARGIN");
                            case 1:
                                return doubleTransferObject.setValue(dayMarginBlocked);

                        }

                        /*
                  case 9:
                      switch (iCol) {
                          case 0:
                              return Language.getString("PORTFOLIO_MARGIN");
                          case 1:
                              return doubleTransferObject.setValue(portfolioMargin);
                      }
                  case 10:
                      switch (iCol) {
                          case 0:
                              return Language.getString("PORTFOLIO_MARGIN_BLOCKED");
                          case 1:
                              return doubleTransferObject.setValue(portfolioMarginBlocked);
                      }
                  case 11:
                      switch (iCol) {
                          case 0:
                              return Language.getString("PORTFOLIO_MARGIN_DUE");
                          case 1:
                              return doubleTransferObject.setValue(portfolioMarginDue);
                      }
                  case 12:
                      switch (iCol) {
                          case 0:
                              return Language.getString("PORTFOLIO_DAY_MARGIN_DUE");
                          case 1:
                              return doubleTransferObject.setValue(portfolioDayMarginDue);
                      }  */
                    default:
                        return "";
                }
            } else {
                switch (iRow) {
                    case -6:
                        return coverage;

                    case -5:
                        return doubleTransferObject.setValue(marginUtilized);
                    case -4:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                return doubleTransferObject.setValue(balance);
                        }
                    case -3:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                return doubleTransferObject.setValue(portfoliovaluetemp);
                        }
                    case -2:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                // return doubleTransferObject.setValue(dayCashMargin);
                                return doubleTransferObject.setValue(0);
                        }
                    case -1:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                // return doubleTransferObject.setValue(portfolioMarginDay);
                                return doubleTransferObject.setValue(0);
                        }
                        /*case 0:
                        switch (iCol) {
                            case 0:
                                return Language.getString("CASH_FOR_WITHDRAWAL");
                            case 1:
                                return doubleTransferObject.setValue(cashForWithdrawal);
                        }
                    case 1:
                        switch (iCol) {
                            case 0:
                                return Language.getString("UNRIALIZED_SALES");
                            case 1:
                                return doubleTransferObject.setValue(unrealizedSales);
                        }*/


                    case 0:
                        switch (iCol) {
//                        case 0:
//                            return Language.getString("BUYING_POWER");
//                        case 1:
//                            return doubleTransferObject.setValue(adjustedBuyingpower);
                            case 0:
                                return Language.getString("CASH_BALANCE");
                            case 1:
                                return doubleTransferObject.setValue(balance);
                        }

                    case 1:
                        switch (iCol) {
//                        case 0:
//                            return Language.getString("ACCOUNT_BALANCE");
//                        case 1:
//                            return doubleTransferObject.setValue(balance);
                            case 0:
                                return Language.getString("BLOCKED_AMOUNT");
                            case 1:
                                return doubleTransferObject.setValue(blockedamount);
                        }
//                case 3:
//                    switch (iCol) {
////                        case 0:
////                            return Language.getString("BLOCKED_AMOUNT");
////                        case 1:
////                            return doubleTransferObject.setValue(blockedamount);
//                        case 0:
//                            return Language.getString("MARGIN_AMOUNT");
//                        case 1:
//                            return doubleTransferObject.setValue(cashMargin + portfolioMargin);
//                    }
                    case 2:
                        switch (iCol) {
//                        case 0:
//                            return Language.getString("MARGIN_AMOUNT");
//                        case 1:
//                            return doubleTransferObject.setValue(cashMargin + portfolioMargin);
                            case 0:
                                return Language.getString("OD_LIMIT");
                            case 1:
                                return doubleTransferObject.setValue(odlimit);
                        }
                        /*case 4:
                        switch (iCol) {
//                        case 0:
//                            return Language.getString("OD_LIMIT");
//                        case 1:
//                            return doubleTransferObject.setValue(odlimit);
                            case 0:
                                return Language.getString("PENDING_DEPOSITES");
                            case 1:
                                return doubleTransferObject.setValue(pendingDeposits);
                        }*/

                    case 3:
                        if (showNetSecurityAndPosition) {
                            switch (iCol) {
                                case 0:
                                    return Language.getString("NET_SECURITY");
                                case 1:
                                    return doubleTransferObject.setValue(netSecurity);
                            }
                        } else {
                            switch (iCol) {
//                        case 0:
//                            return Language.getString("PORTFOLIO_VALUATION");
//                        case 1:
//                            return doubleTransferObject.setValue(portfoliovalue);
                                case 0:
                                    return Language.getString("HOLDINGS");
                                case 1:
                                    return doubleTransferObject.setValue(portfoliovaluetemp);
                            }
                        }

                    case 4:
                        switch (iCol) {
//                         case 0:
//                            return Language.getString("PENDING_DEPOSITES");
//                         case 1:
//                            return doubleTransferObject.setValue(pendingDeposits);
                            case 0:
                                return Language.getString("BUYING_POWER");
                            case 1:
                                return doubleTransferObject.setValue(adjustedBuyingpowertemp);
                        }

                    case 5:
                        if (showNetSecurityAndPosition) {
                            switch (iCol) {
                                case 0:
                                    return Language.getString("NET_PORTFOLIO_POSITION");
                                case 1:
                                    return doubleTransferObject.setValue(netPortfolioPosition);
                            }
                        } else {
                            switch (iCol) {
//                         case 0:
//                            return Language.getString("PENDING_TRANSFERS");
//                         case 1:
//                            return doubleTransferObject.setValue(pendingTransfers);
                                case 0:
                                    return Language.getString("TOTAL_PF");
                                case 1:
                                    if (isCashAddToTotPF) {
                                        return doubleTransferObject.setValue(balance + portfoliovalue);
                                    } else {
                                        return doubleTransferObject.setValue(portfoliovaluetemp);
                                    }
                            }
                        }

                        /*case 6:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_TOP_UP_AMT");
                            case 1:
                                return doubleTransferObject.setValue(topUpAmount);

                        }
                    case 7:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_LIQUIDATION_AMT");
                            case 1:
                                return doubleTransferObject.setValue(liquidationAmount);

                        }

                    case 8:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_MARGIN_DUE");
                            case 1:
                                return doubleTransferObject.setValue(marginDue);

                        }
                    case 9:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_BLOCKED_MARGIN");
                            case 1:
                                return doubleTransferObject.setValue(marginBlocked);

                        }
                    case 10:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_DAY_MARGIN_DUE");
                            case 1:
                                return doubleTransferObject.setValue(dayMarginDue);

                        }
                    case 11:
                        switch (iCol) {
                            case 0:
                                return Language.getString("MARGIN_TRADING_BLOCKED_DAY_MARGIN");
                            case 1:
                                return doubleTransferObject.setValue(dayMarginBlocked);

                        }*/

                        /*
                  case 9:
                      switch (iCol) {
                          case 0:
                              return Language.getString("PORTFOLIO_MARGIN");
                          case 1:
                              return doubleTransferObject.setValue(portfolioMargin);
                      }
                  case 10:
                      switch (iCol) {
                          case 0:
                              return Language.getString("PORTFOLIO_MARGIN_BLOCKED");
                          case 1:
                              return doubleTransferObject.setValue(portfolioMarginBlocked);
                      }
                  case 11:
                      switch (iCol) {
                          case 0:
                              return Language.getString("PORTFOLIO_MARGIN_DUE");
                          case 1:
                              return doubleTransferObject.setValue(portfolioMarginDue);
                      }
                  case 12:
                      switch (iCol) {
                          case 0:
                              return Language.getString("PORTFOLIO_DAY_MARGIN_DUE");
                          case 1:
                              return doubleTransferObject.setValue(portfolioDayMarginDue);
                      }  */
                    default:
                        return "";
                }
            }

        } catch (Exception e) {
            return "";
        }
    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 3:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 4:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 5:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 6:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 7:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 8:
                switch (iCol) {
                    case 0:
                        return 'v';
                    case 1:
                        return 'x';
                }
            case 9:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 10:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 11:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 12:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 13:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }
            case 14:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                }

        }
        return 0;
    }

    public long getTimeOffset() {
        return 0;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        try {
            return getValueAt(0, iCol).getClass();
        } catch (Exception e) {
            return Object.class;
            //e.printStackTrace();
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[3];
        customizerRecords[0] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[1] = new CustomizerRecord(Language.getString("ROW_COLOR_1"), FIELD_BASIC_ROW1, Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("ROW_COLOR_2"), FIELD_BASIC_ROW2, Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2"), Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2"));
        return customizerRecords;
    }

    public void runThread() {
        try {
            if ((selPortfolios != null) && (accounts.size() == 0)) {
                setPotfolios(selPortfolios);
            }
            doCalculate();
        } catch (Exception e) {
        }
    }

    public String getTitle() {
        return "Account Table Model";
    }

    public Constants.ThreadTypes getTableID() {
        return Constants.ThreadTypes.DEFAULT;
    }

    public void currencyAdded() {
        doCalculate();
    }

    public void setSymbol(String symbol) {

    }

    public boolean isMarginMapsValid() {
        if (selectedPFs != null && selectedPFs.length > 0) {
            int count = 0;
            for (int i = 0; i < selectedPFs.length; i++) {
                TradingPortfolioRecord record = TradingShared.getTrader().getPortfolio(selectedPFs[i]);
                if (record.isMarginable()) {
                    count = count + 1;
                }
            }
            if (count == 1) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    private String getMarginMapPortfolio() {
        if (selectedPFs != null && selectedPFs.length > 0) {

            for (int i = 0; i < selectedPFs.length; i++) {
                TradingPortfolioRecord record = TradingShared.getTrader().getPortfolio(selectedPFs[i]);
                if (record.isMarginable()) {
                    return selectedPFs[i];
                }
            }
            return null;

        } else {
            return null;
        }
    }

    public void resetMarginValues() {
        isPortfolioMarginableDay = false;
        isPortfolioMarginable = false;

    }

    private double calculateBuyingPowers() {
        if (accounts != null && selectedPFs != null) {
            Hashtable<String, Double> table = new Hashtable<String, Double>();
            Hashtable<String, Account> curr = new Hashtable<String, Account>();
            for (int i = 0; i < accounts.size(); i++) {
                table.put(accounts.get(i).getAccountID(), 0D);
                curr.put(accounts.get(i).getAccountID(), accounts.get(i));
            }
            for (int j = 0; j < selectedPFs.length; j++) {
                String record = TradingShared.getTrader().getPortfolio(selectedPFs[j]).getAccountNumber();
                Double val = MarginCalculator.getSharedInstance().getBuyingPower(selectedPFs[j], "*");
                Double exsisting = table.get(record);
                table.put(record, (Double) Math.max(val, exsisting));

            }
            double power = 0;
            Enumeration<String> en = table.keys();
            while (en.hasMoreElements()) {
                String key = en.nextElement();

//                power = power + CurrencyStore.getAdjustedPriceForBuy(curr.get(key).getCurrency(), currency, table.get(key), curr.get(key).getPath());
                power = power + table.get(key);
                // power = power +   table.get(key);
            }
            return power;
        } else {
            return 0;
        }
    }

    private void calculateRatios(ArrayList<Account> accounts) {

    }

    public void setMarginReferenceExchange(String marginReferenceExchange) {
        this.marginReferenceExchange = marginReferenceExchange;
    }

    public void setCashAddToTotPF(boolean cashAddToTotPF) {
        isCashAddToTotPF = cashAddToTotPF;
    }

    public void clear() {
        cashForWithdrawal = 0;
        unrealizedSales = 0;
        blockedamount = 0;
        odlimit = 0;
        pendingDeposits = 0;
        balance = 0;
        portfoliovalue = 0;
        adjustedBuyingpower = 0;


    }

}