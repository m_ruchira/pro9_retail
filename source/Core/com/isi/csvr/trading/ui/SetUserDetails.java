package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 8, 2006
 * Time: 4:27:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class SetUserDetails extends JDialog implements ActionListener {

    private static int PASSPORT_NO = 1;
    private static int NIC_NO = 2;
    private static int COMPANY_REG_NO = 3;
    private JLabel lblTop;
    private JLabel lblInvesterNo;
    private JLabel lblPassportNo;
    private JLabel lblNICNo;
    private JLabel lblPOBoxNo;
    private JLabel lblCompRegNo;
    private JTextField fldInvesterNo;
    private JTextField fldPassportNo;
    private JTextField fldNICNo;
    private JTextField fldPOBoxNo;
    private JTextField fldCompRegNo;
    private TWButton btnSubmit;
    private TWButton btnCancel;
    private JPanel mainPanel;
    private JPanel labelPanel;
    private JPanel buttonPanel;
    private JLabel lblmessage;
    private TWComboBox cmbList;
    private ArrayList<TWComboItem> portfolioList;
    private TWTextField txtNumber;
    private String selectedString = null;
    private String strInvesterNo = "";
    private String strPassportNo = "";
    //    private String message = null;
    private String strNICNo = "";
    private String strPOBoxNo = "";
    private String strCompRegNo = "";
    private boolean isSuccess = false;

    public SetUserDetails(Frame parent, String title) {
        super(parent, title, true);
//        this.message = Language.getString("TRADE_USER_VERIFICATION_MESSAGE");
        try {
            createUI();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void createUI() {
        this.setSize(320, 200);  //260
        mainPanel = new JPanel();
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"0", "40", "35"}, 10, 10)); // 155
        labelPanel = new JPanel();
        labelPanel.setLayout(new FlexGridLayout(new String[]{"46%", "54%"}, new String[]{"20", "20", "20", "20", "20"}, 10, 10));
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING, 10, 5));

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new FlexGridLayout(new String[]{"10", "100%"}, new String[]{"100%"}));
        topPanel.add(new JLabel(""));
        lblTop = new JLabel(Language.getString("TRADE_USER_VERIFICATION_MESSAGE"));
        topPanel.add(lblTop);
//        if(message!=null){
//            lblTop.setText(message);
//        }
        lblInvesterNo = new JLabel(Language.getString("INVESTER_NO"));
        lblPassportNo = new JLabel(Language.getString("PASSPORT_NO"));
        lblNICNo = new JLabel(Language.getString("NIC_NO"));
        lblPOBoxNo = new JLabel(Language.getString("POBOX_NO"));
        lblCompRegNo = new JLabel(Language.getString("COMPANY_REG_NO"));

        fldInvesterNo = new JTextField("", 15);
        fldPassportNo = new JTextField("", 15);
        fldNICNo = new JTextField("", 15);
        fldPOBoxNo = new JTextField("", 15);
        fldCompRegNo = new JTextField("", 15);

        labelPanel.add(lblInvesterNo);
        labelPanel.add(fldInvesterNo);
        labelPanel.add(lblPassportNo);
        labelPanel.add(fldPassportNo);
        labelPanel.add(lblNICNo);
        labelPanel.add(fldNICNo);
        labelPanel.add(lblPOBoxNo);
        labelPanel.add(fldPOBoxNo);
        labelPanel.add(lblCompRegNo);
        labelPanel.add(fldCompRegNo);

        btnSubmit = new TWButton(Language.getString("OK"));
        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnSubmit.setPreferredSize(new Dimension(70, 25));
        btnCancel.setPreferredSize(new Dimension(75, 25));
        btnSubmit.addActionListener(this);
        btnCancel.addActionListener(this);

        buttonPanel.add(btnSubmit);
        buttonPanel.add(btnCancel);

//        mainPanel.add(topPanel);
//        mainPanel.add(labelPanel);
//        mainPanel.add(buttonPanel);

        lblmessage = new JLabel(Language.getString("MSG_FIRST_TIME_DETAILS"));
        portfolioList = new ArrayList<TWComboItem>();
        cmbList = new TWComboBox(new TWComboModel(portfolioList));
        String list = TradingShared.getQuestionList();
        StringTokenizer tokens = new StringTokenizer(list, Meta.ID);
        while (tokens.hasMoreElements()) {
            String str = tokens.nextToken();
            portfolioList.add(new TWComboItem(str.substring(0, 1), Language.getLanguageSpecificString(str.substring(1))));
        }

//        portfolioList.add(new TWComboItem(PASSPORT_NO ,Language.getString("PASSPORT_NO")));
//        portfolioList.add(new TWComboItem(NIC_NO, Language.getString("NIC_NO")));
//        portfolioList.add(new TWComboItem(COMPANY_REG_NO, Language.getString("COMPANY_REG_NO")));
        cmbList.setSelectedIndex(0);
        txtNumber = new TWTextField();
        JPanel newPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"25"}, 10, 10));
        newPanel.add(cmbList);
        newPanel.add(txtNumber);
        mainPanel.add(lblmessage);
        mainPanel.add(newPanel);
        mainPanel.add(buttonPanel);
        this.getContentPane().add(mainPanel);
        this.setResizable(false);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(Client.getInstance().getFrame());
    }

    private boolean validateInput() {
//        int count = 0;
//        strInvesterNo = fldInvesterNo.getText();
//        strPassportNo = fldPassportNo.getText();
//        strNICNo = fldNICNo.getText();
//        strPOBoxNo = fldPOBoxNo.getText();
//        strCompRegNo = fldCompRegNo.getText();
//
//        if(!strInvesterNo.equals("")){
//            count ++;
//        }
//        if(!strPassportNo.equals("")){
//            count++;
//        }
//        if(!strNICNo.equals("")){
//            count++;
//        }
//        if(!strPOBoxNo.equals("")){
//            count++;
//        }
//        if(!strCompRegNo.equals("")){
//            count++;
//        }
//        if(count>0){
//            return true;
//        }else{
//            return false;
//        }

        if ((txtNumber.getText() != null) || (!txtNumber.getText().equals(""))) {
            selectedString = txtNumber.getText();
            return true;
        } else {
            return false;
        }
    }

    public void actionPerformed(ActionEvent e) {
        boolean isValid;
        if (e.getSource() == btnSubmit) {
            isValid = validateInput();
            if (isValid) {
                sendRequest();
                isSuccess = true;

                System.out.println("Input validated in SetUserDetails dialog");
                close();
            } else {
                isSuccess = false;
            }
        } else if (e.getSource() == btnCancel) {
            isSuccess = false;
            Client.getInstance().disconnectFromTradingServer();
            close();
        }
    }

    private boolean sendRequest() {
        try {
//            int  selecteditem = Integer.parseInt (((TWComboItem)cmbList.getSelectedItem()).getId()) ;
            TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_AUTH_VALIDITY);
            tradeMessage.addData("A" + TradingShared.getTrader().getMubasherID(Constants.PATH_PRIMARY));
            tradeMessage.addData("B" + TradingShared.getTrader().getUserID(Constants.PATH_PRIMARY));
            tradeMessage.addData(((TWComboItem) cmbList.getSelectedItem()).getId() + UnicodeUtils.getUnicodeString(selectedString));
//            if(selecteditem==PASSPORT_NO){
//                tradeMessage.addData("D" + UnicodeUtils.getUnicodeString(selectedString));
//            }else if(selecteditem==NIC_NO){
//                tradeMessage.addData("G" + UnicodeUtils.getUnicodeString(selectedString));
//            }else if(selecteditem==COMPANY_REG_NO){
//                tradeMessage.addData("F" + UnicodeUtils.getUnicodeString(selectedString));
//            }
//            if(!strInvesterNo.equals(""))
//                tradeMessage.addData("C" + UnicodeUtils.getUnicodeString(strInvesterNo));
//            if(!strPassportNo.equals(""))
//                tradeMessage.addData("D" + UnicodeUtils.getUnicodeString(strPassportNo));
//            if(!strPOBoxNo.equals(""))
//                tradeMessage.addData("E" + UnicodeUtils.getUnicodeString(strPOBoxNo));
//            if(!strCompRegNo.equals(""))
//                tradeMessage.addData("F" + UnicodeUtils.getUnicodeString(strCompRegNo));
//            if(!strNICNo.equals(""))
//                tradeMessage.addData("G" + UnicodeUtils.getUnicodeString(strNICNo));
            com.isi.csvr.trading.SendQueue.getSharedInstance().writeData(tradeMessage.toString(), Constants.PATH_PRIMARY);
            System.out.println("User validation request  = " + tradeMessage.toString());
            tradeMessage = null;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showDialog() {
        this.setVisible(true);
        return isSuccess;
    }

    public void close() {
        strInvesterNo = null;
        strCompRegNo = null;
        strNICNo = null;
        strPassportNo = null;
        strPOBoxNo = null;
        this.dispose();
    }
}
