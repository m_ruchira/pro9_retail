package com.isi.csvr.trading.ui;

import com.isi.csvr.shared.DynamicArray;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA. User: admin Date: 28-Sep-2007 Time: 10:47:12 To change this template use File | Settings |
 * File Templates.
 */
public class ConditionTableModel implements TableModel {
    private DynamicArray store;
    private String[] columns;
    private JTable table;

    public ConditionTableModel(JTable table, DynamicArray store, String[] columns) {
        this.store = store;
        this.table = table;
        this.columns = columns;
    }

    public int getRowCount() {
        return 5;
    }

    public int getColumnCount() {
        return columns.length;
    }

    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }

    public Class getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 5:
                return Number.class;
            default:
                return String.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
//        if (columnIndex == 6) {
//            if   (rowIndex < (store.size() - 1))
//                return true;
//            else
//                return false;
//        }else {
//            return false;
//        }
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            ConditionObject conObject = (ConditionObject) (store.get(rowIndex));

            switch (columnIndex) {
                case 0:
                    return conObject.getField();
                case 1:
                    return conObject.getField();
                case 2:
                    return conObject.getSymbol();
                case 3:
                    return conObject.getField();
                case 4:
                    return conObject.getCondition();
                case 5:
                    return conObject.getValue();
//                case 6:
//                    if   (rowIndex < (store.size() - 1))
//                        return ((FunctionBuilderComboItem) store.getOperator(rowIndex)).getDescription();
//                    else
//                        return "";
            }
        } catch (Exception e) {
            return "";
        }
        return "";
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

        try {
            ConditionObject conObject = (ConditionObject) (store.get(rowIndex));

            switch (columnIndex) {
                case 2:
                    conObject.setField((String) aValue);
                    break;
                case 3:
                    conObject.setCondition((String) aValue);
                    break;
                case 4:
                    conObject.setValue(Double.parseDouble((String) aValue));
                    break;
                default:
//                    store.setOperator(rowIndex, aValue);
            }
        } catch (Exception e) {

        }
        table.updateUI();
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }
}
