package com.isi.csvr.trading.ui;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.trading.datastore.OpenPositionRecord;
import com.isi.csvr.trading.datastore.OpenPositionsStore;
import com.isi.csvr.trading.shared.Account;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.table.TableModel;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Nov 17, 2007
 * Time: 12:55:40 PM
 */
public class OpenPositionsModel extends CommonTable
        implements TableModel, CommonTableInterface, ClipboardOwner, DDELinkInterface, ExchangeListener {

    private List<OpenPositionRecord> records;
    private DoubleTransferObject doubleTransferObject;
    private LongTransferObject longTransferObject;

    public OpenPositionsModel() {
        doubleTransferObject = new DoubleTransferObject();
        longTransferObject = new LongTransferObject();
    }

    public int getRowCount() {
        try {
            if (records == null) {
                records = OpenPositionsStore.getSharedInstance().getRecords();
            }
            return records.size();
        } catch (Exception e) {
            return 0;
        }
    }

    public int getColumnCount() {
        return 15;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if (records == null) {
            records = OpenPositionsStore.getSharedInstance().getRecords();
        }

        //.,.,Exchange,Symbol,Portfolio,Value Date,Side,Open Amount,Open Price,Closed Amount,Current Price,Profit/Loss,%Price
        try {
            switch (columnIndex) {
                case -104:
                    return records.get(rowIndex).getOpenQuantity() - records.get(rowIndex).getClosedQuantity();
                case -103:
                    return records.get(rowIndex).getSide();
                case -102:
                    return records.get(rowIndex).getKey();
                case -101:
                    return OpenPositionsStore.getSharedInstance().getExpandable(records.get(rowIndex).getKey()).isExpanded();
                case 0:
                    return (!records.get(rowIndex).isBaseRecord()) && (records.get(rowIndex).getOpenQuantity() > records.get(rowIndex).getClosedQuantity());
                case 1:
                    return records.get(rowIndex).isBaseRecord();
                case 2:
//                    return records.get(rowIndex).getExchange();
                    return ExchangeStore.getSharedInstance().getExchange(records.get(rowIndex).getExchange().trim()).getDisplayExchange(); // To display exchange
                case 3:
                    return TradingShared.getDisplaySmbol(records.get(rowIndex).getSymbol(), records.get(rowIndex).getPortfolio());
                case 4:
                    return records.get(rowIndex).getPortfolio();
                case 5:
                    return longTransferObject.setValue(records.get(rowIndex).getDate());
                case 6:
                    return TradingShared.getForexActionString(records.get(rowIndex).getSide());
                case 7:
                    return longTransferObject.setValue(records.get(rowIndex).getOpenQuantity());
                case 8:
                    if (records.get(rowIndex).isBaseRecord()) {
                        return doubleTransferObject.setValue(Double.POSITIVE_INFINITY);
                    } else {
                        return doubleTransferObject.setValue(records.get(rowIndex).getOpenPrice());
                    }
                case 9:
                    return longTransferObject.setValue(records.get(rowIndex).getClosedQuantity());
                case 10:
                    return getCurrentPrice(records.get(rowIndex).getKey());
                case 11:
                    if (records.get(rowIndex).isBaseRecord()) {
                        return "0.0";//doubleTransferObject.setValue(Double.POSITIVE_INFINITY);
                    } else {
//                        return doubleTransferObject.setValue(getProfitRelative(records.get(rowIndex).getKey(), records.get(rowIndex).getOpenPrice(), records.get(rowIndex).getSide(), records.get(rowIndex).getOpenQuantity(), records.get(rowIndex).getStatus()));
                        return getProfitRelative(records.get(rowIndex).getKey(), records.get(rowIndex).getOpenPrice(), records.get(rowIndex).getSide(), records.get(rowIndex).getOpenQuantity(), records.get(rowIndex).getStatus());
                    }
                case 12:
                    return "0"; // currently not used
                case 13:
                    return TradingShared.getForexStatusString(records.get(rowIndex).getStatus());
                case 14:
                    if (records.get(rowIndex).isBaseRecord()) {
                        return "0.0";//doubleTransferObject.setValue(Double.POSITIVE_INFINITY);
                    } else {
//                        return doubleTransferObject.setValue(getProfit(records.get(rowIndex).getKey(), records.get(rowIndex).getOpenPrice(), records.get(rowIndex).getSide(), records.get(rowIndex).getOpenQuantity(), records.get(rowIndex).getStatus(),records.get(rowIndex).getPortfolio()));
                        return getProfit(records.get(rowIndex).getKey(), records.get(rowIndex).getOpenPrice(), records.get(rowIndex).getSide(), records.get(rowIndex).getOpenQuantity(), records.get(rowIndex).getStatus(), records.get(rowIndex).getPortfolio());
                    }

            }
        } catch (Exception e) {
            return "";
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return "";
    }

    private String getProfitRelative(String key, double openPrice, int side, long openQty, int status) {
        double currentPrice = 0.0;
        try {
            Stock st = DataStore.getSharedInstance().getStockObject(key);
            if ((st.getInstrumentType() == Meta.INSTRUMENT_FOREX) || (st.getInstrumentType() == Meta.INSTRUMENT_FUTURE)) {
                currentPrice = st.getBestAskPrice();

            }
            st = null;
            if (status == 0) { //if position remains poen
                if (side == TradeMeta.SHORT) {
                    return (openPrice - currentPrice) * openQty + "~" + SharedMethods.getSymbolFromKey(key).substring(3);
                } else {
                    return (currentPrice - openPrice) * openQty + "~" + SharedMethods.getSymbolFromKey(key).substring(3);
                }
            } else {
                return "0.0";    // not calculating profit loss for closed positions
            }


        } catch (Exception e) {
            return "0.0";
        }
    }

    private String getProfit(String key, double openPrice, int side, long openQty, int status, String pfID) {
        double currentPrice = 0.0;
        Account account = TradingShared.getTrader().findAccountByPortfolio(pfID);
        try {
            Stock st = DataStore.getSharedInstance().getStockObject(key);
            String stockCurr = SharedMethods.getSymbolFromKey(key).substring(3);
            if ((st.getInstrumentType() == Meta.INSTRUMENT_FOREX) || (st.getInstrumentType() == Meta.INSTRUMENT_FUTURE)) {
                currentPrice = st.getBestAskPrice();

            }
            st = null;
            if (status == 0) { //if position remains poen
                if (side == TradeMeta.SHORT) {
                    return convertToDefault(stockCurr, account.getCurrency(), (openPrice - currentPrice) * openQty, pfID) + "~" + account.getCurrency();
                } else {
                    return convertToDefault(stockCurr, account.getCurrency(), (currentPrice - openPrice) * openQty, pfID) + "~" + account.getCurrency();
                }
            } else {
                return "0.0";    // not calculating profit loss for closed positions
            }


        } catch (Exception e) {
//            e.printStackTrace();
            return "0.0";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return String.class;
    }

    private DoubleTransferObject getCurrentPrice(String key) {
        DoubleTransferObject currentPrice = new DoubleTransferObject();
        try {
            Stock st = DataStore.getSharedInstance().getStockObject(key);
            if ((st.getInstrumentType() == Meta.INSTRUMENT_FOREX) || (st.getInstrumentType() == Meta.INSTRUMENT_FUTURE)) {
                if (st.getBestAskPrice() != 0) {
                    currentPrice.setValue(st.getBestAskPrice());
                } else {
                    currentPrice.setValue(st.getPreviousClosed());
                }

            }
            st = null;
            return currentPrice;

        } catch (Exception e) {
            return currentPrice;
        }

    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {

    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void getDDEString(Table table, boolean withHeadings) {

    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }

    public void setSymbol(String symbol) {

    }

    private double convertToDefault(String selectedCurrency, String baseCurrency, double value, String portfolio) {
        return value * CurrencyStore.getBuyRate(selectedCurrency, baseCurrency, TradingShared.getTrader().getPath(portfolio));
    }
}
