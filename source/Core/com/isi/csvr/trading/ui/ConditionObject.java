package com.isi.csvr.trading.ui;

/**
 * Created by IntelliJ IDEA. User: admin Date: 01-Oct-2007 Time: 12:05:15 To change this template use File | Settings |
 * File Templates.
 */
public class ConditionObject {


    public int conditionType;
    private String symbol;
    private String field;
    private String condition;
    private Double value;
    private int type;

    public ConditionObject() {

    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public int getConditionType() {
        return conditionType;
    }

    public void setConditionType(int conditionType) {
        this.conditionType = conditionType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

}
