package com.isi.csvr.trading.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 25, 2004
 * Time: 1:01:54 PM
 */
public interface TraderProfileDataListener {

    public void accountDataChanged(String accountID);

    public void portfolioDataChanged(String portfolioID);
}
