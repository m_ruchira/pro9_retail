package com.isi.csvr.trading.shared;

import com.dfn.mtr.mix.beans.AuthenticationResponse;
import com.dfn.mtr.mix.beans.MIXHeader;
import com.dfn.mtr.mix.beans.MIXObject;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.shared.Constants;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jul 23, 2009
 * Time: 2:44:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class MixStore {

    private static MixStore self;
    private MIXObject primaryAuthenticationObj;
    private MIXObject secondaryAuthenticationObj;

    private MixStore() {

    }

    public static MixStore getSharedInstance() {
        if (self == null) {
            self = new MixStore();
        }
        return self;
    }

    public void clear(byte logintype) {
        if (logintype == Constants.PATH_PRIMARY) {
            primaryAuthenticationObj = null;
            secondaryAuthenticationObj = null;
        } else {
            secondaryAuthenticationObj = null;
        }
    }

    public void setPrimaryAuthenticationObj(MIXObject primaryAuthenticationObj) {
        this.primaryAuthenticationObj = primaryAuthenticationObj;
    }

    public void setSecondaryAuthenticationObj(MIXObject secondaryAuthenticationObj) {
        this.secondaryAuthenticationObj = secondaryAuthenticationObj;
    }

    public AuthenticationResponse getAuthenticationResponse(byte path) {
        try {
            if (path == Constants.PATH_PRIMARY) {
                return (AuthenticationResponse) primaryAuthenticationObj.getTransferObjects()[0];
            } else if (path == Constants.PATH_SECONDARY) {
                return (AuthenticationResponse) secondaryAuthenticationObj.getTransferObjects()[0];
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }

    public MIXHeader getAuthenticationResponseHeader(byte path) {
        try {
            if (path == Constants.PATH_PRIMARY) {
                return primaryAuthenticationObj.getMIXHeader();
            } else if (path == Constants.PATH_SECONDARY) {
                return secondaryAuthenticationObj.getMIXHeader();
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }

    public MIXObject getMixObject(int group, int requestType, byte path) {
        MIXObject baseMixObject = new MIXObject();
        MIXHeader header = new MIXHeader();
        header.setGroup(group);
        header.setRequestType(requestType);
        header.setSessionID(getAuthenticationResponse(path).getSSessionID());
//        header.setChannelID(MIXConstants.CHANNEL_TYPE_TW);
        header.setChannelID(MIXConstants.CHANNEL_TYPE_NEWTW);
        baseMixObject.setMIXHeader(header);
        return baseMixObject;
    }


}
