package com.isi.csvr.trading.shared;

import bsh.EvalError;
import bsh.Interpreter;
import com.isi.csvr.MarketTimer;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.trading.datastore.Rule;
import com.isi.csvr.trading.datastore.RuleManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class TradingPortfolioRecord {
    TWDateFormat formater = new TWDateFormat("HH:mm:ss");
    Calendar cal = Calendar.getInstance();
    private String portfolioID;
    private String name;
    private String accountNumber = "0";
    private String brokerID;
    private int status;
    private boolean defaultPortfolio;
    private String allowedExchanges;
    // marginPct related
    private boolean marginEnabled;
    private boolean dayMarginEnabled;
    private double marginPct;
    private double dayMarginPct;
    // private double marginBlocked;
    //private double marginDue;
    //private double dayMarginDue;
    private long dayLiquidationTime;
    private double maxMarginAmt;
    private double maxDayMarginAmt;
    private ArrayList bookKeepers;
    private ArrayList<String> exchanges = new ArrayList<String>();

    /*
    [9:03:53 AM] Niroshan Serasinghe says: "A" + PortfolioAccount + TRSMeta.FD + "B" + marginEnabledStatus +
    TRSMeta.FD + "C" + dayMarginEnabledStatus + TRSMeta.FD + "D" + marginPct + TRSMeta.FD + "E" + margin_block +
    TRSMeta.FD + "H" + margin_due + TRSMeta.FD + "G" + day_margin_due() + TRSMeta.FD
     */

    public TradingPortfolioRecord(String portfolioID, String brokerID, int status,
                                  String account, boolean defaultPortfolio, String name, String allowedExchanges, ArrayList bookKeepers) {
        this.brokerID = brokerID;
        this.status = status;
        this.accountNumber = account;
        this.defaultPortfolio = defaultPortfolio;
        this.name = name;
        this.portfolioID = portfolioID;
        this.allowedExchanges = allowedExchanges;
        this.bookKeepers = bookKeepers;
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        setExchanges();
    }

    private void setExchanges() {
        if (allowedExchanges != null) {
            String[] exchangesStr = allowedExchanges.split(",");
            for (int i = 0; i < exchangesStr.length; i++) {
                exchanges.add(exchangesStr[i]);
            }
        }
    }

    public ArrayList<String> getExchangeList() {
        return exchanges;
    }

    public String getExchangeString() {
        try {
            String exchangeString = "";
            for (String exchange : exchanges) {
                exchangeString += ",";
                exchangeString += exchange;

            }
            return exchangeString.substring(1);
        } catch (Exception e) {
            return "";
        }
    }

    public boolean isDefaultPortfolio() {
        return defaultPortfolio;
    }

    public String getPortfolioID() {
        return portfolioID;
    }

    public String getName() {
        if (Settings.getBooleanItem("SHOW_PORTFOLIO_IDS"))
            return portfolioID;
        if ((name == null) || (name.trim().equals("")))
            return portfolioID;
        else
            return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getBrokerID() {
        return brokerID;
    }

    public int getStatus() {
        return status;
    }

    public String getCurrencyID() {
        try {
            return (TradingShared.getTrader().findAccount(accountNumber)).getCurrency();
        } catch (Exception e) {
            return "NA";
        }
    }


    public boolean isMarginEnabled() {
        return marginEnabled;
    }

    public void setMarginEnabled(boolean marginEnabled) {
        this.marginEnabled = marginEnabled;
    }

    public double getMarginPct() {
        return marginPct;
    }

//    public double getApplicableMarginPct(boolean isMktClosed) {
//        if (marginEnabled && dayMarginEnabled) {
//            if (isMarginTransisitionPassed()) {
//                return marginPct;
//            } else {
//                return dayMarginPct;
//            }
//        } else if (marginEnabled) {
//            return marginPct;
//        } else if (dayMarginEnabled) {
//            if (isMarginTransisitionPassed()) {
//                return 0;
//            } else {
//                return dayMarginPct;
//            }
//        } else {
//            return 0;
//
//        }
//    }

//    public double getApplicableMarginPct(boolean isMktClosed) {
//        if (marginEnabled && dayMarginEnabled) {
//            if (isMarginTransisitionPassed()) {
//                if (isMktClosed) {
//                    return dayMarginPct;
//                } else {
//                    return marginPct;
//                }
//            } else {
//                return dayMarginPct;
//            }
//        } else if (marginEnabled) {
//            return marginPct;
//        } else if (dayMarginEnabled) {
//            if (isMarginTransisitionPassed()) {
//                if (isMktClosed) {
//                    return dayMarginPct;
//                } else {
//                    return 0;
//                }
//            } else {
//                return dayMarginPct;
//            }
//        } else {
//            return 0;
//
//        }
//    }

    public void setMarginPct(double marginPct) {
        this.marginPct = marginPct;
    }

    public double getApplicableMarginPct(String exchange) {

        if (marginEnabled && dayMarginEnabled) {
            if (isDayMarginApplicable(exchange)) {
                return dayMarginPct;
            } else {
                return marginPct;
            }
        } else if (marginEnabled) {
            return marginPct;
        } else if (dayMarginEnabled) {
            if (isDayMarginApplicable(exchange)) {
                return dayMarginPct;
            } else {
                return 0;
            }
        } else {
            return 0;

        }
    }

    private boolean isDayMarginApplicable(String exchange) {
        Boolean isdayapplicable = false;
        long time;
        if (exchange != null && (!exchange.isEmpty()) && (!exchange.trim().equals("*"))) {
            Rule rule = (RuleManager.getSharedInstance().getRule("APPLICABLE_MARGIN", exchange, "*"));
            Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
            if (ex == null) {
                return false;
            }
            if (Settings.isConnected()) {
                time = MarketTimer.getSharedInstance().getMarketTime(exchange);
            } else {
                time = ex.getMarketDateTime();
            }

            TWDateFormat format = new TWDateFormat("yyyy:MM:dd:HH:mm:ss");
            if (rule != null) {
//            if (rule == null) {

                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("markettime", format.format(time));
                    interpreter.set("exchange", exchange);
                    interpreter.set("mktstatus", ex.getMarketStatus());
//                    interpreter.set("mktstatus",2);
                    isdayapplicable = (Boolean) interpreter.eval(rule.getRule());
//                    isdayapplicable = (Boolean) interpreter.eval("if((mktstatus == 2) && (4<= Integer.parseInt(markettime.split(\":\")[3])) && (5<= Integer.parseInt(markettime.split(\":\")[4]))){return false;} else {return true;}");

                } catch (EvalError evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    isdayapplicable = false;

                }
            }


        }


        return isdayapplicable;
    }


//    public boolean isMarginTransisitionPassed() {
//        if (dayLiquidationTime > 0) {
//            Date date = new Date(System.currentTimeMillis());
//            cal.setTime(date);
//            cal.set(Calendar.HOUR_OF_DAY, 0);
//            cal.set(Calendar.MINUTE, 0);
//            cal.set(Calendar.SECOND, 0);
//
//            return (System.currentTimeMillis() - cal.getTimeInMillis()) > dayLiquidationTime;
////            return true;
//        }
//        return false;
//    }

    public boolean isDayMarginAvailable(String exchange) {
        Boolean isdayapplicable = false;
        long time;
        if (exchange != null && (!exchange.isEmpty()) && (!exchange.trim().equals("*"))) {
            Rule rule = (RuleManager.getSharedInstance().getRule("APPLICABLE_MARGIN", exchange, "*"));
            Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
            if (Settings.isConnected()) {
                time = MarketTimer.getSharedInstance().getMarketTime(exchange);
            } else {
                time = ex.getMarketDateTime();
            }

            TWDateFormat format = new TWDateFormat("yyyy:MM:dd:HH:mm:ss");
            if (rule != null) {
//            if (rule == null) {

                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("markettime", format.format(time));
                    interpreter.set("exchange", exchange);
                    interpreter.set("mktstatus", ex.getMarketStatus());
                    isdayapplicable = (Boolean) interpreter.eval(rule.getRule());
//                     isdayapplicable = (Boolean) interpreter.eval("if((mktstatus == 2) && (4<= Integer.parseInt(markettime.split(\":\")[3])) && (5<= Integer.parseInt(markettime.split(\":\")[4]))){return false;} else {return true;}");
//                    isdayapplicable = (Boolean) interpreter.eval("if((mktstatus == 2) && (9 <= Integer.parseInt(markettime.split(\":\")[3])) && (30 <= Integer.parseInt(markettime.split(\":\")[4]))){return false;} else {return true;}");
                } catch (EvalError evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    isdayapplicable = false;
                }
            }


        }
        return isdayapplicable;
    }

    public boolean isDayMarginEnabled() {
        return dayMarginEnabled;
    }

    public void setDayMarginEnabled(boolean dayMarginEnabled) {
        this.dayMarginEnabled = dayMarginEnabled;
    }

    public boolean isMarginable() {
        return (dayMarginEnabled || marginEnabled);
    }

    public String getXML() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<Portfolio>");
        buffer.append("<ID value=\"").append(getPortfolioID()).append("\"/>");
        buffer.append("<Name value=\"").append(getName()).append("\"/>");
        buffer.append("<Account value=\"").append(getAccountNumber()).append("\"/>");
        buffer.append("<Exchanges value=\"").append(getExchangeString()).append("\"/>");
        buffer.append("</Portfolio>\n");

        return buffer.toString();

    }

    public double getDayMarginPct() {
        return dayMarginPct;
    }

    public void setDayMarginPct(double dayMarginPct) {
        this.dayMarginPct = dayMarginPct;
    }

    public ArrayList getBookKeepers() {
        return bookKeepers;
    }

    public String getDefaultBookKeeper() {
        String bookKeeper = "";
        for (int i = 0; i < bookKeepers.size(); i++) {
            BookKeeper bk = (BookKeeper) bookKeepers.get(i);
            if (bk.isDefault()) {
                bookKeeper = bk.getCode();
            }


        }
        return bookKeeper;
    }

    public long getDayLiquidationTime() {
        return dayLiquidationTime;
    }

    public void setDayLiquidationTime(long dayLiquidationTime) {
        this.dayLiquidationTime = dayLiquidationTime;
    }

    public double getMaxMarginAmt() {
        return maxMarginAmt;
    }

    public void setMaxMarginAmt(double maxMarginAmt) {
        this.maxMarginAmt = maxMarginAmt;
    }

    public double getMaxDayMarginAmt() {
        return maxDayMarginAmt;
    }

    public void setMaxDayMarginAmt(double maxDayMarginAmt) {
        this.maxDayMarginAmt = maxDayMarginAmt;
    }
}