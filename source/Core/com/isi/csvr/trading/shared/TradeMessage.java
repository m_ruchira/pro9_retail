package com.isi.csvr.trading.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 21, 2004
 * Time: 12:16:39 PM
 */
public class TradeMessage {
    public String sessionID = "0";
    public String userID = "0";
    public String TRSID = "0";
    public String signature = "0";
    String[] frameData;
    String messageData;
    private int type;
    private long messageID;
    private StringBuffer data;

    public TradeMessage(int type) {
        this.type = type;
    }

    public TradeMessage(String frame) throws Exception {
        try {
            String[] fields = frame.split(TradeMeta.FS);
            type = Integer.parseInt(fields[0]);
            messageID = Long.parseLong(fields[1]);
            sessionID = fields[2];
            userID = fields[3];
            TRSID = fields[4];
            signature = fields[5];
            messageData = fields[6];
            frameData = fields[6].split(TradeMeta.DS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getMessageData() {
        return messageData;
    }

    public String[] getFrameData() {
        return frameData;
    }

    public String getFrameData(int index) {
        return frameData[index];
    }

    public int dataCount() {
        return frameData.length;
    }

    public int getType() {
        return type;
    }

    public long getMessageID() {
        return messageID;
    }

    public void addData(String str) {
        if (data == null) {
            data = new StringBuffer();
            data.append(type);
            data.append(TradeMeta.FS);
            data.append(TradingShared.getNextMessageID());
            data.append(TradeMeta.FS);
            data.append(sessionID);
            data.append(TradeMeta.FS);
            data.append(userID);
            data.append(TradeMeta.FS);
            data.append(TRSID);
            data.append(TradeMeta.FS);
            data.append(signature);
            data.append(TradeMeta.FS);
            data.append(str);
        } else {
            data.append(TradeMeta.DS);
            data.append(str);
        }
    }

    public void addData(long value) {
        addData("" + value);
    }

    public void addData(double value) {
        addData("" + value);
    }

    public String toString() {
        return data.toString() + TradeMeta.EOL;
    }
}
