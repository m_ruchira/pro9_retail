package com.isi.csvr.trading.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Niroshan
 * Date: Oct 19, 2004
 * Time: 10:39:53 AM
 * To change this template use File | Settings | File Templates.
 */

import com.dfn.mtr.mix.beans.Order;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

import java.util.StringTokenizer;

public class TRSOrder {

    private static final char PORTFOLIO_NO = 'A';
    private static final char SYMBOL = 'B';
    private static final char ORDER_STATUS = 'C';
    private static final char CL_ORDER_ID = 'D';
    private static final char ORG_CL_ORDER_ID = 'E';
    private static final char ORDER_ID = 'F';
    private static final char EXEC_ID = 'G';
    private static final char SECURITY_TYPE = 'H';
    private static final char SECURITY_EXCH = 'I';
    private static final char CURRENCY = 'J';
    private static final char ORDER_TYPE = 'K';
    private static final char ORDER_SIDE = 'L';
    private static final char ORDER_QTY = 'M';
    private static final char ORDER_PRICE = 'N';
    private static final char ORDER_TIF = 'O';
    private static final char EXPIRE_TIME = 'P';
    private static final char MIN_QTY = 'Q';
    private static final char MAX_FLOOR = 'R';
    private static final char SETTLEMENT_TYPE = 'S';
    private static final char HANDLE_INST = 'T';
    private static final char LAST_PRICE = 'U';
    private static final char AVG_PRICE = 'V';
    private static final char EXEC_TYPE = 'W';
    private static final char EXEC_TRANS_TYPE = 'X';
    private static final char REJECT_REASON = 'Y';
    private static final char TEXT = 'Z';
    private static final char LAST_SHARES = 'a';
    private static final char CUMMULATIVE_QTY = 'b';
    private static final char LEAVES_QTY = 'c';
    private static final char TRAS_TIME = 'd';
    private static final char COMMISSION = 'e';
    private static final char ESIS_NO = 'f';
    private static final char CHANNEL = 'g';
    private static final char USER_ID = 'h';
    private static final char DEALER_ID = 'i';
    private static final char CREATE_DATE = 'j';
    private static final char MANUAL_REQ_STATUS = 'k';
    //'l' IS MISSING HERE
    private static final char GTD = 'm';
    private static final char MUBASHER_ORDER_NO = 'n';
    private static final char ORDER_VALUE = 'o';
    private static final char ORDER_NET_VALUE = 'p';
    private static final char CUM_COMMISSION = 'q';
    private static final char CUM_ORDER_VALUE = 'r';
    private static final char CUM_ORDER_NET_VALUE = 's';
    private static final char SEQUENCE_NO = 't';
    private static final char TW_ORDER_ID = 'u';
    private static final char TAKE_PROFIT = 'v';
    private static final char PASSWORD = 'w';
    private static final char CERTIFICATE = 'x';
    private static final char BROKER_ID = 'y';
    private static final char PUT_OR_CALL = 'z';
    private static final char STRIKE_PRICE = '1';
    private static final char MATURITY_MONTH_YEAR = '2';
    private static final char STOPLOSS_PRICE = '3';
    private static final char BASE_SYMBOL = '4';
    private static final char SETTLE_CURRENCY = '5';
    private static final char SETTLE_RATE = '6';
    private static final char AVG_COST = '7';
    private static final char CONDITION = '8';
    private static final char CONDITION_STATUS = '9';
    private static final char CONDITION_EXPIRE_TIME = '0';
    private static final char CONDITION_START_TIME = '.';
    private static final char CONDITION_TYPE = '!';
    private static final char SLICE_EXEC_TYPE = '%';
    private static final char SLICE_BLOCK_SIZE = '&';
    private static final char SLICE_TIME_INTERVAL = '^';
    private static final char SLICE_ORDER_ID = '*';
    private static final char SLICE_ORDER_STATUS = '+';
    private static final char STOP_LOSS_TYPE = '=';
    private static final char QUOTE_ID = '[';
    private static final char AMEND_FILEDS = ']';
    private static final char AMEND_CONDITION = '{';
    private static final char AMEND_CONDITION_STATUS = '}';
    private static final char AMEND_CONDTION_EXPIRE_TIME = '<';
    private static final char CANCEL_CONDITION = '~';
    private static final char CANCEL_CONDITION_STATUS = '?';
    private static final char CANCEL_CONDTION_EXPIRE_TIME = '>';
    private static final char IS_DAY_ORDER = '#';
    private static final char STOPLOSS_TRAIL_PRICE = ':';
    private static final char MARKED_FOR_DELIVERY = ',';
    private static final char FOREX_BROCKER_ID = ';';
    private static final char FOREX_BESTQUOTE_ID = '/';
    private static final char FOREX_BEST_PRICE = '|';
    private static final char SYMBOL_INSTRUMENT_TYPE = '"';
    private static final char ORDER_REF_ID = '\u00A7';
    private static final char BOOK_KEEPER = '$';
    private static final char TPLUS = '`';
    private String mubasherOrderNumber = null; //9998
    private String clOrdID = null; //11
    private String OrigClOrdID = null; //41
    private String brokerID = null;
    private String OrderID = null; //37
    private String execID = null; //17
    private String symbol = null; //55
    private int SecurityType = Meta.INSTRUMENT_EQUITY; //167 //priority String
    //    private int SecurityType2 = -1; //167 //priority String
    private String SecurityExchange = null; //207
    private String currency; //15
    private String portfolioNo = null; //1 - exchange account/ saudi portfolio same
    private char ordType = 0; //40 - 1 market,2 limit,3 stop
    private int side = -1; //54 - 1 buy,2 sell,sell short
    private int orderQty = -1; //38
    private double price = -1; //44 - limit price
    private double stopLossPrice = -1; //44 - limit price
    private double trailStopLossPrice = -1; //44 - limit price
    private int TimeInForce = -1; //59 0 = Day,1 = Good Till Cancel  (GTC),2 = At the Opening  (OPG),3 = Immediate or Cancel  (IOC),4 = Fill or Kill  (FOK),5 = Good Till Crossing  (GTX),6 = Good Till Date,7 = At the Close
    private String ExpireTime = null; //126
    private int MinQty = 0; //110
    private int MaxFloor = -1; //111 disclose qty
    private int SettlmntTyp = -1;//63  0 = Regular,1 = Cash,2 = Next Day,3 = T+2,4 = T+3,5 = T+4,6 = Future,7 = When And If Issued,8 = Sellers Option,9 = T+ 5,A = T+1
    private int HandlInst = -1; // 21 - 1 = Automated execution order, private, no Broker intervention
    private double lastPx = -1;//31
    private double avgPx = -1; //6
    private char execType = 'X'; //150
    private char ordStatus = 'X'; //39
    private int execTransType = -1; //20
    private int OrdRejReason = -1; //103
    private int TextType;
    private String Text = null; //58 -Reason for rejection.
    private int lastShares = -1; //32
    private int cumQty = 0;//14
    private int leavesQty = 0; //151
    private String transactTime = null; //60
    private double commission = 0; //12
    private double ordValue = 0; //806
    private double ordNetValue = 0; //807
    private double cum_commission = 0; //808
    private double cum_ordValue = 0; //809
    private double cum_ordNetValue = 0; //810
    private long ESISNumber = -1; //800
    private int channel = -1; //801
    private String userID = null; //802
    private long dealerID = -1; //803
    private String createdDate = null; //804
    private int manualRequestStatus = -1; //805
    private int goodTillDays = -1;
    private long sequenceNumber;
    private String twOrderID = null;
    private String password = null;
    private String certificate = null;
    private double takeProfit = -1;
    private String quoteID = null; //117
    private String forexBrockerID = null; //117
    private String forexBestQuoteID = null; //117
    private double forexBestPrice;
    private int putOrCall;                    // 201 0 = Put, 1 = Call
    private double strikePrice = 0;              // 202
    private String maturityMonthYear = null;     // 200 Maturity Month or Year - YYYYMM
    private String baseSymbol = null;     // 200 Maturity Month or Year - YYYYMM
    private String settleCurrency = null; //820
    private double settleRate = 1; //821
    private double avgCost = -1;  //819
    private String condition = null; //8 | 822
    private int conditionStatus = 0;    //9 | 823
    private String conditionExpireTime = null; //0 | 824
    private String conditionStartTime = null; //0 | 825
    private int conditionType = TradeMeta.CONDITION_TYPE_NONE; //0 | 826
    private int sliceExecType = -1;         //830
    private long sliceTimeInterval = -1;    //831
    private int sliceBlockSIze = -1;        //832
    private long sliceOrderID = -1;
    private int sliceOrderStatus = -1;
    private boolean isDayOrder = false;
    private boolean isMarkedForDelivery = false;
    private int stopLossType = -1;
    //adding reference ID for an Order: requested By Ruwan, Done by Chandika
    private String refID = "";
    private String bookKeeper = "";
    private int tPlus = -1;
//    private static final char ORDER_REF_ID                  ='?';


    /**
     *
     */
    public TRSOrder() {
    }

    // From FIX message generate Order object
    public void parseMubasherMessage(String strFIX) {
        String sToken;
        String sValue;
        StringTokenizer st = new StringTokenizer(strFIX, TradeMeta.FD);
        char tag;
        while (st.hasMoreTokens()) {
            sToken = st.nextToken();
            tag = sToken.charAt(0);
            sValue = sToken.substring(1);
            switch (tag) {
                case PORTFOLIO_NO:
                    portfolioNo = sValue;
                    break;
                case SYMBOL:
                    symbol = sValue;
                    break;
                case ORDER_STATUS:
                    ordStatus = sValue.charAt(0);
                    break;
                case CL_ORDER_ID:
                    clOrdID = sValue;
                    break;
                case ORG_CL_ORDER_ID:
                    OrigClOrdID = sValue;
                    break;
                case ORDER_ID:
                    OrderID = sValue;
                    break;
                case EXEC_ID:
                    execID = sValue;
                    break;
                case SYMBOL_INSTRUMENT_TYPE:
                    SecurityType = Integer.parseInt(sValue);
                    if (SecurityType < 0) {
                        SecurityType = Meta.INSTRUMENT_EQUITY;
                    }
                    break;
                case SECURITY_EXCH:
                    SecurityExchange = sValue;
                    break;
                case CURRENCY:
                    currency = sValue;
                    break;
                case ORDER_TYPE:
                    ordType = sValue.charAt(0);
                    break;
                case ORDER_SIDE:
                    side = Integer.parseInt(sValue);
                    break;
                case ORDER_QTY:
                    orderQty = Integer.parseInt(sValue);
                    break;
                case ORDER_PRICE:
                    price = Double.parseDouble(sValue);
                    break;
                case ORDER_TIF:
                    TimeInForce = Integer.parseInt(sValue);
                    break;
                case EXPIRE_TIME:
                    ExpireTime = sValue;
                    break;
                case MIN_QTY:
                    MinQty = Integer.parseInt(sValue);
                    break;
                case MAX_FLOOR:
                    MaxFloor = Integer.parseInt(sValue);
                    break;
                case SETTLEMENT_TYPE:
                    SettlmntTyp = Integer.parseInt(sValue);
                    break;
                case HANDLE_INST:
                    HandlInst = Integer.parseInt(sValue);
                    break;
                case LAST_PRICE:
                    lastPx = Double.parseDouble(sValue);
                    break;
                case AVG_PRICE:
                    avgPx = Double.parseDouble(sValue);
                    break;
                case EXEC_TYPE:
                    execType = sValue.charAt(0);
                    break;
                case EXEC_TRANS_TYPE:
                    execTransType = Integer.parseInt(sValue);
                    break;
                case REJECT_REASON:
                    OrdRejReason = Integer.parseInt(sValue);
                    break;
                case TEXT:
                    Text = sValue;
                    break;
                case LAST_SHARES:
                    lastShares = Integer.parseInt(sValue);
                    break;
                case CUMMULATIVE_QTY:
                    cumQty = Integer.parseInt(sValue);
                    break;
                case LEAVES_QTY:
                    leavesQty = Integer.parseInt(sValue);
                    break;
                case TRAS_TIME:
                    transactTime = sValue;
//                    System.out.println("============== TXN Time " + symbol + " " + sValue);
                    break;
                case COMMISSION:
                    commission = Double.parseDouble(sValue);
                    break;
                case ESIS_NO:
                    ESISNumber = Long.parseLong(sValue);
                    break;
                case CHANNEL:
                    channel = Integer.parseInt(sValue);
                    break;
                case USER_ID:
                    userID = sValue;
                    break;
                case DEALER_ID:
                    dealerID = Long.parseLong(sValue);
                    break;
                case CREATE_DATE:
                    createdDate = sValue;
                    break;
                case MANUAL_REQ_STATUS:
                    manualRequestStatus = Integer.parseInt(sValue);
                    break;
                case GTD:
                    goodTillDays = Integer.parseInt(sValue);
                    break;
                case MUBASHER_ORDER_NO:
                    mubasherOrderNumber = sValue;
                    break;
                case ORDER_VALUE:
                    ordValue = Double.parseDouble(sValue);
                    break;
                case ORDER_NET_VALUE:
                    ordNetValue = Double.parseDouble(sValue);
                    break;
                case CUM_COMMISSION:
                    cum_commission = Double.parseDouble(sValue);
                    break;
                case CUM_ORDER_VALUE:
                    cum_ordValue = Double.parseDouble(sValue);
                    break;
                case CUM_ORDER_NET_VALUE:
                    cum_ordNetValue = Double.parseDouble(sValue);
                    break;
                case SEQUENCE_NO:
                    sequenceNumber = Long.parseLong(sValue);
                    break;
                case TW_ORDER_ID:
                    twOrderID = sValue;
                    break;
                case TAKE_PROFIT:
                    takeProfit = Double.parseDouble(sValue);
                    break;
                case BROKER_ID:
                    brokerID = sValue;
                    break;
                case PUT_OR_CALL:
                    putOrCall = Integer.parseInt(sValue);
                    break;
                case STRIKE_PRICE:
                    strikePrice = Double.parseDouble(sValue);
                    break;
                case MATURITY_MONTH_YEAR:
                    maturityMonthYear = sValue;
                    break;
                case STOPLOSS_PRICE:
                    stopLossPrice = Double.parseDouble(sValue);
                    break;
                case STOPLOSS_TRAIL_PRICE:
                    trailStopLossPrice = Double.parseDouble(sValue);
                    break;
                case BASE_SYMBOL:
                    baseSymbol = sValue;
                    break;
                case SETTLE_CURRENCY:
                    settleCurrency = sValue;
                    break;
                case SETTLE_RATE:
                    settleRate = Double.parseDouble(sValue);
                    break;
                case AVG_COST:
                    avgCost = Double.parseDouble(sValue);
                    break;
                case CONDITION:
                    condition = sValue;
                    break;
                case CONDITION_STATUS:
                    conditionStatus = Integer.parseInt(sValue);
                    break;
                case CONDITION_EXPIRE_TIME:
                    conditionExpireTime = sValue;
                    break;
                case CONDITION_START_TIME:
                    conditionStartTime = sValue;
                    break;
                case CONDITION_TYPE:
                    conditionType = Integer.parseInt(sValue);
                    break;
                case SLICE_EXEC_TYPE:
                    sliceExecType = Integer.parseInt(sValue);
                    break;
                case SLICE_BLOCK_SIZE:
                    sliceBlockSIze = Integer.parseInt(sValue);
                    break;
                case SLICE_ORDER_ID:
                    sliceOrderID = Long.parseLong(sValue);
                    break;
                case SLICE_TIME_INTERVAL:
                    sliceTimeInterval = Long.parseLong(sValue);
                    break;
                case SLICE_ORDER_STATUS:
                    sliceOrderStatus = Integer.parseInt(sValue);
                    break;
                case STOP_LOSS_TYPE:
                    stopLossType = Integer.parseInt(sValue);
                    break;
                case QUOTE_ID:
                    quoteID = sValue;
                    break;
                case IS_DAY_ORDER:
                    isDayOrder = sValue.trim().equals("1");
                    break;
                case MARKED_FOR_DELIVERY:
                    isMarkedForDelivery = sValue.trim().equals("1");
                    break;
                case FOREX_BESTQUOTE_ID:
                    forexBestQuoteID = sValue;
                    break;
                case FOREX_BROCKER_ID:
                    forexBrockerID = sValue;
                    break;
                case FOREX_BEST_PRICE:
                    forexBestPrice = Double.parseDouble(sValue);
                    break;
                case ORDER_REF_ID:
                    refID = sValue;
//                      SharedMethods.printLine("REF ID "+sValue,true);
                    break;
                case BOOK_KEEPER:
                    bookKeeper = sValue;
//                     SharedMethods.printLine("Bookkeeper"+sValue,true);
                    break;
                case TPLUS:
                    tPlus = Integer.parseInt(sValue);
                default:
                    break;
            }//switch
        }
        symbol = SharedMethods.getSymbolFromKey(TradingShared.getPriceSymbol(SecurityExchange, symbol, SecurityType, portfolioNo));
        if (baseSymbol != null) {
            baseSymbol = SharedMethods.getSymbolFromKey(TradingShared.getPriceSymbol(SecurityExchange, baseSymbol, SecurityType, portfolioNo));
        }

    }

    public void setMixOrderObject(Order mixOrderObject) {

    }

    public String getMubasherMessage() {
        StringBuffer sb = new StringBuffer();
        if (portfolioNo != null) sb.append(PORTFOLIO_NO + portfolioNo + TradeMeta.FD);
        if (symbol != null) sb.append(SYMBOL + symbol + TradeMeta.FD);
        if (ordStatus != TradeMeta.T39_DEFAULT) sb.append(ORDER_STATUS + ordStatus + TradeMeta.FD);
        if (clOrdID != null) sb.append(CL_ORDER_ID + clOrdID + TradeMeta.FD);
        if (OrigClOrdID != null) sb.append(ORG_CL_ORDER_ID + OrigClOrdID + TradeMeta.FD);
        if (OrderID != null) sb.append(ORDER_ID + OrderID + TradeMeta.FD);
        if (execID != null) sb.append(EXEC_ID + execID + TradeMeta.FD);
        if (SecurityType != -1) sb.append("\"" + SecurityType + TradeMeta.FD);
        if (SecurityType != -1) sb.append("H" + SecurityType + TradeMeta.FD);
        if (SecurityExchange != null) sb.append(SECURITY_EXCH + SecurityExchange + TradeMeta.FD);
        if (currency != null) sb.append(CURRENCY + currency + TradeMeta.FD);
        if (ordType != '0') sb.append("K" + ordType + TradeMeta.FD);
        if (side != -1) sb.append("L" + side + TradeMeta.FD);
        if (orderQty != -1) sb.append("M" + orderQty + TradeMeta.FD);
        if (price > 0) sb.append("N" + price + TradeMeta.FD);
        if (TimeInForce != -1) sb.append("O" + TimeInForce + TradeMeta.FD);
        if (ExpireTime != null) sb.append("P" + ExpireTime + TradeMeta.FD);
        if (MinQty > 0) sb.append("Q" + MinQty + TradeMeta.FD);
        if (MaxFloor != -1) sb.append("R" + MaxFloor + TradeMeta.FD);
        if (SettlmntTyp != -1) sb.append("S" + SettlmntTyp + TradeMeta.FD);
        if (HandlInst != -1) sb.append("T" + HandlInst + TradeMeta.FD);
        if (lastPx != -1) sb.append("U" + lastPx + TradeMeta.FD);
        if (avgPx != -1) sb.append("V" + avgPx + TradeMeta.FD);
        if (execType != -1) sb.append("W" + execType + TradeMeta.FD);
        if (execTransType != -1) sb.append("X" + execTransType + TradeMeta.FD);
        if (OrdRejReason != -1) sb.append("Y" + OrdRejReason + TradeMeta.FD);
        if (Text != null) sb.append("Z" + Text + TradeMeta.FD);
        if (lastShares != -1) sb.append("a" + lastShares + TradeMeta.FD);
        if (cumQty > 0) sb.append("b" + cumQty + TradeMeta.FD);
        if (leavesQty > 0) sb.append("c" + leavesQty + TradeMeta.FD);
        if (transactTime != null) sb.append("d" + transactTime + TradeMeta.FD);
        if (commission > 0) sb.append("e" + commission + TradeMeta.FD);
        if (ESISNumber != -1) sb.append("f" + ESISNumber + TradeMeta.FD);
//        if (channel != -1) sb.append("g" + channel + TradeMeta.FD);
        sb.append("g" + TradeMeta.CHANNEL + TradeMeta.FD);
        if (userID != null) sb.append("h" + userID + TradeMeta.FD);
        if (dealerID != -1) sb.append("i" + dealerID + TradeMeta.FD);
        if (createdDate != null) sb.append("j" + createdDate + TradeMeta.FD);
        if (manualRequestStatus != -1) sb.append("k" + manualRequestStatus + TradeMeta.FD);
        if (goodTillDays != -1) sb.append("m" + goodTillDays + TradeMeta.FD);
        if (mubasherOrderNumber != null) sb.append("n" + mubasherOrderNumber + TradeMeta.FD);
        if (twOrderID != null) sb.append("u" + twOrderID + TradeMeta.FD);
        if (takeProfit > 0) sb.append("v" + takeProfit + TradeMeta.FD);
        if (password != null) sb.append("w" + password + TradeMeta.FD);
        if (certificate != null) sb.append("x" + certificate + TradeMeta.FD);
        if (brokerID != null) sb.append("y" + brokerID + TradeMeta.FD);
        if (putOrCall != -1) sb.append("z" + putOrCall + TradeMeta.FD);
        if (strikePrice != -1) sb.append(STRIKE_PRICE + strikePrice + TradeMeta.FD);
        if (maturityMonthYear != null) sb.append("" + MATURITY_MONTH_YEAR + maturityMonthYear + TradeMeta.FD);
        if (stopLossPrice > 0) sb.append("" + STOPLOSS_PRICE + stopLossPrice + TradeMeta.FD);
        if (trailStopLossPrice > 0) sb.append(":" + trailStopLossPrice + TradeMeta.FD);
        if (baseSymbol != null) sb.append(BASE_SYMBOL + baseSymbol + TradeMeta.FD);
        if (settleCurrency != null) sb.append(SETTLE_CURRENCY + settleCurrency + TradeMeta.FD);
        if (settleRate != -1) sb.append("" + SETTLE_RATE + settleRate + TradeMeta.FD);
        if (avgCost != -1) sb.append(AVG_COST + avgCost + TradeMeta.FD);
        if (condition != null) sb.append(CONDITION + condition + TradeMeta.FD);
        if (conditionStatus != -1) sb.append("9" + conditionStatus + TradeMeta.FD);
        if (conditionExpireTime != null) sb.append(CONDITION_EXPIRE_TIME + conditionExpireTime + TradeMeta.FD);
        if (conditionStartTime != null) sb.append(CONDITION_START_TIME + conditionStartTime + TradeMeta.FD);
        if (conditionType != -1) sb.append(CONDITION_TYPE).append(conditionType).append(TradeMeta.FD);
        if (sliceExecType != -1) sb.append(SLICE_EXEC_TYPE).append(sliceExecType).append(TradeMeta.FD);
        if (sliceBlockSIze != -1) sb.append(SLICE_BLOCK_SIZE).append(sliceBlockSIze).append(TradeMeta.FD);
        if (sliceTimeInterval != -1) sb.append(SLICE_TIME_INTERVAL).append(sliceTimeInterval).append(TradeMeta.FD);
        if (sliceOrderID != -1) sb.append(SLICE_ORDER_ID).append(sliceOrderID).append(TradeMeta.FD);
        if (sliceOrderStatus != -1) sb.append(SLICE_ORDER_STATUS).append(sliceOrderStatus).append(TradeMeta.FD);
        if (quoteID != null) sb.append(QUOTE_ID).append(quoteID).append(TradeMeta.FD);
        if (forexBestQuoteID != null) sb.append(FOREX_BESTQUOTE_ID).append(forexBestQuoteID).append(TradeMeta.FD);
        if (forexBrockerID != null) sb.append(FOREX_BROCKER_ID).append(forexBrockerID).append(TradeMeta.FD);
        if (forexBestPrice != -1) sb.append(FOREX_BEST_PRICE).append(forexBestPrice).append(TradeMeta.FD);
        if (stopLossType != -1) sb.append(STOP_LOSS_TYPE).append(stopLossType).append(TradeMeta.FD);
        sb.append(IS_DAY_ORDER).append(dayOrderStatus()).append(TradeMeta.FD);
        sb.append(MARKED_FOR_DELIVERY).append(markForDeliveryStatus()).append(TradeMeta.FD);
        if ((refID != null) && (!refID.equals(""))) sb.append(ORDER_REF_ID).append(getRefID()).append(TradeMeta.FD);
        if ((bookKeeper != null) && (!bookKeeper.equals("")))
            sb.append(BOOK_KEEPER).append(getBookKeeper()).append(TradeMeta.FD);
        if ((tPlus != -1)) sb.append(TPLUS).append(getTPlus()).append(TradeMeta.FD);
        return sb.toString();
    }

    private int dayOrderStatus() {
        if (isDayOrder) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean isDayOrder() {
        return isDayOrder;
    }

    public void setDayOrder(boolean dayOrder) {
        isDayOrder = dayOrder;
    }

    private int markForDeliveryStatus() {
        if (isMarkedForDelivery) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean isMarkedForDelivery() {
        return isMarkedForDelivery;
    }

    public void setMarkedForDelivery(boolean markedForDelivery) {
        isMarkedForDelivery = markedForDelivery;
    }

    public String getMubasherOrderNumber() {
        return mubasherOrderNumber;
    }

    public void setMubasherOrderNumber(String mubasherOrderNumber) {
        this.mubasherOrderNumber = mubasherOrderNumber;
    }

    public double getAvgPx() {
        return avgPx;
    }

    public void setAvgPx(double d) {
        avgPx = d;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int i) {
        channel = i;
    }

    public String getClOrdID() {
        return clOrdID;
    }

    public void setClOrdID(String l) {
        clOrdID = l;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double d) {
        commission = d;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String l) {
        createdDate = l;
    }

    public int getCumQty() {
        return cumQty;
    }

    public void setCumQty(int i) {
        cumQty = i;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String string) {
        currency = string;
    }

    public long getDealerID() {
        return dealerID;
    }

    public void setDealerID(long l) {
        dealerID = l;
    }

    public long getESISNumber() {
        return ESISNumber;
    }

    public void setESISNumber(long l) {
        ESISNumber = l;
    }

    public String getExecID() {
        return execID;
    }

    public void setExecID(String l) {
        execID = l;
    }

    public int getExecTransType() {
        return execTransType;
    }

    public void setExecTransType(int i) {
        execTransType = i;
    }

    public char getExecType() {
        return execType;
    }

    public void setExecType(char i) {
        execType = i;
    }

    public String getExpireTime() {
        return ExpireTime;
    }

    public void setExpireTime(String l) {
        ExpireTime = l;
    }

    public int getHandlInst() {
        return HandlInst;
    }

    public void setHandlInst(int i) {
        HandlInst = i;
    }

    public double getLastPx() {
        return lastPx;
    }

    public void setLastPx(double d) {
        lastPx = d;
    }

    public int getLastShares() {
        return lastShares;
    }

    public void setLastShares(int i) {
        lastShares = i;
    }

    public int getLeavesQty() {
        return leavesQty;
    }

    public void setLeavesQty(int i) {
        leavesQty = i;
    }

    public int getManualRequestStatus() {
        return manualRequestStatus;
    }

    public void setManualRequestStatus(int i) {
        manualRequestStatus = i;
    }

    public int getMaxFloor() {
        return MaxFloor;
    }

    public void setMaxFloor(int i) {
        MaxFloor = i;
    }

    public int getMinQty() {
        return MinQty;
    }

    public void setMinQty(int i) {
        MinQty = i;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String l) {
        OrderID = l;
    }

    public int getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(int i) {
        orderQty = i;
    }

    public int getOrdRejReason() {
        return OrdRejReason;
    }

    public void setOrdRejReason(int i) {
        OrdRejReason = i;
    }

    public char getOrdStatus() {
        return ordStatus;
    }

    public void setOrdStatus(char c) {
        ordStatus = c;
    }

    public char getOrdType() {
        return ordType;
    }

    public void setOrdType(char c) {
        ordType = c;
    }

    public String getOrigClOrdID() {
        return OrigClOrdID;
    }

    /**
     * @param l
     */
    public void setOrigClOrdID(String l) {
        OrigClOrdID = l;
    }

    public String getPortfolioNo() {
        return portfolioNo;
    }

    /**
     * @param l
     */
    public void setPortfolioNo(String l) {
        portfolioNo = l;
    }

    public String getSecurityExchange() {
        return SecurityExchange;
    }

    /**
     * @param string
     */
    public void setSecurityExchange(String string) {
        SecurityExchange = string;
    }

    public int getSecurityType() {
        return SecurityType;
    }

    /**
     * @param i
     */
    public void setSecurityType(int i) {
        SecurityType = i;
    }

    public int getSettlmntTyp() {
        return SettlmntTyp;
    }

    /**
     * @param i
     */
    public void setSettlmntTyp(int i) {
        SettlmntTyp = i;
    }

    public int getSide() {
        return side;
    }

    /**
     * @param i
     */
    public void setSide(int i) {
        side = i;
    }

    public double getPrice() {
        return price;
    }

    /**
     * @param d
     */
    public void setPrice(double d) {
        price = d;
    }

    public String getSymbol() {
        return symbol;
    }

    /**
     * @param string
     */
    public void setSymbol(String string) {
        symbol = string;
    }

    public String getText() {
        return Text;
    }

    public int getTimeInForce() {
        return TimeInForce;
    }

    /**
     * @param i
     */
    public void setTimeInForce(int i) {
        TimeInForce = i;
    }

    public String getTransactTime() {
        return transactTime;
    }

    /**
     * @param string
     */
//    public void setText(String string) {
//        Text = string;
//    }

    /**
     * @param l
     */
    public void setTransactTime(String l) {
        transactTime = l;
    }

    public String getUserID() {
        return userID;
    }

    /**
     * @param l
     */
    public void setUserID(String l) {
        userID = l;
    }

    public int getGoodTillDays() {
        return goodTillDays;
    }

    public void setGoodTillDays(int goodTillDays) {
        this.goodTillDays = goodTillDays;
    }


    public double getOrdValue() {
        return ordValue;
    }

    public double getOrdNetValue() {
        return ordNetValue;
    }

    public double getCum_commission() {
        return cum_commission;
    }

    public double getCum_ordValue() {
        return cum_ordValue;
    }

    public double getCum_ordNetValue() {
        return cum_ordNetValue;
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public String getTwOrderID() {
        return twOrderID;
    }

    public void setTwOrderID(String twOrderID) {
        this.twOrderID = twOrderID;
    }

    public double getTakeProfit() {
        return takeProfit;
    }

    public void setTakeProfit(double takeProfit) {
        this.takeProfit = takeProfit;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setBrokerID(String brokerID) {
        this.brokerID = brokerID;
    }

    public String getMaturityMonthYear() {
        return maturityMonthYear;
    }

    public void setMaturityMonthYear(String maturityMonthYear) {
        this.maturityMonthYear = maturityMonthYear;
    }

    public double getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public int getPutOrCall() {
        return putOrCall;
    }

    public void setPutOrCall(int putOrCall) {
        this.putOrCall = putOrCall;
    }

    public String getBaseSymbol() {
        return baseSymbol;
    }

    public void setBaseSymbol(String baseSymbol) {
        this.baseSymbol = baseSymbol;
    }

    public double getStopLossPrice() {
        return stopLossPrice;
    }

    public void setStopLossPrice(double stopLossPrice) {
        this.stopLossPrice = stopLossPrice;
    }

    public double getTrailStopLossPrice() {
        return trailStopLossPrice;
    }

    public void setTrailStopLossPrice(double stopLossPrice) {
        this.trailStopLossPrice = stopLossPrice;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public double getSettleRate() {
        return settleRate;
    }

    public void setSettleRate(double settleRate) {
        this.settleRate = settleRate;
    }

    public String getSettleCurrency() {
        return settleCurrency;
    }

    public void setSettleCurrency(String settle_currency) {
        this.settleCurrency = settle_currency;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getConditionExpireTime() {
        return conditionExpireTime;
    }

    public void setConditionExpireTime(String conditionExpireTime) {
        this.conditionExpireTime = conditionExpireTime;
    }

    public String getConditionStartTime() {
        return conditionStartTime;
    }

    public void setConditionStartTime(String conditionStartTime) {
        this.conditionStartTime = conditionStartTime;
    }

    public int getConditionType() {
        return conditionType;
    }

    public void setConditionType(int conditionType) {
        this.conditionType = conditionType;
    }

    public int getConditionStatus() {
        return conditionStatus;
    }

    public void setConditionStatus(int conditionStatus) {
        this.conditionStatus = conditionStatus;
    }

    public int getSliceExecType() {
        return sliceExecType;
    }

    public void setSliceExecType(int sliceExecType) {
        this.sliceExecType = sliceExecType;
    }

    public long getSliceTimeInterval() {
        return sliceTimeInterval;
    }

    public void setSliceTimeInterval(long sliceTimeInterval) {
        this.sliceTimeInterval = sliceTimeInterval;
    }

    public int getSliceBlockSIze() {
        return sliceBlockSIze;
    }

    public void setSliceBlockSIze(int sliceBlockSIze) {
        this.sliceBlockSIze = sliceBlockSIze;
    }


    public long getSliceOrderID() {
        return sliceOrderID;
    }

    public void setSliceOrderID(long sliceOrderID) {
        this.sliceOrderID = sliceOrderID;
    }

    public int getSliceOrderStatus() {
        return sliceOrderStatus;
    }

    public void setSliceOrderStatus(int sliceOrderStatus) {
        this.sliceOrderStatus = sliceOrderStatus;
    }

    public String getQuoteID() {
        return this.quoteID;
    }

    public void setQuoteID(String qid) {
        this.quoteID = qid;
    }

    public int getStopLossType() {
        return stopLossType;
    }

    public void setStopLossType(int stopLossType) {
        this.stopLossType = stopLossType;
    }

    public String getForexBrockerID() {
        return forexBrockerID;
    }

    public void setForexBrockerID(String forexBrockerID) {
        this.forexBrockerID = forexBrockerID;
    }

    public String getForexBestQuoteID() {
        return forexBestQuoteID;
    }

    public void setForexBestQuoteID(String forexBestQuoteID) {
        this.forexBestQuoteID = forexBestQuoteID;
    }

    public double getForexBestPrice() {
        return forexBestPrice;
    }

    public void setForexBestPrice(double forexBestPrice) {
        this.forexBestPrice = forexBestPrice;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getBookKeeper() {
        return bookKeeper;
    }

    public void setBookKeeper(String bookKeeper) {
        this.bookKeeper = bookKeeper;
    }

    public int getTPlus() {
        return tPlus;
    }

    public void setTPlus(int tPlus) {
        this.tPlus = tPlus;
    }
}

