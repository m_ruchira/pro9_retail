package com.isi.csvr.trading.shared;

import com.dfn.mtr.mix.beans.CashTransfer;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 9, 2006
 * Time: 2:55:12 PM
 */
public class FundTransferRequest implements FundTransfer {
    SimpleDateFormat formatted = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat unformatted = new SimpleDateFormat("yyyyMMdd");
    private String currency;
    private int method;
    private double amount;
    private int status;
    private String reference;
    private String chequeNo;
    private String chequeDate;
    private String bank;
    private String bankID;
    private String depositeDate;
    private long date;
    private String referenceNo;
    private String note;
    private String bankAddress;
    private String branch;
    private String portfolio;
    private String reason = "";
    private CashTransfer fundTransfer;

    public FundTransferRequest() {
        fundTransfer = new CashTransfer();
    }

    public CashTransfer getFundTransfer() {
        return fundTransfer;
    }

    public void setFundTransfer(CashTransfer fundTransfer) {
        this.fundTransfer = fundTransfer;
    }

    public int getType() {
        return TradingConstants.TRANSFER_FUND_TRANSFER;
    }

    public double getAmount() {
        return fundTransfer.getAmount();
//        return amount;
    }

    public void setAmount(double amount) {
        fundTransfer.setAmount(amount);
//        this.amount = amount;
    }

    public String getCurrency() {
        return fundTransfer.getCurrency();
//        return currency;
    }

    public void setCurrency(String currency) {
        fundTransfer.setCurrency(currency);
//        this.currency = currency;
    }

    public int getMethod() {
        try {
            return Integer.parseInt(fundTransfer.getPayMethod());
        } catch (NumberFormatException e) {
            return -1;
        }
//        return method;
    }

    public void setMethod(int method) {
        fundTransfer.setPayMethod(method + "");
//        this.method = method;
    }

    public String getReference() {
        return fundTransfer.getReferenceID();  //todo check for reference
//      return  fundTransfer.getCashTransactionID();  //todo check for reference
//        return reference;
    }

    public void setReference(int reference) {
        fundTransfer.setReferenceID(reference + "");
//        this.reference = reference;     //todo check for reference
    }

    public int getStatus() {
        return fundTransfer.getStatus();
        // return status;
    }

    public void setStatus(int status) {
        fundTransfer.setStatus(status);
//        this.status = status;
    }

    public String getChequeNo() {
        return fundTransfer.getChequeNumber();
//        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        // fundTransfer.setChequeNumber();
        this.chequeNo = chequeNo;
    }

    public String getChequeDate() {
        try {
            return formatted.format(unformatted.parse((fundTransfer.getChequeDate())));
        } catch (Exception e) {
            return chequeDate;
        }
    }

    public void setChequeDate(String chequeDate) {
        fundTransfer.setChequeDate(chequeDate);
//        this.chequeDate = chequeDate;
    }

    public String getBank() {
        return fundTransfer.getBank();
//        return bank;
    }

    public void setBank(String bank) {
        fundTransfer.setBank(bank);
//        this.bank = bank;
    }

    public String getDepositeDate() {
//        return depositeDate;
        try {
            if (fundTransfer.getDepositDate() != null)
                return formatted.format(unformatted.parse(fundTransfer.getDepositDate()));
            else return depositeDate;
        } catch (ParseException e) {
            return depositeDate;
        }
    }

    public void setDepositeDate(String depositeDate) {
        fundTransfer.setDepositDate(depositeDate);
//        this.depositeDate = depositeDate;
    }

    public String getReferenceNo() {
        return fundTransfer.getBankReferenceNo();   // the input text field "reference no"
//        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        fundTransfer.setBankReferenceNo(referenceNo);
//        this.referenceNo = referenceNo;
    }

    public String getNote() {
        return fundTransfer.getNotes();
//        return note;
    }

    public void setNote(String note) {
        fundTransfer.setNotes(note);
//        this.note = note;
    }

    public String getBankAddress() {
        return fundTransfer.getBankAddress();
//        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        fundTransfer.setBankAddress(bankAddress);
//        this.bankAddress = bankAddress;
    }

    public String getBranch() {
        return fundTransfer.getBranch();
//        return branch;
    }

    public void setBranch(String branch) {
        fundTransfer.setBranch(branch);
//        this.branch = branch;
    }

    public String getPortfolio() {
        return fundTransfer.getPortfolioID();
//        return portfolio;
    }

    public void setPortfolio(String portfolio) {
        fundTransfer.setPortfolioID(portfolio);
//        this.portfolio = portfolio;
    }

    public String getBankID() {
        return fundTransfer.getBankID();
//        return bankID;
    }

    public void setBankID(String bankID) {
        fundTransfer.setBankID(bankID);
//        this.bankID = bankID;
    }

    public String getReason() {
        return fundTransfer.getReason();
//        return reason;
    }

    public void setReason(String reason) {
        fundTransfer.setReason(reason);
//        this.reason = reason;
    }

    public long getDepositeDateLong() {
        try {
            return (unformatted.parse(depositeDate)).getTime();  //To change body of implemented methods use File | Settings | File Templates.
        } catch (Exception e) {
            return System.currentTimeMillis();
        }
    }

    public String getCashTransactionID() {
        return fundTransfer.getCashTransactionID();
    }

}
