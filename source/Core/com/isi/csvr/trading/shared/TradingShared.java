package com.isi.csvr.trading.shared;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.BrokerConfig;
import com.isi.csvr.trading.datastore.*;

import javax.swing.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 2:45:27 PM
 */
public class TradingShared {
    public static final String INITAL_USB_PW = "00000000";
    public static final String MIN_PRICE = "1";
    public static final String MAX_PRICE = "2";
    public static final String LAST_TRADEDPRICE = "3";
    public static final String BEST_ASK_PRICE = "4";
    public static final String BEST_ASK_QUANTITY = "5";
    public static final String TOTAL_BID_QTY = "6";
    public static final String BEST_BID_PRICE = "7";
    public static final String BEST_BID_QUANTITY = "8";
    public static final String TOTAL_OFFER_QTY = "9";
    public static final String EQUAL = "=";
    public static final String NOT_EQUAL = "!=";
    public static final String GREATER_THAN = ">";
    public static final String GREATER_THAN_OR_EQUAL = ">=";
    public static final String LESS_THAN = "<";
    public static final String LESS_THAN_OR_EQUAL = "<=";
    public static final String T39_NEW = Language.getString("ORDER_STAUS_NEW");
    public static final String T39_PARTIALLY_FILLED = Language.getString("ORDER_STAUS_PARTIALLY_FILLED");
    public static final String T39_FILLED = Language.getString("ORDER_STAUS_FILLED");
    public static final String T39_EXECUTED = Language.getString("ORDER_STAUS_EXECUTED");
    public static final String T39_DONE_FOR_DAY = Language.getString("ORDER_STAUS_DONE_FOR_DAY");
    public static final String T39_CANCELED = Language.getString("ORDER_STAUS_CANCELED");
    public static final String T39_REPLACED = Language.getString("ORDER_STAUS_REPLACED");
    public static final String T39_PENDING_CANCEL = Language.getString("ORDER_STAUS_PENDING_CANCEL");
    public static final String T39_STOPPED = Language.getString("ORDER_STAUS_STOPPED");
    public static final String T39_REJECTED = Language.getString("ORDER_STAUS_REJECTED");
    public static final String T39_REJECTED_HTML = Language.getString("ORDER_STAUS_REJECTED_HTML");
    public static final String T39_SUSPENDED = Language.getString("ORDER_STAUS_SUSPENDED");
    public static final String T39_PENDING_NEW = Language.getString("ORDER_STAUS_PENDING_NEW");
    public static final String T39_CALCULATED = Language.getString("ORDER_STAUS_CALCULATED");
    public static final String T39_EXPIRED = Language.getString("ORDER_STAUS_EXPIRED");
    public static final String T39_ACCEPTED_FOR_BIDDING = Language.getString("ORDER_STAUS_ACCEPTED_FOR_BIDDING");
    public static final String T39_PENDING_REPLACE = Language.getString("ORDER_STAUS_PENDING_REPLACE");
    public static final String T39_DEFAULT = Language.getString("ORDER_STAUS_DEFAULT");
    public static final String T39_RECEIVED = Language.getString("ORDER_STAUS_RECEIVED");
    public static final String T39_OMSACCEPTED = Language.getString("ORDER_STAUS_OMS_ACCEPTED");
    public static final String T39_SEND_TO_EXCHANGE_NEW = Language.getString("ORDER_STAUS_SEND_TO_EXCHANGE_NEW");
    public static final String T39_SEND_TO_EXCHANGE_CANCEL = Language.getString("ORDER_STAUS_SEND_TO_EXCHANGE_CANCEL");
    public static final String T39_SEND_TO_EXCHANGE_AMEND = Language.getString("ORDER_STAUS_SEND_TO_EXCHANGE_AMEND");
    public static final String T39_OMS_RECEIVED = Language.getString("ORDER_STAUS_OMS_RECEIVED");
    public static final String T39_NEW_DELETED = Language.getString("ORDER_STAUS_NEW_DELETED");
    public static final String T39_SEND_TO_OMS_NEW = Language.getString("ORDER_STAUS_SEND_TO_OMS_NEW");
    public static final String T39_SEND_TO_OMS_CANCEL = Language.getString("ORDER_STAUS_SEND_TO_OMS_CANCEL");
    public static final String T39_SEND_TO_OMS_AMEND = Language.getString("ORDER_STAUS_SEND_TO_OMS_AMEND");
    public static final String T39_NEW_WAITING = Language.getString("ORDER_STAUS_NEW_WAITING");
    public static final String T39_CANCEL_WAITING = Language.getString("ORDER_STAUS_CANCEL_WAITING");
    public static final String T39_AMEND_WAITING = Language.getString("ORDER_STAUS_AMEND_WAITING");
    public static final String T39_INITIATED = Language.getString("ORDER_STAUS_INITIATED");
    public static final String T39_REQUEST_FAILED = Language.getString("ORDER_STAUS_REQUEST_FAILED");
    public static final String T39_AMEND_REJECTED = Language.getString("ORDER_STAUS_AMEND_REJECTED");
    public static final String T39_CANCEL_REJECTED = Language.getString("ORDER_STAUS_CENCEL_REJECTED");
    public static final String T39_APPROVED = Language.getString("ORDER_STAUS_APPROVED");
    public static final String T39_APPROVAL_REJECTED = Language.getString("ORDER_STAUS_APPROVAL_REJECTED");
    public static final String T39_WAITING_FOR_APPROVAL = Language.getString("ORDER_STAUS_WAITING_FOR_APPROVAL");
    public static final String T39_APPROVAL_VALIDATION_REJECTED = Language.getString("ORDER_STAUS_APPROVAL_VALIDATION_REJECTED");
    public static final String T39_PARTIAL_FILLED_EXPIRED = Language.getString("ORDER_STAUS_PARTIAL_FILLED_EXPIRED");
    public static final String T39_CANELED_BY_EXCHANGE = Language.getString("ORDER_STAUS_CANELED_BY_EXCHANGE");
    public static final String T39_INVALIDATED_BY_CANCEL = Language.getString("ORDER_STATUS_INVALIDATED_BY_CANCEL");
    public static final String T39_PROCESSED = Language.getString("ORDER_STATUS_PROCESSED");
    public static final String T39_INVALIDATED_BY_CHANGE = Language.getString("ORDER_STATUS_INVALIDATED_BY_CHANGE");
    public static final String T39_PENDING_TRIGGER = Language.getString("ORDER_STATUS_PENDING_TRIGGER");
    public static final String T39_SEND_TO_DEALER_NEW = Language.getString("ORDER_STAUS_SEND_TO_DEALER_NEW");
    public static final String T39_SEND_TO_DEALER_CANCEL = Language.getString("ORDER_STAUS_SEND_TO_DEALER_CANCEL");
    public static final String T39_SEND_TO_DEALER_AMEND = Language.getString("ORDER_STAUS_SEND_TO_DEALER_AMEND");
    public static final String T39_PROCESSING = Language.getString("ORDER_STAUS_PROCESSING");
    public static final String T39_COMPLETED = Language.getString("ORDER_STAUS_COMPLETED");
    public static final String T39_SENDING = Language.getString("ORDER_STAUS_SENDING");
    public static final String T39_PF_CANCELLED = Language.getString("ORDER_STAUS_PF_CANCELLED");
    public static final int TEXTTYPE_DEFAULT = -1;
    public static final int TEXTTYPE_VALIDATIONMSG = 1;
    public static final int TEXTTYPE_OMSMSG = 2;
    public static final int TEXTTYPE_EXCHANGE_REJECT_REASON = 3;
    public static final int TEXTTYPE_MSG = 4;
    public static final int TEXTTYPE_ENGINE_ERROR = 5;
    public static final String TIFF_DAY = Language.getString("DAY");
    public static final String TIFF_DATE = Language.getString("DATE");
    public static final String TIFF_GTC = Language.getString("GTC");
    public static final String TIFF_FOK = Language.getString("FILL_OR_KILL");
    public static final String TIFF_MONTH = Language.getString("MONTH");
    public static final String TIFF_WEEK = Language.getString("WEEK");
    public static final String TIFF_IOC = Language.getString("IOC");
    public static final String TIFF_AOP = Language.getString("AOP");
    public static final String TIFF_GTD = Language.getString("GTD");
    public static final String TIFF_GTX = Language.getString("GTX");
    public static final String ORDER_TYPE_SQUARE_OFF = Language.getString("ORDER_TYPE_SQUARE_OFF");
    //    private static final TWComboItem ORDER_TYPE_DAY_MARKET_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_DAY_MARKET, ORDER_TYPE_DAY_MARKET);
    //    private static final TWComboItem ORDER_TYPE_DAY_LIMIT_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_DAY_LIMIT, ORDER_TYPE_DAY_LIMIT);
    public static final TWComboItem ORDER_TYPE_SQUARE_OFF_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_SQUARE_OFF, ORDER_TYPE_SQUARE_OFF);
    /*
        WIPFEE	Withdrawal Processing Fee
        MDAFEE	Market Data Access Fee
        CRPFEE	Cheque Return Processing Fee
        MISFEE	Miscellaneous
        REFUND	Refund
        COUFEE	Courier Charges
        REVBUY	Buy Reverse
        REVSEL	Sell Reverse
        TRFFEE	Bank Charges
        DEPOST	Deposit
        WITHDR	Withdraw
        STLBUY	Buy
        STLSEL	Sell
        TRNSFR	Cash Transfer
        CONOPN	Contract Open
        CONCLS	Contract Close
        MKTMKT	Mark to Market
    */
    public static final String CASHLOG_TYPE_WIPFEE = Language.getString("CASHLOG_TYPE_WIPFEE");
    public static final String CASHLOG_TYPE_MDAFEE = Language.getString("CASHLOG_TYPE_MDAFEE");
    public static final String CASHLOG_TYPE_CRPFEE = Language.getString("CASHLOG_TYPE_CRPFEE");
    public static final String CASHLOG_TYPE_MISFEE = Language.getString("CASHLOG_TYPE_MISFEE");
    public static final String CASHLOG_TYPE_REFUND = Language.getString("CASHLOG_TYPE_REFUND");
    public static final String CASHLOG_TYPE_COUFEE = Language.getString("CASHLOG_TYPE_COUFEE");
    public static final String CASHLOG_TYPE_REVBUY = Language.getString("CASHLOG_TYPE_REVBUY");
    public static final String CASHLOG_TYPE_REVSEL = Language.getString("CASHLOG_TYPE_REVSEL");
    public static final String CASHLOG_TYPE_TRFFEE = Language.getString("CASHLOG_TYPE_TRFFEE");
    public static final String CASHLOG_TYPE_DEPOST = Language.getString("CASHLOG_TYPE_DEPOST");
    public static final String CASHLOG_TYPE_WITHDR = Language.getString("CASHLOG_TYPE_WITHDR");
    public static final String CASHLOG_TYPE_STLBUY = Language.getString("CASHLOG_TYPE_STLBUY");
    public static final String CASHLOG_TYPE_STLSEL = Language.getString("CASHLOG_TYPE_STLSEL");
    public static final String CASHLOG_TYPE_TRNSFR = Language.getString("CASHLOG_TYPE_TRNSFR");
    public static final String CASHLOG_TYPE_CONOPN = Language.getString("CASHLOG_TYPE_CONOPN");
    public static final String CASHLOG_TYPE_CONCLS = Language.getString("CASHLOG_TYPE_CONCLS");
    public static final String CASHLOG_TYPE_MKTMKT = Language.getString("CASHLOG_TYPE_MKTMKT");
    public static final String NA = Language.getString("NA");
    public static final int AMEND_MODE_FULL = 0;
    private static int amendMode = AMEND_MODE_FULL;
    public static final int AMEND_MODE_DELTA = 1;
    public static final int STOP_PRICE_TYPE_LIMIT = 0;
    public static final int STOP_PRICE_TYPE_TRAILING = 1;
    public static final int STOP_PRICE_TYPE_PER_TRAILING = 2;
    private static final String BUY = Language.getString("BUY");
    private static final String SELL = Language.getString("SELL");
    private static final String AMEND = Language.getString("AMEND");
    private static final String CANCEL = Language.getString("CANCEL");
    private static final String FOREX_BUY = Language.getString("FOREX_BUY");
    private static final String FOREX_SELL = Language.getString("FOREX_SELL");
    private static final String FOREX_SHORT = Language.getString("FOREX_SHORT");
    private static final String FOREX_LONG = Language.getString("FOREX_LONG");
    private static final String FOREX_SQUARE = Language.getString("FOREX_SQUARE");
    private static final String FOREX_STATUS_OPEN = Language.getString("FOREX_STATUS_OPEN");
    private static final String FOREX_STATUS_CLOSED = Language.getString("FOREX_STATUS_CLOSED");
    private static final String FOREX_STATUS_BLOCKED = Language.getString("FOREX_STATUS_BLOCKED");
    private static final String FOREX_STATUS_SQUARED = Language.getString("FOREX_STATUS_SQUARED");
    private static final String ORDER_TYPE_LIMIT = Language.getString("ORDER_TYPE_LIMIT");
    private static final TWComboItem ORDER_TYPE_LIMIT_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_LIMIT, ORDER_TYPE_LIMIT);
    private static final String ORDER_TYPE_MARKET = Language.getString("ORDER_TYPE_MARKET");
    private static final TWComboItem ORDER_TYPE_MARKET_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_MARKET, ORDER_TYPE_MARKET);
    private static final String ORDER_TYPE_STOPLOSS_MARKET = Language.getString("ORDER_TYPE_STOP_MARKET");
    private static final TWComboItem ORDER_TYPE_STOPLOSS_MARKET_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_STOPLOSS_MARKET, ORDER_TYPE_STOPLOSS_MARKET);
    private static final String ORDER_TYPE_STOPLOSS_LIMIT = Language.getString("ORDER_TYPE_STOP_LIMIT");
    private static final TWComboItem ORDER_TYPE_STOPLOSS_LIMIT_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT, ORDER_TYPE_STOPLOSS_LIMIT);
    //    private static final String ORDER_TYPE_STOPLOSS_MARKET = Language.getString("ORDER_TYPE_STOPLOSS_MARKET");
    //    private static final String ORDER_TYPE_STOPLOSS_LIMIT = Language.getString("ORDER_TYPE_STOPLOSS_LIMIT");
    private static final String ORDER_TYPE_TRAILING_STOP_MARKET = Language.getString("ORDER_TYPE_TRAILING_STOP_MARKET");
    private static final String ORDER_TYPE_TRAILING_STOP_LIMIT = Language.getString("ORDER_TYPE_TRAILING_STOP_LIMIT");
    private static final String ORDER_TYPE_MARKET_ON_CLOSE = Language.getString("ORDER_TYPE_MARKET_ON_CLOSE");
    private static final TWComboItem ORDER_TYPE_MARKET_ON_CLOSE_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE, ORDER_TYPE_MARKET_ON_CLOSE);
    private static final String ORDER_TYPE_LIMIT_ON_CLOSE = Language.getString("ORDER_TYPE_LIMIT_ON_CLOSE");
    //    private static final TWComboItem ORDER_TYPE_TRAILING_STOP_MARKET_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_TRAILING_STOP_MARKET, ORDER_TYPE_TRAILING_STOP_MARKET);
    //    private static final TWComboItem ORDER_TYPE_TRAILING_STOP_LIMIT_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_TRAILING_STOP_LIMIT, ORDER_TYPE_TRAILING_STOP_LIMIT);
    private static final TWComboItem ORDER_TYPE_LIMIT_ON_CLOSE_ITEM = new TWComboItem("" + TradeMeta.ORDER_TYPE_LIMIT_ON_CLOSE, ORDER_TYPE_LIMIT_ON_CLOSE);
    private static final String ORDER_TYPE_DAY_MARKET = Language.getString("ORDER_TYPE_DAY_MARKET");
    private static final String ORDER_TYPE_DAY_LIMIT = Language.getString("ORDER_TYPE_DAY_LIMIT");
    public static boolean level2AuthenticationSucess = false;
    public static boolean level2AuthenticationPending = false;
    public static boolean TRADING_ENABLED = false;
    public static boolean okForTradeConnection = false;
    public static double commission = .0015;
    public static long messageID = 0;
    public static boolean SECONDARY_LOGIN_SUCCESS = false;
    public static boolean IS_SECONDARY_LOGIN_ENABLED = false;
    public static boolean INVALID_SECONDARY_LOGIN = false;
    public static int INVALID_SECONDARY_LOGIN_ATEMPTS = 0;
    public static String STOP_PRICE_TYPE_LIMIT_STR = Language.getString("STOP_PRICE_TYPE_LIMIT");
    public static String STOP_PRICE_TYPE_TRAILING_STR = Language.getString("STOP_PRICE_TYPE_TRAILING");
    public static String STOP_PRICE_TYPE_PER_TRAILING_STR = Language.getString("STOP_PRICE_TYPE_PER_TRAILING");
    public static String TRADE_USER_STATUS = "";
    private static boolean connected;
    private static boolean ready;
    private static SimpleDateFormat goodTillFormat;
    private static SimpleDateFormat goodTillFormatyyyymmdd;
    private static SimpleDateFormat yyyyMMddFormat;
    private static SimpleDateFormat inverseGoodTillFormat;
    private static SimpleDateFormat yyyy_MM_ddFormat;
    private static SimpleDateFormat twTillFormat;
    private static String tradeUserName = "";
    private static int level2AuthType = 1;
    private static String level1Password;
    private static String level2Password;
    private static String usbPin;
    private static String currentDay;
    private static String sessionID = "*";
    private static String trsID = null;
    private static String ip = "*";
    private static Trader trader;
    private static String brokerID = "";
    private static String reportURL;
    private static int reportDepth;
    private static String defaultCurrency;
    private static boolean isConnecting = false;
    private static String uuid = "";
    private static boolean useVirtualKB = true;
    private static int disconnectionMode = TradingConstants.DISCONNECTION_UNEXPECTED;
    private static String questionList = "";
    //    private static boolean isUserDetailsNeeded = false;
    private static byte changePWDType = TradingConstants.CHANGE_PASSWORD_TYPE_NONE;
    private static boolean isChangePassowrdNeeded = false;
    private static int maxCondionCount = 10;
    private static boolean isBookKeepersAvailable = false;
    private static String duInvestURL;

    private static boolean keepOrderWindowOpen = false;
    private static boolean showConfigPopup = true;
    private static boolean isMandatoryPWDChangeSuccess = true;
    // private static int coveragedecimals = 2;

    public static Trader createTrader() {
        if (trader != null) {
            trader.inInitialize();
        }
        trader = new Trader();
        return trader;
    }

    public static Trader getTrader() {
        return trader;
    }

    public static String getQuestionList() {
        return questionList;
    }

    public static void setQuestionList(String questionList) {
        TradingShared.questionList = questionList;
    }

//    public static boolean isUserDetailsNeeded() {
//        return isUserDetailsNeeded;
//    }
//
//    public static void setUserDetailsNeeded(boolean userDetailsNeeded) {
//        isUserDetailsNeeded = userDetailsNeeded;
//    }

    public static boolean isChangePassowrdNeeded() {
        return isChangePassowrdNeeded;
    }

    public static void setChangePassowrdNeeded(boolean changePassowrdNeeded) {
        isChangePassowrdNeeded = changePassowrdNeeded;
    }

    public static byte getChangePWDType() {
//        return TradingConstants.CHANGE_PASSWORD_TYPE_NONE;
        return changePWDType;
    }

    public static void setChangePWDType(byte changePWDType) {
        TradingShared.changePWDType = changePWDType;
    }

    public static String getTradeUserName() {
        return tradeUserName;
    }

    public static void setTradeUserName(String username) {
        tradeUserName = username;
    }

    public static void clearAccounts() {
        try {
            trader.inInitialize();
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    /*public static void setDefaultCurrency(String currencyID){
        defaultCurrency = currencyID;
    }
    public static String getDefaultCurrency(){
        return defaultCurrency;
    }*/


    /* Returns default TIF type for Market Types. 
    *     return FOK for market orders
    *     return DAY for limit orders
    * */

    public static int getDefaultTifType(char type) {
        switch (type) {
            case '1':
                return TradeMeta.TIF_FOK;
            case '2':
                return TradeMeta.TIF_DAY;
            default:
                return TradeMeta.TIF_DAY;
        }
    }

    public static String getTiffString(int id, String defaultValue) {
        switch (id) {
            case TradeMeta.TIF_GTD:
                if (defaultValue == null)
                    return TIFF_DATE;
                else
                    return defaultValue;
            case TradeMeta.TIF_DAY:
                return TIFF_DAY;
            case TradeMeta.TIF_GTC:
                return TIFF_GTC;
            case TradeMeta.TIF_FOK:
                return TIFF_FOK;
            case TradeMeta.TIF_WEEK:
                return TIFF_WEEK;
            case TradeMeta.TIF_MONTH:
                return TIFF_MONTH;
            case TradeMeta.TIF_AOP:
                return TIFF_AOP;
            case TradeMeta.TIF_IOC:
                return TIFF_IOC;
            case TradeMeta.TIF_GTX:
                return TIFF_GTX;
            default:
                if (defaultValue == null)
                    return Language.getString("NA");
                else
                    return defaultValue;
        }
    }

    public static String getActionString(int type) {
        if (type == TradeMeta.BUY) {
            return BUY;
        } else if (type == TradeMeta.SELL) {
            return SELL;
        } else if (type == TradeMeta.AMEND) {
            return AMEND;
        } else if (type == TradeMeta.CANCEL) {
            return CANCEL;
        }
        return "";
    }

    public static String getForexActionString(int type) {
        if (type == TradeMeta.BUY) {
            return FOREX_BUY;
        } else if (type == TradeMeta.SELL) {
            return FOREX_SELL;
        } else if (type == TradeMeta.SHORT) {
            return FOREX_SHORT;
        } else if (type == TradeMeta.LONG) {
            return FOREX_LONG;
        } else if (type == TradeMeta.SQUARE) {
            return FOREX_SQUARE;
        }
        return "";
    }

    public static String getForexStatusString(int type) {
        if (type == TradeMeta.OPEN) {
            return FOREX_STATUS_OPEN;
        } else if (type == TradeMeta.CLOSED) {
            return FOREX_STATUS_CLOSED;
        } else if (type == TradeMeta.BLOCKED) {
            return FOREX_STATUS_BLOCKED;
        } else if (type == TradeMeta.SQUARED) {
            return FOREX_STATUS_SQUARED;
        }
        return "";
    }

    public static String getOMSStatusString(int type) {
        return getOMSStatusString(type, false);
    }

    public static String getOMSStatusString(int type, boolean useHTML) {
        switch (type) {
            case TradeMeta.T39_Accepted_For_Bidding:
                return T39_ACCEPTED_FOR_BIDDING;
            case TradeMeta.T39_Calculated:
                return T39_CALCULATED;
            case TradeMeta.T39_Canceled:
                return T39_CANCELED;
            case TradeMeta.T39_DEFAULT:
                return T39_DEFAULT;
            case TradeMeta.T39_Done_For_Day:
                return T39_DONE_FOR_DAY;
            case TradeMeta.T39_Expired:
                return T39_EXPIRED;
            case TradeMeta.T39_Filled:
                return T39_FILLED;
            case TradeMeta.T39_New:
                return T39_NEW;
            case TradeMeta.T39_OMSACCEPTED:
                return T39_OMSACCEPTED;
            case TradeMeta.T39_Partially_Filled:
                return T39_PARTIALLY_FILLED;
            case TradeMeta.T39_Pending_Cancel:
                return T39_PENDING_CANCEL;
            case TradeMeta.T39_Pending_New:
                return T39_PENDING_NEW;
            case TradeMeta.T39_Pending_Replace:
                return T39_PENDING_REPLACE;
            case TradeMeta.T39_RECEIVED:
                return T39_RECEIVED;
            case TradeMeta.T39_Rejected:
                if (useHTML) {
                    return T39_REJECTED_HTML;
                } else {
                    return T39_REJECTED;
                }
            case TradeMeta.T39_Replaced:
                return T39_REPLACED;
            case TradeMeta.T39_SEND_TO_EXCHANGE_AMEND:
                return T39_SEND_TO_EXCHANGE_AMEND;
            case TradeMeta.T39_SEND_TO_EXCHANGE_CANCEL:
                return T39_SEND_TO_EXCHANGE_CANCEL;
            case TradeMeta.T39_SEND_TO_EXCHANGE_NEW:
                return T39_SEND_TO_EXCHANGE_NEW;
            case TradeMeta.T39_Stopped:
                return T39_STOPPED;
            case TradeMeta.T39_Suspended:
                return T39_SUSPENDED;
            case TradeMeta.T39_Executed:
                return T39_EXECUTED;
            case TradeMeta.T39_OMS_RECEIVED:
                return T39_OMS_RECEIVED;
            case TradeMeta.T39_NEW_DELETED:
                return T39_NEW_DELETED;
            case TradeMeta.T39_SEND_TO_OMS_NEW:
                return T39_SEND_TO_OMS_NEW;
            case TradeMeta.T39_SEND_TO_OMS_CANCEL:
                return T39_SEND_TO_OMS_CANCEL;
            case TradeMeta.T39_SEND_TO_OMS_AMEND:
                return T39_SEND_TO_OMS_AMEND;
            case TradeMeta.T39_NEW_WAITING:
                return T39_NEW_WAITING;
            case TradeMeta.T39_CANCEL_WAITING:
                return T39_CANCEL_WAITING;
            case TradeMeta.T39_AMEND_WAITING:
                return T39_AMEND_WAITING;
            case TradeMeta.T39_INITIATED:
                return T39_INITIATED;
            case TradeMeta.T39_REQUEST_FAILED:
                return T39_REQUEST_FAILED;
            case TradeMeta.T39_AMEND_REJECTED:
                return T39_AMEND_REJECTED;
            case TradeMeta.T39_CANCEL_REJECTED:
                return T39_CANCEL_REJECTED;
            case TradeMeta.T39_APPROVED:
                return T39_APPROVED;
            case TradeMeta.T39_APPROVAL_REJECTED:
                return T39_APPROVAL_REJECTED;
            case TradeMeta.T39_WAITING_FOR_APPROVAL:
                return T39_WAITING_FOR_APPROVAL;
            case TradeMeta.T39_APPROVAL_VALIDATION_REJECTED:
                return T39_APPROVAL_VALIDATION_REJECTED;
//            case TradeMeta.T39_PARTIAL_FILLED_EXPIRED:
//                return T39_PARTIAL_FILLED_EXPIRED;
            case TradeMeta.T39_CANELED_BY_EXCHANGE:
                return T39_CANELED_BY_EXCHANGE;
            case TradeMeta.T39_INVALIDATED_BY_CANCEL:
                return T39_INVALIDATED_BY_CANCEL;
            case TradeMeta.T39_PROCESSED:
                return T39_PROCESSED;
            case TradeMeta.T39_INVALIDATED_BY_CHANAGE:
            case TradeMeta.T39_INVALIDATED_BY_REPLACE:
                return T39_INVALIDATED_BY_CHANGE;
            case TradeMeta.T39_PENDING_TRIGGER:
                return T39_PENDING_TRIGGER;
            case TradeMeta.T39_SEND_TO_DEALER_NEW:
                return T39_SEND_TO_DEALER_NEW;
            case TradeMeta.T39_SEND_TO_DEALER_AMEND:
                return T39_SEND_TO_DEALER_AMEND;
            case TradeMeta.T39_SEND_TO_DEALER_CANCEL:
                return T39_SEND_TO_DEALER_CANCEL;
            case TradeMeta.T39_PROCESSING:
                return T39_PROCESSING;
            case TradeMeta.T39_COMPLETED:
            case TradeMeta.T39_Completed:
                return T39_COMPLETED;
            case TradeMeta.T39_PF_CANCELLED:
                return T39_PF_CANCELLED;
            case TradeMeta.T39_SENDING:
                return T39_SENDING;
            default:
                return NA;
        }
    }

    public static int getLevel2AuthType() {
        return level2AuthType;
    }

    public static void setLevel2AuthType(int type) {
        if (type == -1) {
            level2AuthType = TradeMeta.LEVEL2_AUTH_NONE;
        } else {
            level2AuthType = type;
        }
        TRADING_ENABLED = type > 0;
    }

    public static int getLevel2AuthType(String portfolio) {
        return trader.getLevel2AuthType(portfolio);
    }

    public static String getTypeString(int type) {
        switch (type) {
            case TradeMeta.ORDER_TYPE_LIMIT:
                return ORDER_TYPE_LIMIT;
            case TradeMeta.ORDER_TYPE_MARKET:
                return ORDER_TYPE_MARKET;
//            case TradeMeta.ORDER_TYPE_STOP_MARKET:
//                return ORDER_TYPE_STOP_MARKET;
//            case TradeMeta.ORDER_TYPE_STOP_LIMIT:
//                return ORDER_TYPE_STOP_LIMIT;
            case TradeMeta.ORDER_TYPE_STOPLOSS_MARKET:
                return ORDER_TYPE_STOPLOSS_MARKET;
            case TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT:
                return ORDER_TYPE_STOPLOSS_LIMIT;
//            case TradeMeta.ORDER_TYPE_DAY_LIMIT:
//                return ORDER_TYPE_DAY_LIMIT;
//            case TradeMeta.ORDER_TYPE_DAY_MARKET:
//                return ORDER_TYPE_DAY_MARKET;
            case TradeMeta.ORDER_TYPE_LIMIT_ON_CLOSE:
                return ORDER_TYPE_LIMIT_ON_CLOSE;
            case TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE:
                return ORDER_TYPE_MARKET_ON_CLOSE;
            case TradeMeta.ORDER_TYPE_SQUARE_OFF:
                return ORDER_TYPE_SQUARE_OFF;
            case TradeMeta.ORDER_TYPE_TRAILING_STOP_LIMIT:
                return ORDER_TYPE_TRAILING_STOP_LIMIT;
            case TradeMeta.ORDER_TYPE_TRAILING_STOP_MARKET:
                return ORDER_TYPE_TRAILING_STOP_MARKET;
            default:
                return NA;

        }
        /*if (type == TradeMeta.ORDER_TYPE_LIMIT){
            return ORDER_TYPE_LIMIT;
        }else{
            return ORDER_TYPE_MARKET;
        }*/
    }

    public static String formatExpireDate(long date) {
        SimpleDateFormat format;
        try {
            format = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
            return format.format(new Date(date));
        } finally {
            format = null;
        }
    }

    public static String formatExpireDate(long date, short tiff) {
        SimpleDateFormat format;
        try {
            if ((tiff == 4) || (tiff == 1)) {
                return "";
            } else {
                format = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
                return format.format(new Date(date));
            }
        } finally {
            format = null;
        }
    }

    public static String formatOpraExpireDate(long date) {
        SimpleDateFormat format;
        try {
            format = new SimpleDateFormat("yyyyMM");
            return format.format(new Date(date));
        } finally {
            format = null;
        }
    }

    public static boolean isTiffDateValid(int tiff) {
        //           GTC
        if ((tiff == 1)) {
            return false;
        } else {
            return true;
        }
    }

    public static String formatGoodTill(Date date) {
        if (goodTillFormat == null) {
            goodTillFormat = new SimpleDateFormat("dd / MM / yyyy");
        }
        return goodTillFormat.format(date);
    }

    public static String formatGoodTill(long date) {
        if (goodTillFormat == null) {
            goodTillFormat = new SimpleDateFormat("dd / MM / yyyy");
        }
        return goodTillFormat.format(new Date(date));
    }

    public static String formatGoodTill(String date) {
        try {
            if (goodTillFormat == null) {
                goodTillFormat = new SimpleDateFormat("dd / MM / yyyy");
            }

            if (date.length() == 10) {
                if (yyyy_MM_ddFormat == null) {
                    yyyy_MM_ddFormat = new SimpleDateFormat("yyyy-MM-dd");
                }
                return goodTillFormat.format(yyyy_MM_ddFormat.parse(date));
            } else {
                if (inverseGoodTillFormat == null) {
                    inverseGoodTillFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
                }
                return goodTillFormat.format(inverseGoodTillFormat.parse(date));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return Language.getString("NA");
        }
    }

    public static String formatTWToyyyyMMdd(String date) {
        try {
            if (goodTillFormat == null) {
                goodTillFormat = new SimpleDateFormat("dd / MM / yyyy");
            }
            if (yyyyMMddFormat == null) {
                yyyyMMddFormat = new SimpleDateFormat("yyyyMMdd");
            }
            return yyyyMMddFormat.format(goodTillFormat.parse(date));
        } catch (Exception e) {
//            e.printStackTrace();
            return Language.getString("NA");
        }
    }

    public static String formatLongTimeToTW(long date) {

        if (twTillFormat == null) {
            twTillFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        }
        return twTillFormat.format(new Date(date));
    }

    public static String formatFIXTimeToTW(String date) {
        try {
            if (twTillFormat == null) {
                twTillFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            }
            if (inverseGoodTillFormat == null) {
                inverseGoodTillFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
            }
            return twTillFormat.format(inverseGoodTillFormat.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
            return Language.getString("NA");
        }
    }

    public static long formatFIXTimeToLong(String date) {
        try {
            if (date != null)
                date = date.replaceAll("T", "");
            if (date.length() == 10) {
                if (yyyy_MM_ddFormat == null) {
                    yyyy_MM_ddFormat = new SimpleDateFormat("yyyy-MM-dd");
                }
                return yyyy_MM_ddFormat.parse(date).getTime();
            } else {
                if (inverseGoodTillFormat == null) {
                    inverseGoodTillFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
                }
                return inverseGoodTillFormat.parse(date).getTime();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String formatLongToFIXTime(long date) {
        try {
            if (inverseGoodTillFormat == null) {
                inverseGoodTillFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
            }
            return inverseGoodTillFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getGoodTillDate(long time) {
        Calendar cal;

        try {
            if (time == -1) { // GTC
                return Language.getString("GTC");
            } else {
                cal = Calendar.getInstance();
                cal.setTimeInMillis(time);
                return formatGoodTill(cal.getTime());
            }
        } catch (Exception e) {
            return "";
        } finally {
            cal = null;
        }
    }

    public static long getGoodTillLong(String formatted) {
        try {
            if (goodTillFormat == null) {
                goodTillFormat = new SimpleDateFormat("dd / MM / yyyy");
            }
            Date goodTillDate = goodTillFormat.parse(formatted);

            return goodTillDate.getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    public static long getGoodTillLongYYYYDDMMHHMMSS(String formatted) {
        try {
            if (goodTillFormatyyyymmdd == null) {
                goodTillFormatyyyymmdd = new SimpleDateFormat("yyyyMMddhhmmss");
            }
            Date goodTillDate = goodTillFormatyyyymmdd.parse(formatted);

            return goodTillDate.getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    public static void populateOrderTypes(String[] types, ArrayList list, byte path) {
        //todo - added temparary to enable the order types
//        types = new String[]{"1","2","3","4","5","6","7","8","9","a","b","c","B"};
        for (int i = 0; i < types.length; i++) {
            if (types[i].equals("" + TradeMeta.ORDER_TYPE_LIMIT)) {
                list.add(ORDER_TYPE_LIMIT_ITEM);
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_MARKET)) {
                list.add(ORDER_TYPE_MARKET_ITEM);
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_STOPLOSS_MARKET)) {
                if (isBracketOrderEnabled(path)) {
                    list.add(ORDER_TYPE_STOPLOSS_MARKET_ITEM);
                }
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT)) {
                if (isBracketOrderEnabled(path)) {
                    list.add(ORDER_TYPE_STOPLOSS_LIMIT_ITEM);
                }
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_LIMIT_ON_CLOSE)) {
                list.add(ORDER_TYPE_LIMIT_ON_CLOSE_ITEM);
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE)) {
                list.add(ORDER_TYPE_MARKET_ON_CLOSE_ITEM);
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_SQUARE_OFF)) {
                list.add(ORDER_TYPE_SQUARE_OFF_ITEM);
            }
        }
    }

    public static void populateSquareOffOrderType(ArrayList list) {
        list.add(ORDER_TYPE_SQUARE_OFF_ITEM);
    }

    public static void populateRapidOrderTypes(String[] types, ArrayList list) {
        //todo - added temparary to enable the order types
//        types = new String[]{"1","2","3","4","5","6","7","8","9","a","b","c","y","z","B"};
        for (int i = 0; i < types.length; i++) {
            if (types[i].equals("" + TradeMeta.ORDER_TYPE_LIMIT)) {
                list.add(ORDER_TYPE_LIMIT_ITEM);
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_MARKET)) {
                list.add(ORDER_TYPE_MARKET_ITEM);
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_DAY_LIMIT)) {
//                list.add(ORDER_TYPE_DAY_LIMIT_ITEM);
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_DAY_MARKET)) {
//                list.add(ORDER_TYPE_DAY_MARKET_ITEM);
            } else if (types[i].equals("" + TradeMeta.ORDER_TYPE_SQUARE_OFF)) {
                list.add(ORDER_TYPE_SQUARE_OFF_ITEM);
            }
        }
    }

    public static double getCommissionPercentage() {
        return commission;
    }

    public static void setCommissionPercentage(double commission) {
        TradingShared.commission = commission;
    }

    public static double getCommission(double value) {
        return Math.max(value * commission, 15);
    }

    public static boolean isConnected() {
        return connected;
    }

    public static void setConnected() {
        TradingShared.connected = true;
        Client.getInstance().setTitleText();
    }

    public static void setDisconnected() {
        TradingShared.connected = false;
        TradingShared.SECONDARY_LOGIN_SUCCESS = false;
        TradingShared.setReadyForTrading(false);
        TradingShared.setSecondaryLoginEnabled(false);
        Client.getInstance().setTitleText();
    }

    public static synchronized long getNextMessageID() {
        return messageID++;
    }

    public static synchronized void resetMessageID() {
        TradingShared.messageID = 0;
    }

    public static synchronized double getDoubleValue(String str) {
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static synchronized int getIntValue(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static synchronized long getLongValue(String str) {
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static String getLevel2Password() {
        return level2Password;
    }

    public static void setLevel2Password(String level2Password) {
        if (level2Password != null)
            TradingShared.level2Password = level2Password;
        else
            TradingShared.level2Password = null;
    }

    public static void setLevel2AuthenticationPending(boolean status) {
        TradingShared.level2AuthenticationPending = status;
    }

    public static String getUSBPin() {
        return usbPin;
    }

    public static void setUSBPin(String usbPin) {
        TradingShared.usbPin = usbPin;
    }

    public static String getCurrentDay() {
        return currentDay;
    }

    public static void setCurrentDay(String currentDay) {
        TradingShared.currentDay = currentDay;
    }

    public static boolean isReadyForTrading() {
        return ready;
    }

    public static void setReadyForTrading(boolean ready) {
        TradingShared.ready = ready;
    }

    public static String getSessionID() {
        return sessionID;
    }

    public static void setSessionID(String sessionID) {
        TradingShared.sessionID = sessionID;
    }

    public static String getIp() {
        return ip;
    }

    public static String setIp(String ip) {
        TradingShared.ip = ip;
        return ip;
    }

    public static boolean isOkForTradeConnection() {
        return okForTradeConnection;
    }

    public static void setOkForTradeConnection(boolean okForTradeConnection) {
        TradingShared.okForTradeConnection = okForTradeConnection;
        Client.getInstance().setTitleText();
    }

    public static String getLevel1Password() {
        return level1Password;
    }

    public static void setLevel1Password(String level1Password) {
        TradingShared.level1Password = level1Password;
    }

    public static boolean isConnecting() {
        return isConnecting;
    }

    public static void setConnecting(boolean connecting) {
        isConnecting = connecting;
        Client.getInstance().setTitleText();
    }

    public static boolean isExchangeAllowedToTrade(String exchange, boolean showMessage) {
        if (isTradableExchange(exchange)) {
            return true;
        } else {
            String msg = Language.getString("CANNOT_TRADE_SYMBOL");
            msg = msg.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getDescription(exchange));
            SharedMethods.showMessage(msg, JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    public static void setReportURL(String reportURL) {
        TradingShared.reportURL = reportURL;
    }

    public static void setReportDepth(int reportDepth) {
        TradingShared.reportDepth = reportDepth;
    }

    public static Enumeration getTradingExchanges() {
        try {
            return getTrader().getExchanges();
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isTradableExchange(String exchange) {
        try {
            return getTrader().isTradableExchange(exchange);
        } catch (Exception e) {
            return false;
        }
    }
/*
    public static boolean isConditionalTradableExchange() {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_MANUAL_BROKER);
        } catch (Exception e) {
            return false;
        }
    }*/

    public static boolean isConditionalOrdersEnabled(byte path) {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_CONDITIONAL_ORDERS, path);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isManaulBroker() {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_MANUAL_BROKER);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isManaulBroker(byte path) {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_MANUAL_BROKER, path);
        } catch (Exception e) {
            return false;
        }
    }

    /* public static boolean isBracketOrderEnabled() {
//         return false;
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_ENABLE_BRACKET_ORDERS);
        } catch (Exception e) {
            return false;
        }
    }*/

    public static boolean isBracketOrderEnabled(byte path) {
//         return false;
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_ENABLE_BRACKET_ORDERS, path);
        } catch (Exception e) {
            return false;
        }
    }

    /*public static boolean isAdvacedOrderEnabled() {
     return false;
    *//*try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_ENABLE_ADVANCED_ORDERS);
        } catch (Exception e) {
            return false;
        }*//*
    }*/

    public static boolean isSlicedOrderEnabled(byte path) {
//        return false;
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_ENABLE_SLICED_ORDERS, path);
        } catch (Exception e) {
            return false;
        }
    }

    /*public static boolean isTradeServicesEnable(){
         try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_TRADE_SERVICES);
         } catch (Exception e) {
         return false;
    }
    }*/

    public static boolean isTradeServicesEnable(byte path) {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_TRADE_SERVICES, path);
        } catch (Exception e) {
            return false;
        }
    }

    /*public static boolean isDayOrdersEnabled() {
         try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_ENABLE_DAY_ORDERS);
        } catch (Exception e) {
            return false;
        }
    }*/

    public static boolean isDayOrdersEnabled(byte path) {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_ENABLE_DAY_ORDERS, path);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isTradingAPIEnabled() {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_ENABLE_TRADE_API);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isTradingAPIEnabled(byte path) {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_ENABLE_TRADE_API, path);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isBlotterRefreshPullOnly() {
        try {
            Exchange exchangeObj = ExchangeStore.getSharedInstance().getApplicationExchange();
            return exchangeObj.isValidTradingIinformationType(TradeMeta.TIT_BLOTTER_PULL_ONLY);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isT0ordersEnable() {
        if (TPlusStore.getSharedInstance().getTplusRecodrsStore().size() > 0) {
            return true;
        } else {
            return false;
        }
//        return false;
    }

    public static boolean isT0ordersEnable(String exchange) {
        try {
            if (TPlusStore.getSharedInstance().getMainTPlusStore().containsKey(exchange)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isShowOrderNotoficationPopup() {
        return Settings.getBooleanItem("SHOW_ORDER_NOTIFICATION_POPUP", true);
    }

    public static void setShowOrderNotoficationPopup(boolean showOrderNotoficationPopup) {
        Settings.setItem("SHOW_ORDER_NOTIFICATION_POPUP", "" + showOrderNotoficationPopup);
    }

    public static boolean isSelectDayOrder() {
        return Settings.getBooleanItem("DEFAULT_SELECT_DAY_ORDER", false);
    }

    public static void setSelectDayOrder(boolean defaultSelectDayOrder) {
        Settings.setItem("DEFAULT_SELECT_DAY_ORDER", "" + defaultSelectDayOrder);
    }

    public static boolean isShariaTestPassed(String key) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);
        return isShariaTestPassed(exchange, symbol, instrument);
    }

    public static boolean isShariaTestPassed(String exchange, String symbol, int instrument) {
        try {
            Rule rule = RuleManager.getSharedInstance().getRule("SHARIA_RESTRICTED_SYMBOLS", exchange, "NEW_ORDER");
            String[] symbols = rule.getRule().split(",");

            for (int i = 0; i < symbols.length; i++) {
                if (symbols[i].equals(symbol)) { // sharia restricted symbol
                    if (getShariaRestrictionLevel(exchange) == TradeMeta.SHARIA_WARNING) {
                        int result = SharedMethods.showConfirmMessage(Language.getString("MSG_SHARIA_WARNING_TRADE"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                        if (result == JOptionPane.YES_OPTION) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        SharedMethods.showMessage(Language.getString("MSG_SHARIA_RESTRICTED_TRADE"), JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    private static int getShariaRestrictionLevel(String exchange) {
        try {
            Rule rule = RuleManager.getSharedInstance().getRule("SHARIA_RESTRICTION_LEVEL", exchange, "NEW_ORDER");
            return Integer.parseInt(rule.getRule().trim());
        } catch (Exception e) {
            return TradeMeta.SHARIA_WARNING;
        }
    }

    public static boolean isManualDisconnection() {
        return disconnectionMode == TradingConstants.DISCONNECTION_MANUAL;
    }

    public static boolean isUnexpectedDisconnection() {
        return disconnectionMode == TradingConstants.DISCONNECTION_UNEXPECTED;
    }

    public static boolean isPriceDisconnection() {
        return disconnectionMode == TradingConstants.DISCONNECTION_PRICE_CONNECTION;
    }

    public static void setDisconnectionMode(int disconnectionMode) {
        TradingShared.disconnectionMode = disconnectionMode;
    }

    public static String getMessageString(String sID) {
        String sString = null;

        sString = Language.getString(sID);
        if (sString == null) {
            return "";
        } else {
            if (sString.indexOf("[BROKER_NAME]") >= 0)
                sString = sString.replaceFirst("\\[BROKER_NAME\\]", getBrokerID());
//                sString = sString.replaceFirst("\\[BROKER_NAME\\]", Brokers.getSharedInstance().getBrokerName(Brokers.getSharedInstance().getSelectedBrokerID()));
            if (sString.indexOf("[BROKER_FAX]") >= 0)
                sString = sString.replaceFirst("\\[BROKER_FAX\\]", Settings.getBrokerFax());
            if (sString.indexOf("[BROKER_PHONE]") >= 0)
                sString = sString.replaceFirst("\\[BROKER_PHONE\\]", Settings.getBrokerPhone());
            if (sString.indexOf("[BROKER_URL]") >= 0)
                sString = sString.replaceFirst("\\[BROKER_URL\\]", Settings.getBrokerURL());
            if (sString.indexOf("[DATACENTER_FAX]") >= 0)
                sString = sString.replaceFirst("\\[DATACENTER_FAX\\]", Settings.getBrokerFax());
            if (sString.indexOf("[DATACENTER_PHONE]") >= 0)
                sString = sString.replaceFirst("\\[DATACENTER_PHONE\\]", Settings.getBrokerPhone());
            if (sString.indexOf("[DATACENTER_URL]") >= 0)
                sString = sString.replaceFirst("\\[DATACENTER_URL\\]", Settings.getBrokerURL());
            return sString;
        }
    }

    public static String getFundTransferStatus(int type) {
        switch (type) {
            case TradingConstants.TRANSFER_STATUS_PENDING:
                return Language.getString("FT_PENDING");
            case TradingConstants.TRANSFER_STATUS_VALIDATED:
                return Language.getString("FT_VALIDATED");
            case TradingConstants.TRANSFER_STATUS_L1_APPROVED:
                return Language.getString("FT_LEVEL_1_APPROVED");
            case TradingConstants.TRANSFER_STATUS_L2_APPROVED:
                return Language.getString("FT_LEVEL_2_APPROVED");
            case TradingConstants.TRANSFER_STATUS_CANCELLED:
                return Language.getString("FT_CANCELLED");
            case TradingConstants.TRANSFER_STATUS_REJECTED:
                return Language.getString("FT_REJECTED");
            case TradingConstants.TRANSFER_STATUS_APPROVED:
                return Language.getString("FT_APPROVED");
            default:
                return Language.getString("NA");
        }
    }

    public static String getFundTransferType(int type) {
        switch (type) {
            case TradingConstants.TRANSFER_FUND_TRANSFER:
                return Language.getString("FUND_TRANSFER_NOTIFICATION");
            case TradingConstants.TRANSFER_WITHDRAWAL:
                return Language.getString("WITHDRAWAL_REQUEST");
            case TradingConstants.TRANSFER_STOCK_TRANSFER:
                return Language.getString("STOCK_TRANSFER_REQUEST");
            default:
                return Language.getString("NA");
        }
    }

    public static String getFundTransferMethod(int type) {
        switch (type) {
            case TradeMeta.TRANSFER_CHEQUE:
                return Language.getString("CHEQUE");
            case TradeMeta.TRANSFER_BANK:
                return Language.getString("BANK_TRANSFER");
            case TradeMeta.TRANSFER_CASH:
                return Language.getString("CASH");
            default:
                return Language.getString("NA");
        }
    }

    public static void getWithdrawalMethods(ArrayList<TWComboItem> methods, String portfolio) {
        methods.clear();
        String[] types = BrokerConfig.getSharedInstance().getWithdrawalTypes(TradingShared.getTrader().getPath(portfolio));
        if (types == null) { // no predefined types. Add all
            methods.add(new TWComboItem(TradeMeta.TRANSFER_BANK, Language.getString("BANK_TRANSFER")));
            methods.add(new TWComboItem(TradeMeta.TRANSFER_CHEQUE, Language.getString("CHEQUE")));
        } else {
            for (String type : types) {
                if (type.equals("" + TradeMeta.TRANSFER_BANK)) {
                    methods.add(new TWComboItem(TradeMeta.TRANSFER_BANK, Language.getString("BANK_TRANSFER")));
                } else if (type.equals("" + TradeMeta.TRANSFER_CHEQUE)) {
                    methods.add(new TWComboItem(TradeMeta.TRANSFER_CHEQUE, Language.getString("CHEQUE")));
                }

            }
        }
    }

    public static void getDepositMethods(ArrayList<TWComboItem> methods, String portfolio, boolean... all) {
        String[] types = BrokerConfig.getSharedInstance().getDepositTypes(TradingShared.getTrader().getPath(portfolio));
        if ((types == null) || ((all.length > 0) && (all[0]))) { // no predefined types. Add all
            methods.add(new TWComboItem(TradeMeta.TRANSFER_BANK, Language.getString("BANK_TRANSFER")));
            methods.add(new TWComboItem(TradeMeta.TRANSFER_CHEQUE, Language.getString("CHEQUE")));
            methods.add(new TWComboItem(TradeMeta.TRANSFER_CASH, Language.getString("CASH")));
        } else {
            for (String type : types) {
                if (type.equals("" + TradeMeta.TRANSFER_BANK)) {
                    methods.add(new TWComboItem(TradeMeta.TRANSFER_BANK, Language.getString("BANK_TRANSFER")));
                } else if (type.equals("" + TradeMeta.TRANSFER_CHEQUE)) {
                    methods.add(new TWComboItem(TradeMeta.TRANSFER_CHEQUE, Language.getString("CHEQUE")));
                } else if (type.equals("" + TradeMeta.TRANSFER_CASH)) {
                    methods.add(new TWComboItem(TradeMeta.TRANSFER_CASH, Language.getString("CASH")));
                }

            }
        }
    }

    public static String getFundTransferReason(int type) {
        switch (type) {
            case TradingConstants.TRANSFER_REJECT_REASON_NONE:
                return "";
            case TradingConstants.TRANSFER_REJECT_REASON_NO_FUND:
                return Language.getString("INSUFFICIENT_FUND");
            default:
                return "";
        }
    }

    public static String getTrsID() {
        return trsID;
    }

    public static void setTrsID(String trsID) {
        TradingShared.trsID = trsID;
    }

    public static boolean isExchangeAllowedToTrading(String exchange, String portfolio) {
        ArrayList<String> exchanges = TradingShared.getTrader().getPortfolio(portfolio).getExchangeList();
        if (exchanges.contains(exchange)) {
            return true;
        } else {
            return false;
        }
    }

    public static void setBrokerForSingleSignOn() {
        try {
            Broker broker = Brokers.getSharedInstance().getFirstBroker();
            Object[] ips = broker.getIPs();
            com.isi.csvr.trading.IPSettings.getSharedInstance().removeIPS();
            for (int i = 0; i < ips.length; i++) {
                com.isi.csvr.trading.IPSettings.getSharedInstance().addIP((String) ips[i]);
            }
            Brokers.getSharedInstance().setSelectedBroker(broker.getId());
            ips = null;
            broker = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setBrokerForSSO() {
        try {
            Broker broker = Brokers.getSharedInstance().getFirstBroker();
            Object[] ips = broker.getIPs();
            com.isi.csvr.trading.IPSettings.getSharedInstance().removeIPS();
            for (int i = 0; i < ips.length; i++) {
                com.isi.csvr.trading.IPSettings.getSharedInstance().addIP((String) ips[i]);
            }
            Brokers.getSharedInstance().setSelectedBroker(broker.getId());
            ips = null;
            broker = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getMaxCondionCount() {
        return maxCondionCount;
    }

    public static void setMaxCondionCount(int maxCondionCount) {
        TradingShared.maxCondionCount = maxCondionCount;
    }

    public static boolean isAmendModeFull() {
        return amendMode == AMEND_MODE_FULL;
    }

    public static boolean isAmendModeDelta() {
        return amendMode == AMEND_MODE_DELTA;
    }

    public static void setAmendMode(int mode) {
        amendMode = mode;
    }

    public static String getSliceOrderType(int type) {
        switch (type) {
            case TradeMeta.SLICE_ORD_TYPE_ALL:
                return Language.getString("SLICE_TYPE_ALL");
            case TradeMeta.SLICE_ORD_TYPE_ICEBURG:
                return Language.getString("SLICE_TYPE_ICEBURG");
            case TradeMeta.SLICE_ORD_TYPE_TIME_INTERVAL:
                return Language.getString("SLICE_TYPE_TIME_INTERVAL");
            default:
                return Language.getString("SLICE_TYPE_NONE");
        }
    }

    public static String getBrokerID() {
        return brokerID;
    }

    public static void setBrokerID(String brokerID) {
        if (brokerID != null) {
            TradingShared.brokerID = brokerID;
        }
    }

    public static boolean isSecondaryLoginEnabled() {
        return IS_SECONDARY_LOGIN_ENABLED;
    }

    public static void setSecondaryLoginEnabled(boolean SecondaryLoginEnabled) {
        TradingShared.IS_SECONDARY_LOGIN_ENABLED = SecondaryLoginEnabled;
    }

    public static boolean isBookKeepersAvailable() {
        return isBookKeepersAvailable;
    }

    public static void setBookKeepersAvailable(boolean bookKeepersAvailable) {
        isBookKeepersAvailable = bookKeepersAvailable;
    }

    public static String getTradingSymbol(String key, String portfolio) {
        if (SharedMethods.isFullKey(key)) {
            String symbol = SharedMethods.getSymbolFromKey(key);
            String exchange = SharedMethods.getExchangeFromKey(key);
            int instrument = SharedMethods.getInstrumentTypeFromKey(key);
            if (BrokerConfig.getSharedInstance().isRemoveMarketCodeFromSymbol(TradingShared.getTrader().getPath(portfolio))) {
                String[] data = symbol.split(Constants.MARKET_SEPERATOR_CHARACTER);
                return SharedMethods.getKey(exchange, data[0], instrument);
            } else {
                return key;
            }
        } else {
            if (BrokerConfig.getSharedInstance().isRemoveMarketCodeFromSymbol(TradingShared.getTrader().getPath(portfolio))) {
                String[] data = key.split(Constants.MARKET_SEPERATOR_CHARACTER);
                return data[0];
            } else {
                return key;
            }
        }
    }

    public static String getTradingSymbolMarketCode(String key) {
        try {
            if (SharedMethods.isFullKey(key)) {
                String symbol = SharedMethods.getSymbolFromKey(key);
                String[] data = symbol.split(Constants.MARKET_SEPERATOR_CHARACTER);
                return data[1];
            } else {
                String[] data = key.split(Constants.MARKET_SEPERATOR_CHARACTER);
                return data[1];
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }

    public static String getDisplaySmbol(String key, String portfolio) {
        if (SharedMethods.isFullKey(key)) {
            String symbol = SharedMethods.getSymbolFromKey(key);
            String exchange = SharedMethods.getExchangeFromKey(key);
            int instrument = SharedMethods.getInstrumentTypeFromKey(key);
            if (BrokerConfig.getSharedInstance().isRemoveMarketCodeFromSymbol(TradingShared.getTrader().getPath(portfolio))) {
                if (symbol.indexOf(Constants.MARKET_SEPERATOR_CHARACTER) >= 0) {
                    return SharedMethods.getKey(exchange, symbol.split(Constants.MARKET_SEPERATOR_CHARACTER)[0], instrument);
                } else {
                    return key;
                }
            } else {
                return key;
            }
        } else {
            if (BrokerConfig.getSharedInstance().isRemoveMarketCodeFromSymbol(TradingShared.getTrader().getPath(portfolio))) {
                String[] data = key.split(Constants.MARKET_SEPERATOR_CHARACTER);
                return data[0];
            } else {
                return key;
            }
        }
    }

    public static String getPriceSymbol(String key, String portfolio) {
        if (SharedMethods.isFullKey(key)) {
            String symbol = SharedMethods.getSymbolFromKey(key);
            String exchange = SharedMethods.getExchangeFromKey(key);
            int instrument = SharedMethods.getInstrumentTypeFromKey(key);
            if (BrokerConfig.getSharedInstance().isRemoveMarketCodeFromSymbol(TradingShared.getTrader().getPath(portfolio))) {
                if (symbol.indexOf(Constants.MARKET_SEPERATOR_CHARACTER) >= 0) {
                    return key;
                } else {
                    Exchange excg = ExchangeStore.getSharedInstance().getExchange(exchange);
                    if (excg.hasSubMarkets()) {
                        symbol = symbol + Constants.MARKET_SEPERATOR_CHARACTER + excg.getDefaultMarket().getMarketID();
                        return SharedMethods.getKey(exchange, symbol, instrument);
                    } else {
                        return key;
                    }
                }
            } else {
                return key;
            }
        } else {
            return key;
        }
    }

    public static String getPriceSymbol(String exchange, String symbol, int instrument, String portfolio) {
        if (BrokerConfig.getSharedInstance().isRemoveMarketCodeFromSymbol(TradingShared.getTrader().getPath(portfolio))) {
            if (symbol.indexOf(Constants.MARKET_SEPERATOR_CHARACTER) >= 0) {
                return SharedMethods.getKey(exchange, symbol, instrument);
            } else {
                Exchange excg = ExchangeStore.getSharedInstance().getExchange(exchange);
                if (excg.hasSubMarkets() && excg.isUserSubMarketBreakdown()) {
                    symbol = symbol + Constants.MARKET_SEPERATOR_CHARACTER + excg.getDefaultMarket().getMarketID();
                    return SharedMethods.getKey(exchange, symbol, instrument);
                } else {
                    return SharedMethods.getKey(exchange, symbol, instrument);
                }
            }
        } else {
            return SharedMethods.getKey(exchange, symbol, instrument);
        }
    }

    public static String getDuInvestURL() {
        return duInvestURL;
    }

    public static void setDuInvestURL(String duInvestURL) {
        TradingShared.duInvestURL = duInvestURL;
    }

    public static boolean isKeepOrderWindowOpen() {
        return keepOrderWindowOpen;
    }

    public static void setKeepOrderWindowOpen(boolean keepOrderWindowOpen) {
        TradingShared.keepOrderWindowOpen = keepOrderWindowOpen;
    }

    public static boolean isShowConfigPopup() {
        return showConfigPopup;
    }

    public static void setShowConfigPopup(boolean showConfigPopup) {
        TradingShared.showConfigPopup = showConfigPopup;
    }

    public static boolean isMandatoryPWDChangeSuccess() {
        return isMandatoryPWDChangeSuccess;
    }

    public static void setMandatoryPWDChangeSuccess(boolean mandatoryPWDChangeSuccess) {
        isMandatoryPWDChangeSuccess = mandatoryPWDChangeSuccess;
    }

    public static String getUUID() {
        return uuid;
    }

    public static void setUUID(String id) {
        uuid = id;
    }
}
