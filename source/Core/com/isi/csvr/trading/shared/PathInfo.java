package com.isi.csvr.trading.shared;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Aug 23, 2007
 * Time: 1:50:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class PathInfo {

    private String userID;
    private String traderName;
    private byte path;
    private String mubasherID;
    private String sessionId;
    private int level2AuthType;
    private String currentDay;
    private String brokerID;
    //    private String sysID;
    private String reportURL;
    private ArrayList<String[]> banks;
    private int reportDepth = 6;

    public PathInfo(String userID, String mubasherNo, byte path) {
        this.userID = userID;
        this.mubasherID = mubasherNo;
        this.path = path;
    }

    public PathInfo(String userID) {
        this.userID = userID;
    }

    public ArrayList<String[]> getBanks() {
        return banks;
    }

    public void setBanks(ArrayList<String[]> banks) {
        this.banks = banks;
    }

    public String getReportURL() {
        return reportURL;
    }

    public void setReportURL(String reportURL) {
        this.reportURL = reportURL;
    }

    public String getUserID() {
        return userID;
    }

    public String getTraderName() {
        return traderName;
    }

    public void setTraderName(String traderName) {
        this.traderName = traderName;
    }

    public byte getPath() {
        return path;
    }

    public void setPath(byte path) {
        this.path = path;
    }

    public String getMubasherID() {
        return mubasherID;
    }

    public void setMubasherID(String mubasherID) {
        this.mubasherID = mubasherID;
    }

    public int getReportDepth() {
        return reportDepth;
    }

    public void setReportDepth(int reportDepth) {
        this.reportDepth = reportDepth;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getLevel2AuthType() {
        return level2AuthType;
    }

    public void setLevel2AuthType(int level2AuthType) {
        this.level2AuthType = level2AuthType;
    }

    public String getCurrentDay() {
        return currentDay;
    }

    public void setCurrentDay(String currentDay) {
        this.currentDay = currentDay;
    }

    public String getBrokerID() {
        return brokerID;
    }

    public void setBrokerID(String brokerID) {
        this.brokerID = brokerID;
    }

//    public String getSysID() {
//        return sysID;
//    }
//
//    public void setSysID(String sysID) {
//        this.sysID = sysID;
//    }
}
