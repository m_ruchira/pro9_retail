package com.isi.csvr.trading.shared;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: May 6, 2010
 * Time: 9:35:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class TradeKey {
    String exchange;
    String symbol;
    int instrumentType;
    String bookKeeper;

    public TradeKey(String exchange, String symbol, int instrumentType, String bookKeeper) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.instrumentType = instrumentType;
        this.bookKeeper = bookKeeper;
    }

    public TradeKey(String exchange, String symbol, int instrumentType) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.instrumentType = instrumentType;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getBookKeeper() {
        return bookKeeper;
    }

    public void setBookKeeper(String bookKeeper) {
        this.bookKeeper = bookKeeper;
    }

    public String getTradeKey() {
        if ((bookKeeper == null) || bookKeeper.equals("")) {
            return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol;
        } else {
            return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol + Constants.KEY_SEPERATOR_CHARACTER + bookKeeper;
        }
    }

    public boolean equals(String exchange, String symbol) {
        if ((this.exchange.equals(exchange)) && (this.symbol.equals(symbol))) {
            return true;
        } else {
            return false;
        }

    }

    public boolean equals(String key) {
        if ((this.exchange.equals(SharedMethods.getExchangeFromKey(key))) && (this.symbol.equals(SharedMethods.getSymbolFromKey(key)))) {
            return true;
        } else {
            return false;
        }

    }

    public boolean equals(TradeKey key) {
        if ((this.exchange.equals(key.getExchange()) && (this.symbol.equals(key.getSymbol())))) {
            return true;
        } else {
            return false;
        }

    }

}
