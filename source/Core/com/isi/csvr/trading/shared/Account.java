package com.isi.csvr.trading.shared;

import com.dfn.mtr.mix.beans.CashAccount;
import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 11, 2004
 * Time: 3:27:16 PM
 */
public class Account {
    private String accountNumber;
    private CashAccount cashAccount;
    private byte path;


    public Account(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setCashAccount(CashAccount cashAccount) {
        this.cashAccount = cashAccount;
    }

    public double getUnrializedSales() {
        return cashAccount.getUnrealizedSales();
    }

    public void setUnrializedSales(double unrealizedSales) {
        cashAccount.setUnrealizedSales(unrealizedSales);
    }

    public byte getPath() {
        return path;
    }

    public void setPath(byte path) {
        this.path = path;
    }

    public long getDate() {
        return Long.parseLong(cashAccount.getLastAccountUpdatedTime());
    }

    public void setDate(long date) {
        cashAccount.setLastAccountUpdatedTime(date + "");
    }

    public String getAccountID() {
        return accountNumber;
    }

    public double getBlockedAmount() {
        return cashAccount.getBlockedAmount();
    }

    public void setBlockedAmount(double blockedAmount) {
        cashAccount.setBlockedAmount(blockedAmount);
    }

    public double getCashAvailableForWithdrawal() {
        return cashAccount.getCashForWithdrawal();
    }

    public void setCashAvailableForWithdrawal(double availableCash) {
        cashAccount.setCashForWithdrawal(availableCash);
    }

    public double getBuyingPower() {
        return cashAccount.getBuyingPower();
    }

    public void setBuyingPower(double buyingPower) {
        cashAccount.setBuyingPower(buyingPower);
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getType() {
        return cashAccount.getAccountType();
    }

    public void setType(int type) {
        cashAccount.setAccountType(type);
    }

    public double getBalance() {
        return cashAccount.getBalance();
    }

    public void setBalance(double balance) {
        cashAccount.setBalance(balance);
    }

    public double getODLimit() {
        return cashAccount.getODLimit();
    }

    public void setODLimit(double ODLimit) {
        cashAccount.setODLimit(ODLimit);
    }

    public double getMarginLimit() {
        return cashAccount.getMarginLimit();
    }

    public void setMarginLimit(double marginLimit) {
        cashAccount.setMarginLimit(marginLimit);
    }

    public String getCurrency() {
        return cashAccount.getCurrency();
    }

    public void setCurrency(String currency) {
        cashAccount.setCurrency(currency);
    }

    public double getDayMarginLimit() {
        return cashAccount.getDayCashMargin();
    }

    public void setDayMarginLimit(double dayMarginLimit) {
        cashAccount.setDayCashMargin(dayMarginLimit);
    }

    public void clear() {
        cashAccount = new CashAccount();
    }

    public String toString() {
        return "Account -> Buying Power " + getBuyingPower() + " Balance " + getBalance();
    }

//    private boolean isUpdated() {
//        synchronized (this) {
//            return updated;
//        }
//    }
//
//    private void setUpdated(boolean updated) {
//        synchronized (this) {
//            this.updated = updated;
//        }
//    }

    public double getPendingDeposites() {
        return cashAccount.getPendingDeposits();
    }

    public void setPendingDeposites(double pendingDeposites) {
        cashAccount.setPendingDeposits(pendingDeposites);
    }

    public double getPendingTransfers() {
        return cashAccount.getPendingTrasfers();
    }

    public void setPendingTransfers(double pendingTransfers) {
        this.cashAccount.setPendingTrasfers(pendingTransfers);
    }


    public String getXML() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<Account>");
        buffer.append("<ID value=\"").append(getAccountID()).append("\"/>");
        buffer.append("<Currency value=\"").append(getCurrency()).append("\"/>");
        //buffer.append("<Balance value=").append(SharedMethods.formatToCurrencyFormatNoComma(getCurrency(), getBalance())).append("/>");
        buffer.append("<Blocked value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getBlockedAmount())).append("\"/>");
        buffer.append("<ODLimit value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getODLimit())).append("\"/>");
        buffer.append("<BuyingPower value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getBuyingPower())).append("\"/>");
        buffer.append("<Withdrawable value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getCashAvailableForWithdrawal())).append("\"/>");
        buffer.append("</Account>\n");

        return buffer.toString();

    }

    public double getTotMarginBlock() {
        return cashAccount.getMarginBlock();
    }

    public double getTotMarginDue() {
        return cashAccount.getMarginDue();
    }

    public double getNonMarginableCashBlock() {
        return cashAccount.getNonMarginableCashBlock();
    }

    public double getNetSecurityValue() {
        return cashAccount.getNetSecurityValue();
    }

    public double getNetPosition() {
        return cashAccount.getNetPosition();
    }

}
