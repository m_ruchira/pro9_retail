package com.isi.csvr.trading.positionMoniter;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: udayaa
 * Date: Jul 9, 2010
 * Time: 12:14:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class PositionModel extends CommonTable
        implements TableModel, CommonTableInterface {
    ArrayList<IntraDayRecord> dataStore;
    String currency;
    byte currencyDecimals;
    private double totCost;
    private double totPositionCost;
    private double totCurrentValue;
    private double totunrealizedgain;
    private double totuSoleAmt;
    private double totrealizedGain;

    public PositionModel() {
        dataStore = new ArrayList<IntraDayRecord>();
    }

    public ArrayList<IntraDayRecord> getDataStore() {
        return dataStore;
    }

    public void setDataStore(ArrayList<IntraDayRecord> dataStore) {
        this.dataStore = dataStore;
    }

    public int getRowCount() {
        return dataStore.size() + 1;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
        currencyDecimals = (byte) com.isi.csvr.datastore.CurrencyStore.getSharedInstance().getDecimalPlaces(currency);
    }

    public int getColumnCount() {
        return getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        if (rowIndex == dataStore.size()) {
            switch (columnIndex) {
                case -4:
                    return currencyDecimals;
                case -1:
                    return null;
                case 3:
                    return getTotalCost();
                case 6:
                    return getTotPositionCost();
                case 10:
                    return getTotCurrentValue();
                case 11:
                    return getTotUnrealizedgain();
                case 14:
                    return getTotSoldAmt();
                case 15:
                    return getTotRealizesGain();
                default:
                    return "";

            }
        }
        IntraDayRecord record = dataStore.get(rowIndex);
        Stock stk = DataStore.getSharedInstance().getStockObject(record.getSKey());
        try {
            switch (columnIndex) {
                case -4:
                    if (stk != null) {

                        return stk.getDecimalCount();
                    } else {
                        return 2;
                    }
                case -1:
                    return record;
                case 0:
                    return record.getSymbol();
                case 1:
                    return record.getCurrency();
                case 2:
                    return record.getPurchasedQty();
                case 3:
                    return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getTotCost(), record.getPfId());
                case 4:
                    return record.getHoldings();
                case 5:
                    return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getAvgCost(), record.getPfId());
                case 6:
                    return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getPositionCost(), record.getPfId());

                case 7:
                    if (stk != null) {
                        return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, stk.getBestBidPrice(), record.getPfId());
                    } else {
                        return 0;
                    }
                case 8:
                    if (stk != null) {
                        return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, stk.getBestAskPrice(), record.getPfId());
                    } else {
                        return 0;
                    }
                case 9:
                    if (stk != null) {
                        return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, stk.getLastTradeValue(), record.getPfId());
                    } else {
                        return 0;
                    }
                case 10:
                    return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getCurrentValue(), record.getPfId());
                case 11:
                    return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getUnrealizedGain(), record.getPfId());
                case 12:
                    return record.getSoldQty();
                case 13:
                    return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getAvgSellPrice(), record.getPfId());
                case 14:
                    return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getSoldAmount(), record.getPfId());
                case 15:
                    return convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getRealizedGain(), record.getPfId());
                default:
                    return "";

            }
        } catch (Exception e) {
            return "";
        }
    }

    private double getTotalCost() {
        double totcost = 0;
        for (IntraDayRecord record : dataStore) {
            totcost += convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getTotCost(), record.getPfId());
        }
        return totcost;
    }

    private double getTotPositionCost() {
        double totcost = 0;
        for (IntraDayRecord record : dataStore) {
            totcost += convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getPositionCost(), record.getPfId());
        }
        return totcost;
    }

    private double getTotCurrentValue() {
        double currnetval = 0;
        for (IntraDayRecord record : dataStore) {
            currnetval += convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getCurrentValue(), record.getPfId());
        }
        return currnetval;
    }

    private double getTotUnrealizedgain() {
        double currnetval = 0;
        for (IntraDayRecord record : dataStore) {
            currnetval += convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getUnrealizedGain(), record.getPfId());
        }
        return currnetval;
    }

    private double getTotSoldAmt() {
        double currnetval = 0;
        for (IntraDayRecord record : dataStore) {
            currnetval += convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getSoldAmount(), record.getPfId());
        }
        return currnetval;
    }

    private double getTotRealizesGain() {
        double currnetval = 0;
        for (IntraDayRecord record : dataStore) {
            currnetval += convertToSelectedCurrency(record.getExchange(), record.getCurrency(), false, record.getRealizedGain(), record.getPfId());
        }
        return currnetval;
    }


    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
            case 1:
                return String.class;
            case 2:
            case 4:
            case 12:
                return Long.class;
            case 3:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 13:
            case 14:
            case 15:
                return Double.class;


            default:
                return Object.class;
        }
    }

    private double convertToSelectedCurrency(String exchangeCode, String sCurrency, boolean applyPriceModificationFactor, double value, String portfolio) {
        if (applyPriceModificationFactor) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            if (exchange != null)
//                return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
                return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
            else
//                return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
                return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
        } else {
//            return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
            return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
        }
    }
}
