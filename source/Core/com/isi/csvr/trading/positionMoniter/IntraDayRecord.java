package com.isi.csvr.trading.positionMoniter;

import com.dfn.mtr.mix.beans.Order;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.trading.shared.TradeMeta;

/**
 * Created by IntelliJ IDEA.
 * User: udayaa
 * Date: Jul 12, 2010
 * Time: 12:10:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class IntraDayRecord implements Comparable<String> {
    public static int SINGLE_PF = 0;
    public static int MULTIPLE_PF = 1;

    String sKey;
    String symbol;
    String exchange;
    String currency;
    double totCost;
    long holdings;
    long purchasedQty;
    double avgCost;
    long soldQty;
    double avgSellPrice;
    Stock stock;

    String PfId;
    int type;

    public IntraDayRecord(int type) {
        this.type = type;
    }

    public String getSKey() {
        return sKey;
    }

    public void setSKey(String sKey) {
        this.sKey = sKey;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCurrency() {
        if (currency == null || currency.isEmpty()) {
            if (DataStore.getSharedInstance().getStockObject(sKey) != null) {
                currency = DataStore.getSharedInstance().getStockObject(sKey).getCurrencyCode();
            }
        }
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getTotCost() {
        return purchasedQty * avgCost;
//        return totCost;
    }

    public void setTotCost(double totCost) {
        this.totCost = totCost;
    }

    public long getHoldings() {
//        return holdings;
        if (purchasedQty - soldQty > 0) {
            return purchasedQty - soldQty;
        } else {
            return 0;
        }

    }

    public void setHoldings(long holdings) {
        this.holdings = holdings;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public long getSoldQty() {
        return soldQty;
    }

    public void setSoldQty(long soldQty) {
        this.soldQty = soldQty;

    }

    public double getAvgSellPrice() {
        return avgSellPrice;
    }

    public void setAvgSellPrice(double avgSellPrice) {
        this.avgSellPrice = avgSellPrice;
    }

    public String getPfId() {
        return PfId;
    }

    public void setPfId(String pfId) {
        PfId = pfId;
    }

    public long getPurchasedQty() {
        return purchasedQty;
    }

    public void setPurchasedQty(long purchasedQty) {
        this.purchasedQty = purchasedQty;
    }


    public double getPositionCost() {
        return getHoldings() * avgCost;
    }

    public double getCurrentValue() {
        try {
            return getHoldings() * DataStore.getSharedInstance().getStockObject(sKey).getBestBidPrice();
        } catch (Exception e) {
            return 0;
        }
    }

    public double getUnrealizedGain() {
        return getCurrentValue() - getPositionCost();
    }

    public double getRealizedGain() {
        return getSoldAmount() - (getSoldQty() * getAvgCost());
    }

    public double getSoldAmount() {
        return getSoldQty() * getAvgSellPrice();
    }

    public int compareTo(String o) {
        if (o.equals(this.getSKey())) {
            return 1;
        } else {
            return 0;  //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void setData(Order record, String key) {
        setSKey(key);
        Stock stk = DataStore.getSharedInstance().getStockObject(key);
        setSymbol(record.getSymbol());
//        setSymbol(stk.getSymbol());
//        setCurrency(stk.getCurrencyCode());
        setCurrency(record.getCurrency());
        setExchange(record.getSecurityExchange());
        if (type == 0) {
            setPfId(record.getPortfolioNo());
        }
        //   this.stock = stk;
        if (record.getSide() == TradeMeta.BUY) {
            setAvgCost(record.getLastPrice());
            setPurchasedQty(record.getLastShares());
        } else {
            setAvgSellPrice(record.getLastPrice());
            setSoldQty(record.getLastShares());
        }

    }

    public void updateData(Order record) {
        if (record.getSide() == TradeMeta.BUY) {
            updateAvgCost(record);
            updatePurchasedQuantity(record);
        } else {
            if (getHoldings() > 0) {
                updateAvgSellingPrice(record);
                updateSoldQuantity(record);
            }
        }

    }

    private void updateAvgCost(Order record) {
        long currentPurchased = getPurchasedQty();
        double currentAvg = getAvgCost();
        long newPurchased = record.getLastShares();
        double newAvg = record.getLastPrice();
        if (Double.isNaN(newAvg) || Double.isNaN(newPurchased)) {
            return;
        }

        if (currentPurchased + newPurchased > 0) {
            double avgCost = ((currentPurchased * currentAvg) + (newPurchased * newAvg)) / (currentPurchased + newPurchased);
            if (!Double.isInfinite(avgCost)) {

                setAvgCost(avgCost);
            }
        }
    }

    private void updatePurchasedQuantity(Order record) {
        setPurchasedQty(getPurchasedQty() + record.getLastShares());
    }

    private void updateAvgSellingPrice(Order record) {
        long currentSold = getSoldQty();
        double currentAvg = getAvgSellPrice();
        long newSold = record.getLastShares();
        double newAvg = record.getLastPrice();
        if (Double.isNaN(newAvg) || Double.isNaN(newSold)) {
            return;
        }

        if (currentSold + newSold > getPurchasedQty()) {
            newSold = getPurchasedQty() - currentSold;
        }

        if (currentSold + newSold > 0) {
            double avgSold = ((currentSold * currentAvg) + (newSold * newAvg)) / (currentSold + newSold);
            if (!Double.isInfinite(avgSold)) {
                setAvgSellPrice(avgSold);
            }
        }
    }

    private void updateSoldQuantity(Order record) {
        if (getHoldings() - record.getLastShares() > 0) {
            setSoldQty(getSoldQty() + record.getLastShares());
        } else {
            setSoldQty(getPurchasedQty());
        }
    }

}
