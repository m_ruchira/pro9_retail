package com.isi.csvr.trading.positionMoniter;

import com.dfn.mtr.mix.beans.Order;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.shared.TradeMeta;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: udayaa
 * Date: Jul 9, 2010
 * Time: 12:14:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class PositionDataStore implements TradingConnectionListener {

    static PositionDataStore self;
    private ArrayList<Order> positionStore;
    private Hashtable<String, ArrayList<IntraDayRecord>> filteredPositionStore;
    private ArrayList<IntraDayRecord> allPositionStore;

    private PositionDataStore() {
        positionStore = new ArrayList<Order>();
        filteredPositionStore = new Hashtable<String, ArrayList<IntraDayRecord>>();
        allPositionStore = new ArrayList<IntraDayRecord>();
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static PositionDataStore getSharedInstance() {
        if (self == null) {
            self = new PositionDataStore();
        }
        return self;
    }

    public void addPositionData(ArrayList<Order> data) {
        // for(int i=data.size()-1; i>=0; i--){
        for (int i = 0; i < data.size(); i++) {
            positionStore.add(data.get(i));

            updateAllPositionStore(data.get(i));
            updateFilteredStore(data.get(i));
            Order order = data.get(i);


        }

    }

    public void addPositionData(Order data) {

        positionStore.add(data);

        updateAllPositionStore(data);
        updateFilteredStore(data);
    }

    private void updateAllPositionStore(Order record) {
        int instrument = 0;
        try {
            instrument = Integer.parseInt(record.getSecurityType());         //todo instrument type change
//            instrument =(record.getMubasherSecurityType());
        } catch (NumberFormatException e) {
            instrument = 0;
        }
        String key = SharedMethods.getKey(record.getSecurityExchange(), record.getSymbol(), instrument);
        IntraDayRecord intraDayRecord = null;
        for (IntraDayRecord rec : allPositionStore) {
            if (rec.getSKey().equals(key)) {
                intraDayRecord = rec;
                break;
            }
        }
        if (intraDayRecord == null && (record.getSide() == TradeMeta.SELL)) {
            return;
        }

        if (intraDayRecord == null) {
            intraDayRecord = new IntraDayRecord(IntraDayRecord.MULTIPLE_PF);
            intraDayRecord.setData(record, key);
            allPositionStore.add(intraDayRecord);
        } else {
            intraDayRecord.updateData(record);
        }


    }

    private void updateFilteredStore(Order record) {
        String pfID = record.getPortfolioNo();
        ArrayList<IntraDayRecord> pfWiselist = filteredPositionStore.get(pfID);
        if (pfWiselist == null) {
            pfWiselist = new ArrayList<IntraDayRecord>();
            filteredPositionStore.put(pfID, pfWiselist);
        }

        int instrument = 0;
        try {
            instrument = Integer.parseInt(record.getSecurityType());      //todo instrument type change
//            instrument = (record.getMubasherSecurityType());      //todo instrument type change
        } catch (NumberFormatException e) {
            instrument = 0;
        }
        String key = SharedMethods.getKey(record.getSecurityExchange(), record.getSymbol(), instrument);
        IntraDayRecord intraDayRecord = null;
        for (IntraDayRecord rec : pfWiselist) {
            if (rec.getSKey().equals(key)) {
                intraDayRecord = rec;
                break;
            }
        }
        if (intraDayRecord == null && (record.getSide() == TradeMeta.SELL)) {
            return;
        }
        if (intraDayRecord == null) {
            intraDayRecord = new IntraDayRecord(IntraDayRecord.SINGLE_PF);
            intraDayRecord.setData(record, key);
            pfWiselist.add(intraDayRecord);
        } else {
            intraDayRecord.updateData(record);
        }
    }

    public ArrayList<IntraDayRecord> getAllPositionStore() {
        return allPositionStore;
    }

    public ArrayList<IntraDayRecord> getFilteredStore(String pfId) {
        ArrayList<IntraDayRecord> rec = filteredPositionStore.get(pfId);
        if (rec == null) {
            rec = new ArrayList<IntraDayRecord>();
            filteredPositionStore.put(pfId, rec);
        }

        return rec;
    }

    public void tradeServerConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        filteredPositionStore.clear();
        positionStore.clear();
        allPositionStore.clear();
    }

    public void clear() {
        filteredPositionStore.clear();
        positionStore.clear();
        allPositionStore.clear();
    }

    public ArrayList<IntraDayRecord> getRecordsForSelecteSymbol(String sKey) {
        ArrayList<IntraDayRecord> records = new ArrayList<IntraDayRecord>();
        Enumeration<String> enu = filteredPositionStore.keys();
        while (enu.hasMoreElements()) {
            ArrayList<IntraDayRecord> temp = filteredPositionStore.get(enu.nextElement());
            for (int i = 0; i < temp.size(); i++) {
                if (temp.get(i).getSKey().equals(sKey)) {
                    records.add(temp.get(i));
                }
            }
        }
        return records;
    }

    public IntraDayRecord getRecordByPfId(String pfID, String key) {
        ArrayList<IntraDayRecord> records = filteredPositionStore.get(pfID);
        if (records != null) {
            for (IntraDayRecord rec : records) {
                if (rec.getSKey().equals(key)) {
                    return rec;
                }
            }
            return null;
        } else {
            return null;
        }

    }
}
