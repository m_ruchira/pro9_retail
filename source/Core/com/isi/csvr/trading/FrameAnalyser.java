package com.isi.csvr.trading;

import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.beans.TransferObject;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.api.TradeServer;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.TransactRecord;
import com.isi.csvr.trading.portfolio.ValuationRecord;
import com.isi.csvr.trading.positionMoniter.PositionDataStore;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.trading.ui.CashTransactionUI;
import com.isi.csvr.trading.ui.Level2AuthWait;
import com.isi.csvr.trading.ui.OrderSearchWindow;
import com.isi.csvr.trading.ui.SetUserDetails;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 18, 2004
 * Time: 3:05:45 PM
 */
public class
        FrameAnalyser extends Thread implements TradeMeta {
    private List<MixObjectWrapper> list;
    private SimpleDateFormat unformatted = new SimpleDateFormat("yyyyMMdd");

    public FrameAnalyser() {
        super("TradingFrameAnalyser");
        list = Collections.synchronizedList(new LinkedList());
        start();
    }

    public List getList() {
        return list;
    }

    public void setQueue(List list) {
        this.list = list;
    }

    public void run() {
        while (TradingShared.isConnected()) {
            try {
                analyse();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void analyse() {
        String record = "";
        String[] fields = null;
        int messageType = 0;
        MIXObject response;
        MixObjectWrapper wrapper;
        int group;
        int reqType;
        Object object;


        while (TradingShared.isConnected() && (list.size() > 0)) {
            // wrapper = (list.remove(0));
            //  fields = record.split(TradeMeta.FS);
            //  messageType = Byte.parseByte(fields[0]);
//            byte path = wrapper.getPath();
//            response = (MIXObject) wrapper;
            object = (list.remove(0));
            byte path = 0;
            try {
                wrapper = (MixObjectWrapper) object;
                path = wrapper.getPath();
                response = wrapper;
            } catch (Exception e) {
                e.printStackTrace();
                path = Constants.PATH_PRIMARY;
                response = (MIXObject) object;
            }
            group = response.getMIXHeader().getGroup();
            reqType = response.getMIXHeader().getRequestType();
            switch (group) {
                case MIXConstants.GROUP_AUTHENTICATION:
//                    ResponseFileWriter.getSharedInstance().writeToFile(response);
                    switch (reqType) {
                        case MIXConstants.RESPONSE_TYPE_AUTH_LEVEL2:
                            processTradingAuthentication(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_CHANG_PWD:
                            changeTradingPassword(response);
                            break;
                        default:
                            break;
                    }
                    break;
                case MIXConstants.GROUP_SYSTEM:
                    switch (reqType) {
                        case MIXConstants.RESPONSE_TYPE_PULSE:
                            break;
                        case MIXConstants.RESPONSE_TYPE_EDIT_PORTFOLIO_ALIAS:
                            setEdittedPortfolioName(response);
                            break;
                        default:
                            break;
                    }
                    break;
                case MIXConstants.GROUP_INQUIRY:
                    switch (reqType) {
                        case MIXConstants.RESPONSE_TYPE_PORTFOLIO:
                            updatePortfolio(response);
//                            ResponseFileWriter.getSharedInstance().writeToFile(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_BUYING_POWER:
                            updateAccount(response, path);
//                            ResponseFileWriter.getSharedInstance().writeToFile(response,path);
                            break;
                        case MIXConstants.RESPONSE_TYPE_BANK_ACC_DETAILS:
                            processBankAccountDetails(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_CASH_LOG_SEARCH:
                            CashLogSearchStore.getSharedInstance().setData(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_MARGINABLE_SYMBOL:
                            MarginSymbolStore.getSharedInstance().setData(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_MARGIN_CONFIG_PARAM:
                            TradingShared.getTrader().updatePortfolios(response);
                            break;
                        default:
                            break;
                    }
                    break;
                case MIXConstants.GROUP_TRADING_INQUIRY:
                    switch (reqType) {
                        case MIXConstants.RESPONSE_TYPE_ORDER_LIST:
                            //    System.out.println("OL RES " + response.getMIXString());
                            processOrderList(response, false);
                            break;
                        case MIXConstants.RESPONSE_TYPE_ORDER_SEARCH:
                            processSearchedOrder(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_EXECUTION_HISTORY:
                            System.out.println("OD RES " + response.getMIXString());
                            processOrderDepth(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_INTRADAY_POSITION_MONITOR:
                            processIntradayResponse(response);
                        default:
                            break;

                    }
                    break;
                case MIXConstants.GROUP_TRADING:
                    switch (reqType) {
                        case MIXConstants.RESPONSE_TYPE_NEW_ORDER:
                            processPendingOrder(response, false);
                            break;
                        case MIXConstants.RESPONSE_TYPE_AMEND_ORDER:
                            processPendingOrder(response, false);
                            break;
                        case MIXConstants.RESPONSE_TYPE_CANCEL_ORDER:
                            processPendingOrder(response, false);
                            break;
                        default:
                            break;
                    }
                    break;
                case MIXConstants.GROUP_FINANCE_AND_HOLDING:
                    switch (reqType) {
                        case MIXConstants.RESPONSE_TYPE_EXCHANGE_RATE:
                            CurrencyStore.getSharedInstance().addCurrency(response, path);
                            break;
                        case MIXConstants.RESPONSE_TYPE_WITHDRAWAL:
                            processWithdrawalResponse(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_DEPOSITE:
                            processDepositeResponse(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_DEPOSIT_WITHDRAWAL_LIST:
                            setPendingFundTransfer(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_CASH_TRANSFER:
                        case MIXConstants.RESPONSE_TYPE_THIRD_PARTY_CASH_TRANSFER:
                            processCashTransferResponce(response);
                            break;
                        case MIXConstants.RESPONSE_TYPE_DEPOSITE_WITHDRAWAL_CANCEL:
                            cancelFundTransfer(response);
                            break;
                        default:
                            break;
                    }
                    break;

            }

//            switch (messageType) {
//                case MT_ACK:         //done
//                    break;
//                case MT_SEARCH:
//                    //     processSearchedOrder(record);
//                    break;
//                case MT_ORDER:
//                case MT_PENDING:
//                    //    processPendigOrder(record, false);
//                    break;
//                case MT_CONDITIONAL_ORDER:
//                case MT_CONDITIONAL_PENDING:
//                    processPendigOrder(record, true);
//                    break;
//                case MT_SLICED_ORDER:
//                case MT_SLICED_ORDER_LIST:
            //    processPendigOrder(record, false);
//                    break;
//                case MT_OPEN_POSITIONS_LIST:
//                    processOpenOrder(record, false);
//                    break;
//                case MT_PULSE:
//                    break;
//                case MT_QUEUED:
//                    break;
////                case MT_PORTFOLIO_DETAILS:
////                    updatePrortfolio(record);
////                    break;
////                case MT_ACCOUNT:        //done
////                    updateAccount(record);
////                    break;
//                case MT_ORDER_DEPTH:
//                    //       processOrderDepth(record);
//                    break;
//                case MT_CHANGE_PW:
////                    getTradingDetails(record);
////                    changeTradingPassword(record);
//                    break;
//                case MT_AUTH_2:
//                    processTradingAuthentication(record);
//                    break;
//                case MT_CASH_SUMMARY:
//                    processCashTransactionSummary(record);
//                    break;
//                case MT_CASH_DETAIL:
//                    processCashDetails(record);
//                    break;
//                case MT_AUTH_VALIDITY:
//                    getUserValidation(record);
////                    changeTradingPass(record);
////                    changePassword(record);
//                    break;
//                case MT_WITHDRAWAL:
////                    System.out.println("withDra  = " + record);
//                    //   processWithdrawalResponse(record);
//                    break;
//                case MT_FUND_TRANSFER_PENDING:
////                    setPendingFundTransfer(record);
//                    break;
//                case MT_FUND_TRANSFER_CANCEL:
//                    //                   cancelFundTransfer(record);
////                    System.out.println("withdrawal cancelled");
//                    break;
//                case MT_DEPOSITE:
////                    System.out.println("Fund  = " + record);
//                    //    processDepositeResponse(record);
//                    break;
//                case MT_STOCK_TRANSFER:
//                    setStocktransferData(record, true);
//                    break;
//                case MT_STOCK_TRANSFER_PENDING:
//                    setStocktransferData(record, false);
//                    break;
//                case MT_STOCK_TRANSFER_CANCEL:
//                    cancelStockTransfer(record);
//                    break;
//                case MT_CURRENCY_DATA:
//                    //System.out.println("--> Currency " + record);
//                    //    CurrencyStore.getSharedInstance().addCurrency(record);
//                    break;
//                case MT_OMS_SYSTEM_MESSAGE:
//                    OMSMessageList.getSharedInstance().addData(record);
//                    break;
//                case MARK_FOR_DELIVERY:
//                    processMarkForDelivery(record);
//                    break;
//                case MT_EDIT_PORTFOLIO:
//                    setEdittedPortfolioName(record);
//                    break;
//                case MT_PORTFOLIO_MARGIN:
//                    setMarginDetails(record);
//                    break;
//                case MT_CASH_LOG_SEARCH:
////                    CashLogSearchStore.getSharedInstance().setData(record);
//                    break;
//                case MT_CASH_TRANSFER:
//                case MT_3rd_Party_CASH_TRANSFER:
//                    processCashTransferResponce(record);
//                    break;
//                case MT_FUTURE_BASE_ATTRIBUTES:
//                    processFutureBaseAttributes(record);
//                    break;
//                case MT_FOREX_QUOTE:
            //ForexBuySell.getInstance().processQuoteResponse(record);
//                    break;
//                case MT_FORX_DISCOUNTS:
//                    //ForexBuySell.getInstance().processQuoteResponse(record);
//                    ForexStore.getSharedInstance().processForexDicounts(record);
//                    break;
//                case MT_IPO_SUBSCRIPTION_RQ:
////                    System.out.println("Test IPO 1 : "+record);
//                    break;
//                case MT_IPO_AVAILABLE:
////                    System.out.println("Test IPO 2 : "+record);
//                    IPOStore.getSharedInstance().addToIPOCombo(record);
//                    break;
//                case MT_IPO_SUBSCRIBED:
////                    System.out.println("Test IPO 3 : "+record);
//                    IPOStore.getSharedInstance().addToIPOTableRecords(record);
//                    break;
//                case MT_IPO_CANCEL_RQ:
//                    IPOStore.getSharedInstance().processCancelRequests(record);
//                    break;
//                case MT_MARGIN_SYMBOLS:
//                    MarginSymbolStore.getSharedInstance().setData(record);
//                    MarginSymbolStore.getSharedInstance().getMarginStore();
//                    break;
//                case MT_TPLUS_SYMBOLS_NEW:
//                    TPlusStore.getSharedInstance().setData(record);
////                    TPlusStore.getSharedInstance().getTplusRecodrsStore();
////                    TPlusStore.getSharedInstance().getTPlusRecords();
//                    break;
//                case MT_TPLUS_COMMISIONS:
//                    TPlusStore.getSharedInstance().setTPlusCommisionData(record);
//                    break;
//                case MT_BANK_ACC_DETAILS:
//                    //                   processBankAccountDetails(record);
//                    break;
//            }
            fields = null;
            record = null;
        }
    }

    private void processBankAccountDetails(String frame) {
        try {
            TradeMessage message = new TradeMessage(frame);
            String[] marginRecords = message.getMessageData().split(TradeMeta.DS);
            TradingShared.getTrader().setBanks(marginRecords[0], marginRecords);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processBankAccountDetails(MIXObject bankReply) {
        try {
            TransferObject[] banks = bankReply.getTransferObjects();
            if (banks == null || banks.length == 0) {
                return;
            }
            BankDetailsResponse response = (BankDetailsResponse) banks[0];
            if (response.getBankRecords() != null && response.getBankRecords().size() > 0) {
                TradingShared.getTrader().setBanks(response.getUserID(), response.getBankRecords());
            }
//            TransferObject[] banklist = banks.getTransferObjects();
//            if(banklist==null || banklist.length ==0){
//                return;
//            }
//            TradeMessage message = new TradeMessage(frame);
//            String[] marginRecords = message.getMessageData().split(TradeMeta.DS);
//            TradingShared.getTrader().setBanks(marginRecords[0], marginRecords);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMarginDetails(String frame) {
        try {
            TradeMessage message = new TradeMessage(frame);
            String[] marginRecords = message.getMessageData().split(TradeMeta.DS);
            TradingPortfolioRecord portflio = null;
            /*
    [9:03:53 AM] Niroshan Serasinghe says: "A" + PortfolioAccount + TRSMeta.FD + "B" + marginEnabledStatus +
    TRSMeta.FD + "C" + dayMarginEnabledStatus + TRSMeta.FD + "D" + margin + TRSMeta.FD + "E" + margin_block +
    TRSMeta.FD + "H" + margin_due + TRSMeta.FD + "G" + day_margin_due() + TRSMeta.FD
     */

            for (String marginRecord : marginRecords) {
                try {
                    String[] marginData = marginRecord.split(TradeMeta.FD);
                    for (String marginField : marginData) {
                        try {
                            if (marginField.startsWith("A")) {
                                String portfolioID = marginField.substring(1);
                                portflio = TradePortfolios.getInstance().getPortfolio(portfolioID);
                                if (portflio == null) {
                                    break;
                                }
                            } else if ((portflio != null) && (marginField.startsWith("B"))) {
                                portflio.setMarginEnabled(marginField.substring(1).trim().equals("1"));
                            } else if ((portflio != null) && (marginField.startsWith("C"))) {
                                portflio.setDayMarginEnabled(marginField.substring(1).trim().equals("1"));
                            } else if ((portflio != null) && (marginField.startsWith("D"))) {
                                portflio.setMarginPct(SharedMethods.doubleValue(marginField.substring(1)));
                            } else if ((portflio != null) && (marginField.startsWith("E"))) {
                                //  portflio.setMarginBlocked(SharedMethods.doubleValue(marginField.substring(1)));
                            } else if ((portflio != null) && (marginField.startsWith("F"))) {
                                //   portflio.setMarginDue(SharedMethods.doubleValue(marginField.substring(1)));
                            } else if ((portflio != null) && (marginField.startsWith("G"))) {
                                //  portflio.setDayMarginDue(SharedMethods.doubleValue(marginField.substring(1)));
                            } else if ((portflio != null) && (marginField.startsWith("H"))) {
                                portflio.setDayMarginPct(SharedMethods.doubleValue(marginField.substring(1)));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    portflio = null;
                    marginData = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            marginRecords = null;
            //txn = null;
            message = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setEdittedPortfolioName(String record) {
        try {
            TradeMessage message = new TradeMessage(record);
            String messageDat = message.getMessageData();
            String[] messageData = messageDat.split(TradeMeta.DS);
//            System.out.println("data1 ="+messageData[0]);
            if (Integer.parseInt(messageData[3]) == 1) {
                String portfolio = messageData[1];
                String name = UnicodeUtils.getNativeString(messageData[2]);
                TradingShared.getTrader().getPortfolio(portfolio).setName(name);
                TradingShared.getTrader().firePortfolioDataChanged(portfolio);
                String str = Language.getString("MSG_EDIT_PORTFOLIO_NAME_SUCCESS");
                str = str.replaceAll("\\[PORTFOLIOID\\]", portfolio);
                str = str.replaceAll("\\[NAME\\]", name);
                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
                str = null;
                name = null;
                portfolio = null;
            } else {
                SharedMethods.showMessage(Language.getString("MSG_EDIT_PORTFOLIO_NAME_FAILED"), JOptionPane.ERROR_MESSAGE);
            }
            messageData = null;
            messageDat = null;
            message = null;
        } catch (Exception e) {
            SharedMethods.showMessage(Language.getString("MSG_EDIT_PORTFOLIO_NAME_FAILED"), JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void setEdittedPortfolioName(MIXObject record) {
        try {
            TransferObject[] response = record.getTransferObjects();
            if (response == null || response.length == 0) {
                SharedMethods.showMessage(Language.getString("MSG_EDIT_PORTFOLIO_NAME_FAILED"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            EditPortfolioAliasResponse editResp = (EditPortfolioAliasResponse) response[0];
            if (true) {
                String portfolio = editResp.getPortfolioNumber();
                String name = UnicodeUtils.getNativeString(editResp.getPortfolioAlias());
                TradingShared.getTrader().getPortfolio(portfolio).setName(name);
                TradingShared.getTrader().firePortfolioDataChanged(portfolio);
                String str = Language.getString("MSG_EDIT_PORTFOLIO_NAME_SUCCESS");
                str = str.replaceAll("\\[PORTFOLIOID\\]", portfolio);
                str = str.replaceAll("\\[NAME\\]", name);
                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
                str = null;
                name = null;
                portfolio = null;
            } else {
                SharedMethods.showMessage(Language.getString("MSG_EDIT_PORTFOLIO_NAME_FAILED"), JOptionPane.ERROR_MESSAGE);
            }
            // messageData = null;
            // messageDat = null;
            //  message = null;
        } catch (Exception e) {
            SharedMethods.showMessage(Language.getString("MSG_EDIT_PORTFOLIO_NAME_FAILED"), JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void processCashDetails(String record) {
        try {
            TradeMessage message = new TradeMessage(record);
            String messageDat = message.getMessageData();
            String[] messageData = messageDat.split(TradeMeta.FD);
            if (messageData.length < 3) {
                String str = Language.getString("CASH_TRANSACTION_MSG");
                CashTransactionSearchStore.getSharedInstance().setNewPage();
                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
                CashTransactionUI.getSharedInstance().enableNext(0);
            } else {
                String index;
                if ((messageData[1] != null) && (!messageData[1].equals(""))) {
                    index = messageData[1];
                    if (new Integer(messageData[1]) == 1) {
                        CashTransactionSearchStore.getSharedInstance().setCanGoNext(true);
                    } else {
                        CashTransactionSearchStore.getSharedInstance().setCanGoNext(false);
                    }
                } else {
                    index = "0";
                }
                int hasnext = new Integer(index);
                CashTransactionUI.getSharedInstance().enableNext(hasnext);

                for (int i = 2; i < messageData.length; i++) {
                    String[] messData = messageData[i].split(Meta.ID);
                    String datetime = messData[0];
                    String transtype = messData[1];
                    String refernece = messData[2];
                    String amount = messData[3];
                    String narration = "";
                    if ((messData[4] == null) || (messData[4].equals("")) || (messData[4].equals("null"))) {
                        narration = Language.getString("NA");
                    } else {
                        narration = messData[4];
                    }
                    //int type = new Integer(transtype);
                    long ref = new Long(refernece);
                    float amou = new Float(amount);
                    long date = new Long(datetime);
                    CashTransaction transaction = new CashTransaction(transtype, amou, ref, date, narration);
                    CashTransactionSearchStore.getSharedInstance().addTransaction(transaction);
                    if (i == 2) {
                        CashTransactionSearchStore.getSharedInstance().setCurrentPageTop(transaction.getReference());
                    } else if (i == messageData.length - 1) {
                        CashTransactionSearchStore.getSharedInstance().setCurrentPageBottom(transaction.getReference());
                    }

                }
            }
            /*int i =1;
            while(messageData.length>i){
                String datetime = messageData[i];
                String transtype = messageData[i+1];
                String refernece = messageData[i+2];
                String amount = messageData[i+3];
                String narration="";
                if(messageData[i+4]!=null){
                    narration = messageData[i+4];
                }
                i=i+5;
                int type = new Integer(transtype);
                int ref = new Integer(refernece);
                float amou =new Float(amount);
                long date = new Long(datetime);
                CashTransaction transaction = new CashTransaction(type,amou,ref,date,narration);
                CashTransactionSearchStore.getSharedInstance().addTransaction(transaction);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processCashTransactionSummary(String frame) {

        try {

            TradeMessage message = new TradeMessage(frame);
            if (message.getType() == TradeMeta.MT_CASH_SUMMARY) {

                String messageDat = message.getMessageData();
                String[] messageData = messageDat.split(TradeMeta.FD);
                //String
                String startdate = messageData[0];
                long sdate = new Long(startdate);
                String enddate = messageData[1];
                long edate = new Long(enddate);
                String accountno = messageData[2];
                int accno = new Integer(accountno);
                String opening = messageData[3];
                float open = new Float(opening);
                String buytotal = messageData[4];
                float buy = new Float(buytotal);
                String selltotal = messageData[5];
                float sell = new Float(selltotal);
                String deposites = messageData[6];
                float deposite = new Float(deposites);
                String withdrawals = messageData[7];
                float withdraw = new Float(withdrawals);
                String charges = messageData[8];
                float charge = new Float(charges);
                String closing = messageData[9];
                float close = new Float(closing);
                CashTransactionUI.getSharedInstance().setSummaryDetail(startdate, enddate, accno, open, buy, sell, deposite, withdraw, charge, close);
                CashTransactionUI.getSharedInstance().setVisible(true);
                //CashTransaction cashT = new CashTransaction();
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

//    private void processDepositeResponse(String frame) {
//        try {
//            TradeMessage message = new TradeMessage(frame);
//            String messageDat = message.getMessageData();
//            String[] messageData = messageDat.split(TradeMeta.FD);
//            String type = "0";
//            int source = 0;
//            int count = 0;
//            String OMSRefID = Language.getString("NA");
//            boolean isValidRecord = true;
////            String TWRefID = "";
//            FundTransferRequest fundRequest = new FundTransferRequest();
//            for (int i = 0; i < messageData.length; i++) {
//                if (messageData[i].startsWith("A")) {
//                    type = messageData[i].substring(1);
//                    fundRequest.setStatus(Integer.parseInt(messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("B")) {
//                    OMSRefID = messageData[i].substring(1);
//                    fundRequest.setReference(messageData[i].substring(1));
//                    count++;
////                } else if (messageData[i].startsWith("C")) {
////                    TWRefID = messageData[i].substring(1);
//                } else if (messageData[i].startsWith("D")) {
//                    source = Integer.parseInt(messageData[i].substring(1));
//                    fundRequest.setMethod(Integer.parseInt(messageData[i].substring(1)));
//                    count++;
//                } else if (messageData[i].startsWith("E")) {
//                    fundRequest.setCurrency((messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("F")) {
//                    fundRequest.setBank(UnicodeUtils.getNativeString(messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("G")) {
//                    fundRequest.setBankAddress(UnicodeUtils.getNativeString(messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("H")) {
//                    fundRequest.setChequeNo(UnicodeUtils.getNativeString(messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("I")) {
//                    fundRequest.setChequeDate((messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("J")) {
//                    fundRequest.setAmount(Double.parseDouble(messageData[i].substring(1)));
//                    count++;
//                } else if (messageData[i].startsWith("K")) {
//                    fundRequest.setDepositeDate((messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("L")) {
//                    fundRequest.setReferenceNo(UnicodeUtils.getNativeString(messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("M")) {
//                    fundRequest.setNote(UnicodeUtils.getNativeString(messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("N")) {
//                    fundRequest.setBankID(messageData[i].substring(1));
//                } else if (messageData[i].startsWith("O")) {
//                    fundRequest.setBranch(UnicodeUtils.getNativeString(messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("Q")) {
//                    fundRequest.setPortfolio((messageData[i].substring(1)));
//                    try {
//                        TradingShared.getTrader().getPortfolio(fundRequest.getPortfolio()).getPortfolioID();
//                        isValidRecord = true;
//                    } catch (Exception e) {
//                        isValidRecord = false;
//                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                        break;
//                    }
//                    count++;
//                } else if (messageData[i].startsWith("R")) {
//                    fundRequest.setReason(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(messageData[i].substring(1))));
//                } else if (messageData[i].startsWith("S")) {
//                    fundRequest.setDepositeDate(unformatted.format(new Date(Long.parseLong(messageData[i].substring(1))))); //messageData[i].substring(1));
//                }
//            }
//            if ((count > 3) && (isValidRecord)) {
//                FundTransferStore.getSharedInstance().addTransaction(fundRequest);
//                FundTransferStore.getSharedInstance().applyFilter();
//            }
//            String status;
//            String str;
//            if (type.equals("" + TradingConstants.TRANSFER_STATUS_PENDING)) {
//                if ((source == TradeMeta.TRANSFER_CASH) || (source == TradeMeta.TRANSFER_CHEQUE)) {
//                    str = Language.getString("FUNDTRANSFER_CASH_PENDING");
//                    status = TradingShared.getFundTransferStatus(Integer.parseInt(type));
//                    str = str.replaceAll("\\[REF\\]", OMSRefID);
//                    str = str.replaceAll("\\[STATUS\\]", status);
//                } else {
//                    str = Language.getString("FUNDTRANSFER_BANK_PENDING");
//                    str = str.replaceAll("\\[REF\\]", OMSRefID);
//                }
//            } else {
//                status = TradingShared.getFundTransferStatus(Integer.parseInt(type)); // Language.getString("REJECTED");
//                str = Language.getString("FUNDTRANSFER_MSG");
//                str = str.replaceAll("\\[STATUS\\]", status);
//                str = str.replaceAll("\\[REF\\]", OMSRefID);
//            }
//
//            SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
//
//        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
//    }

    private void processDepositeResponse(MIXObject depositResponse) {
        try {
            TransferObject[] resp = depositResponse.getTransferObjects();
            if (resp == null || resp.length == 0) {
                return;
            }

            CashTransfer deposit = (CashTransfer) resp[0];

            int source = 0;
            int count = 0;
            String OMSRefID = Language.getString("NA");
            boolean isValidRecord = true;
//            String TWRefID = "";
            FundTransferRequest fundRequest = new FundTransferRequest();
            fundRequest.setFundTransfer(deposit);
            int type = fundRequest.getStatus();
            OMSRefID = fundRequest.getCashTransactionID();

            try {
                TradingShared.getTrader().getPortfolio(fundRequest.getPortfolio()).getPortfolioID();
                isValidRecord = true;
            } catch (Exception e) {
                isValidRecord = false;
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            if ((OMSRefID != null) && (!OMSRefID.isEmpty()) && (fundRequest.getMethod() > -1) && (fundRequest.getAmount() > 0)
                    && (isValidRecord)) {
                FundTransferStore.getSharedInstance().addTransaction(fundRequest);
                FundTransferStore.getSharedInstance().applyFilter();
            }
            String status;
            String str;
            if (type == TradingConstants.TRANSFER_STATUS_PENDING) {
                if ((source == TradeMeta.TRANSFER_CASH) || (source == TradeMeta.TRANSFER_CHEQUE)) {
                    str = Language.getString("FUNDTRANSFER_CASH_PENDING");
                    status = TradingShared.getFundTransferStatus(type);
                    str = str.replaceAll("\\[REF\\]", OMSRefID);
                    str = str.replaceAll("\\[STATUS\\]", status);
                } else {
                    str = Language.getString("FUNDTRANSFER_BANK_PENDING");
                    str = str.replaceAll("\\[REF\\]", OMSRefID);
                }
            } else {
                status = TradingShared.getFundTransferStatus(type); // Language.getString("REJECTED");
                str = Language.getString("FUNDTRANSFER_MSG");
                str = str.replaceAll("\\[STATUS\\]", status);
                str = str.replaceAll("\\[REF\\]", OMSRefID);
            }

            SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void setPendingFundTransfer(String record) {
        try {
            TradeMessage message = new TradeMessage(record);
            String messageDat = message.getMessageData();
            String[] fields = messageDat.split(TradeMeta.DS);
            String OMSRefID = Language.getString("NA");
            String type = Language.getString("NA");
            boolean isNew = false;
            boolean isValidRecord = true;

            for (int j = 0; j < fields.length; j++) {
                if (fields[j].equals("0")) {
                    continue;
                }
                String[] messageData = fields[j].split(TradeMeta.FD);
                int count = 0;
                for (int i = 0; i < messageData.length; i++) {
                    if (messageData[i].startsWith("A")) {
                        OMSRefID = messageData[i].substring(1);
                        count++;
                    } else if (messageData[i].startsWith("G")) {
                        type = messageData[i].substring(1);
                        count++;
                    }
                    if (count == 2)
                        break;
                }
                FundTransfer ftransfer = FundTransferStore.getSharedInstance().getFilteredTransaction(OMSRefID);
                WithdrawaRequest withReq = null;
                FundTransferRequest fundReq = null;
                if (ftransfer == null) {
                    isNew = true;
                    if (type.equals("DEPOST"))
                        fundReq = new FundTransferRequest();
                    else
                        withReq = new WithdrawaRequest();
                } else {
                    isNew = false;
                    if (type.equals("DEPOST"))
                        fundReq = (FundTransferRequest) ftransfer;
                    else
                        withReq = (WithdrawaRequest) ftransfer;
                }
                if (fundReq != null) {
                    for (int i = 0; i < messageData.length; i++) {
                        try {
                            if (messageData[i].startsWith("A")) {
                                //  fundReq.setReference(messageData[i].substring(1));
                            } else if (messageData[i].startsWith("B")) {
                                fundReq.setAmount(Double.parseDouble(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("C")) {
                                fundReq.setChequeNo(messageData[i].substring(1));
                            } else if (messageData[i].startsWith("D")) {
                                fundReq.setChequeDate(unformatted.format(new Date(Long.parseLong(messageData[i].substring(1)))));
                            } else if (messageData[i].startsWith("E")) {
//                                fundReq.setDepositeDate(messageData[i].substring(1));
                            } else if (messageData[i].startsWith("H")) {
                                fundReq.setDepositeDate(unformatted.format(new Date(Long.parseLong(messageData[i].substring(1))))); //messageData[i].substring(1));
                            } else if (messageData[i].startsWith("I")) {
                                fundReq.setCurrency(messageData[i].substring(1));
                            } else if (messageData[i].startsWith("J")) {
                                fundReq.setBank(UnicodeUtils.getNativeString(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("K")) {
                                fundReq.setBankAddress(UnicodeUtils.getNativeString(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("L")) {
                                fundReq.setNote(UnicodeUtils.getNativeString(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("M")) {
                                fundReq.setStatus(Integer.parseInt(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("N")) {
                                fundReq.setPortfolio((messageData[i].substring(1)).trim());
                                try {
                                    TradingShared.getTrader().getPortfolio(fundReq.getPortfolio()).getPortfolioID();
                                    isValidRecord = true;
                                } catch (Exception e) {
                                    isValidRecord = false;
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    break;
                                }
                            } else if (messageData[i].startsWith("O")) {
                                fundReq.setMethod(Integer.parseInt(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("P")) {
                                fundReq.setBankID((messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("Q")) {
                                fundReq.setBranch(UnicodeUtils.getNativeString(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("R")) {
                                fundReq.setReason(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(messageData[i].substring(1))));
                            } else if (messageData[i].startsWith("S")) {
                                fundReq.setReferenceNo(UnicodeUtils.getNativeString(messageData[i].substring(1)));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                    if ((isNew) && (isValidRecord))
                        FundTransferStore.getSharedInstance().addTransaction(fundReq);
                } else {
                    for (int i = 0; i < messageData.length; i++) {
                        try {
                            if (messageData[i].startsWith("A")) {
                                //    withReq.setReference(messageData[i].substring(1));
                            } else if (messageData[i].startsWith("B")) {
                                withReq.setAmount(Double.parseDouble(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("P")) {
                                withReq.setBank((messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("O")) {
                                withReq.setMethod(Integer.parseInt(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("M")) {
                                withReq.setStatus(Integer.parseInt(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("N")) {
                                withReq.setPortfolio((messageData[i].substring(1)).trim());
                                try {
                                    TradingShared.getTrader().getPortfolio(withReq.getPortfolio()).getPortfolioID();
                                    isValidRecord = true;
                                } catch (Exception e) {
                                    isValidRecord = false;
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    break;
                                }
                                try {
                                    withReq.setCurrency((TradingShared.getTrader().getPortfolio((messageData[i].substring(1)))).getCurrencyID());
                                } catch (Exception e) {
                                    withReq.setCurrency(Language.getString("NA"));
                                    e.printStackTrace();
                                }
                            } else if (messageData[i].startsWith("F")) {
                                withReq.setAccount(messageData[i].substring(1));
                            } else if (messageData[i].startsWith("R")) {
                                withReq.setReason(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(messageData[i].substring(1))));
                            } else if (messageData[i].startsWith("H")) {
                                withReq.setDepositeDate(unformatted.format(new Date(Long.parseLong(messageData[i].substring(1))))); //messageData[i].substring(1));
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                    if ((isNew) && (isValidRecord))
                        FundTransferStore.getSharedInstance().addTransaction(withReq);
                }
//                FundTransferStore.getSharedInstance().applyFilter();
            }
            FundTransferStore.getSharedInstance().applyFilter();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void setPendingFundTransfer(MIXObject pendingTransfers) {
        try {
            TransferObject[] pendingFundTransfers = pendingTransfers.getTransferObjects();
            if (pendingFundTransfers == null || pendingFundTransfers.length == 0) {
                return;
            }
            CashTransferListResponse response = (CashTransferListResponse) pendingFundTransfers[0];
            if (response == null || response.getCashTransferList() == null) {
                return;
            }

            String OMSRefID = Language.getString("NA");
            int type = -1;
            boolean isNew = false;
            boolean isValidRecord = true;
            ArrayList<CashTransfer> transferlist = response.getCashTransferList();

            for (int j = 0; j < transferlist.size(); j++) {
                CashTransfer fundTransfer = (CashTransfer) transferlist.get(j);
                // OMSRefID = fundTransfer.getReferenceID() + "";
                OMSRefID = fundTransfer.getCashTransactionID() + "";
                type = fundTransfer.getTransactionType();

                FundTransfer ftransfer = FundTransferStore.getSharedInstance().getFilteredTransaction(OMSRefID);
                WithdrawaRequest withReq = null;
                FundTransferRequest fundReq = null;
                if (ftransfer == null) {
                    isNew = true;
                    if (type == MIXConstants.TXN_TYPE_DEPOSIT)
                        fundReq = new FundTransferRequest();
                    else
                        withReq = new WithdrawaRequest();
                } else {
                    isNew = false;
                    if (type == MIXConstants.TXN_TYPE_DEPOSIT)
                        fundReq = (FundTransferRequest) ftransfer;
                    else
                        withReq = (WithdrawaRequest) ftransfer;
                }
                if (fundReq != null) {

                    fundReq.setFundTransfer(fundTransfer);
                    try {
                        TradingShared.getTrader().getPortfolio(fundReq.getPortfolio()).getPortfolioID();
                        isValidRecord = true;
                    } catch (Exception e) {
                        isValidRecord = false;
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        //  break;
                    }


                    if ((isNew) && (isValidRecord)) {
                        FundTransferStore.getSharedInstance().addTransaction(fundReq);
                    }
                } else {
                    withReq.setWithdrawal(fundTransfer);

                    try {
                        TradingShared.getTrader().getPortfolio(withReq.getPortfolio()).getPortfolioID();
                        isValidRecord = true;
                    } catch (Exception e) {
                        isValidRecord = false;
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        //  break;
                    }

                    //  if ((isNew) && (isValidRecord)) {             // todo portfolio checking is removed since it canot be send by server
                    if ((isNew)) {
                        FundTransferStore.getSharedInstance().addTransaction(withReq);
                    }
                }
//                FundTransferStore.getSharedInstance().applyFilter();
            }
            FundTransferStore.getSharedInstance().applyFilter();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void cancelFundTransfer(String record) {
        try {

            TradeMessage message = new TradeMessage(record);
            String messageDat = message.getMessageData();
            String[] messageData = messageDat.split(TradeMeta.FD);
            int status = 0;
            String statusStr;
            String rejReason = null;
            String OMSRefID = Language.getString("NA");
            for (int i = 0; i < messageData.length; i++) {
                if (messageData[i].startsWith("A")) {
                    status = Integer.parseInt(messageData[i].substring(1));
                } else if (messageData[i].startsWith("B")) {
                    OMSRefID = messageData[i].substring(1);
                } else if (messageData[i].startsWith("C")) {
                    rejReason = messageData[i].substring(1);
                }
            }
            statusStr = TradingShared.getFundTransferStatus(status);
            String str = Language.getString("FUNDTRANSFER_CANCEL_MSG");
            str = str.replaceAll("\\[STATUS\\]", statusStr);
            str = str.replaceAll("\\[REF\\]", OMSRefID);
            SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
            try {
                if (status == TradingConstants.TRANSFER_STATUS_CANCELLED) {
                    FundTransferStore.getSharedInstance().getFilteredTransaction(OMSRefID).setStatus(status);
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void cancelFundTransfer(MIXObject record) {
        try {
            TransferObject[] response = record.getTransferObjects();
            if (response == null || response.length == 0) {
                return;
            }
            CashTransfer fundTransfer = (CashTransfer) response[0];

            //   TradeMessage message = new TradeMessage(record);
            //   String messageDat = message.getMessageData();
            //   String[] messageData = messageDat.split(TradeMeta.FD);
            if (fundTransfer != null) {
                int status = 0;
                String statusStr;
                String rejReason = null;
                String OMSRefID = Language.getString("NA");

                status = fundTransfer.getStatus();
//               OMSRefID = fundTransfer.getReferenceID();
                OMSRefID = fundTransfer.getCashTransactionID();
                rejReason = fundTransfer.getReason();

                statusStr = TradingShared.getFundTransferStatus(status);
                String str = Language.getString("FUNDTRANSFER_CANCEL_MSG");
                str = str.replaceAll("\\[STATUS\\]", statusStr);
                str = str.replaceAll("\\[REF\\]", OMSRefID);
                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
                try {
                    if (status == TradingConstants.TRANSFER_STATUS_CANCELLED) {
                        FundTransferStore.getSharedInstance().getFilteredTransaction(OMSRefID).setStatus(status);
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        } catch (
                Exception e
                )

        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

//    private void processWithdrawalResponse(String record) {
//        try {
//            TradeMessage message = new TradeMessage(record);
//            String messageDat = message.getMessageData();
//            String[] messageData = messageDat.split(TradeMeta.FD);
//
//            WithdrawaRequest withdRequest = new WithdrawaRequest();
//            String type = "0";
//            boolean isValidRecord = true;
//
//            String OMSRefID = Language.getString("NA");
////            String TWRefID = "";
//            int count = 0;
//            for (int i = 0; i < messageData.length; i++) {
//                if (messageData[i].startsWith("A")) {
//                    type = messageData[i].substring(1);
//                    withdRequest.setStatus(Integer.parseInt(messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("B")) {
//                    OMSRefID = messageData[i].substring(1);
//                    withdRequest.setReference(messageData[i].substring(1));
//                    count++;
////                } else if (messageData[i].startsWith("C")) {
////                    TWRefID = messageData[i].substring(1);
//                } else if (messageData[i].startsWith("D")) {
//                    withdRequest.setMethod(Integer.parseInt(messageData[i].substring(1)));
//                    count++;
//                } else if (messageData[i].startsWith("E")) {
//                    withdRequest.setAmount(Double.parseDouble(messageData[i].substring(1)));
//                    count++;
//                } else if (messageData[i].startsWith("F")) {
//                    withdRequest.setDate(messageData[i].substring(1));
//                } else if (messageData[i].startsWith("H")) {
//                    withdRequest.setBank((messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("I")) {
//                    withdRequest.setAccount((messageData[i].substring(1)));
//                } else if (messageData[i].startsWith("J")) {
//                    withdRequest.setPortfolio((messageData[i].substring(1)).trim());
//                    try {
//                        TradingShared.getTrader().getPortfolio(withdRequest.getPortfolio()).getPortfolioID();
//                        isValidRecord = true;
//                    } catch (Exception e) {
//                        isValidRecord = false;
//                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                        break;
//                    }
//                    try {
//                        withdRequest.setCurrency((TradingShared.getTrader().getPortfolio((messageData[i].substring(1)))).getCurrencyID());
//                    } catch (Exception e) {
//                        withdRequest.setCurrency(Language.getString("NA"));
//                        e.printStackTrace();
//                    }
//                    count++;
//                } else if (messageData[i].startsWith("K")) {
//                    withdRequest.setReason(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(messageData[i].substring(1))));
//                } else if (messageData[i].startsWith("S")) {
//                    withdRequest.setDepositeDate(unformatted.format(new Date(Long.parseLong(messageData[i].substring(1))))); //messageData[i].substring(1));
//                }
//            }
//            if (count > 3) {
//                FundTransferStore.getSharedInstance().addTransaction(withdRequest);
//                FundTransferStore.getSharedInstance().applyFilter();
//            }
//            String status;
//            String str;
//            if (type.equals("" + TradingConstants.TRANSFER_STATUS_PENDING)) {
//                str = Language.getString("WITHDRAWEL_PENDING_MSG");
//                str = str.replaceAll("\\[REF\\]", OMSRefID);
////                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
//
//
//            } else {
//                status = TradingShared.getFundTransferStatus(Integer.parseInt(type));
//                str = Language.getString("WITHDRAWEL_MSG");
//                str = str.replaceAll("\\[STATUS\\]", status);
//                str = str.replaceAll("\\[REF\\]", OMSRefID);
//            }
//            SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void processWithdrawalResponse(MIXObject withdrawlResponse) {
        try {
            TransferObject[] reply = withdrawlResponse.getTransferObjects();
            if (reply == null || reply.length == 0) {
                return;
            }
            CashTransfer withdrawal = (CashTransfer) reply[0];
            WithdrawaRequest withdRequest = new WithdrawaRequest();
            withdRequest.setWithdrawal(withdrawal);
            int type = withdRequest.getStatus();
            boolean isValidRecord = true;

            String OMSRefID = Language.getString("NA");
//            String TWRefID = "";
            int count = 0;

            try {
                TradingShared.getTrader().getPortfolio(withdRequest.getPortfolio()).getPortfolioID();
                isValidRecord = true;
            } catch (Exception e) {
                isValidRecord = false;
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

            }
            // OMSRefID = withdrawal.getReferenceID() + "";
            OMSRefID = withdrawal.getCashTransactionID() + "";

            if (withdrawal.getCashTransactionID() != null && withdrawal.getPayMethod() != null && withdrawal.getAmount() > 0 && isValidRecord) {
                FundTransferStore.getSharedInstance().addTransaction(withdRequest);
                FundTransferStore.getSharedInstance().applyFilter();
            }
            String status;
            String str;
            if (type == TradingConstants.TRANSFER_STATUS_PENDING) {
                str = Language.getString("WITHDRAWEL_PENDING_MSG");
                str = str.replaceAll("\\[REF\\]", OMSRefID);
//                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);


            } else {
                status = TradingShared.getFundTransferStatus(type);
                str = Language.getString("WITHDRAWEL_MSG");
                str = str.replaceAll("\\[STATUS\\]", status);
                str = str.replaceAll("\\[REF\\]", OMSRefID);
            }
            SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setStocktransferData(String frame, boolean showMessage) {
        try {
            TradeMessage message = new TradeMessage(frame);
            String messageDat = message.getMessageData();
            String[] fields = messageDat.split(TradeMeta.DS);
            String OMSRefID = Language.getString("NA");
            String status = Language.getString("NA");
            String rejReason = Language.getString("NA");
            boolean isNew = false;
            SimpleDateFormat unformatted = new SimpleDateFormat("yyyyMMdd");
            for (int j = 0; j < fields.length; j++) {
                try {
                    String[] messageData = fields[j].split(TradeMeta.FD);
                    StockTransferObject object = null;
                    int count = 0;
                    for (int i = 0; i < messageData.length; i++) {
                        try {
                            if (messageData[i].startsWith("A")) {
                                OMSRefID = messageData[i].substring(1);
                            }
                        } catch (Exception e) {
                        }
                    }
                    object = StockTransferObjectStore.getSharedInstance().getStockTransObject(OMSRefID);
                    if (object == null) {
                        isNew = true;
                        object = new StockTransferObject(OMSRefID);
                    } else {
                        isNew = false;
                    }

                    for (int i = 0; i < messageData.length; i++) {
                        try {
                            if (messageData[i].startsWith("A")) {
//                                object = new StockTransferObject(messageData[i].substring(1));
//                                OMSRefID = messageData[i].substring(1);
                                count++;
                            } else if (messageData[i].startsWith("B")) {
                                object.setStatus(Integer.parseInt(messageData[i].substring(1)));
                                status = messageData[i].substring(1);
                            } else if (messageData[i].startsWith("C")) {
                                object.setRejectReason(Integer.parseInt(messageData[i].substring(1)));
                                rejReason = messageData[i].substring(1);
                            } else if (messageData[i].startsWith("D")) {
                                object.setPortfolioID(messageData[i].substring(1));
                                count++;
                            } else if (messageData[i].startsWith("E")) {
                                object.setSymbol(messageData[i].substring(1));
                                count++;
                            } else if (messageData[i].startsWith("F")) {
                                object.setBroker(UnicodeUtils.getNativeString(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("G")) {
                                object.setQuantity(Integer.parseInt(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("H")) {
                                object.setAvgCost(Double.parseDouble(messageData[i].substring(1)));
                            } else if (messageData[i].startsWith("I")) {
                                object.setExchange(messageData[i].substring(1));
                            } else if (messageData[i].startsWith("J")) {
                                object.setTransferDate(unformatted.format(new Date(Long.parseLong(messageData[i].substring(1)))));
                            }
                        } catch (Exception e) {
                        }
                    }
                    if (count > 2) {
                        if (isNew)
                            StockTransferObjectStore.getSharedInstance().addStockTransObject(object);
                    }
                } catch (Exception e) {
                }
            }
            if (showMessage) {
                status = TradingShared.getFundTransferStatus(Integer.parseInt(status));
                String str = Language.getString("STOCK_TRANSFER_STATUS_MSG");
                str = str.replaceAll("\\[STATUS\\]", status);
                str = str.replaceAll("\\[REF\\]", OMSRefID);
                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void cancelStockTransfer(String record) {
        try {
            TradeMessage message = new TradeMessage(record);
            String messageDat = message.getMessageData();
            String[] messageData = messageDat.split(TradeMeta.FD);
            int status = 0;
            String statusStr;
            int rejReason = 0;
            String OMSRefID = Language.getString("NA");
            for (int i = 0; i < messageData.length; i++) {
                if (messageData[i].startsWith("A")) {
                    OMSRefID = messageData[i].substring(1);
                } else if (messageData[i].startsWith("B")) {
                    status = Integer.parseInt(messageData[i].substring(1));
                } else if (messageData[i].startsWith("C")) {
                    rejReason = Integer.parseInt(messageData[i].substring(1));
                }
            }
            statusStr = TradingShared.getFundTransferStatus(status);
            String str = Language.getString("STOCK_TRANSFER_CANCEL_MSG");
            str = str.replaceAll("\\[STATUS\\]", statusStr);
            str = str.replaceAll("\\[REF\\]", OMSRefID);
            SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
            try {
                if (status == TradingConstants.TRANSFER_STATUS_CANCELLED) {
                    StockTransferObjectStore.getSharedInstance().getStockTransObject(OMSRefID).setStatus(status);
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

/*private void updateAccount(String frame) {
try {
TradeMessage message = new TradeMessage(frame);
String[] messageRecords = message.getMessageData().split(TradeMeta.RS);
for (String record : messageRecords) {
    String[] messageData = record.split(TradeMeta.DS);
    Account account = null;

    System.out.println("Update account Message " + message.getMessageData());

    for (int j = 0; j < messageData.length; j++) {
        if (messageData[j].startsWith("B")) {
            String accountID = messageData[j].substring(1);
            account = TradingShared.getTrader().findAccount(accountID);
            if (account == null) {
                account = TradingShared.getTrader().createAccount(accountID);
            }
        } else if (messageData[j].startsWith("C")) {
            account.setType(SharedMethods.intValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("D")) {
            account.setCurrency(messageData[j].substring(1));
        } else if (messageData[j].startsWith("E")) {
            account.setBalance(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("F")) {
            account.setBlockedAmount(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("G")) {
            account.setODLimit(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("H")) {
            account.setBuyingPower(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("I")) {
            account.setMarginLimit(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("J")) {
//                    account.setNetSecurity(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("K")) {
//                    account.setTotalPledged(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("L")) {
            account.setDate(SharedMethods.longValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("M")) {
            account.setUnrializedSales(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("N")) {
            account.setCashAvailableForWithdrawal(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("O")) {
            account.setDayMarginLimit(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("P")) {
            account.setPendingDeposites(SharedMethods.doubleValue(messageData[j].substring(1)));
        } else if (messageData[j].startsWith("Q")) {
            account.setPendingTransfers(SharedMethods.doubleValue(messageData[j].substring(1)));
        }
    }
    TradingShared.getTrader().notifyAccountListeners(account.getAccountID());
    TradeServer.write(account.getXML());
    messageData = null;
    account = null;
}
*//*TradeMethods.getSharedInstance().getAccountFrame().setTitle(Language.getString("ACCOUNT_SUMMARY") + " : " +
                    TradingShared.getTrader().getAccount(0).getAccountID() + " : " +
                    TradingShared.getTrader().getAccount(0).getCurrency());*//*

            message = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void updateAccount(MIXObject basicMixReply, byte path) {
        MIXHeader header = basicMixReply.getMIXHeader();
        TransferObject[] accountDetailsresponse = basicMixReply.getTransferObjects();
        if (accountDetailsresponse == null || accountDetailsresponse.length == 0) {
            return;
        }
        CashAccountResponse response = (CashAccountResponse) accountDetailsresponse[0];
        if (response == null) {
            return;
        }
        List<CashAccount> accountDetails = response.getCashAccountRec();
        if (accountDetails != null && accountDetails.size() > 0) {
            for (int i = 0; i < accountDetails.size(); i++) {
                Account account;
                CashAccount cashAccount = (CashAccount) accountDetails.get(i);
                String accountID = cashAccount.getAccountID();
                account = TradingShared.getTrader().findAccount(accountID);
                if (account == null) {
                    account = TradingShared.getTrader().createAccount(accountID, path);
                }
                account.setCashAccount(cashAccount);
//                account.setType(cashAccount.getAccountType());
//                account.setCurrency(cashAccount.getCurrency());
//                account.setBalance(cashAccount.getBalance());
//                account.setBlockedAmount(cashAccount.getBlockedAmount());
//                account.setODLimit(cashAccount.getODLimit());
//                account.setBuyingPower(cashAccount.getBuyingPower());
//                account.setMarginLimit(cashAccount.getMarginLimit());
//                // account.setDate( cashAccount.getDate() );    // todo check date format
//                account.setUnrializedSales(cashAccount.getUnrealizedSales());
//                account.setCashAvailableForWithdrawal(cashAccount.getCashForWithdrawal());
//                account.setDayMarginLimit(cashAccount.getMarginLimit());
//  account.setPendingDeposites(cashAccount.get);     // todo check for pending deposits
//account.setPendingTransfers(cashAccount.set);    // todo check for pending transfers
                TradingShared.getTrader().notifyAccountListeners(account.getAccountID());
                TradeServer.write(account.getXML());
                account = null;

            }

        }
//        if (TradeMethods.getSharedInstance().getAccountWindow().isVisible() &&TradeMethods.getSharedInstance().getAccountWindow().isInitdataFromUpdate() ) {
//            TradeMethods.getSharedInstance().getAccountWindow().setInitialData();
//            TradeMethods.getSharedInstance().getAccountWindow().setInitdataFromUpdate(false);
//        }


    }

    private void getUserValidation(String frame) { //changeTradingPass(String frame){
        try {
            Level2AuthWait.getSharedInstance().dispose();
            TradeMessage message = new TradeMessage(frame);
            boolean sucess = false;
            String messsageData = message.getMessageData();
            boolean response = false;
            SetUserDetails detailsDialog;
            String[] txnData = messsageData.split(TradeMeta.DS);
            if (messsageData.equals("\n")) { // internally generated popup request
                detailsDialog = new SetUserDetails(Client.getInstance().getFrame(), Language.getString("SET_USER_DETAILS"));
                sucess = detailsDialog.showDialog();
                detailsDialog.close();
                if (sucess) {
                    Level2AuthWait.getSharedInstance().showWithNoneBlocking();
//                sucess = Client.getInstance().changeTradePass(Language.getString("CHANGE_LOGIN_EXPLANATION_MESSAGE"));
                } else {
                    Client.getInstance().disconnectFromTradingServer();
                }
            } else {
                for (int i = 0; i < txnData.length; i++) {
                    if ((txnData[i].startsWith("A"))) {
                        response = Integer.parseInt(txnData[i].substring(1)) == 1;
                        if ((response) && (TradingShared.isChangePassowrdNeeded())) {
                            TradeMessage message2 = new TradeMessage(TradeMeta.MT_CHANGE_PW);
                            message2.addData("");
                            getList().add(message2.toString());
                            message2 = null;
                        }
                        break;
                    }
                }
                if (response) {
//                    sucess = Client.getInstance().changeTradePass(Language.getString("CHANGE_LOGIN_EXPLANATION_MESSAGE"));
//                    if (!sucess) {
//                        Client.getInstance().disconnectFromTradingServer();
//                    }
                } else {
                    int type = SharedMethods.showConfirmMessage(Language.getString("WRONG_USER_DETAILS"), JOptionPane.ERROR_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
//                    new ShowMessage(Language.getString("WRONG_USER_DETAILS"), "E");
                    if (type == JOptionPane.OK_OPTION) {
                        Client.getInstance().setTradeSessionDisconnected(true);
                        AutoTradeLoginDaemon.setMode(AutoTradeLoginDaemon.IDLE);
//                        TradingShared.setLevel1Password(null);
//                        Client.getInstance().disconnectFromTradingServer();
                    } else {
                        Client.getInstance().setTradeSessionDisconnected(true);
//                        AutoTradeLoginDaemon.setMode(AutoTradeLoginDaemon.STOPPED);
//                        Client.getInstance().disconnectFromTradingServer();
                    }
                }
            }
            detailsDialog = null;
        } catch (Exception e) {
            Level2AuthWait.getSharedInstance().dispose();
            Client.getInstance().disconnectFromTradingServer();
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void processMarkForDelivery(String frame) {
        try {
            TradeMessage message = new TradeMessage(frame);
            String messsageData = message.getMessageData();
            String[] data = messsageData.split("\\|");
//code|clOrdId|Text
//code  =  1  is success
//code  = -1 is failure

            try {
                if (Integer.parseInt(data[0].trim()) == 1) {
                    SharedMethods.showMessage(Language.getString("MSG_MARK_FOR_DELIVERY_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
                } else {
                    String str = Language.getString("MSG_MARK_FOR_DELIVERY_ERROR");
                    str = str.replaceAll("\\[REASON\\]", Language.getLanguageSpecificString(UnicodeUtils.getNativeString(data[2])));
                    SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                String str = Language.getString("MSG_MARK_FOR_DELIVERY_ERROR");
                str = str.replaceAll("\\[REASON\\]", Language.getLanguageSpecificString(UnicodeUtils.getNativeString(messsageData)));
                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void processCashTransferResponce(String frame) {
        try {
            TradeMessage message = new TradeMessage(frame);
            String messsageData = message.getMessageData();

            String[] txnData = messsageData.split(TradeMeta.DS);
            boolean isSuccess = false;
            String strMessage = "";

            if ((txnData[0].startsWith("A"))) {
                if (txnData[0].substring(1).equals("1")) { // sucess
                    isSuccess = true;
//new ShowMessage(Language.getString("CASH_TRANSFER_SUCESS"), "I");
                } else {
                    isSuccess = false;
//new ShowMessage(Language.getString("CASH_TRANSFER_FAILED"), "I");
                }
            }
            try {
                if (txnData[1].startsWith("B")) {
                    strMessage = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(txnData[1].substring(1)));
                }
            } catch (Exception e) {
                // it is possible to get an empty msg
            }
            if (strMessage.trim().equals("")) {
                if (isSuccess) { // sucess
                    new ShowMessage(Language.getString("CASH_TRANSFER_SUCESS"), "I");
                } else {
                    new ShowMessage(Language.getString("CASH_TRANSFER_FAILED"), "I");
                }
            } else {
                new ShowMessage(strMessage, "I");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processCashTransferResponce(MIXObject response) {
        try {
            TransferObject[] responseobject = response.getTransferObjects();

            boolean isSuccess = false;
            String strMessage = "";
            CashTransfer resp = null;
            if (responseobject == null || responseobject.length == 0) {
                isSuccess = false;
            } else {
                resp = (CashTransfer) responseobject[0];
            }

            if (resp != null) {
                if (resp.getStatus() == 1) { // sucess
                    isSuccess = true;
//new ShowMessage(Language.getString("CASH_TRANSFER_SUCESS"), "I");
                } else {
                    isSuccess = false;
//new ShowMessage(Language.getString("CASH_TRANSFER_FAILED"), "I");
                }
            }
            try {
                if (resp != null) {
                    strMessage = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(resp.getReason()));
                }
            } catch (Exception e) {
                // it is possible to get an empty msg
            }
            if (strMessage == null || strMessage.trim().equals("")) {
                if (isSuccess) { // sucess
                    new ShowMessage(Language.getString("CASH_TRANSFER_SUCESS"), "I");
                } else {
                    new ShowMessage(Language.getString("CASH_TRANSFER_FAILED"), "I");
                }
            } else {
                new ShowMessage(strMessage, "I");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeTradingPassword(String frame) {   // getTradingDetails(String frame) {
        try {
            //System.out.println("Frame " + frame);
            Level2AuthWait.getSharedInstance().dispose();
            boolean sucess = false;
            String reason = Language.getString("NA");
            TradeMessage message = new TradeMessage(frame);
            String messsageData = message.getMessageData();
//            SetUserDetails detailsDialog;
            if (messsageData.equals("\n")) { // internally generated popup request
//                detailsDialog = new SetUserDetails(Client.getInstance().getFrame(), Language.getString("SET_USER_DETAILS") );
//                sucess =  detailsDialog.showDialog();
//                detailsDialog.close();
//                if(sucess){
//                    Level2AuthWait.getSharedInstance().showWithNoneBlocking();
////                sucess = Client.getInstance().changeTradePass(Language.getString("CHANGE_LOGIN_EXPLANATION_MESSAGE"));
//                } else{
//                    Client.getInstance().disconnectFromTradingServer();
//                }
                String pWDMessage = Language.getString("CHANGE_LOGIN_EXPLANATION_MESSAGE");
                if (TradingShared.getChangePWDType() == TradingConstants.CHANGE_PASSWORD_TYPE_EXPIRED) {
                    pWDMessage = Language.getString("CHANGE_PASSWORD_EXPIRED_MESSAGE");
                }
                sucess = Client.getInstance().changeTradePass(pWDMessage);
                if (!sucess) {
                    Client.getInstance().disconnectFromTradingServer();
                }
            } else {
                String[] txnData = messsageData.split(TradeMeta.DS);
                for (int i = 0; i < txnData.length; i++) {
                    if ((txnData[i].startsWith("B"))) {
                        if (txnData[i].substring(1).equals("1")) { // sucess
                            new ShowMessage(Language.getString("TRADING_PASSWORDS_CHANGED"), "I");
                            sucess = true;

                        }
                    } else if ((txnData[i].startsWith("C"))) {
                        reason = Language.getLanguageSpecificString(txnData[i].substring(1), "|");
                        break;
                    }
                }
                txnData = null;
                if (!sucess) {
                    String template = Language.getString("TRADING_PASSWORDS_CHANGE_ERROR");
                    template = template.replaceFirst("\\[REASON\\]", reason);
                    new ShowMessage(template, "E");
                }
            }
//            detailsDialog = null;
            message = null;
        } catch (Exception e) {
            Level2AuthWait.getSharedInstance().dispose();
            Client.getInstance().disconnectFromTradingServer();
            e.printStackTrace();
        }
    }

    private void changeTradingPassword(MIXObject changePWDReply) {   // getTradingDetails(String frame) {
        try {
            TransferObject[] reply = changePWDReply.getTransferObjects();
            if (reply == null || reply.length == 0) {
                return;
            }
            ChangePasswordResponse resp = (ChangePasswordResponse) reply[0];
//System.out.println("Frame " + frame);
            Level2AuthWait.getSharedInstance().dispose();
            boolean sucess = false;
            String reason = Language.getString("NA");
            if (resp.getUserID() != null && resp.getUserID().equals("N/A")) {
                String pWDMessage = Language.getString("CHANGE_LOGIN_EXPLANATION_MESSAGE");
                if (TradingShared.getChangePWDType() == TradingConstants.CHANGE_PASSWORD_TYPE_EXPIRED) {
                    pWDMessage = Language.getString("CHANGE_PASSWORD_EXPIRED_MESSAGE");
                }
                sucess = Client.getInstance().changeTradePass(pWDMessage);
                TradingShared.setMandatoryPWDChangeSuccess(false);
                if (!sucess) {
                    Client.getInstance().disconnectFromTradingServer();
                }
            } else {

                if (resp.getAuthResult() == TradeMeta.AUTH_OK) {
                    new ShowMessage(Language.getString("TRADING_PASSWORDS_CHANGED"), "I");
                    sucess = true;
                    TradingShared.setMandatoryPWDChangeSuccess(true);
                } else if ((resp.getAuthResult() == TradeMeta.AUTH_ERROR) || (resp.getAuthResult() == TradeMeta.AUTH_INACTIVE) ||
                        (resp.getAuthResult() == TradeMeta.AUTH_LOCKED) || (resp.getAuthResult() == TradeMeta.AUTH_SUSPENDED)) {
//                    Language.getLanguageSpecificString(resp.getMessage());
                    reason = Language.getLanguageSpecificString(resp.getMessage());
                    String template = Language.getString("TRADING_PASSWORDS_CHANGE_ERROR");
                    template = template.replaceFirst("\\[REASON\\]", reason);
                    SharedMethods.showBlockingMessage(template, JOptionPane.ERROR_MESSAGE);
//                    new ShowMessage(template, "E");
                    if (!TradingShared.isMandatoryPWDChangeSuccess()) {
                        Client.getInstance().disconnectFromTradingServer();

                    }
                }
            }

//            TradeMessage message = new TradeMessage();
//            String messsageData = message.getMessageData();
////            SetUserDetails detailsDialog;
//            if (messsageData.equals("\n")) { // internally generated popup request
////                detailsDialog = new SetUserDetails(Client.getInstance().getFrame(), Language.getString("SET_USER_DETAILS") );
////                sucess =  detailsDialog.showDialog();
////                detailsDialog.close();
////                if(sucess){
////                    Level2AuthWait.getSharedInstance().showWithNoneBlocking();
//////                sucess = Client.getInstance().changeTradePass(Language.getString("CHANGE_LOGIN_EXPLANATION_MESSAGE"));
////                } else{
////                    Client.getInstance().disconnectFromTradingServer();
////                }
//                String pWDMessage = Language.getString("CHANGE_LOGIN_EXPLANATION_MESSAGE");
//                if (TradingShared.getChangePWDType() == TradingConstants.CHANGE_PASSWORD_TYPE_EXPIRED) {
//                    pWDMessage = Language.getString("CHANGE_PASSWORD_EXPIRED_MESSAGE");
//                }
//                sucess = Client.getInstance().changeTradePass(pWDMessage);
//                if (!sucess) {
//                    Client.getInstance().disconnectFromTradingServer();
//                }
//            } else {
//                String[] txnData = messsageData.split(TradeMeta.DS);
//                for (int i = 0; i < txnData.length; i++) {
//                    if ((txnData[i].startsWith("B"))) {
//                        if (txnData[i].substring(1).equals("1")) { // sucess
//                            new ShowMessage(Language.getString("TRADING_PASSWORDS_CHANGED"), "I");
//                            sucess = true;
//
//                        }
//                    } else if ((txnData[i].startsWith("C"))) {
//                        reason = Language.getLanguageSpecificString(txnData[i].substring(1), "|");
//                        break;
//                    }
//                }
//                txnData = null;
//                if (!sucess) {
//                    String template = Language.getString("TRADING_PASSWORDS_CHANGE_ERROR");
//                    template = template.replaceFirst("\\[REASON\\]", reason);
//                    new ShowMessage(template, "E");
//                }
//            }
//            detailsDialog = null;
//            message = null;
        } catch (Exception e) {
            Level2AuthWait.getSharedInstance().dispose();
            Client.getInstance().disconnectFromTradingServer();
            e.printStackTrace();
        }
    }

    private void processTradingAuthentication(String frame) {
        try {
            TradeMessage message = new TradeMessage(frame);
            String[] messsageData = message.getMessageData().split(TradeMeta.DS);

// A- TraderID B- AuthResult C- Message D- MessageCode
            for (int i = 0; i < messsageData.length; i++) {
                if (messsageData[i].startsWith("B")) {
                    int result = Integer.parseInt(messsageData[i].substring(1));
                    TradingShared.level2AuthenticationSucess = (result == TradeMeta.AUTH_OK);
                    if (!TradingShared.level2AuthenticationSucess) {
                        TradingShared.setLevel2Password(null);
                    }
                } else if (messsageData[i].startsWith("C")) {
                    String messageStr = messsageData[i].substring(1);
                    Object[] options = {Language.getString("OK")};
                    JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                            Language.getLanguageSpecificString(messageStr, "|"),
                            Language.getString("ERROR"),
                            JOptionPane.OK_OPTION,
                            JOptionPane.ERROR_MESSAGE,
                            null,
                            options,
                            options[0]);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Level2AuthWait.getSharedInstance().dispose();  // remove the login wait message
    }

    private void processTradingAuthentication(MIXObject baseMixObject) {
        try {
            MIXHeader header = baseMixObject.getMIXHeader();
            TransferObject[] result = baseMixObject.getTransferObjects();
            if (result == null || result.length == 0) {
                String messageStr = "";
                Object[] options = {Language.getString("OK")};
                JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                        Language.getLanguageSpecificString(messageStr, "|"),
                        Language.getString("ERROR"),
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE,
                        null,
                        options,
                        options[0]);
            } else {
                AuthenticationResponse response = (AuthenticationResponse) result[0];
                int authStatus = response.getAuthenticationStatus();
                TradingShared.level2AuthenticationSucess = (authStatus == TradeMeta.AUTH_OK);
                if (!TradingShared.level2AuthenticationSucess) {
                    TradingShared.setLevel2Password(null);
                    String messageStr = response.getRejectReason();
                    Object[] options = {Language.getString("OK")};
                    JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                            Language.getLanguageSpecificString(messageStr, "|"),
                            Language.getString("ERROR"),
                            JOptionPane.OK_OPTION,
                            JOptionPane.ERROR_MESSAGE,
                            null,
                            options,
                            options[0]);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Level2AuthWait.getSharedInstance().dispose();  // remove the login wait message
    }


    private void updatePortfolio(MIXObject basicMixObj) {
        TransferObject[] response = basicMixObj.getTransferObjects();
        if (response == null || response.length == 0) {
            return;
        }
        PortfolioDetailsResponse details = (PortfolioDetailsResponse) response[0];
        List<PortfolioRecord> results = details.getPortfolioRec();
        if (results != null && results.size() > 0) {
            TransactRecord txn = null;
            for (int i = 0; i < results.size(); i++) {
                try {
                    PortfolioRecord rec = (PortfolioRecord) results.get(i);
                    txn = new TransactRecord(rec.getPortfolioNo());
                    txn.setMixPortfolioRecord(rec);
                    if (txn.getBaseSymbol() != null) {
                        txn.setBaseSymbol(SharedMethods.getSymbolFromKey(TradingShared.getPriceSymbol(txn.getExchange(), txn.getBaseSymbol(), txn.getInstrument(), txn.getPfID())));
                    }
                    if (txn.getSymbol() != null) {
                        txn.setSymbol(SharedMethods.getSymbolFromKey(TradingShared.getPriceSymbol(txn.getExchange(), txn.getSymbol(), txn.getInstrument(), txn.getPfID())));
                    }
                    FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(txn.getExchange(), txn.getBaseSymbol(), txn.getInstrument()));
                    if (futureBaseAttributes != null) {
                        txn.setContractSize(futureBaseAttributes.getContractSize());
                        txn.setPriceCorrectionFactor(futureBaseAttributes.getPriceCorrectionFactor());
                    }
                    if (txn.getInstrument() == -1) {
                        Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(txn.getExchange(), txn.getSymbol());
                        if (stock != null) {
                            txn.setInstrument(stock.getInstrumentType());
                        } else {
                            sendSymbolvalidationWithoutInstrument(txn.getExchange(), txn.getSymbol());
                        }
                    }
                    TradePortfolios.getInstance().addTransaction(txn);          // request. 3/ Mar/ 2005
                    TradePortfolios.getInstance().createValuationList(null, null);
                    TradePortfolios.getInstance().createDCValList(null, null);
                    TradePortfolios.getInstance().createPfWiseValutionLists(null, null);
                    ValuationRecord valRecord = TradePortfolios.getInstance().getValuationRecord(txn.getKey().getTradeKey());
                    if (valRecord != null) {
                        TradeServer.write(valRecord.getXML());
                        valRecord = null;
                    }

//                    if (!DataStore.getSharedInstance().isValidSymbol(SharedMethods.getKey(txn.getExchange(), txn.getSymbol(), txn.getInstrument()))) {
//                        sendValidateSymbolRequest(txn.getExchange(), txn.getSymbol(), txn.getInstrument());
//                    }
                    txn = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }

    }

    private void processOrderList(MIXObject baseMixObj, boolean conditional) {
        MIXHeader header = baseMixObj.getMIXHeader();
        TransferObject[] pendingOrders = baseMixObj.getTransferObjects();

        if (pendingOrders == null || pendingOrders.length == 0) {
            return;
        }
        OrderInquiryResponse response = (OrderInquiryResponse) pendingOrders[0];

        ArrayList<Order> orders = response.getOrders();
        if (orders == null || orders.size() == 0) {
            return;
        }


        try {

            if (TradingShared.isBlotterRefreshPullOnly()) {
                OrderStore.getSharedInstance().clear();
            }


            for (int i = 0; i < orders.size(); i++) {
                Order trsOrder = orders.get(i);
                conditional = ((trsOrder.getOrderCategory() == MIXConstants.ORDER_CATEGORY_CONDITIONAL) || (trsOrder.getOrderCategory() == MIXConstants.ORDER_CATEGORY_ADVANCE));

                if (conditional && (trsOrder.getBehaviourList() != null)) {
                    boolean txnremoved = false;
                    for (ConditionalBehaviour behaviour : trsOrder.getBehaviourList()) {
                        if ((behaviour.getConditionCategory() == MIXConstants.CONDITION_CATEGORY_PRE_CONDITION) && (behaviour.getConditionStatus() == MIXConstants.CONDITION_STATUS_TRIGGERED)) {
//                            removeTransaction(trsOrder.getClOrdID() + "");
                            txnremoved = true;
                            break;

                        }
                    }
                    if (txnremoved) {
                        continue;
                    }
                    txnremoved = false;
                }
                /*if (conditional && (trsOrder.getBehaviourList() != null && trsOrder.getBehaviourList().size() > 0) &&
                        (trsOrder.getBehaviourList().get(0).getConditionStatus() == TradeMeta.CONDITION_STATUS_TRIGGERED)) {
                    removeTransaction(trsOrder.getClOrdID() + "");
                    continue;
                }*/

                if ((trsOrder.getMubasherOrderNumber() > 0) || (trsOrder.getClOrdID() != null)) {
                    Transaction transaction = null;

                    if (trsOrder.getOrigClOrdID() != null) {
                        transaction = OrderStore.getSharedInstance().searhTransactionByOrigClOrderID(trsOrder.getOrigClOrdID() + "");
                        if (transaction == null) {
                            transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(trsOrder.getOrigClOrdID() + "");
                        }
                    }

                    if ((transaction == null) && (trsOrder.getClOrdID() != null)) {
                        transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(trsOrder.getClOrdID() + "");
                    }

                    if ((transaction == null) && (trsOrder.getMubasherOrderNumber() > 0)) { // if not found, try the Mubasher Order ID
                        transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(trsOrder.getMubasherOrderNumber() + "");
                    }
                    if ((transaction == null) && (trsOrder.getTwOrderID() != null)) { // if not found, try the TW Order ID
                        transaction = OrderStore.getSharedInstance().searhTransactionByTWOrderID(trsOrder.getTwOrderID() + "", true);

                        try {
                            if (transaction != null && transaction.getStatus() != TradeMeta.T39_SENDING) { // issue with child orders of algo orders
                                transaction = null;
                            }
                        } catch (Exception e) {
                            transaction = null;
                        }
                    }

                    if (transaction != null) { // existing order - replace
                        int oldStatus = transaction.getStatus();
                        if ((oldStatus == TradeMeta.T39_Filled) || (oldStatus == TradeMeta.T39_Canceled)) {
                            Transaction newTransaction = new Transaction();
//   newTransaction.setOrderData(trsOrder);
                            newTransaction.setMixOrderObject(trsOrder);
                            if ((trsOrder.getText() != null) && ((header.getRequestType() == MIXConstants.RESPONSE_TYPE_NEW_ORDER) || (header.getRequestType() == TradeMeta.MT_SLICED_ORDER))) {    // todo add sliced order
                                TradeMethods.getSharedInstance().showRejectReasonFull(newTransaction);
                            } else if (transaction.getOrderCategory() == MIXConstants.ORDER_CATEGORY_ALGO) {     //added by udaya
                                transaction.setMixOrderObject(trsOrder);     // when algo order is cancled it does not get updated
                            }
                            continue;
                        }
                        //transaction.setOrderData(trsOrder);
                        transaction.setMixOrderObject(trsOrder);
                        if ((transaction.getStatus() == TradeMeta.T39_Filled) ||
                                (transaction.getStatus() == TradeMeta.T39_Partially_Filled)) {
                            OrderDepthStore.getSharedInstance().updateRecord((Transaction) transaction.clone());

                        }
                        if ((transaction.getStatus() == TradeMeta.T39_Filled) && (oldStatus != TradeMeta.T39_Filled)) {
                            showOrderExecutedMessage(transaction);
                        }
                    } else { // newly accepted order by back office - check for the empty order in store and replace
                        Transaction newTransaction = new Transaction();
//newTransaction.setOrderData(trsOrder);
                        newTransaction.setMixOrderObject(trsOrder);
                        OrderStore.getSharedInstance().addTransaction(newTransaction);
                        if ((newTransaction.getStatus() == TradeMeta.T39_Filled) ||
                                (newTransaction.getStatus() == TradeMeta.T39_Partially_Filled)) {
                            OrderDepthStore.getSharedInstance().updateRecord((Transaction) newTransaction.clone());
                        }
                        transaction = newTransaction;
                    }
                    /*if ((trsOrder.getText() != null) && (message.getType() == TradeMeta.MT_ORDER)) {
                        TradeMethods.getSharedInstance().showRejectReasonFull(transaction);
                    }*/

                    if ((trsOrder.getText() != null) && ((header.getRequestType() == MIXConstants.RESPONSE_TYPE_NEW_ORDER) || (header.getRequestType() == TradeMeta.MT_SLICED_ORDER))) {  //todo add sliced order
                        TradeMethods.getSharedInstance().showRejectReasonFull(transaction);
                    }

                    if ((header.getRequestType() == MIXConstants.RESPONSE_TYPE_NEW_ORDER) && ((trsOrder.getOrdStatus() == TradeMeta.T39_OMSACCEPTED)
                            || (trsOrder.getOrdStatus() == TradeMeta.T39_SEND_TO_EXCHANGE_NEW))) {
                        TradeMethods.getSharedInstance().showOrderAcceptedPopup(trsOrder);
                    }

                    if (!DataStore.getSharedInstance().isValidSymbol(SharedMethods.getKey(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()))) {
                        sendValidateSymbolRequest(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType());
                    }
//                    if (message.getType() == TradeMeta.MT_ORDER) {
//                        TransactionDialog.setOrderStatus(trsOrder);
//                    }
                    OrderStore.getSharedInstance().fireOrderStatusChanged(trsOrder.getMubasherOrderNumber() + "");
                    try {
                        TradeServer.write(transaction.getXML());
                    } catch (Exception e) {
                        //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                trsOrder = null;
            }
            OrderStore.getSharedInstance().applyFilter();
//            TradeMethods.getSharedInstance().sortBlotter();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void processPendingOrder(MIXObject baseMixObj, boolean conditional) {
        MIXHeader header = baseMixObj.getMIXHeader();
        TransferObject[] pendingOrders = baseMixObj.getTransferObjects();

        if (pendingOrders == null || pendingOrders.length == 0) {
            return;
        }
//        OrderInquiryResponse response = (OrderInquiryResponse) pendingOrders[0];
//
//        ArrayList<Order>  orders = response.getOrders();
//        if(orders==null || orders.size()==0) {
//            return;
//        }


        try {

            if (TradingShared.isBlotterRefreshPullOnly()) {
                OrderStore.getSharedInstance().clear();
            }


            for (int i = 0; i < pendingOrders.length; i++) {
                Order trsOrder = (Order) pendingOrders[i];
                /*conditional = trsOrder.getOrderCategory() == MIXConstants.ORDER_CATEGORY_CONDITIONAL;

                if (conditional && (trsOrder.getBehaviourList() != null && trsOrder.getBehaviourList().size() > 0) &&
                        (trsOrder.getBehaviourList().get(0).getConditionStatus() == TradeMeta.CONDITION_STATUS_TRIGGERED)) {
                    removeTransaction(trsOrder.getClOrdID() + "");
                    continue;
                }*/

                conditional = ((trsOrder.getOrderCategory() == MIXConstants.ORDER_CATEGORY_CONDITIONAL) || (trsOrder.getOrderCategory() == MIXConstants.ORDER_CATEGORY_ADVANCE));

                if (conditional && (trsOrder.getBehaviourList() != null)) {
                    boolean txnremoved = false;
                    for (ConditionalBehaviour behaviour : trsOrder.getBehaviourList()) {
                        if ((behaviour.getConditionCategory() == MIXConstants.CONDITION_CATEGORY_PRE_CONDITION) && (behaviour.getConditionStatus() == MIXConstants.CONDITION_STATUS_TRIGGERED)) {
                            removeTransaction(trsOrder.getClOrdID() + "");
                            txnremoved = true;
                            break;

                        }
                    }
                    if (txnremoved) {
                        continue;
                    }
                    txnremoved = false;
                }


                if ((trsOrder.getMubasherOrderNumber() > 0) || (trsOrder.getClOrdID() != null) || (trsOrder.getTwOrderID() != null)) {
                    Transaction transaction = null;
//                    int ORIGcl = -1;
//                    try {
//                        ORIGcl = Integer.parseInt(trsOrder.getOrigClOrdID());
//                    } catch (NumberFormatException e) {
//                        ORIGcl = -1;
//                    }

                    if (trsOrder.getOrigClOrdID() != null) {
                        transaction = OrderStore.getSharedInstance().searhTransactionByOrigClOrderID(trsOrder.getOrigClOrdID() + "");
                        if (transaction == null) {
                            transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(trsOrder.getOrigClOrdID() + "");
                        }
                    }

                    if ((transaction == null) && (trsOrder.getClOrdID() != null)) {
                        transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(trsOrder.getClOrdID() + "");
                    }

                    if ((transaction == null) && (trsOrder.getMubasherOrderNumber() > 0)) { // if not found, try the Mubasher Order ID
                        transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(trsOrder.getMubasherOrderNumber() + "");
                    }
                    if ((transaction == null) && (trsOrder.getTwOrderID() != null)) { // if not found, try the TW Order ID
                        transaction = OrderStore.getSharedInstance().searhTransactionByTWOrderID(trsOrder.getTwOrderID() + "", true);

                        try {
                            if (transaction != null && transaction.getStatus() != TradeMeta.T39_SENDING) { // issue with child orders of algo orders
                                transaction = null;
                            }
                        } catch (Exception e) {
                            transaction = null;
                        }

                    }
                    if (transaction != null) { // existing order - replace
                        int oldStatus = transaction.getStatus();
                        if ((oldStatus == TradeMeta.T39_Filled) || (oldStatus == TradeMeta.T39_Canceled)) {
                            Transaction newTransaction = new Transaction();
//   newTransaction.setOrderData(trsOrder);
                            newTransaction.setMixOrderObject(trsOrder);
                            if ((trsOrder.getText() != null) && ((header.getRequestType() == MIXConstants.RESPONSE_TYPE_NEW_ORDER) || (header.getRequestType() == TradeMeta.MT_SLICED_ORDER))) {    // todo add sliced order
                                TradeMethods.getSharedInstance().showRejectReasonFull(newTransaction);
                            } else if (transaction.getOrderCategory() == MIXConstants.ORDER_CATEGORY_ALGO) {     //added by udaya
                                transaction.setMixOrderObject(trsOrder);     // when algo order is cancled it does not get updated
                            }
                            continue;
                        }
                        //transaction.setOrderData(trsOrder);
                        transaction.setMixOrderObject(trsOrder);
                        if ((transaction.getStatus() == TradeMeta.T39_Filled) ||
                                (transaction.getStatus() == TradeMeta.T39_Partially_Filled)) {
                            OrderDepthStore.getSharedInstance().updateRecord((Transaction) transaction.clone());

                        }
                        if ((transaction.getStatus() == TradeMeta.T39_Filled) && (oldStatus != TradeMeta.T39_Filled)) {
                            showOrderExecutedMessage(transaction);
                        }
                    } else { // newly accepted order by back office - check for the empty order in store and replace
                        Transaction newTransaction = new Transaction();
//newTransaction.setOrderData(trsOrder);
                        newTransaction.setMixOrderObject(trsOrder);
                        OrderStore.getSharedInstance().addTransaction(newTransaction);
                        if ((newTransaction.getStatus() == TradeMeta.T39_Filled) ||
                                (newTransaction.getStatus() == TradeMeta.T39_Partially_Filled)) {
                            OrderDepthStore.getSharedInstance().updateRecord((Transaction) newTransaction.clone());
                        }
                        transaction = newTransaction;
                    }
                    /*if ((trsOrder.getText() != null) && (message.getType() == TradeMeta.MT_ORDER)) {
                        TradeMethods.getSharedInstance().showRejectReasonFull(transaction);
                    }*/

                    if ((trsOrder.getText() != null) && ((header.getRequestType() == MIXConstants.RESPONSE_TYPE_NEW_ORDER) || (header.getRequestType() == TradeMeta.MT_SLICED_ORDER))) {  //todo add sliced order
                        TradeMethods.getSharedInstance().showRejectReasonFull(transaction);
                    }

                    if ((header.getRequestType() == MIXConstants.RESPONSE_TYPE_NEW_ORDER) && ((trsOrder.getOrdStatus() == TradeMeta.T39_OMSACCEPTED)
                            || (trsOrder.getOrdStatus() == TradeMeta.T39_SEND_TO_EXCHANGE_NEW))) {
                        TradeMethods.getSharedInstance().showOrderAcceptedPopup(trsOrder);
                    }

                    if (!DataStore.getSharedInstance().isValidSymbol(SharedMethods.getKey(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()))) {
                        sendSymbolvalidationWithoutInstrument(transaction.getExchange(), transaction.getSymbol());
                    }
//                    if (message.getType() == TradeMeta.MT_ORDER) {
//                        TransactionDialog.setOrderStatus(trsOrder);
//                    }
                    OrderStore.getSharedInstance().fireOrderStatusChanged(trsOrder.getMubasherOrderNumber() + "");
                    try {
                        TradeServer.write(transaction.getXML());
                    } catch (Exception e) {
                        //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                trsOrder = null;
            }
            OrderStore.getSharedInstance().applyFilter();
//            TradeMethods.getSharedInstance().sortBlotter();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showOrderExecutedMessage(final Transaction transaction) {
        if (TradingShared.isShowOrderNotoficationPopup()) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {

                    Object[] message = new Object[2];

                    JCheckBox chkDoNotShow = new JCheckBox(Language.getString("DONOT_SHOW_DIALOG"));
                    chkDoNotShow.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            TradingShared.setShowOrderNotoficationPopup(false);
                        }
                    });
                    message[1] = chkDoNotShow;
                    message[0] = Language.getString("MSG_ORDER_EXECUTED");
                    message[0] = ((String) message[0]).replaceFirst("\\[NUMBER\\]", transaction.getMubasherOrderNumber());
                    message[0] = ((String) message[0]).replaceFirst("\\[SYMBOL\\]", transaction.getSymbol());
                    if (((String) message[0]).contains("[SHORT]")) {
                        Stock stk = DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType());
                        String shortD = "";
                        if (stk != null) {
                            shortD = stk.getShortDescription();
                        }
                        message[0] = ((String) message[0]).replaceFirst("\\[SHORT\\]", shortD);
                    }
                    if (((String) message[0]).contains("[LONG]")) {
                        Stock stk = DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType());
                        String shortD = "";
                        if (stk != null) {
                            shortD = stk.getLongDescription();
                        }
                        message[0] = ((String) message[0]).replaceFirst("\\[LONG\\]", shortD);
                    }
                    if (((String) message[0]).contains("[SYMCODE]")) {
                        Stock stk = DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType());
                        String shortD = "";
                        if (stk != null) {
                            shortD = stk.getSymbolCode();
                        }
                        message[0] = ((String) message[0]).replaceFirst("\\[SYMCODE\\]", shortD);
                    }

                    NativeMethods.play("sounds/traded.wav", 1);

                    ImageIcon icon = new ImageIcon("images/Theme" + Theme.getID() + "/traded.gif");
                    final JButton[] options = new JButton[2];
                    options[0] = new JButton(Language.getString("OK"));
                    options[1] = new JButton(Language.getString("TRADE_DETAILS"));
                    final JOptionPane optionPane = new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE,
                            JOptionPane.DEFAULT_OPTION, icon, options, options[0]);

                    final JDialog dialog = optionPane.createDialog(Client.getInstance().getFrame(), Language.getString("INFORMATION"));
                    dialog.setContentPane(optionPane);
                    dialog.setResizable(false);
                    dialog.pack();
                    dialog.setLocationRelativeTo(Client.getInstance().getFrame());

                    options[0].addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            dialog.dispose();
                        }
                    });

                    options[1].addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Transaction order = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(transaction.getMubasherOrderNumber());
                            TradeMethods.getSharedInstance().showDetailQuoteOrder(order);
                            order = null;
                            dialog.dispose();
                        }
                    });
                    options[0].requestFocus();
                    dialog.show();
                }
            });
        }
    }

    private void removeTransaction(String id) {
        if ((id != null) && (!id.equals(""))) {
            OrderStore.getSharedInstance().removeTransactionByClOrdID(id);
        }
    }

    private void sendValidateSymbolRequest(String exchange, String symbol, int instrumentType) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            String requestID = "OrderList" + ":" + exchange + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol, requestID, exchange, instrumentType);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
            System.out.println("Validate request sent for Order List");
        }
    }

    private void sendSymbolvalidationWithoutInstrument(String exchange, String symbol) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            String requestID = "OrderListWithoutInstrument" + ":" + exchange + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequestWithoutInstrumentType(symbol, requestID, exchange);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
            System.out.println("Validate request sent for Order List without Instrumrument Type");
        }
    }

    private void sendOpenPositionsValidation(String exchange, String symbol, int instrumentType) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            String requestID = "OpenPositions" + ":" + exchange + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol, requestID, exchange, instrumentType);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
            System.out.println("Validate request sent for Open positions");
        }
    }

    private void processSearchedOrder(String frame) {
        try {
            //SharedMethods.printLine(frame);
            TradeMessage message = new TradeMessage(frame);
            if (message.getType() == TradeMeta.MT_SEARCH) {
                int recordCount = 0;
                boolean hasMoreRecords = false;

                String messageData = message.getMessageData();
                String[] messageRecords = messageData.split(TradeMeta.DS);
                OrderSearchStore.getSharedInstance().setNewPage();
                for (int i = 0; i < messageRecords.length; i++) {
                    String messageRecord = messageRecords[i];
                    if (i == 0) {

                    } else if (i == 1) {
                        recordCount = Integer.parseInt(messageRecord);
                    } else if (i == 2) {
                        hasMoreRecords = Integer.parseInt(messageRecord) == 1;
                        OrderSearchStore.getSharedInstance().setCanGoNext(hasMoreRecords);
                    } else {
                        TRSOrder trsOrder = new TRSOrder();
                        trsOrder.parseMubasherMessage(messageRecord);
                        Transaction transaction = new Transaction();
                        transaction.setOrderData(trsOrder);
                        OrderSearchStore.getSharedInstance().addTransaction(transaction);
                        System.out.println("Seq " + transaction.getSequenceNumber());
                        if (i == 3) {
                            System.out.println("First Seq " + transaction.getSequenceNumber());
                            OrderSearchStore.getSharedInstance().setCurrentPageTop(transaction.getSequenceNumber());
                        } else if (i == messageRecords.length - 1) {
                            System.out.println("Last Seq " + transaction.getSequenceNumber());
                            OrderSearchStore.getSharedInstance().setCurrentPageBottom(transaction.getSequenceNumber());
                        }
                        trsOrder = null;
                        transaction = null;
                    }
                }
                if (recordCount == 0) {
                    new ShowMessage(Language.getString("TRADE_SEARCH_EMPTY"), "I");
                }
                message = null;
                OrderSearchWindow.setSearchInProgress(false);
            }
//            OrderSearchStore.getSharedInstance().applyFilter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void processSearchedOrder(MIXObject baseMixObject) {
        MIXHeader header = baseMixObject.getMIXHeader();
        TransferObject[] results = baseMixObject.getTransferObjects();
        if (results == null || results.length == 0) {
            new ShowMessage(Language.getString("TRADE_SEARCH_EMPTY"), "I");
            OrderSearchWindow.setSearchInProgress(false);
            return;
        }
        try {
            //SharedMethods.printLine(frame);
            OrderSearchStore.getSharedInstance().setNewPage();
            OrderInquiryResponse response = (OrderInquiryResponse) results[0];
            ArrayList<Order> orders = response.getOrders();
            if (orders == null) {
                new ShowMessage(Language.getString("TRADE_SEARCH_EMPTY"), "I");
                OrderSearchWindow.setSearchInProgress(false);
                return;
            }
            int recordCount = orders.size();
            for (int i = 0; i < orders.size(); i++) {
                Order serchRec = (Order) orders.get(i);

                Transaction transaction = new Transaction();
                transaction.setMixOrderObject(serchRec);
                OrderSearchStore.getSharedInstance().addTransaction(transaction);
                //   System.out.println("Seq " + transaction.getSequenceNumber());
                if (i == recordCount - 1) {
                    System.out.println("Last Seq " + transaction.getSequenceNumber());
                    OrderSearchStore.getSharedInstance().setCurrentPageBottom(transaction.getSequenceNumber());
                }
                transaction = null;

            }
            if (recordCount > 0) {
                OrderSearchStore.getSharedInstance().setCanGoNext(response.getPages().getIsNextPageAvailable() == 1);
                OrderSearchStore.getSharedInstance().setCurrentPageTop(Long.parseLong(response.getPages().getStartingSequenceNumber()));
                System.out.println("First Seq " + response.getPages().getStartingSequenceNumber());
            }
            if (recordCount == 0) {
                new ShowMessage(Language.getString("TRADE_SEARCH_EMPTY"), "I");
            }
            OrderSearchWindow.setSearchInProgress(false);
//            OrderSearchStore.getSharedInstance().applyFilter();
        } catch (Exception e) {
            e.printStackTrace();
            OrderSearchWindow.setSearchInProgress(false);
        }
    }

    private void processOrderDepth(String frame) {
        try {
            TradeMessage message = new TradeMessage(frame);
            String messageData = message.getMessageData();
            String[] messageRecords = messageData.split(TradeMeta.DS);

            for (int i = 0; i < messageRecords.length; i++) {
                TRSOrder trsOrder = new TRSOrder();
                trsOrder.parseMubasherMessage(messageRecords[i]);
                Transaction transaction = new Transaction();
                transaction.setOrderData(trsOrder);
                OrderDepthStore.getSharedInstance().addRecord(transaction);
                trsOrder = null;
                transaction = null;
            }
            message = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processOrderDepth(MIXObject orderDepth) {
        try {
            TransferObject[] orders = orderDepth.getTransferObjects();
            if (orders == null || orders.length == 0) {
                return;
            }
            OrderInquiryResponse response = (OrderInquiryResponse) orders[0];
            ArrayList<Order> orderdepth = response.getOrders();
            if (orderdepth == null || orderdepth.size() == 0) {
                return;
            }

            for (int i = 0; i < orderdepth.size(); i++) {
                Order serchRec = (Order) orderdepth.get(i);
                Transaction transaction = new Transaction();
                transaction.setMixOrderObject(serchRec);
                OrderDepthStore.getSharedInstance().addRecord(transaction);
                transaction = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processIntradayResponse(MIXObject intraday) {
        TransferObject[] orders = intraday.getTransferObjects();
        if (orders == null || orders.length == 0) {
            return;
        }

        OrderInquiryResponse response = (OrderInquiryResponse) orders[0];
        ArrayList<Order> orderdepth = response.getOrders();
        if (orderdepth == null || orderdepth.size() == 0) {
            return;
        }
        for (int i = 0; i < orderdepth.size(); i++) {
            Order order = orderdepth.get(i);
            PositionDataStore.getSharedInstance().addPositionData(order);
            int instrument = 0;
            try {
                instrument = Integer.parseInt(order.getSecurityType());
            } catch (NumberFormatException e) {
                instrument = 0;
            }
            if (!DataStore.getSharedInstance().isValidSymbol(SharedMethods.getKey(order.getSecurityExchange(), order.getSymbol(), instrument))) {
                sendSymbolvalidationWithoutInstrument(order.getSecurityExchange(), order.getSymbol());
            }
        }


    }


    private void processFutureBaseAttributes(String frame) {
        //46?13?0?0?DEVTRS?0?DGCX?DEUR?1000.0?100?DGCX?DG?1200.0?100?
        try {
            TradeMessage message = new TradeMessage(frame);
            String[] txnRecords = message.getMessageData().split(TradeMeta.DS);
            TransactRecord txn = null;

            String exchange = null;
            String symbol = null;
            double margin = 0;
            int maxContractSize = 0;
            int contractSize = 0;
            int instrument = Meta.INSTRUMENT_FUTURE;
            double priceCorrectioFactor = 0;


            for (int j = 0; j < txnRecords.length; j++) {
                try {
                    String[] txnData = txnRecords[j].split(TradeMeta.FD);
                    for (int i = 0; i < txnData.length; i++) {
                        if (txnData[i].startsWith("A")) {
                            exchange = txnData[i].substring(1);
                        } else if ((txnData[i].startsWith("B"))) {
                            symbol = txnData[i].substring(1);
                        } else if ((txnData[i].startsWith("C"))) {
                            margin = SharedMethods.doubleValue(txnData[i].substring(1));
                        } else if ((txnData[i].startsWith("D"))) {
                            maxContractSize = SharedMethods.intValue(txnData[i].substring(1));
                        } else if ((txnData[i].startsWith("E"))) {
                            contractSize = SharedMethods.intValue(txnData[i].substring(1));
                        } else if ((txnData[i].startsWith("F"))) {
                            priceCorrectioFactor = SharedMethods.doubleValue(txnData[i].substring(1));
                        } else if ((txnData[i].startsWith("F"))) {
                            instrument = SharedMethods.intValue(txnData[i].substring(1));
                            if (instrument < 0) {
                                instrument = Meta.INSTRUMENT_FUTURE;
                            }
                        }
                    }
                    symbol = SharedMethods.getSymbolFromKey(TradingShared.getPriceSymbol(exchange, symbol, instrument, null));
                    FutureBaseAttributes futureBaseAttributes = new FutureBaseAttributes(contractSize, margin, maxContractSize, priceCorrectioFactor);
                    FutureBaseStore.getSharedInstance().addAttribute(SharedMethods.getKey(exchange, symbol, instrument), futureBaseAttributes);

                    txn = null;
                    txnData = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            txnRecords = null;
            txn = null;
            message = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
//        "1=MIX_1.0\u001D2=3\u001D3=101\u001D7=TRS0112542228705844659269\u001D12=I000000\u0002\u001D2=TDWL\u001D3=1010\u001D8356=1010\u001D7=1025\u001D84=65.0"
//          "1=MIX_1.0\u001D2=2\u001D3=1\u001D7=TRS0112541198433988781792\u0002\u001D11=-9\u001D7067=0\u001D7069=25\u001D\u001E\u001D7003=100100\u001D7000=S000000090\u001D7060=20090901\u001D7061=20090928\u001D54=0\u001D7018=0\u001D55=*\u001D207=*\u001D11=-9\u001D39=X"
//        "MIX_1.0\u001D2=2\u001D3=1\u0002\u001D11=-9\u001D7067=0\u001D7069=25\u001D\u001E\u001D7003=100100\u001D7000=S000000090\u001D7060=20090902\u001D7061=20090928\u001D54=0\u001D7018=0\u001D55=*\u001D207=*\u001D11=-9\u001D39=X";
//        "MIX_1.0\u001D2=2\u001D3=1\u001D7=TRS0112541165319768102650\u0002\u001D11=-9\u001D7067=0\u001D7069=25\u001D\u001E\u001D7003=34986\u001D7000=S000008952\u001D7060=20090902\u001D7061=20090928\u001D54=0\u001D7018=0\u001D55=*\u001D207=*\u001D11=-9\u001D39=X"
    }
}
