package com.isi.csvr.trading.marginTrading;

import bsh.Interpreter;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.trading.datastore.MarginSymbolStore;
import com.isi.csvr.trading.datastore.Rule;
import com.isi.csvr.trading.datastore.RuleManager;
import com.isi.csvr.trading.portfolio.PfValutionRecord;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.TransactRecord;
import com.isi.csvr.trading.portfolio.ValuationRecord;
import com.isi.csvr.trading.shared.*;

import java.util.ArrayList;
import java.util.Hashtable;


/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 7, 2009
 * Time: 2:22:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarginCalculator implements TradingConnectionListener {

    private static MarginCalculator self;
    private static String TIF_ERROR_NORAML = "0";
    private static String TIF_ERROR_DAY = "1";
    private static String TIF_ERROR_NO_MARGIN = "2";
    private Hashtable<String, Boolean[]> tifMatrix;
    private String currency;

    private MarginCalculator() {
        tifMatrix = new Hashtable<String, Boolean[]>();
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static MarginCalculator getSharedInstance() {
        if (self == null) {
            self = new MarginCalculator();
        }
        return self;


    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    private Rule getRule(String id, String group) {
        try {
            Rule r = (Rule) (RuleManager.getSharedInstance().getRule(id, "*", group)).clone();
            return r;
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }

    }

//    public double getCoveragePctForAccSummary(String portfolio) {
//        if (portfolio != null) {
//            double coverageBenchMark = getCoverageBenchmark(TradingShared.getTrader().getPortfolio(portfolio).getApplicableMarginPct(false));
//            double coveragePF = calculateCoverageForAccountSummary(new String[]{portfolio});
//            if (!Double.isNaN(coverageBenchMark) && !Double.isNaN(coveragePF) && coveragePF > 0) {
//                return ((coveragePF - coverageBenchMark) / (coveragePF));
//            } else {
//                return Double.NaN;
//            }
//        } else {
//            return Double.NaN;
//        }
//    }

    public synchronized double getCoveragePctForNewOrder(String portfolio, double netval, double nwOrderMargin, String sKey, boolean isErrmsg) {
        if (!isErrmsg) {
            if (portfolio != null) {
                double coverageBenchMark = getCoverageBenchmarkNewOrder(TradingShared.getTrader().getPortfolio(portfolio).getApplicableMarginPct(getexchange(sKey)));
                double coveragePF = getCoverageWithNewOrder(portfolio, netval, sKey, nwOrderMargin);
                if (!Double.isNaN(coverageBenchMark) && !Double.isNaN(coveragePF) && coveragePF > 0) {
                    return ((coveragePF - coverageBenchMark) / (coveragePF));
                } else {
                    return Double.NaN;
                }
            } else {
                return Double.NaN;
            }
        } else {
            if (portfolio != null) {
                double coveragePF = getCoverageWithNewOrder(portfolio, netval, sKey, nwOrderMargin);
                if (!Double.isNaN(coveragePF) && coveragePF > 0) {
                    return ((coveragePF));
                } else {
                    return Double.NaN;
                }
            } else {
                return Double.NaN;
            }
        }
    }

    public double[] getCoveragevaluesForNewOrder(String portfolio, double netval, double nwOrderMargin, String sKey) {
        double[] values = new double[2];
        values[0] = 0.00;
        values[1] = 0.00;
        if (portfolio != null) {
            double coverageBenchMark = getCoverageBenchmarkNewOrder(TradingShared.getTrader().getPortfolio(portfolio).getApplicableMarginPct(getexchange(sKey)));
            double coveragePF = getCoverageWithNewOrder(portfolio, netval, sKey, nwOrderMargin);
            if (!Double.isNaN(coverageBenchMark) && !Double.isNaN(coveragePF) && coveragePF > 0) {
                values[0] = coverageBenchMark;
                values[1] = coveragePF;
                return values;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public double[] getCoveragevaluesForAccSummary(String portfolio, String refExchange) {
        double[] values = new double[2];
        values[0] = 0.00;
        values[1] = 0.00;
        if (portfolio != null) {
            double coverageBenchMark = getCoverageBenchmark(TradingShared.getTrader().getPortfolio(portfolio).getApplicableMarginPct(refExchange));
            double coveragePF = calculateCoverageForAccountSummary(new String[]{portfolio});
            if (!Double.isNaN(coverageBenchMark) && !Double.isNaN(coveragePF) && coveragePF > 0) {
                values[0] = coverageBenchMark;
                values[1] = coveragePF;
                return values;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public double getMarginUtilized(String portfolio) {
        TradingPortfolioRecord record = getTradingPortfolio(portfolio);
        double utilized = 0.00;
        if (record != null) {
            Account account = TradingShared.getTrader().getAccoutOfPortfolio(record.getPortfolioID());
            double margindue = account.getTotMarginDue();
            margindue = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, margindue, record.getPortfolioID());
            double marginBlock = (-account.getTotMarginBlock());// todo
            marginBlock = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, marginBlock, record.getPortfolioID());

            double calcMargin = calculateNormalMargin(new String[]{portfolio}, currency);
            double calcMarginDay = calculateDayMargin(new String[]{portfolio}, currency);

            Rule rule = getRule("MARGIN_UTILIZED", "ACCOUNT_SUMMARY");
            if (rule != null && (calcMargin + calcMarginDay) > 0) {
                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("totMarginBlock", marginBlock);
                    interpreter.set("totMarginDue", margindue);
                    interpreter.set("calcMargin", calcMargin);
                    interpreter.set("calcDayMargin", calcMarginDay);
                    utilized = (Double) interpreter.eval(rule.getRule());
                    if (utilized > 1) {
                        utilized = 1;
                    }

                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    utilized = 0.00;
                }
            } else {
                if (account.getBalance() == 0.00 && getMarketValue(portfolio) == 0.0) {
                    utilized = 0.00;
                } else {
                    utilized = 1;
                }
            }
        }
        return utilized;
    }

    private double getMarketValue(String portfolioID) {
        double mValue = 0d;
        try {
            Stock stock = null;
            TransactRecord record = null;
            ArrayList dataStore = TradePortfolios.getInstance().getTransactionList();
            for (int i = 0; i < dataStore.size(); i++) {
                record = (TransactRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getKey().getExchange(), record.getKey().getSymbol());
                if (record.getPfID().equals(portfolioID)) {
                    try {
                        if (stock.getLastTradeValue() > 0) {
                            mValue += Math.abs(record.getQuantity()) * PortfolioInterface.getLastTrade(stock);
                        } else {
                            mValue += Math.abs(record.getQuantity()) * PortfolioInterface.getPreviousClosed(stock);
                        }
                    } catch (Exception e) {
                        //do nothing  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                record = null;
                stock = null;
            }
            return mValue;
        } catch (Exception e) {
            return mValue;//To change body of catch statement use File | Settings | File Templates.
        }
    }

    public double calculateCoverageForAccountSummary(String[] portfolios) {
        if (portfolios != null && portfolios.length > 0) {
            double RAPV = 0;
            double RAPVPending = 0;
            double loanblock = 0;
            double loandue = 0;
            for (int i = 0; i < portfolios.length; i++) {
                String pfId = portfolios[i];
                Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
                RAPV = RAPV + getRAPVPprtfolio(pfId);
                RAPVPending = RAPVPending + getRAPVPendinOrders(pfId);
                loanblock = loanblock + (-account.getTotMarginBlock());
                loanblock = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, loanblock, pfId);
                loandue = loandue + account.getTotMarginDue();
                loandue = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, loandue, pfId);
            }
            Rule rule = getRule("MARGIN_COVERAGE", "ACCOUNT_SUMMARY");
            if (rule != null) {
                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("RAPV", RAPV);
                    interpreter.set("RAPVPending", RAPVPending);
                    interpreter.set("totMarginBlock", loanblock);
                    interpreter.set("totMarginDue", loandue);
                    double result = (Double) interpreter.eval(rule.getRule());
                    return result;
                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    return Double.NaN;
                }
            }
        }
        return Double.NaN;

    }


    public double getCoverageBenchmark(double margin) {
        Rule rule = getRule("MARGIN_COVERAGE_BM", "ACCOUNT_SUMMARY");
        if (rule != null) {
            Interpreter interpreter = new Interpreter();
            try {
                interpreter.set("margin", margin);
                double result = (Double) interpreter.eval(rule.getRule());
                return result;
            } catch (Exception evalError) {
                evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return Double.NaN;
            }
        }
        return Double.NaN;
    }

    public double getCoverageBenchmarkNewOrder(double margin) {
        Rule rule = getRule("MARGIN_COVERAGE_BM", "ACCOUNT_SUMMARY");
        if (rule != null) {
            Interpreter interpreter = new Interpreter();
            try {
                interpreter.set("margin", margin);
                double result = (Double) interpreter.eval(rule.getRule());
                return result;
            } catch (Exception evalError) {
                evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return Double.NaN;
            }
        }
        return Double.NaN;
    }

    public synchronized double getRAPVPprtfolio(String pfId) {
        double rapv = 0.00;
        if (getTradingPortfolio(pfId) != null) {

            // TradingPortfolioRecord records = getTradingPortfolio(pfId);
            ArrayList<ValuationRecord> valuationRecords = getValuationFilteredList(pfId);
            if (valuationRecords != null && !valuationRecords.isEmpty()) {
                for (int i = 0; i < valuationRecords.size(); i++) {
                    ValuationRecord record = valuationRecords.get(i);
                    if (isSymbolMarginable(record.getSKey())) {
                        Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(record.getSKey().getExchange(), record.getSKey().getSymbol());
                        double pct = getSymbolMarginPct(record.getSKey());
                        if (stock.getLastTradeValue() > 0) {
                            double val = (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock) +
                                    record.getCashDividends()) * pct;
                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, record.getPortfolioID());
                            rapv = rapv + val;
                        } else {
                            double val = (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock) +
                                    record.getCashDividends()) * pct;
                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, record.getPortfolioID());
                            rapv = rapv + val;
                        }
                    }

                }
            }
        }
        return rapv;
    }

    public synchronized double getRAPVPprtfolioForNewOrder(String pfId, String target) {
        double rapv = 0.00;
        if (getTradingPortfolio(pfId) != null) {

            // TradingPortfolioRecord records = getTradingPortfolio(pfId);
            ArrayList<ValuationRecord> valuationRecords = getValuationFilteredList(pfId);
            if (valuationRecords != null && !valuationRecords.isEmpty()) {
                for (int i = 0; i < valuationRecords.size(); i++) {
                    ValuationRecord record = valuationRecords.get(i);
                    if (isSymbolMarginable(record.getSKey())) {
                        Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(record.getSKey().getExchange(), record.getSKey().getSymbol());
                        double pct = getSymbolMarginPct(record.getSKey());
                        if (stock.getLastTradeValue() > 0) {
                            double val = (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock) +
                                    record.getCashDividends()) * pct;
                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), target, true, val, record.getPortfolioID());
                            rapv = rapv + val;
                        } else {
                            double val = (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock) +
                                    record.getCashDividends()) * pct;
                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), target, true, val, record.getPortfolioID());
                            rapv = rapv + val;

                        }
                    }

                }


            }


        }
        return rapv;
    }

    public synchronized double getRAPVPendinOrders(String pfId) {
        double RAPVPending = 0.00;
        if (getTradingPortfolio(pfId) != null) {
//            ArrayList<Transaction> pendinglist = OrderStore.getSharedInstance().getFilteredList("1", "OPEN", "*", pfId);
            ArrayList<TransactRecord> pendinglist = TradePortfolios.getInstance().getTransactionRecordsForPortfolio(pfId);
            if (pendinglist != null && pendinglist.size() > 0) {
                for (int i = 0; i < pendinglist.size(); i++) {
                    TransactRecord txn = pendinglist.get(i);
                    if (txn.getPendingBuy() <= 0) {
                        continue;
                    }
                    if (isSymbolMarginable(txn.getKey())) {
                        Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(txn.getKey().getExchange(), txn.getKey().getSymbol());
                        double pct = getSymbolMarginPct(txn.getKey());
                        // if (stock.getLastTradeValue() > 0)
//                        if (txn.getPrice() > 0) {
//                            double val = (txn.getPendingBuy() * txn.getPrice() * pct);
//                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, pfId);
//                            RAPVPending = RAPVPending + val;
//                        }
//                        else
                        if (stock.getLastTradeValue() > 0) {
                            double val = (txn.getPendingBuy() * PortfolioInterface.getLastTrade(stock)) * pct;
                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, pfId);
                            RAPVPending = RAPVPending + val;
                        } else {
                            double val = (txn.getPendingBuy() * PortfolioInterface.getPreviousClosed(stock)) * pct;
                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, pfId);
                            RAPVPending = RAPVPending + val;
                        }
                    }
                }
            }

        }
        return RAPVPending;
    }

    public synchronized double getRAPVPendinOrdersForNewOrder(String pfId, String target) {
        double RAPVPending = 0.00;
        if (getTradingPortfolio(pfId) != null) {
//            ArrayList<Transaction> pendinglist = OrderStore.getSharedInstance().getFilteredList("1", "OPEN", "*", pfId);
            ArrayList<TransactRecord> pendinglist = TradePortfolios.getInstance().getTransactionRecordsForPortfolio(pfId);
            if (pendinglist != null && pendinglist.size() > 0) {
                for (int i = 0; i < pendinglist.size(); i++) {
                    TransactRecord txn = pendinglist.get(i);
                    if (isSymbolMarginable(txn.getKey())) {
                        Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(txn.getKey().getExchange(), txn.getKey().getSymbol());
                        double pct = getSymbolMarginPct(txn.getKey());
//                        if (txn.getPrice() > 0) {
//                            double val = (txn.getPendingBuy() * txn.getPrice() * pct);
//                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), target, true, val, pfId);
//                            RAPVPending = RAPVPending + val;
//                        }
//                        else
                        if (stock.getLastTradeValue() > 0) {
                            double val = (txn.getPendingBuy() * PortfolioInterface.getLastTrade(stock)) * pct;
                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), target, true, val, pfId);
                            RAPVPending = RAPVPending + val;
                        } else {
                            double val = (txn.getPendingBuy() * PortfolioInterface.getPreviousClosed(stock)) * pct;
                            val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), target, true, val, pfId);
                            RAPVPending = RAPVPending + val;
                        }

                    }
                }
            }

        }
        return RAPVPending;
    }

    private ArrayList<ValuationRecord> getValuationFilteredList(String pfID) {
        ArrayList<ValuationRecord> records = new ArrayList<ValuationRecord>();
        ArrayList<PfValutionRecord> list = TradePortfolios.getInstance().getPfValuationList();
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                PfValutionRecord record = list.get(i);
                if (record.getRecType() == PfValutionRecord.VALRECORD && record.getValRecord() != null && record.getValRecord().getPortfolioID().equalsIgnoreCase(pfID)) {
                    records.add(record.getValRecord());
                }

            }


        }
        return records;

    }

    public synchronized double calculateNormalMargin(String[] portfolios, String currency) {
        double normalMargin = 0.00;
        if (portfolios != null && portfolios.length > 0) {
            for (int i = 0; i < portfolios.length; i++) {
                TradingPortfolioRecord record = getTradingPortfolio(portfolios[i]);
                if (record.isMarginEnabled()) {
                    double max = record.getMaxMarginAmt();
                    Account account = TradingShared.getTrader().getAccoutOfPortfolio(record.getPortfolioID());
                    double cash = account.getBalance();
                    cash = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, cash, record.getPortfolioID());
                    max = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, max, record.getPortfolioID());
                    double nonBlock = -account.getNonMarginableCashBlock();
                    nonBlock = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, nonBlock, record.getPortfolioID());
                    double rapv = getRAPVPprtfolio(record.getPortfolioID());
                    Rule rule = getRule("MARGIN_CALCULATION", "ACCOUNT_SUMMARY");
                    if (rule != null) {
                        Interpreter interpreter = new Interpreter();
                        try {
                            interpreter.set("RAPV", rapv);
                            interpreter.set("cashBal", cash);
                            interpreter.set("NonMarginCashBlock", nonBlock);
                            interpreter.set("margin", record.getMarginPct());
                            double result = (Double) interpreter.eval(rule.getRule());
                            if (result < 0) {
                                result = 0;
                            }
                            double margin = Math.min(max, result);
                            normalMargin = normalMargin + margin;
                        } catch (Exception evalError) {
                            evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            // return Double.NaN;
                        }
                    }

                }

            }
        }

        return normalMargin;

    }

    public synchronized double calculateDayMargin(String[] portfolios, String currency) {
        double normalMargin = 0.00;
        if (portfolios != null && portfolios.length > 0) {
            for (int i = 0; i < portfolios.length; i++) {
                TradingPortfolioRecord record = getTradingPortfolio(portfolios[i]);
                if (record.isDayMarginEnabled()) {
                    double max = record.getMaxDayMarginAmt();
                    Account account = TradingShared.getTrader().getAccoutOfPortfolio(record.getPortfolioID());
                    double cash = account.getBalance();
                    cash = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, cash, record.getPortfolioID());
                    max = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, max, record.getPortfolioID());
                    double nonBlock = -account.getNonMarginableCashBlock();
                    nonBlock = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, nonBlock, record.getPortfolioID());
                    double rapv = getRAPVPprtfolio(record.getPortfolioID());
                    Rule rule = getRule("MARGIN_CALCULATION", "ACCOUNT_SUMMARY");
                    double substraction = 0;
                    if (record.isMarginEnabled()) {
                        substraction = calculateNormalMargin(new String[]{record.getPortfolioID()}, currency);
                    }
                    if (rule != null) {
                        Interpreter interpreter = new Interpreter();
                        try {
                            interpreter.set("RAPV", rapv);
                            interpreter.set("cashBal", cash);
                            interpreter.set("NonMarginCashBlock", nonBlock);
                            interpreter.set("margin", record.getDayMarginPct());
                            double result = (Double) interpreter.eval(rule.getRule());
                            result = result - substraction;
                            if (result < 0) {
                                result = 0;
                            }
                            double margin = Math.min(result, max);
                            normalMargin = normalMargin + margin;
                        } catch (Exception evalError) {
                            evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            // return Double.NaN;
                        }
                    }

                }

            }
        }
        return normalMargin;

    }

    public double[] getTotalMarginDueMarginBlock(String[] portfolios, String currency) {
        double marginDue = 0;
        double daymarginDue = 0;
        double marginBlock = 0;
        double daymarginBlock = 0;

        double[] allocation = new double[]{marginDue, marginBlock, daymarginDue, daymarginBlock};
        if (portfolios != null && portfolios.length > 0) {
            for (int i = 0; i < portfolios.length; i++) {
                TradingPortfolioRecord record = getTradingPortfolio(portfolios[i]);
                if (record.isMarginable()) {
                    double[] values = allocateTotalMarginDueMarginBlock(record.getPortfolioID(), currency);
                    allocation[0] = allocation[0] + values[0];
                    allocation[1] = allocation[1] + values[1];
                    allocation[2] = allocation[2] + values[2];
                    allocation[3] = allocation[3] + values[3];
                }
            }
        }
        return allocation;
    }

    public double[] allocateTotalMarginDueMarginBlock(String pfId, String targetCurr) {
        Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
        TradingPortfolioRecord record = getTradingPortfolio(pfId);
        double marginDue = 0;
        double daymarginDue = 0;
        double marginBlock = 0;
        double daymarginBlock = 0;
        String curr = "";

        double[] allocation = new double[]{marginDue, marginBlock, daymarginDue, daymarginBlock};

        if (account != null) {
            double totMargindue = account.getTotMarginDue();
            double totMarginBlock = -account.getTotMarginBlock();  //todo
            curr = account.getCurrency();
            //  if (record.isMarginable() && record.isDayMarginEnabled()) {
            if (record.isMarginEnabled() && record.isDayMarginEnabled()) {
                double normalMargin = calculateNormalMargin(new String[]{pfId}, account.getCurrency());
                double dayMargin = calculateDayMargin(new String[]{pfId}, account.getCurrency());
                if (normalMargin == 0 && dayMargin == 0) {
                    daymarginDue = totMargindue;
                    daymarginBlock = totMarginBlock;
                } else {
                    if ((normalMargin - totMargindue) > 0) {
                        marginDue = totMargindue;
                        daymarginDue = 0.00;
                        normalMargin = normalMargin - totMargindue;
                    } else {
                        marginDue = normalMargin;
                        daymarginDue = totMargindue - normalMargin;
                        normalMargin = 0.00;
                        dayMargin = dayMargin - daymarginDue;
                    }

                    if ((normalMargin - totMarginBlock) > 0) {
                        marginBlock = totMarginBlock;
                        daymarginBlock = 0.00;
                        normalMargin = normalMargin - totMarginBlock;
                    } else if (normalMargin > 0) {
                        marginBlock = normalMargin;
                        daymarginBlock = totMarginBlock - marginBlock;
                        dayMargin = dayMargin - daymarginBlock;
                        normalMargin = 0.00;
                    } else if (normalMargin <= 0) {
                        daymarginBlock = totMarginBlock;
                        dayMargin = dayMargin - totMarginBlock;
                    }
                }

            } else if (record.isMarginEnabled()) {
                double normalMargin = calculateNormalMargin(new String[]{pfId}, account.getCurrency());
                if (normalMargin == 0) {
                    marginDue = totMargindue;
                    marginBlock = totMarginBlock;
                } else {
                    marginDue = totMargindue;
                    marginBlock = totMarginBlock;
                    normalMargin = normalMargin - totMargindue - totMarginBlock;
                }
            } else if (record.isDayMarginEnabled()) {
                double dayMargin = calculateDayMargin(new String[]{pfId}, account.getCurrency());
                if (dayMargin == 0) {
                    daymarginDue = totMargindue;
                    daymarginBlock = totMarginBlock;
                } else {
                    daymarginDue = totMargindue;
                    daymarginBlock = totMarginBlock;
                    dayMargin = dayMargin - totMargindue - totMarginBlock;
                }
            }


        }
        allocation[0] = CurrencyStore.getAdjustedPriceForBuy(curr, targetCurr, marginDue, pfId);
        allocation[1] = CurrencyStore.getAdjustedPriceForBuy(curr, targetCurr, -marginBlock, pfId);
        allocation[2] = CurrencyStore.getAdjustedPriceForBuy(curr, targetCurr, daymarginDue, pfId);
        allocation[3] = CurrencyStore.getAdjustedPriceForBuy(curr, targetCurr, -daymarginBlock, pfId);
        return allocation;

    }

    public synchronized double getBuyingPower(String pfId, String sKey) {
        Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
        TradingPortfolioRecord record = getTradingPortfolio(pfId);
        if (!record.isMarginable() || record.getApplicableMarginPct(TradeMethods.getReferenceMarginExchangeForAccSummary()) <= 0) {
            double pwr = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, account.getBuyingPower(), pfId);
            return pwr;
        }
        double buyingpower = 0.00;
        if (account != null) {
            double cash = account.getBalance() + account.getBlockedAmount(); // block amt negative
            if (cash < 0) {
                cash = 0;
            }
            cash = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, cash, pfId);
            double loan = (-account.getTotMarginBlock()) + account.getTotMarginDue();
            loan = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, loan, pfId);
            double rapv = getRAPVPprtfolio(pfId);
            double rapvpending = getRAPVPendinOrders(pfId);
            double pct = 1;
            if (sKey.equals("*")) {
                pct = 1;
            } else {
                pct = getSymbolMarginPct(sKey);
            }

            Rule rule = getRule("MARGIN_BUYING_PWR", "ACCOUNT_SUMMARY");
            if (rule != null) {
                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("RAPV", rapv);
                    interpreter.set("RAPVPending", rapvpending);
                    interpreter.set("cash", cash);
                    interpreter.set("loan", loan);
//                    interpreter.set("margin", record.getApplicableMarginPct(getexchange(sKey)));
                    interpreter.set("margin", record.getApplicableMarginPct(TradeMethods.getReferenceMarginExchangeForAccSummary()));
//                    interpreter.set("symMarginPct", pct);
                    buyingpower = (Double) interpreter.eval(rule.getRule());

                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    // return Double.NaN;
                }
            }

        }
        if (Double.isInfinite(buyingpower) || Double.isNaN(buyingpower)) {
            buyingpower = 0;
        }
        //   "return ( (((1-margin)*(RAPV + RAPVPending))+cash-(loan +(commission*symMarginPct*(1-margin))))/(1- (symMarginPct*(1-margin))));"
        return buyingpower;
    }


    public synchronized double getBuyingPowerForNewOrder(String pfId, String sKey, double comission) {
        Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
        TradingPortfolioRecord record = getTradingPortfolio(pfId);
        if (!record.isMarginable()) {
            return account.getBuyingPower();
        }
        double buyingpower = 0.00;
        boolean isMarketClose = false;
        double applicableMargin = 0;
        if (account != null) {
            double cash = account.getBalance() + account.getBlockedAmount(); // block amt negative
            if (cash < 0) {
                cash = 0;
            }
            double loan = (-account.getTotMarginBlock()) + account.getTotMarginDue();
            // loan = CurrencyStore.getAdjustedPriceForBuy(account.getCurrency(), currency, loan, pfId);
            double rapv = getRAPVPprtfolioForNewOrder(pfId, account.getCurrency());
            double rapvpending = getRAPVPendinOrdersForNewOrder(pfId, account.getCurrency());
            double pct = 1;
            if (sKey.equals("*")) {
                pct = 1;
            } else {
                pct = getSymbolMarginPct(sKey);
            }
            Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
            comission = convertToSelectedCurrency(stock.getExchange(), stock.getCurrencyCode(), account.getCurrency(), true, comission, pfId);
//            try {
//                Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
//                getexchange= (ex.getMarketStatus()== Meta.MARKET_CLOSE);
//                applicableMargin = record.getApplicableMarginPct(getexchange);
//            } catch (Exception e) {
//                applicableMargin = record.getApplicableMarginPct();
//
//            }
            Rule rule = getRule("MARGIN_BUYING_PWR", "NEW_ORDER");
            if (rule != null) {
                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("RAPV", rapv);
                    interpreter.set("RAPVPending", rapvpending);
                    interpreter.set("cash", cash);
                    interpreter.set("loan", loan);
                    interpreter.set("margin", record.getApplicableMarginPct(getexchange(sKey)));
                    interpreter.set("symMarginPct", pct);
                    interpreter.set("commission", comission);
                    buyingpower = (Double) interpreter.eval(rule.getRule());
//                    buyingpower = (Double) interpreter.eval("return ( (((1-margin)*(RAPV + RAPVPending))+cash-(loan +(commission*symMarginPct*(1-margin))))/(1- (symMarginPct*(1-margin))));");

                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    // return Double.NaN;
                }
            }

        }
        if (Double.isInfinite(buyingpower) || Double.isNaN(buyingpower)) {
            buyingpower = 0;
        }

        return buyingpower;
    }

    public boolean getCoverageWarning(String pfId, double ordervalue, String sKey, double nwOrderMargin) {
        Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
        TradingPortfolioRecord record = getTradingPortfolio(pfId);
        Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
        boolean canbecovered = true;
        double pct = getSymbolMarginPct(sKey);
        ordervalue = convertToSelectedCurrency(stock.getExchange(), stock.getCurrencyCode(), account.getCurrency(), true, ordervalue, pfId);
        nwOrderMargin = convertToSelectedCurrency(stock.getExchange(), stock.getCurrencyCode(), account.getCurrency(), true, nwOrderMargin, pfId);
        double rapvadj = getRAPVPprtfolioForNewOrder(pfId, account.getCurrency()) + (ordervalue * pct);
        double rapvpending = getRAPVPendinOrdersForNewOrder(pfId, account.getCurrency());
        double totblock = (-account.getTotMarginBlock());
        double totdue = account.getTotMarginDue();

        Rule rule = getRule("MARGIN_COVERAGE", "NEW_ORDER");
        if (rule != null) {
            Interpreter interpreter = new Interpreter();
            try {
                interpreter.set("RAPVadj", rapvadj);
                interpreter.set("RAPVPending", rapvpending);
                interpreter.set("totMarginBlock", totblock);
                interpreter.set("totMarginDue", totdue);
                interpreter.set("margin", record.getApplicableMarginPct(getexchange(sKey)));
                interpreter.set("nwOrderMargin", nwOrderMargin);
                canbecovered = (Integer) interpreter.eval(rule.getRule()) == 1;

            } catch (Exception evalError) {
                evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                // return Double.NaN;
            }
        }
        return !canbecovered;
    }

    public synchronized double getCoverageWithNewOrder(String portfolios, double ordervalue, String sKey, double nwOrderMargin) {
        if (portfolios != null && !portfolios.trim().isEmpty()) {
            double RAPV = 0;
            double RAPVPending = 0;
            double loanblock = 0;
            double loandue = 0;
            double pct = getSymbolMarginPct(sKey);
            String pfId = portfolios;
            Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
            Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
            ordervalue = convertToSelectedCurrency(stock.getExchange(), stock.getCurrencyCode(), account.getCurrency(), true, ordervalue, pfId);
            nwOrderMargin = convertToSelectedCurrency(stock.getExchange(), stock.getCurrencyCode(), account.getCurrency(), true, nwOrderMargin, pfId);
            RAPV = getRAPVPprtfolioForNewOrder(pfId, account.getCurrency()) + (ordervalue * pct);
            RAPVPending = getRAPVPendinOrdersForNewOrder(pfId, account.getCurrency());
            loanblock = (-account.getTotMarginBlock());
            loandue = account.getTotMarginDue() + nwOrderMargin;

            Rule rule = getRule("MARGIN_COVERAGE", "ACCOUNT_SUMMARY");
            if (rule != null) {
                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("RAPV", RAPV);
                    interpreter.set("RAPVPending", RAPVPending);
                    interpreter.set("totMarginBlock", loanblock);
                    interpreter.set("totMarginDue", loandue);
                    double result = (Double) interpreter.eval(rule.getRule());
                    return result;
                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    return Double.NaN;
                }
            }
        }
        return Double.NaN;

    }

    public double getAdjustedQty(double coverage, double cash, double loan, double commission, double orderPrice, String sKey, String portfolios) {
        double RAPV = 0;
        double RAPVPending = 0;
        double pct = getSymbolMarginPct(sKey);
        String pfId = portfolios;
        Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
        Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
        orderPrice = convertToSelectedCurrency(stock.getExchange(), stock.getCurrencyCode(), account.getCurrency(), true, orderPrice, pfId);
        commission = convertToSelectedCurrency(stock.getExchange(), stock.getCurrencyCode(), account.getCurrency(), true, commission, pfId);
        RAPV = getRAPVPprtfolioForNewOrder(pfId, account.getCurrency());
        RAPVPending = getRAPVPendinOrdersForNewOrder(pfId, account.getCurrency());
        double positiveCashBal = Math.max(0.00, account.getBalance());
        Rule rule = getRule("MARGIN_ADJ_QTY", "NEW_ORDER");
//         Rule rule = getRule("MARGIN_COVERAGE", "ACCOUNT_SUMMARY");
        if (rule != null) {
            Interpreter interpreter = new Interpreter();
            try {
                interpreter.set("RAPV", RAPV);
                interpreter.set("RAPVPending", RAPVPending);
                interpreter.set("cash", cash);
                interpreter.set("coverage", coverage);
                interpreter.set("loan", loan);
                interpreter.set("symMarginPct", pct);
                interpreter.set("commission", commission);
                interpreter.set("orderPrice", orderPrice);
                interpreter.set("positiveCashBal", cash);
//                  String a=  " (cash)+ ( ( (RAPV+RAPVPending)-(coverage*loan) )/(coverage-symMarginPct) )";
//                  String a=  "( (cash)+ ( ( (RAPV+RAPVPending+positiveCashBal)-(coverage*loan) )/(coverage-symMarginPct) ) - ((coverage*commission)/(coverage-symMarginPct) )    )/(orderPrice)";
//                    double Xmax = (Double) interpreter.eval(a);
                double result = (Double) interpreter.eval(rule.getRule());

                return result;
            } catch (Exception evalError) {
                evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return 0.00;
            }
        }

        return 0.00;
    }

    public double getMaxBuyingpower(double coverage, double cash, double loan, double commission, double orderPrice, String sKey, String portfolios) {
        double RAPV = 0;
        double RAPVPending = 0;
        double pct = getSymbolMarginPct(sKey);
        String pfId = portfolios;
        Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
        Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
        orderPrice = convertToSelectedCurrency(stock.getExchange(), stock.getCurrencyCode(), account.getCurrency(), true, orderPrice, pfId);
        RAPV = getRAPVPprtfolioForNewOrder(pfId, account.getCurrency());
        RAPVPending = getRAPVPendinOrdersForNewOrder(pfId, account.getCurrency());
        double positiveCashBal = Math.max(0.00, account.getBalance());

        Rule rule = getRule("MARGIN_MAX_BP", "NEW_ORDER");
//         Rule rule = getRule("MARGIN_COVERAGE", "ACCOUNT_SUMMARY");
        if (rule != null) {
            Interpreter interpreter = new Interpreter();
            try {
                interpreter.set("RAPV", RAPV);
                interpreter.set("RAPVPending", RAPVPending);
                interpreter.set("cash", cash);
                interpreter.set("coverage", coverage);
                interpreter.set("loan", loan);
                interpreter.set("symMarginPct", pct);
                interpreter.set("commission", commission);
                interpreter.set("orderPrice", orderPrice);
                interpreter.set("positiveCashBal", cash);
//                     String a=  " (cash)+ ( ( (RAPV+RAPVPending+(positiveCashBal*symMarginPct))-(coverage*loan) )/(coverage-symMarginPct) )";
//                  String a=  "( (cash)+ ( ( (RAPV+RAPVPending)-(coverage*loan) )/(coverage-symMarginPct) ) - ((coverage*commission)/(coverage-symMarginPct) )    )/(orderPrice)";
                double Xmax = (Double) interpreter.eval(rule.getRule());
//                       double Xmax = (Double) interpreter.eval(a);

                return Xmax;
            } catch (Exception evalError) {
                evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return Double.NaN;
            }
        }

        return 0.00;
    }


    public double getLiquidationAmount(String[] selPF) {
        double liq = 0.00;
        if (selPF != null && selPF.length > 0) {
            for (int i = 0; i < selPF.length; i++) {
                TradingPortfolioRecord record = getTradingPortfolio(selPF[i]);
                if (record.isMarginable()) {
                    liq = liq + calculateLiquadationAmount(selPF[i]);
                }
            }
        }
        return liq;
    }

    public double getTopUPamount(String[] selPF) {
        double topup = 0.00;
        if (selPF != null && selPF.length > 0) {
            for (int i = 0; i < selPF.length; i++) {
                TradingPortfolioRecord record = getTradingPortfolio(selPF[i]);
                if (record.isMarginable()) {
                    topup = topup + calculateTopUPamount(selPF[i]);
                }
            }
        }
        return topup;
    }

    private double calculateTopUPamount(String pfId) {
        // (loan- marketValue + (margin*marketValue))     
        Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
        TradingPortfolioRecord record = getTradingPortfolio(pfId);
        double topup = 0.00;
        double marketValue = 0.00;
        double margin = 0.00;
        double loan = 0.00;
        if (account != null && record != null && record.isMarginEnabled()) {
            marketValue = getMVPendinOrders(pfId) + getMVPprtfolio(pfId);
            //  margin = record.getApplicableMarginPct(false); //todo
            margin = record.getMarginPct(); //todo
            loan = (-account.getTotMarginBlock()) + account.getTotMarginDue();
            loan = convertToSelectedCurrency(null, account.getCurrency(), false, loan, pfId);
            Rule rule = getRule("MARGIN_TOPUP", "ACCOUNT_SUMMARY");
            if (rule != null) {
                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("loan", loan);
                    interpreter.set("marketValue", marketValue);
                    interpreter.set("margin", margin);
                    topup = (Double) interpreter.eval(rule.getRule());
                    if (topup < 0) {
                        topup = 0.00;
                    }

                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    topup = 0.00;
                }
            }
        }

        return topup;
    }

    private double calculateLiquadationAmount(String pfId) {
        // ((loan- marketValue + (margin*marketValue))/margin)
        Account account = TradingShared.getTrader().getAccoutOfPortfolio(pfId);
        TradingPortfolioRecord record = getTradingPortfolio(pfId);
        double topup = 0.00;
        double marketValue = 0.00;
        double margin = 0.00;
        double loan = 0.00;
        if (account != null && record != null && record.isMarginable()) {
            marketValue = getMVPendinOrders(pfId) + getMVPprtfolio(pfId);
//            margin = record.getApplicableMarginPct(false); //todo
            margin = record.getMarginPct(); //todo
            loan = (-account.getTotMarginBlock()) + account.getTotMarginDue();
            loan = convertToSelectedCurrency(null, account.getCurrency(), false, loan, pfId);
            Rule rule = getRule("MARGIN_LIQUIDATION", "ACCOUNT_SUMMARY");
            if (rule != null) {
                Interpreter interpreter = new Interpreter();
                try {
                    interpreter.set("loan", loan);
                    interpreter.set("marketValue", marketValue);
                    interpreter.set("margin", margin);
                    topup = (Double) interpreter.eval(rule.getRule());
                    if (Double.isInfinite(topup) || Double.isNaN(topup)) {
                        topup = 0.00;
                    }
                    if (topup < 0) {
                        topup = 0.00;
                    }

                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    topup = 0.00;
                }
            }
        }

        return topup;
    }

    public double getMVPendinOrders(String pfId) {
        double mvPending = 0.00;
        if (getTradingPortfolio(pfId) != null) {
//            ArrayList<Transaction> pendinglist = OrderStore.getSharedInstance().getFilteredList("1", "OPEN", "*", pfId);
            ArrayList<TransactRecord> pendinglist = TradePortfolios.getInstance().getTransactionRecordsForPortfolio(pfId);
            if (pendinglist != null && pendinglist.size() > 0) {
                for (int i = 0; i < pendinglist.size(); i++) {
                    TransactRecord txn = pendinglist.get(i);
                    if (txn.getPendingBuy() <= 0) {
                        continue;
                    }
                    //  if (isSymbolMarginable(txn.getSKey())) {
                    Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(txn.getKey().getExchange(), txn.getKey().getSymbol());
                    // double pct = getSymbolMarginPct(txn.getSKey());
                    // if (stock.getLastTradeValue() > 0)
                    if (stock.getLastTradeValue() > 0) {
                        double val = (txn.getPendingBuy() * PortfolioInterface.getLastTrade(stock));
                        val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, pfId);
                        mvPending = mvPending + val;
                    }
//                    else if (txn.getPrice() > 0) {
//                        double val = (txn.getPendingQuantity() * txn.getPrice());
//                        val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, pfId);
//                        mvPending = mvPending + val;
//                    }
                    else {
                        double val = (txn.getPendingBuy() * PortfolioInterface.getPreviousClosed(stock));
                        val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, pfId);
                        mvPending = mvPending + val;
                    }
                    //  }
                }
            }

        }
        return mvPending;
    }

    public double getMVPprtfolio(String pfId) {
        double mv = 0.00;
        if (getTradingPortfolio(pfId) != null) {

            // TradingPortfolioRecord records = getTradingPortfolio(pfId);
            ArrayList<ValuationRecord> valuationRecords = getValuationFilteredList(pfId);
            if (valuationRecords != null && !valuationRecords.isEmpty()) {
                for (int i = 0; i < valuationRecords.size(); i++) {
                    ValuationRecord record = valuationRecords.get(i);
                    // if (isSymbolMarginable(record.getSKey())) {
                    Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(record.getSKey().getExchange(), record.getSKey().getSymbol());
                    //  double pct = getSymbolMarginPct(record.getSKey());
                    if (stock.getLastTradeValue() > 0) {
                        double val = (record.getBalanceForMargin() * record.getContractSize() * PortfolioInterface.getLastTrade(stock) +
                                record.getCashDividends());
                        val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, pfId);
                        mv = mv + val;
                    } else {
                        double val = (record.getBalanceForMargin() * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock) +
                                record.getCashDividends());
                        val = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), true, val, pfId);
                        mv = mv + val;
                    }
                    //  }

                }


            }


        }
        return mv;
    }

    public Object[] validateTIF(short tif, String pfId, boolean isnormalMargin, boolean isdaymargin) {
        Object[] params = new Object[2];
        Boolean isTifvalid = false;
        String error = "";
        String errormsg = "";
        Rule marginTif = getRule("MARGIN_TIF", "NEW_ORDER");
        Rule dayMarginTif = getRule("DAYMARGIN_TIF", "NEW_ORDER");
        if (tifMatrix.isEmpty() && (marginTif != null || dayMarginTif != null)) {
            createTifMatrix();
        }

        if (!tifMatrix.isEmpty()) {
            String tiff = tif + "";
            Boolean[] valMatrix = tifMatrix.get(tiff);
            if (valMatrix != null) {
                boolean normalpass = isnormalMargin && valMatrix[0];
                boolean daypass = isdaymargin && valMatrix[1];
                if (normalpass != isnormalMargin) {
                    isTifvalid = false;
                    error = TIF_ERROR_NORAML;
                } else if (isdaymargin != daypass) {
                    isTifvalid = false;
                    error = TIF_ERROR_DAY;
                } else {
                    isTifvalid = true;
                    error = "";
                }

            } else {
                isTifvalid = false;
                error = TIF_ERROR_NO_MARGIN;
            }
            if (!isTifvalid) {
                errormsg = getTIFInvalidMessage(tif, error);
            }
            params[0] = isTifvalid;
            params[1] = errormsg;
            return params;
        } else {
            isTifvalid = true;
            params[0] = isTifvalid;
            params[1] = errormsg;
            return params;
        }

//        TradingPortfolioRecord record = getTradingPortfolio(pfId);
//        if (record != null) {
//            Rule rule;
//            int margintype = 0;
//            if (!isnormalMargin) {
//                if ((record.isMarginEnabled() && record.isDayMarginEnabled()) && record.isMarginTransisitionPassed()) {
//                    rule = getRule("MARGIN_TIF", "NEW_ORDER");
//                    margintype = 1;
//                } else
//                if ((record.isMarginEnabled() && record.isDayMarginEnabled()) && (!record.isMarginTransisitionPassed())) {
//                    rule = getRule("DAYMARGIN_TIF", "NEW_ORDER");
//                } else if (record.isMarginEnabled()) {
//                    rule = getRule("MARGIN_TIF", "NEW_ORDER");
//                    margintype = 1;
//                } else {
//                    rule = getRule("DAYMARGIN_TIF", "NEW_ORDER");
//                }
//            } else {
//                rule = getRule("MARGIN_TIF", "NEW_ORDER");
//                margintype = 1;
//            }
//            if (rule != null) {
//                String[] allowed = rule.getRule().split(",");
//                for (int j = 0; j < allowed.length; j++) {
//                    if (allowed[j].equalsIgnoreCase(tif + "")) {
//                        isTifvalid = true;
//                    }
//                }
////                isTifvalid = false;
//                if (!isTifvalid) {
//                    error = getTIFInvalidMessage(tif, margintype);
//                }
//                params[0] = isTifvalid;
//                params[1] = error;
//            } else {
//                isTifvalid = true;
//            }
//            return params;
//
//        } else {
//            return params;
//        }
    }

    private String getTIFInvalidMessage(short tif, String margintype) {
        String message = Language.getString("MARGIN_TIF_ERROR");
        String margin = "";
        String tifname = "";
        if (margintype.equalsIgnoreCase(TIF_ERROR_NORAML)) {
            margin = Language.getString("MARGIN_TRADING_NORMAL_MARGIN");
        } else if (margintype.equalsIgnoreCase(TIF_ERROR_DAY)) {
            margin = Language.getString("MARGIN_TRADING_DAY_MARGIN");

        } else {
            margin = Language.getString("MARGIN_TRADING_MARGIN");
        }


        switch (tif) {
            case TradeMeta.TIF_DAY:
                tifname = TradingShared.TIFF_DAY;
                break;
            case TradeMeta.TIF_GTC:
                tifname = TradingShared.TIFF_GTC;
                break;
            case TradeMeta.TIF_AOP:
                tifname = TradingShared.TIFF_AOP;
                break;
            case TradeMeta.TIF_IOC:
                tifname = TradingShared.TIFF_IOC;
                break;
            case TradeMeta.TIF_FOK:
                tifname = TradingShared.TIFF_FOK;
                break;
            case TradeMeta.TIF_GTD:
                tifname = TradingShared.TIFF_GTD;
                break;
            case TradeMeta.TIF_GTX:
                tifname = TradingShared.TIFF_GTX;
                break;
            case TradeMeta.TIF_WEEK:
                tifname = TradingShared.TIFF_WEEK;
                break;
            case TradeMeta.TIF_MONTH:
                tifname = TradingShared.TIFF_MONTH;
                break;
        }

        message = message.replaceFirst("\\[tif\\]", tifname);
        message = message.replaceFirst("\\[margin\\]", margin);
        return message;
    }

    public boolean isMarginsApplicable(String pfId) {
        TradingPortfolioRecord record = getTradingPortfolio(pfId);
        if (record.isMarginable()) {
            if (record.isMarginEnabled()) {
                return true;
            } else if (record.isDayMarginEnabled() && record.isDayMarginAvailable(TradeMethods.getReferenceMarginExchangeForAccSummary())) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    private TradingPortfolioRecord getTradingPortfolio(String id) {

        return TradingShared.getTrader().getPortfolio(id);
    }


    private boolean isSymbolMarginable(TradeKey sKey) {

        if (sKey != null) {
            return MarginSymbolStore.getSharedInstance().isMarginSymbolAvailable(sKey);
        } else {
            return false;
        }
    }

    private double getSymbolMarginPct(TradeKey sKey) {
        if (sKey != null) {
            return MarginSymbolStore.getSharedInstance().getMarginSymbolRecord(sKey).getMarginBuypct();
        } else {
            return 0;
        }
    }

    private double getSymbolMarginPct(String sKey) {
        if (sKey != null) {
            return MarginSymbolStore.getSharedInstance().getMarginSymbolRecord(new TradeKey(SharedMethods.getExchangeFromKey(sKey), SharedMethods.getSymbolFromKey(sKey), SharedMethods.getInstrumentTypeFromKey(sKey))).getMarginBuypct();
        } else {
            return 0;
        }
    }

    private void createTifMatrix() {
        Rule marginTif = getRule("MARGIN_TIF", "NEW_ORDER");
        Rule dayMarginTif = getRule("DAYMARGIN_TIF", "NEW_ORDER");
        if (tifMatrix == null) {
            tifMatrix = new Hashtable<String, Boolean[]>();
        }

        if (marginTif != null) {
            String[] allowed = marginTif.getRule().split(",");
            if (allowed != null) {
                for (int j = 0; j < allowed.length; j++) {
                    Boolean[] matrixVar = new Boolean[2];
                    matrixVar[0] = (new Boolean(true));   // normal margin
                    matrixVar[1] = (new Boolean(false));  // day margin
                    String key = allowed[j];
                    tifMatrix.put(key, matrixVar);

                }
            }

        }

        if (dayMarginTif != null) {
            String[] allowed = dayMarginTif.getRule().split(",");
            if (allowed != null) {
                for (int j = 0; j < allowed.length; j++) {
                    Boolean[] matrixVar = new Boolean[2];
                    matrixVar[0] = (new Boolean(false));   // normal margin
                    matrixVar[1] = (new Boolean(true));  // day margin
                    String key = allowed[j];
                    if (tifMatrix.get(key) != null) {
                        (tifMatrix.get(key))[1] = true;
                    } else {
                        tifMatrix.put(key, matrixVar);
                    }

                }
            }

        }
    }

    public void tradeServerConnected() {
        createTifMatrix();
    }

    public void tradeSecondaryPathConnected() {
        tifMatrix.clear();
    }

    public void tradeServerDisconnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private String getexchange(String Skey) {
        try {
            return SharedMethods.getExchangeFromKey(Skey);
        } catch (Exception e) {
            return "*";
        }
    }

    private double convertToSelectedCurrency(String exchangeCode, String sCurrency, boolean applyPriceModificationFactor, double value, String portfolio) {
        if (applyPriceModificationFactor) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            if (exchange != null)
//                return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
                return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
            else
//                return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
                return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
        } else {
//            return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
            return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
        }
    }

    private double convertToSelectedCurrency(String exchangeCode, String sCurrency, String tgetCurrency, boolean applyPriceModificationFactor, double value, String portfolio) {
        if (applyPriceModificationFactor) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            if (exchange != null)
//                return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
                return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio)) / exchange.getPriceModificationFactor();
            else
//                return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
                return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
        } else {
//            return value * CurrencyStore.getBuyRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
            return value * CurrencyStore.getSellRate(sCurrency, currency, TradingShared.getTrader().getPath(portfolio));
        }
    }
}
