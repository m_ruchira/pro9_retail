/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Dec 12, 2007
 * Time: 3:12:04 PM
 */


package com.isi.csvr.trading.portfolio;

// Copyright (c) 2000 Integrated Systems International (ISI)

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.portfolio.PFUtilities;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

public class TradingPfDCRenderer extends TWBasicTableRenderer {

    private static Color g_oUpFGColor;
    private static Color g_oDownFGColor;
    private static Color g_oUpBGColor;
    private static Color g_oDownBGColor;
    private static Color g_oUpColor;
    private static Color g_oDownColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oBuyBGColor;
    private static Color g_oSellBGColor;
    private static Color g_oOpBalBGColor;
    private static Color g_oBuyFGColor;
    private static Color g_oSellFGColor;
    private static Color g_oOpBalFGColor;
    private static Color g_oPfDetailBGColor;
    private static Color g_oPfDetailFGColor;
    private static Border selectedBorder;
    private static Border unselectedBorder;
    private static ImageIcon upImage = null;
    private static ImageIcon downImage = null;
    int rowCount;
    boolean isLastRow;
    boolean isTotalRow = false;
    boolean isPfStartRow = false;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private String g_sNA = "NA";
    private Color upColor;
    private Color downColor;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oPerFormat;
    private TWDecimalFormat oQuantityFormat;
    private TWDecimalFormat oChangeFormat;
    private TWDecimalFormat oPChangeFormat;
    private TWDateFormat g_oDateFormat;
    private TWDateFormat g_oDateTimeFormatHM;
    private TWDateFormat g_oDateTimeFormatHMS;
    private TWDateFormat g_oTimeFormat;
    private Double floatValue;
    private Double[] floatValuePair;
    private long longValue;
    private int intValue;
    private Date date;
    private Color foreground = null;
    private Color background = null;
    private String sBuyType = null;
    private String sSellType = null;
    private String sOpBalType = null;
    private String sDIvidendType = null;
    private String sTxType = null;
    private byte decimalPlaces = Constants.TWO_DECIMAL_PLACES;
    private byte tableDecimals = Constants.TWO_DECIMAL_PLACES;
    private byte stockDecimals = Constants.TWO_DECIMAL_PLACES;
    private boolean isValuationType = false;

    public TradingPfDCRenderer(String[] asColumns, int[] asRendIDs) {
        date = new Date();

        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oPerFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oChangeFormat = new TWDecimalFormat(" ###,##0.00 ");
        oPChangeFormat = new TWDecimalFormat(" ###,##0.00 ");

        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HM_FORMAT"));//" yyyy/MM/dd HH:mm ");
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));//" dd:MM:yyyy '-' HH:mm:ss ");
        g_oTimeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));//" HH:mm:ss ");

        g_asRendIDs = asRendIDs;

        reload();

        sBuyType = Language.getString("BUY");
        sSellType = Language.getString("SELL");
        sOpBalType = Language.getString("OPENING_BAL");
        sDIvidendType = Language.getString("DIVIDEND");

        try {
            upImage = new ImageIcon("images/Common/up.gif");
        } catch (Exception e) {
            upImage = null;
        }
        try {
            downImage = new ImageIcon("images/Common/down.gif");
        } catch (Exception e) {
            downImage = null;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;

        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oUpColor = Color.black;
        g_oDownColor = Color.black;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
        g_oBuyBGColor = Color.white;
        g_oSellBGColor = Color.white;
        g_oOpBalBGColor = Color.white;
        g_oBuyFGColor = Color.black;
        g_oSellFGColor = Color.black;
        g_oOpBalFGColor = Color.black;
        g_oPfDetailFGColor = Color.black;
        g_oPfDetailBGColor = Color.white;

    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");

            g_oBuyBGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_BUY_BGCOLOR");
            g_oSellBGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_SELL_BGCOLOR");
            g_oOpBalBGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR");
            g_oBuyFGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_BUY_FGCOLOR");
            g_oSellFGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_SELL_FGCOLOR");
            g_oOpBalFGColor = Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_FGCOLOR");
            g_oPfDetailFGColor = Theme.getColor("TRADINGPF_DETAIL_FGCOLOR");
            g_oPfDetailBGColor = Theme.getColor("TRADINGPF_DETAIL_BGCOLOR");

            selectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oSelectedFG);
            unselectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oFG1);
        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            g_oBuyBGColor = Color.white;
            g_oSellBGColor = Color.white;
            g_oOpBalBGColor = Color.white;
            g_oBuyFGColor = Color.black;
            g_oSellFGColor = Color.black;
            g_oOpBalFGColor = Color.black;
            g_oPfDetailBGColor = Color.white;
            g_oPfDetailFGColor = Color.GREEN;
        }
    }

    public void propertyChanged(int property) {
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void setValuationType() {
        isValuationType = true;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        rowCount = table.getModel().getRowCount();
        isTotalRow = false;
        isPfStartRow = false;

        if (TradePortfolios.getRowType(row) == PfValutionRecord.TOTOAL) {
            isTotalRow = true;
        } else if (TradePortfolios.getRowType(row) == PfValutionRecord.PORTFOLIODETAIL) {
            isPfStartRow = true;
        }

        isLastRow = (rowCount == (row + 1));
        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        tableDecimals = ((SmartTable) table).getDecimalPlaces();
        try {

            stockDecimals = (Byte) table.getModel().getValueAt(row, -4);
        } catch (Exception e) {
            stockDecimals = 2;
        }
        if (tableDecimals == Constants.UNASSIGNED_DECIMAL_PLACES) { // do not use table's decimal places
            if (decimalPlaces != stockDecimals) {
                applyDecimalPlaces(stockDecimals);
            }
        } else { // use decimals from table
            if (decimalPlaces != tableDecimals) {
                applyDecimalPlaces(tableDecimals);
            }
        }

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;

        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isPfStartRow) {
                foreground = sett.getPfDetailColorFG();
                background = sett.getPfDetailColorBG();
            } else {
                if (isSelected) {
                    foreground = sett.getSelectedColumnFG();
                    background = sett.getSelectedColumnBG();
                } else {
                    if (row % 2 == 0) {
                        foreground = sett.getRowColor1FG();
                        background = sett.getRowColor1BG();
                    } else {
                        foreground = sett.getRowColor2FG();
                        background = sett.getRowColor2BG();
                    }
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isPfStartRow) {
                foreground = g_oPfDetailFGColor;
                background = g_oPfDetailBGColor;
            } else {
                if (isSelected) {
                    foreground = g_oSelectedFG;
                    background = g_oSelectedBG;
                } else if (row % 2 == 0) {
                    foreground = g_oFG1;
                    background = g_oBG1;
                } else {
                    foreground = g_oFG2;
                    background = g_oBG2;
                }
                upColor = g_oUpColor;
                downColor = g_oDownColor;
            }
        }

        if (!isValuationType) {
            byte type = ((Byte) table.getModel().getValueAt(row, 5));
            if (isCustomThemeEnabled) {
                switch (type) {
                    case com.isi.csvr.portfolio.PFUtilities.BUY:
                        if (column == 5)
                            sTxType = sBuyType;
                        if (!isSelected) {
                            foreground = sett.getBuyColorFG();
                            background = sett.getBuyColorBG();
                        }
                        break;
                    case PFUtilities.SELL:
                        if (column == 5)
                            sTxType = sSellType;
                        if (!isSelected) {
                            foreground = sett.getSellColorFG();
                            background = sett.getSellColorBG();
                        }
                        break;
                    case PFUtilities.OP_BAL:
                        if (column == 5)
                            sTxType = sOpBalType;
                        if (!isSelected) {
                            foreground = sett.getOpBalColorFG();
                            background = sett.getOpBalColorBG();
                        }
                        break;
                    case PFUtilities.DIVIDEND:
                        if (column == 5)
                            sTxType = sDIvidendType;
                        if (!isSelected) {
                            foreground = sett.getDivColorFG();
                            background = sett.getDivColorBG();
                        }
                        break;
                }
            } else {
                switch (type) {
                    case PFUtilities.BUY:
                        if (column == 5)
                            sTxType = sBuyType;
                        if (!isSelected) {
                            foreground = g_oBuyFGColor;
                            background = g_oBuyBGColor;
                        }
                        break;
                    case PFUtilities.SELL:
                        if (column == 5)
                            sTxType = sSellType;
                        if (!isSelected) {
                            foreground = g_oSellFGColor;
                            background = g_oSellBGColor;
                        }
                        break;
                    case PFUtilities.OP_BAL:
                        if (column == 5)
                            sTxType = sOpBalType;
                        if (!isSelected) {
                            foreground = g_oOpBalFGColor;
                            background = g_oOpBalBGColor;
                        }
                        break;
                    case PFUtilities.DIVIDEND:
                        if (column == 5)
                            sTxType = sDIvidendType;
                        if (!isSelected) {
                            foreground = g_oOpBalFGColor;
                            background = g_oOpBalBGColor;
                        }
                        break;
                }
            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        int iRendID = 0;
        if (isPfStartRow) {
            iRendID = 1;
        } else {
            iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        }


        try {
            lblRenderer.setIcon(null);
            if (isValuationType)
                lblRenderer.setBorder(getBorder(table, isTotalRow, column, cellBorder));
            switch (iRendID) {
                case 0: // DEFAULT
                    if (value == null)
                        lblRenderer.setText(g_sNA);
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                case 2: // DESCRIPTION
                    if (value == null)
                        lblRenderer.setText(g_sNA);
                    else {
                        if (Settings.isShowArabicNumbers())
                            lblRenderer.setText(GUISettings.arabize((String) value));
                        else
                            lblRenderer.setText((String) value);
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    lblRenderer.setText(oPriceFormat.format(((Float) (value))));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(((Long) (value))));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 5: // CHANGE
                    floatValue = ((Double) value); // toFloatValue(value);
                    lblRenderer.setText(oPriceFormat.format(floatValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (floatValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (floatValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 6: // % CHANGE
                    floatValue = ((Double) value); // toFloatValue(value);
                    lblRenderer.setText(oPriceFormat.format(floatValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (floatValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (floatValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 7: // DATE
                    longValue = ((Long) (value));
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 8: {// DATE TIME
                    longValue = ((Long) (value));
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHM.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                }
                case 'D': {// DATE TIME with secs
                    longValue = ((Long) (value));
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHMS.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                }
                case 9: // TICK
                    intValue = (int) toLongValue(value);
                    switch (intValue) {
                        case Settings.TICK_UP:
                            lblRenderer.setIcon(upImage);
                            lblRenderer.setText("");
                            break;
                        case Settings.TICK_DOWN:
                            lblRenderer.setIcon(downImage);
                            lblRenderer.setText("");
                            break;
                        default:
                            lblRenderer.setIcon(null);
                            lblRenderer.setText("");
                    }
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    break;
                case 'A': // TIME
                    longValue = ((Long) (value));
                    if (longValue == 0) {
                        lblRenderer.setText(g_sNA);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oTimeFormat.format(date));
                        lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    }
                    break;
                case 'B': // Broker ID
                    if ((value == null) || (((String) value)).equals("")
                            || (((String) value)).equals("null"))
                        lblRenderer.setText(g_sNA);
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 'P': // PRICE
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    floatValue = ((Double) value);
                    lblRenderer.setText(oPriceFormat.format(floatValue));
                    break;
                case 'p': // PRICE without decimals
                    floatValuePair = (Double[]) value;
                    lblRenderer.setText(oQuantityFormat.format(floatValuePair[0]));
                    if (floatValuePair.length > 1) {
                        if (floatValuePair[0] > floatValuePair[1]) {
                            lblRenderer.setForeground(upColor);
                        } else if (floatValuePair[0] < floatValuePair[1]) {
                            lblRenderer.setForeground(downColor);
                        } else {
                            if (floatValuePair[0] > 0)
                                lblRenderer.setForeground(upColor);
                            else if (floatValuePair[0] < 0)
                                lblRenderer.setForeground(downColor);
                            else
                                lblRenderer.setForeground(foreground);
                        }
                        floatValuePair[1] = floatValuePair[0];
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'F': // PRICE
                    floatValuePair = (Double[]) value;
                    lblRenderer.setText(oPriceFormat.format(floatValuePair[0]));
                    if (floatValuePair.length > 1) {
                        if (floatValuePair[0] > floatValuePair[1]) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (floatValuePair[0] < floatValuePair[1]) {
                            lblRenderer.setBackground(g_oDownBGColor);
                            lblRenderer.setForeground(g_oDownFGColor);
                        } else {
                            if (floatValuePair[0] > 0)
                                lblRenderer.setForeground(upColor);
                            else if (floatValuePair[0] < 0)
                                lblRenderer.setForeground(downColor);
                            else
                                lblRenderer.setForeground(foreground);
                        }
                        floatValuePair[1] = floatValuePair[0];
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'T': // PORTFOLIO TRANSACTION TYPE
                    if (sTxType != null)
                        lblRenderer.setText(sTxType);
                    else
                        lblRenderer.setText(g_sNA);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 'X': // PORTFOLIO TRANSACTION TYPE
                    if ((Integer) table.getModel().getValueAt(row, -10) == 1) {
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        floatValue = (Double) value;
                        lblRenderer.setText("<HTML><i><u><b>" + oPriceFormat.format(floatValue));
                    } else {
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        floatValue = (Double) value;
                        lblRenderer.setText(oPriceFormat.format(floatValue));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'Y':
                    floatValue = ((Double) value);
                    lblRenderer.setText(oPerFormat.format(floatValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (floatValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (floatValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 'Z': // percentages 2 decimal places
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    floatValue = ((Double) value).doubleValue();
                    lblRenderer.setText(oPerFormat.format(floatValue));
                    break;
                default:
                    lblRenderer.setText("");
            }

        } catch (Exception e) {
            lblRenderer.setText("");
        }
        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    public Border getBorder(JTable table, boolean isTotalRow, int column, Border cellBorder) {
        if ((isTotalRow) && (table.convertColumnIndexToModel(column) > 9) && (table.convertColumnIndexToModel(column) < 20)) {
            return selectedBorder;
        } else {
            return cellBorder;
        }
    }

    private void applyDecimalPlaces(byte decimalPlaces) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                break;
            case Constants.ONE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case Constants.TWO_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case Constants.THREE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
            case Constants.FIVE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                break;
            case Constants.SIX_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                break;
        }
        this.decimalPlaces = decimalPlaces;
    }
}



