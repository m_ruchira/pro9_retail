package com.isi.csvr.trading.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.trading.datastore.TradingCurrencyConverter;
import com.isi.csvr.trading.shared.TradeKey;

/**
 * <p>Title: TW International</p>
 * <p>Description: Contains teh calculated Transaction details for the selected Portfolio(s)
 * transactions</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Bandula
 * @version 1.0
 */

public class ValuationRecord {

    private TradeKey sKey;
    private String portfolioID;
    private int owned;
    private double totCost;
    private String currency;
    private double avgCost;
    private double cashDividends;
    private long pledged;
    private long buyPending;
    private long sellPending;
    //    private double realAveragecost;
    private double marginDue;
    private double dayMarginDue;
    private int dayHoldings;
    private int dayMarginHoldings;
    private int marginHoldings;
    private double marginBuyPct;
    private int openBuyCount;
    private int openSellCount;
    private int contractSize;
    private double priceCorrectionFactor = 1;
    private String bookKeeper = "";
    private long tPlusDayNetHolding = 0;
    private long tPlusPendingStock = 0;
    private long tPlusDaySellPending = 0;
    private double conversionRate = 1D;
//    public boolean addCostomerAverageCost = false;

    public ValuationRecord() {
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public String getCurrency() {
        try {
            if (currency == null) {
                Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(sKey.getExchange(), sKey.getSymbol());
                String curr = PortfolioInterface.getCurrency(stock);
                currency = TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(curr);
                conversionRate = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(curr);
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getConversionRate() {
        return conversionRate;
    }

    public int getOwned(int instrumentType) {
        if (instrumentType == Meta.INSTRUMENT_FUTURE) {
            return openBuyCount + openSellCount;
        } else {
            return owned;
        }
    }

    public long getBalance() {
//        return (owned - pledged)-tPlusPendingStock;
//        return (owned - pledged)-tPlusDaySellPending;
        return (owned - pledged) - tPlusDaySellPending - tPlusPendingStock;
    }

    public long getBalanceForMargin() {     // for margin trading
//        return (owned - pledged)-tPlusPendingStock;
//        return (owned - pledged)-tPlusDaySellPending;
        return (owned - pledged);
    }

    public TradeKey getSKey() {
        return sKey;
    }

    /*public double getrealaverageCost() {
        return realAveragecost;
    }*/

    public void setSKey(TradeKey sKey) {
        this.sKey = sKey;
    }

    public double getCumCost() {
        return totCost;
    }

    public void setCumCost(double totCost) {
        this.totCost = totCost;
    }

    public int getDayHoldings() {
        return dayHoldings;
    }

    public void setDayHoldings(int dayHoldings) {
        this.dayHoldings = dayHoldings;
    }

    public void setOwned(int owned) {
        this.owned = owned;
    }

    public double getCashDividends() {
        return cashDividends;
    }

    public void setCashDividends(double cashDividends) {
        this.cashDividends = cashDividends;
    }

    public long getPledged() {
        return pledged;
    }

    public void setPledged(long pledged) {
        this.pledged = pledged;
    }

    public long getBuyPending() {
        return buyPending;
    }

    public void setBuyPending(long buyPending) {
        this.buyPending = buyPending;
    }

    public long getSellPending() {
        return sellPending;
    }

    public void setSellPending(long sellPending) {
        this.sellPending = sellPending;
    }

    public String getPortfolioID() {
        return portfolioID;
    }

    public void setPortfolioID(String portfolioID) {
        this.portfolioID = portfolioID;
    }

    /*public void setRealAverageCost(double realaverageCost) {
        this.realAveragecost = realaverageCost;

    }*/

/*    public void setIschanged(boolean state) {
        this.addCostomerAverageCost = state;
    }*/


    public double getMarginDue() {
        return marginDue;
    }

    public void setMarginDue(double marginDue) {
        this.marginDue = marginDue;
    }

    public double getDayMarginDue() {
        return dayMarginDue;
    }

    public void setDayMarginDue(double dayMarginDue) {
        this.dayMarginDue = dayMarginDue;
    }

    public double getMarginBuyPct() {
        return marginBuyPct;
    }

    public void setMarginBuyPct(double marginBuyPct) {
        this.marginBuyPct = marginBuyPct;
    }

    public int getOpenBuyCount() {
        return openBuyCount;
    }

    public void setOpenBuyCount(int openBuyCount) {
        this.openBuyCount = openBuyCount;
    }

    public int getOpenSellCount() {
        return openSellCount;
    }

    public void setOpenSellCount(int openSellCount) {
        this.openSellCount = openSellCount;
    }

    public int getContractSize() {
        return contractSize;
    }

    public void setContractSize(int contractSize) {
        this.contractSize = contractSize;
    }

    public double getPriceCorrectionFactor() {
        return priceCorrectionFactor;
    }

    public void setPriceCorrectionFactor(double priceCorrectionFactor) {
        this.priceCorrectionFactor = priceCorrectionFactor;
    }

    public int getDayMarginHoldings() {
        return dayMarginHoldings;
    }

    public void setDayMarginHoldings(int dayMarginHoldings) {
        this.dayMarginHoldings = dayMarginHoldings;
    }

    public int getMarginHoldings() {
        return marginHoldings;
    }

    public void setMarginHoldings(int marginHoldings) {
        this.marginHoldings = marginHoldings;
    }

    public String getBookKeeper() {
        return bookKeeper;
    }

    public void setBookKeeper(String bookKeeper) {
        this.bookKeeper = bookKeeper;
    }

    public long getTPlusDayNetHolding() {
        return tPlusDayNetHolding;
    }

    public void setTPlusDayNetHolding(long tPlusDayNetHolding) {
        this.tPlusDayNetHolding = tPlusDayNetHolding;
    }

    public long getTPlusPendingStock() {
        return tPlusPendingStock;
    }

    public void setTPlusPendingStock(long tPlusPendingStock) {
        this.tPlusPendingStock = tPlusPendingStock;
    }

    public long getTPlusDaySellPending() {
        return tPlusDaySellPending;
    }

    public void setTPlusDaySellPending(long tPlusDaySellPending) {
        this.tPlusDaySellPending = tPlusDaySellPending;
    }

    public String getXML() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<PortfolioData>");
        buffer.append("<ID value=\"").append(getPortfolioID()).append("\"/>");
        buffer.append("<Symbol value=\"").append(getSKey().getSymbol()).append("\"/>");
        buffer.append("<Exchange value=\"").append(getSKey().getExchange()).append("\"/>");
        buffer.append("<InstrumentType value=\"").append(getSKey().getInstrumentType()).append("\"/>");
        buffer.append("<AverageCost value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getAvgCost())).append("\"/>");
        buffer.append("<Quantity value=\"").append(owned).append("\"/>");
        buffer.append("<BuyPending value=\"").append(getBuyPending()).append("\"/>");
        buffer.append("<SellPending value=\"").append(getSellPending()).append("\"/>");
        buffer.append("</PortfolioData>\n");

        return buffer.toString();
    }
}