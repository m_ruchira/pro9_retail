package com.isi.csvr.trading.portfolio;

import com.isi.csvr.Client;
import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.shared.DownArrow;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class FilterPanel extends JDialog implements ActionListener, DateSelectedListener {

    private final int FROM_DATE = 0;
    private final int TO_DATE = 1;

    private JLabel lblFilterByPeriod = new JLabel();
    private JLabel lblFilterBySymbol = new JLabel();
    private JLabel lblFilterByTxType = new JLabel();
    private JLabel lblFrom = new JLabel();
    private JLabel lblTo = new JLabel();
    private TWButton btnApply = new TWButton();
    private TWButton btnClose = new TWButton();
    //    private JTextField      txtFilterBySymbol   = new JTextField();
    private JComboBox cmbSymbols = new JComboBox();
    private JComboBox cmbTxType = new JComboBox();
    private TWButton btnFromDate;
    private TWButton btnToDate;
    private DatePicker datePicker;
    private JLabel lblFromDate;
    private JLabel lblToDate;
    private TradingPortfolioWindow pfWindow;
    private int dateSelector;
    private int width = 600; //570;
    private int height = 80;

    public FilterPanel(Frame parent, String title, boolean isModal, TradingPortfolioWindow pfWindow) {
        super(parent, title, isModal);
        try {
            this.pfWindow = pfWindow;
            jbInit();
            applyTextLabels();
            loadCurrentSymbols();
            GUISettings.applyOrientation(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.getContentPane().setLayout(new BorderLayout()); // new FlowLayout(FlowLayout.LEADING, 5, 2));
        this.setTitle(Language.getString("FILTER_TXN_HISTORY"));
        JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 2));

        datePicker = new DatePicker(Client.getInstance().getFrame(), true);
        datePicker.getCalendar().addDateSelectedListener(this);

        JPanel datePanel = new JPanel();
        datePanel.setLayout(new GridLayout(2, 1)); //new FlowLayout(FlowLayout.LEADING,10,0));
        datePanel.setPreferredSize(new Dimension(160, 60));

        JPanel fromDatePanel = new JPanel();
        fromDatePanel.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 0));

        lblFrom.setPreferredSize(new Dimension(40, 20));
        //upperControlPanel.add(lblFrom);
        fromDatePanel.add(lblFrom);

        btnFromDate = new TWButton();
        lblFromDate = new JLabel();
        fromDatePanel.add(getDatePanel("FD", btnFromDate, lblFromDate));

        JPanel toDatePanel = new JPanel();
        toDatePanel.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 0));

        lblTo.setPreferredSize(new Dimension(40, 20));
        toDatePanel.add(lblTo);

        btnToDate = new TWButton();
        lblToDate = new JLabel();
        toDatePanel.add(getDatePanel("TD", btnToDate, lblToDate));

        datePanel.add(fromDatePanel);
        datePanel.add(toDatePanel);

        cmbSymbols.setPreferredSize(new Dimension(100, 20));
        cmbTxType.setPreferredSize(new Dimension(100, 20));

        centerPanel.add(lblFilterByPeriod);
        centerPanel.add(datePanel);
        centerPanel.add(lblFilterBySymbol);
        centerPanel.add(cmbSymbols);
        centerPanel.add(lblFilterByTxType);
        centerPanel.add(cmbTxType);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 5, 2));
        btnApply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                applyFilter();
                closeDialog();
            }
        });
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
        });
        btnApply.setPreferredSize(new Dimension(120, 20));
        btnClose.setPreferredSize(new Dimension(80, 20));

        buttonPanel.add(btnApply);
        buttonPanel.add(btnClose);

        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    }

    private void applyTextLabels() {
        lblFilterByPeriod.setText(Language.getString("FILTER_BY_PERIOD")); //"By Period");
        lblFilterBySymbol.setText(Language.getString("FILTER_BY_SYMBOL")); //"By Symbol");
        lblFilterByTxType.setText(Language.getString("FILTER_BY_TYPE")); //"By Type");
        lblFrom.setText(Language.getString("FROM")); //"From");
        lblTo.setText(Language.getString("TO")); //"To");
        btnApply.setText(Language.getString("APPLY_FILTER")); // "Apply Filter");
        btnClose.setText(Language.getString("CLOSE")); // "Close");

        cmbTxType.addItem(Language.getString("ALL"));
        cmbTxType.addItem(Language.getString("BUY")); //"Buy");
        cmbTxType.addItem(Language.getString("SELL")); //"Sell");
        cmbTxType.addItem(Language.getString("OPENING_BAL")); //"Opening Balance");
        cmbTxType.addItem(Language.getString("DIVIDEND"));

        cmbSymbols.addItem(Language.getString("ALL"));
    }

    private void loadCurrentSymbols() {
        ValuationRecord record = null;
        ArrayList list = TradePortfolios.getInstance().getValuationList();
        for (int i = 0; i < list.size(); i++) {
            record = (ValuationRecord) list.get(i);
            cmbSymbols.addItem(record.getSKey());
            record = null;
        }
        list = null;
    }

    public void showDialog() {
        this.setSize(new Dimension(width + 10, height + 30));
        centerUI();
        this.setVisible(true);
    }

    private void centerUI() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }

    private void closeDialog() {
        pfWindow = null;
        this.dispose();
    }

    public JPanel getDatePanel(String type, final TWButton arrowButton, final JLabel lblDate) {
        JPanel datePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        datePanel.setPreferredSize(new Dimension(100, 20));
        datePanel.setBorder(BorderFactory.createEtchedBorder());

        lblDate.setPreferredSize(new Dimension(78, 16));
        lblDate.setOpaque(true);
        datePanel.add(lblDate);
        arrowButton.setIcon(new DownArrow());
        arrowButton.setBorder(null);
        arrowButton.setActionCommand(type);
        arrowButton.addActionListener(this);
        arrowButton.setPreferredSize(new Dimension(18, 16));
        datePanel.add(arrowButton);
        //fromDatePanel.addMouseListener(this);
        //panel3.add(fromDatePanel);
        //mainPanel.add(panel3);
        return datePanel;
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        DecimalFormat numberFormat = new DecimalFormat("00");

        iMonth++;
        switch (dateSelector) {
            case FROM_DATE:
                lblFromDate.setText(PFUtilities.getConvertedData("" + iDay + "/" + iMonth + "/" + iYear));
//                 lblFromDate.setText("" + iYear  + "/" + iMonth + "/" +  iDay);
//                fromDate = "" + iYear  + "" + numberFormat.format(iMonth)  + "" +
//                       numberFormat.format(iDay) + numberFormat.format(cmbFromHour.getSelectedIndex());
                break;
            case TO_DATE:
                lblToDate.setText(PFUtilities.getConvertedData("" + iDay + "/" + iMonth + "/" + iYear));
//                toDate = "" + iYear  + "" + numberFormat.format(iMonth)  + "" +
//                     numberFormat.format(iDay) + numberFormat.format(cmbToHour.getSelectedIndex());
                break;
        }
        datePicker.setVisible(false);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("FD")) {
            this.dateSelector = FROM_DATE;
            JComponent source = (JComponent) e.getSource();
            Point location = source.getParent().getLocation();
            location.move(-80, 20);
            SwingUtilities.convertPointToScreen(location, source);
            datePicker.setLocation(location);
            datePicker.show();
        } else if (e.getActionCommand().equals("TD")) {
            this.dateSelector = TO_DATE;
            JComponent source = (JComponent) e.getSource();
            Point location = source.getParent().getLocation();
            location.move(-80, 20);
            SwingUtilities.convertPointToScreen(location, source);
            datePicker.setLocation(location);
            datePicker.show();
        }
    }

    private void applyFilter() {
        int iDate = 0;
        int iYear = 0;
        int iMonth = 0;
        byte txnType = 0;
        String sKey = null;
        Calendar fromDate = Calendar.getInstance();
        Calendar toDate = Calendar.getInstance();
        Date startDate = null;
        Date endDate = null;
        StringTokenizer st = null;

        try {
            st = new StringTokenizer(lblFromDate.getText(), "/", false);
            iDate = Integer.parseInt(st.nextToken());
            iMonth = Integer.parseInt(st.nextToken()) - 1;
            iYear = Integer.parseInt(st.nextToken());
            fromDate.set(iYear, iMonth, iDate);
            startDate = new Date(fromDate.getTimeInMillis());
            st = null;
            fromDate = null;
        } catch (Exception ex1) {
            fromDate = null;
            startDate = null;
        }
        try {
            st = new StringTokenizer(lblToDate.getText(), "/", false);
            iDate = Integer.parseInt(st.nextToken());
            iMonth = Integer.parseInt(st.nextToken()) - 1;
            iYear = Integer.parseInt(st.nextToken());
            toDate.set(iYear, iMonth, iDate);
//            toDate.set(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken())-1, Integer.parseInt(st.nextToken()));
            endDate = new Date(toDate.getTimeInMillis());
            st = null;
            toDate = null;
        } catch (Exception ex1) {
            toDate = null;
            endDate = null;
        }
        sKey = (String) cmbSymbols.getSelectedItem();
        if (sKey.equals(Language.getString("ALL")))
            sKey = null;
//System.out.println("cmbTxType = " + cmbTxType.getSelectedIndex() + " " + sKey);
        switch (cmbTxType.getSelectedIndex()) {
            case 0:
                txnType = PFUtilities.NONE;
                break;
            case 1:
                txnType = PFUtilities.BUY;
                break;
            case 2:
                txnType = PFUtilities.SELL;
                break;
            case 3:
                txnType = PFUtilities.OP_BAL;
                break;
            case 4:
                txnType = PFUtilities.DIVIDEND;
                break;
        }

        pfWindow.createFilteredTransactionList(sKey, txnType, startDate, endDate);
        pfWindow.setFilterDescription(true, startDate, endDate, sKey, txnType);
    }

//    private JComboBox       cmbSymbols          = new JComboBox();
//    private JComboBox       cmbTxType           = new JComboBox();
//    private TWButton 	    btnFromDate;
//    private TWButton 	    btnToDate;
//    private DatePicker      datePicker;
//    private JLabel		    lblFromDate;
//    private JLabel		    lblToDate;

    public void setLastFilterSettings(String sKey, byte type, Date fromDate, Date toDate) {
        if (sKey != null)
            cmbSymbols.setSelectedItem(sKey);
        else
            cmbSymbols.setSelectedIndex(0);
        switch (type) {
            case PFUtilities.NONE:
                cmbTxType.setSelectedIndex(0);
                break;
            case PFUtilities.BUY:
                cmbTxType.setSelectedIndex(1);
                break;
            case PFUtilities.SELL:
                cmbTxType.setSelectedIndex(2);
                break;
            case PFUtilities.OP_BAL:
                cmbTxType.setSelectedIndex(3);
                break;
            case PFUtilities.DIVIDEND:
                cmbTxType.setSelectedIndex(4);
                break;
        }
        if (fromDate != null)
            lblFromDate.setText(PFUtilities.getFormattedDate(fromDate));

        if (toDate != null)
            lblToDate.setText(PFUtilities.getFormattedDate(toDate));
    }
}