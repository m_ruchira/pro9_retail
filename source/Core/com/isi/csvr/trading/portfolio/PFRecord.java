package com.isi.csvr.trading.portfolio;

import com.isi.csvr.PortfolioInterface;

import java.util.StringTokenizer;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class PFRecord {

    public static final byte NONE = 0;
    public static final byte BUY = 1;
    public static final byte SELL = 2;
    public static final byte OP_BAL = 3;

    public static final byte TXT = 4;
    public static final byte CSV = 5;
    public static final byte QIF = 6;

    public static final byte TRANSACTION_RECORD = 7;
    public static final byte PORTFOLIO_RECORD = 8;

//    private static int     txn_ID;         // Auto increment int field

    private long transID;
    private String sKey;
    private long txnDate;
    //    private int     quantity;
    private float price;                  //
    private float brokerage;              // Commission
    private byte txnType;                // Buy / Sell / Opening Balance
    private int holding;                // Qty
    private String currency;
    private String memo;
    private long pfID;                   // PF ID that belongs this transaction

    public PFRecord() {
//        txn_ID++;
        transID = System.currentTimeMillis(); //txn_ID;
    }

    public float getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(float brokerage) {
        this.brokerage = brokerage;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getQuantity() {
        return holding;
    }

    public void setQuantity(int holding) {
        this.holding = holding;
    }

    public long getId() {
        return transID;
    }
//    public void setId(int txn_ID) {
//        this.txn_ID = txn_ID;
//    }

    public long getPfID() {
        return pfID;
    }

    public void setPfID(long pfID) {
        this.pfID = pfID;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
//    public int getQuantity() {
//        return quantity;
//    }
//    public void setQuantity(int quantity) {
//        this.quantity = quantity;
//    }

    public String getSKey() {
        return sKey;
    }

    public void setSKey(String sKey) {
        this.sKey = sKey;
    }

    public long getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(long txnDate) {
        this.txnDate = txnDate;
    }

    public byte getTxnType() {
        return txnType;
    }

    public void setTxnType(byte txnType) {
        this.txnType = txnType;
    }

    public void retrieveTransaction(String sData) {
        String sKey = null;
        StringTokenizer st = new StringTokenizer(sData, "|", false);
        sKey = st.nextToken();
        if (!PortfolioInterface.isContainedSymbol(sKey))
            PortfolioInterface.setSymbol(true, sKey);
        this.setSKey(sKey);
        this.setTxnDate(Long.parseLong(st.nextToken()));
        this.setPrice(Float.parseFloat(st.nextToken()));
        this.setBrokerage(Float.parseFloat(st.nextToken()));
        this.setTxnType(Byte.parseByte(st.nextToken()));
        this.setQuantity(Integer.parseInt(st.nextToken()));
        this.setCurrency(st.nextToken());
        this.setPfID(Long.parseLong(st.nextToken()));
        this.transID = Long.parseLong(st.nextToken());
//        String sText = st.nextToken();
//        if (sText
        try {
            sKey = st.nextToken();
            if (sKey.equals("null"))
                sKey = "";
            this.setMemo(sKey); //st.nextToken());
        } catch (Exception ex) {
        }
        sKey = null;
        st = null;
//        return transID;
    }

    public String toString(byte fileType) {
        String delim = "";
        switch (fileType) {
            case TXT:
                delim = "|";
                break;
            case CSV:
                delim = ",";
                break;
            case QIF:
                delim = "|";
                break;
        }
//        if (memo == null)
//            memo = "";
        return TRANSACTION_RECORD + delim + sKey + delim + txnDate + delim + price + delim + brokerage + delim + txnType + delim +
                holding + delim + currency + delim + pfID + delim + transID + delim + memo + delim + "\n";
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}