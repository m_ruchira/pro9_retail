package com.isi.csvr.trading.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.shared.TradeKey;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;

import javax.swing.*;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.View;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

//////////////////////////////
/*
VALUE=Value|Value
LBL_PF_REPORT_PERCENT=Percent|Percent
LBL_PF_REPORT_STOCKS=Stocks|Stocks
LBL_PF_REPORT_CASH=Cash|Cash
LBL_PF_REPORT_TOTAL=Total Value|Total Value
LBL_PF_REPORT_SYMBOL_DIVERSIFICATION=Symbol Diversification|Symbol Diversification
LBL_PF_REPORT_ASSET_CLASS=Asset Class|Asset Class
SYMBOL=Symbol|Symbol
LBL_PF_REPORT_ASSET_CLASS_DIVERSIFICATION=Asset Class Diversification|Asset Class Diversification
SECTOR=Sector|Sector
LBL_PF_REPORT_SECTOR_DIVERSIFICATION=Sector Diversification|Sector Diversification
LBL_PF_REPORT_ACCOUNT=Account|Account
LBL_PF_REPORT_ACCOUNT_SUMMARY=Account Summary|Account Summary
LBL_PF_REPORT_NO_PORTFOLIOES=No Portfolioes Selected|No Portfolioes Selected

LBL_PF_REPORT_TITLE=UniQuotes Portfolio Review|UniQuotes Portfolio Review
LBL_PF_REPORT_VAL_SUMMARY=Valuation Summary|Valuation Summary
LBL_PF_REPORT_PFNAMES_ASAT_DATE=[PFNAMES] - as at [DATE]|[PFNAMES] - as at [DATE]

 */
//////////////////////////////

public class ReportPanel extends JPanel implements Printable {

    public static boolean isLTR = Language.isLTR();
    private static Image uniQuotesLogo = null;
    final byte ALIGN_LEFT = 0;
    final byte ALIGN_CENTER = 1;
    final byte ALIGN_RIGHT = 2;
    final int[] columnArray = {2, 5, 8, 9, 10, 11, 14, 15, 16};
    final int TABLE_LEFT = 15;
    final int TOP = 10;
    private int vPos = TOP;
    final int SHADOW_ADJ = 4;
    final int PREFERRED_WIDTH = 800;// 650;
    final int FOOTER_HEIGHT = 18;
    final int SPACING = 2;
    final Font fontBold16 = Theme.getDefaultFont(Font.BOLD, 16);
    private Font fontTitle = fontBold16;
    final Font fontPlain12 = Theme.getDefaultFont(Font.PLAIN, 12);
    private Font fontPlain = fontPlain12;
    final Font fontBold12 = Theme.getDefaultFont(Font.BOLD, 12);
    private Font fontBold = fontBold12;
    final Font fontBold14 = Theme.getDefaultFont(Font.BOLD, 14);
    final Font fontPlain10 = Theme.getDefaultFont(Font.PLAIN, 10);
    final Font fontBold10 = Theme.getDefaultFont(Font.BOLD, 10);
    final Font fontPlain8 = Theme.getDefaultFont(Font.PLAIN, 8);
    public int CHART_LEFT = 60;
    public int CHART_HEIGHT = 130;
    String strValue = Language.getString("VALUE");
    String strPercent = Language.getString("PERCENT");
    String strAssetClass = Language.getString("ASSET_CLASS");
    String strSymbol = Language.getString("SYMBOL");
    String strTotal = Language.getString("TOTAL_VALUE");
    String strAssetClassTitle = Language.getString("ASSET_CLASS_DIVERSIFICATION");
    String strSymbolTitle = Language.getString("SYMBOL_DIVERSIFICATION");
    String strSector = Language.getString("SECTOR");
    String strSectorTitle = Language.getString("SECTOR_DIVERSIFICATION");
    String strAccount = Language.getString("ACCOUNT");
    String strAccountTitle = Language.getString("ACCOUNT_SUMMARY");
    String strNoPortfolioes = Language.getString("REPORT_NO_PORTFOLIOES");
    String strValSummary = Language.getString("VALUATION_SUMMARY");
    String strPortfolioNames = Language.getString("REPORT_PFNAMES_ASAT_DATE");
    String strReportTitle = Language.getString("PORTFOLIO_REPORT_TITLE");
    String[] saTypes = {Language.getString("STOCKS"),
            Language.getString("CASH")
    };
    String strExchange = Language.getString("EXCHANGE");  // Added - Pramoda
    String strExchangeTitle = Language.getString("EXCHANGE_DIVERSIFICATION"); //Added - Pramoda
    int COLUMN_COUNT = columnArray.length;
    int halfW = 300;
    int pageW = 600;
    int valAdj = 170;
    int percAdj = 70;
    int colWidth = 80;
    int LineGap = 18;
    int titleGap = 30;
    int ColumnGap = 10;
    Color[] colors = SharedMethods.getChartColors();
    Color headingColor = new Color(10, 10, 100);
    int colorCount = colors.length;
    int printItemIndex = 0;
    int prePageIndex = -1;
    TWDateFormat sdf = new TWDateFormat("dd MMM yyyy");
    TWDecimalFormat dateFormatter = new TWDecimalFormat("#,##0.00");
    TWDateFormat dateTimeFormatter = new TWDateFormat(" dd/MM/yyyy - HH:mm:ss ");
    private TradingPortfolioWindow pfWindow = null;
    private ReportElement[] repElements = null;
    private ArrayList selectedPortfoioList = new ArrayList();

    public ReportPanel(TradingPortfolioWindow pfWindow) {
        this.pfWindow = pfWindow;
        loadFooterImage();
    }

    public void paint(Graphics g) {
        super.paint(g);
        drawOnGraphics(g, this.getWidth(), false);
    }

    public void drawOnGraphics(Graphics g, int w, boolean isPrinting) {
        initPaint(g, w, isPrinting);
        paintTitle(g);

        if (repElements == null) {
            paintEmptyMsg(g);
        } else {
            drawValuationTable(g, TABLE_LEFT - 10, vPos, 0, isPrinting);
            updateReport();
            for (int i = 0; i < repElements.length; i++) {
                drawChartOnGraphics(g, repElements[i], halfW, vPos);
                drawTableOnGraphics(g, repElements[i], TABLE_LEFT, vPos);
            }
        }
    }

    private void paintEmptyMsg(Graphics g) {
        g.setColor(headingColor);
        g.setFont(fontBold);
        //g.drawString(strNoPortfolioes, TABLE_LEFT, vPos);
        drawString(g, strNoPortfolioes, TABLE_LEFT, vPos, 0, ALIGN_LEFT, 0);
    }

    private void paintTitle(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(fontTitle);
        vPos += 10;
        String str;
//        if(Language.isLTR()){
        str = strReportTitle + " ( " + pfWindow.getSelectedCurrency() + " ) ";
//        }else{
//            str =  " ( " + pfWindow.getSelectedCurrency()  + " ) " + strReportTitle;
//        }

        drawString(g, str, TABLE_LEFT - 10, vPos, 0, ALIGN_LEFT, 0);
        str = null;
        g.setFont(fontPlain);
        vPos += 15;
        drawString(g, getTitleSubString(), TABLE_LEFT - 10, vPos, 0, ALIGN_LEFT, 0);
        vPos += titleGap;
    }

    private String getTitleSubString() {
        String result = strPortfolioNames;
        int size = selectedPortfoioList.size();
        String tmpS = "";
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                if (i > 0) tmpS += ", ";
                tmpS += (String) selectedPortfoioList.get(i);
            }
        }
        result = result.replaceAll("\\[PFNAMES\\]", tmpS);
        tmpS = sdf.format(new Date(System.currentTimeMillis()));
        result = result.replaceAll("\\[DATE\\]", tmpS);
        return result;
    }

    private void initPaint(Graphics g, int w, boolean isPrinting) {
        pageW = w;
        halfW = w * 3 / 5;
        if (isPrinting) {
            CHART_HEIGHT = Math.min(130, w * 2 / 5 - 20);
            LineGap = 14;
            fontTitle = fontBold14;
            fontPlain = fontPlain10;
            fontBold = fontBold10;
        } else {
            CHART_HEIGHT = 160;
            LineGap = 18;
            fontTitle = fontBold16;
            fontPlain = fontPlain12;
            fontBold = fontBold12;
        }
        CHART_LEFT = Math.min(60, (w * 2 / 5 - CHART_HEIGHT) / 2);
        g.setFont(fontBold);
        valAdj = colWidth + g.getFontMetrics().stringWidth(strValue);
        percAdj = g.getFontMetrics().stringWidth(strPercent);
        vPos = TOP;
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    private void drawChartOnGraphics(Graphics g, ReportElement rEle, int x, int y) {
        float startAngle = 0;
        double currAngle = 0;
        double[] fa = rEle.getPercents();
        if (!isLTR) {
            x = pageW - x - 2 * CHART_LEFT - CHART_HEIGHT;
        }
        g.setColor(Color.LIGHT_GRAY);
        g.fillOval(x + CHART_LEFT + SHADOW_ADJ, y + SHADOW_ADJ, CHART_HEIGHT, CHART_HEIGHT);
        for (int i = 0; i < fa.length; i++) {
            g.setColor(colors[i % colorCount]);
            currAngle = 360f * fa[i] / 100f; // in degrees
            g.fillArc(x + CHART_LEFT, y, CHART_HEIGHT, CHART_HEIGHT,
                    Math.round(startAngle), Math.round((float) currAngle));
            startAngle += currAngle;
        }
        g.setColor(Color.BLACK);
        float pi = 355f / 113f;
        currAngle = 0f;
        for (int i = 0; i < fa.length; i++) {
            currAngle += 360f * fa[i] / 100f * pi / 180f; // in radians
            int r = CHART_HEIGHT / 2;
            int cX = x + CHART_LEFT + r;
            int cY = y + r;
            g.drawLine(cX, cY, cX + (int) Math.round(r * Math.cos(currAngle)),
                    cY - (int) Math.round(r * Math.sin(currAngle)));
        }
        g.drawOval(x + CHART_LEFT, y, CHART_HEIGHT, CHART_HEIGHT);
    }

    private void drawTableOnGraphics(Graphics g, ReportElement rEle, int x, int y) {
        int newY = y;
        g.setColor(headingColor);
        g.setFont(fontBold);
        drawString(g, rEle.getDescription(), x - 10, newY, 0, ALIGN_LEFT, 0);
        newY += 2 * LineGap;
        x += 20;
        g.setColor(Color.BLACK);
        drawString(g, rEle.getTitle(), x, newY, 0, ALIGN_LEFT, 0);
        drawStringRightAligned(g, strValue, halfW - colWidth, newY);
        drawStringRightAligned(g, strPercent, halfW, newY);
        g.setFont(fontPlain);
        for (int i = 0; i < rEle.getElements().length; i++) {
            newY += LineGap;
            g.setColor(colors[i % colorCount]);
            fillRect(g, x - 20, newY - 11, 12, 12);
            g.setColor(Color.BLACK);
            drawRect(g, x - 20, newY - 11, 12, 12);
            drawString(g, rEle.getElements()[i], x, newY, 0, ALIGN_LEFT, 0);
            drawNumberRightAligned(g, rEle.getValues()[i], halfW - colWidth, newY, false);
            drawNumberRightAligned(g, rEle.getPercents()[i], halfW, newY, true);
        }
        newY += (LineGap * 3 / 2);
        drawString(g, strTotal, x, newY, 0, ALIGN_LEFT, 0);
        drawNumberRightAligned(g, rEle.getTotal(), halfW - colWidth, newY, false);
        drawNumberRightAligned(g, 100f, halfW, newY, true);
        newY -= 15;
        drawLineRightAligned(g, halfW - colWidth + ColumnGap, newY, halfW, newY);
        drawLineRightAligned(g, halfW - 2 * colWidth + ColumnGap, newY, halfW - colWidth, newY);
        newY += 20;
        drawLineRightAligned(g, halfW - colWidth + ColumnGap, newY, halfW, newY);
        drawLineRightAligned(g, halfW - 2 * colWidth + ColumnGap, newY, halfW - colWidth, newY);
        newY += 2;
        drawLineRightAligned(g, halfW - colWidth + ColumnGap, newY, halfW, newY);
        drawLineRightAligned(g, halfW - 2 * colWidth + ColumnGap, newY, halfW - colWidth, newY);
        // do something to calc vPos next !!!
        vPos = Math.max(newY, y + CHART_HEIGHT) + 2 * LineGap;
    }

    private void drawNumberRightAligned(Graphics g, double num, int x, int y, boolean isPercent) {
        String sNum = dateFormatter.format(num);
        if (isPercent) sNum += "%";
        int sLen = g.getFontMetrics().stringWidth(sNum);
        if (isLTR) {
            x = x - sLen;
        } else {
            x = pageW - x + colWidth - sLen;
        }
        g.drawString(sNum, x, y);
    }

    private void drawStringRightAligned(Graphics g, String text, int x, int y) {
        int sLen = g.getFontMetrics().stringWidth(text);
        if (isLTR) {
            x = x - sLen;
        } else {
            x = pageW - x + colWidth - sLen;
        }
        g.drawString(text, x, y);
    }

    private void drawLineRightAligned(Graphics g, int x1, int y1, int x2, int y2) {
        if (!isLTR) {
            x1 = pageW - x1 + ColumnGap;
            x2 = pageW - x2 + ColumnGap;
        }
        g.drawLine(x1, y1, x2, y2);
    }

    public void updateReport() {
        //update data here//********
        //no drawing is done//******

        // following are the 4 catogaries to be displayed.
        // 1. Asset Class
        // 2. Symbol Diversification
        // 3. Sector Diversification
        // 4. Account Summary
        final int CATAGORIES = 5; // Changed to 4 for new Exchange report - Pramoda
        String[] selectedPFIDs = pfWindow.getSelectedPFIDs();
        if ((selectedPFIDs == null) || (selectedPFIDs.length <= 0)) {
            repElements = null;
            return;
        }
        //reduces its size by one, because of removal of sectors
        repElements = new ReportElement[CATAGORIES - 1];

        //Symbol Diversification
        String[] saSymbols = getSymbolArray();
        String[][] saSymbolswithPF = getSymbolArrayWithPFID();
        double[] faMktValues = extractMktValueArray(saSymbolswithPF);
        saSymbols = extractShortDescArray(saSymbols);
        ReportElement eleSymblDivers = new ReportElement(strSymbol, saSymbols,
                faMktValues, strSymbolTitle);
        repElements[0] = eleSymblDivers;

        //InvestmentType
        double[] faValues = {pfWindow.getValuationModel().getMarketValue(false),
                pfWindow.getCashBalanceForSelectedPFs()
        };
        ReportElement eleInvsType = new ReportElement(strAssetClass, saTypes,
                faValues, strAssetClassTitle);
        repElements[1] = eleInvsType;


        //Sector Diversification
        //removed for anagram international version
//        Hashtable hashSectors = getSectorsHash();
//          int size = hashSectors.size();
//          String[] saSectors = new String[size];
//        double[] faSectors = new double[size];
//          extractKeysAndElements(hashSectors, saSectors, faSectors);
//          ReportElement eleSectors = new ReportElement(strSector, saSectors,
//                  faSectors, strSectorTitle);
//          hashSectors = null;
//        repElements[2] = eleSectors;

        //Exchange Summary - Added - Pramoda
        Hashtable hashExchange = getExchangeHash();
        int eSize = hashExchange.size();
        String[] saExchanges = new String[eSize];
        double[] faExchanges = new double[eSize];
        extractKeysAndElements(hashExchange, saExchanges, faExchanges);
        ReportElement eleExchanges = new ReportElement(strExchange, saExchanges,
                faExchanges, strExchangeTitle);
        hashExchange = null;
        repElements[2] = eleExchanges;

        //Accounts summary
        Hashtable hashAccounts = getAccountsHash();
        int aSize = hashAccounts.size();
        String[] saAccounts = new String[aSize];
        double[] faAccounts = new double[aSize];
        extractKeysAndElements(hashAccounts, saAccounts, faAccounts);
        ReportElement eleAccounts = new ReportElement(strAccount, saAccounts,
                faAccounts, strAccountTitle);
        hashAccounts = null;
        repElements[3] = eleAccounts;


        this.setPreferredSize(new Dimension(PREFERRED_WIDTH, getPanelHeight()));
        //repaint();
    }


    // Added - Pramoda
    private Hashtable getExchangeHash() {
        Hashtable hashExchange = new Hashtable();
        ArrayList list = TradePortfolios.getInstance().getTransactionList();
        TransactRecord txRec;
        TradeKey key;
        String exchange;
        ValuationModel vm = pfWindow.getValuationModel();
        double[] marketValue;
        for (int i = 0; i < list.size(); i++) {
            txRec = (TransactRecord) list.get(i);
            if (!portfolioIsSelected(txRec.getPfID())) {
                continue;
            }
            key = txRec.getKey();
            exchange = txRec.getExchange();
            marketValue = (double[]) hashExchange.get(exchange);
            if (marketValue == null) {
                marketValue = new double[1];
                marketValue[0] = 0f;
                hashExchange.put(exchange, marketValue);
            }
            double txnValue = vm.getMktValuePerShareForSymbol(key, txRec.getPfID()) * Math.abs(txRec.getQuantity());
            if (txRec.getTxnType() == PFUtilities.SELL) {
                marketValue[0] -= txnValue;
            } else {
                marketValue[0] += txnValue;
            }
        }
        list = null;
        return hashExchange;
    }


    private String[] extractShortDescArray(String[] saSymbols) {
        String[] sa = new String[saSymbols.length];
        for (int i = 0; i < saSymbols.length; i++) {
            sa[i] = PortfolioInterface.getShortDescription(saSymbols[i]);
        }
        return sa;
    }

//    private Hashtable getSectorsHash() {
//        try {
//            Hashtable hashSectors = new Hashtable();
//            ArrayList list = TradePortfolios.getInstance().getValuationList();
//            //String[] sa = new String[list.size()];
//            String key;
//            String sectorDescr;
//            ValuationModel vm = pfWindow.getValuationModel();
//            double[]  marketValue;
//            for (int i = 0; i < list.size(); i++) {
//                key = ((ValuationRecord) list.get(i)).getSKey();
//                //Pro
//    //			sectorDescr = IndexStore.getDescription(DataStore.getSectorCode(key));
//                //Reg
//                String exchange = SharedMethods.getExchangeFromKey(key);
//                String sectorCode = DataStore.getSharedInstance().getSectorCode(key);
//                sectorDescr = SectorStore.getSharedInstance().getSector(exchange, sectorCode).getDescription();
//                //IndexStore.getDescription(DataStore.getSharedInstance().getSectorCode(key));
//                marketValue = (double[]) hashSectors.get(sectorDescr);
//                if (marketValue == null) {
//                    marketValue = new double[1];
//                    marketValue[0] = 0f;
//                    hashSectors.put(sectorDescr, marketValue);
//                }
//               // marketValue[0] += vm.getMarketValueForSymbol(key);
//            }
//            list = null;
//            return hashSectors;
//        } catch (Exception e) {
//            return new Hashtable();
//        }
//    }

    private boolean portfolioIsSelected(String pfID) {
        String[] selectedPFIDs = pfWindow.getSelectedPFIDs();
        for (int i = 0; i < selectedPFIDs.length; i++) {
            if (selectedPFIDs[i].equals(pfID)) return true;
        }
        return false;
    }

    private Hashtable getAccountsHash() {
        Hashtable hashAcc = new Hashtable();
        ArrayList list = TradePortfolios.getInstance().getTransactionList();
        //String[] sa = new String[list.size()];
        TradeKey key;
        String accName;
        TransactRecord txRec;
        ValuationModel vm = pfWindow.getValuationModel();
        double[] marketValue;
        for (int i = 0; i < list.size(); i++) {
            txRec = (TransactRecord) list.get(i);
            if (!portfolioIsSelected(txRec.getPfID())) {
                continue;
            }
            key = txRec.getKey();
            accName = TradePortfolios.getInstance().getPortfolio(txRec.getPfID()).getName();
            marketValue = (double[]) hashAcc.get(accName);
            if (marketValue == null) {
                marketValue = new double[1];
                marketValue[0] = 0f;
                hashAcc.put(accName, marketValue);
            }
            double txnValue = vm.getMktValuePerShareForSymbol(key, txRec.getPfID()) * Math.abs(txRec.getQuantity());
            if (txRec.getTxnType() == PFUtilities.SELL) {
                marketValue[0] -= txnValue;
            } else {
                marketValue[0] += txnValue;
            }
        }
        //add the cash balance for each portfolio
        TradingPortfolioRecord pfRec;
        list = TradePortfolios.getInstance().getPortfolioList();
        selectedPortfoioList.clear();
        for (int i = 0; i < list.size(); i++) {
            pfRec = (TradingPortfolioRecord) list.get(i);
            if (!portfolioIsSelected(pfRec.getPortfolioID())) {
                continue;
            }
            accName = pfRec.getName();
            selectedPortfoioList.add(accName);
            marketValue = (double[]) hashAcc.get(accName);
            if (marketValue == null) {
                marketValue = new double[1];
                marketValue[0] = 0f;
                hashAcc.put(accName, marketValue);
            }

            // marketValue[0] += 100;//todo check cash balance-  pfRec.getCashBalance();
        }

        list = null;
        return hashAcc;
    }

    private void extractKeysAndElements(Hashtable hashSectors, String[] sa, double[] fa) {
        int count = 0;
        String key;
        Enumeration keys = hashSectors.keys();
        while (keys.hasMoreElements()) {
            key = (String) keys.nextElement();
            sa[count] = key;
            fa[count] = ((double[]) hashSectors.get(key))[0];
            count++;
            if (count >= sa.length) break;
        }
    }

    private String[] getSymbolArray() {
        ArrayList list = TradePortfolios.getInstance().getValuationList();
        String[] sa = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            sa[i] = ((ValuationRecord) list.get(i)).getSKey().getTradeKey();
            //sa[i] = PortfolioInterface.getCompanyName(((ValuationRecord)list.get(i)).getSKey());
        }
        return sa;
    }

    private String[][] getSymbolArrayWithPFID() {
        ArrayList list = TradePortfolios.getInstance().getValuationList();
        String[][] sa = new String[list.size()][2];
        for (int i = 0; i < list.size(); i++) {
            sa[i][0] = ((ValuationRecord) list.get(i)).getSKey().getTradeKey() + Constants.KEY_SEPERATOR_CHARACTER + ((ValuationRecord) list.get(i)).getSKey().getInstrumentType();
            sa[i][1] = ((ValuationRecord) list.get(i)).getPortfolioID();
            //sa[i] = PortfolioInterface.getCompanyName(((ValuationRecord)list.get(i)).getSKey());
        }
        return sa;
    }

    private double[] extractMktValueArray(String[][] keys) {
        double[] fa = new double[keys.length];
        for (int i = 0; i < keys.length; i++) {
            fa[i] = pfWindow.getValuationModel().getMarketValueForSymbol(keys[i][0], keys[i][1]);
        }
        return fa;
    }

    public int getPanelHeight() {
        //initPaint
        //halfW = w*3/5;
        CHART_HEIGHT = 160;
        LineGap = 18;
        //CHART_LEFT = Math.min(60, (w*2/5-CHART_HEIGHT)/2);
        int vertPos = TOP;

        //draw title
        vertPos += 10;
        vertPos += 15;
        vertPos += titleGap;

        //draw Valuation table
        int rowCnt = pfWindow.getValuationModel().getTable().getModel().getRowCount();
        int colCnt = COLUMN_COUNT;
        vertPos += 2 * LineGap;
        int tableTop = vertPos - 14;
        int NewLineGap = LineGap + 2;
        int tableBot = tableTop + (rowCnt + 1) * NewLineGap;
        vertPos = tableBot + 2 * LineGap;

        if (repElements != null)
            for (int i = 0; i < repElements.length; i++) {
                ReportElement rEle = repElements[i];
                int newY = vertPos;
                newY += 2 * LineGap;
                for (int j = 0; j < rEle.getElements().length; j++) {
                    newY += LineGap;
                }
                newY += (LineGap * 3 / 2);
                newY -= 15;
                newY += 20;
                newY += 2;
                // do something to calc vPos next !!!
                vertPos = Math.max(newY, vertPos + CHART_HEIGHT) + 2 * LineGap;
            }
        return vertPos + 50;
    }

    /**
     * The method @print@ must be implemented for @Printable@ interface.
     * Parameters are supplied by system.
     */
    public int print(Graphics g, PageFormat pf, int pageIndex) throws
            PrinterException {

//		System.out.println("____________________________________________________________");
//		System.out.println("start printing pageIndex "+pageIndex);
        if (prePageIndex != pageIndex) {
            if (printItemIndex >= repElements.length) {
//				System.out.println("printing over "+(pageIndex+1)+" page - printItemIndex = "+printItemIndex);
//				System.out.println("____________________________________________________________");
                printItemIndex = 0;
                prePageIndex = -1;
                return Printable.NO_SUCH_PAGE;
            } else {
                prePageIndex = pageIndex;
                return Printable.PAGE_EXISTS;
            }
        } else {
            //prePageIndex = pageIndex;
        }

        Graphics2D g2 = (Graphics2D) g;

        int pageHeight = (int) Math.round(pf.getImageableHeight()); //height of printer page
        int pageWidth = (int) Math.round(pf.getImageableWidth()); //width of printer page

//		if (pageIndex > 1) {
//			return Printable.NO_SUCH_PAGE;
//		}

        //shift Graphic to line up with beginning of print-imageable region
        g2.translate(pf.getImageableX(), pf.getImageableY());

        //drawOnGraphics(g, (int)Math.min(pageWidth, PREFERRED_WIDTH), true);
        int w = (int) Math.min(pageWidth, PREFERRED_WIDTH);
        initPaint(g, w, true);
        if (pageIndex == 0) {
            paintTitle(g);
            drawValuationTable(g, TABLE_LEFT - 10, vPos, pageWidth, true);
        }

        if (repElements == null) {
            paintEmptyMsg(g);
            if (pageIndex == 0) {
                return Printable.PAGE_EXISTS;
            } else {
                prePageIndex = -1;
                return Printable.NO_SUCH_PAGE;
            }
        } else {
            if (printItemIndex >= repElements.length) {
//				System.out.println("printing over "+(pageIndex+1)+" page - printItemIndex = "+printItemIndex);
//				System.out.println("____________________________________________________________");
                printItemIndex = 0;
                prePageIndex = -1;
                return Printable.NO_SUCH_PAGE;
            }
            int tmpIndx = printItemIndex;
            for (int i = tmpIndx; i < repElements.length; i++) {
                if (notEnoughSpace(pageHeight, vPos)) {
//					System.out.println("printed 1 "+(pageIndex+1)+"page(s)");
                    drawPageFooter(g, TABLE_LEFT - 10, vPos, pageHeight, pageWidth);
                    return Printable.PAGE_EXISTS;
                }
                drawChartOnGraphics(g, repElements[i], halfW, vPos);
                drawTableOnGraphics(g, repElements[i], TABLE_LEFT, vPos);
//				System.out.println("drawing item "+i+" vPos = "+vPos);
                printItemIndex++;
            }
        }

//		System.out.println("printed 2 "+(pageIndex+1)+"page(s)");
        drawPageFooter(g, TABLE_LEFT - 10, vPos, pageHeight, pageWidth);
        return Printable.PAGE_EXISTS;
    }


    private void drawPageFooter(Graphics g, int x, int vPos, int imgHt, int pageWd) {
        int newH = 15;
        if (vPos + FOOTER_HEIGHT <= imgHt) {
            String dateTag = PortfolioInterface.getFormattedPrintingTime();
            g.setColor(Color.GRAY);
            g.setFont(fontPlain8);
            drawString(g, Language.getString("PRINT_FOOTER_CLIENT_TITLE"), x, imgHt - 2, pageWd - x, ALIGN_LEFT, SPACING);
            drawString(g, dateTag, x, imgHt - 2, pageWd - x, ALIGN_RIGHT, SPACING);
        }
    }

    private boolean notEnoughSpace(int imageableH, int vertPos) {
//		System.out.println("printItemIndex "+printItemIndex+" vertPos "+vertPos+" imageableH "+imageableH);
        if (vertPos <= TOP) return false;
        ReportElement rEle = repElements[printItemIndex];
        int newY = vertPos;
        newY += 2 * LineGap;
        for (int j = 0; j < rEle.getElements().length; j++) {
            newY += LineGap;
        }
        newY += (LineGap * 3 / 2);
        newY -= 15;
        newY += 20;
        newY += 2;
        // do something to calc vPos next !!!
        newY = Math.max(newY, vertPos + CHART_HEIGHT);
//		System.out.println("returning "+(newY>imageableH));
        return (newY > imageableH);
    }

    private void drawValuationTable(Graphics g, int x, int y, int pageW, boolean isPrinting) {
        int rowCnt = pfWindow.getValuationModel().getTable().getModel().getRowCount();
        //int colCnt = COLUMN_COUNT;
        JLabel renderer;
        int sw = 0;
        JTable table = pfWindow.getValuationTable().getTable();
        int[] colWidths = new int[COLUMN_COUNT];
        FontMetrics fmPln = g.getFontMetrics(fontPlain);
        FontMetrics fmBld = g.getFontMetrics(fontBold);
        Document d;
        for (int j = 0; j < COLUMN_COUNT; j++) {
            int currCol = columnArray[j];
//            colWidths[j] = fmBld.stringWidth(table.getColumnName(currCol));
            colWidths[j] = fmBld.stringWidth(table.getColumn(currCol + "").getHeaderValue().toString());
//            System.out.println(currCol + " " + table.getIndexSymbol(currCol));
            for (int i = 0; i < rowCnt; i++) {
                renderer = (JLabel) table.getCellRenderer(i, table.convertColumnIndexToView(currCol)).getTableCellRendererComponent(table,
                        table.getModel().getValueAt(i, currCol), false, false, i, table.convertColumnIndexToView(currCol));
//                renderer = (JLabel) table.getCellRenderer(i, table.convertColumnIndexToView(currCol)).getTableCellRendererComponent(table,
//                        table.getModel().getValueAt(i, currCol), false, false, i, table.convertColumnIndexToView(currCol));
                if (i < rowCnt - 1) {
                    View v = (View) renderer.getClientProperty(BasicHTML.propertyKey);
                    if (v != null) {
                        d = v.getDocument();
                        try {
                            sw = fmPln.stringWidth(d.getText(0, d.getLength()).trim());
                        } catch (BadLocationException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            System.out.println("Error at rendering valuation Table....");
                        }
                    } else {
                        sw = fmPln.stringWidth(renderer.getText());
                    }

                } else {
                    sw = fmBld.stringWidth(renderer.getText());
//                    System.out.println(table.getIndexSymbol(currCol)+ " " +sw +" : " +renderer.getText());

                }
                colWidths[j] = Math.max(colWidths[j], sw);
            }
        }

        //drawing title
        int colLeft = x + SPACING;
        int tmpY = y;
        g.setFont(fontBold);
        g.setColor(headingColor);
        //g.drawString(strValSummary, x, tmpY);
        drawString(g, strValSummary, x, tmpY, 0, ALIGN_LEFT, 0);

        //calculating reduce ratio for print and scaling..
        double tableR = x;
        for (int i = 0; i < colWidths.length; i++) {
            tableR += (colWidths[i] + 2 * SPACING);
        }
        double ratio = 1d;
        int tmpPageW = this.pageW;
        Graphics2D g2D = null;
        if (isPrinting) {
            if (pageW < tableR) {
                ratio = pageW / tableR;
            }
            //System.out.println("table width reduced ratio "+ratio);
            g2D = (Graphics2D) g;
            g2D.scale(ratio, 1);
            this.pageW = (int) Math.round(pageW / ratio);
            //System.out.println("pageW "+tmpPageW+" adjuasted pageW "+pageW);
        }


        tmpY += 2 * LineGap;
        int tableTop = tmpY - 15;
        if (isPrinting) tableTop = tmpY - 13;
        int NewLineGap = LineGap + 2;
        int topY = tmpY;
        g.setColor(Color.BLACK);
        for (int j = 0; j < COLUMN_COUNT; j++) {
            int currCol = columnArray[j];
            int modelIndex = table.convertColumnIndexToModel(currCol);
            tmpY = topY;
            g.setFont(fontBold);
            //SharedMethods.printLine(table.getIndexSymbol(currCol));
//			drawAlignedCellString(g, table.getIndexSymbol(currCol), colLeft-SPACING,
//							  tmpY, colWidths[j], ALIGN_CENTER, g.getFont());
            drawString(g, table.getColumn(currCol + "").getHeaderValue().toString(), colLeft - SPACING,
                    tmpY, colWidths[j] + 2 * SPACING, ALIGN_CENTER, SPACING);
            //currCol = table.convertColumnIndexToModel(currCol);
            for (int i = 0; i < rowCnt; i++) {
                tmpY += NewLineGap;
                if (i < rowCnt - 1) g.setFont(fontPlain);
                else g.setFont(fontBold);
//                System.out.println(table.getModel().getValueAt(i,modelIndex) + " " + currCol +  " " + table.getIndexSymbol(currCol));
//                renderer = (JLabel) table.getCellRenderer(i, table.convertColumnIndexToView(currCol)).getTableCellRendererComponent(table,
//                        table.getModel().getValueAt(i, modelIndex), false, false, i, table.convertColumnIndexToView(currCol));
                renderer = (JLabel) table.getCellRenderer(i, table.convertColumnIndexToView(currCol)).getTableCellRendererComponent(table,
                        table.getModel().getValueAt(i, currCol), false, false, i, table.convertColumnIndexToView(currCol));

                //System.out.println("text alin column Header "+renderer.getHorizontalAlignment());
//				System.out.println(renderer.getText() + " " + currCol +  " " + table.getIndexSymbol(currCol));
                byte alignment = getTextAlignment(renderer.getHorizontalAlignment());
//				drawAlignedCellString(g, renderer.getText(), colLeft-SPACING, tmpY,
//								  colWidths[j], alignment, g.getFont());
                if (!isLTR) {
                    alignment = toggleAlignment(alignment);
                }
                View v = (View) renderer.getClientProperty(BasicHTML.propertyKey);
                if (v != null) {
                    d = v.getDocument();
                    try {
                        drawString(g, d.getText(0, d.getLength()).trim(), colLeft - SPACING, tmpY, colWidths[j] + 2 * SPACING, alignment, SPACING);
                    } catch (BadLocationException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        System.out.println("Error at rendering valuation Table....");
                    }
                } else {
                    drawString(g, renderer.getText(), colLeft - SPACING, tmpY, colWidths[j] + 2 * SPACING, alignment, SPACING);
                }
            }
            colLeft += (colWidths[j] + 2 * SPACING);
        }

        g.setColor(Color.GRAY);
        int tableBot = tableTop + (rowCnt + 1) * NewLineGap;
        colLeft = x - 1;
        drawLine(g, colLeft, tableTop, colLeft, tableBot);
        for (int i = 0; i < COLUMN_COUNT; i++) {
            colLeft += (colWidths[i] + 2 * SPACING);
            drawLine(g, colLeft, tableTop, colLeft, tableBot);
        }
        int tableRight = colLeft;
        tmpY = tableTop;
        for (int i = 0; i <= rowCnt + 1; i++) {
            drawLine(g, x - 1, tmpY, tableRight, tmpY);
            tmpY += NewLineGap;
        }

        if (isPrinting) {
            g2D.scale(1 / ratio, 1);
            this.pageW = tmpPageW;
            //System.out.println("pageW "+tmpPageW+" restored pageW "+pageW);
        }

        vPos = tableBot + 2 * LineGap;
    }

    private byte toggleAlignment(byte alignment) {
        return (byte) (2 - alignment);
    }

    private byte getTextAlignment(int alignX) {
        if (alignX == JLabel.LEFT) {
            return ALIGN_LEFT;
        } else if (alignX == JLabel.RIGHT) {
            return ALIGN_RIGHT;
        } else return ALIGN_CENTER;
    }

    public void drawAlignedCellString(Graphics g, String text, int x, int y, int width,
                                      byte alignment, Font font) {
        int left = x;
        int strW;
        switch (alignment) {
            case ALIGN_LEFT:
                left = x + SPACING;
                break;
            case ALIGN_RIGHT:
                strW = g.getFontMetrics(font).stringWidth(text);
                left = x + width + SPACING - strW;
                break;
            default:
                strW = g.getFontMetrics(font).stringWidth(text);
                left = x + (width - strW) / 2 + SPACING;
                break;
        }
        g.drawString(text, left, y);
    }

    private void loadFooterImage() {
        File file = null;
        MediaTracker comp = null;
        try {
            uniQuotesLogo = Toolkit.getDefaultToolkit().getImage(".\\images\\Common\\mubasher_" + Language.getLanguageTag() + ".gif");
            comp = new MediaTracker(this);
            comp.addImage(uniQuotesLogo, 0);
            comp.waitForID(0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        file = null;

    }

    private void drawString(Graphics g, String text, int x, int y, int w,
                            byte alignment, int spacing) {
        int left = 0;
        int strW = g.getFontMetrics().stringWidth(text);
        switch (alignment) {
            case ALIGN_LEFT:
                left = x + spacing;
                break;
            case ALIGN_RIGHT:
                left = x + w - spacing - strW;
                break;
            default:
                left = x + (w - strW) / 2;
                break;
        }
        if (!isLTR) {
            left = pageW - (left) - strW;
            //System.out.println("page width *********** "+pageW);
        }

        g.drawString(text, left, y);
    }

    private void fillRect(Graphics g, int x, int y, int w, int h) {
        if (!isLTR) {
            x = pageW - x - w;
        }
        g.fillRect(x, y, w, h);
    }

    private void drawRect(Graphics g, int x, int y, int w, int h) {
        if (!isLTR) {
            x = pageW - x - w;
        }
        g.drawRect(x, y, w, h);
    }

    private void drawLine(Graphics g, int x1, int y1, int x2, int y2) {
        if (!isLTR) {
            x1 = pageW - x1;
            x2 = pageW - x2;
        }
        g.drawLine(x1, y1, x2, y2);
    }

}