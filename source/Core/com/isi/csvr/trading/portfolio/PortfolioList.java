package com.isi.csvr.trading.portfolio;

//import javax.swing.JPopupMenu;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.ui.AccountWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class PortfolioList extends JPopupMenu implements MouseListener {

    private JPanel basePanel;
    private JPanel btnPanel;
    private JCheckBox[] checkPFs;
    //    private Vector      pfVector;
    private TWButton btnOK;
    private TWButton btnCancel;
    private TWButton btnCreatePF;
    private TradingPortfolioWindow parent;
    private AccountWindow accountParent;
    private String[] selIDs;
    private boolean isAccountWindow = false;
    private ArrayList pfRecords;

    public PortfolioList(TradingPortfolioWindow parentIn, String[] selIDs) {
        this.parent = parentIn;
        this.selIDs = selIDs;
        isAccountWindow = false;
        createUI();
        this.setLightWeightPopupEnabled(false);
    }

    public PortfolioList(AccountWindow parentIn, String[] selIDs) {
        this.accountParent = parentIn;
        this.selIDs = selIDs;
        isAccountWindow = true;
        createUI();
        this.setLightWeightPopupEnabled(false);
    }

    public void createUI() {
        pfRecords = TradePortfolios.getInstance().getPortfolioList();
        basePanel = new JPanel(new BorderLayout());
        int noOfElems = pfRecords.size();
        JPanel centerPanel = new JPanel(new GridLayout(noOfElems, 2, 5, 5));
        btnPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 2));
        checkPFs = new JCheckBox[noOfElems];
        if (noOfElems > 0) {
            String pfName = null;
//            TradingPortfolioRecord record = null;
            for (int i = 0; i < pfRecords.size(); i++) {
                final int index = i;
                final TradingPortfolioRecord record = (TradingPortfolioRecord) pfRecords.get(i);
                pfName = record.getName();// + " (" + record.getCurrency() + ")";
                checkPFs[i] = new JCheckBox(pfName);
                if (selIDs != null) {
                    for (int j = 0; j < selIDs.length; j++) {
                        if (record.getPortfolioID().equals(selIDs[j]))
                            checkPFs[i].setSelected(true);
                    }
                }
                centerPanel.add(checkPFs[i]);
                JLabel label = new JLabel(Language.getString("EDIT") + "      ", JLabel.TRAILING);
//                label.setPreferredSize(new Dimension(50,checkPFs[i].getHeight()));
                label.setAlignmentX(JLabel.TRAILING);
                label.setName("" + i);
                label.setForeground(Color.BLUE);
                label.setCursor(new Cursor(Cursor.HAND_CURSOR));
                label.addMouseListener(this);
                centerPanel.add(label);
                label = null;
            }
            pfRecords = null;
            btnOK = new TWButton(Language.getString("OK"));
            btnOK.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    applyCustomFilter();
                    disposePopup();
                }
            });
            btnCancel = new TWButton(Language.getString("CANCEL"));
            btnCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    disposePopup();
                }
            });

            btnOK.setPreferredSize(new Dimension(80, 20));
            btnCancel.setPreferredSize(new Dimension(80, 20));

            GUISettings.setSameSize(btnOK, btnCancel);
            btnPanel.add(btnOK);
            btnPanel.add(btnCancel);
        }
        basePanel.add(centerPanel, BorderLayout.CENTER);
        basePanel.add(btnPanel, BorderLayout.SOUTH);

        this.add(basePanel);
        GUISettings.applyOrientation(this);

    }

    private void editPortfolio() {

    }

    private void applyCustomFilter() {
        // change start
        try {
            int counter = 0;
            String[] selPFIDs = null;
            int noOfItemsSelected = 0;
            for (int i = 0; i < checkPFs.length; i++) {
                if (checkPFs[i].isSelected()) {
                    noOfItemsSelected++;
                }
            }
            selPFIDs = new String[noOfItemsSelected];
            for (int i = 0; i < checkPFs.length; i++) {
                if (checkPFs[i].isSelected()) {
                    selPFIDs[counter++] = (TradePortfolios.getInstance().getPortfolio(i)).getPortfolioID();
                }
            }
            if (!isAccountWindow) {
                parent.setSelectedPFIDs(selPFIDs);
                parent.activateCurrentSymbols();
                parent.displayCashBalance();
                parent.setFilterDescription(false, null, null, null, PFUtilities.NONE);
            } else {
                accountParent.setSelectedPFIDs(selPFIDs);
//                accountParent.activateCurrentSymbols();
//                accountParent.displayCashBalance();
//                accountParent.setFilterDescription(false, null, null, null, PFUtilities.NONE);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        // change end


    }

    private void hidePopup() {
        this.setVisible(false);
    }

    private void disposePopup() {
        this.setVisible(false);
        checkPFs = null;
        parent = null;
//        pfVector.removeAllElements();
//        pfVector    = null;
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof JLabel) {
            try {
                int index = Integer.parseInt(((JLabel) e.getSource()).getName());
//                System.out.println("mouse clicked in edit label ="+index);
                System.out.println((TradePortfolios.getInstance().getPortfolio(index)).getPortfolioID());
                EditPortfolio edit = new EditPortfolio((TradePortfolios.getInstance().getPortfolio(index))); //(TradePortfolios.getInstance().getPortfolio(index)));
                edit.setVisible(true);
            } catch (NumberFormatException e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }
}