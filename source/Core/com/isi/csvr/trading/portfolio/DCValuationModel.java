// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.trading.portfolio;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.trading.datastore.TPlusStore;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

public class DCValuationModel
        extends CommonTable
        implements TableModel, CommonTableInterface {
    private TradePortfolios tradePortfolios;
    //private ArrayList dataStore;                                                                                   s
//    private ValuationModel  selfReference; // Holds the self reference
    private boolean isCashIncluded = false;
    private TradingPortfolioWindow pfWindow;
    private String sCash;
    private ArrayList<PfValutionRecord> pfValRecords;
    private String message = "";

    private String sPortFolioDetail = "";
    private String sPFDetailCurrency = "";

    /**
     * Constructor
     */


    public DCValuationModel(ArrayList pfvalRecordsIn) { //,ArrayList valListIn) {
        this.pfValRecords = pfvalRecordsIn;
        /******************************************************************************************/
        //setup the data store using comploete recordsset

//                                                int iPfValRecs = pfValRecords.size();
//                                                ArrayList<ValuationRecord> aTempValRecs =new ArrayList<ValuationRecord>();
//                                                for (int i = 0 ;i< iPfValRecs;i++){
//
//                                                    PfValutionRecord tempPfValRec = (PfValutionRecord) pfValRecords.get(i);
//
//                                                    int iType = tempPfValRec.getRecType();
//                                                    if (iType == PfValutionRecord.VALRECORD ){
//
//                                                        aTempValRecs.add(tempPfValRec.getValRecord());
//
//                                                    }
//                                                }
        // this.dataStore = valListIn;
        //this.dataStore = pfvalRecordsIn;
        //        selfReference = this;

        /*********************************************//*********************************************/
        //    this.dataStore = pfvalRecordsIn;
        tradePortfolios = TradePortfolios.getInstance();
        sCash = Language.getString("CASH");
        sPortFolioDetail = Language.getString("PORTFOLIO_DETAIL");
        sPFDetailCurrency = Language.getString("PFDETAIL_CURRENCY");

    }

    public void includeCashBalance(boolean status) {
        isCashIncluded = status;
    }

    public void setPFWindow(TradingPortfolioWindow pfWindow) {
        this.pfWindow = pfWindow;
    }

    public void setSymbol(String symbol) {

    }

    public ViewSetting getViewSetting() {
        return super.getViewSettings();
    }

    /* --- Table Model metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void clear() {
        pfValRecords.clear();
    }

    public int getRowCount() {
        if (TradingShared.isReadyForTrading()) {
//            if (isCashIncluded)
//                //return dataStore.size() + 2;
//                return pfValRecords.size() + 1;
//            else
//            //    return dataStore.size() ;
            return pfValRecords.size() + 1;

        } else {
            return 0;
        }
    }

    public Object getValueAt(int row, int col) {
        //float conversionRate = CurrencyStore.getCurrency("SR|" + getBaseCurrency());
        if (row == 25) {
            System.out.println("row" + row);
        }
        PfValutionRecord pfValRec = null;
        int extraRows = 1;
        int gainLossModifier = 1;
        //int lotSize = 1; // for Futures only
        if (isCashIncluded)
            extraRows = 2;
        try {
            if (row < pfValRecords.size()) {
                pfValRec = pfValRecords.get(row);
                int recType = pfValRec.getRecType();
                if (recType == PfValutionRecord.PORTFOLIODETAIL) {

                    switch (col) {
                        case -12:
                            return "combine";
                        default:


                            String pfDetail = "";
                            String currency = TradingShared.getTrader().getCurrencyCode(pfValRec.getPfID());
//                        System.out.println("sathcurrency---------------------" +  currency);
//
//                        if ( pfValRecords.get(row + 1 ).getRecType() ==    PfValutionRecord.VALRECORD){
                            pfDetail = sPFDetailCurrency.replace("[NAME]", pfValRec.getPfName());
                            pfDetail = pfDetail.replace("[CURRENCY]", currency);        //currecny
//                        }else {
//                            pfDetail =  sPortFolioDetail.replace("[NAME]",pfValRec.getPfName());
//                        }


                            return pfDetail;
                    }
                } else if (recType == PfValutionRecord.TOTOAL) {

                    switch (col) {
                        case -12:
                            return "";
                        case 10:         // CUMULATIVE TOTAL COST
                            return getCumulativeTotalCost(row);         //ok
//                        return new Long((long)getCumulativeTotalCost());
                        case 11:         // CUMULATIVE MARKET VALUE
                            return getMarketValue(row);             //row
                        case 12:        // CUMULATIVE GAIN LOSS TODAY   //sath
//                        System.out.println("12row " + row);
                            return getCumulativeGainLoss(row); //ok
                        case 13: {       // CUMULATIVE % GAIN LOSS TODAY
//                        System.out.println("13row " + row);
                            double cumCost = getCumulativeTotalCost(row);   //ok
                            if (cumCost == 0)
                                return 0D;
                            else
                                return (getCumulativeGainLoss(row) / cumCost) * 100;
//                        return new Float(getCumulativePercentGainLoss());
                        }
                        case 14:        // CUMULATIVE GAIN LOSS OVERALL
                            return getCumGainLossOverall(row);   //ok
                        case 15: {       // CUMULATIVE % GAIN LOSS OVERALL
//                        return new Float(getCumPercentGainLossOverall());
                            double cumCost = getCumulativeTotalCost(row);
                            if (cumCost == 0)
                                return 0f;
                            else
                                return (getCumGainLossOverall(row) / cumCost) * 100;
                        }
                        case 16:        //  % OF PORTFOLIO

                            PfValutionRecord bulkRecord = pfValRecords.get(row - 1);
                            int bulkRecType = bulkRecord.getRecType();
                            if (bulkRecType == PfValutionRecord.VALRECORD)
                                return new Double("100");
                            else
                                return new Double("0");
                        case 17:   //  CASH DIVIDENDS
                            return getTotalCashDividends(row);         //ok
                        case 18:
                            return getCumulativeBuyPending(row);
                        case 19:
                            return getCumulativeSellPending(row);
                        default:
                            return "";
                    }
                } else if (recType == PfValutionRecord.EMPTYROW) {

                    return "";

                } else if (recType == PfValutionRecord.VALRECORD) {


                    ValuationRecord record = pfValRec.getValRecord();
                    //ValuationRecord record = (ValuationRecord) dataStore.get(row);
                    if (record == null)
                        return ""; //null;
                    Stock stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                    if (stock == null)
                        return ""; //null;
                    String exchange = stock.getExchange();
                    if (record.getOpenSellCount() > 0) { // this is a Sell Contract
                        gainLossModifier = -1; // for Sell contracts,  negative gain is a profit
                    }

                /*try {
                    FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol()));
                    lotSize = futureBaseAttributes.getContractSize();
                } catch (Exception e) {
                }*/

                    switch (col) {
                        case -12:
                            return "";
                        case -11:
                            return record.getPortfolioID();
                        case -10:
                            try {
                                if ((tradePortfolios.getTransactRecord(record.getSKey().getTradeKey())).addCostomerAverageCost) {
                                    return 1; // custom mode
                                } else {
                                    return 2; // normal mode
                                }
                            } catch (Exception e) {
                                return 2;
                            }
                        case -4:
                            return stock.getDecimalCount();
                        case -1:
                            return stock.getKey();
                        case 0:     // SYMBOL
                            return stock.getSymbolCode();
                        case 1:     // EXCHANGE CODE
//                        return PortfolioInterface.getExchangeCode(stock);
                            return ExchangeStore.getSharedInstance().getExchange(PortfolioInterface.getExchangeCode(stock).trim()).getDisplayExchange(); //Display Exchange

                        case 2:     // COMPANY NAME
                            if (PortfolioInterface.getCompanyName(stock) == null)
                                return "";
                            else
                                return PortfolioInterface.getCompanyName(stock);
                        case 3:     // CURRENCY
                            if (PortfolioInterface.getCurrency(stock) == null)
                                return "";
                            else
                                return PortfolioInterface.getCurrency(stock);
                        case 4:     // EXCHANGE(Conversion) RATE
//                        return CurrencyStore.getCurrency(PortfolioInterface.getCurrency(stock)
//                            + "|" + PFStore.getBaseCurrency()) + "";
                            // trad
//                        return new Float(CurrencyStore.getCurrency(PortfolioInterface.getCurrency(stock)
                            // Reg
                            return CurrencyStore.getBuyRate(PortfolioInterface.getCurrency(stock), TradePortfolios.getInstance().getBaseCurrency(), TradingShared.getTrader().getPath(record.getPortfolioID()));
                        case 5:     // Owned
                            return (long) record.getOwned(stock.getInstrumentType());
                        case 6:   //  Pledge
                            return record.getPledged();
                        case 7: // Balance
                            return record.getBalance();
                        case 8:     // AVERAGE COST
                            //  return  record.getAvgCost();
                            // return 10.00;
                            //return (convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),PortfolioInterface.getCurrency(stock), false,  record.getAvgCost()));
                            return record.getAvgCost();
                        case 9:     // LATEST(MARKET) PRICE
                            Exchange exch = ExchangeStore.getSharedInstance().getExchange(exchange);
                            float priceModiFactor = exch.getPriceModificationFactor();
                            if (stock.getLastTradeValue() > 0)
                                // return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getLastTrade(stock));
//                        return convertToSelectedCurrency(exchange, pfValRec.getValRecord().getCurrency(), true, PortfolioInterface.getLastTrade(stock), record.getConversionRate(), record.getPortfolioID());
                                return PortfolioInterface.getLastTrade(stock) / priceModiFactor;
                                // return PortfolioInterface.getLastTrade(stock);
                            else
                                //return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getPreviousClosed(stock));
//                        return convertToSelectedCurrency(exchange, pfValRec.getValRecord().getCurrency(), true, PortfolioInterface.getPreviousClosed(stock), record.getConversionRate(), record.getPortfolioID());
                                return PortfolioInterface.getPreviousClosed(stock) / priceModiFactor;
                            //return PortfolioInterface.getPreviousClosed(stock);
                        case 10:     // TOTAL COST (COST BASICS)
                            return record.getCumCost();
                        //    return record.getCumCost();
//                        return new Long((long)convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), record.getTotCost()));
                        case 11:     // MARKET VALUE (VALUATION)
                            if (stock.getLastTradeValue() > 0)
                                return record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock) +
                                        record.getCashDividends();

//                            return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),PortfolioInterface.getCurrency(stock),
//                                    record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock) +
//                                            record.getCashDividends());


                            else
//                            return   record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock) +
//                                                      record.getCashDividends();
                                return record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock) +
                                        record.getCashDividends();
//                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),PortfolioInterface.getCurrency(stock),
//                                    record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock) +
//                                            record.getCashDividends());
                        case 12:    //  GAIN LOSS TODAY
                            //  return (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0]) * record.getPriceCorrectionFactor() * gainLossModifier;
                            return (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0]) * record.getPriceCorrectionFactor() * gainLossModifier;
                        //return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),PortfolioInterface.getCurrency(stock), (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0]) * record.getPriceCorrectionFactor() * gainLossModifier);
                        case 13:    //  % GAIN LOSS TODAY

                            return adjustToPriceModofication(exchange, ((record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0] * gainLossModifier) / record.getCumCost()) * 100);
                        case 14: {  //  GAIN LOSS OVERALL
                            double marketValue = 0f;

                            if (stock.getLastTradeValue() > 0)
                                marketValue = adjustToPriceModofication(exchange, ((record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock))));
                            else
                                marketValue = adjustToPriceModofication(exchange, (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock)));
                            marketValue += record.getCashDividends();
                            // return  (marketValue - record.getCumCost()) * record.get PriceCorrectionFactor()* gainLossModifier;
                            return (marketValue - record.getCumCost()) * record.getPriceCorrectionFactor() * gainLossModifier;
                            //return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), PortfolioInterface.getCurrency(stock),false, (marketValue - record.getCumCost()) * record.getPriceCorrectionFactor()* gainLossModifier);
                        }
                        case 15: {  //  % GAIN LOSS OVERALL
                            //return 23.00;
                            double marketValue = 0f;
                            if (stock.getLastTradeValue() > 0)
                                marketValue = adjustToPriceModofication(exchange, (record.getOwned(stock.getInstrumentType()) * record.getContractSize() *
                                        PortfolioInterface.getLastTrade(stock)));
                            else
                                marketValue = adjustToPriceModofication(exchange, (record.getOwned(stock.getInstrumentType()) * record.getContractSize() *
                                        PortfolioInterface.getPreviousClosed(stock)));
                            marketValue += record.getCashDividends();
                            //return new Float  (((marketValue - record.getCumCost()) / record.getCumCost()) * 100 * gainLossModifier);
                            return ((marketValue - record.getCumCost()) / record.getCumCost()) * 100 * gainLossModifier;
                            //return new Float(convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((marketValue - record.getTotCost()) / record.getTotCost()))*100);
                        }
                        case 16:   //  % OF PORTFOLIO(HOLDINGS)
                            return getPercOfPortfolio(row);
                        case 17:   //  CASH DIVIDENDS
                            return record.getCashDividends();
                        case 18:   //  BUY PENDING
                            return record.getBuyPending();
                        case 19:   //  SELL PENDING
                            return record.getSellPending();
                        case 20:   //  Company code
                            return PortfolioInterface.getCompanyCode(stock);
                        case 21:   //  margin due ammount
                            return record.getMarginDue();
                        case 22:   //  daily margin due ammount
                            return record.getDayMarginDue();
                        case 23:   //  open buy contracts
                            return (long) record.getOpenBuyCount();
                        case 24:   //  open sell contracts
                            return (long) record.getOpenSellCount();
                        case 25:   //  open sell contracts
                            return (long) record.getDayHoldings();
                        case 26:   //  open sell contracts
                            return (long) record.getMarginHoldings();
                        case 27:   //  open sell contracts
                            return (long) record.getDayMarginHoldings();
                        case 28:   //  portfolio name
                            return " " + TradePortfolios.getInstance().getPortfolioName(record.getPortfolioID()) + " ";
                        case 29:   //  bookkeeper
                            return "" + TPlusStore.getSharedInstance().getBookKeeper(record.getBookKeeper()).getName();

                        case 30:
                            return (long) record.getTPlusDayNetHolding();
                        case 31:
                            return (long) record.getTPlusPendingStock();
                        case 32:
                            return (long) record.getTPlusDaySellPending();
                        default:
                            return "";
                    }
                } else {               //end of val record
                    return "";
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    private double convertToSelectedCurrency(String exchangeCode, String sCurrency, boolean applyPriceModificationFactor, double value, double multiplyfactor, String portfolio) {
        if (applyPriceModificationFactor) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            if (exchange != null)
                return value * CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio)) * multiplyfactor / exchange.getPriceModificationFactor();
            else
                return value * CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio)) * multiplyfactor;
        } else {
            return value * CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio)) * multiplyfactor;
        }
    }

    public double getTotalCashDividends(int row) {

        int iThisPfStart = 0;
        try {
            iThisPfStart = pfValRecords.get(row).getPrePfNo();
        } catch (Exception e) {
            return 0;//chekck
        }
        double totMValue = 0D;
        ValuationRecord valRec = null;
        PfValutionRecord temp = null;
        for (int i = iThisPfStart; i < row; i++) {

            temp = pfValRecords.get(i);
            valRec = temp.getValRecord();
            totMValue += valRec.getCashDividends();
            valRec = null;
        }
        return totMValue;
    }

    public double getPercOfPortfolio(int row) {

        int pfTotalRow = 0;
        for (int i = 0; i < pfValRecords.size(); i++) {
            if (pfValRecords.get(i).getRecType() == PfValutionRecord.TOTOAL) {//total rows
                if (row < i) {
                    pfTotalRow = i;
                    break;
                }
            }
        }
        int iThisPfStart = 0;
        try {
            iThisPfStart = pfValRecords.get(pfTotalRow).getPrePfNo();
        } catch (Exception e) {
            return 0;//chekck
        }

        double mValue = 0D;
        double selMValue = 0D;
        double totMValue = 0D;
        Stock stock = null;

        ValuationRecord valRec = null;
        PfValutionRecord temp = null;


        for (int i = iThisPfStart; i < pfTotalRow; i++) {
            temp = pfValRecords.get(i);
            valRec = temp.getValRecord();
            stock = PortfolioInterface.getStockObject(valRec.getSKey().getExchange(), valRec.getSKey().getSymbol());
            if (stock.getLastTradeValue() > 0)
                mValue = valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * (PortfolioInterface.getLastTrade(stock) + valRec.getCashDividends());
            else
                mValue = valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * (PortfolioInterface.getPreviousClosed(stock) + valRec.getCashDividends());
            if (row == i)
                selMValue = mValue;
            totMValue += mValue;
            valRec = null;
            stock = null;
        }
        if (totMValue == 0)
            return 0D;
        else
            return (selMValue / totMValue) * 100;
    }

/*
        public float getPercOfPortfolio(int row) {
            float mValue    = 0f;
            float selMValue = 0f;
            float totMValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (stock.getLastTrade()[0] > 0)
                    mValue += record.getHolding() * convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock)[0]);
                else
                    mValue += record.getHolding() * convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock)[0]);
                if (row == i)
                    selMValue = mValue;
                totMValue += mValue;
                record = null;
                stock  = null;
            }
            if (isCashIncluded) {
                if (row == (getRowCount() - 2))
                    selMValue = pfWindow.getCashBalanceForSelectedPFs();
                totMValue += pfWindow.getCashBalanceForSelectedPFs();
            }
            return (selMValue / totMValue) * 100;
        }
*/

    public double getCumulativeTotalCost(int row) {


        try {
            int iThisPfStart = 0;
            try {
                iThisPfStart = pfValRecords.get(row).getPrePfNo();
            } catch (Exception e) {
                return 0D;//chekck
            }

            double totCost = 0D;
            Stock stock = null;
            int gainLossModifier = 1;
            ValuationRecord valRec = null;
            PfValutionRecord temp = null;
            for (int i = iThisPfStart; i < row; i++) {

                temp = pfValRecords.get(i);
                valRec = temp.getValRecord();
                stock = PortfolioInterface.getStockObject(valRec.getSKey().getExchange(), valRec.getSKey().getSymbol());
                if (valRec.getOpenSellCount() > 0) {
                    gainLossModifier = -1; // for Sell contracts,  negative gain is a profit

                } else {
                    gainLossModifier = 1;
                }

                //totCost += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),PortfolioInterface.getCurrency(stock), false, valRec.getCumCost() * valRec.getPriceCorrectionFactor()  * gainLossModifier);
                totCost += valRec.getCumCost() * valRec.getPriceCorrectionFactor() * gainLossModifier;
                valRec = null;
                temp = null;
            }
            //todo

//            System.out.println("sath+ totocost: " + totCost);
            return totCost;
        } catch (Exception e) {
            return 0;
        }


    }

    public long getCumulativeBuyPending(int row) {
        long total = 0;
        int iThisPfStart = 0;
        try {
            iThisPfStart = pfValRecords.get(row).getPrePfNo();
        } catch (Exception e) {
            return 0;//chekck
        }
        ValuationRecord valRec = null;
        PfValutionRecord temp = null;
        for (int i = iThisPfStart; i < row; i++) {

            temp = pfValRecords.get(i);
            valRec = temp.getValRecord();
            total += valRec.getBuyPending();
            valRec = null;
            temp = null;
        }

        return total;
    }

    public long getCumulativeSellPending(int row) {
        long total = 0;

        //get prepf start number
        int iThisPfStart = 0;
        try {
            iThisPfStart = pfValRecords.get(row).getPrePfNo();
        } catch (Exception e) {
            return 0;//chekck
        }

        ValuationRecord valRec = null;
        PfValutionRecord temp = null;
        for (int i = iThisPfStart; i < row; i++) {

            temp = pfValRecords.get(i);
            valRec = temp.getValRecord();
            total += valRec.getSellPending();
            valRec = null;
            temp = null;
        }

        return total;
    }


    public double getMarketValue(int row) {

        int iThisPfStart = 0;
        try {
            iThisPfStart = pfValRecords.get(row).getPrePfNo();
        } catch (Exception e) {
            return 0;//chekck
        }

        double mValue = 0D;
        Stock stock = null;
        ValuationRecord valRec = null;
        PfValutionRecord temp = null;

        for (int i = iThisPfStart; i < row; i++) {

            temp = pfValRecords.get(i);
            valRec = temp.getValRecord();

            stock = PortfolioInterface.getStockObject(valRec.getSKey().getExchange(), valRec.getSKey().getSymbol());

            if (stock.getLastTradeValue() > 0)
                //mValue += valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
                mValue += valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * PortfolioInterface.getLastTrade(stock);
            else
                mValue += valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * PortfolioInterface.getPreviousClosed(stock);
            valRec = null;
            stock = null;
        }

        return mValue;
    }


    public double getCumGainLossOverall(int row) {
        int iThisPfStart = 0;
        try {
            iThisPfStart = pfValRecords.get(row).getPrePfNo();
        } catch (Exception e) {
            return 0D;//chekck
        }

        double mValue = 0D;
        double cumValue = 0D;
        Stock stock = null;
        ValuationRecord valRec = null;
        PfValutionRecord temp = null;
        int gainLossModifier = 1;
        for (int i = iThisPfStart; i < row; i++) {

            temp = pfValRecords.get(i);
            valRec = temp.getValRecord();
            stock = PortfolioInterface.getStockObject(valRec.getSKey().getExchange(), valRec.getSKey().getSymbol());
            if (valRec.getOpenSellCount() > 0) { // this is a Sell Contract
                gainLossModifier = -1; // for Sell contracts,  negative gain is a profit
            } else {
                gainLossModifier = 1;
            }
            if (stock.getLastTradeValue() > 0)
                mValue = adjustToPriceModofication(stock.getExchange(), valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * PortfolioInterface.getLastTrade(stock));
            else
                mValue = adjustToPriceModofication(stock.getExchange(), valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * PortfolioInterface.getPreviousClosed(stock));
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
            cumValue += (mValue - valRec.getCumCost()) * valRec.getPriceCorrectionFactor() * gainLossModifier;
            valRec = null;
            stock = null;
        }
//         System.out.println("row: " + row  + "cumvalue: " + cumValue);
        return cumValue;
    }

///SATH TEMPORTY OUT*******************************************************************************
//    public double getCumGainLossOverall(int row) {
//
//        //get previous pf start row
//        int iThisPfStart  = 0;
//                             try{
//                                 iThisPfStart   =  pfValRecords.get(row).getPrePfNo();
//                             } catch(Exception e){
//                                return  0D;//chekck
//                             }
//        double mValue = 0D;
//        double cumValue = 0D;
//        Stock stock = null;
//         ValuationRecord valRec  = null;
//        PfValutionRecord temp = null;
//        int gainLossModifier = 1;
//
//          for (int i = iThisPfStart + 1;i<  row  ;i++){
//           temp = pfValRecords.get(i);
//            valRec =  temp.getValRecord() ;
//            stock = PortfolioInterface.getStockObject(valRec.getSKey());
//            if (valRec.getOpenSellCount() > 0 ){ // this is a Sell Contract
//                gainLossModifier = -1; // for Sell contracts,  negative gain is a profit
//            } else {
//                gainLossModifier = 1;
//            }
//            if (stock.getLastTradeValue() > 0)
//                mValue = adjustToPriceModofication(stock.getExchange(), valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * PortfolioInterface.getLastTrade(stock));
//            else
//                mValue = adjustToPriceModofication(stock.getExchange(), valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * PortfolioInterface.getPreviousClosed(stock));
////            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
//            cumValue += (mValue - valRec.getCumCost())* valRec.getPriceCorrectionFactor() * gainLossModifier;
//            //cumValue += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),  PortfolioInterface.getCurrency(stock),false, (mValue - valRec.getCumCost())* valRec.getPriceCorrectionFactor() * gainLossModifier);
//            //apply same currency
//            //cumValue += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),PortfolioInterface.getCurrency(stock) , false, (mValue - record.getCumCost())* record.getPriceCorrectionFactor() * gainLossModifier);
//           // cumValue += (mValue - record.getCumCost())* record.getPriceCorrectionFactor() * gainLossModifier;
//            valRec = null;
//            stock = null;
//        }
//        return cumValue;
//    }

///SATH TEMPORTY OUT*******************************************************************************


/*
        public float getCumGainLossOverall() {
            float mValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (stock.getLastTrade()[0] > 0)
                    mValue += record.getHolding() * PortfolioInterface.getLastTrade(stock)[0];
                else
                    mValue += record.getHolding() * PortfolioInterface.getPreviousClosed(stock)[0];
                mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
                record = null;
                stock  = null;
            }
            return mValue;
        }
*/
    //sathproblem
    //    public double getCumPercentGainLossOverall() {
    //        double mValue = 0D;
    //        double cumValue = 0D;
    //        Stock stock = null;
    //        ValuationRecord record = null;
    //        for (int i = 0; i < dataStore.size(); i++) {
    //            record = (ValuationRecord) dataStore.get(i);
    //            stock = PortfolioInterface.getStockObject(record.getSKey());
    //            if (PortfolioInterface.getLastTrade(stock) > 0)
    //                mValue = record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock);
    //            else
    //                mValue = record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock);
    ////            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((mValue - record.getTotCost()) / record.getTotCost()));
    //            if (record.getCumCost() != 0)
    //                cumValue += ((mValue - record.getCumCost()) / record.getCumCost()) * 100;
    //            record = null;
    //            stock = null;
    //        }
    //        return cumValue;
    //    }


/*        public float getCumPercentGainLossOverall() {
            float mValue = 0f;
            float cumValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (PortfolioInterface.getLastTrade(stock)[0] > 0)
                    mValue += record.getHolding() * PortfolioInterface.getLastTrade(stock)[0];
                else
                    mValue += record.getHolding() * PortfolioInterface.getPreviousClosed(stock)[0];
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((mValue - record.getTotCost()) / record.getTotCost()));
                cumValue += ((mValue - record.getTotCost()) / record.getTotCost()) * 100;
                record = null;
                stock  = null;
            }
            return cumValue;
        }
     */


    public double getCumulativeGainLoss(int row) {

        int iThisPfStart = 0;
        try {
            iThisPfStart = pfValRecords.get(row).getPrePfNo();
        } catch (Exception e) {
            return 0;//chekck
        }
        double totCost = 0D;
        Stock stock = null;
        int gainLossModifier = 1;
        ValuationRecord valRec = null;
        PfValutionRecord temp = null;
        for (int i = iThisPfStart; i < row; i++) {
            temp = pfValRecords.get(i);
            valRec = temp.getValRecord();
            stock = PortfolioInterface.getStockObject(valRec.getSKey().getExchange(), valRec.getSKey().getSymbol());
            if (valRec.getOpenSellCount() > 0) { // this is a Sell Contract
                gainLossModifier = -1; // for Sell contracts,  negative gain is a profit
            } else {
                gainLossModifier = 1;
            }
            //totCost += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getCurrency(stock), (valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * PortfolioInterface.getChange(stock)[0]) * valRec.getPriceCorrectionFactor() * gainLossModifier);
            totCost += (valRec.getOwned(stock.getInstrumentType()) * valRec.getContractSize() * PortfolioInterface.getChange(stock)[0]) * valRec.getPriceCorrectionFactor() * gainLossModifier;
            valRec = null;

        }
        return totCost;
    }

    //sathproblme data store currency conversion problem
//    public double getCumulativePercentGainLoss() {
//        double totCost = 0D;
//        Stock stock = null;
//        ValuationRecord record = null;
//        for (int i = 0; i < dataStore.size(); i++) {
//            record = (ValuationRecord) dataStore.get(i);
//            stock = PortfolioInterface.getStockObject(record.getSKey());
//            if (record.getCumCost() != 0)
//                totCost += (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0] / record.getCumCost()) * 100;
////            totCost += convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (record.getHolding() * PortfolioInterface.getChange(stock)[0] / record.getTotCost()));
//            record = null;
//        }
//        return totCost;
//    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        /*try {
            return getValueAt(0, iCol).getClass();
                 } catch (Exception e) {
            return Object.class;
                 }*/
        switch (iCol) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 20:
            case 28:
            case 29:
                return String.class;
            case 5:
            case 6:
            case 7:
            case 18:
            case 19:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 30:
            case 31:
            case 32:
                return Long.class;
            case 4:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 21:
            case 22:
                return Double.class;
            default:
                return Object.class;
        }

//        if ( (iCol == 8) || (iCol >= 10)) {
//        if (iCol == 7) {
//            return Number[].class;
//        } else {
//            return getValueAt(0, iCol).getClass();
//        }
//        return Object.class;
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object value, int rowIndex, int column) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        //CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("PFDETAIL_ROW"), FIELD_PFDETAILS_ROW, Theme.getColor("TRADINGPF_DETAIL_BGCOLOR"), Theme.getColor("TRADINGPF_DETAIL_FGCOLOR"));


        return customizerRecords;
    }


//sath removeed conversion at all
    /*******************************************************************************************************************************/
//    private double convertToSelectedCurrency(String exchangeCode, String sBaseCurrency, String sTargetCurrency, boolean applyPriceModificationFactor, double value) {
//        if (applyPriceModificationFactor){
//            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
//            if (exchange != null)
//                return value * CurrencyStore.getBuyRate(sBaseCurrency, tradePortfolios.getBaseCurrency()) / exchange.getPriceModificationFactor();
//            else
//                return value * CurrencyStore.getBuyRate(sBaseCurrency, tradePortfolios.getBaseCurrency());
//        } else {
//            return value *  CurrencyStore.getBuyRate(sBaseCurrency, tradePortfolios.getBaseCurrency());
//        }
//    }


//    private double convertToSelectedCurrency(String exchangeCode, String sBaseCurrency, String sTargetCurrency, double value) {
//        return convertToSelectedCurrency(exchangeCode, sBaseCurrency, sBaseCurrency, true, value);
//    }


    /**
     * ***************************************************************************************************************************
     */

    private double adjustToPriceModofication(String exchangeCode, double value) {
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
        if (exchange != null)
            return value / exchange.getPriceModificationFactor();
        else
            return value;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private float toFloatValue(Object oValue) throws Exception {
        return Float.parseFloat((String) oValue);
    }

    public double getMarketValueForSymbol(String sKey) {
        double marketValue = 0d;
        ValuationRecord valRecord = null;
        ArrayList list = TradePortfolios.getInstance().getValuationList();
        Stock stock = PortfolioInterface.getStockObject(sKey);
        if (stock == null)
            return 0f;

        for (int i = 0; i < list.size(); i++) {
            valRecord = (ValuationRecord) list.get(i);
            if (valRecord.getSKey().equals(sKey)) {
                if (stock.getLastTradeValue() > 0)
                    marketValue = valRecord.getOwned(stock.getInstrumentType()) * valRecord.getContractSize() * PortfolioInterface.getLastTrade(stock);
                    //marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getCurrency(stock), valRecord.getOwned(stock.getInstrumentType()) * valRecord.getContractSize() * PortfolioInterface.getLastTrade(stock));
                else
                    marketValue = valRecord.getOwned(stock.getInstrumentType()) * valRecord.getContractSize() * PortfolioInterface.getPreviousClosed(stock);
                break;
            }
            valRecord = null;
        }
        valRecord = null;
        list = null;
        stock = null;

        return marketValue;
    }

    public double getMktValuePerShareForSymbol(String sKey) {
        double marketValue = 0d;
        Stock stock = PortfolioInterface.getStockObject(sKey);
        if (stock == null)
            return 0f;

        if (stock.getLastTradeValue() > 0)
            //marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
            marketValue = PortfolioInterface.getLastTrade(stock);
        else
            marketValue = PortfolioInterface.getPreviousClosed(stock);
        //marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock));

        stock = null;
        return marketValue;
    }

    //sathproblem data store currency conversion problem why conversion of defcurr mode working for unified mode
//    public double getMarketValue(boolean isCashIncludedLocal) { //no usages
//        double mValue = 0D;
//        Stock stock = null;
//        ValuationRecord record = null;
//        for (int i = 0; i < dataStore.size(); i++) {
//            record = (ValuationRecord) dataStore.get(i);
//            stock = PortfolioInterface.getStockObject(record.getSKey());
//            if (stock.getLastTradeValue() > 0)
//                mValue += record.getOwned(stock.getInstrumentType()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
//            else
//                mValue += record.getOwned(stock.getInstrumentType()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock));
//            record = null;
//            stock = null;
//        }
//        if (isCashIncludedLocal)
//            mValue += pfWindow.getCashBalanceForSelectedPFs();
//        return mValue;
//    }

    public int getRowType(int row) {


        return 5;
    }

}