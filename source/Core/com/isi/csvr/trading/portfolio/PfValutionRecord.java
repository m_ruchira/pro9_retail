package com.isi.csvr.trading.portfolio;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Dec 11, 2007
 * Time: 2:01:44 PM
 * To change this template use File | Settings | File Templates.
 */


public class PfValutionRecord {

    public static int PORTFOLIODETAIL = 0;
    public static int TOTOAL = 1;
    public static int VALRECORD = 2;
    public static int EMPTYROW = 3;
    ValuationRecord valRecord;
    private int iRecType = 0;
    private String pfName = "";
    private String currency = "";
    private int prePfNo = 0;
    private String pfId = "";


    public PfValutionRecord(ValuationRecord valRec, int type) {

        this.valRecord = valRec;
        this.iRecType = type;

    }

    public ValuationRecord getValRecord() {
        return valRecord;
    }

    public void setValRecord(ValuationRecord valrec) {

        this.valRecord = valrec;

    }

    public int getRecType() {
        return iRecType;
    }

    public void setRecType(int valRec) {
        this.iRecType = valRec;

    }

    public String getPfName() {
        return pfName;

    }

    public void setPfName(String pfName) {
        this.pfName = pfName;

    }

    public String getCurrency() {
        return currency;

    }

    public void setCurrency(String currency) {
        this.currency = currency;

    }

    public int getPrePfNo() {
        return prePfNo;
    }

    public void setPrePfNo(int prePfNo) {
        this.prePfNo = prePfNo;
    }

    public String getPfID() {
        return pfId;
    }

    public void setPfID(String pfID) {
        this.pfId = pfID;
    }


}
