package com.isi.csvr.trading.portfolio;

//import javax.swing.JPopupMenu;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class SymbolList extends JPopupMenu implements MouseListener {

    private JPanel basePanel;
    private JPanel btnPanel;
    private JCheckBox[] checkPFs;
    //    private Vector      pfVector;
    private TWButton btnOK;
    private TWButton btnCancel;
    private TWButton btnCreatePF;
    private ExportDialog parent;
    private Vector symbolsVec;

    public SymbolList(ExportDialog parentIn, String[] selIDs, Vector symbolsVec) { //String selSymbols) {
        this.parent = parentIn;
        this.symbolsVec = symbolsVec;

        basePanel = new JPanel(new BorderLayout());
        int noOfElems = 0;
        if (symbolsVec != null)
            noOfElems = symbolsVec.size(); // valRecords.size();
        JPanel centerPanel = new JPanel(new GridLayout(noOfElems + 1, 1));
        btnPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 2));
        checkPFs = new JCheckBox[noOfElems + 1];
        if (noOfElems == 0) {
//            JLabel nullLabel1 = new JLabel("");
//            centerPanel.add(nullLabel1);
            String mesg = "<html><center><b>" +
                    Language.getString("MSG_NO_TRANSACTIONS_FOUND") + "</b><br>" +
                    Language.getString("MSG_PLEASE_CREATE_TRANSACTIONS") + "</p></center></html>";
            JLabel displayMesg = new JLabel(mesg);
            centerPanel.add(displayMesg);
//            JLabel nullLabel2 = new JLabel("");
//            centerPanel.add(nullLabel2);
//            centerPanel.setPreferredSize(new Dimension(200, 50));
        } else {
            // Add first item
            checkPFs[0] = new JCheckBox(Language.getString("ALL"));
            checkPFs[0].addMouseListener(this);
            centerPanel.add(checkPFs[0]);

            for (int i = 0; i < symbolsVec.size(); i++) {
                checkPFs[i + 1] = new JCheckBox(PortfolioInterface.getCompanyName((String) symbolsVec.elementAt(i)));
                centerPanel.add(checkPFs[i + 1]);
            }
            btnOK = new TWButton(Language.getString("OK"));
            btnOK.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    applySelectedSymbols();
                    disposePopup();
                }
            });
            btnCancel = new TWButton(Language.getString("CANCEL"));
            btnCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    disposePopup();
                }
            });

            btnOK.setPreferredSize(new Dimension(80, 20));
            btnCancel.setPreferredSize(new Dimension(80, 20));

            GUISettings.setSameSize(btnOK, btnCancel);
            btnPanel.add(btnOK);
            btnPanel.add(btnCancel);
        }
        basePanel.add(centerPanel, BorderLayout.CENTER);
        basePanel.add(btnPanel, BorderLayout.SOUTH);

        this.add(basePanel);

        this.pack();
    }

    private void applySelectedSymbols() {
        int counter = 0;
        StringBuffer symbolStr = null;
        int noOfItemsSelected = 0;

        symbolStr = new StringBuffer();
        if (checkPFs[0].isSelected()) {
            symbolStr.append(Language.getString("ALL"));
        } else {
            for (int i = 1; i < checkPFs.length; i++) {
                if (checkPFs[i].isSelected()) {
                    noOfItemsSelected++;
                }
            }
            for (int i = 1; i < checkPFs.length; i++) {
                if (checkPFs[i].isSelected()) {
                    try {
                        symbolStr.append(symbolsVec.elementAt(i - 1));
//                        symbolStr.append(((ValuationRecord)PFStore.getInstance().getValuationList().
//                                          get(i - 1)).getSKey());
                        //                selSymbols[counter] = ((ValuationRecord)PFStore.getInstance().getValuationList().get(i)).getSKey();
                        //                symbolStr.append(selSymbols[counter-1] + " ");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    if (counter != (noOfItemsSelected - 1))
                        symbolStr.append(",");
                    counter++;
                }
            }
        }
        parent.setSymbols(symbolStr.toString());
//        parent.setSelectedPFIDs(selPFIDs);
//        parent.displayCashBalance();
//        parent.setFilterDescription(false, null, null, null, TransactRecord.NONE);
    }

    private void hidePopup() {
        this.setVisible(false);
    }

    private void disposePopup() {
        this.setVisible(false);
        checkPFs = null;
        parent = null;
//        pfVector.removeAllElements();
//        pfVector    = null;
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == checkPFs[0]) {
            if (checkPFs[0].isSelected()) {
                for (int i = 1; i < checkPFs.length; i++) {
                    checkPFs[i].setEnabled(false);
                }
            } else {
                for (int i = 1; i < checkPFs.length; i++) {
                    checkPFs[i].setEnabled(true);
                }
            }
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

}