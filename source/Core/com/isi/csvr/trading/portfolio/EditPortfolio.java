package com.isi.csvr.trading.portfolio;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 3, 2006
 * Time: 11:52:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class EditPortfolio extends JDialog implements ActionListener {

    private JLabel txtPortfolio;
    private TWTextField txtName;
    private TWButton btnOk;
    private TWButton btnCancel;
    private TradingPortfolioRecord record;

    public EditPortfolio(TradingPortfolioRecord record) {
        super(Client.getInstance().getFrame());
        this.record = record;
        this.setModal(true);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"5", "70", "30"}));
        this.setResizable(false);
        this.setSize(new Dimension(320, 150));
        this.setTitle(Language.getString("EDIT_PORTFOLIO"));
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"30%", "70%"}, new String[]{"20", "20"}, 10, 10));
        topPanel.add(new JLabel(Language.getString("PORTFOLIO")));
        topPanel.add(new JLabel(record.getPortfolioID()));
        topPanel.add(new JLabel(Language.getString("NAME")));
        txtName = new TWTextField();
        topPanel.add(txtName);
        JPanel btnPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 10, 5));
        btnOk = new TWButton();
        btnOk.setText(Language.getString("OK"));
        btnOk.addActionListener(this);
        btnOk.setPreferredSize(new Dimension(80, 25));
        btnCancel = new TWButton();
        btnCancel.setText(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);
        btnCancel.setPreferredSize(new Dimension(80, 25));
        btnPanel.add(btnOk);
        btnPanel.add(btnCancel);
        this.add(new JLabel(""));
        this.add(topPanel);
        this.add(btnPanel);
        GUISettings.applyOrientation(this);

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnOk) {
            if (!txtName.getText().equals("")) {
                TradeMethods.sendEdittedPortfolioData(record.getPortfolioID(), txtName.getText());
//                record.setName(txtName.getText());
//                TradingShared.getTrader().firePortfolioDataChanged(record.getPortfolioID());
                this.dispose();
            }
        } else if (e.getSource() == btnCancel) {
            this.dispose();
        }
    }


}
