package com.isi.csvr.trading.security;

import com.isi.csvr.shared.Settings;
import com.isi.csvr.trading.shared.TradingShared;
import iaik.pkcs.pkcs11.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.security.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 8, 2004
 * Time: 11:18:55 AM
 */
public class TWSecurityManager {
    public static final int PUBLIC_KEY_ENCRYPTED_LENGTH = 128;
    //public static TWSecurityManager self = null;
    private static String library;
    private static Cipher DESKEYDecryptor;

    /*public static synchronized TWSecurityManager getSharedInstance() {
        if (self == null) {
            self = new TWSecurityManager();
        }
        return self;
    }*/
    static {
        library = Settings.CONFIG_DATA_PATH + "/xltCk.dll";
        /*try {
            Properties prop = new Properties();
            FileInputStream in = new FileInputStream("system/pkcs11.cfg");
            prop.load(in);
            library = prop.getProperty("library");
            prop = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    public static synchronized boolean matchPWWithUSB(String password) {
        try {
            String configName = Settings.CONFIG_DATA_PATH + "/pkcs11.cfg";
            Provider p = new sun.security.pkcs11.SunPKCS11(configName);
            KeyStore ks = KeyStore.getInstance("PKCS11", p);
            ks.load(null, password.toCharArray());
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            try {
                Module module = Module.getInstance(library);
                Info info = module.getInfo();
                // list all slots (readers) in which there is currenlty a token present
                Slot[] slotsWithToken = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
                Token token = slotsWithToken[0].getToken();
                token.closeAllSessions();
            } catch (Exception e) {
            }
        }
    }

    public static synchronized boolean isUSBConected() {
        try {
            String configName = Settings.CONFIG_DATA_PATH + "/pkcs11.cfg";
            Provider p = new sun.security.pkcs11.SunPKCS11(configName);
            KeyStore ks = KeyStore.getInstance("PKCS11", p);
            configName = null;
            p = null;
            ks = null;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                Module module = Module.getInstance(library);
                Info info = module.getInfo();
                // list all slots (readers) in which there is currenlty a token present
                Slot[] slotsWithToken = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
                Token token = slotsWithToken[0].getToken();
                token.closeAllSessions();
                slotsWithToken = null;
                info = null;
                token = null;
            } catch (Exception e) {
            }
        }
    }


    public static synchronized boolean changePIN(String OldPIN, String newPIN) throws Exception {

        try {
            System.out.println("++++++++++ Changing USB Pin ++++++++++++");
            Module module = Module.getInstance(library);
            Info info = module.getInfo();
            // list all slots (readers) in which there is currenlty a token present
            Slot[] slotsWithToken = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
            Token token = slotsWithToken[0].getToken();
            //token.closeAllSessions();
            Session session = token.openSession(Token.SessionType.SERIAL_SESSION,
                    Token.SessionReadWriteBehavior.RW_SESSION, null, null);
            try {
                session.logout();
            } catch (Exception e) {
            }
            session.login(Session.UserType.USER, OldPIN.toCharArray());
            session.setPIN(OldPIN.toCharArray(), newPIN.toCharArray());
            return true;
        } finally {
            try {
                Module module = Module.getInstance(library);
                Info info = module.getInfo();
                // list all slots (readers) in which there is currenlty a token present
                Slot[] slotsWithToken = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
                Token token = slotsWithToken[0].getToken();
                token.closeAllSessions();
            } catch (Exception e) {
            }
        }
    }

    public static synchronized String sign(String message, String sPIN) throws Exception {
        try {
            String configName = Settings.CONFIG_DATA_PATH + "/pkcs11.cfg";
            Provider p = new sun.security.pkcs11.SunPKCS11(configName);
            char[] pin = sPIN.toCharArray();
            KeyStore ks = KeyStore.getInstance("PKCS11", p);
            ks.load(null, pin);
            Signature sig = Signature.getInstance("MD5withRSA", p);
            sig.initSign((PrivateKey) ks.getKey("mykey", null));
            sig.update(message.getBytes());
            return base16Encoder(sig.sign());
        } finally {
            try {
                Module module = Module.getInstance(library);
                Info info = module.getInfo();
                // list all slots (readers) in which there is currenlty a token present
                Slot[] slotsWithToken = module.getSlotList(Module.SlotRequirement.TOKEN_PRESENT);
                Token token = slotsWithToken[0].getToken();
                token.closeAllSessions();
            } catch (Exception e) {
            }
        }
    }


    public static synchronized String base16Encoder(byte[] digestMsgByte) {
        StringBuffer verifyMsg = new StringBuffer();
        int hexChar;
        for (int i = 0; i < digestMsgByte.length; i++) {
            hexChar = 0xFF & digestMsgByte[i];
            String hexString = Integer.toHexString(hexChar);
            if (hexString.length() == 1) {
                verifyMsg.append("0");
                verifyMsg.append(hexString);
            } else {
                verifyMsg.append(hexString);
            }
            hexString = null;
        }
        return verifyMsg.toString();
    }

    public static synchronized byte[] base16Decoder(String hex) {
        byte[] bts = new byte[hex.length() / 2];
        for (int i = 0; i < bts.length; i++) {
            bts[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bts;
    }

    /*
     The satellite data comes in encrypted format and has to be decrypted bofore doing
     anything else. This function accepts the encrypted data in byte[] format and the DES Key in base64
     format and returns the decrypted data in byte[] format.
    */

    public static synchronized void initializePriceDataDecryptor(String String_DESKey) {
        try {
            SecretKeyFactory kf = SecretKeyFactory.getInstance("DESede");
            byte[] key = base16Decoder(String_DESKey);
            DESedeKeySpec spec = new DESedeKeySpec(key);
            Key DESKey = kf.generateSecret(spec);
            DESKEYDecryptor = Cipher.getInstance("DESede");
            DESKEYDecryptor.init(Cipher.DECRYPT_MODE, DESKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized byte[] decryptData(byte[] data, int offset, int length) throws BadPaddingException, IllegalBlockSizeException, Exception {
        byte[] decrypted_data = DESKEYDecryptor.doFinal(data, offset, length);
        return decrypted_data;
    }

    /*
      TW needs the DES key of the satellite session for decryptig the satellite feed. This DES key comes in
      encrypted format. Following function decrypt the DES key. It acepts encrypted DES key in byte[]
      format. Privatekey needed for the decryption is stored in the USB token. This decryption happens
      inside the USB token.
    */
    public static synchronized String decryptSubscription(byte[] data, String sPIN) {
        try {
            char[] pin = sPIN.toCharArray();
            String configName = Settings.CONFIG_DATA_PATH + "/pkcs11.cfg";
            Provider p = new sun.security.pkcs11.SunPKCS11(configName);
            KeyStore ks = KeyStore.getInstance("PKCS11", p);
            ks.load(null, pin);

            Cipher rsa = Cipher.getInstance("RSA/ECB/PKCS1Padding", p);
            rsa.init(Cipher.DECRYPT_MODE, (PrivateKey) ks.getKey("mykey", null));
            byte[] res = rsa.doFinal(data, 0, PUBLIC_KEY_ENCRYPTED_LENGTH);

            pin = null;
            p = null;
            ks = null;

            return new String(res);
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }

    public static synchronized boolean isInitialPin(String pin) {

        try {
            return pin.equals(TradingShared.INITAL_USB_PW);
        } catch (Exception e) {
            return false;
        }
    }

    public static synchronized String getSingleSingOnID(String data) {
        return data;
    }

    public static synchronized String getTradeToken(String data) {
        try {
            String[] arr = data.split("\"");
            return arr[2];
        } catch (Exception e) {
            return data;  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}