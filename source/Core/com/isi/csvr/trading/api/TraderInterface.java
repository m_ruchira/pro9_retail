package com.isi.csvr.trading.api;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Aug 30, 2007
 * Time: 9:19:04 AM
 */
public interface TraderInterface {

    public void setActive();

    public void start();

    public void stop();

    public void write();
}
