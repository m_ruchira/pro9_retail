package com.isi.csvr.trading.api;

import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.shared.TradingShared;

import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Aug 30, 2007
 * Time: 9:20:52 AM
 */
public class TradeServer implements Runnable, TradingConnectionListener {

    private static final String LOCK = "TradeServer";
    private static boolean serverActive;
    private static boolean clientActive;
    private static TradeServer self;
    private ServerSocket server;
    private Socket client;
    private List<String> buffer;


    public TradeServer() {
        buffer = Collections.synchronizedList(new LinkedList<String>());
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }


    public static TradeServer getInstance() {
        return self;
    }

    public static void start() {
        try {
            if (self == null)
                self = new TradeServer();
            serverActive = true;
            clientActive = true;
            Thread t = new Thread(self);
            t.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void write(String xml) {
        try {
            self.buffer.add(xml);
        } catch (Exception e) {
        }
    }

    public void stop() {
        serverActive = false;
        clientActive = false;
        self.buffer.clear();
        self.closeServer();
        self.closeClient();
    }

    private void closeServer() {
        try {
            self.server.close();
            self.server = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void closeClient() {
        try {
            self.client.close();
            self.client = null;
            clientActive = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (serverActive) {
            try {
                server = new ServerSocket(9191, 1);
                while (serverActive) {
                    try {
                        client = server.accept();
                        OutputStream out = client.getOutputStream();
                        out.write(TradingShared.getTrader().getAccountsXML().getBytes());
                        out.write(TradePortfolios.getInstance().getPortfolioXML().getBytes());
                        out.write(TradePortfolios.getInstance().getPortfolioDataXML().getBytes());
                        out.write(OrderStore.getSharedInstance().getOrdersXML().getBytes());

                        Reader reader = new Reader(client.getInputStream());
                        //client.setSoTimeout(30000);
                        clientActive = true;

                        while (clientActive && serverActive) {
                            while (self.buffer.size() > 0) {
                                String xml = self.buffer.remove(0);
                                out.write("\r\n".getBytes());
                                out.write(xml.getBytes());
                            }
                            sleep(1000);
                            //out.write("<Pulse/>".getBytes()); //todo send pulse
                        }
                        reader = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                        //closeServer();
                        closeClient();
                        sleep(5000);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                closeServer();
                closeClient();
                sleep(5000);
            }
        }
    }

    public void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
        }
    }


    public void tradeServerConnected() {

    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        stop();
    }
}
