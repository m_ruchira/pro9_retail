package com.isi.csvr.trading.api;

import com.isi.csvr.datastore.DataStore;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Oct 6, 2007
 * Time: 5:54:28 PM
 */
public class SnapshotReader {

    private long lastUpdatedTime = -1;
    private long newUpdatedTime = 0;
    private Set<String> symbols;

    public SnapshotReader() {
        symbols = Collections.synchronizedSet(new HashSet<String>());
    }

    public void registerSymbol(String symbol) {
        symbols.add(symbol);
    }

    public void unregisterSymbol(String symbol) {
        symbols.remove(symbol);
    }

    public void update() {
        Iterator<String> isymbol = symbols.iterator();
        String symbol = isymbol.next();
        while (isymbol.hasNext()) {
            newUpdatedTime = DataStore.getSharedInstance().getStockObject(symbol).pricesDataChangedAt();
            if (lastUpdatedTime < newUpdatedTime) {
                lastUpdatedTime = newUpdatedTime;
                System.out.println("Updated  " + symbol);
            }
        }
    }
}
