package com.isi.csvr.trading;

import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.beans.TransferObject;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.Client;
import com.isi.csvr.Login;
import com.isi.csvr.LoginWindow;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.api.TradeServer;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.datastore.Rule;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.positionMoniter.PositionDataStore;
import com.isi.csvr.trading.shared.*;

import javax.net.ssl.*;
import javax.swing.*;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class AutoTradeLoginDaemon extends Thread {


    public static final int CONECTING = 0;
    public static final int STOPPED = 1;
    public static final int CONNECTED = 2;
    public static final int IDLE = 3;
    private static int connectionStatus = IDLE;
    public static final int DISCONNECTED = 4;
    public static final int SSO_PRICE_LOGIN_FAILED = 4;
    public static boolean PRIMARY_IP_FORCED = false;
    //PulseGenerator g_oPulseGenerator;
    private static boolean manualConnectMode = true;
    Hashtable g_oDataStore;
    private Socket socket = null;
    //    private Socket socketSecondary = null;
    private String user;
    private String pass;
    private OutputStream out;
    private IP currentIP;
    private IP previousIP;
    private ReceiveQueue receiveQueue;
    private TradePulseGenerator pulseGenerator = null;
    private boolean paswordChangeRequest = false;
    private FrameAnalyser frameAnalyser;
    private SecondaryLoginDeamon secondaryLogin = null;
    private WorkInProgressIndicator applyOfflineIndicator;

    public AutoTradeLoginDaemon() {
        super("AutoTradeLoginDaemon");
        start();
    }

    /**
     * Display the invalid authentication dialog
     */


    public static void sleepThread(long time) {
        try {
            sleep(time);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public synchronized static int getMode() {
        return connectionStatus;
    }

    public synchronized static void setMode(int status) {
        connectionStatus = status;
    }

    public synchronized static void setIdle() {
        if (TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
            connectionStatus = IDLE;
        } else if (connectionStatus == STOPPED) {
            IPSettings.getSharedInstance().resetIPIndex();
            connectionStatus = IDLE;
        }
    }

    public synchronized static void setReconnect() {
        if ((connectionStatus != CONECTING) && (connectionStatus != CONNECTED)) {
            connectionStatus = IDLE;
        }
    }

    public static synchronized void setManualConnectMode(boolean mode) {
        manualConnectMode = mode;
    }

    public void run() {
        while (true) {
            try {
                if (isReadyToConnect()) {
                    if ((TradingShared.isOkForTradeConnection()) && (!TradingShared.isConnected())) {
                        setMode(CONECTING);
                        System.err.println("reconnecting trading channel................");
                        sleepThread(500); // just in case, another requesr comes in
                        connect();
                    }
                }
                sleepThread(1000);
            } catch (Exception e) {
                e.printStackTrace();
                sleepThread(1000);
            }
        }
    }

    private boolean isReadyToConnect() {
        if (getMode() == IDLE) {
            return true;
        } else if ((getMode() == DISCONNECTED) && (Settings.isConnected())) {
            Client.removeLoyaltyPoints();
            int result = SharedMethods.showConfirmMessage(Language.getString("MSG_TRADING_DISCONNECTED"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                TradingShared.setLevel1Password(null);
                setMode(IDLE);
                IPSettings.getSharedInstance().resetIPIndex();
                return true;
            } else {
                TradingShared.setLevel1Password(null);
                setMode(STOPPED);
                Client.getInstance().getMenus().unlockTradeServerConnection();
                IPSettings.getSharedInstance().resetIPIndex();
                return false;
            }
        } else {
            return false;
        }
    }

    private synchronized void connect() {
        try {
            String ip;
//            int loopIndex = Integer.MAX_VALUE;

            while (true) {
                try {
                    /*if (IPSettings.getSharedInstance().getIpIndex() > IPSettings.getSharedInstance().getIPCount()){
                        loopIndex = 0;
                    }*/
                    if (getUserInfo(IPSettings.getSharedInstance().getNextIpIndex(), IPSettings.getSharedInstance().getIPCount())) {
//                        System.out.println("Index " + IPSettings.getSharedInstance().getIpIndex());


                        //         ip = "192.168.13.17";
                        ip = getNextIP();
                        if (previousIP != null && previousIP.getIP().equalsIgnoreCase(ip)) {
                            continue;                              //added by udaya to stop connecting to the same ip twice
                        }
                        //       ip = "192.168.17.34";
                        //       ip = "192.168.13.17";   //randula !!!!!!!!!!! be4 release
                        //     ip = "192.168.0.117";
//                        ip = "192.168.2.78";
                        //                  //ip = "192.168.13.213";
//                   ip = "192.168.2.60";
//                   ip = "192.168.2.183";
//                   ip = "192.168.2.138";
                        openSocket(ip);
//                        socket = openSocket(ip);
                        if (authenticate()) {
                            setMode(CONNECTED);
                            TradingShared.setConnecting(false);
                            TradingShared.setConnected();

                            while (!Settings.isConnected() && getMode() != SSO_PRICE_LOGIN_FAILED) {
                                Thread.sleep(1000);
                            }
                            if (getMode() == SSO_PRICE_LOGIN_FAILED) {
                                ssoPriceLoginFailed();
                                break;
                            }
                            initializeApp(); //todo  removed for testing
                            // sendInitialRequests();

                            if (secondaryLogin != null) {
                                secondaryLogin.setFrameAnalyser(frameAnalyser);
                                secondaryLogin.start();
                            }
                            /* Moved to the Settings class
                            if(Settings.isValidSystemWindow(Meta.WT_AllowDynamicIPs)){
                                IPSettings.resetIPIndex(); // make sure to go from the beginning next time
                            }*/
                            break;
                        } else {
//                            setMode(IDLE);
                            TradingShared.setConnecting(false);
                            closeSocket();
                            closeSecondarySocket();
//                            Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                        }
                    } else {
                        Client.getInstance().setTradeSessionDisconnected(true);
                        if (TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                            Client.getInstance().getMenus().unlockTradeServerConnection();
                        }
//                        Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
//                        Client.getInstance().setTradeSessionDisconnected(true);
                        break;
                    }

                } catch (SSLHandshakeException ssle) {
                    ssle.printStackTrace();
                    if (ssle.getCause() instanceof java.security.cert.CertificateExpiredException) {
                        showSSLExpiration(ssle);
                    }
                } catch (Exception ex1) {
                    ex1.printStackTrace();
//                    Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                }
                sleepThread(5000);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            sleepThread(5000);
            // no ips found exception
            setMode(DISCONNECTED);
        }
    }

    private void showSSLExpiration(SSLHandshakeException ssle) {
        String message = Language.getString("MSG_SSL_EXPIRED");
        message = message.replaceFirst("\\[DATE\\]", ssle.getCause().getLocalizedMessage());
        SharedMethods.showMessage(message, JOptionPane.ERROR_MESSAGE);
    }

    private String getNextIP() throws Exception {

        currentIP = IPSettings.getSharedInstance().getNextIP();
        String newip;

        newip = currentIP.getIP();
        return newip;
    }

    private void openSocket(String ip) throws Exception {

        try {
//            ip = "192.168.13.67";
//            ip = "192.168.13.14";
//            ip = "10.0.0.22";
            System.out.println("Connecting to Trading ip " + ip);
//            Socket socket;
            SSLSocketFactory factory = null;
            SSLContext ctx = SSLContext.getInstance("TLS");
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            KeyStore ks = KeyStore.getInstance("JKS");
            char[] passphrase = "mystorepass".toCharArray();
            char[] passphrase2 = "mykeypass".toCharArray();
            ks.load(new FileInputStream(Settings.CONFIG_DATA_PATH + "/trs.dll"), passphrase);
            kmf.init(ks, passphrase2);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            factory = ctx.getSocketFactory();
//            socket = factory.createSocket(ip, 9997);

            socket = factory.createSocket(ip, Settings.TRADE_ROUTER_PORT);
//            return socket;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private boolean getUserInfo(int loopIndex, int ipCount) {
        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
            if (!Settings.getTradeToken().equals("")) {
                user = Settings.getTradeToken();
            } else {
                user = Settings.getSingleSignOnID();
            }
            //user = Settings.getTradeToken();
            //user = Settings.getSingleSignOnID();
            pass = Settings.getInstitutionID();
            TradingShared.setLevel1Password(pass);
            TradingShared.setTradeUserName(user);
            Settings.setItem("TRADE_USER", "");
            TradingShared.setConnecting(true);
            return true;
        } else {
            //if (!isPasswordSaved()) {
            if (loopIndex == 0) {
                LoginWindow loginDialog = new LoginWindow(Client.getInstance().getFrame(), Login.MODE_TRADING_SERVERS);
                previousIP = null;
                currentIP = null;
//                Login loginDialog = new Login(Client.getInstance().getFrame(), Login.MODE_TRADING_SERVERS);
                while (true) {
                    if (loginDialog.showDialog()) {
                        user = loginDialog.getUserName().trim();
                        pass = loginDialog.getPassword().trim();
                        TradingShared.setLevel1Password(pass);
                        TradingShared.setTradeUserName(user);
                        if ((Settings.getItem("IS_TRADE_USER_SAVED") != null) && (Settings.getItem("IS_TRADE_USER_SAVED").equals("true"))) { //Bug Id <#0034> included null check 8.20.012
                            Settings.setItem("TRADE_USER", user);
                        } else {
                            Settings.setItem("TRADE_USER", "");
                        }
                        loginDialog.dispose();
                        TradingShared.setChangePassowrdNeeded(false);
                        TradingShared.setChangePWDType(TradingConstants.CHANGE_PASSWORD_TYPE_NONE);
//                        TradingShared.setUserDetailsNeeded(false);
                        TradingShared.setConnecting(true);
                        loginDialog = null;
                        return true;
                    } else {
                        pass = null;
                        TradingShared.setLevel1Password(null);
                        TradingShared.setConnecting(false);
                        loginDialog = null;
                        if (TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                            if (!Settings.isConnectedFirstTime && !Settings.isOfflineDataLoaded) {
                                applyOfflineIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_OFFLINE_DATA);
                                applyOfflineIndicator.setVisible(true);
                                Settings.setOfflineMode(true);
                                System.out.println("start of work in indicator");
                                new Thread("Offline Data Thread") {
                                    public void run() {
                                        Application.getInstance().fireLoadOfflineData();
                                        if (applyOfflineIndicator != null) {
                                            applyOfflineIndicator.dispose();
                                        }
                                        Settings.isOfflineDataLoaded = true;
                                    }
                                }.start();
                            }
                        }

                        return false; // login cancelled by user
                    }
                }
            } else {
                return true;
            }
        }
        //return true;
    }

    private boolean authenticate() throws Exception {

        TradeMessage tradeMessage;
        AuthenticationRequest mixaAthRequest = new AuthenticationRequest();
        MIXHeader mixHeader = new MIXHeader();
        mixHeader.setGroup(MIXConstants.GROUP_AUTHENTICATION);

        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
            tradeMessage = new TradeMessage(TradeMeta.MT_AUTH_SINGLE_SIGNON);
            mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_AUTH_SSO);

        } else {
            tradeMessage = new TradeMessage(TradeMeta.MT_AUTH);
            mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_AUTH_NORMAL);
        }


        tradeMessage.addData(user);
        tradeMessage.addData(pass);
        tradeMessage.addData(TradingShared.getSessionID());
        tradeMessage.addData(TradingShared.setIp(SharedMethods.getLocalIP()));
        tradeMessage.addData(Settings.TW_VERSION);
        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
//            if (Settings.getSsoId() != null)
//                mixaAthRequest.setSSOId(Settings.getSsoId());
//            else
//                mixaAthRequest.setSSOId(user);
            if (!Settings.getTradeToken().equals("")) {
                mixaAthRequest.setSSOId(Settings.getTradeToken());
            } else {
                mixaAthRequest.setSSOId(Settings.getSsoId());
            }
            mixaAthRequest.setInstitutionType(pass);
        } else {
            mixaAthRequest.setUsername(user);
            mixaAthRequest.setPassword(pass);
        }
//        TradingShared.setUUID(UUID.randomUUID().toString());
//        mixaAthRequest.setSessionID(TradingShared.getUUID());
        mixaAthRequest.setSessionID(TradingShared.getSessionID());
        mixaAthRequest.setIp(TradingShared.setIp(SharedMethods.getLocalIP()));
        mixaAthRequest.setClientVersion(Settings.TW_VERSION);
//        mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_TW + "");
        mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_NEWTW + "");
//        mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_NEWTW + "");

        TransferObject[] mixTransferObjects = new TransferObject[]{mixaAthRequest};
        MIXObject mixBasicObject = new MIXObject();
        mixBasicObject.setMIXHeader(mixHeader);
        mixBasicObject.setTransferObjects(mixTransferObjects);


        out = socket.getOutputStream();
        //    out.write(tradeMessage.toString().getBytes());
        out.write(mixBasicObject.getMIXString().getBytes());

        // AuthenticationRequest tempAuthReq = (AuthenticationRequest) request.getTransferObjects()[0];
//        System.out.println("Trade Authenication Request sent in primary" + tradeMessage.toString());
        InputStream in = socket.getInputStream();

//        SocketTimer soTimer = new SocketTimer((socket)); // Activate the socket timer
        readLine(in); // ignore the ACK message
        String g_sAuthResult = readLine(in); // 1 auth reply
        MIXObject mixReply = new MIXObject();
        mixReply.setMIXString(g_sAuthResult);
        AuthenticationResponse mixAuthReply = (AuthenticationResponse) mixReply.getTransferObjects()[0];
        System.out.println("============ Trade auth reply " + g_sAuthResult);
//        soTimer.deactivate(); // deactivate the socket timer
//        soTimer = null;
        Settings.setBurstReuestMode(false);
        //   mixAuthReply.get
        //    boolean authResult = isAuthenticationValid(g_sAuthResult, Constants.PATH_PRIMARY);
        boolean authResult = processMixAuthenticationResponse(mixAuthReply, Constants.PATH_PRIMARY, mixReply.getMIXHeader());
        System.out.println("result " + authResult);
        MixStore.getSharedInstance().setPrimaryAuthenticationObj(mixReply);
        return authResult;
    }

    public String readLine(InputStream in) throws Exception {
        int iValue;
        StringBuffer buffer = new StringBuffer();
        try {
            while (true) {
                iValue = in.read();
                if (iValue == -1)
                    throw new Exception("End of Stream");
                if (iValue != '\n') {
                    buffer.append((char) iValue);
                    continue;
                } else {
                    return buffer.toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private void initializeApp() throws Exception {
        BrokerConfig.getSharedInstance().loadData(TradingShared.getTrader().getBrokerID(Constants.PATH_PRIMARY), Constants.PATH_PRIMARY);
        TradingShared.setDisconnectionMode(TradingConstants.DISCONNECTION_UNEXPECTED); // clear any maual disconnection  requests
        TradingShared.getTrader().createAccounts();
        receiveQueue = new ReceiveQueue(socket, Constants.PATH_PRIMARY);
        SendQueue.getSharedInstance().init(socket, out, Constants.PATH_PRIMARY);
        SendQueue.getSharedInstance().startThread();
        TradePortfolios.getInstance().clear();
        TradingShared.resetMessageID();
        frameAnalyser = new FrameAnalyser();
        receiveQueue.setReceiveBuffer(frameAnalyser.getList());
        receiveQueue.start();
//        frameAnalyser.setQueue(receiveQueue.getReceiveBuffer());
        OrderStore.getSharedInstance().clear();
        OpenPositionsStore.getSharedInstance().clear();
        CurrencyStore.getSharedInstance().clearStore();
        sendInitialRequests();
//        if (pulseGenerator == null) {
        pulseGenerator = new TradePulseGenerator(Constants.PATH_PRIMARY);
        pulseGenerator.start();
//        }
        PositionDataStore.getSharedInstance().clear();
        TradingShared.getTrader().addAccountListener(Client.getInstance().getTradingPortfolio());
        TradingShared.getTrader().addAccountListener(Client.getInstance().getRapidOrderPanel());
        TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getOrderSearchWindow());
        TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getCashLogSearchWindow());
        TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getAccountWindow());
        if (TradeMethods.getSharedInstance().getBlotterFrame() != null)
            TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getBlotterFrame());
        if (TradeMethods.getSharedInstance().getStockTransferFrame() != null)
            TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getStockTransferFrame());
        if (TradeMethods.getSharedInstance().getFundTransferWindow() != null) {
            TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getFundTransferWindow());
        }
        // TradingShared.getTrader().addAccountListener(IntraDayPositionMoniterUI.getSharedInstance());
        TradingShared.getTrader().notifyAccountListeners(null);
        TradingShared.getTrader().notifyPortfolioListeners(null);
        OrderDepthStore.getSharedInstance().clear();

        try {
            Rule amendMode = RuleManager.getSharedInstance().getRule("AMEND_MODE_DELTA", "*", "NEW_ORDER");
            if ((amendMode.getRule().equals("1") || amendMode.getRule().equals("true"))) {
                TradingShared.setAmendMode(TradingShared.AMEND_MODE_DELTA);
            } else {
                TradingShared.setAmendMode(TradingShared.AMEND_MODE_FULL);
            }
        } catch (Exception e) {
            TradingShared.setAmendMode(TradingShared.AMEND_MODE_FULL);
        }

        if (TradingShared.getChangePWDType() == TradingConstants.CHANGE_PASSWORD_TYPE_VALIDATION) {
//        if(TradingShared.isUserDetailsNeeded()){
            TradeMessage message = new TradeMessage(TradeMeta.MT_AUTH_VALIDITY);
            message.addData("");
            //  frameAnalyser.getList().add(message.toString());
            message = null;
        } else if (paswordChangeRequest) {
            TradeMessage message = new TradeMessage(TradeMeta.MT_CHANGE_PW);
            message.addData("");
            //    frameAnalyser.getList().add(message.toString());
            message = null;

            MIXObject changePasswordReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_AUTHENTICATION, MIXConstants.RESPONSE_TYPE_CHANG_PWD, Constants.PATH_PRIMARY);
            ChangePasswordResponse resp = new ChangePasswordResponse();
            resp.setUserID("N/A");
            changePasswordReq.setTransferObjects(new TransferObject[]{resp});
            frameAnalyser.getList().add(changePasswordReq);
        }
        FundTransferStore.getSharedInstance().clear();
//        TradeMethods.getSharedInstance().pendingFundTransfer(Constants.PATH_PRIMARY);
        StockTransferObjectStore.getSharedInstance().clear();
        //TradeMethods.getSharedInstance().pendingStockTransfer();
        TradingConnectionNotifier.getInstance().fireTWConnected();
//        OMSMessageList.getSharedInstance().requestOMSMessages();
        Client.getInstance().tradingSessionEnabled();
        if (TradingShared.isTradingAPIEnabled()) {
            TradeServer.start();
        }
    }

    private void initMessageQueues() throws Exception {
        // receiveQueue = new ReceiveQueue(socket, Constants.PATH_PRIMARY);
        SendQueue.getSharedInstance().init(socket, out, Constants.PATH_PRIMARY);
        SendQueue.getSharedInstance().startThread();
        // frameAnalyser = new FrameAnalyser();
        // receiveQueue.setReceiveBuffer(frameAnalyser.getList());
        //  receiveQueue.start();
        pulseGenerator = new TradePulseGenerator(Constants.PATH_PRIMARY);
        pulseGenerator.start();
    }

    private void sendInitialRequests() {
        try {
            if (Settings.isBurstRequestMode()) {
                TradeMethods.requestUserDataPrimary(Constants.PATH_PRIMARY, true);
                TradingShared.setReadyForTrading(true);
            } else {
                //  TradeMethods.requestUserDataPrimary(Constants.PATH_PRIMARY, false);  //todo removed for testing
                TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);        //todo removed for testing
                TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
                TradeMethods.requestCurrencyData(Constants.PATH_PRIMARY);
//                TradeMethods.requestTPlusData(Constants.PATH_PRIMARY);
//                TradeMethods.requestTPlusCommisionData(Constants.PATH_PRIMARY);

                TradingShared.setReadyForTrading(true);  // todo  added for testing remove

            }
            TradeMethods.requestBankAccountDetails(Constants.PATH_PRIMARY);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isPasswordSaved() {
        return ((TradingShared.getLevel1Password() != null) && ((user != null) && (!user.trim().equals(""))));
    }

    public synchronized boolean isAuthenticationValid(String sFrame, byte loginType) {

        String[] messageBlocks;
        String userID = null;
        int authStatus = 0;
        Trader trader = null;

        try {
            TradeMessage message = new TradeMessage(sFrame);
            if (loginType == Constants.PATH_PRIMARY) {
                RuleManager.getSharedInstance().clear(); // clear all rules and get ready for new data
                TradingShared.setTrsID(message.TRSID);
            }
            messageBlocks = message.getMessageData().split(TradeMeta.RS);
            for (int i = 0; i < messageBlocks.length; i++) {
                String[] messageData = messageBlocks[i].split(TradeMeta.DS);
                if (messageData[0].equals(TradeMeta.BLOCK_TYPE_AUTH)) {
                    for (int j = 1; j < messageData.length; j++) {
                        if (messageData[j].startsWith("A")) {
                            userID = messageData[j].substring(1);
                            if (loginType == Constants.PATH_SECONDARY) {
                                trader = TradingShared.getTrader();
                                trader.setUserID(userID);
                                trader.setMubasherID(secondaryLogin.getMubasherID());
                                trader.setPath(Constants.PATH_SECONDARY);
                            } else {
                                trader = TradingShared.createTrader();
                                trader.setUserID(userID);
                                trader.setMubasherID(user);
                                trader.setPath(Constants.PATH_PRIMARY);
                            }
                        } else if (messageData[j].startsWith("B")) {
                            authStatus = SharedMethods.intValue(messageData[j].substring(1));
                        } else if (messageData[j].startsWith("C")) {
                            if (loginType == Constants.PATH_PRIMARY) {
                                showInvalidAuthMessage(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(messageData[j].substring(1), "|")));
                            }
                        } else if (messageData[j].startsWith("D")) {
                            if (loginType == Constants.PATH_PRIMARY) {
                                TradingShared.setLevel2AuthType(SharedMethods.intValue(messageData[j].substring(1)));
                            }
                            trader.setLevel2AuthType(SharedMethods.intValue(messageData[j].substring(1)));
                        } else if (messageData[j].startsWith("F")) {
                            TradingShared.setCurrentDay(messageData[j].substring(1));
                            trader.setCurrentDay(messageData[j].substring(1));
                        } else if (messageData[j].startsWith("G")) {
                            if (loginType == Constants.PATH_PRIMARY) {
                                TradingShared.setSessionID(messageData[j].substring(1));
                            }
                            trader.setSessionID(messageData[j].substring(1));
                        } else if (messageData[j].startsWith("I")) {
//                            TradingShared.getTrader().addExchanges(messageData[j].substring(1));
                            trader.addExchanges(messageData[j].substring(1));
                        } else if (messageData[j].startsWith("J")) {
//                            if(loginType == Constants.PATH_PRIMARY){
//                                trader.setTraderName(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(messageData[j].substring(1), "|")));
//                            }
                            trader.setTraderName(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(messageData[j].substring(1), "|")));
                        } else if (messageData[j].startsWith("K")) {
                            if (loginType == Constants.PATH_PRIMARY) {
                                TradingShared.setBrokerID(messageData[j].substring(1));
                            }
                            trader.setBrokerID(messageData[j].substring(1));
                        } else if (messageData[j].startsWith("L")) {
                            TradingShared.setReportURL(messageData[j].substring(1));
                            trader.setReportURL(messageData[j].substring(1));
                        } else if (messageData[j].startsWith("M")) {
                            try {
                                TradingShared.setReportDepth(Integer.parseInt(messageData[j].substring(1)));
                                trader.setReportDepth(Integer.parseInt(messageData[j].substring(1)));
                            } catch (Exception e) {
                                TradingShared.setReportDepth(6);
                                trader.setReportDepth(6);
                            }
                        } else if (messageData[j].startsWith("N")) {
                            // Institution ID
                        } else if (messageData[j].startsWith("O")) {
                            trader.setBanks(messageData[j].substring(1));
                        } else if (messageData[j].startsWith("P")) {
                            if (loginType == Constants.PATH_PRIMARY) {
                                assignContactDetails(messageData[j].substring(1));
                            }
                        } else if (messageData[j].startsWith("Q")) {
//                            TradingShared.setUserDetailsNeeded((Integer.parseInt(messageData[j].substring(1))==1));//todo changed this to 0 to test
                            if (loginType == Constants.PATH_PRIMARY) {
                                TradingShared.setChangePWDType(Byte.parseByte(messageData[j].substring(1)));
                            }
                        } else if (messageData[j].startsWith("R")) {
                            if (loginType == Constants.PATH_PRIMARY) {
                                TradingShared.setQuestionList(messageData[j].substring(1));
                            }
                        } else if (messageData[j].startsWith("S")) {
                            ExchangeStore.getSharedInstance().addTradingWindowTypes(messageData[j].substring(1), loginType);
//                            System.out.println(" === Trading Window Types " + messageData[j].substring(1));
                        } else if (messageData[j].startsWith("T")) {
                            try {
                                if (loginType == Constants.PATH_PRIMARY) {
                                    TradingShared.setMaxCondionCount(Integer.parseInt(messageData[j].substring(1)));
                                }
                            } catch (NumberFormatException e) {
//                                e.printStackTrace();
                            }
                        } else if (messageData[j].startsWith("U")) {
//                            trader.setMubasherID((messageData[j].substring(1)));
                        } else if (messageData[j].startsWith("Y")) {
                            if ((loginType == Constants.PATH_PRIMARY) && TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                                Settings.setSingleSignOnMode(true);
                                Settings.setSingleSignOnID(messageData[j].substring(1));
                            }
                        } else if (messageData[j].startsWith("@")) {
                            if ((loginType == Constants.PATH_PRIMARY) && TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                                Settings.setSingleSignOnMode(true);
                                Settings.setInstitutionID(messageData[j].substring(1));
                            }
                        } else if (messageData[j].startsWith("a")) {
                            if (loginType == Constants.PATH_PRIMARY) {
                                if (messageData[j].substring(1).equals("1")) {
                                    Settings.setBurstReuestMode(true);
                                } else {
                                    Settings.setBurstReuestMode(false);
                                }
                            } else if (loginType == Constants.PATH_SECONDARY) {
                                if (messageData[j].substring(1).equals("1")) {
                                    Settings.setBurstReuestModeSecondary(true);
                                } else {
                                    Settings.setBurstReuestModeSecondary(false);
                                }
                            }
                        } else if (messageData[j].startsWith("b")) {
                            Client.setLoyaltyPoints(messageData[j].substring(1));


                        } else if (messageData[j].startsWith("c")) {
                            ExchangeStore.getSharedInstance().addTradingWindowTypes(messageData[j].substring(1), loginType);
                        }

                    }
                    /*} else if (messageData[0].equals(TradeMeta.BLOCK_TYPE_ACCOUNT)) {
                    if (trader != null) {
                        for (int j = 1; j < messageData.length; j++) {
                            if (messageData[j].startsWith("M")) {
                                trader.createAccount(messageData[j].substring(1));
                            }
                        }
                    }*/
                } else if (messageData[0].equals(TradeMeta.BLOCK_TYPE_PORTFOLIO)) {
                    if (trader != null) {
                        ArrayList<BookKeeper> bookKeepers = new ArrayList<BookKeeper>();
                        String portfolioID = null;
                        String brokerID = null;
                        int status = 0;
                        String accountNumber = null;
                        String name = null;
                        boolean isDefault = true;
                        int requiredCOunt = 0;
                        String allowedExchanges = null;

                        for (int j = 1; j < messageData.length; j++) {
                            //System.out.println(messageData[j]);
                            if (messageData[j].startsWith("a")) {
                                portfolioID = messageData[j].substring(1);
                                requiredCOunt++;
                            } else if (messageData[j].startsWith("b")) {
                                brokerID = messageData[j].substring(1);
                                /*} else if (messageData[j].startsWith("c")) {
                                    accountNumber = messageData[j].substring(1);*/
                            } else if (messageData[j].startsWith("d")) {
                                status = SharedMethods.intValue(messageData[j].substring(1));
                            } else if (messageData[j].startsWith("e")) {
//                                name = messageData[j].substring(1);
                                name = Language.getLanguageSpecificString(messageData[j].substring(1), "|");
                            } else if (messageData[j].startsWith("f")) {
                                isDefault = SharedMethods.intValue(messageData[j].substring(1)) == 1;
                            } else if (messageData[j].startsWith("g")) {
                                accountNumber = messageData[j].substring(1);
                            } else if (messageData[j].startsWith("h")) {
                                allowedExchanges = messageData[j].substring(1);
                            } else if (messageData[j].startsWith("k")) {
                                System.out.println("BOOK KEEPERS : " + messageData[j].substring(1));
                                try {
                                    for (String bookKeeperFrame : messageData[j].substring(1).split(":")) {
                                        if (!bookKeeperFrame.equals(null)) {
                                            String[] bookKeeperData = bookKeeperFrame.split("~");
                                            if (bookKeeperData.length > 0) {
                                                BookKeeper bk = new BookKeeper();
                                                bk.setId(bookKeeperData[0]);
                                                bk.setCode(bookKeeperData[1]);
                                                bk.setName(UnicodeUtils.getNativeString(bookKeeperData[2]));
                                                bk.setDefault(bookKeeperData[3].equals("1"));
                                                bookKeepers.add(bk);
                                                TPlusStore.getSharedInstance().addBookKeeper(bookKeeperData[1], bk);
                                                TradingShared.setBookKeepersAvailable(true);
                                            }

                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();

                                }

                            }
                        }
                        if (requiredCOunt >= 1) { // check if required fields are found
                            trader.createPortfolio(portfolioID, brokerID, status, accountNumber, name, isDefault, allowedExchanges, bookKeepers);
                        }
                        accountNumber = null;
                        name = null;
                    }
                } else if (messageData[0].equals(TradeMeta.BLOCK_TYPE_RULES)) {
                    String id = null;
                    String exchange = null;
                    String group = null;
                    String rule = null;
                    String errormessage = null;
                    int requiredCOunt = 0;
                    String portfolioID = "*";

                    for (int j = 1; j < messageData.length; j++) {
                        if (messageData[j].startsWith("a")) {
                            id = messageData[j].substring(1);
                            requiredCOunt++;
                        } else if (messageData[j].startsWith("b")) {
                            exchange = messageData[j].substring(1);
                            requiredCOunt++;
                        } else if (messageData[j].startsWith("c")) {
                            group = messageData[j].substring(1);
                            requiredCOunt++;
                        } else if (messageData[j].startsWith("d")) {
                            rule = messageData[j].substring(1);
                            requiredCOunt++;
                        } else if (messageData[j].startsWith("e")) {
                            errormessage = Language.getLanguageSpecificString(messageData[j].substring(1), "|");
                            requiredCOunt++;
                        } else if (messageData[j].startsWith("f")) {
                            portfolioID = messageData[j].substring(1);
                        }
//                            requiredCOunt++;
                        /*else if(messageData[j].startsWith("J")){
                            TradingShared.setMubasherID( UnicodeUtils.getNativeString(messageData[j].substring(1)));
                            System.out.println("NAme Rules---------------------------------------"+UnicodeUtils.getNativeString(messageData[j].substring(1)));

                            }*/
                    }
                    if (requiredCOunt == 5) {
                        if (!group.equals("COMMISSION")) {
                            RuleManager.getSharedInstance().setRule(id, exchange, group, rule, errormessage, portfolioID);
                        } else {
//                            String commisionRule="double commision=0;double totalCommision=0;String[] arr=list.split(\"~\");for(int i=0;i<arr.length;i++){String[] arr2=arr[i].split(\",\");double max=Double.parseDouble(arr2[0]);double min=Double.parseDouble(arr2[1]);double flat=Double.parseDouble(arr2[2]);double pct=Double.parseDouble(arr2[3]); commision=(price*quantity*pct/100)+(flat*currencyRate);if((commision!=0)&&(commision<min)){commision=min;}else if((commision!=0)&&(commision>max)){commision=max;}totalCommision=totalCommision+commision;}return totalCommision;";
                            if ((!rule.equals("")) && (rule != null)) {
                                RuleManager.getSharedInstance().setRule(id, exchange, group, rule, errormessage, portfolioID);
                            }
//                            RuleManager.getSharedInstance().setRule(id, exchange, group, commisionRule, errormessage, portfolioID);
                            /*
itemCommission = (orderValue * commissionBean.comPct / 100)
					+ (commissionBean.comFlat * commissionBean.currencyRate);

			if (commissionBean.comMin != 0
					&& itemCommission < commissionBean.comMin) {
				itemCommission = commissionBean.comMin;
			} else if (commissionBean.comMax != 0
					&& itemCommission > commissionBean.comMax) {
				itemCommission = commissionBean.comMax;
			}

                             */
                        }
                    }
                } else if (messageData[0].equals(TradeMeta.BLOCK_TYPE_SECOND_LOGIN)) {
                    String secondaryIP = null;
                    String mubasherNumber = null;
                    for (int j = 1; j < messageData.length; j++) {
                        if (messageData[j].startsWith("a")) {
                            mubasherNumber = messageData[j].substring(1);
                        } else if (messageData[j].startsWith("b")) {
                            secondaryIP = messageData[j].substring(1);
                        }
                    }
                    TradingShared.INVALID_SECONDARY_LOGIN = false;
                    TradingShared.setSecondaryLoginEnabled(true);
                    secondaryLogin = new SecondaryLoginDeamon(mubasherNumber, secondaryIP, this);
                }
                messageData = null;
            }
//            RuleManager.getSharedInstance().setRule("AMEND_MODE_DELTA", "*", "NEW_ORDER", "1", "X", "");

            message = null;
            System.out.println("auth result ---> " + authStatus);
            if (loginType == Constants.PATH_PRIMARY) {
                TradingShared.setChangePassowrdNeeded((authStatus == TradeMeta.AUTH_CHANGE_PW));
            }
            if (authStatus == TradeMeta.AUTH_OK) {
                if (loginType == Constants.PATH_PRIMARY) {
                    paswordChangeRequest = false;
                    Login.setTradeLoginSucess(true);
                }
                return true;
            } else if (authStatus == TradeMeta.AUTH_CHANGE_PW) {
                if (loginType == Constants.PATH_PRIMARY) {
                    paswordChangeRequest = true;
                    TradingShared.setLevel1Password(null);
                } else {
                    TradingShared.INVALID_SECONDARY_LOGIN = true;
                }
                return true;
            } else {
                //showInvalidAuthMessage(Language.getString("INVALID_USER_OR_PASS"));
                if (loginType == Constants.PATH_PRIMARY) {
                    Login.setTradeLoginSucess(false);
                    paswordChangeRequest = false;
                    TradingShared.setLevel1Password(null);
                } else {
                    TradingShared.INVALID_SECONDARY_LOGIN = true;
                }
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean processMixAuthenticationResponse(AuthenticationResponse mixAuthResponse, byte loginType, MIXHeader header) {
        try {
            int authStatus = mixAuthResponse.getAuthenticationStatus();
            Trader trader = null;
            MixStore.getSharedInstance().clear(loginType);

            /*======= Authentication General ============*/
            if (loginType == Constants.PATH_PRIMARY) {
                RuleManager.getSharedInstance().clear(); // clear all rules and get ready for new data
                TradingShared.setTrsID(header.getTrsID());    //todo check TRSID
            }

            if (loginType == Constants.PATH_PRIMARY && (authStatus == TradeMeta.AUTH_ERROR || authStatus == TradeMeta.AUTH_LOCKED)) {
                showInvalidAuthMessage(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(mixAuthResponse.getRejectReason())));
                IPSettings.getSharedInstance().resetIPIndex();
                return false;
            }

            if (loginType == Constants.PATH_SECONDARY) {
                trader = TradingShared.getTrader();
                trader.setUserID(mixAuthResponse.getUserID());
                trader.setMubasherID(secondaryLogin.getMubasherID());
                trader.setPath(Constants.PATH_SECONDARY);
                //Settings.setBurstReuestMode(); todo check for burst request mode
            } else {
                trader = TradingShared.createTrader();
                trader.setUserID(mixAuthResponse.getUserID());
                trader.setMubasherID(user);
                trader.setPath(Constants.PATH_PRIMARY);

                //Settings.setBurstReuestMode(); todo check for burst request mode
            }

            if (loginType == Constants.PATH_PRIMARY) {
                TradingShared.setLevel2AuthType(mixAuthResponse.getLevel2AuthType());
                TradingShared.setChangePassowrdNeeded((authStatus == TradeMeta.AUTH_CHANGE_PW));
                TradingShared.setLevel2AuthType(mixAuthResponse.getLevel2AuthType());
                TradingShared.setSessionID(mixAuthResponse.getSSessionID());
                TradingShared.setBrokerID(mixAuthResponse.getBrokerID());
                assignContactDetails(mixAuthResponse.getContactDetails());
//                try {
//                   // TradingShared.setChangePWDType(Byte.parseByte(mixAuthResponse.getUserDetailsNeeded()));  // todo check fo changePWD type
//                } catch (NumberFormatException e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                }
                //TradingShared.setQuestionList(); todo check for questoin list
                if (mixAuthResponse.getMaxConditionCount() != null) {
                    TradingShared.setMaxCondionCount(Integer.parseInt(mixAuthResponse.getMaxConditionCount()));
                }
                TradingShared.setDuInvestURL(mixAuthResponse.getDUTradeUrl());
            }

            TradingShared.setCurrentDay(mixAuthResponse.getCurrentDay());
            TradingShared.setReportURL(mixAuthResponse.getReportUrl());
            //TradingShared.setReportDepth(mixAuthResponse.getr);  // todo check for report depth

            trader.setLevel2AuthType(mixAuthResponse.getLevel2AuthType());
            trader.setCurrentDay(mixAuthResponse.getCurrentDay());
            trader.setSessionID(mixAuthResponse.getSSessionID());
            try {
                trader.addExchanges(mixAuthResponse.getExchanges());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            //  trader.setTraderName(mixAuthResponse.getCustomerName()); //todo check Trader name
            trader.setTraderName(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(mixAuthResponse.getCustomerName()))); //todo check Trader name
            trader.setBrokerID(mixAuthResponse.getBrokerID());
            trader.setReportURL(mixAuthResponse.getReportUrl());
            //trader.setReportDepth();     todo check for report depth
            trader.setBanks(mixAuthResponse.getUserBankIDs()); //todo check for banks
            //   trader.setBanks(mixAuthResponse.getUserID(),new String[]{mixAuthResponse.getUserBankIDs()}); //todo check for banks

            ExchangeStore.getSharedInstance().addTradingWindowTypes(mixAuthResponse.getTradingWindowTypes(), loginType); //todo check for trading window typs


            if ((loginType == Constants.PATH_PRIMARY) && TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                Settings.setSingleSignOnMode(true);
                //            Settings.setSingleSignOnID(mixAuthResponse.get);// todo check for SSO
                if (mixAuthResponse.getEncryptedSecurityToken() == null || mixAuthResponse.getEncryptedSecurityToken().equalsIgnoreCase("null")
                        || mixAuthResponse.getEncryptedSecurityToken().trim().isEmpty()) {
                    String errMsg = Language.getString("SSO_PRICE_USER_NULL");
                    SharedMethods.showBlockingMessage(errMsg, JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                Settings.setSingleSignOnID(mixAuthResponse.getEncryptedSecurityToken());// todo check for SSO
                Settings.setInstitutionID(mixAuthResponse.getInstitutionID());
                Settings.setBrokerInstitutionID(mixAuthResponse.getBrokerInstitutionID());

            }
            //  Settings.setInstitutionID(mixAuthResponse.getInstitutionID());

            /*======= Authentication General ============*/

            /*============= Portfolio Creation =================   */
            if (trader != null) {
                trader.createPortfolios(mixAuthResponse.getPortfolios());
            }
            //  createPortfolio(mixAuthResponse,trader);
            /*============= Portfolio Creation =================   */
            setRules(mixAuthResponse.getRules());

            //   mixAuthResponse.get
//            if (mixAuthResponse.getSecondaryLoginAvailable() != null && mixAuthResponse.getSecondaryLoginAvailable().equalsIgnoreCase("1")) {
            if (mixAuthResponse.getSecondaryOMSIP() != null && (!mixAuthResponse.getSecondaryOMSIP().isEmpty()) &&
                    mixAuthResponse.getSecondaryOMSLoginName() != null && (!mixAuthResponse.getSecondaryOMSLoginName().isEmpty())) {
                String secondaryIP = null;
                String mubasherNumber = null;
                secondaryIP = mixAuthResponse.getSecondaryOMSIP();
                mubasherNumber = mixAuthResponse.getSecondaryOMSLoginName();
                TradingShared.INVALID_SECONDARY_LOGIN = false;
                TradingShared.setSecondaryLoginEnabled(true);
                secondaryLogin = new SecondaryLoginDeamon(mubasherNumber, secondaryIP, this);
            }

            if (loginType == Constants.PATH_PRIMARY) {
                TradingShared.setChangePassowrdNeeded((authStatus == TradeMeta.AUTH_CHANGE_PW));
            }
            if (authStatus == TradeMeta.AUTH_OK) {
                if (loginType == Constants.PATH_PRIMARY) {
                    paswordChangeRequest = false;
                    Login.setTradeLoginSucess(true);
                }
                return true;
            } else if (authStatus == TradeMeta.AUTH_CHANGE_PW) {
                if (loginType == Constants.PATH_PRIMARY) {
                    paswordChangeRequest = true;
                    TradingShared.setLevel1Password(null);
                } else {
                    TradingShared.INVALID_SECONDARY_LOGIN = true;
                }
                return true;
            } else {
                //showInvalidAuthMessage(Language.getString("INVALID_USER_OR_PASS"));
                if (loginType == Constants.PATH_PRIMARY) {
                    Login.setTradeLoginSucess(false);
                    paswordChangeRequest = false;
                    TradingShared.setLevel1Password(null);
                } else {
                    TradingShared.INVALID_SECONDARY_LOGIN = true;
                }
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return false;
        }


    }

    public void setRules(ArrayList<com.dfn.mtr.mix.beans.Rule> rules) {
        if (rules != null && !rules.isEmpty()) {

            for (int i = 0; i < rules.size(); i++) {
                String id = null;
                String exchange = null;
                String group = null;
                String rule = null;
                String errormessage = "";
                int requiredCOunt = 0;
                String portfolioID = "*";
                com.dfn.mtr.mix.beans.Rule r = rules.get(i);

                id = r.getRuleID();
                exchange = r.getExchange();
                group = r.getGroupID();
                rule = r.getCondition();
                errormessage = r.getErrorMsg();
                if (errormessage == null) {
                    errormessage = "";
                }

                //      errormessage = r.getMessage();
                //    portfolioID = r.getPortfolio();

                if ((id != null) && (exchange != null) && (group != null) && (rule != null) && (errormessage != null)) {
                    if (!group.equals("COMMISSION")) {
                        RuleManager.getSharedInstance().setRule(id, exchange, group, rule, errormessage, portfolioID);
                    } else {
//                            String commisionRule="double commision=0;double totalCommision=0;String[] arr=list.split(\"~\");for(int i=0;i<arr.length;i++){String[] arr2=arr[i].split(\",\");double max=Double.parseDouble(arr2[0]);double min=Double.parseDouble(arr2[1]);double flat=Double.parseDouble(arr2[2]);double pct=Double.parseDouble(arr2[3]); commision=(price*quantity*pct/100)+(flat*currencyRate);if((commision!=0)&&(commision<min)){commision=min;}else if((commision!=0)&&(commision>max)){commision=max;}totalCommision=totalCommision+commision;}return totalCommision;";
                        if ((!rule.equals("")) && (rule != null)) {
                            RuleManager.getSharedInstance().setRule(id, exchange, group, rule, errormessage, portfolioID);
                        }

                    }

                }
            }
        }
    }

    private void createPortfolio(AuthenticationResponse mixAuthResponse, Trader trader) {
        if (trader != null) {
            ArrayList<BookKeeper> bookKeepers = new ArrayList<BookKeeper>();
            String portfolioID = null;
            String brokerID = null;
            int status = 0;
            String accountNumber = null;
            String name = null;
            boolean isDefault = true;
            int requiredCOunt = 0;
            String allowedExchanges = null;
            if (mixAuthResponse.getPortfolios() != null && !mixAuthResponse.getPortfolios().isEmpty()) {
                ArrayList<PortfolioHeader> portfolios = mixAuthResponse.getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    PortfolioHeader pfRecord = portfolios.get(i);
                    portfolioID = pfRecord.getPortflioID();


                }


            }

            // requiredCOunt = mixAuthResponse.getp

        }
    }

    private void assignContactDetails(String data) {
        try {
            String[] fields = data.split("\\|");
            Settings.setBrokerPhone(fields[0]);
            Settings.setBrokerFax(fields[1]);
            Settings.setBrokerURL(fields[2]);
        } catch (Exception e) {
        }
    }

    public void showInvalidAuthMessage(String message) {
        Object[] options = {
                Language.getString("OK")};
        JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                message,
                Language.getString("ERROR"),
                JOptionPane.OK_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);
    }

    public void closeSocket() {
        try {
            ExchangeStore.getSharedInstance().clearTradingWindowTypes();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeSecondarySocket() {
        if (secondaryLogin != null) {
            secondaryLogin.closeSocket();
            secondaryLogin = null;
        }
    }

    private void ssoPriceLoginFailed() {
        TradingShared.setConnecting(false);
        closeSocket();
        closeSecondarySocket();
        setMode(IDLE);
        IPSettings.getSharedInstance().resetIPIndex();
    }
}
 