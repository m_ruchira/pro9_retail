package com.isi.csvr.trading;

import com.dfn.mtr.mix.beans.MIXHeader;
import com.dfn.mtr.mix.beans.MIXObject;
import com.dfn.mtr.mix.beans.Pulse;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 21, 2004
 * Time: 1:44:05 PM
 */
public class TradePulseGenerator extends Thread {

    private byte path = Constants.PATH_PRIMARY;

    public TradePulseGenerator(byte path) {
        super("TradePulseGenerator");
        this.path = path;
    }

    public void run() {
        TradeMessage message = new TradeMessage(TradeMeta.MT_PULSE);
        message.addData("0");
        String PULSE = message.toString();
        MIXHeader mixHeader = new MIXHeader();
        mixHeader.setGroup(MIXConstants.GROUP_SYSTEM);
        mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_PULSE);
        Pulse pulse = new Pulse();
        pulse.setMessageData("0");
        com.dfn.mtr.mix.beans.TransferObject[] mixTransferObjects = new com.dfn.mtr.mix.beans.TransferObject[]{pulse};
        MIXObject mixBasicObject = new MIXObject();
        mixBasicObject.setMIXHeader(mixHeader);
        mixBasicObject.setTransferObjects(mixTransferObjects);

        while (TradingShared.isConnected()) {
            try {
                if (TradingShared.isConnected() && ((path == Constants.PATH_PRIMARY) || ((path == Constants.PATH_SECONDARY) && TradingShared.SECONDARY_LOGIN_SUCCESS))) {
//                    SendQueue.getSharedInstance().addData(PULSE, Constants.PATH_PRIMARY);
                    //  SendQueue.getSharedInstance().addData(PULSE, path);
                    SendQueue.getSharedInstance().addData(mixBasicObject.getMIXString(), path);
                }
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
