/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Feb 21, 2007
 * Time: 1:35:54 PM
 * To change this template use File | Settings | File Templates.
 */
package com.isi.csvr;

import com.isi.csvr.shared.TWFont;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import java.awt.*;


public class CWTabbedPaneUI extends BasicTabbedPaneUI implements Themeable {
    private static final Insets NO_INSETS = new Insets(0, 0, 0, 0);

    /**
     * The height that we want the button bar to be
     */
    private int buttonHeight = 13;
    private int unselectedTab = 10;

    /**
     * The (calculated) color of the button bar and the tabs
     */
    private Color selectedTabColor = null;
    private Color unselectedTabColor = null;
    private Color selectedTextColor = null;
    private Color unselectedTextColor = null;

    /**
     * The background color of the control
     */
    private Color background = null;

    /**
     * A color that is one step darker than the background color
     */
    private Color backgroundDarker1 = null;

    /**
     * A color that is two steps darker than the background color
     */
    private Color backgroundDarker2 = null;

    /**
     * Calculated colors to create a faded shadow
     */
    private Color[] fadeColors;

    /**
     * The number of pixels we want to use to create the shadow
     */
    private int fadeColorCount = 3;

    /**
     * The color of the text of a selected tab
     */
//    private Color selectedTextColor = Color.green;

    /**
     * The number of pixels the first tab should be inset from the left margin
     */
    private int leftInset = 6;

    // ------------------------------------------------------------------------------------------------------------------
    //  Custom installation methods
    // ------------------------------------------------------------------------------------------------------------------

    public static ComponentUI createUI(JComponent c) {
        return new CWTabbedPaneUI();
    }

    protected void installComponents() {
        super.installComponents();

//        selectedTabColor = tabPane.getBackground().darker().darker();
//        unselectedTabColor = tabPane.getBackground().darker().darker();

        background = tabPane.getBackground();
//        backgroundDarker1 = tabPane.getBackground().darker();
//        backgroundDarker2 = tabPane.getBackground().darker().darker();
        applyTheme();
        fadeColors = new Color[fadeColorCount];
        for (int i = 0; i < fadeColorCount; i++) {
            int rs = unselectedTabColor.getRed();
            int gs = unselectedTabColor.getGreen();
            int bs = unselectedTabColor.getBlue();

            int rt = background.getRed();
            int gt = background.getGreen();
            int bt = background.getBlue();

            int rn = (int) Math.min(rs + (Math.abs(rt - rs) / 4 * i), 255);
            int gn = (int) Math.min(gs + (Math.abs(gt - gs) / 4 * i), 255);
            int bn = (int) Math.min(bs + (Math.abs(bt - bs) / 4 * i), 255);

            fadeColors[i] = new Color(rn, gn, bn);
        }

    }

    protected void installDefaults() {
        super.installDefaults();
        tabAreaInsets.left = leftInset;
        selectedTabPadInsets = new Insets(0, 0, 0, 0);
    }

    // ------------------------------------------------------------------------------------------------------------------
    //  Custom sizing methods
    // ------------------------------------------------------------------------------------------------------------------

    public int getTabRunCount(JTabbedPane pane) {
        return 1;
    }

    protected Insets getContentBorderInsets(int tabPlacement) {
        return NO_INSETS;
    }

    protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) {
        if (tabPlacement == tabIndex) {
            return 14;
        } else {
            return 14 + (14 / 2) + 2;
        }
    }

    protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics) {
        return super.calculateTabWidth(tabPlacement, tabIndex, metrics);//+ 14
    }

    // ------------------------------------------------------------------------------------------------------------------
    //  Custom painting methods
    // ------------------------------------------------------------------------------------------------------------------

    protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex) {
        int tw = tabPane.getBounds().width;
        int[] x = new int[3];
        int[] y = new int[3];

        g.setColor(background);
        g.fillRect(0, 0, tw, tabPane.getBounds().height);

        ////////////////////////////////////////////////
        switch (tabPlacement) {

            case BOTTOM:
                g.fillRect(0, 0, tw, unselectedTab);
                g.fillRect(unselectedTab / 2, unselectedTab, tw - unselectedTab, unselectedTab / 2 + 1);

                // Left Polygon
                x[0] = 0;
                y[0] = unselectedTab;
                x[1] = unselectedTab / 2;
                y[1] = unselectedTab + (unselectedTab / 2);
                x[2] = unselectedTab / 2;
                y[2] = unselectedTab;
                g.fillPolygon(x, y, 3);

                // Right Polygon
                x[0] = tw;
                y[0] = unselectedTab;
                x[1] = tw - unselectedTab / 2;
                y[1] = unselectedTab + (unselectedTab / 2);
                x[2] = tw - unselectedTab / 2;
                y[2] = unselectedTab;
                g.fillPolygon(x, y, 3);

                g.setColor(backgroundDarker1);
                g.drawLine(0, unselectedTab, unselectedTab / 2, unselectedTab + (unselectedTab / 2));

                g.setColor(backgroundDarker2);
                g.drawLine(0, unselectedTab + 1, unselectedTab / 2, unselectedTab + (unselectedTab / 2) + 1);
                g.drawLine(unselectedTab / 2, unselectedTab + (unselectedTab / 2) + 1, tw - unselectedTab / 2, unselectedTab + (unselectedTab / 2) + 1);
                g.drawLine(tw - unselectedTab / 2, unselectedTab + (unselectedTab / 2), tw, unselectedTab);
                break;
            case TOP:
                g.fillRect(unselectedTab / 2, 3, tw - unselectedTab, unselectedTab / 2 + 1);
                g.fillRect(0, unselectedTab / 2 + 4, tw, unselectedTab);

                // Left Polygon
                x[0] = 0;
                y[0] = unselectedTab / 2 + 4;
                x[1] = unselectedTab / 2;
                y[1] = unselectedTab / 2 + 4;
                x[2] = unselectedTab / 2;
                y[2] = 3;
                g.fillPolygon(x, y, 3);

                // Right Polygon
                x[0] = tw - unselectedTab / 2;
                y[0] = 3;
                x[1] = tw - unselectedTab / 2;
                y[1] = unselectedTab / 2 + 4;
                x[2] = tw;
                y[2] = unselectedTab / 2 + 4;
                g.fillPolygon(x, y, 3);

                g.setColor(backgroundDarker2);
                g.drawLine(0, unselectedTab / 2 + 3, unselectedTab / 2, 3);
                g.drawLine(tw - unselectedTab / 2, 3, tw, unselectedTab / 2 + 3);
                g.drawLine(unselectedTab / 2, 3, tw - unselectedTab / 2, 3);
//                g.drawLine(0,unselectedTab+unselectedTab/2+3,tw,unselectedTab+unselectedTab/2+3);
                g.drawLine(0, unselectedTab / 2 + 3, 0, unselectedTab + unselectedTab / 2 + 3);
                g.drawLine(tw, unselectedTab / 2 + 3, tw, unselectedTab + unselectedTab / 2 + 3);

                break;
        }
        /////////////////////////////////////////////////////
        g.setColor(Color.black);


//        for (int i = 1; i <= fadeColorCount; i++) {
//            g.setColor(fadeColors[fadeColorCount - 1]);
//            g.drawLine(0, 18 + i, tw - 1, 18 + i);
//        }

        super.paintTabArea(g, tabPlacement, selectedIndex);
    }

    protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int tx, int ty, int tw, int th, boolean isSelected) {
        Graphics2D g2d = (Graphics2D) g;

        g2d.translate(tx, 0);
        switch (tabPlacement) {
            case BOTTOM:
                if (isSelected) {
                    int[] x = new int[3];
                    int[] y = new int[3];
                    g.setColor(selectedTabColor);
                    g.fillRect(0, 0, tw, buttonHeight);
                    g.fillRect(buttonHeight / 2, buttonHeight, tw - buttonHeight + 1, buttonHeight / 2 + 1);

                    // Left Polygon
                    x[0] = 0;
                    y[0] = buttonHeight;
                    x[1] = buttonHeight / 2;
                    y[1] = buttonHeight + (buttonHeight / 2);
                    x[2] = buttonHeight / 2;
                    y[2] = buttonHeight;
                    g.fillPolygon(x, y, 3);

                    // Right Polygon
                    x[0] = tw;
                    y[0] = buttonHeight;
                    x[1] = (tw - buttonHeight / 2);
                    y[1] = buttonHeight + (buttonHeight / 2);
                    x[2] = (tw - buttonHeight / 2);
                    y[2] = buttonHeight;
                    g.fillPolygon(x, y, 3);
                    g.setColor(backgroundDarker2);
                    g.drawLine(0, buttonHeight, buttonHeight / 2, buttonHeight + (buttonHeight / 2));
                    //            g.setColor(backgroundDarker2);
                    g.drawLine(0, buttonHeight + 1, buttonHeight / 2, buttonHeight + (buttonHeight / 2) + 1);
                    g.drawLine(buttonHeight / 2, buttonHeight + (buttonHeight / 2) + 1, tw - buttonHeight / 2, buttonHeight + (buttonHeight / 2) + 1);
                    g.drawLine(tw - (buttonHeight / 2), buttonHeight + (buttonHeight / 2), (tw), buttonHeight);
                    //            g.setColor(Color.black);
                    g.drawLine(0, 0, 0, buttonHeight);
                    g.drawLine(tw, 0, tw, buttonHeight);

                } else {

                    int[] x = new int[3];
                    int[] y = new int[3];

                    g.setColor(unselectedTabColor);

                    g.fillRect(0, 0, tw, unselectedTab);
//                    g.draw3DRect(0, 0, tw - 1, unselectedTab, true);
                    g.fillRect(unselectedTab / 2, unselectedTab, tw - unselectedTab, unselectedTab / 2 + 1);

                    // Left Polygon
                    x[0] = 0;
                    y[0] = unselectedTab;
                    x[1] = unselectedTab / 2;
                    y[1] = unselectedTab + (unselectedTab / 2);
                    x[2] = unselectedTab / 2;
                    y[2] = unselectedTab;
                    g.fillPolygon(x, y, 3);

                    // Right Polygon
                    x[0] = tw;
                    y[0] = unselectedTab;
                    x[1] = tw - unselectedTab / 2;
                    y[1] = unselectedTab + (unselectedTab / 2);
                    x[2] = tw - unselectedTab / 2;
                    y[2] = unselectedTab;
                    g.fillPolygon(x, y, 3);

                    g.setColor(backgroundDarker1);
                    g.drawLine(0, unselectedTab, unselectedTab / 2, unselectedTab + (unselectedTab / 2));

                    //            g.setColor(backgroundDarker2);
                    g.drawLine(0, unselectedTab + 1, unselectedTab / 2, unselectedTab + (unselectedTab / 2) + 1);
                    g.drawLine(unselectedTab / 2, unselectedTab + (unselectedTab / 2) + 1, tw - unselectedTab / 2, unselectedTab + (unselectedTab / 2) + 1);
                    g.drawLine(tw - unselectedTab / 2, unselectedTab + (unselectedTab / 2), tw, unselectedTab);

                    //            g.setColor(Color.black);
                    g.drawLine(0, 0, 0, buttonHeight - 1);
                    g.drawLine(tw, 0, tw, buttonHeight - 1);
                }
                break;
            case TOP:
                if (isSelected) {
                    int[] x = new int[3];
                    int[] y = new int[3];
                    g.setColor(selectedTabColor);
                    g.fillRect(0, 6, tw, buttonHeight);
//                 g.setColor(Color.red);
                    g.fillRect(buttonHeight / 2, 0, tw - buttonHeight + 1, buttonHeight / 2 + 1);

                    // Left Polygon
                    x[0] = buttonHeight / 2;
                    y[0] = 0;
                    x[1] = buttonHeight / 2;
                    y[1] = (buttonHeight / 2);
                    x[2] = 0;
                    y[2] = buttonHeight / 2;
                    g.fillPolygon(x, y, 3);

                    // Right Polygon
                    x[0] = tw - buttonHeight / 2 - 1;
                    y[0] = 0;
                    x[1] = (tw - buttonHeight / 2) - 1;
                    y[1] = (buttonHeight / 2);
                    x[2] = tw;
                    y[2] = buttonHeight / 2;
                    g.fillPolygon(x, y, 3);

//                    g.setColor(backgroundDarker1);
//                    g.drawLine(0, buttonHeight, buttonHeight / 2, buttonHeight + (buttonHeight / 2));
                    g.setColor(backgroundDarker2);
                    g.drawLine(0, buttonHeight / 2, buttonHeight / 2, 0);
                    g.drawLine(buttonHeight / 2, 0, tw - buttonHeight / 2, 0);
                    g.drawLine(tw - (buttonHeight / 2), 0, (tw), buttonHeight / 2);
//                    g.setColor(Color.black);
                    g.drawLine(0, buttonHeight / 2, 0, buttonHeight + buttonHeight / 2);
                    g.drawLine(tw, buttonHeight / 2, tw, buttonHeight + buttonHeight / 2);


                } else {
                    int[] x = new int[3];
                    int[] y = new int[3];
                    g.setColor(unselectedTabColor);
                    g.fillRect(0, 3 + buttonHeight / 2, tw, unselectedTab);
//                    g.draw3DRect(0, 4+buttonHeight/2, tw - 1, unselectedTab, true);
//                     g.setColor(Color.red);
                    g.fillRect(unselectedTab / 2, 3, tw - unselectedTab, unselectedTab / 2 + 1);
//                     g.setColor(Color.blue);
                    // Left Polygon
                    x[0] = buttonHeight / 2;
                    y[0] = 3;
                    x[1] = buttonHeight / 2;
                    y[1] = (buttonHeight / 2 + 3);
                    x[2] = 0;
                    y[2] = buttonHeight / 2 + 3;
                    g.fillPolygon(x, y, 3);

                    // Right Polygon
                    x[0] = tw - buttonHeight / 2 - 1;
                    y[0] = 3;
                    x[1] = (tw - buttonHeight / 2) - 1;
                    y[1] = (buttonHeight / 2) + 3;
                    x[2] = tw;
                    y[2] = buttonHeight / 2 + 3;
                    g.fillPolygon(x, y, 3);

                    g.setColor(backgroundDarker1);
                    g.drawLine(buttonHeight / 2, 3, tw - unselectedTab / 2, 3);
                    g.drawLine(0, 3 + buttonHeight / 2, 0, 1 + buttonHeight / 2 + buttonHeight);
                    g.drawLine(tw, 3 + buttonHeight / 2, tw, 1 + buttonHeight / 2 + buttonHeight);
//                    g.setColor(Color.black);
                    g.drawLine(0, 3 + buttonHeight / 2, buttonHeight / 2, 3);
                    g.drawLine(tw - buttonHeight / 2, 3, tw, buttonHeight / 2 + 3);
                }
                break;
        }


        g2d.translate(-1 * tx, 0);
    }

    protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, int tabIndex, String title, Rectangle textRect, boolean isSelected) {
        Rectangle r = rects[tabIndex];

        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(r.x, 0);
        switch (tabPlacement) {
            case BOTTOM:
                if (isSelected) {
                    g.setFont(new TWFont("arial", 1, 12));
                    FontMetrics fm = getFontMetrics();
                    g.setColor(selectedTextColor);
                    g.drawString(title, (r.width / 2 - fm.stringWidth(title) / 2) + 1, 6 + fm.getMaxDescent() + 6 + 3);
                } else {
                    g.setFont(new TWFont("arial", 0, 10));
                    FontMetrics fm = getFontMetrics();
                    g.setColor(unselectedTextColor);
                    g.drawString(title, (r.width / 2 - fm.stringWidth(title) / 2) + 1, 6 + fm.getMaxDescent() + 5);
                }
                break;
            case TOP:
                if (isSelected) {
                    g.setFont(new TWFont("arial", 1, 12));
                    FontMetrics fm = getFontMetrics();
                    g.setColor(selectedTextColor);
                    g.drawString(title, (r.width / 2 - fm.stringWidth(title) / 2) + 1, 6 + fm.getMaxDescent() + 6);
                } else {
                    g.setFont(new TWFont("arial", 0, 10));
                    FontMetrics fm = getFontMetrics();
                    g.setColor(unselectedTextColor);
                    g.drawString(title, (r.width / 2 - fm.stringWidth(title) / 2) + 1, 6 + fm.getMaxDescent() + 5 + 3);
                }
                break;
        }
        g2d.translate(-1 * r.x, 0);
    }

    // ------------------------------------------------------------------------------------------------------------------
    //  Methods that we want to suppress the behaviour of the superclass
    // ------------------------------------------------------------------------------------------------------------------

    protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {

        // Do nothing
    }

    protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        // Do nothing
    }

    protected void paintContentBorder(Graphics g, int tabPlacement, int selectedIndex) {
        // Do nothing
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        Color captionBGColor = Theme.getColor("MARKET_TITLE_BGCOLOR");
//        Color captionFGColor = Theme.getColor("MARKET_TITLE_FGCOLOR");

        unselectedTabColor = Theme.getColor("TAB_PANE_UNSELECTED_BGCOLOR");
        selectedTabColor = Theme.getColor("TAB_PANE_SELECTED_BGCOLOR");
        selectedTextColor = Theme.getColor("TAB_PANE_SELECTED_FGCOLOR");
        unselectedTextColor = Theme.getColor("TAB_PANE_UNSELECTED_FGCOLOR");
        backgroundDarker2 = Theme.getColor("TAB_PANE_BORDER_COLOR");
        backgroundDarker1 = Theme.getColor("TAB_PANE_UNSELECTED_BORDER_COLOR");
        background = Theme.getColor("TAB_PANE_BGCOLOR");

        /*backgroundDarker1 = Color.RED.brighter();
        backgroundDarker2 = Color.GREEN.darker();
        background = Color.WHITE;
        selectedTabColor = Color.BLUE;
        unselectedTabColor = captionBGColor;
        selectedTextColor = Color.BLACK;
        unselectedTextColor = Color.WHITE;*/
    }
}
