package com.isi.csvr.troubleshoot;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: sandarekaf
 * Date: May 18, 2009
 * Time: 3:04:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class TroubleshootUI extends InternalFrame implements ActionListener, InternalFrameListener {

    private static TroubleshootUI self = null;
    private static boolean exit = false;
    public final byte IDDLE = 0;
    public final byte START = 1;
    public final byte SUCCESS = 2;
    public final byte FAIL = 3;
    private final String CONNECTED = "connected";
    private final String DISCONNECTED = "disconnected";
    private final int PRICE_CONNECTION = 1;
    private final int CONTENT_CONNECTION = 2;
    private final int STATIC_CONNECTION = 3;
    private final int DYNAMIC_CONNECTION = 4;
    // private JProgressBar progressBar = new JProgressBar();
    Task task;
    private JPanel mainPanel;
    //    private JPanel progressPanel;
    private JPanel connectionPanel;
    private JPanel cardConnected;
    private JPanel pricePanel;
    private JPanel priceHeaderPanel;
    private JPanel priceConnectionsPanel;
    private JScrollPane priceScrollPane;
    private JPanel contentPanel;
    //    private JLabel lblPrice;
    private JPanel contentConnectionsPanel;
    private JScrollPane contentScrollPane;
    private JPanel cardDisconnected;
    private JPanel staticPanel;
    private JPanel staticConnectionsPanel;
    private JScrollPane staticScrollPane;
    private JPanel dynamicPanel;
    private JPanel dynamicConnectionsPanel;
    private JScrollPane dynamicScrollPane;
    private JPanel buttonPanel;
    private JPanel infoPanel;
    private JLabel lblInfo;
    private TWButton btnRetry;
    private TWButton btnQuit;
    private boolean pricePathConnected = true;
    private boolean contentPathConnected = true;
    private boolean staticPathConnected = true;
    private boolean dynamicPathConnected = true;
    private boolean isConnectionsSet = false;
    private Thread connector;
    private ArrayList<ConnectionLable> panelItemsPrice = new ArrayList<ConnectionLable>();
    private ArrayList<ConnectionLable> panelItemsContent = new ArrayList<ConnectionLable>();
    private ArrayList<ConnectionLable> panelItemsStatic = new ArrayList<ConnectionLable>();
    private ArrayList<ConnectionLable> panelItemsDynamic = new ArrayList<ConnectionLable>();
    private ArrayList<ConnectionLable> panelItemsDynamicLB = new ArrayList<ConnectionLable>();
    private String[] pricePathConn = {"213.42.31.148|9006|2|Price IP 2", "213.42.31.147|9006|1|Login IP 1"};  /*{"192.168.2.99|8888|2|Login IP 2", "192.168.2.56|8888|1|Login IP 1", "192.168.2.57|8888|1|Login IP 3"};*/
    private String[] contentPathConn = {"192.168.2.99|8888|2|Login IP 2", "192.168.2.56|8888|1|Login IP 1", "192.168.2.59|8888|1|Login IP 3", "192.168.2.57|8888|1|Login IP 4", "192.168.2.52|8888|1|Login IP 5"};
    private String[] staticPathConn;
    private String[] dynamicPathConn;


    public TroubleshootUI() {
        this.setTitle(Language.getString("CONNECTION_MONITOR"));
        loadGUI(pricePathConn, contentPathConn);
        Client.getInstance().getDesktop().add(this);
    }

    public static TroubleshootUI getSharedInstance() {
        if (self == null) {
            self = new TroubleshootUI();
            return self;
        } else {
            return self;
        }
    }

    public void loadGUI(String[] pricePath, String[] contentPath) {
        setLayout(new FlowLayout(FlowLayout.CENTER, 1, 1));
        mainPanel = new JPanel(new FlexGridLayout(new String[]{"320"}, new String[]{"260", "20", "20"}, 10, 10));

        //progress panel
//        progressPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
//        progressBar.setFont(new Font("Arial", Font.BOLD, 10));
//        progressBar.setStringPainted(true);
//        progressBar.setString("");
//        progressPanel.add(new JLabel("Test Progress"));
//        progressPanel.add(progressBar);

        // cardConnected
        priceConnectionsPanel = new JPanel();


        priceScrollPane = new JScrollPane(priceConnectionsPanel);
        priceScrollPane.setPreferredSize(new Dimension(300, 80));
        priceScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        priceScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        priceScrollPane.setBorder(BorderFactory.createEmptyBorder());


        contentConnectionsPanel = new JPanel();

        contentScrollPane = new JScrollPane(contentConnectionsPanel);
        contentScrollPane.setPreferredSize(new Dimension(300, 100));
        contentScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        contentScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        contentScrollPane.setBorder(BorderFactory.createEmptyBorder());

        pricePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        pricePanel.setBorder(BorderFactory.createTitledBorder(Language.getString("PRICE_PATH")));

        contentPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        contentPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CONTENT_PATH")));

        pricePanel.add(priceScrollPane);
        contentPanel.add(contentScrollPane);

        cardConnected = new JPanel(new ColumnLayout(ColumnLayout.MODE_SAME_SIZE));

        cardConnected.add(pricePanel);
        JPanel dummuPanel = new JPanel();
        cardConnected.add(dummuPanel);
        cardConnected.add(contentPanel);

        //cardDisconnected
        staticConnectionsPanel = new JPanel();

        staticScrollPane = new JScrollPane(staticConnectionsPanel);
        staticScrollPane.setPreferredSize(new Dimension(300, 80));
        staticScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        staticScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        staticScrollPane.setBorder(BorderFactory.createEmptyBorder());

        dynamicConnectionsPanel = new JPanel();

        dynamicScrollPane = new JScrollPane(dynamicConnectionsPanel);
        dynamicScrollPane.setPreferredSize(new Dimension(300, 100));
        dynamicScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        dynamicScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        dynamicScrollPane.setBorder(BorderFactory.createEmptyBorder());

        staticPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        staticPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("STATIC_CONNECTIONS")));

        dynamicPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        dynamicPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("DYNAMIC_CONNECTIONS")));

        staticPanel.add(staticScrollPane);
        dynamicPanel.add(dynamicScrollPane);

        cardDisconnected = new JPanel(new ColumnLayout(ColumnLayout.MODE_SAME_SIZE));

        cardDisconnected.add(staticPanel);
        JPanel dummuPanel1 = new JPanel();
        cardDisconnected.add(dummuPanel1);
        cardDisconnected.add(dynamicPanel);

        //connectionPanel
        connectionPanel = new JPanel(new CardLayout());
        connectionPanel.add(cardConnected, CONNECTED);
        connectionPanel.add(cardDisconnected, DISCONNECTED);

        setCard();

        //info panel
        infoPanel = new JPanel();
        if (Language.isLTR()) {
            infoPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        } else {
            infoPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        lblInfo = new JLabel(Language.getString("LOAD_CONNECTIONS"));
        //  lblInfo.setFont(new Font("Arial", Font.BOLD, 13));
        infoPanel.add(lblInfo);
        updateInfoPannel();

        //buttonPanel
        buttonPanel = new JPanel(new FlexGridLayout(new String[]{"75", "130", "80"}, new String[]{"20"}, 10, 0));
        btnRetry = new TWButton(Language.getString("TEST_CONNECTION"));
        btnRetry.addActionListener(this);
        btnRetry.setActionCommand("R");
        btnQuit = new TWButton(Language.getString("EXIT"));
        btnQuit.addActionListener(this);
        btnQuit.setActionCommand("Q");
        buttonPanel.add(new JLabel());
        buttonPanel.add(btnRetry);
        buttonPanel.add(btnQuit);

//        mainPanel.add(progressPanel);
        mainPanel.add(connectionPanel);
        mainPanel.add(infoPanel);
        mainPanel.add(buttonPanel);

        getContentPane().add(mainPanel);
        pack();
        Theme.registerComponent(this);
        this.setSize(340, 365);
        this.setClosable(true);
        this.addInternalFrameListener(this);
        this.hideTitleBarMenu();

        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        GUISettings.applyOrientation(this);
        this.setLayer(GUISettings.TOP_LAYER);

    }

    private void setCard() {
        CardLayout cl = (CardLayout) connectionPanel.getLayout();
        if (Settings.isConnected()) {

            cl.show(connectionPanel, CONNECTED);
        } else {
            cl.show(connectionPanel, DISCONNECTED);
        }
    }

    private void setConnections(String[] pricePath, String[] contentPath) {
        btnRetry.setEnabled(false);
        pricePathConnected = true;
        contentPathConnected = true;
        priceConnectionsPanel.removeAll();
        contentConnectionsPanel.removeAll();
        priceConnectionsPanel.setLayout(null);
        contentConnectionsPanel.removeAll();
        priceConnectionsPanel.setPreferredSize(new Dimension(280, 30));
        contentConnectionsPanel.setPreferredSize(new Dimension(280, 30));
        priceConnectionsPanel.revalidate();
        contentConnectionsPanel.revalidate();
        panelItemsPrice.clear();
        panelItemsContent.clear();
        String[] width = {"280"};

        if (pricePath != null && pricePath.length > 0) {
            int priceConnections = pricePath.length;
            String[] pHeights = new String[priceConnections];
            int length = determineMaxLehgth(pricePath);
            String lengthStr = length + "";
            width = new String[]{lengthStr};
            priceConnectionsPanel.setLayout(null);
            priceConnectionsPanel.setLayout(new FlexGridLayout(width, pHeights, 5, 5));
            //  priceConnectionsPanel.setPreferredSize(new Dimension(280, pHeights.length * 30));
            priceConnectionsPanel.setPreferredSize(new Dimension(length + 30, pHeights.length * 30));
            panelItemsPrice.clear();

            for (int i = 0; i < pricePath.length; i++) {
                pHeights[i] = "25";
                String connection = pricePath[i];
                StringTokenizer tokenizer = new StringTokenizer(connection, "|");
                while (tokenizer.hasMoreTokens()) {
                    String ip = tokenizer.nextToken();
                    String port = tokenizer.nextToken();
                    String id = tokenizer.nextToken();
                    String description = tokenizer.nextToken();
                    String ip_port = ip + " : " + port;

                    //   JLabel lblimage = new JLabel(new ImageIcon("images/theme" + Theme.getID() + "/try_connecting.gif"));
                    JLabel lblimage = new JLabel(new ImageIcon("images/common/tnsQueued.gif"));
                    lblimage.setOpaque(false);
                    JLabel lbldescription = new JLabel(description);
                    JLabel lblIP_Port = new JLabel(ip_port);
                    lblIP_Port.setOpaque(false);
                    JLabel lbl_fill = new JLabel();
                    lbl_fill.setOpaque(false);
                    lblIP_Port.setFont(new Font(Font.DIALOG, Font.PLAIN, 12));
                    ConnectionLable lable = new ConnectionLable(ip, ip_port, port, "", PRICE_CONNECTION, length);
                    priceConnectionsPanel.add(lable);
                    panelItemsPrice.add(lable);

                }
            }
            priceConnectionsPanel.revalidate();
        }
        //  priceScrollPane.revalidate();
        //   priceConnectionsPanel.doLayout();
        if (contentPath != null && contentPath.length > 0) {
            int contentConnections = contentPath.length;
            String[] cHeights = new String[contentConnections];
            int length = determineMaxLehgth(contentPath);
            String lengthStr = length + "";
            width = new String[]{lengthStr};
            //  String[] width = {"20", "20", "200"};

            contentConnectionsPanel.setLayout(null);
            contentConnectionsPanel.setLayout(new FlexGridLayout(width, cHeights, 5, 5));
            contentConnectionsPanel.setPreferredSize(new Dimension(length + 30, cHeights.length * 30));
            panelItemsContent.clear();

            for (int i = 0; i < contentPath.length; i++) {
                cHeights[i] = "25";
                String connection = contentPath[i];
                StringTokenizer tokenizer = new StringTokenizer(connection, "|");
                while (tokenizer.hasMoreTokens()) {
                    String ip = tokenizer.nextToken();
                    String port = tokenizer.nextToken();
                    String id = tokenizer.nextToken();
                    String description = tokenizer.nextToken();
                    String ip_port = ip + " : " + port;

                    //  JLabel lblimage = new JLabel(new ImageIcon("images/theme" + Theme.getID() + "/try_connecting.gif"));
                    JLabel lblimage = new JLabel(new ImageIcon("images/common/tnsQueued.gif"));
                    JLabel lbldescription = new JLabel(description);
                    JLabel lblIP_Port = new JLabel(ip_port);
                    lblIP_Port.setFont(new Font(Font.DIALOG, Font.PLAIN, 12));

                    ConnectionLable lable = new ConnectionLable(ip, ip_port, port, "", CONTENT_CONNECTION, length);
                    contentConnectionsPanel.add(lable);
                    panelItemsContent.add(lable);
                }
            }
            contentConnectionsPanel.revalidate();
        }
        GUISettings.applyOrientation(priceConnectionsPanel);
        GUISettings.applyOrientation(contentConnectionsPanel);
        getContentPane().doLayout();
        if (pricePath != null && pricePath.length > 0) {
            tryConnections(panelItemsPrice);
        }
        if (contentPath != null && contentPath.length > 0) {
            tryConnections(panelItemsContent);
        }

//        progressBar.setIndeterminate(false);
//        progressBar.setString("");
        updateInfoPannel();

        isConnectionsSet = true;
        btnRetry.setEnabled(true);

    }

    private void setOfflineConnections(String[] staticPath, String[] dynamicPath) {
        btnRetry.setEnabled(false);
        staticPathConnected = true;
        dynamicPathConnected = true;
        staticConnectionsPanel.removeAll();
        dynamicConnectionsPanel.removeAll();
        staticConnectionsPanel.setLayout(null);
        dynamicConnectionsPanel.removeAll();
        staticConnectionsPanel.setPreferredSize(new Dimension(280, 30));
        dynamicConnectionsPanel.setPreferredSize(new Dimension(280, 30));
        staticConnectionsPanel.revalidate();
        dynamicConnectionsPanel.revalidate();
        panelItemsStatic.clear();
        panelItemsDynamicLB.clear();
        panelItemsDynamicLB.clear();
        String[] width = {"280"};

        if (staticPath != null && staticPath.length > 0) {
            int staticConnections = staticPath.length;
            String[] pHeights = new String[staticConnections];
            int length = determineMaxLehgth(staticPath);
            String lengthStr = length + "";
            width = new String[]{lengthStr};
            staticConnectionsPanel.setLayout(null);
            staticConnectionsPanel.setLayout(new FlexGridLayout(width, pHeights, 5, 5));
            staticConnectionsPanel.setPreferredSize(new Dimension(length + 30, pHeights.length * 25));
            panelItemsStatic.clear();

            for (int i = 0; i < staticPath.length; i++) {
                pHeights[i] = "25";
                String connection = staticPath[i];
                StringTokenizer tokenizer = new StringTokenizer(connection, "|");
                while (tokenizer.hasMoreTokens()) {
                    String ip = tokenizer.nextToken();
                    String port = tokenizer.nextToken();
                    String id = tokenizer.nextToken();
                    String description = tokenizer.nextToken();
                    String ip_port = ip + " : " + port;

                    //   JLabel lblimage = new JLabel(new ImageIcon("images/theme" + Theme.getID() + "/try_connecting.gif"));
                    JLabel lblimage = new JLabel(new ImageIcon("images/common/tnsQueued.gif"));
                    lblimage.setOpaque(false);
                    JLabel lbldescription = new JLabel(description);
                    JLabel lblIP_Port = new JLabel(ip_port);
                    lblIP_Port.setOpaque(false);
                    JLabel lbl_fill = new JLabel();
                    lbl_fill.setOpaque(false);
                    lblIP_Port.setFont(new Font(Font.DIALOG, Font.PLAIN, 12));
                    ConnectionLable lable = new ConnectionLable(ip, ip_port, port, "", STATIC_CONNECTION, length);
                    staticConnectionsPanel.add(lable);
                    panelItemsStatic.add(lable);

                }
            }
            staticConnectionsPanel.revalidate();
        }

        if (Settings.getBooleanItem("DYNAMIC_IPS_ALLOWED") && dynamicPath != null && dynamicPath.length > 0) {

            int dynamicConnections = dynamicPath.length;
            String[] cHeights = new String[dynamicConnections * 2];
            int lengthstr = this.getGraphics().getFontMetrics().stringWidth(Language.getString("LOADBALANCE"));
            int length = Math.max(250, lengthstr);

            dynamicConnectionsPanel.setLayout(null);
            dynamicConnectionsPanel.setLayout(new FlexGridLayout(width, cHeights, 5, 5));
            //dynamicConnectionsPanel.setPreferredSize(new Dimension(length + 30, cHeights.length * 25));
            panelItemsDynamicLB.clear();

            for (int i = 0; i < dynamicPath.length; i++) {
                cHeights[i * 2] = "20";
                cHeights[i * 2 + 1] = "20";
                String connection = dynamicPath[i];
                StringTokenizer tokenizer = new StringTokenizer(connection, "|");
                while (tokenizer.hasMoreTokens()) {
                    String lbURL = tokenizer.nextToken();
                    String port = tokenizer.nextToken();
                    String id = tokenizer.nextToken();
                    String description = tokenizer.nextToken();
                    //  JLabel lblimage = new JLabel(new ImageIcon("images/theme" + Theme.getID() + "/try_connecting.gif"));
                    JLabel lblimage = new JLabel(new ImageIcon("images/common/tnsQueued.gif"));
                    JLabel lbldescription = new JLabel(description);
                    JLabel lblIP_Port = new JLabel(lbURL);
                    lblIP_Port.setFont(new Font(Font.DIALOG, Font.PLAIN, 12));
                    length = Math.max(this.getGraphics().getFontMetrics().stringWidth(Language.getString("LOADBALANCE")), 250);
                    ConnectionLable urlLable = new ConnectionLable(lbURL, Language.getString("LOADBALANCE") + (i + 1),
                            port, "", DYNAMIC_CONNECTION, length);
                    dynamicConnectionsPanel.add(urlLable);
                    panelItemsDynamicLB.add(urlLable);

                    String dynamicIP = getIPFromLoadBalanceURL(lbURL);
                    if (dynamicIP != null && !dynamicIP.equals("")) {
                        urlLable.setStatus(SUCCESS);
                        String pricePort = Constants.DEFAULT_PRICE_PORT + "";
                        String priceIP_Port = dynamicIP + " : " + pricePort;
                        length = Math.max(250, this.getGraphics().getFontMetrics().stringWidth(priceIP_Port));
                        ConnectionLable ipLabel = new ConnectionLable(dynamicIP, priceIP_Port, pricePort,
                                "", DYNAMIC_CONNECTION, length);
                        dynamicConnectionsPanel.add(ipLabel);
                        panelItemsDynamic.add(ipLabel);
                    } else {
                        dynamicPathConnected = false;
//                        JPanel pnlDisconnected = new JPanel(new FlowLayout());
//                        pnlDisconnected.add(new JLabel());
//                        pnlDisconnected.add(new JLabel("URL not connected"));
//                        dynamicConnectionsPanel.add(pnlDisconnected);
                        urlLable.setStatus(FAIL);
                    }
                }
            }
            dynamicConnectionsPanel.setPreferredSize(new Dimension(length + 30, cHeights.length * 25));
            dynamicConnectionsPanel.revalidate();

        } else {
            dynamicConnectionsPanel.setLayout(new BorderLayout());
            dynamicConnectionsPanel.add(new JLabel(Language.getString("TROUBLESHOOT_NODYMAMIC_IP")));
            dynamicPanel.setEnabled(false);
        }

        GUISettings.applyOrientation(staticConnectionsPanel);
        GUISettings.applyOrientation(dynamicConnectionsPanel);
        getContentPane().doLayout();
        if (staticPath != null && staticPath.length > 0) {
            tryConnections(panelItemsStatic);
        }

        if (Settings.getBooleanItem("DYNAMIC_IPS_ALLOWED") && dynamicPath != null && dynamicPath.length > 0) {
            tryConnections(panelItemsDynamic);
        }

//        progressBar.setIndeterminate(false);
//        progressBar.setString("");
        updateInfoPannel();

        isConnectionsSet = true;
        btnRetry.setEnabled(true);

    }

    private void revalidateComponents() {
        try {
            pricePathConnected = true;
            contentPathConnected = true;
            staticPathConnected = true;
            dynamicPathConnected = true;

            priceConnectionsPanel.removeAll();
            contentConnectionsPanel.removeAll();
            priceConnectionsPanel.setLayout(null);
            contentConnectionsPanel.removeAll();
            priceConnectionsPanel.setPreferredSize(new Dimension(280, 30));
            contentConnectionsPanel.setPreferredSize(new Dimension(280, 30));
            priceConnectionsPanel.revalidate();
            contentConnectionsPanel.revalidate();
            panelItemsPrice.clear();
            panelItemsContent.clear();

            staticConnectionsPanel.removeAll();
            dynamicConnectionsPanel.removeAll();
            staticConnectionsPanel.setLayout(null);
            dynamicConnectionsPanel.removeAll();
            staticConnectionsPanel.setPreferredSize(new Dimension(280, 30));
            dynamicConnectionsPanel.setPreferredSize(new Dimension(280, 30));
            staticConnectionsPanel.revalidate();
            dynamicConnectionsPanel.revalidate();
            panelItemsStatic.clear();
            panelItemsDynamicLB.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void retryConnections(String[] pricePath, String[] contentPath) {

        pricePathConnected = true;
        contentPathConnected = true;
//        progressBar.setString("Retrying price path");
//        progressBar.setIndeterminate(true);

        for (int i = 0; i < pricePath.length; i++) {
            JLabel temp = (JLabel) priceConnectionsPanel.getComponent((i * 3) + 1);
            String connection = pricePath[i];
            StringTokenizer tokenizer = new StringTokenizer(connection, "|");
            while (tokenizer.hasMoreTokens()) {
                String ip = tokenizer.nextToken();
                String port = tokenizer.nextToken();
                String id = tokenizer.nextToken();
                String description = tokenizer.nextToken();
                try {
                    temp.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/try_connecting.gif"));
                    //System.out.println("Testing connection - " + ip + " : " + port);
                    Socket socket = new Socket();
                    socket.connect(new InetSocketAddress(ip, new Integer(port).intValue()), 2000);
                    if (socket.isConnected()) {

                        temp.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/connected.gif"));
                    } else {
                        pricePathConnected = false;
                        temp.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/disconnected.gif"));
                    }


                } catch (Exception e) {
                    pricePathConnected = false;
                    temp.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/disconnected.gif"));
                }

            }
        }
        // progressBar.setString("Retrying content path");

        for (int i = 0; i < contentPath.length; i++) {
            JLabel temp = (JLabel) contentConnectionsPanel.getComponent((i * 3) + 1);
            String connection = contentPath[i];
            StringTokenizer tokenizer = new StringTokenizer(connection, "|");
            while (tokenizer.hasMoreTokens()) {
                String ip = tokenizer.nextToken();
                String port = tokenizer.nextToken();
                String id = tokenizer.nextToken();
                String description = tokenizer.nextToken();
                try {
                    temp.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/try_connecting.gif"));
                    //System.out.println("Testing connection - " + ip + " : " + port);
                    Socket socket = new Socket();
                    socket.connect(new InetSocketAddress(ip, new Integer(port).intValue()), 2000);
//                    progressBar.paintImmediately(progressBar.getBounds());
                    if (socket.isConnected()) {
                        temp.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/connected.gif"));
                    } else {
                        contentPathConnected = false;
                        temp.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/disconnected.gif"));
                    }
                } catch (Exception e) {
                    contentPathConnected = false;
                    temp.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/disconnected.gif"));
                }
            }
        }
//        progressBar.setIndeterminate(false);
//        progressBar.setString("");
        updateInfoPannel();
        priceScrollPane.revalidate();
        contentScrollPane.revalidate();
        getContentPane().doLayout();
    }


    private void updateInfoPannel() {
        if (Settings.isConnected()) {
            if (!pricePathConnected && !contentPathConnected) {
                lblInfo.setText(Language.getString("PRICE_CONTENT_NOT_CONNECTED"));
            } else if (!pricePathConnected) {
                lblInfo.setText(Language.getString("PRICE_NOT_CONNECTED"));
            } else if (!contentPathConnected) {
                lblInfo.setText(Language.getString("CONTENT_NOT_CONNECTED"));
            } else {
                lblInfo.setText(Language.getString("CONNECTIONS_SUCCESSFUL"));
            }
        } else {
            if (!staticPathConnected && !dynamicPathConnected) {
                lblInfo.setText(Language.getString("STATIC_DYNAMIC_NOT_CONNECTED"));
            } else if (!staticPathConnected) {
                lblInfo.setText(Language.getString("STATIC_NOT_CONNECTED"));
            } else if (!dynamicPathConnected) {
                lblInfo.setText(Language.getString("DYNAMIC_NOT_CONNECTED"));
            } else {
                lblInfo.setText(Language.getString("CONNECTIONS_SUCCESSFUL"));
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("R")) {
            clear();
            populateArrays();

//            connector = new Thread(new Runnable() {
//                public void run() {
//                   setConnections(pricePathConn, contentPathConn);
//                }
//            },"");
//            connector.start();
            task = new Task();

            task.execute();
            setCard();
            //  setConnections(pricePathConn, contentPathConn);


        } else if (e.getActionCommand().equals("Q")) {
//            progressBar.setIndeterminate(false);
            exit = true;
//            if(connector!=  null){
//                connector.
//            }
            if (task != null && !task.isDone()) {
                task.cancel(true);
            }
            this.setVisible(false);
        }
    }

    public void setVisible(boolean value) {
        if (value) {
            exit = false;
            revalidateComponents();

        } else {
            exit = true;
        }
        clear();
        if (lblInfo != null && value) {
            lblInfo.setText(Language.getString("TROUBLESHOOT_CLICKTO_START"));
        }
        super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
    }

    private void clear() {
        if (priceConnectionsPanel != null && contentConnectionsPanel != null && lblInfo != null && btnRetry != null) {
            priceConnectionsPanel.removeAll();
            contentConnectionsPanel.removeAll();
            priceConnectionsPanel.setLayout(null);
            contentConnectionsPanel.setLayout(null);
            priceConnectionsPanel.repaint();
            contentConnectionsPanel.repaint();
//            progressBar.setIndeterminate(false);
            lblInfo.setText("");
            btnRetry.setEnabled(true);
            panelItemsPrice.clear();
            panelItemsContent.clear();
        }

    }

    private void populateArrays() {
        Object[] list = new TroubleshootController().getIpList();
        if (list != null && list.length == 2) {
            if (list[1] == null) {
                createStaticAndDynamicConnections((String[]) list[0]);
            } else {
                pricePathConn = (String[]) list[0];
                contentPathConn = (String[]) list[1];
            }
        }
    }

    private void createStaticAndDynamicConnections(String[] connections) {
        ArrayList staticList = new ArrayList();
        ArrayList dynamicList = new ArrayList();
        for (int i = 0; i < connections.length; i++) {
            String conn = connections[i];
            StringTokenizer tokenizer = new StringTokenizer(conn, "|");
            while (tokenizer.hasMoreTokens()) {
                String ip = tokenizer.nextToken();
                String port = tokenizer.nextToken();
                String id = tokenizer.nextToken();
                String description = tokenizer.nextToken();
                if (id.equals("DYNAMIC")) {
                    dynamicList.add(conn);
                } else {
                    staticList.add(conn);
                }
            }
        }
        staticPathConn = new String[staticList.size()];
        dynamicPathConn = new String[dynamicList.size()];
        for (int i = 0; i < dynamicList.size(); i++) {
            dynamicPathConn[i] = (String) dynamicList.get(i);
        }
        for (int i = 0; i < staticList.size(); i++) {
            staticPathConn[i] = (String) staticList.get(i);
        }


    }

    private String getIPFromLoadBalanceURL(String lbURL) {
        //System.out.println("LOAD BALANCE URL = " + lbURL);
        String ip = "";
        try {
            ip = new TroubleshootController().getDynamicIP(lbURL);
            // System.out.println("LOAD BALANCE IP = " + ip);
            if (ip.trim().startsWith("IP:")) {
                ip = ip.substring(3);
            } else {
                return null;
            }

        } catch (Exception e) {
            System.out.println("ERROR: Can not connect to load balance url");
        }

        return ip;
    }

    private void tryConnections(ArrayList<ConnectionLable> list) {
        if (exit) {
            return;
        }
        if (list != null && !list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                if (exit) {
                    return;
                }
                ConnectionLable lable = list.get(i);
                String ip = lable.ip;
                String port = lable.port;
                lable.setStatus(START);
                Socket socket = new Socket();
                try {

                    socket.connect(new InetSocketAddress(ip, new Integer(port).intValue()), 10000);

                    if (socket.isConnected()) {
                        lable.setStatus(SUCCESS);
                    } else {
                        lable.setStatus(FAIL);
                        if (lable.getConnectionType() == PRICE_CONNECTION) {
                            pricePathConnected = false;
                        } else if (lable.getConnectionType() == CONTENT_CONNECTION) {
                            contentPathConnected = false;
                        } else if (lable.getConnectionType() == STATIC_CONNECTION) {
                            staticPathConnected = false;
                        } else if (lable.getConnectionType() == DYNAMIC_CONNECTION) {
                            dynamicPathConnected = false;
                        }
                    }

                } catch (Exception e) {
                    if (lable.getConnectionType() == PRICE_CONNECTION) {
                        pricePathConnected = false;
                    } else if (lable.getConnectionType() == CONTENT_CONNECTION) {
                        contentPathConnected = false;
                    } else if (lable.getConnectionType() == STATIC_CONNECTION) {
                        staticPathConnected = false;
                    } else if (lable.getConnectionType() == DYNAMIC_CONNECTION) {
                        dynamicPathConnected = false;
                    }//   lblimage.setIcon(new ImageIcon("images/theme" + Theme.getID() + "/disconnected.gif"));
                    lable.setStatus(FAIL);
                    try {
                        socket.close();
                    } catch (Exception e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    //  e.printStackTrace();
                }
            }
        }
    }

    private int determineMaxLehgth(String[] list) {
        int width = 250;
        for (int i = 0; i < list.length; i++) {
            String str = list[i];
            StringTokenizer tokenizer = new StringTokenizer(str, "|");
            while (tokenizer.hasMoreElements()) {
                String ip = (String) tokenizer.nextElement();
                int length = this.getGraphics().getFontMetrics().stringWidth(ip);
                width = Math.max(width, length);
                break;
            }
        }
        if (width > 250) {
            width = width + 20;
        }
        return width;
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        super.internalFrameClosing(e);
        if (task != null && !task.isDone()) {
            task.cancel(true);

        }
        this.setVisible(false);

        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        exit = true;
    }

    class Task extends SwingWorker<Void, Void> {
        protected Void doInBackground() throws Exception {

            try {
                if (Settings.isConnected())
                    setConnections(pricePathConn, contentPathConn);
                else
                    setOfflineConnections(staticPathConn, dynamicPathConn);
            } catch (Exception e) {
                btnRetry.setEnabled(true);

                e.printStackTrace();
            }

            return null;
        }
    }

    private class ConnectionLable extends JPanel {
        public String port;
        public String ip;
        public int connectionType;
        private JLabel imagelbl;
        private JLabel iplbl;
        private JLabel desclbl;
        private ImageIcon iconIddle = new ImageIcon("images/theme" + Theme.getID() + "/iddle.gif");     //0
        // private ImageIcon iconStart = new ImageIcon("images/common/tnsQueued.gif");     //1
        private ImageIcon iconStart = new ImageIcon("images/theme" + Theme.getID() + "/try_connecting.gif");     //1
        //  private ImageIcon iconSuccess = new ImageIcon("images/common/tick.gif");   //2
        private ImageIcon iconSuccess = new ImageIcon("images/theme" + Theme.getID() + "/connected.gif");   //2
        //    private ImageIcon iconFail = new ImageIcon("images/common/untick.gif");      //3
        private ImageIcon iconFail = new ImageIcon("images/theme" + Theme.getID() + "/disconnected.gif");      //3


        private ConnectionLable(String ip, String text, String port, String desc, int connType, int length) {
            // this.setLayout(new FlexGridLayout(new String[]{"20", "20", "250", "10"}, new String[]{"25"}));
            String lenstr = length + "";
            this.setLayout(new FlexGridLayout(new String[]{"20", lenstr, "10"}, new String[]{"25"}));
            iplbl = new JLabel(text);
            iplbl.setToolTipText(ip);

            desclbl = new JLabel(desc);
            imagelbl = new JLabel(iconIddle);
            connectionType = connType;
            //  add(new JLabel());
            add(imagelbl);
            add(iplbl);
            add(desclbl);
            this.port = port;
            this.ip = ip;

        }

        public void setStatus(byte status) {
            switch (status) {
                case IDDLE:
                    imagelbl.setIcon(iconIddle);
                    break;
                case START:
                    imagelbl.setIcon(iconStart);
                    break;
                case SUCCESS:
                    imagelbl.setIcon(iconSuccess);
                    break;
                case FAIL:
                    imagelbl.setIcon(iconFail);
                    break;
                default:
                    imagelbl.setIcon(iconIddle);
                    break;
            }
            this.repaint();
        }

        int getConnectionType() {
            return connectionType;
        }

    }
}
