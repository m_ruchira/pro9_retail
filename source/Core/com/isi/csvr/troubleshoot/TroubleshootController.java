package com.isi.csvr.troubleshoot;

import com.isi.csvr.communication.tcp.InternationalConnector;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: May 26, 2009
 * Time: 2:56:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class TroubleshootController {
    // private static TroubleshootController self;


    public TroubleshootController() {

    }

    public Object[] getIpList() {
        Object[] list = new Object[2];
        if (Settings.isConnected()) {

            Object[] sublist = getExchangewiseIpList();
            list[0] = getArrayListFormHashTable((Hashtable<String, String>) sublist[0]);
            list[1] = getArrayListFormHashTable((Hashtable<String, String>) sublist[1]);
        } else {
            Hashtable table = getConnectionIpList();
            list[0] = getArrayListFormHashTable(table);
        }
        return list;
    }

    private Hashtable<String, String> getConnectionIpList() {
        Hashtable<String, String> iplist = new Hashtable<String, String>();
        int count = IPSettings.getIPCount();
        for (int i = 0; i < count; i++) {
            IP ip = IPSettings.getIP(i);
            String ipstring = getPrimaryConnectionPath(ip);
            if (ipstring != null && !ipstring.isEmpty()) {

                iplist.put(ip.getIP(), ipstring);
            }
        }
        return iplist;
    }

    private Object[] getExchangewiseIpList() {
        Object[] iplist = new Object[2];
        Enumeration enu = ExchangeStore.getSharedInstance().getExchanges();
        Hashtable<String, String> content = new Hashtable<String, String>();
        Hashtable<String, String> realtime = new Hashtable<String, String>();
        while (enu.hasMoreElements()) {
            Exchange exchange = (Exchange) enu.nextElement();
            String ccmIP = exchange.getContentCMIP();
            if (!content.containsKey(ccmIP)) {
                String data = ccmIP + "|" + Constants.DEFAULT_DOWNLOAD_PORT + "|x|x ";
                content.put(ccmIP, data);
            }
        }
        String regionalCP = Settings.getRegionalContentProviderIP();
        String globalCP = Settings.getGlobalContentProviderIP();
        if (regionalCP != null && !regionalCP.isEmpty()) {
            String data = regionalCP + "|" + Constants.DEFAULT_DOWNLOAD_PORT + "|x|x";
            content.put(regionalCP, data);
        }
        if (globalCP != null && !globalCP.isEmpty()) {
            String data = globalCP + "|" + Constants.DEFAULT_DOWNLOAD_PORT + "|x|x";
            content.put(globalCP, data);
        }
        populateRealtimeIPS(realtime);
        iplist[0] = realtime;
        iplist[1] = content;

        return iplist;
    }

    private void populateRealtimeIPS(Hashtable<String, String> realtime) {
        String secondary = InternationalConnector.getActiveIP();
        if (secondary != null && !secondary.isEmpty()) {
            String data = secondary + "|" + Constants.DEFAULT_PRICE_PORT + "|x|x";
            realtime.put(secondary, data);
        }
        int count = IPSettings.getIPCount();
        for (int i = 0; i < count; i++) {
            IP ip = IPSettings.getIP(i);
            if (ip != null && ip.getType() == IP.STATIC) {
                String ipstring = getPrimaryConnectionPath(ip);
                if (ipstring != null && !ipstring.isEmpty()) {
                    //  String data = ipstring + "|" + Constants.DEFAULT_DOWNLOAD_PORT + "||";
                    realtime.put(ip.getIP(), ipstring);
                }
            }
        }

    }

    private String getPrimaryConnectionPath(IP ip) {
        if (ip != null) {
            if (ip.getType() == IP.DYNAMIC) {
                String ipstring = ip.getIP() + "|" + Constants.DEFAULT_PRICE_PORT + "|DYNAMIC|x";
                return ipstring;
            } else {
                String ipstring = ip.getIP() + "|" + Constants.DEFAULT_PRICE_PORT + "|x|x";
                return ipstring;
            }

        }
        return null;
    }

    private String[] getArrayListFormHashTable(Hashtable<String, String> table) {
        //   ArrayList<String> list = new ArrayList<String>();
        String[] list = new String[table.size()];
        if (table != null) {
            Enumeration<String> en = table.keys();
            int i = 0;
            while (en.hasMoreElements()) {
                list[i] = (table.get(en.nextElement()));
                i += 1;
            }
        }
        return list;
    }

    String getDynamicIP(String loadBalancerURL) throws Exception {
        System.out.println("Connecting to Load Balancer " + loadBalancerURL);
        String user = Settings.getUserName();
        URL url = new URL(loadBalancerURL + "&U=" + user);


        Socket lbSocket;
        String billingCode;
        String proVersion;
        if (url.getPort() > 0) {
            lbSocket = TWSocket.createSocket(url.getHost(), url.getPort());
        } else {
            lbSocket = TWSocket.createSocket(url.getHost(), 80);
        }
        lbSocket.setSoTimeout(30000); // make sure to timeout in 30 secs, if no responce
        OutputStream out = lbSocket.getOutputStream();
        billingCode = Settings.getBillingCode();
        proVersion = "" + Settings.TW_VERSION;
        if ((billingCode != null) && (!billingCode.equals(""))) {
            billingCode = "&B=" + billingCode;
        } else {
            billingCode = "";
        }
        if ((proVersion != null) && (!proVersion.equals(""))) {
            proVersion = "&V=" + proVersion;
        } else {
            proVersion = "";
        }
        out.write(("GET " + loadBalancerURL + "T=1&U=" + user + billingCode + proVersion + " HTTP/1.0\r\n").getBytes());
        System.out.println(("GET " + loadBalancerURL + "T=1&U=" + user + billingCode + proVersion + " HTTP/1.0\r\n"));
        out.write("\r\n\r\n".getBytes());
        InputStream in = lbSocket.getInputStream();
        StringBuffer newIP = new StringBuffer();
        char ch = (char) in.read();

        while (true) {
            if ((ch != Meta.ERD) && (ch != (char) -1)) {
                newIP.append(ch);
                ch = (char) in.read();
            } else {
                break;
            }
        }
        int ipLocation = newIP.toString().indexOf("IP:");
        newIP = new StringBuffer(newIP.toString().substring(ipLocation));
        System.out.println("Dynamic IP " + newIP.toString());
        try {
            lbSocket.close();
        } catch (IOException e) {
        }
        if (newIP.toString().trim().equals(""))
            return null;
        else
            return newIP.toString().trim();
    }

}
