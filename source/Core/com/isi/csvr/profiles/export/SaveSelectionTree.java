package com.isi.csvr.profiles.export;

import com.isi.csvr.Client;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.customformular.FormularEditorInterface;
import com.isi.csvr.customindex.CustomIndexInterface;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartFileChooser;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.awt.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 27-Apr-2007
 * Time: 10:57:52
 * To change this template use File | Settings | File Templates.
 */
public class SaveSelectionTree extends JDialog {
    public File src, dst;
    SmartFileChooser jFileChooser;

    public SaveSelectionTree(JDialog dialog) {
        super(Client.getInstance().getFrame(), Language.getString("EXPORT_TO_FOLDER"), true);
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        fileSave();
        this.dispose();
        dialog.dispose();
    }

    public static void copy(File src, File dst) {
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fileSave() {
        Object[] objarray;
        Object[] strarray;
        Object[] indarray;
        Object[] otherarray;
        Object[] watcharray;

        jFileChooser = new SmartFileChooser(Settings.getAbsolutepath());
        jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        GUISettings.localizeFileChooserHomeButton(jFileChooser);
        GUISettings.applyOrientation(jFileChooser);
        jFileChooser.setDialogTitle(Language.getString("EXPORT_TO_FOLDER"));
        jFileChooser.hideContainerOf((String) UIManager.get("FileChooser.fileNameLabelText"));
        jFileChooser.hideContainerOf((String) UIManager.get("FileChooser.filesOfTypeLabelText"));

        try {
            jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        int option = jFileChooser.showSaveDialog(this);
        if (option == JFileChooser.APPROVE_OPTION) {
            try {
                File currentFile = jFileChooser.getSelectedFile();
                Date date = new Date();
                SimpleDateFormat formatter;
                formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
                formatter.setTimeZone(Settings.getCurrentZone());
                String s = formatter.format(date);
                String outFilename = currentFile + "/profile" + s + ".mud";
                ZipOutputStream outZip = new ZipOutputStream(new FileOutputStream(outFilename));
                if (DataSelectionTree.workSpaces.size() > 0) {
                    objarray = copyObjectArray(DataSelectionTree.workSpaces, DataSelectionTree.workSpaces.size());
                    for (int i = 0; i < objarray.length; i++) {
                        File file1 = new File(Settings.getAbsolutepath() + "workspaces/" + objarray[i]);
                        System.out.println("Workspaces : " + objarray[i]);
                        FileInputStream in = new FileInputStream(file1);
                        outZip.putNextEntry(new ZipEntry(("/workspace/" + objarray[i])));
                        int len;
                        byte[] buf = new byte[1024];
                        while ((len = in.read(buf)) > 0) {
                            outZip.write(buf, 0, len);
                        }
                        outZip.closeEntry();
                        in.close();
                    }
                }
                if (DataSelectionTree.watchLists.size() > 0) {
                    watcharray = copyObjectArray(DataSelectionTree.watchLists, DataSelectionTree.watchLists.size());
                    outZip.putNextEntry(new ZipEntry("/watch List/watchList.mdf"));
                    boolean conditionalList = false;
                    boolean classicView = false;
                    for (int i = 0; i < watcharray.length; i++) {
                        TreeNode node = (TreeNode) watcharray[i];
                        if ((!(node.getParent().toString().equals(Language.getString("FUNCTION_LISTS")))) && (!(node.getParent().toString().equals(Language.getString("MIST_VIEW"))))) {

//                        System.out.println("Key Testing : " + watcharray[i]);
                            String string = (String) DataSelectionTree.strvector.get("0" + watcharray[i]) + "\n";
//                        System.out.println("Watch list : " + string);
                            byte[] bytearray;
                            bytearray = string.getBytes();
                            outZip.write(bytearray);
                        } else if ((node.getParent().toString().equals(Language.getString("MIST_VIEW")))) {
                            classicView = true;
                        } else {
                            conditionalList = true;
                        }
                    }
                    outZip.closeEntry();
                    //Adding conditinal watchlist entry
                    if (conditionalList) {
                        outZip.putNextEntry(new ZipEntry("/Conditional watch List/functionlist.mdf"));
                        for (int i = 0; i < watcharray.length; i++) {
                            TreeNode node = (TreeNode) watcharray[i];
                            if (node.getParent().toString().equals(Language.getString("FUNCTION_LISTS"))) {
                                String string = (String) DataSelectionTree.strvector.get("2" + watcharray[i]) + "\n";
                                byte[] bytearray;
                                bytearray = string.getBytes();
                                outZip.write(bytearray);
                            }

                        }
                        outZip.closeEntry();
                    }
                    if (classicView) {
                        outZip.putNextEntry(new ZipEntry("/Classic View/classicview.mdf"));
                        for (int i = 0; i < watcharray.length; i++) {
                            TreeNode node = (TreeNode) watcharray[i];
                            if (node.getParent().toString().equals(Language.getString("MIST_VIEW"))) {
                                String string = (String) DataSelectionTree.strvector.get("1" + watcharray[i]) + "\n";
                                byte[] bytearray;
                                bytearray = string.getBytes();
                                outZip.write(bytearray);
                            }

                        }
                        outZip.closeEntry();
                    }


                }
                if (DataSelectionTree.customColumns.size() > 0) {
                    strarray = copyObjectArray(DataSelectionTree.customColumns, DataSelectionTree.customColumns.size());
                    outZip.putNextEntry(new ZipEntry("/Custom Columns/customcolumns.mdf"));
                    for (int i = 0; i < strarray.length; i++) {
                        String string = customColumnSaveFormat(strarray[i].toString()) + "\n";
                        byte[] bytearray;
                        bytearray = string.getBytes();
                        outZip.write(bytearray);
                    }
                    outZip.closeEntry();
                }

                if (DataSelectionTree.customIndex.size() > 0) {
                    indarray = copyObjectArray(DataSelectionTree.customIndex, DataSelectionTree.customIndex.size());
                    outZip.putNextEntry(new ZipEntry("/Custom Index/customindex.mdf"));
                    for (int i = 0; i < indarray.length; i++) {
                        String str = customIndexSaveFormat(indarray[i].toString()) + "\n";
//                        System.out.println("Custom index string = "+str);
                        byte[] bytearray;
                        bytearray = str.getBytes();
                        outZip.write(bytearray);
                    }
                    outZip.closeEntry();
                }
                System.out.println("Update alias table befor saving.....");
                if (DataSelectionTree.symbolAlias.size() > 0) {
                    otherarray = copyObjectArray(DataSelectionTree.symbolAlias, DataSelectionTree.symbolAlias.size());
                    for (int i = 0; i < otherarray.length; i++) {
                        if (("" + otherarray[i]).equals(Language.getString("SYMBOL_ALIAS"))) {
                            SymbolMaster.saveSymbolAlias();
                            otherarray[i] = "alias.mdf";
                            String alias = "" + otherarray[i];
                            System.out.println("alisa : " + alias);
                        }
                        File file1 = new File(Settings.getAbsolutepath() + "datastore\\" + otherarray[i]);
                        FileInputStream in = new FileInputStream(file1);
                        outZip.putNextEntry(new ZipEntry(("/datastore/" + otherarray[i])));
                        int len;
                        byte[] buf = new byte[1024];
                        while ((len = in.read(buf)) > 0) {
                            outZip.write(buf, 0, len);
                        }
                        outZip.closeEntry();
                        in.close();
                    }

                }
                outZip.close();
                SharedMethods.showMessage(Language.getString("EXPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String customColumnSaveFormat(String name) {
        FormularEditorInterface obj = CustomFormulaStore.getSharedInstance().getFormulaObject(name);
        StringBuffer tempString = new StringBuffer();
        tempString.append(ViewConstants.VC_ID);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(UnicodeUtils.getUnicodeString(name));
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);
        tempString.append(ViewConstants.VC_CAPTIONS);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(UnicodeUtils.getUnicodeString(obj.getColumnName()));
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);
        tempString.append(ViewConstants.VC_DEPTH_CALC_VALUE);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(obj.getFunctionString());
        String temp = tempString.toString();

        return temp;
    }

    public String customIndexSaveFormat(String name) {
        CustomIndexInterface object = CustomIndexStore.getSharedInstance().getCustomIndexObject(name);
        return CustomIndexStore.getSharedInstance().createSavingString(object, name);

    }

    private Object[] copyObjectArray(DynamicArray da, int size) {
        Object[] arr = new Object[size];
        for (int i = 0; i < da.size(); i++) {
            arr[i] = da.get(i);

        }
        return arr;
    }
}
