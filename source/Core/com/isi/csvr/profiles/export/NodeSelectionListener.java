package com.isi.csvr.profiles.export;

import javax.swing.*;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 23-Apr-2007
 * Time: 16:49:26
 * To change this template use File | Settings | File Templates.
 */
class NodeSelectionListener extends MouseAdapter {
    JTree tree;

    NodeSelectionListener(JTree tree) {
        this.tree = tree;
    }

    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        int row = tree.getRowForLocation(x, y);
        TreePath path = tree.getPathForRow(row);
        //TreePath  path = tree.getSelectionPath();
        if (path != null) {
            CheckNode node = (CheckNode) path.getLastPathComponent();
            boolean isSelected = !(node.isSelected());
            node.setSelected(isSelected);


            if (node.getSelectionMode() == CheckNode.DIG_IN_SELECTION) {
                if (isSelected) {
                    tree.expandPath(path);
                } else {
                    tree.collapsePath(path);
                }
                if (node.isSelected()) {

                }
            }
            ((DefaultTreeModel) tree.getModel()).nodeChanged(node);
            // I need revalidate if node is root.  but why?
            if (row == 0) {
                tree.revalidate();
                tree.repaint();
            }
        }
    }
}