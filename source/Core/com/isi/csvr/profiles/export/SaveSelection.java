package com.isi.csvr.profiles.export;

import com.isi.csvr.Client;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.customformular.FormularEditorInterface;
import com.isi.csvr.customindex.CustomIndexInterface;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartFileChooser;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Jagoda S.D.
 * Date: May 27, 2005
 * Time: 12:50:40 PM
 */
public class SaveSelection extends JDialog {

    public File src, dst;
    SmartFileChooser jFileChooser;

    public SaveSelection(JDialog dialog) {
        super(Client.getInstance().getFrame(), Language.getString("EXPORT_TO_FOLDER"), true);

        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        fileSave();
        this.dispose();
        dialog.dispose();
    }

    public static void copy(File src, File dst) {
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fileSave() {

        Object[] objarray;
        Object[] strarray;
        Object[] indarray;
        Object[] otherarray;
        int[] watcharray;

        jFileChooser = new SmartFileChooser(".");
        jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        GUISettings.localizeFileChooserHomeButton(jFileChooser);
        jFileChooser.setDialogTitle(Language.getString("EXPORT_TO_FOLDER"));
        jFileChooser.hideContainerOf((String) UIManager.get("FileChooser.fileNameLabelText"));
        jFileChooser.hideContainerOf((String) UIManager.get("FileChooser.filesOfTypeLabelText"));

        try {
            jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        int option = jFileChooser.showSaveDialog(this);
        if (option == JFileChooser.APPROVE_OPTION) {
            try {

                File currentFile = jFileChooser.getSelectedFile();

                Date date = new Date();
                SimpleDateFormat formatter;
                formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
                formatter.setTimeZone(Settings.getCurrentZone());
                String s = formatter.format(date);

                String outFilename = currentFile + "/profile" + s + ".mud";

                ZipOutputStream outZip = new ZipOutputStream(new FileOutputStream(outFilename));
                if (DataSelection.workCheckList.getModel().getSize() > 0) {
                    objarray = DataSelection.workCheckList.getSelectedValues();
                    for (int i = 0; i < objarray.length; i++) {
                        File file1 = new File(Settings.getAbsolutepath() + "workspaces/" + objarray[i]);
                        FileInputStream in = new FileInputStream(file1);
                        System.out.println("Testing one : " + objarray[i]);
                        outZip.putNextEntry(new ZipEntry((Settings.getAbsolutepath() + "workspace/" + objarray[i])));
                        int len;
                        byte[] buf = new byte[1024];
                        while ((len = in.read(buf)) > 0) {
                            outZip.write(buf, 0, len);
                        }
                        outZip.closeEntry();
                        in.close();
                    }
                }

                if (DataSelection.watchList.getModel().getSize() > 0) {
                    watcharray = DataSelection.watchList.getSelectedIndices();
                    outZip.putNextEntry(new ZipEntry("/watch List/watchList.mdf"));
                    for (int i = 0; i < watcharray.length; i++) {
                        System.out.println("Testing two : " + watcharray[i]);
                        String string = DataSelection.strvector.elementAt(watcharray[i]) + "\n";
                        System.out.println("Testing  Three :" + string);
                        byte[] bytearray;
                        bytearray = string.getBytes();
                        outZip.write(bytearray);
                    }
                    outZip.closeEntry();
                }

                if (DataSelection.customColumnList.getModel().getSize() > 0) {

                    strarray = DataSelection.customColumnList.getSelectedValues();
                    outZip.putNextEntry(new ZipEntry("/Custom Columns/customcolumns.mdf"));
                    for (int i = 0; i < strarray.length; i++) {
                        String string = customColumnSaveFormat(strarray[i].toString()) + "\n";
                        byte[] bytearray;
                        bytearray = string.getBytes();
                        outZip.write(bytearray);

                    }
                    outZip.closeEntry();
                }

                if (DataSelection.customIndexList.getModel().getSize() > 0) {
                    indarray = DataSelection.customIndexList.getSelectedValues();
                    outZip.putNextEntry(new ZipEntry("/Custom Index/customindex.mdf"));
                    for (int i = 0; i < indarray.length; i++) {
                        String str = customIndexSaveFormat(indarray[i].toString()) + "\n";
//                        System.out.println("Custom index string = "+str);
                        byte[] bytearray;
                        bytearray = str.getBytes();
                        outZip.write(bytearray);

                    }
                    outZip.closeEntry();
                }
                System.out.println("Update alias table befor saving.....");
                SymbolMaster.saveSymbolAlias();
                if (DataSelection.otherList.getModel().getSize() > 0) {
                    otherarray = DataSelection.otherList.getSelectedValues();
                    for (int i = 0; i < otherarray.length; i++) {
                        if (otherarray[i].equals("Symbol alias")) {
                            otherarray[i] = "alias.mdf";
                        }
                        File file1 = new File(Settings.getAbsolutepath() + "datastore/" + otherarray[i]);
                        FileInputStream in = new FileInputStream(file1);
                        outZip.putNextEntry(new ZipEntry((Settings.getAbsolutepath() + "datastore/" + otherarray[i])));
                        int len;
                        byte[] buf = new byte[1024];
                        while ((len = in.read(buf)) > 0) {
                            outZip.write(buf, 0, len);
                        }
                        outZip.closeEntry();
                        in.close();
                    }

                }
                outZip.close();
                SharedMethods.showMessage(Language.getString("EXPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String customColumnSaveFormat(String name) {
        FormularEditorInterface obj = CustomFormulaStore.getSharedInstance().getFormulaObject(name);
        StringBuffer tempString = new StringBuffer();
        tempString.append(ViewConstants.VC_ID);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(UnicodeUtils.getUnicodeString(name));
//                tempString.append(name);
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);

        tempString.append(ViewConstants.VC_CAPTIONS);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(UnicodeUtils.getUnicodeString(obj.getColumnName()));
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);

        tempString.append(ViewConstants.VC_DEPTH_CALC_VALUE);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(obj.getFunctionString());
        String temp = tempString.toString();

        return temp;
    }

    public String customIndexSaveFormat(String name) {
        CustomIndexInterface object = CustomIndexStore.getSharedInstance().getCustomIndexObject(name);

        return CustomIndexStore.getSharedInstance().createSavingString(object, name);
//        StringBuffer tempString = new StringBuffer();
//
//
//
//                tempString.append(CustomIndexConstants.CI_ID);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(name);
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_CAPTIONS);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(UnicodeUtils.getUnicodeString(object.getSymbol()));
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_DESCRIPTION);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(UnicodeUtils.getUnicodeString(object.getDescription()));
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_TYPE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getType());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_DATE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getDate());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_VALUE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getValue());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_PRICE);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                tempString.append(object.getPrice());
//                tempString.append(CustomIndexConstants.CI_RECORD_DELIMETER);
//
//                tempString.append(CustomIndexConstants.CI_SYMBOLS);
//                tempString.append(CustomIndexConstants.CI_FIELD_DELIMETER);
//                String[] symbols =object.getSymbolsArray();
//                for(int j=0;j<symbols.length; j++){
//                    if(!symbols[j].equals("")){
//                        tempString.append(symbols[j]);
//                        tempString.append(CustomIndexConstants.CI_DATA_SEPERATOR);
//                    }
//                }
//                symbols = null;
//        String temp = tempString.toString();
//
//        return temp;
    }

}
