package com.isi.csvr.profiles.export;

import com.isi.csvr.Client;
import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.util.FlexGridLayout;
import com.isi.util.JCheckBoxList;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Jagoda S.D.
 * Date: May 27, 2005
 * Time: 12:48:27 PM
 */
public class DataSelection extends JDialog implements ActionListener {

    public static JCheckBoxList mainCheckList, workCheckList, watchList, customColumnList, customIndexList, otherList;
    public static Vector strvector = new Vector();
    public boolean isWorkspaceNull = false, isWatchlistNull = false;
    private TWButton ok, cancel;
    private JScrollPane workScroll, watchScroll, columnScroll, indexScroll, otherScroll;
    private JTabbedPane tabPane;
    private JPanel jpanel = new JPanel();
    private String str, sub, newsub;
    private String watchListFolder = "./datastore";
    private String workspaceFolder = Settings.getAbsolutepath() + "workspaces";

    public DataSelection() {

        super(Client.getInstance().getFrame(), Language.getString("EXPORT_USER_DATA"), true);
        WatchListManager.getInstance().save();
        Frame parent1 = Client.getInstance().getFrame();
        /*workCheckList.removeAll();
        watchList.removeAll();
        customColumnList.removeAll();
        customIndexList.removeAll();
        otherList.removeAll();*/
        readFiles();
        createUI(parent1);
    }

    public void readFiles() {
        try {
            BufferedReader in = new BufferedReader(new FileReader(watchListFolder + "/watchlist.mdf"));
            Vector watch = new Vector();
            Vector watch1 = new Vector();
            while ((str = in.readLine()) != null) {
                strvector.add(str);
                if (str.length() > 6) {
                    int firstindex = str.indexOf(";");
                    int lastindex = str.lastIndexOf(";");
                    sub = str.substring(firstindex, lastindex);
                    newsub = UnicodeUtils.getNativeString(sub);
                    watch.add(newsub);
                }
            }
            in.close();
            if (!watch.isEmpty()) {
                for (int j = 0; j < watch.size(); j++) {
                    String[] string = watch.get(j).toString().split("10=");
                    watch1.add(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[1], ",")));
                }
            }

            File file1 = new File(workspaceFolder);
            String[] strin = file1.list();
            Arrays.sort(strin);
            Object[] workspace = new Object[strin.length];
            for (int j = 0; j < strin.length; j++) {
                workspace[j] = strin[j];
            }
            workCheckList = new JCheckBoxList(workspace);
            if (workspace.length > 0) {
                isWorkspaceNull = true;
            }
            watchList = new JCheckBoxList(watch1);
            if (!watch1.isEmpty()) {
                isWatchlistNull = true;
            }

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            Enumeration en = CustomFormulaStore.getSharedInstance().getColumnNames();
            Vector tabs = new Vector();
            String colName;
            while (en.hasMoreElements()) {


                colName = en.nextElement().toString();
                tabs.add(colName);

            }

            customColumnList = new JCheckBoxList(tabs);
            tabs = null;

        } catch (Exception e) {
            System.out.println("Exception occured getting information from CustomFormulaStore");
        }
        try {
            Enumeration en = CustomIndexStore.getSharedInstance().getColumnNames();
            Vector vct = new Vector();
            String indexName;
            while (en.hasMoreElements()) {
                indexName = en.nextElement().toString();
                vct.add(indexName);
            }

            customIndexList = new JCheckBoxList(vct);
            vct = null;
        } catch (Exception e) {

        }
        try {
            Vector otherVector = new Vector();
            String other_one = Language.getString("SYMBOL_ALIAS");
            otherVector.add(other_one);
            otherList = new JCheckBoxList(otherVector);
            other_one = null;

        } catch (Exception e) {

        }


    }

    private void createUI(Frame parent) {

        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");
        tabPane = new JTabbedPane();
        tabPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        // tabPane.setPreferredSize(new Dimension(280,215));


        //tabPane.setBackground(color1);

        this.setPreferredSize(new Dimension(400, 320));
        this.setSize(new Dimension(400, 320));
        this.setResizable(true);
        this.setBackground(color1);
        this.setLocationRelativeTo(parent);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //this.setIcon

        Container container = getContentPane();

        // container.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));

        Border eched = BorderFactory.createEmptyBorder();

        JLabel msgLabel = new JLabel(Language.getString("EXPORT_PROFILE_MESSAGE"));
        msgLabel.setPreferredSize(new Dimension(270, 30));
        //JPanel msgPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        //msgPanel.setPreferredSize(new Dimension(250,40));
        //msgPanel.add(msgLabel);
        // container.add(msgLabel, BorderLayout.NORTH);
        //msgLabel.setPreferredSize(new Dimension(300, 30));

        //Border titled = BorderFactory.createTitledBorder(eched, Language.getString("MENU_WORKSPACES"));
        //workCheckList.setBorder(titled);

        workScroll = new JScrollPane(workCheckList);
        // workScroll.setPreferredSize(new Dimension(270, 175));
        tabPane.addTab(Language.getString("MENU_WORKSPACES"), workScroll);
        tabPane.setMnemonicAt(0, KeyEvent.VK_1);


        //Border title = BorderFactory.createTitledBorder(eched, Language.getString("WATCH_LISTS"));
        //watchList.setBorder(title);
        watchScroll = new JScrollPane(watchList);
        // watchScroll.setPreferredSize(new Dimension(270, 175));
        tabPane.addTab(Language.getString("WATCH_LISTS"), watchScroll);
        tabPane.setMnemonicAt(1, KeyEvent.VK_2);


        columnScroll = new JScrollPane(customColumnList);
        //columnScroll.setPreferredSize(new Dimension(270, 175));
        tabPane.addTab(Language.getString("FORMULAR_CREATER"), columnScroll);
        tabPane.setMnemonicAt(2, KeyEvent.VK_3);

        indexScroll = new JScrollPane(customIndexList);
        //columnScroll.setPreferredSize(new Dimension(270, 175));
        tabPane.addTab(Language.getString("CUSTOM_INDEX"), indexScroll);
        tabPane.setMnemonicAt(3, KeyEvent.VK_4);

        otherScroll = new JScrollPane(otherList);
        //symbolAlerts.setPreferredSize(new Dimension(270, 175));
        tabPane.addTab(Language.getString("OTHER_TAB"), otherScroll);
        tabPane.setMnemonicAt(4, KeyEvent.VK_5);


        JPanel jpanel4 = new JPanel();
        // jpanel4.setPreferredSize(new Dimension(320,220));
        String[] PanelWidths = {"100%"};
        String[] PanelHeights = {"100%"};
        FlexGridLayout flexGridLayoutp = new FlexGridLayout(PanelWidths, PanelHeights, 0, 0);
        jpanel4.setLayout(flexGridLayoutp);
        jpanel4.add(tabPane);
        String[] PanelWidths_lowerCenterPanel = {"100%"};
        String[] PanelHeights_lowerCenterPanel = {"30", "100%", "60"};
        FlexGridLayout flexGridLayout_container = new FlexGridLayout(PanelWidths_lowerCenterPanel, PanelHeights_lowerCenterPanel, 0, 0);
        container.setLayout(flexGridLayout_container);
        container.add(msgLabel);
        container.add(jpanel4);
        ok = new TWButton(Language.getString("EXPORT"));
        ok.addActionListener(this);
        cancel = new TWButton(Language.getString("CLOSE"));
        cancel.addActionListener(this);

        JPanel jpanel1 = new JPanel();
        jpanel1.setLayout(new FlowLayout(FlowLayout.CENTER));
        jpanel1.add(ok);
        jpanel1.add(cancel);
        container.add(jpanel1);
        GUISettings.applyOrientation(container);

        /*this.addComponentListener(new ComponentAdapter(){
            public void componentResized(ComponentEvent e) {
               // setSize(320, 340);
                setResizable(true);
            }

        });*/
    }

    public void actionPerformed(ActionEvent ev) {
        Object button = ev.getSource();
        if (button == ok) {
            int j = countSelectedOptions();
            if (j > 0) {
                SaveSelection sv = new SaveSelection(this);
                this.dispose();
            }
        } else {
            this.dispose();
        }
    }

    public void disableTabs(int[] indexes) {
        if (indexes.length > 0) {
            for (int i = 0; i < indexes.length; i++) {
                try {
                    tabPane.setEnabledAt(indexes[i], false);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void setSelectedTab(int select) {
        try {
            tabPane.setSelectedIndex(select);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    private int countSelectedOptions() {
        int totSelectedOpt = 0;
        if (DataSelection.workCheckList.getModel().getSize() > 0) {
            int workArray[] = DataSelection.workCheckList.getSelectedIndices();
            totSelectedOpt = workArray.length;
        }
        if (DataSelection.workCheckList.getModel().getSize() > 0) {
            int otArray[] = DataSelection.otherList.getSelectedIndices();
            totSelectedOpt = otArray.length;
        }
        if (DataSelection.watchList.getModel().getSize() > 0) {
            int watcharray[] = DataSelection.watchList.getSelectedIndices();
            totSelectedOpt = totSelectedOpt + watcharray.length;
        }
        if (DataSelection.customColumnList.getModel().getSize() > 0) {
            int strarray[] = DataSelection.customColumnList.getSelectedIndices();
            totSelectedOpt = totSelectedOpt + strarray.length;
        }
        if (DataSelection.customIndexList.getModel().getSize() > 0) {
            int indarray[] = DataSelection.customIndexList.getSelectedIndices();
            totSelectedOpt = totSelectedOpt + indarray.length;
        }

        return totSelectedOpt;


    }

}
