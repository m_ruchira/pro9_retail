package com.isi.csvr.profiles.export;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 23-Apr-2007
 * Time: 18:07:47
 * To change this template use File | Settings | File Templates.
 */
public class NodeObject {
    private String name = null;
    private String Parent = null;
    private boolean isChild = false;

    public NodeObject(String name, String parent, boolean isChild) {
        this.name = name;
        this.Parent = parent;
        this.isChild = isChild;
    }

    public boolean isChild() {
        return isChild;
    }

    public String getName() {
        return name;
    }

    public String getParent() {
        return Parent;
    }

    public String toString() {
        return name;
    }

}
