package com.isi.csvr.profiles.export;

import com.isi.csvr.Client;
import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolfilter.FunctionListManager;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.util.FlexGridLayout;
import com.isi.util.JCheckBoxList;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 23-Apr-2007
 * Time: 16:09:28
 */
public class DataSelectionTree extends JDialog implements ActionListener {
    public static JCheckBoxList mainCheckList, workCheckList, watchList, customColumnList, customIndexList, otherList;
    public static Hashtable<String, String> strvector = new Hashtable<String, String>();
    public static DynamicArray watchLists;
    public static DynamicArray workSpaces;
    public static DynamicArray customColumns;
    public static DynamicArray customIndex;
    public static DynamicArray symbolAlias;
    public boolean isWorkspaceNull = false, isWatchlistNull = false;
    private TWButton ok, cancel;
    private String str, sub, newsub;
    private String watchListFolder = Settings.getAbsolutepath() + "/datastore";
    private String workspaceFolder = Settings.getAbsolutepath() + "workspaces";
    private Vector nodeList = new Vector();
    private CheckNode[] nodes;
    private JTree tree;
    private CheckNode g_oTreeTop;
    private CheckNode watchListNode;
    private CheckNode workSpacenode;
    private CheckNode customColumnNode;
    private CheckNode customindexNode;
    private CheckNode otherNode;
    private CheckNode myStocks;
    private CheckNode classicView;
    private CheckNode conditinalWatch;
    private CheckNode forexWatch;


    public DataSelectionTree() {

        super(Client.getInstance().getFrame(), Language.getString("EXPORT_USER_DATA"), true);
        WatchListManager.getInstance().save();
        FunctionListManager.getInstance().save();
        Frame parent1 = Client.getInstance().getFrame();
        readFiles();
//        printnames();
        g_oTreeTop = new CheckNode(Language.getString("IMPORT_EXPORT_USERDATA"));
        watchListNode = new CheckNode(Language.getString("WATCH_LISTS"));
        workSpacenode = new CheckNode(Language.getString("MENU_WORKSPACES"));
        customColumnNode = new CheckNode(Language.getString("FORMULAR_CREATER"));
        customindexNode = new CheckNode(Language.getString("CUSTOM_INDEX"));
        otherNode = new CheckNode(Language.getString("OTHER_TAB"));
        myStocks = new CheckNode(Language.getString("MY_STOCKS"));
        classicView = new CheckNode(Language.getString("MIST_VIEW"));
        forexWatch = new CheckNode(Language.getString("FOREX_WATCH"));
        conditinalWatch = new CheckNode(Language.getString("FUNCTION_LISTS"));
        createUI(parent1);
    }

    public void readFiles() {
        watchLists = new DynamicArray();
        workSpaces = new DynamicArray();
        customColumns = new DynamicArray();
        customIndex = new DynamicArray();
        symbolAlias = new DynamicArray();
        nodeList.clear();
        DynamicArray st = new DynamicArray();
        try {
            BufferedReader in = new BufferedReader(new FileReader(watchListFolder + "/watchlist.mdf"));
            Vector watch = new Vector();
            Vector watchCon = new Vector();
            Vector watchMist = new Vector();
            Vector watch1 = new Vector();
            while ((str = in.readLine()) != null) {
                st.add(str);
                if (str.length() > 6) {
                    System.out.println("watchlist mdf : " + str);
                    int firstindex = str.indexOf("=");
                    int lastindex = str.lastIndexOf(";");
                    sub = str.substring(firstindex + 1, lastindex);
                    newsub = UnicodeUtils.getNativeString(sub);
                    watch.add(newsub);
                }
            }
            in.close();
            //exporting  conditional watchlists
            BufferedReader incon = new BufferedReader(new FileReader(watchListFolder + "/functionlist.mdf"));
            while ((str = incon.readLine()) != null) {
                st.add(str);
                if (str.length() > 6) {
                    System.out.println("functionlist mdf : " + str);
                    int firstindex = str.indexOf("=");
                    int lastindex = str.lastIndexOf(";");
                    sub = str.substring(firstindex + 1, lastindex);
                    newsub = UnicodeUtils.getNativeString(sub);
                    watchCon.add(newsub);
                }
            }
            incon.close();
            nodeList.add(new NodeObject("UserData", "userData", false));
            if (!watch.isEmpty()) {

                for (int j = 0; j < watch.size(); j++) {
                    System.out.println("watch lists : " + watch.get(j).toString());
                    String watchListType = watch.get(j).toString().substring(0, 1);
                    String[] string = watch.get(j).toString().split("10=")[1].split("\\;");
                    watch1.add(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")));
                    nodeList.add(new NodeObject(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")), "wl" + watchListType, true));
                    if (watchListType.equals("0")) {
                        strvector.put("0" + Language.getLanguageSpecificString(string[0], ","), (String) st.get(j));
                    } else if (watchListType.equals("1")) {
                        strvector.put("1" + Language.getLanguageSpecificString(string[0], ","), (String) st.get(j));
                    }
                    watchListType = "";
                }
            }

            //Adding conditional Watchlist nodes to tree
            if (!watchCon.isEmpty()) {
                for (int j = 0; j < watchCon.size(); j++) {
                    String[] string = watchCon.get(j).toString().split("10=")[1].split("\\;");
                    watch1.add(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")));
                    nodeList.add(new NodeObject(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")), "wl2", true));
                    for (int k = 0; k < st.size(); k++) {
                        String sentence = (String) st.get(k);
                        if (UnicodeUtils.getNativeString(sentence).contains(Language.getLanguageSpecificString(string[0], ","))) {
                            strvector.put("2" + Language.getLanguageSpecificString(string[0], ","), (String) st.get(k));
                        }

                    }
                }
            }

            //Adding Classic View watchlists nodes to tree
//            if (!watch.isEmpty()) {
//                for (int j = 0; j < watchCon.size(); j++) {
//                    String[] string = watchCon.get(j).toString().split("10=");
//                    watch1.add(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[1], ",")));
//                    nodeList.add(new NodeObject(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[1], ",")), "wl2", true));
//                    for(int k=0;k<st.size();k++) {
//                        String sentence = (String) st.get(k);
//                        if (UnicodeUtils.getNativeString(sentence).contains(Language.getLanguageSpecificString(string[1], ","))) {
//                            strvector.put(Language.getLanguageSpecificString(string[1], ","), (String) st.get(k));
//                        }
//
//                    }
//                }
//            }

            nodeList.add(new NodeObject(Language.getString("MENU_WORKSPACES"), "userData", false));
            File file1 = new File(workspaceFolder);
            /*String[] strin = file1.list();
            Arrays.sort(strin);
            Object[] workspace = new Object[strin.length];*/
            File[] fileList = file1.listFiles();
            Arrays.sort(fileList);
            Object[] workspace = new Object[fileList.length];
            for (int j = 0; j < fileList.length; j++) {
                if (!fileList[j].isDirectory()) {
                    workspace[j] = fileList[j];
                    nodeList.add(new NodeObject(fileList[j].getName(), "wp", true));
                }
            }
            workCheckList = new JCheckBoxList(workspace);
            if (workspace.length > 0) {
                isWorkspaceNull = true;
            }
            watchList = new JCheckBoxList(watch1);
            if (!watch1.isEmpty()) {
                isWatchlistNull = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        nodeList.add(new NodeObject(Language.getString("FORMULAR_CREATER"), "userData", false));
        try {
            Enumeration en = CustomFormulaStore.getSharedInstance().getColumnNames();
            Vector tabs = new Vector();
            String colName;
            while (en.hasMoreElements()) {
                colName = en.nextElement().toString();
                nodeList.add(new NodeObject(colName, "cs", true));
                tabs.add(colName);
            }
            customColumnList = new JCheckBoxList(tabs);
            tabs = null;
        } catch (Exception e) {
            System.out.println("Exception occured getting information from CustomFormulaStore");
        }
        nodeList.add(new NodeObject(Language.getString("CUSTOM_INDEX"), "userData", false));
        try {
            Enumeration en = CustomIndexStore.getSharedInstance().getColumnNames();
            Vector vct = new Vector();
            String indexName;
            while (en.hasMoreElements()) {
                indexName = en.nextElement().toString();
                vct.add(indexName);
                nodeList.add(new NodeObject(indexName, "ci", true));
            }
            customIndexList = new JCheckBoxList(vct);
            vct = null;
        } catch (Exception e) {
        }
        nodeList.add(new NodeObject(Language.getString("OTHER_TAB"), "userData", false));
        try {
            Vector otherVector = new Vector();
            String other_one = Language.getString("SYMBOL_ALIAS");
            otherVector.add(other_one);
            nodeList.add(new NodeObject(other_one, "ot", true));
            otherList = new JCheckBoxList(otherVector);
            other_one = null;
        } catch (Exception e) {

        }
//        nodeList.add(new NodeObject("MYStocks", "wl", false));
//        nodeList.add(new NodeObject("Classic View", "wl", false));
//        nodeList.add(new NodeObject("Forex Watch", "wl", false));
    }

    public void createUI(Frame parent) {
        Container container = getContentPane();
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");
        this.setPreferredSize(new Dimension(336, 320));
        this.setSize(new Dimension(336, 320));
        this.setResizable(true);
        this.setBackground(color1);
        this.setLocationRelativeTo(parent);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        String[] PanelWidths_lowerCenterPanel = {"100%"};
        String[] PanelHeights_lowerCenterPanel = {"30", "100%", "0"};
        FlexGridLayout flexGridLayout_container = new FlexGridLayout(PanelWidths_lowerCenterPanel, PanelHeights_lowerCenterPanel, 0, 0);
        container.setLayout(flexGridLayout_container);
        container.add(createMsgLabel());
        container.add(createTreePanel(createJTree()));
        container.add(createButtonPanel());
        GUISettings.applyOrientation(this);

    }

    public JTree createJTree() {

        for (int j = 1; j < nodeList.size(); j++) {
            NodeObject no = (NodeObject) nodeList.get(j);
            // separating all kind of watchlists to nodes
            if (no.getParent().startsWith("wl")) {
                if (no.getParent().toString().substring(2).equalsIgnoreCase("0")) {
                    myStocks.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
                } else if (no.getParent().toString().substring(2).equalsIgnoreCase("1")) {
                    classicView.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
                } else if (no.getParent().toString().substring(2).equalsIgnoreCase("8")) {
                    forexWatch.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
                } else if (no.getParent().toString().substring(2).equalsIgnoreCase("2")) {
                    conditinalWatch.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
                }
//                watchListNode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
            } else if (no.getParent().equals("wp")) {
                workSpacenode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
            } else if (no.getParent().equals("cs")) {
                customColumnNode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
            } else if (no.getParent().equals("ci")) {
                customindexNode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
            } else if (no.getParent().equals("ot")) {
                otherNode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));

            }

        }

        tree = new JTree(g_oTreeTop);
        g_oTreeTop.add(watchListNode);
        if (workSpacenode.getChildCount() > 0) {
            g_oTreeTop.add(workSpacenode);
        }
        if (customColumnNode.getChildCount() > 0) {
            g_oTreeTop.add(customColumnNode);
        }
        if (customindexNode.getChildCount() > 0) {
            g_oTreeTop.add(customindexNode);
        }
        if (otherNode.getChildCount() > 0) {
            g_oTreeTop.add(otherNode);
        }
        if (myStocks.getChildCount() > 0) {
            watchListNode.add(myStocks);
        }
        if (classicView.getChildCount() > 0) {
            watchListNode.add(classicView);
        }
        if (forexWatch.getChildCount() > 0) {
            watchListNode.add(forexWatch);
        }
        if (conditinalWatch.getChildCount() > 0) {
            watchListNode.add(conditinalWatch);
        }
        tree.setCellRenderer(new CheckRenderer());
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        tree.putClientProperty("JTree.lineStyle", "Angled");
        tree.addMouseListener(new NodeSelectionListener(tree));
        tree.setShowsRootHandles(true);


        expandAll(tree);
        GUISettings.applyOrientation(tree);
        return tree;
    }

    public void expandAll(JTree tree) {
        int row = 0;
        while (row < tree.getRowCount()) {
            tree.expandRow(row);
            row++;
        }
    }

    public JLabel createMsgLabel() {
        JLabel msgLabel = new JLabel(Language.getString("EXPORT_PROFILE_MESSAGE"));
        msgLabel.setPreferredSize(new Dimension(270, 30));
        return msgLabel;
    }

    public JPanel createTreePanel(JTree tree) {
        JPanel treePanel = new JPanel();
        JScrollPane treeScroll = new JScrollPane(tree);
        String[] PanelWidths = {"100%"};
        String[] PanelHeights = {"100%"};
        FlexGridLayout flexGridLayoutp = new FlexGridLayout(PanelWidths, PanelHeights, 0, 0);
        treePanel.setLayout(flexGridLayoutp);
        treePanel.add(treeScroll);
        return treePanel;
    }

    public JPanel createButtonPanel() {
        ok = new TWButton(Language.getString("EXPORT"));
        ok.addActionListener(this);
        cancel = new TWButton(Language.getString("BTN_CLOSE"));
        cancel.addActionListener(this);
        JPanel buutonPanel = new JPanel();
        buutonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        buutonPanel.add(ok);
        buutonPanel.add(cancel);
        return buutonPanel;
    }

    public void printnames() {
        try {
            for (int i = 0; i < nodeList.size(); i++) {
                System.out.println("Printing elements : " + nodeList.get(i));
            }

        } catch (Exception e) {

        }
    }

    public NodeObject getNodeObject(String name) {
        NodeObject no = null;
        Enumeration e = nodeList.elements();
        while (e.hasMoreElements()) {
            no = (NodeObject) e.nextElement();
            if (no.getName().equals(name)) {
                break;


            }

        }

        return no;
    }


    public void getSelectedCheckBoxes(CheckNode root) {
        Enumeration e = root.breadthFirstEnumeration();  //breadthFirstEnumeration()
        while (e.hasMoreElements()) {
            CheckNode node = (CheckNode) e.nextElement();
            if (node.isSelected()) {
                TreeNode[] nodes = node.getPath();
                if (nodes.length > 2) {
                    if (nodes[1].toString().equals(Language.getString("WATCH_LISTS"))) {
                        watchLists.add(nodes[nodes.length - 1]);

                    } else if (nodes[1].toString().equals(Language.getString("MENU_WORKSPACES"))) {
                        workSpaces.add(nodes[nodes.length - 1]);

                    } else if (nodes[1].toString().equals(Language.getString("FORMULAR_CREATER"))) {
                        customColumns.add(nodes[nodes.length - 1]);

                    } else if (nodes[1].toString().equals(Language.getString("CUSTOM_INDEX"))) {
                        customIndex.add(nodes[nodes.length - 1]);

                    } else if (nodes[1].toString().equals(Language.getString("OTHER_TAB"))) {
                        symbolAlias.add(nodes[nodes.length - 1]);
                    }
                }
            }
        }
    }


    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == ok) {
            int j = getCountOfSelectedOptions(g_oTreeTop);
            if (j > 0) {
                getSelectedCheckBoxes(g_oTreeTop);
                SaveSelectionTree sv = new SaveSelectionTree(this);
                this.dispose();
                getTreeSelection();
            }
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.System, "ExportUserData");
        } else if (e.getSource() == cancel) {
            this.dispose();
        }
    }

    private int getCountOfSelectedOptions(CheckNode root) {
        int totalTreeCount = 0;
        Enumeration e = root.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            CheckNode node = (CheckNode) e.nextElement();
            if (node.isSelected()) {
                totalTreeCount = totalTreeCount + 1;
//                System.out.println("Node : " + "" + node.getPath());
            }
        }
        return totalTreeCount;
    }

    public void getTreeSelection() {
        TreePath[] paths = tree.getSelectionPaths();
//        System.out.println("Testing for tree selection......");
        for (int i = 0; paths != null && i < paths.length; i++) {
            System.out.println(paths[i]);
        }
    }


}
