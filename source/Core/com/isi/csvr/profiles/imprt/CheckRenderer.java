package com.isi.csvr.profiles.imprt;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 23-Apr-2007
 * Time: 16:53:39
 * To change this template use File | Settings | File Templates.
 */
class CheckRenderer extends JPanel implements TreeCellRenderer {
    public static final DefaultTreeCellRenderer DEFAULT_RENDERER = new DefaultTreeCellRenderer();
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oUnSelectedFG;
    private static Color g_oUnSelectedBG;
    private static ImageIcon g_oLeafIcon = null;
    private static ImageIcon g_oClosedIcon = null;
    private static ImageIcon g_oExpandedIcon = null;
    private static ImageIcon g_oNewListIcon = null;
    Color selectionBorderColor, selectionForeground, selectionBackground, textForeground, textBackground;
    private JCheckBox leafRenderer = new JCheckBox();
    private JLabel leaflabel = new JLabel();
    private DefaultTreeCellRenderer nonLeafRenderer = new DefaultTreeCellRenderer();

    public CheckRenderer() {
        reload();

    }

    public static void reload() {
        try {
            g_oSelectedFG = Theme.getColor("MENU_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BACKGROUND_COLOR");
            g_oUnSelectedFG = Theme.getColor("MENU_FGCOLOR");
            g_oUnSelectedBG = Theme.getColor("BACKGROUND_COLOR");
            g_oLeafIcon = new ImageIcon("images/themes/TreeLeaf.gif");
            g_oClosedIcon = new ImageIcon("images/themes/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/themes/TreeFolderOpened.gif");
            g_oNewListIcon = new ImageIcon("images/themes/new.gif");
        } catch (Exception e) {
            g_oSelectedFG = Color.white;
            g_oSelectedBG = Color.black;
            g_oUnSelectedFG = Color.white;
            g_oUnSelectedBG = Color.black;
        }
    }

    protected JCheckBox getLeafRenderer() {
        return leafRenderer;
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean isSelected, boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        Component renderer = DEFAULT_RENDERER.getTreeCellRendererComponent(tree, value, isSelected, expanded, leaf, row, hasFocus);

        if (leaf) {
            if (((DefaultMutableTreeNode) value).getUserObject() instanceof java.lang.String) {
                leaflabel.setIcon(g_oClosedIcon);
                String stringValue = tree.convertValueToText(value, isSelected, expanded, leaf, row, false);
                leaflabel.setText(stringValue);
                renderer = leaflabel;

            } else if (((DefaultMutableTreeNode) value).getUserObject() instanceof WhatchListObject) {
                String stringValue = tree.convertValueToText(value, isSelected, expanded, leaf, row, false);
                leafRenderer.setText(stringValue);
                leafRenderer.setSelected(false);
                leafRenderer.setEnabled(tree.isEnabled());
                if (isSelected) {
                    leafRenderer.setForeground(selectionForeground);
                    leafRenderer.setBackground(selectionBackground);
                } else {
                    leafRenderer.setForeground(textForeground);
                    leafRenderer.setBackground(textBackground);
                }
                if ((value != null) && (value instanceof DefaultMutableTreeNode)) {
                    leafRenderer.setSelected(((CheckNode) value).isSelected());
                }
                renderer = leafRenderer;
            }


        } else {
            renderer = nonLeafRenderer.getTreeCellRendererComponent(tree,
                    value, isSelected, expanded, leaf, row, hasFocus);
            return renderer;
        }
        return renderer;
    }

}
