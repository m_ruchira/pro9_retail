package com.isi.csvr.profiles.imprt;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.symbolfilter.FunctionListManager;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.watchlist.WatchListManager;

import javax.swing.*;
import java.io.*;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User:Modified by Chandika
 * Date: 07-May-2007
 * Time: 13:17:33
 */
public class OpenZipFileTree {
    public static Hashtable<String, String> strvector = new Hashtable<String, String>();
    public static Vector workVector = new Vector();
    public static Vector otherVector = new Vector();
    public static Hashtable<String, String> custColVector = new Hashtable<String, String>();
    public static Hashtable<String, String> custIndexVector = new Hashtable<String, String>();
    public static File currentFile;
    private static Vector workList, watch, watch1, custCol, custInd, otherList, watchCon;
    private static Vector watchMist;
    private String str, sub, newsub;
    private SmartFileChooser jFileChooser;
    private ImportUITree im;
    private Vector nodeList = new Vector();

    public OpenZipFileTree() {
        WatchListManager.getInstance().save();
        FunctionListManager.getInstance().save();
        jFileChooser = new SmartFileChooser(Settings.getAbsolutepath());
        GUISettings.localizeFileChooserHomeButton(jFileChooser);
        GUISettings.applyOrientation(jFileChooser);
        jFileChooser.addChoosableFileFilter(new MyFilterTree());
        jFileChooser.setAcceptAllFileFilterUsed(false);
        jFileChooser.setDialogTitle(Language.getString("IMPORT_USER_DATA"));
        int option = jFileChooser.showOpenDialog(Client.getInstance().getFrame());
        if (option == JFileChooser.CANCEL_OPTION) {
            return;
        } else if (option == JFileChooser.APPROVE_OPTION) {
            currentFile = jFileChooser.getSelectedFile();
            try {
                // Open the ZIP file
                ZipInputStream inZip = new ZipInputStream(new FileInputStream(currentFile));
                workList = new Vector();
                watch = new Vector();
                custCol = new Vector();
                custInd = new Vector();
                otherList = new Vector();
                watchCon = new Vector();
                watchMist = new Vector();

                while (true) {
                    ZipEntry entry = inZip.getNextEntry();
                    if (entry == null) {
                        break;
                    }
                    String zipEntryName = entry.getName();
                    if (zipEntryName.startsWith("/workspace/")) {
                        workVector.add(zipEntryName);
                        int lastindex = zipEntryName.lastIndexOf("/");
                        sub = zipEntryName.substring(lastindex + 1);
                        nodeList.add(new NodeObject(sub, "wp", true));
                        workList.add(sub);
                    } else if (zipEntryName.startsWith("/datastore/")) {
                        otherVector.add(zipEntryName);
                        int lastindex = zipEntryName.lastIndexOf("/");
                        sub = zipEntryName.substring(lastindex + 1);
                        if (sub.equals("alias.mdf")) {
                            sub = Language.getString("SYMBOL_ALIAS");
                        }
                        otherList.add(sub);
                        nodeList.add(new NodeObject(sub, "ot", true));
                    } else if (zipEntryName.startsWith("\\datastore\\")) {
                        otherVector.add(zipEntryName);
                        int lastindex = zipEntryName.lastIndexOf("\\");
                        sub = zipEntryName.substring(lastindex + 1);
                        if (sub.equals("alias.mdf")) {
                            sub = Language.getString("SYMBOL_ALIAS");

                        }
                        otherList.add(sub);
                        nodeList.add(new NodeObject(sub, "ot", true));
                    } else if (zipEntryName.startsWith("/watch List/")) {
                        File outFile = new File(Settings.getAbsolutepath() + "temp/tempProfile");
                        OutputStream out = new FileOutputStream(outFile);
                        // Transfer bytes from the ZIP file to the output file
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = inZip.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }
                        // Close the streams
                        inZip.closeEntry();
                        out.close();
                        BufferedReader in = new BufferedReader(new FileReader(outFile));
                        while ((str = in.readLine()) != null) {
                            System.out.println("StrVector : " + str);
                            if (str.length() > 6) {
                                int firstindex = str.indexOf("=");
                                int lastindex = str.lastIndexOf(";");
                                sub = str.substring(firstindex + 1, lastindex);
                                newsub = UnicodeUtils.getNativeString(sub);
                                System.out.println("New Sub : " + newsub);
                                String[] string = newsub.split("10=")[1].split("\\;");
                                strvector.put("0" + Language.getLanguageSpecificString(string[0], ","), str);
                                watch.add(newsub);
                            }
                        }
//                        System.out.println("Size of Watch : "+watch.size());
                        in.close();
                        try {
                            outFile.delete();
                        } catch (Exception e) {
                        }

                    } else if (zipEntryName.startsWith("/Conditional watch List/")) {
                        File outFile = new File(Settings.getAbsolutepath() + "temp/tempProfile");
                        OutputStream out = new FileOutputStream(outFile);
                        // Transfer bytes from the ZIP file to the output file
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = inZip.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }
                        // Close the streams
                        inZip.closeEntry();
                        out.close();
                        BufferedReader in = new BufferedReader(new FileReader(outFile));
                        while ((str = in.readLine()) != null) {
                            if (str.length() > 6) {
                                int firstindex = str.indexOf("=");
                                int lastindex = str.lastIndexOf(";");
                                sub = str.substring(firstindex + 1, lastindex);
                                newsub = UnicodeUtils.getNativeString(sub);
                                String[] string = newsub.split("10=")[1].split("\\;");
                                strvector.put("2" + Language.getLanguageSpecificString(string[0], ","), str);
                                watchCon.add(newsub);
                            }
                        }
                        in.close();
                        try {
                            outFile.delete();
                        } catch (Exception e) {
                        }

                    }/* else if (zipEntryName.startsWith("/Classic View/")) {
                        File outFile = new File(Settings.getAbsolutepath()+"temp/tempProfile");
                        OutputStream out = new FileOutputStream(outFile);
                        // Transfer bytes from the ZIP file to the output file
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = inZip.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }
                        // Close the streams
                        inZip.closeEntry();
                        out.close();
                        BufferedReader in = new BufferedReader(new FileReader(outFile));
                        while ((str = in.readLine()) != null) {
                            if (str.length() > 6) {
                                int firstindex = str.indexOf("=");
                                int lastindex = str.lastIndexOf(";");
                                sub = str.substring(firstindex + 1, lastindex);
                                newsub = UnicodeUtils.getNativeString(sub);
                                String[] string = newsub.split("10=");
                                strvector.put("1"+Language.getLanguageSpecificString(string[1], ","), str);
                                watchMist.add(newsub);
                            }
                        }
                        in.close();
                        try {
                            outFile.delete();
                        } catch (Exception e) {
                        }

                    }*/ else if (zipEntryName.startsWith("/Custom Columns/")) {
                        String str2, sub2, newsub2;
                        File outFile2 = new File(Settings.getAbsolutepath() + "temp/tempProfile2");
                        OutputStream out2 = new FileOutputStream(outFile2);
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = inZip.read(buf)) > 0) {
                            out2.write(buf, 0, len);
                        }
                        inZip.closeEntry();
                        out2.close();
                        BufferedReader in = new BufferedReader(new FileReader(outFile2));
                        while ((str2 = in.readLine()) != null) {
                            if (str2.length() > 6) {
                                int firstindex = str2.indexOf("=");
                                int lastindex = str2.indexOf(";");
                                sub2 = str2.substring(firstindex + 1, lastindex);
                                newsub2 = UnicodeUtils.getNativeString(sub2);
                                String[] string = newsub2.split("10=");
                                System.out.println("Key : " + newsub2 + " value : " + str2);
                                custColVector.put(newsub2, str2);
                                custCol.add(newsub2);
                                nodeList.add(new NodeObject(newsub2, "cs", true));
                            }
                        }
                        in.close();
                        try {
                            outFile2.delete();
                        } catch (Exception e) {
                        }

                    } else if (zipEntryName.startsWith("/Custom Index/")) {
                        String str2, sub2, newsub2;
                        File outFile3 = new File(Settings.getAbsolutepath() + "temp/tempProfile3");
                        OutputStream out3 = new FileOutputStream(outFile3);
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = inZip.read(buf)) > 0) {
                            out3.write(buf, 0, len);
                        }
                        inZip.closeEntry();
                        out3.close();
                        BufferedReader in = new BufferedReader(new FileReader(outFile3));
                        while ((str2 = in.readLine()) != null) {
                            if (str2.length() > 6) {
                                int firstindex = str2.indexOf("=");
                                int lastindex = str2.indexOf(";");
                                sub2 = str2.substring(firstindex + 1, lastindex);
                                newsub2 = UnicodeUtils.getNativeString(sub2);
                                custInd.add(newsub2);
                                custIndexVector.put(newsub2, str2);
                                nodeList.add(new NodeObject(newsub2, "ci", true));
                            }
                        }
                        in.close();
                        try {
                            outFile3.delete();
                        } catch (Exception e) {
                        }
                    }

                    watch1 = new Vector();
                    //Adding other watchlist types to tree
                    if (!watch.isEmpty()) {
                        for (int j = 0; j < watch.size(); j++) {
                            String watchListType = watch.get(j).toString().substring(0, 1);
                            String[] string = watch.get(j).toString().split("10=")[1].split("\\;");
                            watch1.add(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")));
                            nodeList.add(new NodeObject(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")), "wl" + watchListType, true));
                            watchListType = "";
                        }
                        watch.clear();
                    }
                    //Adding conditional watchlists to import tree
                    if (!watchCon.isEmpty()) {
                        for (int j = 0; j < watchCon.size(); j++) {
                            String[] string = watchCon.get(j).toString().split("10=")[1].split("\\;");
                            watch1.add(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")));
                            nodeList.add(new NodeObject(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")), "wl2", true));
                        }
                        watchCon.clear();
                    }
                    if (!watchMist.isEmpty()) {
                        for (int j = 0; j < watchMist.size(); j++) {
                            String[] string = watchMist.get(j).toString().split("10=")[1].split("\\;");
                            watch1.add(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")));
                            nodeList.add(new NodeObject(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[0], ",")), "wl1", true));
                        }
                        watchMist.clear();
                    }

                }
                inZip.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            ImportUITree im = new ImportUITree(nodeList);
            this.im = im;
        }
    }

    public ImportUITree getObject() {
        return im;
    }
}

class MyFilterTree extends javax.swing.filechooser.FileFilter {
    public boolean accept(File file) {
        String filename = file.getName();
        if (filename.endsWith(".mud")) {
            return true;
        } else if (file.isDirectory()) {
            return true;
        } else {
            return false;
        }
    }

    public String getDescription() {
        return "*.mud";
    }
}
