package com.isi.csvr.profiles.imprt;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 07-May-2007
 * Time: 13:30:38
 * To change this template use File | Settings | File Templates.
 */
public class NodeObject {
    private String name = null;
    private String Parent = null;
    private boolean isChild = false;

    public NodeObject(String name, String parent, boolean isChild) {
        this.name = name;
        this.Parent = parent;
        this.isChild = isChild;
    }

    public boolean isChild() {
        return isChild;
    }

    public String getName() {
        return name;
    }

    public String getParent() {
        return Parent;
    }

    public String toString() {
        return name;
    }

}
