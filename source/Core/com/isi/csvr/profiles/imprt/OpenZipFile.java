package com.isi.csvr.profiles.imprt;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.util.JCheckBoxList;

import javax.swing.*;
import java.io.*;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Jagoda S.D.
 * Date: Jun 1, 2005
 * Time: 5:12:28 PM
 */

public class OpenZipFile {

    public static JCheckBoxList workCheckList, watchList, customColumnList, custIndexList, otherCheckList;
    public static Vector strvector = new Vector();
    public static Vector workVector = new Vector();
    public static Vector otherVector = new Vector();
    public static Vector custColVector = new Vector();
    public static Vector custIndexVector = new Vector();
    public static File currentFile;
    private static Vector workList, watch, watch1, custCol, custInd, otherList;
    private String str, sub, newsub;
    private SmartFileChooser jFileChooser;
    private ImportUI im;

    public OpenZipFile() {
        jFileChooser = new SmartFileChooser(".");
        GUISettings.localizeFileChooserHomeButton(jFileChooser);
        jFileChooser.addChoosableFileFilter(new MyFilter());
        jFileChooser.setAcceptAllFileFilterUsed(false);

        jFileChooser.setDialogTitle(Language.getString("IMPORT_USER_DATA"));
        int option = jFileChooser.showOpenDialog(Client.getInstance().getFrame());


        if (option == JFileChooser.CANCEL_OPTION) {
            return;
        } else if (option == JFileChooser.APPROVE_OPTION) {
            currentFile = jFileChooser.getSelectedFile();
            try {
                // Open the ZIP file
                ZipInputStream inZip = new ZipInputStream(new FileInputStream(currentFile));

                workList = new Vector();
                watch = new Vector();
                custCol = new Vector();
                custInd = new Vector();
                otherList = new Vector();

                while (true) {
                    ZipEntry entry = inZip.getNextEntry();
                    if (entry == null) {
                        break;
                    }
                    String zipEntryName = entry.getName();
                    if (zipEntryName.startsWith("/workspace/")) {
                        workVector.add(zipEntryName);
                        int lastindex = zipEntryName.lastIndexOf("/");
                        sub = zipEntryName.substring(lastindex + 1);
                        workList.add(sub);
                    } else if (zipEntryName.startsWith("/datastore/")) {
                        // System.out.println("One########################");
                        otherVector.add(zipEntryName);
                        //System.out.println("two########################"+zipEntryName);
                        int lastindex = zipEntryName.lastIndexOf("/");
                        //System.out.println("Three######################"+lastindex);
                        sub = zipEntryName.substring(lastindex + 1);
                        // System.out.println("Four#######################"+sub);
                        if (sub.equals("alias.mdf")) {
                            sub = "Symbol alias";

                        }
                        otherList.add(sub);
                    } else if (zipEntryName.startsWith("/watch List/")) {
                        File outFile = new File(Settings.getAbsolutepath() + "temp/tempProfile");
                        OutputStream out = new FileOutputStream(outFile);
                        // Transfer bytes from the ZIP file to the output file
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = inZip.read(buf)) > 0) {
                            out.write(buf, 0, len);
                        }

                        // Close the streams
                        inZip.closeEntry();
                        out.close();

                        BufferedReader in = new BufferedReader(new FileReader(outFile));
                        while ((str = in.readLine()) != null) {
                            System.out.println("StrVector : " + str);
                            strvector.add(str);
                            if (str.length() > 6) {
                                int firstindex = str.indexOf(";");
                                int lastindex = str.lastIndexOf(";");
                                sub = str.substring(firstindex + 4, lastindex);
                                newsub = UnicodeUtils.getNativeString(sub);
                                System.out.println("New Sub : " + newsub);
                                watch.add(newsub);
                            }
                        }
                        in.close();
                        try {
                            outFile.delete();
                        } catch (Exception e) {
                        }


                    } else if (zipEntryName.startsWith("/Custom Columns/")) {
                        String str2, sub2, newsub2;

                        File outFile2 = new File(Settings.getAbsolutepath() + "temp/tempProfile2");
                        OutputStream out2 = new FileOutputStream(outFile2);
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = inZip.read(buf)) > 0) {
                            out2.write(buf, 0, len);
                        }
                        inZip.closeEntry();
                        out2.close();
                        BufferedReader in = new BufferedReader(new FileReader(outFile2));
                        while ((str2 = in.readLine()) != null) {
                            custColVector.add(str2);

                            if (str2.length() > 6) {

                                int firstindex = str2.indexOf("=");
                                int lastindex = str2.indexOf(";");
                                sub2 = str2.substring(firstindex + 1, lastindex);
                                newsub2 = UnicodeUtils.getNativeString(sub2);
                                custCol.add(newsub2);
                            }
                        }
                        in.close();
                        try {
                            outFile2.delete();
                        } catch (Exception e) {
                        }


                    } else if (zipEntryName.startsWith("/Custom Index/")) {
                        String str2, sub2, newsub2;

                        File outFile3 = new File(Settings.getAbsolutepath() + "temp/tempProfile3");
                        OutputStream out3 = new FileOutputStream(outFile3);
                        byte[] buf = new byte[1024];
                        int len;
                        while ((len = inZip.read(buf)) > 0) {
                            out3.write(buf, 0, len);
                        }
                        inZip.closeEntry();
                        out3.close();
                        BufferedReader in = new BufferedReader(new FileReader(outFile3));
                        while ((str2 = in.readLine()) != null) {
                            custIndexVector.add(str2);

                            if (str2.length() > 6) {

                                int firstindex = str2.indexOf("=");
                                int lastindex = str2.indexOf(";");
                                sub2 = str2.substring(firstindex + 1, lastindex);
                                newsub2 = UnicodeUtils.getNativeString(sub2);
                                custInd.add(newsub2);
                            }
                        }
                        in.close();
                        try {
                            outFile3.delete();
                        } catch (Exception e) {
                        }
                    }

                    watch1 = new Vector();
                    if (!watch.isEmpty()) {
                        for (int j = 0; j < watch.size(); j++) {
                            String[] string = watch.get(j).toString().split("10=");
                            watch1.add(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(string[1], ",")));
                        }
                    }
                }
                inZip.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            workCheckList = new JCheckBoxList(workList);
            watchList = new JCheckBoxList(watch1);
            customColumnList = new JCheckBoxList(custCol);
            custIndexList = new JCheckBoxList(custInd);
            otherCheckList = new JCheckBoxList(otherList);
            ImportUI im = new ImportUI(workCheckList, watchList, customColumnList, custIndexList, otherCheckList);

            this.im = im;


            //im.setVisible(true);


        }
    }

    public static void clearStaticVectors() {
//        System.out.println("static vestors cleared");
        custColVector.clear();
        strvector.clear();
        workVector.clear();
        custIndexVector.clear();
        otherVector.clear();
    }

    public void disableTabs(int[] indexes, int select) {

        im.disableTabs(indexes);
        im.setSelectedTab(select);
    }

    public ImportUI getObject() {
        return im;
    }
}

class MyFilter extends javax.swing.filechooser.FileFilter {
    public boolean accept(File file) {
        String filename = file.getName();
        if (filename.endsWith(".mud")) {
            return true;
        } else if (file.isDirectory()) {
            return true;
        } else {
            return false;
        }
    }

    public String getDescription() {
        return "*.mud";
    }


}

