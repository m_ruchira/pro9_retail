package com.isi.csvr.profiles.imprt;

import com.isi.csvr.SymbolMaster;
import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.customformular.FormularEditor;
import com.isi.csvr.customindex.CustomIndexConstants;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.io.*;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Jagoda S.D.
 * Date: Jun 1, 2005
 * Time: 5:18:01 PM
 */

public class OpenSelected {
    private int[] watchInt;

    private int[] custColInt;

    private int[] custIndInt;
    private int[] workArray;
    private int[] otherArray;
    private String str;
    private Vector watch;
    private Vector watchBuff;
    private Vector compare;
    private Vector custCol;
    private Vector custColBuff;
    private Vector newColVector;
    private Vector exsistingColVector;
    private Vector exsistingWatchVector;
    private Vector newWatchVector;
    private Vector custInd;
    private Vector custIndBuff;
    private Vector newIndVector;
    private Vector exsistingIndVector;
    private String sub;
    private String string;
    private ImportUI impo;

    public OpenSelected(ImportUI importUI, boolean type_merged) {
        impo = importUI;
        int index = 0;

        compare = new Vector();   // This vector is filld with both watchlist names and custom column names
        try {

            int totSelected = 0;
            int selection = 1;
            int workArraylen = 0;
            int otherArraylen = 0;
            if (OpenZipFile.watchList.getModel().getSize() > 0) {
                watchInt = OpenZipFile.watchList.getSelectedIndices();
                totSelected = watchInt.length;

                BufferedReader in = null;
                watchBuff = new Vector();
                File newFile = new File(Settings.getAbsolutepath() + "datastore/watchlist.mdf");
                if (newFile.exists()) {
                    try {
                        in = new BufferedReader(new FileReader(Settings.getAbsolutepath() + "datastore/watchlist.mdf"));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    watch = new Vector();


                    while ((str = in.readLine()) != null) {
                        watch.add(str);
                    }
                    in.close();

                    for (int i = 0; i < watchInt.length; i++) {
                        string = (String) OpenZipFile.strvector.elementAt(watchInt[i]);
                        watchBuff.add(string);
                        System.out.println("String : " + string);
                        String[] str1 = string.split("10=");
                        int int1 = str1[1].indexOf(";");
                        String str2 = str1[1].substring(0, int1);

                        for (int j = 0; j < watch.size(); j++) {
                            sub = (String) watch.elementAt(j);
                            String[] sub1 = sub.split("10=");

                            int int2 = sub1[1].indexOf(";");
                            String sub2 = sub1[1].substring(0, int2);
//                            System.out.println("sub2 : "+sub2+"str2 : "+str2);
                            if (sub2.equals(str2)) {
                                index = index + 1;
                                String msg = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(str2), ",") + "----" + Language.getString("WATCH_LIST");
                                compare.add(msg);

                            }

                        }

                    }
                    /*if (index > 0) {
                        String str = Language.getString("MSG_ASKING_TO_REPLACE_WATCHLISTS");
                        String str2 = "";
                        for (int j = 0; j < compare.size(); j++) {
                            str2 = "\t" + str2 + "<li>" + compare.elementAt(j) + "</li>";
                        }
                        str = str.replaceAll("\\[SELECTED_LIST\\]", str2);
                        int i = SharedMethods.showConfirmMessage(str, JOptionPane.WARNING_MESSAGE);
                        if (i == 0) {
                            writeToFile();
                        }
                    } else {
                        writeToFile();
                    }*/
                }
            }
            if (OpenZipFile.customColumnList.getModel().getSize() > 0) {
                String str1, str2, str4, orig1, orig3;


                custColInt = OpenZipFile.customColumnList.getSelectedIndices();
                totSelected = totSelected + custColInt.length;
                BufferedReader in = null;
                custColBuff = new Vector();
                File newFile = new File(Settings.getAbsolutepath() + "datastore/customformula.mdf");
                if (newFile.exists()) {
                    try {
                        in = new BufferedReader(new FileReader(newFile));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    custCol = new Vector();
                    while ((str1 = in.readLine()) != null) {
                        custCol.add(str1);
                    }
                    in.close();
                    for (int i = 0; i < custColInt.length; i++) {
                        str2 = (String) OpenZipFile.custColVector.elementAt(custColInt[i]);
                        custColBuff.add(str2);
                        String[] str3 = str2.split("10=");

                        int end = str3[1].indexOf(";");
                        str4 = str3[1].substring(0, end);

                        for (int j = 0; j < custCol.size(); j++) {
                            orig1 = (String) custCol.elementAt(j);
                            String[] orig2 = orig1.split("10=");
                            int end1 = orig2[1].indexOf(";");
                            orig3 = orig2[1].substring(0, end1);

                            if (orig3.equals(str4)) {
                                index = index + 1;
                                String msg = UnicodeUtils.getNativeString(orig3) + "----" + Language.getString("FORMULAR_CREATER");
                                compare.add(msg);

                            }

                        }


                    }
                }

            }
            if (OpenZipFile.custIndexList.getModel().getSize() > 0) {
                String str1, str2, str4, orig1, orig3;

                custIndInt = OpenZipFile.custIndexList.getSelectedIndices();
                totSelected = totSelected + custIndInt.length;
                BufferedReader in = null;
                custIndBuff = new Vector();
                File newFile = new File(Settings.getAbsolutepath() + "datastore/customindex.mdf");
                if (newFile.exists()) {
                    try {
                        in = new BufferedReader(new FileReader(newFile));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                custInd = new Vector();
                while ((str1 = in.readLine()) != null) {
                    custInd.add(str1);
                }
                in.close();
                for (int i = 0; i < custIndInt.length; i++) {
                    str2 = (String) OpenZipFile.custIndexVector.elementAt(custIndInt[i]);
                    custIndBuff.add(str2);
                    String[] str3 = str2.split("2=");
                    int end = str3[1].indexOf(";");
                    str4 = str3[1].substring(0, end);
                    for (int j = 0; j < custInd.size(); j++) {
                        orig1 = (String) custInd.elementAt(j);
                        String[] orig2 = orig1.split("2=");
                        int end1 = orig2[1].indexOf(";");
                        orig3 = orig2[1].substring(0, end1);

                        if (orig3.equals(str4)) {
                            index = index + 1;
                            String msg = UnicodeUtils.getNativeString(orig3) + "----" + Language.getString("CUSTOM_INDEX");
                            compare.add(msg);

                        }

                    }


                }

            }


            if (OpenZipFile.workCheckList.getModel().getSize() > 0) {
                workArray = OpenZipFile.workCheckList.getSelectedIndices();
                workArraylen = workArray.length;
                File workFolder = new File(Settings.getAbsolutepath() + "workspaces");
                if (!workFolder.exists()) {
                    workFolder.mkdir();
                }
                for (int i = 0; i < workArray.length; i++) {
                    ZipInputStream inZip = new ZipInputStream(new FileInputStream(OpenZipFile.currentFile));
                    System.out.println("File name : " + OpenZipFile.currentFile);
                    while (inZip.available() == 1) {
                        ZipEntry entry = inZip.getNextEntry();
                        System.out.println("Entry : " + entry);

                        String zipEntryName = entry.getName();
                        System.out.println("Entry name: " + zipEntryName);

                        if ((zipEntryName).equals(OpenZipFile.workVector.elementAt(workArray[i]))) {

                            int lastindex = zipEntryName.lastIndexOf("/");
                            sub = zipEntryName.substring(lastindex + 1);

                            File file = new File(Settings.getAbsolutepath() + "workspaces/" + sub);

                            OutputStream out = new FileOutputStream(file);
                            byte[] buf = new byte[1024];
                            int len;
                            while ((len = inZip.read(buf)) > 0) {
                                out.write(buf, 0, len);
                            }
                            inZip.closeEntry();
                            out.close();
                        }
                    }
                }
            }
            if (OpenZipFile.otherCheckList.getModel().getSize() > 0) {
                otherArray = OpenZipFile.otherCheckList.getSelectedIndices();
                otherArraylen = otherArray.length;
                File workFolder = new File("datastore");
                if (!workFolder.exists()) {
                    workFolder.mkdir();
                }
                for (int i = 0; i < otherArray.length; i++) {
                    ZipInputStream inZip = new ZipInputStream(new FileInputStream(OpenZipFile.currentFile));
                    while (inZip.available() == 1) {
                        ZipEntry entry = inZip.getNextEntry();
                        String zipEntryName = entry.getName();
                        if ((zipEntryName).equals(OpenZipFile.otherVector.elementAt(otherArray[i]))) {

                            int lastindex = zipEntryName.lastIndexOf("/");
                            sub = zipEntryName.substring(lastindex + 1);

                            File file = new File(Settings.getAbsolutepath() + "datastore/" + sub);

                            OutputStream out = new FileOutputStream(file);
                            byte[] buf = new byte[1024];
                            int len;
                            while ((len = inZip.read(buf)) > 0) {
                                out.write(buf, 0, len);
                            }
                            inZip.closeEntry();
                            out.close();
                        }
                    }
                }

            }
            System.out.println("Loading..... alias.....");
            if (type_merged) {
                SymbolMaster.mergeAlias();
            } else if (!type_merged) {
                SymbolMaster.loadAlias();
            }

            UpdateNotifier.setShapshotUpdated();
            if (index > 0) {
                String str = Language.getString("MSG_ASKING_TO_REPLACE_WATCHLISTS");
                String str2 = "";
                for (int j = 0; j < compare.size(); j++) {
                    str2 = "\t" + str2 + "<li>" + compare.elementAt(j) + "</li>";
                }
                str = str.replaceAll("\\[SELECTED_LIST\\]", str2);
                int i = SharedMethods.showConfirmMessage(str, JOptionPane.WARNING_MESSAGE);
                selection = i;

                if (i == JOptionPane.OK_OPTION) {
                    createCustColUpdateVectors(true);
                    createCustIndUpdateVectors(true);
                    createWatchListUpdateVectors(true);
                    //OpenZipFile.clearStaticVectors();  //done in Menues.java (actionperformed)

                } else if (i == JOptionPane.CANCEL_OPTION) {
                    createCustColUpdateVectors(false);
                    createCustIndUpdateVectors(false);
                    createWatchListUpdateVectors(false);
                    //OpenZipFile.clearStaticVectors();

                }

            } else {
                createCustColUpdateVectors(false);
                createCustIndUpdateVectors(false);
                createWatchListUpdateVectors(false);
                //OpenZipFile.clearStaticVectors();
            }


            if (selection == JOptionPane.OK_OPTION) {

                SharedMethods.showMessage(Language.getString("IMPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
                impo.dispose();
                if (watchInt != null && watchInt.length > 0) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                }
            } else if (selection == JOptionPane.CANCEL_OPTION && (!(totSelected == index))) {

                SharedMethods.showMessage(Language.getString("IMPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
                impo.dispose();
                if (newWatchVector != null && (!newWatchVector.isEmpty())) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                }
            } else if ((index == 0 && totSelected > 0) || workArraylen > 0) {
                SharedMethods.showMessage(Language.getString("IMPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
                impo.dispose();
                if (newWatchVector != null && (!newWatchVector.isEmpty())) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                }
            } else if ((index == 0 && totSelected > 0) || otherArraylen > 0) {
                SharedMethods.showMessage(Language.getString("IMPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
                impo.dispose();
                if (newWatchVector != null && (!newWatchVector.isEmpty())) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void writeToFile(boolean isEditOk) {

        try {
            File newOutFile = new File(Settings.getAbsolutepath() + "temp/watchlist.mdf");
            if (!newOutFile.exists()) {
                newOutFile.createNewFile();
            }
            BufferedWriter buffWriter = new BufferedWriter(new FileWriter(newOutFile));
            if ((!exsistingWatchVector.isEmpty()) && isEditOk) {

                Enumeration exen = exsistingWatchVector.elements();
                while (exen.hasMoreElements()) {
                    String string2 = (String) exen.nextElement().toString();
                    buffWriter.write(string2 + " \n");

                }
            }

            if (!newWatchVector.isEmpty()) {

                Enumeration nwen = newWatchVector.elements();
                while (nwen.hasMoreElements()) {
                    String string3 = (String) nwen.nextElement().toString();
                    buffWriter.write(string3 + " \n");
                }
            }

            buffWriter.close();

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NullPointerException e) {
            System.out.println("No watch Lists were saved: Watch List tab empty ");
        }


    }

    private void createWatchListUpdateVectors(boolean isEditOk) {
        String str1, str3, orig1, orig3;
        boolean state = false;
        if (watchInt != null) {
            newWatchVector = new Vector();
            exsistingWatchVector = new Vector();
            for (int i = 0; i < watchInt.length; i++) {
                str1 = (String) OpenZipFile.strvector.elementAt(watchInt[i]);
                String[] str2 = str1.split("10=");
                int end = str2[1].indexOf(";");
                str3 = str2[1].substring(0, end);
                state = false;
                for (int j = 0; j < watch.size(); j++) {

                    orig1 = (String) watch.elementAt(j);
                    String[] orig2 = orig1.split("10=");
                    int end1 = orig2[1].indexOf(";");
                    orig3 = orig2[1].substring(0, end1);

                    if (orig3.equals(str3)) {
                        exsistingWatchVector.add(str1);
                        state = true;
                    }

                }
                if (!state) {
                    newWatchVector.add(str1);
                }
            }
            writeToFile(isEditOk);
        }

    }

    private void createCustColUpdateVectors(boolean isEditOk) {
        String str1, str3, orig1, orig3;
        boolean state = false;
        if (custColInt != null) {
            newColVector = new Vector();
            exsistingColVector = new Vector();
            for (int i = 0; i < custColInt.length; i++) {
                str1 = (String) OpenZipFile.custColVector.elementAt(custColInt[i]);
                String[] str2 = str1.split("10=");
                int end = str2[1].indexOf(";");
                str3 = str2[1].substring(0, end);
                state = false;
                for (int j = 0; j < custCol.size(); j++) {
                    orig1 = (String) custCol.elementAt(j);
                    String[] orig2 = orig1.split("10=");
                    int end1 = orig2[1].indexOf(";");
                    orig3 = orig2[1].substring(0, end1);

                    if (orig3.equals(str3)) {
                        exsistingColVector.add(str1);
                        state = true;
                    }

                }

                if (!state) {
                    newColVector.add(str1);
                }
            }

            updateCustomFormulaStore(isEditOk);

        }


    }

    private void updateCustomFormulaStore(boolean isEditOk) {
        String exStr1, exFunctionString, exColumnName, newStr1, newFunctionString, newColumnName;

        try {
            if ((!exsistingColVector.isEmpty()) && isEditOk) {
                Enumeration exEnu = exsistingColVector.elements();
                while (exEnu.hasMoreElements()) {

                    exStr1 = exEnu.nextElement().toString();
                    String[] exStr2 = exStr1.split("=");
                    exFunctionString = exStr2[3];
                    int delim = exStr2[1].lastIndexOf(";");
                    exColumnName = exStr2[1].substring(0, delim);
                    CustomFormulaStore.getSharedInstance().createFile(FormularEditor.EDIT_FORMULA, exColumnName, exFunctionString, exColumnName, true, false);
                    System.out.println("Formula store Edited++++++++++++++++++++++################");

                }
            }
            if (!newColVector.isEmpty()) {
                Enumeration newenu = newColVector.elements();
                while (newenu.hasMoreElements()) {
                    newStr1 = newenu.nextElement().toString();
                    String[] newStr2 = newStr1.split("=");
                    newFunctionString = newStr2[3];
                    int delim = newStr2[1].lastIndexOf(";");
                    newColumnName = newStr2[1].substring(0, delim);
                    CustomFormulaStore.getSharedInstance().createFile(FormularEditor.NEW_FORMULA, newColumnName, newFunctionString, null, true, false);
                    System.out.println("New Formula Added++++++++++++++++++++++################");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createCustIndUpdateVectors(boolean isEditOk) {
        String str1, str3, orig1, orig3;
        boolean state = false;
        newIndVector = new Vector();
        exsistingIndVector = new Vector();
        if (custIndInt != null) {
            for (int i = 0; i < custIndInt.length; i++) {
                str1 = (String) OpenZipFile.custIndexVector.elementAt(custIndInt[i]);
                String[] str2 = str1.split("2=");
                int end = str2[1].indexOf(";");
                str3 = str2[1].substring(0, end);
                state = false;
                for (int j = 0; j < custInd.size(); j++) {
                    orig1 = (String) custInd.elementAt(j);
                    String[] orig2 = orig1.split("2=");
                    int end1 = orig2[1].indexOf(";");
                    orig3 = orig2[1].substring(0, end1);

                    if (orig3.equals(str3)) {
                        exsistingIndVector.add(str1);
                        state = true;
                    }

                }

                if (!state) {
                    newIndVector.add(str1);
                }
            }

            updateCustomIndexStore(isEditOk);
        }
    }

    private void updateCustomIndexStore(boolean isEditOK) {
        String exStr1, newStr1;

        try {
            if ((!exsistingIndVector.isEmpty()) && isEditOK) {
                Enumeration exEnu = exsistingIndVector.elements();
                while (exEnu.hasMoreElements()) {
                    exStr1 = exEnu.nextElement().toString();

                    CustomIndexStore.getSharedInstance().createFile(exStr1, CustomIndexConstants.EDIT_INDEX);
                    System.out.println("Index store Edited++++++++++++++++++++++################");

                }
            }
            if (!newIndVector.isEmpty()) {
                Enumeration nwEnu = newIndVector.elements();
                while (nwEnu.hasMoreElements()) {
                    newStr1 = nwEnu.nextElement().toString();

                    CustomIndexStore.getSharedInstance().createFile(newStr1, CustomIndexConstants.NEW_INDEX);
                    System.out.println("Index store Added++++++++++++++++++++++################");
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

