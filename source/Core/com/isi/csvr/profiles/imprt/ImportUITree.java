package com.isi.csvr.profiles.imprt;

import com.isi.csvr.Client;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Modified by Chandika
 * Date: 07-May-2007
 * Time: 13:17:52
 * To change this template use File | Settings | File Templates.
 */
public class ImportUITree extends JDialog implements ActionListener {
    public static DynamicArray watchLists;
    public static DynamicArray workSpaces;
    public static DynamicArray customColumns;
    public static DynamicArray customIndex;
    public static DynamicArray symbolAlias;
    public static boolean typeBoth = false;
    public static boolean typeMyStock = false;
    public static boolean typeClassic = false;
    JPanel jpanel2;
    JRadioButton merge;//= new TWRadioButtonMenuItem(Language.getString("IMPORT_TYPE_MERGE"));
    JRadioButton restore;//= new TWRadioButtonMenuItem(Language.getString("IMPORT_TYPE_RESTORE"));
    CheckNode g_oTreeTop;
    CheckNode watchListNode;
    CheckNode workSpacenode;
    CheckNode customColumnNode;
    CheckNode customindexNode;
    CheckNode otherNode;
    private TWButton ok, cancel;
    private JTabbedPane tabPane;
    private CheckNode myStocks;
    private CheckNode classicView;
    private CheckNode conditinalWatch;
    private CheckNode forexWatch;
    private JTree tree;


    public ImportUITree(Vector nodes) {
        super(Client.getInstance().getFrame(), Language.getString("IMPORT_USER_DATA"), true);
        g_oTreeTop = new CheckNode(Language.getString("IMPORT_EXPORT_USERDATA"));
        watchListNode = new CheckNode(Language.getString("WATCH_LISTS"));
        workSpacenode = new CheckNode(Language.getString("MENU_WORKSPACES"));
        customColumnNode = new CheckNode(Language.getString("FORMULAR_CREATER"));
        customindexNode = new CheckNode(Language.getString("CUSTOM_INDEX"));
        otherNode = new CheckNode(Language.getString("OTHER_TAB"));
        myStocks = new CheckNode(Language.getString("MY_STOCKS"));
//        classicView = new CheckNode(Language.getString("MIST_VIEW"));
        forexWatch = new CheckNode(Language.getString("FOREX_WATCH"));
        conditinalWatch = new CheckNode(Language.getString("FUNCTION_LISTS"));
        watchLists = new DynamicArray();
        workSpaces = new DynamicArray();
        customColumns = new DynamicArray();
        customIndex = new DynamicArray();
        symbolAlias = new DynamicArray();
        createUI(nodes);

    }

    public void createUI(Vector nodes) {
        Container container = getContentPane();
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");
        tabPane = new JTabbedPane();
        tabPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        tabPane.setPreferredSize(new Dimension(220, 215));
        this.setResizable(false);
        this.setPreferredSize(new Dimension(336, 320));
        this.setSize(new Dimension(336, 320));
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        this.setBackground(color1);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        String[] PanelWidths_lowerCenterPanel = {"100%"};
        String[] PanelHeights_lowerCenterPanel = {"30", "100%", "60"};
        FlexGridLayout flexGridLayout_container = new FlexGridLayout(PanelWidths_lowerCenterPanel, PanelHeights_lowerCenterPanel, 0, 0);
        container.setLayout(flexGridLayout_container);
        container.add(createMsgLabel());
        container.add(createTreePanel(createJTree(nodes)));
        container.add(createButtonPanel());
        GUISettings.applyOrientation(this);

    }

    public JLabel createMsgLabel() {
        JLabel msgLabel = new JLabel(Language.getString("IMPORT_PROFILE_MESSAGE"));
        msgLabel.setPreferredSize(new Dimension(270, 30));
        return msgLabel;
    }

    public JTree createJTree(Vector nodes) {

        for (int j = 0; j < nodes.size(); j++) {
            com.isi.csvr.profiles.imprt.NodeObject no = (com.isi.csvr.profiles.imprt.NodeObject) nodes.get(j);
            //Adding watchlist types to tree
            if (no.getParent().startsWith("wl")) {
                if (no.getParent().toString().substring(2).equalsIgnoreCase("0")) {
                    myStocks.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
                } else if (no.getParent().toString().substring(2).equalsIgnoreCase("1")) {
//                   classicView.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
                } else if (no.getParent().toString().substring(2).equalsIgnoreCase("8")) {
                    forexWatch.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
                } else if (no.getParent().toString().substring(2).equalsIgnoreCase("2")) {
                    conditinalWatch.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
                }
            } else if (no.getParent().equals("wp")) {
                workSpacenode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
            } else if (no.getParent().equals("cs")) {
                customColumnNode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
            } else if (no.getParent().equals("ci")) {
                customindexNode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));
            } else if (no.getParent().equals("ot")) {
                otherNode.add(new CheckNode(new WhatchListObject(no.getName(), 0)));

            }

        }
        tree = new JTree(g_oTreeTop);
//        g_oTreeTop.add(watchListNode);
//        g_oTreeTop.add(workSpacenode);
//        g_oTreeTop.add(customColumnNode);
//        g_oTreeTop.add(customindexNode);
//        g_oTreeTop.add(otherNode);
//        watchListNode.add(myStocks);
//        watchListNode.add(classicView);
//        watchListNode.add(forexWatch);
//        watchListNode.add(conditinalWatch);
        if (myStocks.getChildCount() > 0) {
            watchListNode.add(myStocks);
        }
//        if (classicView.getChildCount()>0) {
////            watchListNode.add(classicView);
//        }
        if (forexWatch.getChildCount() > 0) {
            watchListNode.add(forexWatch);
        }
        if (conditinalWatch.getChildCount() > 0) {
            watchListNode.add(conditinalWatch);
        }
        if (watchListNode.getChildCount() > 0) {
            g_oTreeTop.add(watchListNode);
        }
        if (workSpacenode.getChildCount() > 0) {
            g_oTreeTop.add(workSpacenode);
        }
        if (customColumnNode.getChildCount() > 0) {
            g_oTreeTop.add(customColumnNode);
        }
        if (customindexNode.getChildCount() > 0) {
            g_oTreeTop.add(customindexNode);
        }
        if (otherNode.getChildCount() > 0) {
            g_oTreeTop.add(otherNode);
        }
//         if (watchListNode.getChildCount()>0) {
//            g_oTreeTop.add(watchListNode);
//        }
        try {
            tree.setCellRenderer(new CheckRenderer());
        } catch (Exception e) {
            System.out.println("Rendere failed...");
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        tree.putClientProperty("JTree.lineStyle", "Angled");
        tree.addMouseListener(new NodeSelectionListener(tree));
//      tree.setRootVisible(false);
        tree.setShowsRootHandles(true);
        expandAll(tree);
        GUISettings.applyOrientation(tree);
        return tree;
    }

    public void expandAll(JTree tree) {
        int row = 0;
        while (row < tree.getRowCount()) {
            tree.expandRow(row);
            row++;
        }
    }

    public JPanel createTreePanel(JTree tree) {
        JPanel treePanel = new JPanel();
        JScrollPane treeScroll = new JScrollPane(tree);
        String[] PanelWidths = {"100%"};
        String[] PanelHeights = {"100%"};
        FlexGridLayout flexGridLayoutp = new FlexGridLayout(PanelWidths, PanelHeights, 0, 0);
        treePanel.setLayout(flexGridLayoutp);
        treePanel.add(treeScroll);

        return treePanel;
    }

    public JPanel createButtonPanel() {
        ok = new TWButton(Language.getString("IMPORT"));
        ok.addActionListener(this);
        cancel = new TWButton(Language.getString("BTN_CLOSE"));
        cancel.addActionListener(this);
        JPanel buutonPanel = new JPanel();
        buutonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        buutonPanel.add(ok);
        buutonPanel.add(cancel);
        return buutonPanel;
    }

    public void actionPerformed(ActionEvent ev) {
        Object button = ev.getSource();
        if (button == ok) {
            getSelectedCheckBoxes(g_oTreeTop);
            if (!isVectorsEmpty()) {
                if (symbolAlias.size() > 0) {
                    String result = getAliasImportType();
                    System.out.println("Results... " + result);
                    if ((result.equals("OK"))) {
                        if (merge.isSelected()) {
                            new OpenSelectedTree(this, SymbolMaster.ALIAS_MERGE);
                        }
                        if (restore.isSelected()) {
                            new OpenSelectedTree(this, SymbolMaster.ALIAS_REPLACE);
                        }
                    }
                    if (result.equals("CANCEL")) {
                        OpenZipFile.clearStaticVectors();
                        this.dispose();
                    }
                } else {
                    new OpenSelectedTree(this, SymbolMaster.ALIAS_MERGE);
                }
            }
        } else {
            OpenZipFile.clearStaticVectors();
            this.dispose();
        }
    }

    private String getAliasImportType() {
        final StringBuffer result = new StringBuffer();
        final StringBuffer type = new StringBuffer();
        TWButton ok = new TWButton(Language.getString("OK"));
        TWButton cancel = new TWButton(Language.getString("CANCEL"));
        Object options[] = {ok, cancel};
        JOptionPane pane = new JOptionPane();
        final boolean type_merge = true;
        merge = new JRadioButton(Language.getString("IMPORT_TYPE_MERGE"));
        restore = new JRadioButton(Language.getString("IMPORT_TYPE_RESTORE"));
        JPanel middlePanel = new JPanel();
        middlePanel.setPreferredSize(new Dimension(220, 70));
        String[] PanelWidths = {"20", "100%", "20"};
        String[] PanelHeights = {"20", "25", "25"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 0, 0);
        middlePanel.setLayout(flexGridLayout);
        ButtonGroup oGroup = new ButtonGroup();
        oGroup.add(merge);
        oGroup.add(restore);
        merge.setSelected(true);
        merge.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                type.append("MERGE");
            }
        });
        restore.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                type.append("RESTORE");
            }
        });
        middlePanel.add(new JLabel());
        middlePanel.add(new JLabel(Language.getString("IMPORT_TYPE")));
        middlePanel.add(new JLabel());
        middlePanel.add(new JLabel());
        middlePanel.add(merge);
        middlePanel.add(new JLabel());
        middlePanel.add(new JLabel());
        middlePanel.add(restore);
        middlePanel.add(new JLabel());
        pane.setMessage(middlePanel);
        pane.setOptions(options);
        pane.setMessageType(JOptionPane.INFORMATION_MESSAGE);
        final JDialog exitDialog = pane.createDialog(Client.getInstance().getFrame(), Language.getString("INFORMATION"));
        exitDialog.setModal(true);
        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result.append("OK");
                exitDialog.dispose();
            }
        });
        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result.append("CANCEL");
                exitDialog.dispose();
            }
        });
        exitDialog.setVisible(true);
        return result.toString();

    }

    public void getSelectedCheckBoxes(CheckNode root) {
        Enumeration e = root.breadthFirstEnumeration();
        while (e.hasMoreElements()) {
            CheckNode node = (CheckNode) e.nextElement();
            if (node.isSelected()) {
                TreeNode[] nodes = node.getPath();
                if (nodes[1].toString().equals(Language.getString("WATCH_LISTS"))) {
                    TreeNode tr = (TreeNode) nodes[nodes.length - 1];
                    if (tr.getParent().toString().equals(Language.getString("MIST_VIEW"))) {
                        typeClassic = true;
                    } else if (tr.getParent().toString().equals(Language.getString("MY_STOCKS"))) {
                        typeMyStock = true;
                    }

                    watchLists.add(nodes[nodes.length - 1]);
                } else if (nodes[1].toString().equals(Language.getString("MENU_WORKSPACES"))) {
                    workSpaces.add(nodes[nodes.length - 1]);
                } else if (nodes[1].toString().equals(Language.getString("FORMULAR_CREATER"))) {
                    customColumns.add(nodes[nodes.length - 1]);
                } else if (nodes[1].toString().equals(Language.getString("CUSTOM_INDEX"))) {
                    customIndex.add(nodes[nodes.length - 1]);
                } else if (nodes[1].toString().equals(Language.getString("OTHER_TAB"))) {
                    symbolAlias.add(nodes[nodes.length - 1]);
                }
            }
        }
    }

    private boolean isVectorsEmpty() {

        if (watchLists.isEmpty() && workSpaces.isEmpty() && customColumns.isEmpty() && customIndex.isEmpty() && symbolAlias.isEmpty()) {
            return true;
        } else {
            return false;
        }

    }
}


