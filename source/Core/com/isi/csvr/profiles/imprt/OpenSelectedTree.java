package com.isi.csvr.profiles.imprt;

import com.isi.csvr.SymbolMaster;
import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.customformular.FormularEditor;
import com.isi.csvr.customindex.CustomIndexConstants;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.shared.*;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import java.io.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA. User: Modified by Chandika Date: 07-May-2007 Time: 14:36:27
 */
public class OpenSelectedTree {

    Object[] watchL;
    Object[] workS;
    Object[] customC;
    Object[] customI;
    Object[] otherL;
    private String str;
    private Vector watch;
    private Vector conwatch;
    private Vector watchBuff;
    private Vector compare;
    private Vector custCol;
    private Vector custColBuff;
    private Vector newColVector;
    private Vector exsistingColVector;
    private Vector exsistingConWatchVector;
    private Vector exsistingWatchVector;
    private Vector newConWatchVector;
    private Hashtable newWatchVector;
    private Vector custInd;
    private Vector custIndBuff;
    private Vector newIndVector;
    private Vector exsistingIndVector;
    private String sub;
    private String string;
    private ImportUITree impo;

    public OpenSelectedTree(ImportUITree importUI, byte type_merged) {
        impo = importUI;
        int index = 0;

        compare = new Vector();   // This vector is filld with both watchlist names and custom column names
        try {
            int totSelected = 0;
            int selection = 1;
            int workArraylen = 0;
            int otherArraylen = 0;
            int watchArraylen = 0;
            if (ImportUITree.watchLists.size() > 0) {
                watchL = copyObjectArray(ImportUITree.watchLists, ImportUITree.watchLists.size());
                watchArraylen = watchL.length;
                totSelected = watchL.length;
                BufferedReader in = null;
                watchBuff = new Vector();
                File newFile = new File(Settings.getAbsolutepath() + "datastore/watchlist.mdf");
                if (newFile.exists()) {
                    try {
                        in = new BufferedReader(new FileReader(Settings.getAbsolutepath() + "datastore/watchlist.mdf"));
                    } catch (FileNotFoundException e) {
                        System.out.println("Reading watchlist.mdf failed.....");
                        e.printStackTrace();
                    }
                    watch = new Vector();
                    while ((str = in.readLine()) != null) {
                        watch.add(str);
                    }
                    in.close();
                    for (int i = 0; i < watchL.length; i++) {
                        try {
                            string = (String) OpenZipFileTree.strvector.get("0" + watchL[i]);
                            watchBuff.add(string);
                            String[] str1 = string.split("10=");
                            int int1 = str1[1].indexOf(";");
                            String str2 = str1[1].substring(0, int1);
                            for (int j = 0; j < watch.size(); j++) {
                                sub = (String) watch.elementAt(j);
                                String type = ((String) watch.get(j)).substring(2, 3);//string.split("0=");
                                if (type.startsWith("0")) {
                                    String[] sub1 = sub.split("10=");
                                    int int2 = sub1[1].indexOf(";");
                                    String sub2 = sub1[1].substring(0, int2);
                                    //                            System.out.println("sub2 : " + sub2 + "str2 : " + str2);
                                    if (sub2.equals(str2)) {
                                        index = index + 1;
                                        String msg = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(str2), ",") + "----" + Language.getString("WATCH_LIST");
                                        compare.add(msg);
                                    }
                                } else {
//                                        index = index + 1;
//                                        String msg = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(str2), ",") + "----" + Language.getString("MIST_VIEW");
//                                        compare.add(msg);


                                }

                            }
                        } catch (Exception e) {
//                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                    }
                    //change
                   /* for (int i = 0; i < watchL.length; i++) {
                        try {
                            string = (String) OpenZipFileTree.strvector.get("1" + watchL[i]);
                            watchBuff.add(string);
                            String[] str1 = string.split("10=");
                            int int1 = str1[1].indexOf(";");
                            String str2 = str1[1].substring(0, int1);
                            for (int j = 0; j < watch.size(); j++) {
                                sub = (String) watch.elementAt(j);
                                String type = ((String) watch.get(j)).substring(2, 3);//string.split("0=");
                                if (type.startsWith("1")) {
                                    String[] sub1 = sub.split("10=");
                                    int int2 = sub1[1].indexOf(";");
                                    String sub2 = sub1[1].substring(0, int2);
                                    //                            System.out.println("sub2 : " + sub2 + "str2 : " + str2);
                                    if (sub2.equals(str2)) {
                                        index = index + 1;
                                        String msg = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(str2), ",") + "----" + Language.getString("MIST_VIEW");
                                        compare.add(msg);
                                    }
                                } else {
//                                     index = index + 1;
//                                     String msg = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(str2), ",") + "----" + Language.getString("WATCH_LIST");
//                                     compare.add(msg);


                                }

                            }
                        } catch (Exception e) {
//                             e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                    }*/
                }
                // Dealing with conditional watchlist elements
                for (int i = 0; i < watchL.length; i++) {
                    TreeNode node = (TreeNode) watchL[i];
                    if (node.getParent().toString().equals(Language.getString("FUNCTION_LISTS"))) {
                        BufferedReader cin = null;
                        watchBuff = new Vector();
                        File conditionalWF = new File(Settings.getAbsolutepath() + "datastore/functionlist.mdf");
                        if (conditionalWF.exists()) {
                            try {
                                cin = new BufferedReader(new FileReader(Settings.getAbsolutepath() + "datastore/functionlist.mdf"));
                            } catch (FileNotFoundException e) {
                                System.out.println("Reading functionlist.mdf failed.....");
                                e.printStackTrace();
                            }
                            conwatch = new Vector();
                            while ((str = cin.readLine()) != null) {
                                conwatch.add(str);
                            }
                            in.close();
                            string = (String) OpenZipFileTree.strvector.get("2" + watchL[i]);
                            watchBuff.add(string);
                            String[] str1 = string.split("10=");
                            int int1 = str1[1].indexOf(";");
                            String str2 = str1[1].substring(0, int1);
                            for (int k = 0; k < conwatch.size(); k++) {
                                sub = (String) conwatch.elementAt(k);
                                String[] sub1 = sub.split("10=");
                                int int2 = sub1[1].indexOf(";");
                                String sub2 = sub1[1].substring(0, int2);
//                                    System.out.println("sub2 : " + sub2 + "str2 : " + str2);
                                if (sub2.equals(str2)) {
                                    index = index + 1;
                                    String msg = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(str2), ",") + "----" + Language.getString("WATCH_LIST");
                                    compare.add(msg);
                                }

                            }


                        }

                    }
//                    else if(){
//                         BufferedReader cin = null;
//                        watchBuff = new Vector();
//                        File conditionalWF = new File(Settings.getAbsolutepath()+"datastore/functionlist.mdf");
//                        if (conditionalWF.exists()) {
//                            try {
//                                cin = new BufferedReader(new FileReader(Settings.getAbsolutepath()+"datastore/functionlist.mdf"));
//                            } catch (FileNotFoundException e) {
//                                System.out.println("Reading functionlist.mdf failed.....");
//                                e.printStackTrace();
//                            }
//                            conwatch = new Vector();
//                            while ((str = cin.readLine()) != null) {
//                                conwatch.add(str);
//                            }
//                            in.close();
//                            string = (String) OpenZipFileTree.strvector.get("" + watchL[i]);
//                            watchBuff.add(string);
//                            String[] str1 = string.split("10=");
//                            int int1 = str1[1].indexOf(";");
//                            String str2 = str1[1].substring(0, int1);
//                            for (int k = 0; k < conwatch.size(); k++) {
//                                sub = (String) conwatch.elementAt(k);
//                                String[] sub1 = sub.split("10=");
//                                int int2 = sub1[1].indexOf(";");
//                                String sub2 = sub1[1].substring(0, int2);
////                                    System.out.println("sub2 : " + sub2 + "str2 : " + str2);
//                                if (sub2.equals(str2)) {
//                                    index = index + 1;
//                                    String msg = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(str2), ",") + "----" + Language.getString("WATCH_LIST");
//                                    compare.add(msg);
//                                }
//
//                            }
//
//
//                        }
//                    }
                }

            }
            if (ImportUITree.customColumns.size() > 0) {
                String str1, str2, str4, orig1, orig3;
                customC = copyObjectArray(ImportUITree.customColumns, ImportUITree.customColumns.size());
                totSelected = totSelected + customC.length;
                BufferedReader in = null;
                custColBuff = new Vector();
                File newFile = new File(Settings.getAbsolutepath() + "datastore/customformula.mdf");
                if (newFile.exists()) {
                    try {
                        in = new BufferedReader(new FileReader(newFile));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    custCol = new Vector();
                    while ((str1 = in.readLine()) != null) {
                        custCol.add(str1);
                    }
                    in.close();
                    for (int i = 0; i < customC.length; i++) {
                        str2 = (String) OpenZipFileTree.custColVector.get("" + customC[i]);
                        custColBuff.add(str2);
                        String[] str3 = str2.split("10=");
                        int end = str3[1].indexOf(";");
                        str4 = str3[1].substring(0, end);
                        for (int j = 0; j < custCol.size(); j++) {
                            orig1 = (String) custCol.elementAt(j);
                            String[] orig2 = orig1.split("10=");
                            int end1 = orig2[1].indexOf(";");
                            orig3 = orig2[1].substring(0, end1);
                            if (orig3.equals(str4)) {
                                index = index + 1;
                                String msg = UnicodeUtils.getNativeString(orig3) + "----" + Language.getString("FORMULAR_CREATER");
                                compare.add(msg);

                            }
                        }
                    }
                }

            }
            if (ImportUITree.customIndex.size() > 0) {
                String str1, str2, str4, orig1, orig3;
                customI = copyObjectArray(ImportUITree.customIndex, ImportUITree.customIndex.size());
                totSelected = totSelected + customI.length;
                BufferedReader in = null;
                custIndBuff = new Vector();
                File newFile = new File(Settings.getAbsolutepath() + "datastore/customindex.mdf");
                if (newFile.exists()) {
                    try {
                        in = new BufferedReader(new FileReader(newFile));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                custInd = new Vector();
                while ((str1 = in.readLine()) != null) {
                    custInd.add(str1);
                }
                in.close();
                for (int i = 0; i < customI.length; i++) {
                    str2 = (String) OpenZipFileTree.custIndexVector.get("" + customI[i]);
                    custIndBuff.add(str2);
                    String[] str3 = str2.split("2=");
                    int end = str3[1].indexOf(";");
                    str4 = str3[1].substring(0, end);
                    for (int j = 0; j < custInd.size(); j++) {
                        orig1 = (String) custInd.elementAt(j);
                        String[] orig2 = orig1.split("2=");
                        int end1 = orig2[1].indexOf(";");
                        orig3 = orig2[1].substring(0, end1);
                        if (orig3.equals(str4)) {
                            index = index + 1;
                            String msg = UnicodeUtils.getNativeString(orig3) + "----" + Language.getString("CUSTOM_INDEX");
                            compare.add(msg);
                        }
                    }
                }
            }

            if (ImportUITree.workSpaces.size() > 0) {
                workS = copyObjectArray(ImportUITree.workSpaces, ImportUITree.workSpaces.size());
                workArraylen = workS.length;
                File workFolder = new File(Settings.getAbsolutepath() + "workspaces");
                if (!workFolder.exists()) {
                    workFolder.mkdir();
                }
                for (int i = 0; i < workS.length; i++) {
                    ZipInputStream inZip = new ZipInputStream(new FileInputStream(OpenZipFileTree.currentFile));
                    while (inZip.available() == 1) {
                        ZipEntry entry = inZip.getNextEntry();
                        String zipEntryName = "" + entry;
                        if ((zipEntryName).equals("/workspace/" + workS[i])) {
                            int lastindex = zipEntryName.lastIndexOf("/");
                            sub = zipEntryName.substring(lastindex + 1);
                            File file = new File(Settings.getAbsolutepath() + "workspaces/" + sub);
                            OutputStream out = new FileOutputStream(file);
                            byte[] buf = new byte[1024];
                            int len;
                            while ((len = inZip.read(buf)) > 0) {
                                out.write(buf, 0, len);
                            }
                            inZip.closeEntry();
                            out.close();
                        }
                    }
                }
            }
            if (ImportUITree.symbolAlias.size() > 0) {
                otherL = copyObjectArray(ImportUITree.symbolAlias, ImportUITree.symbolAlias.size());
                otherArraylen = otherL.length;
                File workFolder = new File(Settings.getAbsolutepath() + "datastore");
                if (!workFolder.exists()) {
                    workFolder.mkdir();
                }
                for (int i = 0; i < otherL.length; i++) {
                    if (otherL[i].toString().equals(Language.getString("SYMBOL_ALIAS"))) {
                        otherL[i] = "alias.mdf";
                    }
                    ZipInputStream inZip = new ZipInputStream(new FileInputStream(OpenZipFileTree.currentFile));
                    while (inZip.available() == 1) {
                        ZipEntry entry = inZip.getNextEntry();
                        String zipEntryName = "" + entry;
                        if ((zipEntryName).equals("/datastore/" + otherL[i])) {
                            int lastindex = zipEntryName.lastIndexOf("/");
                            sub = zipEntryName.substring(lastindex + 1);
                            File file = new File(Settings.getAbsolutepath() + "datastore\\" + sub);
                            OutputStream out = new FileOutputStream(file);
                            byte[] buf = new byte[1024];
                            int len;
                            while ((len = inZip.read(buf)) > 0) {
                                out.write(buf, 0, len);
                            }
                            inZip.closeEntry();
                            out.close();
                        }
                    }
                }
            }
//            if (type_merged) {
            SymbolMaster.mergeAlias(type_merged);
//            } else if (!type_merged) {
//                SymbolMaster.loadAlias();
//            }
            UpdateNotifier.setShapshotUpdated();
            if (index > 0) {
                String str = Language.getString("MSG_ASKING_TO_REPLACE_WATCHLISTS");
                String str2 = "";
                for (int j = 0; j < compare.size(); j++) {
                    str2 = "\t" + str2 + "<li>" + compare.elementAt(j) + "</li>";
                }
                str = str.replaceAll("\\[SELECTED_LIST\\]", str2);
                int i = SharedMethods.showConfirmMessage(str, JOptionPane.WARNING_MESSAGE);
                selection = i;
                if (i == JOptionPane.OK_OPTION) {
                    createCustColUpdateVectors(true);
                    createCustIndUpdateVectors(true);
                    createWatchListUpdateVectors(true);
                    createConditionalWatchListUpdateVectors(true);
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.System, "ImportUserData");
                } else if (i == JOptionPane.CANCEL_OPTION) {
                    createCustColUpdateVectors(false);
                    createCustIndUpdateVectors(false);
                    createWatchListUpdateVectors(false);
                    createConditionalWatchListUpdateVectors(false);
                }

            } else {
                createCustColUpdateVectors(false);
                createCustIndUpdateVectors(false);
                createWatchListUpdateVectors(false);
                createConditionalWatchListUpdateVectors(false);
            }
            if (selection == JOptionPane.OK_OPTION) {
                SharedMethods.showMessage(Language.getString("IMPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
                impo.dispose();
                if (watchL != null && watchArraylen > 0) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                }
            } else if (selection == JOptionPane.CANCEL_OPTION && (!(totSelected == index))) {
                SharedMethods.showMessage(Language.getString("IMPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
                impo.dispose();
                if (newWatchVector != null && (!newWatchVector.isEmpty())) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                }
            } else if ((index == 0 && totSelected > 0) || workArraylen > 0) {
                SharedMethods.showMessage(Language.getString("IMPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
                impo.dispose();
                if ((newWatchVector != null && (!newWatchVector.isEmpty()))
                        || (newConWatchVector != null && (!newConWatchVector.isEmpty()))) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                }
            } else if ((index == 0 && totSelected > 0) || otherArraylen > 0) {
                SharedMethods.showMessage(Language.getString("IMPORT_SUCCESS_MESSAGE"), JOptionPane.INFORMATION_MESSAGE);
                impo.dispose();
                if (newWatchVector != null && (!newWatchVector.isEmpty())) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Settings.setHasImporteddata(true);
        impo.dispose();
    }

    private void writeToFile(boolean isEditOk) {

        try {
            File newOutFile = new File(Settings.getAbsolutepath() + "temp/watchlist.mdf");
            if (!newOutFile.exists()) {
                newOutFile.createNewFile();
            }
            BufferedWriter buffWriter = new BufferedWriter(new FileWriter(newOutFile));
            if ((!exsistingWatchVector.isEmpty()) && isEditOk) {
                Enumeration exen = exsistingWatchVector.elements();
                while (exen.hasMoreElements()) {
                    String string2 = (String) exen.nextElement().toString();
                    buffWriter.write(string2 + " \n");
                }
            }

            if (!newWatchVector.isEmpty()) {
                Enumeration nwen = newWatchVector.elements();
                while (nwen.hasMoreElements()) {
                    String string3 = (String) nwen.nextElement().toString();
                    buffWriter.write(string3 + " \n");
                }
            }
            buffWriter.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NullPointerException e) {
            System.out.println("No watch Lists were saved: Watch List tab empty ");
        }
    }

    private void writeToConditionalWatchListFile(boolean isEditOk) {

        try {
            File newOutFile = new File(Settings.getAbsolutepath() + "temp/functionlist.mdf");
            if (!newOutFile.exists()) {
                newOutFile.createNewFile();
            }
            BufferedWriter buffWriter = new BufferedWriter(new FileWriter(newOutFile));
            if ((!exsistingConWatchVector.isEmpty()) && isEditOk) {
                Enumeration exen = exsistingConWatchVector.elements();
                while (exen.hasMoreElements()) {
                    String string2 = (String) exen.nextElement().toString();
                    buffWriter.write(string2 + " \n");
                }
            }

            if (!newConWatchVector.isEmpty()) {
                Enumeration nwen = newConWatchVector.elements();
                while (nwen.hasMoreElements()) {
                    String string3 = (String) nwen.nextElement().toString();
                    buffWriter.write(string3 + " \n");
                }
            }
            buffWriter.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NullPointerException e) {
            System.out.println("No watch Lists were saved: Watch List tab empty ");
        }
    }

    private void createWatchListUpdateVectors(boolean isEditOk) {
        String str1, str3, orig1, orig3;
        boolean state = false;
        if (watchL != null && watch != null) {
            newWatchVector = new Hashtable();
            exsistingWatchVector = new Vector();
            if (ImportUITree.typeMyStock) {

                for (int i = 0; i < watchL.length; i++) {
                    try {
                        str1 = (String) OpenZipFileTree.strvector.get("0" + watchL[i]);
                        String[] str2 = str1.split("10=");
                        int end = str2[1].indexOf(";");
                        str3 = str2[1].substring(0, end);
                        String type = "0";
                        state = false;
                        for (int j = 0; j < watch.size(); j++) {

                            orig1 = (String) watch.elementAt(j);
                            String[] orig2 = orig1.split("10=");
                            int end1 = orig2[1].indexOf(";");
                            orig3 = orig2[1].substring(0, end1);
                            String itype = ((String) watch.get(j)).substring(2, 3);
                            if (orig3.equals(str3)) {

                                if (!(str1.startsWith("1="))) { // to avoid adding of conditional watchlist elements
                                    if (type.equals(itype)) {
                                        exsistingWatchVector.add(str1);
                                        state = true;
                                    }
                                }


                            }

                        }
                        if (!state) {
                            if (!(str1.startsWith("1="))) {// to avoid adding of conditional watchlist elements
                                newWatchVector.put(str1, str1);
                            }
                        }
                    } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
            if (ImportUITree.typeClassic) {

                for (int i = 0; i < watchL.length; i++) {
                    try {
                        str1 = (String) OpenZipFileTree.strvector.get("1" + watchL[i]);
                        String[] str2 = str1.split("10=");
                        int end = str2[1].indexOf(";");
                        str3 = str2[1].substring(0, end);
                        state = false;
                        String type = "1";
                        for (int j = 0; j < watch.size(); j++) {

                            orig1 = (String) watch.elementAt(j);
                            String[] orig2 = orig1.split("10=");
                            int end1 = orig2[1].indexOf(";");
                            orig3 = orig2[1].substring(0, end1);
                            String itype = ((String) watch.get(j)).substring(2, 3);
                            if (orig3.equals(str3)) {

                                if (!(str1.startsWith("1="))) { // to avoid adding of conditional watchlist elements
                                    if (type.equals(itype)) {
                                        exsistingWatchVector.add(str1);
                                        state = true;
                                    }
                                }


                            }

                        }
                        if (!state) {
                            if (!(str1.startsWith("1="))) {// to avoid adding of conditional watchlist elements
                                newWatchVector.put(str1, str1);
                            }
                        }
                    } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
            //Change
            writeToFile(isEditOk);
        }

    }

    private void createConditionalWatchListUpdateVectors(boolean isEditOk) {
        String str1, str3, orig1, orig3;
        boolean state = false;
        if (watchL != null && conwatch != null) {
            newConWatchVector = new Vector();
            exsistingConWatchVector = new Vector();
            for (int i = 0; i < watchL.length; i++) {
                try {
                    str1 = (String) OpenZipFileTree.strvector.get("2" + watchL[i]);
                    String[] str2 = str1.split("10=");
                    int end = str2[1].indexOf(";");
                    str3 = str2[1].substring(0, end);
                    state = false;
                    for (int j = 0; j < conwatch.size(); j++) {

                        orig1 = (String) watch.elementAt(j);
                        String[] orig2 = orig1.split("10=");
                        int end1 = orig2[1].indexOf(";");
                        orig3 = orig2[1].substring(0, end1);

                        if (orig3.equals(str3)) {
//                            exsistingConWatchVector.add(str1);
                            eliminateDuplicateAndAdd(exsistingConWatchVector, str1);
                            state = true;
                        }

                    }
                    if (!state) {
                        newConWatchVector.add(str1);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            writeToConditionalWatchListFile(isEditOk);
        }

    }

    private void eliminateDuplicateAndAdd(Vector vc, String st) {
        try {
            for (int i = 0; i < vc.size(); i++) {
                String str = (String) vc.get(i);
                boolean match = false;
                if (str.equals(str)) {
                    match = true;
                }
                if (match) {
                    vc.remove(i);
                }
            }
            vc.add(st);
        } catch (Exception e) {
            vc.add(st);
        }

    }

    private void createCustColUpdateVectors(boolean isEditOk) {
        String str1, str3, orig1, orig3;
        boolean state = false;
        if (customC != null && custCol != null) {
            newColVector = new Vector();
            exsistingColVector = new Vector();
            for (int i = 0; i < customC.length; i++) {
                str1 = (String) OpenZipFileTree.custColVector.get("" + customC[i]);
                String[] str2 = str1.split("10=");
                int end = str2[1].indexOf(";");
                str3 = str2[1].substring(0, end);
                state = false;
                for (int j = 0; j < custCol.size(); j++) {
                    orig1 = (String) custCol.elementAt(j);
                    String[] orig2 = orig1.split("10=");
                    int end1 = orig2[1].indexOf(";");
                    orig3 = orig2[1].substring(0, end1);
                    if (orig3.equals(str3)) {
                        exsistingColVector.add(str1);
                        state = true;
                    }

                }

                if (!state) {
                    newColVector.add(str1);
                }
            }

            updateCustomFormulaStore(isEditOk);

        }


    }

    private void updateCustomFormulaStore(boolean isEditOk) {
        String exStr1, exFunctionString, exColumnName, newStr1, newFunctionString, newColumnName;

        try {
            if ((!exsistingColVector.isEmpty()) && isEditOk) {
                Enumeration exEnu = exsistingColVector.elements();
                while (exEnu.hasMoreElements()) {
                    exStr1 = exEnu.nextElement().toString();
                    String[] exStr2 = exStr1.split("=");
                    exFunctionString = exStr2[3];
                    int delim = exStr2[1].lastIndexOf(";");
                    exColumnName = exStr2[1].substring(0, delim);
                    CustomFormulaStore.getSharedInstance().createFile(FormularEditor.EDIT_FORMULA, exColumnName, exFunctionString, exColumnName, true, false);

                }
            }
            if (!newColVector.isEmpty()) {
                Enumeration newenu = newColVector.elements();
                while (newenu.hasMoreElements()) {
                    newStr1 = newenu.nextElement().toString();
                    String[] newStr2 = newStr1.split("=");
                    newFunctionString = newStr2[3];
                    int delim = newStr2[1].lastIndexOf(";");
                    newColumnName = newStr2[1].substring(0, delim);
                    CustomFormulaStore.getSharedInstance().createFile(FormularEditor.NEW_FORMULA, newColumnName, newFunctionString, null, true, false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createCustIndUpdateVectors(boolean isEditOk) {
        String str1, str3, orig1, orig3;
        boolean state = false;
        newIndVector = new Vector();
        exsistingIndVector = new Vector();
        if (customI != null && custInd != null) {
            for (int i = 0; i < customI.length; i++) {
                str1 = (String) OpenZipFileTree.custIndexVector.get("" + customI[i]);
                String[] str2 = str1.split("2=");
                int end = str2[1].indexOf(";");
                str3 = str2[1].substring(0, end);
                state = false;
                for (int j = 0; j < custInd.size(); j++) {
                    orig1 = (String) custInd.elementAt(j);
                    String[] orig2 = orig1.split("2=");
                    int end1 = orig2[1].indexOf(";");
                    orig3 = orig2[1].substring(0, end1);

                    if (orig3.equals(str3)) {
                        exsistingIndVector.add(str1);
                        state = true;
                    }

                }

                if (!state) {
                    newIndVector.add(str1);
                }
            }

            updateCustomIndexStore(isEditOk);
        }
    }

    private void updateCustomIndexStore(boolean isEditOK) {
        String exStr1, newStr1;

        try {
            if ((!exsistingIndVector.isEmpty()) && isEditOK) {
                Enumeration exEnu = exsistingIndVector.elements();
                while (exEnu.hasMoreElements()) {
                    exStr1 = exEnu.nextElement().toString();
                    CustomIndexStore.getSharedInstance().createFile(exStr1, CustomIndexConstants.EDIT_INDEX);
                    System.out.println("Index store Edited++++++++++++++++++++++################");

                }
            }
            if (!newIndVector.isEmpty()) {
                Enumeration nwEnu = newIndVector.elements();
                while (nwEnu.hasMoreElements()) {
                    newStr1 = nwEnu.nextElement().toString();
                    CustomIndexStore.getSharedInstance().createFile(newStr1, CustomIndexConstants.NEW_INDEX);
                    System.out.println("Index store Added++++++++++++++++++++++################");
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Object[] copyObjectArray(DynamicArray da, int size) {
        Object[] arr = new Object[size];
        for (int i = 0; i < da.size(); i++) {
            arr[i] = da.get(i);

        }
        return arr;
    }

}
