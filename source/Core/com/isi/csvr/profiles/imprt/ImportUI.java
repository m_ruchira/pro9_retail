package com.isi.csvr.profiles.imprt;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.util.FlexGridLayout;
import com.isi.util.JCheckBoxList;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum
 * Date: Sep 6, 2005
 * Time: 6:07:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ImportUI extends JDialog implements ActionListener {
    JPanel jpanel2;
    JRadioButtonMenuItem merge = new JRadioButtonMenuItem(Language.getString("IMPORT_TYPE_MERGE"));
    JRadioButtonMenuItem restore = new JRadioButtonMenuItem(Language.getString("IMPORT_TYPE_RESTORE"));
    private TWButton ok, cancel;
    private JScrollPane workScroll, watchScroll, columnScroll, indexScroll, otherScroll;
    private JPanel jpanel, jpanel1;
    private JTabbedPane tabPane;
    private JCheckBoxList workCheckList, watchList, customColumnList;

    public ImportUI(JCheckBoxList workCheckList, JCheckBoxList watchList, JCheckBoxList customColunmList, JCheckBoxList customIndexList, JCheckBoxList otherList) {
        super(Client.getInstance().getFrame(), Language.getString("IMPORT_USER_DATA"), true);
        createUI(workCheckList, watchList, customColunmList, customIndexList, otherList);
    }

    public void createUI(JCheckBoxList workCheckList, JCheckBoxList watchList, JCheckBoxList customColunmList, JCheckBoxList customIndexList, JCheckBoxList otherList) {
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");
        tabPane = new JTabbedPane();
        tabPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        tabPane.setPreferredSize(new Dimension(220, 215));


        this.setResizable(false);
        this.setPreferredSize(new Dimension(336, 320));
        this.setSize(new Dimension(336, 320));
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        this.setBackground(color1);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                OpenZipFile.clearStaticVectors();
            }
        });
        Container container = getContentPane();
        container.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        Border eched = BorderFactory.createEmptyBorder();

        JLabel msgImport = new JLabel(Language.getString("IMPORT_PROFILE_MESSAGE"));
// msgImport.setPreferredSize(new Dimension(210,30));
//container.add(msgImport);

        jpanel = new JPanel();
// Border titled = BorderFactory.createTitledBorder(eched, Language.getString("MENU_WORKSPACES"));
//workCheckList.setBorder(titled);
        workScroll = new JScrollPane(workCheckList);
// workScroll.setPreferredSize(new Dimension(210, 175));

// tabPane.addTab(Language.getString("MENU_WORKSPACES"),workScroll);
// tabPane.setMnemonicAt(0, KeyEvent.VK_1 );
        tabPane.insertTab(Language.getString("MENU_WORKSPACES"), null, workScroll, null, Constants.IMPORT_TAB_WORKSPACE);
        tabPane.setMnemonicAt(Constants.IMPORT_TAB_WORKSPACE, KeyEvent.VK_1);

//Border title = BorderFactory.createTitledBorder(eched, Language.getString("WATCH_LISTS"));
//watchList.setBorder(title);
        watchScroll = new JScrollPane(watchList);
// watchScroll.setPreferredSize(new Dimension(210, 175));

// tabPane.addTab(Language.getString("WATCH_LISTS"),watchScroll);
// tabPane.setMnemonicAt(1, KeyEvent.VK_2 );
        tabPane.insertTab(Language.getString("WATCH_LISTS"), null, watchScroll, null, Constants.IMPORT_TAB_WATCHLIST);
        tabPane.setMnemonicAt(Constants.IMPORT_TAB_WATCHLIST, KeyEvent.VK_2);

        columnScroll = new JScrollPane(customColunmList);
//columnScroll.setPreferredSize(new Dimension(210, 175));

// tabPane.addTab(Language.getString("FORMULAR_CREATER"),columnScroll);
// tabPane.setMnemonicAt(2, KeyEvent.VK_3 );
        tabPane.insertTab(Language.getString("FORMULAR_CREATER"), null, columnScroll, null, Constants.IMPORT_TAB_CUS_COLUMN);
        tabPane.setMnemonicAt(Constants.IMPORT_TAB_CUS_COLUMN, KeyEvent.VK_3);

        indexScroll = new JScrollPane(customIndexList);
// indexScroll.setPreferredSize(new Dimension(210, 175));

// tabPane.addTab(Language.getString("CUSTOM_INDEX"),indexScroll);
// tabPane.setMnemonicAt(3, KeyEvent.VK_4);
        tabPane.insertTab(Language.getString("CUSTOM_INDEX"), null, indexScroll, null, Constants.IMPORT_TAB_CUS_INDEX);
        tabPane.setMnemonicAt(Constants.IMPORT_TAB_CUS_INDEX, KeyEvent.VK_4);

        otherScroll = new JScrollPane(otherList);
// otherScroll.setPreferredSize(new Dimension(210, 175));
// tabPane.addTab("Other", otherScroll);
// tabPane.setMnemonicAt(4, KeyEvent.VK_5);
        tabPane.insertTab(Language.getString("OTHER_TAB"), null, otherScroll, null, Constants.IMPORT_TAB_OTHERS);
        tabPane.setMnemonicAt(Constants.IMPORT_TAB_OTHERS, KeyEvent.VK_5);


        JPanel jpanel5 = new JPanel();
//jpanel5.setPreferredSize(new Dimension(260,220));
        String[] PanelWidths = {"100%"};
        String[] PanelHeights = {"100%"};
        FlexGridLayout flexGridLayoutp = new FlexGridLayout(PanelWidths, PanelHeights, 0, 0);
        jpanel5.setLayout(flexGridLayoutp);
        jpanel5.add(tabPane);
//container.add(jpanel5, BorderLayout.CENTER);
        ok = new TWButton(Language.getString("IMPORT"));
        ok.addActionListener(this);
        cancel = new TWButton(Language.getString("CLOSE"));
        cancel.addActionListener(this);

        jpanel2 = new JPanel();
        jpanel2.setLayout(new FlowLayout(FlowLayout.CENTER));
        jpanel2.add(ok);
        jpanel2.add(cancel);
        String[] PanelWidths_lowerCenterPanel = {"100%"};
        String[] PanelHeights_lowerCenterPanel = {"30", "100%", "60"};
        FlexGridLayout flexGridLayout_container = new FlexGridLayout(PanelWidths_lowerCenterPanel, PanelHeights_lowerCenterPanel, 0, 0);
        container.setLayout(flexGridLayout_container);
        container.add(msgImport);
        container.add(jpanel5);
        container.add(jpanel2);
        this.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                setSize(320, 340);
            }

        });
        GUISettings.applyOrientation(container);
    }


    public void actionPerformed(ActionEvent ev) {
        Object button = ev.getSource();
        if (button == ok) {
            if (tabPane.getSelectedIndex() == Constants.IMPORT_TAB_OTHERS) {
                String result = getAliasImportType();
                if ((result.equals("OK"))) {
                    if (merge.isSelected()) {
                        new OpenSelected(this, true);
                    }
                    if (restore.isSelected()) {
                        new OpenSelected(this, false);
                    }
                }
                if (result.equals("CANCEL")) {
                    OpenZipFile.clearStaticVectors();
                    this.dispose();
                }
            } else {
                OpenSelected sv = new OpenSelected(this, false);
            }
        } else {
            OpenZipFile.clearStaticVectors();
            this.dispose();
        }
    }

    public void disableTabs(int[] indexes) {
        if (indexes.length > 0) {
            for (int i = 0; i < indexes.length; i++) {
                try {
                    tabPane.setEnabledAt(indexes[i], false);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void setSelectedTab(int select) {
        try {
            tabPane.setSelectedIndex(select);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    private void importAlias() {

    }

    private String getAliasImportType() {
        final StringBuffer result = new StringBuffer();
        final StringBuffer type = new StringBuffer();
        TWButton ok = new TWButton(Language.getString("OK"));
        TWButton cancel = new TWButton(Language.getString("CANCEL"));
        Object options[] = {ok, cancel};
        JOptionPane pane = new JOptionPane();
        final boolean type_merge = true;

        JPanel middlePanel = new JPanel();
        middlePanel.setPreferredSize(new Dimension(220, 70));
        String[] PanelWidths = {"20", "100%", "20"};
        String[] PanelHeights = {"20", "25", "25"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 0, 0);
        middlePanel.setLayout(flexGridLayout);

        ButtonGroup oGroup = new ButtonGroup();
//        JRadioButtonMenuItem merge = new JRadioButtonMenuItem("Merge alias Table");
//        JRadioButtonMenuItem restore = new JRadioButtonMenuItem("Restore alias Table");

        oGroup.add(merge);
        oGroup.add(restore);
        merge.setSelected(true);
        merge.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {


                type.append("MERGE");

            }
        });
        restore.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                type.append("RESTORE");

            }
        });
//        if (merge.isSelected()) {
//            type.append("MERGE");
//        }
//        if (restore.isSelected()) {
//            type.append("RESTORE");
//        }


        middlePanel.add(new JLabel());
        middlePanel.add(new JLabel(Language.getString("IMPORT_TYPE")));
        middlePanel.add(new JLabel());
        middlePanel.add(new JLabel());
        middlePanel.add(merge);
        middlePanel.add(new JLabel());
        middlePanel.add(new JLabel());
        middlePanel.add(restore);
        middlePanel.add(new JLabel());
        // pane.add(middlePanel);


        pane.setMessage(middlePanel);
        pane.setOptions(options);
        pane.setMessageType(JOptionPane.INFORMATION_MESSAGE);

        final JDialog exitDialog = pane.createDialog(Client.getInstance().getFrame(), Language.getString("WARNING"));
        exitDialog.setModal(true);

        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                result.append("OK");
                exitDialog.dispose();

            }
        });

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result.append("CANCEL");
                exitDialog.dispose();
            }
        });


        exitDialog.setVisible(true);

        return result.toString();


    }

}
