/**
 * @Author Uditha Nagahawatta
 * Data store for dde records. Dde records added to a vector, once they are
 * requested by a dde client
 * @see DdeDataRecord
 */
package com.isi.csvr.dde;

import java.util.Vector;

public class DdeRecordStore {

    private Vector records;

    /**
     * Constructor
     */
    public DdeRecordStore() {
        records = new Vector(20, 20);
    }

    /**
     * Adds a new record to the data store. Before adding the recod, it is checked
     * against the store to make sure that it is a new record. If it is already there
     * it will not be added but the last value of the record is rest, so that
     * it will be ewsent to the dde client.
     *
     * @param newRecord the new dde recod
     */
    public void addRecord(DdeDataRecord newRecord) {
        // first check if the record is already there in the store
        DdeDataRecord storedRecord = null;
        boolean valueFound = false;

        for (int i = 0; i < records.size(); i++) {
            storedRecord = (DdeDataRecord) records.elementAt(i);
            if ((storedRecord.getTopic().equals(newRecord.getTopic()))
                    && (storedRecord.getColumn() == newRecord.getColumn())) {
                // if the record is already there reset the prev value so that
                // it is resend to the dde client                
                storedRecord.setLastValue("");
                storedRecord.incCount();
                valueFound = true;
                break;
            }
        }

        if (!valueFound) {
            records.addElement(newRecord);
        }
    }

    /**
     * Remove the record containing symbol and column.
     * If the particular record has more than one dde entries, the count
     * is reduced by one.
     *
     * @param topic, column
     */
    public void removeRecord(String topic, String column) {
        try {
            DdeDataRecord storedRecord = null;

            int iColumn = Integer.parseInt(column);

            for (int i = 0; i < records.size(); i++) {
                storedRecord = (DdeDataRecord) records.elementAt(i);
                if ((storedRecord.getTopic().equals(topic))
                        && (storedRecord.getColumn() == iColumn)) {
                    if (storedRecord.getCount() == 1)
                        records.remove(storedRecord);
                    else
                        storedRecord.decCount();
                    break;
                }
            }
            storedRecord = null;
            records.trimToSize();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public DdeDataRecord getRecord(String topic, String column) {
        try {
            DdeDataRecord storedRecord = null;

            int iColumn = Integer.parseInt(column);

            for (int i = 0; i < records.size(); i++) {
                storedRecord = (DdeDataRecord) records.elementAt(i);
                if ((storedRecord.getTopic().equals(topic))
                        && (storedRecord.getColumn() == iColumn)) {
                    return storedRecord;
                }
            }
            storedRecord = null;
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Return all elements in the vector as an array
     *
     * @return an array of dde records.
     */
    public Object[] getRecords() {
        return records.toArray();
    }

    /**
     * Return the size of the data store
     *
     * @return size of the vector
     */
    public int getSize() {
        return records.size();
    }
}