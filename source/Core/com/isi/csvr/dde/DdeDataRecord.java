/**
 * Contains symbol (Topic) and the column (Item) of the Dde request.
 * Also contain
 *      lastValue:  Last value of the symbol/column. this is used to
 *                  minimize DdeServer.postAdvise() calls. If the last
 *                  value is not equal to the current value postAdvise() 
 *                  will not be called.
 *      count:      Number of times a dde call has registered. 
 *                  e.g. if the same symbol/column has registered twice,
 *                  column will contain 2. Once a dde call is unregistered
 *                  this value will reduce by one.
 *
 * @Author Uditha Nagahawatta
 */
package com.isi.csvr.dde;


public class DdeDataRecord {
    public static final int TYPE_BOARD = 0;
    public static final int TYPE_DEPTH = 1;

    private String symbol;
    private String topic;
    private int column;
    // private int type = TYPE_BOARD;
    private String lastValue = "";
    private int count = 0;

    /**
     * Constructor
     *
     * @param topic (Topic), column (Item)
     */
    public DdeDataRecord(String topic, int column) {
        //setType(type);
        setSymbol(topic);
        setColumn(column);
        this.topic = topic;
        incCount();
    }

    /**
     * Return the symbol
     * @return symbol
     */
    /* public String getSymbol(){
        return symbol;
    }*/

    /**
     * Set the symbol
     */
    public void setSymbol(String topic) {
        symbol = topic.substring(2);
    }

    public String getTopic() {
        return topic;
    }

    /**
     * Return the column
     *
     * @return column
     */
    public int getColumn() {
        return column;
    }

    /**
     * Set the column
     */
    public void setColumn(int newColumn) {
        column = newColumn;
    }

    /*public int getType() {
        return type;
    }

    public void setType(String type) {
        if (type.equals("D")){
            this.type = TYPE_DEPTH;
        }else{
            this.type = TYPE_BOARD;
        }

    }*/

    /**
     * Return the Last Value
     *
     * @return lastValue
     */
    public String getLastValue() {
        return lastValue;
    }

    /**
     * Update the lastValue
     */
    public void setLastValue(String newLastValue) {
        lastValue = newLastValue;
    }

    /**
     * Return the count value
     *
     * @return count
     */
    public int getCount() {
        return count;
    }

    /**
     * Increase the cout by one
     */
    public void incCount() {
        count++;
    }

    /**
     * Decrease the count by one
     */
    public void decCount() {
        count--;
    }
}