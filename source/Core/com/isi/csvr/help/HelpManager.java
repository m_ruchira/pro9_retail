package com.isi.csvr.help;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.nanoxml.XMLElement;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Sep 24, 2009
 * Time: 12:48:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class HelpManager {
    public static HelpManager self;
    public static Properties helpProperties;
    private static ArrayList<HelpItem> helpItems;
    private static String todFileName;
    private static int tipCount;

    private HelpManager() {
        loadHelpItems();
    }

    public static HelpManager getSharedInsatance() {
        if (self == null) {
            self = new HelpManager();
        }
        return self;
    }

    public static ArrayList<HelpItem> getHelpItems() {
        return helpItems;
    }

    public static String getTodFileName() {
        return todFileName;
    }

    public static void setTodFileName(String todFileName) {
        HelpManager.todFileName = todFileName;
    }

    public static int getTipCount() {
        return tipCount;
    }

    public static void setTipCount(int tipCount) {
        HelpManager.tipCount = tipCount;
    }

    public static boolean isTodAvailable() {
        if ((todFileName == null) || (todFileName.equalsIgnoreCase(""))) {
            return false;
        }
        return true;
    }

    public static String getFileNameFromItemId(String id) {
        try {
            for (HelpItem hItem : helpItems) {
                if (hItem.getId().equalsIgnoreCase(id)) {
                    return hItem.getFileName();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return "";
    }

    public void loadHelpItems() {
        try {
            helpItems = new ArrayList<HelpItem>();

            FileInputStream in = new FileInputStream("help/" + Language.getSelectedLanguage() + "/help_config.xml");
            StringBuilder buffer = new StringBuilder();
            byte[] data = new byte[1000];
            int len = 0;
            while (len >= 0) {
                try {
                    len = in.read(data);
                } catch (IOException e) {
                }
                if (len > 0) {
                    buffer.append(new String(data, 0, len));
                }
            }
            XMLElement root = new XMLElement();
            root.parseString(buffer.toString());
            ArrayList<XMLElement> rootElements = root.getChildren();
            for (int i = 0; i < rootElements.size(); i++) {
                XMLElement rootItem = rootElements.get(i);
                if (rootItem.getName().equals("HELPITEM")) {
                    HelpItem hItem = new HelpItem();
                    hItem.setId(rootItem.getAttribute("ID"));
                    hItem.setFileName(rootItem.getAttribute("FILENAME"));
                    hItem.setMenuLabel(rootItem.getAttribute("MENULABEL"));
                    hItem.setDisplayed(rootItem.getAttribute("ISDISPLAYED").equalsIgnoreCase("1"));
                    hItem.setImagePath(rootItem.getAttribute("ICON"));
                    if (!rootItem.getAttribute("ACCID").equalsIgnoreCase("")) {
                        hItem.setAccrId(Integer.parseInt(rootItem.getAttribute("ACCID")));
                    }
                    helpItems.add(hItem);
                } else if (rootItem.getName().equals("TOD")) {
                    todFileName = rootItem.getAttribute("FILENAME");
                    tipCount = Integer.parseInt(rootItem.getAttribute("TIPCOUNT"));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
