package com.isi.csvr;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class Navigator extends JInternalFrame implements ActionListener, Themeable {
    private JTextField txtSymbol = new JTextField();

//    private static boolean disabled = false;

    public Navigator() {

        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 2, 2));
        txtSymbol.setPreferredSize(new Dimension(80, 25));

        NavigatorButton btnF2 = new NavigatorButton("F2 - " + Language.getString("TIME_AND_SALES"), KeyEvent.VK_F2, this);

        NavigatorButton btnF3 = new NavigatorButton("F3 - " + Language.getString("GRAPH"), KeyEvent.VK_F3, this);

        NavigatorButton btnF4 = new NavigatorButton("F4 - " + Language.getString("SNAP_QUOTE"), KeyEvent.VK_F4, this);

        NavigatorButton btnF5 = new NavigatorButton("F5 - " + Language.getString("MARKET_DEPTH_BY_PRICE"), KeyEvent.VK_F5, this);

        NavigatorButton btnF6 = new NavigatorButton("F8 - " + Language.getString("MULTI_DIALOG_MENU"), KeyEvent.VK_F8, this);

        NavigatorButton btnF9 = new NavigatorButton("F9 - " + Language.getString("BUY"), KeyEvent.VK_F9, this);
        btnF9.setGradientDark("BOARD_TABLE_CELL_BID_BGCOLOR1");
        btnF9.setGradientLight("BOARD_TABLE_CELL_BID_BGCOLOR2");

        NavigatorButton btnF10 = new NavigatorButton("F10 - " + Language.getString("SELL"), KeyEvent.VK_F10, this);
        btnF10.setGradientDark("BOARD_TABLE_CELL_ASK_BGCOLOR1");
        btnF10.setGradientLight("BOARD_TABLE_CELL_ASK_BGCOLOR2");

        this.getContentPane().add(txtSymbol);
        this.getContentPane().add(btnF2);
        this.getContentPane().add(btnF3);
        this.getContentPane().add(btnF4);
        this.getContentPane().add(btnF5);
        this.getContentPane().add(btnF6);
        this.getContentPane().add(btnF9);
        this.getContentPane().add(btnF10);
        setClosable(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle(Language.getString("NAVIGATOR"));
        pack();
        Theme.registerComponent(this);
    }

    public void updateUI() {
        super.updateUI();
    }

    public JTextField getKeybordListener() {
        return txtSymbol;
    }

   /* public static boolean isDisabled() {
        return disabled;
    }*/

    public String getSymbol() {
        return txtSymbol.getText().trim().toUpperCase();
    }

    /*public static void setDisabled(boolean status) {
        disabled = status;
    }*/

    public void clear() {
        txtSymbol.setText("");
    }

    public void applyTheme() {
    }

    public void setVisible(boolean status) {
        if ((this.getLocation().getX() == -1000) && (status == true)) {
            centerSymbolBarOnDesktop();
        }
        if (status == false) { // clear the symbol while hiding
            try {
                clear();
            } catch (Exception e) {
            }
        }
        super.setVisible(status);
    }

    public void actionPerformed(ActionEvent e) {
        try {
            KeyEvent k = new KeyEvent(this, 0, System.currentTimeMillis(), 0, Integer.parseInt(e.getActionCommand()), '0');
//            TWActions.getInstance().processEvent(k);
            TWActions.getInstance().actionPerformed(k);
        } catch (Exception ex) {
        }
    }

    private void centerSymbolBarOnDesktop() {
        pack();
        Dimension desktopSize = Client.getInstance().getDesktop().getSize();
        Dimension symbolBarSize = this.getSize();
        this.setLocation((desktopSize.width - symbolBarSize.width) / 2, (desktopSize.height - symbolBarSize.height) / 2);
    }

    class NavigatorButton extends TWButton {
        public NavigatorButton(String text, int key, ActionListener actionListener) {
            setText("<html><u>" + text + "</u>");
            setCursor(new Cursor(Cursor.HAND_CURSOR));
            addActionListener(actionListener);
            setActionCommand("" + key);
            setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            setContentAreaFilled(false);
        }
    }
}