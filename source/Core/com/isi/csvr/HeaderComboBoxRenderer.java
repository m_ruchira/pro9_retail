package com.isi.csvr;

import com.isi.csvr.shared.TWComboBox;
import com.isi.csvr.shared.TWComboItem;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 10, 2008
 * Time: 9:07:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class HeaderComboBoxRenderer extends TWComboBox implements TableCellRenderer {

    private Color darkColor = Color.red.darker();
    private Color lightColor = Color.red.brighter();
    private Color borderColor = Color.BLACK;
    private Color sorterColor = Color.YELLOW;

    public HeaderComboBoxRenderer(TWComboItem[] items) {

        for (int i = 0; i < items.length; i++) {
            addItem(items[i]);
        }
    }

    public Component getTableCellRendererComponent(JTable table,
                                                   Object value, boolean isSelected, boolean hasFocus, int row,
                                                   int column) {
        setSelectedItem(value);
        return this;
    }

    public void paint(Graphics g) {
        paintGradient(g, getWidth(), getHeight());
        super.paint(g);    //To change body of overridden methods use File | Settings | File Templates.
    }

    private void paintGradient(Graphics g, int width, int height) {

        int halfheight = height / 2;
        for (int i = 0; i <= halfheight; i++) {
            g.setColor(new Color((((lightColor.getRed() * (halfheight - i)) + darkColor.getRed() * i) / halfheight),
                    (((lightColor.getGreen() * (halfheight - i)) + darkColor.getGreen() * i) / halfheight),
                    (((lightColor.getBlue() * (halfheight - i)) + darkColor.getBlue() * i) / halfheight)));
            g.drawLine(0, i, width, i);
        }
        for (int i = 0; i <= halfheight; i++) {
            g.setColor(new Color((((lightColor.getRed() * i) + darkColor.getRed() * (halfheight - i)) / halfheight),
                    (((lightColor.getGreen() * i) + darkColor.getGreen() * (halfheight - i)) / halfheight),
                    (((lightColor.getBlue() * i) + darkColor.getBlue() * (halfheight - i)) / halfheight)));
            g.drawLine(0, i + halfheight, width, i + halfheight);
        }
    }
}
