package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Jun 22, 2010
 * Time: 10:33:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class SearchNavigator extends JInternalFrame implements ActionListener, Themeable, KeyListener, FocusListener {
    public static SearchNavigator self;
    private TWTextField txtSymbol = new TWTextField();
    private TWTextField txtStockID = new TWTextField();
    private JLabel stockIDlbl;
    private JLabel symbolLbl;
    private TWButton searchBtn;

    public SearchNavigator() {

        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static SearchNavigator getSharedInstance() {
        if (self == null) {
            self = new SearchNavigator();
        }
        return self;
    }

    private void jbInit() throws Exception {
        this.setSize(200, 60);
        this.setResizable(false);
        this.getContentPane().setLayout(new FlexGridLayout(new String[]{"35%", "35%", "30%"}, new String[]{"100%", "25"}, 3, 3));
        this.getContentPane().addKeyListener(this);
        txtSymbol.addFocusListener(this);
        txtStockID.addFocusListener(this);
        stockIDlbl = new JLabel(Language.getString("STOCK_ID"));
        stockIDlbl.setHorizontalAlignment(SwingConstants.CENTER);
        stockIDlbl.setFont(new Font("Arial", 1, 12));
        symbolLbl = new JLabel(Language.getString("SYMBOL"));
        symbolLbl.setFont(new Font("Arial", 1, 12));
        symbolLbl.setHorizontalAlignment(SwingConstants.CENTER);
        searchBtn = new TWButton(Language.getString("SEARCH"));
        searchBtn.addActionListener(this);
        searchBtn.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        searchBtn.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    setSearchedSymbolHilighted(Client.getInstance().getSelectedTable());
                }
            }
        });

        this.getContentPane().add(stockIDlbl);
        this.getContentPane().add(symbolLbl);
        this.getContentPane().add(new JLabel());
        this.getContentPane().add(txtStockID);
        this.getContentPane().add(txtSymbol);
        this.getContentPane().add(searchBtn);

        setResizable(true);
        setClosable(true);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setTitle(Language.getString("SEARCH_NAVI"));
        pack();
        Theme.registerComponent(this);
    }

    public void updateUI() {
        super.updateUI();
    }

    public JTextField getKeybordListener() {
        return txtSymbol;
    }


    public String getSymbol() {
        return txtSymbol.getText().trim().toUpperCase();
    }


    public void clear() {
        txtSymbol.setText("");
        txtStockID.setText("");
    }

    public void applyTheme() {
    }

    public void setVisible(boolean status) {
        if ((this.getLocation().getX() == -1000) && (status == true)) {
            centerSymbolBarOnDesktop();
        }
        if (status == false) { // clear the symbol while hiding
            try {
                clear();
                this.setLocation(-1000, -1000);
            } catch (Exception e) {
            }
        }
        this.setSize(260, 75);
        super.setVisible(status);
    }

    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource().equals(searchBtn)) {
                if (Client.getInstance().getSelectedTable().isVisible()) {
                    setSearchedSymbolHilighted(Client.getInstance().getSelectedTable());
                } else {
                    SharedMethods.showMessage(Language.getString("ERROR_WATCHLIST_NOT_SELECTED"), JOptionPane.ERROR_MESSAGE);
                }
            }


            KeyEvent k = new KeyEvent(this, 0, System.currentTimeMillis(), 0, Integer.parseInt(e.getActionCommand()), '0');
            TWActions.getInstance().actionPerformed(k);
        } catch (Exception ex) {
        }
    }

    private void centerSymbolBarOnDesktop() {
        pack();
//        Dimension desktopSize = Client.getInstance().getDesktop().getSize();
//        Dimension symbolBarSize = this.getSize();
//        this.setLocation((desktopSize.width - symbolBarSize.width) / 2, (desktopSize.height - symbolBarSize.height) / 2);

        //geting the table dimention
        Dimension tableSize = Client.getInstance().getSelectedTable().getSize();
        Point p = Client.getInstance().getSelectedTable().getLocation();//ocationOnScreen();
        Dimension searchNaviSize = this.getSize();
        this.setLocation((tableSize.width - searchNaviSize.width) / 2, (int) p.getY() + ((tableSize.height - searchNaviSize.height) / 2));


    }

    private void setSearchedSymbolHilighted(ClientTable otable) {
        Symbols symbols = otable.getSymbols();
        String[] symbolsArray;
        //if(otable.getViewSettings().getMainType() == ViewSettingsManager.FILTERED_VIEW){
        symbolsArray = symbols.getFilteredSymbols();
//        } else {
//           symbolsArray = symbols.getSymbols();
//        }

        System.out.println(symbolsArray.length);
        int index = 0;
        String stockID = txtStockID.getText().trim();
        String symbolCode = txtSymbol.getText().trim();
        Stock oStock;
        if ((!stockID.equals("")) && (!symbolCode.equals(""))) {
            for (int i = 0; i < symbolsArray.length; i++) {
                oStock = DataStore.getSharedInstance().getStockObject(symbolsArray[i]);
                index = -1;
                if ((oStock.getCompanyCode().equals(stockID.toUpperCase())) && (oStock.getSymbol().equals(symbolCode.toUpperCase()))) {
                    index = i;
                    break;
                }
            }
        } else if ((!stockID.equals("")) && (symbolCode.equals(""))) {
            for (int i = 0; i < symbolsArray.length; i++) {
                oStock = DataStore.getSharedInstance().getStockObject(symbolsArray[i]);
                index = -2;
                if ((oStock.getCompanyCode().equals(stockID.toUpperCase()))) {
                    index = i;
                    break;
                }
            }
        } else if ((stockID.equals("")) && (!symbolCode.equals(""))) {
            for (int i = 0; i < symbolsArray.length; i++) {
                oStock = DataStore.getSharedInstance().getStockObject(symbolsArray[i]);
                index = -3;
                if ((oStock.getSymbol().equals(symbolCode.toUpperCase()))) {
                    index = i;
                    break;
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_FILL_REQUIRED_ENTRIES"), JOptionPane.INFORMATION_MESSAGE);

        }
        oStock = null;
        //highlighting the searched symbol
        if (index == -1) {
            SharedMethods.showMessage(Language.getString("SEARCH_NAVI_NOT_MATCHING"), JOptionPane.INFORMATION_MESSAGE);
        } else if (index == -2) {
            SharedMethods.showMessage(Language.getString("SEARCH_NAVI_NOT_AVAILABLE"), JOptionPane.INFORMATION_MESSAGE);
        } else if (index == -3) {
            SharedMethods.showMessage(Language.getString("SEARCH_NAVI_NOT_AVAILABLE_SYMBOL"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            ListSelectionModel selectionModel = otable.getTable().getSelectionModel();
            selectionModel.setSelectionInterval(index, index);
            otable.getTable().scrollRectToVisible(new Rectangle(0, otable.getTable().getRowHeight() * (index), 100, otable.getTable().getRowHeight()));
        }


    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {

        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getKeyChar() == KeyEvent.VK_ENTER) {
            setSearchedSymbolHilighted(Client.getInstance().getSelectedTable());
        }
    }

    public void focusGained(FocusEvent e) {
        if (e.getSource() == txtStockID) {
            txtSymbol.setText("");
        } else if (e.getSource() == txtSymbol) {
            txtStockID.setText("");
        }
    }

    public void focusLost(FocusEvent e) {

    }

    class NavigatorButton extends TWButton {
        public NavigatorButton(String text, int key, ActionListener actionListener) {
            setText("<html><u>" + text + "</u>");
            setCursor(new Cursor(Cursor.HAND_CURSOR));
            addActionListener(actionListener);
            setActionCommand("" + key);
            setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            setContentAreaFilled(false);
        }
    }
}
