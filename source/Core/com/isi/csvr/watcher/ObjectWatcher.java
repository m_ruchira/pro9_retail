// Copyright (c) 2000 ISI Sri Lanka
package com.isi.csvr.watcher;

/*********************************
 * Class: ObjectWatcher
 * @author Navneet Mathur
 * Date:
 * Class for detecting memory leaks. This class provides static methods to register
 * and unregister objects as they are created and destroyed, it maintains a count of
 * the number of each type of object that has been registered and has not been
 * unregistered. On creation an object should register itself in its constructor,
 * it should then unregister itself in its finalize() method.
 *********************************/

import java.util.Enumeration;
import java.util.Hashtable;

public class ObjectWatcher {

    private static Hashtable TheObjects = new Hashtable(10);

    public synchronized static void register(Object ThisRef) {
        String ClassName = ThisRef.getClass().getName();
        int count;

        // check if an object of this type has already been registered
        Object Obj = TheObjects.get(ClassName);
        if (Obj != null) {
            Integer c = (Integer) Obj;
            count = c.intValue();
            count++;
            TheObjects.remove(ClassName);
        } else {
            count = 1;
        }

        TheObjects.put(ClassName, new Integer(count));
    }

    public synchronized static void unregister(Object ThisRef) {
        String ClassName = ThisRef.getClass().getName();
        int count;

        // check if an object of this type has already been registered
        Object Obj = TheObjects.get(ClassName);
        if (Obj != null) {
            Integer c = (Integer) Obj;
            count = c.intValue();
            count--;
            TheObjects.remove(ClassName);
            TheObjects.put(ClassName, new Integer(count));
        } else {
        }
    }

    public static String dump() {
        String ClassName;
        Integer Count;
        StringBuilder res = new StringBuilder();
        //DebugOut Debug = new DebugOut( true );

        for (Enumeration e = TheObjects.keys(); e.hasMoreElements(); ) {
            ClassName = (String) e.nextElement();
            Count = (Integer) TheObjects.get(ClassName);
            res.append(ClassName + ":  " + Count.toString() + "\n");
        }
        return res.toString();
    }
}



