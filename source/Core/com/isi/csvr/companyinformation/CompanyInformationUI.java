package com.isi.csvr.companyinformation;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Jun 12, 2009
 * Time: 4:44:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class CompanyInformationUI {

    private String profileTemplate;
    private String symbol = null;
    private Stock stock = null;

    public CompanyInformationUI(String symbol) {
        this.symbol = symbol;
        stock = DataStore.getSharedInstance().getStockObject(symbol);
        loadTemplate(Language.getLanguageTag());
        show();
    }

    public void show() {
        try {
            String page = profileTemplate;
            Client.getInstance().getBrowserFrame().setTitle(Language.getString("COMPANY_INFORMATION"));

            page = page.replace("[COMPANY]", stock.getLongDescription());                                    //DataStore.getSharedInstance().getStockObject()
            page = page.replace("[SYMBOL]", stock.getDisplaySymbol());
            page = page.replace("[EXCHANGE]", stock.getExchange());
            page = page.replace("[UNDERLYING_SYMBOL]", stock.getBaseSymbol());
            page = page.replace("[SECURITY_TYPE]", "" + stock.getInstrumentType());
            page = page.replace("[EXPIRY_DATE]", "" + stock.getExpirationDate());
            page = page.replace("[LOT_SIZE]", "" + stock.getLotSize());
            page = page.replace("[CURRENCY]", "" + stock.getCurrencyCode());
            page = page.replace("[TIME_ZONE]", "" + ExchangeStore.getSharedInstance().getExchange(stock.getExchange()).getTimeZoneID());
            Client.getInstance().getBrowserFrame().setContentType("text/html");
            Client.getInstance().getBrowserFrame().setText(page);
            Client.getInstance().getBrowserFrame().show(Constants.DOC_TYPE_COMPANY_PROFILE);
            Client.getInstance().getBrowserFrame().bringToFont();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadTemplate(String language) {
        StringBuffer buffer = new StringBuffer();
        byte[] temp = new byte[1000];
        int count = 0;

        try {
            InputStream in = new FileInputStream("./Templates/Company_Information_" + language + ".htm");
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            profileTemplate = buffer.toString();
            buffer = null;
            in = null;
            temp = null;
        } catch (IOException ex) {
            profileTemplate = "";
        }
    }

}
