package com.isi.csvr;

//import com.isi.csvr.datastore.QuoteStore;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.heatmap.HeatPanel;
import com.isi.csvr.heatmap.HeatRecord;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.theme.Theme;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class HeatInterface {

    public static final float DEFAULT_DOUBLE_VALUE = -9999;
    //------------------- Criteria List -------------------------//
    public final static byte CRITERIA_PE_RATIO = 0;
    public final static byte CRITERIA_PERC_CHANGE = 1;
    public final static byte CRITERIA_MARKET_CAP = 2;
    public final static byte CRITERIA_NUM_TRADES = 3;
    public final static byte CRITERIA_VOLUME = 4;
    public final static byte CRITERIA_BID_ASK_RATIO = 5;
    public final static byte CRITERIA_RANGE = 6;
    public final static byte CRITERIA_PCT_RANGE = 7;
    public final static byte CRITERIA_SPREAD = 8;
    public final static byte CRITERIA_PCT_SPREAD = 9;
    public final static byte CRITERIA_MONEY_FLOW = 10;
    // NOT USED IN TWt
    public final static byte CRITERIA_SNP_STAR_RATING = 11;
    public final static byte CRITERIA_PER = 12;
    public final static byte CRITERIA_DIVIDEND_YIELD = 13;
    public final static byte CRITERIA_5_YEAR_GROWTH = 14;
    public final static byte CRITERIA_BETA = 15;
    public final static byte CRITERIA_INSTITUTIONAL_HOLDING = 16;
    public final static byte CRITERIA_PERC_GAIN = 17; // not used
    public final static byte CRITERIA_SNP_RANK = 18;
    public final static int CRITERIA_COUNT = 19;
    private static String[] saCriteria = {
            //Language.getString("HEAT_MAP_CRITERIA_SNP_RANK"),
            //Language.getString("HEAT_MAP_CRITERIA_SNP_STAR_RATING"),
            Language.getString("HEAT_MAP_CRITERIA_PE_RATIO"),
            //Language.getString("HEAT_MAP_CRITERIA_DIVIDEND_YIELD"),
            //Language.getString("HEAT_MAP_CRITERIA_5_YEAR_GROWTH"),
            //Language.getString("HEAT_MAP_CRITERIA_BETA"),
            Language.getString("HEAT_MAP_CRITERIA_PERC_CHANGE"),
            //Language.getString("HEAT_MAP_CRITERIA_INSTITUTIONAL_HOLDING"),
            Language.getString("HEAT_MAP_CRITERIA_MARKET_CAP"),
            //Language.getString("HEAT_MAP_CRITERIA_PER"),
            Language.getString("HEAT_MAP_CRITERIA_NUM_TRADES"),
            Language.getString("HEAT_MAP_CRITERIA_VOLUME"),
            Language.getString("HEAT_MAP_CRITERIA_BID_ASK_RATIO"),
            Language.getString("RANGE"),
            Language.getString("PCT_RANGE"),
            Language.getString("SPREAD"),
            Language.getString("PCT_SPREAD"),
            Language.getString("MONEY_FLOW_INDEX")
            //Language.getString("HEAT_MAP_CRITERIA_PERC_GAIN")
    };
    //-----------------------------------------------------------//

//    public static String[] getWatchListSymbols() {
//        String[] saTemp = new String[DataStore.getSymbolCount()];
//        Enumeration enum = DataStore.getSymbols();
//        int count = 0;
//        while (enum.hasMoreElements()){
//            saTemp[count] = (String)enum.nextElement();
//            count++;
//            if (count>=saTemp.length) break;
//        }
//        return saTemp;
//    }

    public static String getCompanyName(String symbol) {
//        if (QuoteStore.isValidSymbol(symbol)) {
//            return DataStore.getStockObject(symbol).getLongDescription();
//        } else if (IndexStore.isValidIndex(symbol)) {
//            return IndexStore.getDescription(symbol);
//        }
        try {
            return DataStore.getSharedInstance().getCompanyName(symbol);
        } catch (Exception ex) {
            return "";
        }
    }

    public static String getSymbolFromKey(String sKey) {
        return SharedMethods.getSymbolFromKey(sKey);
    }

    public static Stock getStockObject(String sKey) {
        return DataStore.getSharedInstance().getStockObject(sKey);
    }

    public static String getSymbol(Stock stock) {
        return stock.getSymbol();
    }

    public static String getCompanyName(Stock stock) {
        if (stock.getLongDescription() != null)
            return stock.getLongDescription();
        else
            return Language.getString("NA");
    }

    public static double getLastTrade(Stock stock) {
        return stock.getLastTradeValue();
    }

    public static double getBidPrice(Stock stock) {
        return stock.getBestBidPrice();
    }

    public static double getAskPrice(Stock stock) {
        return stock.getBestAskPrice();
    }

    public static long getVolume(Stock stock) {
        return stock.getVolume();
    }

//    public static float[] getPreviousClosed(Stock stock) {
//        return stock.getPreviousClosed();
//    }

    public static double getChange(Stock stock) {
        return stock.getChange();
    }

//    public static float[] getPercentChange(Stock stock) {
//        return stock.getPercentChange();
//    }

    public static String getThemeID() {
        try {
            return Theme.getID();
        } catch (NumberFormatException ex) {
            return "0";
        }
    }

    public static boolean isContainedSymbol(String symbol) {
        return DataStore.getSharedInstance().isValidSymbol(symbol);
    }

//    public static String getColumnSetting(String sID) {
//        return TWColumnSettings.getItem(sID);
//    }

    public static String getShortDescription(String symbol) {
        return DataStore.getSharedInstance().getCompanyName(symbol);
    }

//    public static Symbols getSymbolsObject() {
//        return new Symbols(); //Meta.QUOTE);
//    }
//
//    public static Symbols getSymbolsObject(String sSymbols) {
//        return new Symbols(sSymbols, false);
//    }

    public static void removeSymbol(String sKey) {
    }

    public static void updateHeatStore(String[] sa, DynamicArray heatBuffer) {
        HeatRecord hR;
        Stock stockObj;
        double val;
        String valS;
        try {
            for (int i = 0; i < sa.length; i++) {
                double[] valArr = new double[CRITERIA_COUNT];
//                String[] symbol = sa[i].split(Constants.KEY_SEPERATOR_CHARACTER);
                String key = sa[i];
//                System.out.println("SYMBOL : "+symbol[1]);

                /*try {
                    key = SharedMethods.getKey(symbol[0], symbol[1]);
                } catch (Exception e) {
                    key = sa[i];
                }*/
                stockObj = HeatInterface.getStockObject(key);
//                if (symbol[1].equals("TGII")) {
////                    System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
//                    Stock stockObjTest = HeatInterface.getStockObject(key);
//                    float vals = (float) stockObjTest.getPercentChange();
//                    if (vals == HeatInterface.DEFAULT_DOUBLE_VALUE) {
//                        vals = 0;
//                    }
//                    System.out.println("VALUE : "+vals);
//                }
                //if ((stockObj == null) || ((stockObj.getBestBidPrice() == 0) && (stockObj.getBestAskPrice() == 0)))
                if ((stockObj == null) /*|| (stockObj.getCashFlowRatio() == -1 )*/ ||
                        ((stockObj.getBestBidPrice() == 0) && (stockObj.getBestAskPrice() == 0) && (stockObj.getLastTradeValue() == 0)))
                    continue;
                //S&P Star Rank (B+) - map to numeric
                valS = "0";//stockObj.getSPRank();
                val = getMappedValueForRank(valS);
                valArr[CRITERIA_SNP_RANK] = val;
                //S&P Star Rating (1 - 5) - numeric no decimals
                valS = "0";//stockObj.getSPStarRating();
                if (valS == null) val = HeatInterface.DEFAULT_DOUBLE_VALUE;
                else val = Integer.parseInt(valS);
                valArr[CRITERIA_SNP_STAR_RATING] = val;
                //P/E Ratio - numeric with two decimals
                valArr[CRITERIA_PE_RATIO] = 0;//stockObj.getPERatio();
                //Dividend Yield - % with two decimals
                valArr[CRITERIA_DIVIDEND_YIELD] = 0;//stockObj.getDividendYield();
                //5 year Growth - % with two decimals
                valArr[CRITERIA_5_YEAR_GROWTH] = 0;//stockObj.getFiveYearGrowthPercentage();
                //Beta  - Numeric with two decimals
                valArr[CRITERIA_BETA] = 0;//stockObj.getBeta();
                //Institutional Holding - % with two decimals
                valArr[CRITERIA_INSTITUTIONAL_HOLDING] = 0;//stockObj.getPctHeldByInstitutions();
                //percentage change
                val = (double) stockObj.getPercentChange();
                if (val == HeatInterface.DEFAULT_DOUBLE_VALUE) {
                    val = 0;
                }
                valArr[CRITERIA_PERC_CHANGE] = val;
                //Market Cap - (value in millions, two decimals)
                valArr[CRITERIA_MARKET_CAP] = (float) stockObj.getMarketCap() / 1000; // conver to million format
                //% gains - % with two decimals
                valArr[CRITERIA_PERC_GAIN] = 0f;
                //PE Ratio - two decimals
                valArr[CRITERIA_PE_RATIO] = (float) stockObj.getPER();
                //Number of Trades
                valArr[CRITERIA_NUM_TRADES] = stockObj.getNoOfTrades();
                //Number of Trades
                valArr[CRITERIA_VOLUME] = stockObj.getVolume() / 1000;
                // bid ask ratio
                valArr[CRITERIA_BID_ASK_RATIO] = (float) stockObj.getBidaskRatio();
                // range
                valArr[CRITERIA_RANGE] = (float) stockObj.getRange();
                // pct range
                valArr[CRITERIA_PCT_RANGE] = (float) stockObj.getPctRange();
                // spread
                valArr[CRITERIA_SPREAD] = (float) stockObj.getSpread();
                // pct spread
                valArr[CRITERIA_PCT_SPREAD] = (float) stockObj.getPctSpread();//Float.isNaN ((float)stockObj.getPctSpread())?0:(float)stockObj.getPctSpread();
                // money flow
                valArr[CRITERIA_MONEY_FLOW] = (float) stockObj.getCashFlowRatio();

                hR = new HeatRecord(key, valArr);
                key = null;
                heatBuffer.insert(hR);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static float getMappedValueForRank(String rank) {
        String[] sa = HeatPanel.saSPRank;
        if (rank == null) return 0;
        float result = 1;
        for (int i = 0; i < sa.length; i++) {
            if (rank.equals(sa[i])) {
                return sa.length - 1 - i;
            }
        }
        return result;
    }

    public static String getRankForMappedValue(double val) {
        String[] sa = HeatPanel.saSPRank;
        int value = (int) Math.round(val);
        if (value < 0) {
            return sa[sa.length - 1];
        } else if (value >= sa.length) {
            return sa[0];
        } else {
            return sa[sa.length - 1 - value];
        }
    }

    public static boolean isValidWindow() {
        // return Client.getInstance().isValidSystemWindow(Meta.WT_MarketMap);
        return true;
    }

    public static void setSymbol(boolean extra, String key, String caption, String instrument, boolean chained) {
        // do nothing
    }

    public static void setSymbol(boolean extra, String key, String caption, int instrument) {
        // do nothing
    }

    public static void setSymbol(boolean extra, String key) {
        // do nothing
    }

    public static boolean isSymbolLimitExceeded() {
        return true;
//        if (QuoteStore.getUnchanedSymbolCount()+effectiveSymbolCount > Settings.getMaximumSymbolCount()){
//            new ShowMessage(false,"<html><b>" + getTitle() + "</b><br> " + Language.getString("MSG_QUOTE_LIMIT_EXCEED_FOR_MARKET_MAP"),"E");
//            return false;
//        }
    }

    public static void showSummaryQuote(String symbol) {
        Client.getInstance().showDetailQuote(symbol);
    }

    public static String[] getCriteriaDescriptions() {
        return saCriteria;
    }

    public static boolean isInternationalType() {
        return false;
    }
}