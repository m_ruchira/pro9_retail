package com.isi.csvr.symbolsearch;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class SymbolTypeRecord {

    private String desciption;
    private int type;

    public SymbolTypeRecord(String description, int type) {
        this.desciption = description;
        this.type = type;
    }

    public String getDesciption() {
        return desciption;
    }

    public int getType() {
        return type;
    }

    public String toString() {
        return desciption;
    }

}