package com.isi.csvr.symbolsearch;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.UnicodeUtils;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class SymbolSearchListItem {

    private String exchange = null;
    private String symbol = null;
    private String originalSymbol = null;
    private String key = null;
    private String description = null;
    private String shortDescription = null;
    private String record;
    private int instrumentType;

    public SymbolSearchListItem(String exchange, String symbol, String description, String instrumentType, String record, String shortDesc, String symbolWithMarket) {
        this.symbol = symbolWithMarket;
        this.exchange = exchange;
        this.description = description;
        this.instrumentType = Integer.parseInt(instrumentType);
        this.record = record;
        this.shortDescription = shortDesc;
        this.originalSymbol = symbol;
        key = SharedMethods.getKey(exchange, symbol, this.instrumentType);

        this.description = this.description.replaceAll("\\\\U", "\\\\u");
        this.description = UnicodeUtils.getNativeString(this.description);
        this.description = Language.getLanguageSpecificString(this.description, "|");

        this.shortDescription = this.shortDescription.replaceAll("\\\\U", "\\\\u");
        this.shortDescription = UnicodeUtils.getNativeString(this.shortDescription);
        this.shortDescription = Language.getLanguageSpecificString(this.shortDescription, "|");
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginalSymbol() {
        return originalSymbol;
    }

    public String getExchange() {
        return exchange;
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getRecord() {
        return record;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String toString() {
        //if (description == null)
        return originalSymbol;
        //else
        //	return description;
    }

}