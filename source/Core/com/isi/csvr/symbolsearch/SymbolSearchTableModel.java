package com.isi.csvr.symbolsearch;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Vector;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class SymbolSearchTableModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private Vector g_oDataStore;
    private String NA;

    /**
     * Constructor
     */
    public SymbolSearchTableModel() {
        NA = Language.getString("NA");
    }


    protected void finalize() throws Throwable {
        g_oDataStore = null;
    }

    public Vector getDataStore() {
        return g_oDataStore;
    }

    public void setDataStore(Vector oDataStore) {
        g_oDataStore = oDataStore;
    }

    public void setSymbol(String symbol) {

    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if (g_oDataStore != null)
            return g_oDataStore.size();
        else
            return 0;
    }

    public Object getValueAt(int iRow, int iCol) {
        SymbolSearchListItem record = (SymbolSearchListItem) g_oDataStore.elementAt(iRow);

        switch (iCol) {
            case -1:
                return record.getKey();
            case 0:
                return record.getSymbol();
            case 1:
                return record.getDescription();
            case 2:
                return getIndtrumentType(record.getInstrumentType());
            case 3:
//                return record.getExchange();
                try {
                    return ExchangeStore.getSharedInstance().getExchange(record.getExchange().trim()).getDisplayExchange(); // To display exchange
                } catch (Exception e) {
                    return record.getExchange();  //To change body of catch statement use File | Settings | File Templates.
                }
            case 4:
                return record.getShortDescription();
            case 5:
                try {
                    String marketID = DataStore.getSharedInstance().getStockObject(record.getKey()).getMarketID();
//                    if(marketID!=null){
                    return ExchangeStore.getSharedInstance().getMarket(record.getExchange(), marketID).getDescription();
//                    } else{
//                        return NA;
//                    }
                } catch (Exception e) {
                    return NA;
                }

        }
        return "";
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (iCol) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return String.class;
            default:
                return Object.class;

        }
//        try {
//            return getValueAt(0, iCol).getClass();
//        } catch (Exception ex) {
//            return Object.class;
//            //return Number.class;
//        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    private String getIndtrumentType(int type) {
        switch (type) {
            case Meta.INSTRUMENT_BOND:
                return Language.getString("INSTRUMENT_BOND");
            case Meta.INSTRUMENT_CD:
                return Language.getString("INSTRUMENT_CD");
            case Meta.INSTRUMENT_COMMON_STOCK:
                return Language.getString("INSTRUMENT_COMMON_STOCK");
            case Meta.INSTRUMENT_CONVERTIBLE_BOND:
                return Language.getString("INSTRUMENT_CONVERTIBLE_BOND");
            case Meta.INSTRUMENT_CORP_BOND:
                return Language.getString("INSTRUMENT_CORP_BOND");
            case Meta.INSTRUMENT_EQUITY:
//                return Language.getString("INSTRUMENT_QUOTE");
                return Language.getString("INSTRUMENT_EQUITY");   //to make a common type
            case Meta.INSTRUMENT_EXCHANGE_TRADED_FUNDS:
                return Language.getString("INSTRUMENT_EXCHANGE_TRADED_FUNDS");
            case Meta.INSTRUMENT_FIXED_INCOME:
                return Language.getString("INSTRUMENT_FIXED_INCOME");
            case Meta.INSTRUMENT_FOREX:
                return Language.getString("INSTRUMENT_FOREX");
            case Meta.INSTRUMENT_FOREX_DEPOSIT:
                return Language.getString("INSTRUMENT_FOREX_DEPOSIT");
            case Meta.INSTRUMENT_FOREX_FORWARD:
                return Language.getString("INSTRUMENT_FOREX_FORWARD");
            case Meta.INSTRUMENT_FOREX_FRA:
                return Language.getString("INSTRUMENT_FOREX_FRA");
            case Meta.INSTRUMENT_FOREX_STATISTICS:
                return Language.getString("INSTRUMENT_FOREX_STATISTICS");
            case Meta.INSTRUMENT_FUTURE:
                return Language.getString("INSTRUMENT_FUTURES");
            case Meta.INSTRUMENT_GOV_BOND:
                return Language.getString("INSTRUMENT_GOV_BOND");
            case Meta.INSTRUMENT_INDEX:
                return Language.getString("INSTRUMENT_INDEX");
            case Meta.INSTRUMENT_MBS:
                return Language.getString("INSTRUMENT_MBS");
            case Meta.INSTRUMENT_MONEY_MARKET:
                return Language.getString("INSTRUMENT_MONEY_MARKET");
            case Meta.INSTRUMENT_MUTUALFUND:
                return Language.getString("INSTRUMENT_MUTUALFUND");
            case Meta.INSTRUMENT_OPTION:
                return Language.getString("INSTRUMENT_OPTIONS");
            case Meta.INSTRUMENT_PREFERRED_STOCK:
                return Language.getString("INSTRUMENT_PREFERRED_STOCK");
            case Meta.INSTRUMENT_PREMIUM:
                return Language.getString("INSTRUMENT_PREMIUM");
//            case Meta.INSTRUMENT_QUOTE:
//                return Language.getString("INSTRUMENT_QUOTE");
            case Meta.INSTRUMENT_RIGHT:
                return Language.getString("INSTRUMENT_RIGHT");
            case Meta.INSTRUMENT_TRUST:
                return Language.getString("INSTRUMENT_TRUST");
            case Meta.INSTRUMENT_US_AGENCY_BOND:
                return Language.getString("INSTRUMENT_US_AGENCY_BOND");
            case Meta.INSTRUMENT_US_TREASURY_BILL:
                return Language.getString("INSTRUMENT_US_TREASURY_BILL");
            case Meta.INSTRUMENT_US_TREASURY_COUPON:
                return Language.getString("INSTRUMENT_US_TREASURY_COUPON");
            case Meta.INSTRUMENT_WARRANT:
                return Language.getString("INSTRUMENT_WARRANT");
            case Meta.INSTRUMENT_WARRANT_RIGHT:
                return Language.getString("INSTRUMENT_WARRANT_RIGHT");
        }
        return Language.getString("LBL_NA");
    }

}
