package com.isi.csvr.stockreport;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author unascribed
 * @version 1.0
 */


import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Vector;

public class StockReportModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private Vector g_oDataStore;

    /**
     * Constructor
     */
    public StockReportModel() {
    }


    protected void finalize() throws Throwable {
        g_oDataStore = null;
    }

    public Vector getDataStore() {
        return g_oDataStore;
    }

    public void setDataStore(Vector oDataStore) {
        g_oDataStore = oDataStore;
    }

	/* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if (g_oDataStore != null)
            return g_oDataStore.size();
        else
            return 0;
    }

    public Object getValueAt(int iRow, int iCol) {
        StockReportRecord record = null;
        try {
            record = (StockReportRecord) g_oDataStore.elementAt(iRow);

            switch (iCol) {
                case 0:
                    return record.getDescription();
                case 1:
                    return record.getId();
            }
            record = null;
            return "";
        } catch (Exception ex) {
            record = null;
            return "";
        }

    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        try {
            return getValueAt(0, iCol).getClass();
        } catch (Exception ex) {
            return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */


    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}

