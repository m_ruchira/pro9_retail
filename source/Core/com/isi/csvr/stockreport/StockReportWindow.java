package com.isi.csvr.stockreport;

import com.isi.csvr.Client;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.downloader.ContentListener;
import com.isi.csvr.downloader.ContentListenerStore;
import com.isi.csvr.downloader.ZipFileDownloader;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;
import java.awt.*;
import java.io.*;
import java.util.zip.ZipInputStream;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class StockReportWindow extends JEditorPane implements HyperlinkListener, ContentListener {
    public static String exchange = "";
    private static JEditorPane self;
    private static boolean busy = false;
    private static String template = null;
    private BorderLayout borderLayout1 = new BorderLayout();
    private WorkInProgressIndicator loading = null;

    public StockReportWindow() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static String getCompanyName(String data) {
        String[] reportData = data.split(Meta.FD);
        String companyName = null;
        try {
            companyName = DataStore.getSharedInstance().getCompanyName(SharedMethods.getKey(reportData[0], reportData[1], Integer.parseInt(reportData[3])));
        } catch (Exception e) {
        }
        if ((companyName != null) && (!companyName.equals(""))) {
            return companyName;
        } else {
            if ((reportData[2] != null) && (!reportData[2].equalsIgnoreCase("null"))) {
                return Language.getLanguageSpecificString(reportData[1], "|");
            } else {
                return "";
            }
        }
    }

    private static String getTable(String data) {

        String[] reportData = data.split(Meta.FD);
        StringBuffer buffer = new StringBuffer();
        exchange = reportData[0];
        boolean dataFound = false;
        if (data.length() == 0) {
            buffer.append("<table>");
            buffer.append("<tr><td>");
            buffer.append("Loading ...");
            buffer.append("</tr></td>");
            return buffer.toString();
        } else {
            try {
                String company = reportData[2];
                buffer.append("<table>");

                for (int i = 2; i < reportData.length; i++) {
                    String[] fields = reportData[i].split(Meta.ID);
                    buffer.append("<tr>");
                    buffer.append("<td style=\"font-size: 12pt; color: #12439D; font-family: Arial;\">");
                    buffer.append(fields[0]);
                    buffer.append("</td>");
                    if (!fields[1].equals("0")) { // thre is valida data for pdf
                        buffer.append("<td style=\"font-size: 12pt; color: #12439D; font-family: Arial;\">");
                        buffer.append("&nbsp;<a href='http://");
                        buffer.append(fields[1]);
                        buffer.append("-PDF'>");
                        buffer.append("PDF");
                        buffer.append("</a>");
                    } else {
                        buffer.append("<td>&nbsp;");
                    }
                    buffer.append("</td>");
                    if ((fields.length == 3) && (!fields[2].equals("*"))) { // thre is valida data for html
                        buffer.append("<td style=\"font-size: 12pt; color: #12439D; font-family: Arial;\">");
                        buffer.append("&nbsp;&nbsp;<a href='");
                        buffer.append(fields[2]);
                        buffer.append("-HTML'>");
                        buffer.append("HTML");
                        buffer.append("</a>");
                    } else {
                        buffer.append("<td>&nbsp;");
                    }
                    buffer.append("</td>");
                    buffer.append("</tr>");
                    //record = null;
                    fields = null;
                    dataFound = true;
                }
                if (!dataFound) {
                    buffer.append("<tr>");
                    buffer.append("<td style=\"font-size: 12pt; color: #12439D; font-family: Arial;\">");
                    buffer.append(Language.getString("STOCK_REPORTS_NOT_AVAILABLE"));
                    buffer.append("</td></tr>");
                }
                buffer.append("</table>");
                reportData = null;

//				SharedMethods.printLine(buffer.toString());
                return buffer.toString();
            } catch (Exception ex) {
                buffer.append("<html><table>");
                buffer.append("<tr><td>");
                buffer.append("Error loading data");
                buffer.append("</tr></td>");
                return buffer.toString();
            }
        }
        //self.updateUI();
        //buffer = null;
    }

    public static void showLoading() {
        self.setText("<html><br><center>Loading...</center>");
    }

    public static void setData(String data) {
//        String test="NSDQ\u0019DELL\u0019DELL INC. \u001961\u00191\u0019Industry Outlook  - 19/07/2003\u001E2564\u001Ehttp://www.uniquotes.com/htmlreports/io/0/filestream.aspx?STYPE=HTML&ID=2564\u0019Stock Report\t  - 19/07/2003\u001E12682\u001Ehttp://www.uniquotes.com/htmlreports/SR/0/filestream.aspx?STYPE=HTML&ID=12682\u0019Stock Report News  - 22/07/2003\u001E9984\u001Ehttp://www.uniquotes.com/htmlreports/SRN/0/filestream.aspx?STYPE=HTML&ID=9984\u0019Wall Street Consensus  - 19/07/2003\u001E15249\u001Ehttp://www.uniquotes.com/htmlreports/WSC/0/filestream.aspx?STYPE=HTML&ID=15249\u0019";
        StringBuffer buffer = new StringBuffer();
        String updateableTemplate = "";
        ;
        byte[] temp = new byte[1000];
        int count = 0;
        if (data.length() < 10) {
            SharedMethods.showConfirmMessage(Language.getString("STOCK_REPORTS_NOT_AVAILABLE"), JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION);
            Client.getInstance().closeStockReportWindow();
            return;
        }
        try {
            if (template == null) {
                InputStream in = new FileInputStream(".\\Templates\\StockReports_" + Language.getSelectedLanguage() + ".html");
                while (true) {
                    count = in.read(temp);
                    if (count == -1) break;
                    if (count > 0) {
                        String str = new String(temp, 0, count);
                        buffer.append(str);
                        str = null;
                    }
                }
                template = buffer.toString();
                in.close();
                in = null;
            }
            updateableTemplate = template;
            data = data.replaceAll("\\\\U", "\\\\u");
            data = UnicodeUtils.getNativeString(data);
            updateableTemplate = updateableTemplate.replaceFirst("\\[LOGO\\]", getImage());
            if (!data.equals("")) {
                updateableTemplate = updateableTemplate.replaceFirst("\\[REPORTS\\]", getTable(data));
                updateableTemplate = updateableTemplate.replaceFirst("\\[COMPANY\\]", getCompanyName(data));    //getCompanyName(data)
            } else {
                updateableTemplate = updateableTemplate.replaceFirst("\\[COMPANY\\]", "Please wait.");
                updateableTemplate = updateableTemplate.replaceFirst("\\[REPORTS\\]", "Loading...");
            }
            buffer = null;
            temp = null;
        } catch (Exception ex) {
            ex.printStackTrace();
            updateableTemplate = "";
        }
        self.setText(updateableTemplate);
        updateableTemplate = null;
        Client.getInstance().getStockReportFrame().show();
//            }
//        }

        //System.out.println(template);
    }

    private static String getImage() {
        //buffer.append("<tr><td>");
        StringBuffer buffer = new StringBuffer();
        //buffer.append("<img src='file://");
        buffer.append(System.getProperties().get("user.dir"));
        buffer.append("\\images\\Theme");
        buffer.append(Theme.getID() + "\\stockreportsLogo.gif");
        return buffer.toString().replaceAll("\\\\", "/");
        //return "F:\\Projects\\Java\\TW\\International\\Classes\\images\\Theme0\\StockreportLogo.gif".replaceAll("\\\\","/");
        //buffer.append("</tr></td>");
    }

    private static synchronized boolean isBusy() {
        return busy;
    }

    public static synchronized void setBusy(boolean status) {
        busy = status;
    }

    void jbInit() throws Exception {
        this.setLayout(borderLayout1);
        self = this;
        this.setContentType("text/html");
        this.setEditable(false);
        this.addHyperlinkListener(this);
        ContentListenerStore.getSharedInstance().addContentListener(this);
    }

    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (!Settings.isConnected()) return; // ignore if not connected

        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            JEditorPane pane = (JEditorPane) e.getSource();
            if (e instanceof HTMLFrameHyperlinkEvent) {
            } else {
                if (!isBusy()) {
                    String url = e.getURL().toString();
                    if (url.endsWith("-PDF")) {
                        loading = new WorkInProgressIndicator(WorkInProgressIndicator.DOWNLOADING_NONBLOKING);
                        loading.setVisible(true);
                        requestData(url.substring(0, url.length() - 4));
                    } else if (url.endsWith("-HTML")) {
                        try {
                            Client.getInstance().getBrowserFrame().setTitle(Language.getString("WINDOW_TITLE_STOCK_REPORT"));
                            Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_COROPARTE_ACTIONS);
                            Client.getInstance().getBrowserFrame().setURL(url.substring(0, url.length() - 5) +
                                    "&dm=3&TW=1&uid=0&sid=" + Settings.getSessionID() + "&ln=" + Settings.getUserName());
//                            Runtime.getRuntime().exec("START " + url.substring(0,url.length()-5) +
//                                    "&dm=3&TW=1&uid=0&sid="+Settings.getSessionID()+"&ln="+Settings.getUserName());
                        } catch (Exception e1) {
                            e1.printStackTrace();  //To change body of catch statement use Options | File Templates.
                        }
                    } else {
                        loading = new WorkInProgressIndicator(WorkInProgressIndicator.DOWNLOADING_NONBLOKING);
                        loading.setVisible(true);
                        requestData(url.substring(0, url.length()));
                    }
                    //requestData(e.getURL().toString());
//				    SharedMethods.printLine("requesting");
                }
                try {
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }
    }

    private void requestData(final String url) {
        System.out.println("URL TEST : " + url);
        new Thread() {

            public void run() {
                if (url.indexOf("link#") >= 0) {
                    try {
                        Runtime.getRuntime().exec("START " + url.replaceFirst("link#", ""));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    setBusy(true);
                    requestPDF(url.substring(7));
                }
                if (loading != null) {
                    loading.dispose();
                }
            }
        }.start();
    }

    private void requestPDF(String id) {
        Client.getInstance().setStockReportDownloading(true);
//	    SendQFactory.addContentData(Constants.PATH_PRIMARY, Meta.STOCK_REPORT_BODY +Meta.DS+exchange+ Meta.FD + id  + Meta.EOL, exchange);
        try {
            ZipFileDownloader fileDownloader = null;
            File[] files = null;
            fileDownloader = new ZipFileDownloader(Meta.STOCK_REPORT_BODY + Meta.DS + exchange + Meta.FD + id + Meta.EOL, exchange, id);
            files = fileDownloader.downloadFiles();
            fileDownloader = null;

            for (int i = 0; i < files.length; i++) {
                if (files[i].length() > 0) {
                    files[i].length();
                    extractFile(files[i]);
                    //AnnouncementStore.getSharedInstance().applyAnnouncementBulbs(item.exchange);
                }
                try {
                    files[i].delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void extractFile(File file) {

//        synchronized (Constants.FILE_EXTRACT_LOCK) {
        String fileName = "" + System.currentTimeMillis();
        try {
            int rs = Meta.RS.charAt(0);
            StringBuilder sFrame = new StringBuilder();
            ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
            zIn.getNextEntry();
            int iValue;

            while ((zIn.available() == 1)) {
                File files = new File(".\\temp\\" + fileName + ".pdf");
                OutputStream out = new FileOutputStream(files);
                byte[] buf = new byte[1024];
                int len;
                while ((len = zIn.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                zIn.closeEntry();
                out.close();
                zIn.close();
                zIn = null;

            }
//                Client.getInstance().savePDF(fileName);
//                while (true) {
//                    iValue = zIn.read();
//                    if (iValue == -1) break; // end of streeam
//                    if (iValue != rs) {
//                        sFrame.append((char) iValue);
//                        continue;
//                    }
//                }
//                   if ((sFrame != null) && (sFrame.length() > 0)) {
//                         FileOutputStream out = null;
//                          String fileName = "" + System.currentTimeMillis();
//                                try {
//                                    File f = new File(".\\temp\\" + fileName + ".pdf");
//                                    out = new FileOutputStream(f);
//                                    f.deleteOnExit();
//                                    f = null;
//                                }
//                                catch (Exception ex) {
//                                    ex.printStackTrace();
//                                }
////                                SharedMethods.printLine("pdf received");
//                                try {
//                                    out.write((sFrame.toString()).getBytes());
//                                    out.flush();
//                                    out.close();
//                                }
//                                catch (Exception ex) {
//                                    ex.printStackTrace();
//                                }
//                                Client.getInstance().savePDF(fileName);
////                                break;
//
//                    }
//                zIn.closeEntry();
//                zIn.close();
//                zIn = null;
        } catch (Exception e) {
            Client.getInstance().savePDF(fileName);
        }

        Runtime.getRuntime().gc();
    }

    public void contentDownloaded(boolean status, int type) {
        if ((type == Meta.STOCK_REPORT_SEARCH) && (!status) && Client.getInstance().getStockReportFrame().isVisible()) {
            SharedMethods.showConfirmMessage(Language.getString("STOCK_REPORTS_NOT_AVAILABLE"), JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION);
            Client.getInstance().closeStockReportWindow();
        }
    }
}
