package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.SymbolFilter;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Oct 31, 2005
 * Time: 4:24:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class SectorFilter implements SymbolFilter {
    private boolean isSector = false, isMarket = false, isCurrency = false, isAssest = false;
    private ArrayList<String> marArray;
    private ArrayList<String> secArray;
    private ArrayList<String> currArray;
    private ArrayList<String> assArray;

    public void setFilterCriteria(ArrayList markets, ArrayList sectors, ArrayList assests, ArrayList currencies) {
        isMarket = markets != null;
        this.marArray = markets;
        isSector = sectors != null;
        this.secArray = sectors;
        isAssest = assests != null;
        this.assArray = assests;
        isCurrency = currencies != null;
        this.currArray = currencies;
    }

    public boolean isFilterAvailable() {
        return (isSector || isMarket || isCurrency || isAssest);
    }

    public ArrayList<String> getFilterCriteriaMarkets() {
        return marArray;
    }

    public ArrayList<String> getFilterCriteriaSectors() {
        return secArray;
    }

    public ArrayList<String> getFilterCriteriaAssets() {
        return assArray;
    }

    public ArrayList<String> getFilterCriteriaCurrencies() {
        return currArray;
    }

    public String[] getFilteredList(String[] str) {
        ArrayList<String> returnString = new ArrayList<String>();
        Stock stock = null;
        if (isMarket) {
            for (int i = 0; i < str.length; i++) {
                stock = DataStore.getSharedInstance().getStockObject(str[i]);
                if (stock != null) {
                    if ((stock.getMarketID() != null) && (!stock.getMarketID().equals(Constants.DEFAULT_MARKET))) {
                        if ((marArray.contains(stock.getMarketID())))
                            returnString.add(str[i]);
                    }
                }
                stock = null;
            }
            str = returnString.toArray(new String[0]);
            returnString.clear();
        }
        if (isSector) {
            for (int i = 0; i < str.length; i++) {
                stock = DataStore.getSharedInstance().getStockObject(str[i]);
                if (stock != null) {
                    String sec = stock.getSectorCode();
                    //  if(sec.equals(Constants.DEFAULT_SECTOR))    // Filter issue removed by Nishantha

//                    if ((secArray.contains(sec))&&(sec != null)&&(!sec.equals(Constants.DEFAULT_SECTOR))) {
                    if ((secArray.contains(sec)) && (sec != null)) { //	SAUMUBMKT-1727 - Symbols of Unclassified sectors are loaded initially but excluded in the filter apply (Bond symbol issue)
                        returnString.add(str[i]);
                    }
                }
                stock = null;
            }
            str = returnString.toArray(new String[0]);
            returnString.clear();
        }
        if (isAssest) {
            for (int i = 0; i < str.length; i++) {
                stock = DataStore.getSharedInstance().getStockObject(str[i]);
                if (stock != null) {
                    String ass = "" + SharedMethods.getSymbolType(stock.getInstrumentType());
                    if (assArray.contains(ass)) {
                        returnString.add(str[i]);
                    }
                }
                stock = null;
            }
            str = returnString.toArray(new String[0]);
            returnString.clear();
        }
        if (isCurrency) {
            for (int i = 0; i < str.length; i++) {
                stock = DataStore.getSharedInstance().getStockObject(str[i]);
                if (stock != null) {
                    String curr = stock.getCurrencyCode();
                    if ((currArray.contains(curr)) && (curr != null) && (!curr.equals(Constants.DEFAULT_CURRENCY_CODE))) {
                        returnString.add(str[i]);
                    }
                }
                stock = null;
            }
            str = returnString.toArray(new String[0]);
            returnString.clear();
        }

        return str;
    }
}
