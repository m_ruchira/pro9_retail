package com.isi.csvr.win32;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 26, 2006
 * Time: 1:58:17 PM
 */

import javax.swing.*;
import java.awt.*;

public class SortArrow implements Icon {
    public static final int UP = 2;
    public static final int DOWN = 1;
    public static final int NONE = 0;

    private Color color;
    private int size = 5;
    private int direction;


    public SortArrow(int direction) {
        this.direction = direction;
    }


    public void setColor(Color color) {
        this.color = color;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    /**
     * Paint the icon object. This draws the icon for the selected
     * direction
     */
    public void paintIcon(Component c, Graphics g, int x, int y) {
        switch (direction) {
            case UP:
                drawDownArrow(g, x, y);
                break;
            case DOWN:
                drawUpArrow(g, x, y);
                break;
        }
    }

    /**
     * Returns the width of the icon
     */
    public int getIconWidth() {
        if (direction == NONE) {
            return 0;
        } else {
            return size;
        }
    }

    /**
     * Returns the height of the icon
     */
    public int getIconHeight() {
        if (direction == NONE) {
            return 0;
        } else {
            return size;
        }
    }

    private void drawDownArrow(Graphics g, int xo, int yo) {
        g.setColor(color);
        g.drawLine(xo, yo, xo + size, yo);
        g.drawLine(xo, yo + 1, xo + size, yo + 1);
        g.drawLine(xo + 1, yo + 2, xo + size - 1, yo + 2);
        g.drawLine(xo + 1, yo + 3, xo + size - 1, yo + 3);
        g.drawLine(xo + 2, yo + 4, xo + size - 2, yo + 4);
    }

    private void drawUpArrow(Graphics g, int xo, int yo) {
        g.setColor(color);
        g.drawLine(xo, yo + 4, xo + size, yo + 4);
        g.drawLine(xo, yo + 3, xo + size, yo + 3);
        g.drawLine(xo + 1, yo + 2, xo + size - 1, yo + 2);
        g.drawLine(xo + 1, yo + 1, xo + size - 1, yo + 1);
        g.drawLine(xo + 2, yo, xo + size - 2, yo);
    }
}

