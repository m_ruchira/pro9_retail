package com.isi.csvr.ohlc;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Uditha Nagahawatta
 * @version 1.0
 */

import com.isi.csvr.TradeStation.TradeStationManager;
import com.isi.csvr.chart.ChartSplitPoints;
import com.isi.csvr.chart.SplitPointTable;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.customindex.CustomIndexInterface;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.downloader.HistoryArchiveRegister;
import com.isi.csvr.downloader.OHLCDownloadManager;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.metastock.MetaStockManager;
import com.isi.csvr.scanner.Scans.ScanRecord;
import com.isi.csvr.shared.*;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class
        OHLCStore implements ExchangeListener, ApplicationListener {

    private static final int INTRADAY = 0;
    private static final int HISTORY = 1;
    private static Hashtable<String, DynamicArray> IntradayReqRegister;
    private static RequestCounter intradayReqCounter;
    private static RequestCounter historyReqCounter;
    private static RequestCounter scanHistoryReqCounter;
    private static Hashtable<String, DynamicArray> historyReqRegister;
    private static Hashtable<String, DynamicArray> scanHistoryReqRegister;
    private static Hashtable<String, SplitPointTable> splitPointRegister;
    private static OHLCStore self = null;

//    private boolean ohlcDownloading = false;
    private static OHLCComparator comparator;
    private static SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");


    private OHLCStore() {
        historyReqRegister = new Hashtable<String, DynamicArray>();
        scanHistoryReqRegister = new Hashtable<String, DynamicArray>();
        splitPointRegister = new Hashtable<String, SplitPointTable>();
        intradayReqCounter = new RequestCounter();
        historyReqCounter = new RequestCounter();
        scanHistoryReqCounter = new RequestCounter();
        comparator = new OHLCComparator();
        IntradayReqRegister = new Hashtable<String, DynamicArray>(6, 0.9F);
        if (Settings.isOfflineMode()) {
            load();
        }
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Application.getInstance().addApplicationListener(this);
    }

    public synchronized static OHLCStore getInstance() {
        if (self == null) {
            self = new OHLCStore();
        }
        return self;
    }

    public static void setDecription(String key, String description) {
//        try {
//            IntradayReqRegister.get(key).description = description;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static String getDecription(String key) {
//        String descr = null;
//
//        try {
//            descr = IntradayReqRegister.get(key).description;
//            if (descr == null){
//                return (Language.getString("NA"));
//            }else{
//                return descr;
//            }
//        } catch (Exception e) {
//            return (Language.getString("NA"));
//        }
        return "";
    }

    public static void printCount(String exchange) {

        Enumeration keys = IntradayReqRegister.keys();
        int count = 0;
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String exchangeID = SharedMethods.getExchangeFromKey(key);
            if ((exchangeID.equals(exchange)) || (exchange.equals("*"))) {
                DynamicArray array = IntradayReqRegister.get(key);
                //System.out.println(key + " " + array.size());
                System.out.printf("%-20s %d\n", key, array.size());
                count += array.size();
            }
        }
        System.out.println("Intra Count " + count);
    }

    public static void printTASICount() {
        Enumeration keys = IntradayReqRegister.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            if (key.equals("TDWL~TASI")) {
                DynamicArray array = IntradayReqRegister.get(key);
                System.out.printf("%-20s %d\n", key, array.size());
                break;
            }
        }
    }

    public synchronized DynamicArray addIntradayRequest(String key) {

        String exchange = SharedMethods.getExchangeFromKey(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);

        DynamicArray array = getIntradayRegister(key);

        if (intradayReqCounter.increment(key) == 1) {
            Exchange exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
            if (!ExchangeStore.isExpired(exchange) && !ExchangeStore.isInactive(exchange)) {
                if ((exchangeObject != null) && (!exchangeObject.isDefault())) {
                    //todo - need to add the instruemnt type all the requests
                    OHLCDownloadManager.getSharedInstance().addRequest(key, Meta.OHLC_HISTORY);
                    DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrument, Meta.INTRADAYOHLC);
                    String str = Meta.SYMBOL + Meta.DS + symbol + Meta.FS + Meta.EXCHANGE + Meta.DS + exchange + Meta.FS;
                    SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, Meta.ADD_SYMBOL_REQUEST + Meta.DS + Meta.INTRADAYOHLC + Meta.FD + exchange + Meta.FD + symbol + Meta.EOL, exchange, null, -1, str);
                } else {
                    IntradayFileLoader intradayFileLoader = new IntradayFileLoader(array, exchange, symbol, instrument);
                    intradayFileLoader.start();
                }
            } else {
                SharedMethods.showExchangeExpiryMesage(exchange);
            }
            exchangeObject = null;
        }

        exchange = null;
        symbol = null;

        return array;
    }

    public synchronized DynamicArray getOHLCMapStore(String key, long date) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);
        DynamicArray array = new DynamicArray();
        ///////////////////////////////////////////////////////////////////
        File[] paths = SharedMethods.getIntrdayPaths(exchange);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String currentDate = dateFormat.format(new Date(date));
//        if (paths != null) {
//            for (int i = 0; i < paths.length; i++) {
        try {
            //priority check for memory outs
            String data = null;
            String[] ohlcData = null;

//                    File source = new File(paths[i].getAbsolutePath() + "/" + symbol + ".csv");
            File source = new File(Settings.getAbsolutepath() + "ohlc" + "/" + exchange + "/" + currentDate + "/" + symbol + ".csv");
            //            String key = SharedMethods.getKey(exchange, symbol, instrument);

            if (source.exists()) {
                BufferedReader in = new BufferedReader(new FileReader(source));
                String minDate = null;
                while (true) {
                    data = in.readLine();
                    if (data == null) break;
                    ohlcData = data.split(Meta.FS);
                    IntraDayOHLC record = new IntraDayOHLC(key, Long.parseLong(ohlcData[0]),
                            Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
                            Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
                            Long.parseLong(ohlcData[5]), 0, 0); // todo intraday vwap
                    array.add(record);
                    if (minDate == null) {
                        minDate = fmt.format(record.getTime() * 60000);
                    }
                    record = null;
                }
                in.close();
                in = null;
                Thread.sleep(1);
            } else {
                //loading from the memory
                DynamicArray list = getIntradayRegister(key);
                for (int k = 0; k < list.size(); k++) {
                    array.add(list.get(k));

                }

            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

//            }
//            paths = null;
//        }


        //////////////////////////////////////////////////////////////////
//         DynamicArray array = getIntradayRegister(key);
//        if (intradayReqCounter.increment(key) == 1) {
//        Exchange exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
//        if (!ExchangeStore.isExpired(exchange) && !ExchangeStore.isInactive(exchange)){
//            if ((exchangeObject != null) && (!exchangeObject.isDefault())) {
//                //todo - need to add the instruemnt type all the requests
//                        OHLCDownloadManager.getSharedInstance().addRequest(key, Meta.OHLC_HISTORY);
//                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrument, Meta.INTRADAYOHLC);
//                        String str = Meta.SYMBOL + Meta.DS + symbol + Meta.FS + Meta.EXCHANGE + Meta.DS + exchange + Meta.FS;
//                        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY,Meta.ADD_SYMBOL_REQUEST + Meta.DS + Meta.INTRADAYOHLC + Meta.FD + exchange + Meta.FD + symbol+Meta.EOL,exchange, null, -1, str);
//            } else {
//                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
//                IntradayGraphDataLoader intradayFileLoader = new IntradayGraphDataLoader(array, exchange, symbol, instrument,dateFormat.format(new Date(date)));
//                intradayFileLoader.start();
//            }
//        }else {
//            SharedMethods.showExchangeExpiryMesage(exchange);
//        }
//        exchangeObject = null;
//        }

        exchange = null;
        symbol = null;

        return array;

    }

    //NOTE: this is for RadarScreen
    public synchronized DynamicArray getFullIntradayHistory(String key) {

        String exchange = SharedMethods.getExchangeFromKey(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);

        DynamicArray array = getIntradayRegister(key);

        Exchange exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
        if (exchangeObject != null && exchangeObject.isDefault()) {
            intradayReqCounter.increment(key);
            loadIntradayHistryForSymbol(array, exchange, symbol, instrument);
        }
        exchangeObject = null;
        exchange = null;
        symbol = null;

        return array;
    }

    public synchronized DynamicArray getIntradayRegister(String key) {

        DynamicArray list = IntradayReqRegister.get(key);
        if (list == null) {
            list = new DynamicArray();
            IntradayReqRegister.put(key, list);
        }
        return list;
    }

    public void removeIntradayRequest(String key) {
        if (intradayReqCounter.decrement(key) == 0) {
            String exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);

            DynamicArray list = getIntradayRegister(key);

            trimIntrdayForSymbol(list, ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDateTime());

            OHLCDownloadManager.getSharedInstance().removeInreadayRequest(key);
            DataStore.getSharedInstance().removeSymbolRequest(exchange, symbol, Meta.INTRADAYOHLC);
//            SendQFactory.addRemoveRequest(exchange, symbol, Meta.INTRADAYOHLC, DataStore.getSharedInstance().getStockObject(key).getInstrumentType());
            exchange = null;
            symbol = null;
            list = null;
        }
    }

    public void clearForExchangeInitialize(String exchangeID) {
        Enumeration<String> keys = IntradayReqRegister.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            String exchange = SharedMethods.getExchangeFromKey(key);
            if ((exchange.equals(exchangeID)) && (intradayReqCounter.getCountFor(key) == 0)) {
//                if (intradayReqCounter.getCountFor(key) == 0) {
                DynamicArray list = getIntradayRegister(key);
                trimIntrdayForSymbol(list, ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDateTime());
                list = null;
//                }
            }
        }
    }

    private void trimIntrdayForSymbol(DynamicArray list, long currentDay) {
        try {
            ArrayList tempList = new ArrayList();
            currentDay = (currentDay / (1000 * 60 * 60 * 24)) * (1000 * 60 * 60 * 24); // without minutes (in days)
            int size = list.size();
            for (int i = 0; i < size; i++) {
                IntraDayOHLC ohlc = (IntraDayOHLC) list.get(i);
                if ((ohlc.getTime() * 60000) >= currentDay) {
                    tempList.add(ohlc);
                }
                ohlc = null;
            }
            list.clear();
            list.trimToSize();
            list.getList().addAll(tempList);
            tempList = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DynamicArray getIntradayOHLCRecords(String key) {
        DynamicArray list = addIntradayRequest(key);
//        DynamicArray list = getIntradayRegister(key);
        String exchange = SharedMethods.getExchangeFromKey(key);
        DynamicArray tempList = new DynamicArray();
        try {
            long currentDay = ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDateTime();
            currentDay = (currentDay / (1000 * 60 * 60 * 24)) * (1000 * 60 * 60 * 24); // without minutes (in days)
            int size = list.size();
            for (int i = 0; i < size; i++) {
                IntraDayOHLC ohlc = (IntraDayOHLC) list.get(i);
                if ((ohlc.getTime() * 60000) >= currentDay) {
                    tempList.add(ohlc);
                }
                ohlc = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tempList;
    }

    public DynamicArray getIntraDayOHLCMapRecords(String key, long date) {
        DynamicArray list = getOHLCMapStore(key, date);
//        DynamicArray list = getIntradayRegister(key);
        String exchange = SharedMethods.getExchangeFromKey(key);
        DynamicArray tempList = new DynamicArray();
        try {
            long currentDay = ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDateTime();
            currentDay = (currentDay / (1000 * 60 * 60 * 24)) * (1000 * 60 * 60 * 24); // without minutes (in days)
            int size = list.size();
            for (int i = 0; i < size; i++) {
                IntraDayOHLC ohlc = (IntraDayOHLC) list.get(i);
                if ((ohlc.getTime() * 60000) >= currentDay) {
                    tempList.add(ohlc);
                }
                ohlc = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tempList;
    }

    public DynamicArray getOHLCValueArray(String symbol, String exchange, int instrument) {
//        long currentDay =ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDateTime();
//        currentDay = (currentDay / (1000 * 60 * 60 * 24)) * (1000 * 60 * 60 * 24); // without minutes (in days)

//        DynamicArray list = getIntradayRegister(SharedMethods.getKey(exchange, symbol));
        DynamicArray list = getIntradayOHLCRecords(SharedMethods.getKey(exchange, symbol, instrument));
        DynamicArray ohlcarray = new DynamicArray();
        int size = list.size();
        try {
            if (size >= Stock.OHLC_MAP_COUNT) {
                for (int i = size - Stock.OHLC_MAP_COUNT; i < size; i++) {
                    IntraDayOHLC ohlc = (IntraDayOHLC) list.get(i);
                    ohlcarray.add(ohlc);
                    ohlc = null;
                }
            } else {
                for (int i = 0; i < size; i++) {
                    IntraDayOHLC ohlc = (IntraDayOHLC) list.get(i);
                    ohlcarray.add(ohlc);
                    ohlc = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        list = null;
        return ohlcarray;
    }

    public DynamicArray getOHLCValueArrayForCurrentDay(String symbol, String exchange, int instrument, long date) {
//        long currentDay =ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDateTime();
//        currentDay = (currentDay / (1000 * 60 * 60 * 24)) * (1000 * 60 * 60 * 24); // without minutes (in days)

//        DynamicArray list = getIntradayRegister(SharedMethods.getKey(exchange, symbol));
        DynamicArray list = getIntraDayOHLCMapRecords(SharedMethods.getKey(exchange, symbol, instrument), date);
        DynamicArray ohlcarray = new DynamicArray();
        int size = list.size();
        try {
            if (size >= Stock.OHLC_MAP_COUNT) {
                for (int i = size - Stock.OHLC_MAP_COUNT; i < size; i++) {
                    IntraDayOHLC ohlc = (IntraDayOHLC) list.get(i);
                    ohlcarray.add(ohlc);
                    ohlc = null;
                }
            } else {
                for (int i = 0; i < size; i++) {
                    IntraDayOHLC ohlc = (IntraDayOHLC) list.get(i);
                    ohlcarray.add(ohlc);
                    ohlc = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        list = null;
        return ohlcarray;
    }

    public DynamicArray getOHLCForIndex(String key) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        if (key != null) {
            try {
                DynamicArray ohlcVals;
                ohlcVals = getIntradayOHLCRecords(key);
//                ohlcVals = addIntradayRequest(key);
//                trimIntrdayForSymbol(ohlcVals, ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDateTime());
                return ohlcVals;
            } catch (Exception e) {
                System.out.println("Not laoding OHLC");
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return new DynamicArray();
            }
        } else {
            return new DynamicArray();
        }

    }

    /*public synchronized void trimDB(DynamicArray ohlcList, String minDate) {
        try {
            ArrayList<IntraDayOHLC> array = ohlcList.getList();
            String date = "";
            IntraDayOHLC record;
            int size = array.size();

            for (int i = size - 1; i >= 0; i--) {
                record = array.get(i);
                date = fmt.format(record.getTime() * 60000);
                if (date.compareToIgnoreCase(minDate) < 0) {
                    array.remove(i);
                }
                record = null;
            }
            array = null;


        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void resendIntradayRequests() {
        Enumeration keys = intradayReqCounter.getKeys();

        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);
            String request = null;
            Exchange exg = (Exchange) ExchangeStore.getSharedInstance().getExchange(exchange);
            if (!exg.isDefault()) {
                request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + Meta.MESSAGE_TYPE_OHLC_TRADE +
                        Meta.FD + exchange + Meta.FD + symbol + Meta.EOL;
                SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request, exchange);
            }
            key = null;
            symbol = null;
            exchange = null;
            request = null;
        }
        keys = null;
    }

    public void clear() {
        Enumeration keys = IntradayReqRegister.keys();
        while (keys.hasMoreElements()) {
            DynamicArray list = IntradayReqRegister.get(keys.nextElement());
            list.clear();
            list.trimToSize();
            list = null;
        }
    }

    public void trimIntrdayOHLCDB() {
        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = (Exchange) exchanges.nextElement();
                if (exchange.isDefault()) {
                    try {
                        File folder = new File(Settings.getAbsolutepath() + "ohlc/" + exchange.getSymbol());
                        File[] subfolders = folder.listFiles();
                        for (File subfolder : subfolders) {
                            if ((subfolder != null) && (subfolder.getName().compareTo(exchange.getLastIntradayHistorydate()) < 0)) {
                                deleteFiles(subfolder);
                                HistoryArchiveRegister.getSharedInstance().removeEntry(exchange.getSymbol(), subfolder.getName());
                                subfolder.delete();
                            }
                        }
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                }
                exchange = null;
            }
            HistoryArchiveRegister.getSharedInstance().saveData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteFiles(File path) {
        try {
            File[] files = path.listFiles();
            for (File file : files) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Enumeration getSymbols() {
        return IntradayReqRegister.keys();
    }

    public synchronized void addOHLCRecord(IntraDayOHLC record) {
        String key = record.getSymbol();

        addRecord(record, getIntradayRegister(key), INTRADAY);
    }

    public synchronized void addHistoryRecord(IntraDayOHLC record) {
        String key = record.getSymbol();

        addRecord(record, getHistoryRegister(key), HISTORY);
    }

    public void addIntradayRecord(IntraDayOHLC record) {
        DynamicArray ohlcList = null;
        int location = 0;
        try {
            ohlcList = getIntradayRegister(record.getSymbol());
            location = Collections.binarySearch(ohlcList.getList(), record, comparator);
            if (location < 0) {
                /*if (record.getSymbol().equals("DFM~EMAAR")){
                    System.out.println("EMAAR catched");
                }*/
                ohlcList.add((location + 1) * -1, record);
                //MetaStockManager.getInstance().addIntrdayRecord(record); todo - commented  by shashika  to avoid duplicates
            } else {
                ohlcList.set(location, record);
                //MetaStockManager.getInstance().addIntrdayRecord(record); todo - commented  by shashika  to avoid duplicates
            }
            ohlcList = null;
        } catch (Exception e) {
            System.out.println("Error " + record.getSymbol() + " size " + ohlcList.size() + " loca " + location);
        }
    }

    private synchronized void addRecord(IntraDayOHLC record, DynamicArray ohlcList, int type) {
        addRecord(record, ohlcList, type, true);
    }

    private synchronized void addRecord(IntraDayOHLC record, DynamicArray ohlcList, int type, boolean updateMetaStock) {
        IntraDayOHLC lastRecord;
        if (ohlcList == null) return;

        synchronized (ohlcList) {
            try {
                int location = Collections.binarySearch(ohlcList.getList(), record, comparator);
                if (location < 0) {
                    ohlcList.add((location + 1) * -1, record);
                    if ((type == INTRADAY) && (updateMetaStock)) {
                        MetaStockManager.getInstance().addIntrdayRecord(record);
                        TradeStationManager.getInstance().addIntrdayRecord(record);
                    }
                } else {
                    lastRecord = (IntraDayOHLC) ohlcList.get(location);
                    if (lastRecord.getHigh() < record.getHigh()) {
                        lastRecord.setHigh(record.getHigh());
                    }
                    if (lastRecord.getLow() > record.getLow()) {
                        lastRecord.setLow(record.getLow());
                    }
                    lastRecord.setClose(record.getClose());
//                    lastRecord.setVolume(lastRecord.getVolume() + record.getVolume()); //Bug ID <????>
                    lastRecord.setVolume(record.getVolume());
                    lastRecord.setTurnOver(record.getTurnOver());
                    if ((type == INTRADAY) && (updateMetaStock)) {
                        MetaStockManager.getInstance().addIntrdayRecord(lastRecord);
                        TradeStationManager.getInstance().addIntrdayRecord(lastRecord);
                    }
                }
            } catch (Exception ex) {
            } finally {
                lastRecord = null;
            }
        }
    }

    public synchronized IntraDayOHLC getLastRecord(String exchange, String symbol, int instrument) {
        String key = SharedMethods.getKey(exchange, symbol, instrument);
        ArrayList list = getIntradayRegister(key).getList();
        IntraDayOHLC record = null;

        if (list.size() > 0) {
            record = (IntraDayOHLC) list.get(list.size() - 1);
        }

        if (record == null) {
            record = new IntraDayOHLC(key, 0, 0, 0, 0, 0, 0, 0, 0);
        }
        list = null;

        try {
            return (IntraDayOHLC) record.clone();
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }

    public void saveData() {
        try {

            trimIntrdayOHLCDB();
            // first remove non default markets. They should not be saved
            Enumeration<String> keys = IntradayReqRegister.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                //if (intradayReqCounter.getCountFor(key) == 0) {
                String exchange = SharedMethods.getExchangeFromKey(key);
                if (!ExchangeStore.getSharedInstance().isDefault(exchange)) {
                    IntradayReqRegister.remove(key);
                } else {
                    DynamicArray list = getIntradayRegister(key);
                    trimIntrdayForSymbol(list, ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDateTime());
                }
                //}
            }

            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Settings.getAbsolutepath() + "datastore/ohlc.mdf"));
            out.writeObject(IntradayReqRegister);
            out.flush();
            out.close();
        } catch (Exception ex) {
        }
    }

    /*public synchronized void removeScanHistoryRequest(String key) {
        try {
            if (scanHistoryReqCounter.decrement(key) == 0) {
                DynamicArray register = getScanHistoryRegister(key);
                register.clear();
                register.trimToSize();
                scanHistoryReqRegister.remove(key);
            }
        } catch (Exception e) {
        }
    }*/

    public void load() {
        try {
            IntradayReqRegister = null;
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath() + "datastore/ohlc.mdf"));
            IntradayReqRegister = (Hashtable<String, DynamicArray>) in.readObject();
            in.close();
        } catch (Exception ex) {

        }
        if (IntradayReqRegister == null) {
            IntradayReqRegister = new Hashtable<String, DynamicArray>(6, 0.9F);
        }
    }
    /*public DynamicArray getScanHistoryRegister(String key) {
        DynamicArray array = scanHistoryReqRegister.get(key);
        if (array == null) {
            array = new DynamicArray();
            scanHistoryReqRegister.put(key, array);
            loadScanHistory(key, array);
            array.trimToSize();
        }
        return array;
    }*/

    public synchronized DynamicArray addHistoryRequest(String key) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        DynamicArray array = getHistoryRegister(key);
        if (historyReqCounter.increment(key) == 1) {
            Exchange exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
            if ((exchangeObject != null) && (!exchangeObject.isDefault())) {
//                if (getHistoryRegister(key).size() == 0) {
                OHLCDownloadManager.getSharedInstance().addRequest(key, Meta.HISTORY);
//                }
            }
            exchangeObject = null;
//        historyReqCounter.increment(key);
        }
        //System.out.println("Array Size = " + array.size());
        return array;
    }

    public synchronized DynamicArray addScanHistoryRequest(String key) {
        /*String exchange = SharedMethods.getExchangeFromKey(key);

        Exchange exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
        if ((exchangeObject != null) && (!exchangeObject.isDefault())) {
            if (getHistoryRegister(key).size() == 0) {
                OHLCDownloadManager.getSharedInstance().addRequest(key, Meta.HISTORY);
            }
        }
        exchangeObject = null;
        scanHistoryReqCounter.increment(key);
        System.out.println("Array Size = " + getScanHistoryRegister(key).size());*/
        return loadScanHistory(key);
    }

    public synchronized void removeHistoryRequest(String key) {
        try {
            if (historyReqCounter.decrement(key) == 0) {
                DynamicArray register = getHistoryRegister(key);
                register.clear();
                register.trimToSize();
                historyReqRegister.remove(key);
            }
        } catch (Exception e) {
        }
    }

    public DynamicArray getHistoryRegister(String key) {
        DynamicArray array = historyReqRegister.get(key);
        if (array == null) {
            array = new DynamicArray();
            historyReqRegister.put(key, array);
            loadHistory(key, array);
            array.trimToSize();
        }
        return array;
    }

    public SplitPointTable getSplitPointTable(String key) {
        SplitPointTable splitPoints = splitPointRegister.get(key);
        if (splitPoints == null) {
            splitPoints = new SplitPointTable();
            splitPointRegister.put(key, splitPoints);
        }
        return splitPoints;
    }

    private void loadHistory(String key, DynamicArray list) {

       /* DynamicArray rawData = HistoryFilesManager.readHistoryData(key);
        String record;
        IntraDayOHLC ohlcRecord;
        String dateString;
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        float open;
        float high;
        float low;
        float close;
        float vwap;
        long volume;

        if (rawData == null) return;

        SplitPointTable splitPointTable = getSplitPointTable(key);
        splitPointTable.clear();
        LinkedList<IntraDayOHLC> tempList = new LinkedList<IntraDayOHLC>();

        for (int i = 0; i < rawData.size(); i++) {
            record = (String) rawData.get(i);

            try {
                StringTokenizer oTokenizer = new StringTokenizer(record, Meta.FS);
                dateString = oTokenizer.nextToken();
                date = format.parse(dateString);
                calendar.setTime(date);
                calendar.set(Calendar.HOUR, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                open = SharedMethods.floatValue(oTokenizer.nextToken());     // open
                high = SharedMethods.floatValue(oTokenizer.nextToken());     // high
                low = SharedMethods.floatValue(oTokenizer.nextToken());      // low
                close = SharedMethods.floatValue(oTokenizer.nextToken());    // today's close
                oTokenizer.nextToken();  // % change
                oTokenizer.nextToken();   // change
                oTokenizer.nextToken();// prev closed
                volume = SharedMethods.longValue(oTokenizer.nextToken());   // volumn
                long turnover = SharedMethods.longValue(oTokenizer.nextToken());// turnover
                //System.out.println("========= turnover ======= "+turnover);
                try {
                    oTokenizer.nextToken(); // no of trades
                } catch (Exception e) {

                }
                try {
                    *//*
                    group = 0 // announcements
                        type  N/A
                    group = 1 // corporate action
                        type
                            1: Stock Split
                            2: Stock Dividend (Bonus)
                            3: Rights Issue
                            4: Reverse Split
                            5: Capital Reduction
                            6: Cash dividend
                     *//*
                    String splitRec = oTokenizer.nextToken();
                    String[] splitRecords = splitRec.split(Meta.RS);
                    ChartSplitPoints splitPoints;
                    for (int j = 0; j < splitRecords.length; j++) {
                        String[] splitFields = splitRecords[j].split(Meta.DS);
                        splitPoints = new ChartSplitPoints();
                        splitPoints.date = date.getTime();
                        splitPoints.group = Integer.parseInt(splitFields[0]);
                        splitPoints.type = Integer.parseInt(splitFields[1]);
                        splitPoints.value = Float.parseFloat(splitFields[2]);
                        //if ((splitPoints.group == 0) || ((splitPoints.group == 1) && (splitPoints.type == 1))) // announcements or stock splits
                        splitPointTable.addRecord(splitPoints);
                        if (splitPoints.group == 1) {
                            System.out.println(splitPoints.toString());
                        }
                        splitPoints = null;
                    }
                    splitRecords = null;
                } catch (Exception e) {

                }
                try {
                    vwap = SharedMethods.floatValue(oTokenizer.nextToken());    // today's vwap close
                } catch (Exception e) {
                    vwap = 0;
                }
                ohlcRecord = new IntraDayOHLC(key, calendar.getTimeInMillis() / 60000, open, high, low, close, volume, vwap, turnover); //Bug id <#0010>
//                ohlcRecord = new IntraDayOHLC(key, date.getTime() / 60000, open, high, low, close, volume);
                tempList.add(ohlcRecord);
                ohlcRecord = null;
            } catch (ParseException ex) {
            }
        }

        for (IntraDayOHLC intraDayOHLC : tempList) {
            list.add(intraDayOHLC);
        }
        tempList.clear();
        tempList = null;
*/

        DynamicArray rawData = HistoryFilesManager.readHistoryData(key);
        String record;
        IntraDayOHLC ohlcRecord;
        String dateString;
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        float open;
        float high;
        float low;
        float close;
        float vwap;
        long volume;
        double turnover;

        if (rawData == null) return;

        SplitPointTable splitPointTable = getSplitPointTable(key);
        splitPointTable.clear();
        LinkedList<IntraDayOHLC> tempList = new LinkedList<IntraDayOHLC>();

        for (int i = 0; i < rawData.size(); i++) {
            record = (String) rawData.get(i);

            try {
                //StringTokenizer oTokenizer = new StringTokenizer(record, Meta.FS);
                String[] tokens = record.split(Meta.FS, 13);
                dateString = tokens[0]; // date
                date = format.parse(dateString);
                calendar.setTime(date);
                calendar.set(Calendar.HOUR, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);

                open = SharedMethods.floatValue(tokens[1]);     // open
                high = SharedMethods.floatValue(tokens[2]);     // high
                low = SharedMethods.floatValue(tokens[3]);      // low
                close = SharedMethods.floatValue(tokens[4]);    // today's close
                float pctChange = SharedMethods.floatValue(tokens[5]);      // % change
                float change = SharedMethods.floatValue(tokens[6]);   // change
                float previousClosed = SharedMethods.floatValue(tokens[7]);   // prev closed
                volume = SharedMethods.longValue(tokens[8]);   // volume
                turnover = SharedMethods.doubleValue(tokens[9]);// turnover
                //System.out.println("========= turnover ======= "+turnover);
                try {
                    long noOfTrades = SharedMethods.longValue(tokens[10]); // no of trades
                } catch (Exception e) {

                }
                try {
                    /*
                    group = 0 // announcements
                        type  N/A
                    group = 1 // corporate action
                        type
                            1: Stock Split
                            2: Stock Dividend (Bonus)
                            3: Rights Issue
                            4: Reverse Split
                            5: Capital Reduction
                            6: Cash dividend
                     */
                    String splitRec = tokens[11];
                    String[] splitRecords = splitRec.split(Meta.RS);
                    ChartSplitPoints splitPoints;
                    for (int j = 0; j < splitRecords.length; j++) {
                        String[] splitFields = splitRecords[j].split(Meta.DS);
                        splitPoints = new ChartSplitPoints();
                        splitPoints.date = date.getTime();
                        splitPoints.group = Integer.parseInt(splitFields[0]);
                        splitPoints.type = Integer.parseInt(splitFields[1]);
                        splitPoints.value = Float.parseFloat(splitFields[2]);
                        splitPoints.sequence = j;
                        //if ((splitPoints.group == 0) || ((splitPoints.group == 1) && (splitPoints.type == 1))) // announcements or stock splits
                        splitPointTable.addRecord(splitPoints);
                        if (splitPoints.group == 1) {
                            //System.out.println(splitPoints.toString());
                        }
                        splitPoints = null;
                    }
                    splitRecords = null;
                } catch (Exception e) {

                }
                try {
                    vwap = SharedMethods.floatValue(tokens[12]);    // today's vwap close
                } catch (Exception e) {
                    vwap = 0;
                }
                ohlcRecord = new IntraDayOHLC(key, calendar.getTimeInMillis() / 60000, open, high, low, close, volume, vwap, turnover); //Bug id <#0010>
                tempList.add(ohlcRecord);
                ohlcRecord = null;
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }

        for (IntraDayOHLC intraDayOHLC : tempList) {
            list.add(intraDayOHLC);
        }
        tempList.clear();
        tempList = null;

    }

    private DynamicArray loadScanHistory(String key) {
        String record;
        ScanRecord scanRecord;
        String dateString;
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        float open;
        float high;
        float low;
        float close;
        long volume;
        float pctChange;
        float change;
        DynamicArray list = new DynamicArray();


        DynamicArray rawData = HistoryFilesManager.readHistoryData(key);
        if (rawData == null) return list;

        SplitPointTable splitPointTable = getSplitPointTable(key);
        splitPointTable.clear();
        LinkedList<ScanRecord> tempList = new LinkedList<ScanRecord>();

        for (int i = 0; i < rawData.size(); i++) {
            record = (String) rawData.get(i);

            try {
                StringTokenizer oTokenizer = new StringTokenizer(record, Meta.FS);
                dateString = oTokenizer.nextToken();
                date = format.parse(dateString);

                calendar.setTime(date);
                calendar.set(Calendar.HOUR, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
                if (key.equals("DFM~ARMX~0") && dateString.equals("20090728")) {
                    //System.out.println("*********** time 1 ******** " + calendar.getTimeInMillis());
                    //System.out.println("*********** time 2 ******** " + date.getTime());
                }

                open = SharedMethods.floatValue(oTokenizer.nextToken());     // open
                high = SharedMethods.floatValue(oTokenizer.nextToken());     // high
                low = SharedMethods.floatValue(oTokenizer.nextToken());      // low
                close = SharedMethods.floatValue(oTokenizer.nextToken());    // today's close
                pctChange = SharedMethods.floatValue(oTokenizer.nextToken());  // % change
                change = SharedMethods.floatValue(oTokenizer.nextToken());   // change
                oTokenizer.nextToken();// prev closed
                volume = SharedMethods.longValue(oTokenizer.nextToken());   // volumn
                oTokenizer.nextToken();// turnover
                try {
                    oTokenizer.nextToken(); // no of trades
                } catch (Exception e) {

                }
                /*try {
                    *//*
                    group = 0 // announcements
                        type  N/A
                    group = 1 // corporate action
                        type
                            1: Stock Split
                            2: Stock Dividend (Bonus)
                            3: Rights Issue
                            4: Reverse Split
                            5: Capital Reduction
                            6: Cash dividend
                     *//*
                    String splitRec = oTokenizer.nextToken();
                    String[] splitRecords = splitRec.split(Meta.RS);
                    ChartSplitPoints splitPoints;
                    for (int j = 0; j < splitRecords.length; j++) {
                        String[] splitFields = splitRecords[j].split(Meta.DS);
                        splitPoints = new ChartSplitPoints();
                        splitPoints.date = date.getTime();
                        splitPoints.group = Integer.parseInt(splitFields[0]);
                        splitPoints.type = Integer.parseInt(splitFields[1]);
                        splitPoints.value = Float.parseFloat(splitFields[2]);
                        //if ((splitPoints.group == 0) || ((splitPoints.group == 1) && (splitPoints.type == 1))) // announcements or stock splits
                        splitPointTable.addRecord(splitPoints);
                        if (splitPoints.group == 1) {
                            System.out.println(splitPoints.toString());
                        }
                        splitPoints = null;
                    }
                    splitRecords = null;
                } catch (Exception e) {

                }*/
                /*try {
                    vwap = SharedMethods.floatValue(oTokenizer.nextToken());    // today's vwap close
                } catch (Exception e) {
                    vwap = 0;
                }*/

                //scanRecord = new ScanRecord(calendar.getTimeInMillis() / 60000, open, high, low, close, change, pctChange, volume);
                scanRecord = new ScanRecord(calendar.getTimeInMillis(), open, high, low, close, change, pctChange, volume);
//                ohlcRecord = new IntraDayOHLC(key, date.getTime() / 60000, open, high, low, close, volume);
                tempList.add(scanRecord);
                scanRecord = null;
            } catch (ParseException ex) {
            }
        }

        for (ScanRecord tempScanRecord : tempList) {
            list.add(tempScanRecord); // insert method cn be used.
        }
        tempList.clear();
        tempList = null;

        return list;
    }

    public void reloadHistoryForExchange(String exchange) {
        Enumeration<String> keys = historyReqRegister.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            if (key.startsWith(exchange)) {
                loadHistory(key, getHistoryRegister(key));
            }
            key = null;
        }
        keys = null;
    }

    public void createOHLCForNewIndex(String indexName) {
        CustomIndexInterface object = CustomIndexStore.getSharedInstance().getCustomIndexObject(SharedMethods.getSymbolFromKey(indexName));
        String exchange = object.getExchanges();
        String path = Settings.getAbsolutepath() + "ohlc/" + exchange + "/";
        File file = new File(path);
        File[] fileList = file.listFiles();
//        System.out.println("fileList");
//        String[] paths = new String[]{};
        for (int i = 0; i < fileList.length; i++) {
            System.out.println("create OHLC for index = " + indexName);
            createOHLCForIndex(indexName, (fileList[i]).getPath());
        }
    }

    private void createOHLCForIndex(String indexName, String path) {
        FileInputStream fileIN;
        FileInputStream tempIN = null;
        FileOutputStream fileOut;
        BufferedWriter out;
        BufferedReader in;
        BufferedReader tmpin = null;
        boolean isdone;
        boolean iskeeptmp;
        boolean iskeepsymbol;
        boolean isFirsttmpLine = true;
        boolean isFirstSymbolLine = true;
        Vector vecData = null;
        Hashtable<String, String> symbols = CustomIndexStore.getSharedInstance().getSymbolMap(SharedMethods.getSymbolFromKey(indexName));
        File parentFile = new File(path);
        if (symbols != null) {
            Enumeration<String> keyList = symbols.keys();
            int size = symbols.size();
            int count = 0;
            while (keyList.hasMoreElements()) {
                String key = keyList.nextElement();
                String symbol = SharedMethods.getSymbolFromKey(key);
                vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(key), SharedMethods.getExchangeFromKey(key), parentFile.getName());
                String sline = null;
                String templine = null;
                File file = null;
                if (count != (size - 1)) {
                    file = new File(path + "/temp_" + count + "_.csv");
                } else {
                    file = new File(path + "/" + indexName + ".csv");
                }
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                try {
                    fileIN = new FileInputStream(path + "/" + symbol + ".csv");
                    in = new BufferedReader(new InputStreamReader(fileIN));
                    fileOut = new FileOutputStream(file);
                    out = new BufferedWriter(new OutputStreamWriter(fileOut));
                    try {
                        if (count != 0) {
                            tempIN = new FileInputStream(path + "/temp_" + (count - 1) + "_.csv");
                            tmpin = new BufferedReader(new InputStreamReader(tempIN));
                        }
                    } catch (FileNotFoundException e) {
                        tmpin = null;
                        e.printStackTrace();
                    }
                    isdone = false;
                    iskeeptmp = false;
                    iskeepsymbol = false;
                    isFirsttmpLine = true;
                    isFirstSymbolLine = true;
                    long tmpTime = 0;
                    long symbolTime = 0;
                    float tmpOpen = 0;
                    float symbolOpen = 0;
                    float tmpHigh = 0;
                    float symbolHigh = 0;
                    float tmpLow = 0;
                    float symbolLow = 0;
                    float tmpClose = 0;
                    float symbolClose = 0;
                    long tmpVolumn = 0;
                    long symbolVolumn = 0;
                    String[] ohlcData;
                    int symbolLinesRead = 0;
                    int tmpLinesRead = 0;
                    while (!isdone) {
                        try {
                            if (!iskeepsymbol) {
                                sline = in.readLine();
                                symbolLinesRead = symbolLinesRead + 1;
                            }
                            if ((count != 0) && (!iskeeptmp) && (tmpin != null)) {
                                templine = tmpin.readLine();
                                tmpLinesRead = tmpLinesRead + 1;
                            }
                            if (sline != null) {
                                ohlcData = sline.split(Meta.FS);
                                symbolTime = Long.parseLong(ohlcData[0]);
                                symbolOpen = Float.parseFloat(ohlcData[1]);
                                symbolHigh = Float.parseFloat(ohlcData[2]);
                                symbolLow = Float.parseFloat(ohlcData[3]);
                                symbolClose = Float.parseFloat(ohlcData[4]);
                                symbolVolumn = Long.parseLong(ohlcData[5]);
                            }

                            if (templine != null) {
                                ohlcData = templine.split(Meta.FS);
                                tmpTime = Long.parseLong(ohlcData[0]);
                                tmpOpen = Float.parseFloat(ohlcData[1]);
                                tmpHigh = Float.parseFloat(ohlcData[2]);
                                tmpLow = Float.parseFloat(ohlcData[3]);
                                tmpClose = Float.parseFloat(ohlcData[4]);
                                tmpVolumn = Long.parseLong(ohlcData[5]);
                            }
                            if (symbolLinesRead > 1) {
                                isFirstSymbolLine = false;
                            }
                            if (tmpLinesRead > 1) {
                                isFirsttmpLine = false;
                            }

                            if (count == 0) {
                                if (symbolTime > 0) {
                                    out.write(sline);
                                    out.write("\n");
                                    symbolTime = 0;
                                    symbolOpen = 0;
                                    symbolHigh = 0;
                                    symbolLow = 0;
                                    symbolVolumn = 0;
                                } else {
                                    isdone = true;
                                }
                            } else {
                                if (symbolTime > 0 && tmpTime > 0) {
                                    if (tmpTime == symbolTime) {
                                        String str = tmpTime + Meta.FS + (tmpOpen + symbolOpen) + Meta.FS + (tmpHigh + symbolHigh) + Meta.FS
                                                + (tmpLow + symbolLow) + Meta.FS + (tmpClose + symbolClose) + Meta.FS + (tmpVolumn + symbolVolumn);
                                        out.write(str);
                                        out.write("\n");
                                        iskeepsymbol = false;
                                        iskeeptmp = false;
                                    } else if (tmpTime < symbolTime) {
                                        String str;
                                        if (!isFirstSymbolLine) {
                                            str = tmpTime + Meta.FS + (tmpOpen + symbolClose) + Meta.FS + (tmpHigh + symbolClose) + Meta.FS
                                                    + (tmpLow + symbolClose) + Meta.FS + (tmpClose + symbolClose) + Meta.FS + (tmpVolumn);
                                        } else {
                                            if ((vecData != null) && (vecData.size() > 0)) {
                                                StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                                oTokenizer.nextToken();         // Date
                                                oTokenizer.nextToken();         // open
                                                oTokenizer.nextToken();         // high
                                                oTokenizer.nextToken();         // low
                                                symbolClose = Float.parseFloat(oTokenizer.nextToken());      //close
                                                str = tmpTime + Meta.FS + (tmpOpen + symbolClose) + Meta.FS + (tmpHigh + symbolClose) + Meta.FS
                                                        + (tmpLow + symbolClose) + Meta.FS + (tmpClose + symbolClose) + Meta.FS + (tmpVolumn);
//                                                vecData = null;
                                            } else {
                                                str = tmpTime + Meta.FS + (tmpOpen) + Meta.FS + (tmpHigh) + Meta.FS
                                                        + (tmpLow) + Meta.FS + (tmpClose) + Meta.FS + (tmpVolumn);
                                            }
                                        }
                                        out.write(str);
                                        out.write("\n");
                                        iskeepsymbol = true;
                                        iskeeptmp = false;
                                    } else {
                                        String str;
                                        if (!isFirsttmpLine) {
                                            str = symbolTime + Meta.FS + (tmpClose + symbolOpen) + Meta.FS + (tmpClose + symbolHigh) + Meta.FS
                                                    + (tmpClose + symbolLow) + Meta.FS + (tmpClose + symbolClose) + Meta.FS + (symbolVolumn);
                                        } else {
                                            if ((vecData != null) && (vecData.size() > 0)) {
                                                StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                                oTokenizer.nextToken();         // Date
                                                oTokenizer.nextToken();         // open
                                                oTokenizer.nextToken();         // high
                                                oTokenizer.nextToken();         // low
                                                tmpClose = Float.parseFloat(oTokenizer.nextToken());      //close
                                                str = symbolTime + Meta.FS + (tmpClose + symbolOpen) + Meta.FS + (tmpClose + symbolHigh) + Meta.FS
                                                        + (tmpClose + symbolLow) + Meta.FS + (tmpClose + symbolClose) + Meta.FS + (symbolVolumn);
//                                                vecData = null;
                                            } else {
                                                str = symbolTime + Meta.FS + (symbolOpen) + Meta.FS + (symbolHigh) + Meta.FS
                                                        + (symbolLow) + Meta.FS + (symbolClose) + Meta.FS + (symbolVolumn);
                                            }
                                        }
                                        out.write(str);
                                        out.write("\n");
                                        iskeeptmp = true;
                                        iskeepsymbol = false;
                                    }
                                } else if (symbolTime > 0) {
                                    String str;
                                    if ((vecData != null) && (vecData.size() > 0)) {
                                        StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                        oTokenizer.nextToken();         // Date
                                        oTokenizer.nextToken();         // open
                                        oTokenizer.nextToken();         // high
                                        oTokenizer.nextToken();         // low
                                        tmpClose = Float.parseFloat(oTokenizer.nextToken());      //close
                                        str = symbolTime + Meta.FS + (tmpClose + symbolOpen) + Meta.FS + (tmpClose + symbolHigh) + Meta.FS
                                                + (tmpClose + symbolLow) + Meta.FS + (tmpClose + symbolClose) + Meta.FS + (symbolVolumn);
//                                        vecData = null;
                                    } else {
                                        str = symbolTime + Meta.FS + (symbolOpen) + Meta.FS + (symbolHigh) + Meta.FS
                                                + (symbolLow) + Meta.FS + (symbolClose) + Meta.FS + (symbolVolumn);
                                    }

                                    out.write(str);
                                    out.write("\n");
                                    iskeepsymbol = false;
                                } else if (tmpTime > 0) {
                                    String str;
                                    if ((vecData != null) && (vecData.size() > 0)) {
                                        StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                        oTokenizer.nextToken();         // Date
                                        oTokenizer.nextToken();         // open
                                        oTokenizer.nextToken();         // high
                                        oTokenizer.nextToken();         // low
                                        symbolClose = Float.parseFloat(oTokenizer.nextToken());      //close
                                        str = tmpTime + Meta.FS + (tmpOpen + symbolClose) + Meta.FS + (tmpHigh + symbolClose) + Meta.FS
                                                + (tmpLow + symbolClose) + Meta.FS + (tmpClose + symbolClose) + Meta.FS + (tmpVolumn);
                                    } else {
                                        str = tmpTime + Meta.FS + (tmpOpen) + Meta.FS + (tmpHigh) + Meta.FS
                                                + (tmpLow) + Meta.FS + (tmpClose) + Meta.FS + (tmpVolumn);
                                    }

                                    out.write(str);
                                    out.write("\n");
                                    iskeeptmp = false;
                                } else {
                                    iskeepsymbol = false;
                                    iskeeptmp = false;
                                    isdone = true;
                                }
                                if (!iskeeptmp) {
                                    templine = null;
                                    tmpTime = 0;
                                    tmpOpen = 0;
                                    tmpHigh = 0;
                                    tmpLow = 0;
                                    tmpVolumn = 0;
                                }
                                if (!iskeepsymbol) {
                                    sline = null;
                                    symbolTime = 0;
                                    symbolOpen = 0;
                                    symbolHigh = 0;
                                    symbolLow = 0;
                                    symbolVolumn = 0;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            isdone = true;
                        }
                    }
                    in.close();
                    fileIN.close();
                    in = null;
                    fileIN = null;
                    try {
                        if (count != 0) {
                            tmpin.close();
                            tempIN.close();
                            tmpin = null;
                            tempIN = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    out.flush();
                    out.close();
                    fileOut.close();
                    out = null;
                    fileOut = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                try {
                    File fileold = new File(path + "/temp_" + (count - 1) + "_.csv");
                    fileold.delete();
                    fileold = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                count++;
                symbol = null;
                key = null;
            }
            keyList = null;
        }
    }

    public void createOHLCForAllIndexes(String exchange) {
        Enumeration<String> names = CustomIndexStore.getSharedInstance().getColumnNames();
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            CustomIndexInterface object = CustomIndexStore.getSharedInstance().getCustomIndexObject(name);
            if (object.getExchanges().equals(exchange)) {
                createOHLCForNewIndex(SharedMethods.getKey(CustomIndexStore.CUSTOM_INDEX, object.getSymbol(), Meta.INSTRUMENT_INDEX));
//                createHistoryForIndex(SharedMethods.getKey(CustomIndexStore.CUSTOM_INDEX,object.getSymbol()),path);
            }
        }
    }

    public void loadIntradayHistory(String exchange) {

        String symbol;
        String data;
        String key;
        int instrument = -1;
        //MetaStockInterface.initDB(exchange); //clear metastock DB

        Enumeration symbols = DataStore.getSharedInstance().getSymbols(exchange);
        while (symbols.hasMoreElements()) {
            data = (String) symbols.nextElement();
            symbol = SharedMethods.getSymbolFromExchangeKey(data);
            instrument = SharedMethods.getInstrumentFromExchangeKey(data);
            key = SharedMethods.getKey(exchange, symbol, instrument);
            if (intradayReqCounter.getCountFor(key) > 0) {
                DynamicArray ohlcList = getIntradayRegister(key);
                try {
                    synchronized (ohlcList) {
                        loadIntradayHistryForSymbol(ohlcList, exchange, symbol, instrument);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                ohlcList = null;
            }
        }
//        saveData();
    }

    public void loadIntradayHistryForSymbol(DynamicArray ohlcList, String exchange, String symbol, int instrument) {
        File[] paths = SharedMethods.getIntrdayPaths(exchange);

        if (paths != null) {
            for (int i = 0; i < paths.length; i++) {
                loadIntradayHistryForSymbol(ohlcList, exchange, paths[i].getAbsolutePath(), symbol, instrument);
            }
            paths = null;
        }
    }

    public void loadIntradayDataToOHLCGraph(DynamicArray ohlcList, String exchange, String symbol, int instrument, String date) {
        File paths = SharedMethods.getIntrdayPaths(exchange, date);

        if (paths != null) {
            loadIntradayHistryDataForOHLCGrapgh(ohlcList, exchange, paths.getAbsolutePath(), symbol, instrument);
        }
    }

    private void loadIntradayHistryForSymbol(DynamicArray ohlcList, String exchange, String folder, String symbol, int instrument) {
        try {
            //priority check for memory outs
            String data = null;
            String[] ohlcData = null;

            File source = new File(folder + "/" + symbol + ".csv");
            String key = SharedMethods.getKey(exchange, symbol, instrument);

            BufferedReader in = new BufferedReader(new FileReader(source));
            String minDate = null;
            while (true) {
                data = in.readLine();
                if (data == null) break;
                ohlcData = data.split(Meta.FS);

                double turnOver = 0;
                float vwap = 0;
                try {
                    if (ohlcData[6] != null) {
                        vwap = Float.parseFloat(ohlcData[6]);
                    }
                    if (ohlcData[7] != null) {
                        turnOver = Double.parseDouble(ohlcData[7]);
                    }

                } catch (Exception e) {
                    //  e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                IntraDayOHLC record = new IntraDayOHLC(key, Long.parseLong(ohlcData[0]),
                        Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
                        Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
                        Long.parseLong(ohlcData[5]), vwap, turnOver); // todo intraday vwap
                addRecord(record, ohlcList, INTRADAY, false);
                if (minDate == null) {
                    minDate = fmt.format(record.getTime() * 60000);
                }
                record = null;
            }
            in.close();
            in = null;

            ohlcList = null;
            Thread.sleep(1);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    /*public synchronized boolean isOhlcDownloading() {
        return ohlcDownloading;
    }

    public synchronized void setOhlcDownloading(boolean ohlcDownloading) {
        this.ohlcDownloading = ohlcDownloading;
    }*/
    private void loadIntradayHistryDataForOHLCGrapgh(DynamicArray ohlcList, String exchange, String folder, String symbol, int instrument) {
        try {
            //priority check for memory outs
            String data = null;
            String[] ohlcData = null;

            File source = new File(folder + "/" + symbol + ".csv");
            String key = SharedMethods.getKey(exchange, symbol, instrument);
            BufferedReader in = new BufferedReader(new FileReader(source));
            String minDate = null;
            while (true) {
                data = in.readLine();
                if (data == null) break;
                ohlcData = data.split(Meta.FS);
                double turnOver = 0;
                float vWap = 0;
                try {
                    vWap = Float.parseFloat(ohlcData[6]);
                    turnOver = Double.parseDouble(ohlcData[7]);
                } catch (Exception e) {
                    //System.out.println("*********");
                }
                IntraDayOHLC record = new IntraDayOHLC(key, Long.parseLong(ohlcData[0]),
                        Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
                        Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
                        Long.parseLong(ohlcData[5]), vWap, turnOver); // todo intraday vwap
                addRecord(record, ohlcList, INTRADAY, false);
                if (minDate == null) {
                    minDate = fmt.format(record.getTime() * 60000);
                }
                record = null;
            }
            in.close();
            in = null;
            ohlcList = null;
            Thread.sleep(1);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void printPreferences() {
        Enumeration keys = intradayReqCounter.getKeys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            System.out.println(key + " - " + intradayReqCounter.getCountFor(key));

        }
    }

    /* Exchange Listener Methods */
    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        clearForExchangeInitialize(exchange.getSymbol());
    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
//        load();
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath() + "datastore/ohlc.mdf"));
            Hashtable<String, DynamicArray> IntradayRegister = (Hashtable<String, DynamicArray>) in.readObject();
            Enumeration<String> sSymbols = IntradayRegister.keys();
            String symbol;
            DynamicArray data;
            while (sSymbols.hasMoreElements()) {
                symbol = sSymbols.nextElement();
                if (!IntradayReqRegister.containsKey(symbol)) {
                    IntradayReqRegister.put(symbol, IntradayRegister.get(symbol));
                } else {
                    data = IntradayRegister.get(symbol);
                    Collections.copy(IntradayReqRegister.get(symbol).getList(), data.getList());
                }
            }
            in.close();
        } catch (Exception ex) {
        }
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
