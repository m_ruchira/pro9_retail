package com.isi.csvr.ohlc;

import com.isi.csvr.history.DataRecord;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.watcher.ObjectWatcher;

import java.io.Serializable;

public class IntraDayOHLC extends DataRecord implements Serializable, Cloneable {

    private String sSymbol;
    private long snapShotUpdatedtime = 0l;
//    private long updatedtime=0l;

    public IntraDayOHLC(String key, long time, float open, float high, float low, float close, long volume, float vwap, double turnover) {
        sSymbol = key;
        super.Time = time;
        super.open = open;
        super.high = high;
        super.low = low;
        super.close = close;
        super.volume = volume;
        super.vwap = vwap;
        super.turnOver = turnover;

//        ObjectWatcher.register(this);
    }

    public void setData(String exchange, int instrument, String[] data, int decimalFactor) throws Exception {
        if (data == null)
            return;

        char tag;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == null)
                continue;
            if (data[i].length() <= 1)
                continue;
            tag = data[i].charAt(0);
            switch (tag) {
                case 'A':
                    sSymbol = SharedMethods.getKey(exchange, data[i].substring(1), instrument);
                    break;
                case 'C':
                    Time = SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'D':
                    open = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case 'E':
                    high = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case 'F':
                    low = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case 'G':
                    close = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case 'H':
                    volume = SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'M':
                    turnOver = SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case 'N':
                    vwap = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
            }
        }
    }


    public String toString() {
        //return iExchangeCode + Meta.FD + sSymbol + Meta.FD + lTime + Meta.FD + dOpen + Meta.FD + dHigh + Meta.FD + dLow + Meta.FD + dClose;
//        Date dt = new Date();
//        dt.setTime(Time * 60000);
        return sSymbol + "," + SharedMethods.toFulldateFormat(Time * 60000) + ","
                + Time + "," + volume + "," + open + ","
                + high + "," + low + "," + close;
    }

    public void setUpdateFlag() {

    }

    public String getSymbol() {
        return sSymbol;
    }

    public String getDBString() {
        return null;
    }

    public void resetUpdateFlag() {

    }

    public boolean getUpdateFlag() {
        return true;
    }

    public Object clone() throws CloneNotSupportedException {
        ObjectWatcher.register(this);
        return super.clone();
    }

    public synchronized long getSnapShotUpdatedTime() {
        return snapShotUpdatedtime;
    }

    public synchronized void setSnapShotUpdatedTime(long sUpatedTime) {
        snapShotUpdatedtime = sUpatedTime;
    }

    /*protected void finalize() throws Throwable {
        ObjectWatcher.unregister(this);
        super.finalize();
    }*/
    public String getXML() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<OHLCData>");
        buffer.append("<Symbol=\"").append(sSymbol).append("\"/>");
        buffer.append("<Time=\"").append(Time).append("\"/>");
        buffer.append("<Volume=\"").append(volume).append("\"/>");
        buffer.append("<Open=\"").append(open).append("\"/>");
        buffer.append("<High=\"").append(high).append("\"/>");
        buffer.append("<Low=\"").append(low).append("\"/>");
        buffer.append("<Close=\"").append(close).append("\"/>");
        buffer.append("</OHLCDaTa>\n");

        return buffer.toString();
    }
}