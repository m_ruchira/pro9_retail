package com.isi.csvr.ohlc;

import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class OHLCComparator implements Comparator {

    public OHLCComparator() {
    }

    public int compare(Object o1, Object o2) {
        IntraDayOHLC h1 = (IntraDayOHLC) o1;
        IntraDayOHLC h2 = (IntraDayOHLC) o2;

        if (h1.getTime() > h2.getTime())
            return 1;
        else if (h1.getTime() < h2.getTime())
            return -1;
        else
            return 0;
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}