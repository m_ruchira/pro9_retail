package com.isi.csvr;

import com.isi.csvr.shared.GUISettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 20-Dec-2007 Time: 09:41:55 To change this template use File | Settings
 * | File Templates.
 */
public class NewsProviderSelectionDialog extends JDialog implements ActionListener {
    private String providerID;

    public NewsProviderSelectionDialog(String title, String selExchange) throws HeadlessException {
        super(Client.getInstance().getFrame(), true);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createCompoundBorder(
                GUISettings.getTWFrameBorder(),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        setUndecorated(true);
        setLayout(new BorderLayout());

        JLabel label = new JLabel(title);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(label);

//        Hashtable<String,Hashtable> providers= NewsProvidersStore.getSharedInstance().getAllProvidersForExchange(selExchange);
//        Enumeration nProviders = providers.elements();
//        while (nProviders.hasMoreElements()) {
//            NewsProvider np=(NewsProvider)nProviders.nextElement();
//            JButton button = new JButton(np.getDescription());
//            button.setFont(button.getFont().deriveFont(Font.PLAIN));
//            button.setBorderPainted(false);
//            button.setContentAreaFilled(false);
//            button.setAlignmentX(Component.CENTER_ALIGNMENT);
//            button.setActionCommand(np.getID());
//            button.addActionListener(this);
//            button.setCursor(new Cursor(Cursor.HAND_CURSOR));
//            panel.add(button);
//
//        }

//        ArrayList<Exchange> list = new ArrayList<Exchange>();
//        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
//        while (exchanges.hasMoreElements()) {
//            Exchange exchange = (Exchange) exchanges.nextElement();
//            if (exchange.isDefault()) {
//                list.add(exchange);
//            }
//            exchange = null;
//        }
//
//        Collections.sort(list);

//        for (Exchange exchange: list) {
//            if (exchange.isDefault()) {
//                JButton button = new JButton(ExchangeStore.getSharedInstance().getDescription(exchange.getSymbol()));
//                button.setFont(button.getFont().deriveFont(Font.PLAIN));
//                button.setBorderPainted(false);
//                button.setContentAreaFilled(false);
//                button.setAlignmentX(Component.CENTER_ALIGNMENT);
//                button.setActionCommand(exchange.getSymbol());
//                button.addActionListener(this);
//                button.setCursor(new Cursor(Cursor.HAND_CURSOR));
//                panel.add(button);
//            }
//            exchange = null;
//        }
//        exchanges = null;
//        list = null;
        getContentPane().add(panel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(Client.getInstance().getFrame());
    }

    public String getSelectedProvider() {
        setVisible(true);
        return providerID;
    }

    public void actionPerformed(ActionEvent e) {
        providerID = e.getActionCommand();
        dispose();
    }
}
