package com.isi.csvr.parser;

/*
 * Licensed Material - Property of Matthew Hawkins (hawkini@barclays.net)
 *
 * GOLDParser - code ported from VB - Author Devin Cook. All rights reserved.
 *
 * No modifications to this code are allowed without the permission of the author.
 */

/**
 * -------------------------------------------------------------------------------------------<br>
 * <p/>
 * Source File:    LRAction.java<br>
 * <p/>
 * Author:         Devin Cook, Matthew Hawkins<br>
 * <p/>
 * Description:    An LALR Action.<br>
 * <p/>
 * <p/>
 * -------------------------------------------------------------------------------------------<br>
 * <p/>
 * Revision List<br>
 * <pre>
 *      Author          Version         Description
 *      ------          -------         -----------
 *      MPH             1.0             First Issue</pre><br>
 *
 * -------------------------------------------------------------------------------------------<br>
 *
 * IMPORT: NONE<br>
 *
 * -------------------------------------------------------------------------------------------<br>
 */
public class LRAction {
    /**
     * ************************************************************
     * <p/>
     * actionConstant
     * <p/>
     * This is the action that should be taken for this instance.
     * *************************************************************
     */
    public int actionConstant;
    /**
     * ************************************************************
     * <p/>
     * value
     * <p/>
     * The value of the action.
     * *************************************************************
     */
    public int value;      //shift to state, reduce rule, goto state
    private Symbol pSymbol;

    /**
     * ************************************************************
     * <p/>
     * getSymbolIndex
     * <p/>
     * This method will return the index of the smybol in the symbol
     * table.
     *
     * @return The index of the smybol in the symbol table.
     * *************************************************************
     */
    public int getSymbolIndex() {
        return pSymbol.getTableIndex();
    }

    /**
     * ************************************************************
     * <p/>
     * getSymbol
     * <p/>
     * This method will return the symbol associated with this action.
     *
     * @return The symbol associated with this action.
     * *************************************************************
     */
    public Symbol getSymbol() {
        return pSymbol;
    }

    /**
     * ************************************************************
     * <p/>
     * setSymbol
     * <p/>
     * This method will set the symbol of this action.
     *
     * @param sym The symbol to set for this action.
     *            *************************************************************
     */
    public void setSymbol(Symbol sym) {
        pSymbol = sym;
    }
}