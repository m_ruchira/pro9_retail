package com.isi.csvr.globalMarket;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 13, 2008
 * Time: 9:43:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class GlobalMarketRenderer extends TWBasicTableRenderer {
    private static Color g_oUpColor;
    private static Color g_oDownColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color maxBG;
    private static Color minBG;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private String g_sNA = "NA";
    private Color upColor;
    private Color downColor;
    private int tempInt;


    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oPriceFormatLong;
    private TWDecimalFormat oNumericFormat;
    private TWDecimalFormat oQuantityFormat;
    private DefaultTableCellRenderer lblRenderer;
    private double doubleValue;


    public GlobalMarketRenderer(int[] asRendIDs) {

        g_asRendIDs = asRendIDs;

        reload();
        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oPriceFormatLong = new TWDecimalFormat(" ###,##0.0000 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oNumericFormat = new TWDecimalFormat(" ########## ");

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;


    }

    public static void reloadForPrinting() {
        g_oUpColor = Color.black;
        g_oDownColor = Color.black;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oUpColor = Theme.getColor("GM_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("GM_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("GM_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("GM_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("GM_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("GM_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("GM_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("GM_TABLE_CELL_BGCOLOR2");
            maxBG = Theme.getColor("GM_TABLE_MAX_BGCOLOR");
            minBG = Theme.getColor("GM_TABLE_MIN_BGCOLOR");
        } catch (Exception e) {
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            upColor = g_oUpColor;
            downColor = g_oDownColor;
        }

        oPriceFormat = SharedMethods.getDecimalFormat((Integer) table.getModel().getValueAt(row, -4));

        try {
            if (!isSelected) { //Bug ID <#0018> row selection not visible if MIN / MAX color is selected
                tempInt = (Integer) table.getModel().getValueAt(row, -2);
                if (tempInt == Stock.MAX) {
                    background = maxBG;
                } else if (tempInt == Stock.MIN) {
                    background = minBG;
                }
            }
        } catch (Exception e) {
            // ignore
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        //int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        int iRendID = ((GlobalMarket) table.getModel()).getRenderingID(row, column);
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE

                    doubleValue = toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;

                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
                case 5: // CHANGE
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 6: // % CHANGE
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;

                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }


    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public void propertyChanged(int property) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
