package com.isi.csvr.globalMarket.commodities;

import com.isi.csvr.shared.Constants;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 6, 2008
 * Time: 3:07:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class ComoditiesRecord {

    private String exchange;
    private String symbol;
    private String description;
    private String value;
    private String cahnge;
    private String instrumentType;
    private int decimalCount = 2;

    public ComoditiesRecord() {

    }

    public ComoditiesRecord(String exchange, String symbol, String description) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.description = description;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCahnge() {
        return cahnge;
    }

    public void setCahnge(String cahnge) {
        this.cahnge = cahnge;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getKey() {
        return symbol + Constants.KEY_SEPERATOR_CHARACTER + exchange;
    }

    public int getDecimalCount() {
        return decimalCount;
    }

    public void setDecimalCount(int decimalCount) {
        this.decimalCount = decimalCount;
    }

}
