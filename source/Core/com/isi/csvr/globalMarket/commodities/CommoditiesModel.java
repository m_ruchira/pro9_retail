package com.isi.csvr.globalMarket.commodities;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.globalMarket.GlobalMarket;
import com.isi.csvr.globalMarket.MarketSummaryLoader;
import com.isi.csvr.globalMarket.RequestObject;
import com.isi.csvr.globalMarket.TableItemTabListener;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.table.CommonTable;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 09-Jun-2008 Time: 14:43:07 To change this template use File | Settings
 * | File Templates.
 */
public class CommoditiesModel extends CommonTable implements TableModel, TableItemTabListener, GlobalMarket {

    private ArrayList commoditiesTable;
    private Hashtable commoditesStore;
    private ComoditiesRecord record;
    private Vector requestList = new Vector();


    public CommoditiesModel(Hashtable commoditesStore) {
        this.commoditesStore = commoditesStore;
    }

    public CommoditiesModel() {

    }

    public void setEquityTable(ArrayList commoditiesTable) {
        this.commoditiesTable = commoditiesTable;
    }

    public int getRowCount() {
        if ((commoditiesTable != null) && commoditiesTable.size() > 0) {
            return commoditiesTable.size();
        } else {
            return 0;
        }
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }


    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];

    }

    public Class<?> getColumnClass(int columnIndex) {
        try {
            return getValueAt(0, columnIndex).getClass();
        } catch (Exception ex) {
            return Object.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            record = null;
            record = (ComoditiesRecord) commoditiesTable.get(rowIndex);

            switch (columnIndex) {
                case -4:
                    return record.getDecimalCount();
                case 0:
                    return record.getDescription();
                case 1:
                    return record.getValue();
                case 2:
                    return "" + record.getCahnge();

                default:
                    return "";
            }
        } catch (Exception ex) {
            return "";
        }


    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void setSymbol(String symbol) {
    }

    public void changeTab(String tabID) {
        try {
            this.commoditiesTable = new ArrayList();
            ArrayList symbols = (ArrayList) commoditesStore.get(tabID);
            for (int i = 0; i < symbols.size(); i++) {
                ComoditiesRecord cmr = (ComoditiesRecord) symbols.get(i);
                this.commoditiesTable.add(MarketSummaryLoader.getSharedInstance().getComoditiesSymbolStore().get(cmr.getKey()));
            }
            sendAddRequests((ArrayList) commoditesStore.get(tabID));
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void sendAddRequests(ArrayList commoditiesTable) {
        sendRemoveRequest();
        try {
            for (int i = 0; i < commoditiesTable.size(); i++) {
                ComoditiesRecord cmr = (ComoditiesRecord) commoditiesTable.get(i);
                if (!ExchangeStore.isExpired(cmr.getExchange()) && !ExchangeStore.isInactive(cmr.getExchange())) {
                    String request = Meta.GLOBAL_MARKET_ADD_REQUEST + Meta.DS + cmr.getExchange() + Meta.FD + cmr.getSymbol() + Meta.FD + cmr.getInstrumentType() + Meta.EOL;
                    requestList.add(new RequestObject(cmr.getExchange(), cmr.getSymbol(), cmr.getInstrumentType()));
                    if (Settings.getConnectionType() == Meta.CONN_TYPE_INTERNATIONAL) {
                        SendQFactory.addData(Meta.CONN_TYPE_INTERNATIONAL, request);
                    } else {
                        SendQFactory.writeData(request);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void sendRemoveRequest() {
        try {
            if (requestList.size() > 0) {
                for (int i = 0; i < requestList.size(); i++) {
                    RequestObject rqo = (RequestObject) requestList.get(i);
                    if (!ExchangeStore.isExpired(rqo.getExchange()) && !ExchangeStore.isInactive(rqo.getExchange())) {
                        String request = Meta.GLOBAL_MARKET_REMOVE_REQUEST + Meta.DS + rqo.getExchange() + Meta.FD + rqo.getSymbol() + Meta.FD + rqo.getInstrumentType() + Meta.EOL;
                        if (Settings.getConnectionType() == Meta.CONN_TYPE_INTERNATIONAL) {
                            SendQFactory.addData(Meta.CONN_TYPE_INTERNATIONAL, request);
                        } else {
                            SendQFactory.writeData(request);
                        }
                    }
                }

            }
            requestList.removeAllElements();
            requestList.trimToSize();

        } catch (Exception e) {

        }

    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iCol) {
            case 0:
                return 2;
            case 1:
                return 3;
            case 2:
                return 5;
            default:
                return 1;  //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    public void removeRequestsOnClose() {
        try {
            sendRemoveRequest();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}