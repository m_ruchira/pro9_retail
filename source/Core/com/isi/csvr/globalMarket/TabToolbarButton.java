package com.isi.csvr.globalMarket;

import com.isi.csvr.shared.ToolBarButton;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 10, 2008
 * Time: 6:06:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class TabToolbarButton extends ToolBarButton implements Themeable {

    private Image tabLeft;
    private Image tabRight;
    private Image tabCenter;
    private Image tabLeftSelected;
    private Image tabRightSelected;
    private Image tabCenterSelected;
    private int tabLeftWidth;
    private int tabRightWidth;
    private int tabCenterWidth;
    private int tabHeight;

    public TabToolbarButton(String text) {

        super(text);
        Theme.registerComponent(this);
        applyTheme();
    }

    public void applyTheme() {
        String currentLocationID = "dt_";
        // File file = new File("images/theme" + Theme.getID() + "/tab/" + locationID + "_tabLeft.jpg");
//        if (!file.exists()){
//            currentLocationID = "_";
//        }
        tabLeft = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabLeft.jpg")).getImage();
        tabLeftWidth = tabLeft.getWidth(this);
        tabLeftSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabLeftSelected.jpg")).getImage();

        tabRight = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabRight.jpg")).getImage();
        tabRightWidth = tabRight.getWidth(this);
        tabRightSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabRightSelected.jpg")).getImage();

        tabCenter = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabCenter.jpg")).getImage();
        tabCenterWidth = tabCenter.getWidth(this);
        tabCenterSelected = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + currentLocationID + "tabCenterSelected.jpg")).getImage();

        tabLeftWidth = tabLeft.getWidth(this);
        tabRightWidth = tabRight.getWidth(this);
        tabCenterWidth = tabCenter.getWidth(this);

        tabHeight = tabCenter.getHeight(this);
    }


    protected void paintComponent(Graphics g) {
        if (getComponentOrientation() == ComponentOrientation.LEFT_TO_RIGHT) {
            if (isSwitchOn()) {

                g.drawImage(tabLeftSelected, 0, 0, this);
                for (int i = tabLeftWidth; i < (getWidth() - tabRightWidth); i += tabCenterWidth) {
                    g.drawImage(tabCenterSelected, i, 0, this);
                }
                g.drawImage(tabRightSelected, getWidth() - tabRightWidth, 0, this);


            } else {

                g.drawImage(tabLeft, 0, 0, this);
                for (int i = tabLeftWidth; i < (getWidth() - tabRightWidth); i += tabCenterWidth) {
                    g.drawImage(tabCenter, i, 0, this);
                }
                g.drawImage(tabRight, getWidth() - tabRightWidth, 0, this);


            }


        } else {

            if (isSwitchOn()) {


                g.drawImage(tabRightSelected,
                        tabRightSelected.getWidth(this), 0, 0, tabRightSelected.getHeight(this),
                        0, 0, tabRightSelected.getWidth(this), tabRightSelected.getHeight(this), this);
                for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
                    g.drawImage(tabCenterSelected, i, 0, this);
                }
                g.translate(getWidth() - tabLeftWidth, 0);
                g.drawImage(tabLeftSelected,
                        tabLeftSelected.getWidth(this), 0, 0, tabLeftSelected.getHeight(this),
                        0, 0, tabLeftSelected.getWidth(this), tabLeftSelected.getHeight(this), this);
                g.translate(-getWidth() + tabLeftWidth, 0);
            } else {

                g.drawImage(tabRight,
                        tabRight.getWidth(this), 0, 0, tabRight.getHeight(this),
                        0, 0, tabRight.getWidth(this), tabRight.getHeight(this), this);
                for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
                    g.drawImage(tabCenter, i, 0, this);
                }
                g.translate(getWidth() - tabLeftWidth, 0);
                g.drawImage(tabLeft,
                        tabLeft.getWidth(this), 0, 0, tabLeft.getHeight(this),
                        0, 0, tabLeft.getWidth(this), tabLeft.getHeight(this), this);
                g.translate(-getWidth() + tabLeftWidth, 0);

            }


        }


        super.paintComponent(g);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
