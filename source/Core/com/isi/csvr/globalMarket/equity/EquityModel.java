package com.isi.csvr.globalMarket.equity;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.globalMarket.GlobalMarket;
import com.isi.csvr.globalMarket.MarketSummaryLoader;
import com.isi.csvr.globalMarket.RequestObject;
import com.isi.csvr.globalMarket.TableItemTabListener;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.table.CommonTable;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: Udaya Athukorala Date: Jun 6, 2008 Time: 8:09:01 PM To change this template use File
 * | Settings | File Templates.
 */
public class EquityModel extends CommonTable implements TableModel, TableItemTabListener, GlobalMarket {

    private ArrayList equityTable;
    private Hashtable equityStore;
    private EquityMarketRecord record;
    private Vector requestList = new Vector();


    public EquityModel(Hashtable equityStore) {
        this.equityStore = equityStore;
    }

    public void setEquityTable(ArrayList equityTable) {
        this.equityTable = equityTable;
    }

    public int getRowCount() {
        if ((equityTable != null) && equityTable.size() > 0) {
            return equityTable.size();
        } else {
            return 0;
        }
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }


    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];

    }

    public String[] getColumnNames() {
        return super.getViewSettings().getColumnHeadings();
    }

    public Class<?> getColumnClass(int columnIndex) {
        try {
            return getValueAt(0, columnIndex).getClass();
        } catch (Exception ex) {
            return Object.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            record = null;
            record = (EquityMarketRecord) equityTable.get(rowIndex);

            switch (columnIndex) {
                case -4:
                    return record.getDecimalCount();
                case 0:
                    return record.getDescription();
                case 1:
                    return record.getValue();
                case 2:
                    return "" + record.getCahnge();
                case 3:
                    return "" + record.getPercentageChange();
                default:
                    return "";
            }
        } catch (Exception ex) {
            return "";
        }


    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void setSymbol(String symbol) {
    }

    public void changeTab(String tabID) {
        try {
            this.equityTable = new ArrayList();
            ArrayList symbols = (ArrayList) equityStore.get(tabID);
            for (int i = 0; i < symbols.size(); i++) {
                EquityMarketRecord emr = (EquityMarketRecord) symbols.get(i);
                this.equityTable.add(MarketSummaryLoader.getSharedInstance().getEquityMatketSymbolStore().get(emr.getKey()));
            }
            sendAddRequests((ArrayList) equityStore.get(tabID));
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void sendAddRequests(ArrayList equityTable) {
        sendRemoveRequest();
        try {
            for (int i = 0; i < equityTable.size(); i++) {
                EquityMarketRecord emr = (EquityMarketRecord) equityTable.get(i);
                if (!ExchangeStore.isExpired(emr.getExchange()) && !ExchangeStore.isInactive(emr.getExchange())) {
                    String request = Meta.GLOBAL_MARKET_ADD_REQUEST + Meta.DS + emr.getExchange() + Meta.FD + emr.getSymbol() + Meta.FD + emr.getInstrumentType() + Meta.EOL;
                    requestList.add(new RequestObject(emr.getExchange(), emr.getSymbol(), emr.getInstrumentType()));
                    if (Settings.getConnectionType() == Meta.CONN_TYPE_INTERNATIONAL) {
                        SendQFactory.addData(Meta.CONN_TYPE_INTERNATIONAL, request);
                    } else {
                        SendQFactory.writeData(request);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void sendRemoveRequest() {
        try {
            if (requestList.size() > 0) {
                for (int i = 0; i < requestList.size(); i++) {
                    RequestObject rqo = (RequestObject) requestList.get(i);
                    if (!ExchangeStore.isExpired(rqo.getExchange()) && !ExchangeStore.isInactive(rqo.getExchange())) {
                        String request = Meta.GLOBAL_MARKET_REMOVE_REQUEST + Meta.DS + rqo.getExchange() + Meta.FD + rqo.getSymbol() + Meta.FD + rqo.getInstrumentType() + Meta.EOL;
                        if (Settings.getConnectionType() == Meta.CONN_TYPE_INTERNATIONAL) {
                            SendQFactory.addData(Meta.CONN_TYPE_INTERNATIONAL, request);
                        } else {
                            SendQFactory.writeData(request);
                        }
                    }
                }

            }
            requestList.removeAllElements();
            requestList.trimToSize();

        } catch (Exception e) {

        }

    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iCol) {
            case 0:
                return 2;
            case 1:
                return 3;
            case 2:
                return 5;
            case 3:
                return 6;
            default:
                return 0;
        }
    }

    public void removeRequestsOnClose() {
        try {
            sendRemoveRequest();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
