package com.isi.csvr.globalMarket.money;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 6, 2008
 * Time: 3:51:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class MoneyMarketRecord {

    private String exchange;
    private String symbol;
    private String description;
    private double price = 0;
    private String change = "N/A";
    private double yield = 0;
    private String yldChange = "N/A";
    private String instrumentType;
    private int decimalCount;

    public MoneyMarketRecord() {
    }

    public MoneyMarketRecord(String exchange, String symbol, String description) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.description = description;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public double getYield() {
        return yield;
    }

    public void setYield(double yield) {
        this.yield = yield;
    }

    public String getYldChange() {
        return yldChange;
    }

    public void setYldChange(String yldChange) {
        this.yldChange = yldChange;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public int getDecimalCount() {
        return decimalCount;
    }

    public void setDecimalCount(int decimalCount) {
        this.decimalCount = decimalCount;
    }
}
