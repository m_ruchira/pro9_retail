package com.isi.csvr.globalMarket.money;

import com.isi.csvr.globalMarket.GlobalMarket;
import com.isi.csvr.globalMarket.TableItemTabListener;
import com.isi.csvr.table.CommonTable;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 09-Jun-2008 Time: 14:43:35 To change this template use File | Settings
 * | File Templates.
 */
public class MoneyMarketModel extends CommonTable implements TableModel, TableItemTabListener, GlobalMarket {

    private ArrayList moneyTable;
    private Hashtable moneyStore;
    private MoneyMarketRecord record;
    private int decimalCount = 2;

    public MoneyMarketModel(Hashtable moneyStore) {
        this.moneyStore = moneyStore;
        if (moneyStore.size() == 1) {


            moneyTable = (ArrayList) moneyStore.get((String) (moneyStore.keys().nextElement()));
        }
    }

    public MoneyMarketModel() {
        moneyTable = new ArrayList();
        moneyTable.add(new MoneyMarketRecord("test 1", "test 1", "test 1"));
        moneyTable.add(new MoneyMarketRecord("test 2", "test 2", "test 2"));
        moneyTable.add(new MoneyMarketRecord("test 3", "test 3", "test 3"));
    }

    public void setEquityTable(ArrayList moneyTable) {
        this.moneyTable = moneyTable;
    }

    public int getRowCount() {
        if ((moneyTable != null) && moneyTable.size() > 0) {
            return moneyTable.size();
        } else {
            return 0;
        }
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }


    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];

    }

    public Class<?> getColumnClass(int columnIndex) {
        try {
            return getValueAt(0, columnIndex).getClass();
        } catch (Exception ex) {
            return Object.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            record = null;
            record = (MoneyMarketRecord) moneyTable.get(rowIndex);

            switch (columnIndex) {
                case -4:
                    return decimalCount;
                case 0:
                    return record.getDescription();
                case 1:
                    return "" + record.getPrice();
                case 2:
                    return "" + record.getChange();
                case 3:
                    return "" + record.getYield();
                case 4:
                    return "" + record.getYldChange();
                default:
                    return "";
            }
        } catch (Exception ex) {
            return "";
        }


    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void setSymbol(String symbol) {
    }

    public void changeTab(String tabID) {
        moneyTable = (ArrayList) moneyStore.get(tabID);
    }

    public int getRenderingID(int iRow, int iCol) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void removeRequestsOnClose() {

    }
}