package com.isi.csvr.globalMarket;

import com.isi.csvr.Client;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.globalMarket.forex.CombinedForexRecord;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.CommonTable;

import javax.swing.*;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 5, 2008
 * Time: 4:43:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class GlobalMarketSummaryWindow extends InternalFrame implements InternalFrameListener, ComponentListener, ApplicationListener {


    public static GlobalMarketSummaryWindow self;
    GlobalTableItem x;
    GMItemContainerPannel panel;
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private GlobalTableItem equityMarketTableItem;
    private GlobalTableItem comoditiesTableItem;
    private GlobalTableItem forextTableItem;
    private GlobalTableItem moneyMarketTableItem;
    private boolean isFirstTime = false;

    //view settings
    private ViewSetting equityViewSetting;
    private ViewSetting commoditiesViewSetting;
    private ViewSetting forexViewSetting;
    private ViewSetting moneymarketViewSetting;
    private ViewSetting globalMarketViewSetting;
    private int prevHeight = 0;
    private GlobalMarketSummary summaryRef;
    private boolean isloadedFromWsp;
    private boolean showeq;
    private boolean showcomo;
    private boolean showforex;


    private GlobalMarketSummaryWindow() {
        equityViewSetting = ViewSettingsManager.getSummaryView("GLOBALMARKET_EQUITY");
        commoditiesViewSetting = ViewSettingsManager.getSummaryView("GLOBALMARKET_COMMODITIES");
        forexViewSetting = ViewSettingsManager.getSummaryView("GLOBALMARKET_FOREX");
        moneymarketViewSetting = ViewSettingsManager.getSummaryView("GLOBALMARKET_MONEY");
        globalMarketViewSetting = ViewSettingsManager.getSummaryView("GLOBAL_MARKET_SUMMARY");
        addComponentListener(this);
        globalMarketViewSetting.setParent(this);
        this.setViewSetting(globalMarketViewSetting);
        createUI();
        isFirstTime = true;
        Application.getInstance().addApplicationListener(this);
        hideTitleBarMenu();


    }

    public static GlobalMarketSummaryWindow getSharedInstance() {
        if (self == null) {
            self = new GlobalMarketSummaryWindow();
        }
        return self;
    }

    public boolean isFirstTime() {
        return isFirstTime;
    }

    private void createUI() {
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle(Language.getString("GLOBAL_MARKET_SUMMARY"));
        setLocationRelativeTo(Client.getInstance().getDesktop());
        setLayer(GUISettings.TOP_LAYER);
        addTableItemsToFrame();
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
    }

    public void setSummaryRef(GlobalMarketSummary summaryRef) {
        this.summaryRef = summaryRef;
    }

    public void showFrame() {


        adjustSize();
        //  prevHeight = getHeight();
        validateShow();
        setVisible(true);
    }

    public void adjustSize() {

//          Dimension dim= panel.getPreferredSize();
//         int titleH = this.getTitlebarHeight();
//         int height = titleH + dim.height ;
//         int width = dim.width+4;
//         dim = new Dimension(width,height);
//        this.setSize(dim);
        this.pack();
        this.setMaximumSize(new Dimension(1000, getHeight()));
        this.setMinimumSize(new Dimension(50, getHeight()));
        globalMarketViewSetting.setSize(getSize());
//        globalMarketViewSetting.setLocation(this.getLocationOnScreen());
        // setVisible(true);
    }

//    public void adjustSizeSpecial(){
//          Dimension dim= panel.getPreferredSize();
//         int titleH = this.getTitlebarHeight();
//         int height = titleH + dim.height ;
//         int width = dim.width+4;
//         dim = new Dimension(width,height);
//        this.setSize(dim);
//
//
//    }

    public void componentResized(ComponentEvent e) {
        if (prevHeight > 0 && !isFirstTime) {
            // setSize(getWidth(),prevHeight);

        } else if (prevHeight == 0) {
            prevHeight = getHeight();
        }
        if (equityMarketTableItem != null && showeq) {
            equityMarketTableItem.setPreferedWidth(getWidth() - 14);         // 5+5 for empty border in GMItemContainerpanel
        }
        if (comoditiesTableItem != null && showcomo) {
            comoditiesTableItem.setPreferedWidth(getWidth() - 14);
        }
        if (forextTableItem != null && showforex) {
            forextTableItem.setPreferedWidth(getWidth() - 14);
        }

        //  super.componentResized(e);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void componentMoved(ComponentEvent e) {
        if (this.isShowing()) {
            globalMarketViewSetting.setLocation(this.getLocationOnScreen());
        }
    }


    public void workspaceLoaded() {
        try {

            boolean equity = Integer.parseInt(globalMarketViewSetting.getProperty(Constants.VC_GLOBAL_MARKET_EQUITY_OPEN)) == 1;
            boolean commodity = Integer.parseInt(globalMarketViewSetting.getProperty(Constants.VC_GLOBAL_MARKET_COMMODITY_OPEN)) == 1;
            boolean forex = Integer.parseInt(globalMarketViewSetting.getProperty(Constants.VC_GLOBAL_MARKET_FOREX_OPEN)) == 1;

            if (equity) {
                equityMarketTableItem.showTable();
            }
            if (commodity) {
                comoditiesTableItem.showTable();
            }
            if (forex) {
                forextTableItem.showTable();
            }
            //  loadTabsFromVseting();
            isFirstTime = false;
            isloadedFromWsp = true;


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void loadTabsFromVseting() {
        try {
            int eq = Integer.parseInt(globalMarketViewSetting.getProperty(Constants.VC_GLOBAL_MARKET_EQUITY_TAB));
            equityMarketTableItem.selectTab(eq);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            int commodity = Integer.parseInt(globalMarketViewSetting.getProperty(Constants.VC_GLOBAL_MARKET_COMMODITY_TAB));
            comoditiesTableItem.selectTab(commodity);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            int forex = Integer.parseInt(globalMarketViewSetting.getProperty(Constants.VC_GLOBAL_MARKET_FOREX_TAB));
            summaryRef.loadSelectedColFromViewSeting(forex);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void addTableItemsToFrame() {
        equityMarketTableItem = new GlobalTableItem(Language.getString("GLOBALMARKET_EQUITY"), equityViewSetting, Constants.EQUITY_TYPE);
        comoditiesTableItem = new GlobalTableItem(Language.getString("GLOBALMARKET_COMMODITIES"), commoditiesViewSetting, Constants.COMMODITIES_TYPE);
        forextTableItem = new GlobalTableItem(Language.getString("GLOBALMARKET_FOREX"), forexViewSetting, Constants.FOREX_TYPE);
        moneyMarketTableItem = new GlobalTableItem(Language.getString("GLOBALMARKET_MONEY"), moneymarketViewSetting, Constants.MONEYMARKET_TYPE);
        //   equityMarketTableItem.setPreferedWidth(equityViewSetting.getSize().width + 20);
        panel = new GMItemContainerPannel();
        panel.addGMTableItem(equityMarketTableItem);
        panel.addGMTableItem(comoditiesTableItem);
        panel.addGMTableItem(forextTableItem);
        //  panel.addGMTableItem(moneyMarketTableItem);
        JPanel pan2 = new JPanel();
        pan2.add(panel);

        //  this.add(pan2);
        this.add(panel);
        this.setSize(globalMarketViewSetting.getSize());
        this.setLocation(globalMarketViewSetting.getLocation());


    }

    public void setModels(CommonTable eqmodel, CommonTable cmodel, CommonTable formodel, CommonTable moneymodel) {

        equityMarketTableItem.setOModel((TableItemTabListener) eqmodel);
        comoditiesTableItem.setOModel((TableItemTabListener) cmodel);
        forextTableItem.setOModel((TableItemTabListener) formodel);
        moneyMarketTableItem.setOModel((TableItemTabListener) moneymodel);
        equityMarketTableItem.getDataTable().getTable().setDefaultRenderer(Object.class, new GlobalMarketRenderer(eqmodel.getRendIDs()));
        comoditiesTableItem.getDataTable().getTable().setDefaultRenderer(Object.class, new GlobalMarketRenderer(cmodel.getRendIDs()));
        forextTableItem.getDataTable().getTable().setDefaultRenderer(Object.class, new ForexTableRenderer());


    }

    public void applyWidths() {

        equityMarketTableItem.setPreferedWidth(globalMarketViewSetting.getSize().width - 14);         // 5+5 for empty border in GMItemContainerpanel
        comoditiesTableItem.setPreferedWidth(globalMarketViewSetting.getSize().width - 14);
    }


    public void setViewsettings(ViewSetting eqsetting, ViewSetting commoditiesSetting) {

        if (eqsetting == null) {
            eqsetting = equityViewSetting;
        }
        if (commoditiesSetting == null) {
            commoditiesSetting = commoditiesViewSetting;
        }
        equityMarketTableItem.setOSetting(eqsetting);
        comoditiesTableItem.setOSetting(commoditiesSetting);
        //  moneyMarketTableItem.setOSetting(moneySettins);


    }

    public GlobalTableItem getEquityMarketTableItem() {
        return equityMarketTableItem;
    }

    public GlobalTableItem getComoditiesTableItem() {
        return comoditiesTableItem;
    }

    public GlobalTableItem getForextTableItem() {
        return forextTableItem;
    }

    public GlobalTableItem getMoneyMarketTableItem() {
        return moneyMarketTableItem;
    }

    public void shoAllTables() {
        if (isFirstTime) {
            equityMarketTableItem.showTable();
            comoditiesTableItem.showTable();
            forextTableItem.showTable();
            moneyMarketTableItem.showTable();
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_EQUITY_TAB, 0);
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_COMMODITY_TAB, 0);
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_FOREX_TAB, 0);


            isFirstTime = false;
        }


    }

    public void updateTables() {
        equityMarketTableItem.getDataTable().updateGUI();
        equityMarketTableItem.getDataTable().getTable().repaint();
        comoditiesTableItem.getDataTable().updateGUI();
        comoditiesTableItem.getDataTable().getTable().repaint();
        forextTableItem.getDataTable().updateGUI();
        forextTableItem.getDataTable().getTable().repaint();


    }

    public void setTableOnOffInViewSetting(int tableType, boolean visable) {
        int v = 0;
        if (visable) {
            v = 1;
        }

        if (Constants.EQUITY_TYPE == tableType) {
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_EQUITY_OPEN, v);

        } else if (Constants.COMMODITIES_TYPE == tableType) {
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_COMMODITY_OPEN, v);

        } else if (Constants.FOREX_TYPE == tableType) {
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_FOREX_OPEN, v);

        } else if (Constants.MONEYMARKET_TYPE == tableType) {

        }


    }

    public void setSelectedTabInViewSetting(int tableType, int tab) {

        if (Constants.EQUITY_TYPE == tableType) {
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_EQUITY_TAB, tab);

        } else if (Constants.COMMODITIES_TYPE == tableType) {
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_COMMODITY_TAB, tab);

        } else if (Constants.FOREX_TYPE == tableType) {
            globalMarketViewSetting.putProperty(Constants.VC_GLOBAL_MARKET_FOREX_TAB, tab);

        }


    }

    public void applicationReadyForTransactions() {

//         equityMarketTableItem.getOModel().changeTab(equityMarketTableItem.getSelectedTab().getName());
//         comoditiesTableItem.getOModel().changeTab(comoditiesTableItem.getSelectedTab().getName());
        loadTabsFromVseting();
        if (isloadedFromWsp) {
            adjustSize();
            isloadedFromWsp = false;
        }

    }

    public boolean finalizeWindow() {
        ((GlobalMarket) equityMarketTableItem.getOModel()).removeRequestsOnClose();
        ((GlobalMarket) comoditiesTableItem.getOModel()).removeRequestsOnClose();
        CombinedForexRecord.getSharedInstance().removeRequestOnClose();

        return super.finalizeWindow();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void setShowableItems(boolean eq, boolean como, boolean forex) {

        showeq = eq;
        showcomo = como;
        showforex = forex;
//        equityMarketTableItem.setVisible(eq);
//        comoditiesTableItem.setVisible(como);
//        forextTableItem.setVisible(forex);

    }

    private void validateShow() {

        equityMarketTableItem.setVisible(showeq);
        comoditiesTableItem.setVisible(showcomo);
        forextTableItem.setVisible(showforex);

    }

    public class GMItemContainerPannel extends JPanel {


        private GlobalTableItem item1;
        private GlobalTableItem item2;
        private GlobalTableItem item3;
        private GlobalTableItem item4;


        public GMItemContainerPannel() {

            setOpaque(false);
            //  setBackground(Color.cyan);
            setPreferredSize(new Dimension(200, 100));
//            ColumnLayout cl = new ColumnLayout();
//            cl.createGap(5);
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));


        }


        public void setItem1(GlobalTableItem item1) {
            this.item1 = item1;
        }

        public void setItem2(GlobalTableItem item2) {
            this.item2 = item2;
        }

        public void setItem3(GlobalTableItem item3) {
            this.item3 = item3;
        }

        public void setItem4(GlobalTableItem item4) {
            this.item4 = item4;
        }

        public Dimension getPreferredSize() {
            int width = 0;
            int hight = 0;
            Component cmp[] = getComponents();
//        if(cmp.length>0) {
//        width  =cmp[0].getPreferredSize().width+ 10;
//        }else{
//            width = 200;
//        }

            for (int i = 0; i < cmp.length; i++) {
                if (cmp[i] instanceof GlobalTableItem) {
                    if (((GlobalTableItem) cmp[i]).isVisible()) {
                        hight = hight + (cmp[i]).getPreferredSize().height;
                    }

                } else {

                    hight = hight + (cmp[i]).getPreferredSize().height;
                }
                if (width < cmp[i].getPreferredSize().width) {
                    width = cmp[i].getPreferredSize().width;
                }

            }

            width = width + 10;
            hight = hight + 10;
            return new Dimension(width, hight);

        }

        public void addGMTableItem(GlobalTableItem comp) {
            super.add(comp);
            Dimension min = new Dimension();
            Dimension pref = new Dimension(comp.getPreferredSize().width, 5);
            Dimension max = new Dimension();
            Box.Filler gap = new Box.Filler(pref, pref, pref);

            super.add(gap);

            //To change body of overridden methods use File | Settings | File Templates.
        }
    }
}
