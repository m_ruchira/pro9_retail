package com.isi.csvr.globalMarket;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 12-Jun-2008 Time: 21:04:31 To change this template use File | Settings
 * | File Templates.
 */
public class RequestObject {
    String exchange = "";
    String symbol = "";
    String instrumentType = "";

    public RequestObject(String exchange, String symbol) {
        this.exchange = exchange;
        this.symbol = symbol;
    }

    public RequestObject(String exchange, String symbol, String instrumentType) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.instrumentType = instrumentType;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }
}
