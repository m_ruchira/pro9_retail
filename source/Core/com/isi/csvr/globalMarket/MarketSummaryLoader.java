package com.isi.csvr.globalMarket;

import com.isi.csvr.globalMarket.commodities.ComoditiesRecord;
import com.isi.csvr.globalMarket.equity.EquityMarketRecord;
import com.isi.csvr.globalMarket.forex.CombinedForexRecord;
import com.isi.csvr.globalMarket.forex.ForexRecord;
import com.isi.csvr.globalMarket.money.MoneyMarketRecord;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.nanoxml.XMLElement;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 6, 2008
 * Time: 1:06:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarketSummaryLoader {

    public static MarketSummaryLoader self;
    private static int equityType = 1;
    private static int comoditiesType = 2;
    private static int forexType = 3;
    private static int moneyType = 4;
    private StringBuilder buffer;
    private String xmlString = "";
    private Hashtable equityMarketStore;
    private Hashtable equityMatketSymbolStore;
    private Hashtable comoditiesStore;
    private Hashtable comoditiesSymbolStore;
    private Hashtable forexStore;
    private Hashtable forexSymbolStore;
    private Hashtable moneyMarketStore;
    private Hashtable moneyMarketSymbolStore;
    private ViewSetting equitySetting;
    private ViewSetting commoditiesSetting;
    private ViewSetting moneyySetting;
    private String forexExchange;
    private int forexDecimal = 3;

    private MarketSummaryLoader() {
        equityMarketStore = new Hashtable();
        equityMatketSymbolStore = new Hashtable();
        comoditiesStore = new Hashtable();
        comoditiesSymbolStore = new Hashtable();
        forexStore = new Hashtable();
        forexSymbolStore = new Hashtable();
        moneyMarketStore = new Hashtable();
        moneyMarketSymbolStore = new Hashtable();

        boolean x = downloadXML();
        if (x) {
            processXML();
        }
    }

    public static MarketSummaryLoader getSharedInstance() {
        if (self == null) {
            self = new MarketSummaryLoader();
        }
        return self;
    }

    private boolean downloadXML() {


        try {
            BufferedReader in = new BufferedReader(new FileReader(Settings.SYSTEM_PATH + "/GlobalMarketSummary.xml"));
            String str;

            while ((str = in.readLine()) != null) {
                xmlString = xmlString + str;
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }


        //  System.out.println("srt= "+ xmlString) ;

        return true;

    }

    private void processXML() {

        if (xmlString != "") {

            XMLElement root = new XMLElement();
            root.parseString(xmlString);
            ArrayList<XMLElement> rootElements = root.getChildren();

            for (int i = 0; i < rootElements.size(); i++) {
                XMLElement rootItem = rootElements.get(i);
                String id = rootItem.getStringAttribute("ID");
                if (id.equals("INDEX")) {
                    processMarkets(rootItem, equityType);
                } else if (id.equals("COMMODITY")) {
                    processMarkets(rootItem, comoditiesType);
                } else if (id.equals("FOREX")) {
                    processMarkets(rootItem, forexType);
                } else if (id.equals("MONEY")) {
                    processMarkets(rootItem, moneyType);
                }
            }
        }
    }

    public void processMarkets(XMLElement rootItem, int type) {
        ArrayList<XMLElement> tableElemnts = rootItem.getChildren();
        ArrayList<XMLElement> tabs = null;

        if (tableElemnts.size() == 2) {
            if (tableElemnts.get(0).getStringAttribute("DESCR").equalsIgnoreCase("Column Names")) {
                try {
                    processColumnTypes(tableElemnts.get(0), type);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                tabs = tableElemnts.get(1).getChildren();
            } else {
                try {
                    processColumnTypes(tableElemnts.get(1), type);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                tabs = tableElemnts.get(0).getChildren();
            }

        } else if (tableElemnts.size() == 1 && tableElemnts.get(0).getStringAttribute("DESCR").equalsIgnoreCase("Tab Definition")) {
            tabs = tableElemnts.get(0).getChildren();
        } else if (tableElemnts.size() == 1 && tableElemnts.get(0).getStringAttribute("DESCR").equalsIgnoreCase("Column Names")) {
            try {
                processColumnTypes(tableElemnts.get(0), type);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            tabs = null;

        }
        if (tabs != null) {

            for (int i = 0; i < tabs.size(); i++) {
                XMLElement tabElement = tabs.get(i);
                String tabName = tabElement.getStringAttribute("DESCR");
                if (tabName.equals("")) {
                    tabName = rootItem.getStringAttribute("DESCR");
                }
                ArrayList<XMLElement> items = tabElement.getChildren();
                ArrayList list = new ArrayList();

                for (int j = 0; j < items.size(); j++) {
                    XMLElement symbol = items.get(j);

                    String exchange = symbol.getStringAttribute("EXCHANGE");
                    String symbolString = symbol.getStringAttribute("SYMBOL");
                    String discription = symbol.getStringAttribute("DESCR");
                    if (type == equityType) {
                        EquityMarketRecord record = new EquityMarketRecord(exchange, symbolString, discription);
                        // list.add(record);
                        String instrument = symbol.getStringAttribute("INSTRUMENT");
                        String decimalcount = symbol.getStringAttribute("DECIMALCOUNT");
                        record.setInstrumentType(instrument);
                        record.setDecimalCount(Integer.parseInt(decimalcount));
                        list.add(record);
                        String cmpstring = symbolString + Constants.KEY_SEPERATOR_CHARACTER + exchange;
                        equityMatketSymbolStore.put(cmpstring, record);


                    } else if (type == comoditiesType) {
                        ComoditiesRecord record = new ComoditiesRecord(exchange, symbolString, discription);
                        String instrument = symbol.getStringAttribute("INSTRUMENT");
                        String decimalcount = symbol.getStringAttribute("DECIMALCOUNT");
                        record.setInstrumentType(instrument);
                        record.setDecimalCount(Integer.parseInt(decimalcount));
                        list.add(record);
                        String cmpstring = symbolString + Constants.KEY_SEPERATOR_CHARACTER + exchange;

                        comoditiesSymbolStore.put(cmpstring, record);
                    } else if (type == forexType) {
                        if (forexExchange == null || forexExchange.isEmpty()) {
                            forexExchange = exchange;
                        }
                        String decimalcount = symbol.getStringAttribute("DECIMALCOUNT");
                        setForexDecimals(decimalcount);
                        ForexRecord record = new ForexRecord(exchange, symbolString, discription);
                        list.add(record);
                        String cmpstring = symbolString + Constants.KEY_SEPERATOR_CHARACTER + exchange;

                        forexSymbolStore.put(cmpstring, record);
                    } else if (type == moneyType) {
                        MoneyMarketRecord record = new MoneyMarketRecord(exchange, symbolString, discription);
                        String instrument = symbol.getStringAttribute("INSTRUMENT");
                        record.setInstrumentType(instrument);
                        list.add(record);
                        String cmpstring = symbolString + Constants.KEY_SEPERATOR_CHARACTER + exchange;

                        moneyMarketSymbolStore.put(cmpstring, record);
                    }


                }
                if (type == equityType) {
                    equityMarketStore.put(tabName, list);
                } else if (type == comoditiesType) {
                    comoditiesStore.put(tabName, list);
                } else if (type == forexType) {
                    forexStore.put(tabName, list);
                } else if (type == moneyType) {
                    moneyMarketStore.put(tabName, list);
                }

                System.out.println("tabs = " + tabElement + "\n");


            }
        }


    }

    public void processColumnTypes(XMLElement columns, int tabletype) {

        ArrayList<XMLElement> colElemnts = columns.getChildren();
        String[] colNames = new String[colElemnts.size()];
        int[][] colSetings = new int[colElemnts.size()][3];
        String[] percentageWidths = new String[colElemnts.size()];
        String rendID = "";
        for (int i = 0; i < colElemnts.size(); i++) {

            XMLElement column = colElemnts.get(i);
            String name = column.getStringAttribute("NAME");
            String type = column.getStringAttribute("TYPE");
            String widthP = column.getStringAttribute("WIDTH");

            colNames[i] = name;
            percentageWidths[i] = widthP;
            colSetings[i][0] = i;
            colSetings[i][1] = 0;
            colSetings[i][2] = i;

            rendID += "2";


        }

        if (tabletype == equityType) {

            equitySetting = new ViewSetting();
            equitySetting.setColumnHeadings(colNames);
            equitySetting.setColumnSettings(colSetings);
            equitySetting.setRenderingIDs(rendID);
            String[] val = {"0.4", "0.2", "0.2", "0.2"};
            equitySetting.setColumnWidthsx(percentageWidths);

        } else if (tabletype == comoditiesType) {
            commoditiesSetting = new ViewSetting();
            commoditiesSetting.setColumnHeadings(colNames);
            commoditiesSetting.setColumnSettings(colSetings);
            commoditiesSetting.setRenderingIDs(rendID);
            String[] val = {"0.5", "0.25", "0.25"};
            commoditiesSetting.setColumnWidthsx(percentageWidths);


        } else if (tabletype == moneyType) {

        }


    }

    public Hashtable getEquityMarketStore() {
        return equityMarketStore;
    }

    public Hashtable getEquityMatketSymbolStore() {
        return equityMatketSymbolStore;
    }

    public Hashtable getComoditiesStore() {
        return comoditiesStore;
    }

    public Hashtable getComoditiesSymbolStore() {
        return comoditiesSymbolStore;
    }

    public Hashtable getForexStore() {
        return forexStore;
    }

    public Hashtable getForexSymbolStore() {
        return forexSymbolStore;
    }

    public Hashtable getMoneyMarketStore() {
        return moneyMarketStore;
    }

    public Hashtable getMoneyMarketSymbolStore() {
        return moneyMarketSymbolStore;
    }

    public void setData(String exchange, String symbol, int instrumentType, int decimalFactor, String frame) {
        try {
            if (instrumentType == Meta.INSTRUMENT_INDEX) {
                setEquityData(exchange, symbol, decimalFactor, frame);
            } else if (instrumentType == Meta.INSTRUMENT_FUTURE || instrumentType == Meta.INSTRUMENT_OPTION) {
                setCommodityData(exchange, symbol, decimalFactor, frame);
            } else if (instrumentType == Meta.INSTRUMENT_FOREX) {
                setForexData(exchange, symbol, decimalFactor, frame);
            } else if (instrumentType == com.isi.csvr.globalMarket.Constants.MONEYMARKET_TYPE) {
                setMoneyMarketData(frame);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        GlobalMarketSummaryWindow.getSharedInstance().updateTables();
    }

    public void setEquityData(String exchanges, String symbols, int decimalFactor, String frame) {
        try {
            char tag;
            String symbol = symbols;
            String exchange = exchanges;
            String key = symbol + Constants.KEY_SEPERATOR_CHARACTER + exchange;
            EquityMarketRecord emr = (EquityMarketRecord) equityMatketSymbolStore.get(key);
            String[] dataArray = frame.split(Meta.FD);
            for (int i = 0; i < dataArray.length; i++) {
                tag = dataArray[i].charAt(0);
                switch (tag) {
                    case 'W':

                        break;
                    case 'F':
                        emr.setValue("" + (SharedMethods.getDouble(dataArray[i].substring(1))) / decimalFactor);
                        break;
                    case 'N':
                        emr.setCahnge("" + (SharedMethods.getDouble(dataArray[i].substring(1))) / decimalFactor);
                        break;
                    case 'O':
                        emr.setPercentageChange("" + (SharedMethods.getDouble(dataArray[i].substring(1))) / decimalFactor);
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    public void setCommodityData(String exchanges, String symbols, int decimalFactor, String frame) {
        try {
            char tag;
            String symbol = symbols;
            String exchange = exchanges;
            String key = symbol + Constants.KEY_SEPERATOR_CHARACTER + exchange;
            ComoditiesRecord cmr = (ComoditiesRecord) comoditiesSymbolStore.get(key);
            String[] dataArray = frame.split(Meta.FD);
            for (int i = 0; i < dataArray.length; i++) {
                tag = dataArray[i].charAt(0);
                switch (tag) {
                    case 'W':

                        break;
                    case 'F':
                        cmr.setValue("" + SharedMethods.getDouble(dataArray[i].substring(1)) / decimalFactor);
                        break;
                    case 'N':
                        cmr.setCahnge("" + SharedMethods.getDouble(dataArray[i].substring(1)) / decimalFactor);
                        break;

                }
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    public void setForexData(String exchanges, String symbols, int decimalfactor, String frame) {
        double fxValue = 0d;
        char tag;

        String[] dataArray = frame.split(Meta.FD);
        for (int i = 0; i < dataArray.length; i++) {
            tag = dataArray[i].charAt(0);
            switch (tag) {
                case 'F':
                    fxValue = SharedMethods.getDouble(dataArray[i].substring(1)) / decimalfactor;
                    break;

            }
        }
        if (symbols.contains("X:S")) {

            symbols = symbols.substring(symbols.indexOf("S") + 1);

        }
        if (fxValue > 0d) {
            CombinedForexRecord.getSharedInstance().getCombinedStore().put(symbols, fxValue);
        }


    }

    public void setMoneyMarketData(String frame) {

    }

    public ViewSetting getEquityViewSetting() {
        return equitySetting;
    }

    public ViewSetting getCommoditiesViewSetting() {
        return commoditiesSetting;
    }

    public ViewSetting getMoneyyViewSetting() {
        return moneyySetting;
    }

    public String getForexExchange() {
        return forexExchange;
    }

    public int getForexDecimals() {
        return forexDecimal;
    }

    private void setForexDecimals(String decimals) {

        try {
            forexDecimal = Integer.parseInt(decimals);
        } catch (NumberFormatException e) {
            forexDecimal = 4;
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
