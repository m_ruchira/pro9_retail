package com.isi.csvr.globalMarket.forex;

import com.isi.csvr.globalMarket.TableItemTabListener;
import com.isi.csvr.table.CommonTable;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 09-Jun-2008 Time: 14:43:18 To change this template use File | Settings
 * | File Templates.
 */
public class ForexModel extends CommonTable implements TableModel, TableItemTabListener {

    private ArrayList forexTable;
    private Hashtable forexStore;
    private ForexRecord record;
    private int decimalCount = 4;


    public ForexModel(Hashtable forexStore) {
        this.forexStore = forexStore;
        if (this.forexStore != null && this.forexStore.size() == 1) {

            forexTable = (ArrayList) (this.forexStore.get((String) this.forexStore.keys().nextElement()));

        }
    }


    public void setForexTable(ArrayList forexTable) {
        this.forexTable = forexTable;
    }

    public int getRowCount() {
        return 1;
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }


    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];

    }

    public Class<?> getColumnClass(int columnIndex) {
        try {
            return getValueAt(0, columnIndex).getClass();
        } catch (Exception ex) {
            return Object.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            record = null;
            record = (ForexRecord) forexTable.get(rowIndex);
            Hashtable fxHash = CombinedForexRecord.getSharedInstance().getCombinedStore();
            ArrayList fxhash = CombinedForexRecord.getSharedInstance().getOrderedArrayList();

//            return ""+fxhash.get(columnIndex);
            switch (columnIndex) {
                case -4:
                    return (Integer) CombinedForexRecord.getSharedInstance().getForexDecimals();
                default:
                    try {
                        return "" + fxHash.get(fxhash.get(columnIndex));
                    } catch (Exception e) {
                        return "0.0";
                    }
//                case 0:
//                    return ""+  fxHash.get(fxhash.get(columnIndex));
//                case 1:
//                    return "" +fxHash.get(fxhash.get(columnIndex));
//                case 2:
//                    return "" + fxHash.get(fxhash.get(columnIndex));
//                case 3:
//                    return "" +fxHash.get(fxhash.get(columnIndex));
//                case 4:
//                    return "" + fxHash.get(fxhash.get(columnIndex));
//                case 5:
//                    return "" + fxHash.get(fxhash.get(columnIndex));


            }
        } catch (Exception ex) {
            return "";
        }


    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void setSymbol(String symbol) {
    }

    public void changeTab(String tabID) {
        forexTable = (ArrayList) forexStore.get(tabID);
    }
}