package com.isi.csvr.globalMarket.forex;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.globalMarket.GlobalMarketSummaryWindow;
import com.isi.csvr.globalMarket.RequestObject;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 13-Jun-2008 Time: 16:08:58 To change this template use File | Settings
 * | File Templates.
 */
public class CombinedForexRecord {
    public static CombinedForexRecord self;
    private Hashtable<String, Double> fxhash;
    private Vector requestList;
    private ArrayList<String> keys;
    private String forexExchange = "";
    private int forexDecimals = 3;

    public CombinedForexRecord() {
        fxhash = new Hashtable<String, Double>();
        requestList = new Vector();
        keys = new ArrayList<String>();
    }

    public static CombinedForexRecord getSharedInstance() {
        if (self == null) {
            self = new CombinedForexRecord();
        }
        return self;
    }

    public void setForexExchange(String forexExchange) {
        this.forexExchange = forexExchange;
    }

    public ForexRecord getFXRecord(String key) {
        return null;
    }

    public Hashtable getCombinedStore() {
        return fxhash;
    }

    public ArrayList getCombinedArray() {
        try {
            ArrayList<Double> doubleAraay = new ArrayList<Double>();
            Enumeration eforex = fxhash.elements();
            while (eforex.hasMoreElements()) {
                doubleAraay.add((Double) eforex.nextElement());
            }
            return doubleAraay;
        } catch (Exception e) {
            return new ArrayList();
        }
    }

    public ArrayList<String> getOrderedArrayList() {
        return keys;
    }

    public void sendForexAddRequests(int colIndex) {
        sendRemoveRequest();
        fxhash.clear();
        keys.clear();
        try {
            ViewSetting oSeting = GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getModel().getViewSettings();
            String[] cols = removeHTMLTags(oSeting.getColumnHeadings());
            String firstPart = cols[colIndex];

            for (int i = 0; i < cols.length; i++) {
                if (!firstPart.equals(cols[i])) {
                    String request = Meta.GLOBAL_MARKET_ADD_REQUEST + Meta.DS + forexExchange + Meta.FD + firstPart + cols[i].trim() + Meta.FD + Meta.INSTRUMENT_FOREX + Meta.EOL;
                    requestList.add(new RequestObject(forexExchange, (firstPart + cols[i].trim()), (Meta.INSTRUMENT_FOREX + "")));
                    fxhash.put(firstPart + cols[i].trim(), 0d);
                    keys.add(firstPart + cols[i].trim());
                    if (Settings.getConnectionType() == Meta.CONN_TYPE_INTERNATIONAL) {
                        SendQFactory.addData(Meta.CONN_TYPE_INTERNATIONAL, request);
                    } else {
                        SendQFactory.writeData(request);
                    }
                } else {
                    fxhash.put(firstPart + cols[i].trim(), 1d);
                    keys.add(firstPart + cols[i].trim());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void sendRemoveRequest() {
        try {
            if (requestList.size() > 0) {
                for (int i = 0; i < requestList.size(); i++) {
                    RequestObject rqo = (RequestObject) requestList.get(i);
                    String request = Meta.GLOBAL_MARKET_REMOVE_REQUEST + Meta.DS + rqo.getExchange() + Meta.FD + rqo.getSymbol() + Meta.FD + Meta.INSTRUMENT_FOREX + Meta.EOL;
                    if (Settings.getConnectionType() == Meta.CONN_TYPE_INTERNATIONAL) {
                        SendQFactory.addData(Meta.CONN_TYPE_INTERNATIONAL, request);
                    } else {
                        SendQFactory.writeData(request);

                    }
                }

            }
            requestList.removeAllElements();
            requestList.trimToSize();

        } catch (Exception e) {

        }

    }

    public String[] removeHTMLTags(String cols[]) {

        for (int i = 0; i < cols.length; i++) {

            String header = cols[i];
            String realH = "";

            if (header.startsWith("<")) {

                realH = header.substring(9);
                realH = realH.substring(0, realH.indexOf("<"));
                cols[i] = realH;
            }


        }


        return cols;

    }

    public void removeRequestOnClose() {
        try {
            sendRemoveRequest();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public int getForexDecimals() {
        return forexDecimals;
    }

    public void setForexDecimals(int forexDecimals) {
        this.forexDecimals = forexDecimals;
    }
}
