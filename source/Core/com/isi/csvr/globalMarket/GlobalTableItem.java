package com.isi.csvr.globalMarket;

import com.isi.csvr.globalMarket.commodities.CommoditiesModel;
import com.isi.csvr.globalMarket.money.MoneyMarketModel;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.ToolBarButton;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA. User: Udaya Athukorala Date: Jun 5, 2008 Time: 5:31:10 PM To change this template use File
 * | Settings | File Templates.
 */
public class GlobalTableItem extends JPanel implements MouseListener, Themeable {


    private DataTable dataTable;
    private JLabel compoundHeading;
    private JLabel headingIcons;
    private JLabel tabIcons;
    private TablePanel dataTablePanel;
    private ToolBarButton btn1;
    private ToolBarButton showhide;
    private ToolBarButton title;
    private boolean showTable = false;
    private ArrayList tabs;
    private String name = "";
    private TabToolbarButton selectedTab;
    private ViewSetting oSetting;

    private Image tabLeft;
    private Image tabRight;
    private Image tabCenter;
    private Image tabLeftSelected;
    private Image tabRightSelected;
    private Image tabCenterSelected;
    private int tabLeftWidth;
    private int tabRightWidth;
    private int tabCenterWidth;
    private int tabHeight;

    // private CommonTable oModel;
    private TableItemTabListener oModel;
    private int tableType;

    public GlobalTableItem(String title, ViewSetting oSetting, int tableType) {
        this.setLayout(new BorderLayout());
        addMouseListener(this);
        tabs = new ArrayList(5);
        name = title;
        this.oSetting = oSetting;
        createUI(tableType);


    }


    public JLabel getCompoundHeading() {
        return compoundHeading;
    }

    public void createUI(int tableType) {

        this.tableType = tableType;
        compoundHeading = new JLabel();

        //contains the heading and the tabs
        compoundHeading.setPreferredSize(new Dimension(oSetting.getSize().width, 20));
        dataTablePanel = new TablePanel();
        compoundHeading.setOpaque(true);
        compoundHeading.setLayout(new BorderLayout());


        //The table name and the expand icon
        headingIcons = new JLabel();
        headingIcons.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 0));
        showhide = new ToolBarButton();
        showhide.setIcon(getIconFromString("TableExpand"));
        showhide.setRollOverIcon(getIconFromString("TableExpand"));
        showhide.setToggledIcon(getIconFromString("TableCloapse"));
        showhide.setToggleEnabled(true);
        showhide.setSwitchOn(showTable);
        showhide.setPreferredSize(new Dimension(20, 20));
        showhide.addMouseListener(this);
        title = new ToolBarButton(name);
        headingIcons.add(showhide);
        headingIcons.add(title);
        int width = showhide.getPreferredSize().width + title.getPreferredSize().width + 5;
        headingIcons.setPreferredSize(new Dimension(width, 20));

        //The tabs in the table
        tabIcons = new JLabel();
        if (Language.isLTR()) {
            tabIcons.setLayout(new FlowLayout(FlowLayout.RIGHT, 2, 0));
        } else {
            tabIcons.setLayout(new FlowLayout(FlowLayout.LEFT, 2, 0));
        }
        width = compoundHeading.getPreferredSize().width - headingIcons.getPreferredSize().width;
        tabIcons.setPreferredSize(new Dimension(width, 20));


        if (Language.isLTR()) {
            compoundHeading.add(headingIcons, BorderLayout.WEST);
        } else {
            compoundHeading.add(headingIcons, BorderLayout.EAST);
        }
        compoundHeading.add(tabIcons, BorderLayout.CENTER);
        this.add(compoundHeading, BorderLayout.NORTH);

        if (Constants.EQUITY_TYPE == tableType) {
            createEquityTable();
        } else if (Constants.COMMODITIES_TYPE == tableType) {
            createComoditiesTable();
        } else if (Constants.FOREX_TYPE == tableType) {
            createForexTable();
        } else if (Constants.MONEYMARKET_TYPE == tableType) {
            createMoneyMarketTable();
        }
        dataTable.setVisible(false);
        //   dataTable.setAutoResize(true);
        dataTable.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
//        dataTable.getTable().setBorder(BorderFactory.createEmptyBorder());
        dataTable.getPopup().setTMTavailable(false);
        dataTable.addComponentListener(dataTable);
        dataTablePanel.setLayout(new BorderLayout());
        dataTablePanel.add(dataTable, BorderLayout.CENTER);
        this.add(dataTablePanel, BorderLayout.CENTER);

        selectTab();
        applyTheme();
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
//        GUISettings.applyOrientation(dataTable.getTable());
        //      GUISettings.applyOrientation(tabIcons);


    }


    public void createEquityTable() {
        dataTable = new DataTable();
        dataTable.setOpaque(false);
//        oModel = new EquityModel();
//        ((CommonTable)oModel).setViewSettings(oSetting);
//        dataTable.setModel((CommonTable)oModel);
//        ((CommonTable)oModel).setTable(dataTable);
        //  dataTable.getSmartTable().setPreferredSize(new Dimension(compoundHeading.getPreferredSize().width, 400));
        //  this.add(dataTable, BorderLayout.SOUTH);


    }

    public void createComoditiesTable() {
        dataTable = new DataTable();
        dataTable.setOpaque(false);
        oModel = new CommoditiesModel();
//        oModel.setViewSettings(oSetting);
//        dataTable.setModel(oModel);
//        oModel.setTable(dataTable);
        // dataTable.setSize(new Dimension(compoundHeading.getPreferredSize().width, 200));

        // this.add(dataTable, BorderLayout.SOUTH);
    }

    public void createForexTable() {
        dataTable = new DataTable();

        dataTable.setOpaque(false);
//        oModel = new ForexModel();
//        oModel.setViewSettings(oSetting);
//        dataTable.setModel(oModel);
//        oModel.setTable(dataTable);
        //   dataTable.setSize(new Dimension(compoundHeading.getPreferredSize().width, 200));
        //  this.add(dataTable, BorderLayout.SOUTH);
    }

    public void createMoneyMarketTable() {
        dataTable = new DataTable();
        dataTable.setOpaque(false);
        oModel = new MoneyMarketModel();
//        oModel.setViewSettings(oSetting);
//        dataTable.setModel(oModel);
//        oModel.setTable(dataTable);
        //     dataTable.setSize(new Dimension(compoundHeading.getPreferredSize().width, 200));
        //   this.add(dataTable, BorderLayout.SOUTH);
    }

    public void showTable() {
        showTable = true;
        dataTable.setVisible(showTable);
        showHideTabs();
        updateUI();
        GlobalMarketSummaryWindow.getSharedInstance().adjustSize();
        GlobalMarketSummaryWindow.getSharedInstance().setTableOnOffInViewSetting(tableType, true);


    }

    public TabToolbarButton getSelectedTab() {
        return selectedTab;
    }

    private void selectTab() {
        if (tabs.size() > 0) {
            if (oModel != null) {

                oModel.changeTab(((ToolBarButton) tabs.get(0)).getName());
                //          GlobalMarketSummaryWindow.getSharedInstance().setSelectedTabInViewSetting(tableType,0);

            }

            selectedTab = (TabToolbarButton) tabs.get(0);
            selectedTab.setSwitchOn(true);
        }


    }


    public void selectTab(int tab) {

        if (tabs.size() > 0 && tab < tabs.size()) {

            if (oModel != null) {

                oModel.changeTab(((ToolBarButton) tabs.get(tab)).getName());
                GlobalMarketSummaryWindow.getSharedInstance().setSelectedTabInViewSetting(tableType, tab);

            }
            if (selectedTab != null) {
                selectedTab.setSwitchOn(false);

            }

            selectedTab = (TabToolbarButton) tabs.get(tab);
            selectedTab.setSwitchOn(true);


        }

    }

    public void setPreferedWidth(int width) {
        if (width > 0)
            compoundHeading.setPreferredSize(new Dimension(width, 20));

        dataTablePanel.setPreferredSize(new Dimension(width, dataTablePanel.getPreferredSize().height));
        //   dataTable.getTable().setPreferredSize(new Dimension(width-6,dataTablePanel.getPreferredSize().height-9-dataTable.getTable().getTableHeader().getHeight()));
        //      dataTable.setPreferredSize();
        width = compoundHeading.getPreferredSize().width - headingIcons.getPreferredSize().width;
        tabIcons.setPreferredSize(new Dimension(width, 20));
        tabIcons.doLayout();
        resizeCloumnSizes();
        updateUI();

    }

    private void addTabsToUi() {

        if (!tabs.isEmpty()) {

            for (int i = 0; i < tabs.size(); i++) {

                ToolBarButton tab = (ToolBarButton) tabs.get(i);
                tabIcons.add(tab);

            }


        }


    }

    public TableItemTabListener getOModel() {
        return oModel;
    }

    public void setOModel(TableItemTabListener model) {
        this.oModel = model;
        ((CommonTable) oModel).setViewSettings(oSetting);
        dataTable.setModel((CommonTable) oModel);
        ((CommonTable) oModel).setTable(dataTable);

        dataTable.getTable().getTableHeader().setReorderingAllowed(false);
        dataTable.getTable().setBorder(BorderFactory.createEmptyBorder());
        setHeaderRenderer();
    }

    public void setOSetting(ViewSetting oSetting) {
        this.oSetting = oSetting;
    }

    public void setHeaderRenderer() {
        dataTable.getTable().createDefaultColumnsFromModel();
        PlainHeaderRenderer renderer = new PlainHeaderRenderer(false, Language.isLTR());
        //renderer.setShowToolTip();
        TableColumnModel colmodel = dataTable.getTable().getColumnModel();
        ViewSetting oSeting = dataTable.getModel().getViewSettings();

        int n = oSeting.getColumnHeadings().length;
        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }


        dataTable.updateGUI();
        dataTable.updateUI();
        dataTable.getTable().getTableHeader().repaint();

        if (tableType != Constants.FOREX_TYPE) {
            int[] widthx = calculateWiths(oSeting);

            if (widthx != null) {

                int k = oSeting.getColumnHeadings().length;
                for (int j = 0; j < k; j++) {
                    colmodel.getColumn(j).setPreferredWidth(widthx[j]);
                }


            }
        } else {
            dataTable.setAutoResize(true);
        }

        dataTable.updateGUI();
        dataTable.updateUI();

        dataTable.getTable().getTableHeader().repaint();
        GUISettings.applyOrientation(dataTable.getTable());

    }

    public int[] calculateWiths(ViewSetting oSeting) {
        try {
            String[] colWidths = oSeting.getColumnWidthsx();
            //int width = dataTable.getTable().getPreferredSize().width;
            // int width = dataTablePanel.getPreferredSize().width;
            int width = compoundHeading.getPreferredSize().width - 8;
            int[] widthsx = new int[colWidths.length];
            int tot = 0;
            for (int i = 0; i < colWidths.length; i++) {

                Double factor = Double.parseDouble(colWidths[i]);


                if (i == colWidths.length - 1) {
                    widthsx[i] = width - tot;

                } else {

                    widthsx[i] = (int) Math.ceil(factor * width);
                }

                tot += (int) Math.ceil(factor * width);


            }

            return widthsx;

        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }


    }

    private void resizeCloumnSizes() {
        ViewSetting oSeting = dataTable.getModel().getViewSettings();
        int[] widthx = calculateWiths(oSeting);
        TableColumnModel colmodel = dataTable.getTable().getColumnModel();
        if (widthx != null) {

            int k = oSeting.getColumnHeadings().length;
            for (int j = 0; j < k; j++) {
                colmodel.getColumn(j).setPreferredWidth(widthx[j]);
            }

        }

        dataTable.updateGUI();
        dataTable.updateUI();
        dataTable.getTable().getTableHeader().repaint();
    }

    public void AddTab(TabToolbarButton tab) {
        if (!tabs.contains(tab)) {
            tab.setEnabled(false);
            tab.setVisible(false);

            tabs.add(tab);
            tabIcons.add(tab);
            tab.addMouseListener(this);
            selectTab();
        }

    }

    public void removeTab(ToolBarButton tab) {

        if (tabs.contains(tab)) {
            this.remove(tab);

        }


    }

    public void disableTab(ToolBarButton tab) {

        if (tabs.contains(tab)) {
            tab.setVisible(false);
            tab.setEnabled(false);


        }


    }

    public void setSelectdTab(ToolBarButton btn) {

        if (tabs.contains(btn)) {
            selectedTab = (com.isi.csvr.globalMarket.TabToolbarButton) btn;

            btn.setSwitchOn(true);
        }

    }


    public void showHideTabs() {
        if (!showTable) {

            for (int i = 0; i < tabs.size(); i++) {

                ToolBarButton tab = (ToolBarButton) tabs.get(i);
                tab.setVisible(false);
                tab.setEnabled(false);

            }


        } else {

            for (int i = 0; i < tabs.size(); i++) {

                ToolBarButton tab = (ToolBarButton) tabs.get(i);
                tab.setVisible(true);
                tab.setEnabled(true);

            }

        }

        showhide.setSwitchOn(showTable);

    }

//    public void setTable(Table table) {
//
//        dataTable = table;
//        dataTable.updateUI();
//
//    }

    public void setTablePreferredScrollableViewportSize(Dimension d) {

    }

    public void updateUI() {

        this.doLayout();
        if (dataTablePanel != null) {
            dataTablePanel.doLayout();
        }
//        if(dataTable != null){
//        dataTable.updateGUI();
//        dataTable.updateUI();
//        }
//        this.getParent().doLayout();
        super.updateUI();

    }


    public Dimension getPreferredSize() {
        Dimension dim;
        if (showTable) {
            dataTable.getTable().getPreferredSize();
            dim = new Dimension(compoundHeading.getPreferredSize().width, (compoundHeading.getPreferredSize().height + dataTablePanel.getPreferredSize().height)); //dataTable.getPreferredSize().height+ dataTable.getTable().getTableHeader().getHeight()
            //   dataTable.getTable().setPreferredSize(new Dimension(compoundHeading.getPreferredSize().width-10, dataTablePanel.getPreferredSize().height));
            //    dataTable.getScrollPane().setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        } else {
            dim = compoundHeading.getPreferredSize();
        }
        //   updateUI();
        return dim;
    }

    public void mouseClicked(MouseEvent e) {

        if (e.getSource().equals(showhide)) {

            showTable ^= true;
//            showTable = !showTable;
            dataTable.setVisible(showTable);
            showHideTabs();
            updateUI();
            GlobalMarketSummaryWindow.getSharedInstance().adjustSize();
            GlobalMarketSummaryWindow.getSharedInstance().setTableOnOffInViewSetting(tableType, showTable);

        } else if (e.getSource() instanceof ToolBarButton) {

            if (selectedTab != null) {
                selectedTab.setSwitchOn(false);
            }

            selectedTab = (TabToolbarButton) e.getSource();
            GlobalMarketSummaryWindow.getSharedInstance().setSelectedTabInViewSetting(tableType, tabs.indexOf(selectedTab));
            selectedTab.setSwitchOn(true);
            dataTable.updateGUI();
            GlobalMarketSummaryWindow.getSharedInstance().adjustSize();

        }
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath("" + iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    public Table getDataTable() {
        return dataTable;
    }

//    public void setDataTable(Table dataTable) {
//        this.dataTable = dataTable;
//    }

    public void applyTheme() {

        compoundHeading.setBackground(Theme.getColor("GM_COMPOUNDHEAD_BGCOLOR"));


    }


    private class TablePanel extends JPanel {

        public Dimension getPreferredSize() {


            int width = 0;
            int hight = 0;
            Component cmp[] = getComponents();
//        if(cmp.length>0) {
//        width  =cmp[0].getPreferredSize().width+ 10;
//        }else{
//            width = 200;
//        }

            for (int i = 0; i < cmp.length; i++) {

                hight = hight + (cmp[i]).getPreferredSize().height;
                if (cmp[i] instanceof Table) {
                    hight = hight + ((Table) cmp[i]).getTable().getTableHeader().getHeight() + 6;

                }
                if (width < cmp[i].getPreferredSize().width) {
                    width = cmp[i].getPreferredSize().width + 6;
                }

            }


            return new Dimension(width, hight + 3);
        }
    }


    private class DataTable extends Table implements ComponentListener {
        int widhx;


        public Dimension getPreferredSize() {
            int width = getTable().getSize().width;

            Dimension d = super.getPreferredSize();


            return new Dimension(width, d.height);

        }


        public void componentResized(ComponentEvent e) {
            widhx = getSize().width;
        }

        public void componentMoved(ComponentEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void componentShown(ComponentEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void componentHidden(ComponentEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }


}

