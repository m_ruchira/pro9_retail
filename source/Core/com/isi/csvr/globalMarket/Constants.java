package com.isi.csvr.globalMarket;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 09-Jun-2008 Time: 14:25:47 To change this template use File | Settings
 * | File Templates.
 */
public class Constants {
    public static final int EQUITY_TYPE = 1;
    public static final int COMMODITIES_TYPE = 2;
    public static final int FOREX_TYPE = 3;
    public static final int MONEYMARKET_TYPE = 4;

    public static final int VC_GLOBAL_MARKET_EQUITY_OPEN = 1060;
    public static final int VC_GLOBAL_MARKET_EQUITY_TAB = 1061;
    public static final int VC_GLOBAL_MARKET_COMMODITY_OPEN = 1062;
    public static final int VC_GLOBAL_MARKET_COMMODITY_TAB = 1063;
    public static final int VC_GLOBAL_MARKET_FOREX_OPEN = 1064;
    public static final int VC_GLOBAL_MARKET_FOREX_TAB = 1065;


}
