// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

/**
 * Shows a JOptionpane in a thread, to display an un
 * blocking message
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWButton;

import javax.swing.FocusManager;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ShowMessage
        extends JOptionPane implements Runnable, FocusListener {

    private String g_sMessage;
    private String g_sHeading;
    private String g_sURL;
    private int g_iMessageType;
    private boolean g_bYesNo = false;
    private Object[] g_oOptions;
    private int g_iReturnValue;
    private Object g_oParent;

    private String g_sMessageType = null;
    private String g_sValue = null;
    private Thread thread;

    private boolean isMandatoryPopup = false;
    private boolean shortMode = false;
    private JDialog oDialog;

    /**
     * Constructor
     */
    public ShowMessage(String sMessage, String sMessageType) {
        g_sMessage = sMessage;

        g_sURL = null;

        initMessage(sMessageType);
        thread = new Thread(this, "ShowMessage");
        thread.start();
    }

    public ShowMessage(boolean bThread, String sMessage, String sMessageType) {
        g_sMessage = sMessage;
        g_sURL = null;

        initMessage(sMessageType);
        if (bThread) {
            thread = new Thread(this, "ShowMessage");
            thread.start();
        } else {
            run();
        }
    }

    /**
     * Constructor
     */
    public ShowMessage() {

    }

    /**
     * Constructor
     */
    public ShowMessage(String sMessage, String sMessageType, String sURL) {
        g_sMessage = sMessage;
        g_sURL = sURL;

        initMessage(sMessageType);
        thread = new Thread(this, "ShowMessage");
        thread.start();
    }

    /**
     * Constructor
     */
    public ShowMessage(String sMessage, String sMessageType, boolean bYesNo) {
        g_sMessage = sMessage;
        g_bYesNo = bYesNo;
        initMessage(sMessageType);
        run();
    }

    /**
     * Constructor for Mandatory Popups
     */
    public ShowMessage(String sMessage, String sMessageType, boolean bYesNo, boolean isMandatory) {
        g_sMessage = sMessage;
        g_bYesNo = bYesNo;
        isMandatoryPopup = isMandatory;
        initMessage(sMessageType);
        run();
    }

    public ShowMessage(Object oParent, String sMessage, String sMessageType,
                       boolean bYesNo) {
        g_oParent = oParent;
        g_sMessage = sMessage;
        g_bYesNo = bYesNo;
        initMessage(sMessageType);
        run();
    }

    public void showAletPopup(String title, String Description) {
        g_sMessage = title;
        setValue(Description);
        g_sURL = null;

        initMessage("A");
        thread = new Thread(this, "ShowMessage");
        thread.start();
    }

    public void showNewsPopup(String title, String Description) {
        g_sMessage = title;
        setValue(Description);
        g_sURL = null;

        initMessage("N");
        thread = new Thread(this, "ShowMessage");
        thread.start();
    }

    public int getMaxCharactersPerLineCount() {
        if (shortMode)
            return 100;
        else
            return 1000;
    }

    public void setValue(String value) {
        g_sValue = value;
    }

    /**
     * initialize the message by loading defaults
     */
    private void initMessage(String sMessageType) {

        TWButton oButton = getOKButton(this);
        TWButton oURLButton = getURLButton(this);
        Object[] options = {
                oButton};
        Object[] optionsURL = {
                oButton, oURLButton};
        Object[] optionsYesNo = {
                getYesButton(this),
                getNoButton(this)};
        Object[] optionsOkJump = {
                getOKButton(this),
                getOpenItem(this)};

        g_sMessageType = sMessageType;

        if (g_sURL == null)
            g_oOptions = options;
        else
            g_oOptions = optionsURL;

        if (g_bYesNo)
            g_oOptions = optionsYesNo;

        if (sMessageType.equals("I")) {
            g_iMessageType = JOptionPane.INFORMATION_MESSAGE;
            g_sHeading = Language.getString("INFORMATION");
        } else if (sMessageType.equals("W")) {
            g_iMessageType = JOptionPane.WARNING_MESSAGE;
            g_sHeading = Language.getString("WARNING");
        } else if (sMessageType.equals("A")) {
            g_iMessageType = JOptionPane.INFORMATION_MESSAGE;
            g_sHeading = Language.getString("ANNOUNCEMENT_ALERT");
            g_oOptions = optionsOkJump;
        } else if (sMessageType.equals("N")) {
            g_iMessageType = JOptionPane.INFORMATION_MESSAGE;
            g_sHeading = Language.getString("MSG_TITLE_NEWS");
            g_oOptions = optionsOkJump;
        } else {
            g_iMessageType = JOptionPane.ERROR_MESSAGE;
            g_sHeading = Language.getString("ERROR");
        }
    }


    public void run() {

        if (Settings.isConnected() && Settings.isScrollingOn() && (!isMandatoryPopup))
            return;

        Window w = FocusManager.getCurrentManager().getFocusedWindow();
        if (w != null) {
            g_oParent = w;
        } else {
            g_oParent = Client.getInstance().getFrame();
        }

        setMessage(g_sMessage);
        setMessageType(g_iMessageType);
        if (g_sMessage.toUpperCase().startsWith("<HTML>"))
            shortMode = false;
        else
            shortMode = true;
        setOptions(g_oOptions);
        setInitialValue(g_oOptions[0]);
        setInitialSelectionValue(g_oOptions[0]);
        selectInitialValue();
        oDialog = createDialog((Component) g_oParent, g_sHeading);
        oDialog.addFocusListener(this); // see comment in "focusGained()"

        GUISettings.setLocationRelativeTo(oDialog, Client.getInstance().getFrame());
        GUISettings.applyOrientation(oDialog);
        oDialog.show();


    }

    public int getReturnValue() {
        return g_iReturnValue;
    }

    private TWButton getOKButton(final JOptionPane oPane) {
        final TWButton oButton = new TWButton(Language.getString("OK"));

        ActionListener oListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                oPane.setValue(oButton.getText());
            }
        };

        KeyAdapter oKeyAdapter = new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    oPane.setValue(oButton.getText());
            }
        };
        oButton.addActionListener(oListener);
        oButton.addKeyListener(oKeyAdapter);
        return oButton;
    }

    private TWButton getURLButton(final JOptionPane oPane) {
        final TWButton oButton = new TWButton(Language.getString("GOTO_URL"));

        ActionListener oListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                oPane.setValue(oButton.getText());
                try {
                    Runtime.getRuntime().exec("start " + g_sURL);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        };

        KeyAdapter oKeyAdapter = new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    oPane.setValue(oButton.getText());
                    try {
                        Runtime.getRuntime().exec("start " + g_sURL);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        };
        oButton.addActionListener(oListener);
        oButton.addKeyListener(oKeyAdapter);
        return oButton;
    }

    private TWButton getOpenItem(final JOptionPane oPane) {
        final TWButton oButton = new TWButton(Language.getString("OPEN_ITEM"));

        /*ActionListener oListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                oPane.setValue(oButton.getText());
                try {
                    if (g_sMessageType.equals("A")) {
                        if (Settings.isConnected()) {
                            AnnouncementStore.getSharedInstance().showAnnouncementAlert(g_sValue);
                        }
                    }
                    else { // N for news
                        try {
                            Runtime.getRuntime().exec("Start " + g_sValue);
                        }
                        catch (Exception e2) {}
                    }
                }
                catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        };

        KeyAdapter oKeyAdapter = new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    oPane.setValue(oButton.getText());
                    try {
                        if (g_sMessageType.equals("A")) {
                            ( (Client) g_oParent).getBrowserFrame().setVisible(true);
                            ( (Client) g_oParent).getBrowserFrame().setLayer(10);
                            BrowserFrame.setURL(g_sValue);
                        }
                        else { // N
                            try {
                                Runtime.getRuntime().exec("Start " + g_sValue);
                            }
                            catch (Exception e2) {}
                        }
                    }
                    catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        };
        oButton.addActionListener(oListener);
        oButton.addKeyListener(oKeyAdapter);*/
        return oButton;
    }

    private TWButton getYesButton(final JOptionPane oPane) {
        final TWButton oButton = new TWButton(Language.getString("YES"));

        ActionListener oListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                oPane.setValue(oButton.getText());
                g_iReturnValue = 1;
            }
        };

        KeyAdapter oKeyAdapter = new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    oPane.setValue(oButton.getText());
                    g_iReturnValue = 1;
                }
            }
        };
        oButton.addActionListener(oListener);
        oButton.addKeyListener(oKeyAdapter);
        return oButton;
    }

    private TWButton getNoButton(final JOptionPane oPane) {
        final TWButton oButton = new TWButton(Language.getString("NO"));

        ActionListener oListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                oPane.setValue(oButton.getText());
                g_iReturnValue = 0;
            }
        };

        KeyAdapter oKeyAdapter = new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    oPane.setValue(oButton.getText());
                    g_iReturnValue = 0;
                }
            }
        };
        oButton.addActionListener(oListener);
        oButton.addKeyListener(oKeyAdapter);
        return oButton;
    }

    public void focusGained(FocusEvent e) {
        /* OPtion pane's focus model is buggy. Hence this method focuses the first button on option window focus */
        try {
            ((TWButton) g_oOptions[0]).requestFocus();
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void focusLost(FocusEvent e) {

    }

}