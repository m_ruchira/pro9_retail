package com.isi.csvr.variationmap;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: Jun 18, 2007
 * Time: 2:31:41 PM
 */
public class VariationMapUpdator extends Thread implements ExchangeListener, ApplicationListener {

    private static VariationMapUpdator self;
    private static Hashtable<String, Long> updatedTimes;
    private long currentupdatetime;

    private VariationMapUpdator() {
        super("VariationMapUpdator");
    }

    public static void activate() {
        self = new VariationMapUpdator();
        self.setPriority(Thread.NORM_PRIORITY - 3);
        updatedTimes = new Hashtable<String, Long>();
        ExchangeStore.getSharedInstance().addExchangeListener(self);
        Application.getInstance().addApplicationListener(self);
        self.start();
    }

    public static void setLastOHLCTime(long time, String sKey) {
        if (sKey != null && !sKey.isEmpty()) {
            updatedTimes.put(sKey, time);
        }

    }

    public static void reloadOHLC() {
        try {
            self.resetAllOHLCTimes();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void run() {
        int count = 0;
        while (true) {
            try {
                count++;
                DataStore.getSharedInstance().initFields();
                if (count == 10) {
                    count = 0;
                    updateOHLCMap();
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            } catch (Exception e) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    public void updateOHLCMap() {
        currentupdatetime = System.currentTimeMillis();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                Enumeration<String> symbols = DataStore.getSharedInstance().getSymbols(exchange.getSymbol());
                String symbol;
                String data;
                int instrument = -1;
                while (symbols.hasMoreElements()) {
                    try {
                        data = symbols.nextElement();
                        symbol = SharedMethods.getSymbolFromExchangeKey(data);
                        instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                        if (checkIfNewValuesAvailable(SharedMethods.getKey(exchange.getSymbol(), symbol, instrument))) {
                            DynamicArray temp = OHLCStore.getInstance().getOHLCValueArrayForCurrentDay(symbol, exchange.getSymbol(), instrument, exchange.getMarketDate());
                            if (temp != null && !temp.isEmpty()) {
                                IntraDayOHLC last = (IntraDayOHLC) temp.get(temp.size() - 1);
                                setLastOHLCTime(last.getTime(), SharedMethods.getKey(exchange.getSymbol(), symbol, instrument));
                            }
//                            else if(exchange.getMarketStatus() == Meta.MARKET_CLOSE){
//                                setLastOHLCTime(-2L,SharedMethods.getKey(exchange.getSymbol(), symbol, instrument));
//                            }

                            createValueArray(temp, symbol, exchange.getSymbol(), instrument);
                            temp = null;
                        }
                    } catch (Exception e) {
                    }
                }

            }
            exchange = null;
        }
    }

    public double[] createValueArray(DynamicArray array, String symbol, String exhange, int instrument) {
        double[] vals = new double[Stock.OHLC_MAP_COUNT + 2];
        try {
            vals[0] = Double.MAX_VALUE;
            vals[1] = Double.MIN_VALUE;
            Stock stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(exhange, symbol, instrument));

            for (int i = 0; i < array.size(); i++) {
                IntraDayOHLC ohlc = (IntraDayOHLC) array.get(i);
                vals[i + 2] = ohlc.getClose();
                vals[0] = Math.min(vals[0], ohlc.getClose());
                vals[1] = Math.max(vals[1], ohlc.getClose());
            }

            Arrays.fill(stock.ohlcMapvalues, 0);
            for (int i = 0; i < vals.length; i++) {
                stock.ohlcMapvalues[i] = vals[i];
            }
            stock = null;
        } catch (Exception e) {
        }

        return vals;
    }

    private boolean checkIfNewValuesAvailable(String sKey) {
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey));
        DynamicArray array = OHLCStore.getInstance().getIntradayRegister(sKey);
        if (!array.isEmpty() && exchange.getMarketStatus() == Meta.MARKET_OPEN) {
            // if (!array.isEmpty()  ) {
            Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
            IntraDayOHLC ohlc = (IntraDayOHLC) array.get(array.size() - 1);
            long ohlclast = getLastOHLCTime(sKey);

            if (ohlclast < ohlc.getTime()) {
                return true;
                //return true;
            } else {
                return false;
            }

        } else if (((exchange.getMarketStatus() == Meta.MARKET_CLOSE) || (exchange.getMarketStatus() == Meta.MARKET_PRECLOSE)) && getLastOHLCTime(sKey) < 0) {
            setLastOHLCTime(getLastOHLCTime(sKey) + 1L, sKey);
            return true;
        } else {
            return false;
        }
    }

    private long getLastOHLCTime(String sKey) {
        if (!updatedTimes.containsKey(sKey)) {
            // updatedTimes.put(sKey, System.currentTimeMillis());
            updatedTimes.put(sKey, -2L);      // Checks for two times if mkt is closed
        }
        return updatedTimes.get(sKey);
    }

    private Hashtable getSpecificSymbols(String exchange) {
        Hashtable<String, Long> table = new Hashtable();
        Enumeration en = updatedTimes.keys();
        while (en.hasMoreElements()) {
            String key = (String) en.nextElement();
            if (key.startsWith(exchange)) {
                table.put(key, updatedTimes.get(key));
            }
        }
        return table;
    }

    private void resetAllOHLCTimes() {
        Enumeration en = updatedTimes.keys();
        while (en.hasMoreElements()) {
            String key = (String) en.nextElement();
            updatedTimes.put(key, -3l);
        }
    }

    public void exchangeAdded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesAdded(boolean offlineMode) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        resetAllOHLCTimes();
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        resetAllOHLCTimes();
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
