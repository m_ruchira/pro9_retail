package com.isi.csvr.variationmap;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 26, 2004
 * Time: 1:12:22 PM
 */
public class VariationImage extends ImageIcon {
    public static final int TYPE_BID_ASK = 0;
    public static final int TYPE_SPREAD = 1;
    public static final int TYPE_RANGE = 2;
    public static final int TYPE_CHANGE = 3;
    public static final int TYPE_TICK = 4;
    public static final int TICK_FILTER = 3;
    public static final int TYPE_OHLC_MAP = 5;
    public static final int TYPE_CASH_MAP = 6;
    public static final int TYPE_CASH_FLOW = 7;
    public static final int TYPE_COVERAGE_MAP = 8;
//    public static final int TYPE_FULL_MAP=8;

    private int type;
    private int height = 10;
    private int width = 50;
    private int CLEARENCE = 4;
    private int LOW_CLEARENCE = 2;
    private Double value = 0D;
    private int intValue = 0;
    private int imageWidth = 0;
    private String exchange;
    private double[] values;//=new double[13];
    private double[] fullMapValues = new double[7];


    public VariationImage() {
        fullMapValues[0] = 20;
        fullMapValues[6] = 70;
        imageWidth = width - (CLEARENCE * 2);
    }

    public int getIconHeight() {
        return height;
    }

    public int getIconWidth() {
        return imageWidth;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void setValue(int value) {
        this.intValue = value;
    }

    public void setValues(double[] vals) {
        this.values = vals;
    }

//    public void setValue(byte[] value){
//        this.byteData = value;
//    }

    public void setWidth(int width) {
        this.width = width;
        imageWidth = width - (CLEARENCE * 2);
    }

    public void setHeight(int height) {
        this.height = height;
        CLEARENCE = height / 4;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        int val;

        try {
//            g.setColor(Color.BLACK);
//            g.fillRect(0, 0, width, height);

            switch (type) {
                case TYPE_BID_ASK:
                    if (value > 1) {
                        x += ((imageWidth / 2));
                        y += CLEARENCE;
                        val = Math.max(Math.min((int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getBidAskMax() * 2)), imageWidth / 2), 1);
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(x, y, val, height - (CLEARENCE * 2));
                    } else if (value == 0) {
                        // do nothing
                    } else {
                        y += CLEARENCE;
                        //val = (int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getBidAskMin() * 2));
                        val = Math.max((int) (((1 - value) * (imageWidth / 2)) / 1.0001), 1);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(x + (imageWidth / 2) - val, y, val, height - (CLEARENCE * 2));
                    }
                    break;
                case TYPE_CASH_FLOW:
                    if (value > 1) {
                        x += ((imageWidth / 2));
                        y += CLEARENCE;
                        val = Math.max(Math.min((int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getMoneyFlowMax() * 2)), imageWidth / 2), 1);
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(x, y, val, height - (CLEARENCE * 2));
                    } else if (value == 0) {
                        // do nothing
                    } else {
                        y += CLEARENCE;
                        //val = (int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getBidAskMin() * 2));
                        val = Math.max((int) (((1 - value) * (imageWidth / 2)) / 1.0001), 1);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(x + (imageWidth / 2) - val, y, val, height - (CLEARENCE * 2));
                    }
                    break;
                case TYPE_SPREAD:
                    if (value > 0) {
                        x += ((imageWidth / 2));
                        y += CLEARENCE;
                        val = Math.max(Math.min((int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getSpreadMax() * 2)), imageWidth / 2), 1);
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(x, y, val, height - (CLEARENCE * 2));
                    } else if (value == 0) {
                        // do nothing
                    } else {
                        //x += CLEARENCE;
                        y += CLEARENCE;
                        val = Math.max(Math.min((int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getSpreadMin() * 2)), imageWidth / 2), 1);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(x + (imageWidth / 2) - val, y, val, height - (CLEARENCE * 2));
                    }
                    break;
                case TYPE_CHANGE:
                    if (value > 0) {
                        x += (imageWidth / 2);
                        y += CLEARENCE;
                        val = Math.max(Math.min((int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getChangeMax() * 2)), imageWidth / 2), 1);
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(x, y, val, height - (CLEARENCE * 2));
                    } else if (value == 0) {
                        // do nothing
                    } else {
                        //x += CLEARENCE;
                        y += CLEARENCE;
                        val = Math.max(Math.min((int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getChangeMin() * 2)), imageWidth / 2), 1);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(x + (imageWidth / 2) - val, y, val, height - (CLEARENCE * 2));
                    }
                    break;
                case TYPE_RANGE:
                    if (value > 0) {
                        x += CLEARENCE;
                        y += CLEARENCE;
                        val = Math.max(Math.min((int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getRangeMax())), imageWidth), 1);
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(x, y, val, height - (CLEARENCE * 2));
                    } else if (value == 0) {
                        // do nothing
                    }
                    break;
                case TYPE_TICK:
                    //x += CLEARENCE;
                    y += CLEARENCE;
                    int barHeight;
                    int offset;
                    int squareSize = Math.min(100, imageWidth / Stock.TICK_MAP_LENGTH);
                    for (int i = 0; i < Stock.TICK_MAP_LENGTH; i++) {
                        if (i == 0) {
                            barHeight = height - (CLEARENCE * 2);
                            offset = 0;
                        } else {
                            barHeight = height - (CLEARENCE * 2) - 4;
                            offset = 2;
                        }
                        if (((intValue & (TICK_FILTER << i * 2)) >> (i * 2)) == Settings.TICK_UP) {
                            g.setColor(Theme.BIDASK_UP_COLOR);
                            g.fillRect(x + (i * squareSize), y + offset, squareSize - 1, barHeight);
                        } else if (((intValue & (TICK_FILTER << i * 2)) >> (i * 2)) == Settings.TICK_DOWN) {
                            g.setColor(Theme.BIDASK_DOWN_COLOR);
                            g.fillRect(x + (i * squareSize), y + offset, squareSize - 1, barHeight);
//                        }else if (((intValue & (TICK_FILTER << i*2)) >> (i*2)) == Settings.TICK_NOCHANGE){
//                            g.setColor(Color.gray);
//                            g.drawRect(x+(i*squareSize),y+offset,squareSize-1,barHeight-1);
                        }
                        g.setColor(Color.gray);
                        g.drawRect(x + (i * squareSize), y + offset, squareSize - 1, barHeight - 1);
                    }
                    /*x += CLEARENCE;
                    y += CLEARENCE;
                    int barHeight;
                    int offset;
                    int squareSize = Math.min(100,imageWidth/byteData.length);
                    for (int i = 0; i < byteData.length; i++) {
                        if (i==0){
                            barHeight = height-(CLEARENCE*2);
                            offset = 0;
                        }else{
                            barHeight = height-(CLEARENCE*2)-4;
                            offset = 2;
                        }
                        if (byteData[i] == Settings.TICK_UP){
                            g.setColor(Color.green);
                            g.fillRect(x+(i*squareSize),y+offset,squareSize-1,barHeight);
                        }else if (byteData[i] == Settings.TICK_DOWN){
                            g.setColor(Color.red);
                            g.fillRect(x+(i*squareSize),y+offset,squareSize-1,barHeight);
                        }else if (byteData[i] == Settings.TICK_NOCHANGE){
                            g.setColor(Color.gray);
                            g.drawRect(x+(i*squareSize),y+offset,squareSize-1,barHeight-1);
                        }
                    }*/
                    break;
                case TYPE_OHLC_MAP: // by Chandika Kumara
                    if ((values != null) && (values.length > 0) && (values[1] != Double.MIN_VALUE) && (values[2] > 0)) {
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        double[] rangearray = new double[Stock.OHLC_MAP_COUNT + 2];
                        for (int i = 0; i < Stock.OHLC_MAP_COUNT + 2; i++) {
                            rangearray[i] = (values[i] - values[0]);
                        }
                        double yratio = ((height - 2) / rangearray[1]);
                        double xgap = (imageWidth - 4) / (double) Stock.OHLC_MAP_COUNT;
                        int[] xcordinates = new int[Stock.OHLC_MAP_COUNT];
                        for (int i = 0; i < Stock.OHLC_MAP_COUNT; i++) {
                            xcordinates[i] = 2 + (int) Math.round(xgap * i);
                        }
                        for (int i = 0; i < Stock.OHLC_MAP_COUNT - 1; i++) {
                            if (rangearray[i + 3] >= 0) {
                                g.drawLine(x + xcordinates[i], y + (int) ((height - 2) - (rangearray[i + 2]) * yratio) + 1,
                                        x + xcordinates[i + 1], y + (int) ((height - 2) - (rangearray[i + 3]) * yratio) + 1);
                            } else {
                                break;
                            }
                        }
                    }
                    break;
                case TYPE_CASH_MAP:
                    g.setColor(Theme.BIDASK_DOWN_COLOR);
                    g.fillRect(2, 2, width - 4, height - 4);
                    g.setColor(Theme.BIDASK_UP_COLOR);
                    g.fillRect(2, 2, (int) (value * (width - 4)), height - 4);
                    break;
                case TYPE_COVERAGE_MAP:

                    if (!Double.isNaN(value)) {
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(2, 2, width - 4, height - 4);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(2, 2, (int) (value * (width - 4)), height - 4);
                    }
                    break;
                /*case TYPE_OHLC_MAP:
                        g.setColor(Color.RED);
                        g.drawLine(2,2,2,height -2);
                        g.setColor(Color.GREEN);
                        g.drawLine(width - 2,2,width - 2,height -2);
                        g.setColor(Color.RED);
                        drawDelta(g, true,10,  height / 2);
                        g.setColor(Color.GREEN);
                        drawDelta(g, false, 40,  height / 2);
                    break;*/
            }
        } catch (Exception e) {
        }
    }

    private void drawDelta(Graphics g, boolean up, int x, int y) {
        if (up) {
            g.drawLine(x, y, x, y);
            g.drawLine(x - 1, y + 1, x + 1, y + 1);
            g.drawLine(x - 2, y + 2, x + 2, y + 2);
        } else {
            g.drawLine(x, y, x, y);
            g.drawLine(x - 1, y - 1, x + 1, y - 1);
            g.drawLine(x - 2, y - 2, x + 2, y - 2);
        }
    }
}
