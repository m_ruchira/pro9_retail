// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

/**
 * Displays given messages in a popup window.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.broadcastmessage.MessageWindow;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;

import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Vector;

public class MessageList {

    private static MessageList self = null;
    private Vector<Long> g_oMessageList;
    private LinkedList<Message> store;
    private String messageEn;
    private String messageAr;
    private String messageID;

    /**
     * Constructor
     */
    private MessageList() {
        store = new LinkedList<Message>();
        g_oMessageList = new Vector<Long>();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath() + "datastore/messages.mdf"));
            store = (LinkedList<Message>) in.readObject();
            in.close();
            in = null;
        } catch (Exception e) {
            store = new LinkedList<Message>();
            e.printStackTrace();
        }
        try {
            if (store.size() > 0) {
                for (Message message : store) {
                    g_oMessageList.add(Long.parseLong(message.getId()));
                }
                Collections.sort(g_oMessageList);
            }
        } catch (Exception e) {
        }
    }

    public static MessageList getSharedInstance() {
        if (self == null) {
            self = new MessageList();
        }
        return self;
    }

    public LinkedList<Message> getStore() {
        return store;
    }

    public long getLastID() {
        try {
            return g_oMessageList.get(g_oMessageList.size() - 1).longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Display the given message. The id of the messge is added to
     * the message list. if the id of the message is already in the
     * list, the message is discarded.
     * If the message comes with a tag  for the local
     * language setting, it's body will be displayed. But if there are
     * no local language tag is available, english part of the message
     * will be displayed. If neither of above are present, no message
     * will be displayed but the id is added to the list.
     * <p/>
     * Message format
     * id|tag1=body1|tag2=body2...
     * 013456|EN=Alo alo|AR=A(*&%^&%)
     */
    public synchronized void addMessageNew(String sMessage) {
        boolean newMessagesAvailable = false;
        try {
            System.out.println(sMessage);
            String[] records = sMessage.split(Meta.ID);
            Message message = null;
            for (String record : records) {

                String[] data = record.split(Meta.FD);
                messageID = data[0];
                message = new Message(messageID);
                if (g_oMessageList.contains(messageID)) {
                    return;
                } else {
                    message.setDate(data[1]);
                    message.setTitile((data[2]));
                    message.setBody((data[3]));
                    message.setExpired(!(Integer.parseInt(data[4]) == 1));
                    message.setNewMessage();
                    try {
                        if (!message.isExpired()) {
                            newMessagesAvailable = true;
//                            MessageWindow.getSharedInstance().showMessage(message);
//                            JOptionPane.showMessageDialog(Client.getInstance().getFrame(), body, title, JOptionPane.INFORMATION_MESSAGE);
                        }
                    } catch (Exception e) {
                    }
                    store.add(message);
                    if (store.size() > 50) {
                        store.removeFirst();
                    }
                    g_oMessageList.add(Long.parseLong(messageID));
                }
            }

            Collections.sort(store);

        } catch (Exception e) {
            e.printStackTrace();
            // malformed message
        }
        if (newMessagesAvailable) {
            if (MessageWindow.getSharedInstance().isIcon()) {
                try {
                    MessageWindow.getSharedInstance().setIcon(false);
                } catch (PropertyVetoException e) {
                    e.printStackTrace();
                }
            }
//        MessageWindow.getSharedInstance().setVisible(true);
            MessageWindow.getSharedInstance().showMessage(store.getLast());
        }
    }

//    /*public void addMessage(String sMessage) {
//        try {
//            System.out.println(sMessage);
//            String[] data = sMessage.split(Meta.FD);
//                messageID = data[0];
//                if (g_oMessageList.contains(messageID)) {
//                    return;
//                } else {
//                    messageEn = UnicodeUtils.getNativeString(data[1]);
//                    messageAr = UnicodeUtils.getNativeString(data[2]);
//                    if (Language.isLTR()) {
//                        showMessage(messageEn);
//                    } else {
//                        showMessage(messageAr);
//                    }
//                    g_oMessageList.add(Long.parseLong(messageID));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            // malformed message
//        }
//    }*/

    public void requestHistory() {
        System.out.println("broadcast message request send =" + Meta.BROADCAST_MSG_HISTORY + Meta.DS + getLastID() + Meta.FD + Settings.getUserID() + Meta.EOL);
        SendQFactory.addData(Constants.PATH_PRIMARY, Meta.BROADCAST_MSG_HISTORY + Meta.DS + getLastID() + Meta.FD + Settings.getUserID() + Meta.EOL);
//        /Sendq
    }

    /**
     * Displays the messgae
     * <p/>
     * private void showMessage(String sMessage) {
     * SharedMethods.showHTMLMessage("Message",sMessage, JOptionPane.INFORMATION_MESSAGE, JOptionPane.OK_OPTION);
     * }
     */

    public void save() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Settings.getAbsolutepath() + "datastore/messages.mdf"));
            out.writeObject(store);
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

