package com.isi.csvr;

/* (swing1.1) */


import com.isi.csvr.datastore.StockData;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.win32.SortArrow;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * @version 1.0 02/25/99
 */
public class SortButtonRenderer extends JButton implements TableCellRenderer {
    public static final int NONE = 0;
    private static SortArrow sortArrow = new SortArrow(NONE);
    public static final int DOWN = 1;
    public static final int UP = 2;
    private static final int COLUMN = 0;
    private static final int VALUE = 1;
    private int[] state = new int[2];
    private Font preferredFont = null;
    //    private static SortArrow sortArrowDown = new SortArrow(DOWN);
    private int selectedColumn;
    private boolean sortable;
    private Color darkColor = Color.red.darker();
    private Color lightColor = Color.red.brighter();
    private Color borderColor = Color.BLACK;
    private Color sorterColor = Color.YELLOW;

    private Color headerFGColor = Color.BLACK;

    private boolean showToolTip;
    private boolean isCustomThemeEnable;

    public SortButtonRenderer(boolean sortable) {
        state[0] = 0;
        state[1] = -1;
//        sortArrow = new SortArrow();

        this.sortable = sortable;
        this.setMargin(new Insets(0, 0, 0, 0));
        this.setHorizontalTextPosition(LEADING);
        this.setIcon(sortArrow);
        this.setFont(Theme.getDefaultFont());
        setContentAreaFilled(false);
        applyTheme();
    }

    public void setShowToolTip() {
        this.showToolTip = true;
    }

    public void applyTheme() {

        this.setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
        Color color = Theme.getOptionalColor("BOARD_TABLE_HEAD_BGCOLOR1");
        if (color != null) {
            lightColor = color;
        }
        color = Theme.getOptionalColor("BOARD_TABLE_HEAD_BGCOLOR2");
        if (color != null) {
            darkColor = color;
        }
        color = Theme.getOptionalColor("BOARD_TABLE_HEAD_SORT_COLOR");
        if (color != null) {
            sorterColor = color;
        }
        sortArrow.setColor(sorterColor);
//        sortArrowDown.setColor(sorterColor);

        color = Theme.getOptionalColor("BOARD_TABLE_HEAD_BORDER_COLOR");
        if (color != null) {
            borderColor = color;
        }

        //this.setFont(Theme.getDefaultFont());
        if (preferredFont != null) {
            this.setFont(new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.PLAIN, 10));
        }
        setBorder(BorderFactory.createLineBorder(borderColor, 1));
    }

    public void setHeaderFont(Font font) {
        this.setFont(font);
    }

    public void setHeaderFontFG(Color headerFG) {
//        this.setForeground(headerFG);
        headerFGColor = headerFG;
    }

    public boolean isCustomThemeEnable() {
        return isCustomThemeEnable;
    }

    public void setCustomThemeEnable(boolean customThemeEnable) {
        isCustomThemeEnable = customThemeEnable;
    }

    public String getToolTipText() {
        try {
            if (showToolTip) {
                if (selectedColumn == 118) {   //7,30,90 day avg volume
                    if (StockData.getVolumeWatcherMode() == Constants.VOLUME_WATCHER_SEVENDAY) {
                        return getColumnNameForVolume(Constants.VOLUME_WATCHER_SEVENDAY);
                    } else if (StockData.getVolumeWatcherMode() == Constants.VOLUME_WATCHER_THIRTYDAY) {
                        return getColumnNameForVolume(Constants.VOLUME_WATCHER_THIRTYDAY);
                    } else {
                        return getColumnNameForVolume(Constants.VOLUME_WATCHER_NINETYDAY);
                    }

                } else if (selectedColumn == 119) {  //7,30,90 day avg pct volume
                    if (StockData.getVolumeWatcherMode() == Constants.VOLUME_WATCHER_SEVENDAY) {
                        return getColumnNameForPctVolume(Constants.VOLUME_WATCHER_SEVENDAY);
                    } else if (StockData.getVolumeWatcherMode() == Constants.VOLUME_WATCHER_THIRTYDAY) {
                        return getColumnNameForPctVolume(Constants.VOLUME_WATCHER_THIRTYDAY);
                    } else {
                        return getColumnNameForPctVolume(Constants.VOLUME_WATCHER_NINETYDAY);
                    }
                } else {
                    return Language.getList("TABLE_COLUMN_TOOLTIPS")[selectedColumn];
                }
            } else
                return null;
        } catch (Exception e) {
            return null;
        }
    }

    public String getToolTipText(int k) {
        try {
            if (showToolTip) {

                return Language.getList("TABLE_COLUMN_TOOLTIPS")[selectedColumn];
            } else
                return null;
        } catch (Exception e) {
            return null;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        try {
            if (table != null) {
                this.selectedColumn = table.convertColumnIndexToModel(column);
            }


            if ((state[COLUMN] == this.selectedColumn) && (state[VALUE] != -1)) {
                if (state[VALUE] == DOWN) {
//                setIcon(sortArrowDown);
                    sortArrow.setDirection(DOWN);
                } else if (state[VALUE] == UP) {
//                setIcon(sortArrowUP);
                    sortArrow.setDirection(UP);
                }
            } else {
//            setIcon(null);
                sortArrow.setDirection(NONE);
            }

            if (isCustomThemeEnable) {
                this.setForeground(headerFGColor);
            } else {
                this.setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
            }
            this.setText((value == null) ? "" : value.toString());
            return this;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            //System.out.println("Table "+table.getColumnCount() + ", col1 ="+table.getColumnName(0) + " , col2 ="+table.getColumnName(2));
            //System.out.println("Object "+value);
            //System.out.println("row "+row);
            //System.out.println("column "+column);
            return null;
        }
    }

    public void unsort() {
        state[VALUE] = -1;
    }

    public int setSelectedColumn(int col) {
        if (col < 0) return 0;

        if (state[COLUMN] != col) {
            state[VALUE] = DOWN;
            state[COLUMN] = col;
        } else {
            if (state[VALUE] == DOWN) {
                state[VALUE] = UP;
            } else {
                state[VALUE] = DOWN;
            }
        }
        return state[VALUE];
    }

    public void setSelectedColumn(int col, int order) {
        if (col < 0) return;

        if (sortable) {
            state[VALUE] = order;
            state[COLUMN] = col;
        }
    }

    public void setBGColor(Color oBG) {
//        upButton.setBackground(oBG);
//        downButton.setBackground(oBG);
    }


    public void paint(Graphics g) {
        paintGradient(g, getWidth(), getHeight());
        super.paint(g);
    }

    private void paintGradient(Graphics g, int width, int height) {

        int halfheight = height;// / 2;
        for (int i = 0; i <= halfheight; i++) {
            g.setColor(new Color((((lightColor.getRed() * (halfheight - i)) + darkColor.getRed() * i) / halfheight),
                    (((lightColor.getGreen() * (halfheight - i)) + darkColor.getGreen() * i) / halfheight),
                    (((lightColor.getBlue() * (halfheight - i)) + darkColor.getBlue() * i) / halfheight)));
            g.drawLine(0, i, width, i);
        }
        /*for (int i = 0; i <= halfheight; i++) {
            g.setColor(new Color((((lightColor.getRed() * i) + darkColor.getRed() * (halfheight - i)) / halfheight),
                    (((lightColor.getGreen() * i) + darkColor.getGreen() * (halfheight - i)) / halfheight),
                    (((lightColor.getBlue() * i) + darkColor.getBlue() * (halfheight - i)) / halfheight)));
            g.drawLine(0, i + halfheight, width, i + halfheight);
        }*/

        /*if ((state[COLUMN] == column) && (state[VALUE] != -1)) {
            if (state[VALUE] == DOWN) {
                sortArrow.setDirection(DOWN);
                g.setColor(sorterColor);
                g.drawLine(0, 1, width, 1);
                g.drawLine(0, 2, width, 2);
                System.out.println(getText() + " " + DOWN);
            } else if (state[VALUE] == UP) {
                sortArrow.setDirection(UP);
               g.setColor(sorterColor);
                g.drawLine(0, height -3, width, height -3);
                g.drawLine(0, height -2, width, height -2);
                System.out.println(getText() + " " + UP);
            } else {
                sortArrow.setDirection(NONE);
                System.out.println(getText() + " " + NONE);
            }
        }*/


    }

    private String getColumnNameForVolume(int mode) {
        if (mode == 0) {
            return Language.getString("VOLUME_WATCH_7DAY_TOOLTIP");
        } else if (mode == 1) {
            return Language.getString("VOLUME_WATCH_30DAY_TOOLTIP");
        } else if (mode == 2) {
            return Language.getString("VOLUME_WATCH_90DAY_TOOLTIP");
        } else {
            return "Avg. Volume";
        }
    }

    private String getColumnNameForPctVolume(int mode) {
        if (mode == 0) {
            return Language.getString("VOLUME_WATCH_7DAYCHANGE_TOOLTIP");
        } else if (mode == 1) {
            return Language.getString("VOLUME_WATCH_30DAYCHANGE_TOOLTIP");
        } else if (mode == 2) {
            return Language.getString("VOLUME_WATCH_90DAYCHANGE_TOOLTIP");
        } else {
            return "Volume Chg";
        }
    }
}



