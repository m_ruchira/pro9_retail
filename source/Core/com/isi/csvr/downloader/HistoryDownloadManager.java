package com.isi.csvr.downloader;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.customindex.CustomIndexInterface;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.shared.*;

import java.io.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 31, 2005
 * Time: 11:25:13 AM
 */
public class HistoryDownloadManager implements ConnectionListener, ApplicationListener, Runnable {
    private static HistoryDownloadManager self = null;
    private static boolean initialized = false;
    private Hashtable<String, ArrayList<HistoryDownloader>> exchangeDownloderSrore;
    private Hashtable<String, Integer> downloadCount;
    private Hashtable<String, String> exchangeData;
    private boolean isNewHistoryFilesAvailable = false;
    private List<HistoryDownloadListner> listeners;
    private Thread thread;

    private HistoryDownloadManager() {
        listeners = Collections.synchronizedList(new ArrayList<HistoryDownloadListner>());
        exchangeDownloderSrore = new Hashtable<String, ArrayList<HistoryDownloader>>();
        downloadCount = new Hashtable<String, Integer>();
        exchangeData = new Hashtable<String, String>();
        ConnectionNotifier.getInstance().addConnectionListener(this);
        thread = new Thread(this, "HistoryDownloadManager");
        thread.start();
    }

    public static synchronized HistoryDownloadManager getSharedInstance() {
        if (!initialized) throw new RuntimeException("Error: HistoryDownloadManager not Initialized");

        if (self == null) {
            self = new HistoryDownloadManager();
        }
        return self;
    }

    public static void initialize() {
        initialized = true;
        Application.getInstance().addApplicationListener(getSharedInstance());
    }

    private ArrayList<HistoryDownloader> getDownloaderStore(String exchange) {
        ArrayList<HistoryDownloader> downloaders = exchangeDownloderSrore.get(exchange);
        if (downloaders == null) {
            downloaders = new ArrayList<HistoryDownloader>();
            exchangeDownloderSrore.put(exchange, downloaders);
        }
        return downloaders;
    }

    private void addDownloader(String exchange, HistoryDownloader downloader) {
        ArrayList<HistoryDownloader> downloaders = getDownloaderStore(exchange);

        // check if the downloader is already in
        try {
            for (HistoryDownloader item : downloaders) {
                if (item.getExchange().equals(downloader.getExchange()) &&
                        (item.getFileName().equals(downloader.getFileName()))) {
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println("Error while comparing downloaders: " + e.toString());
        }

        downloaders.add(downloader);

        synchronized (thread) {
            if (thread.getState() == Thread.State.WAITING) {
                try {
                    thread.notify();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void startNewDownloadSession(String exchange, String file, int type, long size) {

        // start a new download process
        HistoryDownloader downloader = new HistoryDownloader(exchange, file, type, size);
        addDownloader(exchange, downloader);
    }

    public synchronized void startDownload(String exchange, String file) {
        int count = 0;
        try {
            count = downloadCount.get(exchange);
        } catch (Exception e) {
            e.printStackTrace();
        }
        count = count + 1;
        System.out.println("--------------------------download count increases in exchange=" + exchange + " ,count = " + count);
        downloadCount.put(exchange, count);
    }

    public boolean isHistoryDataReady(String exchange) {
        try {
            if (downloadCount.get(exchange) == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public synchronized void downloadComplete(String exchange, String file) {
        int count = 0;
        try {
            count = downloadCount.get(exchange);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (count != 0) {
            count = count - 1;
        } else {
            count = 0;
        }
        downloadCount.put(exchange, count);
        fireHistoryDownloaded(exchange);
        if (count == 0) {
            HistoryArchiveRegister.getSharedInstance().addEntry(exchange, Constants.HISTORY_DOWNLOADER_EOD_NAME, Long.parseLong(exchangeData.get(exchange)), HistoryArchiveFile.END);
        }
        System.out.println("--------------------------download count decreases in exchange=" + exchange + " ,count = " + count);
    }


    public void addHistoryDownloadListner(HistoryDownloadListner listener) {
        listeners.add(listener);
    }

    public void removeHistoryDownloadListner(HistoryDownloadListner listener) {
        listeners.remove(listener);
    }

    private void fireHistoryDownloaded(String exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                HistoryDownloadListner listener = listeners.get(i);
                listener.historyDownloaded(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireHistoryIntradayDownloaded(String exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                HistoryDownloadListner listener = listeners.get(i);
                listener.historicalIntradayDownload(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setFileList(String fileList) {

        try {
            String[] fields = fileList.split(Meta.FD);

            String exchange = fields[0];
            int type;
            String name;
            long size;
            String record;
            String[] fileData;

            Exchange exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
            if ((exchangeObject == null) || (!exchangeObject.isDefault()) || (exchangeObject.isExpired()) || (exchangeObject.isInactive())) { // check if the exchange is subscribed to
                return; // u are not subscribed or not a default market
            }
            downloadCount.put(exchange, 0);

            for (int i = 2; i < fields.length; i++) {
                record = fields[i];
                fileData = record.split(",");
                type = Integer.parseInt(fileData[0]);
                name = fileData[1];
                size = Long.parseLong(fileData[2]);

                if (type == Meta.OHLC_HISTORY_FILE) {
                    exchangeObject.setLastIntradayHistorydate(name);
                }

                if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, name, size)) {
                    if (type == Meta.YTD_HISTORY_FILE || type == Meta.ARCHIVE_FILE || type == Meta.RECENT_HISTORY_FILE)
                        startDownload(exchange, name);
                    if (!HistoryArchiveRegister.getSharedInstance().isInProgress(exchange, name, size)) {
                        startNewDownloadSession(exchange, name, type, size);
                    } else {
                    }
                } else {
                }
                fileData = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAdvFileList(String fileList) {

        try {
            String[] fields = fileList.split(Meta.FD);

            String exchange = fields[0];
            long exchangeEOD = Long.parseLong(fields[1]);
            int type;
            String name;
            long size;
            String record;
            String[] fileData;

            Exchange exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
            if ((exchangeObject == null) || (!exchangeObject.isDefault()) || (exchangeObject.isExpired()) || (exchangeObject.isInactive())) { // check if the exchange is subscribed to
                return; // u are not subscribed or not a default market
            }
            downloadCount.put(exchange, 0);
            exchangeData.put(exchange, "" + exchangeEOD);

            for (int i = 2; i < fields.length; i++) {
                record = fields[i];
                fileData = record.split(",");
                type = Integer.parseInt(fileData[0]);
                name = fileData[1];
                size = Long.parseLong(fileData[2]);

                if (type == Meta.OHLC_HISTORY_FILE) {
                    if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, name, size)) {
                        exchangeObject.setLastIntradayHistorydate(name);
                        if (!HistoryArchiveRegister.getSharedInstance().isInProgress(exchange, name, size)) {
                            startNewDownloadSession(exchange, name, type, size);
                        }
                    }
                } else if (type == Meta.YTD_HISTORY_FILE || type == Meta.ARCHIVE_FILE || type == Meta.RECENT_HISTORY_FILE) {
                    if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, name, size)) {
                        startDownload(exchange, name);
                        if (!HistoryArchiveRegister.getSharedInstance().isInProgress(exchange, name, size)) {
                            startNewDownloadSession(exchange, name, type, size);
                        }
                    }
                } else if (type == Meta.SYMBOL_HISTORY_FILE) {
                    if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, name, size)) {
                        startDownload(exchange, name);
                        if (!HistoryArchiveRegister.getSharedInstance().isInProgress(exchange, name, size)) {
                            startNewDownloadSession(exchange, name, type, size);
                        }
                    }
                }
                fileData = null;
            }
            int count = 0;
            try {
                count = downloadCount.get(exchange);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (count == 0) {
                HistoryArchiveRegister.getSharedInstance().addEntry(exchange, Constants.HISTORY_DOWNLOADER_EOD_NAME, Long.parseLong(exchangeData.get(exchange)), HistoryArchiveFile.END);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isNewHistoryFileAvailable() {
        return isNewHistoryFilesAvailable;
    }

    public synchronized void setNewHistoryFileAvailable(boolean isNewFileAvailbale) {
        isNewHistoryFilesAvailable = isNewFileAvailbale;
    }

    public void createHistoryForNewIndex(String indexName) {
        CustomIndexInterface object = CustomIndexStore.getSharedInstance().getCustomIndexObject(SharedMethods.getSymbolFromKey(indexName));
        String exchange = object.getExchanges();
        String path = Settings.getAbsolutepath() + "history/" + exchange + "/";
        File file = new File(path);
        File[] fileList = file.listFiles();

        for (int i = 0; i < fileList.length; i++) {
            createHistoryForIndex(indexName, (fileList[i]).getPath());
        }
    }

    public void createHistoryForAllIndexes(String exchange, String path) {
        Enumeration<String> names = CustomIndexStore.getSharedInstance().getColumnNames();
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            CustomIndexInterface object = CustomIndexStore.getSharedInstance().getCustomIndexObject(name);
            if (object.getExchanges().equals(exchange)) {
                createHistoryForNewIndex(SharedMethods.getKey(CustomIndexStore.CUSTOM_INDEX, object.getSymbol(), Meta.INSTRUMENT_INDEX));
            }
        }
    }

    private void createHistoryForIndex(String indexName, String path) {
        FileInputStream fileIN;
        FileInputStream tempIN = null;
        FileOutputStream fileOut;
        BufferedWriter out;
        BufferedReader in;
        BufferedReader tmpin = null;
        boolean isdone;
        boolean iskeeptmp;
        boolean iskeepsymbol;
        boolean isFirsttmpLine = true;
        boolean isFirstSymbolLine = true;
        String previousKey = "";
        Hashtable<String, String> symbols = CustomIndexStore.getSharedInstance().getSymbolMap(SharedMethods.getSymbolFromKey(indexName));
        if (symbols != null) {
            Enumeration<String> keyList = symbols.keys();
            int size = symbols.size();
            int count = 0;
            while (keyList.hasMoreElements()) {
                String key = keyList.nextElement();
                String symbol = SharedMethods.getSymbolFromKey(key);
                String sline = null;
                String templine = null;
                File file = null;
                if (count != (size - 1)) {
                    file = new File(path + "/temp_" + count + "_.csv");
                } else {
                    file = new File(path + "/" + indexName + ".csv");
                }
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    fileIN = new FileInputStream(path + "/" + symbol + ".csv");
                    in = new BufferedReader(new InputStreamReader(fileIN));
                    fileOut = new FileOutputStream(file);
                    out = new BufferedWriter(new OutputStreamWriter(fileOut));
                    try {
                        if (count != 0) {
                            tempIN = new FileInputStream(path + "/temp_" + (count - 1) + "_.csv");
                            tmpin = new BufferedReader(new InputStreamReader(tempIN));
                        }
                    } catch (FileNotFoundException e) {
                        tmpin = null;
                        e.printStackTrace();
                    }
                    isdone = false;
                    iskeeptmp = false;
                    iskeepsymbol = false;
                    Date lineDate = null;
                    Date tempdate = null;
                    String tmpdate = "";
                    isFirsttmpLine = true;
                    isFirstSymbolLine = true;
                    float tmpclosedvalue = 0;
                    float fileclosedvalue = 0;
                    float tmpOpen = 0;
                    float tmpHigh = 0;
                    float tmpLow = 0;
                    float tmpClose = 0;
                    float tmpTurnover = 0;
                    float tmpPrevClosed = 0;
                    float tmpChange = 0;
                    float tmpPerChange = 0;
                    long tmpVolume = 0;
                    int tmpTrades = 0;
                    int symbolLinesRead = 0;
                    int tmpLinesRead = 0;
                    while (!isdone) {
                        try {
                            if (!iskeepsymbol) {
                                sline = in.readLine();
                                symbolLinesRead = symbolLinesRead + 1;
                            }
                            if ((count != 0) && (!iskeeptmp) && (tmpin != null)) {
                                templine = tmpin.readLine();
                                tmpLinesRead = tmpLinesRead + 1;
                            }
                            if (sline != null) {
                                lineDate = HistoryFilesManager.getDateHistoryRecord(sline);
                            }
                            if (symbolLinesRead > 1) {
                                isFirstSymbolLine = false;
                            }
                            if (tmpLinesRead > 1) {
                                isFirsttmpLine = false;
                            }
                            if (templine != null) {
                                tempdate = HistoryFilesManager.getDateHistoryRecord(templine);
                                StringTokenizer fields = new StringTokenizer(templine, Meta.FS);
                                tmpdate = fields.nextToken(); //date
                                tmpOpen = (SharedMethods.floatValue(fields.nextToken()));      // open
                                tmpHigh = (SharedMethods.floatValue(fields.nextToken()));            // high
                                tmpLow = (SharedMethods.floatValue(fields.nextToken()));             // low
                                tmpclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                tmpClose = tmpclosedvalue;
                                tmpPerChange = (SharedMethods.floatValue(fields.nextToken()));  // % chg
                                tmpChange = (SharedMethods.floatValue(fields.nextToken()));      // chg
                                tmpPrevClosed = (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                tmpVolume = (SharedMethods.longValue(fields.nextToken()));           // vol
                                tmpTurnover = (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                tmpTrades = (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                            }
                            if (count == 0) {
                                if (lineDate != null) {
                                    out.write(sline);
                                    out.write("\n");
                                } else {
                                    isdone = true;
                                }
                            } else {
                                if (lineDate != null && tempdate != null) {
                                    if (tempdate.compareTo(lineDate) == 0) {
                                        StringTokenizer fields = new StringTokenizer(sline, Meta.FS);
                                        tmpdate = fields.nextToken(); //date
                                        tmpOpen = tmpOpen + (SharedMethods.floatValue(fields.nextToken()));      // open
                                        tmpHigh = tmpHigh + (SharedMethods.floatValue(fields.nextToken()));            // high
                                        tmpLow = tmpLow + (SharedMethods.floatValue(fields.nextToken()));             // low
                                        fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                        tmpClose = tmpClose + fileclosedvalue;
                                        fields.nextToken();   // % chg
                                        fields.nextToken();          // chg
                                        tmpPrevClosed = tmpPrevClosed + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                        tmpVolume = tmpVolume + (SharedMethods.longValue(fields.nextToken()));           // vol
                                        tmpTurnover = tmpTurnover + (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                        tmpTrades = tmpTrades + (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                                        tmpChange = tmpClose - tmpPrevClosed;
                                        tmpPerChange = (tmpChange / tmpPrevClosed);
                                        String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                                + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                                + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                        out.write(str);
                                        out.write("\n");
                                        iskeepsymbol = false;
                                        iskeeptmp = false;
                                    } else if (tempdate.compareTo(lineDate) < 0) {
                                        if (!isFirstSymbolLine) {
                                            tmpOpen = tmpOpen + fileclosedvalue;      // open
                                            tmpHigh = tmpHigh + fileclosedvalue;            // high
                                            tmpLow = tmpLow + fileclosedvalue;             // low
                                            tmpClose = tmpClose + fileclosedvalue;
                                            tmpPrevClosed = tmpPrevClosed + fileclosedvalue;  // prev close
                                        } else {
                                            Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(key), SharedMethods.getExchangeFromKey(key), tmpdate);
                                            if ((vecData != null) && (vecData.size() > 0) && (vecData.elementAt(0) != null)) {
                                                StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                                oTokenizer.nextToken();         // Date
                                                oTokenizer.nextToken();         // open
                                                oTokenizer.nextToken();         // high
                                                oTokenizer.nextToken();         // low
                                                fileclosedvalue = Float.parseFloat(oTokenizer.nextToken());      //close
                                                tmpOpen = tmpOpen + fileclosedvalue;      // open
                                                tmpHigh = tmpHigh + fileclosedvalue;            // high
                                                tmpLow = tmpLow + fileclosedvalue;             // low
                                                tmpClose = tmpClose + fileclosedvalue;
                                                tmpPrevClosed = tmpPrevClosed + fileclosedvalue;  // prev close
                                                vecData = null;
                                            }
                                        }
                                        tmpChange = tmpClose - tmpPrevClosed;
                                        tmpPerChange = (tmpChange / tmpPrevClosed);
                                        String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                                + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                                + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                        out.write(str);
                                        out.write("\n");
                                        iskeepsymbol = true;
                                        iskeeptmp = false;
                                    } else {
                                        StringTokenizer fields = new StringTokenizer(sline, Meta.FS);
                                        tmpdate = fields.nextToken(); //date
                                        if (!isFirsttmpLine) {
                                            tmpOpen = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));      // open
                                            tmpHigh = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));            // high
                                            tmpLow = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));             // low
                                            fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                            tmpClose = tmpclosedvalue + fileclosedvalue;
                                            fields.nextToken();   // % chg
                                            fields.nextToken();          // chg
                                            tmpPrevClosed = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                        } else {
                                            Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(previousKey), SharedMethods.getExchangeFromKey(previousKey), tmpdate);
                                            if ((vecData != null) && (vecData.size() > 0) && (vecData.elementAt(0) != null)) {
                                                StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                                oTokenizer.nextToken();         // Date
                                                oTokenizer.nextToken();         // open
                                                oTokenizer.nextToken();         // high
                                                oTokenizer.nextToken();         // low
                                                tmpclosedvalue = Float.parseFloat(oTokenizer.nextToken());      //close
                                                tmpOpen = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));      // open
                                                tmpHigh = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));            // high
                                                tmpLow = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));             // low
                                                fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                                tmpClose = tmpclosedvalue + fileclosedvalue;
                                                fields.nextToken();   // % chg
                                                fields.nextToken();          // chg
                                                tmpPrevClosed = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                                vecData = null;
                                            } else {
                                                tmpOpen = (SharedMethods.floatValue(fields.nextToken()));      // open
                                                tmpHigh = (SharedMethods.floatValue(fields.nextToken()));            // high
                                                tmpLow = (SharedMethods.floatValue(fields.nextToken()));             // low
                                                fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                                tmpClose = fileclosedvalue;
                                                fields.nextToken();   // % chg
                                                fields.nextToken();          // chg
                                                tmpPrevClosed = (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                            }
                                        }
                                        tmpVolume = (SharedMethods.longValue(fields.nextToken()));           // vol
                                        tmpTurnover = (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                        tmpTrades = (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                                        tmpChange = tmpClose - tmpPrevClosed;
                                        tmpPerChange = (tmpChange / tmpPrevClosed);
                                        String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                                + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                                + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                        out.write(str);
                                        out.write("\n");
                                        iskeeptmp = true;
                                        iskeepsymbol = false;
                                    }
                                } else if (lineDate != null) {
                                    StringTokenizer fields = new StringTokenizer(sline, Meta.FS);
                                    tmpdate = fields.nextToken(); //date
                                    Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(previousKey), SharedMethods.getExchangeFromKey(previousKey), tmpdate);
                                    if ((vecData != null) && (vecData.size() > 0) && (vecData.elementAt(0) != null)) {
                                        StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                        oTokenizer.nextToken();         // Date
                                        oTokenizer.nextToken();         // open
                                        oTokenizer.nextToken();         // high
                                        oTokenizer.nextToken();         // low
                                        tmpclosedvalue = Float.parseFloat(oTokenizer.nextToken());      //close
                                        vecData = null;
                                    } else {
                                        tmpclosedvalue = 0;
                                    }
                                    tmpOpen = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));      // open
                                    tmpHigh = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));            // high
                                    tmpLow = tmpclosedvalue + (SharedMethods.floatValue(fields.nextToken()));             // low
                                    fileclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                    tmpClose = tmpclosedvalue + fileclosedvalue;
                                    fields.nextToken();   // % chg
                                    fields.nextToken();          // chg
                                    tmpPrevClosed = tmpPrevClosed + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                    tmpVolume = tmpVolume + (SharedMethods.longValue(fields.nextToken()));           // vol
                                    tmpTurnover = tmpTurnover + (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                    tmpTrades = tmpTrades + (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                                    tmpChange = tmpClose - tmpPrevClosed;
                                    tmpPerChange = (tmpChange / tmpPrevClosed);
                                    String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                            + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                            + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                    out.write(str);
                                    out.write("\n");
                                    iskeepsymbol = false;
                                } else if (tempdate != null) {
                                    StringTokenizer fields = new StringTokenizer(templine, Meta.FS);
                                    tmpdate = fields.nextToken(); //date
                                    Vector vecData = HistoryFilesManager.readHistoryDataForDate(SharedMethods.getSymbolFromKey(key), SharedMethods.getExchangeFromKey(key), tmpdate);
                                    if ((vecData != null) && (vecData.size() > 0) && (vecData.elementAt(0) != null)) {
                                        StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                                        oTokenizer.nextToken();         // Date
                                        oTokenizer.nextToken();         // open
                                        oTokenizer.nextToken();         // high
                                        oTokenizer.nextToken();         // low
                                        fileclosedvalue = Float.parseFloat(oTokenizer.nextToken());      //close
                                        vecData = null;
                                    } else {
                                        tmpclosedvalue = 0;
                                    }
                                    tmpOpen = fileclosedvalue + (SharedMethods.floatValue(fields.nextToken()));      // open
                                    tmpHigh = fileclosedvalue + (SharedMethods.floatValue(fields.nextToken()));            // high
                                    tmpLow = fileclosedvalue + (SharedMethods.floatValue(fields.nextToken()));             // low
                                    tmpclosedvalue = (SharedMethods.floatValue(fields.nextToken()));     // close
                                    tmpClose = tmpclosedvalue + fileclosedvalue;
                                    fields.nextToken();   // % chg
                                    fields.nextToken();          // chg
                                    tmpPrevClosed = tmpPrevClosed + (SharedMethods.floatValue(fields.nextToken()));  // prev close
                                    tmpVolume = tmpVolume + (SharedMethods.longValue(fields.nextToken()));           // vol
                                    tmpTurnover = tmpTurnover + (SharedMethods.floatValue(fields.nextToken()));        // turnover
                                    tmpTrades = tmpTrades + (SharedMethods.intValue(fields.nextToken(), 0));     // no of trades
                                    tmpChange = tmpClose - tmpPrevClosed;
                                    tmpPerChange = (tmpChange / tmpPrevClosed);
                                    String str = tmpdate + Meta.FS + tmpOpen + Meta.FS + tmpHigh + Meta.FS + tmpLow
                                            + Meta.FS + tmpClose + Meta.FS + tmpPerChange + Meta.FS + tmpChange + Meta.FS + tmpPrevClosed
                                            + Meta.FS + tmpVolume + Meta.FS + tmpTurnover + Meta.FS + tmpTrades;
                                    out.write(str);
                                    out.write("\n");
                                    iskeeptmp = false;
                                } else {
                                    iskeepsymbol = false;
                                    iskeeptmp = false;
                                    isdone = true;
                                }
                                if (!iskeeptmp) {
                                    templine = null;
                                }
                                if (!iskeepsymbol) {
                                    sline = null;
                                }
                            }
                            tempdate = null;
                            lineDate = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                            isdone = true;
                        }
                    }
                    in.close();
                    fileIN.close();
                    in = null;
                    fileIN = null;
                    try {
                        if (count != 0) {
                            tmpin.close();
                            tempIN.close();
                            tmpin = null;
                            tempIN = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    out.flush();
                    out.close();
                    fileOut.close();
                    out = null;
                    fileOut = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    File fileold = new File(path + "/temp_" + (count - 1) + "_.csv");
                    fileold.delete();
                    fileold = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
                symbol = null;
                previousKey = key;
                key = null;
            }
            keyList = null;
        }
    }

    public void run() {
        while (true) {
            try {
                Enumeration<ArrayList<HistoryDownloader>> downloaders = exchangeDownloderSrore.elements();
                while (downloaders.hasMoreElements()) {
                    ArrayList<HistoryDownloader> list = downloaders.nextElement();
                    if ((list.size() > 0) && (!list.get(0).isInprogress())) {
                        list.get(0).activate();
                    }
                    list = null;
                }
                downloaders = null;
            } catch (Exception e) {
                e.printStackTrace();
            }

            synchronized (thread) {
                if (isIdeling()) {
                    try {
                        thread.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void print() {
        System.out.println("-----------------------------");
        Enumeration<ArrayList<HistoryDownloader>> downloaders = exchangeDownloderSrore.elements();
        while (downloaders.hasMoreElements()) {
            ArrayList<HistoryDownloader> list = downloaders.nextElement();
            for (HistoryDownloader downloader : list) {
                System.out.println(downloader);
            }
        }
    }

    public void removeDownloader(HistoryDownloader downloader) {
        ArrayList<HistoryDownloader> downloaders = getDownloaderStore(downloader.getExchange());
        downloaders.remove(downloader);
    }

    /**
     * Deactivate all downloads if TW disconnected
     */
    public void twDisconnected() {
        Enumeration<ArrayList<HistoryDownloader>> downloaders = exchangeDownloderSrore.elements();
        while (downloaders.hasMoreElements()) {
            ArrayList<HistoryDownloader> list = downloaders.nextElement();
            ListIterator ids = list.listIterator();
            HistoryDownloader downloader;

            while (ids.hasNext()) {
                downloader = (HistoryDownloader) ids.next();
                if (downloader != null) {
                    downloader.deactivate();
                    downloader = null;
                }
            }
            list.clear();
        }
        exchangeDownloderSrore.clear();
    }

    public void requestHistory(Exchange exchange) {
        String request = Meta.HISTORY_FILE_LIST + Meta.DS + exchange.getSymbol() +
                Meta.FD + exchange.getLastEODDate() + Meta.EOL;
//        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request, exchange.getSymbol());
        request = Meta.INTRADAY_FILE_LIST + Meta.DS + exchange.getSymbol() +
                Meta.FD + exchange.getLastEODDate() +
                Meta.FD + Language.getLanguageTag() + Meta.EOL;
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request, exchange.getSymbol());
        sendAdvHistoryRequest(exchange);
        System.out.println("Requestd file list for " + exchange.getSymbol());
    }

    public void sendAdvHistoryRequest(Exchange exchange) {
        String request = Meta.ADV_HISTORY_FILE_LIST + Meta.DS + exchange.getSymbol() +
                Meta.FD + HistoryArchiveRegister.getSharedInstance().getSizeofEntry(exchange.getSymbol(), Constants.HISTORY_DOWNLOADER_EOD_NAME) + Meta.FD + Language.getSelectedLanguage() + Meta.EOL;
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request, exchange.getSymbol(), exchange.getContentCMIP());
//        SendQFactory.addData(exchange.getPath(), request);
    }

    public void sendAdvHistoryRequestForExchanges() {
        Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
        Exchange exchange;
        while (exchanges.hasMoreElements()) {
            try {
                exchange = exchanges.nextElement();
                if (exchange.isDefault()) {
                    sendAdvHistoryRequest(exchange);
                }
            } catch (Exception e) {
//                 e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    private synchronized boolean isIdeling() {
        Enumeration<ArrayList<HistoryDownloader>> downloaders = exchangeDownloderSrore.elements();
        while (downloaders.hasMoreElements()) {
            if (downloaders.nextElement().size() > 0) {
                return false;
            }
        }
        return true;
    }

    public void twConnected() {

    }

    public void applicationExiting() {

    }

    public void applicationLoaded() {

    }

    public void applicationLoading(int percentage) {

    }

    public void workspaceLoaded() {

    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }

    public void applicationReadyForTransactions() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void snapshotProcessingEnded(Exchange exchange) {
        requestHistory(exchange);
//        sendAdvHistoryRequest(exchange);
        downloadCount.put(exchange.getSymbol(), Integer.MAX_VALUE);
    }

    public void loadOfflineData() {
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }
}