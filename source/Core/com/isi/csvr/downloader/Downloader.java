package com.isi.csvr.downloader;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 22, 2005
 * Time: 2:02:46 PM
 */
public interface Downloader {
    public static final int EXCHANGE_DATA = 0;
    public static final int APPLICATION_DATA = 1;
    public static final int EXCHANGE_PROPS = 2;

    void deactivate();

    boolean isActive();
}
