package com.isi.csvr.downloader;

import com.isi.csvr.cashFlowWatch.CashFlowHistory;
import com.isi.csvr.cashFlowWatch.CashFlowHistoryDataStore;
import com.isi.csvr.chart.SplitPointTable;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 18, 2008
 * Time: 10:38:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowSummaryDownloader implements Runnable {

    private static final String FILE_EXTRACT_LOCK = "FILE_EXTRACT_LOCK";
    private String exchange;
    private String symbol;
    private String key;
    private int type;
    private byte path;
    private boolean active = true;
    private SimpleDateFormat format;
    private ZipFileDownloader downloader;

    public CashFlowSummaryDownloader(String exchange, int type, byte path) {
        this.exchange = exchange;
        //   this.symbol = symbol;
        //    this.key = SharedMethods.getKey(exchange, symbol, instrument);
        this.type = type;
        this.path = path;
        format = new SimpleDateFormat("yyyyMMdd");
        Thread thread = new Thread(this, "CashFlowHistory" + exchange);
        thread.start();
    }

    public static void createFolder(String path) {
        try {
            File folder = new File(path);
            folder.mkdirs();
            folder = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void extractFile(String filepath, File file) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                String path;
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                byte[] bytData = new byte[1000];
                int i;

                while (true) {
                    try {
                        ZipEntry oEntry = zIn.getNextEntry();
                        if (oEntry == null)
                            break;
                        if (oEntry.isDirectory()) {
                            path = filepath + "/" + oEntry.getName();
                            createFolder(path);
                        } else {
                            path = filepath;
                        }
                        File f = new File(oEntry.getName());
                        FileOutputStream oOut = new FileOutputStream(path + "/" + f.getName());
                        while (true) {
                            i = zIn.read(bytData);
                            if (i == -1)
                                break;
                            oOut.write(bytData, 0, i);
                        }
                        zIn.closeEntry();
                        oOut.close();
                        f = null;
                        oEntry = null;
                        oOut = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Thread.sleep(2);
                }
                zIn.close();
                zIn = null;
                bytData = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public int getType() {
        return type;
    }

    public void run() {
        String url = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + type +
                Meta.FD + exchange + Meta.EOL;
        //  LinkedList<IntraDayOHLC> tempArray = new LinkedList<IntraDayOHLC>();
        String fpath = "";
        while (active) {
            try {
                System.out.println("[CashFlowHistory Detail] Started " + exchange + SharedMethods.getMemoryDetails());
                downloader = new ZipFileDownloader(url, exchange, "CshFlHist-S" + type, path);
                File[] files = downloader.downloadFiles();
                downloader = null;
                for (int i = 0; i < files.length; i++) {
                    if (files[i].length() > 0) {
                        fpath = CashFlowHistoryDataStore.getSharedInstance().getExchangetDataHome(exchange);
                        createFolder(fpath);
                        extractFile(fpath, files[i]);
                        //  extractFile(files[i], tempArray);
                    }
                    try {
                        files[i].delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
//                while (tempArray.size() > 0) {
//                    if (type == Meta.HISTORY) {
//                        OHLCStore.getInstance().addHistoryRecord(tempArray.removeLast());
//                    } else {
//                        OHLCStore.getInstance().addIntradayRecord(tempArray.removeLast());
//                    }
//                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                }
            } finally {
                //TodaysTradeBacklogDownloadManager.getSharedInstance().removeDownloader(key);
            }
        }
        //   tempArray = null;
        deactivate();


        //    OHLCDownloadManager.getSharedInstance().removeHistoryRequest(key);
        System.out.println("[CashFlowHistory Detail] Completed " + exchange + " " + symbol + SharedMethods.getMemoryDetails());
        if (!fpath.isEmpty()) {
            CFHistoryDownloadManager.getSharedInstance().loadSelectedSummaryFile(fpath, exchange);
            // CashFlowHistoryDataStore.getSharedInstance().loadSelectedSummaryFile(fpath,exchange);
        }
    }

    private void extractFile(File file, LinkedList<IntraDayOHLC> tempArray) {
        String record;
        synchronized (FILE_EXTRACT_LOCK) {
            int count = 0;
            try {
                SplitPointTable splitPointTable = OHLCStore.getInstance().getSplitPointTable(key);
                splitPointTable.clear();

                Date date;
                String[] ohlcData = null;
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                DataInputStream in = new DataInputStream(zIn);
                zIn.getNextEntry();

                while (true) {
                    record = in.readLine();
                    if (record == null) break; // end of streeam
                    count++;
                    ohlcData = record.split(Meta.FS);
                    IntraDayOHLC ohlcRecord;

//                    if (type == Meta.HISTORY) {
//                        date = format.parse(ohlcData[0]);
//                        ohlcRecord = new IntraDayOHLC(key, date.getTime() / 60000,
//                                Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
//                                Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
//                                Long.parseLong(ohlcData[8]), 0); // todo intraday vwap
//                        tempArray.add(ohlcRecord);
////                        OHLCStore.getInstance().addHistoryRecord(ohlcRecord);
//                        date = null;
//                    } else {
//                        ohlcRecord = new IntraDayOHLC(key, Long.parseLong(ohlcData[0]),
//                                Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
//                                Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
//                                Long.parseLong(ohlcData[5]), 0); // todo intraday vwap
//                        tempArray.add(ohlcRecord);
//                        //OHLCStore.getInstance().addOHLCRecord(ohlcRecord);
//                    }

                    record = null;
                    ohlcRecord = null;
                    if (count == 100) {
                        Thread.sleep(1);
                        count = 0;
                    }
                }
                zIn.closeEntry();
                zIn.close();
                zIn = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public void deactivate() {
        CashFlowHistory.getSharedInstance().stopSerchIconOnThreadInturrupt();
        try {
            active = false;
            downloader.interrupt();
            downloader = null;
        } catch (Exception e) {

        }
    }
}
