package com.isi.csvr.downloader;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Feb 18, 2008
 * Time: 1:40:27 PM
 */
public interface HistoryDownloadListner {
    public void historyDownloaded(String exchange);

    public void historicalIntradayDownload(String exchange);
}
