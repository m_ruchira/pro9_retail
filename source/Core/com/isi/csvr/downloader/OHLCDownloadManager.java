package com.isi.csvr.downloader;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

import java.util.Hashtable;
import java.util.TimeZone;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 1, 2005
 * Time: 2:04:08 PM
 */
public class OHLCDownloadManager implements ApplicationListener {

    private static OHLCDownloadManager self = null;
    private Hashtable<String, OHLCDownloader> indradayStore;
    private Hashtable<String, OHLCDownloader> historyStore;

    public OHLCDownloadManager() {
        indradayStore = new Hashtable<String, OHLCDownloader>();
        historyStore = new Hashtable<String, OHLCDownloader>();
        Application.getInstance().addApplicationListener(this);
    }

    public static synchronized OHLCDownloadManager getSharedInstance() {
        if (self == null) {
            self = new OHLCDownloadManager();
        }
        return self;
    }

    public synchronized void addRequest(String key, int type) {
        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = SharedMethods.getExchangeFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);

        byte dataPath = Constants.CONTENT_PATH_PRIMARY;
        if (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() > 0) {
            dataPath = ExchangeStore.getSharedInstance().getExchange(exchange).getPath();
        }

        OHLCDownloader downloader = getDownloader(key, type);
        if ((downloader != null) && (downloader.getType() == type)) { // already downloading
            return;
        } else {
            downloader = new OHLCDownloader(exchange, symbol, instrument, type, dataPath);
            if (type == Meta.HISTORY) {
                historyStore.put(key, downloader);
            } else {
                indradayStore.put(key, downloader);
            }
        }

        /*if (type == Meta.MESSAGE_TYPE_OHLC_HISTORY) {
            String request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + Meta.OHLC_HISTORY +
                    Meta.FD + exchange + Meta.FD + symbol + Meta.EOL;
            SendQFactory.addData(request);
        }else{

        }*/


    }

    private void resendRequests() {
        OHLCStore.getInstance().resendIntradayRequests();
    }

    private OHLCDownloader getDownloader(String key, int type) {
        if (type == Meta.HISTORY) {
            return historyStore.get(key);
        } else {
            return indradayStore.get(key);
        }
    }

    public void removeInreadayRequest(String key) {
        OHLCDownloader downloader = indradayStore.get(key);
        if (downloader != null) {
            downloader.deactivate();
        }
        indradayStore.remove(key);
    }

    public void removeHistoryRequest(String key) {
        OHLCDownloader downloader = historyStore.get(key);
        if (downloader != null) {
            downloader.deactivate();
        }
        historyStore.remove(key);
    }

    public void applicationExiting() {

    }

    public void applicationLoaded() {

    }

    public void applicationLoading(int percentage) {

    }

    public void applicationReadyForTransactions() {
        resendRequests();
    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void workspaceLoaded() {

    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }
}
