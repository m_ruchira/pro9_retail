package com.isi.csvr.downloader;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trade.TradeStore;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 26, 2005
 * Time: 3:38:54 PM
 */
public class TodaysTradeBacklogDownloader implements Runnable {
    private List<String> queue;
    private boolean active = true;
    private String id;
    private ZipFileDownloader downloader;

    public TodaysTradeBacklogDownloader(List<String> queue, String id) {
        this.queue = queue;
        this.id = id;
        Thread thread = new Thread(this, "TodaysTradeBacklogDownloader " + id);
        thread.start();
    }

    public void run() {
        while (active) {
            System.out.println("[Trade Backlog Download] Started " + id + SharedMethods.getMemoryDetails());
            try {
                String url = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + Meta.ALL_TRADES +
                        Meta.FD + id + Meta.FD + TradeStore.getSharedInstance().getLastValidTradeSequence(id) + Meta.EOL;
                downloader = new ZipFileDownloader(url, id, "Trades");
                File[] files = downloader.downloadFiles();
                downloader = null;
                for (int i = 0; i < files.length; i++) {
                    if (files[i].length() > 0) {
                        extractFile(files[i]);
                    }
                    try {
                        files[i].delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e1) {
                }
            } finally {
                TodaysTradeBacklogDownloadManager.getSharedInstance().removeDownloader(id);
            }
        }
        System.out.println("[Trade Backlog Download] Completed " + id + SharedMethods.getMemoryDetails());
    }

    private void extractFile(File file) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                int rs = Meta.RS.charAt(0);
                StringBuilder sFrame = new StringBuilder();
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                zIn.getNextEntry();
                int iValue;

                sFrame.append(Meta.BACKLOG_FRAME);
                sFrame.append(Meta.DS);
                sFrame.append(Meta.YES);
                sFrame.append(Meta.FS);

                while (true) {
                    iValue = zIn.read();
                    if (iValue == -1) break; // end of streeam
                    if (iValue != rs) {
                        sFrame.append((char) iValue);
                        continue;
                    }
                    if ((sFrame != null) && (sFrame.length() > 0)) {
                        queue.add(sFrame.toString());
                        sFrame = null;
                        sFrame = new StringBuilder("");
                        sFrame.append(Meta.BACKLOG_FRAME);
                        sFrame.append(Meta.DS);
                        sFrame.append(Meta.YES);
                        sFrame.append(Meta.FS);
                        Thread.sleep(1);
                    }
                }
                zIn.closeEntry();
                zIn.close();
                zIn = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public void deactivate() {
        if (downloader != null) {
            downloader.interrupt();
        }
        active = false;
    }

    public boolean isActive() {
        return active;
    }
}