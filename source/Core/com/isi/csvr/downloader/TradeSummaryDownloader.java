package com.isi.csvr.downloader;

import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trade.SummaryTrade;
import com.isi.csvr.trade.TradeSummaryStore;

import java.io.File;
import java.io.FileInputStream;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 11, 2005
 * Time: 2:05:44 PM
 */
public class TradeSummaryDownloader implements Runnable, ConnectionListener {
    private boolean active = true;
    private String exchange;
    private long size;
    private ZipFileDownloader downloader;

    public TradeSummaryDownloader(String id, long size) {
        this.exchange = id;
        this.size = size;
        ConnectionNotifier.getInstance().addConnectionListener(this);
        Thread thread = new Thread(this, "TradeSummaryDownloader " + id);

        thread.start();
    }

    public static void extractFile(File file, String exchange) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                int rs = Meta.RS.charAt(0);
                StringBuilder sFrame = new StringBuilder();
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                zIn.getNextEntry();
                int iValue;

                while (true) {
                    iValue = zIn.read();
                    if (iValue == -1) break; // end of streeam
                    if (iValue != rs) {
                        sFrame.append((char) iValue);
                        continue;
                    }
                    if ((sFrame != null) && (sFrame.length() > 0)) {
                        createTrade(sFrame.toString(), exchange);
                        sFrame = null;
                        sFrame = new StringBuilder("");
                        Thread.sleep(1);
                    }
                }
                zIn.closeEntry();
                zIn.close();
                zIn = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    private synchronized static void createTrade(String data, String exchange) {
        String[] fields = data.split(",");
//     SummaryTrade summaryTrade = new SummaryTrade(Float.parseFloat(fields[1]),
//                Long.parseLong(fields[2]),
//                Integer.parseInt(fields[3]));
//
        SummaryTrade summaryTrade = null;
        try {
            summaryTrade = new SummaryTrade(Float.parseFloat(fields[1]),
                    Long.parseLong(fields[2]),
                    Integer.parseInt(fields[3]),
                    Integer.parseInt(fields[4]),
                    Long.parseLong(fields[5]),
                    Integer.parseInt(fields[6]),
                    Long.parseLong(fields[7]));
        } catch (NumberFormatException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        //float tradePrice, long tradeQuantity, int noOfTrades, int cashinTrades, long cashinQuantity, int cashoutTrades, long cashoutQuantity
        TradeSummaryStore.getSharedInstance().addSummaryTrade(exchange, fields[0], summaryTrade);
        summaryTrade = null;
        fields = null;
    }

    public void run() {
        while (active) {
            System.out.println("[Trade Summary Download] Started " + exchange + SharedMethods.getMemoryDetails());
            try {
                if (Settings.hasWriteAccess()) {
                    HistoryArchiveRegister.getSharedInstance().addEntry(exchange, "TradeSummary", 0, HistoryArchiveFile.START);
                    String url = Meta.TRADES_SUMMARY + Meta.DS + exchange + Meta.EOL;
                    downloader = new ZipFileDownloader(url, exchange, "Trade-Summary");
                    File[] files = downloader.downloadFiles();
                    downloader = null;
                    for (int i = 0; i < files.length; i++) {
                        if (files[i].length() > 0) {
                            extractFile(files[i], exchange);
                        }
                        try {
                            files[i].delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    files = null;
                    HistoryArchiveRegister.getSharedInstance().addEntry(exchange, "TradeSummary", size, HistoryArchiveFile.END);
                    break;
                } else {
                    System.out.println("Not sending TradeSummary Download requests");
                    System.out.println("No write access to the loacal Disk to writ TradeSummary");
                    break;
                }
            } catch (Exception e) {
                HistoryArchiveRegister.getSharedInstance().addEntry(exchange, "TradeSummary", 0, HistoryArchiveFile.END);
                e.printStackTrace();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e1) {
                }
                break;
            } finally {
                TodaysTradeBacklogDownloadManager.getSharedInstance().removeDownloader(exchange);
            }
        }
        System.out.println("[Trade Summary Download] Completed " + exchange + SharedMethods.getMemoryDetails());
    }

    public void deactivate() {
        if (downloader != null) {
            downloader.interrupt();
        }
        active = false;
    }

    public boolean isActive() {
        return active;
    }

    public void twConnected() {

    }

    public void twDisconnected() {
        deactivate();
    }
}
