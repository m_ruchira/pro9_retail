package com.isi.csvr.downloader;

import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.shared.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 22, 2005
 * Time: 11:20:55 AM
 */
public class PropertyFileDownloader implements Runnable, Downloader {
    private boolean active = true;
    private String id;
    private int type;
    private String contentIP;
    private ZipFileDownloader downloader;
    private ProgressListener progressListener;
    private float newVersion;

    public PropertyFileDownloader(ProgressListener progressListener, String id, float newVersion, int type) {
        this.id = id;
        this.type = type;
        this.progressListener = progressListener;
        this.newVersion = newVersion;
        Thread thread = new Thread(this, "PropertyFileDownloader " + id);
        thread.start();
    }

    public static void extractFile(File file) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                byte[] data = new byte[1000];
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));

                int len;

                ZipEntry zipEntry = zIn.getNextEntry();
                while (zipEntry != null) {
                    String path = Settings.getAbsolutepath() + zipEntry.getName().substring(1);
                    String folder = null;

                    if (path.lastIndexOf("/") >= 0) {
                        folder = path.substring(0, path.lastIndexOf("/"));
                        File fileObj = new File(folder);
                        fileObj.mkdirs();
                    } else {
                    }

                    if (!zipEntry.isDirectory()) {
                        FileOutputStream out = new FileOutputStream(path);
                        while (true) {
                            len = zIn.read(data);
                            if (len == -1) break; // end of streeam
                            out.write(data, 0, len);
                        }
                        out.flush();
                        out.close();
                        out = null;
                    }

                    zIn.closeEntry();
                    zipEntry = null;
                    zipEntry = zIn.getNextEntry();
                    folder = null;
                    path = null;
                }
                zIn.close();
                zIn = null;
                data = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public void run() {
        while (active) {
            try {
                System.out.println("[Property Download] Started " + id + SharedMethods.getMemoryDetails());

//                String url = Meta.PROPERTY_FILE + Meta.DS + Settings.getPropertyVersion() + Meta.EOL;
                String url = Meta.PROPERTY_FILE + Meta.DS + "0"/*Settings.getPropertyVersion()*/ + Meta.FD + Language.getSelectedLanguage() + Meta.EOL;
                downloader = new ZipFileDownloader(url, id, "MarketData", Constants.CONTENT_PATH_TERTIARY, null);
                downloader.addProgressListener(progressListener);
                File[] files = downloader.downloadFiles();
                downloader = null;

                for (int i = 0; i < files.length; i++) {
                    if (files[i].length() > 0) {
                        extractFile(files[i]);
                        // everything is sucessfull. set the new version as the current version
                        Settings.setPropertyVersion(newVersion);
                    }
                    try {
                        files[i].delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
                active = false; // if error occured, must exit.
            } finally {
                progressListener = null;
                downloader = null;
            }
        }
        MarketDataDownloadManager.getSharedInstance().removeDownloader(id + type);
        System.out.println("[Property Download] Completed " + id + SharedMethods.getMemoryDetails());
    }

    public void deactivate() {
        if (downloader != null) {
            downloader.interrupt();
        }
        active = false;
    }

    public boolean isActive() {
        return active;
    }
}