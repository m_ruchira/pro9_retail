package com.isi.csvr.downloader;


import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.nanoxml.XMLElement;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 31, 2005
 * Time: 3:39:34 PM
 */
public class HistoryArchiveRegister implements Comparator {
    private static HistoryArchiveRegister self = null;
    ArrayList<HistoryArchiveFile> register;

    private HistoryArchiveRegister() {
        load();
    }

    public static synchronized HistoryArchiveRegister getSharedInstance() {
        if (self == null) {
            self = new HistoryArchiveRegister();
        }
        return self;
    }

    public ArrayList<HistoryArchiveFile> getRegister() {
        return register;
    }

    public synchronized void addEntry(String exchange, String fileName, long length, boolean start) {
        addEntry(exchange, fileName, length, start, true);
    }

    public synchronized void addEntry(String exchange, String fileName, long length, boolean start, boolean save) {
        HistoryArchiveFile historyArchiveFile = new HistoryArchiveFile(exchange, fileName, length, start);

        int location = Collections.binarySearch(register, historyArchiveFile, this);
        if (location >= 0) {
            register.set(location, historyArchiveFile);
            historyArchiveFile = null;
        } else {
            register.add(historyArchiveFile);
        }

        Collections.sort(register, this);
        if (save) {
            saveData();
        }
    }

    public synchronized void resetExchange(String exchange) {
        for (HistoryArchiveFile archiveFile : register) {
            if (archiveFile.exchange.equalsIgnoreCase(exchange)) {
                archiveFile.size = 0;
            }
        }
    }

    public boolean isSameSize(String exchange, String fileName, long size) {
        try {
            HistoryArchiveFile historyArchiveFile = new HistoryArchiveFile(exchange, fileName, size, HistoryArchiveFile.START);

            int location = Collections.binarySearch(register, historyArchiveFile, this);
            if (location >= 0) { // found
                historyArchiveFile = null;
                return (register.get(location).size == size);
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public long getSizeofEntry(String exchange, String fileName) {
        try {
            HistoryArchiveFile historyArchiveFile = new HistoryArchiveFile(exchange, fileName, 0, HistoryArchiveFile.END);
            int location = Collections.binarySearch(register, historyArchiveFile, this);
            if (location >= 0) { // found
                return register.get(location).size;
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public boolean contains(String exchange, String fileName) {
        try {
            HistoryArchiveFile historyArchiveFile = new HistoryArchiveFile(exchange, fileName, 0, HistoryArchiveFile.START);

            int location = Collections.binarySearch(register, historyArchiveFile, this);
            if (location >= 0) { // found
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isInProgress(String exchange, String fileName, long size) {
        HistoryArchiveFile historyArchiveFile = new HistoryArchiveFile(exchange, fileName, size, HistoryArchiveFile.START);

        int location = Collections.binarySearch(register, historyArchiveFile, this);
        if (location >= 0) { // found
            return register.get(location).inProgress;
        } else {
            return false;
        }
    }

    public void removeEntry(String exchange, String fileName) {
        try {
            HistoryArchiveFile historyArchiveFile = new HistoryArchiveFile(exchange, fileName, 0, HistoryArchiveFile.START);

            int location = Collections.binarySearch(register, historyArchiveFile, this);
            if (location >= 0) { // found
                register.remove(location);
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public synchronized void saveData() {

        try {
            XMLElement root = new XMLElement();
            root.setName("Archives");
            for (HistoryArchiveFile historyArchiveFile : register) {
                XMLElement child = new XMLElement();
                child.setName("file");
                child.setAttribute("name", historyArchiveFile.fileName);
                child.setAttribute("exchange", historyArchiveFile.exchange);
                child.setAttribute("size", "" + historyArchiveFile.size);
                root.addChild(child);
            }

            Writer writer = new BufferedWriter(new FileWriter(Settings.getAbsolutepath() + "datastore/archives.mdf"));
            root.write(writer);
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void load() {

        register = new ArrayList<HistoryArchiveFile>();

        try {
            String exchange;
            String name;
            String length;
            StringBuilder buffer = new StringBuilder();

            InputStream in = new FileInputStream(Settings.getAbsolutepath() + "datastore/archives.mdf");
            byte[] bytes = new byte[1000];
            int len;
            while (true) {
                len = in.read(bytes);
                if (len <= 0) {
                    break;
                }
                buffer.append(new String(bytes, 0, len));
            }

            XMLElement root = new XMLElement();
            root.parseString(buffer.toString());

            Iterator<XMLElement> elements = root.enumerateChildren();
            while (elements.hasNext()) {
                XMLElement child = elements.next();
                exchange = child.getAttribute("exchange");
                name = child.getAttribute("name");
                length = child.getAttribute("size");
                addEntry(exchange, name, Long.parseLong(length), HistoryArchiveFile.END, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int compare(Object o1, Object o2) {
        HistoryArchiveFile f1 = (HistoryArchiveFile) o1;
        HistoryArchiveFile f2 = (HistoryArchiveFile) o2;

        if (f1.exchange.compareTo(f2.exchange) > 0) {
            return 1;
        }
        if (f1.exchange.compareTo(f2.exchange) < 0) {
            return -1;
        } else {
            return f1.fileName.compareToIgnoreCase(f2.fileName);
        }
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("[ HistoryArchiveRegister");
        for (HistoryArchiveFile file : register) {
            buffer.append("\t");
            buffer.append(file.toString());
            buffer.append("\n");
        }
        buffer.append("]");
        return buffer.toString();
    }
}
