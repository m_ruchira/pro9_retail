package com.isi.csvr.downloader;

import com.isi.csvr.TradeStation.TradeStationManager;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.metastock.MetaStockManager;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.ohlcbacklog.OHLCBacklogStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.variationmap.VariationMapUpdator;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 31, 2005
 * Time: 11:42:04 AM
 */
public class HistoryDownloader implements Runnable {
    public static int POPULATION = 0;
    private static String POPULATION_LOCK = "POPULATION_LOCK";

    private boolean active = true;
    private boolean inprogress = false;
    private String exchange;
    private String file;
    private int type;
    private long size;
    private ZipFileDownloader downloader;
    private int lastEODDate = 0;

    public HistoryDownloader(String exchange, String file, int type, long size) {
        this.exchange = exchange;
        this.file = file;
        this.type = type;
        this.size = size;
    }

    public static void createFolder(String path) {
        try {
            File folder = new File(path);
            folder.mkdirs();
            folder = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void extractFile(String filepath, File file) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                String path;
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                byte[] bytData = new byte[1000];
                int i;

                while (true) {
                    try {
                        ZipEntry oEntry = zIn.getNextEntry();
                        if (oEntry == null)
                            break;
                        if (oEntry.isDirectory()) {
                            path = filepath + "/" + oEntry.getName();
                            createFolder(path);
                        } else {
                            path = filepath;
                        }
                        File f = new File(oEntry.getName());
                        FileOutputStream oOut = new FileOutputStream(path + "/" + f.getName());
                        while (true) {
                            i = zIn.read(bytData);
                            if (i == -1)
                                break;
                            oOut.write(bytData, 0, i);
                        }
                        zIn.closeEntry();
                        oOut.close();
                        f = null;
                        oEntry = null;
                        oOut = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Thread.sleep(2);
                }
                zIn.close();
                zIn = null;
                bytData = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public static void extractSymbolFile(String filepath, File file) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                String path;
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                byte[] bytData = new byte[1000];
                int i;

                while (true) {
                    try {
                        ZipEntry oEntry = zIn.getNextEntry();
                        if (oEntry == null)
                            break;
                        if (oEntry.isDirectory()) {
                            path = filepath + "/" + oEntry.getName();
                            createFolder(path);
                        } else {
                            path = filepath;
                        }
                        File f = new File(oEntry.getName());
                        FileOutputStream oOut = new FileOutputStream(path + "/" + oEntry.getName());
                        while (true) {
                            i = zIn.read(bytData);
                            if (i == -1)
                                break;
                            oOut.write(bytData, 0, i);
                        }
                        zIn.closeEntry();
                        oOut.close();
                        f = null;
                        oEntry = null;
                        oOut = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Thread.sleep(2);
                }
                zIn.close();
                zIn = null;
                bytData = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public void activate() {
        Thread thread = new Thread(this, "HistoryDownloader " + exchange + " : " + file);
        thread.start();
    }

    public void run() {

        /*if (type == Meta.OHLC_HISTORY_FILE){
            HistoryDownloadManager.getSharedInstance().removeDownloader(this);
            return;
        }*/

        synchronized (POPULATION_LOCK) {
            POPULATION++;
        }

        String url = null;
        String path;
        int loopCount = 0;

        inprogress = true;

        while (active) {
            try {
                loopCount++;
                if ((type == Meta.ARCHIVE_FILE) || (type == Meta.OHLC_HISTORY_FILE)) {
                    url = type + Meta.DS + exchange +
                            Meta.FD + file + Meta.EOL;
                } else if (type == Meta.SYMBOL_HISTORY_FILE) {
                    url = type + Meta.DS + exchange +
                            Meta.FD + file + Meta.EOL;
                } else {
                    url = type + Meta.DS + exchange +
                            Meta.FD + "0" + Meta.EOL;
                }
                System.out.println("[History Download] Started " + exchange + " " + file + SharedMethods.getMemoryDetails());
                HistoryArchiveRegister.getSharedInstance().addEntry(exchange, file, 0, HistoryArchiveFile.START);
                downloader = new ZipFileDownloader(url, exchange, file);
                File[] files = downloader.downloadFiles();
                downloader = null;

                synchronized (Constants.HISTORY_LOCK) { // process one instance at a time
                    for (int i = 0; i < files.length; i++) {
                        if (files[i].length() > 0) {
                            switch (type) {
                                case Meta.RECENT_HISTORY_FILE:
                                    updateDBFromRecentHistory(exchange, files[i]);
                                    updateMetaStock();
                                    updateTradeStation();
                                    HistoryDownloadManager.getSharedInstance().downloadComplete(exchange, file);
                                    HistoryDownloadManager.getSharedInstance().setNewHistoryFileAvailable(true);
//                                    HistoryDownloadManager.getSharedInstance().createHistoryForAllIndexes(exchange,("history/" + exchange + "/" + "history"));
                                    break;
                                case Meta.OHLC_HISTORY_FILE:
                                    createFolder(Settings.getAbsolutepath() + "ohlc/" + exchange + "/" + file);
                                    extractFile(Settings.getAbsolutepath() + "ohlc/" + exchange + "/" + file, files[i]);
                                    OHLCStore.getInstance().loadIntradayHistory(exchange);
                                    MetaStockManager.getInstance().updateIntrdayHistory(exchange, file);
                                    TradeStationManager.getInstance().updateIntrdayHistory(exchange, file);
                                    OHLCBacklogStore.getSharedInstance().updateListFromFiles(exchange);
                                    VariationMapUpdator.reloadOHLC();
//                                    OHLCStore.getInstance().createOHLCForAllIndexes(exchange);
                                    HistoryDownloadManager.getSharedInstance().fireHistoryIntradayDownloaded(exchange);
                                    break;
                                case Meta.YTD_HISTORY_FILE:
                                case Meta.ARCHIVE_FILE:
                                    path = Settings.getAbsolutepath() + "history/" + exchange + "/" + file;
                                    createFolder(path);
                                    extractFile(path, files[i]);
                                    OHLCStore.getInstance().reloadHistoryForExchange(exchange);
                                    updateMetaStock(); // update the MetaStock history DB
                                    updateTradeStation();
                                    HistoryDownloadManager.getSharedInstance().downloadComplete(exchange, file);
                                    HistoryDownloadManager.getSharedInstance().setNewHistoryFileAvailable(true);
//                                    HistoryDownloadManager.getSharedInstance().createHistoryForAllIndexes(exchange,("history/" + exchange + "/" + file ));
                                    break;
                                case Meta.SYMBOL_HISTORY_FILE:
                                    path = Settings.getAbsolutepath() + "history/" + exchange + "/"; //+ "/" + file
                                    createFolder(path);
                                    extractSymbolFile(path, files[i]);
                                    OHLCStore.getInstance().reloadHistoryForExchange(exchange);
                                    updateMetaStock(); // update the MetaStock history DB
                                    updateTradeStation();
                                    HistoryDownloadManager.getSharedInstance().downloadComplete(exchange, file);
                                    HistoryDownloadManager.getSharedInstance().setNewHistoryFileAvailable(true);
                                    break;
                            }
                        }
                        try {
                            files[i].delete();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                HistoryArchiveRegister.getSharedInstance().addEntry(exchange, file, size, HistoryArchiveFile.END);
                HistoryDownloadManager.getSharedInstance().removeDownloader(this);
                // update the eod date after sucessful data download
                if ((file.toUpperCase().startsWith("RECENTHISTORY")) ||
                        (file.toUpperCase().startsWith("HISTORY"))) {
                    Exchange exch = ExchangeStore.getSharedInstance().getExchange(exchange);
                    if (exch != null) {
                        exch.setLastEODDate(exch.getCurrentEODDate());
                    }
                    exch = null;
                }
                System.out.println("[History Download] Completed " + exchange + " " + file + SharedMethods.getMemoryDetails());
                break;
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("File download Exception for " + url);
                HistoryArchiveRegister.getSharedInstance().addEntry(exchange, file, 0, HistoryArchiveFile.END);
                if (loopCount > 10) {
                    break;
                }
                //e.printStackTrace();
            }
        }
        synchronized (POPULATION_LOCK) {
            POPULATION--;
        }
        active = false;
        inprogress = false;
    }

    private void updateMetaStock() {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) {
            if (MetaStockManager.autoUpdateEnableMarkets.contains(exchange)) {
                MetaStockManager.getInstance().updateHistory(exchange);
            }
        }
    }

    private void updateTradeStation() {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION)) {
            if (TradeStationManager.autoUpdateEnableMarkets.contains(exchange)) {
                TradeStationManager.getInstance().updateHistory(exchange);
            }
        }
    }

    private void updateDBFromRecentHistory(String exchange, File path) {
        String record = "";
        try {
            int date;
            String symbol = "";

            try {
                File folder = new File(Settings.getAbsolutepath() + "history/" + exchange + "/history/");
                folder.mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (lastEODDate == 0) {
                lastEODDate = ExchangeStore.getSharedInstance().getExchange(exchange).getLastEODDate();
            }

            ZipInputStream zIn = new ZipInputStream(new FileInputStream(path));
            BufferedReader oIn = new BufferedReader(new InputStreamReader(zIn));

            ZipEntry zipEntry = zIn.getNextEntry();
            while (zipEntry != null) {
                symbol = zipEntry.getName();
                FileOutputStream out = HistoryFilesManager.openHistoryFile(exchange, symbol);

                while (true) {
                    record = oIn.readLine();
                    if (record == null)
                        break;
                    date = Integer.parseInt(record.split(Meta.FS, 2)[0]);
                    if (isWithinRange(date)) {
                        HistoryFilesManager.writeHistoryData(out, record);
                    }
                    record = null;
                    Thread.sleep(2);
                }
                try {
                    out.flush();
                    out.close();
                    out = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                symbol = null;
                zipEntry = zIn.getNextEntry();
            }
            record = null;
            zIn.close();
            zIn = null;
            oIn = null;
        } catch (Exception e) {
            // to do error handeling
            e.printStackTrace();
        }
    }

    public String getExchange() {
        return exchange;
    }

    public String getFileName() {
        return file;
    }

    private boolean isWithinRange(int date) {
        return (date > lastEODDate);
    }

    public void deactivate() {
        if (downloader != null) {
            HistoryArchiveRegister.getSharedInstance().removeEntry(exchange, file);
            downloader.interrupt();
        }
        active = false;
        System.out.println("Deactivated " + exchange + " " + file);
    }

    public boolean isActive() {
        return active;
    }

    public boolean isInprogress() {
        return inprogress;
    }

    public String toString() {
        return "HistoryDownloader {" +
                "exchange='" + exchange + "'" +
                ", file='" + file + "'" +
                "}";
    }
}