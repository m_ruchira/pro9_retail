package com.isi.csvr.downloader;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;

import java.util.concurrent.Semaphore;

/**
 * Created by IntelliJ IDEA.
 * User: madhawies
 * Date: Jul 8, 2011
 * Time: 10:42:11 AM
 * To change this template use File | Settings | File Templates.
 */

public class InvestorTypeHistoryDownloadManager {

    public static Semaphore detailSem = new Semaphore(1, true);
    private static InvestorTypeHistoryDownloadManager self;
    //private CashFlowDetailDownloader detailDownloader;
    private InvestorTypeHistoryDownloader summaryDownloader;
    private Semaphore summarySem;

    private InvestorTypeHistoryDownloadManager() {
        //detailSem = new Semaphore(1,true);
        summarySem = new Semaphore(1, true);
    }

    public static InvestorTypeHistoryDownloadManager getSharedInstance() {
        if (self == null) {
            self = new InvestorTypeHistoryDownloadManager();
            return self;
        } else {
            return self;
        }
    }


    public boolean addInvestorTypeHistoryRequest(String exchange) {
        //  if(summarySem.availablePermits())

        summaryDownloader = new InvestorTypeHistoryDownloader(exchange, Meta.INVESTOR_HISTORY, Constants.CONTENT_PATH_PRIMARY);

        return false;
    }

    public void loadSelectedSummaryFile(String fpath, String exchange) {
        //CashFlowHistory.getSharedInstance().setSummaryFileDl(false);
        //CashFlowHistoryDataStore.getSharedInstance().loadSelectedSummaryFile(fpath,exchange);
        //CashFlowHistory.getSharedInstance().SummaryFileLoadComplete();

    }


    /*public boolean addCFHistoryDetailRequest(String exchange, String sKey){
       if(detailSem.availablePermits()>0){
        detailDownloader = new CashFlowDetailDownloader(exchange, SharedMethods.getSymbolFromKey(sKey),
                SharedMethods.getInstrumentTypeFromKey(sKey),Meta.CASHFLOW_HISTORY , Constants.CONTENT_PATH_PRIMARY);
           return true;
       }else{
           return false;
       }
    }*/

    public void laodSelectedDetailFile(String fpath, String exchange, String symbol, String key) {

        //CashFlowHistoryDataStore.getSharedInstance().loadSelectedDetailFile(fpath,key,symbol,exchange);
        //CashFlowHistory.getSharedInstance().detailFileLoadComplete();
    }

    /*public void cancelDetailDownloads(){
        if(detailDownloader != null){
            detailDownloader.deactivate();
            detailSem.release();
        }
    }*/

    public void cancelSummaryDownloads() {
        if (summaryDownloader != null) {
            summaryDownloader.deactivate();
            summaryDownloader = null;
        }
    }

    public void downloadUnSuccesfull() {
        new ShowMessage(Language.getString("CF_NO_DATA"), "I");

    }

}
