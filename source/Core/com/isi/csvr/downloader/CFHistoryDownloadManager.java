package com.isi.csvr.downloader;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.cashFlowWatch.CashFlowHistory;
import com.isi.csvr.cashFlowWatch.CashFlowHistoryDataStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

import java.util.concurrent.Semaphore;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 18, 2008
 * Time: 3:05:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class CFHistoryDownloadManager {

    public static Semaphore detailSem = new Semaphore(1, true);
    private static CFHistoryDownloadManager self;
    private CashFlowDetailDownloader detailDownloader;
    private CashFlowSummaryDownloader summaryDownloader;
    private Semaphore summarySem;

    private CFHistoryDownloadManager() {
        //detailSem = new Semaphore(1,true);
        summarySem = new Semaphore(1, true);
    }

    public static CFHistoryDownloadManager getSharedInstance() {
        if (self == null) {
            self = new CFHistoryDownloadManager();
            return self;
        } else {
            return self;
        }
    }


    public boolean addCFHistorySummaryRequest(String exchange) {
        //  if(summarySem.availablePermits())

        summaryDownloader = new CashFlowSummaryDownloader(exchange, Meta.CASHFLOW_SUMMARY, Constants.CONTENT_PATH_PRIMARY);

        return false;
    }

    public void loadSelectedSummaryFile(String fpath, String exchange) {
        CashFlowHistory.getSharedInstance().setSummaryFileDl(false);
        CashFlowHistoryDataStore.getSharedInstance().loadSelectedSummaryFile(fpath, exchange);
        CashFlowHistory.getSharedInstance().SummaryFileLoadComplete();

    }


    public boolean addCFHistoryDetailRequest(String exchange, String sKey) {
        if (detailSem.availablePermits() > 0) {
            detailDownloader = new CashFlowDetailDownloader(exchange, SharedMethods.getSymbolFromKey(sKey),
                    SharedMethods.getInstrumentTypeFromKey(sKey), Meta.CASHFLOW_HISTORY, Constants.CONTENT_PATH_PRIMARY);
            return true;
        } else {
            return false;
        }

    }

    public void laodSelectedDetailFile(String fpath, String exchange, String symbol, String key) {

        CashFlowHistoryDataStore.getSharedInstance().loadSelectedDetailFile(fpath, key, symbol, exchange);
        CashFlowHistory.getSharedInstance().detailFileLoadComplete();
    }

    public void cancelDetailDownloads() {
        if (detailDownloader != null) {
            detailDownloader.deactivate();
            detailSem.release();
        }
    }

    public void cancelSummaryDownloads() {
        if (summaryDownloader != null) {
            summaryDownloader.deactivate();
            summaryDownloader = null;
        }
    }

    public void downloadUnSuccesfull() {
        new ShowMessage(Language.getString("CF_NO_DATA"), "I");

    }

}
