package com.isi.csvr.downloader;

import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 26, 2005
 * Time: 3:48:21 PM
 */
public class TodaysTradeBacklogDownloadManager implements ConnectionListener {
    private static TodaysTradeBacklogDownloadManager self = null;
    private Hashtable table;
    private List<String> queue;

    private TodaysTradeBacklogDownloadManager() {
        table = new Hashtable();
        ConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static synchronized TodaysTradeBacklogDownloadManager getSharedInstance() {
        if (self == null) {
            self = new TodaysTradeBacklogDownloadManager();
        }
        return self;
    }

    public void setBuffer(List<String> queue) {
        this.queue = queue;
    }

    public void startNewDownloadSession(String id) {
        TodaysTradeBacklogDownloader downloader = (TodaysTradeBacklogDownloader) table.get(id);

        // destroy the current download processtive if still active
        if (downloader != null) {
            downloader = (TodaysTradeBacklogDownloader) table.get(id);
            downloader.deactivate();
            removeDownloader(id);
            downloader = null;
        }

        // start a new download process
        downloader = new TodaysTradeBacklogDownloader(queue, id);
        table.put(id, downloader);
    }

    public void removeDownloader(String id) {
        table.remove(id);
    }

    /**
     * Deactivate all downloads if TW disconnected
     */
    public void twDisconnected() {
        Enumeration ids = table.keys();
        String id;
        TodaysTradeBacklogDownloader downloader;

        while (ids.hasMoreElements()) {
            id = (String) ids.nextElement();
            downloader = (TodaysTradeBacklogDownloader) table.get(id);
            if (downloader != null) {
                downloader.deactivate();
                removeDownloader(id);
                downloader = null;
            }
        }
    }

    public void twConnected() {

    }

}
