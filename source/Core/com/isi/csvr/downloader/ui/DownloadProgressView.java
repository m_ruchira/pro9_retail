package com.isi.csvr.downloader.ui;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 20, 2005
 * Time: 1:27:54 PM
 */
public class DownloadProgressView extends JDialog implements Runnable, Themeable {

    private static DownloadProgressView self = null;
    private JLabel panel;
    private Component component;

    private DownloadProgressView() throws HeadlessException {
        super(Client.getInstance().getFrame(), false);
        setUndecorated(true);

        setLayout(new BorderLayout());
        panel = new JLabel();
        panel.setOpaque(true);
        panel.setBorder(BorderFactory.createLineBorder(Color.black));
        add(panel, BorderLayout.CENTER);
        Theme.registerComponent(this);
        Thread thread = new Thread(this, "DownloadProgressView");
        thread.setPriority(Thread.NORM_PRIORITY - 3);
        thread.start();
    }

    public static synchronized DownloadProgressView getSharedInstance() {
        if (self == null) {
            self = new DownloadProgressView();
        }
        return self;
    }

    public void setReferenceComponent(Component component) {
        this.component = component;
    }

    public void setVisible(boolean visible) {
        if (visible) {
            Point point = component.getLocation();
            SwingUtilities.convertPointToScreen(point, component.getParent());
            point.translate(0, component.getHeight());
            setLocation(point);
        }
        super.setVisible(visible);
    }

    public void run() {
        while (true) {
            if (isVisible()) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("<html><table border=\"0\" cellspacing=\"0\" width=\"265\" cellpadding=\"0\">");
                stringBuilder.append("<tr><td width=\"65\"><b><font face='Arial' size=3>&nbsp;");
                stringBuilder.append(Language.getString("EXCHANGE_SHORT"));
                stringBuilder.append("</font></b></td><td width=\"100\"><b><font face='Arial' size=3>");
                stringBuilder.append(Language.getString("FILE"));
                stringBuilder.append("</font></b></td><td width=\"40\" align=right><b><font face='Arial' size=3>");
                stringBuilder.append(Language.getString("SIZE"));
                stringBuilder.append("</font></b></td><td width=\"60\" align=right><b><font face='Arial' size=3>");
                stringBuilder.append(Language.getString("PROGRESS"));
                stringBuilder.append("</font></b></td></tr>");
                stringBuilder.append(DownloadProgressManager.getStatus());
                stringBuilder.append("</table>");
                panel.setText(stringBuilder.toString());
                pack();
                DownloadProgressManager.removeCompletedItems();
                stringBuilder = null;
            } else {
                if (DownloadProgressManager.count() > 0) {
                    DownloadProgressManager.removeCompletedItems();
                    if (!component.isVisible()) {
                        component.setVisible(true);
                        continue;
                    }
                } else {
                    if (component.isVisible()) {
                        component.setVisible(false);
                    }
                }
            }
            try {
                if (isVisible())
                    Thread.sleep(1000);
                else
                    Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(getContentPane());
    }
}
