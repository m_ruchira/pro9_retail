package com.isi.csvr.downloader;

import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.ohlc.TodaysOHLCDownloadListener;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 26, 2005
 * Time: 3:48:21 PM
 */
public class TodaysOHLCBacklogDownloadManager implements ConnectionListener {
    private static TodaysOHLCBacklogDownloadManager self = null;
    private Hashtable<String, TodaysOHLCBacklogDownloader> table;
    private List<String> queue;
    private ArrayList<TodaysOHLCDownloadListener> listeners;

    private TodaysOHLCBacklogDownloadManager() {
        table = new Hashtable<String, TodaysOHLCBacklogDownloader>();
        listeners = new ArrayList<TodaysOHLCDownloadListener>();
        ConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static synchronized TodaysOHLCBacklogDownloadManager getSharedInstance() {
        if (self == null) {
            self = new TodaysOHLCBacklogDownloadManager();
        }
        return self;
    }

    public void addTodayOHLCDownloadListener(TodaysOHLCDownloadListener listener) {
        listeners.add(listener);
    }

    public void removeTodayOHLCDownloadListener(TodaysOHLCDownloadListener listener) {
        listeners.remove(listener);
    }

    public void setBuffer(List<String> queue) {
        this.queue = queue;
    }


    public void fireOHLCBacklogDownloaded(String exchange) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                listeners.get(i).OHLCBacklogDownloadCompleted(exchange);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void fireOHLCBacklogStarted(String exchange) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                listeners.get(i).OHLCBacklogDownloadStarted(exchange);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void startNewDownloadSession(String id) {
        TodaysOHLCBacklogDownloader downloader = table.get(id);

        // destroy the current download processtive if still active
        if (downloader != null) {
            downloader.deactivate();
            removeDownloader(id);
            downloader = null;
        }

        // start a new download process
        downloader = new TodaysOHLCBacklogDownloader(queue, id);
        table.put(id, downloader);
        fireOHLCBacklogStarted(id);
    }

    public void removeDownloader(String id) {
        table.remove(id);
    }

    /**
     * Deactivate all downloads if TW disconnected
     */
    public void twDisconnected() {
        Enumeration ids = table.keys();
        String id;
        TodaysOHLCBacklogDownloader downloader;

        while (ids.hasMoreElements()) {
            id = (String) ids.nextElement();
            downloader = table.get(id);
            if (downloader != null) {
                downloader.deactivate();
                downloader = null;
            }
        }
        table.clear();
    }

    public void twConnected() {

    }

}