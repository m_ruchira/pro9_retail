package com.isi.csvr.downloader;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;

import java.io.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 21, 2005
 * Time: 11:19:22 AM
 */
public class MarketDataDownloader implements Runnable, Downloader {
    private boolean active = true;
    private String id;
    private float version;
    private ZipFileDownloader downloader;
    private ProgressListener progressListener;
    private String ip = null;
    private int type;
    private boolean isDefault = true;

    public MarketDataDownloader(ProgressListener progressListener, String id, float version, String contentIP, int type, boolean isDef) {
        this.id = id;
        this.version = version;
        this.ip = contentIP;
        this.progressListener = progressListener;
        this.type = type;
        this.isDefault = isDef;
        Thread thread = new Thread(this, "MarketDataDownloader " + id);
        thread.start();
    }

    public static synchronized void extractFile(File file, String exchange) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                byte[] data = new byte[1000];
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));

                int len;

                File outFolder = new File(Settings.EXCHANGES_DATA_PATH + "/" + exchange);
                if (!outFolder.exists()) {
                    outFolder.mkdir();
                }
                outFolder = null;

                ZipEntry zipEntry = zIn.getNextEntry();
                while (zipEntry != null) {
                    FileOutputStream out = new FileOutputStream(Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/" + zipEntry.getName());
                    while (true) {
                        len = zIn.read(data);
                        if (len == -1) break; // end of streeam
                        out.write(data, 0, len);
                    }
                    out.flush();
                    out.close();
                    out = null;
                    zIn.closeEntry();
                    zipEntry = null;
                    zipEntry = zIn.getNextEntry();
                }

                zIn.close();
                zIn = null;
                data = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public void run() {
        while (active) {
            try {
//                String url = Meta.STOCK_MASTER_FILE + Meta.DS + id + Meta.FD + version + Meta.EOL;
                String url = "";
                if (this.type == Downloader.EXCHANGE_DATA) {
                    System.out.println("[StockMaster Download] Started " + id + SharedMethods.getMemoryDetails());
                    url = Meta.STOCK_MASTER_FILE + Meta.DS + id + Meta.FD + version + Meta.FD + Language.getSelectedLanguage() + Meta.EOL;
                    System.out.println("[StockMaster Download] == " + ip + " ,,, " + url);
                    downloader = new ZipFileDownloader(url, id, "EXGData", Constants.CONTENT_PATH_PRIMARY, ip);
                } else {
                    System.out.println("[Exchange Prop Download] Started " + id + SharedMethods.getMemoryDetails());
                    url = Meta.EXCG_PROPERTY_FILE + Meta.DS + id + Meta.FD + version + Meta.FD + Language.getSelectedLanguage() + Meta.EOL;
                    System.out.println("[Exchange Prop Download] == " + ip + " ,,, " + url);
                    downloader = new ZipFileDownloader(url, id, "EXGProp", Constants.CONTENT_PATH_PRIMARY, ip);
                }
                downloader.addProgressListener(progressListener);
                File[] files = downloader.downloadFiles();
                downloader = null;
                for (int i = 0; i < files.length; i++) {
                    if (files[i].length() > 0) {
                        extractFile(files[i], id);
                    }
                    try {
                        files[i].delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + id + "/market_" + Language.getSelectedLanguage() + ".msf";
                File bulkFile = new File(sLangSpecFileName);
                try {
                    if (bulkFile.exists() && !isDefault) {
                        //                    ExchangeStore.getSharedInstance().getExchange(id).setMasterFileType(Meta.FILE_TYPE_MSF);
                        createCSVFile(id);
//                        bulkFile.delete();
                    } else {
                        sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + id + "/market_" + Language.getSelectedLanguage() + ".csv";
                        bulkFile = new File(sLangSpecFileName);
                        if (bulkFile.exists()) {
                            ExchangeStore.getSharedInstance().getExchange(id).setMasterFileType(Meta.FILE_TYPE_CSV);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
//                bulkFile.delete();
                break;
            } catch (Exception e) {
                e.printStackTrace();
                active = false; // if error occured, must exit.
            } finally {
                progressListener = null;
                downloader = null;
            }
        }
        if (this.type == Downloader.EXCHANGE_DATA) {
            System.out.println("[StockMaster Download] Completed " + id + SharedMethods.getMemoryDetails());
        } else {
            System.out.println("[Exchange prop Download] Completed " + id + SharedMethods.getMemoryDetails());
        }
        MarketDataDownloadManager.getSharedInstance().removeDownloader(id + type);
    }

    public void createCSVFile(String exchange) {

        SmartProperties stockMaster = new SmartProperties(Meta.DS);
        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".msf";
        try {
            stockMaster.loadCompressed(sLangSpecFileName);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
//            stockMaster.remove("VERSION");
        } catch (Exception e) {
            e.printStackTrace();
        }
//        File bulkFile = new File(sLangSpecFileName);
//        bulkFile.delete();

        Properties prop = new Properties();
        Hashtable hash = new Hashtable();
        Enumeration exchageSymbols = stockMaster.keys();
        while (exchageSymbols.hasMoreElements()) {
            String symbol = (String) exchageSymbols.nextElement();
            String val = (String) stockMaster.get(symbol);
            hash.put(symbol, val);
        }
        sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + id + "/market_" + Language.getSelectedLanguage() + ".csv";


        FileOutputStream oOut = null;
        try {
            oOut = new FileOutputStream(sLangSpecFileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            Enumeration keys = hash.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String value = (String) hash.get(key);

                oOut.write((key + Meta.DS + value + "\n").getBytes());

            }
            oOut.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        ExchangeStore.getSharedInstance().getExchange(id).setMasterFileType(Meta.FILE_TYPE_CSV);
    }

    public void deactivate() {
        if (downloader != null) {
            downloader.interrupt();
        }
        active = false;
    }

    public boolean isActive() {
        return active;
    }
}
