package com.isi.csvr;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 21, 2006
 * Time: 10:36:44 AM
 */
public class WelcomeScreen extends JDialog implements HyperlinkListener {


    public WelcomeScreen() throws HeadlessException {
        super(Client.getInstance().getFrame(), false);
        try {
            setLayout(new BorderLayout());
            JEditorPane editorPane = new JEditorPane("file:" + ((String) System.getProperties().get("user.dir")).replaceAll("\\\\", "/")
                    + "/templates/WelcomeScreen_" + Language.getLanguageTag() + ".htm");
            editorPane.setEditable(false);
            editorPane.setBorder(BorderFactory.createEtchedBorder());
            editorPane.addHyperlinkListener(this);
            add(editorPane);
//            setUndecorated(true);
            setResizable(false);
            setSize(612, 410);
            setTitle(Language.getString("WELCOME_TO_PRO"));
            setLocationRelativeTo(Client.getInstance().getFrame());

            editorPane.doLayout();
            editorPane.updateUI();
            setVisible(true);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new WelcomeScreen();
            }
        });
    }

    public void hyperlinkUpdate(final HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            if (e instanceof HTMLFrameHyperlinkEvent) {
            } else {
                try {
                    if (e.getDescription().endsWith("[CLOSE]")) {
                        dispose();
//                        System.exit(0);
                    } else if (e.getDescription().endsWith("[DONOTSHOW]")) {
                        Settings.setShowWelcomeScreen(false);
                        dispose();
//                        System.exit(0);
                    } else {
//                        System.out.println(e.getURL().getPath());
//                        System.out.println("11111111"+e.getDescription());
//                        Runtime.getRuntime().exec("start.exe \""+ e.getURL().getPath()+"\"");
                        NativeMethods.showHelp(e.getDescription());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}