package com.isi.csvr.scanner;

import com.isi.csvr.shared.Constants;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Apr 22, 2008
 * Time: 12:39:04 PM
 * To change this template use File | Settings | File Templates.
 */

public class TWProgressBarLabel extends JLabel {
    private int value = 0;
    private int min = 0;
    private int max = 100;
    private int width = 100;
    private int height = 10;
    private int pixels = 0;
    private String label = "";

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setDepth(int value) {
        this.value = value;
        width = getWidth();
        height = 3;
        pixels = Math.round(width * value / max);
        setText(getLabel(value));
        label = (getLabel(value));
        repaint();
    }

    private String getLabel(int result) {
        if (result == 0) {
            return "";
        } else if (result < 2) {
            return Constants.SHORT_PASSWORD;
        } else if (result < 45) {
            return Constants.BAD_PASSWORD;
        } else if (result < 75) {
            return Constants.GOOD_PASSWORD;
        } else {
            return Constants.STRONG_PASSWORD;
        }
    }

    public void paint(Graphics g) {
//            super.paint(g);
//            g.clipRect(0,0,width,getHeight() - height);
        int ascent = g.getFontMetrics().getAscent();
        g.drawString(label, 0, ascent);
        for (int i = 0; i < pixels; i++) {
            boolean isFirstHalf = (i * 2 < width);
            paintPixel(g, i, isFirstHalf);
        }
    }

    public void paintPixel(Graphics g, int value, boolean isFirstHalf) {
        int rColor = 255 * (width - value) / width;
        int gColor = 255 * (value) / width;
        int bColor = 0;
//            if(isFirstHalf){
        g.setColor(new Color(rColor, gColor, bColor));
        g.fillRect(value, getHeight() - height, 1, height);
//            } else {
//                g.setColor(Color.GREEN);
//                g.fillRect(value,getHeight() - height,1,height);
//            }
    }
}
