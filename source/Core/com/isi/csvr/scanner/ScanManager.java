package com.isi.csvr.scanner;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.scanner.Scans.ScanRecord;
import com.isi.csvr.scanner.Scans.ScanSlotComparator;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWDateFormat;

import java.util.*;

//import com.isi.csvr.volumewatcher.ExchangeInfo;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 19, 2008
 * Time: 12:20:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScanManager {

    public static long BeginDayOfWeekAdj = Meta.ADJ_SATURDAY;
    transient private static GregorianCalendar calendar = new GregorianCalendar(); //calen = Calendar.getInstance();
    ArrayList<DynamicArray> sources = new ArrayList<DynamicArray>();
    private ArrayList<String> companyList = new ArrayList<String>();//use the company object later
    private DynamicArray graphStore;
    private int interval = Meta.DAILY;
    private List<ScanResultListner> listeners;

    public ScanManager() {
        listeners = Collections.synchronizedList(new ArrayList<ScanResultListner>());
        //setScanCoympanyList();
        graphStore = new DynamicArray();
        ScanSlotComparator scComparator = new ScanSlotComparator();
        graphStore.setComparator(scComparator);

        //do scan
        // testGraphStore();

    }

    private void testGraphStore() {


    }

    public void addScanResultListener(ScanResultListner listener) {
        listeners.add(listener);
    }

    public void fireUpDateUI() {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ScanResultListner listener = listeners.get(i);
                if (listener != null) {
                    listener.SymbolAdded();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //get all the symbols in all exchanges
    public ArrayList setScanCoympanyList(String exchangeKey, String sector, String selType) {
        companyList.clear();

        if (exchangeKey.equals("All")) {
            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                try {
                    Exchange exchange = exchanges.nextElement();
                    if (exchange.isDefault()) {
                        String exhangeSymbol = exchange.getSymbol();
                        Enumeration e = DataStore.getSharedInstance().getExchangeSymbolStore(exhangeSymbol).keys();
                        while (e.hasMoreElements()) { //go throgh symbols
                            String symbol = (String) e.nextElement();
                            //System.out.println("symbol: " + symbol);
                            Stock stock = DataStore.getSharedInstance().getExchangeSymbolStore(exhangeSymbol).get(symbol);
                            String key = stock.getKey();
                            if (stock.getInstrumentType() != com.isi.csvr.shared.Meta.INSTRUMENT_INDEX) {
                                companyList.add(key);
                            }
                            stock = null;
                        }
                    }
                } catch (Exception e) {

                }
            }
            exchanges = null;
        } else if (selType.equals("EXG")) {//exchange is selected
            Enumeration e = DataStore.getSharedInstance().getExchangeSymbolStore(exchangeKey).keys();
            while (e.hasMoreElements()) { //go throgh symbols
                String symbol = (String) e.nextElement();
                //System.out.println("symbol: " + symbol);
                Stock stock = DataStore.getSharedInstance().getExchangeSymbolStore(exchangeKey).get(symbol);
                String key = stock.getKey();
                if (stock.getInstrumentType() != com.isi.csvr.shared.Meta.INSTRUMENT_INDEX && sector.equals("All")) {
                    companyList.add(key);
                } else if (stock.getInstrumentType() != com.isi.csvr.shared.Meta.INSTRUMENT_INDEX && stock.getSectorCode().equals(sector)) {
                    companyList.add(key);
                }

                stock = null;
            }

        } else if (selType.equals("WL")) {
            //watch lists
            Vector mainboard_view = ViewSettingsManager.getMainBoardViews();
            Enumeration en = mainboard_view.elements();
            while (en.hasMoreElements()) {
                final ViewSetting vs = (ViewSetting) en.nextElement();
//                     System.out.println("table id = "+vs.getID());
                if (vs.getMainType() == ViewSettingsManager.CUSTOM_VIEW) {
//                        System.out.println("table id = "+vs.getID());
                    Symbols symbols = vs.getSymbols();
                    String[] symbolArr = symbols.getSymbols();
                    for (int i = 0; i < symbolArr.length; i++) {
                        companyList.add(symbolArr[i]);
                    }

                }
            }

        }
        System.out.println("companyList.size();  " + companyList.size());

        return companyList;
    }

//public voizid populateGraphStore(){
//
//
//}

    //all the sources
//    public ArrayList  addScanRequest(){
//
//        try{
//        File file = new File("E:\\e\\Development\\Charting\\companysize.txt");
//
//        FileOutputStream fileOut = new FileOutputStream(file);
//        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(fileOut));
//        if (companyList.size()> 1){
//            for (int i = 0; i < companyList.size(); i++) {
//
//               DynamicArray companyHistory  = OHLCStore.getInstance().addScanHistoryRequest(companyList.get(i));
//               // DynamicArray companyHistory  =  HistoryFilesManager.readHistoryData(companyList.get(i));
//                //System.out.println("comapny: " + companyList.get(i) + "  size: " + companyHistory.size());
//                System.out.println("comapny: " + companyList.get(i) + "  size: " + companyHistory.size());
//                out.write("comapny: " + companyList.get(i) + "  size: " + companyHistory.size() + "\n");
//                sources.add(companyHistory);
//            }
//
//        }
//            out.close();
//            fileOut.close();
//        }catch(Exception e){
//
//        }
//        return sources;

    //}

    public DynamicArray addScanRequest(String companyKey) {

        DynamicArray companyHistory = OHLCStore.getInstance().addScanHistoryRequest(companyKey);
        return companyHistory;
    }

//        private void populateGraphStore() {
//            graphStore = new DynamicArray();
//            ScanSlotComparator scComparator = new ScanSlotComparator();
//            graphStore.setComparator(scComparator);
//            for (int j = 0; j < sources.size(); j++) {
//                DynamicArray sourceList = (DynamicArray)sources.get(j);
//                long currentBeginTime;
//                long lastCurrentBeginTime = 0;
//                long latestTime = getLatestTimeMillisec();
//                long tmpTime;
//                try {
//                    for (int i = 0; i < sourceList.size(); i++) {
//                        Object tmpO = sourceList.get(i);
//                        if ((tmpO != null) && (tmpO instanceof ScanRecord)) {
//                            ScanRecord sR = (ScanRecord)tmpO;
//                            if (sR.getTime() == 0) {
//                                continue;
//                            } else {
//                                tmpTime = sR.getTime() * 60000L;
//
//                                ////tmpTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, tmpTime);
//
//                                currentBeginTime = getCurrentBeginTime(false, tmpTime, latestTime);
//                                System.out.println("companyNO"  + j + " timeindex: " + i +    "time: " + tmpTime + "latestTime: " + latestTime  +  " currentBeginTime: " + currentBeginTime);
//                                //if (tmpLatestTime < currentBeginTime) tmpLatestTime = currentBeginTime;
//                                ScanPoint aPoint = getChartPoint(currentBeginTime, j);
//                                if (aPoint.Open == 0) aPoint.Open = sR.getOpen();
//                                if (aPoint.High < sR.getHigh()) aPoint.High = sR.getHigh();
//                                if (aPoint.Low > sR.getLow()) aPoint.Low = sR.getLow();
//                                aPoint.Close = sR.getClose();
//                                aPoint.Change = aPoint.Change + sR.getChange();
//                                aPoint.PercentChange = (100f + aPoint.PercentChange) * (1 + sR.getPcntChange()/ 100f) - 100f;
//                                if (currentBeginTime != lastCurrentBeginTime) {
//                                    aPoint.Volume = sR.getVolume();
//                                } else {
//                                    aPoint.Volume += sR.getVolume();
//                                }
//                                lastCurrentBeginTime = currentBeginTime;
//
//                            }
//                        }
//                    }
//                    //return true;
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                    //return false;
//                }
//            }
//
//            System.out.println("end of populating grapsh store......");
//        }


    public void clearGraphStore() {
        graphStore.clear();

    }

    //todo : should synchronize this:  when more than one scanning is happening
    public synchronized void populateGraphStore(DynamicArray sourceList) {

        long currentBeginTime;
        long lastCurrentBeginTime = 0;
        long latestTime = getLatestTimeMillisec();
        long tmpTime;
        try {
            for (int i = 0; i < sourceList.size(); i++) {
                Object tmpO = sourceList.get(i);
                if ((tmpO != null) /*&& (tmpO instanceof ScanRecord)*/) {
                    ScanRecord sR = (ScanRecord) tmpO;
                    if (sR.getTime() == 0) {
                        continue;
                    } else {
                        //tmpTime = sR.getTime() * 60000L;
                        tmpTime = sR.getTime();
                        TWDateFormat f = new TWDateFormat("yyyy MM dd");
                        //System.out.println("**************** "+f.format(tmpTime));

                        ////tmpTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, tmpTime);

                        currentBeginTime = getCurrentBeginTime(false, tmpTime, latestTime);
                        //System.out.println("companyNO"   + " timeindex: " + i +    "time: " + tmpTime + "latestTime: " + latestTime  +  " currentBeginTime: " + currentBeginTime);
                        //if (tmpLatestTime < currentBeginTime) tmpLatestTime = currentBeginTime;
                        ScanPoint aPoint = getChartPoint(currentBeginTime, Meta.GRAPH_INDEX);
                        if (aPoint.Open == 0) aPoint.Open = sR.getOpen();
                        if (aPoint.High < sR.getHigh()) aPoint.High = sR.getHigh();
                        if (aPoint.Low > sR.getLow())
                            aPoint.Low = sR.getLow(); //low value is assign to a float max value
//                        if (aPoint.Low < sR.getLow()) aPoint.Low = sR.getLow();
                        aPoint.Close = sR.getClose();
                        aPoint.Change = aPoint.Change + sR.getChange();
                        aPoint.PercentChange = (100f + aPoint.PercentChange) * (1 + sR.getPcntChange() / 100f) - 100f;
                        if (currentBeginTime != lastCurrentBeginTime) {
                            aPoint.Volume = sR.getVolume();
                        } else {
                            aPoint.Volume += sR.getVolume();
                        }
                        lastCurrentBeginTime = currentBeginTime;

                    }
                }
            }
            //return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            //return false;
        }
    }

    private ScanPoint getChartPoint(long timeIndex, int graphIndex) {
        ScanSlot singlePoint;
        int searchIndex = 0;
        //graphIndex = graphIndex + 1;
        ScanSlot longArr = new ScanSlot(timeIndex);
        searchIndex = graphStore.indexOf(longArr);
//            System.out.println("searchindex : " + searchIndex + " timeindex:" + timeIndex + "graphIndex: " + graphIndex);
        if (searchIndex >= 0) { // point already exists
            singlePoint = (ScanSlot) graphStore.get(searchIndex);
        } else {
            singlePoint = new ScanSlot(timeIndex);
            graphStore.add(singlePoint);
        }
        return singlePoint.getChartPoint(graphIndex);
    }


    public int getClosestIndexFortheTime(long t) {
        int searchIndex = 0;

        /**************************/
        //graphIndex = graphIndex + 1;
        ScanSlot longArr = new ScanSlot(t);
//        searchIndex = graphStore.indexOf(longArr);
        searchIndex = graphStore.indexOfContainingValue(longArr);
        /**************************/
        longArr = null;
        if (searchIndex >= 0) {
            return searchIndex;
        } else {
            return Math.min(-searchIndex - 1, graphStore.size() - 1);
        }
    }

    public long getCurrentBeginTime(boolean isIntraday, long aTime, long latestTime) {
        if (isIntraday) {
            aTime = aTime / (interval * 10000000L);
            return (aTime) * interval * 10000000L;
        } else {
            Date dateTime;//check whether corect
            int days;
            switch (interval) {
                case Meta.YEARLY: // yearly
                    calendar.setTimeInMillis(aTime);
                    days = calendar.get(Calendar.DAY_OF_YEAR); //-1
                    calendar.add(Calendar.DATE, -days);
                    calendar.add(Calendar.YEAR, 1);
                    long firstDayYear = Math.min(calendar.getTimeInMillis(), latestTime); //sathcheck
                    firstDayYear -= firstDayYear % Meta.TICKS_PER_DAY;
                    return firstDayYear;
                case Meta.QUARTERLY: // quarterly


                    calendar.setTimeInMillis(aTime);
                    days = calendar.get(Calendar.DAY_OF_MONTH); //-1
                    int monthAdj = 3 - ((calendar.get(Calendar.MONTH) + 2) % 3);
                    calendar.add(Calendar.DATE, -days + 1);
                    calendar.add(Calendar.MONTH, monthAdj);
                    calendar.add(Calendar.DATE, -1); // this is the last day of current quarter
                    long lastDayOfQuarter = calendar.getTimeInMillis();//Math.Min(dateTime.Ticks, latestTime);
                    lastDayOfQuarter -= lastDayOfQuarter % Meta.TICKS_PER_DAY;
                    return lastDayOfQuarter;
                case Meta.MONTHLY: // monthly
                    calendar.setTimeInMillis(aTime);
                    days = calendar.get(Calendar.DAY_OF_MONTH); //-1
                    calendar.add(Calendar.DATE, -days + 1);
                    calendar.add(Calendar.MONTH, 1);
                    calendar.add(Calendar.DATE, -1);
                    long firstDayOfMonth = Math.min(calendar.getTimeInMillis(), latestTime);
                    firstDayOfMonth -= firstDayOfMonth % Meta.TICKS_PER_DAY;
                    return firstDayOfMonth;
                case Meta.WEEKLY:  // weekly
                    aTime = (aTime - (long) BeginDayOfWeekAdj * 10000000L) / (interval * 10000000L);
                    long result = (aTime) * interval * 10000000L + (long) BeginDayOfWeekAdj * 10000000L + 6L * 24L * 3600L * 10000000L;
                    result = Math.min(result, latestTime - latestTime % Meta.TICKS_PER_DAY);
                    return result;
                default: // daily
                    aTime = aTime / (interval * 1000);
                    return (aTime) * interval * 1000L;
            }
        }
    }

    public long getLatestTimeMillisec() {
        if (sources.size() > 0) {
            long result = 0;
            long time;
            for (int i = 0; i < sources.size(); i++) {
                DynamicArray al = (DynamicArray) sources.get(i);
                //ArrayList al = (ArrayList)sources.get(i);
                if (al.size() == 0) continue;
                ScanRecord dR = (ScanRecord) al.get(al.size() - 1);
                ////////////// patch to avoid end 0 point /////////////////
                if (dR.getTime() == 0) {
                    if (al.size() > 1) {
                        dR = (ScanRecord) al.get(al.size() - 2);
                    } else {
                        continue;
                    }
                }
                //////////////// end of patch /////////////////////////////
                time = dR.getTime(); // *60000L
                //time += ChartInterface.getTimeZoneAdjustment(graph.Symbol, time);
                result = Math.max(time, result);
            }
            if (result > 0) return result;
        }

        return System.currentTimeMillis();
        //return Calendar.getInstance().getTimeInMillis();//get current time in milliseconds
        //return    DateTime.Now.Ticks;//
    }

//    private void populateGraphStorebackup() {
//            graphStore = new SortedArrayList(new ScanSlot(0));
//            for (int j = 0; j < sources.Count; j++) {
//                ArrayList sourceList = (ArrayList)sources[j];
//                long currentBeginTime;
//                long lastCurrentBeginTime = 0;
//                long latestTime = getLatestTimeMillisec();
//                long tmpTime;
//                try {
//                    for (int i = 0; i < sourceList.Count; i++) {
//                        object tmpO = sourceList[i];
//                        if ((tmpO != null) && (tmpO is ScanRecord)) {
//                            ScanRecord sR = (ScanRecord)tmpO;
//                            if (sR.Time == 0) {
//                                continue;
//                            } else {
//                                tmpTime = sR.Time;
//                                ////tmpTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, tmpTime);
//
//                                currentBeginTime = getCurrentBeginTime(false, tmpTime, latestTime);
//                                //if (tmpLatestTime < currentBeginTime) tmpLatestTime = currentBeginTime;
//                                ScanPoint aPoint = getChartPoint(currentBeginTime, j);
//                                if (aPoint.Open == 0) aPoint.Open = sR.Open;
//                                if (aPoint.High < sR.High) aPoint.High = sR.High;
//                                if (aPoint.Low > sR.Low) aPoint.Low = sR.Low;
//                                aPoint.Close = sR.Close;
//                                aPoint.Change = aPoint.Change + sR.Change;
//                                aPoint.PercentChange = (100f + aPoint.PercentChange) * (1 + sR.PercentChange / 100f) - 100f;
//                                if (currentBeginTime != lastCurrentBeginTime) {
//                                    aPoint.Volume = sR.Volume;
//                                } else {
//                                    aPoint.Volume += sR.Volume;
//                                }
//                                lastCurrentBeginTime = currentBeginTime;
//
//                            }
//                        }
//                    }
//                    //return true;
//                } catch (Exception ex) {
//                    ExceptionHandler.Handle(ex);
//                    //return false;
//                }
//            }

    //        }

    public ArrayList<String> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(ArrayList<String> companyList) {
        this.companyList = companyList;
    }

    public ArrayList<DynamicArray> getSources() {
        return sources;
    }

    public void setSources(ArrayList<DynamicArray> sources) {
        this.sources = sources;
    }

    public DynamicArray getGraphStore() {
        return graphStore;
    }

    public void setGraphStore(DynamicArray graphStore) {
        this.graphStore = graphStore;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public ScanPoint readScanPoint(int pointIndex, int srcIndex) {
        if (pointIndex >= 0) {
            ScanSlot singlePoint = (ScanSlot) (graphStore.get(pointIndex));
            if (singlePoint.getLength() > srcIndex) {
                return singlePoint.get(srcIndex);
            }
        }
        return null;
    }

    public long getPeriodBeginMillisec(long time) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.add(Calendar.YEAR, -1);
        return calendar.getTimeInMillis();
    }
}