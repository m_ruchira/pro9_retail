package com.isi.csvr.scanner.scannerGUIs;

import com.isi.csvr.scanner.Scans.High;
import com.isi.csvr.scanner.Scans.ScanBase;
import com.isi.csvr.shared.TWComboBox;
import com.isi.csvr.shared.TWComboItem;
import com.isi.csvr.table.TWTextField;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Aug 20, 2008
 * Time: 11:55:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class HighUI extends JPanel {
    public TWComboBox cmbDisplayTop;
    public ArrayList<TWComboItem> listDislayTop;
    private JLabel lblDisplayTop;
    private JLabel lblSpecify;
    private TWTextField txtSpecify;


    public HighUI() {

    }

    public String getTxtSpecify() {
        return txtSpecify.getText();
    }

    public void setTxtSpecify(String value) {
        this.txtSpecify.setText(value);
    }
//    public HighUI() {
//        System.out.println("=======================================++++++++");
//
//        lblDisplayTop = new JLabel(Language.getString("DISPLAY_TOP"));
//        listDislayTop = new ArrayList<TWComboItem>(); //initialize each individually
//
//        lblSpecify = new JLabel(Language.getString("SPECIFY"));
//        lblSpecify.setEnabled(false);
//        txtSpecify = new TWTextField();
//        txtSpecify.setText("10");
//        txtSpecify.setEnabled(false);
//
//        listDislayTop.add(new TWComboItem(0, "5", "5"));
//        listDislayTop.add(new TWComboItem(1, "20", "20"));
//        listDislayTop.add(new TWComboItem(2, "50", "50"));
//        listDislayTop.add(new TWComboItem(3, Language.getString("CATEGORY_OTHER"), "OTHER"));
//
//        cmbDisplayTop = new TWComboBox(new TWComboModel(listDislayTop));
//
//        cmbDisplayTop.addItemListener(new java.awt.event.ItemListener() {
//            public void itemStateChanged(java.awt.event.ItemEvent e) {
//                TWComboItem item = (TWComboItem) e.getItem();
//                if (item == null) return;
//
//                if ((item.getType()).equals("OTHER")) {
//                    txtSpecify.setEnabled(true);
//                    lblSpecify.setEnabled(true);
//                } else {
//                    txtSpecify.setEnabled(false);
//                    lblSpecify.setEnabled(false);
//
//                }
//            }
//        });
//        cmbDisplayTop.setSelectedIndex(1);
//        JPanel specifyPnl = new JPanel();
//        specifyPnl.setName("H_S_PANEL");
//        specifyPnl.setLayout(new FlexGridLayout(new String[]{"50", "100%"}, new String[]{"100%"}, 0, 0));
//        specifyPnl.add(lblSpecify);
//        specifyPnl.add(txtSpecify);
//        setLayout(new FlexGridLayout(new String[]{"10", "70", "10", "100%", "10"}, new String[]{"20", "4", "20", "100%"}, 0, 0));
//
//        add(new JLabel());
//        add(lblDisplayTop);
//        add(new JLabel());
//        add(cmbDisplayTop);
//        add(new JLabel());
//
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//        add(specifyPnl);
//        add(new JLabel());
//
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//        add(new JLabel());
//    }

    protected ScanBase getScanObject() {
        High scanObj = new High();

//           if (getSelectedCount() < 0) {
//            throw new NumberFormatException();
//        }
        scanObj.setHigh(5);
//        scanObj.setCount(getSelectedCount());
        return scanObj;
    }

    private int getSelectedCount() {
        switch (cmbDisplayTop.getSelectedIndex()) {
            case 0:
                return 5;
            case 1:
                return 20;
            case 2:
                return 50;
            default:
                return Integer.parseInt(txtSpecify.getText());
        }
    }

    public String getselectedDisplayTop() {
        return (String) cmbDisplayTop.getSelectedItem();
    }


}
