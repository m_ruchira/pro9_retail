package com.isi.csvr.scanner.scannerGUIs;

import com.isi.csvr.scanner.Scans.GapUps;
import com.isi.csvr.scanner.Scans.ScanBase;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 11:51:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class GapUpsUI extends JPanel {
    JLabel lblGapup;

    TWTextField txtGapup;


    public GapUpsUI() {
        lblGapup = new JLabel(Language.getString("GAP_UP_PERC"));
        Dimension commonDimennsion = new Dimension(100, 20);
        txtGapup = new TWTextField();
        txtGapup.setText("1.00");
        String[] widths = {"40%", "40%"};
        String[] heights = {"20"};


        setLayout(new FlexGridLayout(new String[]{"10", "110", "10", "100%", "10"}, new String[]{"20", "100%"}, 0, 0));
        add(new JLabel());
        add(lblGapup);
        add(new JLabel());
        add(txtGapup);
        add(new JLabel());


        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());


    }

    protected ScanBase getScanObject() {
        GapUps scanObj = new GapUps();
        if (Float.parseFloat(txtGapup.getText()) < 0) {
            throw new NumberFormatException();
        }
        scanObj.setGapPercent(Float.parseFloat(txtGapup.getText()));
        return scanObj;
    }


    public String getGapup() {
        return txtGapup.getText();
    }

    public void setGapUp(String value) {
        txtGapup.setText(value);
    }


}
