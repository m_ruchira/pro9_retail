package com.isi.csvr.scanner.scannerGUIs;

import com.isi.csvr.scanner.Scans.ScanBase;
import com.isi.csvr.scanner.Scans.Volume;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWComboBox;
import com.isi.csvr.shared.TWComboItem;
import com.isi.csvr.shared.TWComboModel;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 11:49:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class VolumeUI extends JPanel {

    public TWComboBox cmbType;
    public TWTextField txtNoOfTimes;
    public TWComboBox cmbNoOfDays;
    public ArrayList<TWComboItem> listType;
    public ArrayList<TWComboItem> listNoOfDays;
    private JLabel lblType;
    private JLabel lblNoOfTimes;
    private JLabel lblNoOfDays;
    private JLabel lblSpecify;
    private TWTextField txtSpecify;

    public VolumeUI() {

        lblType = new JLabel(Language.getString("TYPE"));
        lblType.setPreferredSize(new Dimension(100, 20));
        lblNoOfTimes = new JLabel(Language.getString("NO_OF_TIMES"));
        lblNoOfTimes.setPreferredSize(new Dimension(100, 20));
        lblNoOfDays = new JLabel(Language.getString("NO_OF_DAYS"));
        lblNoOfDays.setPreferredSize(new Dimension(100, 20));
        listType = new ArrayList<TWComboItem>(); //initialize each individually

        listNoOfDays = new ArrayList<TWComboItem>(); //initialize each individually
        lblSpecify = new JLabel(Language.getString("SPECIFY"));
        lblSpecify.setEnabled(false);
        lblSpecify.setPreferredSize(new Dimension(100, 20));
        txtSpecify = new TWTextField();
        txtSpecify.setPreferredSize(new Dimension(100, 20));
        txtSpecify.setText("10");
        txtSpecify.setEnabled(false);

        listType.add(new TWComboItem(0, Language.getString("AVERAGE_VOLUME")));
        listType.add(new TWComboItem(1, Language.getString("HIGHEST_VOLUME")));

        listNoOfDays.add(new TWComboItem(0, "5", "5"));
        listNoOfDays.add(new TWComboItem(1, "20", "20"));
        listNoOfDays.add(new TWComboItem(2, "50", "50"));
        listNoOfDays.add(new TWComboItem(3, Language.getString("CATEGORY_OTHER"), "OTHER"));

        cmbType = new TWComboBox(new TWComboModel(listType));
        lblSpecify.setPreferredSize(new Dimension(100, 20));
        cmbType.setSelectedIndex(0);
        txtNoOfTimes = new TWTextField();
        txtNoOfTimes.setText("1.00");
        cmbNoOfDays = new TWComboBox(new TWComboModel(listNoOfDays));
        cmbNoOfDays.setPreferredSize(new Dimension(100, 20));

        cmbNoOfDays.setSelectedIndex(0);
        cmbNoOfDays.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {
                TWComboItem item = (TWComboItem) e.getItem();
                if (item == null) return;

                if ((item.getType()).equals("OTHER")) {
                    txtSpecify.setEnabled(true);
                    lblSpecify.setEnabled(true);
                } else {
                    txtSpecify.setEnabled(false);
                    lblSpecify.setEnabled(false);

                }
            }
        });

        JPanel specifyPnl = new JPanel();
        specifyPnl.setName("V_S_PANEL");
        specifyPnl.setLayout(new FlexGridLayout(new String[]{"50", "100%"}, new String[]{"100%"}, 0, 0));
        specifyPnl.add(lblSpecify);
        specifyPnl.add(txtSpecify);

        setLayout(new FlexGridLayout(new String[]{"10", "70", "10", "100%", "10"}, new String[]{"20", "4", "20", "4", "20", "4", "18", "100%"}, 0, 0));
        add(new JLabel());
        add(lblType);
        add(new JLabel());
        add(cmbType);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(lblNoOfTimes);
        add(new JLabel());
        add(txtNoOfTimes);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(lblNoOfDays);
        add(new JLabel());
        add(cmbNoOfDays);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(specifyPnl);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());


    }

    public TWTextField getTxtNoOfTimes() {
        return txtNoOfTimes;
    }

    public void setTxtNoOfTimes(TWTextField txtNoOfTimes) {
        this.txtNoOfTimes = txtNoOfTimes;
    }

    public String getTxtSpecify() {
        return txtSpecify.getText();
    }

    public void setTxtSpecify(String value) {
        this.txtSpecify.setText(value);
    }

    public ScanBase getScanObject() {

        Volume scanObj = new Volume();
        scanObj.setCalculationType(cmbType.getSelectedIndex());
        if (Float.parseFloat(txtNoOfTimes.getText()) < 0 || getSelectedPeriod() < 0) {
            throw new NumberFormatException();
        }
        scanObj.setNoOfTimes(Float.parseFloat(txtNoOfTimes.getText()));
        scanObj.setPeriod(getSelectedPeriod());
        return scanObj;

    }

    private int getSelectedPeriod() {
        switch (cmbNoOfDays.getSelectedIndex()) {
            case 0:
                return 5;
            case 1:
                return 20;
            case 2:
                return 50;
            default:
                return Integer.parseInt(txtSpecify.getText());
        }
    }

    public JLabel getLblType() {
        return lblType;
    }

    public void setLblType(JLabel lblType) {
        this.lblType = lblType;
    }


    public JLabel getLblNoOfDays() {
        return lblNoOfDays;
    }

    public void setLblNoOfDays(JLabel lblNoOfDays) {
        this.lblNoOfDays = lblNoOfDays;
    }

    public String getselectedType() {
        return (String) cmbType.getSelectedItem();
    }

    public String getselectedNOOfTimes() {
        return txtNoOfTimes.getText();
    }

    public String getselectedNoOfDays() {
        return (String) cmbNoOfDays.getSelectedItem();
    }

    public int getselectedTypeIndex() {
        return cmbType.getSelectedIndex();
    }


    public int getselectedNoOfDaysIndex() {
        return cmbNoOfDays.getSelectedIndex();
    }

    public void setNoOfTimes(String value) {
        txtNoOfTimes.setText(value);
    }
}
