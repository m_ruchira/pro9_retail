package com.isi.csvr.scanner.scannerGUIs;

import com.isi.csvr.scanner.Scans.Breakouts;
import com.isi.csvr.scanner.Scans.ScanBase;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWComboBox;
import com.isi.csvr.shared.TWComboItem;
import com.isi.csvr.shared.TWComboModel;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 11:52:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class BreakOutsUI extends JPanel {

    public TWComboBox cmbType;
    public TWComboBox cmbPriceField;
    public TWComboBox cmbNoOfDays;
    public ArrayList<TWComboItem> listType;
    public ArrayList<TWComboItem> listPriceField;
    public ArrayList<TWComboItem> listNoOfDays;
    private JLabel lblType;
    private JLabel lblPriceField;
    private JLabel lblNoOfDays;
    private JLabel lblSpecify;
    private TWTextField txtSpecify;

    public BreakOutsUI() {
        lblType = new JLabel(Language.getString("TYPE"));
        lblType.setPreferredSize(new Dimension(102, 20));

        lblPriceField = new JLabel(Language.getString("PRICE_FIELD"));
        lblPriceField.setPreferredSize(new Dimension(102, 20));

        lblNoOfDays = new JLabel(Language.getString("NO_OF_DAYS"));
        lblNoOfDays.setPreferredSize(new Dimension(102, 20));
        listType = new ArrayList<TWComboItem>(); //initialize each individually
        listPriceField = new ArrayList<TWComboItem>(); //initialize each individually
        listNoOfDays = new ArrayList<TWComboItem>(); //initialize each individually
        lblSpecify = new JLabel(Language.getString("SPECIFY"));
        lblSpecify.setEnabled(false);
        txtSpecify = new TWTextField();
        txtSpecify.setEnabled(false);
        //txtSpecify.setPreferredSize(new Dimension(102,20));
        txtSpecify.setText("10");


        listType.add(new TWComboItem(0, Language.getString("HIGH")));
        listType.add(new TWComboItem(1, Language.getString("LOW")));

        listPriceField.add(new TWComboItem(0, Language.getString("HIGH")));
        listPriceField.add(new TWComboItem(1, Language.getString("LOW")));
        listPriceField.add(new TWComboItem(2, Language.getString("CLOSE")));


        listNoOfDays.add(new TWComboItem(0, "5", "5"));
        listNoOfDays.add(new TWComboItem(1, "20", "20"));
        listNoOfDays.add(new TWComboItem(2, "50", "50"));
        listNoOfDays.add(new TWComboItem(3, Language.getString("CATEGORY_OTHER"), "OTHER"));

        cmbType = new TWComboBox(new TWComboModel(listType));
        cmbType.setPreferredSize(new Dimension(102, 20));
        cmbType.setSelectedIndex(0);
        cmbPriceField = new TWComboBox(new TWComboModel(listPriceField));
        cmbPriceField.setSelectedIndex(0);
        cmbNoOfDays = new TWComboBox(new TWComboModel(listNoOfDays));
        cmbNoOfDays.setSelectedIndex(0);

        cmbNoOfDays.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {
                TWComboItem item = (TWComboItem) e.getItem();
                if (item == null) return;

                if ((item.getType()).equals("OTHER")) {
                    txtSpecify.setEnabled(true);
                    lblSpecify.setEnabled(true);
                } else {
                    txtSpecify.setEnabled(false);
                    lblSpecify.setEnabled(false);

                }
            }
        });


        JPanel specifyPnl = new JPanel();
        specifyPnl.setName("BO_S_PANEL");
        specifyPnl.setLayout(new FlexGridLayout(new String[]{"60", "100%"}, new String[]{"100%"}, 0, 0));
        specifyPnl.add(lblSpecify);
        specifyPnl.add(txtSpecify);

        setLayout(new FlexGridLayout(new String[]{"10", "90", "10", "100%", "10"}, new String[]{"20", "4", "20", "4", "20", "4", "20", "100%"}, 0, 0));
        add(new JLabel());
        add(lblType);
        add(new JLabel());
        add(cmbType);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(lblPriceField);
        add(new JLabel());
        add(cmbPriceField);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(lblNoOfDays);
        add(new JLabel());
        add(cmbNoOfDays);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(specifyPnl);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
    }

    public String getTxtSpecify() {
        return txtSpecify.getText();
    }

    public void setTxtSpecify(String value) {
        this.txtSpecify.setText(value);
    }

    protected ScanBase getScanObject() {
        Breakouts scanObj = new Breakouts();
        scanObj.setHigh(isHigh());
        scanObj.setPriceField((byte) (cmbPriceField.getSelectedIndex() + 1));
        if (getSelectedPeriod() < 0) {
            throw new NumberFormatException();
        }
        scanObj.setPeriod(getSelectedPeriod());
        return scanObj;
    }

    private boolean isHigh() {

        if (cmbType.getSelectedIndex() == 0) {
            return true;
        } else {
            return false;
        }
    }

    private int getSelectedPeriod() {
        switch (cmbNoOfDays.getSelectedIndex()) {
            case 0:
                return 5;
            case 1:
                return 20;
            case 2:
                return 50;
            default:
                return Integer.parseInt(txtSpecify.getText());
        }
    }

    public JLabel getLblType() {
        return lblType;
    }

    public void setLblType(JLabel lblType) {
        this.lblType = lblType;
    }

    public JLabel getLblPriceField() {
        return lblPriceField;
    }

    public void setLblPriceField(JLabel lblPriceField) {
        this.lblPriceField = lblPriceField;
    }

    public JLabel getLblNoOfDays() {
        return lblNoOfDays;
    }

    public void setLblNoOfDays(JLabel lblNoOfDays) {
        this.lblNoOfDays = lblNoOfDays;
    }

    public String getselectedType() {
        return (String) cmbType.getSelectedItem();
    }

    public String getselectedPriceField() {
        return (String) cmbPriceField.getSelectedItem();
    }

    public String getselectedNoOfDays() {
        return (String) cmbNoOfDays.getSelectedItem();
    }

    public int getselectedTypeIndex() {
        return cmbType.getSelectedIndex();
    }

    public int getselectedPriceFieldIndex() {
        return cmbPriceField.getSelectedIndex();
    }

    public int getselectedNoOfDaysIndex() {
        return cmbNoOfDays.getSelectedIndex();
    }


}
