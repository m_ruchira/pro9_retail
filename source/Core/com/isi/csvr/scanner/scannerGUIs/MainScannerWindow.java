package com.isi.csvr.scanner.scannerGUIs;

import com.isi.csvr.Client;
import com.isi.csvr.ExtensionFileFilter;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.calendar.CalCombo;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Sector;
import com.isi.csvr.datastore.SectorStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.WatchlistListener;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.scanner.Meta;
import com.isi.csvr.scanner.Results.*;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.ScanTreeRenderer;
import com.isi.csvr.scanner.Scans.ScanBase;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.SimpleProgressBar;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 11:35:32 AM
 */
public class MainScannerWindow extends InternalFrame implements ActionListener, MouseListener, FocusListener, TreeSelectionListener, ExchangeListener, Themeable, PopupMenuListener, TWTabbedPaleListener, WatchlistListener {

    //title

    public static int currentScanType = Meta.DEFAULT;
    public static boolean isScanning = false;
    private static TWButton btnSave = new TWButton(Language.getString("SAVE"));
    //
    private static TWTabbedPane tabPane;
    private static SimpleProgressBar lblStatus;
    ScanTreeRenderer scanTreeRenderer;
    ScanManager scMgr;
    //btnscan panel (dockable)
    private JPanel scanPanel;
    private JPanel rightPanel;
    private JPanel helpPanel;
    private JLabel emptylable;
    private JLabel lblExchgTreeGap;
    //cardpanels
    private JPanel cardDefault;
    private JPanel cardBreakOuts;
    private JPanel cardVolume;
    private JPanel cardVolmeGainers;
    private JPanel cardVolumeLoosers;
    private JPanel cardGapUps;
    private JPanel cardGapDowns;
    private JPanel cardIslandTops;
    private JPanel cardIslankdBottoms;
    private JPanel cardGainers;
    private JPanel cardLoosers;
    private JPanel cardHigh;

//    private static  SimpleProgressBar progressBar;
    private JPanel cardLow;
    private Date date;
    //tree panel
    private JPanel scanTreePanel;
    //settings panel (initial selection)
    private JPanel scanSettingsPanel; //
    private JPanel scanSettingsCards; //
    //tempory controls
    private TWButton btnscan = new TWButton(Language.getString("SCAN"));
    private JButton btnHelp = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/charts/indicator_help.gif"));
    private JLabel lblScan = new JLabel("");
    private JPanel pnlCommonButtons = new JPanel();
    private JTree tree;
    private ViewSetting volWatchSettings;
    private JPopupMenu mnuTabPopup = new JPopupMenu();
    private TWMenuItem mnuCloseTab;
    private TWMenuItem mnuCloseAllButThis;
    //    private JLabel lblprogressMeta;
    private TWMenuItem mnuCloseAllTabs;
    //scan progression panel
    private JPanel scanProgressPanel;
    private Hashtable<String, String> scanSettingsStatus = new Hashtable<String, String>();
    //progress bar
    private JPanel exchangePanel;
    private JPanel sectorPanel;
    private JLabel lblSector;
    private JLabel lblSelectEx;
    private TWComboBox exchangeCombo;
    private TWComboBox sectorCombo;
    private boolean isAll = false;
    private Table table = null;

    public MainScannerWindow() {

        super();
        setLayer(GUISettings.TOP_LAYER);

        this.setFont(new Font("Arial", Font.BOLD, 12));
        this.setTitle(Language.getString("SCANNER")); //Language.getString("SCANNER");
        this.setSize(new Dimension(800, 510));
        this.setClosable(true);
        this.setIconifiable(true);
        this.setResizable(true);
        Client.getInstance().getDesktop().add(this);
        Theme.registerComponent(this);
        scanPanel = new JPanel();
        emptylable = new JLabel();
        //emptylable.setPreferredSize(new Dimension(100,20));
//        scanPanel.setLayout(new GridLayout(3,1));
//         scanPanel.setLayout(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
//        scanPanel.setLayout(new GridLayout(3,1));
        scanPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"0", "0", "5", "100%", "140", "5", "30"}, 0, 0));
//        scanPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "5", "190", "100%", "25"}, 0, 0));
        scanTreePanel = new JPanel();
        //create btnscan settings GUI
//        scanSettingsPanel = new JPanel(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        scanSettingsPanel = new JPanel();
        //scanSettingsPanel.setPreferredSize(new Dimension(200, 200));

        scanSettingsCards = createScanSettingsGUI();

//        scanSettingsPanel.setLayout(new BorderLayout());
        String[] widths = {"100%", "70", "10", "70"};
        String[] height = {"25"};
        pnlCommonButtons.setLayout(new FlexGridLayout(widths, height));
        btnscan.addActionListener(this);
        btnSave.addActionListener(this);
        pnlCommonButtons.add(new JLabel());
        pnlCommonButtons.add(btnscan);
        pnlCommonButtons.add(new JLabel());
        pnlCommonButtons.add(btnSave);
        //pnlCommonButtons.add(new JLabel());

        tabPane = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.WrapTabLayout);
//        tabPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top,
//                    TWTabbedPane.CONTENT_PLACEMENT.Absolute,TWTabbedPane.COMPONENT_PLACEMENT.Right,TWTabbedPane.LAYOUT_POLICY.WrapTabLayout, "dt",true);
        tabPane.addMouseListener(this);
        tabPane.addTabPanelListener(this);
        tabPane.addTabMouseListenerToTab(this);
        createTabPopup();


        scanSettingsPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "18"}, 0, 0));
        scanSettingsPanel.add(scanSettingsCards);
        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("BREAKOUTS") + " " + Language.getString("SCANNER_SETTINGS")));
//        scanSettingsPanel.add( pnlCommonButtons);

        createScanTree();
        tree.addTreeSelectionListener(this);
        tree.setSelectionRow(1);
        CardLayout cl = (CardLayout) (scanSettingsCards.getLayout());
        cl.show(scanSettingsCards, Meta.BREAKOUTS_CARD);
        btnscan.setEnabled(true);
        btnSave.setEnabled(false);


        scanTreeRenderer = new ScanTreeRenderer();
        tree.setCellRenderer(scanTreeRenderer);

//        tree.setForeground(Theme.getColor("COMBO_LIS/**/T_FGCOLOR"));


        JScrollPane scrollPane = new JScrollPane(tree);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        exchangePanel = new JPanel();
        lblSelectEx = new JLabel(Language.getString("EXCHANGE_SHORT"));
        exchangePanel.setLayout(new FlexGridLayout(new String[]{"80", "100%"}, new String[]{"25"}, 0, 0));
        exchangePanel.add(lblSelectEx);
        exchangeCombo = new TWComboBox();

        exchangePanel.add(exchangeCombo);
        populateExchangesCombo();
        exchangeCombo.addActionListener(this);

        sectorPanel = new JPanel();
        sectorPanel.setLayout(new FlexGridLayout(new String[]{"80", "100%"}, new String[]{"22"}, 0, 5));
        lblSector = new JLabel(Language.getString("SCANNER_SECTOR"));
        lblSector.setEnabled(false);
        sectorPanel.add(lblSector);
        sectorCombo = new TWComboBox();
        sectorCombo.setEnabled(false);
        sectorPanel.add(sectorCombo);

        scanPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CREATE_SCAN")));
        lblExchgTreeGap = new JLabel();

        scanPanel.add(exchangePanel);
        scanPanel.add(sectorPanel);
        scanPanel.add(lblExchgTreeGap);
        scanPanel.add(scrollPane);
        scanPanel.add(scanSettingsPanel);
        scanPanel.add(new JLabel());
        scanPanel.add(pnlCommonButtons);
//        scanPanel.add(new JButton());
//        scanPanel.add(new JButton());
//        scanPanel.add(new JButton());
//        scanPanel.add(emptylable);


        rightPanel = new JPanel();
        rightPanel.setLayout(new FlexGridLayout(new String[]{"100%", "20"}, new String[]{"100%"}, 0, 0));
        this.getContentPane().setLayout(new FlexGridLayout(new String[]{"250", "100%"}, new String[]{"100%"}, 0, 0));
        createScanProgressPanel();
        scanSettingsPanel.add(lblStatus);


        helpPanel = new JPanel();
        rightPanel.setLayout(new FlexGridLayout(new String[]{"100%", "20"}, new String[]{"100%"}, 0, 0));
        helpPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "100%"}, 0, 0));
//        this.getContentPane().setLayout(new FlexGridLayout(new String[]{"250", "100%"}, new String[]{"100%"}, 0, 0));

        btnHelp.setContentAreaFilled(false);
        btnHelp.setBorder(BorderFactory.createEmptyBorder());
        btnHelp.addActionListener(this);

        JPanel panel = new JPanel();
        helpPanel.add(btnHelp);
        helpPanel.add(panel);

//        this.getContentPane().add(rightPanel, BorderLayout.CENTER);
//        this.getContentPane().add(scanPanel, BorderLayout.WEST);

        this.getContentPane().add(scanPanel);
        this.getContentPane().add(rightPanel);
        //rightPanel.add(scanProgressPanel);
//        rightPanel.add(scanSettingsPanel, BorderLayout.CENTER);
        rightPanel.add(tabPane);
        rightPanel.add(helpPanel);
        GUISettings.applyOrientation(this);
        setVisible(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        scMgr = new ScanManager();
        LoadTheme();
        WatchListManager.getInstance().addWatchlistListener(this);
    }

    public static void closeSelectedTab() {
        if (isScanning) {
            JOptionPane.showMessageDialog(null, Language.getString("SCANNER_CANNOT_CLOSE"));
            return;
        } else {
            tabPane.removeTab(tabPane.getSelectedComponent());
        }
        if (tabPane.getTabCount() == 0) {
            tabPane.setEnabled(false);
            btnSave.setEnabled(false);

        } else {
            tabPane.setSelectedIndex(0);
        }

    }

    public static void closeAllButThisTab() {

        int selTab = tabPane.getSelectedIndex();

        int tabcount = tabPane.getTabCount();
        for (int i = 0; i < tabcount; i++) {
            if (selTab != 0) {
                tabPane.removeTab(0);
                selTab = selTab - 1;
            } else if (selTab == 0 && (tabPane.getTabCount() > 1)) {
                tabPane.removeTab(1);
            }
        }
    }

    public static void closeAllTabs() {
//        tabPane.removeAll();
        int tabCount = tabPane.getTabCount();

        for (int i = 0; i < tabCount; i++) {
            tabPane.removeTab(0);
        }

        if (tabPane.getTabCount() == 0) {
            //remove tabIndex panel
            tabPane.setEnabled(false);
            btnSave.setEnabled(false);
        }
    }

    public static void setStatusMaxProgress(int value) {
        lblStatus.setMaximum(value);
    }

    public static void setStatus(String sStatus) {
        lblStatus.setText(sStatus);
    }

    public static void setStatusProgress(int value) {
        lblStatus.setValue(value);
        lblStatus.repaint();
    }

    public static void setMax(int max) {
        //    progressBar.setMaximum(max);
    }

    private void createTabPopup() {
        mnuCloseTab = new TWMenuItem(Language.getString("CLOSE_TAB"), "graph_Detach.gif");
        mnuCloseTab.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeSelectedTab();
            }
        });
        mnuCloseTab.setVisible(true);


        mnuCloseAllButThis = new TWMenuItem(Language.getString("CLOSE_ALL_BUT_THIS"), "graph_Detach.gif");
        mnuCloseAllButThis.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeAllButThisTab();
            }
        });
        mnuCloseAllButThis.setVisible(true);

        mnuCloseAllTabs = new TWMenuItem(Language.getString("CLOSE_ALL_TABS"), "graph_Detach.gif");
        mnuCloseAllTabs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeAllTabs();
            }
        });
        mnuCloseAllTabs.setVisible(true);

//        mnuTreePopDelete1.setEnabled(false);
        mnuTabPopup.add(mnuCloseTab);
        mnuTabPopup.add(mnuCloseAllButThis);
        mnuTabPopup.add(mnuCloseAllTabs);


        mnuTabPopup.setVisible(false);
        mnuTabPopup.addPopupMenuListener(this);
        GUISettings.applyOrientation(mnuTabPopup);
    }

    private void createScanTree() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(new TreeInfo(Language.getString("PRICE_SCANS"), -1));
        DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("BREAKOUTS"), 1));
        root.add(child1);
        DefaultMutableTreeNode child2 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("VOLUME"), 2));
        root.add(child2);
        DefaultMutableTreeNode child3 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("VOLUME_GAINERS"), 3));
        root.add(child3);
        DefaultMutableTreeNode child4 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("VOLUME_LOOSERS"), 4));
        root.add(child4);
        DefaultMutableTreeNode child5 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("GAP_UPS"), 5));
        root.add(child5);
        DefaultMutableTreeNode child6 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("GAP_DOWNS"), 6));
        root.add(child6);
        DefaultMutableTreeNode child7 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("ISLAND_TOPS"), 7));
        root.add(child7);
        DefaultMutableTreeNode child8 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("ISLAND_BOTTOMS"), 8));
        root.add(child8);
        DefaultMutableTreeNode child9 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("GAINERS"), 9));
        root.add(child9);
        DefaultMutableTreeNode child10 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("LOOSERS"), 10));
        root.add(child10);
        DefaultMutableTreeNode child11 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("HIGH_52_WEEK"), 11));
        root.add(child11);
        DefaultMutableTreeNode child12 = new DefaultMutableTreeNode(new TreeInfo(Language.getString("LOW_52_WEEK"), 12));
        root.add(child12);
        tree = new JTree(root);
        tree.getSelectionModel().setSelectionMode
                (TreeSelectionModel.SINGLE_TREE_SELECTION);
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(mnuTabPopup);
        LoadTheme();
    }

    public void LoadTheme() {
        lblStatus.setProgressColor(Theme.getColor("SCANNER�_PROGRESSBAR_PROGRESS_COLOR"));
        lblStatus.setBackgroundColor(Theme.getColor("STATUS_BAR_COLOR"));
        lblStatus.setForeground(Theme.getColor("SCANNER�_PROGRESSBAR_FOREGROUND_COLOR"));
        tree.setBackground(Theme.getColor("BACKGROUND_COLOR"));
    }

    public void createScanProgressPanel() {
        //  scanProgressPanel = new JPanel();
//        lblprogressMeta = new JLabel()
        lblStatus = new SimpleProgressBar();

        //lblStatus.setPreferredSize(new Dimension(400, 20));


        String[] widths = {"100%"};//, "100", "40"};
        String[] heights = {"20"};
        //scanProgressPanel.setLayout(new FlexGridLayout(widths, heights, 3, 3));
        //scanProgressPanel.add(lblprogresInfo);
        // progressBar = new SimpleProgressBar();
//        progressBar.setHorizontalAlignment(SwingConstants.CENTER);
//        scanProgressPanel.add(progressBar);
//        progressBar.setBorder(UIManager.getDefaults().getBorder("TextField.border"));

//        scanProgressPanel.add(lblprogressMeta);
        //scanProgressPanel.setLayout();
        //progress bar
    }
    //btnscan settings GUI

    public JPanel createScanSettingsGUI() {

        scanSettingsCards = new JPanel(new CardLayout());

        cardDefault = new JPanel();
        scanSettingsCards.add(cardDefault, Meta.DEAFULT_CARD);
        cardBreakOuts = new BreakOutsUI();
        scanSettingsCards.add(cardBreakOuts, Meta.BREAKOUTS_CARD);
        cardVolume = new VolumeUI();
        scanSettingsCards.add(cardVolume, Meta.VOLUME_CARD);
        cardVolmeGainers = new VolumeGainersUI();
        scanSettingsCards.add(cardVolmeGainers, Meta.VOLUME_GAINERS_CARD);
        cardVolumeLoosers = new VolumeLoosersUI();
        scanSettingsCards.add(cardVolumeLoosers, Meta.VOLUME_LOOSERS_CARD);
        cardGapUps = new GapUpsUI();
        scanSettingsCards.add(cardGapUps, Meta.GAP_UPS_CARD);
        cardGapDowns = new GapDownsUI();
        scanSettingsCards.add(cardGapDowns, Meta.GAP_DOWNS_CARD);
        cardIslandTops = new IsalandTopsUI();
        scanSettingsCards.add(cardIslandTops, Meta.ISLAND_TOPS_CARD);
        cardIslankdBottoms = new IslandBottomsUI();
        scanSettingsCards.add(cardIslankdBottoms, Meta.ISLAND_BOTTOMS_CARD);
        cardGainers = new GainersUI();
        scanSettingsCards.add(cardGainers, Meta.GAINERS_CARD);
        cardLoosers = new LoosersUI();
        scanSettingsCards.add(cardLoosers, Meta.LOOSERS_CARD);
        cardHigh = new HighUI();
        scanSettingsCards.add(cardHigh, Meta.HIGH_CARD);
        cardLow = new LowUI();
        scanSettingsCards.add(cardLow, Meta.LOW_CARD);
        return scanSettingsCards;
    }

    //treeee
    public void valueChanged(TreeSelectionEvent e) {
        if (!isScanning) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            CardLayout cl = (CardLayout) (scanSettingsCards.getLayout());

            if (node == null) return;
            try {
                Object nodeInfo = node.getUserObject();

                if (node.isLeaf()) {

                    TreeInfo treeinfo = (TreeInfo) nodeInfo;
                    currentScanType = treeinfo.getType();
                    setStatus("");
                    if (treeinfo.getType() == Meta.DEFAULT) {
                        cl.show(scanSettingsCards, Meta.DEAFULT_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("SCANNER") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(false);
                    } else if (treeinfo.getType() == Meta.BREAKOUTS) {
                        cl.show(scanSettingsCards, Meta.BREAKOUTS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("BREAKOUTS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.VOLUME) {
                        cl.show(scanSettingsCards, Meta.VOLUME_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("VOLUME") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.VOLUME_GAINERS) {
                        cl.show(scanSettingsCards, Meta.VOLUME_GAINERS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("VOLUME_GAINERS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.VOLUME_LOOSERS) {
                        cl.show(scanSettingsCards, Meta.VOLUME_LOOSERS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("VOLUME_LOOSERS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.GAP_UPS) {
                        cl.show(scanSettingsCards, Meta.GAP_UPS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("GAP_UPS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.GAP_DOWNS) {
                        cl.show(scanSettingsCards, Meta.GAP_DOWNS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("GAP_DOWNS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.ISLAND_TOPS) {
                        cl.show(scanSettingsCards, Meta.ISLAND_TOPS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("ISLAND_TOPS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.ISLAND_BOTTOMS) {
                        cl.show(scanSettingsCards, Meta.ISLAND_BOTTOMS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("ISLAND_BOTTOMS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.GAINERS) {
                        cl.show(scanSettingsCards, Meta.GAINERS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("GAINERS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.LOOSERS) {
                        cl.show(scanSettingsCards, Meta.LOOSERS_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("LOOSERS") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.HIGH) {
                        cl.show(scanSettingsCards, Meta.HIGH_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("HIGH_52_WEEK") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    } else if (treeinfo.getType() == Meta.LOW) {
                        cl.show(scanSettingsCards, Meta.LOW_CARD);
                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("LOW_52_WEEK") + " " + Language.getString("SCANNER_SETTINGS")));
                        btnscan.setEnabled(true);
                    }

                } else {
                    btnscan.setEnabled(false);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    //****************************** region - interface implement methods*********************************/
    public void actionPerformed(ActionEvent e) {

        boolean result = false;
        String name = "";
        if (e.getSource() == btnscan && (!isScanning)) {
            try {
                ScanBase scanObj = null;
                switch (currentScanType) {
                    case Meta.BREAKOUTS:
                        BreakOutsUI breakoutUI = (BreakOutsUI) scanSettingsCards.getComponent(Meta.BREAKOUTS);
                        try {
                            scanObj = breakoutUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Breakouts";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            breakoutUI.setTxtSpecify("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.VOLUME:
                        VolumeUI volumeUI = (VolumeUI) scanSettingsCards.getComponent(Meta.VOLUME);
                        try {
                            scanObj = volumeUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Volume";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            volumeUI.setTxtSpecify("");
                            volumeUI.setNoOfTimes("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.VOLUME_GAINERS:
                        VolumeGainersUI vGainers = (VolumeGainersUI) scanSettingsCards.getComponent(Meta.VOLUME_GAINERS);
                        try {
                            scanObj = vGainers.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Volume Gainers";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            vGainers.setTxtSpecify("");
                            vGainers.setNoOfTimes("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.VOLUME_LOOSERS:
                        VolumeLoosersUI vLoosersUI = (VolumeLoosersUI) scanSettingsCards.getComponent(Meta.VOLUME_LOOSERS);
                        try {
                            scanObj = vLoosersUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Volume Losers";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            vLoosersUI.setTxtSpecify("");
                            vLoosersUI.setNoOfTimes("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.GAP_UPS:
                        GapUpsUI gUpUI = (GapUpsUI) scanSettingsCards.getComponent(Meta.GAP_UPS);
                        try {
                            scanObj = gUpUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Gap Ups";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            gUpUI.setGapUp("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.GAP_DOWNS:
                        GapDownsUI gDUI = (GapDownsUI) scanSettingsCards.getComponent(Meta.GAP_DOWNS);
                        try {
                            scanObj = gDUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Gap Downs";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            gDUI.setGapDown("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.ISLAND_TOPS:
                        IsalandTopsUI iTUI = (IsalandTopsUI) scanSettingsCards.getComponent(Meta.ISLAND_TOPS);
                        try {
                            scanObj = iTUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Island Tops";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            iTUI.setGapDown("");
                            iTUI.setGapup("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.ISLAND_BOTTOMS:
                        IslandBottomsUI iBUI = (IslandBottomsUI) scanSettingsCards.getComponent(Meta.ISLAND_BOTTOMS);
                        try {
                            scanObj = iBUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Island Bottoms";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            iBUI.setGapDown("");
                            iBUI.setGapup("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.GAINERS:
                        GainersUI gainersUI = (GainersUI) scanSettingsCards.getComponent(Meta.GAINERS);
                        try {
                            scanObj = gainersUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Gainers";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            gainersUI.setTxtSpecify("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.LOOSERS:
                        LoosersUI loosersUI = (LoosersUI) scanSettingsCards.getComponent(Meta.LOOSERS);
                        try {
                            scanObj = loosersUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "Losers";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            loosersUI.setTxtSpecify("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;

//                    case Meta.HIGH:
//                        System.out.println("*************************************************************88");
//                        break;
                    case Meta.HIGH:
                        HighUI highUI = (HighUI) scanSettingsCards.getComponent(Meta.HIGH);
                        try {
                            scanObj = highUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "52 Week High";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            highUI.setTxtSpecify("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case Meta.LOW:
                        LowUI lowUI = (LowUI) scanSettingsCards.getComponent(Meta.LOW);
                        try {
                            scanObj = lowUI.getScanObject();
                            scanObject_OnScan(scanObj);
                            result = true;
                            name = "52 Week Low";
                        } catch (NumberFormatException nex) {
                            nex.printStackTrace();
                            lowUI.setTxtSpecify("");
                            JOptionPane.showMessageDialog(this, Language.getString("SCAN_ERROR_MESSAGE"), "Scanner", JOptionPane.ERROR_MESSAGE);
                        }
                        break;

                }

                if (result && name.length() > 0) {
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Scanner, name);
                }

            } catch (NumberFormatException nEx) {
                nEx.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("excepton in getting relevent card ");
            }
        } else if (e.getSource() == btnscan && (isScanning)) {
            btnscan.setText(Language.getString("SCAN"));
            isScanning = false;
        } else if (e.getSource() == btnSave) {
            if (table == null || (tabPane.getTabCount() == 0)) {
                JOptionPane.showMessageDialog(this, Language.getString("SCANNER_SAVE_ERROR"));
            } else {
                if (tabPane.getSelectedComponent() instanceof ResultHighUI) {
                    table = ((ResultHighUI) (tabPane.getSelectedComponent())).getResultTable();
                } else if (tabPane.getSelectedComponent() instanceof ResultLowUI) {
                    table = ((ResultLowUI) (tabPane.getSelectedComponent())).getResultTable();
                } else {
                    table = ((ResultBaseScanUI) (tabPane.getSelectedComponent())).getResultTable();
                }
                exportDataToExcelSheet(table);
            }
        } else if (e.getSource().equals(exchangeCombo)) {
            TWComboItem item = (TWComboItem) exchangeCombo.getSelectedItem();
            if (item != null)
                populateSecotrCombo(item.getId());
        } else if (e.getSource() == btnHelp) {
            NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_SCANNER"));
        }
    }

    public void populateSecotrCombo(String exchange) {

        Enumeration<Sector> sectors = SectorStore.getSharedInstance().getSectors(exchange);
        sectorCombo.removeAllItems();
        int count = 0;
        sectorCombo.addItem(new TWComboItem("All", Language.getString("ALL"), "ALL"));
        while (sectors.hasMoreElements()) {
            Sector sector = sectors.nextElement();
            sectorCombo.addItem(new TWComboItem(sector.getId(), sector.getDescription(), sector.getId()));
            count++;
        }
        if (count == 0) {
            sectorCombo.setEnabled(false);
            lblSector.setEnabled(false);
        } else {
            sectorCombo.setSelectedIndex(0);
            sectorCombo.setEnabled(true);
            lblSector.setEnabled(true);
        }

    }

    private String copyHeaders(Table table) {
        int cols = 0;
        StringBuffer buffer = new StringBuffer("");

        cols = table.getTable().getColumnCount();

        //here i begins with 1 since the first column contains an image
        for (int i = 1; i < cols; i++) {
            int modalIndex = table.getTable().convertColumnIndexToModel(i);
            if (table.getTable().getColumn("" + modalIndex).getWidth() != 0) {
                String header = table.getTable().getColumn("" + modalIndex).getHeaderValue().toString();

                //buffer.append("<b>");
                buffer.append(header);
                //buffer.append("</b>");
                buffer.append(",");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public String saveExcelFileAs() {
        int status = 0;
        String sFile;
        String filePathCharts = "./";
        JFileChooser oFC = new JFileChooser(filePathCharts);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        oFC.setDialogTitle(Language.getString("SCANNER_SAVE_RESULTS"));
        ExtensionFileFilter oFilter1 = new ExtensionFileFilter("CSV format", "csv");
        ExtensionFileFilter oFilter2 = new ExtensionFileFilter("Microsoft excel 2007 format", "xlsx");

        oFC.setFileFilter(oFilter1);
//        oFC.setFileFilter(oFilter2); //2007 excel (xlss) format

        oFC.setAcceptAllFileFilterUsed(false);
        status = oFC.showSaveDialog(this);
        if (status == JFileChooser.APPROVE_OPTION) {
            sFile = oFC.getSelectedFile().getAbsolutePath();
            filePathCharts = oFC.getCurrentDirectory().getAbsolutePath();
            if (oFC.getFileFilter() == oFilter1) {
                if (!sFile.toLowerCase().endsWith(".csv"))
                    sFile += ".csv";
            } else if (oFC.getFileFilter() == oFilter2) {
                if (!sFile.toLowerCase().endsWith(".xlsx"))
                    sFile += ".xlsx";
            }
            return sFile;
        } else {
            return null;
        }
    }

    private void exportDataToExcelSheet(Table table) {

        StringBuffer buffer = new StringBuffer("");
        buffer.append(copyHeaders(table));

        int rows = table.getTable().getRowCount();
        int cols = table.getTable().getColumnCount();
        TWDecimalFormat f;
        TWDateFormat g_oDateTimeFormatHM;
        for (int r = 0; r < rows; r++) {
            for (int c = 1; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    Class cls = table.getTable().getColumnClass(c);
                    String temp = table.getTable().getValueAt(r, c).toString();
                    if (c == 1) {
                        temp = temp;
                    } else if (cls == Double.class || cls == Float.class) {
                        f = new TWDecimalFormat("#####0.00");
                        temp = f.format(Double.parseDouble(temp));
                    } else if (cls == String.class) {  //--Added By Shanika-- To save date
                        long longValue;
                        date = new Date();
                        Object value = table.getTable().getValueAt(r, c);
                        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));
                        try {
                            longValue = toLongValue(value);
                            date.setTime(longValue);
                            temp = g_oDateTimeFormatHM.format(date);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                    temp = temp + "  ";
                    buffer.append(temp);
                    buffer.append(",");
                    if (c == cols - 1) {
                        buffer.append("\n");
                    }
                }
            }
        }

        //System.out.println(" =============== BUFFER ============");
        //System.out.println(buffer.toString());

        try {
            String path = saveExcelFileAs();
            FileWriter writer = new FileWriter(path);
            writer.write(buffer.toString());
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private void populateExchangesCombo() {
        exchangeCombo.removeAllItems();
        TWComboItem itemAll = new TWComboItem("All", Language.getString("ALL"), "ALL");
        //exchanges
        exchangeCombo.addItem(itemAll);
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exg = (Exchange) exchanges.nextElement();
            if (exg.isDefault()) {
                TWComboItem item = new TWComboItem(exg.getSymbol(), exg.getDescription(), "EXG");
                exchangeCombo.addItem(item);
            }
        }

        TWComboItem itemSeparate = new TWComboItem("All", Language.getString("SCANNER_WATCH_LIST_SEP"), "ALL");
        exchangeCombo.addItem(itemSeparate);

        //watch lists
        Vector mainboard_view = ViewSettingsManager.getMainBoardViews();
        Enumeration en = mainboard_view.elements();
        while (en.hasMoreElements()) {
            final ViewSetting vs = (ViewSetting) en.nextElement();
//                     System.out.println("table id = "+vs.getID());
            if (vs.getMainType() == ViewSettingsManager.CUSTOM_VIEW) {
//                        System.out.println("table id = "+vs.getID());
                //System.out.println("watchlist names:  " + vs.getCaption());
                TWComboItem item = new TWComboItem(vs.getCaption(), vs.getCaption(), "WL");
                exchangeCombo.addItem(item);
            }
        }

        /* Enumeration<Sector> sectors = SectorStore.getSharedInstance().getSectors(exchange.getSymbol());
                        while (sectors.hasMoreElements()) {
                            Sector sector = sectors.nextElement();
//                            if(!sector.getDescription().equals("unAssign Sectors")){
//                            if (!sector.getId().equals("UNCLASSIFIED")) {
                            addSectorTreeNode(exchange, subMarket, sector, subMarketNode, oTreeModel, true, isCreateForNewExchange);
//                            }
                            sector = null;
                        }*/

    }


    public String getSelectedExchange() {
        String temp = ((TWComboItem) exchangeCombo.getSelectedItem()).getId();


        return temp;
    }

    public String getSelectedListType() {
        String temp = "";
        temp = ((TWComboItem) exchangeCombo.getSelectedItem()).getType();

        return temp;
    }


    public void hideCards(Component mainComp) {

        if (mainComp instanceof JPanel) { //check whether it's a container

            if ((((JPanel) mainComp).getName()) == null) {

                Component[] compArr = ((JPanel) mainComp).getComponents();
                for (int i = 0; i < compArr.length; i++) {
                    hideCards(compArr[i]);
                }


            } else {
                Component[] specifyComps = ((JPanel) mainComp).getComponents();
                for (int i = 0; i < specifyComps.length; i++) {
                    if (specifyComps[i] instanceof TWTextField || specifyComps[i] instanceof CalCombo) {
                        scanSettingsStatus.put(mainComp.getName(), Boolean.toString(specifyComps[i].isEnabled()));

                    }
                    hideCards(specifyComps[i]);
                }
            }


        } else {
            mainComp.setEnabled(false);
        }

//        if (comp2[j] instanceof TWComboBox || comp2[j] instanceof TWComboBox || comp2[j] instanceof TWTextField
//                                    || comp2[j] instanceof JLabel  ) {
    }

    public void showCards(Component mainComp) {

        if (mainComp instanceof JPanel) { //check whether it's a container

            if ((((JPanel) mainComp).getName()) == null) {

                Component[] compArr = ((JPanel) mainComp).getComponents();

                for (int i = 0; i < compArr.length; i++) {
                    showCards(compArr[i]);
                }
            } else {
                Component[] specifyComps = ((JPanel) mainComp).getComponents();
                for (int i = 0; i < specifyComps.length; i++) {
                    //if (specifyComps[i] instanceof TWTextField){
                    boolean isEnable = Boolean.parseBoolean(scanSettingsStatus.get(mainComp.getName()));
                    specifyComps[i].setEnabled(isEnable);
//                         }
                }
            }
        } else {
            mainComp.setEnabled(true);

        }
    }

    private void scanObject_OnScan(final ScanBase scanObj) {
        isScanning = true;
        try {
            new Thread() {
                public void run() {
                    String selExchange = getSelectedExchange();
                    String selSector = "";
                    try {
                        selSector = getSelectedSector();
                        if (selSector == null)
                            selSector = "All";
                    } catch (Exception e) {
                        selSector = "";
                    }
                    String selType = getSelectedListType();
                    scMgr.setScanCoympanyList(selExchange, selSector, selType);

                    System.out.println("@@@@@@@@ Begin Scanning...");
                    RemovableArray alResult = new RemovableArray();
//                        ArrayList alResult = new ArrayList();
                    constructResultsTab(scanObj, alResult, selType);
                    while (isScanning) {

                        hideCards(scanSettingsCards);
                        btnSave.setEnabled(false);
//                        scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(""));
                        tree.setEnabled(false);
                        btnscan.setText(Language.getString("ORDER_TYPE_STOP"));
                        scanObj.scan(scMgr, alResult);

                        setStatus("");
                        setStatusProgress(0);

                        btnscan.setText(Language.getString("SCAN"));
                        isScanning = false;

                    }
                    if (alResult.size() == 0) {
                        setStatus(Language.getString("NO_SCAN_RESULTS_FOUND"));
                    }
                    btnSave.setEnabled(true);
                    showCards(scanSettingsCards);
                    //scanSettingsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("SCANNER_SETTINGS")));
                    tree.setEnabled(true);
                    //   System.out.println("alResult.size(): " + alResult.size());

                    // testResultArray(alResult);
                    // todo sort // alResult.Sort();
                    //   constructResultsTab(scanObj, alResult);
                }
            }.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void testResultArray(ArrayList alResult) {
        for (int i = 0; i < alResult.size(); i++) {
            ResultBase temp = (ResultBase) alResult.get(i);
            System.out.println("compnay: " + temp.getCompanyName() + " high: " + temp.getHigh() + " last: "
                    + temp.getLast() + " change: " + temp.getChange() + " pct change" + temp.getPercentChange());


        }


    }


    public void constructResultsTab(ScanBase scanObj, RemovableArray alResult, String sResultType) {

        //tabbed panel name
        //todo tab title etc
        String tabTitle = scanObj.getShortName();
        //frm.Icon = childIcon;
        //frm.Dock = DockStyle.Fill;

        if (tabPane.getTabCount() == 0) {
            //show tabIndex panel
            tabPane.setEnabled(true);
        }
        switch (scanObj.getScanId()) {
            case Meta.BREAKOUTS:
                ResultBreakOutUI rsBrkOutUI = new ResultBreakOutUI(alResult, scMgr, sResultType);
                rsBrkOutUI.setName(tabTitle);
                tabPane.addTab(tabTitle, rsBrkOutUI);

                //changed by charithn
                table = rsBrkOutUI.getResultTable();
                break;
            case Meta.VOLUME:
            case Meta.VOLUME_GAINERS:
            case Meta.VOLUME_LOOSERS:
                ResultVolumeUI rsVolumeUI = new ResultVolumeUI(alResult, scMgr, sResultType);
                rsVolumeUI.setName(tabTitle);
                tabPane.addTab(tabTitle, rsVolumeUI);
                table = rsVolumeUI.getResultTable();
                break;
            case Meta.GAP_DOWNS:
            case Meta.GAP_UPS:
                ResultGapUpUI rsGapUpUI = new ResultGapUpUI(alResult, scMgr, sResultType);
                rsGapUpUI.setName(tabTitle);
                tabPane.addTab(tabTitle, rsGapUpUI);
                table = rsGapUpUI.getResultTable();
                break;
            case Meta.ISLAND_BOTTOMS:
            case Meta.ISLAND_TOPS:
                ResultIslandTBUI islandTBUI = new ResultIslandTBUI(alResult, scMgr, sResultType);
                islandTBUI.setName(tabTitle);
                tabPane.addTab(tabTitle, islandTBUI);
                table = islandTBUI.getResultTable();
                break;
            case Meta.HIGH:
                ResultHighUI rsHighUI = new ResultHighUI(alResult, scMgr, sResultType);
                rsHighUI.setName(tabTitle);
                tabPane.addTab(tabTitle, rsHighUI);
                table = rsHighUI.getResultTable();
                break;
            case Meta.LOW:
                ResultLowUI rsLowUI = new ResultLowUI(alResult, scMgr, sResultType);
                rsLowUI.setName(tabTitle);
                tabPane.addTab(tabTitle, rsLowUI);
                table = rsLowUI.getResultTable();
                break;
            case Meta.LOOSERS:
            case Meta.GAINERS:

            default:
                ResultGainerUI rsGainersUI = new ResultGainerUI(alResult, scMgr, sResultType);
                rsGainersUI.setName(tabTitle);
                tabPane.addTab(tabTitle, rsGainersUI);
                table = rsGainersUI.getResultTable();
                break;
        }

        tabPane.setSelectedIndex(tabPane.getTabCount() - 1);

//            resTab.Dock = DockStyle.Fill;
//            resTab.OnRequestChart += this.OnRequestChart;
//            frm.Controls.Add(resTab);
//            frm.Show();


    }

    public void focusGained(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusLost(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseClicked(MouseEvent e) {
        //System.out.println("right cliecked");
        if ((e.getModifiers() & e.BUTTON3_MASK) == e.BUTTON3_MASK) {
            if (e.getSource() == tabPane) {
                GUISettings.showPopup(mnuTabPopup, e.getComponent(), e.getX(), e.getY());


            }

        }

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesAdded(boolean offlineMode) {
        //To change body of implemented methods use File | Settings | File Templates.
        populateExchangesCombo();
    }

    public void exchangeAdded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
        populateExchangesCombo();
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }//content pane

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

        if (tabPane.getTabCount() == 0 || isScanning) {
            for (int i = 0; i < mnuTabPopup.getComponentCount(); i++) {
                mnuTabPopup.getComponent(i).setVisible(false);
            }
        } else {
            for (int i = 0; i < mnuTabPopup.getComponentCount(); i++) {
                mnuTabPopup.getComponent(i).setVisible(true);
            }
        }

    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

    }

    public void popupMenuCanceled(PopupMenuEvent e) {

    }

    public void tabStareChanged(TWTabEvent event) {

        if (event.getState() == TWTabEvent.STATE_SELECTED) {
            int i = tabPane.getSelectedIndex();
            Component cmp = tabPane.getSelectedComponent();
            //iframe.updateUI();
        }
    }

    public String getSelectedSector() {
        if (sectorCombo.isEnabled()) {
            return ((TWComboItem) sectorCombo.getSelectedItem()).getId();
        }

        return null;
    }

    public void symbolAdded(String key, String listID) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void symbolRemoved(String key, String listID) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void watchlistAdded(String listID) {
        populateExchangesCombo();
    }

    public void watchlistRenamed(String listID) {
        populateExchangesCombo();

    }

    public void watchlistRemoved(String listID) {
        populateExchangesCombo();

    }
}
