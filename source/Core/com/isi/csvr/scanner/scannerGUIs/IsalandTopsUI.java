package com.isi.csvr.scanner.scannerGUIs;

import com.isi.csvr.scanner.Scans.IslandTops;
import com.isi.csvr.scanner.Scans.ScanBase;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 11:51:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class IsalandTopsUI extends JPanel {
    JLabel lblGapup;
    JLabel lblGapDown;

    TWTextField txtGapup;
    TWTextField txtGapDown;


    public IsalandTopsUI() {
        lblGapup = new JLabel(Language.getString("GAP_UP_PERC"));
        lblGapDown = new JLabel(Language.getString("GAP_DOWN_PERC"));

        txtGapup = new TWTextField();
        txtGapDown = new TWTextField();
        txtGapup.setText("1.00");
        txtGapDown.setText("1.00");


        setLayout(new FlexGridLayout(new String[]{"10", "110", "10", "100%", "10"}, new String[]{"20", "4", "20", "100%"}, 0, 0));
        add(new JLabel());
        add(lblGapup);
        add(new JLabel());
        add(txtGapup);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(lblGapDown);
        add(new JLabel());
        add(txtGapDown);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
    }

    protected ScanBase getScanObject() {
        IslandTops scanObj = new IslandTops();

        if (Float.parseFloat(txtGapup.getText()) < 0 || Float.parseFloat(txtGapDown.getText()) < 0) {
            throw new NumberFormatException();
        }
        scanObj.setGapUpPercent(Float.parseFloat(txtGapup.getText()));
        scanObj.setGapDownPercent(Float.parseFloat(txtGapDown.getText()));
        return scanObj;
    }

    public String getGapup() {
        return txtGapup.getText();
    }

    public void setGapup(String value) {
        txtGapup.setText(value);
    }

    public String getGapDown() {
        return txtGapDown.getText();
    }

    public void setGapDown(String value) {
        txtGapDown.setText(value);
    }
}
