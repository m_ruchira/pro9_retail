package com.isi.csvr.scanner.scannerGUIs;

import com.isi.csvr.scanner.Scans.GapDowns;
import com.isi.csvr.scanner.Scans.ScanBase;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 11:51:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class GapDownsUI extends JPanel {
    JLabel lblGapDown;

    TWTextField txtGapdown;


    public GapDownsUI() {
        lblGapDown = new JLabel(Language.getString("GAP_DOWN_PERC"));

        txtGapdown = new TWTextField();
        txtGapdown.setText("1.00");


        setLayout(new FlexGridLayout(new String[]{"10", "110", "10", "100%", "10"}, new String[]{"20", "100%"}, 0, 0));
        add(new JLabel());
        add(lblGapDown);
        add(new JLabel());
        add(txtGapdown);
        add(new JLabel());


        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());


    }

    protected ScanBase getScanObject() {
        GapDowns scanObj = new GapDowns();
        if (Float.parseFloat(txtGapdown.getText()) < 0) {
            throw new NumberFormatException();
        }
        scanObj.setGapPercent(Float.parseFloat(txtGapdown.getText()));
        return scanObj;
    }

    public String getGapup() {
        return txtGapdown.getText();
    }

    public void setGapDown(String value) {
        txtGapdown.setText(value);
    }
}
