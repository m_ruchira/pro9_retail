package com.isi.csvr.scanner.Results;

import com.isi.csvr.scanner.ScanPoint;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:07:26 PM
 * To change this template use File | Settings | File Templates.
 */


public class RsGapUps extends ResultBase {
    //PercentAboveAvg
    private double pcntGap;

    //        public RsGapUps(Company company, ScanPoint sP, double pcntGap)
    public RsGapUps(String company, ScanPoint sP, double pcntGap) {
        super(company, sP);
        this.pcntGap = pcntGap;
    }

    public double getPercentGap() {
        return pcntGap;

    }

    public void setPercentGap(double value) {
        pcntGap = value;

    }

    protected int compareObj(Object obj) {
        // this is done opposite way to sort descending

        return new Double(Math.abs(((RsGapUps) obj).pcntGap)).compareTo(new Double(Math.abs(this.pcntGap)));
    }
}

