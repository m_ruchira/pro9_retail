package com.isi.csvr.scanner.Results;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 27, 2008
 * Time: 5:29:04 PM
 * To change this template use File | Settings | File Templates.
 */


public class BreakOutModel extends CommonTable
        implements TableModel, CommonTableInterface {
    private static LongTransferObject longTrasferObject = new LongTransferObject();
    //constructor
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    ArrayList<ResultBase> breakOutDataStore;


    public BreakOutModel(ArrayList<ResultBase> breakOutDataStore) {
        this.breakOutDataStore = breakOutDataStore;

    }

    //---------------------------------overide methods from common table ---------------------
    public void setSymbol(String symbol) {

    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {

        //todo for now : should use a datastore
        return breakOutDataStore.size();
    }

    public Object getValueAt(int row, int col) {

        //todo for now
        RsBreakouts vWData = (RsBreakouts) breakOutDataStore.get(row);

        switch (col) {
            case -1:
                return vWData.getCompany();
            case 0:     // SYMBOL
                return "" + 1;
            case 1:     // SYMBOL
                return SharedMethods.getSymbolFromKey(vWData.getCompany());
            case 2:
//                return SharedMethods.getExchangeFromKey(vWData.getCompany());
                return ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(vWData.getCompany()).trim()).getDisplayExchange(); //Display Exchange
            case 3:
                return vWData.getLast(); //double

            case 4:
                return vWData.getChange();//double


            case 5:
                return vWData.getPercentChange();
            case 6:
                return vWData.getPercentAboveHigh();


            default:
                return "";
        }


    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {

        return false;
    }

    //---------------------------------overide methods from common table ---------------------


}