package com.isi.csvr.scanner.Results;

import com.isi.csvr.scanner.ScanPoint;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:08:08 PM
 * To change this template use File | Settings | File Templates.
 */

public class RsIslandTops extends ResultBase {
    //PercentGapUp
    private double pcntGapUp;

    //PercentGapDown
    private double pcntGapDown;

    //public RsIslandTops(Company company, ScanPoint sP, double pcntGapUp, double pcntGapDown)
    public RsIslandTops(String company, ScanPoint sP, double pcntGapUp, double pcntGapDown) {
        super(company, sP);
        this.pcntGapUp = pcntGapUp;
        this.pcntGapDown = pcntGapDown;
    }

    public double getPcntGapUp() {
        return pcntGapUp;
    }

    public void setPcntGapUp(double pcntGapUp) {
        this.pcntGapUp = pcntGapUp;
    }

    public double getPcntGapDown() {
        return pcntGapDown;
    }

    public void setPcntGapDown(double pcntGapDown) {
        this.pcntGapDown = pcntGapDown;
    }

    protected int compareObj(Object obj) {
        // this is done opposite way to sort descending
        return Double.compare(Math.abs(((RsIslandTops) obj).pcntGapUp + ((RsIslandTops) obj).pcntGapDown), Math.abs(this.pcntGapUp + this.pcntGapDown));
    }
}
