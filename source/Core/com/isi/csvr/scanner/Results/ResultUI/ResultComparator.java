package com.isi.csvr.scanner.Results.ResultUI;

import com.isi.csvr.scanner.Results.ResultBase;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Apr 17, 2008
 * Time: 9:54:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResultComparator implements Comparator {
    public ResultComparator() {
    }


    public int compare(Object x, Object y) {

        ResultBase gs1 = (ResultBase) x;
        ResultBase gs2 = (ResultBase) y;
        return (new Double(Math.abs((gs1).getPercentChange()))).compareTo(new Double(Math.abs(gs2.getPercentChange())));

    }


}


