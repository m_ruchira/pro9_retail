package com.isi.csvr.scanner;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 26, 2008
 * Time: 2:04:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScanSlot {

    public long Time;
    public ScanPoint[] Charts;

    public int Length;

    public ScanSlot(long Time) {
        this.Time = Time;
        this.Charts = null;
    }

    public int getLength() {
        if (Charts == null) {
            return 0;
        } else {
            return Charts.length;
        }
    }

    public void setLength(int length) {
        Length = length;
    }//Constructor
    //Indexer (0 based)

    public ScanPoint get(int index) {
        if (Charts == null) {
            return null;
        } else if (index > Charts.length - 1) {
            return null;
        } else {
            return Charts[index];
        }
    }

    public void set(int index, ScanPoint value) { //scanpoint[index]
        if (Charts == null) {
            Charts = new ScanPoint[index + 1];
            Charts[index] = value;
        } else if (index > Charts.length - 1) {
            ScanPoint[] newCharts = new ScanPoint[index + 1];
            for (int i = 0; i < Charts.length; i++) {
                newCharts[i] = Charts[i];
            }
            Charts = newCharts;
            Charts[index] = value;
        } else {
            Charts[index] = value;
        }
    }

    public ScanPoint getChartPoint(int graphIndex) {
        if (Charts == null) {
            Charts = new ScanPoint[graphIndex + 1];
            Charts[graphIndex] = new ScanPoint();
        } else if (graphIndex > Charts.length - 1) {
            ScanPoint[] newCharts = new ScanPoint[graphIndex + 1];
            for (int i = 0; i < Charts.length; i++) {
                newCharts[i] = Charts[i];
            }
            Charts = newCharts;
            Charts[graphIndex] = new ScanPoint();
        } else if (Charts[graphIndex] == null) {
            Charts[graphIndex] = new ScanPoint();
        }
        return Charts[graphIndex];
    }


//        public int Compare(Object x, Object y) {
//            ScanSlot gs1 = (ScanSlot)x;
//            ScanSlot gs2 = (ScanSlot)y;
//
//            return  new Long (gs1.Time).compareTo(new Long(gs2.Time));
//
//        }


}

