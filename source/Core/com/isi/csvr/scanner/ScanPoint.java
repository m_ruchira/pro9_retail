package com.isi.csvr.scanner;

import com.isi.csvr.chart.ChartPoint;
import com.isi.csvr.chart.GraphDataManager;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 26, 2008
 * Time: 1:57:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScanPoint extends ChartPoint {
    public double Change = 0;
    public double PercentChange = 0;

    public ScanPoint() {
        super();
        this.Low = Float.MAX_VALUE;   //intially assign to a big value
    }

    public double getValue(byte OHLCVPriority) {
        switch (OHLCVPriority) {


            case GraphDataManager.INNER_CHANGE:
                return this.Change;
            case GraphDataManager.INNER_PERCENT_CHANGE:
                return this.PercentChange;
            default:
                return super.getValue(OHLCVPriority);
        }
    }

    public void setValue(byte OHLCVPriority, double value) {
        switch (OHLCVPriority) {
            case GraphDataManager.INNER_CHANGE:
                this.Change = value;
                break;
            case GraphDataManager.INNER_PERCENT_CHANGE:
                this.PercentChange = value;
                break;
            default:
                super.setValue(OHLCVPriority, value);
                break;
        }
    }
}

