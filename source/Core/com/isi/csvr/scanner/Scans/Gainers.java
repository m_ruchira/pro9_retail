package com.isi.csvr.scanner.Scans;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.scanner.Meta;
import com.isi.csvr.scanner.Results.ResultBase;
import com.isi.csvr.scanner.Results.ResultUI.ResultComparator;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.ScanPoint;
import com.isi.csvr.scanner.ScanSlot;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.shared.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:09:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class Gainers extends ScanBase {

    //Count
    private int count;
    private long time = -1;

    public Gainers() {
        this.scanID = Meta.GAINERS;
        this.count = 20;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int value) {
        count = value;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void scan(ScanManager SM, ArrayList scResult) {
        //long entryTime = System.currentTimeMillis();
        boolean isGainers = (this.scanID == Meta.GAINERS);
        ArrayList alCompanylist = SM.getCompanyList();
        MainScannerWindow.setStatusMaxProgress(alCompanylist.size());
        for (int itrCompany = 0; itrCompany < alCompanylist.size(); itrCompany++) {
            DynamicArray companyHistory = SM.addScanRequest((String) alCompanylist.get(itrCompany));
            SM.clearGraphStore();
            SM.populateGraphStore(companyHistory);
            if (itrCompany % 100 == 0) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (itrCompany % 10 == 0) {

                MainScannerWindow.setStatus(Language.getString("SCANNING") + " : " + (itrCompany + 1) + "/" + alCompanylist.size() + "  " + SharedMethods.getSymbolFromKey((String) alCompanylist.get(itrCompany)));
                MainScannerWindow.setStatusProgress(itrCompany);
            }
            if (!MainScannerWindow.isScanning) {
                break;
            }
            if (SM.getGraphStore().size() > 1) {

                //reading specified date
                Calendar cal = Calendar.getInstance();
                cal.clear();
                cal.setTimeInMillis(time);
                cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), 0, 0, 0);

                TWDateFormat f = new TWDateFormat("yyyyMMdd ");
                String dateString = f.format(cal.getTimeInMillis());
                int searchIndex = -1;
                if (true) {//TODO:remove later
                    //System.out.println("+++++++++++++++++ " + dateString);
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                    try {
                        Date date = format.parse(dateString);
                        searchIndex = SM.getGraphStore().indexOfContainingValue(new ScanSlot(date.getTime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (searchIndex < 0) {
                    continue;
                }

                //reading todays value
                //ScanPoint sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                ScanPoint sP = SM.readScanPoint(searchIndex, Meta.GRAPH_INDEX);
                if (sP == null) continue;
                if ((isGainers && (sP.PercentChange > 0)) || (!isGainers && (sP.PercentChange < 0))) {
                    String cmpny = SM.getCompanyList().get(itrCompany);
                    //sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                    sP = SM.readScanPoint(searchIndex, Meta.GRAPH_INDEX);    //todo:important
                    ResultBase result = new ResultBase(cmpny, sP);
                    Stock stock = DataStore.getSharedInstance().getStockObject(cmpny);
                    scResult.add(result);
                    Collections.sort(scResult, new ResultComparator());
                    if (scResult.size() > this.count) {
                        //remove range
                        int iCompletesize = scResult.size();
                        for (int i = this.count; i < iCompletesize; i++) {
                            scResult.remove(this.count);
                        }
                    }
                    SM.fireUpDateUI();
                }
            }
        }
    }

    public String getShortName() {
        return Language.getString("TOP") + " " + getCount() + " " + Language.getString("GAINERS");

    }

}