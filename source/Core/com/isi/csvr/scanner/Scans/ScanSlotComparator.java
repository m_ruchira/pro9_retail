package com.isi.csvr.scanner.Scans;

import com.isi.csvr.scanner.ScanSlot;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 30, 2008
 * Time: 9:06:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScanSlotComparator implements Comparator {
    public ScanSlotComparator() {
    }

    public int compare(Object x, Object y) {
        ScanSlot gs1 = (ScanSlot) x;
        ScanSlot gs2 = (ScanSlot) y;

        return new Long(gs1.Time).compareTo(new Long(gs2.Time));

    }


}
