package com.isi.csvr.scanner.Scans;

import com.isi.csvr.scanner.Meta;
import com.isi.csvr.shared.Language;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:12:58 PM
 * To change this template use File | Settings | File Templates.
 */


public class Loosers extends Gainers {

    public Loosers() {
        super();
        this.scanID = Meta.LOOSERS;
    }

    public String getShortName() {
        return Language.getString("TOP") + " " + getCount() + " " + Language.getString("LOOSERS");
    }
}