package com.isi.csvr.scanner.Scans;

import com.isi.csvr.scanner.Meta;
import com.isi.csvr.scanner.Results.RsGapUps;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.ScanPoint;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:10:57 PM
 * To change this template use File | Settings | File Templates.
 */

public class GapUps extends ScanBase {
    //GapPercent
    private float gapPercent;

    public GapUps() {
        this.scanID = Meta.GAP_UPS;
        this.gapPercent = 2.5f;
    }

    public float getGapPercent() {
        return gapPercent;

    }

    public void setGapPercent(float value) {
        gapPercent = value;

    }

    public void scan(ScanManager SM, ArrayList scResult) {
        long entryTime = System.currentTimeMillis();
//            ArrayList scResult = new ArrayList();

        ArrayList alCompanylist = SM.getCompanyList();
        MainScannerWindow.setStatusMaxProgress(alCompanylist.size());
        for (int itrCompany = 0; itrCompany < alCompanylist.size(); itrCompany++) {
            DynamicArray companyHistory = SM.addScanRequest((String) alCompanylist.get(itrCompany));
            SM.clearGraphStore();
            SM.populateGraphStore(companyHistory);
            if (itrCompany % 100 == 0) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (itrCompany % 10 == 0) {

                MainScannerWindow.setStatus(Language.getString("SCANNING") + " : " + (itrCompany + 1) + "/" + alCompanylist.size() + "  " + SharedMethods.getSymbolFromKey((String) alCompanylist.get(itrCompany)));
                MainScannerWindow.setStatusProgress(itrCompany);
            }
            if (!MainScannerWindow.isScanning) {
                break;
            }
            if (SM.getGraphStore().size() > 1) {
                float gapFactor = 1f + gapPercent / 100f;
                //reading todays value
                ScanPoint sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                if (sP == null) continue;
                double currLow = sP.Low;
                sP = SM.readScanPoint(SM.getGraphStore().size() - 2, Meta.GRAPH_INDEX);
                if (sP == null) continue;
                double prevHigh = sP.High;
                if (currLow >= gapFactor * prevHigh) {
//                        String cmpny = SM.getCompanyList().get(j);
                    String cmpny = SM.getCompanyList().get(itrCompany);
                    sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                    double pcntGap = 0.0;
                    if (prevHigh != 0) { //can genarate a devision by zero exception
                        pcntGap = 100f * (currLow - prevHigh) / prevHigh;
                    }
                    RsGapUps result = new RsGapUps(cmpny, sP, pcntGap);
                    scResult.add(result);
                    SM.fireUpDateUI();
                }
            }
        }
        System.out.println("@@@@@@@@ End Scanning GapUps after: " + (System.currentTimeMillis() - entryTime));
//            return scResult;
    }


    public String getShortName() {
//        return   "Gap Ups > " + gapPercent + "% rise";
        return Language.getString("GAP_UPS") + " > " + gapPercent + Language.getString("PERCENTAGE_RISE");
    }
}

