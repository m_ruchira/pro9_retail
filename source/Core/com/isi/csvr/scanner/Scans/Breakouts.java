package com.isi.csvr.scanner.Scans;

import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.scanner.Meta;
import com.isi.csvr.scanner.Results.RsBreakouts;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.ScanPoint;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:09:25 PM
 * To change this template use File | Settings | File Templates.
 */

public class Breakouts extends ScanBase {

    //PriceField
    private byte priceField;

    //Period
    private int period;

    //isHigh
    private boolean isHigh;


    public Breakouts() {
        this.scanID = Meta.BREAKOUTS;
        this.priceField = GraphDataManager.INNER_Close;
        this.period = 5;
    }

    public byte getPriceField() {
        return priceField;
    }

    public void setPriceField(byte priceField) {
        this.priceField = priceField;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public boolean isHigh() {
        return isHigh;
    }

    public void setHigh(boolean high) {
        isHigh = high;
    }
    //public Breakouts(bool isHigh)
    //    : this() {
    //    this.isHigh = isHigh;
    //}

    //    public ArrayList scan(ScanManager SM ,ArrayList scResult) {
    public void scan(ScanManager SM, ArrayList scResult) {

        long entryTime = System.currentTimeMillis();
//        long entryTime2;
//        ArrayList scResult = new ArrayList();

        ArrayList alCompanylist = SM.getCompanyList();
        MainScannerWindow.setStatusMaxProgress(alCompanylist.size());
        for (int itrCompany = 0; itrCompany < alCompanylist.size(); itrCompany++) {
//            entryTime2 = System.currentTimeMillis();
            String cmpKey = (String) alCompanylist.get(itrCompany);
            DynamicArray companyHistory = SM.addScanRequest(cmpKey);
            SM.clearGraphStore();
            SM.populateGraphStore(companyHistory);

//            System.out.println("Time to populate "  +  + (System.currentTimeMillis() - entryTime2));
            int bIndex = Math.max(SM.getGraphStore().size() - period - 1, 0);// thi
            /*if (itrCompany % 100 == 0) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/
            if (itrCompany % 10 == 0) {

                MainScannerWindow.setStatus(Language.getString("SCANNING") + " : " + (itrCompany + 1) + "/" + alCompanylist.size() + "  " + SharedMethods.getSymbolFromKey((String) alCompanylist.get(itrCompany)));
                MainScannerWindow.setStatusProgress(itrCompany);
            }
            //interrupt
            if (!MainScannerWindow.isScanning) {
                break;
            }
            if (SM.getGraphStore().size() > 1) {

                double currVal = 0;
                double diff = Meta.MAX_DOUBLE_VALUE;
                boolean tmpResult = false;
                //reading todays value
                ScanPoint sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                if (sP == null) continue;
                currVal = sP.getValue(priceField);
                for (int i = bIndex; i < SM.getGraphStore().size() - 1; i++) {

                    sP = SM.readScanPoint(i, Meta.GRAPH_INDEX);
                    if (sP != null) {
                        tmpResult = true;
                        double val = sP.getValue(priceField);
                        if (isHigh) {
                            diff = Math.min(diff, currVal - val);
                            if (val >= currVal) {
                                tmpResult = false;
                                break;
                            }
                        } else {
                            diff = Math.min(diff, val - currVal);
                            if (val <= currVal) {
                                tmpResult = false;
                                break;
                            }
                        }
                    }
                }
                String cmpny = SM.getCompanyList().get(itrCompany);
                if (tmpResult) {
//                        Company cmpny = (Company)SM.Companies[j];
                    sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                    diff = isHigh ? diff : -diff;
                    double pcntAbvHigh = 100d * diff / (currVal - diff);
                    RsBreakouts result = new RsBreakouts(cmpny, sP, pcntAbvHigh);
                    scResult.add(result);
                    SM.fireUpDateUI();

                }
//                System.out.println("Time "  + cmpny + " " + (System.currentTimeMillis() - entryTime2));
            }
        }
        System.out.println("@@@@@@@@ End Scanning after: " + (System.currentTimeMillis() - entryTime));
        //return scResult;
    }

    public String getShortName() {
//        return "Breakout " + period + " day " + (isHigh ? "High" : "Low");
        return Language.getString("BREAKOUTS") + " " + period + " " + Language.getString("DAY") + " " +
                (isHigh ? Language.getString("FD2_HIGH") : Language.getString("FD2_LOW"));
    }

}

    

