package com.isi.csvr.scanner.Scans;

import com.isi.csvr.scanner.Meta;
import com.isi.csvr.scanner.Results.RsIslandTops;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.ScanPoint;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:11:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class IslandBottoms extends IslandTops {

    public IslandBottoms() {
        super();
        this.scanID = Meta.ISLAND_BOTTOMS;
    }

    public void scan(ScanManager SM, ArrayList scResult) {
        long entryTime = System.currentTimeMillis();
        ArrayList alCompanylist = SM.getCompanyList();
        MainScannerWindow.setStatusMaxProgress(alCompanylist.size());
        for (int itrCompany = 0; itrCompany < alCompanylist.size(); itrCompany++) {
            DynamicArray companyHistory = SM.addScanRequest((String) alCompanylist.get(itrCompany));
            SM.clearGraphStore();
            SM.populateGraphStore(companyHistory);

            if (itrCompany % 100 == 0) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (itrCompany % 10 == 0) {

                MainScannerWindow.setStatus(Language.getString("SCANNING") + " : " + (itrCompany + 1) + "/" + alCompanylist.size() + "  " + SharedMethods.getSymbolFromKey((String) alCompanylist.get(itrCompany)));
                MainScannerWindow.setStatusProgress(itrCompany);
            }
            if (!MainScannerWindow.isScanning) {
                break;
            }

            if (SM.getGraphStore().size() > 2) {
                float gapUpFactor = 1f + getGapUpPercent() / 100f;
                float gapDownFactor = 1f - getGapDownPercent() / 100f;

                //reading todays value
                ScanPoint sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                if (sP == null) continue;
                double currLow = sP.Low;
                sP = SM.readScanPoint(SM.getGraphStore().size() - 2, Meta.GRAPH_INDEX);
                if (sP == null) continue;
                double prevHigh = sP.High;
                sP = SM.readScanPoint(SM.getGraphStore().size() - 3, Meta.GRAPH_INDEX);
                if (sP == null) continue;
                double firstLow = sP.Low;
                if ((currLow >= gapUpFactor * prevHigh) && (prevHigh <= gapDownFactor * firstLow)) {
                    //Company cmpny = (Company)SM.getCompanyList().get(j);
                    String cmpny = (String) SM.getCompanyList().get(itrCompany);
                    sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                    double pcntGapUp = 100f * (currLow - prevHigh) / prevHigh;
                    double pcntGapDown = 100f * (firstLow - prevHigh) / firstLow;
                    RsIslandTops result = new RsIslandTops(cmpny, sP, pcntGapUp, pcntGapDown); // no need to create new class for this result called RsIslandBottoms as its the same
                    scResult.add(result);
                    SM.fireUpDateUI();
                }
            }
        }
        System.out.println("@@@@@@@@ End Scanning GapDowns after: " + (System.currentTimeMillis() - entryTime));

    }

    public String getShortName() {
//            return "Island Bottoms > " + getGapDownPercent() + "% drop, " + getGapUpPercent()+ "% rise";
        return Language.getString("ISLAND_BOTTOMS") + ">" + getGapDownPercent() + Language.getString("PERCENTAGE_DROP") +
                ", " + getGapUpPercent() + Language.getString("PERCENTAGE_RISE");
    }
}