package com.isi.csvr.scanner.Scans;

import com.isi.csvr.chart.ChartRecord;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 26, 2008
 * Time: 9:55:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class ScanRecord extends ChartRecord implements Comparable {


    private double change;
    private double pcntChange;


    public ScanRecord(long time, double open, double high, double low, double close, double change, double pcntChange, double volume) {
        this.Time = time;
        this.Open = open;
        this.High = high;
        this.Low = low;
        this.Close = close;
        this.change = change;
        this.pcntChange = pcntChange;
        this.Volume = volume;
        //this.tmpSteps = null;
    }

    public ScanRecord() {

    }

    public ScanRecord(long time) {
        this.Time = time;
    }

    public long getTime() {
        return Time;
    }

    public void setTime(long time) {
        this.Time = time;
    }

    public double getOpen() {
        return Open;
    }

    public void setOpen(double open) {
        this.Open = open;
    }

    public double getHigh() {
        return High;
    }

    public void setHigh(double high) {
        this.High = high;
    }

    public double getLow() {
        return Low;
    }

    public void setLow(double low) {
        this.Low = low;
    }

    public double getClose() {
        return Close;
    }

    public void setClose(double close) {
        this.Close = close;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public double getPcntChange() {
        return pcntChange;
    }

    public void setPcntChange(double pcntChange) {
        this.pcntChange = pcntChange;
    }

    public double getVolume() {
        return Volume;
    }

    public void setVolume(double volume) {
        this.Volume = volume;
    }

    public int Compare(Object x, Object y) {
        long t1 = ((ScanRecord) x).Time;
        long t2 = ((ScanRecord) y).Time;
        return (t1 > t2) ? 1 : (t1 < t2) ? -1 : 0;
    }

    public int compareTo(Object o) {
        return Compare(this, o);
    }


}