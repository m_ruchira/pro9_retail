package com.isi.csvr.scanner.Scans;

import com.isi.csvr.scanner.Meta;
import com.isi.csvr.scanner.Results.RsVolume;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.ScanPoint;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:13:33 PM
 * To change this template use File | Settings | File Templates.
 */


public class VolumeGainers extends Volume {


    public static final int AVERAGE_VOLUME = 0;
    public static final int HIGHEST_VOLUME = 1;
    private int period;
    private int calculationType;
    private float noOfTimes;
    public VolumeGainers() {
        this.scanID = Meta.VOLUME_GAINERS;
        this.period = 30;
        this.noOfTimes = 2f;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(int calculationType) {
        this.calculationType = calculationType;
    }//CalculationType

    public float getNoOfTimes() {
        return noOfTimes;
    }

    public void setNoOfTimes(float noOfTimes) {
        this.noOfTimes = noOfTimes;
    }

    //public Volume(VolumeCalcType calculationType)
    //    : this() {
    //    this.calculationType = calculationType;
    //}

    public void scan(ScanManager SM, ArrayList scResult) {

        long entryTime = System.currentTimeMillis();
//                ArrayList scResult = new ArrayList();
        ArrayList alCompanylist = SM.getCompanyList();
        MainScannerWindow.setStatusMaxProgress(alCompanylist.size());
        for (int itrCompany = 0; itrCompany < alCompanylist.size(); itrCompany++) {
            DynamicArray companyHistory = SM.addScanRequest((String) alCompanylist.get(itrCompany));
            SM.clearGraphStore();
            SM.populateGraphStore(companyHistory);

            int bIndex = Math.max(SM.getGraphStore().size() - period - 1, 0);// thi
            if (itrCompany % 100 == 0) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (itrCompany % 10 == 0) {

                MainScannerWindow.setStatus(Language.getString("SCANNING") + " : " + (itrCompany + 1) + "/" + alCompanylist.size() + "  " + SharedMethods.getSymbolFromKey((String) alCompanylist.get(itrCompany)));
                MainScannerWindow.setStatusProgress(itrCompany);
            }
            if (!MainScannerWindow.isScanning) {
                break;
            }
            if (SM.getGraphStore().size() > 1) {

                double sum = 0, high = 0, count = 0;
                boolean tmpResult = false;
                //reading todays value
                ScanPoint sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                if (sP == null) continue;
                double currVal = sP.Volume;
                if (scanID != Meta.VOLUME) {
                    double currClose = sP.Close;
                    sP = SM.readScanPoint(SM.getGraphStore().size() - 2, Meta.GRAPH_INDEX);
                    if (sP == null) continue;
                    double prevClose = sP.Close;
                    if (scanID == Meta.VOLUME_GAINERS && (prevClose >= currClose)) continue;
                    if (scanID == Meta.VOLUME_LOOSERS && (prevClose <= currClose)) continue;
                }
                //sacnning history
                for (int i = bIndex; i < SM.getGraphStore().size() - 1; i++) {
                    sP = SM.readScanPoint(i, Meta.GRAPH_INDEX);
                    if (sP != null) {
                        double val = sP.Volume;
                        if (calculationType == AVERAGE_VOLUME) {
                            sum += val;
                            count++;
                        } else {
                            if (noOfTimes * val > currVal) {
                                tmpResult = false;
                                break;
                            } else {
                                high = Math.max(high, val);
                                tmpResult = true;
                            }
                        }
                    }
                }
                if (calculationType == AVERAGE_VOLUME) {
                    high = sum / count;
                    tmpResult = (noOfTimes * high < currVal);
                }
                if (tmpResult) {
                    String cmpny = (String) SM.getCompanyList().get(itrCompany);
                    sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                    double pcntAbvHigh = currVal / high; //100d * (currVal - high) / high;
                    RsVolume result = new RsVolume(cmpny, sP, pcntAbvHigh, (double) noOfTimes);
                    scResult.add(result);
                    SM.fireUpDateUI();
                }
            }
        }
        System.out.println("@@@@@@@@ End Scanning Volume after: " + (Calendar.getInstance().getTimeInMillis() - entryTime));
//                return scResult;
    }

    public String getShortName() {
        //return "Volume Gainers " + period + " day " + noOfTimes + " x above " + (calculationType== HIGHEST_VOLUME ? "high" : "avg.");
        // return "Volume Gainers " + period + " day " + noOfTimes + " x above " + (calculationType == HIGHEST_VOLUME ? "high" : "avg.");

        return Language.getString("VOLUME_GAINERS") + " " + period + " " +
                Language.getString("DAY") + " " + noOfTimes + " x " + Language.getString("ABOVE") + " " +
                (calculationType == HIGHEST_VOLUME ? Language.getString("FD2_HIGH") : Language.getString("AVG"));
    }


}
    

