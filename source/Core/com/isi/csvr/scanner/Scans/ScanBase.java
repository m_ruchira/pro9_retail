package com.isi.csvr.scanner.Scans;

import com.isi.csvr.scanner.ScanManager;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:13:09 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class ScanBase {

    protected int scanID;

    //    public abstract ArrayList scan(ScanManager SM , ArrayList scResult );
    public abstract void scan(ScanManager SM, ArrayList scResult);

    public abstract String getShortName();


    public int getScanId() {
        return scanID;
    }
}


    

