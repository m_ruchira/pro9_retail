// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.history;

/**
 * Exports files in the StockNet system
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Exporter extends Thread {
    String g_sSourcePath;
    int g_iAction;
    File[] g_oSourceFiles;
    String g_sDestination;
//    String	enteredFileName;

    /**
     * Constructor
     */
    public Exporter() {
        super("Exporter");
        //saveFullHistory();
    }

    /*
    * Saves the history csv file
    */
    public void saveFullHistory() {
        int status = 0;
        g_sSourcePath = Settings.HISTORY_FLAT_DB_PATH;

        JFileChooser oFC = new JFileChooser();
        oFC.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        oFC.setDialogTitle(Language.getString("FILECHOOSER_EXPORT_TITLE"));

        status = oFC.showSaveDialog(null);
        if (status == JFileChooser.APPROVE_OPTION) {
            g_sDestination = oFC.getSelectedFile().getAbsolutePath();

            g_iAction = 1;
            File oSourceDir = new File(g_sSourcePath);
            g_oSourceFiles = oSourceDir.listFiles();
            this.start();
        } else {
        }
    }

    /**
     * Coppies a given file to the given destination
     */
    private void copyFile(File[] oaSource, String g_sDestination) {
        try {
            int iFiles = 0;
            while (iFiles < oaSource.length) {
                FileInputStream oIn = new FileInputStream(oaSource[iFiles]);

                long lTotal = oaSource[iFiles].length();
                long lProgress = 0;
                //ProgressMonitor oMonitor = new ProgressMonitor(null,"Coppying","Files",0,iFiles);
                //oMonitor.setMillisToDecideToPopup(1);
                FileOutputStream oOut = new FileOutputStream(g_sDestination + "\\" + oaSource[iFiles].getName());
                byte[] bytes = new byte[1024];
                int i;

                while (true) {
                    i = oIn.read(bytes);
                    if (i == -1) break;
                    oOut.write(bytes, 0, i);
                    lProgress += i;
                    //System.out.print(lProgress*100/lTotal);
                }
                iFiles++;
                //oMonitor.setProgress(iFiles);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        switch (g_iAction) {
            case 1:
                generateArchive(g_sDestination); //, enteredFileName);
                //copyFile(g_oSourceFiles,g_sDestination);
                // to do - other actions
        }
    }

    /**
     * main
     *
     * @param args
     */
    /*public static void main(String[] args)
    {
        Exporter exporter = new Exporter();
    }*/
    public void generateArchive(String path) { //, String zipFileName) {

        byte[] bytes = null;
        int readValue = 0;
        int iFiles = 0;
        long lTotal = 0;
        long lProgress = 0;
        String fileName = "";
        String sRecord = null;
        FileOutputStream out = null;
        FileInputStream in = null;

        try {
            FileOutputStream oFileOut = new FileOutputStream(path); // + zipFileName);
            ZipOutputStream oZipOut = new ZipOutputStream(oFileOut);
            oZipOut.setLevel(9);
            //System.out.println("Generating Export Archive");

            while (iFiles < g_oSourceFiles.length) {
                in = new FileInputStream(g_oSourceFiles[iFiles]);

                lTotal = g_oSourceFiles[iFiles].length();
                lProgress = 0;
                //ProgressMonitor oMonitor = new ProgressMonitor(null,"Coppying","Files",0,iFiles);
                //oMonitor.setMillisToDecideToPopup(1);

                fileName = g_oSourceFiles[iFiles].getName();
                // Add a Zip Entry
                oZipOut.putNextEntry(new ZipEntry(fileName));

                bytes = new byte[1024];

                while (true) {
                    readValue = in.read(bytes);
                    if (readValue == -1) break;

                    // Write the file contents to the Zip File
                    oZipOut.write(bytes, 0, readValue);
                    lProgress += readValue;
                    //System.out.print(lProgress*100/lTotal);
                }

                iFiles++;
                //oMonitor.setProgress(iFiles);
                //System.out.println("Completed " + fileName);
                oZipOut.closeEntry();

                // Initialize variables
                in = null;
                lTotal = 0;
            }  // End of Outer While

            // Closing OutPutStreams.....
            oZipOut.close();
            oFileOut.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

