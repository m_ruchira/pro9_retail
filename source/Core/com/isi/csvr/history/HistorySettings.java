// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.history;

/**
 * Maintains the history settings property file
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.shared.Settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class HistorySettings {
    public static final String HISTORY_SYNC = "X";
    public static final String HISTORY_PATH = "./FlatDatabase/History/";
    public static final String OHLC_PATH = "./FlatDatabase/OHLC/";
    public static final String ANNOUNCEMENT_PATH = "/StockNetClient/History/Full/Announcements.zip";
    private static String g_sLastTradeSequence = "0";
    private static boolean g_bDownloading = false;
    private static Properties g_oProperties;
    private static boolean g_bUpdating = false;
    private static boolean g_bDownloadInterrupted = false;
    private static boolean g_bLastConnectedDateUpdated = false;
    private static long g_lNoOfTrades = 0;

    /**
     * Constructor
     */
    public HistorySettings() {
        g_oProperties = new Properties();
        try {
            FileInputStream oIn = new FileInputStream(new File(Settings.SYSTEM_PATH + "/hi.dll"));
            g_oProperties.load(oIn);
            g_sLastTradeSequence = (String) g_oProperties.getProperty("LAST_TRADE_ID");
            if ((g_sLastTradeSequence == null) || (g_sLastTradeSequence.equals(""))) {
                g_sLastTradeSequence = "0";
            }
            oIn.close();
        } catch (Exception s_e) {
            //System.out.println(s_e+" error in loading HistorySettings file");
        }
    }

    /**
     * Sets the last trade's sequence number
     * <p/>
     * public static void setLastTradeID(String sID)
     * {
     * g_sLastTradeSequence = sID;
     * }
     * <p/>
     * /*
     * Sets the download status to invalid by making
     * the last traded id to 0. this is usefull if the
     * downlod is interrupted, to start downloading
     * from very begenning.
     */
    /*public static void initiateDownload()
    {
		g_oProperties.put("LAST_TRADE_ID","0");
        g_bDownloading = true;
		save();
        //System.out.println("Download initiated");
    }

    public static void completeDownload()
    {
    	g_oProperties.put("LAST_TRADE_ID",g_sLastTradeSequence);
        g_bDownloading = false;
        save();
    }*/

    /*public static String getLastTradeID()
       {
           return g_sLastTradeSequence;
       }*/
    public static String getLastConnectedDate() {
        return getItem("LAST_CONNECTED");
    }

    public static synchronized void setLastConnectedDate(String sDate) {
        saveItem("LAST_CONNECTED", sDate);
        //save();
    }

    public static int getMarketStatus() {
        try {
            return Integer.parseInt(getItem("MARKET_STATUS"));
        } catch (Exception e) {
            return 0;
        }
    }

    public static synchronized void setMarketStatus(String status) {
        saveItem("MARKET_STATUS", status);
        //save();
    }

    public static long getLastEODDate() {
        String sItem = getItem("LAST_EOD_DATE");
        try {
            return Long.parseLong(sItem);
        } catch (Exception e) {
            return 0;
        }

    }

    public static synchronized void setLastEODDate(long lDate) {
        saveItem("LAST_EOD_DATE", "" + lDate);
        //save();
    }

    public static void setProperty(String property, String value) {
        saveItem(property, value);
    }

    public static String getProperty(String property) {
        return getItem(property);
    }

    /*public static synchronized void setTradeInfo(long lValue,long lsSequence)
      {
          saveItem("NO_OF_TRADES_RECEIVED","" + lValue);
        saveItem("LAST_TRADE_ID","" + lsSequence);
        save();
      }*

    public static String getNoOfTradesReceived()
    {
        return getItem("NO_OF_TRADES_RECEIVED");
    }

    public static String getLastTradeID()
    {
        return getItem("LAST_TRADE_ID");
    }*/

    public static synchronized void save() {
        try {
            FileOutputStream oOut = new FileOutputStream(new File(Settings.SYSTEM_PATH + "/hi.dll"));
            g_oProperties.store(oOut, "");
            oOut.close();
            //System.out.println("history file ");
        } catch (Exception s_e) {
//          	System.out.println("error in Storing "+s_e);
        }
    }

    public static void saveItem(String sTitle, String sItem) {
        g_oProperties.put(sTitle, sItem);
    }

    public static String getItem(String sItem) {

        sItem = g_oProperties.getProperty(sItem);
        return sItem;
    }

    public static boolean isHistoryBeingDownloaded() {
        return g_bDownloading;
    }

    public static void setHistoryUpdating(boolean bUpdating) {
        g_bUpdating = bUpdating;
    }

    /*
    * Interrupt the download process
    */
    public static void interruptDownload() {
//  	System.out.println("Download interrupted");
        g_bDownloadInterrupted = true;
        g_bDownloading = false;
        g_sLastTradeSequence = getItem("LAST_TRADE_ID");
    }

    public static void authorizeDownload() {
//  	System.out.println("Download authorised");
        g_bDownloadInterrupted = false;
    }

    /*
    * Check if the download is interrupted
    */
    public static boolean isDownloadInterruptedx() {
        return g_bDownloadInterrupted;
    }

    /**
     * Sets the laset connected date valid for the system
     *
     public static void setLastConnectedDateUpdated()
     {
     g_bLastConnectedDateUpdated = true;
     }

     /**
     * Sets the laset connected date for the system
     *
     public static void setLastConnectedDateInvalid()
     {
     g_bLastConnectedDateUpdated = false;
     }

     /**
     * Checks if the last connectd date is updated
     *
     public static boolean isLastConnectedDateUpdated()
     {
     return g_bLastConnectedDateUpdated;
     }*/

    /*public static long getNextTradeSequence()
    {
    	return ++g_lNoOfTrades;
    }

    public static void resetTradeSequence()
    {
    	g_lNoOfTrades = 0;
    }*/
}

