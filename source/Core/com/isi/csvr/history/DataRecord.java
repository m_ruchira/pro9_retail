/*
 * Classname:     DataRecord.java
 * Revised By:    Niroshan Serasinghe
 * Version:       Chart 1.0_beta
 * Date:          22nd August 2001
 * Copyright:     (c) 2001 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
 */

package com.isi.csvr.history;

import java.io.Serializable;
import java.util.Date;

public class DataRecord extends Object implements Serializable {

    protected Date dateTime;
    protected String News;               //udaka
    protected float price;
    protected float close;
    protected float vwap;
    protected float open;
    protected float high;
    protected float low;
    protected long volume;
    protected double turnOver;
    protected long Time;               //udaka

    /**
     * Constructor
     */
    public DataRecord() {

    }

    public Date getDate() {
        return dateTime;
    }

    /* Date*/
    public void setDate(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getNews() {   //udaka
        return News;
    }

    /* News*/   //udaka
    public void setNews(String s) { //udaka
        this.News = s;
    }

    public long getTime() {   //udaka
        return Time;
    }

    /* Time*/   //udaka
    public void setTime(long t) { //udaka
        this.Time = t;
    }

    public float getPrice() {
        return price;
    }

    /*Price*/
    public void setPrice(float price) {
        this.price = price;
    }

    public float getClose() {
        return close;
    }

    /*previousclose*/
    public void setClose(float previousclose) {
        this.close = previousclose;
    }

    public float getOpen() {
        return open;
    }

    /*todaysOpen*/
    public void setOpen(float todaysOpen) {
        this.open = todaysOpen;
    }

    public float getHigh() {
        return high;
    }

    /*high*/
    public void setHigh(float high) {
        this.high = high;
    }

    public float getLow() {
        return low;
    }

    /*Low*/
    public void setLow(float low) {
        this.low = low;
    }

    public long getVolume() {
        return volume;
    }

    /*volume*/
    public void setVolume(long volume) {
        this.volume = volume;
    }

    public double getTurnOver() {
        return turnOver;
    }

    /*turn over*/
    public void setTurnOver(double turnover) {
        this.turnOver = turnover;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public float getVwap() {
        return vwap;
    }

    public void setVwap(float vwap) {
        this.vwap = vwap;
    }
}