package com.isi.csvr.history;

// Copyright (c) 2000 Home

import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.util.PageSetup;
import com.isi.csvr.util.WordWriter;

import java.io.*;
import java.math.BigInteger;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class DataExporter extends Thread {
    public final static int CSV = 0;
    public final static int META_STOCK_TXT = 1;
    public final static int META_STOCK_DB = 2;
    public final static int DOC = 3;

    private String[] symbols;
    private int g_iFomDate;
    private int g_iToDate;
    private int g_iType;
    private String g_sDestination;
    private boolean g_isMultipleSelected;
    private ViewSetting g_oViewSetting;
    private ExportListener g_oParent;

    /**
     * Constructor
     */
    public DataExporter(ViewSetting viewSetting, String[] asSymbols, String sFomDate,
                        String sToDate, int iType, String sDestination, boolean isMultipleSelected) {
        super("HistoryDataExporter");
        symbols = asSymbols;
        g_oViewSetting = viewSetting;
        g_iFomDate = (new Integer(sFomDate)).intValue();
        g_iToDate = (new Integer(sToDate)).intValue();
        g_iType = iType;
        g_sDestination = sDestination;
        g_isMultipleSelected = isMultipleSelected;
    }

    public void addExportListener(ExportListener oParent) {
        this.g_oParent = oParent;
    }

    public void run() {
        if (symbols == null) return;

        if (g_iType == CSV) {
            if (g_isMultipleSelected) {
                try {
                    for (int i = 0; i < symbols.length; i++) {
                        try {
                            String symbol = SharedMethods.getSymbolFromKey(symbols[i]);
                            String exchange = SharedMethods.getExchangeFromKey(symbols[i]);
                            PrintStream out = new PrintStream(new FileOutputStream(g_sDestination + "\\" + symbol + ".csv"));
//                            PrintStream out = new PrintStream(new FileOutputStream(g_sDestination));

                            out.println(getColumnHeadings());
                            File[] paths = HistoryFilesManager.getHistoryPaths(exchange);
                            for (int j = 0; j < paths.length; j++) {
                                try {
                                    BufferedReader in = new BufferedReader(new FileReader(paths[j].getAbsolutePath() +
                                            "/" + symbol + ".csv"));
                                    while (true) {
                                        String sRecord = readNextCSVRecord(in);
                                        if (sRecord == null) break;
                                        out.println(sRecord);
                                        sRecord = null;
                                    }

                                    in.close();
                                    in = null;
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                            paths = null;
                            out.flush();
                            out.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (g_oParent != null)
                            g_oParent.exportProgressChanged(i + 1, symbols.length);
                    }
                } catch (Exception e) {
                    //g_oParent.exportProgressChanged(i+1,g_asSymbols.length);
                    if (g_oParent != null)
                        g_oParent.exportCompleted(e);
                }
            } else {
                try {
                    for (int i = 0; i < symbols.length; i++) {
                        try {
                            String symbol = SharedMethods.getSymbolFromKey(symbols[i]);
                            String exchange = SharedMethods.getExchangeFromKey(symbols[i]);
//                        PrintStream out = new PrintStream(new FileOutputStream(g_sDestination + "\\" + symbol + ".csv"));
                            PrintStream out = new PrintStream(new FileOutputStream(g_sDestination));

                            out.println(getColumnHeadings());
                            File[] paths = HistoryFilesManager.getHistoryPaths(exchange);
                            for (int j = 0; j < paths.length; j++) {
                                try {
                                    BufferedReader in = new BufferedReader(new FileReader(paths[j].getAbsolutePath() +
                                            "/" + symbol + ".csv"));
                                    while (true) {
                                        String sRecord = readNextCSVRecord(in);
                                        if (sRecord == null) break;
                                        out.println(sRecord);
                                        sRecord = null;
                                    }

                                    in.close();
                                    in = null;
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                            paths = null;
                            out.flush();
                            out.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (g_oParent != null)
                            g_oParent.exportProgressChanged(i + 1, symbols.length);
                    }
                } catch (Exception e) {
                    //g_oParent.exportProgressChanged(i+1,g_asSymbols.length);
                    if (g_oParent != null)
                        g_oParent.exportCompleted(e);
                }
            }

        } else if (g_iType == DOC) {
            if (g_isMultipleSelected) {
                try {
                    for (int i = 0; i < symbols.length; i++) {
                        try {
                            String symbol = SharedMethods.getSymbolFromKey(symbols[i]);
                            String exchange = SharedMethods.getExchangeFromKey(symbols[i]);
                            WordWriter writer = new WordWriter();
                            writer.addColumnNames(getColumnHeadings() + "\n");

                            //==
                            /*  WordWriter writer = new WordWriter();
                            writer.addColumnNames(columnHeadings);
                            if (trades != null) {
                                Trade trade;
                                for (int i = 0; i < trades.size(); i++) {
                                    trade = (Trade) trades.get(i);
                                    //                            writer.appendText(new String(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns)));
                                    writer.addDetails(new String(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns)));
                                    trade = null;
                                }
                            } else {
                                System.out.println("Export from Historical Intraday");
                                exportTrades(null, columns, exchange, fileName);
                            }
                            writer.setPage(PageSetup.selectedOrientation, PageSetup.selectedPaperSize, PageSetup.selectedLeftMargin, PageSetup.selectedRightMargin, PageSetup.selectedTopMargin, PageSetup.selectedBottomMargin);
                            writer.writeTable();
                            writer.saveFile(sFile);*/
                            //===
                            //                         BufferedReader in = new BufferedReader(new FileReader(g_sDestination));

                            try {
                                File[] paths = HistoryFilesManager.getHistoryPaths(exchange);
                                for (int j = 0; j < paths.length; j++) {
                                    try {
                                        BufferedReader in = new BufferedReader(new FileReader(paths[j].getAbsolutePath() +
                                                "/" + symbol + ".csv"));

                                        while (true) {
                                            String sRecord = readNextCSVRecord(in);
                                            if (sRecord == null) break;
                                            ///out.println(sRecord);
                                            //                                        writer.appendText(sRecord);
                                            writer.addDetails(sRecord + "\n");

                                            sRecord = null;
                                        }

                                        in.close();
                                        in = null;
                                    } catch (Exception e) {
                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    }
                                }
                                paths = null;
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                            /// out.flush();
                            ///out.close();


                            writer.setPage(PageSetup.selectedOrientation, PageSetup.selectedPaperSize, PageSetup.selectedLeftMargin, PageSetup.selectedRightMargin, PageSetup.selectedTopMargin, PageSetup.selectedBottomMargin);
                            writer.writeTable();
                            writer.saveFile(g_sDestination + "\\" + symbol + ".doc");
//                            writer.saveFile(g_sDestination);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (g_oParent != null)
                            g_oParent.exportProgressChanged(i + 1, symbols.length);
                    }
                } catch (Exception e) {
                    //g_oParent.exportProgressChanged(i+1,g_asSymbols.length);
                    if (g_oParent != null)
                        g_oParent.exportCompleted(e);
                }
            } else {
                try {
                    for (int i = 0; i < symbols.length; i++) {
                        try {
                            String symbol = SharedMethods.getSymbolFromKey(symbols[i]);
                            String exchange = SharedMethods.getExchangeFromKey(symbols[i]);
                            WordWriter writer = new WordWriter();
                            writer.addColumnNames(getColumnHeadings() + "\n");

                            //==
                        /*  WordWriter writer = new WordWriter();
                        writer.addColumnNames(columnHeadings);
                        if (trades != null) {
                            Trade trade;
                            for (int i = 0; i < trades.size(); i++) {
                                trade = (Trade) trades.get(i);
                                //                            writer.appendText(new String(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns)));
                                writer.addDetails(new String(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns)));
                                trade = null;
                            }
                        } else {
                            System.out.println("Export from Historical Intraday");
                            exportTrades(null, columns, exchange, fileName);
                        }
                        writer.setPage(PageSetup.selectedOrientation, PageSetup.selectedPaperSize, PageSetup.selectedLeftMargin, PageSetup.selectedRightMargin, PageSetup.selectedTopMargin, PageSetup.selectedBottomMargin);
                        writer.writeTable();
                        writer.saveFile(sFile);*/
                            //===
//                         BufferedReader in = new BufferedReader(new FileReader(g_sDestination));

                            try {
                                File[] paths = HistoryFilesManager.getHistoryPaths(exchange);
                                for (int j = 0; j < paths.length; j++) {
                                    try {
                                        BufferedReader in = new BufferedReader(new FileReader(paths[j].getAbsolutePath() +
                                                "/" + symbol + ".csv"));

                                        while (true) {
                                            String sRecord = readNextCSVRecord(in);
                                            if (sRecord == null) break;
                                            ///out.println(sRecord);
//                                        writer.appendText(sRecord);
                                            writer.addDetails(sRecord + "\n");

                                            sRecord = null;
                                        }

                                        in.close();
                                        in = null;
                                    } catch (Exception e) {
                                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                    }
                                }
                                paths = null;
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                            /// out.flush();
                            ///out.close();


                            writer.setPage(PageSetup.selectedOrientation, PageSetup.selectedPaperSize, PageSetup.selectedLeftMargin, PageSetup.selectedRightMargin, PageSetup.selectedTopMargin, PageSetup.selectedBottomMargin);
                            writer.writeTable();
//                        writer.saveFile(g_sDestination + "\\" + symbol + ".doc");
                            writer.saveFile(g_sDestination);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (g_oParent != null)
                            g_oParent.exportProgressChanged(i + 1, symbols.length);
                    }
                } catch (Exception e) {
                    //g_oParent.exportProgressChanged(i+1,g_asSymbols.length);
                    if (g_oParent != null)
                        g_oParent.exportCompleted(e);
                }
            }
        } else {
            if (g_isMultipleSelected) {
                try {
                    PrintStream out = new PrintStream(new FileOutputStream(g_sDestination));
                    out.println("<ticker>,<per>,<date>,<open>,<high>,<low>,<close>,<vol>");
                    for (int i = 0; i < symbols.length; i++) {
                        try {
                            String symbol = SharedMethods.getSymbolFromKey(symbols[i]);
                            String exchange = SharedMethods.getExchangeFromKey(symbols[i]);
                            File[] paths = HistoryFilesManager.getHistoryPaths(exchange);
                            for (int j = 0; j < paths.length; j++) {
                                try {
                                    BufferedReader in = new BufferedReader(new FileReader(paths[j].getAbsolutePath() + "\\" + symbol + ".csv"));
                                    while (true) {
                                        String sRecord = readNextMetaStockRecord(in);
                                        if (sRecord == null) break;
                                        out.println(symbol + ",D," + sRecord);
                                        sRecord = null;
                                    }
                                    in.close();
                                    in = null;
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (g_oParent != null)
                            g_oParent.exportProgressChanged(i + 1, symbols.length);
                    }
                    out.flush();
                    out.close();
                    out = null;
                } catch (Exception e) {
                    //g_oParent.exportProgressChanged(i+1,g_asSymbols.length);
                    if (g_oParent != null)
                        g_oParent.exportCompleted(e);
                }
            } else {
                try {
                    PrintStream out = new PrintStream(new FileOutputStream(g_sDestination));
                    out.println("<ticker>,<per>,<date>,<open>,<high>,<low>,<close>,<vol>");
                    for (int i = 0; i < symbols.length; i++) {
                        try {
                            String symbol = SharedMethods.getSymbolFromKey(symbols[i]);
                            String exchange = SharedMethods.getExchangeFromKey(symbols[i]);
                            File[] paths = HistoryFilesManager.getHistoryPaths(exchange);
                            for (int j = 0; j < paths.length; j++) {
                                try {
                                    BufferedReader in = new BufferedReader(new FileReader(paths[j].getAbsolutePath() + "\\" + symbol + ".csv"));
                                    while (true) {
                                        String sRecord = readNextMetaStockRecord(in);
                                        if (sRecord == null) break;
                                        out.println(symbol + ",D," + sRecord);
                                        sRecord = null;
                                    }
                                    in.close();
                                    in = null;
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (g_oParent != null)
                            g_oParent.exportProgressChanged(i + 1, symbols.length);
                    }
                    out.flush();
                    out.close();
                    out = null;
                } catch (Exception e) {
                    //g_oParent.exportProgressChanged(i+1,g_asSymbols.length);
                    if (g_oParent != null)
                        g_oParent.exportCompleted(e);
                }
            }
        }
        if (g_oParent != null)
            g_oParent.exportCompleted(null);
    }

    private String getColumnHeadings() {

        String[] headings = Language.getList("EXPORTER_CSV_COLS");
        //change start
        BigInteger columns = g_oViewSetting.getColumns();
//        long columns = g_oViewSetting.getColumns();
        StringBuffer buffer = new StringBuffer("");

        for (int i = 0; i < headings.length; i++) {
            //change start
            if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
                buffer.append(",");
                buffer.append(headings[i]);
            }
            //change start
            columns = columns.shiftRight(1);
//            columns >>= 1;
        }
        return buffer.substring(1);
    }

    public String readNextCSVRecord(BufferedReader oIn) {
        String sRecord = "";
        String sFilteredRecord = "";
        int iDate = 0;
        String sDate = null;
        String sOpen = null;
        String sHigh = null;
        String sLow = null;
        String sChange = null;
        String sPChange = null;
        String sVolume = null;
        String sTurnover = null;
        //String  sPrice      = null;
        String sClose = null;
        //String  sMin        = null;
        //String  sMax        = null;
        //String  sTrades     = null;
        String sPrevClose = null;

        //change start
//        long columns = 0;
//
//        columns = g_oViewSetting.getColumns();
        BigInteger columns = g_oViewSetting.getColumns();
        //change end
        while (true) {
            try {
                sRecord = oIn.readLine();
                if (sRecord == null) return null;
                StringTokenizer oTokenizer = new StringTokenizer(sRecord, Meta.FS);
                sDate = oTokenizer.nextToken();
                iDate = Integer.parseInt(sDate);
                if ((iDate >= g_iFomDate) && (iDate <= g_iToDate)) {
                    //Date,Open,High,Low,Closed,% Change,Change,Prev. Closed,Volume,Turnover
                    sDate = (sDate.substring(0, 4) + "/" +
                            sDate.substring(4, 6) + "/" + sDate.substring(6, 8));
                    sOpen = oTokenizer.nextToken();     // open
                    sHigh = oTokenizer.nextToken();     // high
                    sLow = oTokenizer.nextToken();      // low
                    sClose = oTokenizer.nextToken();    // today's close
                    sPChange = oTokenizer.nextToken();  // % change
                    sChange = oTokenizer.nextToken();   // change
                    sPrevClose = oTokenizer.nextToken();// prev closed
                    sVolume = oTokenizer.nextToken();   // volumn
                    sTurnover = oTokenizer.nextToken(); // turnover


                    StringBuffer buffer = new StringBuffer("");
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sDate);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sOpen);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sHigh);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sLow);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sClose);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sPChange);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sChange);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sPrevClose);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sVolume);
                    }
                    //change start
                    columns = (columns.shiftRight(1));
//                    columns >>= 1;
                    //change start
                    if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//                    if ((columns & 1) == 1) {
                        buffer.append(",");
                        buffer.append(sTurnover);
                    }

                    /*sFilteredRecord += (sDate.substring(0,4) + "/" +
                        sDate.substring(4,6) + "/" + sDate.substring(6,8) + ",");
                    sFilteredRecord += oTokenizer.nextToken() + ","; // last trade price
                    oTokenizer.nextToken(); // ignore last trade quantity
                    sFilteredRecord += oTokenizer.nextToken() + ","; // open
                    sFilteredRecord += oTokenizer.nextToken() + ","; // high
                    sFilteredRecord += oTokenizer.nextToken() + ","; // low
                    sFilteredRecord += oTokenizer.nextToken() + ","; // % change
                    sFilteredRecord += oTokenizer.nextToken() + ","; // change
                    oTokenizer.nextToken(); // ignore prev closed
                    sFilteredRecord += oTokenizer.nextToken() + ","; // volume
                    sFilteredRecord += oTokenizer.nextToken(); // turnover*/
                    sDate = null;
                    sOpen = null;
                    sHigh = null;
                    sLow = null;
                    sChange = null;
                    sPChange = null;
                    sVolume = null;
                    sTurnover = null;
                    sClose = null;
                    sPrevClose = null;

                    return buffer.substring(1);
                }
            } catch (NoSuchElementException e) {
                //e.printStackTrace();
            } catch (Exception e) {
                return null;
            }
        }

    }

    public String readNextMetaStockRecord(BufferedReader oIn) {
        String sRecord = "";
        StringBuffer sFilteredRecord = new StringBuffer("");
        int iDate = 0;
        String sDate = null;

        while (true) {
            try {
                sRecord = oIn.readLine();
                if (sRecord == null) return null;
                StringTokenizer oTokenizer = new StringTokenizer(sRecord, Meta.FS);
                sDate = oTokenizer.nextToken();
                iDate = Integer.parseInt(sDate);
                //"<symbol>,<date>,<open>,<high>,<low>,<close>,<vol>"
                if ((iDate >= g_iFomDate) && (iDate <= g_iToDate)) {
                    sFilteredRecord.append(sDate); // date
                    sFilteredRecord.append(",");
                    sFilteredRecord.append(oTokenizer.nextToken()); // open
                    sFilteredRecord.append(",");
                    sFilteredRecord.append(oTokenizer.nextToken()); // high
                    sFilteredRecord.append(",");
                    sFilteredRecord.append(oTokenizer.nextToken()); // low
                    sFilteredRecord.append(",");
                    sFilteredRecord.append(oTokenizer.nextToken()); // close
                    sFilteredRecord.append(",");
                    oTokenizer.nextToken();
                    oTokenizer.nextToken();
                    oTokenizer.nextToken();
                    sFilteredRecord.append(oTokenizer.nextToken()); // volume


                    return sFilteredRecord.toString();
                }
            } catch (NoSuchElementException e) {
            } catch (Exception e) {
                return null;
            }
        }
    }
}

/*
symbol,

date,

Open

High

Low

Close

%chnage

change

previousclosed

volume

Turnover


*/