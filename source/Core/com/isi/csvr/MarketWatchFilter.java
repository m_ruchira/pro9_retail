package com.isi.csvr;

import com.isi.csvr.datastore.*;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum
 * Date: Nov 1, 2005
 * Time: 12:21:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarketWatchFilter extends JDialog implements ActionListener, Themeable {
    public static Color ASSETS_BACKGROUND = Color.GRAY;
    public static Color ASSETS_FOREGROUND = Color.BLACK;
    private Symbols symbols;
    private SectorFilter filter;
    private String exchange;
    private JPanel marketPanel;
    private JPanel sectorPanel;
    private JPanel currencyPanel;
    private JPanel assestsPanel;
    private TWButton okBtn, cancelBtn, resetBtn;
    private boolean ismarket = false, isAssest = false;


    public MarketWatchFilter(Frame frame, ClientTable clientTable, Symbols symbols) {
        super(frame, Language.getString("MARKET_WATCH_FILTER"), true);
        this.symbols = symbols;
//        this.clientTable = clientTable;
        this.exchange = clientTable.getMarketCode();
        if (symbols.getFilter() != null) {
            filter = (SectorFilter) symbols.getFilter();
        } else {
            filter = new SectorFilter();
        }
        this.applyTheme();
        createUI();
    }

    public void createUI() {

        ArrayList<String> marArray = new ArrayList<String>();
        ArrayList<String> secArray = new ArrayList<String>();
        ArrayList<String> currArray = new ArrayList<String>();
        ArrayList<String> assArray = new ArrayList<String>();
        int countMarket = 0;
        int countAssest = 0;
        // Closing operation added by Nishantha
        WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {

                filter.setFilterCriteria(null, null, null, null);
                MarketWatchFilter.this.dispose();
            }
        };
        this.addWindowListener(exitListener);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        //
        //this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        Container container = this.getContentPane();
        container.setLayout(new FlexGridLayout(new String[]{"810"}, new String[]{"30", "240", "40"}, 5, 5));
        this.setResizable(false);
        this.setSize(820, 330);
        container.setSize(820, 330);
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        this.setUndecorated(false);

//        String filteredCurr = "";
//        String filteredMar = "";
//        String filteredAss = "";
//        String filteredSec = "";
        if (filter.isFilterAvailable()) {
            if (filter.getFilterCriteriaCurrencies() != null) {
                currArray = filter.getFilterCriteriaCurrencies();
            }
            if (filter.getFilterCriteriaMarkets() != null) {
                marArray = filter.getFilterCriteriaMarkets();
            }
            if (filter.getFilterCriteriaAssets() != null) {
                assArray = filter.getFilterCriteriaAssets();
            }
            if (filter.getFilterCriteriaSectors() != null) {
                secArray = filter.getFilterCriteriaSectors();
            }
        } else {
            String[] filteredSymbols = symbols.getFilteredSymbols();
            for (int i = 0; i < filteredSymbols.length; i++) {
                Stock stock = DataStore.getSharedInstance().getStockObject(filteredSymbols[i]);
                if (stock != null) {
                    if (((stock.getCurrencyCode()) != null) && (!stock.getCurrencyCode().equals(Constants.DEFAULT_CURRENCY_CODE))) {
                        if (!currArray.contains(stock.getCurrencyCode())) {
//                            filteredCurr = filteredCurr + stock.getCurrencyCode() + ",";
                            currArray.add(stock.getCurrencyCode());
                        }
                    }
//                    if(((stock.getSectorCode())!=null)&&(!stock.getSectorCode().equals(Constants.DEFAULT_SECTOR))){
                    if ((stock.getSectorCode()) != null) {         //	SAUMUBMKT-1727 - Symbols of Unclassified sectors are loaded initially but not applied in the filter
                        if (!secArray.contains(stock.getSectorCode())) {
//                            filteredSec = filteredSec + stock.getSectorCode() + ",";
                            secArray.add(stock.getSectorCode());
                        }
                    }
                    if (((stock.getMarketID()) != null) && (!stock.getMarketID().equals(Constants.DEFAULT_MARKET))) {
                        if (!marArray.contains(stock.getMarketID())) {
//                            filteredMar = filteredMar + stock.getMarketID() +",";
                            marArray.add(stock.getMarketID());
                        }
                    }
                    if ((stock.getInstrumentType()) >= 0) {
                        if (!assArray.contains("" + SharedMethods.getSymbolType(stock.getInstrumentType()))) {
//                            filteredAss = filteredAss + SharedMethods.getSymbolType(stock.getInstrumentType()) + ",";
                            assArray.add("" + SharedMethods.getSymbolType(stock.getInstrumentType()));
                        }
                    }
                }
                stock = null;
            }
        }
        /*try {
            String[] ids = filteredAss.split("\\,");
            for(String id : ids){
                assArray.add(id);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            String[] ids = filteredMar.split("\\,");
            for(String id : ids){
                marArray.add(id);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            String[] ids = filteredSec.split("\\,");
            for(String id : ids){
                secArray.add(id);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            String[] ids = filteredCurr.split("\\,");
            for(String id : ids){
                currArray.add(id);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }*/

        JPanel mainPanel = new JPanel();
        MarketLabel marketLabel = new MarketLabel(Language.getString("MARKET"));
        MarketLabel sectorLabel = new MarketLabel(Language.getString("SECTORS"));
        MarketLabel assetsClassesLabel = new MarketLabel(Language.getString("ASSETS_CLASSES"));
        MarketLabel currencyLabel = new MarketLabel(Language.getString("CURRENCIES"));

        marketPanel = new JPanel();
        marketPanel.setLayout(new ColumnLayout());
        marketPanel.setBackground(ASSETS_BACKGROUND);
        Market[] markets = ExchangeStore.getSharedInstance().getExchange(exchange).getSubMarkets();
        if ((markets != null) && (markets.length > 0)) {
            countMarket = markets.length;
            for (Market market : markets) {
                String id = market.getMarketID();
                String des = market.getDescription();

                JCheckBox box = new JCheckBox();
                box.setBackground(ASSETS_BACKGROUND);
                box.setForeground(ASSETS_FOREGROUND);
                box.setText(des);
                box.setFont(new TWFont("Arial", 0, 14));
                box.setName(id);
                marketPanel.add(box);
                if (marArray.contains(id)) {
                    box.setSelected(true);
                }
            }
        }
        marArray.clear();
        JScrollPane marketScroll = new JScrollPane(marketPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        sectorPanel = new JPanel();
        sectorPanel.setLayout(new ColumnLayout());
        sectorPanel.setBackground(ASSETS_BACKGROUND);
        Enumeration<Sector> sectorEnum = SectorStore.getSharedInstance().getSectors(exchange);
        while (sectorEnum.hasMoreElements()) {
            Sector sector = sectorEnum.nextElement();

            JCheckBox box = new JCheckBox();
            box.setBackground(ASSETS_BACKGROUND);
            box.setForeground(ASSETS_FOREGROUND);
            box.setText(sector.getDescription());
            box.setFont(new TWFont("Arial", 0, 14));
            box.setName(sector.getId());
            sectorPanel.add(box);
            if (secArray.contains(sector.getId())) {
                box.setSelected(true);
            }
        }
        secArray.clear();
        JScrollPane sectorScroll = new JScrollPane(sectorPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        assestsPanel = new JPanel();
        assestsPanel.setLayout(new ColumnLayout());
        assestsPanel.setBackground(ASSETS_BACKGROUND);
        Enumeration<Short> assetsEnum = SharedMethods.getAssestsStore().keys();
        while (assetsEnum.hasMoreElements()) {
            Short id = assetsEnum.nextElement();
            countAssest++;
            String des = SharedMethods.getAssestsStore().get(id);

            JCheckBox box = new JCheckBox();
            box.setBackground(ASSETS_BACKGROUND);
            box.setForeground(ASSETS_FOREGROUND);
            box.setText(des);
            box.setFont(new TWFont("Arial", 0, 14));
            box.setName(id.toString());
            assestsPanel.add(box);
            if (assArray.contains(id.toString())) {
                box.setSelected(true);
            }
        }
        assArray.clear();
        JScrollPane assestScroll = new JScrollPane(assestsPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        currencyPanel = new JPanel();
        currencyPanel.setLayout(new ColumnLayout());
        currencyPanel.setBackground(ASSETS_BACKGROUND);
        Enumeration<String> currencyEnum = CurrencyStore.getSharedInstance().getCurrencyIDs();
        while (currencyEnum.hasMoreElements()) {
            String id = currencyEnum.nextElement();
            String des = CurrencyStore.getSharedInstance().getCurrencyDescription(id);

            JCheckBox box = new JCheckBox();
            box.setBackground(ASSETS_BACKGROUND);
            box.setForeground(ASSETS_FOREGROUND);
            box.setText(des);
            box.setFont(new TWFont("Arial", 0, 14));
            box.setName(id);
            currencyPanel.add(box);
            if (currArray.contains(id)) {
                box.setSelected(true);
            }
        }
        currArray.clear();
        JScrollPane currencyScroll = new JScrollPane(currencyPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        JPanel dataPanel = new JPanel();
        if (countMarket <= 1) {
            if (countAssest <= 1) {
                dataPanel.setLayout(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"230"}, 5, 5));
                mainPanel.setLayout(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"30"}, 5, 5));
            } else {
                dataPanel.setLayout(new FlexGridLayout(new String[]{"33%", "33%", "34%"}, new String[]{"230"}, 5, 5));
                mainPanel.setLayout(new FlexGridLayout(new String[]{"33%", "33%", "34%"}, new String[]{"30"}, 5, 5));
            }
        } else {
            if (countAssest <= 1) {
                dataPanel.setLayout(new FlexGridLayout(new String[]{"33%", "33%", "34%"}, new String[]{"230"}, 5, 5));
                mainPanel.setLayout(new FlexGridLayout(new String[]{"33%", "33%", "34%"}, new String[]{"30"}, 5, 5));
            } else {
                dataPanel.setLayout(new FlexGridLayout(new String[]{"25%", "25%", "25%", "25%"}, new String[]{"230"}, 5, 5));
                mainPanel.setLayout(new FlexGridLayout(new String[]{"25%", "25%", "25%", "25%"}, new String[]{"30"}, 5, 5));
            }
        }

        if (countMarket > 1) {
            mainPanel.add(marketLabel);
            dataPanel.add(marketScroll);
            ismarket = true;
        }
        mainPanel.add(sectorLabel);
        dataPanel.add(sectorScroll);
        if (countAssest > 1) {
            mainPanel.add(assetsClassesLabel);
            dataPanel.add(assestScroll);
            isAssest = true;
        }
        mainPanel.add(currencyLabel);
        dataPanel.add(currencyScroll);

        container.add(mainPanel); //,BorderLayout.NORTH);
        container.add(dataPanel); //,BorderLayout.CENTER);

        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
        resetBtn = new TWButton(Language.getString("RESET"));
        okBtn = new TWButton(Language.getString("OK"));
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        resetBtn.addActionListener(this);
        okBtn.addActionListener(this);
        cancelBtn.addActionListener(this);
        btnPanel.add(resetBtn);
        btnPanel.add(okBtn);
        btnPanel.add(cancelBtn);
        container.add(btnPanel); //, BorderLayout.SOUTH);
        this.pack();
        this.applyTheme();
        GUISettings.applyOrientation(getContentPane());
    }

    public void actionPerformed(ActionEvent ev) {
        Object obj = ev.getSource();
        if (obj == okBtn) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    ArrayList<String> marArray = null;
                    ArrayList<String> secArray = new ArrayList<String>();
                    ArrayList<String> currArray = new ArrayList<String>();
                    ArrayList<String> assArray = null;

                    Component[] exitems = sectorPanel.getComponents();
                    for (Component exitem : exitems) {
                        JCheckBox item = (JCheckBox) exitem;
                        if (item.isSelected()) {
                            secArray.add(item.getName());
                        }
                    }
                    exitems = currencyPanel.getComponents();
                    for (Component exitem : exitems) {
                        JCheckBox item = (JCheckBox) exitem;
                        if (item.isSelected()) {
                            currArray.add(item.getName());
                        }
                    }
                    if (isAssest) {
                        assArray = new ArrayList<String>();
                        exitems = assestsPanel.getComponents();
                        for (Component exitem : exitems) {
                            JCheckBox item = (JCheckBox) exitem;
                            if (item.isSelected()) {
                                assArray.add(item.getName());
                            }
                        }
                    }
                    if (ismarket) {
                        marArray = new ArrayList<String>();
                        exitems = marketPanel.getComponents();
                        for (Component exitem : exitems) {
                            JCheckBox item = (JCheckBox) exitem;
                            if (item.isSelected()) {
                                marArray.add(item.getName());
                            }
                        }
                    }
                    filter.setFilterCriteria(marArray, secArray, assArray, currArray);
                    symbols.setFilter(filter);
                    closeApp();
                }
            });

        } else if (obj == resetBtn) {
            filter.setFilterCriteria(null, null, null, null);
            symbols.setFilter(filter);
            closeApp();

        } else if (obj == cancelBtn) {
            filter.setFilterCriteria(null, null, null, null);
            closeApp();
        }
    }

    private void closeApp() {
        this.dispose();
    }

    public void applyTheme() {
        ASSETS_BACKGROUND = Theme.getColor("LIST_BGCOLOR");
        ASSETS_FOREGROUND = Theme.getColor("LIST_FGCOLOR");
    }

    class MarketLabel extends JLabel {
        public MarketLabel(String label) {
            super(label);
            super.setPreferredSize(new Dimension(150, 20));
        }
    }
}
