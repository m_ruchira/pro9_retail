// Copyright (c) 2000 Home
package com.isi.csvr;

import javax.swing.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


/**
 * This class listens for UISwitches, and updates a given component.
 *
 * @author Steve Wilson
 * @version 1.4 04/23/99
 */

public class UISwitchListener implements PropertyChangeListener {
    JComponent componentToSwitch;

    public UISwitchListener(JComponent c) {
        componentToSwitch = c;
    }

    public void propertyChange(PropertyChangeEvent e) {
        try {
            String name = e.getPropertyName();
            if (name.equals("lookAndFeel")) {
                SwingUtilities.updateComponentTreeUI(componentToSwitch);
                componentToSwitch.invalidate();
                componentToSwitch.validate();
                componentToSwitch.repaint();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}