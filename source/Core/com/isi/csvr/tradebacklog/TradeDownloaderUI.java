package com.isi.csvr.tradebacklog;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 20, 2003
 * Time: 2:43:23 PM
 * To change this template use Options | File Templates.
 */
public class TradeDownloaderUI extends InternalFrame
        implements ActionListener, TradeDataChangeListener,
        ProgressListener, Themeable, ExchangeListener, NonResizeable {
    private static TradeDownloaderUI self;
    private TradeFileList fileList;
    private JCheckBox chkAutoDownload;
    private TWButton btnDownload;
    private TWButton btnClose;
    private TWComboBox exchangeCombo;
    private ArrayList<TWComboItem> exchangeList;
    private JLabel lblDestination;
    private String exchange;

    public TradeDownloaderUI() throws HeadlessException {
        super(false);
        Theme.registerComponent(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Client.getInstance().getDesktop().add(this);
        //init();
    }

    public static TradeDownloaderUI getSharedInstance() {
        if (self == null) {
            self = new TradeDownloaderUI();
        }
        return self;
    }

    public void init() {
//        setLayer(GUISettings.TOP_LAYER);
        String[] widths = {"100%"};
        String[] heights = {"0", "0", "100%", "30"};
        getContentPane().setLayout(new FlexGridLayout(widths, heights, 0, 5));

        JPanel exchangePanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 2));
        JLabel exchangeLabel = new JLabel(Language.getString("EXCHANGE"));
        exchangeList = new ArrayList<TWComboItem>();
        exchangeCombo = new TWComboBox(new TWComboModel(exchangeList));
        exchangeCombo.setActionCommand("EXCHANGE");
        exchangeCombo.setPreferredSize(new Dimension(200, 25));
        exchangePanel.add(exchangeLabel);
        exchangePanel.add(exchangeCombo);
        getContentPane().add(exchangePanel);

        JLabel lblTitle = new JLabel("  " + Language.getString("AVAILABLE_DATES") + "  ");
        getContentPane().add(lblTitle);

        JPanel listPanel = new JPanel(new BorderLayout());
        JPanel panel0 = new JPanel();
        String[] widths1 = {"130", "70", "100%"};
        String[] heights1 = {"0"};
        panel0.setLayout(new FlexGridLayout(widths1, heights1, 0, 5));
        JLabel lblHead2 = new JLabel("  " + Language.getString("DATE") + "  ", JLabel.CENTER);
        lblHead2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        JLabel lblHead3 = new JLabel("  " + Language.getString("SIZE") + "  ", JLabel.CENTER);
        lblHead3.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        JLabel lblHead4 = new JLabel("  " + Language.getString("STATUS") + "  ");
        lblHead4.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        panel0.add(lblHead2);
        panel0.add(lblHead3);
        panel0.add(lblHead4);
        listPanel.add(panel0, BorderLayout.NORTH);

        fileList = new TradeFileList(TradeBacklogStore.getSharedInstance());
        JScrollPane scroll = new JScrollPane(fileList);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        listPanel.add(scroll, BorderLayout.CENTER);
        getContentPane().add(listPanel);

        chkAutoDownload = new JCheckBox(Language.getString("AUTO_DOWNLOAD_RECENT_FILE"));
        String value = HistorySettings.getProperty("AUTO_DOWNLOAD_LAST_TNS");
        if ((value == null) || (value.equals("")) || (value.equals("0"))) {
            chkAutoDownload.setSelected(false);
        } else {
            chkAutoDownload.setSelected(true);
        }
        value = null;
        chkAutoDownload.setActionCommand("AUTO");
        chkAutoDownload.addActionListener(this);
        //getContentPane().add(chkAutoDownload);

        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout(FlowLayout.TRAILING));
        btnDownload = new TWButton(Language.getString("DOWNLOAD"));
        btnDownload.setActionCommand("DOWNLOAD");
        btnDownload.addActionListener(this);
        btnClose = new TWButton(Language.getString("CLOSE"));
        btnClose.setActionCommand("CLOSE");
        btnClose.addActionListener(this);
        if (Settings.isTCPMode()) {
            panel2.add(btnDownload);
        }
        panel2.add(btnClose);
        getContentPane().add(panel2);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setResizable(false);
        setSize(350, 450);
        this.setClosable(true);
        setTitle(Language.getString("TIME_N_SALES_HISTORY"));

        TradeBacklogStore.getSharedInstance().addDataListener(this);
        TradesHistoryDownloader.getSharedInstance().addProgressListener(this);

        GUISettings.applyOrientation(this);
        setLayer(GUISettings.TOP_LAYER);
        fileList.updateUI();
    }

//    public void setVisible(boolean status) {
//        super.setVisible(status);
//        if (status == true) {
//
//        }
//    }

    public void initFileList() {
        for (int i = 1; i < fileList.getModel().getSize(); i++) {
            TradeFileListItem item = (TradeFileListItem) fileList.getModel().getElementAt(i);
            item.setStatus(TradeFileList.STATUS_SAVED);
            item = null;
        }
        fileList.repaint();
    }

    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);

        if (e.getActionCommand().equals("DOWNLOAD")) {
            TradesHistoryDownloader.getSharedInstance().startDownload();
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "HistoricalTrades");
        } else if (e.getActionCommand().equals("CLOSE")) {
            setVisible(false);
        } else if (e.getActionCommand().equals("AUTO")) {
            HistorySettings.setProperty("AUTO_DOWNLOAD_LAST_TNS", chkAutoDownload.isSelected() ? "1" : "0");
            TradeBacklogStore.getSharedInstance().setAutoDownloadRecent(chkAutoDownload.isSelected());
        } else if (e.getActionCommand().equals("BROWSE")) {
            browseFolders();
        } else if (e.getActionCommand().equals("OPEN_FILE")) {
            showFolder();
        } else if (e.getActionCommand().equals("EXCHANGE")) {
            exchange = ((TWComboItem) exchangeCombo.getSelectedItem()).getId();
            TradeBacklogStore.getSharedInstance().setCurrentExchange(exchange);
            fileList.updateUI();
        }
    }

    public void browseFolders() {
        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FOLDERNAME"));
        SmartFileChooser chooser = new SmartFileChooser(Settings.getTradeArchivePath());
        GUISettings.localizeFileChooserHomeButton(chooser);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle(Language.getString("OPEN_FOLDER"));
        chooser.hideContainerOf((String) UIManager.get("FileChooser.fileNameLabelText"));
        chooser.hideContainerOf((String) UIManager.get("FileChooser.filesOfTypeLabelText"));

        int status = chooser.showOpenDialog(this);
        if (status == JFileChooser.APPROVE_OPTION) {
            Settings.setTradeArchivePath(chooser.getSelectedFile().getAbsolutePath());
            lblDestination.setText("<HTML><U>" + Settings.getTradeArchivePath() + "</U>");
            initFileList();
            updateFileStatus();
        }


    }

    public void showFolder() {
        try {
            Runtime.getRuntime().exec("start explorer \"" + Settings.getTradeArchivePath() + "\"");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }

    public synchronized void updateFileStatus() {
        try {

            for (int i = 0; i < fileList.getModel().getSize(); i++) {
                TradeFileListItem item = (TradeFileListItem) fileList.getModel().getElementAt(i);
//                try {
//                    item.setDescription(dateFormatOut.format(dateFormatIn.parse(tokens[i].substring(0,8))));
//                } catch (ParseException e) {
//                    item.setDescription(tokens[i]);
//                }
                TradeBacklogStore.getSharedInstance().currentFile = item.getFileName();
                File[] files = TradeBacklogStore.getSharedInstance().getDownloadedFiles(exchange);
//                item.setFileName(currentFile);
//                item.setSize(Integer.parseInt(tokens[i+1]));
//                item = updateStore(item);

                TradeBacklogStore.getSharedInstance().checkAvalability(files, item); // check if the file is already downloaded
                item = null;
                files = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//        dataListener.listDownloaded();
//        fireDataChanged();
//        downloadRecentFile();
    }

    public void dataChanged() {
        fileList.repaint();// updateUI();
    }

    public void listDownloaded() {
        fileList.updateUI();
    }

    public void show() {
        setLocationRelativeTo(Client.getInstance().getDesktop());
        super.show();
        TradeBacklogStore.getSharedInstance().requestData();
        TradeBacklogStore.getSharedInstance().setCurrentExchange(exchange);
        fileList.updateUI();
    }

    public void setProcessStarted(String id) {
        btnDownload.setEnabled(false);
        exchangeCombo.setEnabled(false);
        btnClose.setText(Language.getString("HIDE"));
    }

    public void setMax(String id, int max) {

    }

    public void setProcessCompleted(String id, boolean sucess) {
        btnDownload.setEnabled(true);
        exchangeCombo.setEnabled(true);
        btnClose.setText(Language.getString("CLOSE"));
        fileList.unselectAll();
    }

    public void setProgress(String id, int progress) {
        fileList.repaint();
    }

    /*public void show(boolean b) {
        super.show(b);
        setLocationRelativeTo(Client.getInstance().getDesktop());
    }*/

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(getContentPane());
        fileList.initColors();
    }

    /*public void mouseClicked(MouseEvent e) {
        showFolder();
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }*/

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        exchangeCombo.removeActionListener(this);
        //exchangeCombo.removeAllItems();

        exchangeList.clear();
        //enabling download historical time n Sales for non default exchanges
        // 2009-06-22
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.getEnabledForIndexPanel() == 1) {
                if (ExchangeStore.getSharedInstance().ismarketSummaryAvailable(exchange.getSymbol())) {
                    TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                    exchangeList.add(comboItem);
                }
            } else if (exchange.getEnabledForIndexPanel() == -1 && exchange.isDefault()) {
                if (ExchangeStore.getSharedInstance().ismarketSummaryAvailable(exchange.getSymbol())) {
                    TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                    exchangeList.add(comboItem);
                }
            }
        }
     /*   for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
            if (exchange.isDefault() && !exchange.isExpired() && !exchange.isInactive()) {
                TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                exchangeList.add(comboItem);
            }
            exchange = null;
        }*/
        Collections.sort(exchangeList);
        exchangeCombo.updateUI();

        if (exchange != null) {
            try {
                for (int i = 0; i < exchangeCombo.getItemCount(); i++) {
                    if (((TWComboItem) exchangeCombo.getItemAt(i)).getId().equals(exchange)) {
                        exchangeCombo.setSelectedIndex(i);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use Options | File Templates.
            }
        } else {
            if (exchangeCombo.getModel().getSize() > 0) {
                if (exchangeCombo.getSelectedIndex() >= 0) {
                    exchange = ((TWComboItem) exchangeCombo.getItemAt(exchangeCombo.getSelectedIndex())).getId();
                } else {
                    exchange = ((TWComboItem) exchangeCombo.getItemAt(0)).getId();
                }
            }
        }

        exchangeCombo.addActionListener(this);
    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }
}
