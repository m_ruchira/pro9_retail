package com.isi.csvr.tradebacklog;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.downloader.ZipFileDownloader;
import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import javax.swing.*;
import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 21, 2003
 * Time: 11:28:57 AM
 * To change this template use Options | File Templates.
 */
public class TradesHistoryDownloader implements Runnable, ProgressListener {

    private static TradesHistoryDownloader self = null;
    private TradeFileListItem itemInProgress;
    private ProgressListener uiProgressListener;

    public TradesHistoryDownloader() {
    }

    public static TradesHistoryDownloader getSharedInstance() {
        if (self == null) {
            self = new TradesHistoryDownloader();
        }
        return self;
    }

    public void startDownload() {

        boolean canProceed = false;

        String exchange = TradeBacklogStore.getSharedInstance().getCurrentExchange();
        if (ExchangeStore.getSharedInstance().getExchange(exchange).getMarketStatus() == Meta.MARKET_OPEN) {
            int result = SharedMethods.showConfirmMessage(Language.getString("TRADE_HISTORY_DOWNLOAD_WARING"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                canProceed = true;
            }
        } else {
            canProceed = true;
        }

        if (canProceed) {
            Thread thread = new Thread(this, "TradesHistoryDownloader");
            thread.start();
        }
    }

    public void addProgressListener(ProgressListener listener) {
        uiProgressListener = listener;
    }

//    public void removeProgressListener(ProgressListener listener) {
//    }

    public void run() {
        String exchange = TradeBacklogStore.getSharedInstance().getCurrentExchange();
        int size = TradeBacklogStore.getSharedInstance().getDataStore(exchange).size();

        // Queue all
        for (int i = 0; i < size; i++) {
            TradeFileListItem item = TradeBacklogStore.getSharedInstance().getDataStore(exchange).get(i);
            if (item.isChecked()) {
                item.setStatus(TradeFileList.STATUS_QUEUED);
            }
            item = null;
        }
        TradeBacklogStore.getSharedInstance().fireDataChanged();

        // download queued
        for (int i = 0; i < size; i++) {
            TradeFileListItem item = TradeBacklogStore.getSharedInstance().getDataStore(exchange).get(i);
            if (item.getStatus() == TradeFileList.STATUS_QUEUED) {
                item.setStatus(TradeFileList.STATUS_DOWNLOADENG);
                if (download(exchange, item) == true) {
                    item.setStatus(TradeFileList.STATUS_SAVED);
                    item.setProgress("");
                    //DetailedTradeStore.getSharedInstance().loadFile(exchange, item, false);
                } else {
                    item.setStatus(TradeFileList.STATUS_ERROR);
                }
            }
            item = null;
            TradeBacklogStore.getSharedInstance().fireDataChanged();
        }

    }

    public boolean download(String exchange, TradeFileListItem item) {
        boolean bSucess = false;

        try {
            itemInProgress = item;

            String url = Meta.DAILY_TRADES + Meta.DS + exchange + Meta.FD + item.getFileName() + "\n";

            ZipFileDownloader downloader = new ZipFileDownloader(url, exchange, item.getFileName());
            downloader.addProgressListener(this);
            downloader.addProgressListener(uiProgressListener);
            File[] files = downloader.downloadFiles();

            if (files[0].length() > 0) {
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(files[0]));
                File f = new File(Settings.getTradeArchivePath() + exchange);
                if (!f.exists()) {
                    f.mkdir();
                }
                OutputStream out = new FileOutputStream(Settings.getTradeArchivePath() + exchange + "\\" + item.getFileName(), false);
                byte[] buffer = new byte[2000];
                int len;
                while (true) {
                    len = in.read(buffer);
                    if (len > 0) {
                        out.write(buffer, 0, len);
                        Thread.sleep(5);
                    } else {
                        bSucess = true;
                        break;
                    }
                }
            }
            if (!Settings.isConnected()) {
                bSucess = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            bSucess = false;
        }
        itemInProgress = null;
        return bSucess;
    }

    /* Progress Listener Methods */
    public void setProcessStarted(String id) {
        try {
            itemInProgress.setProgress("0%");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setMax(String id, int max) {

    }

    public void setProcessCompleted(String id, boolean sucess) {
        try {
            itemInProgress.setProgress("");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setProgress(String id, int progress) {
        try {
            itemInProgress.setProgress(progress + "%");
            uiProgressListener.setProgress(null, 0);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}
