package com.isi.csvr.tradebacklog;

import com.isi.csvr.Client;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trade.DetailedTradeStore;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 20, 2003
 * Time: 1:31:06 PM
 * To change this template use Options | File Templates.
 */
public class TradeFileList extends JList implements MouseListener, MouseMotionListener, Themeable {

    public static byte STATUS_NOT_SAVED = 0;
    public static byte STATUS_SAVED = 1;
    public static byte STATUS_DOWNLOADENG = 2;
    public static byte STATUS_QUEUED = 3;
    public static byte STATUS_ERROR = 4;

    public static byte MOUSE_LOCATION_DEFAULT = 0;
    private int mouseLocation = MOUSE_LOCATION_DEFAULT;
    public static byte MOUSE_LOCATION_OPEN = 1;
    public static byte MOUSE_LOCATION_EXPORT = 2;
    private ImageIcon[] icons;
    private Color foreground;
    private Color backround;
    private ImageIcon openIcon;//=new ImageIcon("images/common/open.gif");
    private ImageIcon exportIcon;//=new ImageIcon("images/common/exportdata.gif");

    public TradeFileList(ListModel dataModel) {
        super(dataModel);
        init();
    }

    public TradeFileList(final Object[] listData) {
        super(listData);
        init();
    }

    public TradeFileList(final Vector listData) {
        super(listData);
        init();
    }

    public TradeFileList() {
        init();
    }

    private void init() {
        addMouseListener(this);
        addMouseMotionListener(this);
        setCellRenderer(new JMultiColumnListRenderer());
        icons = new ImageIcon[5];
        icons[STATUS_SAVED] = new ImageIcon("images/common/tnsSaved.gif");
        icons[STATUS_NOT_SAVED] = new ImageIcon("images/common/tnsNotSaved.gif");
        icons[STATUS_DOWNLOADENG] = new ImageIcon("images/common/tnsDownloading.gif");
        icons[STATUS_QUEUED] = new ImageIcon("images/common/tnsQueued.gif");
        icons[STATUS_ERROR] = new ImageIcon("images/common/tnsError.gif");
        openIcon = new ImageIcon("images/common/graph_tb_open.gif");             //"images/Theme" + Theme.getID() + "/menu/classicview.gif"
//        openIcon=new ImageIcon("images/Theme"+ Theme.getID() +"/menu/opentns.gif");             //"images/Theme" + Theme.getID() + "/menu/classicview.gif"
        exportIcon = new ImageIcon("images/Theme" + Theme.getID() + "/menu/exporttrades.gif");
//        exportIcon=new ImageIcon("images/Theme"+ Theme.getID() +"/menu/exporttns.gif");
        initColors();
        Theme.registerComponent(this);
    }

    public void initColors() {
        foreground = Theme.getColor("LIST_FGCOLOR");
        backround = Theme.getColor("LIST_BGCOLOR");
    }

    public boolean isSelected(int index) {
        return ((TradeFileListItem) getModel().getElementAt(index)).isChecked();
    }

    /**
     * Returns an array on indices with checked items in the list
     *
     * @return int[] array of indices with checked items
     */
    public int[] getSelectedIndices() {
        int selectedCount = 0;

        for (int i = 0; i < getModel().getSize(); i++) {
            if (((TradeFileListItem) getModel().getElementAt(i)).isChecked()) {
                selectedCount++;
            }
        }
        int[] selectedIndices = new int[selectedCount];
        int j = 0;
        for (int i = 0; i < getModel().getSize(); i++) {
            if (((TradeFileListItem) getModel().getElementAt(i)).isChecked()) {
                selectedIndices[j] = i;
                j++;
            }
        }
        return selectedIndices;
    }

    /**
     * Returns an array on objects with checked items in the list
     *
     * @return Object[] array of objects with checked items
     */
    public Object[] getSelectedValues() {
        int selectedCount = 0;

        for (int i = 0; i < getModel().getSize(); i++) {
            if (((TradeFileListItem) getModel().getElementAt(i)).isChecked()) {
                selectedCount++;
            }
        }
        Object[] selectedIndices = new Object[selectedCount];
        int j = 0;
        for (int i = 0; i < getModel().getSize(); i++) {
            if (((TradeFileListItem) getModel().getElementAt(i)).isChecked()) {
                selectedIndices[j] = getModel().getElementAt(i);
                j++;
            }
        }
        return selectedIndices;
    }

    public void unselectAll() {
        for (int i = 0; i < getModel().getSize(); i++) {
            ((TradeFileListItem) getModel().getElementAt(i)).setChecked(false);
        }
    }

    public void selectAll() {
        for (int i = 0; i < getModel().getSize(); i++) {
            ((TradeFileListItem) getModel().getElementAt(i)).setChecked(true);
        }
    }

    private boolean isFileSizeOkToOpen(String fileName) {
        File file = new File(fileName);
        if (file.length() > (Constants.OPENABLE_TRADE_FILE_SIZE)) {
            int status = SharedMethods.showConfirmMessage(Language.getString("MSG_LARGE_TRADE_FILE"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
            return (status == JOptionPane.YES_OPTION);
//            return false;
        } else {
            return true;
        }
    }

    private int showExtractPopup() {
        ExtractConfirmDialog confirmDialog = new ExtractConfirmDialog();
        return confirmDialog.showDialog();
    }


    public void mouseClicked(MouseEvent e) {
        try {
            String exchange = TradeBacklogStore.getSharedInstance().getCurrentExchange();
            TradeFileListItem item = ((TradeFileListItem) getModel().getElementAt(getSelectedIndex()));
            CellComponent component = (CellComponent) getCellRenderer().getListCellRendererComponent(this, item, getSelectedIndex(), false, false);
            String archive = Settings.getTradeArchivePath() + "\\" + exchange + "\\" + item.getFileName();

            if ((item.getStatus() != STATUS_DOWNLOADENG) && (item.getStatus() != STATUS_QUEUED)) {
                item.setChecked(component.isCheckCLieked(e.getPoint()) && !item.isChecked());
                if (item.getStatus() != STATUS_NOT_SAVED) {
                    if (component.isOpenClieked(e.getPoint())) {
                        Client.getInstance().showDetailedTrades();
                        DetailedTradeStore.getSharedInstance().setFileItem(item);
                        //commented below segment because of now we are using cache Store which is more efficient in memory handling
                        //Date 2009-06-22
                    /*if (isFileSizeOkToOpen(archive)) { // check if the file is safe to open in memory       //
                        Client.getInstance().showDetailedTrades();
                        DetailedTradeStore.getSharedInstance().setFileItem(item);
                    } else { // no. must export file file
                        int value = showExtractPopup();
                        if ((value == ExtractConfirmDialog.STATUS_EXTRACT) ||
                                (value == ExtractConfirmDialog.STATUS_EXTRACT_OPEN)) {
                            TradeExporter tradeExporter = new TradeExporter(exchange, item.getFileName(), value == ExtractConfirmDialog.STATUS_EXTRACT_OPEN);
                            tradeExporter.start();
                        }
                    }*/
                    } else if (component.isExportClieked(e.getPoint())) {
                        System.out.println("Export Clicked");
                        Client.getInstance().exportAllTrades(null, exchange, item.getFileName());
//                    TradeExporter tradeExporter = new TradeExporter(exchange, item.getFileName(), false);
//                     tradeExporter.start();
                    }
                }
            }
            repaint();
            item = null;
            component = null;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
        try {
            int location = locationToIndex(e.getPoint());
            TradeFileListItem item = ((TradeFileListItem) getModel().getElementAt(location));
            CellComponent component = (CellComponent) getCellRenderer().getListCellRendererComponent(this, item, getSelectedIndex(), false, false);
            if (((component.isOpenClieked(e.getPoint())) || (component.isExportClieked(e.getPoint()))) && (item.getStatus() == TradeFileList.STATUS_SAVED)) {
                setCursor(new Cursor(Cursor.HAND_CURSOR));
                if (component.isOpenClieked(e.getPoint())) {
                    mouseLocation = MOUSE_LOCATION_OPEN;
                } else if (component.isExportClieked(e.getPoint())) {
                    mouseLocation = MOUSE_LOCATION_EXPORT;
                } else {
                    mouseLocation = MOUSE_LOCATION_DEFAULT;
                }
            } else {
                setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                mouseLocation = MOUSE_LOCATION_DEFAULT;
            }
            component = null;
            item = null;
        } catch (Exception e1) {

        }
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
//        openIcon=new ImageIcon("images/Theme"+ Theme.getID() +"/menu/opentns.gif");             //"images/Theme" + Theme.getID() + "/menu/classicview.gif"
//        exportIcon=new ImageIcon("images/Theme"+ Theme.getID() +"/menu/exporttns.gif");
        openIcon = new ImageIcon("images/common/graph_tb_open.gif");             //"images/Theme" + Theme.getID() + "/menu/classicview.gif"
//        openIcon=new ImageIcon("images/Theme"+ Theme.getID() +"/menu/opentns.gif");             //"images/Theme" + Theme.getID() + "/menu/classicview.gif"
        exportIcon = new ImageIcon("images/Theme" + Theme.getID() + "/menu/exporttrades.gif");
    }

    class JMultiColumnListRenderer extends CellComponent implements ListCellRenderer {

        public JMultiColumnListRenderer() {
            super();
            setOpaque(true);
            status.setOpaque(false);

        }

        public Component getListCellRendererComponent(JList list, Object value,
                                                      int index, boolean isSelected, boolean cellHasFocus) {

            TradeFileListItem item = (TradeFileListItem) value;
            descrLabel.setText(item.getDescription());

            int fileSize = item.getSize() / 1024;
            if (fileSize <= 1) {
                size.setText(1 + "k");
            } else {
                size.setText((item.getSize() / 1024) + "k");
            }
            try {
                imageLabel.setIcon(icons[item.getStatus()]);
            } catch (Exception e) {
            }
            imageLabel.setText(item.getProgress());
            status.setSelected(item.isChecked());
            if ((item.getStatus() == STATUS_QUEUED) || (item.getStatus() == STATUS_DOWNLOADENG)) {
                status.setEnabled(false);
            } else {
                status.setEnabled(true);
            }
            showOpen(item.getStatus() == STATUS_SAVED);
            item = null;

            setBackground(backround);
            setForeground(foreground);// list.getForeground());
            setEnabled(list.isEnabled());
            setFont(list.getFont());

            return this;
        }
    }

    class CellComponent extends JPanel {
        JCheckBox status = new JCheckBox();
        JLabel descrLabel;
        JLabel imageLabel;
        JLabel size;
        JLabel open;
        JLabel export;
        String openText;
        String exportText;

        public CellComponent() {
            descrLabel = new JLabel();
            imageLabel = new JLabel();
            open = new JLabel();
            export = new JLabel();
            size = new JLabel();
            JLabel dummy = new JLabel("");
            size.setHorizontalAlignment(JLabel.TRAILING);
            imageLabel.setHorizontalAlignment(JLabel.LEFT);
            openText = Language.getString("OPEN_FILE");
            exportText = "Export";
            open.setBackground(Color.blue);
            export.setBackground(Color.blue);
            open.setToolTipText(Language.getString("OPEN_FOLDER"));
            export.setToolTipText(Language.getString("EXPORT"));
            open.setHorizontalAlignment(JLabel.LEFT);
            export.setHorizontalAlignment(JLabel.LEFT);
            setLayout(new FlowLayout(FlowLayout.LEADING, 10, 2));
            add(status);
            add(descrLabel);
            add(size);
            add(dummy);
            add(imageLabel);
            add(open);
            add(export);
            dummy.setPreferredSize(new Dimension(5, 20));
            descrLabel.setPreferredSize(new Dimension(80, 20));
            size.setPreferredSize(new Dimension(50, 20));
            imageLabel.setPreferredSize(new Dimension(50, 20));
            open.setPreferredSize(new Dimension(21, 20));
            export.setPreferredSize(new Dimension(21, 20));
            GUISettings.applyOrientation(this);
            setToolTipText("alo");
        }

        public boolean isOpenClieked(Point point) {
            // if (Language.isLTR()){
            if ((point.x >= open.getBounds().x) && (point.x <= open.getBounds().x + open.getPreferredSize().getWidth())) {
                return true;
            } else {
                return false;
            }
//            }else {
//                if ((point.x <= open.getBounds().x) &&
//                        (open.getPreferredSize().getWidth() > 0)){
//                    return true;
//                }
//                else{
//                    return false;
//                }
//            }
        }

        public boolean isExportClieked(Point point) {
            //    if (Language.isLTR()){
            if ((point.x >= export.getBounds().x) && (point.x <= export.getBounds().x + export.getPreferredSize().getWidth())) {
                return true;
            } else {
                return false;
            }
//            }else {
//               if((point.x <= export.getBounds().x) &&
//                        (export.getPreferredSize().getWidth() > 0)){
//                     return true;
//                }
//                else{
//                    return false;
//                }
//            }
        }

        public boolean isCheckCLieked(Point point) {
            if ((point.x >= status.getBounds().x) && (point.x <= status.getBounds().x + status.getPreferredSize().getWidth()))
                return true;
            else
                return false;
        }

        public void showOpen(boolean show) {
            if (show) {
                open.setIcon(openIcon);
                export.setIcon(exportIcon);
                open.setToolTipText(Language.getString("OPEN_FOLDER"));
                export.setToolTipText(Language.getString("EXPORT"));
            } else {
                open.setIcon(null);
                export.setIcon(null);
            }
        }

        public void setForeground(Color fg) {
            try {
                descrLabel.setForeground(fg);
                imageLabel.setForeground(fg);
                size.setForeground(fg);
                open.setForeground(fg);
                export.setForeground(fg);
            } catch (Exception e) {

            }
        }

        public String getToolTipText() {
            if (mouseLocation == MOUSE_LOCATION_EXPORT) {
                return Language.getString("TIP_EXPORT_TRADES");
            } else if (mouseLocation == MOUSE_LOCATION_OPEN) {
                return Language.getString("TIP_OPEN_TRADES");
            } else {
                return null;
            }
        }
    }


}
