package com.isi.csvr.tradebacklog;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.util.ColumnLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Oct 6, 2005
 * Time: 10:56:45 AM
 */
public class ExtractConfirmDialog extends JDialog implements ActionListener {

    public static final int STATUS_CANCEL = 0;
    public static final int STATUS_EXTRACT = 1;
    public static final int STATUS_EXTRACT_OPEN = 2;

    private int returnValue;
    private JCheckBox chkExtract;

    public ExtractConfirmDialog() throws HeadlessException {
        super(Client.getInstance().getFrame(), Language.getString("ERROR"), true);
        JPanel panel = new JPanel(new ColumnLayout());

        JLabel lblMessage = new JLabel(Language.getString("FILE_TOO_LARGE_TO_OPEN"));
        panel.add(lblMessage);
        chkExtract = new JCheckBox(Language.getString("OPEN_AFTER_EXTRACT"));
        panel.add(chkExtract);

        TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);
        btnCancel.setActionCommand("CANCEL");
        TWButton btnExtrect = new TWButton(Language.getString("EXPORT_TRADES"));
        btnExtrect.addActionListener(this);
        btnExtrect.setActionCommand("EXTRACT");
        Object[] buttons = {btnExtrect, btnCancel};
        JOptionPane optionPane = new JOptionPane(panel, JOptionPane.WARNING_MESSAGE, 0);
        optionPane.setOptions(buttons);

        add(optionPane);
        GUISettings.applyOrientation(this);
        pack();
        setLocationRelativeTo(Client.getInstance().getFrame());
    }

    public int showDialog() {
        show();
        return returnValue;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("CANCEL")) {
            dispose();
            returnValue = STATUS_CANCEL;
        } else {
            if (chkExtract.isSelected())
                returnValue = STATUS_EXTRACT_OPEN;
            else
                returnValue = STATUS_EXTRACT;
            dispose();
        }
    }

}
