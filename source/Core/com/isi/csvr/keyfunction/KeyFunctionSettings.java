package com.isi.csvr.keyfunction;

import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: May 22, 2008
 * Time: 2:25:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeyFunctionSettings {
    private static Properties prop;
    private static SmartProperties customViews;
    private Hashtable<String, String> customViewSettings = new Hashtable<String, String>();

    //  public synchronized static void save() {
    public void save() {
        try {
            // synchronized(prop)
            System.out.println("in prop");
            FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/keyconfig1.msf");

            String t = "uuu";
            //  oOut.write(t);
            oOut.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void saveCustomViewSettings(Hashtable hash) {
        try {
            FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/keyconfig.msf");
            Enumeration keys = hash.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String workspaceSetting = (String) hash.get(key);
//                customViews.put(key, workspaceSetting);
                oOut.write((key + Meta.RS + workspaceSetting + "\n").getBytes());
            }
//            customViews.store(oOut, "");
            oOut.close();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void loadCustomViewSettings() {

        try {
            FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH + "/keyconfig.msf");
            customViews = new SmartProperties(Meta.RS);
            customViews.load(oIn);
            oIn.close();
            oIn = null;
            Enumeration keys = customViews.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String workspaceSetting = customViews.getProperty(key);
                customViewSettings.put(key, workspaceSetting);

            }
            setCustomViewSettings(customViewSettings);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public Hashtable<String, String> getCustomViewSettings() {
        return customViewSettings;
    }

    public void setCustomViewSettings(Hashtable<String, String> customViewSettings) {
        this.customViewSettings = customViewSettings;
    }
}
