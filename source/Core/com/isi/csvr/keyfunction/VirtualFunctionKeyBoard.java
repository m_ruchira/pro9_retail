package com.isi.csvr.keyfunction;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.shared.TWPasswordField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.virtualkeyboard.jButton;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: May 21, 2008
 * Time: 3:56:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class VirtualFunctionKeyBoard extends JDialog implements MouseListener, KeyListener, ActionListener {

//public class VirtualFunctionKeyBoard extends JDialog {

    String completeText = "";
    KeyFunctionPanel keyPanel;
    boolean isFrameUp = false;
    int checkClose;
    private JPanel virtual;
    private JPanel numPanel;
    private JPanel keyPanel1;
    private JPanel keyPanel2;
    private JPanel keyPanel3;
    private JPanel functionPanel;
    private JPanel keyPanel4;
    private JPanel leftPanel;
    //private JToggleButton caps;
    private JPanel rightPanel;
    private MyButton myb;

//    private String[][] charArray = {{"Q", "q"}, {"W", "w"}, {"E", "e"}, {"R", "r"}, {"T", "t"}, {"Y", "y"}, {"U", "u"}, {"I", "i"}, {"O", "o"}, {"P", "p"}, {"+", "+"}, {"A", "a"}, {"S", "s"}, {"D", "d"}, {"F", "f"}, {"G", "g"}, {"H", "h"}, {"J", "j"}, {"K", "k"}, {"L", "l"}, {"Z", "z"}, {"X", "x"}, {"C", "c"}, {"V", "v"}, {"B", "b"}, {"N", "n"}, {"M", "m"}};
    private JButton close, clear, caps;
    private JButton ctrl, shift, alt, home, end, esc, pageup, space, backspace, pagedown;
    private boolean isCapsClicked = false;
    private String[][] charArray = {{"Q", "q"}, {"W", "w"}, {"E", "e"}, {"R", "r"}, {"T", "t"}, {"Y", "y"}, {"U", "u"}, {"I", "i"}, {"O", "o"}, {"P", "p"}, {"{", "["}, {"}", "]"}, {"A", "a"}, {"S", "s"}, {"D", "d"}, {"F", "f"}, {"G", "g"}, {"H", "h"}, {"J", "j"}, {"K", "k"}, {"L", "l"}, {":", ";"}, {"\"", "\'"}, {"Z", "z"}, {"X", "x"}, {"C", "c"}, {"V", "v"}, {"B", "b"}, {"N", "n"}, {"M", "m"}, {"<", ","}, {">", "."}, {"?", "/"}};
    private String[][] numArray = {{"~", "`"}, {"!", "1"}, {"@", "2"}, {"#", "3"}, {"$", "4"}, {"%", "5"}, {"^", "6"}, {"&", "7"}, {"*", "8"}, {"(", "9"}, {")", "0"}, {"_", "-"}, {"+", "="}, {"|", "\\"}};
    private String[] functionArray = {"F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12"};
    private jButton[] charButton;
    private jButton[] numButton;
    private MyFunctionButton[] functionButton;
    private String str = "";

//    private VirtualKeyboardTextField passwordField;
    private String sThemeID;
    private TWPasswordField passwordField;
    private Container container = getContentPane();

    public VirtualFunctionKeyBoard(MyButton my) {
        myb = my;
    }

    public void createUI() {

        System.out.println("111111111111111111");
        ((JPanel) getContentPane()).setBorder(BorderFactory.createLineBorder(Color.black));
        // this.setTitle("my keyboard");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(500, 230);
        this.setResizable(false);
        this.setFocusable(true);
        // this.addWindowFocusListener((WindowFocusListener)this);
        this.setVisible(true);


        //container.setLayout(new FlowLayout(2, 0, 0));


        GridLayout mainLayout = new GridLayout(0, 2);
        //  GridBagLayout newGrid =

        //pane.setLayout(new GridBagLayout());
        virtual = new JPanel();
        virtual.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();


//        virtual.setLayout(new GridLayout(4, 1, 0, 0));
        rightPanel = new JPanel();
        leftPanel = new JPanel();


        leftPanel.setSize(new Dimension(50, 200));
        leftPanel.add(new JLabel("left panel"));
        rightPanel.setLayout(new GridLayout(4, 1, 0, 0));
        Border eched = BorderFactory.createEtchedBorder(Color.blue, Color.gray);
        Border titled = BorderFactory.createTitledBorder(eched, Language.getString("VIRTUAL_KEYBOARD"));
//        virtual.setBorder(titled);
        rightPanel.setBorder(titled);
        Font font = new TWFont("Arial", Font.BOLD, 16);

        close = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        clear = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        caps = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };


        ctrl = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        shift = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        alt = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        home = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        end = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        esc = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        pageup = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };

        space = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };

        backspace = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };

        pagedown = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };


        clear.setPreferredSize(new Dimension(60, 20));
        close.setPreferredSize(new Dimension(60, 20));
        caps.setPreferredSize(new Dimension(60, 20));
        clear.setFont(font);
        clear.setText(Language.getString("CLEAR"));
        close.setFont(font);
        close.setText(Language.getString("CLOSE"));
        caps.setFont(font);

        ctrl.setPreferredSize(new Dimension(60, 20));
        ctrl.setFont(font);
        ctrl.setText(Language.getString("CTRL"));

        shift.setPreferredSize(new Dimension(60, 20));
        shift.setFont(font);
        shift.setText(Language.getString("SHIFT"));

        alt.setPreferredSize(new Dimension(60, 20));
        alt.setFont(font);
        alt.setText(Language.getString("ALT"));


        home.setPreferredSize(new Dimension(60, 20));
        home.setFont(font);
        home.setText(Language.getString("HOME"));

        end.setPreferredSize(new Dimension(60, 20));
        end.setFont(font);
        end.setText(Language.getString("END"));

        esc.setPreferredSize(new Dimension(60, 20));
        esc.setFont(font);
        esc.setText(Language.getString("ESC"));

        pageup.setPreferredSize(new Dimension(60, 20));
        pageup.setFont(font);
        pageup.setText(Language.getString("PAGE_UP"));

        space.setPreferredSize(new Dimension(60, 20));
        space.setFont(font);
        space.setText(Language.getString("SPACE"));

        backspace.setPreferredSize(new Dimension(60, 20));
        backspace.setFont(font);
        backspace.setText(Language.getString("BCKSPACE"));

        pagedown.setPreferredSize(new Dimension(60, 20));
        pagedown.setFont(font);
        pagedown.setText(Language.getString("PAGE_DOWN"));


        //caps.setIcon();


        close.addActionListener(this);
        clear.addActionListener(this);
        caps.addActionListener(this);

        charButton = new jButton[33];
        for (int i = 0; i < charButton.length; i++) {
            charButton[i] = new jButton(charArray[i][1]);
            charButton[i].addActionListener(this);
        }

        numButton = new jButton[14];
        for (int i = 0; i < numButton.length; i++) {
            numButton[i] = new jButton(numArray[i][1]);
            numButton[i].addActionListener(this);
        }
        functionButton = new MyFunctionButton[12];
        for (int i = 0; i < functionButton.length; i++) {
            functionButton[i] = new MyFunctionButton(functionArray[i]);
            functionButton[i].addActionListener(this);
        }


        caps.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/caps_up.gif"));
        numPanel = new JPanel();
        for (int i = 0; i < numButton.length; i++) {
            numPanel.add(numButton[i]);
        }

        keyPanel1 = new JPanel();
        keyPanel1.add(esc);
        for (int i = 0; i < 12; i++) {
            keyPanel1.add(charButton[i]);
        }
        //  keyPanel1.add(home);

        keyPanel2 = new JPanel();
        keyPanel2.add(caps);
        for (int i = 0; i < 11; i++) {
            keyPanel2.add(charButton[i + 12]);
        }

        // keyPanel2.add(pageup);

        keyPanel3 = new JPanel();
        keyPanel3.add(clear);
        for (int i = 0; i < 10; i++) {
            keyPanel3.add(charButton[i + 23]);
        }
        //  keyPanel3.add(pagedown);
        //  keyPanel3.add(close);

        keyPanel4 = new JPanel();
        keyPanel4.add(ctrl);
        keyPanel4.add(alt);
        keyPanel4.add(space);
        keyPanel4.add(shift);
        keyPanel4.add(end);
        keyPanel4.add(shift);


        functionPanel = new JPanel();
        for (int i = 0; i < functionButton.length; i++) {
            functionPanel.add(functionButton[i]);
        }


//        virtual.add(numPanel, BorderLayout.NORTH);
//        virtual.add(functionPanel, BorderLayout.NORTH);
//
//        virtual.add(keyPanel1, BorderLayout.NORTH);
//        virtual.add(keyPanel2, BorderLayout.NORTH);
//        virtual.add(keyPanel3, BorderLayout.NORTH);
        //virtual.add(keyPanel4, BorderLayout.NORTH);
        rightPanel.add(functionPanel, BorderLayout.NORTH);

        rightPanel.add(keyPanel1, BorderLayout.NORTH);
        rightPanel.add(keyPanel2, BorderLayout.NORTH);
        rightPanel.add(keyPanel3, BorderLayout.NORTH);

//        virtual.add(leftPanel, BorderLayout.WEST );
//        virtual.add(rightPanel, BorderLayout.EAST );

        //   virtual.addKeyListener(this);
        this.addKeyListener(this);
        this.addMouseListener(this);


        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 2;
        c.gridx = 0;
        c.gridy = 0;
        virtual.add(leftPanel, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 2;
        c.gridx = 1;
        c.gridy = 0;
        virtual.add(rightPanel, c);

        container.add(virtual, BorderLayout.NORTH);


    }


    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        System.out.println("key pressed");
        System.out.println(e.getKeyChar());
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public void actionPerformed(ActionEvent ex) {

        Object obj = ex.getSource();
        if (obj == close) {
            this.setVisible(false);
        } else if (obj == clear) {
            str = "";
        } else if (obj == caps) {
            if (isCapsClicked) {
                isCapsClicked = false;
                setKeyboard();
            } else {
                isCapsClicked = true;
                setKeyboard();
            }
        } else {
            JButton but = (JButton) obj;
            //editText(but.getText());
            System.out.println("))))))))))))))))))))))))))))" + but.getText());
//            String completeText = but.getText();
//            myb.setText(completeText+"+"+but.getText());
            makingText(but.getText());

        }

    }

    public void makingText(String passString) {

        myb.setText(completeText + passString);
        completeText = completeText + passString;
//        keyPanel.setData();
//        keyPanel.setData(myb.getName(),completeText);
        // keyPanel.setData("first",completeText);


    }

    private void setKeyboard() {
        if (isCapsClicked) {
            for (int i = 0; i < charButton.length; i++) {
                charButton[i].setText(charArray[i][0]);
            }
            for (int i = 0; i < numButton.length; i++) {
                numButton[i].setText(numArray[i][0]);
            }
            caps.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/caps_down.gif"));
        } else {
            for (int i = 0; i < charButton.length; i++) {
                charButton[i].setText(charArray[i][1]);
            }
            for (int i = 0; i < numButton.length; i++) {
                numButton[i].setText(numArray[i][1]);
            }
            caps.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/caps_up.gif"));
        }
    }

}
