package com.isi.csvr.keyfunction;

import com.isi.csvr.TWDesktop;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.NonTabbable;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: May 20, 2008
 * Time: 4:19:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeyFunctionUI extends InternalFrame
        implements ActionListener, MouseListener,
        ItemListener,
        KeyListener, Themeable, InternalFrameListener {

    ViewSetting settings = null;
    JLayeredPane oLayer;
    boolean isModel = false;
    GridLayout mainLayout = new GridLayout(0, 1);
    GridBagLayout gridLayout = new GridBagLayout();
    String[] keys = {"tesr", "ryur", "fjjfj", "fjhfjfk", "fsgdg", "gdfg", "fgfd", "gdfgf", "gfdgdf", "gdfg", "rwer", "rer", "rer"};
    JList list;
    JScrollPane scrollpane;
    KeyFunctionPanel keyPanel;
    private JPanel symbolsPanel = new JPanel();
    private JPanel mainPanel = new JPanel();
    private JTextField lblSelectedSymbols;


    public KeyFunctionUI(JFrame parent, boolean modal) {
        settings = new ViewSetting();

        try {
            //   jbInit();
            //pack();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //this.setSize(450,400);
        pack();
        createUI();

    }

    private void jbInit() throws Exception {
        mainPanel.setLayout(mainLayout);
        mainLayout.setRows(9);
        mainLayout.setColumns(2);
        mainLayout.setVgap(3);

        mainPanel.setOpaque(true);
        mainPanel.setForeground(Color.red);
        //getContentPane().add(mainPanel);

    }


    private void createUI() {

        System.out.println(" create ui called---------");
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");

        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        setTitle(Language.getString("KEY_FUNCTIONS"));

        oLayer = new JLayeredPane();
//        oLayer.setPreferredSize(new Dimension(420, 305));
        oLayer.setPreferredSize(new Dimension(375, 305));

        oLayer.setBorder(BorderFactory.createLineBorder(Color.black));
        oLayer.add(mainPanel, new Integer(0));
//        mainPanel.setBounds(3, 3, 415, 300);
        mainPanel.setBounds(3, 3, 370, 300);

        oLayer.setOpaque(true);
        oLayer.addMouseListener(this);

        getContentPane().add(oLayer);

        setContentPane(new KeyFunctionPanel());

   /*


        JPanel panel1 = new JPanel();

        panel1.setLayout(new FlowLayout());
        JLabel lblSectSymbols = new JLabel(Language.getString("FUNCTIONS"));
      //  lblSectSymbols.setPreferredSize(new Dimension(200, 25));
        lblSectSymbols.setSize(200,200);
        panel1.add(lblSectSymbols);

        JLabel lblKeySymbols = new JLabel(Language.getString("KEYS"));
        //lblSectSymbols.setPreferredSize(new Dimension(200, 25));
        //panel1.add(lblSectSymbols);
        lblKeySymbols.setSize(200,200);
        panel1.add(lblKeySymbols);




       mainPanel.add(panel1);
        //getContentPane().setLayout(new GridBagLayout());

        JPanel panel = new JPanel();


        list = new JList(keys);

        scrollpane = new JScrollPane(list);




        mainPanel.add(scrollpane);
//        topPanel.add(scrollpane);
        Theme.registerComponent(this);
        this.addInternalFrameListener(this);


       // mainPanel.add(g_oProgressPanel);
     //   selectFullRange();
        if (!Language.isLTR())
            GUISettings.applyOrientation(mainPanel, ComponentOrientation.RIGHT_TO_LEFT);
   */
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //this.dispose();
        setOrientation();
//        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setResizable(false);
        this.setSize(500, 400);
        this.setClosable(true);
    }


    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void itemStateChanged(ItemEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setLocationRelativeTo(Component c) {
        Dimension parentSize = c.getSize();

        if (Settings.isDualScreenMode() && (Settings.isMultiScreenEnvr())) {
            if (Settings.isLeftAlignPopups()) {
                this.setBounds((int) ((parentSize.getWidth() / 2 - this.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                        this.getWidth(), this.getHeight());
            } else {
                this.setBounds((int) ((parentSize.getWidth() / 2) + (parentSize.getWidth() / 2 - this.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                        this.getWidth(), this.getHeight());
            }
        } else {


            this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
                    (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                    this.getWidth(), this.getHeight());
        }


//        this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
//                (int) ((parentSize.getHeight() - this.getHeight()) / 2),
//                this.getWidth(), this.getHeight());
    }


    public void setVisible(boolean value) {

        super.setVisible(value);
        if (isModel) {
            if (value) {
                System.out.println("model started");
                //    startModal();
            } else {
                System.out.println("Model stopped");
                // stopModal();
            }
        }

        if (!TWDesktop.isInTabbedPanel(this)) {
            if (value) {
                TWDesktop.addWindowTab(this);
            }
        }
        if (TWDesktop.isInTabbedPanel(this)) {
            if (!value) {
                TWDesktop.removeWindowtab(this);
            }
        }

        if (!TWDesktop.checkInternalFrameAvailability(this)) {
            if (value) {
                TWDesktop.removeWindowtab(this);
            }
        }
        if (TWDesktop.checkInternalFrameAvailability(this) && !TWDesktop.isInTabbedPanel(this)) {
            if (value) {
                TWDesktop.addWindowTab(this);
            }
        }

        if (value) {
            ensureVisibility();
        }
    }

    public void internalFrameClosing(InternalFrameEvent e) {
//        keyPanel = new KeyFunctionPanel("ttuti");
//        keyPanel.setData();
//        System.out.println(":::::::::::::::::::::::::");

    }

    public void ensureVisibility() {
        try {
            if (this instanceof NonTabbable) return;

            Dimension parentSize = this.getDesktopPane().getSize();
            Dimension mySize = this.getSize();
            Dimension myNewSize = this.getSize();

            Point myLocation = this.getLocation();
            Point myNewLocation = this.getLocation();

            boolean resized = false;
            boolean repositioned = false;

            if (mySize.height > parentSize.height) {
                myNewSize.height = (int) (parentSize.getHeight() * .75);
                resized = true;
            } else {
                myNewSize.height = mySize.height;
            }

            if (mySize.width > parentSize.width) {
                myNewSize.width = (int) (parentSize.getWidth() * .75);
                resized = true;
            } else {
                myNewSize.width = mySize.width;
            }
            if (resized) {
                this.setSize(myNewSize);
                this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                        this.getWidth(), this.getHeight());
            }

            if ((myLocation.y < 0) || (myLocation.y > parentSize.height - 50)) {
                myNewLocation.y = 0;
                repositioned = true;
            } else {
                myNewLocation.y = myLocation.y;
            }

            if ((myLocation.x < 0) || (myLocation.x > parentSize.width - 50)) {
                myNewLocation.x = 0;
                repositioned = true;
            } else {
                myNewLocation.x = myLocation.x;
            }
            if (repositioned) {
                this.setLocation(myNewLocation);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
