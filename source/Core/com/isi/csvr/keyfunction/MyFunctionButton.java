package com.isi.csvr.keyfunction;

import com.isi.csvr.shared.TWFont;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: May 23, 2008
 * Time: 2:35:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyFunctionButton extends JButton {

    public MyFunctionButton() {
        setButton();
    }

    public MyFunctionButton(String sr) {
        setButton();
        this.setText(sr);
    }

    public void setButton() {
        this.setPreferredSize(new Dimension(30, 25));
        Font font = new TWFont("Arial", Font.BOLD, 14);
        this.setFont(font);
    }

    public Insets getInsets() {
        return new Insets(1, 1, 1, 1);
    }
}

