package com.isi.csvr.customizer;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.TableThemeSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TableCustomizerDialog extends JDialog implements ActionListener {
    private static TableCustomizerDialog self = null;
    private JPanel panel1 = new JPanel();
    private BorderLayout borderLayout1 = new BorderLayout();
    private ColorConfigPanel colorConfigPanel;

    private TableCustomizerDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        try {
            jbInit();
            pack();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private TableCustomizerDialog() {
        this(Client.getInstance().getFrame(), Language.getString("CUSTOMIZER"), true);
        //colorConfigPanel.setTarget(target);
        setLocationRelativeTo(Client.getInstance().getFrame());
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        colorConfigPanel.registerKeyboardAction(this, "ESC", KeyStroke.getKeyStroke("released ESCAPE"), JComponent.WHEN_IN_FOCUSED_WINDOW);//KeyEvent.VK_ESCAPE,0
    }

    public static TableCustomizerDialog getSharedInstance() {
        if (self == null) {
            self = new TableCustomizerDialog();
        }
        return self;
    }

    public void show(TableThemeSettings target) {
        colorConfigPanel.setTarget(target);
        colorConfigPanel.applyTheme();
        show();
    }

    public Font getBodyFont() {
        return colorConfigPanel.getBodyFont();
    }

    public int getBodyGap() {
        return colorConfigPanel.getBodyGap();
    }

    public Font getHeadingFont() {
        return colorConfigPanel.getHeadingFont();
    }

    private void jbInit() throws Exception {
        panel1.setLayout(new FlowLayout());
        colorConfigPanel = new ColorConfigPanel(this);
        getContentPane().add(colorConfigPanel);
        setResizable(false);
    }

    public void actionPerformed(ActionEvent e) {
        setVisible(false);
    }

}