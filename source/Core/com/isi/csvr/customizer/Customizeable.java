package com.isi.csvr.customizer;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 24, 2003
 * Time: 8:53:54 AM
 * To change this template use Options | File Templates.
 */
public interface Customizeable {
    public CustomizerRecord[] getCustomizerRecords();
}
