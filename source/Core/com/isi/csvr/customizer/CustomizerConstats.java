package com.isi.csvr.customizer;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 24, 2003
 * Time: 9:50:06 AM
 * To change this template use Options | File Templates.
 */
public interface CustomizerConstats {
    public static final byte FIELD_BASIC_ROW1 = 0;
    public static final byte FIELD_BASIC_ROW2 = 8;
    public static final byte FIELD_SELECTED_ROW = 1;
    public static final byte FIELD_HEADER_COLOR_ROW = 2;
    public static final byte FIELD_VALUE_UP_ROW = 3;
    public static final byte FIELD_VALUE_DOWN_ROW = 4;
    public static final byte FIELD_POSITIVE_CHANGE_ROW = 5;
    public static final byte FIELD_NEGATIVE_CHANGE_ROW = 6;
    public static final byte FIELD_SMALLTRADE_ROW = 7;
    public static final byte FIELD_BID_ROW1 = 9;
    public static final byte FIELD_BID_ROW2 = 10;
    public static final byte FIELD_ASK_ROW1 = 11;
    public static final byte FIELD_ASK_ROW2 = 12;
    public static final byte FIELD_BUY = 13;
    public static final byte FIELD_SELL = 14;
    public static final byte FIELD_OPBAL = 15;
    public static final byte FIELD_DIVIDAND = 16;
    public static final byte FIELD_NEW_ANNOUNCEMENT = 17;
    public static final byte FIELD_DEPOSITE_ROW1 = 18;
    public static final byte FIELD_DEPOSITE_ROW2 = 19;
    public static final byte FIELD_WITHDRAW_ROW1 = 20;
    public static final byte FIELD_WITHDRAW_ROW2 = 21;
    public static final byte FIELD_COMBINED_BID_ROW1 = 22;
    public static final byte FIELD_COMBINED_BID_ROW2 = 23;
    public static final byte FIELD_COMBINED_ASK_ROW1 = 24;
    public static final byte FIELD_COMBINED_ASK_ROW2 = 25;
    public static final byte FIELD_COMBINED_PRICE_ROW1 = 26;
    public static final byte FIELD_COMBINED_PRICE_ROW2 = 27;
    public static final byte FIELD_COMBINED_SELECTED_ROW = 28;
    public static final byte FIELD_TIMENSALES_ROW1 = 29;
    public static final byte FIELD_TIMENSALES_ROW2 = 30;
    public static final byte FIELD_TIMENSALES_SELECTED_ROW = 31;
    public static final byte FIELD_DEP_BY_ORD_BID_ROW1 = 32;
    public static final byte FIELD_DEP_BY_ORD_BID_ROW2 = 33;
    public static final byte FIELD_DEP_BY_ORD_ASK_ROW1 = 34;
    public static final byte FIELD_DEP_BY_ORD_ASK_ROW2 = 35;
    public static final byte FIELD_DEP_BY_ORD_SELECTED_ROW = 36;
    public static final byte FIELD_CONDITIONAL_BUY = 37;
    public static final byte FIELD_CONDITIONAL_SELL = 38;
    public static final byte FIELD_DEPOSITE = 39;
    public static final byte FIELD_WITHDRAW = 40;
    public static final byte FIELD_CHARGES = 41;
    public static final byte FIELD_REFUNDS = 42;
    public static final byte FIELD_NEWS_HOT_NEWS = 43;
    public static final byte FIELD_NEWS_TEMPORARY_NEWS = 44;
    public static final byte FIELD_OPTION_CHAIN_STRIKE_ROW1 = 45;
    public static final byte FIELD_OPTION_CHAIN_STRIKE_ROW2 = 46;
    public static final byte FIELD_OPTION_CHAIN_PUTS_ROW1 = 47;
    public static final byte FIELD_OPTION_CHAIN_PUTS_ROW2 = 48;
    public static final byte FIELD_OPTION_CHAIN_CALLS_ROW1 = 49;
    public static final byte FIELD_OPTION_CHAIN_CALLS_ROW2 = 50;
    public static final byte FIELD_PFDETAILS_ROW = 51;
}
