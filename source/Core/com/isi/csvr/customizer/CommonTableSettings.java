package com.isi.csvr.customizer;

import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.TableSettings;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class CommonTableSettings implements TableSettings {

    public static final byte NORMAL_TABLE = 0;
    public static final byte PORTFOLIO_TRANS_HISTORY_TABLE = 1;

    private Color rowColor1FG;
    private Color rowColor1BG;
    private Color rowColor2FG;
    private Color rowColor2BG;
    private Color bidColor1FG;
    private Color bidColor1BG;
    private Color bidColor2FG;
    private Color bidColor2BG;
    private Color askColor1FG;
    private Color askColor1BG;
    private Color askColor2FG;
    private Color askColor2BG;

    private Color gridColor;
    private Color selectedColumnFG;
    private Color selectedColumnBG;
    private Color cellHighLightedDownFG;
    private Color cellHighLightedDownBG;
    private Color cellHighLightedUpFG;
    private Color cellHighLightedUpBG;
    private Color positiveChangeFG;
    private Color negativeChangeFG;
    private Color headerColorBG;
    private Color headerColorFG;

    private Color buyColorFG;
    private Color buyColorBG;
    private Color sellColorFG;
    private Color sellColorBG;
    private Color conditionBuyColorFG;
    private Color conditionBuyColorBG;
    private Color conditionSellColorFG;
    private Color conditionSellColorBG;
    private Color opBalColorFG;
    private Color opBalColorBG;
    private Color divColorFG;
    private Color divColorBG;
    private Color smallTradeFG;
    private Color newAnnouncementFG;
    private Color deposColorFG;
    private Color deposColorBG;
    private Color withdraColorFG;
    private Color withdraColorBG;
    private Color chargesColorFG;
    private Color chargesColorBG;
    private Color refundColorFG;
    private Color refundColorBG;

    private Color depositeColor1FG;
    private Color depositeColor1BG;
    private Color depositeColor2FG;
    private Color depositeColor2BG;
    private Color withdrawColor1FG;
    private Color withdrawColor1BG;
    private Color withdrawColor2FG;
    private Color withdrawColor2BG;

    private Color combinedBidColor2FG;
    private Color combinedBidColor2BG;
    private Color combinedBidColor1FG;
    private Color combinedBidColor1BG;
    private Color combinedAskColor1FG;
    private Color combinedAskColor1BG;
    private Color combinedAskColor2FG;
    private Color combinedAskColor2BG;
    private Color combinedSelectedColorFG;
    private Color combinedSelectedColorBG;

    private Color combinedPriceColor1FG;
    private Color combinedPriceColor1BG;
    private Color combinedPriceColor2FG;
    private Color combinedPriceColor2BG;

    private Color timeNSalesRowColor1FG;
    private Color timeNSalesRowColor2FG;
    private Color timeNSalesRowColor1BG;
    private Color timeNSalesRowColor2BG;
    private Color timeNSalesSelectedColorFG;
    private Color timeNSalesSelectedColorBG;

    private Color depthOrderBidColor2FG;
    private Color depthOrderBidColor2BG;
    private Color depthOrderBidColor1FG;
    private Color depthOrderBidColor1BG;
    private Color depthOrderAskColor1FG;
    private Color depthOrderAskColor1BG;
    private Color depthOrderAskColor2FG;
    private Color depthOrderAskColor2BG;
    private Color depthOrderSelectedColorFG;
    private Color depthOrderSelectedColorBG;

    private Color pfDetailColorFG;
    private Color pfDetailColorBG;

    private Color newsHotNewsColorFG;
    private Color newsTemporaryNewsColorBG;

    private Color optionChainStrikeColor2FG;
    private Color optionChainStrikeColor2BG;
    private Color optionChainStrikeColor1FG;
    private Color optionChainStrikeColor1BG;
    private Color optionChainPutColor1FG;
    private Color optionChainPutColor1BG;
    private Color optionChainPutColor2FG;
    private Color optionChainPutColor2BG;
    private Color optionChainCallColor2FG;
    private Color optionChainCallColor2BG;
    private Color optionChainCallColor1FG;
    private Color optionChainCallColor1BG;

    private byte tableType;
    private int gridType;
    private Border cellBorder;

    private Font bodyFont;
    private Font headingFont;
    private int bodyGap = 6;

    private boolean LTR = true;

    private boolean isCustomThemeEnabled = false;

    public CommonTableSettings(boolean LTR) {
        tableType = NORMAL_TABLE;
        this.LTR = LTR;
    }

    public void init() {
        gridColor = null;
        selectedColumnBG = null;
        selectedColumnFG = null;

        cellHighLightedDownFG = null;
        cellHighLightedDownBG = null;

        cellHighLightedUpFG = null;
        cellHighLightedUpBG = null;

        rowColor1FG = null;
        rowColor1BG = null;
        rowColor2FG = null;
        rowColor2BG = null;

        bidColor1FG = null;
        bidColor1BG = null;
        bidColor2FG = null;
        bidColor2BG = null;
        askColor1FG = null;
        askColor1BG = null;
        askColor2FG = null;

        depositeColor1FG = null;
        depositeColor1BG = null;
        depositeColor2FG = null;
        depositeColor2BG = null;
        withdrawColor1FG = null;
        withdrawColor1BG = null;
        withdrawColor2FG = null;
        withdrawColor2BG = null;

        positiveChangeFG = null;
        negativeChangeFG = null;

        headerColorBG = null;
        headerColorFG = null;
        newAnnouncementFG = null;

        smallTradeFG = null;

        combinedBidColor1FG = null;
        combinedBidColor1BG = null;
        combinedBidColor2FG = null;
        combinedBidColor2BG = null;
        combinedAskColor1FG = null;
        combinedAskColor1BG = null;
        combinedAskColor2FG = null;
        combinedAskColor2BG = null;
        combinedSelectedColorFG = null;
        combinedSelectedColorBG = null;

        combinedPriceColor1FG = null;
        combinedPriceColor1BG = null;
        combinedPriceColor2FG = null;
        combinedPriceColor2BG = null;

        timeNSalesRowColor1FG = null;
        timeNSalesRowColor2FG = null;
        timeNSalesRowColor1BG = null;
        timeNSalesRowColor2BG = null;
        timeNSalesSelectedColorFG = null;
        timeNSalesSelectedColorBG = null;

        depthOrderBidColor2FG = null;
        depthOrderBidColor2BG = null;
        depthOrderBidColor1FG = null;
        depthOrderBidColor1BG = null;
        depthOrderAskColor1FG = null;
        depthOrderAskColor1BG = null;
        depthOrderAskColor2FG = null;
        depthOrderAskColor2BG = null;
        depthOrderSelectedColorFG = null;
        depthOrderSelectedColorBG = null;

        conditionBuyColorFG = null;
        conditionBuyColorBG = null;
        conditionSellColorFG = null;
        conditionSellColorBG = null;
    }

    public void setTransactionHistoryTableType() {
        tableType = PORTFOLIO_TRANS_HISTORY_TABLE;

        buyColorFG = Theme.getColor("PORTFOLIO_TABLE_CELL_BUY_FGCOLOR");
        buyColorBG = Theme.getColor("PORTFOLIO_TABLE_CELL_BUY_BGCOLOR");
        sellColorFG = Theme.getColor("PORTFOLIO_TABLE_CELL_SELL_FGCOLOR");
        sellColorBG = Theme.getColor("PORTFOLIO_TABLE_CELL_SELL_BGCOLOR");
        opBalColorFG = Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_FGCOLOR");
        opBalColorBG = Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR");
        divColorFG = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
        divColorBG = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
        deposColorFG = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
        deposColorBG = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
        withdraColorFG = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
        withdraColorBG = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
        chargesColorFG = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
        chargesColorBG = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
        refundColorFG = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
        refundColorBG = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
    }

    public Font getBodyFont() {
        if (bodyFont != null)
            return bodyFont;
        else {
            UIDefaults oDefaults = UIManager.getDefaults();
            return oDefaults.getFont("Table.font");
        }
    }

    public void setBodyFont(Font font) {
        if (font != null) {
            bodyFont = font;
        }
    }

    public Font getHeadingFont() {
        if (headingFont == null) {
            UIDefaults oDefaults = UIManager.getDefaults();
            return oDefaults.getFont("TableHeader.font");
        } else {
            return headingFont;
        }
    }

    public void setHeadingFont(Font font) {
        headingFont = font;
    }

    public Color getNewAnnouncementFG() {
        return newAnnouncementFG;
    }

    public void setNewAnnouncementFG(Color newAnnouncementFG) {
        this.newAnnouncementFG = newAnnouncementFG;
    }


    public Color getSelectedColumnBG() {
        return selectedColumnBG;
    }

    public void setSelectedColumnBG(Color selectedColumnBG) {
        this.selectedColumnBG = selectedColumnBG;
    }

    public Color getSelectedColumnFG() {
        return selectedColumnFG;
    }

    public void setSelectedColumnFG(Color selectedColumnFG) {
        this.selectedColumnFG = selectedColumnFG;
    }

    public Color getGridColor() {
        if (gridColor == null)
            return Theme.getColor("BOARD_TABLE_GRIDCOLOR");
        else
            return gridColor;
    }

    public void setGridColor(Color gridColor) {
        this.gridColor = gridColor;
        setGridType(gridType); // recreate the cell border
    }

    public Color getCellHighLightedDownBG() {
        return cellHighLightedDownBG;
    }

    public void setCellHighLightedDownBG(Color cellHighLightedDownBG) {
        this.cellHighLightedDownBG = cellHighLightedDownBG;
    }

    public Color getCellHighLightedDownFG() {
        return cellHighLightedDownFG;
    }

    public void setCellHighLightedDownFG(Color cellHighLightedDownFG) {
        this.cellHighLightedDownFG = cellHighLightedDownFG;
    }

    public Color getCellHighLightedUpBG() {
        return cellHighLightedUpBG;
    }

    public void setCellHighLightedUpBG(Color cellHighLightedUpBG) {
        this.cellHighLightedUpBG = cellHighLightedUpBG;
    }

    public Color getCellHighLightedUpFG() {
        return cellHighLightedUpFG;
    }

    public void setCellHighLightedUpFG(Color cellHighLightedUpFG) {
        this.cellHighLightedUpFG = cellHighLightedUpFG;
    }

    public Color getNegativeChangeFG() {
        return negativeChangeFG;
    }

    public void setNegativeChangeFG(Color negativeChangeFG) {
        this.negativeChangeFG = negativeChangeFG;
    }

    public Color getPositiveChangeFG() {
        return positiveChangeFG;
    }

    public void setPositiveChangeFG(Color positiveChangeFG) {
        this.positiveChangeFG = positiveChangeFG;
    }

    public Color getRowColor1BG() {
        return rowColor1BG;
    }

    public void setRowColor1BG(Color rowColor1BG) {
        this.rowColor1BG = rowColor1BG;
    }

    public Color getRowColor1FG() {
        return rowColor1FG;
    }

    public void setRowColor1FG(Color rowColor1FG) {
        this.rowColor1FG = rowColor1FG;
    }

    public Color getRowColor2BG() {
        return rowColor2BG;
    }

    public void setRowColor2BG(Color rowColor2BG) {
        this.rowColor2BG = rowColor2BG;
    }

    public Color getRowColor2FG() {
        return rowColor2FG;
    }

    public void setRowColor2FG(Color rowColor2FG) {
        this.rowColor2FG = rowColor2FG;
    }

    public Color getHeaderColorBG() {
        return headerColorBG;
    }

    public void setHeaderColorBG(Color headerColorBG) {
        this.headerColorBG = headerColorBG;
    }

    public Color getHeaderColorFG() {
        return headerColorFG;
    }

    public void setHeaderColorFG(Color headerColorFG) {
        this.headerColorFG = headerColorFG;
    }

    public Color getBuyColorBG() {
        return buyColorBG;
    }

    public void setBuyColorBG(Color buyColorBG) {
        this.buyColorBG = buyColorBG;
    }

    public Color getBuyColorFG() {
        return buyColorFG;
    }

    public void setBuyColorFG(Color buyColorFG) {
        this.buyColorFG = buyColorFG;
    }

    public Color getDivColorBG() {
        return divColorBG;
    }

    public void setDivColorBG(Color divColorBG) {
        this.divColorBG = divColorBG;
    }

    public Color getDivColorFG() {
        return divColorFG;
    }

    public void setDivColorFG(Color divColorFG) {
        this.divColorFG = divColorFG;
    }

    public Color getOpBalColorBG() {
        return opBalColorBG;
    }

    public void setOpBalColorBG(Color opBalColorBG) {
        this.opBalColorBG = opBalColorBG;
    }

    public Color getOpBalColorFG() {
        return opBalColorFG;
    }

    public void setOpBalColorFG(Color opBalColorFG) {
        this.opBalColorFG = opBalColorFG;
    }

    public Color getSellColorBG() {
        return sellColorBG;
    }

    public void setSellColorBG(Color sellColorBG) {
        this.sellColorBG = sellColorBG;
    }

    public Color getSellColorFG() {
        return sellColorFG;
    }

    public void setSellColorFG(Color sellColorFG) {
        this.sellColorFG = sellColorFG;
    }

    public byte getTableType() {
        return tableType;
    }

    public int getGridType() {
        return gridType;
    }

    public void setGridType(int gridType) {
        this.gridType = gridType;

        switch (gridType) {
            case TableThemeSettings.GRID_NONE:
                cellBorder = null;
                break;
            case TableThemeSettings.GRID_HORIZONTAL:
                cellBorder = BorderFactory.createMatteBorder(0, 0, 1, 0, gridColor);
                break;
            case TableThemeSettings.GRID_VERTICAL:
                if (LTR) {
                    cellBorder = BorderFactory.createMatteBorder(0, 0, 0, 1, gridColor);
                } else {
                    cellBorder = BorderFactory.createMatteBorder(0, 1, 0, 0, gridColor);
                }
                break;
            case TableThemeSettings.GRID_BOTH:
                if (LTR) {
                    cellBorder = BorderFactory.createMatteBorder(0, 0, 1, 1, gridColor);
                } else {
                    cellBorder = BorderFactory.createMatteBorder(0, 1, 1, 0, gridColor);
                }
                break;
        }
    }

    public boolean isGridOn() {
        return gridType > TableThemeSettings.GRID_NONE;
    }

    public Border getCellBorder() {
        return cellBorder;
    }

    public Color getSmallTradeFG() {
        return smallTradeFG;
    }

    public void setSmallTradeFG(Color smallTradeFG) {
        this.smallTradeFG = smallTradeFG;
    }

    public Color getAskColor1BG() {
        return askColor1BG;
    }

    public void setAskColor1BG(Color askColor1BG) {
        this.askColor1BG = askColor1BG;
    }

    public Color getAskColor1FG() {
        return askColor1FG;
    }

    public void setAskColor1FG(Color askColor1FG) {
        this.askColor1FG = askColor1FG;
    }

    public Color getAskColor2BG() {
        return askColor2BG;
    }

    public void setAskColor2BG(Color askColor2BG) {
        this.askColor2BG = askColor2BG;
    }

    public Color getAskColor2FG() {
        return askColor2FG;
    }

    public void setAskColor2FG(Color askColor2FG) {
        this.askColor2FG = askColor2FG;
    }

    public Color getBidColor1BG() {
        return bidColor1BG;
    }

    public void setBidColor1BG(Color bidColor1BG) {
        this.bidColor1BG = bidColor1BG;
    }

    public Color getBidColor1FG() {
        return bidColor1FG;
    }

    public void setBidColor1FG(Color bidColor1FG) {
        this.bidColor1FG = bidColor1FG;
    }

    public Color getBidColor2BG() {
        return bidColor2BG;
    }

    public void setBidColor2BG(Color bidColor2BG) {
        this.bidColor2BG = bidColor2BG;
    }

    public Color getBidColor2FG() {
        return bidColor2FG;
    }

    public void setBidColor2FG(Color bidColor2FG) {
        this.bidColor2FG = bidColor2FG;
    }

    public Color getDepositeColor1FG() {
        return depositeColor1FG;
    }

    public void setDepositeColor1FG(Color depositeColor1FG) {
        this.depositeColor1FG = depositeColor1FG;
    }

    public Color getDepositeColor1BG() {
        return depositeColor1BG;
    }

    public void setDepositeColor1BG(Color depositeColor1BG) {
        this.depositeColor1BG = depositeColor1BG;
    }

    public Color getDepositeColor2FG() {
        return depositeColor2FG;
    }

    public void setDepositeColor2FG(Color depositeColor2FG) {
        this.depositeColor2FG = depositeColor2FG;
    }

    public Color getDepositeColor2BG() {
        return depositeColor2BG;
    }

    public void setDepositeColor2BG(Color depositeColor2BG) {
        this.depositeColor2BG = depositeColor2BG;
    }

    public Color getWithdrawColor1FG() {
        return withdrawColor1FG;
    }

    public void setWithdrawColor1FG(Color withdrawColor1FG) {
        this.withdrawColor1FG = withdrawColor1FG;
    }

    public Color getWithdrawColor1BG() {
        return withdrawColor1BG;
    }

    public void setWithdrawColor1BG(Color withdrawColor1BG) {
        this.withdrawColor1BG = withdrawColor1BG;
    }

    public Color getWithdrawColor2FG() {
        return withdrawColor2FG;
    }

    public void setWithdrawColor2FG(Color withdrawColor2FG) {
        this.withdrawColor2FG = withdrawColor2FG;
    }

    public Color getWithdrawColor2BG() {
        return withdrawColor2BG;
    }

    public void setWithdrawColor2BG(Color withdrawColor2BG) {
        this.withdrawColor2BG = withdrawColor2BG;
    }

    public Color getCombinedBidColor1FG() {
        return combinedBidColor1FG;
    }

    public void setCombinedBidColor1FG(Color combinedBidColor1FG) {
        this.combinedBidColor1FG = combinedBidColor1FG;
    }

    public Color getCombinedBidColor1BG() {
        return combinedBidColor1BG;
    }

    public void setCombinedBidColor1BG(Color combinedBidColor1BG) {
        this.combinedBidColor1BG = combinedBidColor1BG;
    }

    public Color getCombinedBidColor2FG() {
        return combinedBidColor2FG;
    }

    public void setCombinedBidColor2FG(Color combinedBidColor2FG) {
        this.combinedBidColor2FG = combinedBidColor2FG;
    }

    public Color getCombinedAskColor1FG() {
        return combinedAskColor1FG;
    }

    public void setCombinedAskColor1FG(Color combinedAskColor1FG) {
        this.combinedAskColor1FG = combinedAskColor1FG;
    }

    public Color getCombinedAskColor1BG() {
        return combinedAskColor1BG;
    }

    public void setCombinedAskColor1BG(Color combinedAskColor1BG) {
        this.combinedAskColor1BG = combinedAskColor1BG;
    }

    public Color getCombinedAskColor2FG() {
        return combinedAskColor2FG;
    }

    public void setCombinedAskColor2FG(Color combinedAskColor2FG) {
        this.combinedAskColor2FG = combinedAskColor2FG;
    }

    public Color getCombinedAskColor2BG() {
        return combinedAskColor2BG;
    }

    public void setCombinedAskColor2BG(Color combinedAskColor2BG) {
        this.combinedAskColor2BG = combinedAskColor2BG;
    }

    public Color getCombinedSelectedColorFG() {
        return combinedSelectedColorFG;
    }

    public void setCombinedSelectedColorFG(Color combinedSelectedColorFG) {
        this.combinedSelectedColorFG = combinedSelectedColorFG;
    }

    public Color getCombinedSelectedColorBG() {
        return combinedSelectedColorBG;
    }

    public void setCombinedSelectedColorBG(Color combinedSelectedColorBG) {
        this.combinedSelectedColorBG = combinedSelectedColorBG;
    }

    public Color getCombinedBidColor2BG() {
        return combinedBidColor2BG;
    }

    public void setCombinedBidColor2BG(Color combinedBidColor2BG) {
        this.combinedBidColor2BG = combinedBidColor2BG;
    }

    public Color getCombinedPriceColor1FG() {
        return combinedPriceColor1FG;
    }

    public void setCombinedPriceColor1FG(Color combinedPriceColor1FG) {
        this.combinedPriceColor1FG = combinedPriceColor1FG;
    }

    public Color getCombinedPriceColor1BG() {
        return combinedPriceColor1BG;
    }

    public void setCombinedPriceColor1BG(Color combinedPriceColor1BG) {
        this.combinedPriceColor1BG = combinedPriceColor1BG;
    }

    public Color getCombinedPriceColor2FG() {
        return combinedPriceColor2FG;
    }

    public void setCombinedPriceColor2FG(Color combinedPriceColor2FG) {
        this.combinedPriceColor2FG = combinedPriceColor2FG;
    }

    public Color getCombinedPriceColor2BG() {
        return combinedPriceColor2BG;
    }

    public void setCombinedPriceColor2BG(Color combinedPriceColor2BG) {
        this.combinedPriceColor2BG = combinedPriceColor2BG;
    }

    public Color getTimeNSalesRowColor1FG() {
        return timeNSalesRowColor1FG;
    }

    public void setTimeNSalesRowColor1FG(Color timeNSalesRowColor1FG) {
        this.timeNSalesRowColor1FG = timeNSalesRowColor1FG;
    }

    public Color getTimeNSalesRowColor2FG() {
        return timeNSalesRowColor2FG;
    }

    public void setTimeNSalesRowColor2FG(Color timeNSalesRowColor2FG) {
        this.timeNSalesRowColor2FG = timeNSalesRowColor2FG;
    }

    public Color getTimeNSalesRowColor1BG() {
        return timeNSalesRowColor1BG;
    }

    public void setTimeNSalesRowColor1BG(Color timeNSalesRowColor1BG) {
        this.timeNSalesRowColor1BG = timeNSalesRowColor1BG;
    }

    public Color getTimeNSalesRowColor2BG() {
        return timeNSalesRowColor2BG;
    }

    public void setTimeNSalesRowColor2BG(Color timeNSalesRowColor2BG) {
        this.timeNSalesRowColor2BG = timeNSalesRowColor2BG;
    }

    public Color getTimeNSalesSelectedColorFG() {
        return timeNSalesSelectedColorFG;
    }

    public void setTimeNSalesSelectedColorFG(Color timeNSalesSelectedColorFG) {
        this.timeNSalesSelectedColorFG = timeNSalesSelectedColorFG;
    }

    public Color getTimeNSalesSelectedColorBG() {
        return timeNSalesSelectedColorBG;
    }

    public void setTimeNSalesSelectedColorBG(Color timeNSalesSelectedColorBG) {
        this.timeNSalesSelectedColorBG = timeNSalesSelectedColorBG;
    }

    public Color getDepthOrderBidColor2FG() {
        return depthOrderBidColor2FG;
    }

    public void setDepthOrderBidColor2FG(Color depthOrderBidColor2FG) {
        this.depthOrderBidColor2FG = depthOrderBidColor2FG;
    }

    public boolean isCustomThemeEnabled() {
        return isCustomThemeEnabled;
    }

    public void setCustomThemeEnabled(boolean customThemeEnabled) {
        isCustomThemeEnabled = customThemeEnabled;
    }

    public Color getDepthOrderBidColor2BG() {
        return depthOrderBidColor2BG;
    }

    public void setDepthOrderBidColor2BG(Color depthOrderBidColor2BG) {
        this.depthOrderBidColor2BG = depthOrderBidColor2BG;
    }

    public Color getDepthOrderBidColor1FG() {
        return depthOrderBidColor1FG;
    }

    public void setDepthOrderBidColor1FG(Color depthOrderBidColor1FG) {
        this.depthOrderBidColor1FG = depthOrderBidColor1FG;
    }

    public Color getDepthOrderBidColor1BG() {
        return depthOrderBidColor1BG;
    }

    public void setDepthOrderBidColor1BG(Color depthOrderBidColor1BG) {
        this.depthOrderBidColor1BG = depthOrderBidColor1BG;
    }

    public Color getDepthOrderAskColor1FG() {
        return depthOrderAskColor1FG;
    }

    public void setDepthOrderAskColor1FG(Color depthOrderAskColor1FG) {
        this.depthOrderAskColor1FG = depthOrderAskColor1FG;
    }

    public Color getDepthOrderAskColor1BG() {
        return depthOrderAskColor1BG;
    }

    public void setDepthOrderAskColor1BG(Color depthOrderAskColor1BG) {
        this.depthOrderAskColor1BG = depthOrderAskColor1BG;
    }

    public Color getDepthOrderAskColor2FG() {
        return depthOrderAskColor2FG;
    }

    public void setDepthOrderAskColor2FG(Color depthOrderAskColor2FG) {
        this.depthOrderAskColor2FG = depthOrderAskColor2FG;
    }

    public Color getDepthOrderAskColor2BG() {
        return depthOrderAskColor2BG;
    }

    public void setDepthOrderAskColor2BG(Color depthOrderAskColor2BG) {
        this.depthOrderAskColor2BG = depthOrderAskColor2BG;
    }

    public Color getDepthOrderSelectedColorFG() {
        return depthOrderSelectedColorFG;
    }

    public void setDepthOrderSelectedColorFG(Color depthOrderSelectedColorFG) {
        this.depthOrderSelectedColorFG = depthOrderSelectedColorFG;
    }

    public Color getDepthOrderSelectedColorBG() {
        return depthOrderSelectedColorBG;
    }

    public void setDepthOrderSelectedColorBG(Color depthOrderSelectedColorBG) {
        this.depthOrderSelectedColorBG = depthOrderSelectedColorBG;
    }

    public Color getConditionBuyColorFG() {
        return conditionBuyColorFG;
    }

    public void setConditionBuyColorFG(Color conditionBuyColorFG) {
        this.conditionBuyColorFG = conditionBuyColorFG;
    }

    public Color getConditionBuyColorBG() {
        return conditionBuyColorBG;
    }

    public void setConditionBuyColorBG(Color conditionBuyColorBG) {
        this.conditionBuyColorBG = conditionBuyColorBG;
    }

    public Color getConditionSellColorFG() {
        return conditionSellColorFG;
    }

    public void setConditionSellColorFG(Color conditionSellColorFG) {
        this.conditionSellColorFG = conditionSellColorFG;
    }

    public Color getConditionSellColorBG() {
        return conditionSellColorBG;
    }

    public void setConditionSellColorBG(Color conditionSellColorBG) {
        this.conditionSellColorBG = conditionSellColorBG;
    }

    public Color getDeposColorFG() {
        return deposColorFG;
    }

    public void setDeposColorFG(Color deposColorFG) {
        this.deposColorFG = deposColorFG;
    }

    public Color getDeposColorBG() {
        return deposColorBG;
    }

    public void setDeposColorBG(Color deposColorBG) {
        this.deposColorBG = deposColorBG;
    }

    public Color getWithdraColorFG() {
        return withdraColorFG;
    }

    public void setWithdraColorFG(Color withdraColorFG) {
        this.withdraColorFG = withdraColorFG;
    }

    public Color getWithdraColorBG() {
        return withdraColorBG;
    }

    public void setWithdraColorBG(Color withdraColorBG) {
        this.withdraColorBG = withdraColorBG;
    }

    public Color getChargesColorFG() {
        return chargesColorFG;
    }

    public void setChargesColorFG(Color chargesColorFG) {
        this.chargesColorFG = chargesColorFG;
    }

    public Color getChargesColorBG() {
        return chargesColorBG;
    }

    public void setChargesColorBG(Color chargesColorBG) {
        this.chargesColorBG = chargesColorBG;
    }

    public Color getRefundColorFG() {
        return refundColorFG;
    }

    public void setRefundColorFG(Color refundColorFG) {
        this.refundColorFG = refundColorFG;
    }

    public Color getRefundColorBG() {
        return refundColorBG;
    }

    public void setRefundColorBG(Color refundColorBG) {
        this.refundColorBG = refundColorBG;
    }

    public Color getPfDetailColorFG() {
        return pfDetailColorFG;
    }

    public void setPfDetailColorFG(Color cPfDetailColorFG) {
        this.pfDetailColorFG = cPfDetailColorFG;
    }

    public Color getPfDetailColorBG() {
        return pfDetailColorBG;
    }

    public void setPfDetailColorBG(Color cPfDetailColorBG) {
        this.pfDetailColorBG = cPfDetailColorBG;
    }

    public Color getNewsHotNewsColorFG() {
        return newsHotNewsColorFG;
    }

    public void setNewsHotNewsColorFG(Color newsHotNewsColorFG) {
        this.newsHotNewsColorFG = newsHotNewsColorFG;
    }

    public Color getNewsTemporaryNewsColorBG() {
        return newsTemporaryNewsColorBG;
    }

    public void setNewsTemporaryNewsColorBG(Color newsTemporaryNewsColorBG) {
        this.newsTemporaryNewsColorBG = newsTemporaryNewsColorBG;
    }

    public Color getOptionChainStrikeColor2FG() {
        return optionChainStrikeColor2FG;
    }

    public void setOptionChainStrikeColor2FG(Color optionChainStrikeColor2FG) {
        this.optionChainStrikeColor2FG = optionChainStrikeColor2FG;
    }

    public Color getOptionChainStrikeColor2BG() {
        return optionChainStrikeColor2BG;
    }

    public void setOptionChainStrikeColor2BG(Color optionChainStrikeColor2BG) {
        this.optionChainStrikeColor2BG = optionChainStrikeColor2BG;
    }

    public Color getOptionChainStrikeColor1FG() {
        return optionChainStrikeColor1FG;
    }

    public void setOptionChainStrikeColor1FG(Color optionChainStrikeColor1FG) {
        this.optionChainStrikeColor1FG = optionChainStrikeColor1FG;
    }

    public Color getOptionChainStrikeColor1BG() {
        return optionChainStrikeColor1BG;
    }

    public void setOptionChainStrikeColor1BG(Color optionChainStrikeColor1BG) {
        this.optionChainStrikeColor1BG = optionChainStrikeColor1BG;
    }

    public Color getOptionChainPutColor1FG() {
        return optionChainPutColor1FG;
    }

    public void setOptionChainPutColor1FG(Color optionChainPutColor1FG) {
        this.optionChainPutColor1FG = optionChainPutColor1FG;
    }

    public Color getOptionChainPutColor1BG() {
        return optionChainPutColor1BG;
    }

    public void setOptionChainPutColor1BG(Color optionChainPutColor1BG) {
        this.optionChainPutColor1BG = optionChainPutColor1BG;
    }

    public Color getOptionChainPutColor2FG() {
        return optionChainPutColor2FG;
    }

    public void setOptionChainPutColor2FG(Color optionChainPutColor2FG) {
        this.optionChainPutColor2FG = optionChainPutColor2FG;
    }

    public Color getOptionChainPutColor2BG() {
        return optionChainPutColor2BG;
    }

    public void setOptionChainPutColor2BG(Color optionChainPutColor2BG) {
        this.optionChainPutColor2BG = optionChainPutColor2BG;
    }

    public Color getOptionChainCallColor2FG() {
        return optionChainCallColor2FG;
    }

    public void setOptionChainCallColor2FG(Color optionChainCallColor2FG) {
        this.optionChainCallColor2FG = optionChainCallColor2FG;
    }

    public Color getOptionChainCallColor2BG() {
        return optionChainCallColor2BG;
    }

    public void setOptionChainCallColor2BG(Color optionChainCallColor2BG) {
        this.optionChainCallColor2BG = optionChainCallColor2BG;
    }

    public Color getOptionChainCallColor1FG() {
        return optionChainCallColor1FG;
    }

    public void setOptionChainCallColor1FG(Color optionChainCallColor1FG) {
        this.optionChainCallColor1FG = optionChainCallColor1FG;
    }

    public Color getOptionChainCallColor1BG() {
        return optionChainCallColor1BG;
    }

    public void setOptionChainCallColor1BG(Color optionChainCallColor1BG) {
        this.optionChainCallColor1BG = optionChainCallColor1BG;
    }

    public String getWorkspaceString() {
        // order of colors are important
        StringBuffer buffer = new StringBuffer();
        buffer.append(getColorString(rowColor1FG)); //1
        buffer.append(",");
        buffer.append(getColorString(rowColor1BG)); //2
        buffer.append(",");
        buffer.append(getColorString(rowColor2FG)); //3
        buffer.append(",");
        buffer.append(getColorString(rowColor2BG)); //4
        buffer.append(",");
        buffer.append(getColorString(selectedColumnFG)); //5
        buffer.append(",");
        buffer.append(getColorString(selectedColumnBG)); //6
        buffer.append(",");
        buffer.append(getColorString(cellHighLightedUpFG)); //7
        buffer.append(",");
        buffer.append(getColorString(cellHighLightedUpBG)); //8
        buffer.append(",");
        buffer.append(getColorString(cellHighLightedDownFG)); //9
        buffer.append(",");
        buffer.append(getColorString(cellHighLightedDownBG)); //10
        buffer.append(",");
        buffer.append(getColorString(positiveChangeFG)); //11
        buffer.append(",");
        buffer.append(getColorString(negativeChangeFG)); //12
        buffer.append(",");
        buffer.append(getColorString(headerColorFG)); //13
        buffer.append(",");
        buffer.append(getColorString(headerColorBG)); //14
        buffer.append(",");
        buffer.append(getColorString(buyColorFG)); //15
        buffer.append(",");
        buffer.append(getColorString(buyColorBG)); //16
        buffer.append(",");
        buffer.append(getColorString(sellColorFG)); //17
        buffer.append(",");
        buffer.append(getColorString(sellColorBG)); //18
        buffer.append(",");
        buffer.append(getColorString(opBalColorFG)); //19
        buffer.append(",");
        buffer.append(getColorString(opBalColorBG)); //20
        buffer.append(",");
        buffer.append(getColorString(divColorFG)); //21
        buffer.append(",");
        buffer.append(getColorString(divColorBG)); //22
        buffer.append(",");
        buffer.append(getColorString(this.getGridColor())); //23
        buffer.append(",");
        buffer.append(this.gridType); //24
        buffer.append(",");
        buffer.append(getColorString(askColor1FG)); //25
        buffer.append(",");
        buffer.append(getColorString(askColor1BG)); //26
        buffer.append(",");
        buffer.append(getColorString(askColor2FG)); //27
        buffer.append(",");
        buffer.append(getColorString(askColor2BG)); //28
        buffer.append(",");
        buffer.append(getColorString(bidColor1FG)); //29
        buffer.append(",");
        buffer.append(getColorString(bidColor1BG)); //30
        buffer.append(",");
        buffer.append(getColorString(bidColor2FG)); //31
        buffer.append(",");
        buffer.append(getColorString(bidColor2BG)); //32
        buffer.append(",");
        buffer.append(getColorString(combinedAskColor1FG)); //33
        buffer.append(",");
        buffer.append(getColorString(combinedAskColor1BG)); //34
        buffer.append(",");
        buffer.append(getColorString(combinedAskColor2FG)); //35
        buffer.append(",");
        buffer.append(getColorString(combinedAskColor2BG)); //36
        buffer.append(",");
        buffer.append(getColorString(combinedBidColor1FG)); //37
        buffer.append(",");
        buffer.append(getColorString(combinedBidColor1BG)); //38
        buffer.append(",");
        buffer.append(getColorString(combinedBidColor2FG)); //39
        buffer.append(",");
        buffer.append(getColorString(combinedBidColor2BG)); //40
        buffer.append(",");
        buffer.append(getColorString(combinedSelectedColorFG)); //41
        buffer.append(",");
        buffer.append(getColorString(combinedSelectedColorBG)); //42
        buffer.append(",");
        buffer.append(getColorString(combinedPriceColor1FG)); //43
        buffer.append(",");
        buffer.append(getColorString(combinedPriceColor1BG)); //44
        buffer.append(",");
        buffer.append(getColorString(combinedPriceColor2FG)); //45
        buffer.append(",");
        buffer.append(getColorString(combinedPriceColor2BG)); //46
        buffer.append(",");
        buffer.append(getColorString(timeNSalesRowColor1FG)); //47
        buffer.append(",");
        buffer.append(getColorString(timeNSalesRowColor1BG)); //48
        buffer.append(",");
        buffer.append(getColorString(timeNSalesRowColor2FG)); //49
        buffer.append(",");
        buffer.append(getColorString(timeNSalesRowColor2BG)); //50
        buffer.append(",");
        buffer.append(getColorString(timeNSalesSelectedColorFG)); //51
        buffer.append(",");
        buffer.append(getColorString(timeNSalesSelectedColorBG)); //52
        buffer.append(",");
        buffer.append(getColorString(depthOrderAskColor1FG)); //53
        buffer.append(",");
        buffer.append(getColorString(depthOrderAskColor1BG)); //54
        buffer.append(",");
        buffer.append(getColorString(depthOrderAskColor2FG)); //55
        buffer.append(",");
        buffer.append(getColorString(depthOrderAskColor2BG)); //56
        buffer.append(",");
        buffer.append(getColorString(depthOrderBidColor1FG)); //57
        buffer.append(",");
        buffer.append(getColorString(depthOrderBidColor1BG)); //58
        buffer.append(",");
        buffer.append(getColorString(depthOrderBidColor2FG)); //59
        buffer.append(",");
        buffer.append(getColorString(depthOrderBidColor2BG)); //60
        buffer.append(",");
        buffer.append(getColorString(depthOrderSelectedColorFG)); //61
        buffer.append(",");
        buffer.append(getColorString(depthOrderSelectedColorBG)); //62
        buffer.append(",");
        buffer.append(getColorString(conditionBuyColorFG)); //63
        buffer.append(",");
        buffer.append(getColorString(conditionBuyColorBG)); //64
        buffer.append(",");
        buffer.append(getColorString(conditionSellColorFG)); //65
        buffer.append(",");
        buffer.append(getColorString(conditionSellColorBG)); //66
        buffer.append(",");
        buffer.append(getColorString(deposColorFG)); //67
        buffer.append(",");
        buffer.append(getColorString(deposColorBG)); //68
        buffer.append(",");
        buffer.append(getColorString(withdraColorFG)); //69
        buffer.append(",");
        buffer.append(getColorString(withdraColorBG)); //70
        buffer.append(",");
        buffer.append(getColorString(chargesColorFG)); //71
        buffer.append(",");
        buffer.append(getColorString(chargesColorBG)); //72
        buffer.append(",");
        buffer.append(getColorString(refundColorFG)); //73
        buffer.append(",");
        buffer.append(getColorString(refundColorBG)); //74
        buffer.append(",");
        buffer.append(getBodyGap()); //75
        buffer.append(",");
        buffer.append(getColorString(newsHotNewsColorFG)); //76
        buffer.append(",");
        buffer.append(getColorString(newsTemporaryNewsColorBG)); //77
        buffer.append(",");
        buffer.append(getColorString(optionChainStrikeColor1FG)); //78
        buffer.append(",");
        buffer.append(getColorString(optionChainStrikeColor1BG)); //79
        buffer.append(",");
        buffer.append(getColorString(optionChainStrikeColor2FG)); //80
        buffer.append(",");
        buffer.append(getColorString(optionChainStrikeColor2BG)); //81
        buffer.append(",");
        buffer.append(getColorString(optionChainCallColor1FG)); //82
        buffer.append(",");
        buffer.append(getColorString(optionChainCallColor1BG)); //83
        buffer.append(",");
        buffer.append(getColorString(optionChainCallColor2FG)); //84
        buffer.append(",");
        buffer.append(getColorString(optionChainCallColor2BG)); //85
        buffer.append(",");
        buffer.append(getColorString(optionChainPutColor1FG)); //86
        buffer.append(",");
        buffer.append(getColorString(optionChainPutColor1BG)); //87
        buffer.append(",");
        buffer.append(getColorString(optionChainPutColor2FG)); //88
        buffer.append(",");
        buffer.append(getColorString(optionChainPutColor2BG)); //89
        buffer.append(",");
        buffer.append(getColorString(pfDetailColorFG)); //90
        buffer.append(",");
        buffer.append(getColorString(pfDetailColorBG)); //91

        return buffer.toString();
    }

    public void setWorkspaceString(String setting) {
        try {
            // order of colors are important

            String[] colors = setting.split(",");

            rowColor1FG = getColor(colors[0]);
            rowColor1BG = getColor(colors[1]);
            rowColor2FG = getColor(colors[2]);
            rowColor2BG = getColor(colors[3]);
            selectedColumnFG = getColor(colors[4]);
            selectedColumnBG = getColor(colors[5]);
            cellHighLightedUpFG = getColor(colors[6]);
            cellHighLightedUpBG = getColor(colors[7]);
            cellHighLightedDownFG = getColor(colors[8]);
            cellHighLightedDownBG = getColor(colors[9]);
            positiveChangeFG = getColor(colors[10]);
            negativeChangeFG = getColor(colors[11]);
            headerColorFG = getColor(colors[12]);
            headerColorBG = getColor(colors[13]);
            buyColorFG = getColor(colors[14]);
            buyColorBG = getColor(colors[15]);
            sellColorFG = getColor(colors[16]);
            sellColorBG = getColor(colors[17]);
            opBalColorFG = getColor(colors[18]);
            opBalColorBG = getColor(colors[19]);
            divColorFG = getColor(colors[20]);
            divColorBG = getColor(colors[21]);
            setGridColor(getColor(colors[22]));
            setGridType(SharedMethods.intValue(colors[23], TableThemeSettings.GRID_NONE));
            askColor1FG = getColor(colors[24]);
            askColor1BG = getColor(colors[25]);
            askColor2FG = getColor(colors[26]);
            askColor2BG = getColor(colors[27]);
            bidColor1FG = getColor(colors[28]);
            bidColor1BG = getColor(colors[29]);
            bidColor2FG = getColor(colors[30]);
            bidColor2BG = getColor(colors[31]);
            combinedAskColor1FG = getColor(colors[32]);
            combinedAskColor1BG = getColor(colors[33]);
            combinedAskColor2FG = getColor(colors[34]);
            combinedAskColor2BG = getColor(colors[35]);
            combinedBidColor1FG = getColor(colors[36]);
            combinedBidColor1BG = getColor(colors[37]);
            combinedBidColor2FG = getColor(colors[38]);
            combinedBidColor2BG = getColor(colors[39]);
            combinedSelectedColorFG = getColor(colors[40]);
            combinedSelectedColorBG = getColor(colors[41]);
            timeNSalesRowColor1FG = getColor(colors[42]);
            timeNSalesRowColor1BG = getColor(colors[43]);
            timeNSalesRowColor2FG = getColor(colors[44]);
            timeNSalesRowColor2BG = getColor(colors[45]);
            timeNSalesRowColor1FG = getColor(colors[46]);
            timeNSalesRowColor1BG = getColor(colors[47]);
            timeNSalesRowColor2FG = getColor(colors[48]);
            timeNSalesRowColor2BG = getColor(colors[49]);
            timeNSalesSelectedColorFG = getColor(colors[50]);
            timeNSalesSelectedColorBG = getColor(colors[51]);
            depthOrderAskColor1FG = getColor(colors[52]);
            depthOrderAskColor1BG = getColor(colors[53]);
            depthOrderAskColor2FG = getColor(colors[54]);
            depthOrderAskColor2BG = getColor(colors[55]);
            depthOrderBidColor1FG = getColor(colors[56]);
            depthOrderBidColor1BG = getColor(colors[57]);
            depthOrderBidColor2FG = getColor(colors[58]);
            depthOrderBidColor2BG = getColor(colors[59]);
            depthOrderSelectedColorFG = getColor(colors[60]);
            depthOrderSelectedColorBG = getColor(colors[61]);
            conditionBuyColorFG = getColor(colors[62]);
            conditionBuyColorBG = getColor(colors[63]);
            conditionSellColorFG = getColor(colors[64]);
            conditionSellColorBG = getColor(colors[65]);
            deposColorFG = getColor(colors[66]);
            deposColorBG = getColor(colors[67]);
            withdraColorFG = getColor(colors[68]);
            withdraColorBG = getColor(colors[69]);
            chargesColorFG = getColor(colors[70]);
            chargesColorBG = getColor(colors[71]);
            refundColorFG = getColor(colors[72]);
            refundColorBG = getColor(colors[73]);
            bodyGap = Integer.parseInt(colors[74]);
            newsHotNewsColorFG = getColor(colors[75]);
            newsTemporaryNewsColorBG = getColor(colors[76]);
            optionChainStrikeColor1FG = getColor(colors[77]);
            optionChainStrikeColor1BG = getColor(colors[78]);
            optionChainStrikeColor2FG = getColor(colors[79]);
            optionChainStrikeColor2BG = getColor(colors[80]);
            optionChainCallColor1FG = getColor(colors[81]);
            optionChainCallColor1BG = getColor(colors[82]);
            optionChainCallColor2FG = getColor(colors[83]);
            optionChainCallColor2BG = getColor(colors[84]);
            optionChainPutColor1FG = getColor(colors[85]);
            optionChainPutColor1BG = getColor(colors[86]);
            optionChainPutColor2FG = getColor(colors[87]);
            optionChainPutColor2BG = getColor(colors[88]);
            pfDetailColorFG = getColor(colors[89]);
            pfDetailColorBG = getColor(colors[90]);
        } catch (Exception e) {
        }

    }

    private Color getColor(String value) {
        int intVal = Integer.parseInt(value);
        if (intVal == -2) {
            return null;
        } else {
            return new Color(intVal);
        }
    }

    private String getColorString(Color color) {
        if (color == null)
            return "-2";
        else
            return "" + color.getRGB();
    }


    public int getBodyGap() {
        return bodyGap;
    }

    public void setBodyGap(int bodyGap) {
        this.bodyGap = bodyGap;
    }
}