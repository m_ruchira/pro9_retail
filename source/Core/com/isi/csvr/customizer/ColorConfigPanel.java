package com.isi.csvr.customizer;

import com.isi.csvr.Client;
import com.isi.csvr.ClientTable;
import com.isi.csvr.FontChooser;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.CustomThemeInterface;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class ColorConfigPanel extends JPanel implements ActionListener {

    private int WIDTH = 185;
    private int BUTTON_WIDTH = 60;
    private JColorChooser colorChooser;
    private JDialog colorDialog;
    private FontChooser fontChooser;
    private Font oldFont;
    private Font newBodyFont;
    private Font newHeadingFont;

    private FlowLayout flowLayout1 = new FlowLayout(FlowLayout.TRAILING);

    private JButton btnFgRow1 = new JButton();
    private JLabel lblRow1 = new JLabel();
    private JButton btnBgRow1 = new JButton();

    private JLabel lblFgDown = new JLabel();
    private JButton btnFgRow2 = new JButton();
    private JLabel lblRow2 = new JLabel();
    private JButton btnBgRow2 = new JButton();

    private JButton btnDownFG = new JButton();
    private JButton btnDownBG = new JButton();


    private JLabel lblSelected = new JLabel();
    private JButton btnUpFg = new JButton();
    private JButton btnUpBg = new JButton();
    private JPanel colorPanel = new JPanel();
    private JPanel fontPanel = new JPanel();
    private JButton btnBodyFont = new JButton();
    private JButton btnHeadingFont = new JButton();
    private JLabel lblBodyFont = new JLabel();
    private JLabel lblHeadingFont = new JLabel();
    private JLabel lblBodyGap = new JLabel();
    private TWComboBox btnBodyGap = null;

    private Color oldColor;
    private Color newColor;
    private JButton source;
    private TableThemeSettings target;
    private String lastCommand = "";
    private TWButton btnCLose = new TWButton();
    private JCheckBox chkEnable = new JCheckBox();
    private JDialog parent;
    private JLabel lblUp = new JLabel();
    private JButton btnSelectedFG = new JButton();
    private JButton btnSelectedBG = new JButton();
    private JLabel lblTrade1 = new JLabel();
    private JButton btnTradeFG1 = new JButton();
    private JButton btnTradeBG1 = new JButton();
    private JLabel lblBid = new JLabel();
    private JButton btnBidBG1 = new JButton();
    private JButton btnBidFG1 = new JButton();
    private JLabel lbl = new JLabel();
    private JButton btnAskFG1 = new JButton();
    private JButton btnAskBG1 = new JButton();
    //    private JLabel chkGrid = new JLabel();
    private JButton btnGrid = new JButton();
    private ArrayList<TWComboItem> gridTypes;
    private TWComboBox comGrid;
    private JLabel lblTrade2 = new JLabel();
    private JButton btnTradeFG2 = new JButton();
    private JButton btnTradeBG2 = new JButton();
    private JLabel lblBid2 = new JLabel();
    private JButton btnBidFG2 = new JButton();
    private JButton btnBidBG2 = new JButton();
    private JLabel lblAsk2 = new JLabel();
    private JButton btnAskFG2 = new JButton();
    private JButton btnAskBG2 = new JButton();
    private JLabel lblChangeUp = new JLabel();
    private JButton btnChangeUpFG = new JButton();
    private JLabel lblChangeDown = new JLabel();
    private JButton btnChangeDownFG = new JButton();
    private JLabel lblGrid = new JLabel();

    private int bodyGap = 2;

    private byte tableType;
    private JRadioButton thisWindowOnly;
    private JRadioButton allOpenedWindows;
    private JRadioButton allWindows;

    public ColorConfigPanel(JDialog parent) {
        this.parent = parent;
        try {
            jbInit();
            // pack();
            //Theme.registerComponent(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setTarget(TableThemeSettings target) {
        this.target = target;
        loadInitialSettings(target);
        enableButtons(colorPanel, chkEnable.isSelected());
    }

    void jbInit() throws Exception {

        JLabel cap1 = new JLabel(Language.getString("SETTING"));
        cap1.setFont(cap1.getFont().deriveFont(Font.BOLD));
        cap1.setPreferredSize(new Dimension(160, 20));
        JLabel cap2 = new JLabel(Language.getString("CELL_COLOR"));
        cap2.setFont(cap1.getFont().deriveFont(Font.BOLD));
        //cap1.setPreferredSize(new Dimension(100, 20));
        JLabel cap3 = new JLabel(Language.getString("FONT_COLOR"));
        cap3.setFont(cap1.getFont().deriveFont(Font.BOLD));
        //cap1.setPreferredSize(new Dimension(100, 20));
        GUISettings.setSameSize(cap2, cap3);
        BUTTON_WIDTH = (int) cap2.getPreferredSize().getWidth();

        WIDTH += (BUTTON_WIDTH * 2);
        JLabel previewPane = new JLabel(Language.getString("WINDOW_TITLE_CLIENT"));
        previewPane.setFont(new TWFont("Arial", Font.BOLD, 20));
        colorChooser = new JColorChooser(Color.red);
        colorChooser.setPreviewPanel(previewPane);
        colorDialog = JColorChooser.createDialog(Client.getInstance().getFrame(), Language.getString("SELECT_COLOR"), true, colorChooser, this, this);
        fontChooser = new FontChooser(Client.getInstance().getFrame(), true);
        //this.setBorder(BorderFactory.createEtchedBorder());
        //this.setPreferredSize(new Dimension(250, 520));
        BoxLayout topLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
//        this.setLayout(topLayout);
        this.setLayout(new FlexGridLayout(new String[]{"0"}, new String[]{"0", "0", "0", "0"}, 0, 0));

        fontPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        fontPanel.setBorder(BorderFactory.createEtchedBorder());
        fontPanel.setPreferredSize(new Dimension(WIDTH, 80));
        btnBodyFont.setPreferredSize(new Dimension(20, 20));
        btnBodyFont.setActionCommand("BodyFont");
        btnBodyFont.setText("...");
        btnBodyFont.setActionCommand("FONT_BODY");
        btnBodyFont.addActionListener(this);
        btnHeadingFont.setPreferredSize(new Dimension(20, 20));
        btnHeadingFont.setToolTipText("");
        btnHeadingFont.setActionCommand("HeadFont");
        btnHeadingFont.setText("...");
        btnHeadingFont.setActionCommand("FONT_HEADING");
        btnHeadingFont.addActionListener(this);
        lblBodyFont.setPreferredSize(new Dimension(180, 20));
        lblBodyFont.setText(Language.getString("MAIN_BOARD_FONT"));
        lblHeadingFont.setPreferredSize(new Dimension(180, 20));
        lblHeadingFont.setText(Language.getString("HEADING_FONT"));
        btnCLose.setText(Language.getString("CLOSE"));
        btnCLose.addActionListener(this);
        btnCLose.setActionCommand("CLOSE");

        lblBodyGap.setPreferredSize(new Dimension(180, 20));
        lblBodyGap.setText(Language.getString("BODY_GAP"));
        ArrayList elements = new ArrayList();
        for (int i = 0; i < 11; i++) {
            elements.add(i);
        }
        TWComboModel model = new TWComboModel(elements);
        btnBodyGap = new TWComboBox(model);
        btnBodyGap.setPreferredSize(new Dimension(50, 20));
        btnBodyGap.setToolTipText("");
        btnBodyGap.setActionCommand("BODY_GAP");
        btnBodyGap.addActionListener(this);
        flowLayout1.setHgap(2);

        chkEnable.setPreferredSize(new Dimension(WIDTH, 25));
        chkEnable.setText(Language.getString("ENABLE_CUSTOMIZER_CUSTOM_SETTINGS"));
        chkEnable.addActionListener(this);
        chkEnable.setActionCommand("ENABLE");
        lblUp.setPreferredSize(new Dimension(160, 17));
        lblUp.setText(Language.getString("HIGHLIGHT_UP"));
        btnSelectedFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnSelectedFG.setActionCommand("COLOR_FG_SELECTED");
        btnSelectedFG.addActionListener(this);
        btnSelectedBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnSelectedBG.setActionCommand("COLOR_BG_SELECTED");
        btnSelectedBG.addActionListener(this);

        lblTrade1.setPreferredSize(new Dimension(160, 20));
        lblTrade1.setText(Language.getString("TRADE_ROW_1"));
        btnTradeFG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnTradeFG1.setActionCommand("COLOR_FG_TRADE1");
        btnTradeFG1.addActionListener(this);
        btnTradeBG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnTradeBG1.setActionCommand("COLOR_BG_TRADE1");
        btnTradeBG1.addActionListener(this);

        lblBid.setPreferredSize(new Dimension(160, 20));
        lblBid.setText(Language.getString("BID_ROW_1"));
        btnBidBG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBidBG1.setActionCommand("COLOR_BG_BID1");
        btnBidBG1.addActionListener(this);

        lbl.setPreferredSize(new Dimension(160, 20));
        lbl.setText(Language.getString("OFFER_ROW_1"));
        btnAskBG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnAskBG1.setActionCommand("COLOR_BG_ASK1");
        btnAskBG1.addActionListener(this);

        btnBidFG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBidFG1.setActionCommand("COLOR_FG_BID1");
        btnBidFG1.addActionListener(this);

        btnAskFG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnAskFG1.setActionCommand("COLOR_FG_ASK1");
        btnAskFG1.addActionListener(this);

        lblGrid.setPreferredSize(new Dimension(105, 20));
        lblGrid.setText(Language.getString("GRID"));
//        lblGrid.setActionCommand("ENABLE_GRID");
//        chkGrid.setBorder(BorderFactory.createLineBorder(Color.red));
//        chkGrid.addActionListener(this);
        btnGrid = new JButton() {
            public void setEnabled(boolean b) {
                super.setEnabled(b);
                SharedMethods.printLine("Grid " + b, true);
            }
        };
        btnGrid.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnGrid.setActionCommand("COLOR_GRID");
        btnGrid.addActionListener(this);
//        btnGrid.setBorder(BorderFactory.createLineBorder(Color.blue));

        gridTypes = new ArrayList<TWComboItem>();
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_BOTH, Language.getString("CO_BOTH")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_VERTICAL, Language.getString("VERTICLE")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_HORIZONTAL, Language.getString("HORIZONTAL")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_NONE, Language.getString("NONE")));
        comGrid = new TWComboBox(new TWComboModel(gridTypes));
        comGrid.setPreferredSize(new Dimension(100, 20));
        comGrid.setActionCommand("GRID_TYPE");
        comGrid.setSelectedIndex(0);
        comGrid.addActionListener(this);

        lblTrade2.setPreferredSize(new Dimension(160, 20));
        lblTrade2.setText(Language.getString("TRADE_ROW_2"));
        btnTradeFG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnTradeFG2.setActionCommand("COLOR_FG_TRADE2");
        btnTradeFG2.addActionListener(this);
        btnTradeBG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnTradeBG2.setActionCommand("COLOR_BG_TRADE2");
        btnTradeBG2.addActionListener(this);
        lblBid2.setPreferredSize(new Dimension(160, 20));
        lblBid2.setText(Language.getString("BID_ROW_2"));
        btnBidFG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBidFG2.setActionCommand("COLOR_FG_BID2");
        btnBidFG2.addActionListener(this);
        btnBidBG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBidBG2.setActionCommand("COLOR_BG_BID2");
        btnBidBG2.addActionListener(this);
        lblAsk2.setPreferredSize(new Dimension(160, 20));
        lblAsk2.setText(Language.getString("OFFER_ROW_2"));
        btnAskFG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnAskFG2.setActionCommand("COLOR_FG_ASK2");
        btnAskFG2.addActionListener(this);
        btnAskBG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnAskBG2.setActionCommand("COLOR_BG_ASK2");
        btnAskBG2.addActionListener(this);
        //change start
        lblChangeUp.setPreferredSize(new Dimension(160 + BUTTON_WIDTH + 5, 20));   //222
        lblChangeUp.setText(Language.getString("POSITIVE_NET_CHANGE"));
        btnChangeUpFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnChangeUpFG.setActionCommand("COLOR_FG_CHANGE_UP");
        btnChangeUpFG.addActionListener(this);
        //change start
        lblChangeDown.setPreferredSize(new Dimension(160 + BUTTON_WIDTH + 5, 20));  //222
        lblChangeDown.setText(Language.getString("NEGATIVE_NET_CHANGE"));
        btnChangeDownFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnChangeDownFG.setActionCommand("COLOR_FG_CHANGE_DOWN");
        btnChangeDownFG.addActionListener(this);


//        lblGrid.setPreferredSize(new Dimension(222, 20));
//        lblGrid.setText(Language.getString("GRID_COLOR"));
        this.add(fontPanel, null);
        fontPanel.add(lblBodyFont, null);
        fontPanel.add(btnBodyFont, null);
        fontPanel.add(lblHeadingFont, null);
        fontPanel.add(btnHeadingFont, null);
        fontPanel.add(lblBodyGap, null);
        fontPanel.add(btnBodyGap, null);

        JPanel pnlChk = new JPanel(new FlowLayout(FlowLayout.LEADING));
        pnlChk.setPreferredSize(new Dimension(WIDTH, 35));

        pnlChk.add(chkEnable);
        this.add(pnlChk, null);
        //this.add(chkEnable, null);

        colorPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        colorPanel.setBorder(BorderFactory.createEtchedBorder());
        colorPanel.setPreferredSize(new Dimension(WIDTH, 385));
        this.add(colorPanel, null);

        colorPanel.add(lblGrid);
        colorPanel.add(comGrid);
        colorPanel.add(btnGrid);

        colorPanel.add(cap1);
        colorPanel.add(cap2);
        colorPanel.add(cap3);

        btnFgRow1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgRow1.setActionCommand("COLOR_FG_ROW1");
        btnFgRow1.addActionListener(this);
        lblRow1.setPreferredSize(new Dimension(160, 20));
        lblRow1.setText(Language.getString("ROW_COLOR_1"));

        btnBgRow1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgRow1.setActionCommand("COLOR_BG_ROW1");
        btnBgRow1.addActionListener(this);

        colorPanel.add(lblRow1, null);
        colorPanel.add(btnBgRow1, null);
        colorPanel.add(btnFgRow1, null);
        colorPanel.add(lblRow2, null);

        lblFgDown.setPreferredSize(new Dimension(160, 20));
        lblFgDown.setText(Language.getString("CELL_HIGHLIGHT_DOWN"));
        btnFgRow2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgRow2.setActionCommand("COLOR_FG_ROW2");
        btnFgRow2.addActionListener(this);
        lblRow2.setPreferredSize(new Dimension(160, 20));
        lblRow2.setText(Language.getString("ROW_COLOR_2"));
        btnBgRow2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgRow2.setActionCommand("COLOR_BG_ROW2");
        btnBgRow2.addActionListener(this);

        colorPanel.add(btnBgRow2, null);
        colorPanel.add(btnFgRow2, null);
        colorPanel.add(lblFgDown, null);
        colorPanel.add(btnDownBG, null);
        colorPanel.add(btnDownFG, null);
        colorPanel.add(lblUp, null);
        colorPanel.add(btnUpBg, null);
        colorPanel.add(btnUpFg, null);

        btnDownFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnDownFG.setActionCommand("COLOR_FG_DOWN");
        btnDownFG.addActionListener(this);
        btnDownBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnDownBG.setActionCommand("COLOR_BG_DOWN");
        btnDownBG.addActionListener(this);


        lblSelected.setPreferredSize(new Dimension(160, 20));
        lblSelected.setText(Language.getString("SELECTED_CELLS"));
        btnUpFg.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnUpFg.setActionCommand("COLOR_FG_UP");
        btnUpFg.addActionListener(this);
        btnUpBg.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnUpBg.setActionCommand("COLOR_BG_UP");
        btnUpBg.addActionListener(this);
        colorPanel.add(lblSelected, null);
        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlexGridLayout(new String[]{"0"}, new String[]{"0", "0"}, 0, 0));
        JPanel closePanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 2, 5));
        closePanel.add(btnCLose);
        closePanel.setPreferredSize(new Dimension(WIDTH, 40));
        bottomPanel.add(createSaveModePanel());
        bottomPanel.add(closePanel);
        bottomPanel.setPreferredSize(new Dimension(WIDTH, 110));
        this.add(bottomPanel, null);
        colorPanel.add(btnSelectedBG, null);
        colorPanel.add(btnSelectedFG, null);
        colorPanel.add(lblTrade1, null);
        colorPanel.add(btnTradeBG1, null);
        colorPanel.add(btnTradeFG1, null);
        colorPanel.add(lblTrade2, null);
        colorPanel.add(btnTradeBG2, null);
        colorPanel.add(btnTradeFG2, null);
        colorPanel.add(lblBid, null);
        colorPanel.add(btnBidBG1, null);
        colorPanel.add(btnBidFG1, null);
        colorPanel.add(lblBid2, null);
        colorPanel.add(btnBidBG2, null);
        colorPanel.add(btnBidFG2, null);
        colorPanel.add(lbl, null);
        colorPanel.add(btnAskBG1, null);
        colorPanel.add(btnAskFG1, null);
        colorPanel.add(lblAsk2, null);
        colorPanel.add(btnAskBG2, null);
        colorPanel.add(btnAskFG2, null);


        colorPanel.add(lblChangeUp, null);
        colorPanel.add(btnChangeUpFG, null);
        colorPanel.add(lblChangeDown, null);
        colorPanel.add(btnChangeDownFG, null);
//        colorPanel.add(lblGrid, null);
        //colorPanel.add(chkGrid, null);
        //colorPanel.add(btnGrid, null);

        GUISettings.applyOrientation(this);
        setBorders();
    }

    public JPanel createSaveModePanel() { //radio button Panel
        thisWindowOnly = new JRadioButton(Language.getString("THIS_WINDOW_ONLY"));
        allOpenedWindows = new JRadioButton(Language.getString("ALL_OPENED") + " " + Language.getString("CUTOMIZER_APPLY_MESSAGE"));
        allWindows = new JRadioButton(Language.getString("ALL_WINDOWS") + " " + Language.getString("CUTOMIZER_APPLY_MESSAGE"));
        thisWindowOnly.setSelected(true);
        ButtonGroup savingMode = new ButtonGroup();
        savingMode.add(thisWindowOnly);
        savingMode.add(allOpenedWindows);
        savingMode.add(allWindows);
        JPanel radioBtnPanel = new JPanel();
        radioBtnPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), Language.getString("APPLY_TO")));
        radioBtnPanel.setLayout(new FlexGridLayout(new String[]{"3%", "94%", "3%"}, new String[]{"33%", "33%", "34%"}, 0, 0));
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(thisWindowOnly);
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(allOpenedWindows);
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(allWindows);
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.setPreferredSize(new Dimension(new Dimension(WIDTH, 70)));
        GUISettings.applyOrientation(radioBtnPanel);
        return radioBtnPanel;
    }

    private void loadInitialSettings(TableThemeSettings target) {
        ((CustomThemeInterface) ((JTable) target).getDefaultRenderer(Object.class)).getCurrentData(target);
        chkEnable.setSelected(target.isCuatomThemeEnabled());
//        chkGrid.setSelected(target.isGdidOn());
        if (target.isGdidOn()) {
            for (TWComboItem item : gridTypes) {
                if (item.getId().equals("" + target.getGdidType())) {
                    comGrid.setSelectedItem(item);
                    break;
                }
            }
        }
        btnUpBg.setBackground(target.getUpBGColor());
        btnUpFg.setBackground(target.getUpFGColor());
        btnDownBG.setBackground(target.getDownBGColor());
        btnDownFG.setBackground(target.getDownFGColor());

        btnBgRow1.setBackground(target.getRow1BGColor());
        btnFgRow1.setBackground(target.getRow1FGColor());
        btnBgRow2.setBackground(target.getRow2BGColor());
        btnFgRow2.setBackground(target.getRow2FGColor());

        btnSelectedBG.setBackground(target.getHighlighterBGColor());
        btnSelectedFG.setBackground(target.getHighlighterFGColor());

        btnTradeBG1.setBackground(target.getTradeBGColor1());
        btnTradeFG1.setBackground(target.getTradeFGColor1());
        btnTradeBG2.setBackground(target.getTradeBGColor2());
        btnTradeFG2.setBackground(target.getTradeFGColor2());

        btnBidBG1.setBackground(target.getBidBGColor1());
        btnBidFG1.setBackground(target.getBidFGColor1());
        btnBidBG2.setBackground(target.getBidBGColor2());
        btnBidFG2.setBackground(target.getBidFGColor2());

        btnAskBG1.setBackground(target.getAskBGColor1());
        btnAskFG1.setBackground(target.getAskFGColor1());
        btnAskBG2.setBackground(target.getAskBGColor2());
        btnAskFG2.setBackground(target.getAskFGColor2());

        btnChangeDownFG.setBackground(target.getChangeDownFGColor());
        btnChangeUpFG.setBackground(target.getChangeUPFGColor());

        btnBodyGap.setSelectedItem(target.getBodyGap());
        bodyGap = target.getBodyGap();

        comGrid.setEnabled(target.isCuatomThemeEnabled());
        if (/*target.isGdidOn() ||*/ chkEnable.isSelected()) {
            btnGrid.setBackground(target.getTableGridColor());
            target.setTableGridColor(target.getTableGridColor());
            target.updateUI();
            btnGrid.setEnabled(true);
        } else {
            btnGrid.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            btnGrid.setEnabled(false);
        }

    }

    public int getBodyGap() {
        return bodyGap;
    }

    public void applyGridToAll(int gridType) {
        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i] instanceof ClientTable) {
                TableThemeSettings table = (TableThemeSettings) ((ClientTable) frames[i]).getTable();
                table.setGdidOn(gridType); //todo grid change
//                table.setGdidOn(TableThemeSettings.GRID_HORIZONTAL); //todo grid change
                table.updateUI();
                table = null;
            }
        }
        frames = null;
    }

    public void applyGridToAllOpened(int gridType) {
        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i].isVisible()) {
                if (frames[i] instanceof ClientTable) {
                    TableThemeSettings table = (TableThemeSettings) ((ClientTable) frames[i]).getTable();
                    table.setGdidOn(gridType); //todo grid change
//                    table.setGdidOn(TableThemeSettings.GRID_HORIZONTAL); //todo grid change
                    table.updateUI();
                    table = null;
                }
            }
        }
        frames = null;
    }

    public void applyColorChanges(TableThemeSettings target) {
        target.setUpBGColor(this.target.getUpBGColor());
        target.setUpFGColor(this.target.getUpFGColor());
        target.setDownFGColor(this.target.getDownFGColor());
        target.setDownBGColor(this.target.getDownBGColor());
        target.setRow2FGColor(this.target.getRow2FGColor());
        target.setRow2BGColor(this.target.getRow2BGColor());
        target.setRow1BGColor(this.target.getRow1BGColor());
        target.setRow1FGColor(this.target.getRow2FGColor());
        target.setAskBGColor2(this.target.getAskBGColor2());
        target.setAskFGColor2(this.target.getAskFGColor2());
        target.setBidFGColor2(this.target.getBidFGColor2());
        target.setBidBGColor2(this.target.getBidBGColor2());
        target.setAskBGColor1(this.target.getAskBGColor1());
        target.setAskFGColor1(this.target.getAskFGColor1());
        target.setBidFGColor1(this.target.getBidFGColor1());
        target.setBidBGColor1(this.target.getBidBGColor1());
        target.setTradeFGColor1(this.target.getTradeFGColor1());
        target.setTradeBGColor1(this.target.getTradeBGColor1());
        target.setTradeFGColor2(this.target.getTradeFGColor2());
        target.setTradeBGColor2(this.target.getTradeBGColor2());
        target.setChangeDownFGColor(this.target.getChangeDownFGColor());
        target.setChangeUPFGColor(this.target.getChangeUPFGColor());
        target.setHighlighterFGColor(this.target.getHighlighterFGColor());
        target.setHighlighterBGColor(this.target.getHighlighterBGColor());
        if (target.isGdidOn()) {
            target.setTableGridColor(this.target.getTableGridColor());
        }

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("ENABLE")) {
            target.setCustomThemeEnabled(chkEnable.isSelected());
            comGrid.setEnabled(chkEnable.isSelected());
            btnGrid.setEnabled(chkEnable.isSelected());
            enableButtons(colorPanel, chkEnable.isSelected());
            if (chkEnable.isSelected()) {
                loadInitialSettings(target);
            }
            /* must enable the grid */
            String type = ((TWComboItem) comGrid.getSelectedItem()).getId();
            target.setGdidOn(Integer.parseInt(type));
            target.updateUI();

        } else if (e.getActionCommand().equals("GRID_TYPE")) {
            String type = ((TWComboItem) comGrid.getSelectedItem()).getId();
            target.setGdidOn(Integer.parseInt(type));
            target.updateUI();
        } else if (e.getActionCommand().equals("CLOSE")) {
            if (thisWindowOnly.isSelected()) {
                bodyGap = Integer.parseInt("" + btnBodyGap.getSelectedItem());
                target.setBodyGap(bodyGap);
                target.updateUI();
            }
            if (allWindows.isSelected()) {
                applyGridToAll(Integer.parseInt(((TWComboItem) comGrid.getSelectedItem()).getId()));
                JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
                for (int i = 0; i < frames.length; i++) {
                    if (frames[i] instanceof ClientTable) {
                        loadInitialSettings(target);
                        TableThemeSettings table = (TableThemeSettings) ((ClientTable) frames[i]).getTable();
                        table.setCustomThemeEnabled(chkEnable.isSelected());
                        applyColorChanges(table);
                        table.setHeadingFont(target.getHeadingFont());
                        table.updateHeaderUI();
                        table.setBodyFont(target.getBodyFont());
                        table.setBodyGap(target.getBodyGap());
                        FontMetrics oMetrices = ((ClientTable) frames[i]).getTable().getGraphics().getFontMetrics();
                        ((ClientTable) frames[i]).getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + target.getBodyGap());
                        table.updateUI();
                        table.updateUI();
                        table = null;
                    }
                }
                frames = null;
            }
            if (allOpenedWindows.isSelected()) {
                applyGridToAllOpened(Integer.parseInt(((TWComboItem) comGrid.getSelectedItem()).getId()));
                JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
                for (int i = 0; i < frames.length; i++) {
                    if (frames[i].isVisible()) {
                        if (frames[i] instanceof ClientTable) {
                            loadInitialSettings(target);
                            TableThemeSettings table = (TableThemeSettings) ((ClientTable) frames[i]).getTable();
                            table.setCustomThemeEnabled(chkEnable.isSelected());
                            applyColorChanges(table);
                            table.setHeadingFont(target.getHeadingFont());
                            table.updateHeaderUI();
                            table.setBodyFont(target.getBodyFont());
                            table.setBodyGap(target.getBodyGap());
                            FontMetrics oMetrices = ((ClientTable) frames[i]).getTable().getGraphics().getFontMetrics();
                            ((ClientTable) frames[i]).getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + target.getBodyGap());
                            table.updateUI();
                            table.updateUI();
                            table = null;
                        }
                    }
                }
                frames = null;

            }
            parent.setVisible(false);
        } else if (e.getActionCommand().startsWith("COLOR")) {
            source = (JButton) e.getSource();
            oldColor = source.getBackground();
            colorChooser.setColor(oldColor);
            lastCommand = e.getActionCommand();
            disableResetButton();
            colorDialog.show();
        } else if (e.getActionCommand().equals("FONT_BODY")) {
            oldFont = target.getBodyFont();
            newBodyFont = fontChooser.showDialog(Language.getString("SELECT_FONT"), target.getBodyFont());
            if (newBodyFont != oldFont) {
                target.setBodyFont(newBodyFont);
                target.updateUI();
            }
        } else if (e.getActionCommand().equals("FONT_HEADING")) {
            oldFont = target.getHeadingFont();
            newHeadingFont = fontChooser.showDialog(Language.getString("SELECT_FONT"), target.getHeadingFont());
            if ((newHeadingFont != null) && (newHeadingFont != oldFont)) {
                target.setHeadingFont(newHeadingFont);
                target.updateHeaderUI();
            }
        } else if (e.getActionCommand().equals("BODY_GAP")) {
            bodyGap = Integer.parseInt("" + btnBodyGap.getSelectedItem());
            target.setBodyGap(bodyGap);
            target.updateUI();
        } else if (e.getActionCommand().equalsIgnoreCase("OK")) {
            newColor = colorChooser.getColor();

            if (oldColor != newColor) {
                source.setBackground(newColor);
                if (lastCommand.equals("COLOR_FG_UP")) {
                    target.setUpFGColor(newColor);
                } else if (lastCommand.equals("COLOR_BG_UP")) {
                    target.setUpBGColor(newColor);
                } else if (lastCommand.equals("COLOR_FG_DOWN")) {
                    target.setDownFGColor(newColor);
                } else if (lastCommand.equals("COLOR_BG_DOWN")) {
                    target.setDownBGColor(newColor);

                } else if (lastCommand.equals("COLOR_FG_ROW2")) {
                    target.setRow2FGColor(newColor);
                } else if (lastCommand.equals("COLOR_BG_ROW2")) {
                    target.setRow2BGColor(newColor);

                } else if (lastCommand.equals("COLOR_BG_ROW1")) {
                    target.setRow1BGColor(newColor);
                } else if (lastCommand.equals("COLOR_FG_ROW1")) {
                    target.setRow1FGColor(newColor);

                } else if (lastCommand.equals("COLOR_BG_ASK2")) {
                    target.setAskBGColor2(newColor);
                } else if (lastCommand.equals("COLOR_FG_ASK2")) {
                    target.setAskFGColor2(newColor);

                } else if (lastCommand.equals("COLOR_FG_BID2")) {
                    target.setBidFGColor2(newColor);
                } else if (lastCommand.equals("COLOR_BG_BID2")) {
                    target.setBidBGColor2(newColor);

                } else if (lastCommand.equals("COLOR_BG_ASK1")) {
                    target.setAskBGColor1(newColor);
                } else if (lastCommand.equals("COLOR_FG_ASK1")) {
                    target.setAskFGColor1(newColor);
                } else if (lastCommand.equals("COLOR_FG_BID1")) {
                    target.setBidFGColor1(newColor);
                } else if (lastCommand.equals("COLOR_BG_BID1")) {
                    target.setBidBGColor1(newColor);
                } else if (lastCommand.equals("COLOR_FG_TRADE1")) {
                    target.setTradeFGColor1(newColor);
                } else if (lastCommand.equals("COLOR_BG_TRADE1")) {
                    target.setTradeBGColor1(newColor);
                } else if (lastCommand.equals("COLOR_FG_TRADE2")) {
                    target.setTradeFGColor2(newColor);
                } else if (lastCommand.equals("COLOR_BG_TRADE2")) {
                    target.setTradeBGColor2(newColor);
                } else if (lastCommand.equals("COLOR_FG_CHANGE_DOWN")) {
                    target.setChangeDownFGColor(newColor);
                } else if (lastCommand.equals("COLOR_FG_CHANGE_UP")) {
                    target.setChangeUPFGColor(newColor);
                } else if (lastCommand.equals("COLOR_FG_SELECTED")) {
                    target.setHighlighterFGColor(newColor);
                } else if (lastCommand.equals("COLOR_BG_SELECTED")) {
                    //System.err.println("selected clicked");
                    target.setHighlighterBGColor(newColor);
                } else if (lastCommand.equals("COLOR_GRID")) {
                    target.setTableGridColor(newColor);
                }
                target.updateUI();
            }
        }
    }

    public Font getBodyFont() {
        return newBodyFont;
    }

    public Font getHeadingFont() {
        return newHeadingFont;
    }

    public void setBorders() {
        Component c;
        int ncomponents = colorPanel.getComponentCount();
        for (int i = 0; i < ncomponents; ++i) {
            c = colorPanel.getComponent(i);
            if (c instanceof JButton) {
                ((JButton) c).setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
            }
        }
        c = null;
    }

    public void enableButtons(Component c, boolean status) {

        if (c instanceof JButton) {
            c.setEnabled(status);
            if (status == false)
                c.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            return;
        }

        if (c instanceof JLabel) {
            c.setEnabled(status);
            return;
        }

        if (c instanceof JCheckBox) {
            c.setEnabled(status);
            return;
        } else if (c instanceof java.awt.Container) {
            java.awt.Container container = (java.awt.Container) c;
            int ncomponents = container.getComponentCount();
            for (int i = 0; i < ncomponents; ++i) {
                enableButtons(container.getComponent(i), status);
            }
        }
    }

    /*private void applyTableGrid() {
//        target.setGridColor(Color.red); // lblGridColor.getBackground());
        smartTable.setGdidOn(chkGrid.isSelected());
        if (chkGrid.isSelected()) {
            smartTable.setIntercellSpacing(new Dimension(1, 1));
            lblGridColor.setBackground(target.getGridColor());
            smartTable.setGridColor(target.getGridColor());
        } else {
            smartTable.setIntercellSpacing(new Dimension(0, 0));
            lblGridColor.setBackground(this.getBackground());
//            smartTable.setGridColor(this.getBackground());
        }
    }*/

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(colorChooser);
        SwingUtilities.updateComponentTreeUI(fontChooser);
        SwingUtilities.updateComponentTreeUI(colorDialog);
        setBorders();
    }


    private void disableResetButton() {

        Component[] comp = colorDialog.getContentPane().getComponents();

        for (int i = 0; i < comp.length; i++) {

            if (comp[i] instanceof JPanel) {

                JPanel btnpan = (JPanel) comp[i];
                Component[] btncomps = btnpan.getComponents();
                String resetString = UIManager.getString("ColorChooser.resetText");

                for (int j = 0; j < btncomps.length; j++) {

                    if (btncomps[j] instanceof JButton && ((JButton) btncomps[j]).getText().equals(resetString)) {
                        btncomps[j].setVisible(false);
                    }
                }
            }
        }
    }
}