package com.isi.csvr.customizer;

//import javax.swing.JPanel;
//import java.awt.FlowLayout;
//import javax.swing.JLabel;
//import javax.swing.JButton;

import java.awt.*;

/**
 * <p>Title: TW Test Projects</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: ISI</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class CustomObject {

    private String fieldName;
    private Color fieldBGColor;
    private Color fieldFGColor;
    private Color fieldBGColor2;
    private Color fieldFGColor2;

    public CustomObject(String fieldName, Color bgColor, Color fgColor, Color bgColor2, Color fgColor2) {
        this.fieldName = fieldName;
        fieldBGColor = bgColor;
        fieldFGColor = fgColor;
        fieldBGColor2 = bgColor2;
        fieldFGColor2 = fgColor2;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Color getFieldBGColor() {
        return fieldBGColor;
    }

    public void setFieldBGColor(Color fieldBGColor) {
        this.fieldBGColor = fieldBGColor;
    }

    public Color getFieldFGColor() {
        return fieldFGColor;
    }

    public void setFieldFGColor(Color fieldFGColor) {
        this.fieldFGColor = fieldFGColor;
    }

    public Color getFieldBGColor2() {
        return fieldBGColor2;
    }

    public void setFieldBGColor2(Color fieldBGColor2) {
        this.fieldBGColor2 = fieldBGColor2;
    }

    public Color getFieldFGColor2() {
        return fieldFGColor2;
    }

    public void setFieldFGColor2(Color fieldFGColor2) {
        this.fieldFGColor2 = fieldFGColor2;
    }

}