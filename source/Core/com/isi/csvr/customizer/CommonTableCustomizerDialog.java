package com.isi.csvr.customizer;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class CommonTableCustomizerDialog extends JDialog implements ActionListener, WindowListener {

    private static CommonTableCustomizerDialog self = null;
    private JPanel panel1 = new JPanel();
    private CustomizeListPanel colorConfigPanel;

    private CommonTableCustomizerDialog(Component invoker, String title, boolean modal) {
        super((Window) SwingUtilities.getAncestorOfClass(Window.class, invoker), title, Dialog.ModalityType.APPLICATION_MODAL);
        try {
            jbInit();
            pack();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private CommonTableCustomizerDialog(Component invoker) {
        this(invoker, Language.getString("CUSTOMIZER"), true);
        //colorConfigPanel.setTarget(target);
        setLocationRelativeTo(Client.getInstance().getFrame());
        //setDefaultCloseOperation(HIDE_ON_CLOSE);
        colorConfigPanel.registerKeyboardAction(this, "ESC", KeyStroke.getKeyStroke("released ESCAPE"), JComponent.WHEN_IN_FOCUSED_WINDOW);//KeyEvent.VK_ESCAPE,0
    }


    public static CommonTableCustomizerDialog getSharedInstance(Component invoker) {
        if (self == null) {
            self = new CommonTableCustomizerDialog(invoker);
        }
        return self;
    }

//    public void show(SmartTable target){

    public void show(Table target) {
//        colorConfigPanel.setTable();
        colorConfigPanel.setTarget(target);
        colorConfigPanel.applyTheme();
        show();
    }

    public Font getBodyFont() {
        return colorConfigPanel.getBodyFont();
    }

    public Font getHeadingFont() {
        return colorConfigPanel.getHeadingFont();
    }

    private void jbInit() throws Exception {
        panel1.setLayout(new FlowLayout());
        colorConfigPanel = new CustomizeListPanel(this);
        getContentPane().add(colorConfigPanel);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(this);
    }

    public void actionPerformed(ActionEvent e) {
//        setVisible(false);
        dispose();
    }


    public void windowActivated(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
        self = null;
    }

    public void windowClosing(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
    }
}