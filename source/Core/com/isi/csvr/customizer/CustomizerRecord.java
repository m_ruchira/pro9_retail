package com.isi.csvr.customizer;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 24, 2003
 * Time: 8:47:30 AM
 * To change this template use Options | File Templates.
 */
public class CustomizerRecord {
    private String description;
    private Color fgColor1;
    private Color bgColor1;
    private String method;
    private int type;

    public CustomizerRecord(String description, int type, Color bgColor1, Color fgColor1) {
        this.type = type;
        this.bgColor1 = bgColor1;
        this.description = description;
        this.fgColor1 = fgColor1;
    }

    public Color getBgColor() {
        return bgColor1;
    }

//    public Color getBgColor2() {
//        return bgColor2;
//    }

    public void setBgColor(Color bgColor1) {
        this.bgColor1 = bgColor1;
    }

    public String getDescription() {
        return description;
    }

//    public Color getFgColor2() {
//        return fgColor2;
//    }

    public Color getFgColor() {
        return fgColor1;
    }

//    public void setBgColor2(Color bgColor2) {
//        this.bgColor2 = bgColor2;
//    }

    public void setFgColor(Color fgColor1) {
        this.fgColor1 = fgColor1;
    }

//    public void setFgColor2(Color fgColor2) {
//        this.fgColor2 = fgColor2;
//    }

    public int getType() {
        return type;
    }

    public String getMethod() {
        return method;
    }
}
