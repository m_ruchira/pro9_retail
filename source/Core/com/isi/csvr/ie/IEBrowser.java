package com.isi.csvr.ie;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.BrowserDialogEventHandler;
import com.jniwrapper.win32.ie.event.NavigationEventListener;
import com.jniwrapper.win32.ie.event.NewWindowEventHandlerExt;
import com.jniwrapper.win32.ui.MessageBox;
import com.jniwrapper.win32.ui.Wnd;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;

public class IEBrowser
        extends InternalFrame
        implements NavigationEventListener, Themeable, NewWindowEventHandlerExt {

    private JProgressBar progress;
    private Browser wb;
    private boolean completed;

    public IEBrowser() {
        progress = new JProgressBar();
        this.setLayout(new BorderLayout());
        wb = new Browser();
//        ProxyConfiguration proxyConfiguration = new ProxyConfiguration();
//        proxyConfiguration.setProxy("192.168.2.191:5050", ProxyConfiguration.ServerType.HTTP);
//        wb.setProxy(proxyConfiguration);
        wb.addNavigationListener(this);
        wb.setDialogEventHandler(new BrowserDialogEventHandler() {
            @Override
            public int showDialog(Wnd wnd, String title, String text, int type) {
                if ("JExplorer".equals(title)) {
                    return MessageBox.OK;
                }
                return super.showDialog(wnd, title, text, type);
            }
        });

        this.add(wb, BorderLayout.CENTER);
        progress.setMaximum(1000);
        progress.setMinimum(0);
        progress.setForeground(Theme.getColor("BLACK_COLOR"));
        progress.setPreferredSize(new Dimension(1, 10));
        progress.setBorder(BorderFactory.createEmptyBorder());
        this.setSize(new Dimension(1, 1));
        wb.setNewWindowHandler(this);
        this.add(progress, BorderLayout.SOUTH);
        Theme.registerComponent(this);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setResizable(true);
        setIconifiable(true);
        setClosable(true);
        setMaximizable(true);
        setLayer(10);
        hideTitleBarMenu();
        Client.getInstance().getDesktop().add(this);
    }

    public void setBrowserSize(Dimension size) {
        wb.setPreferredSize(size);
    }

    public void applyTheme() {
        progress.setForeground(Theme.getColor("BLACK_COLOR"));
    }

    public void documentCompleted(WebBrowser webBrowser, String s) {
    }

    public void downloadBegin() {
    }

    public void downloadCompleted() {
    }

    public void entireDocumentCompleted(WebBrowser webBrowser, String s) {
    }

    public void navigationCompleted(WebBrowser webBrowser, String s) {
    }

    public void progressChanged(int /*[in]*/ _Progress, int /*[in]*/ _ProgressMax) {
        progress.setMaximum(_ProgressMax);
        if (_Progress > 0) {
            progress.setValue(_Progress);
        } else {
            progress.setValue(0);
        }
    }

    public synchronized void showLoading(String title) {
        setTitle(title);
        wb.navigate(System.getProperties().get("user.dir")
                + "\\templates\\loading_" + Language.getLanguageTag() + ".htm");
        wb.refresh();
    }

    public synchronized void setURL(String url, String title) {
        setTitle(title);
        showLoading(title);
        completed = false;
        wb.navigate(url);
        setVisible(true);
    }

    private synchronized void idle() {
        try {
            setTitle("");
            wb.navigate("about:blank");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callURL(String url) {
        System.out.println(url);
        try {
            wb.navigate(url);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isCompleted() {
        return completed;
    }

    @Override
    public void internalFrameClosing(InternalFrameEvent e) {
        try {
            wb.stop();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        idle();
    }

    public NewWindowAction newWindow(String s, String s1, NewWindowManagerFlags newWindowManagerFlags) {
        return null;
    }

    public NewWindowAction newWindow() {
        return null;
    }

}
