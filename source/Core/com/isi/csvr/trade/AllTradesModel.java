// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.trade;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.util.TWList;

import javax.swing.event.TableModelListener;
import java.awt.*;
import java.util.Date;

public class AllTradesModel extends CommonTable
        implements CommonTableInterface {

    private FilterableTradeStore g_oQueue;
    private TWList list;
    private DoubleTransferObject doubleTransferObject;
    private LongTransferObject longTransferObject;
    private IntTransferObject intTransferObject;
    private IntTransferObject intTransferObjectOddLot;
    private String defaultString;
    private Date date;
    private int tickMode = 0;
    private TWDateFormat timeFormat;

    /**
     * Constructor
     */
    public AllTradesModel() {
        doubleTransferObject = new DoubleTransferObject();
        longTransferObject = new LongTransferObject();
        intTransferObject = new IntTransferObject();
        intTransferObjectOddLot = new IntTransferObject();
        defaultString = Language.getString("NA");
        timeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));//" HH:mm:ss ");

        date = new Date();
    }

    public void setDataStore(TWList oQueue) { // no usage after filter added
        list = oQueue;
    }

    public void setDataStore(FilterableTradeStore store) {
        g_oQueue = store;
    }

    public FilterableTradeStore getStore() {
        return g_oQueue;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if (g_oQueue != null)
            return g_oQueue.size();
        else
            return 0;
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int iRow, int iCol) {
        boolean isSussesiveTrades = false;
        int priceStatus = 0;
        int volumeStatus = 0;
        int turnOverStatus = 0;

        Trade trade = null;
        Trade oTrade_prev = null;
        if (g_oQueue.getType() == FilterableTradeStore.TYPE_DETAILED) {
            trade = g_oQueue.getCachedTrade(iRow);

        } else {
            trade = g_oQueue.getTrade(g_oQueue.size() - iRow - 1);
            //oTrade_prev = g_oQueue.getPreviousTrade(trade,g_oQueue.size() - iRow - 1 );
        }

        if (trade == null) {
            return "";
        }
        float priceModifFac = ExchangeStore.getSharedInstance().getExchange(trade.getExchange()).getPriceModificationFactor();
        switch (iCol) { //O11A93456S
            case -8:       //return key
                return DataStore.getSharedInstance().getStockObject(trade.getExchange(), trade.getSymbol(), trade.getInstrument(), true).getKey();
            case -7:
                if (g_oQueue.isFilteredByTurnoverMarket()) {
                    if (trade.getTurnover() > g_oQueue.getStartTurnoverMarket() && trade.getTurnover() <= g_oQueue.getEndTurnoverMarket()) {
                        if (trade.getTurnover() < g_oQueue.getBelowTurnoverMarket() && g_oQueue.getBelowTurnoverMarket() != 0) {
                            turnOverStatus = 1; // red
                        } else if (trade.getTurnover() >= g_oQueue.getAboveTurnoverMarket() && g_oQueue.getAboveTurnoverMarket() != 0) {
                            turnOverStatus = 2; //green
                        } else {
                            turnOverStatus = 0;
                        }
                    }
                }
                return turnOverStatus;
            case -6:
                if (g_oQueue.isFilteredByVolumeMarket()) {
                    if (trade.getTradeQuantity() > g_oQueue.getStartVolumeMarket() && trade.getTradeQuantity() <= g_oQueue.getEndVolumeMarket()) {
                        if (trade.getTradeQuantity() < g_oQueue.getBelowVolumeMarket() && g_oQueue.getBelowVolumeMarket() != 0) {
                            volumeStatus = 1; // red
                        } else if (trade.getTradeQuantity() >= g_oQueue.getAboveVolumeMarket() && g_oQueue.getAboveVolumeMarket() != 0) {
                            volumeStatus = 2; //green
                        } else {
                            volumeStatus = 0;
                        }
                    }
                }
                return volumeStatus;
            case -5:
                if (g_oQueue.isFilteredByPriceMarket()) {
                    if (trade.getTradePrice() > g_oQueue.getStartPriceMarket() && trade.getTradePrice() <= g_oQueue.getEndPriceMarket()) {
                        if (trade.getTradePrice() < g_oQueue.getBelowPriceMarket() && g_oQueue.getBelowPriceMarket() != 0.0) {
                            priceStatus = 1; // red
                        } else if (trade.getTradePrice() >= g_oQueue.getAbovePriceMarket() && g_oQueue.getAbovePriceMarket() != 0.0) {
                            priceStatus = 2; //green
                        } else {
                            priceStatus = 0;
                        }
                    }
                }
                return priceStatus;

            case -4:
                return SharedMethods.getDecimalPlaces(trade.getExchange(), trade.getSymbol(), trade.getInstrument());
            case -3:
                if (iRow == 0) {
                    Trade tradeBefore = (Trade) g_oQueue.getTrade(g_oQueue.size() - iRow - 2);
                    if (trade.getSymbol().equals(tradeBefore.getSymbol())) {
                        isSussesiveTrades = true;
                    } else {
                        isSussesiveTrades = false;
                    }
                } else if (iRow > 0 && g_oQueue.size() > 1) {
                    Trade tradeAfter = (Trade) g_oQueue.getTrade(g_oQueue.size() - iRow);
                    if (iRow + 1 != g_oQueue.size()) {
                        Trade tradeBefore = (Trade) g_oQueue.getTrade(g_oQueue.size() - iRow - 2);
                        if ((trade.getSymbol().equals(tradeBefore.getSymbol())) || (trade.getSymbol().equals(tradeAfter.getSymbol()))) {
                            isSussesiveTrades = true;
                        } else {
                            isSussesiveTrades = false;
                        }
                    } else {
                        if ((trade.getSymbol().equals(tradeAfter.getSymbol()))) {
                            isSussesiveTrades = true;
                        } else {
                            isSussesiveTrades = false;
                        }
                    }
                }
                return isSussesiveTrades;
            case -2:
                return g_oQueue.isShowSuccessiveTrades();
            case 0:
                return intTransferObjectOddLot.setValue(trade.getOddlot());
            case 1:
                return trade.getSymbol();
            case 2:
//                return ""+trade.getInstrument();
                return DataStore.getSharedInstance().getShortDescription(trade.getExchange(), trade.getSymbol(), trade.getInstrument());
            case 3:
                return longTransferObject.setValue(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(trade.getExchange(), trade.getTradeTime()));
            case 4:
//                return intTransferObject.setValue(trade.getTick());
                if (g_oQueue.getType() != FilterableTradeStore.TYPE_DETAILED) {
                    if (tickMode == Constants.TICK_MODE_LAST_TRADE) {
//                        return intTransferObject.setValue(getLastTradebasedTick(trade, oTrade_prev));
                        return intTransferObject.setValue(trade.getTick());
                    } else if (tickMode == Constants.TICK_MODE_PREVIOUS_CLOSED) {
//                        return intTransferObject.setValue(getPrevClossedbasedTick(trade, oTrade_prev));
                        return intTransferObject.setValue(trade.getTickPrevClosed());
                    }
                } else {
                    return intTransferObject.setValue(trade.getTick());
                }
            case 5:
                return doubleTransferObject.setValue(trade.getTradePrice());
            case 6:
                return longTransferObject.setValue(trade.getTradeQuantity());
            case 7:
                return doubleTransferObject.setValue(trade.getNetChange());
            case 8:
                return doubleTransferObject.setValue(trade.getPrecentNetChange());
            case 9:
                return intTransferObject.setValue(trade.getSplits());
            case 10:
                return trade.getTicketNumber();
            case 11:
                return trade.getBuyOrderNumber();
            case 12:
                return trade.getSellOrderNumber();
            case 13:
                try {
                    return (ExchangeStore.getSharedInstance().getMarket(trade.getExchange(), (trade.marketID))).getDescription();
                } catch (Exception e) {
                    return "";
                }
            case 14:
                return doubleTransferObject.setValue(trade.getTradeVWAP());
                /*case 15:
                try {
                    String value = trade.getSellerCode();
                    if(value == null){
                        return defaultString;
                    }else{
                        return value;
                    }
                } catch (Exception e) {
                    return defaultString;
                }
            case 16:
                try {
                    String value = SharedMethods.getBrokerTypeString(trade.getSellerType());
                    if(value == null){
                        return defaultString;
                    }else{
                        return value;
                    }
                } catch (Exception e) {
                    return defaultString;
                }
            case 17:
                try {
                    String value = trade.getBuyerCode();
                    if(value == null){
                        return defaultString;
                    }else{
                        return value;
                    }
                } catch (Exception e) {
                    return defaultString;
                }
            case 18:
                try {
                    String value = SharedMethods.getBrokerTypeString(trade.getBuyerType());
                    if(value == null){
                        return defaultString;
                    }else{
                        return value;
                    }
                } catch (Exception e) {
                    return defaultString;*
                }*/
            case 15:
                return intTransferObject.setValue(trade.getSide());
            case 16:
                try {
                    String value = Settings.getMarketCenter(trade.getMarketCenter(), 1);
                    if (value == null) {
                        return defaultString;
                    } else {
                        return value;
                    }
                } catch (Exception e) {
                    return defaultString;
                }
            case 17:
                return doubleTransferObject.setValue(trade.getTurnover() / priceModifFac);   //KSE CR, show value in KWD
            case 18:
                return doubleTransferObject.setValue(trade.getPriceChange());
            default:
                return "";
        }
    }

    private int getLastTradebasedTick(Trade current, Trade previous) {
        int lastTradeBasedTick = Settings.TICK_NOCHANGE;
        float prev_price = 0;
        try {
            prev_price = previous.getTradePrice();
        } catch (Exception e) {
            prev_price = (float) current.previousClosed;
        }
        if (current.getTradePrice() > prev_price) {
            lastTradeBasedTick = Settings.TICK_UP;
        } else if (current.getTradePrice() < prev_price) {
            lastTradeBasedTick = Settings.TICK_DOWN;
        } else {
            try {
                if ((previous.getTick() == Settings.TICK_UP) || (previous.getTick() == Settings.TICK_POSITIVE_EQUAL)) {
                    ;
                    lastTradeBasedTick = Settings.TICK_POSITIVE_EQUAL;
                } else {
                    lastTradeBasedTick = Settings.TICK_NEGATIVE_EQUAL;
                }
            } catch (Exception e) {
                lastTradeBasedTick = Settings.TICK_POSITIVE_EQUAL;
            }
        }
        current.setTick(lastTradeBasedTick);
        return lastTradeBasedTick;

    }

    private int getPrevClossedbasedTick(Trade current, Trade previous) {
        int previousTradeBasedTick = Settings.TICK_NOCHANGE;
        Stock stock;
        double previousClosed = 0.0d;
        try {
            stock = DataStore.getSharedInstance().getStockObject(current.getExchange(), current.getSymbol(), current.getInstrument());
            previousClosed = stock.getPreviousClosed();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if (current.getTradePrice() > previousClosed) {
            previousTradeBasedTick = Settings.TICK_UP;
        } else if (current.getTradePrice() < previousClosed) {
            previousTradeBasedTick = Settings.TICK_DOWN;
        } else {
            try {
                if ((previous.getTickPrevClosed() == Settings.TICK_UP) || (previous.getTickPrevClosed() == Settings.TICK_POSITIVE_EQUAL)) {
                    previousTradeBasedTick = Settings.TICK_POSITIVE_EQUAL;
                } else {
                    previousTradeBasedTick = Settings.TICK_NEGATIVE_EQUAL;
                }
            } catch (Exception e) {
                previousTradeBasedTick = Settings.TICK_POSITIVE_EQUAL;
            }
        }
        current.setTickPrevClosed(previousTradeBasedTick);
        return previousTradeBasedTick;
    }

    public String getValue(int iRow, int iCol) {
        boolean isSussesiveTrades = false;
        int priceStatus = 0;
        int volumeStatus = 0;
        int turnOverStatus = 0;

        Trade trade = null;

        if (g_oQueue.getType() == FilterableTradeStore.TYPE_DETAILED) {
            trade = g_oQueue.getCachedTrade(iRow);

        } else {
            trade = g_oQueue.getTrade(g_oQueue.size() - iRow - 1);
        }

        if (trade == null) {
            return "";
        }
        float priceModifFac = ExchangeStore.getSharedInstance().getExchange(trade.getExchange()).getPriceModificationFactor();
        switch (iCol) { //O11A93456S
//                case -8:       //return key
//                    return DataStore.getSharedInstance().getStockObject(trade.getExchange(),trade.getSymbol(),trade.getInstrument(),true).getKey();
//                case -7:
//                    if (g_oQueue.isFilteredByTurnoverMarket()) {
//                        if (trade.getTurnover() > g_oQueue.getStartTurnoverMarket() && trade.getTurnover() <= g_oQueue.getEndTurnoverMarket()) {
//                            if (trade.getTurnover() < g_oQueue.getBelowTurnoverMarket() && g_oQueue.getBelowTurnoverMarket() != 0) {
//                                turnOverStatus = 1; // red
//                            } else
//                            if (trade.getTurnover() >= g_oQueue.getAboveTurnoverMarket() && g_oQueue.getAboveTurnoverMarket() != 0) {
//                                turnOverStatus = 2; //green
//                            } else {
//                                turnOverStatus = 0;
//                            }
//                        }
//                    }
//                    return ""+turnOverStatus;
//                case -6:
//                    if (g_oQueue.isFilteredByVolumeMarket()) {
//                        if (trade.getTradeQuantity() > g_oQueue.getStartVolumeMarket() && trade.getTradeQuantity() <= g_oQueue.getEndVolumeMarket()) {
//                            if (trade.getTradeQuantity() < g_oQueue.getBelowVolumeMarket() && g_oQueue.getBelowVolumeMarket() != 0) {
//                                volumeStatus = 1; // red
//                            } else
//                            if (trade.getTradeQuantity() >= g_oQueue.getAboveVolumeMarket() && g_oQueue.getAboveVolumeMarket() != 0) {
//                                volumeStatus = 2; //green
//                            } else {
//                                volumeStatus = 0;
//                            }
//                        }
//                    }
//                    return ""+volumeStatus;
//                case -5:
//                    if (g_oQueue.isFilteredByPriceMarket()) {
//                        if (trade.getTradePrice() > g_oQueue.getStartPriceMarket() && trade.getTradePrice() <= g_oQueue.getEndPriceMarket()) {
//                            if (trade.getTradePrice() < g_oQueue.getBelowPriceMarket() && g_oQueue.getBelowPriceMarket() != 0.0) {
//                                priceStatus = 1; // red
//                            } else
//                            if (trade.getTradePrice() >= g_oQueue.getAbovePriceMarket() && g_oQueue.getAbovePriceMarket() != 0.0) {
//                                priceStatus = 2; //green
//                            } else {
//                                priceStatus = 0;
//                            }
//                        }
//                    }
//                    return ""+priceStatus;
//
//                case -4:
//                    return ""+SharedMethods.getDecimalPlaces(trade.getExchange(), trade.getSymbol(), trade.getInstrument());
//                case -3:
//                    if (iRow == 0) {
//                        Trade tradeBefore = (Trade) g_oQueue.getTrade(g_oQueue.size() - iRow - 2);
//                        if (trade.getSymbol().equals(tradeBefore.getSymbol())) {
//                            isSussesiveTrades = true;
//                        } else {
//                            isSussesiveTrades = false;
//                        }
//                    } else if (iRow > 0 && g_oQueue.size() > 1) {
//                        Trade tradeAfter = (Trade) g_oQueue.getTrade(g_oQueue.size() - iRow);
//                        if (iRow + 1 != g_oQueue.size()) {
//                            Trade tradeBefore = (Trade) g_oQueue.getTrade(g_oQueue.size() - iRow - 2);
//                            if ((trade.getSymbol().equals(tradeBefore.getSymbol())) || (trade.getSymbol().equals(tradeAfter.getSymbol()))) {
//                                isSussesiveTrades = true;
//                            } else {
//                                isSussesiveTrades = false;
//                            }
//                        } else {
//                            if ((trade.getSymbol().equals(tradeAfter.getSymbol()))) {
//                                isSussesiveTrades = true;
//                            } else {
//                                isSussesiveTrades = false;
//                            }
//                        }
//                    }
//                    return ""+isSussesiveTrades;
//                case -2:
//                    return ""+g_oQueue.isShowSuccessiveTrades();
            case 0:
                return "" + trade.getOddlot();
            case 1:
                return trade.getSymbol();
            case 2:
//                return ""+trade.getInstrument();

                return DataStore.getSharedInstance().getShortDescription(trade.getExchange(), trade.getSymbol(), trade.getInstrument());
            case 3:
                date.setTime(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(trade.getExchange(), trade.getTradeTime()));
                return timeFormat.format(date);
            case 4:
                if (tickMode == Constants.TICK_MODE_LAST_TRADE) {
                    return getTick(trade.getTick());
                } else if (tickMode == Constants.TICK_MODE_PREVIOUS_CLOSED) {
                    return getTick(trade.getTickPrevClosed());
                }
            case 5:
                return "" + trade.getTradePrice();
            case 6:
                return "" + trade.getTradeQuantity();
            case 7:
                return "" + trade.getNetChange();
            case 8:
                return "" + trade.getPrecentNetChange();
            case 9:
                return "" + trade.getSplits();
            case 10:
                return trade.getTicketNumber();
            case 11:
                return trade.getBuyOrderNumber();
            case 12:
                return trade.getSellOrderNumber();
            case 13:
                try {
                    return (ExchangeStore.getSharedInstance().getMarket(trade.getExchange(), (trade.marketID))).getDescription();
                } catch (Exception e) {
                    return "";
                }
            case 14:
                return "" + trade.getTradeVWAP();
                    /*case 15:
                    try {
                        String value = trade.getSellerCode();
                        if(value == null){
                            return defaultString;
                        }else{
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 16:
                    try {
                        String value = SharedMethods.getBrokerTypeString(trade.getSellerType());
                        if(value == null){
                            return defaultString;
                        }else{
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 17:
                    try {
                        String value = trade.getBuyerCode();
                        if(value == null){
                            return defaultString;
                        }else{
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 18:
                    try {
                        String value = SharedMethods.getBrokerTypeString(trade.getBuyerType());
                        if(value == null){
                            return defaultString;
                        }else{
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;*
                    }*/
            case 15:
                if (trade.getSide() == Constants.TRADE_SIDE_SELL) {
                    return Language.getString("SC_SELL");
                } else if (trade.getSide() == Constants.TRADE_SIDE_BUY) {
                    return Language.getString("STRATEGY_BUY");
                } else {
                    return "";
                }
            case 16:
                try {
                    String value = Settings.getMarketCenter(trade.getMarketCenter(), 1);
                    if (value == null) {
                        return defaultString;
                    } else {
                        return value;
                    }
                } catch (Exception e) {
                    return defaultString;
                }
            case 17:
                return "" + (trade.getTurnover() / priceModifFac);   //KSE CR, show value in KWD
            case 18:
                return "" + (trade.getTradePrice() - trade.getPreviousTradePrice());
            default:
                return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 10:
            case 11:
            case 12:
            case 13:
            case 16:
            case 17:
            case 18:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
            case 14:
            case 15:
            case 19:
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));   //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("ROW_COLOR_1"), FIELD_TIMENSALES_ROW1, Theme.getColor("TIME_N_SALES_TABLE_BGCOLOR1"), Theme.getColor("TIME_N_SALES_TABLE_FGCOLOR1"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("ROW_COLOR_2"), FIELD_TIMENSALES_ROW2, Theme.getColor("TIME_N_SALES_TABLE_BGCOLOR2"), Theme.getColor("TIME_N_SALES_TABLE_FGCOLOR2"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_TIMENSALES_SELECTED_ROW, Theme.getColor("TIME_N_SALES_TABLE_SELECTED_BGCOLOR"), Theme.getColor("TIME_N_SALES_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("SMALL_TRADE"), FIELD_SMALLTRADE_ROW, null, Color.WHITE);
        return customizerRecords;
    }

    public void windowClosing() {

    }

    public int getTickMode() {
        return tickMode;
    }

    public void setTickMode(int tickMode) {
        this.tickMode = tickMode;
    }

    public String getTick(int tick) {
        String tickString = "";
        if (tick == 1) {
            tickString = Language.getString("UP");
        } else if (tick == -1) {
            tickString = Language.getString("DOWN_COLON");
        } else {
            tickString = Language.getString("EQUAL");
        }
        return tickString;
    }
}
