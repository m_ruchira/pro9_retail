package com.isi.csvr.trade;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TradeSummaryPanel extends JPanel implements TradeListener, Runnable {

    private final int PANEL_WIDTH = 300;
    private final int PANEL_HEIGHT = 40;
    boolean isActive = true;
    private JPanel namePanel = new JPanel();
    private JPanel dataPanel = new JPanel();
    private JLabel lblOpen = new JLabel(Language.getString("OPEN"));
    private JLabel lblHigh = new JLabel(Language.getString("HIGH"));
    private JLabel lblLow = new JLabel(Language.getString("LOW"));
    private JLabel lblClose = new JLabel(Language.getString("CLOSE"));
    private JLabel lblVolume = new JLabel(Language.getString("VOLUME"));
    private JLabel dataOpen = new JLabel("0");
    private JLabel dataHigh = new JLabel("0");
    private JLabel dataLow = new JLabel("0");
    private JLabel dataClose = new JLabel("0");
    private JLabel dataVolume = new JLabel("0");
    private TWDecimalFormat priceFormat = new TWDecimalFormat("###,##0.00");
    private TWDecimalFormat volumeFormat = new TWDecimalFormat("###,##0");
    private String symbol;
    private FilterableTradeStore store;
    private Stock stock = null;

    public TradeSummaryPanel(String symbol) {
        this.symbol = symbol;
        stock = DataStore.getSharedInstance().getStockObject(symbol);
        priceFormat = SharedMethods.getDecimalFormat(stock.getDecimalCount());
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (stock != null) {
            setDecimalPlaces(stock.getDecimalCount());
        }
    }

    public TradeSummaryPanel(FilterableTradeStore store) {
        this.store = store;
        symbol = null;
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        setVisible(false);
    }

    public void changeSymbol(String sKey) {
        symbol = null;
        stock = DataStore.getSharedInstance().getStockObject(sKey);
        symbol = sKey;
        if (stock != null) {
            setDecimalPlaces(stock.getDecimalCount());
        }

    }

    void jbInit() throws Exception {
        GUISettings.applyOrientation(this);
        this.setMaximumSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        this.setMinimumSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        this.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        this.setLayout(new GridLayout(2, 1));
        namePanel.setLayout(new GridLayout(1, 5, 2, 2));
        dataPanel.setLayout(new GridLayout(1, 5, 2, 2));
        this.add(namePanel, null);
        this.add(dataPanel, null);

        namePanel.add(lblOpen);
        namePanel.add(lblHigh);
        namePanel.add(lblLow);
        namePanel.add(lblClose);
        namePanel.add(lblVolume);

        dataPanel.add(dataOpen);
        dataPanel.add(dataHigh);
        dataPanel.add(dataLow);
        dataPanel.add(dataClose);
        dataPanel.add(dataVolume);

        dataOpen.setHorizontalAlignment(JLabel.TRAILING);
        dataHigh.setHorizontalAlignment(JLabel.TRAILING);
        dataLow.setHorizontalAlignment(JLabel.TRAILING);
        dataClose.setHorizontalAlignment(JLabel.TRAILING);
        dataVolume.setHorizontalAlignment(JLabel.TRAILING);

        lblOpen.setHorizontalAlignment(JLabel.TRAILING);
        lblHigh.setHorizontalAlignment(JLabel.TRAILING);
        lblLow.setHorizontalAlignment(JLabel.TRAILING);
        lblClose.setHorizontalAlignment(JLabel.TRAILING);
        lblVolume.setHorizontalAlignment(JLabel.TRAILING);
        if (symbol != null)
            setvalues();
        else {
            if (store.isFilteredBySymbol()) {
                dataOpen.setText(priceFormat.format(store.getOpen()));
                dataHigh.setText(priceFormat.format(store.getHigh()));
                dataLow.setText(priceFormat.format(store.getLow()));
                dataClose.setText(priceFormat.format(store.getClose()));
                dataVolume.setText(volumeFormat.format(store.getTotalVolume()));
            }
        }

    }

    public void setDecimalPlaces(int decimalPlaces) {
        priceFormat = SharedMethods.getDecimalFormat(decimalPlaces);
    }

    public void newTradeAdded(Object reference, FilterableTradeStore store, Trade trade) {
        if (symbol == null) {
            if (store.isFilteredBySymbol()) {
                dataOpen.setText(priceFormat.format(this.store.getOpen()));
                dataHigh.setText(priceFormat.format(this.store.getHigh()));
                dataLow.setText(priceFormat.format(this.store.getLow()));
                dataClose.setText(priceFormat.format(this.store.getClose()));
                dataVolume.setText(volumeFormat.format(this.store.getTotalVolume()));
            }
        }
    }

    public void setvalues() {
        if (symbol != null) {
            if (stock.getPreviousClosed() == 0.0) {
                stock = DataStore.getSharedInstance().getStockObject(this.symbol);
            }
            dataOpen.setText(priceFormat.format(stock.getTodaysOpen()));
            dataHigh.setText(priceFormat.format(stock.getHigh()));
            dataLow.setText(priceFormat.format(stock.getLow()));
            dataClose.setText(priceFormat.format(stock.getPreviousClosed()));
            dataVolume.setText(volumeFormat.format(stock.getVolume()));
        } else {
            if (store.isFilteredBySymbol()) {
                dataOpen.setText(priceFormat.format(store.getOpen()));
                dataHigh.setText(priceFormat.format(store.getHigh()));
                dataLow.setText(priceFormat.format(store.getLow()));
                dataClose.setText(priceFormat.format(store.getClose()));
                dataVolume.setText(volumeFormat.format(store.getTotalVolume()));
            }
        }
    }

    public void run() {
        while (isActive) {
            setvalues();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void setInactive() {
        isActive = false;
    }
}