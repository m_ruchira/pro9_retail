// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.trade;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;


public class TimeAndSalesRenderer extends TWBasicTableRenderer {
    private static Color g_oUpColor;
    private static Color g_oDownColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color smallTradeFG;
    private static Color above_bgColor;
    private static Color below_bgColor;
    private final SuccessiveTradeImage tradeImage;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private TWDateFormat g_oDateFormat;
    private TWDateFormat g_oDateTimeFormatHM;
    private TWDateFormat g_oDateTimeFormatHMS;
    private TWDateFormat g_oTimeFormat;
    private String g_sNA = "NA";
    private Color upColor;
    private Color downColor;
    private ImageIcon g_oUPImage = null;
    private ImageIcon g_oDownImage = null;
    private ImageIcon g_oEqualImage = null;
    private ImageIcon negEqualImage = null;
    private Color foreground, background;
    private TWDecimalFormat defaultPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
    private TWDecimalFormat oNumericFormat = new TWDecimalFormat(" ########## ");
    private DefaultTableCellRenderer lblRenderer;
    private double doubleValue;
    private long longValue;
    private Long longObjectValue;
    private Date date;
    private String TRADE_SIDE_BUY;
    private String TRADE_SIDE_SELL;
    private boolean isSussessiveTrades = false;
    private boolean isSussessiveTradesEnabled = false;

    private int priceValidator = 0;
    private int quantityValidator = 0;
    private int volumeValidator = 0;

    private TWDecimalFormat percentageChangeFormat = new TWDecimalFormat(" ###,##0.00 ");


    public TimeAndSalesRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;

        reload();

        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HM_FORMAT"));//" yyyy/MM/dd HH:mm ");
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));//" dd:MM:yyyy '-' HH:mm:ss ");
        g_oTimeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));//" HH:mm:ss ");

        date = new Date();
        tradeImage = new SuccessiveTradeImage();

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;

        try {
            g_oUPImage = new ImageIcon("images/Common/up.gif");
        } catch (Exception e) {
            g_oUPImage = null;
        }
        try {
            g_oDownImage = new ImageIcon("images/Common/down.gif");
        } catch (Exception e) {
            g_oDownImage = null;
        }
        try {
            g_oEqualImage = new ImageIcon("images/Common/equal_green.gif");
        } catch (Exception e) {
            g_oEqualImage = null;
        }
        try {
            negEqualImage = new ImageIcon("images/Common/equal_red.gif");
        } catch (Exception e) {
            negEqualImage = null;
        }

        TRADE_SIDE_SELL = Language.getString("TRADE_SIDE_SELL");
        TRADE_SIDE_BUY = Language.getString("TRADE_SIDE_BUY");
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("TIME_N_SALES_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("TIME_N_SALES_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("TIME_N_SALES_TABLE_FGCOLOR1");
            g_oBG1 = Theme.getColor("TIME_N_SALES_TABLE_BGCOLOR1");
            g_oFG2 = Theme.getColor("TIME_N_SALES_TABLE_FGCOLOR2");
            g_oBG2 = Theme.getColor("TIME_N_SALES_TABLE_BGCOLOR2");
            smallTradeFG = Theme.getColor("SMALL_TRADE_FG_COLOR");
            above_bgColor = Theme.getColor("TNS_ABOVE_BG_COLOR");
            below_bgColor = Theme.getColor("TNS_BELOW_BG_COLOR");

        } catch (Exception e) {
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void reloadForPrinting() {
        g_oUpColor = Color.black;
        g_oDownColor = Color.black;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void propertyChanged(int property) {
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();

        CommonTableSettings sett;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getTimeNSalesSelectedColorFG();
                background = sett.getTimeNSalesSelectedColorBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getTimeNSalesRowColor1FG();
                    background = sett.getTimeNSalesRowColor1BG();
                } else {
                    foreground = sett.getTimeNSalesRowColor2FG();
                    background = sett.getTimeNSalesRowColor2BG();
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
            if ((table.getModel().getValueAt(row, 0)).equals("1")) {
                foreground = sett.getSmallTradeFG();
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }

            try {
                if (((IntTransferObject) (table.getModel().getValueAt(row, 0))).getValue() == 1) {
                    foreground = smallTradeFG;
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            upColor = g_oUpColor;
            downColor = g_oDownColor;
        }
        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        tradeImage.setHeight(table.getRowHeight());

        try {
            oPriceFormat = SharedMethods.getDecimalFormat((Byte) table.getModel().getValueAt(row, -4));
        } catch (Exception e) {
            e.printStackTrace();
            oPriceFormat = defaultPriceFormat;
        }

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    isSussessiveTradesEnabled = (Boolean) table.getModel().getValueAt(row, -2);
                    if (isSussessiveTradesEnabled) {
                        try {
                            isSussessiveTrades = (Boolean) table.getModel().getValueAt(row, -3);
                        } catch (Exception e) {
                            isSussessiveTrades = false;
                        }
                        if (isSussessiveTrades) {
                            lblRenderer.setIcon(tradeImage);
                            if (Language.isLTR()) {
                                lblRenderer.setHorizontalTextPosition(JLabel.RIGHT);
                            } else {
                                lblRenderer.setHorizontalTextPosition(JLabel.LEADING);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);

                    } else {
                        if (Settings.isShowArabicNumbers()) {
                            lblRenderer.setText(GUISettings.arabize((String) value));
                        } else {
                            lblRenderer.setText((String) value);
                        }
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    }
                    break;
                case 2: // DESCRIPTION
                    if (Settings.isShowArabicNumbers()) {
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    } else {
                        lblRenderer.setText((String) value);
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    priceValidator = (Integer) table.getModel().getValueAt(row, -5);
                    if (priceValidator == 1) {
                        lblRenderer.setBackground(below_bgColor);
                    } else if (priceValidator == 2) {
                        lblRenderer.setBackground(above_bgColor);
                    }
                    lblRenderer.setText(oPriceFormat.format(toDoubleValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'N': // Numeric
                    lblRenderer.setText(oNumericFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    quantityValidator = (Integer) table.getModel().getValueAt(row, -6);
                    if (quantityValidator == 1) {
                        lblRenderer.setBackground(below_bgColor);
                    } else if (quantityValidator == 2) {
                        lblRenderer.setBackground(above_bgColor);
                    }
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'S': // Splits
                    lblRenderer.setText(oQuantityFormat.format(toIntValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 5: // CHANGE
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 6: // % CHANGE
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(percentageChangeFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 7: // DATE
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 'T': // TurnOver
                    volumeValidator = (Integer) table.getModel().getValueAt(row, -7);
                    if (volumeValidator == 1) {
                        lblRenderer.setBackground(below_bgColor);
                    } else if (volumeValidator == 2) {
                        lblRenderer.setBackground(above_bgColor);
                    }
                    lblRenderer.setText(oPriceFormat.format(toDoubleValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'L': // LONG DATE for Net change calculator
                    longObjectValue = (Long) value;
                    if (longObjectValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longObjectValue);
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 8: {// DATE TIME
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHM.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                }
                case 'D': {// DATE TIME with secs
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHMS.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                }
                case 'A': // TIME
                    longValue = toLongValue(value);
                    if (longValue == 0) {
                        lblRenderer.setText(g_sNA);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oTimeFormat.format(date));
                        lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    }
                    break;
                case 'I': // Side
                    longValue = toIntValue(value);
                    if (longValue == Constants.TRADE_SIDE_SELL) {
                        lblRenderer.setForeground(downColor);
                        lblRenderer.setText(TRADE_SIDE_SELL);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else if (longValue == Constants.TRADE_SIDE_BUY) {
                        lblRenderer.setForeground(upColor);
                        lblRenderer.setText(TRADE_SIDE_BUY);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        lblRenderer.setText("");
                    }
                    break;
                case 'O': // odd lot (ignore)
                    break;
                case 9: // TICK
                    int iValue = toIntValue(value);
                    switch (iValue) {
                        case Settings.TICK_UP:
                            lblRenderer.setIcon(g_oUPImage);
                            lblRenderer.setText("");
                            break;
                        case Settings.TICK_DOWN:
                            lblRenderer.setIcon(g_oDownImage);
                            lblRenderer.setText("");
                            break;
                        case Settings.TICK_POSITIVE_EQUAL:
                            lblRenderer.setIcon(g_oEqualImage);
                            lblRenderer.setText("");
                            break;
                        case Settings.TICK_NEGATIVE_EQUAL:
                            lblRenderer.setIcon(negEqualImage);
                            lblRenderer.setText("");
                            break;
                        default:
                            lblRenderer.setIcon(null);
                            lblRenderer.setText("");
                    }
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return ((LongTransferObject) oValue).getValue();
    }

    private int toIntValue(Object oValue) throws Exception {
        return ((IntTransferObject) oValue).getValue();
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return ((DoubleTransferObject) oValue).getValue();
    }
}