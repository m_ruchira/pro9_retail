package com.isi.csvr.trade.ui;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.trade.DetailedTradeStore;
import com.isi.csvr.trade.FilterableTradeStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TradeFilterUI extends JDialog implements ActionListener {
    public static boolean isUpdatingCache = false;
    private JPanel contentPanel = new JPanel();
    private JLabel lblFilterType = new JLabel();
    private JLabel lblFrom = new JLabel();
    private JLabel lblTo = new JLabel();
    private JTextField txtFromPrice = new JTextField();
    private JTextField txtToPrice = new JTextField();
    private TimeField txtFromTime = new TimeField();
    private TimeField txtToTime = new TimeField();
    private JTextField txtFromVolume = new JTextField();
    private JTextField txtToVolume = new JTextField();
    private JTextField txtFromTurnover = new JTextField();
    private JTextField txtToTurnover = new JTextField();
    private JCheckBox chkPrice = new JCheckBox();
    private JCheckBox chkSymbol = new JCheckBox();
    private JCheckBox chkTime = new JCheckBox();
    private JCheckBox chkVolume = new JCheckBox();
    private JCheckBox chkTurnover = new JCheckBox();
    private TWButton btnOk = new TWButton();
    private TWButton btnCancel = new TWButton();
    private TWTextField companySelector;
    private JLabel dummy = new JLabel();
    private FlexGridLayout panelLayout;
    //    private CompanySelector companySelector;;
    private FilterableTradeStore store = null;

    public TradeFilterUI(Frame parent, String title, FilterableTradeStore store) {
        super(parent, title, Dialog.ModalityType.APPLICATION_MODAL);
//        super((Window) SwingUtilities.getAncestorOfClass(Window.class, parent), title, Dialog.ModalityType.APPLICATION_MODAL);
        this.store = store;
        try {
            jbInit();
            setLocationRelativeTo(parent);
            loadDefaults();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void jbInit() throws Exception {
        String[] widths = {"80", "80", "80"};
        String[] heights = {"25", "25", "25", "25", "25", "25", "25"};
        panelLayout = new FlexGridLayout(widths, heights, 10, 10);
        contentPanel.setLayout(panelLayout);
//        setSize(290, 210);
        setSize(290, 285);
        setResizable(false);

        companySelector = new TWTextField();
        /*CompanySelectorModel model = new CompanySelectorModel(Client.companyList);
        companySelector = new CompanySelector(model);
        companySelector.setModel(model);
        companySelector.setEditable(true);
        companySelector.setEditor(new TWTextField());
        CompanySelectorRenderer renderer = new CompanySelectorRenderer();
        companySelector.setRenderer(renderer);*/

        chkSymbol.setText(Language.getString("SYMBOL"));
        chkSymbol.setBounds(new Rectangle(5, 30, 84, 32));
        chkSymbol.addActionListener(this);
        chkSymbol.setActionCommand("SYMBOL");
        lblFilterType.setText(Language.getString("FILTER_BY"));
        //lblFilterType.setBounds(new Rectangle(5, 0, 84, 32));
        companySelector.setBounds(new Rectangle(95, 35, 181, 21));
        lblFrom.setText(Language.getString("FROM"));
        //lblFrom.setBounds(new Rectangle(95, 53, 87, 32));
        lblTo.setText(Language.getString("TO"));
        //lblTo.setBounds(new Rectangle(192, 53, 87, 32));
        // txtFromPrice.setBounds(new Rectangle(95, 87, 84, 21));
        // txtToPrice.setBounds(new Rectangle(192, 87, 84, 21));

        //txtFromTime.setBounds(new Rectangle(95, 124, 84, 21));
        // txtToTime.setBounds(new Rectangle(192, 124, 84, 21));
        chkPrice.setText(Language.getString("PRICE"));
        //chkPrice.setBounds(new Rectangle(5, 87, 81, 21));
        chkPrice.addActionListener(this);
        chkPrice.setActionCommand("PRICE");
        chkTime.setText(Language.getString("TIME"));
        //chkTime.setBounds(new Rectangle(5, 124, 87, 22));
        chkTime.addActionListener(this);
        chkTime.setActionCommand("TIME");

        //------------- Added by Shanika--- V-8.93-------
        chkVolume.setText(Language.getString("QUANTITY"));
        chkVolume.addActionListener(this);
        chkVolume.setActionCommand("VOLUME");

        chkTurnover.setText(Language.getString("TURNOVER"));
        chkTurnover.addActionListener(this);
        chkTurnover.setActionCommand("TURNOVER");

        //----------------------
        // btnOk.setBounds(new Rectangle(192, 163, 84, 21));
        btnOk.setActionCommand("OK");
        btnOk.setText(Language.getString("OK"));
        btnOk.addActionListener(this);
        //btnCancel.setBounds(new Rectangle(95, 163, 84, 21));
        btnCancel.setText(Language.getString("CANCEL"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.addActionListener(this);
        getContentPane().add(contentPanel);
        contentPanel.add(companySelector);

        contentPanel.add(lblFilterType);
        contentPanel.add(lblFrom);
        contentPanel.add(lblTo);

        contentPanel.add(chkSymbol);
        contentPanel.add(companySelector);
        contentPanel.add(dummy); // dunmmy

        contentPanel.add(chkPrice);
        txtFromPrice.setHorizontalAlignment(JTextField.RIGHT);
        contentPanel.add(txtFromPrice);
        txtToPrice.setHorizontalAlignment(JTextField.RIGHT);
        contentPanel.add(txtToPrice);

        contentPanel.add(chkTime);
        contentPanel.add(txtFromTime);
        contentPanel.add(txtToTime);

        contentPanel.add(chkVolume);
        txtFromVolume.setHorizontalAlignment(JTextField.RIGHT);
        contentPanel.add(txtFromVolume);
        txtToVolume.setHorizontalAlignment(JTextField.RIGHT);
        contentPanel.add(txtToVolume);

        contentPanel.add(chkTurnover);
        txtFromTurnover.setHorizontalAlignment(JTextField.RIGHT);
        contentPanel.add(txtFromTurnover);
        txtToTurnover.setHorizontalAlignment(JTextField.RIGHT);
        contentPanel.add(txtToTurnover);

        contentPanel.add(new JLabel()); // dunmmy
        contentPanel.add(btnOk);
        contentPanel.add(btnCancel);

        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        GUISettings.applyOrientation(contentPanel);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public boolean isInputValid() {

        boolean sucess = false;
        try {
            float price1 = 0.0F;
/*if (chkSymbol.isSelected()){
                if ( (companySelector.getSymbol() == null) ||
                        (companySelector.getSymbol().trim().equals("")) ||
                        (!DataStore.getSharedInstance().isValidSymbol(companySelector.getSymbol().trim()))){
                    showErrorMessage(Language.getString("INVALID_SYMBOL"));
				    return sucess;
                }
            }*/
            if (txtFromPrice.getText().trim().startsWith("-") && chkPrice.isSelected()) {
                showErrorMessage(Language.getString("INVALID_PRICE"));
                txtFromPrice.requestFocus();
                return sucess;
            }

            if (txtToPrice.getText().trim().startsWith("-") && chkPrice.isSelected()) {
                showErrorMessage(Language.getString("INVALID_PRICE"));
                txtToPrice.requestFocus();
                return sucess;
            }

            if (txtFromVolume.getText().trim().startsWith("-") && chkVolume.isSelected()) {
                showErrorMessage(Language.getString("INVALID_VOLUME"));
                txtFromVolume.requestFocus();
                return sucess;
            }

            if (txtToVolume.getText().trim().startsWith("-") && chkVolume.isSelected()) {
                showErrorMessage(Language.getString("INVALID_VOLUME"));
                txtToVolume.requestFocus();
                return sucess;
            }

            if (txtFromTurnover.getText().trim().startsWith("-") && chkTurnover.isSelected()) {
                showErrorMessage(Language.getString("INVALID_TURNOVER"));
                txtFromTurnover.requestFocus();
                return sucess;
            }

            if (txtToTurnover.getText().trim().startsWith("-") && chkTurnover.isSelected()) {
                showErrorMessage(Language.getString("INVALID_TURNOVER"));
                txtToTurnover.requestFocus();
                return sucess;
            }

            try {
                if (chkPrice.isSelected()) {
                    price1 = Float.parseFloat(txtFromPrice.getText());
                    if ((price1 < 0)) {
                        showErrorMessage(Language.getString("INVALID_PRICE"));
                        txtFromPrice.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("INVALID_PRICE"));
                txtFromPrice.requestFocus();
                return sucess;
            }

            float price2 = 0.0F;
            try {
                if (chkPrice.isSelected()) {
                    price2 = Float.parseFloat(txtToPrice.getText());
                    if ((price2 < 0)) {
                        showErrorMessage(Language.getString("INVALID_PRICE"));
                        txtToPrice.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("INVALID_PRICE"));
                txtToPrice.requestFocus();
                return sucess;
            }

            try {
                if (chkPrice.isSelected()) {
                    if ((price1 > price2)) {
                        showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                        txtFromPrice.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                txtFromPrice.requestFocus();
                return sucess;
            }

            if (((!txtFromTime.isValidTime()) || (!txtToTime.isValidTime())) && chkTime.isSelected()) {
                showErrorMessage(Language.getString("MSG_INVALID_TIME"));
                return sucess;
            }

            //----------- Shanika------------
            long vol1 = 0;
            try {
                if (chkVolume.isSelected()) {
                    vol1 = Long.parseLong(txtFromVolume.getText());
                    if ((vol1 < 0)) {
                        showErrorMessage(Language.getString("INVALID_VOLUME"));
                        txtFromVolume.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("INVALID_VOLUME"));
                txtFromVolume.requestFocus();
                return sucess;
            }

            long vol2 = 0;
            try {
                if (chkVolume.isSelected()) {
                    vol2 = Long.parseLong(txtToVolume.getText());
                    if ((vol2 < 0)) {
                        showErrorMessage(Language.getString("INVALID_VOLUME"));
                        txtToVolume.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("INVALID_VOLUME"));
                txtToVolume.requestFocus();
                return sucess;
            }

            try {
                if (chkVolume.isSelected()) {
                    if ((vol1 > vol2)) {
                        showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                        txtFromVolume.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                txtFromVolume.requestFocus();
                return sucess;
            }

            double turnovr1 = 0;
            try {
                if (chkTurnover.isSelected()) {
                    turnovr1 = Double.parseDouble(txtFromTurnover.getText());
                    if ((turnovr1 < 0)) {
                        showErrorMessage(Language.getString("INVALID_TURNOVER"));
                        txtFromTurnover.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("INVALID_TURNOVER"));
                txtFromTurnover.requestFocus();
                return sucess;
            }
            double turnovr2 = 0;
            try {
                if (chkTurnover.isSelected()) {
                    turnovr2 = Double.parseDouble(txtToTurnover.getText());
                    if ((turnovr2 < 0)) {
                        showErrorMessage(Language.getString("INVALID_TURNOVER"));
                        txtToTurnover.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("INVALID_TURNOVER"));
                txtToTurnover.requestFocus();
                return sucess;
            }

            try {
                if (chkTurnover.isSelected()) {
                    if ((turnovr1 > turnovr2)) {
                        showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                        txtFromTurnover.requestFocus();
                        return sucess;
                    }
                }
            } catch (NumberFormatException ex) {
                showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                txtFromTurnover.requestFocus();
                return sucess;
            }

            sucess = true;
            return sucess;
        } catch (Exception ex) {
            return sucess;
        }
    }

    private void showErrorMessage(String message) {
        ShowMessage oMessage = new ShowMessage(this, message, "W", false);

    }

    public void loadDefaults() {
        chkTime.setSelected(store.isFilteredByDate());
        chkPrice.setSelected(store.isFilteredByPrice());
        chkSymbol.setSelected(store.isFilteredBySymbol());
        chkVolume.setSelected(store.isFilteredByVolume());
        chkTurnover.setSelected(store.isFilteredByTurnover());

        txtFromPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtToPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));

        txtFromVolume.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtToVolume.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));

        txtFromTurnover.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtToTurnover.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));

        companySelector.setText(store.getSymbol());

        if (store.isFilteredByPrice()) {
            txtFromPrice.setText("" + store.getStartPrice());
            txtToPrice.setText("" + store.getEndPrice());
        } else {
            txtFromPrice.setText("0.0");
            txtToPrice.setText("0.0");
        }
        txtFromTime.setTime(store.getStartDate());
        txtToTime.setTime(store.getEndDate());

        txtFromPrice.setEditable(chkPrice.isSelected());
        txtToPrice.setEditable(chkPrice.isSelected());

        if (store.isFilteredByVolume()) {
            txtFromVolume.setText("" + store.getStartVolume());
            txtToVolume.setText("" + store.getEndVolume());
        } else {
            txtFromVolume.setText("0");
            txtToVolume.setText("0");
        }
        txtFromVolume.setEditable(chkVolume.isSelected());
        txtToVolume.setEditable(chkVolume.isSelected());

        if (store.isFilteredByTurnover()) {
            txtFromTurnover.setText("" + store.getStartTurnover());
            txtToTurnover.setText("" + store.getEndTurnover());
        } else {
            txtFromTurnover.setText("0");
            txtToTurnover.setText("0");
        }
        txtFromTurnover.setEditable(chkTurnover.isSelected());
        txtToTurnover.setEditable(chkTurnover.isSelected());

//        companySelector.setEnabled((store.getType() == FilterableTradeStore.TYPE_DETAILED) && chkSymbol.isSelected());
        if ((store.getType() == FilterableTradeStore.TYPE_DETAILED)) {
            panelLayout.setRowHeight(1, "25");
            chkSymbol.setVisible(true);
            companySelector.setVisible(true);
            dummy.setVisible(true);
            setSize(290, 285);
            contentPanel.revalidate();
        } else {
            panelLayout.setRowHeight(1, "1");
            chkSymbol.setVisible(false);
            companySelector.setVisible(false);
            dummy.setVisible(false);
            setSize(290, 260);
            contentPanel.revalidate();
//          contentPanel.add(new JLabel());
        }
        SharedMethods.updateComponent(contentPanel);
        chkSymbol.setEnabled((store.getType() == FilterableTradeStore.TYPE_DETAILED));

        txtFromTime.setEditable(chkTime.isSelected());
        txtToTime.setEditable(chkTime.isSelected());

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            if (isInputValid()) {
                if (chkTime.isSelected()) {
                    store.setStartDate(txtFromTime.getTime());
                    store.setEndDate(txtToTime.getTime());
                }
                if (chkPrice.isSelected()) {
                    store.setStartPrice(SharedMethods.floatValue(txtFromPrice.getText(), 0));
                    store.setEndPrice(SharedMethods.floatValue(txtToPrice.getText(), 0));
                }
                if (chkVolume.isSelected()) {
                    store.setStartVolume(SharedMethods.longValue(txtFromVolume.getText(), 0));
                    store.setEndVolume(SharedMethods.longValue(txtToVolume.getText(), 0));
                }
                if (chkTurnover.isSelected()) {
                    store.setStartTurnover(SharedMethods.doubleValue(txtFromTurnover.getText(), 0));
                    store.setEndTurnover(SharedMethods.doubleValue(txtToTurnover.getText(), 0));
                }

                if (chkPrice.isSelected()) {
                    store.setFilteredByPrice(true);
                } else {
                    store.setFilteredByPrice(false);
                }
                if (chkTime.isSelected()) {
                    store.setFilteredByDate(true);
                } else {
                    store.setFilteredByDate(false);
                }
                if (chkVolume.isSelected()) {
                    store.setFilteredByVolume(true);
                } else {
                    store.setFilteredByVolume(false);
                }
                if (chkTurnover.isSelected()) {
                    store.setFilteredByTurnover(true);
                } else {
                    store.setFilteredByTurnover(false);
                }
                if (chkSymbol.isSelected() && !companySelector.getText().equals("") && companySelector.getText() != null) {
                    store.setFilteredBySymbol(true);
                    store.setSymbol(companySelector.getText());
                } else {
                    store.setFilteredBySymbol(false);
                    store.setSymbol(null);
                }
//                store.clear(false);
                if (store.getType() == FilterableTradeStore.TYPE_DETAILED) {
                    new Thread() {
                        public void run() {
                            DetailedTradeStore.isWritingCache = true;
                            store.clearCachedStroe(false);
                            store.clearAndRefilterCache();
                            store.reload();
                            DetailedTradeStore.isWritingCache = false;
                            Client.getInstance().updateDetailedTrades();
                        }
                    }.start();
                } else {
                    store.clear(false);
                    store.reload();
                }
                hide();
            }
        } else if (e.getActionCommand().equals("CANCEL")) {
            hide();
        }
        if (e.getActionCommand().equals("PRICE")) {
            if (!chkPrice.isSelected()) {
                txtFromPrice.setEditable(false);
                txtToPrice.setEditable(false);
            } else {
                txtFromPrice.setEditable(true);
                txtToPrice.setEditable(true);
            }
        }
        if (e.getActionCommand().equals("TIME")) {
            if (!chkTime.isSelected()) {
                txtFromTime.setEditable(false);
                txtToTime.setEditable(false);
            } else {
                txtFromTime.setEditable(true);
                txtToTime.setEditable(true);
            }
        }
        if (e.getActionCommand().equals("VOLUME")) {
            if (!chkVolume.isSelected()) {
                txtFromVolume.setEditable(false);
                txtToVolume.setEditable(false);
            } else {
                txtFromVolume.setEditable(true);
                txtToVolume.setEditable(true);
            }
        }
        if (e.getActionCommand().equals("TURNOVER")) {
            if (!chkTurnover.isSelected()) {
                txtFromTurnover.setEditable(false);
                txtToTurnover.setEditable(false);
            } else {
                txtFromTurnover.setEditable(true);
                txtToTurnover.setEditable(true);
            }
        }

        if (e.getActionCommand().equals("SYMBOL")) {
            companySelector.setEnabled(chkSymbol.isSelected());
        }
    }
}