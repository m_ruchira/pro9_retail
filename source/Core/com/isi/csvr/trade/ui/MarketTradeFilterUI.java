package com.isi.csvr.trade.ui;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.shared.*;
import com.isi.csvr.trade.AllTradesModel;
import com.isi.csvr.trade.FilterableTradeStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Apr 17, 2009
 * Time: 3:38:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarketTradeFilterUI extends JDialog implements ActionListener, KeyListener, MouseListener {
    private JPanel topPanel = new JPanel();
    private JPanel topUpPanel = new JPanel();
    private JPanel topDownPanel = new JPanel();
    private JPanel middlePanel = new JPanel();
    private JPanel middleTopPanel = new JPanel();
    private JPanel middleDownPanel = new JPanel();
    private JPanel downPanel = new JPanel();
    private JPanel downRightPanel = new JPanel();
    private JPanel downLeftPanel = new JPanel();

    private JLabel lblFilterBy = new JLabel();
    private JLabel lblViewTrades = new JLabel();
    private JLabel lblShowAddMark = new JLabel();

    private JLabel lblFrom = new JLabel();
    private JLabel lblTo = new JLabel();
    private JLabel lblAbove = new JLabel();
    private JLabel lblBelow = new JLabel();

    private JCheckBox chkPrice = new JCheckBox(); // -- Price
    private JTextField txtFromPrice = new JTextField();
    private JTextField txtToPrice = new JTextField();
    private JTextField txtAbovePrice = new JTextField();
    private JTextField txtBelowPrice = new JTextField();

    private JCheckBox chkVolume = new JCheckBox(); //-- Quantity
    private JTextField txtFromVolume = new JTextField();
    private JTextField txtToVolume = new JTextField();
    private JTextField txtAboveVolume = new JTextField();
    private JTextField txtBelowVolume = new JTextField();

    private JCheckBox chkTurnover = new JCheckBox(); //---Value
    private JTextField txtFromTurnover = new JTextField();
    private JTextField txtToTurnover = new JTextField();
    private JTextField txtAboveTurnover = new JTextField();
    private JTextField txtBelowTurnover = new JTextField();

    private JCheckBox chkSuccessiveTrades = new JCheckBox();
    private boolean showSuccessiveTrades = false;


    private JRadioButton radioMatchAll;
    private JRadioButton radioMatchAny;
    private TWButton okBtn;
    private TWButton cancelBtn;

    private FilterableTradeStore store = null;
    private AllTradesModel model;

    public MarketTradeFilterUI(Frame parent, String title, FilterableTradeStore store, AllTradesModel model) {
        super((Window) SwingUtilities.getAncestorOfClass(Window.class, parent), title, Dialog.ModalityType.APPLICATION_MODAL);
        this.store = store;
        this.model = model;
        try {
            jbInit();
            setLocationRelativeTo(parent);
            loadDefaults();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void jbInit() throws Exception {
        String[] widths = {"80", "80", "80", "80", "80"};
        String[] heights = {"25", "25", "25"};
        middleTopPanel.setLayout(new FlexGridLayout(widths, heights, 10, 10));

        lblFilterBy.setFont(new TWFont("Arial", Font.BOLD, 12));
        lblFilterBy.setText(Language.getString("TNS_FILTER_BY"));
        lblViewTrades.setFont(new TWFont("Arial", Font.BOLD, 12));
        lblViewTrades.setText(Language.getString("TNS_VIEW_TRADES"));
        lblShowAddMark.setFont(new TWFont("Arial", Font.BOLD, 12));
        lblShowAddMark.setText(Language.getString("TNS_SHOW_ADD_MARK"));

        lblFrom.setFont(new TWFont("Arial", Font.BOLD, 12));
        lblFrom.setText(Language.getString("FROM"));
        lblTo.setFont(new TWFont("Arial", Font.BOLD, 12));
        lblTo.setText(Language.getString("TO"));
        lblAbove.setFont(new TWFont("Arial", Font.BOLD, 12));
        lblAbove.setText(Language.getString("MARKET_TNS_ABOVE"));
        lblBelow.setFont(new TWFont("Arial", Font.BOLD, 12));
        lblBelow.setText(Language.getString("MARKET_TNS_BELOW"));

        chkPrice.setText(Language.getString("PRICE"));
        chkPrice.addActionListener(this);
        chkPrice.setActionCommand("PRICE");

        chkVolume.setText(Language.getString("QUANTITY"));
        chkVolume.addActionListener(this);
        chkVolume.setActionCommand("VOLUME");

        chkTurnover.setText(Language.getString("TNS_VALUE"));
        chkTurnover.addActionListener(this);
        chkTurnover.setActionCommand("TURNOVER");

        chkSuccessiveTrades.setText(Language.getString("TNS_SUCCESSIVE_TRDES"));
        chkSuccessiveTrades.addMouseListener(this);
        chkSuccessiveTrades.setActionCommand("TRDES");
        setShowSuccessiveTrades(store.isShowSuccessiveTrades());
        chkSuccessiveTrades.setSelected(isShowSuccessiveTrades());

        topPanel.setLayout(new FlexGridLayout(new String[]{"460"}, new String[]{"20", "20"}, 10, 0));
        // topPanel.setLayout(new ColumnLayout(ColumnLayout.MODE_SAME_SIZE));
        topUpPanel.setLayout(new FlexGridLayout(new String[]{"100", "160", "160"}, new String[]{"20"}, 10, 2));
        topUpPanel.add(lblFilterBy);
        topUpPanel.add(lblViewTrades);
        topUpPanel.add(lblShowAddMark);
        topPanel.add(topUpPanel);
        topPanel.add(topDownPanel);

        topDownPanel.setLayout(new FlexGridLayout(new String[]{"80", "80", "80", "80", "80"}, new String[]{"20"}, 10, 2));
        topDownPanel.add(new JLabel());
        topDownPanel.add(lblFrom);
        topDownPanel.add(lblTo);
        topDownPanel.add(lblAbove);
        topDownPanel.add(lblBelow);

        middleTopPanel.add(chkPrice);
        middleTopPanel.add(txtFromPrice);
        middleTopPanel.add(txtToPrice);
        middleTopPanel.add(txtAbovePrice);
        middleTopPanel.add(txtBelowPrice);

        middleTopPanel.add(chkVolume);
        middleTopPanel.add(txtFromVolume);
        middleTopPanel.add(txtToVolume);
        middleTopPanel.add(txtAboveVolume);
        middleTopPanel.add(txtBelowVolume);

        middleTopPanel.add(chkTurnover);
        middleTopPanel.add(txtFromTurnover);
        middleTopPanel.add(txtToTurnover);
        middleTopPanel.add(txtAboveTurnover);
        middleTopPanel.add(txtBelowTurnover);

        middleDownPanel.setLayout(new FlexGridLayout(new String[]{"440"}, new String[]{"20"}, 10, 0));
        middleDownPanel.add(chkSuccessiveTrades);
        middlePanel.setLayout(new FlexGridLayout(new String[]{"460"}, new String[]{"115", "20"}, 0, 0));
        //middlePanel.setLayout(new ColumnLayout(ColumnLayout.MODE_SAME_SIZE));
        middlePanel.add(middleTopPanel);
        middlePanel.add(middleDownPanel);

        ButtonGroup conditionGroup = new ButtonGroup();

        radioMatchAll = new JRadioButton(Language.getString("TNS_MATCH_ALL"));
        radioMatchAll.setEnabled((store.isFilteredByPriceMarket() && store.isFilteredByTurnoverMarket()) ||
                (store.isFilteredByPriceMarket() && store.isFilteredByVolumeMarket()) ||
                (store.isFilteredByVolumeMarket() && store.isFilteredByTurnoverMarket()));
        radioMatchAll.setSelected(store.isMatchAll());

        radioMatchAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (radioMatchAll.isSelected()) {
                    store.setMatchAll(true);
                    store.setMatchAny(false);
                } else if (!radioMatchAll.isSelected()) {
                }
            }
        });

        conditionGroup.add(radioMatchAll);
        radioMatchAny = new JRadioButton(Language.getString("TNS_MATCH_ANY"));
        radioMatchAny.setEnabled((store.isFilteredByPriceMarket() && store.isFilteredByTurnoverMarket()) ||
                (store.isFilteredByPriceMarket() && store.isFilteredByVolumeMarket()) ||
                (store.isFilteredByVolumeMarket() && store.isFilteredByTurnoverMarket()));
        radioMatchAny.setSelected(store.isMatchAny());
        radioMatchAny.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (radioMatchAny.isSelected()) {
                    store.setMatchAny(true);
                    store.setMatchAll(false);
                } else if (!radioMatchAny.isSelected()) {
                }
            }
        });

        conditionGroup.add(radioMatchAny);
        downPanel.setLayout(new FlexGridLayout(new String[]{"270", "180"}, new String[]{"40"}, 0, 0));
        downLeftPanel.setLayout(new FlexGridLayout(new String[]{"270"}, new String[]{"20", "20"}, 10, 0));
        downLeftPanel.add(radioMatchAll);
        downLeftPanel.add(radioMatchAny);

        okBtn = new TWButton(Language.getString("OK"));
        okBtn.setPreferredSize(new Dimension(80, 20));
        okBtn.setText(Language.getString("OK"));
        okBtn.addActionListener(this);
        okBtn.setActionCommand("OK");
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        cancelBtn.setPreferredSize(new Dimension(80, 20));
        cancelBtn.addActionListener(this);
        cancelBtn.setActionCommand("CANCEL");
        downRightPanel.setLayout(new FlexGridLayout(new String[]{"80", "80"}, new String[]{"20", "20"}, 10, 0));
        downRightPanel.add(new JLabel());
        downRightPanel.add(new JLabel());
        downRightPanel.add(okBtn);
        downRightPanel.add(cancelBtn);

        downPanel.add(downLeftPanel);
        downPanel.add(downRightPanel);
//        setSize(500, 320);
        setSize(473, 275);
        setResizable(false);
//        setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"18%", "1%", "60%", "1%", "20%"}, 5, 2));
        setLayout(new FlexGridLayout(new String[]{"460"}, new String[]{"40", "1", "140", "1", "45"}, 2, 2));
        getContentPane().add(topPanel);
        getContentPane().add(new JSeparator(0));
        getContentPane().add(middlePanel);
        getContentPane().add(new JSeparator(0));
        getContentPane().add(downPanel);

        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        GUISettings.applyOrientation(topPanel);
        GUISettings.applyOrientation(middlePanel);
        GUISettings.applyOrientation(downPanel);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public void loadDefaults() {
        chkPrice.setSelected(store.isFilteredByPriceMarket());
        chkVolume.setSelected(store.isFilteredByVolumeMarket());
        chkTurnover.setSelected(store.isFilteredByTurnoverMarket());

        txtFromPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtToPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtAbovePrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtBelowPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));

        txtFromVolume.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtToVolume.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtAboveVolume.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtBelowVolume.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));

        txtFromTurnover.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtToTurnover.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtAboveTurnover.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtBelowTurnover.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));

        txtFromPrice.setHorizontalAlignment(JTextField.RIGHT);
        txtToPrice.setHorizontalAlignment(JTextField.RIGHT);
        txtAbovePrice.setHorizontalAlignment(JTextField.RIGHT);
        txtBelowPrice.setHorizontalAlignment(JTextField.RIGHT);

        txtFromVolume.setHorizontalAlignment(JTextField.RIGHT);
        txtToVolume.setHorizontalAlignment(JTextField.RIGHT);
        txtAboveVolume.setHorizontalAlignment(JTextField.RIGHT);
        txtBelowVolume.setHorizontalAlignment(JTextField.RIGHT);

        txtFromTurnover.setHorizontalAlignment(JTextField.RIGHT);
        txtToTurnover.setHorizontalAlignment(JTextField.RIGHT);
        txtAboveTurnover.setHorizontalAlignment(JTextField.RIGHT);
        txtBelowTurnover.setHorizontalAlignment(JTextField.RIGHT);

        txtFromPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));


        if (store.isFilteredByPriceMarket()) {
            txtFromPrice.setText("" + store.getStartPriceMarket());
            txtToPrice.setText("" + store.getEndPriceMarket());
        } else {
            txtFromPrice.setText("0.0");
            txtToPrice.setText("0.0");
        }

        txtToPrice.addKeyListener(this);
        txtFromPrice.setEditable(chkPrice.isSelected());
        txtToPrice.setEditable(chkPrice.isSelected());

        if (chkPrice.isSelected()) {
            txtAbovePrice.setText("" + store.getAbovePriceMarket());
            txtBelowPrice.setText("" + store.getBelowPriceMarket());
        } else {
            txtAbovePrice.setText("");
            txtBelowPrice.setText("");
        }
        txtAbovePrice.setEditable(chkPrice.isSelected());
        txtBelowPrice.setEditable(chkPrice.isSelected());

        if (store.isFilteredByVolumeMarket()) {
            txtFromVolume.setText("" + store.getStartVolumeMarket());
            txtToVolume.setText("" + store.getEndVolumeMarket());
        } else {
            txtFromVolume.setText("0");
            txtToVolume.setText("0");
        }
        txtToVolume.addKeyListener(this);
        txtFromVolume.setEditable(chkVolume.isSelected());
        txtToVolume.setEditable(chkVolume.isSelected());

        if (chkVolume.isSelected()) {
            txtAboveVolume.setText("" + store.getAboveVolumeMarket());
            txtBelowVolume.setText("" + store.getBelowVolumeMarket());
        } else {
            txtAboveVolume.setText("");
            txtBelowVolume.setText("");
        }
        txtAboveVolume.setEditable(chkVolume.isSelected());
        txtBelowVolume.setEditable(chkVolume.isSelected());

        if (store.isFilteredByTurnoverMarket()) {
            txtFromTurnover.setText("" + store.getStartTurnoverMarket());
            txtToTurnover.setText("" + store.getEndTurnoverMarket());
        } else {
            txtFromTurnover.setText("0");
            txtToTurnover.setText("0");
        }
        txtToTurnover.addKeyListener(this);
        txtFromTurnover.setEditable(chkTurnover.isSelected());
        txtToTurnover.setEditable(chkTurnover.isSelected());

        if (chkTurnover.isSelected()) {
            txtAboveTurnover.setText("" + store.getAboveTurnoverMarket());
            txtBelowTurnover.setText("" + store.getBelowTurnoverMarket());
        } else {
            txtAboveTurnover.setText("");
            txtBelowTurnover.setText("");
        }
        txtAboveTurnover.setEditable(chkTurnover.isSelected());
        txtBelowTurnover.setEditable(chkTurnover.isSelected());

    }

    public boolean isInputValid() { // -This will do all the validations - Logically and in value
        //-- 1= from 2=to 3=Above 4=Below
        boolean sucess = false;
        try {
            if (chkPrice.isSelected()) {

                if (txtFromPrice.getText().trim().startsWith("-") || txtToPrice.getText().trim().startsWith("-") || txtAbovePrice.getText().trim().startsWith("-") || txtBelowPrice.getText().trim().startsWith("-")) {
                    showErrorMessage(Language.getString("INVALID_PRICE"));
                    txtFromPrice.requestFocus();
                    return sucess;
                }
                float price1 = 0.0F;
                if (!txtFromPrice.getText().trim().equals("")) {
                    try {
                        price1 = Float.parseFloat(txtFromPrice.getText());
                        if (price1 < 0) {
                            showErrorMessage(Language.getString("INVALID_PRICE"));
                            txtFromPrice.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_PRICE"));
                        txtFromPrice.requestFocus();
                        return sucess;
                    }
                }

                float price2 = 0.0F;
                if (!txtToPrice.getText().trim().equals("")) {
                    try {
                        price2 = Float.parseFloat(txtToPrice.getText());
                        if (price2 < 0) {
                            showErrorMessage(Language.getString("INVALID_PRICE"));
                            txtToPrice.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_PRICE"));
                        txtToPrice.requestFocus();
                        return sucess;
                    }
                }

                try {
                    if (price1 > price2) {
                        showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                        txtFromPrice.requestFocus();
                        return sucess;
                    }
                } catch (NumberFormatException ex) {
                    showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                    txtFromPrice.requestFocus();
                    return sucess;
                }

                float price3 = 0.0F;
                if (!txtAbovePrice.getText().trim().equals("")) {
                    try {
                        price3 = Float.parseFloat(txtAbovePrice.getText());
                        if (price3 < 0) {
                            showErrorMessage(Language.getString("INVALID_PRICE"));
                            txtAbovePrice.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_PRICE"));
                        txtAbovePrice.requestFocus();
                        return sucess;
                    }
                }

                float price4 = 0.0F;
                if (!txtBelowPrice.getText().trim().equals("")) {
                    try {
                        price4 = Float.parseFloat(txtBelowPrice.getText());
                        if (price4 < 0) {
                            showErrorMessage(Language.getString("INVALID_PRICE"));
                            txtBelowPrice.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_PRICE"));
                        txtBelowPrice.requestFocus();
                        return sucess;
                    }
                }

                if (price3 != 0.0) {
                    try {
                        if (price2 < price3) {
                            showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                            txtBelowPrice.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_PRICE"));
                        txtBelowPrice.requestFocus();
                        return sucess;
                    }
                }

                if (price4 != 0.0) {
                    try {
                        if (price1 >= price4) {
                            showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                            txtBelowPrice.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                        txtBelowPrice.requestFocus();
                        return sucess;
                    }
                }

                if (price4 != 0.0 && price3 != 0.0) {
                    try {
                        if (price3 < price4) {
                            showErrorMessage(Language.getString("ABOVE_BELOW_LIMIT_ERROR"));
                            txtBelowPrice.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("ABOVE_BELOW_LIMIT_ERROR"));
                        txtBelowPrice.requestFocus();
                        return sucess;
                    }
                }
            }

            if (chkVolume.isSelected()) {
                if (txtFromVolume.getText().trim().startsWith("-") || txtToVolume.getText().trim().startsWith("-") || txtAboveVolume.getText().trim().startsWith("-") || txtBelowVolume.getText().trim().startsWith("-")) {
                    showErrorMessage(Language.getString("INVALID_VOLUME"));
                    txtFromVolume.requestFocus();
                    return sucess;
                }
                long vol1 = 0;
                if (!txtFromVolume.getText().trim().equals("")) {
                    try {
                        vol1 = Long.parseLong(txtFromVolume.getText());
                        if (vol1 < 0) {
                            showErrorMessage(Language.getString("INVALID_VOLUME"));
                            txtFromVolume.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_VOLUME"));
                        txtFromVolume.requestFocus();
                        return sucess;
                    }
                }

                long vol2 = 0;
                if (!txtToVolume.getText().trim().equals("")) {
                    try {
                        vol2 = Long.parseLong(txtToVolume.getText());
                        if (vol2 < 0) {
                            showErrorMessage(Language.getString("INVALID_VOLUME"));
                            txtToVolume.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_VOLUME"));
                        txtToVolume.requestFocus();
                        return sucess;
                    }
                }

                try {
                    if (vol1 > vol2) {
                        showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                        txtFromVolume.requestFocus();
                        return sucess;
                    }
                } catch (NumberFormatException ex) {
                    showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                    txtFromVolume.requestFocus();
                    return sucess;
                }

                long vol3 = 0;
                if (!txtAboveVolume.getText().trim().equals("")) {
                    try {
                        vol3 = Long.parseLong(txtAboveVolume.getText());
                        if (vol3 < 0) {
                            showErrorMessage(Language.getString("INVALID_VOLUME"));
                            txtAboveVolume.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_VOLUME"));
                        txtAboveVolume.requestFocus();
                        return sucess;
                    }
                }

                long vol4 = 0;
                if (!txtBelowVolume.getText().trim().equals("")) {
                    try {
                        vol4 = Long.parseLong(txtBelowVolume.getText());
                        if (vol4 < 0) {
                            showErrorMessage(Language.getString("INVALID_VOLUME"));
                            txtBelowVolume.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_VOLUME"));
                        txtBelowVolume.requestFocus();
                        return sucess;
                    }
                }

                if (vol3 != 0) {
                    try {
                        if (vol2 < vol3) {
                            showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                            txtBelowVolume.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                        txtBelowVolume.requestFocus();
                        return sucess;
                    }
                }

                if (vol4 != 0) {
                    try {
                        if (vol1 >= vol4) {
                            showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                            txtBelowVolume.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                        txtBelowVolume.requestFocus();
                        return sucess;
                    }
                }

                if (vol4 != 0 && vol3 != 0) {
                    try {
                        if (vol3 < vol4) {
                            showErrorMessage(Language.getString("ABOVE_BELOW_LIMIT_ERROR"));
                            txtBelowVolume.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("ABOVE_BELOW_LIMIT_ERROR"));
                        txtBelowVolume.requestFocus();
                        return sucess;
                    }
                }
            }

            if (chkTurnover.isSelected()) {

                if (txtFromTurnover.getText().trim().startsWith("-") || txtToTurnover.getText().trim().startsWith("-") || txtAboveTurnover.getText().trim().startsWith("-") || txtBelowTurnover.getText().trim().startsWith("-")) {
                    showErrorMessage(Language.getString("INVALID_VALUE"));
                    txtFromTurnover.requestFocus();
                    return sucess;
                }
                double turnovr1 = 0;
                if (!txtFromTurnover.getText().trim().equals("")) {
                    try {
                        turnovr1 = Double.parseDouble(txtFromTurnover.getText());
                        if (turnovr1 < 0) {
                            showErrorMessage(Language.getString("INVALID_VALUE"));
                            txtFromTurnover.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_VALUE"));
                        txtFromTurnover.requestFocus();
                        return sucess;
                    }
                }
                double turnovr2 = 0;
                if (!txtToTurnover.getText().trim().equals("")) {
                    try {
                        turnovr2 = Double.parseDouble(txtToTurnover.getText());
                        if (turnovr2 < 0) {
                            showErrorMessage(Language.getString("INVALID_VALUE"));
                            txtToTurnover.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_VALUE"));
                        txtToTurnover.requestFocus();
                        return sucess;
                    }
                }

                try {
                    if (turnovr1 > turnovr2) {
                        showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                        txtFromTurnover.requestFocus();
                        return sucess;
                    }
                } catch (NumberFormatException ex) {
                    showErrorMessage(Language.getString("FROM_TO_LIMIT_ERROR"));
                    txtFromTurnover.requestFocus();
                    return sucess;
                }

                double turnovr3 = 0;
                if (!txtAboveTurnover.getText().trim().equals("")) {
                    try {
                        turnovr3 = Double.parseDouble(txtAboveTurnover.getText());
                        if (turnovr3 < 0) {
                            showErrorMessage(Language.getString("INVALID_VALUE"));
                            txtAboveTurnover.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_VALUE"));
                        txtAboveTurnover.requestFocus();
                        return sucess;
                    }
                }
                double turnovr4 = 0;
                if (!txtBelowTurnover.getText().trim().equals("")) {
                    try {
                        turnovr4 = Double.parseDouble(txtBelowTurnover.getText());
                        if (turnovr4 < 0) {
                            showErrorMessage(Language.getString("INVALID_VALUE"));
                            txtBelowTurnover.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("INVALID_VALUE"));
                        txtBelowTurnover.requestFocus();
                        return sucess;
                    }
                }


                if (turnovr3 != 0) {
                    try {
                        if (turnovr2 < turnovr3) {
                            showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                            txtBelowTurnover.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                        txtBelowTurnover.requestFocus();
                        return sucess;
                    }
                }

                if (turnovr4 != 0) {
                    try {
                        if (turnovr1 >= turnovr4) {
                            showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                            txtBelowTurnover.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("TNS_ERROR_MSG"));
                        txtBelowTurnover.requestFocus();
                        return sucess;
                    }
                }

                if (turnovr4 != 0 && turnovr3 != 0) {
                    try {
                        if (turnovr3 < turnovr4) {
                            showErrorMessage(Language.getString("ABOVE_BELOW_LIMIT_ERROR"));
                            txtBelowTurnover.requestFocus();
                            return sucess;
                        }
                    } catch (NumberFormatException ex) {
                        showErrorMessage(Language.getString("ABOVE_BELOW_LIMIT_ERROR"));
                        txtBelowTurnover.requestFocus();
                        return sucess;
                    }
                }
            }

            sucess = true;
            return sucess;

        } catch (Exception ex) {
            return sucess;
        }
    }

    private void showErrorMessage(String message) {
        ShowMessage oMessage = new ShowMessage(this, message, "W", false);

    }


    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            if (isInputValid()) {
                if (chkPrice.isSelected()) {
                    store.setStartPriceMarket(SharedMethods.floatValue(txtFromPrice.getText(), 0));
                    store.setEndPriceMarket(SharedMethods.floatValue(txtToPrice.getText(), 0));
                    store.setAbovePriceMarket(SharedMethods.floatValue(txtAbovePrice.getText(), 0));
                    store.setBelowPriceMarket(SharedMethods.floatValue(txtBelowPrice.getText(), 0));
                }
                if (chkVolume.isSelected()) {
                    store.setStartVolumeMarket(SharedMethods.longValue(txtFromVolume.getText(), 0));
                    store.setEndVolumeMarket(SharedMethods.longValue(txtToVolume.getText(), 0));

                    store.setAboveVolumeMarket(SharedMethods.longValue(txtAboveVolume.getText(), 0));
                    store.setBelowVolumeMarket(SharedMethods.longValue(txtBelowVolume.getText(), 0));
                }
                if (chkTurnover.isSelected()) {
                    store.setStartTurnoverMarket(SharedMethods.doubleValue(txtFromTurnover.getText(), 0));
                    store.setEndTurnoverMarket(SharedMethods.doubleValue(txtToTurnover.getText(), 0));

                    store.setAboveTurnoverMarket(SharedMethods.doubleValue(txtAboveTurnover.getText(), 0));
                    store.setBelowTurnoverMarket(SharedMethods.doubleValue(txtBelowTurnover.getText(), 0));
                }

                if (chkPrice.isSelected()) {
                    store.setFilteredByPriceMarket(true);
                } else {
                    store.setFilteredByPriceMarket(false);
                }

                if (chkVolume.isSelected()) {
                    store.setFilteredByVolumeMarket(true);
                } else {
                    store.setFilteredByVolumeMarket(false);
                }
                if (chkTurnover.isSelected()) {
                    store.setFilteredByTurnoverMarket(true);
                } else {
                    store.setFilteredByTurnoverMarket(false);
                }

                store.clear(false);
                if (store.getType() == FilterableTradeStore.TYPE_DETAILED) {
                    store.clearAndRefilter();
                    store.reload();
                } else {
//                    store.clearAndRefilter();
                    store.refilter();
                }
                hide();
            }
            store.setShowSuccessiveTrades(isShowSuccessiveTrades());
            store.setMatchAll(radioMatchAll.isSelected());
            store.setMatchAny(radioMatchAny.isSelected());
        } else if (e.getActionCommand().equals("CANCEL")) {
            hide();
        }
        if (e.getActionCommand().equals("PRICE")) {
            if (!chkPrice.isSelected()) {
                txtFromPrice.setEditable(false);
                txtToPrice.setEditable(false);
            } else {
                txtFromPrice.setEditable(true);
                txtToPrice.setEditable(true);
            }
            txtAbovePrice.setEditable(false);
            txtBelowPrice.setEditable(false);
            radioMatchAll.setEnabled((chkPrice.isSelected() && chkVolume.isSelected()) ||
                    (chkPrice.isSelected() && chkTurnover.isSelected()) ||
                    (chkVolume.isSelected() && chkTurnover.isSelected()));
            radioMatchAny.setEnabled((chkPrice.isSelected() && chkVolume.isSelected()) ||
                    (chkPrice.isSelected() && chkTurnover.isSelected()) ||
                    (chkVolume.isSelected() && chkTurnover.isSelected()));
        }

        if (e.getActionCommand().equals("VOLUME")) {
            if (!chkVolume.isSelected()) {
                txtFromVolume.setEditable(false);
                txtToVolume.setEditable(false);
            } else {
                txtFromVolume.setEditable(true);
                txtToVolume.setEditable(true);
            }
            txtAboveVolume.setEditable(false);
            txtBelowVolume.setEditable(false);
            radioMatchAll.setEnabled((chkPrice.isSelected() && chkVolume.isSelected()) ||
                    (chkPrice.isSelected() && chkTurnover.isSelected()) ||
                    (chkVolume.isSelected() && chkTurnover.isSelected()));
            radioMatchAny.setEnabled((chkPrice.isSelected() && chkVolume.isSelected()) ||
                    (chkPrice.isSelected() && chkTurnover.isSelected()) ||
                    (chkVolume.isSelected() && chkTurnover.isSelected()));
        }
        if (e.getActionCommand().equals("TURNOVER")) {
            if (!chkTurnover.isSelected()) {
                txtFromTurnover.setEditable(false);
                txtToTurnover.setEditable(false);
            } else {
                txtFromTurnover.setEditable(true);
                txtToTurnover.setEditable(true);
            }
            txtAboveTurnover.setEditable(false);
            txtBelowTurnover.setEditable(false);
            radioMatchAll.setEnabled((chkPrice.isSelected() && chkVolume.isSelected()) ||
                    (chkPrice.isSelected() && chkTurnover.isSelected()) ||
                    (chkVolume.isSelected() && chkTurnover.isSelected()));
            radioMatchAny.setEnabled((chkPrice.isSelected() && chkVolume.isSelected()) ||
                    (chkPrice.isSelected() && chkTurnover.isSelected()) ||
                    (chkVolume.isSelected() && chkTurnover.isSelected()));
        }

    }

    public boolean isShowSuccessiveTrades() {
        return showSuccessiveTrades;
    }

    public void setShowSuccessiveTrades(boolean showSuccessiveTrades) {
        this.showSuccessiveTrades = showSuccessiveTrades;
    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(txtToPrice)) {
            txtAbovePrice.setEditable(true);
            txtBelowPrice.setEditable(true);
        } else if (e.getSource().equals(txtToVolume)) {
            txtAboveVolume.setEditable(true);
            txtBelowVolume.setEditable(true);
        } else if (e.getSource().equals(txtToTurnover)) {
            txtAboveTurnover.setEditable(true);
            txtBelowTurnover.setEditable(true);
        }
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(chkSuccessiveTrades)) {
            setShowSuccessiveTrades(!isShowSuccessiveTrades());
        }
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
