package com.isi.csvr.trade.ui;

import com.isi.csvr.shared.SharedMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright ISI Sri Lanka  2002</p>
 * <p>Company: ISI Sri lanka</p>
 *
 * @author Uditha Nagahawatta.
 * @version 1.0
 */

public class TimeField extends JPanel implements KeyListener {

    public JTextField txtHours = new JTextField("");
    public JTextField txtMinutes = new JTextField("");
    public JTextField txtSeconds = new JTextField("");

    JLabel colon1 = new JLabel(":");
    JLabel colon2 = new JLabel(":");

    public TimeField() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception {
        this.setBorder(txtHours.getBorder());
        colon1.setOpaque(true);
        colon1.setBackground(txtHours.getBackground());
        colon2.setOpaque(true);
        colon2.setBackground(txtHours.getBackground());

        JPanel hourPanel = new JPanel();
        hourPanel.setLayout(new BorderLayout());
        txtHours.setHorizontalAlignment(JTextField.RIGHT);
        hourPanel.add(txtHours, BorderLayout.CENTER);
        hourPanel.add(colon1, BorderLayout.EAST);
        txtHours.addKeyListener(this);
        txtHours.setBorder(null);

        JPanel minutePanel = new JPanel();
        minutePanel.setLayout(new BorderLayout());
        txtMinutes.setHorizontalAlignment(JTextField.RIGHT);
        minutePanel.add(txtMinutes, BorderLayout.CENTER);
        minutePanel.add(colon2, BorderLayout.EAST);
        txtMinutes.addKeyListener(this);
        txtMinutes.setBorder(null);

        JPanel secondPanel = new JPanel();
        secondPanel.setLayout(new BorderLayout());
        txtSeconds.setHorizontalAlignment(JTextField.RIGHT);
        secondPanel.add(txtSeconds, BorderLayout.CENTER);
        txtSeconds.addKeyListener(this);
        txtSeconds.setBorder(null);

//        this.setLayout(new GridLayout(1, 3));
        minutePanel.setPreferredSize(new Dimension((int) (this.getPreferredSize().getWidth() / 3), (int) this.getPreferredSize().getHeight()));
        this.setLayout(new FlexGridLayout(new String[]{"35%", "40%", "25%"}, new String[]{"100%"}));
        this.add(hourPanel);
        this.add(minutePanel);
        this.add(secondPanel);
    }

    public void setTime(long time) {
        if (time != 0) {
            Calendar cal = Calendar.getInstance();
            Date date = new Date();
            date.setTime(time);//+Settings.getCurrentZone().getRawOffset() + Settings.getCurrentZone().getDSTSavings());
            this.setTime(date);
        } else {
            txtHours.setText("hh");
            txtMinutes.setText("mm");
            txtSeconds.setText("ss");
        }
    }

    /*
     * Return time in milli seconds
     */
    public long getTime() {
        long time = 0;

        time += (SharedMethods.longValue(txtSeconds.getText(), 0) * 1000);
        time += (SharedMethods.longValue(txtMinutes.getText(), 0) * 60 * 1000);
        time += (SharedMethods.longValue(txtHours.getText(), 0) * 60 * 60 * 1000);

        return time;
    }

    public void setTime(Date date) {
        Calendar cal = Calendar.getInstance();
        //date.setTime(date.getTime()+Settings.getCurrentZone().getRawOffset() + Settings.getCurrentZone().getDSTSavings());
        cal.setTime(date);

        //if ((cal.get(Calendar.AM_PM)) == cal.get(Calendar.AM)){
        txtHours.setText("" + cal.get(Calendar.HOUR_OF_DAY));
//		}else{
//			txtHours.setText("" + (cal.get(Calendar.HOUR)+12));
//		}
        txtMinutes.setText("" + cal.get(Calendar.MINUTE));
        txtSeconds.setText("" + cal.get(Calendar.SECOND));

        date = null;
        cal = null;
    }

    public boolean isValidTime() {
        try {
            int hour = Integer.parseInt(txtHours.getText());
            int minute = Integer.parseInt(txtMinutes.getText());
            int second = Integer.parseInt(txtSeconds.getText());

            if ((hour < 0) || (hour >= 24))
                txtHours.requestFocus();

            if ((minute < 0) || (minute >= 60))
                txtMinutes.requestFocus();

            if ((second < 0) || (second >= 60))
                txtSeconds.requestFocus();

            return ((hour >= 0) && (hour < 24) &&
                    (minute >= 0) && (minute < 60) &&
                    (second >= 0) && (second < 60));
        } catch (Exception ex) {
            return false;
        }

    }

    public void setEnabled(boolean enabled) {
        txtHours.setEnabled(enabled);
        txtMinutes.setEnabled(enabled);
        txtSeconds.setEnabled(enabled);
        colon1.setForeground(txtHours.getForeground());
        colon1.setBackground(txtHours.getBackground());
        colon2.setForeground(txtHours.getForeground());
        colon2.setBackground(txtHours.getBackground());
    }

    public void setEditable(boolean enabled) {
        txtHours.setEditable(enabled);
        txtMinutes.setEditable(enabled);
        txtSeconds.setEditable(enabled);
        colon1.setForeground(txtHours.getForeground());
        colon1.setBackground(txtHours.getBackground());
        colon2.setForeground(txtHours.getForeground());
        colon2.setBackground(txtHours.getBackground());
    }

    /**
     * Invoked when a key has been typed.
     * See the class description for {@link KeyEvent} for a definition of
     * a key typed event.
     */
    public void keyTyped(KeyEvent e) {
        //SharedMethods.printLine("t>>" + e.getKeyCode());
        if (!Character.isDigit(e.getKeyChar()) &&
                (e.getKeyChar() != KeyEvent.VK_BACK_SPACE) &&
                (e.getKeyCode() == KeyEvent.VK_UNDEFINED)) {
            e.consume();
        }
    }

    /**
     * Invoked when a key has been pressed.
     * See the class description for {@link KeyEvent} for a definition of
     * a key pressed event.
     */
    public void keyPressed(KeyEvent e) {
        //SharedMethods.printLine("p>>" + e.getKeyCode());
        /*if (((e.getKeyCode() >= e.VK_0) && ((e.getKeyCode() <= e.VK_9)))){
              SharedMethods.printLine("" +e.getKeyCode());
              //e.vk
              e.consume();
              e.
          }*/


    }

    /**
     * Invoked when a key has been released.
     * See the class description for {@link KeyEvent} for a definition of
     * a key released event.
     */
    public void keyReleased(KeyEvent e) {
        //SharedMethods.printLine("r>>" + e.getKeyCode());
    }
}