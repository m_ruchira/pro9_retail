package com.isi.csvr.trade;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.util.TWList;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 8, 2005
 * Time: 1:05:01 PM
 */
public class CompanyTradeStore {
    private static CompanyTradeStore self = null;

    private Hashtable<String, Hashtable<String, FilterableTradeStore>> exchangeTable;
    private Hashtable<String, Trade> lastZipeedTrades;           // Contains the details about the trade requests
    private TradeSerialComparator serialComparator;

    public CompanyTradeStore() {
        exchangeTable = new Hashtable<String, Hashtable<String, FilterableTradeStore>>();
        lastZipeedTrades = new Hashtable<String, Trade>();
        serialComparator = new TradeSerialComparator();
        self = this;
    }

    public static CompanyTradeStore getSharedInstance() {
        return self;
    }

    public Hashtable<String, FilterableTradeStore> getExchangeTable(String exchange) {
        Hashtable<String, FilterableTradeStore> exchangeStore = exchangeTable.get(exchange);
        if (exchangeStore == null) {
            exchangeStore = new Hashtable<String, FilterableTradeStore>();
            exchangeTable.put(exchange, exchangeStore);
        }
        return exchangeStore;
    }

    public synchronized Trade getLastTrade(String exchange, String symbol, int instrument, boolean backlog) {
        try {
            Trade trade = null;
            String key = SharedMethods.getKey(exchange, symbol, instrument);
            Hashtable<String, FilterableTradeStore> exchangeStore = getExchangeTable(exchange);
            if (backlog) {
                trade = (Trade) lastZipeedTrades.get(key);
                if (trade != null)
                    return (Trade) trade.clone();
                else
                    return null;
            } else {
                FilterableTradeStore oList = (FilterableTradeStore) exchangeStore.get(key);
                if (oList != null) {
                    trade = oList.getLastTrade();
                    if (trade != null)
                        return (Trade) trade.clone();
                    else
                        return null;
                } else {
                    return null;
                }
            }
        } catch (Exception e) {
            return null;
        }
    }

    public void reloadCompanylTradesFor(String key) {
        Trade trade = null;
        //synchronized (SYNC) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        FilterableTradeStore store = TradeStore.getAllTradesStore(exchange);
        if (store != null) {
            for (int i = 0; i < store.size(); i++) {
                trade = store.getTrade(i);
                if ((SharedMethods.getKey(exchange, trade.getSymbol(), trade.getInstrument())).equals(key))
                    updateDataStore(exchange, key, trade);
                trade = null;
            }
            store = null;
        }
        //}
    }

    public void addZippedReferance(String key, Trade trade) {
        lastZipeedTrades.put(key, trade);
    }

    public FilterableTradeStore getCompanyTradesStore(String key) {
//        SharedMethods.printLine("Im called in Company tradestore by---",true);
        try {
//            System.out.println("0000000000000000000000000000 =="+key);
            String exchange = SharedMethods.getExchangeFromKey(key);
            FilterableTradeStore oList = getExchangeTable(exchange).get(key);
            if ((oList == null)) {// && (DataStore.getSharedInstance().isValidSymbol(key))) {
                oList = createCompanyTradesStore(exchange, key);
//                System.out.println("111111111111111111111111");
                if (!TradeStore.isInitCalled()) {
//                    System.out.println("2222222222222222222222222222");
                    reloadCompanylTradesFor(key);
                }
            }
//            System.out.println("333333333333333333333333333");
            return oList;
        } catch (Exception e) {
            return null;
        }
    }

    public void updateDataStore(String exchange, String key, Trade oTrade) {
        FilterableTradeStore store = getExchangeTable(exchange).get(key);
        if (store == null) {
            store = createCompanyTradesStore(exchange, key);
        }
        store.insert(serialComparator, oTrade);
    }

    private FilterableTradeStore createCompanyTradesStore(String exchange, String key) {
//        SharedMethods.printLine("--Company Store created --- " ,true);
        FilterableTradeStore store = new FilterableTradeStore(FilterableTradeStore.TYPE_COMPANY, key, TWList.LIST);
        getExchangeTable(exchange).put(key, store);
        return store;
    }

    public void addTrade(String key, Trade trade) {
        getCompanyTradesStore(key).insert(serialComparator, trade);
    }

    public void initialize(String exchange) {
        Enumeration stores = getExchangeTable(exchange).elements();
        while (stores.hasMoreElements()) {
            ((FilterableTradeStore) stores.nextElement()).clear(true);
        }
        stores = null;
    }

    public void count() {
        int count = 0;
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                Hashtable table = getExchangeTable(exchange.getSymbol());
                Enumeration enume = table.elements();
                while (enume.hasMoreElements()) {
                    count += ((FilterableTradeStore) enume.nextElement()).size();
                }
                enume = null;
            }
            exchange = null;
        }
        System.out.println("Count " + count);
    }
}
