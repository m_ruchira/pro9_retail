package com.isi.csvr.trade;

import com.isi.csvr.Client;
import com.isi.csvr.ExchangeMap;
import com.isi.csvr.cachestore.CachedStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.tradebacklog.TradeBacklogStore;
import com.isi.csvr.tradebacklog.TradeFileList;
import com.isi.csvr.tradebacklog.TradeFileListItem;
import com.isi.csvr.util.TWList;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 10, 2004
 * Time: 1:03:44 PM
 */
public class DetailedTradeStore extends Thread implements InternalFrameListener {
    public static final int MODE_SYMBOL = 0;
    public static final int MODE_TIME = 1;
    public static final int MODE_UNKNOWN = 2;
    public static DetailedTradeStore self = null;
    public static boolean isWritingCache = false;
    private final int FILE_LOADING = 0;
    private final int FILE_LOADED = 1;
    private final int FILE_NOT_LOADED = 2;
    private int fileStatus = FILE_NOT_LOADED;
    //private boolean frameOpen = false;
    TradeSummaryPanel summaryPanel = null;
    private FilterableTradeStore store;
    private InternalFrame gui;
    private int decimalPlaces = 2;
    private int prevStatus = TradeFileList.STATUS_QUEUED;
    private AllTradesSorter comparator;
    private SimpleDateFormat timeFormat;
    private String title;
    private JLabel msgDownload;
    private boolean canDownload = false;
    private TradeFileListItem fileItem;
    private String currentDownloadingFile = null;
    private JLabel sortIndicator;

    private DetailedTradeStore() {
        super("DetailedTradeStore");
        store = new FilterableTradeStore(FilterableTradeStore.TYPE_DETAILED, "DETAILED", TWList.ARRAY, false);
        comparator = new AllTradesSorter(DetailedTradeStore.MODE_TIME);
        timeFormat = new SimpleDateFormat("dd/MM/yyyyHHmmss");
        start();
    }

    public static synchronized DetailedTradeStore getSharedInstance() {
        if (self == null) {
            self = new DetailedTradeStore();
        }
        return self;
    }

    private static void createFolder(String path) {
        try {
            File folder = new File(path);
            folder.mkdirs();
            folder = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String extractFile(File file, String exchange) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                createFolder(Settings.getAbsolutepath() + "trades/" + exchange);

                /* file name is in the format TDWL-xxxxx.zip
                   must drop the exchange name part in the file */
                String fileName = file.getName();
                int hypen = fileName.indexOf('-');
                fileName = fileName.substring(hypen + 1);

                FileInputStream in = new FileInputStream(file);
                FileOutputStream oOut = new FileOutputStream(Settings.getAbsolutepath() + "trades/" + exchange + "/" + fileName + ".zip");
                byte[] bytData = new byte[1000];
                int i;

                while (true) {
                    i = in.read(bytData);
                    if (i == -1)
                        break;
                    oOut.write(bytData, 0, i);
                }
                oOut.close();
                oOut = null;
                bytData = null;

                return fileName;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static void sleepThread(long time) {
        try {
            sleep(time);
        } catch (InterruptedException ex) {
        }
    }

    public void setSortIndicator(JLabel sortIndicator) {
        this.sortIndicator = sortIndicator;
    }

    public CachedStore getStore() {
        return store.getUnfilteredCachedStore();
    }

    public FilterableTradeStore getFilterableStore() {
        return store;
    }

    public void init() {
        store.clearCachedStroe(true);
        prevStatus = TradeFileList.STATUS_QUEUED;
    }

    public void activateCurrentDataDownload(String exchange) {
        canDownload = true;
        init();
        this.fileItem = null;
        TradeBacklogStore.getSharedInstance().setCurrentExchange(exchange);
        interrupt();
    }

    public synchronized int getFileStatus() {
        return fileStatus;
    }

    public synchronized void setFileStatus(int fileStatus) {
        this.fileStatus = fileStatus;
    }

    public void setFileItem(TradeFileListItem fileItem) {
        if (getFileStatus() != FILE_LOADING) { // proceed only if the file download is idle
            this.fileItem = fileItem;
            init();
            setFileStatus(FILE_NOT_LOADED);
            interrupt();
        }
    }

    public void interuptThread() {
        interrupt();
    }

    public synchronized boolean addTrade(Trade trade) {

        boolean result = false;
        try {
            store.appendTrade(trade); // insert the element
            trade = null;
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

   /* public synchronized void extractFile(String exchange, String filename, String date) throws Exception {
        isWritingCache = true;
        ZipInputStream in = new ZipInputStream(new FileInputStream(Settings.getTradeArchivePath() + "\\" + exchange + "\\" + filename));
        
        ZipEntry entry = in.getNextEntry();
        if (entry != null) {
            String exchangeCode = ExchangeMap.getIDFor(exchange);
            String record;
            while (gui.isVisible()) {
                record = readLine(in);
                if (record == null) break;
                createTrade(exchangeCode, record, date,true);
                record = null;
            }
        }
        isWritingCache = false;
 //       store.setFileSize();
        in.close();
        in = null;
        entry = null;

    }*/

    public void run() {
        TradeFileListItem listItem = null;
        boolean downloading = false;
        String exchange;

        while (true) {
            try {
                if (Settings.isConnected() && gui.isVisible()) {
                    if (fileItem == null) {
                        exchange = TradeBacklogStore.getSharedInstance().getCurrentExchange();
                        TradeBacklogStore.getSharedInstance().reValidateRecentFile(exchange);
                        listItem = TradeBacklogStore.getSharedInstance().getRecentFile(exchange);

                        // if the file is new show the download link
                        if (listItem.getStatus() == TradeFileList.STATUS_NOT_SAVED) {
                            msgDownload.setVisible(true);
                        }

                        if (canDownload) {
                            msgDownload.setVisible(false);
                            // download the file
                            downloading = TradeBacklogStore.getSharedInstance().checkRecentFile(exchange);
                            if (!downloading)
                                loadExistingFile(exchange, listItem); // load the existing downloaded file
                            canDownload = false;
                        }

                        if (downloading) {
                            store.clear(true);
                            prevStatus = TradeFileList.STATUS_QUEUED;
                            currentDownloadingFile = listItem.getFileName();
                            downloading = false;
                        }
                    } else if (getFileStatus() == FILE_NOT_LOADED) {
                        exchange = TradeBacklogStore.getSharedInstance().getCurrentExchange();
                        setFileStatus(FILE_LOADING);
                        listItem = fileItem;
                        msgDownload.setVisible(false);
                        currentDownloadingFile = listItem.getFileName();
                        loadFile(exchange, listItem, false);
                        setFileStatus(FILE_LOADED);
                    }

                    if (listItem.getStatus() == TradeFileList.STATUS_DOWNLOADENG) {
                        gui.setTitle(title + " - " + Language.getString("STATUS_TRADE_DOWNLOAD") + getPercentage(listItem));
                    }
                }
            } catch (Exception e) {
            }
            if ((listItem != null) && (listItem.getStatus() == TradeFileList.STATUS_SAVED))
                sleepThread(30000);
            else
                sleepThread(2000);
        }
    }

    public String getPercentage(TradeFileListItem listItem) {
        try {
            if (listItem.getProgress() == null) {
                return "";
            } else {
                return " " + listItem.getProgress();
            }
        } catch (Exception e) {
            return "";
        }
    }

    public synchronized void extractFile(String exchange, String filename, String date) throws Exception {
        isWritingCache = true;
        SwingUtilities.updateComponentTreeUI(gui.getTableComponent());
        ZipInputStream zin = new ZipInputStream(new FileInputStream(Settings.getTradeArchivePath() + "\\" + exchange + "\\" + filename));
        ZipEntry entry = zin.getNextEntry();
        InputStreamReader converter = new InputStreamReader(zin);
        BufferedReader in = new BufferedReader(converter);

        if (entry != null) {
            String exchangeCode = ExchangeMap.getIDFor(exchange);
            String record;
            while (gui.isVisible()) {
                record = in.readLine();
                if (record == null) break;
                createTrade(exchangeCode, record, date, true);
                record = null;
            }
        }
        store.pack();
        isWritingCache = false;
        SwingUtilities.updateComponentTreeUI(gui.getTableComponent());
        in.close();
        in = null;
        entry = null;
    }

    public synchronized void loadExistingFile(String exchange, TradeFileListItem item) {
        try {
            gui.setTitle(title);
            gui.show();
            if (gui.isIcon()) {
                gui.setIcon(false);
            }
            gui.requestFocus();
            comparator.setMode(MODE_UNKNOWN);
            sortIndicator.setEnabled(false);
            sortIndicator.repaint();
            extractFile(exchange, item.getFileName(), item.getDescription());
            SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss");
            Trade lastTrade = store.getUnfilteredTrade(store.unfilteredSize() - 1);
            if (lastTrade != null) {
                gui.setTitle(title + " - " + Language.getString("AS_AT").replaceFirst("\\[TIME\\]",
                        item.getDescription() + " " + formater.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, lastTrade.getTradeTime())))));
            }
            lastTrade = null;
            gui.getTableComponent().updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sortIndicator.setEnabled(true);
        }

    }

    public synchronized void loadFile(String exchange, TradeFileListItem item, boolean loadImmediately) {
        try {
            try {
                decimalPlaces = ExchangeStore.getSharedInstance().getExchange(exchange).getPriceDecimalPlaces();
            } catch (Exception e) {
                decimalPlaces = 2;
            }
            if (summaryPanel != null) {
                summaryPanel.setVisible(false);
//                SharedMethods.printLine("Alo", true);
            }

            if ((loadImmediately) || ((currentDownloadingFile != null) && (currentDownloadingFile.equals(item.getFileName())))) {
                gui.setTitle(title);
                gui.show();
                if (gui.isIcon()) {
                    gui.setIcon(false);
                }
                gui.requestFocus();
                sortIndicator.setEnabled(false);
                sortIndicator.repaint();
                extractFile(exchange, item.getFileName(), item.getDescription());
                prevStatus = TradeFileList.STATUS_SAVED;
                SimpleDateFormat formater = new SimpleDateFormat("HH:mm:ss");
                Trade lastTrade = store.getUnfilteredTrade(store.unfilteredSize());
                if (lastTrade != null) {
                    gui.setTitle(title + " - " + Language.getString("AS_AT").replaceFirst("\\[TIME\\]",
                            item.getDescription() + " " + formater.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, lastTrade.getTradeTime())))));
                }

                lastTrade = null;
                gui.getTableComponent().updateUI();
                currentDownloadingFile = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sortIndicator.setEnabled(true);
        }
    }

    private String readLine(InputStream in) throws Exception {
        int iValue;
        StringBuilder buffer = new StringBuilder();
        while (true) {
            iValue = in.read();
            if (iValue == -1) {
                if (buffer.length() > 0) {
                    return buffer.toString();
                } else {
                    return null;
                }
            }
            if (iValue != '\n') {
                buffer.append((char) iValue);
                continue;
            } else {
                return buffer.toString().trim();
            }
        }
    }

//    private void createTrade(String exchange, String record, String date) {
//        try {
//            String[] fields = record.split(",");
//            Trade trade = new Trade(exchange, fields[0]);
//            trade.setTradeTime(timeFormat.parse(date+fields[1]).getTime());
//            trade.setTradePrice(Float.parseFloat(fields[2]));
//            trade.setTradeQuantity(Long.parseLong(fields[3]));
//            trade.setNetChange(Float.parseFloat(fields[4]));
//            trade.setPrecentNetChange(Float.parseFloat(fields[5]));
//            trade.setOddlot((byte) Integer.parseInt(fields[6]));
//            try {
//                trade.setTicketNumber(fields[7]);
//            } catch (Exception e) {
//            }
//            try {
//                trade.setBuyOrderNumber(fields[8]);
//            } catch (Exception e) {
//            }
//            try {
//                trade.setSellOrderNumber(fields[9]);
//            } catch (Exception e) {
//            }
//            try {
//                trade.setMarketID(fields[10]);
//            } catch (Exception e) {
//            }
//            try {
//                trade.setTradeVWAP(Float.parseFloat(fields[11]));
//            } catch (Exception e) {
////                trade.setTradeVWAP(0.00f);
//            }
//            try {
//                trade.setSide(fields[12].charAt(0));
//            } catch (Exception e) {
//            }
//            addTrade(trade);
//            trade = null;
//            fields = null;
//        } catch (Exception e) {
//            System.out.println(record);
//            e.printStackTrace();
//        }

//    }

    public void deleteCachedStore() {
        store.deleteCachedStore();
    }

    public Trade createTrade(String exchange, String record, String date, boolean addingToTrade) {
        try {
//            String[] fields = record.split(",");
            SimpleTokenizer simpleTokenizer = new SimpleTokenizer(record, ',');
            String[] fields = simpleTokenizer.getTokens();
            int instrument;
            try {
                instrument = Integer.parseInt(fields[17]);
            } catch (Exception e) {
                instrument = Meta.INSTRUMENT_EQUITY;
            }
            Trade trade = new Trade(exchange, fields[0], instrument);
            trade.setTradeTime(timeFormat.parse(date + fields[1]).getTime());
            trade.setTradePrice(Float.parseFloat(fields[2]));
            trade.setTradeQuantity(Long.parseLong(fields[3]));
            trade.setNetChange(Float.parseFloat(fields[4]));
            trade.setPrecentNetChange(Float.parseFloat(fields[5]));
            trade.setOddlot((byte) Integer.parseInt(fields[6]));
            try {
                trade.setTicketNumber(fields[7]);
            } catch (Exception e) {
            }
            try {
                trade.setBuyOrderNumber(fields[8]);
            } catch (Exception e) {
            }
            try {
                trade.setSellOrderNumber(fields[9]);
            } catch (Exception e) {
            }
            try {
                trade.setMarketID(fields[10]);
            } catch (Exception e) {
            }
            try {
                trade.setTradeVWAP(Float.parseFloat(fields[11]));
            } catch (Exception e) {
//                trade.setTradeVWAP(0.00f);
            }
            try {
                trade.setSide(fields[12].charAt(0));
            } catch (Exception e) {
            }
            try {
                trade.setBuyerCode(fields[13]);
            } catch (Exception e) {
            }
            try {
                trade.setBuyerType(fields[14].charAt(0));
            } catch (Exception e) {
            }
            try {
                trade.setSellerCode(fields[15]);
            } catch (Exception e) {
            }
            try {
                trade.setSellerType(fields[16].charAt(0));
            } catch (Exception e) {
            }
            if (addingToTrade) {
                addTrade(trade);
            }
            fields = null;
            return trade;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(record);
            return null;
        }
    }

    public void setGui(InternalFrame gui) {
        this.gui = gui;
        this.title = gui.getTitle();

        JPanel msgPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(msgPanel, BoxLayout.LINE_AXIS);
        msgPanel.setLayout(boxLayout);

        msgDownload = new JLabel(Language.getString("DOWNLOAD_DETAILED_TRADES"));
        msgDownload.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                enableRedownload();
            }
        });
        msgDownload.setForeground(Color.blue);
        msgDownload.setCursor(new Cursor(Cursor.HAND_CURSOR));
        msgDownload.setVisible(false);

        msgPanel.add(getTradeSorter());
        msgPanel.add(Box.createHorizontalGlue());
        msgPanel.add(msgDownload);

        JPanel topPanel = new JPanel(new BorderLayout(0, 0));
        topPanel.add(getSummaryPanel(), BorderLayout.NORTH);
        topPanel.add(msgPanel, BorderLayout.CENTER);

        gui.getTableComponent().setNorthPanel(topPanel);
        gui.getTableComponent().revalidate();
    }

    public JLabel getTradeSorter() {
        final JLabel linkButton = new JLabel(Language.getString("CLICK_TO_FILTER"));
        linkButton.setBorder(BorderFactory.createEmptyBorder());
        linkButton.setForeground(Color.blue);
        linkButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        DetailedTradeStore.getSharedInstance().setSortIndicator(linkButton);

        linkButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Client.getInstance().filterTimeAndSalesSummaryForDetailedTrades();
                summaryPanel.setvalues();
            }
        });
        return linkButton;
    }

    public TradeSummaryPanel getSummaryPanel() {
        if (summaryPanel == null)
            summaryPanel = new TradeSummaryPanel(store);
        store.addTradeListener(summaryPanel);
        store.fireNewPanelAdded();
        summaryPanel.setDecimalPlaces(decimalPlaces);
        return summaryPanel;
    }

    public void enableRedownload() {
        canDownload = true;
        gui.setTitle(title);
    }

    // listeners to notify the status of the ui frame status
    public void internalFrameOpened(InternalFrameEvent e) {
    }

    public void internalFrameClosing(InternalFrameEvent e) {

        try {
            deleteCachedStore();
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void internalFrameClosed(InternalFrameEvent e) {
    }

    public void internalFrameIconified(InternalFrameEvent e) {
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    public void internalFrameActivated(InternalFrameEvent e) {
    }

    public void internalFrameDeactivated(InternalFrameEvent e) {

    }

    private class SimpleTokenizer {

        private String string;
        private char delim;

        private int currPos = 0;
        private int maxPos;

        private String[] tokens;


        /**
         * Constructs a tokenizer for the specified string, splitting it on the
         * specified delim char.
         *
         * @param str   The string to be split
         * @param delim The char to split the string on.
         */
        public SimpleTokenizer(String str, char delim) {
            this.string = str;
            this.delim = delim;
            if (str != null) {
                maxPos = str.length();
                tokenize();
            }
        }

        /**
         * Tests if there are more tokens available from this tokenizer's string
         *
         * @return true if and only if there is at least one token in the string after the current position; false otherwise.
         */
        public boolean hasMoreTokens() {
            return (tokens != null && tokens.length > currPos);
        }


        /**
         * Returns the next token from this object's String.
         *
         * @return the next token from this object's String.
         */
        public String nextToken() {
            String str = tokens[currPos];
            currPos++;
            return str;
        }

        public String getToken(int index) {
            return tokens[index];
        }


        /**
         * Splits the string on the delim char. The tokens are placed into
         * a String array.
         */
        private void tokenize() {
            int curr = 0;
            int max = maxPos;
            int next = 0;
            ArrayList list = new ArrayList();
            String token = "";
            while (next >= 0) {
                next = string.indexOf(delim, curr);
                if (next == curr) { // first char was a delim
                    token = "";
                    curr = curr + 1;
                } else if (next > curr) {
                    token = string.substring(curr, next);
                    curr = next + 1;
                } else if (next < 0) { // delim not found
                    token = string.substring(curr, max);
                }

                //System.out.println( "token was *" + token +"*" );
                list.add(token);
            }
            tokens = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                tokens[i] = (String) list.get(i);
            }
        }


        /**
         * Returns the number of tokens produced from this object's
         * string and delimiter.
         *
         * @return The number of tokens.
         */
        public int countTokens() {
            int size = 0;
            if (tokens != null) {
                size = tokens.length;
            }
            return size;
        }


        /**
         * Return the token array.
         */

        public String[] getTokens() {
            return tokens;
        }

    }
}
