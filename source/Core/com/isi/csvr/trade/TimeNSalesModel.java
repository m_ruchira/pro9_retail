// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.trade;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.Date;

public class TimeNSalesModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private FilterableTradeStore g_oQueue;
    private DoubleTransferObject doubleTransferObject;
    private LongTransferObject longTransferObject;
    private IntTransferObject intTransferObject;
    private IntTransferObject intTransferObjectOddLot;
    private String defaultString;
    private int tickMode = 0;

    private TWDateFormat timeFormat;
    private Date date;

    /**
     * Constructor
     */
    public TimeNSalesModel() {
        doubleTransferObject = new DoubleTransferObject();
        longTransferObject = new LongTransferObject();
        intTransferObject = new IntTransferObject();
        intTransferObjectOddLot = new IntTransferObject();
        defaultString = Language.getString("NA");
        timeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));//" HH:mm:ss ");

        date = new Date();
    }

    public void setSymbol(String symbol) {

    }

    public void setDataStore(FilterableTradeStore oQueue) {
        g_oQueue = oQueue;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if (g_oQueue != null)
            return g_oQueue.size();
        else
            return 0;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            Trade oTrade = g_oQueue.getTrade(g_oQueue.size() - iRow - 1);
            Trade oTrade_prev = g_oQueue.getTrade(g_oQueue.size() - iRow - 2);
            float priceModifFac = ExchangeStore.getSharedInstance().getExchange(oTrade.getExchange()).getPriceModificationFactor();

            switch (iCol) {
                case -7:
                    return 0;
                case -6:
                    return 0;
                case -5:
                    return 0;
                case -4:
                    return SharedMethods.getDecimalPlaces(oTrade.getExchange(), oTrade.getSymbol(), oTrade.getInstrument());
                case -3:
                    return false;
                case -2:
                    return false;
                case 0:
                    return intTransferObjectOddLot.setValue(oTrade.getOddlot()); // this must have a different transfer obj
                case 1:
                    return longTransferObject.setValue(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(oTrade.getExchange(), oTrade.getTradeTime()));
                case 2:
                    return doubleTransferObject.setValue(oTrade.getTradePrice());
                case 3:
                    return longTransferObject.setValue(oTrade.getTradeQuantity());
                case 4:
                    return doubleTransferObject.setValue(oTrade.getNetChange());
                case 5:
                    return doubleTransferObject.setValue(oTrade.getPrecentNetChange());
                case 6:
                    return intTransferObject.setValue(oTrade.getSplits());
                case 7:
                    if (tickMode == Constants.TICK_MODE_LAST_TRADE) {
                        return intTransferObject.setValue(getLastTradebasedTick(oTrade, oTrade_prev));
                        //return intTransferObject.setValue(oTrade.getTick());
                    } else if (tickMode == Constants.TICK_MODE_PREVIOUS_CLOSED) {
                        return intTransferObject.setValue(getPrevClossedbasedTick(oTrade, oTrade_prev));
                        //return intTransferObject.setValue(oTrade.getTickPrevClosed());
                    }
                case 8:
                    return longTransferObject.setValue(oTrade.getSequence());
                case 9:
                    return oTrade.getTicketNumber();
                case 10:
                    return oTrade.getBuyOrderNumber();
                case 11:
                    return oTrade.getSellOrderNumber();
                case 12:
//                    return "khkhjkj";
                    try {
                        String marketID = DataStore.getSharedInstance().getStockObject(oTrade.getExchange(), oTrade.getSymbol(), oTrade.getInstrument(), true).getMarketID();
                        return (ExchangeStore.getSharedInstance().getMarket(oTrade.getExchange(), marketID)).getDescription();
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 13:
                    return doubleTransferObject.setValue(oTrade.getTradeVWAP());
                case 14:
                    try {
                        String value = Settings.getMarketCenter(oTrade.getMarketCenter(), 1);
                        if (value == null) {
                            return defaultString;
                        } else {
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }
                    /*case 15:
                    try {
                        String value = oTrade.getSellerCode();
                        if(value == null){
                            return defaultString;
                        }else{
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 16:
                    try {
                        String value = SharedMethods.getBrokerTypeString(oTrade.getSellerType());
                        if(value == null){
                            return defaultString;
                        }else{
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 17:
                    try {
                        String value = oTrade.getBuyerCode();
                        if(value == null){
                            return defaultString;
                        }else{
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 18:
                    try {
                        String value = SharedMethods.getBrokerTypeString(oTrade.getBuyerType());
                        if(value == null){
                            return defaultString;
                        }else{
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }*/
                case 15:
                    return intTransferObject.setValue(oTrade.getSide());
                case 16:
                    return doubleTransferObject.setValue(oTrade.getTurnover() / priceModifFac);
                /*case 17:
                    try {
                        oTrade.setPriceChange(oTrade.getTradePrice() - oTrade_prev.getTradePrice());
                        return doubleTransferObject.setValue(oTrade.getTradePrice() - oTrade_prev.getTradePrice());
                    } catch (Exception e) {
                        oTrade.setPriceChange(oTrade.getTradePrice() - oTrade.previousClosed);
                        return doubleTransferObject.setValue(oTrade.getTradePrice() - oTrade.previousClosed);
                    }*/
                case 17:
                    return "" + oTrade.getSettlementPrice();
                case 18:
                    return "" + oTrade.getAccruedInterest();
                default:
                    return "";

            }
        } catch (Exception e) {
            return "";
        }
    }

    private int getLastTradebasedTick(Trade current, Trade previous) {
        int lastTradeBasedTick = Settings.TICK_NOCHANGE;
        float prev_price = 0;
        try {
            prev_price = previous.getTradePrice();
        } catch (Exception e) {
            prev_price = (float) current.previousClosed;
        }
        if (current.getTradePrice() > prev_price) {
            lastTradeBasedTick = Settings.TICK_UP;
        } else if (current.getTradePrice() < prev_price) {
            lastTradeBasedTick = Settings.TICK_DOWN;
        } else {
            try {
                if ((previous.getTick() == Settings.TICK_UP) || (previous.getTick() == Settings.TICK_POSITIVE_EQUAL)) {
                    ;
                    lastTradeBasedTick = Settings.TICK_POSITIVE_EQUAL;
                } else {
                    lastTradeBasedTick = Settings.TICK_NEGATIVE_EQUAL;
                }
            } catch (Exception e) {
                lastTradeBasedTick = Settings.TICK_POSITIVE_EQUAL;
            }
        }
        current.setTick(lastTradeBasedTick);
        return lastTradeBasedTick;

    }

    private int getPrevClossedbasedTick(Trade current, Trade previous) {
        int previousTradeBasedTick = Settings.TICK_NOCHANGE;
        Stock stock;
        double previousClosed = 0.0d;
        try {
            stock = DataStore.getSharedInstance().getStockObject(current.getExchange(), current.getSymbol(), current.getInstrument());
            previousClosed = stock.getPreviousClosed();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if (current.getTradePrice() > previousClosed) {
            previousTradeBasedTick = Settings.TICK_UP;
        } else if (current.getTradePrice() < previousClosed) {
            previousTradeBasedTick = Settings.TICK_DOWN;
        } else {
            try {
                if ((previous.getTickPrevClosed() == Settings.TICK_UP) || (previous.getTickPrevClosed() == Settings.TICK_POSITIVE_EQUAL)) {
                    previousTradeBasedTick = Settings.TICK_POSITIVE_EQUAL;
                } else {
                    previousTradeBasedTick = Settings.TICK_NEGATIVE_EQUAL;
                }
            } catch (Exception e) {
                previousTradeBasedTick = Settings.TICK_POSITIVE_EQUAL;
            }
        }
        current.setTickPrevClosed(previousTradeBasedTick);
        return previousTradeBasedTick;
    }

    public String getValue(int row, int col) {
        try {
            Trade oTrade = g_oQueue.getTrade(g_oQueue.size() - row - 1);
            float priceModifFac = ExchangeStore.getSharedInstance().getExchange(oTrade.getExchange()).getPriceModificationFactor();
            switch (col) {
                case 0:
                    return "" + oTrade.getOddlot(); // this must have a different transfer obj
                case 1:
                    date.setTime(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(oTrade.getExchange(), oTrade.getTradeTime()));
                    return timeFormat.format(date);
                case 2:
                    return "" + oTrade.getTradePrice();
                case 3:
                    return "" + oTrade.getTradeQuantity();
                case 4:
                    return "" + oTrade.getNetChange();
                case 5:
                    return "" + oTrade.getPrecentNetChange();
                case 6:
                    return "" + oTrade.getSplits();
                case 7:
                    if (tickMode == Constants.TICK_MODE_LAST_TRADE) {
                        return getTick(oTrade.getTick());
                    } else if (tickMode == Constants.TICK_MODE_PREVIOUS_CLOSED) {
                        return getTick(oTrade.getTickPrevClosed());
                    }
                case 8:
                    return "" + oTrade.getSequence();
                case 9:
                    return oTrade.getTicketNumber();
                case 10:
                    return oTrade.getBuyOrderNumber();
                case 11:
                    return oTrade.getSellOrderNumber();
                case 12:
                    try {
                        String marketID = DataStore.getSharedInstance().getStockObject(oTrade.getExchange(), oTrade.getSymbol(), oTrade.getInstrument(), true).getMarketID();
                        return (ExchangeStore.getSharedInstance().getMarket(oTrade.getExchange(), marketID)).getDescription();
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 13:
                    return "" + oTrade.getTradeVWAP();
                case 14:
                    try {
                        String value = Settings.getMarketCenter(oTrade.getMarketCenter(), 1);
                        if (value == null) {
                            return defaultString;
                        } else {
                            return value;
                        }
                    } catch (Exception e) {
                        return defaultString;
                    }
                case 15:
                    if (oTrade.getSide() == Constants.TRADE_SIDE_SELL) {
                        return Language.getString("SC_SELL");
                    } else if (oTrade.getSide() == Constants.TRADE_SIDE_BUY) {
                        return Language.getString("STRATEGY_BUY");
                    } else {
                        return "";
                    }
                case 16:
                    return "" + oTrade.getTurnover() / priceModifFac;
                case 17:
                    return "" + (oTrade.getPreviousTradePrice());

                //return ""+(oTrade.getTradePrice() - oTrade.getPreviousTradePrice());
                default:
                    return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        if (isLTR()) {
            return super.getViewSettings().getColumnHeadings()[iCol];
        } else {
            return super.getViewSettings().getColumnHeadings()[super.getViewSettings().getColumnHeadings().length - 1 - iCol];
        }
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 9:
            case 10:
            case 11:
            case 12:
            case 14:
            case 16:
            case 17:
            case 18:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 0:
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 13:
            case 15:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
//        return Number.class;
        /*if (iCol == 0)
            return Long.class;
        else
            return getValueAt(0, iCol).getClass();*/
        /*switch(iCol)
        {
            case 0:
                return Date.class;
            case 1:
            case 3:
            case 4:
                return Double.class;
            case 2:
            case 5:
            case 6:
            case 7:
                return Long.class;
        }
        return Object.class;*/
        /*switch(super.getViewSettings().getRendererID(iCol))
        {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        } */
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }


    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));         //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("ROW_COLOR_1"), FIELD_TIMENSALES_ROW1, Theme.getColor("TIME_N_SALES_TABLE_BGCOLOR1"), Theme.getColor("TIME_N_SALES_TABLE_FGCOLOR1"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("ROW_COLOR_2"), FIELD_TIMENSALES_ROW2, Theme.getColor("TIME_N_SALES_TABLE_BGCOLOR2"), Theme.getColor("TIME_N_SALES_TABLE_FGCOLOR2"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_TIMENSALES_SELECTED_ROW, Theme.getColor("TIME_N_SALES_TABLE_SELECTED_BGCOLOR"), Theme.getColor("TIME_N_SALES_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("SMALL_TRADE"), FIELD_SMALLTRADE_ROW, null, Color.blue);
        return customizerRecords;
    }

    public int getTickMode() {
        return this.tickMode;
    }

    public void setTickMode(int tickMode) {
        this.tickMode = tickMode;
    }

    public String getTick(int tick) {
        String tickString = "";
        if (tick == 1) {
            tickString = Language.getString("UP");
        } else if (tick == -1) {
            tickString = Language.getString("DOWN_COLON");
        } else {
            tickString = Language.getString("EQUAL");
        }
        return tickString;
    }
}
