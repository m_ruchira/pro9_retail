package com.isi.csvr.datastore;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Jan 9, 2006
 * Time: 4:49:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class Market implements Serializable, Comparable, ExchangeInterface {

    public static final char BATE_MKT_STATUS = 'A';
    public static final char BATE_VOLUME = 'B';
    public static final char BATE_TURNOVER = 'C';
    public static final char BATE_NO_TRADES = 'D';
    public static final char BATE_SYM_TRADED = 'E';
    public static final char BATE_NO_UPS = 'F';
    public static final char BATE_NO_DOWNS = 'G';
    public static final char BATE_NO_CHANGE = 'H';
    public long volume = 0;
    public double turnover = 0;
    public String marketName;
    public String marketID;
    public String symbol;
    public String exchange;
    //public String marketStatus;
    public int noOfTrades = 0;
    public int symbolsTraded = 0;
    public int noOfUps = 0;
    public int noOfDown = 0;
    public int noOfNoChange = 0;
    private int marketStatus = -1;
    private boolean defaultMarket;
    private boolean showInTicker;

    public Market(String exchange, String marketID) {
        this.exchange = exchange;
        this.marketID = marketID;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public double getTurnover() {
        return turnover;
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
    }

    public int getMarketStatus() {
        return marketStatus;
    }

    public void setMarketStatus(int marketStatus) {
        this.marketStatus = marketStatus;
    }

    public String getDescription() {
        return marketName;
    }

    public void setDescription(String marketName) {
        this.marketName = marketName;
    }

    public String getMarketID() {
        return marketID;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getNoOfTrades() {
        return noOfTrades;
    }

    public void setNoOfTrades(int noOfTrades) {
        this.noOfTrades = noOfTrades;
    }

    public int getSymbolsTraded() {
        return symbolsTraded;
    }

    public void setSymbolsTraded(int symbolsTraded) {
        this.symbolsTraded = symbolsTraded;
    }

    public int getNoOfUps() {
        return noOfUps;
    }

    public void setNoOfUps(int noOfUps) {
        this.noOfUps = noOfUps;
    }

    public int getNoOfDown() {
        return noOfDown;
    }

    public void setNoOfDown(int noOfDown) {
        this.noOfDown = noOfDown;
    }

    public int getNoOfNoChange() {
        return noOfNoChange;
    }

    public void setNoOfNoChange(int noOfNoChange) {
        this.noOfNoChange = noOfNoChange;
    }

    public void setData(String[] data, int decimalFactor) {
        if (data == null)
            return;
        char tag;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == null)
                continue;
            if (data[i].length() <= 1)
                continue;
            tag = data[i].charAt(0);
            switch (tag) {
                case BATE_MKT_STATUS:
                    marketStatus = Integer.parseInt(data[i].substring(1));
                    break;
                case BATE_VOLUME:
                    volume = SharedMethods.getLong(data[i].substring(1));
                    break;
                case BATE_TURNOVER:
                    turnover = SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case BATE_NO_TRADES:
                    noOfTrades = Integer.parseInt(data[i].substring(1));
                    break;
                case BATE_SYM_TRADED:
                    symbolsTraded = Integer.parseInt(data[i].substring(1));
                    break;
                case BATE_NO_UPS:
                    noOfUps = Integer.parseInt(data[i].substring(1));
                    break;
                case BATE_NO_DOWNS:
                    noOfDown = Integer.parseInt(data[i].substring(1));
                    break;
                case BATE_NO_CHANGE:
                    noOfNoChange = Integer.parseInt(data[i].substring(1));
                    break;
            }
        }

    }

    public int getExpansionStatus() {
        return Constants.EXPANSION_STATUS_NA;
    }

    public boolean isDefaultMarket() {
        return defaultMarket;
    }

    public void setDefaultMarket(boolean defaultMarket) {
        this.defaultMarket = defaultMarket;
    }

    public boolean isShowInTicker() {
        return showInTicker;
    }

    public void setShowInTicker(boolean showInTicker) {
        this.showInTicker = showInTicker;
    }

    public int compareTo(Object other) {
        try {
            return marketName.compareToIgnoreCase(((Market) other).getDescription());
        } catch (Exception e) {
            return 0;
        }
    }

}
