package com.isi.csvr.datastore;

import com.isi.csvr.ticker.custom.TickerDataInterface;

import java.util.*;

public class PortfolioStore implements TickerDataInterface {

    private List<PortfolioRecord> symbolList;
    private String baseCurrency;
    private double[] totalMarketValue = new double[2];
    private double[] totalGainLoss = new double[2];
    private double[] totalPctGainLoss = new double[2];
    private Vector<String> symbolIndex;

    public PortfolioStore(String data) {
        setData(data);
        symbolIndex = new Vector<String>(10, 10);
    }

    public PortfolioStore() {
        symbolList = Collections.synchronizedList(new LinkedList<PortfolioRecord>());
        symbolIndex = new Vector<String>(10, 10);
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String newBaseCurrency) {
        baseCurrency = newBaseCurrency;
    }

    public List<PortfolioRecord> getSymbolList() {
        return symbolList;
    }

    public void addRecord(PortfolioRecord record) {
        if (symbolList == null) {
            symbolList = Collections.synchronizedList(new LinkedList<PortfolioRecord>());
        }
        record.setStore(this);
        symbolList.add(record);
        if (!symbolIndex.contains(record.getSymbol())) {
            symbolIndex.addElement(record.getSymbol());
        }
    }

    public void addSymbol(String symbol) {
        PortfolioRecord record = new PortfolioRecord();
        record.setSymbol(symbol);
        record.setStore(this);
        addRecord(record);
        record = null;
    }

    public void addSymbols(String[] symbols) {
        for (int i = 0; i < symbols.length; i++) {
            if (!contains(symbols[i]))
                addSymbol(symbols[i]);
        }
        syncSymbols(symbols);
    }

    /**
     * Remove symbols from the portfolio if not in the
     * reference list
     */
    private void syncSymbols(String[] symbols) {
        Symbols refSymbols = new Symbols();
        refSymbols.setSymbols(symbols);

        String[] portfolioList = getSymbols();
        for (int i = 0; i < portfolioList.length; i++) {
            if (!refSymbols.isAlreadyIn(portfolioList[i])) {
                removeSymbol(portfolioList[i]);
            }
        }

    }

    public void removeSymbol(String symbol) {
        try {
            //System.out.println(getRecord(symbol) == null);
            symbolList.remove(getRecord(symbol));
            if (!contains(symbol)) {
                symbolIndex.removeElement(symbol);
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }

    }

    public void removeAll() {
        symbolList.clear();
        symbolIndex.clear();
        symbolIndex.trimToSize();
    }

    public boolean contains(String symbol) {
        for (int i = 0; i < symbolList.size(); i++) {
            PortfolioRecord record = symbolList.get(i);
            if (record.getSymbol().equals(symbol)) {
                return true;
            }
            record = null;
        }
        return false;
    }

    public String[] getSymbols() {
        String[] symbols = new String[symbolList.size()];

        for (int i = 0; i < symbolList.size(); i++) {
            PortfolioRecord record = symbolList.get(i);
            symbols[i] = record.getSymbol();
            record = null;
        }
        return symbols;
    }

    public String[] getSymbolIndex() {
        String[] symbols = new String[symbolIndex.size()];
        for (int i = 0; i < symbols.length; i++) {
            symbols[i] = (String) symbolIndex.elementAt(i);
        }
        return symbols;
    }

    public String[] readTickerData() {
        return getSymbolIndex();
    }

    public int getRowCount() {
        return symbolList.size();
    }

    public PortfolioRecord getValueAt(int row) {
        return symbolList.get(row);
    }

    public PortfolioRecord getRecord(String symbol) {
        PortfolioRecord record = null;

        for (int i = 0; i < symbolList.size(); i++) {
            record = symbolList.get(i);
            if (symbol == record.getSymbol())
                break;
            record = null;
        }
        return record;
    }

    public void setData(String data) {
        try {
            symbolList = Collections.synchronizedList(new LinkedList<PortfolioRecord>());
            StringTokenizer tokens = new StringTokenizer(data, ",");
            if ((tokens.countTokens() % 4) == 0) { // vlid portfolio string
                while (tokens.hasMoreElements()) {
                    PortfolioRecord record = new PortfolioRecord();
                    record.setSymbol(tokens.nextToken());
                    record.setHolding(Long.parseLong(tokens.nextToken()));
                    record.setAvgCost(Float.parseFloat(tokens.nextToken()));
                    record.setCoupon(Float.parseFloat(tokens.nextToken()));
                    //System.out.println("************ " + record.getCoupon());
                    record.setStore(this);
                    symbolList.add(record);
                    if (!symbolIndex.contains(record.getSymbol())) {
                        symbolIndex.addElement(record.getSymbol());
                    }
                    record = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        String tempString = "";

        for (int i = 0; i < symbolList.size(); i++) {
            PortfolioRecord record = symbolList.get(i);
            tempString += "," + record.toString();
            record = null;
        }
        if (tempString.length() > 0)
            return tempString.substring(1);
        else
            return "";
    }

    /* Total values */
    public double getTotalCost() {
        int size = symbolList.size();
        double totalCost = 0;

        for (int i = 0; i < size; i++) {
            PortfolioRecord record = getValueAt(i);
            totalCost += record.getTotalCost();
            record = null;
        }
        return totalCost;
    }

    public double[] getTotalMarketValue() {
        int size = symbolList.size();
        double totalMarketValue = 0;

        for (int i = 0; i < size; i++) {
            PortfolioRecord record = getValueAt(i);
            totalMarketValue += record.getMarketValue()[0];
            record = null;
        }
        this.totalMarketValue[0] = totalMarketValue;
        return this.totalMarketValue;
    }

    public double[] getTotalGainLoss() {
        int size = symbolList.size();
        double totalGainLoss = 0;

        for (int i = 0; i < size; i++) {
            PortfolioRecord record = getValueAt(i);
            totalGainLoss += record.getGainLoss()[0];
            record = null;
        }
        this.totalGainLoss[0] = totalGainLoss;
        return this.totalGainLoss;
    }

    public double[] getTotalPctGainLoss() {
        double totalCost = getTotalCost();
        double totalGainLoss = getTotalGainLoss()[0];
        if ((totalCost > 0) && (totalGainLoss != 0)) {
            totalPctGainLoss[0] = (totalGainLoss * 100) / totalCost;
        } else {
            totalPctGainLoss[0] = 0;
        }
        return totalPctGainLoss;
    }
}