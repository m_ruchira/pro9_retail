// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.datastore;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;

import java.util.Enumeration;
import java.util.StringTokenizer;

public class SymbolList implements Enumeration {

    public static boolean isUnclassifiedSectorNeeds = false;
    String g_sSector;
    String g_sAssetClass;
    String g_sPattern;
    String exchange;
    Enumeration g_oSymbols;
    String g_sNextElement = null;
    String market = null;
    boolean ishasMarket = false;
    boolean defaultSector = false;
    Enumeration<Sector> sectorList = null;
    int i = 0;

    /**
     * Constructor
     */
    public SymbolList(String exchange, String sSector, String sAssetClass, String sPattern) {
        isUnclassifiedSectorNeeds = false;
        this.exchange = exchange;
        g_oSymbols = DataStore.getSharedInstance().getSymbols(exchange);
        g_sSector = sSector;
        g_sAssetClass = sAssetClass;
        g_sPattern = sPattern.toUpperCase();
        if (g_sSector.equals(Constants.DEFAULT_SECTOR)) {
            defaultSector = true;
            sectorList = SectorStore.getSharedInstance().getSectors(exchange);
        }
    }

    public SymbolList(String exchange, String marketID, String sSector, String sAssetClass, String sPattern) {
        isUnclassifiedSectorNeeds = false;
        this.exchange = exchange;
        g_oSymbols = DataStore.getSharedInstance().getSymbols(exchange);
        g_sSector = sSector;
        g_sAssetClass = sAssetClass;
        g_sPattern = sPattern.toUpperCase();
        this.market = marketID;
        if (marketID != null) {
            ishasMarket = true;
        }
        if (g_sSector.equals(Constants.DEFAULT_SECTOR)) {
            defaultSector = true;
            sectorList = SectorStore.getSharedInstance().getSectors(exchange);
        }
    }

    public boolean hasMoreElements() {
        String sElement;
        String sSector;
        String sAssetClass;
        String sMarket = null;
        if (defaultSector) {
            if (ishasMarket) {
                while (g_oSymbols.hasMoreElements()) {
                    sElement = (String) g_oSymbols.nextElement();
                    sSector = DataStore.getSharedInstance().getSectorCode(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    sAssetClass = DataStore.getSharedInstance().getAssetClass(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    sMarket = DataStore.getSharedInstance().getMarketID(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
//                System.out.println("symbol ="+sElement+" , market ID ="+sMarket);
                    i++;
                    if ((!g_sAssetClass.equals("")) && (sAssetClass == null)) continue;
                    if ((!g_sSector.equals("")) && (sSector == null)) continue;
                    if ((!market.equals("")) && (sMarket == null)) continue;

                    StringTokenizer fields = new StringTokenizer(sElement, "~");
                    String symbol = fields.nextToken();
                    String insType = fields.nextToken();
                    if (isEqual(sSector, g_sSector) && isEqual(sAssetClass, g_sAssetClass) && isEqual(sMarket, market)
                            && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0) && (Integer.parseInt(insType) != Meta.INSTRUMENT_INDEX)) {
                        g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
                        return true;
                    }
                }
            } else {
                while (g_oSymbols.hasMoreElements()) {
                    sElement = (String) g_oSymbols.nextElement();
                    sSector = DataStore.getSharedInstance().getSectorCode(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    sAssetClass = DataStore.getSharedInstance().getAssetClass(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    i++;
                    if ((!g_sAssetClass.equals("")) && (sAssetClass == null)) continue;
                    if ((!g_sSector.equals("")) && (sSector == null)) continue;
                    StringTokenizer fields = new StringTokenizer(sElement, "~");
                    String symbol = fields.nextToken();
                    String insType = fields.nextToken();
                    if (isEqual(sSector, g_sSector) && isEqual(sAssetClass, g_sAssetClass)
                            && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0) && (Integer.parseInt(insType) != Meta.INSTRUMENT_INDEX)) {

                        g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
                        return true;
                    }

                }
            }

            /*{
                while (g_oSymbols.hasMoreElements()) {
                    sElement = (String) g_oSymbols.nextElement();
                    sSector = DataStore.getSharedInstance().getSectorCode(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    sAssetClass = DataStore.getSharedInstance().getAssetClass(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    i++;
                         if(SharedMethods.getInstrumentFromExchangeKey(sElement) == Meta.INSTRUMENT_INDEX) continue;
                    if ((!g_sAssetClass.equals("")) && (sAssetClass == null)) continue;
                    if((sSector == null) || (sSector.equals("")) || (sSector.equals("N/A"))){
                        if (isEqual(sAssetClass, g_sAssetClass)
                            && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0))
                        {
//                            System.out.println("1111 sElement, sSector, exchange =="+sElement +" ,"+ sSector + " , "+exchange);
                            g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
                            return true;
                        }
                    } else {
                        boolean isFound = false;
                        sectorList = SectorStore.getSharedInstance().getSectors(exchange);
                        while(sectorList.hasMoreElements()){
                            if(sectorList.nextElement().getId().equals(sSector)){
                                isFound  = true;
//                                break;
                            }
                        }
                        if(!isFound){
                            if (isEqual(sAssetClass, g_sAssetClass)
                                    && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0))
                            {
//                                System.out.println("2222 sElement, sSector, exchange =="+sElement +" ,"+ sSector + " , "+exchange);
                                g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
                                return true;
                            }
                        }
                    }
                }
            }

            */
        } else {
            if (ishasMarket) {
//                isUnclassifiedSectorNeeds = false;
                while (g_oSymbols.hasMoreElements()) {
                    sElement = (String) g_oSymbols.nextElement();
                    sSector = DataStore.getSharedInstance().getSectorCode(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    sAssetClass = DataStore.getSharedInstance().getAssetClass(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    sMarket = DataStore.getSharedInstance().getMarketID(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
//                System.out.println("symbol ="+sElement+" , market ID ="+sMarket);
                    i++;
                    if ((!g_sAssetClass.equals("")) && (sAssetClass == null)) continue;
                    if ((!g_sSector.equals("")) && (sSector == null)) continue;
                    if ((!market.equals("")) && (sMarket == null)) continue;
                    if (isEqual(sSector, g_sSector) && isEqual(sAssetClass, g_sAssetClass) && isEqual(sMarket, market)
                            && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0)) {
                        g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
                        return true;
                    } else if (isEqual(sSector, Constants.DEFAULT_SECTOR) && isEqual(sAssetClass, g_sAssetClass)
                            && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0)) {
                        isUnclassifiedSectorNeeds = true;
//                        g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
//                        return true;
                    }
                }
            } else {
//                isUnclassifiedSectorNeeds = false;
                while (g_oSymbols.hasMoreElements()) {
                    sElement = (String) g_oSymbols.nextElement();
                    sSector = DataStore.getSharedInstance().getSectorCode(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    sAssetClass = DataStore.getSharedInstance().getAssetClass(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
                    i++;
                    if ((!g_sAssetClass.equals("")) && (sAssetClass == null)) continue;
                    if ((!g_sSector.equals("")) && (sSector == null)) continue;
                    if (isEqual(sSector, g_sSector) && isEqual(sAssetClass, g_sAssetClass)
                            && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0)) {
                        g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
                        return true;
                    } else if (isEqual(sSector, Constants.DEFAULT_SECTOR) && isEqual(sAssetClass, g_sAssetClass)
                            && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0)) {
                        isUnclassifiedSectorNeeds = true;
//                        g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
//                        return true;
                    }

                }
            }
        }
//        while (g_oSymbols.hasMoreElements()) {
//            sElement = (String) g_oSymbols.nextElement();
//            sSector = DataStore.getSharedInstance().getSectorCode(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
//            sAssetClass = DataStore.getSharedInstance().getAssetClass(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
//            if(market!=null){
//                sMarket = DataStore.getSharedInstance().getMarketID(exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement);
//            }
//            i++;
//            if ((!g_sAssetClass.equals("")) && (sAssetClass == null)) continue;
//            if ((!g_sSector.equals("")) && (sSector == null)) continue;
//            if(market!=null){
//                if ((!market.equals("")) && (sMarket == null)) continue;
//                if (isEqual(sSector, g_sSector) && isEqual(sAssetClass, g_sAssetClass) && isEqual(sMarket, market)
//                        && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0))
//                {
//                    g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
//                    return true;
//                }
//            } else {
//                if (isEqual(sSector, g_sSector) && isEqual(sAssetClass, g_sAssetClass)
//                        && (DataStore.getSharedInstance().getCompanyName(sElement).toUpperCase().indexOf(g_sPattern) >= 0))
//                {
//                    g_sNextElement = exchange + Constants.KEY_SEPERATOR_CHARACTER + sElement;
//                    return true;
//                }
//            }
//
//        }
        return false;
    }

    public Object nextElement() {
        return g_sNextElement;
    }

    private boolean isEqual(String sSource, String sCompare) {
        if (((sSource != null) && (sSource.equals(sCompare))) || sCompare.equals(""))
            return true;
        else
            return false;
    }


    public String getSymbols() {
        String sList = "";

        while (hasMoreElements()) {
            sList += ("," + nextElement());
        }
        return sList.substring(0);
    }
}

 