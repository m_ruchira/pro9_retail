package com.isi.csvr.datastore;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 8, 2005
 * Time: 12:02:23 PM
 */
public class Currency implements Serializable {
    double rate;

    public Currency(double rate) {
        this.rate = rate;
    }
}
