package com.isi.csvr.datastore;

import com.isi.csvr.event.CurrencyListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.shared.*;
import com.isi.csvr.util.Decompress;

import java.io.*;
import java.util.*;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 23, 2005
 * Time: 4:06:43 PM
 */
public class CurrencyStore implements ExchangeListener {
    private static CurrencyStore self = null;

    private static HashMap<String, HashMap<String, Currency>> store;
    private static Hashtable<String, CurrencyData> descriptions;
    private static ArrayList<CurrencyListener> listeners;
    private static ArrayList<String> currencyIndex;

    private CurrencyStore() {
        store = new HashMap<String, HashMap<String, Currency>>();
        descriptions = new Hashtable<String, CurrencyData>();
        listeners = new ArrayList<CurrencyListener>();
        currencyIndex = new ArrayList<String>();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath() + "datastore/currencies.mdf"));
            store = (HashMap<String, HashMap<String, Currency>>) in.readObject();
            try {
                currencyIndex = (ArrayList<String>) in.readObject();
            } catch (IOException e) {
                currencyIndex = new ArrayList<String>();
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (ClassNotFoundException e) {
                currencyIndex = new ArrayList<String>();
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            in.close();
            in = null;
        } catch (Exception e) {
            store = new HashMap<String, HashMap<String, Currency>>();
            e.printStackTrace();
        }
    }

    public static synchronized CurrencyStore getSharedInstance() {
        return self;
    }

    public static void initialize() {
        if (self == null) {
            self = new CurrencyStore();
        }
//        self.loadCurrencyDetails();
        ExchangeStore.getSharedInstance().addExchangeListener(self);
    }

    private static void updateCurrencyIndex(String currency) {
        if (!currencyIndex.contains(currency)) {
            currencyIndex.add(currency);
        }
    }

    public static ArrayList<String> getCurrencyIndex() {
        return currencyIndex;
    }

    private static synchronized Currency getCurrencyObject(String base, String target) {
        HashMap<String, Currency> map = store.get(base);
        if (map == null) {
            map = new HashMap<String, Currency>();
            store.put(base, map);
        }
        Currency currency = map.get(target);
        if (currency == null) {
            currency = new Currency(0);
            map.put(target, currency);
        }
        return currency;
    }

    /**
     * returns the cross currencsy rate for given symbols
     *
     * @param base
     * @param target
     * @return rate or 0 if error
     */
    public static double getRate(String base, String target) {
        try {
            if (base.equals(target)) {
                return 1;
            } else {
                return getCurrencyObject(base, target).rate;
            }
        } catch (Exception e) {
            return 0D;
        }
    }

    public static double getAdjustedPriceFor(String base, String ref, double value) {
        try {
            double rate = getRate(base, ref);
            return value * rate;
        } catch (Exception e) {
            return value;
        }
    }

    public static float getAdjustedPriceFor(String base, String ref, float value) {
        try {
            double rate = getRate(base, ref);
            return (float) (value * rate);
        } catch (Exception e) {
            return value;
        }
    }

    public static boolean isValidCurrency(String curr) {
        if (store.containsKey(curr)) {
            return true;
        } else {
            return false;
        }
    }

    public void clearCurrencies() {
        store.clear();
//        currencyIndex.clear();
//        loadCurrencyDetails();
    }

    public void save() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Settings.getAbsolutepath() + "datastore/currencies.mdf"));
            out.writeObject(store);
            out.writeObject(currencyIndex);
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the currency record specified in the string to the store
     *
     * @param currencyData
     */
    public void addCurrency(String currencyData) {
        try {
            String[] records = currencyData.split("\\|");
            for (int i = 0; i < records.length; i++) {
                String[] fields = records[i].split(",");
                addCurrency(fields[1], fields[0], Double.parseDouble(fields[2]));
                fields = null;
            }
            records = null;
            fireCurrencyAdded();
        } catch (Exception e) {
            // do nothing
            e.printStackTrace();
        }
    }

    /**
     * Add/set currency data to the store
     *
     * @param base   currency
     * @param target currency
     * @param rate
     */
    private void addCurrency(String base, String target, double rate) {
        Currency currency = getCurrencyObject(base, target);

        currency.rate = rate;
        currency = null;
    }

    public int getSize() {
        return descriptions.size();
    }

    private void loadCurrencyDetails() {
        try {
            //SmartProperties properties = new SmartProperties(Meta.DS);
            String record;
            boolean bIsLangSpec = false;
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/currencies_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            bIsLangSpec = f.exists();
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out;
            //if (bIsLangSpec){
            out = decompress.setFiles(sLangSpecFileName);
            //} else {
            //    out = decompress.setFiles("system/currencies.msf");
            //}

            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            do {
                record = SharedMethods.readLine(in);
                try {
                    if (record != null) {
                        record = record.trim();
                        String[] fields = record.split(Meta.DS);
                        String key = fields[0];
                        CurrencyData currencyData = new CurrencyData();
                        currencyData.description = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(fields[1]));
                        currencyData.decimalPlaces = Integer.parseInt(fields[2]);
//                    System.out.println("description in currencies = "+fields[1]);
//                    System.out.println("currency descriptions = "+currencyData.description );
                        descriptions.put(key, currencyData);
                        updateCurrencyIndex(key);
                    }
                    Collections.sort(currencyIndex);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            } while (record != null);

            /*while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String description = Language.getLanguageSpecificString(properties.getProperty(key));
                descriptions.put(key, UnicodeUtils.getNativeString(description));
                updateCurrencyIndex(key);
            }
            properties = null;*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Enumeration<String> getCurrencyIDs() {
        return descriptions.keys();
    }

    public String getCurrencyDescription(String id) {
        return descriptions.get(id).description;
    }

    public ArrayList<String> getSortedCurrencyIDs() {
        ArrayList<String> currencyIDs = new ArrayList<String>();
        CurrencyData temp;
        Enumeration<String> ids = descriptions.keys();
        while (ids.hasMoreElements()) {
            String id = ids.nextElement();
            temp = (CurrencyData) descriptions.get(id);
            currencyIDs.add(temp.getDescription() + Constants.KEY_SEPERATOR_CHARACTER + id);

        }
        Collections.sort(currencyIDs);
        return currencyIDs;
    }

    public int getDecimalPlaces(String id) {
        return descriptions.get(id).decimalPlaces;
    }

    public void addCurrencyListener(CurrencyListener listener) {
        listeners.add(listener);
    }

    public void removeCurrencyListener(CurrencyListener listener) {
        listeners.remove(listener);
    }

    public void notifyCurrenciesLoaded() {
        fireCurrencyAdded();
    }

    /*public void print() {
        Set<String> ids = store.keySet();
        for(String id: ids){
            System.out.println(id + ":" + store.get(id).base  + "->" + store.get(id).ref  + "=" + store.get(id).rate);
        }
    }*/

    private void fireCurrencyAdded() {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                CurrencyListener listener = listeners.get(i);
                listener.currencyAdded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        loadCurrencyDetails(); // load the currency file again
    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    class CurrencyData {
        public String description;
        public int decimalPlaces;

        public String getDescription() {
            return description;
        }
    }
}
