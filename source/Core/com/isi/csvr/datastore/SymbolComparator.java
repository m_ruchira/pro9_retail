package com.isi.csvr.datastore;

// Copyright (c) 2000 Home

import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;

import java.util.Comparator;

public class SymbolComparator implements Comparator {

    private int sortColumn = -1;
    private int sortOrder = 1;

    private int colcount = 0;

    /**
     * Constructor
     */


    public int compare(Object o1, Object o2) {
        Stock stock1;
        Stock stock2;
        try {
            stock1 = DataStore.getSharedInstance().getStockObject((String) o1);
            stock2 = DataStore.getSharedInstance().getStockObject((String) o2);
            if (getColumnCount() > sortColumn)
                return StockData.compare(stock1, stock2, sortColumn, sortOrder);
            else {
                int column = sortColumn - getColumnCount();
                return CustomFormulaStore.getSharedInstance().compare(stock1, stock2, column, sortOrder);
            }
        } catch (Exception e) {
            return 0;
        } finally {
            stock1 = null;
            stock2 = null;
        }
    }


    public int getColumnCount() {
        if (colcount == 0) {
            colcount = Language.getList("TABLE_COLUMNS").length;
        }
        return colcount;
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public void setSortColumn(int column) {
        sortColumn = column;
    }

    public void setSortOrder(boolean sortOrder) {
        if (sortOrder)
            this.sortOrder = 1;
        else
            this.sortOrder = -1;
    }

    public void setSortColumn(int column, boolean sortOrder) {
        this.sortColumn = column;
        setSortOrder(sortOrder);
    }
}