package com.isi.csvr.datastore;

import com.isi.csvr.shared.Settings;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 31, 2005
 * Time: 1:05:47 PM
 */
public class ValidatedSymbols {
    private static ValidatedSymbols self = null;

    private Hashtable<String, String> symbols;

    public ValidatedSymbols() {
        loadData();
    }

    public static synchronized ValidatedSymbols getSharedInstance() {
        if (self == null) {
            self = new ValidatedSymbols();
        }
        return self;
    }

    public void addSymbol(String key, String frame) {
        symbols.put(key, frame);
        DataStore.getSharedInstance().setValidatedDataForStock(key, frame);
    }

    public String getFrame(String key) {
        return symbols.get(key);
    }

    public void removeSymbol(String key) {
        symbols.remove(key);
    }

    public Enumeration<String> getSymbols() {
        return symbols.keys();
    }

    /**
     * Save the hash table as an object to the disk
     */
    public void saveData() {
        try {
            trimValidateSymbolStore();

            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/validated.msf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(symbols);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*public void trimValidateSymbolStore() {
        Enumeration<String> validatedSymbols = getSymbols();
        while (validatedSymbols.hasMoreElements()) {
            String key = validatedSymbols.nextElement();
            if (!DataStore.getSharedInstance(). isValidSymbol(key)) {
                removeSymbol(key);
            }
        }
    }*/

    public void trimValidateSymbolStore() {
        Enumeration<String> validatedSymbols = getSymbols();
        boolean isFound = false;
        while (validatedSymbols.hasMoreElements()) {
            String key = validatedSymbols.nextElement();
            isFound = false;
            if (!DataStore.getSharedInstance().isValidSymbol(key)) {
                for (WatchListStore store : WatchListManager.getInstance().getStores()) {
                    if (isFound) break;
                    for (String symbol : store.getSymbols()) {
                        if (isFound) break;
                        if (symbol != null && symbol.equals(key)) {
                            isFound = true;
                        }
                    }
                }
                if (!isFound) {
                    removeSymbol(key);
                }
            }
        }
    }

    /**
     * Loads the saved hash table from the disk as an object
     */
    public void loadData() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/validated.msf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            symbols = (Hashtable<String, String>) oObjIn.readObject();
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (symbols == null) {
            symbols = new Hashtable<String, String>();
        }

    }
}
