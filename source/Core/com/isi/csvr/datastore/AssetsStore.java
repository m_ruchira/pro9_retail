package com.isi.csvr.datastore;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.util.Decompress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 3, 2005
 * Time: 10:51:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class AssetsStore {

    private static AssetsStore self = null;
    public Hashtable<String, String> store;

    private AssetsStore() {
        store = new Hashtable<String, String>();
        loadAssetsList();
    }

    public static synchronized AssetsStore getSharedInstance() {
        if (self == null) {
            self = new AssetsStore();
        }
        return self;
    }

    public void loadAssetsList() {
        if (store != null) {
            store.clear();
        }
        try {
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/assets_" + Language.getSelectedLanguage() + ".msf";
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            String des = null;
            while ((record = in.readLine()) != null) {
                String[] data = record.split(Meta.DS);
                des = Language.getLanguageSpecificString(data[1]);
                des = UnicodeUtils.getNativeString(des);
                store.put(data[0], des);
            }
            out = null;
            decompress = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getAssetDescription(String id) {
        return store.get(id);
    }

    public Enumeration<String> getAssetDescriptions() {
        return store.elements();
    }

    public Enumeration<String> getAssetIds() {
        return store.keys();
    }
}
