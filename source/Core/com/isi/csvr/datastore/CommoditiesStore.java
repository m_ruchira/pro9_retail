package com.isi.csvr.datastore;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 7, 2009
 * Time: 2:41:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommoditiesStore {

    private static CommoditiesStore self;
    private static SmartProperties prop;
    private ArrayList<CommodityItem> commodityList;

    private CommoditiesStore() {
        commodityList = new ArrayList<CommodityItem>();
        prop = new SmartProperties("=", true);
    }

    public static CommoditiesStore getSharedInstance() {
        if (self == null) {
            self = new CommoditiesStore();
        }
        return self;
    }

    public void loadCommoditiesFile() {
        try {
            prop.loadCompressed(Settings.SYSTEM_PATH + "/commodities_" + Language.getSelectedLanguage() + ".msf");
        } catch (Exception s_e) {
        }
        Enumeration enu = prop.keys();
        while (enu.hasMoreElements()) {
            String data = (String) enu.nextElement();
            String[] sList = data.split(",");
            if (sList != null && sList.length == 3) {
                String symbol = "";
                String delayed = sList[0];
                String realTime = sList[1];
                String desc = sList[2];
                symbol = delayed;
                boolean valid = ExchangeStore.getSharedInstance().isValidExchange(SharedMethods.getExchangeFromKey(realTime));
                boolean validDelayed = ExchangeStore.getSharedInstance().isValidExchange(SharedMethods.getExchangeFromKey(delayed));
                if (valid) {
                    symbol = realTime;
                }
                if (valid || validDelayed) {
                    commodityList.add(new CommodityItem(symbol, desc, !valid));
                }
//             String msgData= ValidatedSymbols.getSharedInstance().getFrame(symbol);
//             if(msgData==null){
//                 SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(symbol),"Commodities", SharedMethods.getExchangeFromKey(symbol)
//                 , SharedMethods.getInstrumentTypeFromKey(symbol));
//             }
            }
        }


    }

    public void sendValidationRequests() {
        for (int i = 0; i < commodityList.size(); i++) {
            CommodityItem item = commodityList.get(i);
            String msgData = ValidatedSymbols.getSharedInstance().getFrame(item.sKey);
            if (msgData == null) {
                SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(item.sKey), "Commodities", SharedMethods.getExchangeFromKey(item.sKey)
                        , SharedMethods.getInstrumentTypeFromKey(item.sKey));
            }
        }
    }

    public void addSymbolRequest(String sKey) {
        if (ValidatedSymbols.getSharedInstance().getFrame(sKey) == null) {
            SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(sKey), "Commodities", SharedMethods.getExchangeFromKey(sKey)
                    , SharedMethods.getInstrumentTypeFromKey(sKey));
        } else {
            DataStore.getSharedInstance().addSymbolRequest(sKey);
        }

    }

    public void clear() {
        prop.clear();
        commodityList.clear();
    }

    public ArrayList<CommodityItem> getCommodityList() {
        return commodityList;
    }

    public class CommodityItem {
        public String sKey;
        public String description;
        public boolean delayed = false;
        public CommodityItem(String sKey, String description, boolean delayed) {
            this.sKey = sKey;
            this.description = description;
            this.delayed = delayed;
        }

    }

}
