// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.datastore;

/**
 * Maintains the symbol list to be
 * used by the table object
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.ticker.custom.TickerDataInterface;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class Symbols extends Object implements TickerDataInterface {

    public static int FILTER_BY_CURRENCY = 1;
    public static int FILTER_BY_MARKET = 2;
    public static long sequence = 0;
    private String[] g_asSymbols;
    private String[] g_asFilteredSymbols;
    private String[] g_asIndexList;
    private Hashtable g_asOriginalList;
    private SymbolFilter filter = null;
    private SymbolComparator symbolComparator;
    private int sortColumn = -1;
    private boolean sortOrder = true;
    private boolean unsortable = false;
    private boolean registerFlag = false;

//    public boolean  temp = false;
//

    /**
     * Constructor
     */
    public Symbols() {
        this.unsortable = false;
        g_asSymbols = new String[0];
        symbolComparator = new SymbolComparator();
    }

    /*
      This method is for the use of chart only
    */
    public Symbols(byte type) {
        this();
    }

    public Symbols(boolean unsortable) {
        this.unsortable = unsortable;
        g_asSymbols = new String[0];
        if (isUnsortable())
            g_asOriginalList = new Hashtable();
        symbolComparator = new SymbolComparator();
    }

    public Symbols(String sSymbols, boolean unsortable) {
        int i = 0;
        int r = 0;

        symbolComparator = new SymbolComparator();
        this.unsortable = unsortable;
        if (isUnsortable())
            g_asOriginalList = new Hashtable();
        StringTokenizer tokens = new StringTokenizer(sSymbols, ",");
        i = tokens.countTokens();

        g_asSymbols = new String[i];
        while (tokens.hasMoreTokens()) {
            g_asSymbols[r] = tokens.nextToken().toUpperCase();
            if (isUnsortable())
                g_asOriginalList.put(g_asSymbols[r], new Long(getSequence()));
            r++;
        }
        applyFilter();
    }

    public synchronized static long getSequence() {
        return sequence++;
    }

    public synchronized void setSymbols(String sSymbols) {
        int i = 0;
        int r = 0;

        StringTokenizer tokens = new StringTokenizer(sSymbols, ",");
        i = tokens.countTokens();

        g_asSymbols = new String[i];
        while (tokens.hasMoreTokens()) {
            g_asSymbols[r] = tokens.nextToken();
            if (isUnsortable())
                g_asOriginalList.put(g_asSymbols[r], new Long(getSequence()));
            r++;
        }
        applyFilter();
    }

    public synchronized void reallocateIndex() {
        g_asIndexList = new String[g_asFilteredSymbols.length];
        System.arraycopy(g_asFilteredSymbols, 0, g_asIndexList, 0,
                g_asFilteredSymbols.length);
        try {
            Arrays.sort(g_asIndexList);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

//    public synchronized String getFilter()
//	{
//		return g_sFilter;
//    }

    /**
     * Apply the filter to the symbol map
     */
    private synchronized void applyFilter() {
        if (filter == null) {
            g_asFilteredSymbols = g_asSymbols;
            reallocateIndex();
        } else {
            //g_asFilteredSymbols = new String[g_asSymbols.length];

            g_asFilteredSymbols = filter.getFilteredList(g_asSymbols);
            reallocateIndex();

//            if (filterType == FILTER_BY_CURRENCY){
//                for(int i = 0;i<g_asSymbols.length;i++){
//                    if (g_sFilter.equals(DataStore.getActiveCurrency(g_asSymbols[i]))){
//                        g_asFilteredSymbols[j] = g_asSymbols[i];
//                        j++;
//                    }
//                }
//            }else{
//                for(int i = 0;i<g_asSymbols.length;i++){
//                    if (g_sFilter.equals(DataStore.getAssetClass(g_asSymbols[i]))){
//                        g_asFilteredSymbols[j] = g_asSymbols[i];
//                        j++;
//                    }
//                }
//            }
//        	String[] asSelected = new String[j];
//			System.arraycopy(g_asFilteredSymbols,0,asSelected,0,j);
//    	    g_asFilteredSymbols = asSelected;
        }
    }

    public void reFilter() {
        if (filter != null) {
            applyFilter();
            sortSymbols();
        }
    }

    public SymbolFilter getFilter() {
        return filter;
    }

    /**
     * Sets the filter to the symbol map
     */
    public synchronized void setFilter(SymbolFilter filter) {
        //filterType = type;
        this.filter = filter;
        applyFilter();
    }

    /**
     * Returns the symbols array
     */
    public synchronized String[] getSymbols() {
        return g_asSymbols;
    }

    public synchronized void setSymbols(String[] asSymbols) {
        g_asSymbols = asSymbols;
        applyFilter();
        if (isUnsortable())
            reallocateOriginalList();
    }

    /**
     * Returns the filtered symbols array
     */
    public synchronized String[] getFilteredSymbols() {
        return g_asFilteredSymbols;
    }

    public String[] readTickerData() {
        return g_asFilteredSymbols;
    }

    /**
     * Returns the index symbols array
     */
    public synchronized String[] getSymbolIndex() {
        return g_asIndexList;
    }

    /**
     * Appends a new symbol to the symbol list
     */
    public synchronized boolean appendSymbol(String sSymbol) {

        /* do no append if the symbol is already there*/
        if (isAlreadyIn(sSymbol)) return false;

        int i = 0;
        String[] asData = new String[g_asSymbols.length + 1];
        //String[] asOrigData = new String[g_asSymbols.length+1];
        while (i < g_asSymbols.length) {
            asData[i] = g_asSymbols[i];
            i++;
        }
        asData[i] = sSymbol;
        g_asSymbols = asData;
        if ((isUnsortable()) && (!g_asOriginalList.containsKey(sSymbol))) {
            g_asOriginalList.put(sSymbol, new Long(getSequence()));
        }
        applyFilter();
        return true;
    }

    public boolean insertSymbol(String sSymbol, int location) {
        try {
            if (isAlreadyIn(sSymbol)) return false;
            if (location > g_asFilteredSymbols.length - 1) {
                String[] asData = new String[g_asSymbols.length + 1];
                System.arraycopy(g_asFilteredSymbols, 0, asData, 0, g_asFilteredSymbols.length);
                asData[g_asFilteredSymbols.length] = sSymbol;
                g_asFilteredSymbols = asData;
                g_asSymbols = new String[g_asFilteredSymbols.length];
                System.arraycopy(g_asFilteredSymbols, 0, g_asSymbols, 0, g_asFilteredSymbols.length);
                asData = null;
                return true;
            }
            //int i=0;
            String[] asData = new String[g_asFilteredSymbols.length + 1];
            for (int i = 0; i < location; i++) {
                asData[i] = g_asFilteredSymbols[i];
            }
            asData[location] = sSymbol;
            for (int i = location + 1; i <= g_asFilteredSymbols.length; i++) {
                asData[i] = g_asFilteredSymbols[i - 1];
            }

            g_asFilteredSymbols = asData;
            g_asSymbols = new String[g_asFilteredSymbols.length];
            System.arraycopy(g_asFilteredSymbols, 0, g_asSymbols, 0, g_asFilteredSymbols.length);
            reallocateIndex();
            //applyFilter();
            asData = null;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean moveSymbol(int sourceIndex, int destIndex) {
        try {
            String movingSymbol = g_asFilteredSymbols[sourceIndex];
            if (destIndex > sourceIndex) {
                for (int i = sourceIndex; i < destIndex - 1; i++) {
                    g_asFilteredSymbols[i] = g_asFilteredSymbols[i + 1];
                }
                g_asFilteredSymbols[destIndex - 1] = movingSymbol;
            } else if (destIndex < sourceIndex) {
                for (int i = sourceIndex; i > destIndex; i--) {
                    g_asFilteredSymbols[i] = g_asFilteredSymbols[i - 1];
                }
                g_asFilteredSymbols[destIndex] = movingSymbol;
            }
            System.arraycopy(g_asFilteredSymbols, 0, g_asSymbols, 0, g_asFilteredSymbols.length);
            movingSymbol = null;
            //applyFilter();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Checks if the symbol is already there.
     */
    public synchronized boolean isAlreadyIn(String sNewSymbol) {
        int i = 0;
        while (i < g_asSymbols.length) {
            if (g_asSymbols[i].equals(sNewSymbol))
                return true;
            i++;
        }
        return false;
    }

    /**
     * Checks location of the symbol in the list
     */
    public synchronized int getLocation(String sNewSymbol) {
        int i = 0;
        while (i < g_asFilteredSymbols.length) {
            if (g_asFilteredSymbols[i].equals(sNewSymbol))
                return i;
            i++;
        }
        return -1;
    }

    /**
     * Remove all symbols from the list
     */
    public synchronized void clear() {
        g_asFilteredSymbols = new String[0];
        g_asSymbols = new String[0];
        if (isUnsortable())
            g_asOriginalList.clear();
    }

    /**
     * Remove a symbol from the list
     */
    public synchronized void removeSymbol(String sSymbol) {

        int i = 0;
        int iPos = 0;
        boolean bFound = false;
        String[] asData = null;

        /* Check if available */
        while (i < g_asSymbols.length) {
            if (g_asSymbols[i].equals(sSymbol)) {
                bFound = true;
                iPos = i;
                break;
            }
            i++;
        }

        /* if the symbol is available, remove it */
        if (bFound) {
            asData = new String[g_asSymbols.length - 1];
            i = 0;
            while (i < iPos) {
                asData[i] = g_asSymbols[i];
                i++;
            }

            while (i < g_asSymbols.length - 1) {
                asData[i] = g_asSymbols[i + 1];
                i++;
            }
            g_asSymbols = asData;

        }

        if (isUnsortable())
            g_asOriginalList.remove(sSymbol);

        applyFilter();
    }

    /**
     * Returns the symbols string
     */
    public String toString() {
        String sSymbols = "";

        for (int i = 0; i < g_asSymbols.length; i++)
            sSymbols += ("," + g_asSymbols[i]);

        if (sSymbols.length() == 0)
            return sSymbols;
        else
            return sSymbols.substring(1);
    }

    public String getWatchlistString() {
        String sSymbols = "";

        for (int i = 0; i < g_asSymbols.length; i++)
            sSymbols += ("," + g_asSymbols[i]);

        if (sSymbols.length() == 0)
            return sSymbols;
        else
            return sSymbols.substring(1);
    }

    public String toFiltered() {
        String sSymbols = "";

        for (int i = 0; i < g_asSymbols.length; i++)
            sSymbols += ("," + g_asSymbols[i]);

        if (sSymbols.length() == 0)
            return sSymbols;
        else
            return sSymbols.substring(1);
    }

    /**
     * Returns the symbols string
     */
    public String toOroginalString() {
        Object[] list = g_asOriginalList.keySet().toArray();
        String sSymbols = "";
        SymbolUnsortComparator comparator = new SymbolUnsortComparator(g_asOriginalList);
        Arrays.sort(list, comparator);

        for (int i = 0; i < list.length; i++)
            sSymbols += ("," + (String) list[i]);

        comparator = null;
        if (sSymbols.length() == 0)
            return sSymbols;
        else
            return sSymbols.substring(1);
    }

    /**
     * list the symbols string
     */
    /*public void listx() {
        for (int i = 0; i < g_asFilteredSymbols.length; i++)
            System.out.println(g_asSymbols[i]);
    }*/
    public synchronized void sortSymbols() {
        try {
            Arrays.sort(getFilteredSymbols(), symbolComparator);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public synchronized void unsortSymbols() {
        try {
            setSortColumn(-1);
            SymbolUnsortComparator comparator = new SymbolUnsortComparator(g_asOriginalList);
            Arrays.sort(getFilteredSymbols(), comparator);
            comparator = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void sortSymbols(int column, boolean sortOrder) {
        this.sortColumn = column;
        this.sortOrder = sortOrder;
        symbolComparator.setSortColumn(column, sortOrder);
        sortSymbols();
    }

    public int getSortColumn() {
        return sortColumn;
    }

    public synchronized void setSortColumn(int column) {
        this.sortColumn = column;
        symbolComparator.setSortColumn(column);
    }

    public boolean getSortOrder() {
        return sortOrder;
    }

    public synchronized void setSortOrder(boolean sortOrder) {
        this.sortOrder = sortOrder;
        symbolComparator.setSortOrder(sortOrder);
    }

    private void reallocateOriginalList() {
        /* remove old symbols*/
        Enumeration symbols = g_asOriginalList.keys();
        String symbol;
        while (symbols.hasMoreElements()) {
            symbol = (String) symbols.nextElement();
            if (!isAlreadyIn(symbol)) {
                g_asOriginalList.remove(symbol);
            }
            symbol = null;
        }

        /* add new symbols */
        for (int i = 0; i < g_asSymbols.length; i++) {
            if (!g_asOriginalList.containsKey(g_asSymbols[i])) {
                g_asOriginalList.put(g_asSymbols[i], new Long(getSequence()));
            }
        }

    }

    private boolean isUnsortable() {
        return unsortable;
    }

    public synchronized boolean isRegisterFlagSet() {
        return registerFlag;
    }

    public synchronized void setRegisterFlag(boolean registerFlag) {
        this.registerFlag = registerFlag;
    }
}

