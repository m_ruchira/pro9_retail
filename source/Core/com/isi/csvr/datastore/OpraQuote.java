package com.isi.csvr.datastore;


import com.isi.csvr.shared.Constants;

import java.io.Serializable;

public class OpraQuote implements Serializable, Cloneable {

    // Data Storing Format - Calls are at 0 and Puts are at 1 location in the Array
    public byte iExchangeCode;
    public String[] sSymbol = new String[2];
    public String[] sMarketCenter = new String[2];
    public String[] sCurrency = new String[2];
    ;

    public float[] dBestBid = new float[4];
    public int[] lBestBidQty = new int[2];
    public float[] dBestAsk = new float[4];
    public int[] lBestAskQty = new int[2];

    public float[] dLastTrade = new float[4];
    public int[] lLastTradeQty = new int[2];
    public long[] lLastTradeTime = new long[2];
    public double[] dNetChange = new double[2];

    public long[] lTodaysVolume = new long[2];
    public long[] lYesterdaysVolume = new long[2];

    public float[] dPreviousClose = new float[2];
    public float[] dTodaysOpen = new float[2];
    public float[] dTodaysHigh = new float[2];
    public float[] dTodaysLow = new float[2];
    public float[] dTodaysClose = new float[2];
    public float[] dCurrentPrice = new float[2];

    public String[] sQuoteCondition = new String[2];
    public int[] iOpenInterest = new int[2];

    public float dStrikePrice;

    /*public OpraQuote(byte iExCode, float strikePrice) {
        iExchangeCode = iExCode;
        dStrikePrice  = strikePrice;
        init();
    }*/

    public OpraQuote(byte iExCode) {
        iExchangeCode = iExCode;
        init();
    }

    private void init() {
        dBestBid[0] = -1;
        dBestBid[1] = -1;
        lBestBidQty[0] = -1;
        lBestBidQty[1] = -1;
        dBestAsk[0] = -1;
        dBestAsk[1] = -1;
        lBestAskQty[0] = -1;
        lBestAskQty[1] = -1;

        dLastTrade[0] = -1;
        dLastTrade[1] = -1;
        lLastTradeQty[0] = -1;
        lLastTradeQty[1] = -1;
        lLastTradeTime[0] = -1;
        lLastTradeTime[1] = -1;
        dNetChange[0] = Constants.DEFAULT_DOUBLE_VALUE + 1;
        dNetChange[1] = Constants.DEFAULT_DOUBLE_VALUE + 1;

        lTodaysVolume[0] = -1;
        lTodaysVolume[1] = -1;
        lYesterdaysVolume[0] = -1;
        lYesterdaysVolume[1] = -1;

        dPreviousClose[0] = -1;
        dPreviousClose[1] = -1;
        dTodaysOpen[0] = -1;
        dTodaysOpen[1] = -1;
        dTodaysHigh[0] = -1;
        dTodaysHigh[1] = -1;
        dTodaysLow[0] = -1;
        dTodaysLow[1] = -1;
        dTodaysClose[0] = -1;
        dTodaysClose[1] = -1;
        dCurrentPrice[0] = -1;
        dCurrentPrice[1] = -1;

        iOpenInterest[0] = -1;
        iOpenInterest[1] = -1;
    }

    public void clear() {
        sSymbol = new String[2];
        sMarketCenter = new String[2];
        sCurrency = new String[2];
        ;

        dBestBid = new float[4];
        lBestBidQty = new int[2];
        dBestAsk = new float[4];
        lBestAskQty = new int[2];

        dLastTrade = new float[4];
        lLastTradeQty = new int[2];
        lLastTradeTime = new long[2];
        dNetChange = new double[2];

        lTodaysVolume = new long[2];
        lYesterdaysVolume = new long[2];

        dPreviousClose = new float[2];
        dTodaysOpen = new float[2];
        dTodaysHigh = new float[2];
        dTodaysLow = new float[2];
        dTodaysClose = new float[2];
        dCurrentPrice = new float[2];

        sQuoteCondition = new String[2];
        iOpenInterest = new int[2];

        dStrikePrice = 0;
    }

    protected void finalize() {
        //ObjectWatcher.Unregister(this);
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public float[] getBestAsk(int index) {
        float askArray[] = new float[2];
        if (index == 0) {
            askArray[0] = dBestAsk[0];
            askArray[1] = dBestAsk[1];

            dBestAsk[1] = dBestAsk[0];
        } else {
            askArray[0] = dBestAsk[2];
            askArray[1] = dBestAsk[3];

            dBestAsk[3] = dBestAsk[2];
        }
        return askArray;
    }

    public void setBestAsk(int index, float newDBestAsk) {
        if (index == 0) {
            dBestAsk[1] = dBestAsk[0];
            dBestAsk[0] = newDBestAsk;
        } else {
            dBestAsk[3] = dBestAsk[2];
            dBestAsk[2] = newDBestAsk;
        }
        //dBestAsk[index] = newDBestAsk;
    }

    public float[] getBestBid(int index) {
        float bidArray[] = new float[2];
        if (index == 0) {
            bidArray[0] = dBestBid[0];
            bidArray[1] = dBestBid[1];

            dBestBid[1] = dBestBid[0];
        } else {
            bidArray[0] = dBestBid[2];
            bidArray[1] = dBestBid[3];

            dBestBid[3] = dBestBid[2];
        }
        return bidArray;
    }

    public void setBestBid(int index, float newDBestBid) {
        if (index == 0) {
            dBestBid[1] = dBestBid[0];
            dBestBid[0] = newDBestBid;
        } else {
            dBestBid[3] = dBestBid[2];
            dBestBid[2] = newDBestBid;
        }
        //dBestBid[index] = newDBestBid;
    }

    public float[] getCurrentPrice() {
        return dCurrentPrice;
    }

    public void setCurrentPrice(int index, float newDCurrentPrice) {
        dCurrentPrice[index] = newDCurrentPrice;
    }

    public float[] getLastTrade(int index) {
        float tradeArray[] = new float[2];
        if (index == 0) {
            tradeArray[0] = dLastTrade[0];
            tradeArray[1] = dLastTrade[1];

            dLastTrade[1] = dLastTrade[0];
        } else {
            tradeArray[0] = dLastTrade[2];
            tradeArray[1] = dLastTrade[3];

            dLastTrade[3] = dLastTrade[2];
        }
        return tradeArray;
        //return dLastTrade;
    }

    public void setLastTrade(int index, float newDLastTrade) {
        if (index == 0) {
            dLastTrade[1] = dLastTrade[0];
            dLastTrade[0] = newDLastTrade;
        } else {
            dLastTrade[3] = dLastTrade[2];
            dLastTrade[2] = newDLastTrade;
        }
        //dLastTrade[index] = newDLastTrade;
    }

    public double[] getNetChange() {
        return dNetChange;
    }

    public void setNetChange(int index, double newDNetChange) {
        dNetChange[index] = newDNetChange;
    }

    public float[] getPreviousClose() {
        return dPreviousClose;
    }

    public void setPreviousClose(int index, float newDPreviousClose) {
        dPreviousClose[index] = newDPreviousClose;
    }

    public float getStrikePrice() {
        return dStrikePrice;
    }

    public void setStrikePrice(float newDStrikePrice) {
        dStrikePrice = newDStrikePrice;
    }

    public float[] getTodaysClose() {
        return dTodaysClose;
    }

    public void setTodaysClose(int index, float newDTodaysClose) {
        dTodaysClose[index] = newDTodaysClose;
    }

    public float[] getTodaysHigh() {
        return dTodaysHigh;
    }

    public void setTodaysHigh(int index, float newDTodaysHigh) {
        dTodaysHigh[index] = newDTodaysHigh;
    }

    public float[] getTodaysLow() {
        return dTodaysLow;
    }

    public void setTodaysLow(int index, float newDTodaysLow) {
        dTodaysLow[index] = newDTodaysLow;
    }

    public float[] getTodaysOpen() {
        return dTodaysOpen;
    }

    public void setTodaysOpen(int index, float newDTodaysOpen) {
        dTodaysOpen[index] = newDTodaysOpen;
    }

    public byte getExchangeCode() {
        return iExchangeCode;
    }

    public void setExchangeCode(byte newIExchangeCode) {
        iExchangeCode = newIExchangeCode;
    }

    public int[] getOpenInterest() {
        return iOpenInterest;
    }

    public void setOpenInterest(int index, int newIOpenInterest) {
        iOpenInterest[index] = newIOpenInterest;
    }

    public int[] getBestAskQty() {
        return lBestAskQty;
    }

    public void setBestAskQty(int index, int newLBestAskQty) {
        lBestAskQty[index] = newLBestAskQty;
    }

    public int[] getBestBidQty() {
        return lBestBidQty;
    }

    public void setBestBidQty(int index, int newLBestBidQty) {
        lBestBidQty[index] = newLBestBidQty;
    }

    public int[] getLastTradeQty() {
        return lLastTradeQty;
    }

    public void setLastTradeQty(int index, int newLLastTradeQty) {
        lLastTradeQty[index] = newLLastTradeQty;
    }

    public long[] getLastTradeTime() {
        return lLastTradeTime;
    }

    public void setLastTradeTime(int index, long newLLastTradeTime) {
        lLastTradeTime[index] = newLLastTradeTime;
    }

    public long[] getTodaysVolume() {
        return lTodaysVolume;
    }

    public void setTodaysVolume(int index, long newLTodaysVolume) {
        lTodaysVolume[index] = newLTodaysVolume;
    }

    public long[] getYesterdaysVolume() {
        return lYesterdaysVolume;
    }

    public void setYesterdaysVolume(int index, long newLYesterdaysVolume) {
        lYesterdaysVolume[index] = newLYesterdaysVolume;
    }

    public String[] getCurrency() {
        return sCurrency;
    }

    public void setCurrency(int index, String newSCurrency) {
        sCurrency[index] = newSCurrency;
    }

    public String[] getMarketCenter() {
        return sMarketCenter;
    }

    public void setMarketCenter(int index, String newSMarketCenter) {
        sMarketCenter[index] = newSMarketCenter;
    }

    public String[] getQuoteCondition() {
        return sQuoteCondition;
    }

    public void setQuoteCondition(int index, String newSQuoteCondition) {
        sQuoteCondition[index] = newSQuoteCondition;
    }

    public String[] getSymbol() {
        return sSymbol;
    }

    public void setSymbol(int index, String newSSymbol) {
        sSymbol[index] = newSSymbol;
    }


}

