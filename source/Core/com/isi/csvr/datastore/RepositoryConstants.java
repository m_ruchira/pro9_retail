package com.isi.csvr.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 22, 2005
 * Time: 11:06:04 AM
 */
public interface RepositoryConstants {
//    int EXCHANGE = 0;
//    int SYMBOL = 1;
//    int DESCRIPTION = 2;
//    int TYPE = 3;
//    int REQUEST_COUNT = 4;
//    int SHORT_DESCRIPTION = 5;

    int EXCHANGE = 0;
    int SYMBOL = 1;
    int DESCRIPTION = 3;
    int TYPE = 4;
    int REQUEST_COUNT = 5;
    int SHORT_DESCRIPTION = 2;

    int TYPE_ANY = -1;
}
