package com.isi.csvr.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 28, 2005
 * Time: 11:26:28 AM
 */
public class Sector implements Comparable {
    private String id;
    private String description;

    //---------------- Added by Shanika-----

    private boolean selectedSector;

    public Sector(String id, String description) {
        this.description = description;
        this.id = id;
    }

    public boolean isSelectedSector() {
        return selectedSector;
    }

    public void setSelectedSector(boolean selectedSector) {
        this.selectedSector = selectedSector;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public int compareTo(Object other) {
        try {
            return description.compareToIgnoreCase(((Sector) other).getDescription());
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
