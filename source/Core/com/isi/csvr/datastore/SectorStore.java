package com.isi.csvr.datastore;

import com.isi.csvr.shared.*;
import com.isi.csvr.util.Decompress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 28, 2005
 * Time: 11:23:32 AM
 */
public class SectorStore {
    private static SectorStore self = null;
    private Hashtable<String, LinkedList<Sector>> exchanges;

    private SectorStore() {
        exchanges = new Hashtable<String, LinkedList<Sector>>();

        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            //if (exchange.isDefault()) {
            loadExchange(exchange.getSymbol());
            //}
            exchange = null;
        }
        exchanges = null;
    }

    public static synchronized SectorStore getSharedInstance() {
        if (self == null) {
            throw new RuntimeException("Sector Store not initialized");
        }
        return self;
    }

    public static void initialize() {
        if (self == null) {
            self = new SectorStore();
        }
    }

    public LinkedList<Sector> getSectorLIst(String exchange) {
        return exchanges.get(exchange);
    }

    public void setSectorLIst(String exchange, LinkedList<Sector> exchanges) {
        this.exchanges.put(exchange, exchanges);
    }

    public void loadExchange(String exchange) {
        try {
            boolean isUnclassified = false;
            LinkedList<Sector> sectors = exchanges.get(exchange);

            if (sectors != null) {
                for (int i = 0; i < sectors.size(); i++) {
                    Sector newSec = sectors.get(i);
                    if (newSec.getId().equals(Constants.DEFAULT_SECTOR)) {
                        isUnclassified = true;
                        break;
                    }
                }
            }
            if (sectors != null) {
                sectors.clear();
            } else {
                sectors = new LinkedList<Sector>();
                exchanges.put(exchange, sectors);
            }

            Decompress decompress = new Decompress();
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/sectors_" + Language.getSelectedLanguage() + ".msf";
            ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            if (isUnclassified) {
                Sector sector = new Sector(Constants.DEFAULT_SECTOR, Language.getString("UNCLASSIFIED_SECTOR"));
                sectors.add(sector);
            }

            do {
                record = null;
                record = in.readLine();
                if (record != null) {
                    String[] fields = record.split(Meta.DS);
//                    if (exchange.equals(fields[0])) {
                    String caption = Language.getLanguageSpecificString(fields[2]);
                    caption = UnicodeUtils.getNativeString(caption);
                    Sector sector = new Sector(fields[1], caption);
                    if (!sector.getId().equals("UNCLASSIFIED")) {
                        sectors.add(sector);
                    }
                    sector = null;
                    caption = null;
//                    }
                    fields = null;
                }
            } while (record != null);

            out = null;
            decompress = null;
            in.close();
            in = null;

            Collections.sort(sectors);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Sector getSector(String exchange, String id) {
        LinkedList<Sector> sectors = exchanges.get(exchange);
        if (sectors != null) {
            for (Sector sector : sectors) {
                if (sector.getId().equals(id))
                    return sector;
                /*else{
                    id = Constants.DEFAULT_SECTOR;
                    if(sector.getId().equals(id))
                    return sector;
                }*/
            }
        }
        return null;
    }

    public Enumeration<Sector> getSectors(String exchange) {
        return new Sectors(exchange);
    }

    private class Sectors implements Enumeration<Sector> {

        private LinkedList<Sector> sectors;
        private int index;

        public Sectors(String exchange) {
            sectors = exchanges.get(exchange);
            index = 0;
        }

        public boolean hasMoreElements() {
//            return sectors.size() > index;
            if (sectors != null) {
                if (sectors.size() > index) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        public Sector nextElement() {
            Sector sector = null;
            try {
                sector = sectors.get(index);
                if (sector == null)
                    index = Integer.MAX_VALUE;
                else
                    index++;
            } catch (Exception e) {
                e.printStackTrace();
                index = Integer.MAX_VALUE;
            }
            return sector;
        }
    }
}
