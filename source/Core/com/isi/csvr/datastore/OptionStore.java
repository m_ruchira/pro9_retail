/**
 * This is the container class for the stock object hashtable
 * Contains methods save and load the hash object from the disk.
 */

// Copyright (c) 2001 Integrated Systems International (ISI)
package com.isi.csvr.datastore;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 * modified by Bandula Priyadarshana
 */

import com.isi.csvr.RequestManager;
import com.isi.csvr.options.*;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class OptionStore {
    //extends Thread {

    public static final byte CHAIN_TYPE_OPTION = 0;
    public static final byte CHAIN_TYPE_FUTURES = 1;
    public static final int REQUEST_FROM_OPTION_WINDOW = 0;
    public static final int REQUEST_FROM_OPTION_STRATEGY = 1;
    private static OptionStore self = null;
    private static SmartProperties g_oOptionBuilder;
    private final int DATA_WAIT_TIME = 10;//Seconds
    private final int DATA_WAIT_SLEEP_TIME = 100;// Millies
    private Hashtable optionRegister;
    private Hashtable<String, OptionStrategyStore> optionStrategyRegister;
    private String selectedMonth = null;
    private String key = null;
    private byte selectedType = -1;
    private byte chainType = 0;
    private OptionStrategyListener listener;

    /**
     * Constructor
     */
    private OptionStore() {
        optionRegister = new Hashtable();
        optionStrategyRegister = new Hashtable<String, OptionStrategyStore>();
        loadProperties();
    }

    public synchronized static OptionStore getSharedInstance() {
        if (self == null) {
            self = new OptionStore();
        }
        return self;
    }

    public static void addOptionListener(OptionListener optionListener) {
    }

    public static void removeOptionListener(OptionListener optionListener) {
    }

    public static void reset() {
    }

    /**
     * Converts the given string to a long value
     */
    private static long toLong(String sValue) {
        long longValue = 0;
        try {
            longValue = Long.parseLong(sValue);
        } catch (Exception e) {
            longValue = 0;
        }
        return longValue;
    }

    /**
     * Converts the given string to an int value
     */
    private static int toInt(String sValue) {
        int intValue = 0;
        try {
            intValue = Integer.parseInt(sValue);
        } catch (Exception e) {
            intValue = 0;
        }
        return intValue;
    }

    /**
     * Converts the given string to a double value
     */
    private static double toDouble(String sValue) {
        double floatValue = 0;
        try {
            floatValue = Double.parseDouble(sValue);
        } catch (Exception e) {
            floatValue = 0D;
        }
        return floatValue;
    }

    /**
     * Converts the given string to a byte value
     */
    private static byte toByte(String sValue) throws Exception {
        return Byte.parseByte(sValue);
    }

    private void loadProperties() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/opt.dll");
            g_oOptionBuilder = new SmartProperties("=");
            g_oOptionBuilder.load(oIn);
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();//To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void addOptionsStrategyListener(OptionStrategyListener listener) {
        this.listener = listener;
    }

    public String getOptionBuilderProperty(String property) {
        return g_oOptionBuilder.getProperty(property);
    }

    public void setOptionSymbolList(String symbol, String exchange, int instrumentType, String value) {
        System.out.println(value);
        // a+ Exchange + Meta.FD + b + Symbol + Meta.FD + c + Month List + Meta.FD + C + Selected Month + Meta.FD + d + View Type List
        // + Meta.FD + D + Selected View Type + Meta.FD + e + Option List(Price|P.Symbol|C.Symbol,Price|P.Symbol|C.Symbol,..) for the selected month and view type + Meta.FD +
        // todo - protocol changed -  17/12/2007 - this is the old protocol
        // + Meta.FD + D + Selected View Type + Meta.FD + e + Call List for the selected month and view type + Meta.FD +
        // f + Put List for the selected month and view type + Meta.FD + g + instrument Type + Meta.LF

        String[] data = value.split(Meta.FD);
        char tag;
        String baseExchange = null;
        //        String symbol   = null;
        int baseInstrumentType = -1;
        String[] monthList = null;
        String[] typeList = null;
        String[] symbolList = null;
        String expirationDate = null;
        int requestWindow = REQUEST_FROM_OPTION_WINDOW;
        byte chainType = 0;

        if (data.length < 1) {
            SharedMethods.showConfirmMessage(Language.getString("OPTION_CHAIN_NOT_AVAILABLE"), JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION);
            return;
        }
        for (int i = 0; i < data.length; i++) {
            try {
                tag = data[i].toCharArray()[0];
                switch (tag) {
                    case 'a':
                        exchange = data[i].substring(1);
                        break;
                    case 'b':
                        symbol = data[i].substring(1);
                        break;
                    case 'c':
                        if ((data[i].substring(1)).trim().equals("-1")) {
                            SharedMethods.showConfirmMessage(Language.getString("OPTION_CHAIN_NOT_AVAILABLE"), JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION);
                            return;
                        }
                        monthList = (data[i].substring(1)).split(",");
                        break;
                    case 'C':
                        selectedMonth = data[i].substring(1);
                        break;
                    case 'd':
                        typeList = (data[i].substring(1)).split(",");
                        break;
                    case 'D':
                        selectedType = Byte.parseByte(data[i].substring(1));
                        break;
                    case 'e':
                        symbolList = (data[i].substring(1)).split(",");
                        break;
                    case 'f':
                        instrumentType = Byte.parseByte(data[i].substring(1));
                        break;
                    case 'h':
                        baseExchange = (data[i].substring(1));
                        break;
                    case 'g':
                        chainType = Byte.parseByte(data[i].substring(1));
                        break;
                    case 'j':
                        baseInstrumentType = Integer.parseInt(data[i].substring(1));
                        break;
                    case 'k':
                        expirationDate = (data[i].substring(1));
                        break;
                    case 'm':
                        requestWindow = Integer.parseInt(data[i].substring(1));
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();//To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (requestWindow == REQUEST_FROM_OPTION_STRATEGY) {
            OptionStrategyStore map = getOptionStrategyStore(SharedMethods.getKey(baseExchange, symbol, baseInstrumentType));
            if ((monthList != null) && (!monthList[0].equals("null"))) {
                map.setMonths(monthList);
            }
            if ((typeList != null) && ((!typeList[0].equals("null")))) {
                map.setTypes(typeList);
            }
            map.setOptionExchange(exchange);
            map.setOptionInstrument(instrumentType);
            map.setExpirationDate(selectedMonth, expirationDate);
            map.setSelectedType(selectedType);
            map.setSymbols(selectedMonth, symbolList);
            if (listener != null) {
                listener.dataReceived(SharedMethods.getKey(baseExchange, symbol, baseInstrumentType), selectedMonth);
            }
        } else {
            OpraMap map = getOpraMap(SharedMethods.getKey(baseExchange, symbol, baseInstrumentType), chainType);
            synchronized (this) {
                try {
                    if (map != null) {
                        map.clear();// if already exist from a previouse call
                        map.setBaseSymbol(symbol);
                        map.setExchange(exchange);
                        map.setCurrentMonth(selectedMonth);
                        if (!monthList[0].equals("null")) {
                            map.setMonths(monthList);
                        } else {
                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    SharedMethods.showConfirmMessage(Language.getString("OPTION_CHAIN_NOT_AVAILABLE"), JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION);
                                }
                            });
                            return;
                        }
                        map.setCurrentType(selectedType);
                        map.setTypes(typeList);
                        map.setInstrumentType(instrumentType);
                        map.setExpirationDate(expirationDate);
                        setSymbols(symbolList, map);
                        registerOrpaSymbols(map);
                        prepareSymbolLists(map, baseExchange, baseInstrumentType);
                        optionRegister.put(SharedMethods.getKey(baseExchange, symbol, baseInstrumentType) + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + chainType, map);
                        map.fileOptionDataCompleted();
                        map = null;
                    }
                } catch (Exception e) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            SharedMethods.showConfirmMessage(Language.getString("OPTION_CHAIN_NOT_AVAILABLE"), JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION);
                        }
                    });
                }
            }
        }


    }

    public void resetDefaultValues() {
        key = null;
        selectedMonth = null;
        selectedType = 0;
    }

    public void clearSymbolData(String sSelectedSymbol, byte chainType) {
        Stock stock = DataStore.getSharedInstance().getStockObject(sSelectedSymbol);
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(sSelectedSymbol);
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            String symbol = SharedMethods.getSymbolFromKey(sSelectedSymbol);
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                sSelectedSymbol = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sSelectedSymbol), symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter())), instrumentType);
            }
        }
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
            DataStore.getSharedInstance().removeSymbolRequest(sSelectedSymbol);
        }
        removeOpraQuoteObject(sSelectedSymbol, chainType);
        removeMap(sSelectedSymbol, chainType);
        resetDefaultValues();
    }

    /* public void clear() {
        try {
            Enumeration enu = optionRegister.keys();

            while (enu.hasMoreElements()){
                OpraMap map = (OpraMap)optionRegister.get(enu.nextElement());
                map.clear();
            }
        } catch (Exception e) {

        }
    }*/

    private String getNextMonth(String[] months) {
        SimpleDateFormat serverFormat = new SimpleDateFormat("MMM yyyy");
        //SimpleDateFormat opraFormat = new SimpleDateFormat("yyyy MM");
        Date[] dates = new Date[months.length];

        for (int i = 0; i < months.length; i++) {
            try {
                Date dt = serverFormat.parse(months[i]);
                dates[i] = dt;
                dt = null;
            } catch (ParseException e) {
                //e.printStackTrace();  //To change body of catch statement use Options | File Templates.
            }
        }
        Arrays.sort(dates);

        return serverFormat.format(dates[0]);
    }

    //    public void sendOptionChainRequest(byte chainType){
    ////        SendQFactory.createOpraRequest(key, selectedMonth, selectedType);
    //        removeOpraQuoteObject(key, chainType);
    //        addOpraQuoteObject(key, selectedMonth, selectedType, chainType);
    //    }

    private void setSymbols(String[] data, OpraMap map) {
        //        DynamicArray opraObjects = new DynamicArray();
        try {
            String record = null;
            String[] price_symbol = null;
            for (int i = 0; i < data.length; i++) {
                try {
                    record = data[i];
                    if (!record.trim().equals("")) {
                        price_symbol = record.split("\\|", -3);
                        OpraObject opraObject = new OpraObject();
                        opraObject.setInstrumentType(map.getInstrumentType());
                        opraObject.setStrikePrice(toDouble(price_symbol[0]));
                        try {
                            opraObject.setPutSymbol(price_symbol[1]);
                        } catch (Exception e) {
                        }
                        try {
                            opraObject.setCallSymbol(price_symbol[2]);
                        } catch (Exception e) {
                        }
                        map.getSymbols().add(opraObject);

                    }
                } catch (Exception e) {
                    e.printStackTrace();//To change body of catch statement use File | Settings | File Templates.
                }
            }
            map.setSymbols(map.getSymbols());
            Collections.sort(map.getSymbols(), new OptionSymbolComparator());
        } catch (Exception e) {
            e.printStackTrace();//To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void registerOrpaSymbols(OpraMap map) {
        ArrayList allSymbols = map.getSymbols();
        for (int i = 0; i < allSymbols.size(); i++) {
            OpraObject opraObject = (OpraObject) allSymbols.get(i);
            if ((opraObject.getCallSymbol() != null) && (!opraObject.getCallSymbol().equals(""))) {
                DataStore.getSharedInstance().addSymbolRequest(map.getExchange(), opraObject.getCallSymbol(), map.getInstrumentType());
            }
            if ((opraObject.getPutSymbol() != null) && (!opraObject.getPutSymbol().equals(""))) {
                DataStore.getSharedInstance().addSymbolRequest(map.getExchange(), opraObject.getPutSymbol(), map.getInstrumentType());
            }
            //            DataStore.setSymbol(false,map.getExchange() + Constants.KEY_SEPERATOR_CHARACTER + opraObject.getCallSymbol(),
            //                    "","" + map.getInstrumentType(),true);
            //            DataStore.setSymbol(false,map.getExchange() + Constants.KEY_SEPERATOR_CHARACTER + opraObject.getPutSymbol(),
            //                    "","" + map.getInstrumentType(),true);
            opraObject = null;
        }
    }

    public void registerOutOfMoneySymbols(OpraMap map, boolean all) {
    }

    public void removeSymbol(String sKey) {
    }

    public void addOpraQuoteObject(String sKey, String month, byte viewType, byte chainType) {
        if ((sKey == null) || (sKey.equals(""))) {
            sKey = this.key;
        }
        this.key = sKey;
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(sKey);
        Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            String symbol = SharedMethods.getSymbolFromKey(sKey);
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                sKey = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sKey), symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter())), instrumentType);
            }
        }
        this.chainType = chainType;
        if (month != null) {
            selectedMonth = month;
            selectedType = viewType;
        }
        //        SendQFactory.createOpraRequest(sKey, month, viewType,chainType);
        addOpraRequest(sKey, month, viewType, chainType);
        //        }
    }

    public synchronized void addOpraRequest(String sKey, String month, byte viewType, int chainType) {
        if (sKey == null) {
            return;
        }
        String key = sKey + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + Meta.OPTION_REQUEST + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + chainType;
        String reqString = null;
        String exCode = SharedMethods.getExchangeFromKey(sKey);
        if (ExchangeStore.isExpired(exCode) || ExchangeStore.isInactive(exCode)) {
            return;
        }
        String symbol = SharedMethods.getSymbolFromKey(sKey);
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(sKey);
        reqString = Meta.OPTION_REQUEST + Meta.DS + exCode + Meta.FD + symbol + Meta.FD + instrumentType + Meta.FD + chainType;
        if (month != null) {
            reqString = reqString + Meta.FD + viewType + Meta.FD + month + Meta.FD + REQUEST_FROM_OPTION_WINDOW;
        } else {
            reqString = reqString + Meta.FD + "" + Meta.FD + "" + Meta.FD + REQUEST_FROM_OPTION_WINDOW;
        }
        RequestManager.getSharedInstance().addAddRequest(ExchangeStore.getSharedInstance().getDataPath(exCode), key, reqString);
    }

    public synchronized void removeOpraRequest(String sKey, int chainType) {
        if (sKey == null) {
            return;
        }
        String key = sKey + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + Meta.OPTION_REQUEST + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + chainType;
        RequestManager.getSharedInstance().removeAddRequest(key);
    }

    public void addOpraQuoteObject(String sKey, String month, byte viewType, byte chainType, Object frame) {
        if ((sKey == null) || (sKey.equals(""))) {
            sKey = this.key;
        }
        this.key = sKey;
        Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            String symbol = SharedMethods.getSymbolFromKey(sKey);
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                sKey = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sKey), symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter())), SharedMethods.getInstrumentTypeFromKey(sKey));
            }
        }
        this.chainType = chainType;
        if (month != null) {
            selectedMonth = month;
            selectedType = viewType;
        }
        //        if (Settings.isConnected()) {
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sKey))) {
            DataStore.getSharedInstance().addSymbolRequest(SharedMethods.getExchangeFromKey(sKey), SharedMethods.getSymbolFromKey(sKey), SharedMethods.getInstrumentTypeFromKey(sKey), true, frame, Meta.SYMBOL_TYPE_EQUITY, false, null);
        }
        //        SendQFactory.createOpraRequest(sKey, month, viewType,chainType);
        addOpraRequest(sKey, month, viewType, chainType);
        //        }
    }

    public void removeOpraQuoteObject(String sKey, byte chainType) {
        synchronized (this) {
            try {
                removeOpraRequest(sKey, chainType);
                Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
                if ((stock != null) && (stock.getMarketCenter() != null)) {
                    String symbol = SharedMethods.getSymbolFromKey(sKey);
                    if (symbol.endsWith("." + stock.getMarketCenter())) {
                        sKey = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sKey), symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter())), SharedMethods.getInstrumentTypeFromKey(sKey));
                    }
                }
                OpraMap map = getOpraMap(sKey, chainType);
                Object[] list = map.getSymbols().toArray();
                for (int i = 0; i < list.length; i++) {
                    OpraObject opraObject = (OpraObject) list[i];
                    //                    SendQFactory.addRemoveRequest(map.getExchange(),opraObject.getPutSymbol(), map.getInstrumentType(),map.getInstrumentType());
                    //                    SendQFactory.addRemoveRequest(map.getExchange(),opraObject.getCallSymbol(), map.getInstrumentType(), map.getInstrumentType());
                    //                    DataStore.addRemoveSymbol("OPRA" + Constants.KEY_SEPERATOR_CHARACTER+ opraObject.getPutSymbol(),true);
                    DataStore.getSharedInstance().removeSymbolRequest(map.getExchange(), opraObject.getCallSymbol(), map.getInstrumentType());
                    DataStore.getSharedInstance().removeSymbolRequest(map.getExchange(), opraObject.getPutSymbol(), map.getInstrumentType());
                }
                list = null;
            } catch (Exception e) {
                e.printStackTrace();//To change body of catch statement use Options | File Templates.
            }
        }
    }

    public void removeMap(String key, byte chainType) {
        try {
            getOpraMap(key, chainType).clear();
            optionRegister.remove(key + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + chainType);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Returns the hashtable object
     */
    /*public Hashtable getHashtable() {
        return optionRegister;
    }*/

    /*public OpraMap createOpraMap(String key, boolean removeBaseOnExit, String currentMonth, byte chainType){
        synchronized(this){
            OpraMap map = (OpraMap)optionRegister.get(key + Constants.INSTRUMENT_TYPE_SEPERATOR_CHARACTER + chainType);
            if (map == null){
                map = new OpraMap(removeBaseOnExit);
                map.setCurrentMonth(currentMonth);
                optionRegister.put(key + Constants.INSTRUMENT_TYPE_SEPERATOR_CHARACTER + chainType, map);
            }
            return map;
        }
    }*/
    public OptionStrategyStore getOptionStrategyStore(String key) {
        OptionStrategyStore store = null;
        try {
            store = optionStrategyRegister.get(key);
            if (store == null) {
                store = new OptionStrategyStore(key);
                optionStrategyRegister.put(key, store);
            }
        } catch (Exception e) {
            if (store == null) {
                store = new OptionStrategyStore(key);
                optionStrategyRegister.put(key, store);
            }
        }
        return store;
    }

    public OpraMap getOpraMap(String key, byte chainType) {
        Stock stock = DataStore.getSharedInstance().getStockObject(key);
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            String symbol = SharedMethods.getSymbolFromKey(key);
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                key = SharedMethods.getKey(SharedMethods.getExchangeFromKey(key), symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter())), instrumentType);
            }
        }
        synchronized (this) {
            OpraMap map = (OpraMap) optionRegister.get(key + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + chainType);
            if (map == null) {
                map = new OpraMap();
                optionRegister.put(key + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + chainType, map);
            }
            return map;
        }
    }

    /**
     * Returns the list of keys as an enumeration
     */
    public Enumeration getSymbols() {
        return optionRegister.keys();
    }

    /**
     * Returns the symbol count
     */
    public int getSymbolCount() {
        return optionRegister.size();
    }

    public OpraMap createOpraMap(String key, boolean removeBaseOnExit, String currentMonth) {
        synchronized (this) {
            OpraMap map = (OpraMap) optionRegister.get(key);
            if (map == null) {
                map = new OpraMap(removeBaseOnExit);
                map.setCurrentMonth(currentMonth);
                optionRegister.put(key, map);
            }
            return map;
        }
    }

    /////////////////////////////////////////////////////////
    private void prepareSymbolLists(OpraMap map, String baseExchange, int intrumentType) {
        int nearTheMoneyValueLocation = 0;
        ArrayList nearTheMoneySymbols = new ArrayList();
        ArrayList outOfTheMoneySymbols = new ArrayList();
        double prevClosed = 0;
        try {
            int waitCounter = (DATA_WAIT_TIME * 1000 / DATA_WAIT_SLEEP_TIME);// (10 Sec / 100 millis to get total iterations)
            Stock stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(baseExchange, map.getBaseSymbol(), intrumentType));
            // wait till prev closed value is recieved
            while ((waitCounter > 0) && ((prevClosed = stock.getPreviousClosed()) <= 0)) {
                Thread.sleep(DATA_WAIT_SLEEP_TIME);
                waitCounter--;
            }
        } catch (Exception e) {
            e.printStackTrace();
            prevClosed = 0;
        }

        ArrayList allSymbols = map.getAllSymbols();
        OpraObject tempObject = new OpraObject(prevClosed);
        int location = Collections.binarySearch(allSymbols, tempObject, new OptionSymbolComparator());// get the location where value fits.
        if (location < 0) {
            location = -(location + 1);
        }

        // get the closest value to the prev-close from the selection location (location and location+1) //
        double value1 = 0;
        try {
            value1 = ((OpraObject) allSymbols.get(location)).getStrikePrice();
        } catch (Exception e) {
            value1 = 0;
        }
        double value2;
        try {
            value2 = ((OpraObject) allSymbols.get(location + 1)).getStrikePrice();
        } catch (Exception e) {
            value2 = 0;
        }
        if ((Math.abs(value1 - prevClosed)) < (Math.abs(value2 - prevClosed))) {
            nearTheMoneyValueLocation = location;
        } else {
            nearTheMoneyValueLocation = location + 1;
        }
        // -- //

        for (int i = 0; i < allSymbols.size(); i++) {
            //if (int i = nearTheMoneyValueLocation-ESPConstants.NEAR_THE_MONEY_LIMIT; i <= nearTheMoneyValueLocation+ESPConstants.NEAR_THE_MONEY_LIMIT; i++) {
            if ((i < nearTheMoneyValueLocation + Constants.NEAR_THE_MONEY_LIMIT) && (i >= nearTheMoneyValueLocation - Constants.NEAR_THE_MONEY_LIMIT)) {
                try {
                    OpraObject opraObject = (OpraObject) allSymbols.get(i);
                    opraObject.setNearTheMoney(true);
                    nearTheMoneySymbols.add(opraObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    OpraObject opraObject = (OpraObject) allSymbols.get(i);
                    opraObject.setNearTheMoney(false);
                    outOfTheMoneySymbols.add(opraObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        nearTheMoneySymbols.trimToSize();
        outOfTheMoneySymbols.trimToSize();
        map.setNearTheMoneySymbols(nearTheMoneySymbols);
        map.setOutOfTheMoneySymbols(outOfTheMoneySymbols);
        //return nearTheMoneySymbols;
    }

    public void removeOutOfMoneySymbols(String sKey, byte chanType) {
        synchronized (this) {
            try {
                Object[] list = getOpraMap(sKey, chanType).getOutOfTheMoneySymbols().toArray();
                for (int i = 0; i < list.length; i++) {
                    OpraObject opraObject = (OpraObject) list[i];
                    DataStore.getSharedInstance().removeStock("OPRA", opraObject.getPutSymbol(), opraObject.getInstrumentType());
                    DataStore.getSharedInstance().removeStock("OPRA", opraObject.getCallSymbol(), opraObject.getInstrumentType());
                }
                list = null;
            } catch (Exception e) {
                e.printStackTrace();//To change body of catch statement use Options | File Templates.
            }
        }
    }


}