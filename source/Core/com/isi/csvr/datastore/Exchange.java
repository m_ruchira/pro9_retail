package com.isi.csvr.datastore;

import com.isi.csvr.MarketTimer;
import com.isi.csvr.brokers.Broker;
import com.isi.csvr.downloader.HistoryDownloadManager;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import static com.isi.csvr.shared.Constants.PREOPEN;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 3, 2005
 * Time: 1:18:28 PM
 */
public class Exchange implements Serializable, ExchangeInterface, Comparable {
    public static boolean bandwidthOpEnabled;
    public long volume = 0;
    public double turnover = 0;
    public int noOfTrades = 0;
    public int symbolsTraded = 0;
    public int noOfUps = 0;
    public int noOfDown = 0;
    public int noOfNoChange = 0;
    public byte priceDecimalPlaces = 2;
    public int priceModificationFactor = 1;
    public transient String lastIntradayHistorydate = "99999999";
    public boolean isMarketFileAvailable;
    public int masterFileStaus;
    public int masterFileType;
    private String symbol;
    private String code;
    private String description;
    private String shortDescription;
    private String country;
    private int satelliteDataPort;
    private int satelliteFilePort;
    private byte path;
    private boolean def;
    private boolean userSubMarketBreakdown;
    private boolean timensalesEnabled;
    private transient boolean flaged;
    private transient int currentEODDate; // last received eod date fort this exchange
    private transient String timeZoneID;
    private TimeZone activeTimeZone;
    private transient String activeCurrency;
    private int[] infoTypes;
    //    private int[] tradingInfoTypes;
    private Hashtable<Byte, int[]> tradingWindowTypes;
    private Hashtable<String, Hashtable> exchangeIndices;
    private Hashtable<String, String> watchListVisibility;
    private Market defaultMarket = null;
    //private float    masterDataVersion;
    private String displayExchange = "";
    private int marketStatus = -1;
    private int lastEODDate = 19700101;
    private long marketDateInt = 0;
    private long marketDate;
    private long prevMarketDate;
    private long marketTime;
    private long marketDateTime;
    private Market[] subMarkets;
    private Broker[] brokers;
    private int expansionStatus = Constants.EXPANSION_STATUS_NO_CHILDREN;
    // Variation map settings
    private double bidAskMax = 0;
    private double bidAskMin = Double.POSITIVE_INFINITY;
    private double moneyFlowMax = 0;
    private double moneyFlowMin = Double.POSITIVE_INFINITY;
    private double spreadMax = Double.NEGATIVE_INFINITY;
    private double spreadMin = Double.POSITIVE_INFINITY;
    private double rangeMax = Double.NEGATIVE_INFINITY;
    private double changeMax = Double.NEGATIVE_INFINITY;
    private double changeMin = Double.POSITIVE_INFINITY;
    private String contentCMIP = null;
    private String expiryDate = "";
    private boolean exchangeExpired = false;
    private boolean exchangeIsInactive = false;
    private boolean exchangeIsDelayed = false;
    // ------ Shanika --------
    private boolean isExchageMode = true;
    private short isEnabledForIndexPanel = -1;
    private String mainindex;

    private transient Hashtable<String, ArrayList<String>> cashFlowExchangeIndices = null;

    public Exchange(String code, String symbol, String country, boolean def, byte path
            , int satelliteDataPort, int satelliteFilePort, boolean isDelayed) {
        this(code, symbol, country, def, path, satelliteDataPort, satelliteFilePort, null, isDelayed);
    }

    public Exchange(String code, String symbol, String country, boolean def, byte path
            , int satelliteDataPort, int satelliteFilePort, String contentCMIP, boolean isDelayed) {

        this.code = code;
        this.def = def;
//        if (symbol.toUpperCase().endsWith("_D")) {
//            this.symbol = symbol.split("_")[0];
        if (isDelayed) {

            setExchangeDelayed(true);
        }
//        }else {
        this.symbol = symbol;
//        }
        this.description = this.symbol;
        if ((contentCMIP != null) && (!contentCMIP.equals("")) && (!contentCMIP.equalsIgnoreCase("NULL"))) {
            this.contentCMIP = contentCMIP;
        }
        this.path = path;
        this.country = country;
        this.activeTimeZone = null;
        this.satelliteDataPort = satelliteDataPort;
        this.satelliteFilePort = satelliteFilePort;
        subMarkets = new Market[0];
        brokers = new Broker[0];
        setFlag(); // new exchange. flag is up
        tradingWindowTypes = new Hashtable<Byte, int[]>();
        exchangeIndices = new Hashtable<String, Hashtable>();
        watchListVisibility = new Hashtable<String, String>();
        cashFlowExchangeIndices = new Hashtable<String, ArrayList<String>>();
    }

    public static boolean isBandwidthOpEnabled() {
        return bandwidthOpEnabled;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isDefault() {
        return this.def;
    }

    public void setDefault(boolean def) {
        this.def = def;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        if (shortDescription == null) {
            shortDescription = description;
        }
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public byte getPath() {
        return path;
    }

    public void setPath(byte path) {
        this.path = path;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getExchangeByDisplayExchange(String displayExchange) {
        return symbol;
    }

    public String getDisplayExchange() {
        return displayExchange;
    }

    public void setDisplayExchange(String displayExchange) {
        this.displayExchange = displayExchange;
    }

    public boolean isFlaged() {
        return flaged;
    }

    public void setFlag() {
        this.flaged = true;
    }

    public void clearFlag() {
        this.flaged = false;
    }

    public int getCurrentEODDate() {
        return currentEODDate;
    }

    public void setCurrentEODDate(int currentEODDate) {
        this.currentEODDate = currentEODDate;
    }

    public int getSatelliteDataPort() {
        return satelliteDataPort;
    }

    public void setSatelliteDataPort(int satelliteDataPort) {
        this.satelliteDataPort = satelliteDataPort;
    }

    public int getSatelliteFilePort() {
        return satelliteFilePort;
    }

    public void setSatelliteFilePort(int satelliteFilePort) {
        this.satelliteFilePort = satelliteFilePort;
    }

    public boolean isMarketFileAvailable() {
        return isMarketFileAvailable;
    }

    public void setMarketFileAvailable(boolean marketFileAvailable) {
        isMarketFileAvailable = marketFileAvailable;
    }

    public int getMasterFileStaus() {
        return masterFileStaus;
    }

    public void setMasterFileStaus(int masterFileStaus) {
        this.masterFileStaus = masterFileStaus;
    }

    public int getMasterFileType() {
        return masterFileType;
    }

    public void setMasterFileType(int masterFileType) {
        this.masterFileType = masterFileType;
    }

    public void setData(String[] data, int decimalFactor) throws Exception {
        if (data == null)
            return;
        char tag;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == null)
                continue;
            if (data[i].length() <= 1)
                continue;
            tag = data[i].charAt(0);
            switch (tag) {
                case 'A':
                    marketStatusChanged(Integer.parseInt(data[i].substring(1)));
//                    marketStatus = Integer.parseInt(data[i].substring(1));
                    break;
                case 'B':
                    setCurrentEODDate(Integer.parseInt(data[i].substring(1)));
//                    currentEODDate = Integer.parseInt(data[i].substring(1));
                    break;
                case 'C':
                    marketDate = SharedMethods.getDateLongFromDateString(data[i].substring(1));
                    try {
                        this.marketDateInt = Integer.parseInt(SharedMethods.toDateString(marketDate));
                    } catch (NumberFormatException e) {
                        marketDateInt = 0;
                    }
                    marketDateTime = marketDate + marketTime;
                    MarketTimer.getSharedInstance().setMarketTime(getSymbol(), marketDateTime);
                    checkMarketInit(marketDate);
                    checkMarketInitKSE();
                    break;
                case 'D':
                    marketTime = SharedMethods.getTimeLongFromTimeString(data[i].substring(1));
                    marketDateTime = marketDate + marketTime;
                    MarketTimer.getSharedInstance().setMarketTime(getSymbol(), marketDateTime);
                    break;
                case 'E':
                    volume = SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'F':
                    turnover = SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case 'G':
                    noOfTrades = Integer.parseInt(data[i].substring(1));
                    break;
                case 'H':
                    symbolsTraded = Integer.parseInt(data[i].substring(1));
                    break;
                case 'I':
                    noOfUps = Integer.parseInt(data[i].substring(1));
                    break;
                case 'J':
                    noOfDown = Integer.parseInt(data[i].substring(1));
                    break;
                case 'K':
                    noOfNoChange = Integer.parseInt(data[i].substring(1));
                    break;
            }
        }
    }

    private void initializeExchange() {
        volume = 0;
        turnover = 0;
        noOfTrades = 0;
        symbolsTraded = 0;
        noOfUps = 0;
        noOfDown = 0;
        noOfNoChange = 0;
    }

    private void checkMarketInitKSE() {
        if (marketStatus == Constants.PREOPEN && this.getSymbol().equals("KSE")) {
            ExchangeStore.getSharedInstance().fireExchangeMustInitializeKSE(this);
            initializeExchange();
            prevMarketDate = marketDate;
        }
    }

    private void checkMarketInit(long marketDate) {
        if (marketDate != prevMarketDate &  marketStatus == Constants.PREOPEN ) {
            ExchangeStore.getSharedInstance().fireExchangeMustInitialize(this, marketDate, prevMarketDate);

            initializeExchange();
            prevMarketDate = marketDate;
        }
    }

    private void marketStatusChanged(int status) {
        int oldStatus = marketStatus;
        marketStatus = status;
        ExchangeStore.getSharedInstance().fireMarketStatusChanged(oldStatus, this);
        if (marketStatus == Constants.CLOSED) {
            HistoryDownloadManager.getSharedInstance().requestHistory(this);
        }
    }

    public int getLastEODDate() {
        return lastEODDate;
    }

    public void setLastEODDate(int lastEODDate) {
        this.lastEODDate = lastEODDate;
    }

    public long getMarketDate() {
        return marketDate;
    }

    public int getMarketStatus() {
        return marketStatus;
    }

    public long getMarketTime() {
        return marketTime;
    }

    public int getNoOfDown() {
        return noOfDown;
    }

    public int getNoOfNoChange() {
        return noOfNoChange;
    }

    public int getNoOfTrades() {
        return noOfTrades;
    }

    public int getNoOfUps() {
        return noOfUps;
    }

    public int getSymbolsTraded() {
        return symbolsTraded;
    }

    public double getTurnover() {
        return turnover;
    }

    public long getVolume() {
        return volume;
    }

    public long getMarketDateTime() {
        return marketDateTime;
    }

    public boolean isTimensalesEnabled() {
        return timensalesEnabled;
    }

    public void setTimensalesEnabled(boolean timensalesEnabled) {
        this.timensalesEnabled = timensalesEnabled;
    }

    public double getBidAskMax() {
        return bidAskMax;
    }

    public void setBidAskMax(double bidAskMax) {
        this.bidAskMax = bidAskMax;
    }

    public double getBidAskMin() {
        return bidAskMin;
    }

    public void setBidAskMin(double bidAskMin) {
        this.bidAskMin = bidAskMin;
    }

    public void initBidAskRange() {
        bidAskMax = 0;
        bidAskMin = Double.POSITIVE_INFINITY;
    }

    public double getMoneyFlowMax() {
        return moneyFlowMax;
    }

    public void setMoneyFlowMax(double moneyFlowMax) {
        this.moneyFlowMax = moneyFlowMax;
    }

    public double getMoneyFlowMin() {
        return moneyFlowMin;
    }

    public void setMoneyFlowMin(double moneyFlowMin) {
        this.moneyFlowMin = moneyFlowMin;
    }

    public void initMoneyFlowRange() {
        moneyFlowMax = 0;
        moneyFlowMin = Double.POSITIVE_INFINITY;
    }

    public double getSpreadMax() {
        return spreadMax;
    }

    public void setSpreadMax(double spreadMax) {
        this.spreadMax = spreadMax;
    }

    public void initSpreadRange() {
        spreadMax = Double.NEGATIVE_INFINITY;
        spreadMin = Double.POSITIVE_INFINITY;
    }

    public double getRangeMax() {
        return rangeMax;
    }

    public void setRangeMax(double rangeMax) {
        this.rangeMax = rangeMax;
    }

    public double getSpreadMin() {
        return spreadMin;
    }

    public void setSpreadMin(double spreadMin) {
        this.spreadMin = spreadMin;
    }

    public void initRangeRange() {
        rangeMax = Double.NEGATIVE_INFINITY;
    }

    public double getChangeMax() {
        return changeMax;
    }

    public void setChangeMax(double changeMax) {
        this.changeMax = changeMax;
    }

    public double getChangeMin() {
        return changeMin;
    }

    public void setChangeMin(double changeMin) {
        this.changeMin = changeMin;
    }

    public void initChangeRange() {
        changeMax = Double.NEGATIVE_INFINITY;
        changeMin = Double.POSITIVE_INFINITY;
    }

    public String toString() {
        return symbol;
    }

    public String getActiveCurrency() {
        return activeCurrency;
    }

    public void setActiveCurrency(String activeCurrency) {
        this.activeCurrency = activeCurrency;
    }

    public boolean isValidIinformationType(int type) {
        try {
            return (Arrays.binarySearch(getInfoTypes(), type) >= 0);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isValidTradingIinformationType(int type) {
        try {
            if (Arrays.binarySearch(getTradingInfoTypes(Constants.PATH_PRIMARY), type) >= 0) {
                return true;
            } else {
                return (Arrays.binarySearch(getTradingInfoTypes(Constants.PATH_SECONDARY), type) >= 0);
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isValidTradingIinformationType(int type, byte path) {
        try {
            return (Arrays.binarySearch(getTradingInfoTypes(path), type) >= 0);
        } catch (Exception e) {
            return false;
        }
    }

    public TimeZone getActiveTimeZone() {
        return activeTimeZone;
    }

    /*public TimeZone getPrevTimeZone() {
        return prevTimeZone;
    }*/

    protected void setActiveTimeZone(TimeZone activeTimeZone) {
        this.activeTimeZone = activeTimeZone;
    }

    public void setActiveTimeZone(String newTimeZone) {
        try {
            if (newTimeZone == null) {
                this.activeTimeZone = TimeZone.getTimeZone(timeZoneID);
                this.activeTimeZone.useDaylightTime();
            } else {
                this.activeTimeZone = TimeZone.getTimeZone(newTimeZone);
                this.activeTimeZone.useDaylightTime();
            }
        } catch (Exception e) {
            this.activeTimeZone = null;
        }

    }

    public String getTimeZoneID() {
        return timeZoneID;
    }

    public void setTimeZoneID(String timeZoneID) {
        this.timeZoneID = timeZoneID;
    }

    public long getZoneAdjustedTime(long date) {
        try {
            return date + activeTimeZone.getOffset(date);
        } catch (Exception e) {
            return date;
        }
    }

    public long getZoneAdjustment(long date) {
        try {
            return activeTimeZone.getOffset(date);
        } catch (Exception e) {
            return 0;
        }
    }

    public long getTodaysZoneAdjustment() {
        return activeTimeZone.getOffset(getMarketDateTime());
    }

    public int[] getInfoTypes() {
        return infoTypes;
    }

    /*public int[] getTradingInfoTypes() {
        return tradingInfoTypes;
    }*/

    public void setInfoTypes(int[] infoTypes) {
        this.infoTypes = infoTypes;
        timensalesEnabled = ((isValidIinformationType(Meta.IT_SymbolTimeAndSales)) || (isValidIinformationType(Meta.IT_MarketTimeAndSales)));
    }

    public int[] getTradingInfoTypes(byte path) {
        try {
            return tradingWindowTypes.get(new Byte("" + path));
        } catch (Exception e) {
            return null;
        }
    }

    public void setInfoBandWidth() {
        bandwidthOpEnabled = isValidIinformationType(Meta.IT_Bandwidth_Optimizer);
    }

    public void setTradingInfoTypes(int[] infoTypes, byte path) {
        this.tradingWindowTypes.put(path, infoTypes);
    }

    // bandu - New method for correct Market Date Time
    public long getZoneAdjustedMarketTime() {
        return getZoneAdjustedTime(marketDateTime);
    }

    public String getLastIntradayHistorydate() {
        return lastIntradayHistorydate;
    }

    public synchronized void setLastIntradayHistorydate(String lastIntradayHistorydate) {
        if ((this.lastIntradayHistorydate == null) || (this.lastIntradayHistorydate.compareTo(lastIntradayHistorydate) > 0))
            this.lastIntradayHistorydate = lastIntradayHistorydate;
    }

    public Market[] getSubMarkets() {
        return subMarkets;
    }

    public void setSubMarkets(Market[] subMarkets) {
        this.subMarkets = subMarkets;
    }


    public Broker[] getBrokers() {
        return brokers;
    }

    public void setBrokers(Broker[] brokers) {
        this.brokers = brokers;
    }

    public int getExpansionStatus() {
        return expansionStatus;
    }

    public void setExpansionStatus(int expansionStatus) {
        this.expansionStatus = expansionStatus;
    }

    public int getSubMarketCount() {
        return subMarkets.length;
    }

    public boolean hasSubMarkets() {
        return subMarkets.length > 0;
    }

    public int getBrokerCount() {
        return brokers.length;
    }

    public boolean hasBrokers() {
        return brokers.length > 0;
    }

    public boolean hasEodRun() {
        return currentEODDate == marketDateInt;
    }

    public boolean isUserSubMarketBreakdown() {
        return userSubMarketBreakdown;
    }

    public void setUserSubMarketBreakdown(boolean userSubMarketBreakdown) {
        this.userSubMarketBreakdown = userSubMarketBreakdown;
    }

    public byte getPriceDecimalPlaces() {
        return priceDecimalPlaces;
    }

    public void setPriceDecimalPlaces(byte decimal) {
        priceDecimalPlaces = decimal;
    }

    public float getPriceModificationFactor() {
        return priceModificationFactor;
    }

    public void setPriceModificationFactor(int priceModificationFactor) {
        this.priceModificationFactor = priceModificationFactor;
    }

    public int compareTo(Object o) {
        return getDescription().compareTo(((Exchange) o).getDescription());
    }

    public Market getDefaultMarket() {
        return defaultMarket;
    }

    public void setDefaultMarket(Market defaultMarket) {
        this.defaultMarket = defaultMarket;
    }

    public String getContentCMIP() {
        return contentCMIP;
    }

    public void setContentCMIP(String contentCMIP) {
        this.contentCMIP = contentCMIP;
    }

    public boolean isDelayed() {
        return exchangeIsDelayed;
    }

    public void setExchangeDelayed(boolean exchangeDelayed) {
        this.exchangeIsDelayed = exchangeDelayed;
    }

    public boolean isExpired() {
        return exchangeExpired;
    }

    public void setExchangeExpired(boolean exchangeExpired) {
        this.exchangeExpired = exchangeExpired;
    }

    public boolean isInactive() {
        return exchangeIsInactive;
    }

    public void setExchangeInactive(boolean exchangeInactive) {
        this.exchangeIsInactive = exchangeInactive;
    }

    public String getExpiaryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isExchageMode() {
        return isExchageMode;
    }

    public void setExchageMode(boolean exchageMode) {
        isExchageMode = exchageMode;
    }

    public short getEnabledForIndexPanel() {
        return isEnabledForIndexPanel;
    }

    public void setEnabledForIndexPanel(short enabledForIndexPanel) {
        isEnabledForIndexPanel = enabledForIndexPanel;
    }

    public double getCashMapPrecentage() {
        try {
            double cashin = 0;
            double total = 0;
            if (isDefault()) {
                Enumeration equity = DataStore.getSharedInstance().getFilteredList(symbol, Meta.INSTRUMENT_EQUITY);
                while (equity.hasMoreElements()) {
                    String Key = (String) equity.nextElement();
                    Stock stk = DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(symbol, Key, Meta.INSTRUMENT_EQUITY));
                    if (stk != null) {
                        cashin = cashin + stk.getCashInTurnover();
                        total = total + stk.getCashInTurnover() + stk.getCashOutTurnover();
                    }
                }

                if (total > 0 && cashin > 0) {
                    return (cashin / total);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return Double.NaN;
    }

    public double getNetCashMapPercentage() {
        try {
            double netcashin = 0;
            double cashin = 0;
            double cashout = 0;
            double total = 0;
            if (isDefault()) {
                Enumeration equity = DataStore.getSharedInstance().getFilteredList(symbol, Meta.INSTRUMENT_EQUITY);
                while (equity.hasMoreElements()) {
                    String Key = (String) equity.nextElement();
                    Stock stk = DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(symbol, Key, Meta.INSTRUMENT_EQUITY));
                    if (stk != null) {
                        netcashin = netcashin + stk.getCashInTurnover() - stk.getCashOutTurnover();
                        cashin = cashin + stk.getCashInTurnover();
                        cashout = cashout + stk.getCashOutTurnover();
                        total = total + stk.getCashInTurnover() + stk.getCashOutTurnover();
                    }
                }

                if (total > 0 && cashin > 0 && cashout > 0) {
                    return (netcashin / total) * 100;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Double.NaN;
    }

    /*  public String getMainIndexForExchange() {

        try {
            Enumeration<String> sectors = null;
            String mainIndex = "";
            sectors = DataStore.getSharedInstance().getFilteredList(symbol, Meta.INSTRUMENT_INDEX);
            ArrayList<StringKeys> symbolList = new ArrayList<StringKeys>();
            String selectedsymbol1 = "";
            while (sectors.hasMoreElements()) {
                selectedsymbol1 = sectors.nextElement();
                Stock sector1 = DataStore.getSharedInstance().getStockObject(symbol, selectedsymbol1, Meta.INSTRUMENT_INDEX);
                StringKeys selectedsymbol = new StringKeys((String) sectors.nextElement(), sector1.getLongDescription());
                symbolList.add(selectedsymbol);
                sector1 = null;
            }
            Collections.sort(symbolList);
            Iterator iterator = symbolList.iterator();
            while (iterator.hasNext()) {
                String selectedsymbol = ((StringKeys) iterator.next()).getKey();
                Stock sector = DataStore.getSharedInstance().getStockObject(symbol, selectedsymbol, Meta.INSTRUMENT_INDEX);
                if ((sector.getParams() != null) && (sector.getParams().indexOf("M") >= 0)) { // check for main index Bug id <#0037> 8.20.009 / 29 June 2006 added null checking
                    mainIndex = selectedsymbol;
                }
                selectedsymbol = null;
                sector = null;
            }
            return mainIndex;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return "";
    }*/

    public String getMainIndexForExchange() {

        if (mainindex == null) {
            try {
                Enumeration<String> sectors = null;
                String mainIndex = "";
                sectors = DataStore.getSharedInstance().getFilteredList(symbol, Meta.INSTRUMENT_INDEX);
                ArrayList<StringKeys> symbolList = new ArrayList<StringKeys>();
                String selectedsymbol1 = "";
                while (sectors.hasMoreElements()) {
                    selectedsymbol1 = sectors.nextElement();
                    Stock sector1 = DataStore.getSharedInstance().getStockObject(symbol, selectedsymbol1, Meta.INSTRUMENT_INDEX);
                    StringKeys selectedsymbol = new StringKeys((String) sectors.nextElement(), sector1.getLongDescription());
                    symbolList.add(selectedsymbol);
                    sector1 = null;
                }
                Collections.sort(symbolList);
                Iterator iterator = symbolList.iterator();
                while (iterator.hasNext()) {
                    String selectedsymbol = ((StringKeys) iterator.next()).getKey();
                    Stock sector = DataStore.getSharedInstance().getStockObject(symbol, selectedsymbol, Meta.INSTRUMENT_INDEX);
                    //                if ((sector.getParams() != null) && (sector.getParams().indexOf("M") >= 0)) { // check for main index Bug id <#0037> 8.20.009 / 29 June 2006 added null checking
                    //                    mainIndex = selectedsymbol;
                    //                }
                    try {
                        if (isMainIndex(symbol, selectedsymbol)) { // check for main index Bug id <#0037> 8.20.009 / 29 June 2006 added null checking
                            //                    if ((sector.getParams() != null) && (sector.getParams().indexOf("M") >= 0)) { // check for main index Bug id <#0037> 8.20.009 / 29 June 2006 added null checking
                            mainindex = selectedsymbol;
                        }
                    } catch (Exception e) {
                        System.out.println("===index error===" + symbol + "--ex==" + selectedsymbol);
                    }
                    //if(iterator.)
                    selectedsymbol = null;
                    sector = null;
                }
                // return mainIndex;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            //return "";
        }
        return mainindex;
    }

    private boolean isMainIndex(String exchange, String symbol) {
        boolean isMainIndex = false;
        try {
            SmartProperties exgIndexes = new SmartProperties(Meta.DS);
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/index_" + Language.getSelectedLanguage() + ".msf";
            exgIndexes.loadCompressed(sLangSpecFileName);
            String data = (String) exgIndexes.get(symbol);
            try {
                if (Integer.parseInt(data.split(Meta.RS)[1]) == 1) {
                    String[] field = data.split(Meta.FS);
                    String dataRec = field[5] + Meta.ID + exchange + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                    String keyR = SharedMethods.getKey(exchange, symbol, Meta.INSTRUMENT_INDEX);
                    StringKeys index = new StringKeys(keyR, field[2]);
                    //todo check for duplicates

                    if (field[1].equals("IM")) { //--To Solve the conflict in MSM -Shanika
                        isMainIndex = true;
                    }

                } else {
                    //do nothing
                }
            } catch (Exception e) {
                e.printStackTrace();

            }


        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return isMainIndex;
    }

    public String getIndexName(String mainIndex) {
        return DataStore.getSharedInstance().getCompanyName(SharedMethods.getKey(symbol, mainIndex, Meta.INSTRUMENT_INDEX));
    }

    public double getIndexValue(String indexId) {
        Stock index = DataStore.getSharedInstance().getStockObject(symbol, indexId, Meta.INSTRUMENT_INDEX);
        return index.getLastTradeValue();
    }

    public double getIndexChange(String indexId) {
        Stock index = DataStore.getSharedInstance().getStockObject(symbol, indexId, Meta.INSTRUMENT_INDEX);
        return index.getChange();
    }

    public double getIndexPerChange(String indexId) {
        Stock index = DataStore.getSharedInstance().getStockObject(symbol, indexId, Meta.INSTRUMENT_INDEX);
        return index.getPercentChange();
    }

    public Hashtable<String, Hashtable> getExchangeIndices() {
        return exchangeIndices;
    }

    public void setEXGWhatchListVisibility(String id, String visibility) {
        watchListVisibility.put(id, visibility);

    }

    public boolean isEXGWatchListShouldVisible(String id) {
        int visibility = Integer.parseInt((String) watchListVisibility.get(id));
        if (visibility == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void putExchangeIndices(String key, Hashtable<String, Hashtable> table) {
        this.exchangeIndices.put(key, table);
    }

    public boolean isExchangehasIndicesWatchListsExists() {
        if (exchangeIndices.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public Hashtable<String, ArrayList<String>> getCashFlowExchangeIndices() {
        if (cashFlowExchangeIndices == null)
            cashFlowExchangeIndices = new Hashtable<String, ArrayList<String>>();
        return cashFlowExchangeIndices;
    }

    public void setCashFlowExchangeIndices(Hashtable<String, ArrayList<String>> cashFlowExchangeIndices) {
        this.cashFlowExchangeIndices = cashFlowExchangeIndices;
    }

    public boolean cashFlowExchageIndicesExists() {
        try {
            if (cashFlowExchangeIndices.size() > 0)
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }

    public Symbols getSymbols() {
        Symbols symbols = new Symbols();
        Enumeration exgEnum = exchangeIndices.elements();
        while (exgEnum.hasMoreElements()) {
            Hashtable temphash = (Hashtable) exgEnum.nextElement();
            Enumeration symbolEnum = temphash.elements();
            while (symbolEnum.hasMoreElements()) {
                String symbolList = (String) symbolEnum.nextElement();
                String[] symbolArray = symbolList.split(",");
                for (int i = 0; i < symbolArray.length; i++) {
                    symbols.appendSymbol(symbolArray[i]);
                }

            }
        }


        return symbols;
    }

    class StringKeys implements Comparable {
        String key;
        String name = "";

        StringKeys(String key1, String name1) {
            key = key1;
            name = name1;
        }

        public String toString() {
            return name;
        }

        public String getKey() {
            return key;
        }

        public int compareTo(Object o) {
            return toString().compareTo(o.toString());
        }


    }
}