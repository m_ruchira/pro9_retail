package com.isi.csvr.datastore;

import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.radarscreen.*;
import com.isi.csvr.shared.*;

import java.text.ParseException;
import java.util.Date;


public class
        StockData {
    public static final int COLUMN_SYMBOL = 0;
    public static final int COLUMN_COMPANY_CODE = 81;
    public static final int COLUMN_BID_PRICE = 20;
    public static final int COLUMN_BID_QUANTITY = 21;
    public static final int COLUMN_ASK_PRICE = 22;
    public static final int COLUMN_ASK_QUANTITY = 23;
    public static final int COLUMN_BID_ASK_RATIO = 36;
    public static final int COLUMN_OHLC = 92;
    public static final int COLUMN_CASH_FLOW_IN_ORDERS = 93;
    public static final int COLUMN_CASH_FLOW_OUT_ORDERS = 94;
    public static final int COLUMN_CASH_FLOW_IN_VOLUME = 95;
    public static final int COLUMN_CASH_FLOW_OUT_VOLUME = 96;
    public static final int COLUMN_CASH_FLOW_IN_TURNOVER = 97;
    public static final int COLUMN_CASH_FLOW_OUT_TURNOVER = 98;
    public static final int COLUMN_CASH_FLOW_NET_VALUE = 99;
    public static final int COLUMN_CASH_FLOW_CASH_MAP = 100;
    private static final TWDateFormat timeFormat = new TWDateFormat(Language.getString("BOARD_TIME_FORMAT"));
    private static final TWDateFormat dateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));
    private static final TWDateFormat forexDateFormat = new TWDateFormat(Language.getString("FOREX_DATE_FORMAT"));
    private static final LongTransferObject longTrasferObject = new LongTransferObject();
    private static final IntTransferObject minMaxTrasferObject = new IntTransferObject();
    private static final IntTransferObject week52HLTrasferObject = new IntTransferObject();
    private static final IntTransferObject announcementTrasferObject = new IntTransferObject();
    private static final LongTransferObject zoneTrasferObject = new LongTransferObject();
    private static final DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private static final StringTransferObject stringTransferObject = new StringTransferObject();
    private static final VariationImageTransferObject variationImageTransferObject = new VariationImageTransferObject();
    private static final TWDecimalFormat defaultFormat = new TWDecimalFormat("###,##0.00");
    //volumeWatcher related static varaible
    public static int volumeWatcherMode = 0;
    private static int COLUMN_COUNT = 0;
    //  private static PeriodicIndicator periodicIndicator= PeriodicIndicator.getSharedInstance();
    private static TWDecimalFormat decimalformat = new TWDecimalFormat("###,##0.####");

    public static synchronized Object getObjectData(Stock stock, String targetCurrency, int column) {

        if (stock.getInstrumentType() == Constants.TYPE_ORDER) {
            switch (column) {
                case -2:
                case -1:
                case 0:
                case 1:
                case 2:
                    stringTransferObject.setBlank(true);
                    break;
                case -11:
                    longTrasferObject.setBlank(true);
                    break;
                case 4:
                    doubleTransferObject.setBlank(stock.getLastTradeValue() <= 0);
                    break;
                case 5:
                    longTrasferObject.setBlank(stock.getTradeQuantity() <= 0);
                    break;
                case 20:
                    doubleTransferObject.setBlank(stock.getBestBidPrice() <= 0);
                    break;
                case 21:
                    longTrasferObject.setBlank(stock.getBestBidQuantity() <= 0);
                    break;
                case 22:
                    doubleTransferObject.setBlank(stock.getBestAskPrice() <= 0);
                    break;
                case 23:
                    longTrasferObject.setBlank(stock.getBestAskQuantity() <= 0);
                    break;
//                case -2:
                case 47:
                case 49:
                case 118:
                case 119:
                    // control values. do nothing
                    break;
                default:
                    doubleTransferObject.setBlank(true);
                    longTrasferObject.setBlank(true);
                    stringTransferObject.setBlank(false);
                    break;

            }
        } else {
            doubleTransferObject.setBlank(false);
            stringTransferObject.setBlank(false);
            longTrasferObject.setBlank(false);
        }

//        System.out.println("Col " + column + " blank " + doubleTransferObject.isBlank());

        switch (column) {
            case -14:
                return CurrencyStore.getSharedInstance().getDecimalPlaces(stock.getCurrencyCode());
            case -13:
                return stock.isSymbolEnabled();
            case -12:
                return stock.getNewsAvailability();
            case -11:
                return longTrasferObject.setValue(stock.getInstrumentType());
            case -10:
                return week52HLTrasferObject.setValue(stock.get52WeekHighLowTracker());
            case -9:
                if (stock.getTodaysOpen() == 0) {
                    return Stock.NORMAL;
                } else if (stock.getTodaysOpen() > stock.getPreviousClosed()) {
                    return Constants.STOCK_OPEN_ABOVE_PREV_CLOSED;
                } else if (stock.getTodaysOpen() < stock.getPreviousClosed()) {
                    return Constants.STOCK_OPEN_BELOW_PREV_CLOSED;
                } else {
                    return Stock.NORMAL;         //return zero
                }
            case -8:
                return stock.getEventAvailability();
            case -7:
                return stock.getNewsAvailability();
            case -6:
                return stock.getAnnouncementAvailability();
            case -5:
                return stock.getSymbolStatus();
            case -4:
                return stock.getDecimalCount();
            case -3:
                return stock.getShortDescription();
            case -2:
                return stringTransferObject.setValue(stock.getExchange());
            case -1:
                return stringTransferObject.setValue(stock.getSymbolCode());
            case 0:
                return stringTransferObject.setValue(stock.getSymbol());
            case 1:
//                if((Settings.isShowAlias())&&(stock.getAlias()!=null)){
//                    return stringTransferObject.setValue(stock.getAlias());
//                }else
//                  return stringTransferObject.setValue(stock.getInstrumentType()+"");
                return stringTransferObject.setValue(stock.getShortDescription());
            case 2:
                return stringTransferObject.setValue(stock.getLongDescription());

            /* Trade related */
            case 3:
                return longTrasferObject.setValue(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), stock.getLastTradeTime()));
            case 4:
                doubleTransferObject.setFlag(stock.getLastTradeFlag());
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getLastTradeValue()));
            case 5:
                longTrasferObject.setFlag(stock.getTradeQuantityFlag());
                return longTrasferObject.setValue(stock.getTradeQuantity());
            case 6:
                return longTrasferObject.setValue(stock.getTradeTick());
            case 7:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getChange()));
            case 8:
                return doubleTransferObject.setValue(stock.getPercentChange());
            case 9:
                // return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getAvgTradePrice()));
                if (stock.getExchange().equals("TDWL")) {
                    if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                        if (Double.compare(stock.getAvgTradePrice(), 0.0) == 0) {
                            return doubleTransferObject.setValue(Double.NaN);
                        }
                        return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getAvgTradePrice()));
                    } else
                        return doubleTransferObject.setValue(Double.NaN);

                } else
                    return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getAvgTradePrice()));

            case 10:
                longTrasferObject.setFlag(stock.getVolumeFlag());
                return longTrasferObject.setValue(stock.getVolume());
            case 11:
                doubleTransferObject.setFlag(stock.getTurnoverFlag());
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getTurnover()));
            case 12:
                return longTrasferObject.setValue(stock.getNoOfTrades());
            case 13:
                return longTrasferObject.setValue(stock.getAvgTradeVolume());
            case 14:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getMinPrice()));
            case 15:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getMaxPrice()));
            case 16:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getTodaysOpen()));
            case 17:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getHigh()));
            case 18:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getLow()));
            case 19:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getPreviousClosed()));

            /* Bid related */
            case 20:
                doubleTransferObject.setFlag(stock.getBestBidPriceFlag());
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getBestBidPrice()));
            case 21:
                return longTrasferObject.setValue(stock.getBestBidQuantity());

            /* Ask related */
            case 22:
                doubleTransferObject.setFlag(stock.getBestAskPriceFlag());
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getBestAskPrice()));
            case 23:
                return longTrasferObject.setValue(stock.getBestAskQuantity());

            /* Symbol Related */
            case 24:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getMarketCap()));
            case 25:
                return doubleTransferObject.setValue(stock.getPER());
            case 26:
                return doubleTransferObject.setValue(stock.getPBR());
            case 27:
                return doubleTransferObject.setValue(stock.getYield());
            case 28:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getHighPriceOf52Weeks()));
            case 29:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getLowPriceOf52Weeks()));
            case 30:
                return longTrasferObject.setValue(stock.getLastTradedDate());
            case 31:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getLastTradedPrice()));
            case 32:
                return longTrasferObject.setValue(stock.getTotalBidQty());
            case 33:
                return longTrasferObject.setValue(stock.getNoOfBids());
            case 34:
                return longTrasferObject.setValue(stock.getTotalAskQty());
            case 35:
                return longTrasferObject.setValue(stock.getNoOfAsks());
            case 36:
                return doubleTransferObject.setValue(stock.getBidaskRatio());
            case 37:
                return doubleTransferObject.setValue(stock.getSpread());
            case 38:
                return doubleTransferObject.setValue(stock.getRange());
            case 39:
                return doubleTransferObject.setValue(stock.getPctSpread());
            case 40:
                return doubleTransferObject.setValue(stock.getPctRange());
            case 41:
                variationImageTransferObject.setExchange(stock.getExchange());
                return variationImageTransferObject.setValue(stock.getBidaskRatio());
            case 42:
                variationImageTransferObject.setExchange(stock.getExchange());
                return variationImageTransferObject.setValue(stock.getPctSpread());
            case 43:
                variationImageTransferObject.setExchange(stock.getExchange());
                return variationImageTransferObject.setValue(stock.getPctRange());
            case 44:
                variationImageTransferObject.setExchange(stock.getExchange());
                return variationImageTransferObject.setValue(stock.getPercentChange());
            case 45:
                return longTrasferObject.setValue(stock.getTickMap());
            case 46:
//                return stringTransferObject.setValue(stock.getExchange());
                return stringTransferObject.setValue(ExchangeStore.getSharedInstance().getExchange(stock.getExchange()).getDisplayExchange()); // To display exchange
            case 47:
                return minMaxTrasferObject.setValue(stock.getMinOrMax());
            case 48:
                return announcementTrasferObject.setValue(stock.getBulbStatus());
            case 49:
                return zoneTrasferObject.setValue(stock.getTimeOffset());
            // Option Symbol Data
            case 50:
                return longTrasferObject.setValue(stock.getOpenInterest());
            case 51:
                return longTrasferObject.setValue(stock.getExpirationDate());
            case 52:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getStrikePrice()));
            case 53:
                return stock.getOptionBaseSymbol();
            case 54:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getContractHigh()));
            case 55:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getContractLow()));
            case 56:
                return stringTransferObject.setValue(stock.getCurrencyCode());
            case 57:
                double topTcpValue = stock.getTopTcpValue();
                if (topTcpValue == -1)
                    return stringTransferObject.setValue(Language.getString("NA_SHORT"));
                topTcpValue = getCurrencyAdjustedValue(stock, targetCurrency, stock.getTopTcpValue());
                return stringTransferObject.setValue("" + defaultFormat.format(topTcpValue));
            case 58:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getHighAsk()));
            case 59:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getLowBid()));
            case 60:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getSimpleAverageAsk()));
            case 61:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getWAverageAsk()));
            case 62:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getSimpleAverageBid()));
            case 63:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getWAverageBid()));
            case 64:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getLastAskPrice()));
            case 65:
                return longTrasferObject.setValue(stock.getLastAskQuantity());
            case 66:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getLastBidPrice()));
            case 67:
                return longTrasferObject.setValue(stock.getLastBidQuantity());
            case 68:
                RSSimpleMovingAverage.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getSimpleMovingAverage());
            case 69:
                RSExpMovingAverage.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getExponentialMovingAverage());
            case 70:
                RSWghtMovingAverage.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getWeightedMovingAverage());
            case 71:
                RSMACD.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getMACD());
            case 72:
                RSIMI.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getIMI());
            case 73:
                RSRSI.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getRSI());
            case 74:
                RSROC.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getROC());
            case 75:
                RSFastStochastic.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getFastStochastic());
            case 76:
                RSSlowStochastic.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getSlowStochastic());
            case 77:
                RSBollingerMiddle.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getBollingerMiddle());
            case 78:
                RSBollingerUpper.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getBollingerUpper());
            case 79:
                RSBollingerLower.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getBollingerLower());
            case 80:
                RSChaikinOsc.setLastVisibleTime(System.currentTimeMillis());
                RadarScreenInterface.getInstance().getSymbols().get(stock.getKey()).setLastVisibleTime(System.currentTimeMillis());
                return doubleTransferObject.setValue(stock.getChaikinOsc());
            case 81:
                return stringTransferObject.setValue(stock.getCompanyCode());
            case 82:
                return stringTransferObject.setValue((ExchangeStore.getSharedInstance().getMarket(stock.getExchange(), stock.getMarketID())).getDescription());
            case 83:
//                if(stock.getSectorCode().equals(Constants.DEFAULT_SECTOR)){
                //   return stringTransferObject.setValue("UNCLASSIFIED");
                //}else
                return stringTransferObject.setValue(SectorStore.getSharedInstance().getSector(stock.getExchange(), stock.getSectorCode()).getDescription());
            case 84:
                return longTrasferObject.setValue(stock.getSharesForForeigners());
            case 85:
                return stringTransferObject.setValue(stock.getISINCode());
            case 86:
                return doubleTransferObject.setValue(stock.getNetAssetValue()); // NAV
            case 87:
                return doubleTransferObject.setValue(stock.getFundValue()); // Mutual Fund total value
            case 88:
                return doubleTransferObject.setValue(stock.getPerformanceYTD());
            case 89:
                return doubleTransferObject.setValue(stock.getPerformance12M());
            case 90:
                return doubleTransferObject.setValue(stock.getPerformance3Y());
            case 91:
                return doubleTransferObject.setValue(stock.getPerformance5Y());
            case 92:
                variationImageTransferObject.setExchange(stock.getExchange());
                return variationImageTransferObject.setValueArray(stock.getOHLCArray());
            case 93:
                return longTrasferObject.setValue(stock.getCashInOrders());
            case 94:
                return longTrasferObject.setValue(stock.getCashInVolume());
            case 95:
//                 return longTrasferObject.setValue((long)stock.getCashInTurnover());
                return doubleTransferObject.setValue(stock.getCashInTurnover());
            case 96:
                return longTrasferObject.setValue(stock.getCashOutOrders());
            case 97:
                return longTrasferObject.setValue(stock.getCashOutVolume());
            case 98:
//                 return longTrasferObject.setValue((long)stock.getCashOutTurnover());
                return doubleTransferObject.setValue(stock.getCashOutTurnover());
            case 99:
                return doubleTransferObject.setValue(stock.getCashFlowNet());
            case 100:
                return doubleTransferObject.setValue(stock.getCashFlowPct());
            case 101:
                return doubleTransferObject.setValue(stock.getCashFlowRatio());
            case 102:
                return doubleTransferObject.setValue(stock.getLotSize());
            case 103:
                return stringTransferObject.setValue(stock.getUnit());
            case 104:
                return longTrasferObject.setValue(stock.getListedShares());
            case 105:
                return doubleTransferObject.setValue(stock.getEPS());
            case 106:
                return stringTransferObject.setValue(forexDateFormat.format(SharedMethods.getDateForex(stock.getForexBuyDate())));
            case 107:
                return stringTransferObject.setValue(forexDateFormat.format(SharedMethods.getDateForex(stock.getForexSellDate())));
            //new mutual funds
//            case 108:
//                return stringTransferObject.setValue(stock.getDescription());//description
//            case 109:
//                return stringTransferObject.setValue(stock.getFundName());   //fundname
            case 108:
                if (stock.getAmc() != null) {
                    return stringTransferObject.setValue(stock.getAmc());       //amc
                } else {
                    return stringTransferObject.setValue(Language.getString("NA"));
                }
            case 109:
                if (stock.getCountry() != null) {
                    return stringTransferObject.setValue(stock.getCountry());    //country
                } else {
                    return stringTransferObject.setValue(Language.getString("NA"));
                }
            case 110:
                return doubleTransferObject.setValue(stock.getRisk1Y());     //reisk1y
            case 111:
                return doubleTransferObject.setValue(stock.getMinInitPurchase());   //mininitpurchase
            case 112:
                return doubleTransferObject.setValue(stock.getFEndLoad());       //fend
            case 113:
                return doubleTransferObject.setValue(stock.getBEndLoad());       //bend
            case 114:
                return longTrasferObject.setValue(stock.getFundsize());         //fundsize
            case 115:
                return doubleTransferObject.setValue(stock.getSubsPurchase());   //subspurchase
            case 116:
                return doubleTransferObject.setValue(stock.getTotAsset());      //tot asset
            case 117:
                return longTrasferObject.setValue(stock.getTotAssetDate());     //tot assetdate
            case 118:
                return longTrasferObject.setValue(stock.getAvgVolume(getVolumeWatcherMode()));     //avg Volume
            case 119:
                return doubleTransferObject.setValue(stock.getPctVolume(getVolumeWatcherMode()));     //pct% Volume
            case 120:
                return longTrasferObject.setValue(stock.get7DVolume());       // 7Day avg volume new UI for volume watcher
            case 121:
                return longTrasferObject.setValue(stock.get30DVolume());      // 30Day avg volume new UI for volume watcher
            case 122:
                return longTrasferObject.setValue(stock.get90DVolume());      // 90Day avg volume new UI for volume watcher
            case 123:
                return doubleTransferObject.setValue(stock.get7DVolumeChange());    // 7Day avg volume change new UI for volume watcher
            case 124:
                return doubleTransferObject.setValue(stock.get30DVolumeChange());   // 30Day avg volume change new UI for volume watcher
            case 125:
                return doubleTransferObject.setValue(stock.get90DVolumeChange());   // 90Day avg volume change new UI for volume watcher
            case 126:
                return doubleTransferObject.setValue(getCurrencyAdjustedValue(stock, targetCurrency, stock.getChgVWAPBase()));
            case 127:
                return doubleTransferObject.setValue(stock.getPercentageChgVWAPBase());
            //related to BOND Market
            //Added by chandika 2009-06-30

            case 128:
                return longTrasferObject.setValue(stock.getSettlementDate());       //bond settlement date
            case 129:
                return doubleTransferObject.setValue(stock.getIssuedAmount());      //bond issued amount
            case 130:
                return longTrasferObject.setValue(stock.getDateofIssue());          //bond Date of issue
            case 131:
                return doubleTransferObject.setValue(stock.getOutstandingAmount()); //bond outstanding amount
            case 132:
                return longTrasferObject.setValue(stock.getMatuarityDate());        //bond matuarity date
            case 133:
                return doubleTransferObject.setValue(stock.getCouponRate());        //bond coupon rate
            case 134:
                return doubleTransferObject.setValue(stock.getNominalValue());      //bond nominal value
            case 135:
                return stringTransferObject.setValue(stock.getIssueCurrency());     //bond issue currency
            case 136:
                return longTrasferObject.setValue(stock.getListingDate());          //bond listing date
            case 137:
                return longTrasferObject.setValue(stock.getCouponDate());          //next  coupon date
            case 138:
                return longTrasferObject.setValue(stock.getBondType());             //bond bond type
            case 139:
                return longTrasferObject.setValue(stock.getTradedThoughDate());     //bond traded though date
            case 140:
                return longTrasferObject.setValue(stock.getRateCalcType());         //bond rate calc type
            case 141:
                return longTrasferObject.setValue(stock.getDayCountMethod());       //bond day count method
            case 142:
                return stringTransferObject.setValue(stock.getLoanNumber());        //bond loan number
            case 143:
                return stringTransferObject.setValue(stock.getExternalBondType());  //bond external type
            case 144:
                return stringTransferObject.setValue(stock.getEarlyLateMaturity()); //bond early late maturity
            case 145:
                return doubleTransferObject.setValue(stock.getMaturityValue());     //bond maturity value
            case 146:
//				return doubleTransferObject.setValue(stock.getYearlyCouponFreq());  //bond early coupon frequancy
                return longTrasferObject.setValue((long) stock.getYearlyCouponFreq());  //bond early coupon frequancy

            case 147:
                return stringTransferObject.setValue(stock.getRecordDateType());    //bond record date type
            case 148:
                return longTrasferObject.setValue(stock.getNumberOfRecordDays());   //bond number of record days
            case 149:
                return longTrasferObject.setValue(stock.getInterestFromDate());     //bond interest from date
            case 150:
                return longTrasferObject.setValue(stock.getInterestDayType());   //bond interest day type
            case 151:
                return longTrasferObject.setValue(stock.getPreviousCouponDate());   //bond previous coupon date
            case 152:
                return longTrasferObject.setValue(stock.getLastCouponDate());       //bond last coupon date
            case 153:
                return longTrasferObject.setValue(stock.getCouponRecordDate());     //bond coupon record date
            case 154:
                return longTrasferObject.setValue(stock.getFaceValueDimension());   //bond face value dimension
            case 155:
                return longTrasferObject.setValue(stock.getNumberOfCashFlows());    //bond number of cash flows
            case 156:
                return doubleTransferObject.setValue(stock.getCouponFrequancy());   //bond coupon frequancy
            case 157:
                return longTrasferObject.setValue(stock.getDaysToFirstCashFlow());  //bond days to first cashflow
            case 158:
                return longTrasferObject.setValue(stock.getDaysInFirstRecord());    //bond days in first period(record)
            case 159:
                return doubleTransferObject.setValue(stock.getIndexCouponAdjustments());     //bond index coupon adjustment
            case 160:
                return doubleTransferObject.setValue(stock.getAccuredInterest());   //bond accured interest
            case 161:
                return doubleTransferObject.setValue(stock.getIndexFactor());       //bond index factor
            case 162:
                return longTrasferObject.setValue(stock.getCalcConvention());       //bond calc convention
            case 163:
                return stringTransferObject.setValue(stock.getBenchmark());         //MF benchmark
            case 164:
                return longTrasferObject.setValue(stock.getDeadline());             //MF dealing deadline
            case 165:
                return doubleTransferObject.setValue(stock.getMgtFee());            //MF management fee
            case 166:
                return stringTransferObject.setValue(stock.getFundName());          //MF fund name
            case 167:
                return doubleTransferObject.setValue(stock.getBenchmarkIndVal());   //MF benchmark ind value
            case 168:
                return doubleTransferObject.setValue(stock.getFaceValue());         //bond face value
            case 169:
                return doubleTransferObject.setValue(stock.getYTDStockValue());
            case 170:
                return stringTransferObject.setValue(stock.getGroupName());
            case 171:
                return stringTransferObject.setValue(stock.getGroupStatus());
            case 172:
                return doubleTransferObject.setValue(stock.getWeekAvgPrice());
            case 173:
                return doubleTransferObject.setValue(stock.getMonthAvgPrice());
            case 174:
                return doubleTransferObject.setValue(stock.getIntrinsicValue());
            /*if (stock.getInstrumentType() == Constants.TRADABLE_RIGHTS) {
                return doubleTransferObject.setValue(stock.getIntrinsicValue());
            } else {
                return decimalformat.format(-0d);
            }*/
            case 175:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getClosingVWAP(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getClosingVWAP());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 176:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getTWAP(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getTWAP());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
                /*case 177:
           if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))  {
               if(Double.compare(stock.getPeriodicIndicator().getEndOfDayIndicator(),0.0) == 0){
                   return  doubleTransferObject.setValue(Double.NaN);
               }
               return doubleTransferObject.setValue(stock.getPeriodicIndicator().getEndOfDayIndicator());
           }else
               return doubleTransferObject.setValue(Double.NaN);*/
            case 177:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (stock.getPeriodicIndicator().getPeriodStart() == null) {
                        return stringTransferObject.setValue("N/A");
                    }
                    // return longTrasferObject.setValue(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(),Long.parseLong(stock.getPeriodicIndicator().getPeriodStart())));
                    // String time = timeFormat.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), Long.parseLong(stock.getPeriodicIndicator().getPeriodStart()))));
                    // String time = stock.getPeriodicIndicator().getPeriodStart();
                    String time = null;
                    try {
                        time = timeFormat.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), (new TWDateFormat("DDMMyyyyHHmmss").parse(new TWDateFormat("DDMMyyyy").format(new Date()) + stock.getPeriodicIndicator().getPeriodStart()).getTime()))));
                    } catch (ParseException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }

                    return stringTransferObject.setValue(time);


                } else
                    return stringTransferObject.setValue("N/A");
            case 178:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (stock.getPeriodicIndicator().getPeriodStop() == null) {
                        return stringTransferObject.setValue("N/A");
                    }
                    //String time = timeFormat.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), Long.parseLong(stock.getPeriodicIndicator().getPeriodStop()))));
                    String time = null;
                    try {
                        time = timeFormat.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), (new TWDateFormat("DDMMyyyyHHmmss").parse(new TWDateFormat("DDMMyyyy").format(new Date()) + stock.getPeriodicIndicator().getPeriodStop()).getTime()))));
                    } catch (ParseException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }

                    return stringTransferObject.setValue(time);
                } else
                    return stringTransferObject.setValue("N/A");
            case 179:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    //if(Double.compare(periodicIndicator.getTWAS(),0.0) == 0){
                    if (Double.compare(stock.getPeriodicIndicator().getTWAS(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTWAS());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 180:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getRTWAS(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getRTWAS());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 181:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTWABBid(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTWABBid());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 182:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTWABAsk(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTWABAsk());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 183:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVWAQ(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVWAQ());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 184:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOBAskVolume(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOBAskVolume());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 185:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOBAskValue(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOBAskValue());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 186:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTNOBAsk(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTNOBAsk());
                } else
                    return doubleTransferObject.setValue(Double.NaN);


            case 187:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOAAskVolume(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOAAskVolume());
                } else
                    return doubleTransferObject.setValue(Double.NaN);

            case 188:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOAAskValue(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOAAskValue());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 189:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTNOAAsk(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTNOAAsk());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 190:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOBBidVolume(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOBBidVolume());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 191:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOBBidValue(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOBBidValue());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 192:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTNOBBid(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTNOBBid());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 193:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOABidVolume(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOABidVolume());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 194:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOABidValue(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOABidValue());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 195:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTNOABid(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTNOABid());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 196:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOBVolume(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOBVolume());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 197:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOBValue(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOBValue());
                } else
                    return doubleTransferObject.setValue(Double.NaN);

            case 198:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTNOB(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTNOB());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 199:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOAVolume(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOAVolume());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 200:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTVOAValue(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTVOAValue());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 201:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTNOA(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTNOA());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 202:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTotalOrderCoverage(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTotalOrderCoverage());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 203:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getBidOrderCoverage(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getBidOrderCoverage());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 204:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getAskOrderCoverage(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getAskOrderCoverage());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 205:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getBEVWAPBid(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getBEVWAPBid());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 206:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getBEVWAPAsk(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getBEVWAPAsk());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 207:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTotalVolumeBid(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTotalVolumeBid());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 208:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getTotalVolumeAsk(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getTotalVolumeAsk());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 209:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getBAIBest(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getBAIBest());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 210:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getBAIAll(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getBAIAll());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 211:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getCIBid(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getCIBid());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 212:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
                    if (Double.compare(stock.getPeriodicIndicator().getCIAsk(), 0.0) == 0) {
                        return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getPeriodicIndicator().getCIAsk());
                } else
                    return doubleTransferObject.setValue(Double.NaN);
            case 213:
                if (Double.compare(stock.getTodaysClose(), 0.0) == 0) {
                    return doubleTransferObject.setValue(Double.NaN);
                }
                return doubleTransferObject.setValue(stock.getTodaysClose());
            case 214:

                //  return doubleTransferObject.setValue(stock.getLoosingCategory());

                if (stock.getExchange().equals("TDWL")) {
                    //if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) { // added by Nishantha, premium user filter
                    if (stock.getLoosingCategory() == 0) {
                        doubleTransferObject.setFlag(stock.getLoosingCategory());
                        //return doubleTransferObject.setValue(Double.NaN);
                    }
                    return doubleTransferObject.setValue(stock.getLoosingCategory());
                    //  } else
                    //      return doubleTransferObject.setValue(Double.NaN);

                } else
                    return doubleTransferObject.setValue(stock.getLoosingCategory());
            case 215:
                return doubleTransferObject.setValue(stock.getTop());
            case 216:
                return longTrasferObject.setValue(stock.getTov());
            case 217:
                return doubleTransferObject.setValue(stock.getTcp());
            case 218:
                return longTrasferObject.setValue(stock.getTcv());
            case 219:
                return doubleTransferObject.setValue(stock.getShareCapital());
            case 220:
                return longTrasferObject.setValue(stock.getCurrentAdjstVal());
            case 221:
                return doubleTransferObject.setValue(stock.getStaticMax());
            case 222:
                return doubleTransferObject.setValue(stock.getStaticMin());


                /*    case 214:
           if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))  {
               if(stock.getPeriodicIndicator().getLocalRoundTripClasses().equals("")){
                   return  doubleTransferObject.setValue(Double.NaN);
               }
               return doubleTransferObject.setValue(Double.parseDouble(stock.getPeriodicIndicator().getLocalRoundTripClasses()));
           }else
               return doubleTransferObject.setValue(Double.NaN);
       case 215:
           if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))  {
               if(stock.getPeriodicIndicator().getLocalRoundTripCosts().equals("")){
                   return  doubleTransferObject.setValue(Double.NaN);
               }
               return doubleTransferObject.setValue(Double.parseDouble(stock.getPeriodicIndicator().getLocalRoundTripCosts()));
           }else
               return doubleTransferObject.setValue(Double.NaN);
       case 216:
           if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))  {
               if(stock.getPeriodicIndicator().getLocalRoundTripCoverages().equals("")){
                   return  doubleTransferObject.setValue(Double.NaN);
               }
               return doubleTransferObject.setValue(Double.parseDouble(stock.getPeriodicIndicator().getLocalRoundTripCoverages()));
           }else
               return doubleTransferObject.setValue(Double.NaN);
       case 217:
           if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))  {
               if(stock.getPeriodicIndicator().getReferenceRoundTripClasses().equals("")){
                   return  doubleTransferObject.setValue(Double.NaN);
               }
               return doubleTransferObject.setValue(Double.parseDouble(stock.getPeriodicIndicator().getReferenceRoundTripClasses()));
           }else
               return doubleTransferObject.setValue(Double.NaN);
       case 218:
           if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))  {
               if(stock.getPeriodicIndicator().getReferenceRoundTripCosts().equals("")){
                   return  doubleTransferObject.setValue(Double.NaN);
               }
               return doubleTransferObject.setValue(Double.parseDouble(stock.getPeriodicIndicator().getReferenceRoundTripCosts()));
           }else
               return doubleTransferObject.setValue(Double.NaN);

       case 219:
           if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))  {
               if(stock.getPeriodicIndicator().getReferenceRoundTripCoverages().equals("")){
                   return  doubleTransferObject.setValue(Double.NaN);
               }
               return doubleTransferObject.setValue(Double.parseDouble(stock.getPeriodicIndicator().getReferenceRoundTripCoverages()));
           }else
               return doubleTransferObject.setValue(Double.NaN);*/

            default:
                return null;
        }
    }


    private static double getCurrencyAdjustedValue(Stock stock, String targetCurrency, double value) {
        if (targetCurrency != null) {
            return CurrencyStore.getAdjustedPriceFor(stock.getCurrencyCode(), targetCurrency, value);
        } else {
            return value;
        }
    }

    /* private static float getCurrencyAdjustedValue(Stock stock, String targetCurrency, float value) {
        if (targetCurrency != null) {
            return CurrencyStore.getAdjustedPriceFor(stock.getCurrencyCode(), targetCurrency, value);
        } else {
            return value;
        }
    }

    private static float getCurrencyAdjustedValue(Stock stock, float value) {
        return CurrencyStore.getAdjustedPriceFor(stock.getCurrencyCode(),
                ExchangeStore.getSharedInstance().getExchange(stock.getExchange()).getActiveCurrency(),
                value);
    }*/

    private static String getStringData(Stock stock, int column) {

        applyDecimalPlaces(stock.getDecimalCount());

        switch (column) {
            case -2:
                return "" + stock.getExchange();
            case -1:
                return stock.getSymbolCode();
            case 0:
                return "" + stock.getSymbol();
            case 1:
//                if((Settings.isShowAlias())&&(stock.getAlias()!=null)){
//                    return stock.getAlias();
//                }else
                return stock.getShortDescription();
            case 2:
                return stock.getLongDescription();

            /* Trade related */
            case 3:
                if (stock.getLastTradeTime() > 86400000) {
                    return "" + timeFormat.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), stock.getLastTradeTime())));
                } else {
                    return "";
                }
//                return "" + timeFormat.format(new Date(stock.getLastTradeTime() + stock.getTimeOffset()));
            case 4:
                return decimalformat.format(stock.getLastTradeValue());
            case 5:
                return "" + stock.getTradeQuantity();
            case 6:
                return "" + stock.getTradeTick();
            case 7:
                return decimalformat.format(stock.getChange());
            case 8:
                return defaultFormat.format(stock.getPercentChange());

            case 9:

                //return decimalformat.format(stock.getAvgTradePrice());
                return "" + stock.getAvgTradePrice();


            case 10:
                return "" + stock.getVolume();
            case 11:
                return defaultFormat.format(stock.getTurnover());
            case 12:
                return "" + stock.getNoOfTrades();
            case 13:
                return "" + stock.getAvgTradeVolume();
            case 14:
                return decimalformat.format(stock.getMinPrice());
            case 15:
                return decimalformat.format(stock.getMaxPrice());
            case 16:
                return decimalformat.format(stock.getTodaysOpen());
            case 17:
                return decimalformat.format(stock.getHigh());
            case 18:
                return decimalformat.format(stock.getLow());
            case 19:
                return decimalformat.format(stock.getPreviousClosed());

            /* Bid related */
            case 20:
                return decimalformat.format(stock.getBestBidPrice());
            case 21:
                return "" + stock.getBestBidQuantity();

            /* Ask related */
            case 22:
                return decimalformat.format(stock.getBestAskPrice());
            case 23:
                return "" + stock.getBestAskQuantity();

            /* Symbol Related */
            case 24:
                return decimalformat.format(stock.getMarketCap());
            case 25:
                return decimalformat.format(stock.getPER());
            case 26:
                return decimalformat.format(stock.getPBR());
            case 27:
                return decimalformat.format(stock.getYield());
            case 28:
                return decimalformat.format(stock.getHighPriceOf52Weeks());
            case 29:
                return decimalformat.format(stock.getLowPriceOf52Weeks());
            case 30:
                return "" + dateFormat.format(new Date(stock.getLastTradedDate()));
            case 31:
                return decimalformat.format(stock.getLastTradedPrice());
            case 32:
                return "" + stock.getTotalBidQty();
            case 33:
                return "" + stock.getNoOfBids();
            case 34:
                return "" + stock.getTotalAskQty();
            case 35:
                return "" + stock.getNoOfAsks();
            case 36:
                if (Double.isNaN(stock.getBidaskRatio()) || (stock.getBidaskRatio() < 0)) {
                    return Language.getString("NA");
                } else if ((stock.getBidaskRatio() == Double.MAX_VALUE) || (stock.getBidaskRatio() == Double.POSITIVE_INFINITY)) {
                    return Language.getString("BID");
                } else if ((stock.getBidaskRatio() == 0)) {
                    return Language.getString("ASK");
                } else {
                    return decimalformat.format(stock.getBidaskRatio());
                }

            case 37:
                //return "" + stock.getSpread();
                if (Double.isNaN(stock.getSpread())) {
                    return Language.getString("NA");
                } else if ((stock.getSpread() == Double.MAX_VALUE) || (stock.getSpread() == Double.POSITIVE_INFINITY)) {
                    return Language.getString("BID");
                } else if ((stock.getSpread() == Double.MIN_VALUE) || (stock.getSpread() == Double.NEGATIVE_INFINITY)) {
                    return Language.getString("ASK");
                } else {
                    return decimalformat.format(stock.getSpread());
                }
            case 38:
                return decimalformat.format(stock.getRange());
            case 39:
                //return "" + stock.getPctSpread();
                if (Double.isNaN(stock.getPctSpread())) {
                    return Language.getString("NA");
                } else if ((stock.getPctSpread() == Double.MAX_VALUE) || (stock.getPctSpread() == Double.POSITIVE_INFINITY)) {
                    return Language.getString("BID");
                } else if ((stock.getPctSpread() == Double.MIN_VALUE) || (stock.getPctSpread() == Double.NEGATIVE_INFINITY)) {
                    return Language.getString("ASK");
                } else {
                    return decimalformat.format(stock.getPctSpread());
                }
            case 40:
                return decimalformat.format(stock.getPctRange());
            case 41:
                if (Double.isNaN(stock.getBidaskRatio())) {
                    return Language.getString("NA");
                } else if ((stock.getBidaskRatio() == Double.MAX_VALUE) || (stock.getBidaskRatio() == Double.POSITIVE_INFINITY)) {
                    return Language.getString("BID");
                } else if ((stock.getBidaskRatio() == 0)) {
                    return Language.getString("ASK");
                } else {
                    return decimalformat.format(stock.getBidaskRatio());
                }
            case 42:
                //return "" + stock.getPctSpread();
                if (Double.isNaN(stock.getPctSpread())) {
                    return Language.getString("NA");
                } else if ((stock.getPctSpread() == Double.MAX_VALUE) || (stock.getPctSpread() == Double.POSITIVE_INFINITY)) {
                    return Language.getString("BID");
                } else if ((stock.getPctSpread() == Double.MIN_VALUE) || (stock.getPctSpread() == Double.NEGATIVE_INFINITY)) {
                    return Language.getString("ASK");
                } else {
                    return decimalformat.format(stock.getPctSpread());
                }
            case 43:
                return decimalformat.format(stock.getPctRange());
            case 44:
                return decimalformat.format(stock.getPercentChange());
            case 45:
                return "" + stock.getTickMap();
            case 46:
                return ExchangeStore.getSharedInstance().getExchange(stock.getExchange()).getDisplayExchange();
            case 47:
                return "" + stock.getMinOrMax();
            case 48:
                return "" + stock.getBulbStatus();
            case 49:
                return "" + stock.getTimeOffset();
            case 50:
                return "" + stock.getOpenInterest();
            case 51:
                if (stock.getExpirationDate() != 0) {
                    return dateFormat.format(new Date(stock.getExpirationDate()));
                } else {
                    return "";
                }
            case 52:
                return decimalformat.format(stock.getStrikePrice());
            case 53:
                if (stock.getOptionBaseSymbol() != null) {

                    return "" + stock.getOptionBaseSymbol();
                } else {
                    return "";
                }

            case 54:
                return decimalformat.format(stock.getContractHigh());
            case 55:
                return decimalformat.format(stock.getContractLow());
            case 56:
                return "" + stock.getCurrencyCode();
            case 57:
                return defaultFormat.format(stock.getTopTcpValue());
            case 58:
                return decimalformat.format(stock.getHighAsk());
            case 59:
                return decimalformat.format(stock.getLowBid());
            case 60:
                return decimalformat.format(stock.getSimpleAverageAsk());
            case 61:
                return decimalformat.format(stock.getWAverageAsk());
            case 62:
                return decimalformat.format(stock.getSimpleAverageBid());
            case 63:
                return decimalformat.format(stock.getWAverageBid());
            case 64:
                return decimalformat.format(stock.getLastAskPrice());
            case 65:
                return "" + stock.getLastAskQuantity();
            case 66:
                return decimalformat.format(stock.getLastBidPrice());
            case 67:
                return "" + stock.getLastBidQuantity();
            case 68:
                return decimalformat.format(stock.getSimpleMovingAverage());
            case 69:
                return decimalformat.format(stock.getExponentialMovingAverage());
            case 70:
                return decimalformat.format(stock.getWeightedMovingAverage());
            case 71:
                return decimalformat.format(stock.getMACD());
            case 72:
                return decimalformat.format(stock.getIMI());
            case 73:
                return decimalformat.format(stock.getRSI());
            case 74:
                return decimalformat.format(stock.getROC());
            case 75:
                return decimalformat.format(stock.getFastStochastic());
            case 76:
                return decimalformat.format(stock.getSlowStochastic());
            case 77:
                return decimalformat.format(stock.getBollingerMiddle());
            case 78:
                return decimalformat.format(stock.getBollingerUpper());
            case 79:
                return decimalformat.format(stock.getBollingerLower());
            case 80:
                return decimalformat.format(stock.getChaikinOsc());
            case 81:
                return stock.getCompanyCode();
            case 82:
                try {
                    return (ExchangeStore.getSharedInstance().getMarket(stock.getExchange(), stock.getMarketID())).getDescription();
                } catch (Exception e) {
                    return "";
                }
            case 83:
                try {
                    return SectorStore.getSharedInstance().getSector(stock.getExchange(), stock.getSectorCode()).getDescription();
                } catch (Exception e) {
                    return "";
                }
            case 84:
                return "" + stock.getSharesForForeigners();
            case 85:
                return stock.getISINCode();
            case 86:
                return decimalformat.format(stock.getNetAssetValue()); // NAV
            case 87:
                return decimalformat.format(stock.getFundValue()); // Mutual Fund total value
            case 88:
                return decimalformat.format(stock.getPerformanceYTD());
            case 89:
                return decimalformat.format(stock.getPerformance12M());
            case 90:
                return decimalformat.format(stock.getPerformance3Y());
            case 91:
                return decimalformat.format(stock.getPerformance5Y());
            case 92:
                return "";
            case 93:
                return "" + stock.getCashInOrders();
            case 94:
                return "" + stock.getCashInVolume();
            case 95:
                return decimalformat.format(stock.getCashInTurnover());
            case 96:
                return "" + stock.getCashOutOrders();
            case 97:
                return "" + stock.getCashOutVolume();
            case 98:
                return decimalformat.format(stock.getCashOutTurnover());
            case 99:
                return decimalformat.format(stock.getCashFlowNet());
            case 100:
                double value = stock.getCashFlowPct();
                if (value >= 0) {
                    return decimalformat.format(value);
                } else {
                    return "-1";
                }
            case 101:
                return "" + decimalformat.format(stock.getCashFlowRatio());
            case 102:
                return "" + stock.getLotSize();
            case 103:
                return stock.getUnit();
            case 104:
                return "" + stock.getListedShares();
            case 105:
                return decimalformat.format(stock.getEPS());
            case 106:
                return forexDateFormat.format(SharedMethods.getDateForex(stock.getForexBuyDate()));
            case 107:
                return forexDateFormat.format(SharedMethods.getDateForex(stock.getForexSellDate()));
            //mutual funds new
//            case 108:
//                System.out.println("MDEX 1: "+stock.getDescription());
//                 return "" + stock.getDescription();
//            case 108:
//                System.out.println("MDEX 2: "+stock.getFundName());
//                 return "" + stock.getFundName();
            case 108:
                if (stock.getAmc() != null) {

                    return "" + stock.getAmc();
                } else {
                    return "";
                }
            case 109:
                if (stock.getAmc() != null) {
                    return "" + stock.getCountry();

                } else {
                    return "";
                }
            case 110:
                return decimalformat.format(stock.getRisk1Y());
            case 111:
                return decimalformat.format(stock.getMinInitPurchase());
            case 112:
                return decimalformat.format(stock.getFEndLoad());
            case 113:
                return decimalformat.format(stock.getBEndLoad());
            case 114:
                return "" + stock.getFundsize();
            case 115:
                return decimalformat.format(stock.getSubsPurchase());
            case 116:
                return decimalformat.format(stock.getTotAsset());
            case 117:
                return "" + stock.getTotAssetDate();
            /*case 118:
         return "" + stock.get52WeekHighLowTracker();*/
            case 118:
                return "" + stock.getAvgVolume(getVolumeWatcherMode());
            case 119:
                return decimalformat.format(stock.getPctVolume(getVolumeWatcherMode()));
            case 120:
                return "" + stock.get7DVolume();
            case 121:
                return "" + stock.get30DVolume();
            case 122:
                return "" + stock.get90DVolume();
            case 123:
                return decimalformat.format(stock.get7DVolumeChange());
            case 124:
                return decimalformat.format(stock.get30DVolumeChange());
            case 125:
                return decimalformat.format(stock.get90DVolumeChange());
            case 126:
                return decimalformat.format(stock.getChgVWAPBase());
            case 127:
                return defaultFormat.format(stock.getPercentageChgVWAPBase());

            //related to BOND Market
            //Added by chandika 2009-07-01

            case 128:
                return "" + stock.getSettlementDate();             //bond settlement date
            case 129:
                return decimalformat.format(stock.getIssuedAmount());               //bond issued amount
            case 130:
                return "" + stock.getDateofIssue();                //bond Date of issue
            case 131:
                return decimalformat.format(stock.getOutstandingAmount());          //bond outstanding amount
            case 132:
                return "" + stock.getMatuarityDate();              //bond matuarity date
            case 133:
                return decimalformat.format(stock.getCouponRate());                 //bond coupon rate
            case 134:
                return decimalformat.format(stock.getNominalValue());               //bond nominal value
            case 135:
                return "" + stock.getIssueCurrency();              //bond issue currency
            case 136:
                return "" + stock.getListingDate();                //bond listing date
            case 137:
                return "" + stock.getCouponDate();                  //bond next coupon date
            case 138:
                return "" + stock.getBondType();                   //bond bond type
            case 139:
                return "" + stock.getTradedThoughDate();           //bond traded though date
            case 140:
                return "" + stock.getRateCalcType();               //bond rate calc type
            case 141:
                return "" + stock.getDayCountMethod();             //bond day count method
            case 142:
                return "" + stock.getLoanNumber();                 //bond loan number
            case 143:
                return "" + stock.getExternalBondType();           //bond external type
            case 144:
                return "" + stock.getEarlyLateMaturity();          //bond early late maturity
            case 145:
                return decimalformat.format(stock.getMaturityValue());              //bond maturity value
            case 146:
                return decimalformat.format(stock.getYearlyCouponFreq());           //bond early coupon frequancy
            case 147:
                return "" + stock.getRecordDateType();             //bond record date type
            case 148:
                return "" + stock.getNumberOfRecordDays();         //bond number of record days
            case 149:
                return "" + stock.getInterestFromDate();           //bond interest from date
            case 150:
                return "" + stock.getInterestDayType();            //bond interest day type
            case 151:
                return "" + stock.getPreviousCouponDate();         //bond previous coupon date
            case 152:
                return "" + stock.getLastCouponDate();             //bond last coupon date
            case 153:
                return "" + stock.getCouponRecordDate();           //bond coupon record date
            case 154:
                return "" + stock.getFaceValueDimension();         //bond face value dimension
            case 155:
                return "" + stock.getNumberOfCashFlows();          //bond number of cash flows
            case 156:
                return decimalformat.format(stock.getCouponFrequancy());            //bond coupon frequancy
            case 157:
                return "" + stock.getDaysToFirstCashFlow();        //bond days to first cashflow
            case 158:
                return "" + stock.getDaysInFirstRecord();          //bond days in first period(record)
            case 159:
                return decimalformat.format(stock.getIndexCouponAdjustments());     //bond index coupon adjustment
            case 160:
                return decimalformat.format(stock.getAccuredInterest());            //bond accured interest
            case 161:
                return decimalformat.format(stock.getIndexFactor());                //bond index factor
            case 162:
                return "" + stock.getCalcConvention();             //bond calc convention
            case 163:
                return "" + stock.getBenchmark();                  //MF benchmark
            case 164:
                return "" + stock.getDeadline();                   //MF dealing dead line
            case 165:
                return decimalformat.format(stock.getMgtFee());                     //MF management fee
            case 166:
                return "" + stock.getFundName();                   //MF fund name
            case 167:
                return decimalformat.format(stock.getBenchmarkIndVal());            //MF benchmark ind value
            case 168:
                return decimalformat.format(stock.getFaceValue());                  //bond face value\
            case 169:
                return decimalformat.format(stock.getYTDStockValue());
            case 170:
                return "" + stock.getGroupName();
            case 171:
                return "" + stock.getGroupStatus();
            case 172:
                return "" + stock.getWeekAvgPrice();
            case 173:
                return "" + stock.getMonthAvgPrice();
            case 174:
                return "" + stock.getIntrinsicValue();
            /*if(stock.getInstrumentType() == Constants.TRADABLE_RIGHTS){
            return decimalformat.format(stock.getIntrinsicValue());
            }else{
                return decimalformat.format(-0d);

            }*/
            case 175:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getClosingVWAP();
                else
                    return "" + Double.NaN;
            case 176:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getTWAP();
                else
                    return "" + Double.NaN;
                /*case 177:
          if (ExchangeStore.isValidIinformationType(stock.getExchange(),Meta.IT_TWAP_ClosingVWAP))
              return "" + stock.getPeriodicIndicator().getEndOfDayIndicator();
          else
              return ""+ Double.NaN;*/
            case 177:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
//                    return "" + stock.getPeriodicIndicator().getPeriodStart();
                    String time = null;
                    try {
                        time = timeFormat.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), (new TWDateFormat("DDMMYYYYHHmmss").parse(new TWDateFormat("DDMMYYYY").format(new Date()) + stock.getPeriodicIndicator().getPeriodStart()).getTime()))));
                    } catch (ParseException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }

                    return "" + stringTransferObject.setValue(time);
                } else
                    return "" + Double.NaN;
            case 178:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP)) {
//                    return "" + stock.getPeriodicIndicator().getPeriodStop();
                    String time = null;
                    try {
                        time = timeFormat.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), (new TWDateFormat("DDMMYYYYHHmmss").parse(new TWDateFormat("DDMMYYYY").format(new Date()) + stock.getPeriodicIndicator().getPeriodStop()).getTime()))));
                    } catch (ParseException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }

                    return "" + stringTransferObject.setValue(time);

                } else
                    return "" + Double.NaN;
            case 179:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTWAS();
                else
                    return "" + Double.NaN;
            case 180:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getRTWAS();
                else
                    return "" + Double.NaN;
            case 181:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTWABBid();
                else
                    return "" + Double.NaN;
            case 182:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTWABAsk();
                else
                    return "" + Double.NaN;
            case 183:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVWAQ();
                else
                    return "" + Double.NaN;
            case 184:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOBAskVolume();
                else
                    return "" + Double.NaN;
            case 185:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOBAskValue();
                else
                    return "" + Double.NaN;
            case 186:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTNOBAsk();
                else
                    return "" + Double.NaN;
            case 187:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOAAskVolume();
                else
                    return "" + Double.NaN;
            case 188:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOAAskValue();
                else
                    return "" + Double.NaN;
            case 189:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTNOAAsk();
                else
                    return "" + Double.NaN;
            case 190:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOBBidVolume();
                else
                    return "" + Double.NaN;
            case 191:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOBBidValue();
                else
                    return "" + Double.NaN;
            case 192:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTNOBBid();
                else
                    return "" + Double.NaN;
            case 193:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOABidVolume();
                else
                    return "" + Double.NaN;
            case 194:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOABidValue();
                else
                    return "" + Double.NaN;
            case 195:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTNOABid();
                else
                    return "" + Double.NaN;
            case 196:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOBVolume();
                else
                    return "" + Double.NaN;
            case 197:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOBValue();
                else
                    return "" + Double.NaN;
            case 198:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTNOB();
                else
                    return "" + Double.NaN;
            case 199:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOAVolume();
                else
                    return "" + Double.NaN;
            case 200:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTVOAValue();
                else
                    return "" + Double.NaN;
            case 201:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTNOA();
                else
                    return "" + Double.NaN;
            case 202:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTotalOrderCoverage();
                else
                    return "" + Double.NaN;
            case 203:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getBidOrderCoverage();
                else
                    return "" + Double.NaN;
            case 204:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getAskOrderCoverage();
                else
                    return "" + Double.NaN;
            case 205:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getBEVWAPBid();
                else
                    return "" + Double.NaN;
            case 206:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getBEVWAPAsk();
                else
                    return "" + Double.NaN;
            case 207:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTotalVolumeBid();
                else
                    return "" + Double.NaN;
            case 208:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getTotalVolumeAsk();
                else
                    return "" + Double.NaN;
            case 209:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getBAIBest();
                else
                    return "" + Double.NaN;
            case 210:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getBAIAll();
                else
                    return "" + Double.NaN;
            case 211:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getCIBid();
                else
                    return "" + Double.NaN;
            case 212:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return "" + stock.getPeriodicIndicator().getCIAsk();
                else
                    return "" + Double.NaN;

            case 213:
                if (ExchangeStore.isValidIinformationType(stock.getExchange(), Meta.IT_TWAP_ClosingVWAP) || true)
                    return "" + stock.getTodaysClose();
                else
                    return "" + Double.NaN;
            case 214:
                return "" + stock.getLoosingCategory();
            case 215:
                return decimalformat.format(stock.getTop());
            case 216:
                return "" + stock.getTov();
            case 217:
                return decimalformat.format(stock.getTcp());
            case 218:
                return "" + stock.getTcv();
            case 219:
                return decimalformat.format(stock.getShareCapital());
            case 220:
                return decimalformat.format(stock.getCurrentAdjstVal());
            case 221:
                return decimalformat.format(stock.getStaticMax());
            case 222:
                return decimalformat.format(stock.getStaticMin());


                /*  case 214:
          if (ExchangeStore.isValidIinformationType(stock.getExchange(),Meta.IT_TWAP_ClosingVWAP))
              return "" + stock.getPeriodicIndicator().getLocalRoundTripClasses();
          else
              return ""+ Double.NaN;
      case 215:
          if (ExchangeStore.isValidIinformationType(stock.getExchange(),Meta.IT_TWAP_ClosingVWAP))
              return "" + stock.getPeriodicIndicator().getLocalRoundTripCosts();
          else
              return ""+ Double.NaN;
      case 216:
          if (ExchangeStore.isValidIinformationType(stock.getExchange(),Meta.IT_TWAP_ClosingVWAP))
              return "" + stock.getPeriodicIndicator().getLocalRoundTripCoverages();
          else
              return ""+ Double.NaN;
      case 217:
          if (ExchangeStore.isValidIinformationType(stock.getExchange(),Meta.IT_TWAP_ClosingVWAP))
              return "" + stock.getPeriodicIndicator().getReferenceRoundTripClasses();
          else
              return ""+ Double.NaN;
      case 218:
          if (ExchangeStore.isValidIinformationType(stock.getExchange(),Meta.IT_TWAP_ClosingVWAP))
              return "" + stock.getPeriodicIndicator().getReferenceRoundTripCosts();
          else
              return ""+ Double.NaN;
      case 219:
          if (ExchangeStore.isValidIinformationType(stock.getExchange(),Meta.IT_TWAP_ClosingVWAP))
              return "" + stock.getPeriodicIndicator().getReferenceRoundTripCoverages();
          else
              return ""+ Double.NaN;*/

            default:
                return null;
        }
    }

    public static int compare(Object o1, Object o2, int sortColumn, int sortOrder) {
        Stock stock1 = (Stock) o1;
        Stock stock2 = (Stock) o2;

        switch (sortColumn) {
            case 0:
                return compareValues(stock1.getSymbol(), stock2.getSymbol(), sortOrder);
            case 1:
//                if((Settings.isShowAlias())){
//                    if((stock1.getAlias() != null) && (stock2.getAlias() != null)){
//                        return compareValues(stock1.getAlias(), stock2.getAlias(), sortOrder);
//                    } else if(stock1.getAlias() == null){
//                        return compareValues(stock1.getShortDescription(), stock2.getAlias(), sortOrder);
//                    } else if(stock2.getAlias() == null){
//                        return compareValues(stock1.getAlias(), stock2.getShortDescription(), sortOrder);
//                    } else{
//                        return compareValues(stock1.getShortDescription(), stock2.getShortDescription(), sortOrder);
//                    }
//                }else
                return compareValues(stock1.getShortDescription(), stock2.getShortDescription(), sortOrder);
            case 2:
                return compareValues(stock1.getLongDescription(), stock2.getLongDescription(), sortOrder);

            /* Trade related */
            case 3:
                return compareValues(stock1.getLastTradeTime(), stock2.getLastTradeTime(), sortOrder);
            case 4:
                return compareValues(stock1.getLastTradeValue(), stock2.getLastTradeValue(), sortOrder);
            case 5:
                return compareValues(stock1.getTradeQuantity(), stock2.getTradeQuantity(), sortOrder);
            case 6:
                return compareValues(stock1.getTradeTick(), stock2.getTradeTick(), sortOrder);
            case 7:
                return compareValues(stock1.getChange(), stock2.getChange(), sortOrder);
            case 8:
                return compareValues(stock1.getPercentChange(), stock2.getPercentChange(), sortOrder);
            case 9:
                return compareValues(stock1.getAvgTradePrice(), stock2.getAvgTradePrice(), sortOrder);
            case 10:
                return compareValues(stock1.getVolume(), stock2.getVolume(), sortOrder);
            case 11:
                return compareValues(stock1.getTurnover(), stock2.getTurnover(), sortOrder);
            case 12:
                return compareValues(stock1.getNoOfTrades(), stock2.getNoOfTrades(), sortOrder);
            case 13:
                return compareValues(stock1.getAvgTradeVolume(), stock2.getAvgTradeVolume(), sortOrder);
            case 14:
                return compareValues(stock1.getMinPrice(), stock2.getMinPrice(), sortOrder);
            case 15:
                return compareValues(stock1.getMaxPrice(), stock2.getMaxPrice(), sortOrder);
            case 16:
                return compareValues(stock1.getTodaysOpen(), stock2.getTodaysOpen(), sortOrder);
            case 17:
                return compareValues(stock1.getHigh(), stock2.getHigh(), sortOrder);
            case 18:
                return compareValues(stock1.getLow(), stock2.getLow(), sortOrder);
            case 19:
                return compareValues(stock1.getPreviousClosed(), stock2.getPreviousClosed(), sortOrder);

            /* Bid related */
            case 20:
                return compareValues(stock1.getBestBidPrice(), stock2.getBestBidPrice(), sortOrder);
            case 21:
                return compareValues(stock1.getBestBidQuantity(), stock2.getBestBidQuantity(), sortOrder);

            /* Ask related */
            case 22:
                return compareValues(stock1.getBestAskPrice(), stock2.getBestAskPrice(), sortOrder);
            case 23:
                return compareValues(stock1.getBestAskQuantity(), stock2.getBestAskQuantity(), sortOrder);

            /* Symbol Related */
            case 24:
                return compareValues(stock1.getMarketCap(), stock2.getMarketCap(), sortOrder);
            case 25:
                return compareValues(stock1.getPER(), stock2.getPER(), sortOrder);
            case 26:
                return compareValues(stock1.getPBR(), stock2.getPBR(), sortOrder);
            case 27:
                return compareValues(stock1.getYield(), stock2.getYield(), sortOrder);
            case 28:
                return compareValues(stock1.getHighPriceOf52Weeks(), stock2.getHighPriceOf52Weeks(), sortOrder);
            case 29:
                return compareValues(stock1.getLowPriceOf52Weeks(), stock2.getLowPriceOf52Weeks(), sortOrder);
            case 30:
                return compareValues(stock1.getLastTradedDate(), stock2.getLastTradedDate(), sortOrder);
            case 31:
                return compareValues(stock1.getLastTradedPrice(), stock2.getLastTradedPrice(), sortOrder);
            case 32:
                return compareValues(stock1.getTotalBidQty(), stock2.getTotalBidQty(), sortOrder);
            case 33:
                return compareValues(stock1.getNoOfBids(), stock2.getNoOfBids(), sortOrder);
            case 34:
                return compareValues(stock1.getTotalAskQty(), stock2.getTotalAskQty(), sortOrder);
            case 35:
                return compareValues(stock1.getNoOfAsks(), stock2.getNoOfAsks(), sortOrder);
            case 36:
                return compareBidAskValues(stock1.getBidaskRatio(), stock2.getBidaskRatio(), sortOrder);
            case 37:
                return compareValues(stock1.getSpread(), stock2.getSpread(), sortOrder);
            case 38:
                return compareValues(stock1.getRange(), stock2.getRange(), sortOrder);
            case 39:
                return compareValues(stock1.getPctSpread(), stock2.getPctSpread(), sortOrder);
            case 40:
                return compareValues(stock1.getPctRange(), stock2.getPctRange(), sortOrder);
            case 41:
                return compareBidAskValues(stock1.getBidaskRatio(), stock2.getBidaskRatio(), sortOrder);
            case 42:
                return compareValues(stock1.getPctSpread(), stock2.getPctSpread(), sortOrder);
            case 43:
                return compareValues(stock1.getPctRange(), stock2.getPctRange(), sortOrder);
            case 44:
                return compareValues(stock1.getPercentChange(), stock2.getPercentChange(), sortOrder);
            case 45:
                return compareValues(stock1.getTradeTick(), stock2.getTradeTick(), sortOrder);
            case 46:
                return compareValues(ExchangeStore.getSharedInstance().getExchange(stock1.getExchange()).getDisplayExchange(),
                        ExchangeStore.getSharedInstance().getExchange(stock2.getExchange()).getDisplayExchange(), sortOrder);
            case 47:
                return compareValues(stock1.getMinOrMax(), stock2.getMinOrMax(), sortOrder);
            case 48:
                return compareValues(stock1.getBulbStatus(), stock2.getBulbStatus(), sortOrder);
            case 49:
                return compareValues(stock1.getTimeOffset(), stock2.getTimeOffset(), sortOrder);
            case 56:
                return compareValues(stock1.getCurrencyCode(), stock2.getCurrencyCode(), sortOrder);
            case 57:
                return compareValues(stock1.getTopTcpValue(), stock2.getTopTcpValue(), sortOrder);
            case 58:
                return compareValues(stock1.getHighAsk(), stock2.getHighAsk(), sortOrder);
            case 59:
                return compareValues(stock1.getLowBid(), stock2.getLowBid(), sortOrder);
            case 60:
                return compareValues(stock1.getSimpleAverageAsk(), stock2.getSimpleAverageAsk(), sortOrder);
            case 61:
                return compareValues(stock1.getWAverageAsk(), stock2.getWAverageAsk(), sortOrder);
            case 62:
                return compareValues(stock1.getSimpleAverageBid(), stock2.getSimpleAverageBid(), sortOrder);
            case 63:
                return compareValues(stock1.getWAverageBid(), stock2.getWAverageBid(), sortOrder);
            case 64:
                return compareValues(stock1.getLastAskPrice(), stock2.getLastAskPrice(), sortOrder);
            case 65:
                return compareValues(stock1.getLastAskQuantity(), stock2.getLastAskQuantity(), sortOrder);
            case 66:
                return compareValues(stock1.getLastBidPrice(), stock2.getLastBidPrice(), sortOrder);
            case 67:
                return compareValues(stock1.getLastBidQuantity(), stock2.getLastBidQuantity(), sortOrder);
            case 68:
                return compareValues(stock1.getSimpleMovingAverage(), stock2.getSimpleMovingAverage(), sortOrder);
            case 69:
                return compareValues(stock1.getExponentialMovingAverage(), stock2.getExponentialMovingAverage(), sortOrder);
            case 70:
                return compareValues(stock1.getWeightedMovingAverage(), stock2.getWeightedMovingAverage(), sortOrder);
            case 71:
                return compareValues(stock1.getMACD(), stock2.getMACD(), sortOrder);
            case 72:
                return compareValues(stock1.getIMI(), stock2.getIMI(), sortOrder);
            case 73:
                return compareValues(stock1.getRSI(), stock2.getRSI(), sortOrder);
            case 74:
                return compareValues(stock1.getROC(), stock2.getROC(), sortOrder);
            case 75:
                return compareValues(stock1.getFastStochastic(), stock2.getFastStochastic(), sortOrder);
            case 76:
                return compareValues(stock1.getSlowStochastic(), stock2.getSlowStochastic(), sortOrder);
            case 77:
                return compareValues(stock1.getBollingerMiddle(), stock2.getBollingerMiddle(), sortOrder);
            case 78:
                return compareValues(stock1.getBollingerUpper(), stock2.getBollingerUpper(), sortOrder);
            case 79:
                return compareValues(stock1.getBollingerLower(), stock2.getBollingerLower(), sortOrder);
            case 80:
                return compareValues(stock1.getChaikinOsc(), stock2.getChaikinOsc(), sortOrder);
            case 81:
                return compareComanyCodeValues(stock1.getCompanyCode(), stock2.getCompanyCode(), sortOrder);
//                return compareValues(stock1.getCompanyCode(), stock2.getCompanyCode(), sortOrder);
            case 82:
                return compareValues((ExchangeStore.getSharedInstance().getMarket(stock1.getExchange(), stock1.getMarketID())).getDescription(),
                        (ExchangeStore.getSharedInstance().getMarket(stock2.getExchange(), stock2.getMarketID())).getDescription(), sortOrder);
            case 83:
                return compareValues(SectorStore.getSharedInstance().getSector(stock1.getExchange(), stock1.getSectorCode()).getDescription(),
                        SectorStore.getSharedInstance().getSector(stock2.getExchange(), stock2.getSectorCode()).getDescription(), sortOrder);
            case 84:
                return compareValues(stock1.getSharesForForeigners(), stock2.getSharesForForeigners(), sortOrder);
            case 85:
                return compareValues(stock1.getISINCode(), stock2.getISINCode(), sortOrder);
            case 86:
                return compareValues(stock1.getNetAssetValue(), stock2.getNetAssetValue(), sortOrder); // NAV
            case 87:
                return compareValues(stock1.getFundValue(), stock2.getFundValue(), sortOrder); // Mutual Fund total value
            case 88:
                return compareValues(stock1.getPerformanceYTD(), stock2.getPerformanceYTD(), sortOrder);
            case 89:
                return compareValues(stock1.getPerformance12M(), stock2.getPerformance12M(), sortOrder);
            case 90:
                return compareValues(stock1.getPerformance3Y(), stock2.getPerformance3Y(), sortOrder);
            case 91:
                return compareValues(stock1.getPerformance5Y(), stock2.getPerformance5Y(), sortOrder);
            case 92:
                return 0;
            case 93:
                return compareValues(stock1.getCashInOrders(), stock2.getCashInOrders(), sortOrder);
            case 94:
                return compareValues(stock1.getCashInVolume(), stock2.getCashInVolume(), sortOrder);
            case 95:
                return compareValues(stock1.getCashInTurnover(), stock2.getCashInTurnover(), sortOrder);
            case 96:
                return compareValues(stock1.getCashOutOrders(), stock2.getCashOutOrders(), sortOrder);
            case 97:
                return compareValues(stock1.getCashOutVolume(), stock2.getCashOutVolume(), sortOrder);
            case 98:
                return compareValues(stock1.getCashOutTurnover(), stock2.getCashOutTurnover(), sortOrder);
            case 99:
                return compareValues(stock1.getCashFlowNet(), stock2.getCashFlowNet(), sortOrder);
            case 100:
                return compareValues(stock1.getCashFlowPct(), stock2.getCashFlowPct(), sortOrder);
            case 101:
                return compareValues(stock1.getCashFlowRatio(), stock2.getCashFlowRatio(), sortOrder);
            case 102:
                return compareValues(stock1.getLotSize(), stock2.getLotSize(), sortOrder);
            case 103:
                return compareValues(stock1.getUnit(), stock2.getUnit(), sortOrder);
            case 104:
                return compareValues(stock1.getListedShares(), stock2.getListedShares(), sortOrder);
            case 105:
                return compareValues(stock1.getEPS(), stock2.getEPS(), sortOrder);
            case 106:
                return compareValues(stock1.getForexBuyDate(), stock2.getForexBuyDate(), sortOrder);
            case 107:
                return compareValues(stock1.getForexSellDate(), stock2.getForexSellDate(), sortOrder);

            //new mutual funds
//           case 108:
//                 return compareValues(stock1.getDescription(),stock1.getDescription(),sortOrder);
//            case 109:
//                 return compareValues(stock1.getFundName(),stock1.getFundName(),sortOrder);
            case 108:
                return compareValues(stock1.getAmc(), stock2.getAmc(), sortOrder);
            case 109:
                return compareValues(stock1.getCountry(), stock2.getCountry(), sortOrder);
            case 110:
                return compareValues(stock1.getRisk1Y(), stock2.getRisk1Y(), sortOrder);
            case 111:
                return compareValues(stock1.getMinInitPurchase(), stock2.getMinInitPurchase(), sortOrder);
            case 112:
                return compareValues(stock1.getFEndLoad(), stock2.getFEndLoad(), sortOrder);
            case 113:
                return compareValues(stock1.getBEndLoad(), stock2.getBEndLoad(), sortOrder);
            case 114:
                return compareValues(stock1.getFundsize(), stock2.getFundsize(), sortOrder);
            case 115:
                return compareValues(stock1.getSubsPurchase(), stock2.getSubsPurchase(), sortOrder);
            case 116:
                return compareValues(stock1.getTotAsset(), stock2.getTotAsset(), sortOrder);
            case 117:
                return compareValues(stock1.getTotAssetDate(), stock2.getTotAssetDate(), sortOrder);
            /*case 108:
                 return compareValues(stock1.getForexBlockSize(),stock2.getForexBlockSize(),sortOrder);*/
            /*case 109:
                 return compareValues(stock1.getForexBaseValue(),stock2.getForexBaseValue(),sortOrder);*/
            /*case 118:
                 return compareValues(stock1.get52WeekHighLowTracker(),stock1.get52WeekHighLowTracker(),sortOrder);*/
            case 118:
                return compareValues(stock1.getAvgVolume(getVolumeWatcherMode()), stock2.getAvgVolume(getVolumeWatcherMode()), sortOrder);
            case 119:
                return compareValues(stock1.getPctVolume(getVolumeWatcherMode()), stock2.getPctVolume(getVolumeWatcherMode()), sortOrder);
            case 120:
                return compareValues(stock1.get7DVolume(), stock2.get7DVolume(), sortOrder);
            case 121:
                return compareValues(stock1.get30DVolume(), stock2.get30DVolume(), sortOrder);
            case 122:
                return compareValues(stock1.get90DVolume(), stock2.get90DVolume(), sortOrder);
            case 123:
                return compareValues(stock1.get7DVolumeChange(), stock2.get7DVolumeChange(), sortOrder);
            case 124:
                return compareValues(stock1.get30DVolumeChange(), stock2.get30DVolumeChange(), sortOrder);
            case 125:
                return compareValues(stock1.get90DVolumeChange(), stock2.get90DVolumeChange(), sortOrder);
            case 126:
                return compareValues(stock1.getChgVWAPBase(), stock2.getChgVWAPBase(), sortOrder);
            case 127:
                return compareValues(stock1.getPercentageChgVWAPBase(), stock2.getPercentageChgVWAPBase(), sortOrder);

            //related to BOND Market
            //Added by chandika 2009-07-01
            case 128:
                return compareValues(stock1.getSettlementDate(), stock2.getSettlementDate(), sortOrder);                     //bond settlement date
            case 129:
                return compareValues(stock1.getIssuedAmount(), stock2.getIssuedAmount(), sortOrder);                         //bond issued amount
            case 130:
                return compareValues(stock1.getDateofIssue(), stock2.getDateofIssue(), sortOrder);                           //bond Date of issue
            case 131:
                return compareValues(stock1.getOutstandingAmount(), stock2.getOutstandingAmount(), sortOrder);               //bond outstanding amount
            case 132:
                return compareValues(stock1.getMatuarityDate(), stock2.getMatuarityDate(), sortOrder);                       //bond matuarity date
            case 133:
                return compareValues(stock1.getCouponRate(), stock2.getCouponRate(), sortOrder);                             //bond coupon rate
            case 134:
                return compareValues(stock1.getNominalValue(), stock2.getNominalValue(), sortOrder);                         //bond nominal value
            case 135:
                return compareValues(stock1.getIssueCurrency(), stock2.getIssueCurrency(), sortOrder);                       //bond issue currency

            case 136:
                return compareValues(stock1.getListingDate(), stock2.getListingDate(), sortOrder);                           //bond listing date
            case 137:
                return compareValues(stock1.getListingDate(), stock2.getListingDate(), sortOrder);                           //bond next coupon date
            case 138:
                return compareValues(stock1.getBondType(), stock2.getBondType(), sortOrder);                                 //bond bond type
            case 139:
                return compareValues(stock1.getTradedThoughDate(), stock2.getTradedThoughDate(), sortOrder);                 //bond traded though date
            case 140:
                return compareValues(stock1.getRateCalcType(), stock2.getRateCalcType(), sortOrder);                         //bond rate calc type
            case 141:
                return compareValues(stock1.getDayCountMethod(), stock2.getDayCountMethod(), sortOrder);                     //bond day count method
            case 142:
                return compareValues(stock1.getLoanNumber(), stock2.getLoanNumber(), sortOrder);                             //bond loan number
            case 143:
                return compareValues(stock1.getExternalBondType(), stock2.getExternalBondType(), sortOrder);                 //bond external type
            case 144:
                return compareValues(stock1.getEarlyLateMaturity(), stock2.getEarlyLateMaturity(), sortOrder);               //bond early late maturity
            case 145:
                return compareValues(stock1.getMaturityValue(), stock2.getMaturityValue(), sortOrder);                       //bond maturity value
            case 146:
                return compareValues(stock1.getYearlyCouponFreq(), stock2.getYearlyCouponFreq(), sortOrder);                 //bond early coupon frequancy
            case 147:
                return compareValues(stock1.getRecordDateType(), stock2.getRecordDateType(), sortOrder);                     //bond record date type
            case 148:
                return compareValues(stock1.getNumberOfRecordDays(), stock2.getNumberOfRecordDays(), sortOrder);             //bond number of record days
            case 149:
                return compareValues(stock1.getInterestFromDate(), stock2.getInterestFromDate(), sortOrder);                 //bond interest from date
            case 150:
                return compareValues(stock1.getInterestDayType(), stock2.getInterestDayType(), sortOrder);                   //bond interest day type
            case 151:
                return compareValues(stock1.getPreviousCouponDate(), stock2.getPreviousCouponDate(), sortOrder);             //bond previous coupon date
            case 152:
                return compareValues(stock1.getLastCouponDate(), stock2.getLastCouponDate(), sortOrder);                     //bond last coupon date
            case 153:
                return compareValues(stock1.getCouponRecordDate(), stock2.getCouponRecordDate(), sortOrder);                 //bond coupon record date
            case 154:
                return compareValues(stock1.getFaceValueDimension(), stock2.getFaceValueDimension(), sortOrder);             //bond face value dimension
            case 155:
                return compareValues(stock1.getNumberOfCashFlows(), stock2.getNumberOfCashFlows(), sortOrder);               //bond number of cash flows
            case 156:
                return compareValues(stock1.getCouponFrequancy(), stock2.getCouponFrequancy(), sortOrder);                   //bond coupon frequancy
            case 157:
                return compareValues(stock1.getDaysToFirstCashFlow(), stock2.getDaysToFirstCashFlow(), sortOrder);           //bond days to first cashflow
            case 158:
                return compareValues(stock1.getDaysInFirstRecord(), stock2.getDaysInFirstRecord(), sortOrder);               //bond days in first period(record)
            case 159:
                return compareValues(stock1.getIndexCouponAdjustments(), stock2.getIndexCouponAdjustments(), sortOrder);     //bond index coupon adjustment
            case 160:
                return compareValues(stock1.getAccuredInterest(), stock2.getIndexCouponAdjustments(), sortOrder);            //bond accured interest
            case 161:
                return compareValues(stock1.getIndexFactor(), stock2.getIndexCouponAdjustments(), sortOrder);                //bond index factor
            case 162:
                return compareValues(stock1.getCalcConvention(), stock2.getIndexCouponAdjustments(), sortOrder);             //bond calc convention
            case 163:
                return compareValues(stock1.getBenchmark(), stock2.getBenchmark(), sortOrder);                               //MF benchmark
            case 164:
                return compareValues(stock1.getDeadline(), stock2.getDeadline(), sortOrder);                                 //MF dealing dead line
            case 165:
                return compareValues(stock1.getMgtFee(), stock2.getMgtFee(), sortOrder);                                     //MF management fee
            case 166:
                return compareValues(stock1.getFundName(), stock2.getFundName(), sortOrder);                                 //MF fund name
            case 167:
                return compareValues(stock1.getBenchmarkIndVal(), stock2.getBenchmarkIndVal(), sortOrder);                   //MF benchmark ind value
            case 168:
                return compareValues(stock1.getFaceValue(), stock2.getFaceValue(), sortOrder);                               //bond face value
            case 169:
                return compareValues(stock1.getYTDStockValue(), stock2.getYTDStockValue(), sortOrder);
            case 170:
                return compareValues(stock1.getGroupName(), stock2.getGroupName(), sortOrder);
            case 171:
                return compareValues(stock1.getGroupStatus(), stock2.getGroupStatus(), sortOrder);
            case 172:
                return compareValues(stock1.getWeekAvgPrice(), stock2.getWeekAvgPrice(), sortOrder);
            case 173:
                return compareValues(stock1.getMonthAvgPrice(), stock2.getMonthAvgPrice(), sortOrder);
            case 174:
                return compareValues(stock1.getIntrinsicValue(), stock2.getIntrinsicValue(), sortOrder);
            case 175:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getClosingVWAP(), stock2.getClosingVWAP(), sortOrder);
            case 176:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getTWAP(), stock2.getTWAP(), sortOrder);
                /*case 178:
         if (ExchangeStore.isValidIinformationType(stock1.getExchange(),Meta.IT_TWAP_ClosingVWAP))
             return compareValues(stock1.getPeriodicIndicator().getEndOfDayIndicator(), stock2.getPeriodicIndicator().getEndOfDayIndicator(), sortOrder);*/
            case 177:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getPeriodStart(), stock2.getPeriodicIndicator().getPeriodStart(), sortOrder);
            case 178:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getPeriodStop(), stock2.getPeriodicIndicator().getPeriodStop(), sortOrder);
            case 179:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTWAS(), stock2.getPeriodicIndicator().getTWAS(), sortOrder);
            case 180:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getRTWAS(), stock2.getPeriodicIndicator().getRTWAS(), sortOrder);
            case 181:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTWABBid(), stock2.getPeriodicIndicator().getTWABBid(), sortOrder);
            case 182:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTWABAsk(), stock2.getPeriodicIndicator().getTWABAsk(), sortOrder);
            case 183:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVWAQ(), stock2.getPeriodicIndicator().getTVWAQ(), sortOrder);
            case 184:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOBAskVolume(), stock2.getPeriodicIndicator().getTVOBAskVolume(), sortOrder);
            case 185:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOBAskValue(), stock2.getPeriodicIndicator().getTVOBAskValue(), sortOrder);
            case 186:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTNOBAsk(), stock2.getPeriodicIndicator().getTNOBAsk(), sortOrder);
            case 187:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOAAskVolume(), stock2.getPeriodicIndicator().getTVOAAskVolume(), sortOrder);
            case 188:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOAAskValue(), stock2.getPeriodicIndicator().getTVOAAskValue(), sortOrder);
            case 189:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTNOAAsk(), stock2.getPeriodicIndicator().getTNOAAsk(), sortOrder);
            case 190:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOBBidVolume(), stock2.getPeriodicIndicator().getTVOBBidVolume(), sortOrder);
            case 191:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOBBidValue(), stock2.getPeriodicIndicator().getTVOBBidValue(), sortOrder);
            case 192:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTNOBBid(), stock2.getPeriodicIndicator().getTNOBBid(), sortOrder);
            case 193:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOABidVolume(), stock2.getPeriodicIndicator().getTVOABidVolume(), sortOrder);
            case 194:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOABidValue(), stock2.getPeriodicIndicator().getTVOABidValue(), sortOrder);
            case 195:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTNOABid(), stock2.getPeriodicIndicator().getTNOABid(), sortOrder);
            case 196:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOBVolume(), stock2.getPeriodicIndicator().getTVOBVolume(), sortOrder);
            case 197:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOBValue(), stock2.getPeriodicIndicator().getTVOBValue(), sortOrder);
            case 198:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTNOB(), stock2.getPeriodicIndicator().getTNOB(), sortOrder);
            case 199:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOAVolume(), stock2.getPeriodicIndicator().getTVOAVolume(), sortOrder);
            case 200:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTVOAValue(), stock2.getPeriodicIndicator().getTVOAValue(), sortOrder);
            case 201:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTNOA(), stock2.getPeriodicIndicator().getTNOA(), sortOrder);
            case 202:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTotalOrderCoverage(), stock2.getPeriodicIndicator().getTotalOrderCoverage(), sortOrder);
            case 203:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getBidOrderCoverage(), stock2.getPeriodicIndicator().getBidOrderCoverage(), sortOrder);
            case 204:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getAskOrderCoverage(), stock2.getPeriodicIndicator().getAskOrderCoverage(), sortOrder);
            case 205:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getBEVWAPBid(), stock2.getPeriodicIndicator().getBEVWAPBid(), sortOrder);
            case 206:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getBEVWAPAsk(), stock2.getPeriodicIndicator().getBEVWAPAsk(), sortOrder);
            case 207:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTotalVolumeBid(), stock2.getPeriodicIndicator().getTotalVolumeBid(), sortOrder);
            case 208:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getTotalVolumeAsk(), stock2.getPeriodicIndicator().getTotalVolumeAsk(), sortOrder);
            case 209:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getBAIBest(), stock2.getPeriodicIndicator().getBAIBest(), sortOrder);
            case 210:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getBAIAll(), stock2.getPeriodicIndicator().getBAIAll(), sortOrder);
            case 211:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getCIBid(), stock2.getPeriodicIndicator().getCIBid(), sortOrder);
            case 212:
                if (ExchangeStore.isValidIinformationType(stock1.getExchange(), Meta.IT_TWAP_ClosingVWAP))
                    return compareValues(stock1.getPeriodicIndicator().getCIAsk(), stock2.getPeriodicIndicator().getCIAsk(), sortOrder);
            case 213:
                return compareValues(stock1.getTodaysClose(), stock2.getTodaysClose(), sortOrder);
            case 214:
                return compareValues(stock1.getLoosingCategory(), stock2.getLoosingCategory(), sortOrder);
            case 215:
                return compareValues(stock1.getTop(), stock2.getTop(), sortOrder);
            case 216:
                return compareValues(stock1.getTov(), stock2.getTov(), sortOrder);
            case 217:
                return compareValues(stock1.getTcp(), stock2.getTcp(), sortOrder);
            case 218:
                return compareValues(stock1.getTcv(), stock2.getTcv(), sortOrder);
            case 219:
                return compareValues(stock1.getShareCapital(), stock2.getShareCapital(), sortOrder);
            case 220:
                return compareValues(stock1.getCurrentAdjstVal(), stock2.getCurrentAdjstVal(), sortOrder);
            case 221:
                return compareValues(stock1.getStaticMax(), stock2.getStaticMax(), sortOrder);
            case 222:
                return compareValues(stock1.getStaticMin(), stock2.getStaticMin(), sortOrder);
            default:
                return 0;
        }

    }

    private static int compareValues(int val1, int val2, int sortOrder) {
        return ((val1 - val2) * sortOrder);
    }

    private static int compareValues(long val1, long val2, int sortOrder) {
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

    private static int compareValues(double val1, double val2, int sortOrder) {
        if (Double.isNaN(val1) && Double.isNaN(val2)) {
            return 0;
        } else if (Double.isNaN(val1)) {
            return 1;
        } else if (Double.isNaN(val2)) {
            return -1;
        }
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

    private static int compareBidAskValues(double val1, double val2, int sortOrder) {
        if (Double.isNaN(val1)) {
            return -1;
        } else if (Double.isNaN(val2)) {
            return sortOrder * -1;
        }
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

    private static int compareValues(float val1, float val2, int sortOrder) {
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }

    private static int compareValues(String val1, String val2, int sortOrder) {
        return ((val1.toUpperCase()).compareTo((val2.toUpperCase())) * sortOrder);
    }

    private static int compareComanyCodeValues(String val1, String val2, int sortOrder) {
        String s1 = val1;
        String s2 = val2;
        double i2 = 0;
        double i1 = 0;
        boolean first = false;
        boolean second = false;
        try {
            i1 = Double.parseDouble(s1);
//                i1 = Integer.parseInt(s1);
            first = true;
        } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            i2 = Double.parseDouble(s2);
//                i2 = Integer.parseInt(s2);
            second = true;
        } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (first && second) {
            if (i1 < i2)
                return -1 * sortOrder;
            else if (i1 > i2)
                return sortOrder;
            else
                return 0;
        } else if (first) {
            return -1 * sortOrder;
        } else if (second) {
            return sortOrder;
        } else {
            int result = s1.compareTo(s2);
            if (result < 0)
                return -1 * sortOrder;
            else if (result > 0)
                return sortOrder;
            else
                return 0;
//                return s1.compareTo(s2);
        }
    }

    /*public static String getValue(String symbol, int column) {
        Stock stock = null;

        stock = DataStore.getSharedInstance().getStockObject(symbol);
        return getStringData(stock, column);
    }

    public static String getValue(String exchange, String symbol, int column) {
        Stock stock = null;

        stock = DataStore.getSharedInstance().getStockObject(exchange, symbol);
        return getStringData(stock, column);
    }*/

    public static String getValue(String symbol, int column) {
        Stock stock = null;

        stock = DataStore.getSharedInstance().getStockObject(symbol);
        if (getColumnCount() > column)
            return getStringData(stock, column);
        else {
            column = column - getColumnCount();
            return getCustomFormulaData(stock, column);
        }
    }

    public static String getValue(String exchange, String symbol, int column, int instrument) {
        Stock stock = null;
        String data;
        stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
        if (getColumnCount() > column) {
            data = getStringData(stock, column);
            if (data.equalsIgnoreCase("NaN")) {
                return Language.getString("NA");
            } else {
                return getStringData(stock, column);
            }
        } else {
            column = column - getColumnCount();
            return getCustomFormulaData(stock, column);
        }
    }

    private static void applyDecimalPlaces(int decimal) {

        switch (decimal) {
            case Constants.NO_DECIMAL_PLACES:
                decimalformat.applyPattern(Constants.PATTERN_NO_DECIMAL.trim());
                break;
            case Constants.ONE_DECIMAL_PLACES:
                decimalformat.applyPattern(Constants.PATTERN_ONE_DECIMAL_NOZERO.trim());
                break;
            case Constants.TWO_DECIMAL_PLACES:
                decimalformat.applyPattern(Constants.PATTERN_TWO_DECIMAL_NOZERO.trim());
                break;
            case Constants.THREE_DECIMAL_PLACES:
                decimalformat.applyPattern(Constants.PATTERN_THREE_DECIMAL_NOZERO.trim());
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                decimalformat.applyPattern(Constants.PATTERN_FOUR_DECIMAL_NOZERO.trim());
                break;
            default:
                decimalformat.applyPattern(Constants.PATTERN_FOUR_DECIMAL_NOZERO.trim());
                break;
        }
    }

    private static String getCustomFormulaData(Stock stock, int column) {
        return "" + (CustomFormulaStore.getSharedInstance().getColumnValue(stock, column)).getValue();
    }

    public static boolean isNumericData(int column) {
        switch (column) {
            case -3:
            case -2:
            case -1:
            case 0:
            case 1:
            case 2:
            case 3:
            case 30:
            case 36:
            case 37:
            case 39:
            case 41:
            case 42:
            case 46:
            case 51:
            case 53:
            case 56:
            case 81:
            case 82:
            case 83:
            case 85:
            case 92:
            case 103:
            case 106:
            case 107:
            case 108:
            case 109:
                return false;
            default:
                return true;
        }
    }


    public static boolean isDynamicData(int column) {
        switch (column) {
            case 0:
            case 1:
            case 2:
                return false;
            default:
                return true;
        }
    }

    private static int getColumnCount() {
        if (COLUMN_COUNT == 0) {
            COLUMN_COUNT = Language.getList("TABLE_COLUMNS").length;
        }
        return COLUMN_COUNT;
    }

    public static int getVolumeWatcherMode() {
        return volumeWatcherMode;
    }

    public static void setVolumeWatcherMode(int volumeWatcherMode) {
        StockData.volumeWatcherMode = volumeWatcherMode;
    }
}