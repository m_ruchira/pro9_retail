// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.browser;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.text.Style;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A Class class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class BrowserPane extends JPanel {// implements Themeable {
    // private static JEditorPane editorPane;
    //private static JTextArea editorPane;
    // private static JTextPane editorPane;
    private static JLabel editorPane;
    //private static URL url;
    //private int iCurrentPos=0;
    //private static NavigatorPanel navPanel;
    //private static URL homeUrl;
    //private static URL currentURL;
    //private static String sHome;
    //private static String sOfflineURL;
    //private static URL offlineURL;
    //private static SList urlList;
    //private static int pos;
    //private static int size;
    //private static LoadPageThread pageLoader;
    //private static Thread pageLoaderThread;
    private JInternalFrame parent;
    private Style style;


    /**
     * Constructor
     */
    public BrowserPane(JInternalFrame parent) {
        this.parent = parent;
        //urlList = new SList();

        //creating HTML window
        //editorPane = new JEditorPane();
        editorPane = new JLabel();
        //editorPane.setFont(new TWFont("Arial",0,14));
        //editorPane = new JTextArea();
        /*if(!Language.isLTR()){
            GUISettings.applyOrientation(editorPane,ComponentOrientation.RIGHT_TO_LEFT);
        }*/
        //editorPane.setAlignmentX(SwingConstants.LEADING);
        editorPane.setAlignmentY(SwingConstants.TOP);
        //editorPane.setWrapStyleWord(true);
        //editorPane.setLineWrap(true);
        //editorPane.setEditable(false);

        //-BrowserURLHandler urlHandler = new  BrowserURLHandler();
        //-editorPane.addHyperlinkListener(urlHandler);
        // creating navigator pannel
        //-navPanel = new NavigatorPanel();

        // set layout
        this.setLayout(new BorderLayout());
        this.add(editorPane, BorderLayout.CENTER);
        //-this.add(navPanel,BorderLayout.NORTH);

        // Create page loader thread
        //-pageLoader = new LoadPageThread(this);
        //-pageLoader.setPriority(Thread.MIN_PRIORITY+2);
        //-pageLoader.start();

        // set offline page and home url

        /*StyleContext context = new StyleContext();
        StyledDocument document = new DefaultStyledDocument(context);

        style = context.getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setAlignment(style, StyleConstants.ALIGN_RIGHT);
        StyleConstants.setFontSize(style, 14);
        StyleConstants.setSpaceAbove(style, 4);
        StyleConstants.setSpaceBelow(style, 4);

        try {
          document.insertString(document.getLength(), "alo alo", style);
        } catch (BadLocationException badLocationException) {
          System.err.println("Oops");
        }

        editorPane.setDocument(document);*/

    }

    public void loadTheme() {
        //Theme.registerComponent(this);
        editorPane.setBackground(Theme.getColor("GRAPH_BGCOLOR"));
        editorPane.setForeground(Theme.getColor("GRAPH_FONT_COLOR"));

        //-navPanel.loadTheme();
    }

    /* public void applyTheme(){
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(editorPane);
    }*/
    /*public  void setURL(String sURL){
          //prepare URL
          try {

            //-url = new URL(sURL);

          } catch (Exception e) {
            setText(Language.getString("MSG_BROWSER_URL_ERROR")
                          +"<BR>"+e.toString());
            e.printStackTrace();
            //System.err.println("Couldn't create URL: " + sURL);
          }

          //load html page
          try {
            //-editorPane.setContentType("text/html");
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            //-editorPane.setPage(url);

            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

            //-currentURL = url;

            if ( urlList.indexOf(url)<0 )
            {
                urlList.add(currentURL);
                pos = urlList.size()-1;

                  if ( urlList.size()>10 ){
                    urlList.remove(0);
                  }
            }
            else
            {
              pos = urlList.indexOf(url);
            }
            NavigatorPanel.getbtnBack().setEnabled(true);

          } catch (Exception e) {
            setText(Language.getString("MSG_BROWSER_URL_ERROR"));
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            //System.err.println("Error in loading url:"+url+" "+e.toString());
          }
    }*/

    public void loadHTMLText(String path) {
        StringBuffer buffer = new StringBuffer();
        byte[] data = new byte[1000];
        int len = 0;

        try {
            InputStream in = new FileInputStream(path);
            while (true) {
                len = in.read(data);
                if (len == -1) break;
                buffer.append(new String(data, 0, len));
            }
            //editorPane.setContentType("text/html");
            editorPane.setText(buffer.toString());
            in.close();
            in = null;
            data = null;
            buffer = null;
        } catch (IOException e) {

        }
    }

    public void setText(String sText) {
        try {
            //editorPane.setContentType("text/plain");
            /*if(Language.isLTR()){
                editorPane.setText("<html><table width='"+(parent.getWidth()-50)+
                "'><tr><td width='100%' valign='top' align='center'>"+
                "<font face='"+Theme.getFontName("DEFAULT_FONT") +
                "'>"+sText+"</font></td></table>");
            }else{*/

            editorPane.setText("<html><table width='" + (parent.getWidth() - 10) +
                    "'><tr><td width='100%' align='center'>" +
                    "<font face='" + Theme.getFontName("DEFAULT_FONT") + "'>" + sText +
                    "</font></td></table>");

            //editorPane.setText(sText);
            /*editorPane.getDocument().remove(0,editorPane.getDocument().getLength());
            editorPane.getDocument().insertString(0,sText,style);
            editorPane.updateUI();*/
            //}
            //"<font face='"+Theme.getFontName("DEFAULT_FONT") +"'>"+sText+"</font></td></tr></table>");

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    /*public static URL getCurrentURL(){
      return currentURL;
    }*/

    /*public static URL getHomeURL(){
      return homeUrl;
    }*/

    /*public static void homeAction(){
      //if ( Settings.isConnected() ){
        pageLoader.setURL(homeUrl.toString());
        synchronized (pageLoader){
          pageLoader.notify();
       }
          //.out.println(homeUrl.toString());
     // }
    }

    public static void backAction(){
      URL url;
      if ( urlList.size() > 1 && pos != 0)
      {
        url = (URL)urlList.at(pos-1);
        pageLoader.setURL(url.toString());
        NavigatorPanel.getbtnForward().setEnabled(true);
        synchronized (pageLoader){
          pageLoader.notify();
        }
      }else if (pos==0){
        NavigatorPanel.getbtnBack().setEnabled(false);
      }
    }

    public static void forwardAction(){
      URL url;
      if ( urlList.size() > (pos+1) )
      {
        url = (URL)urlList.at(pos+1);
        pageLoader.setURL(url.toString());
        NavigatorPanel.getbtnBack().setEnabled(true);
        synchronized (pageLoader){
         pageLoader.notify();
        }
      }else if(urlList.size() ==(pos+1)){
        NavigatorPanel.getbtnForward().setEnabled(false);
      }
    }

    public static void callloadURL(final String sURL)
    {
      SwingUtilities.invokeLater(new Runnable()
      {
          public void run()
          {
              pageLoader.setURL(sURL);
              synchronized (pageLoader){
                pageLoader.notify();
              }
          }
      });
    }

    public static void showHomeURLx()
    {
        // set home URL
          try
          {
              sHome = Settings.getHomeURL() +"home_"+Settings.getItem("LANGUAGE")+".html";
              homeUrl = new URL(sHome);
          }
          catch ( Exception e )
          {
              e.printStackTrace();
          }
          callloadURL(homeUrl.toString());
    }*/

}


