// Copyright (c) 2000 ISI Sri Lanka
package com.isi.csvr.browser;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;


public class NavigatorPanel extends JPanel {
    //private static TWButton btnHome;
    private static TWButton btnBack;
    private static TWButton btnForward;
    private static BrowserActionHandler actionHandler;

    /**
     * Constructor
     */
    public NavigatorPanel() {

        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        //btnHome = new TWButton(Language.getString("BTN_BROWSER_HOME"));


        btnBack = new TWButton(Language.getString("BTN_BROWSER_BACK"));
        //btnBack.setBackground(Color.black);
        //btnBack.setForeground(Color.white);


        btnForward = new TWButton(Language.getString("FORWARD"));
        //btnForward.setBackground(Color.black);
        //btnForward.setForeground(Color.white);


        this.add(Box.createRigidArea(new Dimension(5, 5)));

        // Adding action handlers
        actionHandler = new BrowserActionHandler();
        //btnHome.addActionListener(actionHandler);
        btnBack.addActionListener(actionHandler);
        btnForward.addActionListener(actionHandler);

        /*JToolBar tb = new JToolBar();
        tb.add(btnHome);
        btnHome.setFocusPainted(false);
        btnHome.setEnabled(false);
        btnBack.setEnabled(false);
        btnBack.setFocusPainted(false);
        btnForward.setEnabled(false);
        btnForward.setFocusPainted(false);
        tb.setFloatable(false);
        this.add(tb);*/

    }

    public static TWButton getbtnBack() {
        return btnBack;
    }

    /*public static TWButton getbtnHome(){
      return btnHome;
    }*/

    public static TWButton getbtnForward() {
        return btnForward;
    }

    public void loadTheme() {
        setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
        setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
        //btnHome.setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
        // btnHome.setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
    }

}