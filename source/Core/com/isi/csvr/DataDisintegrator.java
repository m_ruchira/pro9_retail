// Copyright (c) 2000 ISI Sri Lanka
package com.isi.csvr;

/**
 * This class breaks the data elemnt
 * to it's tag and value
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class DataDisintegrator extends Object {

    private String g_sTag;
    private String g_sData;
    private String g_sSeperator = "=";

    /**
     * Constructor
     */
    public DataDisintegrator() {
    }

    /*
    * Returns the tag part
    */
    public String getTag() {
        return g_sTag;
    }

    /*
    * Returns the value part
    */
    public String getData() {
        return g_sData;
    }

    /*
    * Break the data element
    */
    public void setData(String sData) {
        int iEqual = sData.indexOf(g_sSeperator);

        if (iEqual > 0) {
            g_sTag = sData.substring(0, iEqual);
            g_sData = sData.substring(iEqual + 1).trim();
        } else {
            g_sTag = null;
            g_sData = null;
        }
    }

    /**
     * Sets the separator char for the disintegrator
     */
    public void setSeperator(String sSeperator) {
        g_sSeperator = sSeperator;
    }
}

