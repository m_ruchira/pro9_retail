package com.isi.csvr.heatmap;

import com.isi.csvr.*;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.print.PrintPreview;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.ListButton;
import com.isi.csvr.util.ListButtonActionListener;
import com.isi.csvr.util.ListButtonItem;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.beans.PropertyVetoException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.StringTokenizer;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 *         modified Chandika Hewage
 * @version 1.0
 */


public class HeatMapFrame extends InternalFrame implements ActionListener,
        TWDesktopInterface, ViewConstants, WindowWrapper, InternalFrameListener,
        MouseMotionListener, MouseListener, Printable, ExchangeListener, Themeable,
        ListButtonActionListener, ApplicationListener {

    public static int MIN_WIDTH = 90;
    public int heatMapMode = 0;
    public boolean valueMode = true;
    Color[] themeDownColors = {new Color(204, 0, 0), new Color(255, 0, 0),
            new Color(255, 78, 23), new Color(255, 0, 0)
    };
    Color[] themeUpColors = {new Color(0, 102, 102), new Color(0, 102, 0),
            new Color(0, 102, 204), new Color(0, 0, 102)
    };
    ImageIcon[] imgThemes = {
            new ImageIcon("images/Common/heatmap_theme4.gif"),
            new ImageIcon("images/Common/heatmap_theme2.gif"),
            new ImageIcon("images/Common/heatmap_theme3.gif"),
            new ImageIcon("images/Common/heatmap_theme1.gif")
    };
    ImageIcon imgColumns = new ImageIcon("images/Theme" + Theme.getID() + "/heatmap_columns.gif");
    ImageIcon imgWidths = new ImageIcon("images/Theme" + Theme.getID() + "/heatMap_cellWidths.gif");
    ImageIcon imgViews = new ImageIcon("images/Theme" + Theme.getID() + "/heatMap_changeView.gif");
    ImageIcon imgPrint = new ImageIcon("images/Theme" + Theme.getID() + "/graph_tb_print.gif");
    ImageIcon imgValueOnf = new ImageIcon("images/Theme" + Theme.getID() + "/heatMap_valueOnOff.gif");
    ImageIcon imgMain = new ImageIcon("images/Theme" + Theme.getID() + "/heatMap_mainView.gif");
    ImageIcon imgHr = new ImageIcon("images/Theme" + Theme.getID() + "/heatMap_hrView.gif");
    ImageIcon imgVr = new ImageIcon("images/Theme" + Theme.getID() + "/heatMap_vrView.gif");
    ImageIcon[] imgStyles = {new ImageIcon("images/Theme" + Theme.getID() + "/heatmap_style_normal.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/heatmap_style_hive.gif")};
    String[] saSortBy = {Language.getString("HEAT_MAP_LBL_SORT_DESCENDING"),
            Language.getString("HEAT_MAP_LBL_SORT_ASCENDING"),
            Language.getString("HEAT_MAP_LBL_SORT_SYMBOL")};
    ImageIcon[] imgSortBy = {new ImageIcon("images/Theme" + Theme.getID() + "/heatmap_sort_desc.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/heatmap_sort_asc.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/heatmap_sort_symbl.gif")};
    String[] saMapStyle = {Language.getString("HEAT_MAP_STYLE_NORMAL"),
            Language.getString("HEAT_MAP_STYLE_HIVE")};
    Font fontLBL = Theme.getDefaultFont(Font.PLAIN, 11);
    Font fontCMB = Theme.getDefaultFont(Font.PLAIN, 10);
    FlexGridLayout mainPanelLayout;
    FlexGridLayout mainFrmameLayout;
    int FRAME_WIDTH = 550;

//	1)       S&P Rank (1 - 5) - numeric no decimals
//	2)       P/E Ratio - numeric with two decimals
//	3)       Dividend Yield - % with two decimals
//	4)       5 year Growth - % with two decimals
//	5)       Beta  - Numeric with two decimals
//	6)       Market Cap - (value in millions, two decimals)
//	7)       Institutional Holding - % with two decimals
//	8)       % change - % with two decimals - we may decide to drop this based on performance considerations
//	9)       % gains - % with two decimals  (applicable only for portfolio, this is nice if we can have)
    int FRAME_HEIGHT = 390;
    boolean isFirstTime = true;
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private int windowType;
    private ViewSetting oSettings = null;
    private String watchlistID = "1052477760429";
    private byte themeID = 0;
    private boolean symbolsRegistered = false;
    private HeatPanel heatPanel = new HeatPanel();
    private HeatToolTip pnlToolTip = new HeatToolTip();
    private HeatPanel.Footer footer;
    private JPanel hintPane;
    private JPanel panelOne;
    private JPanel panelTwo;
    private JPanel footerPanel;
    private JButton btnPrint;
    private TWComboBox cellSizebtn;
    private JCheckBox valueonoff;
    private JToggleButton valueOnOffBtn;
    private ListButton lstbtnTheme;
    private ListButton lstbtnCriteria;
    private ListButton lstbtnExchange;
    private ListButton lstbtnSortBy;
    private ListButton lstbtnColumns;
    private ListButton cellwidthBtn;
    private ListButton tileBtn;
    private ListButton lstbtnStyles;
    private ListButton lstbtnMarkets;
    private ToolBarButton hideBtn;
    private JScrollPane scrollPane;
    private String selectedExchange = null;
    private JPanel mainPanel;
    private String[] symbolArray = null;
    private JPanel switchPanel;
    private JPanel switchPanelVertical;
    private String selectedMarket = "";
    private TWMenuItem mainView;
    private TWMenuItem horizontalView;
    private TWMenuItem verticalView;
    private JPopupMenu rightClickPopup;

    public HeatMapFrame() {
        // do not use
    }

    public HeatMapFrame(ViewSetting oSettings) {
        super("", oSettings);
        this.oSettings = oSettings;
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        addInternalFrameListener(this);
        createUI();
        this.setBorder(GUISettings.getTWFrameBorder());
        this.setResizable(false);
        this.setDetachable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Application.getInstance().addApplicationListener(this);
        this.setSize(550, 390);
    }

    public static String getStringFromCriteria(byte criteria) {
        switch (criteria) {
            case HeatInterface.CRITERIA_SNP_RANK:
                return Language.getString("HEAT_MAP_CRITERIA_SNP_RANK");
            case HeatInterface.CRITERIA_SNP_STAR_RATING:
                return Language.getString("HEAT_MAP_CRITERIA_SNP_STAR_RATING");
            case HeatInterface.CRITERIA_PE_RATIO:
                return Language.getString("HEAT_MAP_CRITERIA_PE_RATIO");
            case HeatInterface.CRITERIA_DIVIDEND_YIELD:
                return Language.getString("HEAT_MAP_CRITERIA_DIVIDEND_YIELD");
            case HeatInterface.CRITERIA_5_YEAR_GROWTH:
                return Language.getString("HEAT_MAP_CRITERIA_5_YEAR_GROWTH");
            case HeatInterface.CRITERIA_BETA:
                return Language.getString("HEAT_MAP_CRITERIA_BETA");
            case HeatInterface.CRITERIA_MARKET_CAP:
                return Language.getString("HEAT_MAP_CRITERIA_MARKET_CAP");
            case HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING:
                return Language.getString("HEAT_MAP_CRITERIA_INSTITUTIONAL_HOLDING");
            case HeatInterface.CRITERIA_PERC_CHANGE:
                return Language.getString("HEAT_MAP_CRITERIA_PERC_CHANGE");
            case HeatInterface.CRITERIA_PERC_GAIN:
                return Language.getString("HEAT_MAP_CRITERIA_PERC_GAIN");
            case HeatInterface.CRITERIA_BID_ASK_RATIO:
                return Language.getString("HEAT_MAP_CRITERIA_BID_ASK_RATIO");
            case HeatInterface.CRITERIA_PER:
                return Language.getString("HEAT_MAP_CRITERIA_PER");
            case HeatInterface.CRITERIA_NUM_TRADES:
                return Language.getString("HEAT_MAP_CRITERIA_NUM_TRADES");
            case HeatInterface.CRITERIA_VOLUME:
                return Language.getString("HEAT_MAP_CRITERIA_VOLUME");
            case HeatInterface.CRITERIA_SPREAD:
                return Language.getString("SPREAD");
            case HeatInterface.CRITERIA_PCT_SPREAD:
                return Language.getString("PCT_SPREAD");
            case HeatInterface.CRITERIA_RANGE:
                return Language.getString("RANGE");
            case HeatInterface.CRITERIA_PCT_RANGE:
                return Language.getString("PCT_RANGE");
            case HeatInterface.CRITERIA_MONEY_FLOW:
                return Language.getString("MONEY_FLOW_INDEX");
            default:
                return Language.getString("HEAT_MAP_CRITERIA_PERC_CHANGE");
        }
    }

    public void setValueOnOffMode(boolean mode) {
        if (mode) {
            HeatPanel.showValues = true;
            heatPanel.setCellHeight(30);
        } else {
            HeatPanel.showValues = false;
            heatPanel.setCellHeight(20);
        }
        heatPanel.setPreferredBounds();
        heatPanel.repaint();
        footer.repaint();
    }

    public void createUI() {
        footer = heatPanel.getFooter();
        mainPanel = new JPanel();
        mainPanelLayout = new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "40"}, 0, 0);
        mainPanel.setLayout(mainPanelLayout);
        scrollPane = new JScrollPane(heatPanel);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        scrollPane.setBackground(Color.BLACK);
        scrollPane.getViewport().setBackground(Color.BLACK);
        mainPanel.add(scrollPane);
        mainPanel.add(footer);
        mainPanel.setBorder(BorderFactory.createEmptyBorder());
        mainPanel.setBorder(BorderFactory.createEmptyBorder());
        mainPanel.setBackground(Color.BLACK);
        mainPanel.addMouseListener(this);
        panelOne = new JPanel();
//        panelOne.setLayout(new FlowLayout(FlowLayout.LEADING, 6, 1));
        panelOne.setLayout(new FlexGridLayout(new String[]{"145", "230", "125"}, new String[]{"22"}, 5, 3));

        panelOne.setBackground(Color.BLACK);
        panelTwo = new JPanel();
        panelTwo.setLayout(new FlowLayout(FlowLayout.LEADING, 6, 1));
        panelTwo.setBackground(Color.BLACK);
        hintPane = (JPanel) this.getGlassPane();
        hintPane.setVisible(false);
        hintPane.setLayout(null);
        hintPane.add(pnlToolTip);
        heatPanel.setBackground(Color.BLACK);
        heatPanel.addMouseMotionListener(this);
        heatPanel.addMouseListener(this);
        createThemeButton();
        createCriteriaButton();
        createExchangeButton();
        createSortByButton();
        createColumnButton();
        createCellWidthBtn();
        createTileBtn();
        createStyleButton();
        createMarketButton();
        valueOnOffBtn = new JToggleButton(imgValueOnf);
        valueOnOffBtn.setPreferredSize(new Dimension(40, 22));
        valueOnOffBtn.addActionListener(this);
        valueOnOffBtn.setToolTipText(Language.getString("HEAT_MAP_LBL_VALUE_OFF"));
        btnPrint = new JButton(imgPrint);
        btnPrint.setToolTipText(Language.getString("PRINT"));
        btnPrint.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
        btnPrint.addActionListener(this);
        btnPrint.setPreferredSize(new Dimension(25, 22));
        lstbtnExchange.setPreferredSize(new Dimension(90, 22));
        lstbtnMarkets.setPreferredSize(new Dimension(80, 22));
        panelOne.add(lstbtnCriteria);
        panelOne.add(lstbtnExchange);
        panelOne.add(lstbtnMarkets);
        panelTwo.add(lstbtnTheme);
        panelTwo.add(lstbtnSortBy);
        panelTwo.add(lstbtnColumns);
        panelTwo.add(btnPrint);
        panelTwo.add(cellwidthBtn);
        panelTwo.add(valueOnOffBtn);
        panelTwo.add(tileBtn);
        footerPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        footerPanel.add(panelTwo);
        mainFrmameLayout = new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%", "30"}, 0, 0);
        getContentPane().setLayout(mainFrmameLayout);
        getContentPane().add(panelOne);
        getContentPane().add(mainPanel);
        getContentPane().add(footerPanel);
        setTitle(Language.getString("HEATMAP_TITLE"));
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        createExchange();
    }

    public void createHorizontalUI() {
        panelOne.setVisible(false);
        footerPanel.setVisible(false);
        mainFrmameLayout.setRowHeight(0, "1");
        mainFrmameLayout.setRowHeight(2, "1");
        mainPanel.removeAll();
        mainPanelLayout = new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "40"}, 0, 0);
        mainPanel.setLayout(mainPanelLayout);
        mainPanel.setBackground(Color.BLACK);
        mainPanel.add(scrollPane);
        mainPanel.add(heatPanel.getFooter());
        mainPanel.doLayout();
        heatPanel.setPreferredBounds();
        heatPanel.repaint();
        if (getMainPanelWidth() + 30 < 120) {
            this.setSize(500, 130);
        } else {
            this.setSize(getMainPanelWidth() + 30, getMainPanelHeight() + 30);
        }
        getContentPane().doLayout();
        updateUI();
        heatPanel.repaint();
        SwingUtilities.updateComponentTreeUI(heatPanel);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void createHorizontalUI(Dimension d) {
        panelOne.setVisible(false);
        footerPanel.setVisible(false);
        mainFrmameLayout.setRowHeight(0, "1");
        mainFrmameLayout.setRowHeight(2, "1");
        mainPanel.removeAll();
        mainPanelLayout = new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "40"}, 0, 0);
        mainPanel.setLayout(mainPanelLayout);
        mainPanel.setBackground(Color.BLACK);
        mainPanel.add(scrollPane);
        mainPanel.add(heatPanel.getFooter());
        mainPanel.doLayout();
        heatPanel.setPreferredBounds();
        heatPanel.repaint();
        if (getMainPanelWidth() + 30 < 120) {
            this.setSize(500, 130);
        } else {
            this.setSize((int) d.getWidth() + 30, (int) d.getHeight());
        }
        getContentPane().doLayout();
        updateUI();
        heatPanel.repaint();
        SwingUtilities.updateComponentTreeUI(heatPanel);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void createVerticalUI() {
        panelOne.setVisible(false);
        footerPanel.setVisible(false);
        mainFrmameLayout.setRowHeight(0, "1");
        mainFrmameLayout.setRowHeight(2, "1");
        heatPanel.setPreferredBounds();
        heatPanel.repaint();
        mainPanel.removeAll();
        mainPanelLayout = new FlexGridLayout(new String[]{"100%", "40"}, new String[]{"100%"}, 0, 0);
        mainPanel.setLayout(mainPanelLayout);
        mainPanel.setBackground(Color.BLACK);
        mainPanel.add(scrollPane);
        mainPanel.add(heatPanel.getFooter());
        mainPanel.doLayout();
        if (getMainPanelHeight() + 30 < 120) {
            this.setSize(130, 450);
        } else {
            this.setSize(getMainPanelWidth() + 30, getMainPanelHeight() + 30);
        }
        getContentPane().doLayout();
        updateUI();
        this.repaint();
        heatPanel.repaint();
        SwingUtilities.updateComponentTreeUI(heatPanel);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void createVerticalUI(Dimension d) {
        panelOne.setVisible(false);
        footerPanel.setVisible(false);
        mainFrmameLayout.setRowHeight(0, "1");
        mainFrmameLayout.setRowHeight(2, "1");
        heatPanel.setPreferredBounds();
        heatPanel.repaint();
        mainPanel.removeAll();
        mainPanelLayout = new FlexGridLayout(new String[]{"100%", "40"}, new String[]{"100%"}, 0, 0);
        mainPanel.setLayout(mainPanelLayout);
        mainPanel.setBackground(Color.BLACK);
        mainPanel.add(scrollPane);
        mainPanel.add(heatPanel.getFooter());
        mainPanel.doLayout();
        if (getMainPanelHeight() + 30 < 120) {
            this.setSize(130, 450);
        } else {
            this.setSize((int) d.getWidth(), (int) d.getHeight() + 30);
        }
        getContentPane().doLayout();
        updateUI();
        heatPanel.repaint();
        scrollPane.repaint();
        SwingUtilities.updateComponentTreeUI(heatPanel);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void createMainUI(boolean isMainView) {
        if (isMainView) {
            this.setSize(550, 390);
        }
        panelOne.setVisible(true);
        footerPanel.setVisible(true);
        mainFrmameLayout.setRowHeight(0, "30");
        mainFrmameLayout.setRowHeight(2, "30");
        mainPanel.removeAll();
        mainPanelLayout = new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "40"}, 0, 0);
        mainPanel.setLayout(mainPanelLayout);
        mainPanel.setBackground(Color.BLACK);
        mainPanel.add(scrollPane);
        mainPanel.add(heatPanel.getFooter());
        mainPanel.doLayout();
        HeatPanel.horizontalTile = false;
        HeatPanel.verticalTile = false;
        setHeatMapMode(0);
        heatPanel.setPreferredBounds();
        heatPanel.repaint();
        footer.repaint();
        getContentPane().doLayout();
        updateUI();
        heatPanel.repaint();
        SwingUtilities.updateComponentTreeUI(heatPanel);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void createMainUI(Dimension d) {
        this.setSize((int) d.getWidth(), (int) d.getHeight());
        isFirstTime = false;
        panelOne.setVisible(true);
        footerPanel.setVisible(true);
        mainFrmameLayout.setRowHeight(0, "30");
        mainFrmameLayout.setRowHeight(2, "30");
        mainPanel.removeAll();
        mainPanelLayout = new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "40"}, 0, 0);
        mainPanel.setLayout(mainPanelLayout);
        mainPanel.setBackground(Color.BLACK);
        mainPanel.add(scrollPane);
        mainPanel.add(heatPanel.getFooter());
        mainPanel.doLayout();
        HeatPanel.horizontalTile = false;
        HeatPanel.verticalTile = false;
        setHeatMapMode(0);
        heatPanel.setPreferredBounds();
        heatPanel.repaint();
        footer.repaint();
        getContentPane().doLayout();
        updateUI();
        heatPanel.repaint();
        SwingUtilities.updateComponentTreeUI(heatPanel);
        SwingUtilities.updateComponentTreeUI(this);
    }

    private void createThemeButton() {
        try {
            lstbtnTheme = new ListButton(ListButton.TYPE_IMAGE_IMAGE, this, Language.isLTR());
            lstbtnTheme.setToolTipText(Language.getString("HEAT_MAP_LBL_COLOR_THEME"));
            Object[] items = new Object[imgThemes.length];
            for (int i = 0; i < items.length; i++) {
                ListButtonItem lbi = new ListButtonItem("", imgThemes[i], "", 0f);
                lbi.setUpColor(themeUpColors[i]);
                lbi.setDownColor(themeDownColors[i]);
                items[i] = lbi;
            }
            lstbtnTheme.setItems(items);
            lstbtnTheme.setSelectedIndex(0);
            lstbtnTheme.setBackground(this.getBackground());
        } catch (Exception ex) {
        }
    }

    private void createCriteriaButton() {
        try {
            lstbtnCriteria = new ListButton(ListButton.TYPE_TEXT_TEXT, this, Language.isLTR());
            lstbtnCriteria.setToolTipText(Language.getString("HEAT_MAP_LBL_CRITERIA"));
            Object[] items;
            if (ExchangeStore.getSharedInstance().defaultCount() >= 1) {

                items = new Object[HeatInterface.getCriteriaDescriptions().length];
                for (int i = 0; i < items.length; i++) {
                    ListButtonItem lbi = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[i],
                            null, HeatInterface.getCriteriaDescriptions()[i], 0f);
                    items[i] = lbi;
                }
            } else {
                items = new Object[9];
                ListButtonItem lbi = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[0], null, HeatInterface.getCriteriaDescriptions()[0], 0f);
                items[0] = lbi;
                ListButtonItem lbi1 = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[1], null, HeatInterface.getCriteriaDescriptions()[1], 0f);
                items[1] = lbi1;
                ListButtonItem lbi2 = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[2], null, HeatInterface.getCriteriaDescriptions()[2], 0f);
                items[2] = lbi2;
                ListButtonItem lbi4 = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[4], null, HeatInterface.getCriteriaDescriptions()[4], 0f);
                items[3] = lbi4;
                ListButtonItem lbi6 = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[6], null, HeatInterface.getCriteriaDescriptions()[6], 0f);
                items[4] = lbi6;
                ListButtonItem lbi7 = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[7], null, HeatInterface.getCriteriaDescriptions()[7], 0f);
                items[5] = lbi7;
                ListButtonItem lbi8 = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[8], null, HeatInterface.getCriteriaDescriptions()[8], 0f);
                items[6] = lbi8;
                ListButtonItem lbi9 = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[9], null, HeatInterface.getCriteriaDescriptions()[9], 0f);
                items[7] = lbi9;
                ListButtonItem lbi10 = new ListButtonItem(HeatInterface.getCriteriaDescriptions()[10], null, HeatInterface.getCriteriaDescriptions()[10], 0f);
                items[8] = lbi10;

            }
            lstbtnCriteria.setItems(items);
            lstbtnCriteria.setSelectedIndex(HeatInterface.CRITERIA_PERC_CHANGE);
            lstbtnCriteria.setBorderColor(Color.DARK_GRAY);
            lstbtnCriteria.setForeground(Color.LIGHT_GRAY);
            lstbtnCriteria.setBackground(Color.BLACK);
        } catch (Exception ex) {
        }
    }

    private void createExchangeButton() {
        try {
            lstbtnExchange = new ListButton(ListButton.TYPE_TEXT_TEXT, this, Language.isLTR());
            lstbtnExchange.setBorderColor(Color.DARK_GRAY);
            lstbtnExchange.setForeground(Color.LIGHT_GRAY);
            lstbtnExchange.setBackground(Color.BLACK);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void createMarketButton() {
        try {
            lstbtnMarkets = new ListButton(ListButton.TYPE_TEXT_TEXT, this, Language.isLTR());
            lstbtnMarkets.setBorderColor(Color.DARK_GRAY);
            lstbtnMarkets.setForeground(Color.LIGHT_GRAY);
            lstbtnMarkets.setBackground(Color.BLACK);
            lstbtnMarkets.setVisible(false);
        } catch (Exception ex) {
        }
    }

    private void createSortByButton() {
        try {
            lstbtnSortBy = new ListButton(ListButton.TYPE_IMAGE_TEXT, this, Language.isLTR());
            lstbtnSortBy.setToolTipText(Language.getString("HEAT_MAP_LBL_SORT_BY"));
            Object[] items = new Object[saSortBy.length];
            for (int i = 0; i < items.length; i++) {
                ListButtonItem lbi = new ListButtonItem("", imgSortBy[i], saSortBy[i], 0f);
                items[i] = lbi;
            }
            lstbtnSortBy.setItems(items);
            lstbtnSortBy.setSelectedIndex(0);
            lstbtnSortBy.setBackground(this.getBackground());
        } catch (Exception ex) {
        }
    }

    private void createColumnButton() {
        try {
            Method am;
            int COL_LENGTH = 10;
            lstbtnColumns = new ListButton(ListButton.TYPE_IMAGE_TEXT, this, Language.isLTR());
            lstbtnColumns.setToolTipText(Language.getString("HEAT_MAP_LBL_COLUMNS"));
            Object[] items = new Object[COL_LENGTH];
            for (int i = 0; i < items.length; i++) {
                ListButtonItem lbi = new ListButtonItem("", imgColumns, Integer.toString(10 + i), 0f);
                items[i] = lbi;
            }
            lstbtnColumns.setItems(items);
            lstbtnColumns.setSelectedIndex(0);
            lstbtnColumns.setBackground(this.getBackground());
        } catch (Exception ex) {
        }
    }

    private void createCellWidthBtn() {
        try {
            cellwidthBtn = new ListButton(ListButton.TYPE_IMAGE_TEXT, this, Language.isLTR());
            cellwidthBtn.setToolTipText(Language.getString("HEAT_MAP_LBL_CELL_WIDTH"));
            Object[] items = new Object[4];
            for (int i = 2; i < 6; i++) {
                ListButtonItem lbi = new ListButtonItem("", imgWidths, i * 0.5 + " x", 0f);
                items[i - 2] = lbi;
            }
            cellwidthBtn.setItems(items);
            cellwidthBtn.setSelectedIndex(0);
            cellwidthBtn.setBackground(this.getBackground());
        } catch (Exception ex) {
            System.out.println("Width button not created....");
            ex.printStackTrace();
        }
    }

    private void createTileBtn() {
        try {
            tileBtn = new ListButton(ListButton.TYPE_IMAGE_TEXT, this, Language.isLTR());
            tileBtn.setToolTipText(Language.getString("TILE"));
            Object[] items = new Object[2];
            ListButtonItem lbi1 = new ListButtonItem("", imgViews, Language.getString("TILE_HORIZONTAL"), 0f);
            ListButtonItem lbi2 = new ListButtonItem("", imgViews, Language.getString("TILE_VERTICAL"), 0f);
            items[0] = lbi1;
            items[1] = lbi2;
            tileBtn.setItems(items);
            tileBtn.setSelectedIndex(0);
            tileBtn.setBackground(this.getBackground());
        } catch (Exception ex) {
            System.out.println("Tile button not created....");
            ex.printStackTrace();
        }
    }

    private void createStyleButton() {
        try {
            lstbtnStyles = new ListButton(ListButton.TYPE_IMAGE_TEXT, this, Language.isLTR());
            Object[] items = new Object[saMapStyle.length];
            for (int i = 0; i < items.length; i++) {
                ListButtonItem lbi = new ListButtonItem("", imgStyles[i], saMapStyle[i], 0f);
                items[i] = lbi;
            }
            lstbtnStyles.setItems(items);
            lstbtnStyles.setSelectedIndex(0);
            lstbtnStyles.setBackground(this.getBackground());
        } catch (Exception ex) {
        }
    }

    public void listButtonClicked(Object source) {

        if (source == lstbtnTheme) {
            themeID = (byte) lstbtnTheme.getSelectedIndex();
            ListButtonItem lbi = (ListButtonItem) lstbtnTheme.getSelectedItem();
            heatPanel.setTheme(lbi.getUpColor(), lbi.getDownColor());
            pnlToolTip.setToolTipColors(heatPanel.getToolTipColors());
        } else if (source == lstbtnCriteria) {
            ListButtonItem lbi = (ListButtonItem) lstbtnCriteria.getSelectedItem();
            heatPanel.setCriteria(getCriteriaFromString(lbi.getDescription()));
            heatPanel.updateHeatStore(symbolArray);
            heatPanel.setPreferredBounds();
            heatPanel.repaint();
            footer.repaint();
        } else if (source == lstbtnExchange) {
            ListButtonItem lbi = (ListButtonItem) lstbtnExchange.getSelectedItem();
            selectedExchange = lbi.getID();
            applyExchange(lbi.getID(), null);
            setMarkets(lbi.getID());
            lstbtnMarkets.setText(Language.getString("SELECT_MARKET"));
            setWatchlistID(Constants.HEATMAP_CONSTANT + lbi.getID());
            lstbtnExchange.repaint();
            createMainUI(false);
        } else if (source == lstbtnMarkets) {
            ListButtonItem lbi = (ListButtonItem) lstbtnMarkets.getSelectedItem();
            applyExchange(selectedExchange, lbi.getID());
            lstbtnMarkets.repaint();
        } else if (source == lstbtnSortBy) {
            byte mode = (byte) Math.max(lstbtnSortBy.getSelectedIndex(), 0);
            heatPanel.setSorting(mode);
            heatPanel.updateHeatStore(symbolArray);
            footer.repaint();
        } else if (source == lstbtnColumns) {
            int index = Math.max(lstbtnColumns.getSelectedIndex(), 0);
            heatPanel.setColumnCount(index + 10);
            heatPanel.repaint();
            footer.repaint();
            createMainUI(false);
        } else if (source == cellwidthBtn) {
            ListButtonItem lbi = (ListButtonItem) cellwidthBtn.getSelectedItem();
            if (lbi.getDescription().equals("1.0 x")) {
                heatPanel.setCellWidth(50);
//                createMainUI();
                heatPanel.setPreferredBounds();
            }
            if (lbi.getDescription().equals("1.5 x")) {
                heatPanel.setCellWidth(75);
//                createMainUI();
                heatPanel.setPreferredBounds();
            }
            if (lbi.getDescription().equals("2.0 x")) {
                heatPanel.setCellWidth(100);
//                createMainUI();
                heatPanel.setPreferredBounds();
            }
            if (lbi.getDescription().equals("2.5 x")) {
                heatPanel.setCellWidth(125);
//                createMainUI();
                heatPanel.setPreferredBounds();
            }
            heatPanel.setPreferredBounds();
            heatPanel.repaint();
            footer.repaint();
            createMainUI(false);
        } else if (source == tileBtn) {
            ListButtonItem lbi = (ListButtonItem) tileBtn.getSelectedItem();
            if (lbi.getDescription().equals(Language.getString("TILE_HORIZONTAL"))) {
                HeatPanel.horizontalTile = true;
                HeatPanel.verticalTile = false;
                setHeatMapMode(1);
                createHorizontalUI();
            }
            if (lbi.getDescription().equals(Language.getString("TILE_VERTICAL"))) {
                HeatPanel.verticalTile = true;
                HeatPanel.horizontalTile = false;
                setHeatMapMode(2);
                createVerticalUI();

            }
            heatPanel.repaint();
            footer.repaint();
        } else if (source == lstbtnStyles) {
            int style = lstbtnStyles.getSelectedIndex();
            heatPanel.setMapStyle((byte) Math.max(style, 0));
            if (style == heatPanel.MAP_STYLE_HIVE) {
                lstbtnColumns.setEnabled(false);
            } else {
                lstbtnColumns.setEnabled(true);
            }

            // Added by uditha to avoid a java bug in pack
            heatPanel.setPreferredSize(new Dimension(1, 1));
            heatPanel.setPreferredBounds();
            heatPanel.repaint();
            footer.repaint();

        }
    }

    private void setDefaultUI() {
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == btnPrint) {
            printMarketMap();
        }
        if (e.getSource() == valueOnOffBtn) {
            if (valueOnOffBtn.isSelected()) {
                valueOnOffBtn.setToolTipText(Language.getString("HEAT_MAP_LBL_VALUE_OFF"));
                setValueOnOffMode(true);
                setHeatMapValMode(true);
                heatPanel.setPreferredBounds();
                heatPanel.repaint();
                footer.repaint();
                heatPanel.setPreferredBounds();
                createMainUI(false);
                updateUI();

            } else {
                valueOnOffBtn.setToolTipText(Language.getString("HEAT_MAP_LBL_VALUE_ON"));
                setValueOnOffMode(false);
                setHeatMapValMode(false);
                heatPanel.setPreferredBounds();
                heatPanel.repaint();
                footer.repaint();
                createMainUI(false);
                updateUI();
            }

        }

        if (e.getSource() == mainView) {
            createMainUI(true);
            setHeatMapMode(0);
            adjustMainViewValueOnOff(HeatPanel.showValues);


        }
        if (e.getSource() == horizontalView) {
            HeatPanel.horizontalTile = true;
            HeatPanel.verticalTile = false;
            createHorizontalUI();
            setHeatMapMode(1);
        }
        if (e.getSource() == verticalView) {
            HeatPanel.horizontalTile = false;
            HeatPanel.verticalTile = true;
            createVerticalUI();
            setHeatMapMode(2);
        }
    }

    private void adjustMainViewValueOnOff(boolean show) {
        if (show) {

            setValueOnOffMode(true);
            setHeatMapValMode(true);
            heatPanel.setPreferredBounds();
            heatPanel.repaint();
            footer.repaint();
            heatPanel.setPreferredBounds();
            createMainUI(false);
            updateUI();

        } else {
            setValueOnOffMode(false);
            setHeatMapValMode(false);
            heatPanel.setPreferredBounds();
            heatPanel.repaint();
            footer.repaint();
            createMainUI(false);
            updateUI();

        }


    }

    private void refreshComboBoxItems() {
        lstbtnCriteria.setSelectedItem(getStringFromCriteria(heatPanel.getCriteria()));
        lstbtnSortBy.setSelectedIndex(heatPanel.hComparator.getComparisonMode());
        lstbtnTheme.setSelectedIndex(themeID);
        lstbtnColumns.setSelectedIndex(heatPanel.getColumnCount() - 10);
        lstbtnStyles.setSelectedIndex(heatPanel.getMapStyle());
    }

    private byte getCriteriaFromString(String str) {
        if (str.equals(Language.getString("HEAT_MAP_CRITERIA_SNP_RANK"))) {
            return HeatInterface.CRITERIA_SNP_RANK;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_SNP_STAR_RATING"))) {
            return HeatInterface.CRITERIA_SNP_STAR_RATING;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_PE_RATIO"))) {
            return HeatInterface.CRITERIA_PE_RATIO;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_DIVIDEND_YIELD"))) {
            return HeatInterface.CRITERIA_DIVIDEND_YIELD;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_5_YEAR_GROWTH"))) {
            return HeatInterface.CRITERIA_5_YEAR_GROWTH;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_BETA"))) {
            return HeatInterface.CRITERIA_BETA;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_MARKET_CAP"))) {
            return HeatInterface.CRITERIA_MARKET_CAP;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_INSTITUTIONAL_HOLDING"))) {
            return HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_PERC_CHANGE"))) {
            return HeatInterface.CRITERIA_PERC_CHANGE;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_PERC_GAIN"))) {
            return HeatInterface.CRITERIA_PERC_GAIN;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_PER"))) {
            return HeatInterface.CRITERIA_PER;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_NUM_TRADES"))) {
            return HeatInterface.CRITERIA_NUM_TRADES;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_VOLUME"))) {
            return HeatInterface.CRITERIA_VOLUME;
        } else if (str.equals(Language.getString("HEAT_MAP_CRITERIA_BID_ASK_RATIO"))) {
            return HeatInterface.CRITERIA_BID_ASK_RATIO;
        } else if (str.equals(Language.getString("SPREAD"))) {
            return HeatInterface.CRITERIA_SPREAD;
        } else if (str.equals(Language.getString("PCT_SPREAD"))) {
            return HeatInterface.CRITERIA_PCT_SPREAD;
        } else if (str.equals(Language.getString("RANGE"))) {
            return HeatInterface.CRITERIA_RANGE;
        } else if (str.equals(Language.getString("PCT_RANGE"))) {
            return HeatInterface.CRITERIA_PCT_RANGE;
        } else if (str.equals(Language.getString("MONEY_FLOW_INDEX"))) {
            return HeatInterface.CRITERIA_MONEY_FLOW;
        } else {
            return HeatInterface.CRITERIA_PERC_CHANGE;
        }
    }

    private void applyExchange(String exchange, String market) {
        if (exchange != null) {
            if (market != null) {
                Symbols symbols = DataStore.getSharedInstance().getSymbolsObject(exchange, market);
                show(symbols.getSymbols(), null);
            } else {
                Symbols symbols = DataStore.getSharedInstance().getSymbolsObject(exchange);
                show(symbols.getSymbols(), null);
            }
        }
    }

    public void show(String[] symbolArray, String title) {
        SharedMethods.updateGoogleAnalytics("MarketMap");
        try {
            if (!HeatInterface.isValidWindow()) {
                return;
            }
            if (HeatInterface.isInternationalType())
                unRegisterSymbols();

            setSymbols(symbolArray, title);
            HeatManager.getSharedInstance().addHeatWindow(this);
            if (HeatInterface.isInternationalType())
                registerSymbols();
            heatPanel.setPreferredBounds();
            if (title != null) {
                try {
                    setMarkets(selectedExchange);
                    lstbtnMarkets.setText(selectedMarket);
                } catch (Exception e) {
                }
            }
            if (getHeatMapMode() == 1) {
                HeatPanel.horizontalTile = true;
                HeatPanel.verticalTile = false;
                heatPanel.setPreferredBounds();
                heatPanel.repaint();
                createHorizontalUI();
            } else if (getHeatMapMode() == 2) {
                HeatPanel.horizontalTile = false;
                HeatPanel.verticalTile = true;
                heatPanel.setPreferredBounds();
                heatPanel.repaint();

                createVerticalUI();
            } else if (getHeatMapMode() == 0) {
                HeatPanel.horizontalTile = false;
                HeatPanel.verticalTile = false;
                if (isHeatMapValueShowing()) {
                    setValueOnOffMode(true);
                    setHeatMapValMode(true);
                    valueOnOffBtn.setSelected(true);
                } else {
                    setValueOnOffMode(false);
                    setHeatMapValMode(false);
                    valueOnOffBtn.setSelected(false);
                }
                heatPanel.setPreferredBounds();
                createMainUI(false);
                if (isFirstTime) {
                    setDefaultUI();// todo : remove
                    isFirstTime = false;
                }
            }
            heatPanel.repaint();
            heatPanel.setSelectedSymbol(null);
            setFrameBounds();
            super.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setFrameBounds() {
        //To change body of created methods use File | Settings | File Templates.
    }


    public void show(Dimension d) {
        SharedMethods.updateGoogleAnalytics("MarketMap");
        heatPanel.repaint();
        heatPanel.setPreferredBounds();
        if (getHeatMapMode() == 1) {
            HeatPanel.horizontalTile = true;
            HeatPanel.verticalTile = false;
            heatPanel.setPreferredBounds();
            heatPanel.repaint();
            createHorizontalUI(d);
        } else if (getHeatMapMode() == 2) {
            HeatPanel.horizontalTile = false;
            HeatPanel.verticalTile = true;
            heatPanel.setPreferredBounds();
            heatPanel.repaint();
            createVerticalUI(d);
        } else if (getHeatMapMode() == 0) {
            HeatPanel.horizontalTile = false;
            HeatPanel.verticalTile = false;
            if (isHeatMapValueShowing()) {
                setValueOnOffMode(true);
                setHeatMapValMode(true);
            } else {
                setValueOnOffMode(false);
                setHeatMapValMode(false);
            }
            heatPanel.setPreferredBounds();
            createMainUI(d);
        }
        heatPanel.repaint();
        heatPanel.setSelectedSymbol(null);
        heatPanel.setPreferredBounds();
        getContentPane().doLayout();
        this.updateUI();
        super.show();
    }

    public void setSymbols(String[] symbolArray, String title) {
        try {

            this.symbolArray = symbolArray;
            heatPanel.setWatchlistName(title);
            heatPanel.updateHeatStore(symbolArray);
            pnlToolTip.setToolTipColors(heatPanel.getToolTipColors());
            if (title != null)
                setTitle(Language.getString("HEATMAP_TITLE"));
            else
                setTitle(Language.getString("HEATMAP_TITLE"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setVisible(boolean status) {
        super.setVisible(status);
        if (status == true) {
            if (HeatInterface.isInternationalType())
                registerSymbols();
            HeatManager.getSharedInstance().addHeatWindow(this);
        }
    }

    public synchronized boolean registerSymbols() {
        boolean result = false;
        try {
            if (!isSymbolsRegistered()) {
                if (result = canRegisterSymbols()) {
                    for (int i = 0; i < symbolArray.length; i++) {
                        String key = symbolArray[i];
                        String symbol = SharedMethods.getSymbolFromKey(key);
                        String exchange = SharedMethods.getExchangeFromKey(key);
                        int intrumentType = SharedMethods.getInstrumentTypeFromKey(key);
                        try {
                            // loading from workspace. symbol has instrument type embeded
                            HeatInterface.setSymbol(false, SharedMethods.getKey(symbol, exchange, intrumentType), null, intrumentType);
                        } catch (Exception e) {
                            // loading frm an active watchlist. symbol is already registered.
                            HeatInterface.setSymbol(false, key);
                        }
                        symbol = null;
                    }
                    setSymbolsRegistered(true);
                }
            }
        } catch (Exception e) {

        }
        return result;
    }

    public synchronized void unRegisterSymbols() {
        if (isSymbolsRegistered()) {
            System.out.println("unregistering");
            for (int i = 0; i < symbolArray.length; i++) {
                String key = symbolArray[i];
                HeatInterface.removeSymbol(key);
                key = null;
            }
            setSymbolsRegistered(false);
        }
    }

    public boolean canRegisterSymbols() {
        int effectiveSymbolCount = 0;
        for (int i = 0; i < symbolArray.length; i++) {
            String key = symbolArray[i];
            if (!HeatInterface.isContainedSymbol(key)) {
                effectiveSymbolCount++;
            }
            key = null;
        }
        if (HeatInterface.isSymbolLimitExceeded()) {
            return false;
        }
        return true;
    }

    public void updateUI() {
        super.updateUI();
        this.setBorder(GUISettings.getTWFrameBorder());
    }

    public String toString() {
        return Language.getString("HEATMAP_TITLE");
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void closeWindow() {
        TWDesktop.doCloseWindow(this);
        focusNextWindow();
    }

    public void doDefaultCloseAction() {
        if (HeatInterface.isInternationalType())
            unRegisterSymbols();
        symbolArray = null;
        HeatManager.getSharedInstance().removeHeatWindow(this);
        TWDesktop.doCloseWindow(this);
        focusNextWindow();
    }

    private void focusNextWindow() {
        TWDesktop desktop = (TWDesktop) Client.getInstance().getDesktop();
        desktop.focusNextWindow(desktop.getListPosition(this, true), true);
        desktop = null;
    }

    public int getDesktopItemType() {
        return POPUP_TYPE;
    }

    public int getWindowType() {
        return windowType;
    }

    public void setWindowType(int type) {
        windowType = type;
    }

    public int getWindowStyle() {
        return 0;
    }

    public int getZOrder() {
        return 0;
    }

    public boolean isWindowVisible() {
        return isVisible();
    }

    public JTable getTable1() {
        return null;
    }

    public JTable getTable2() {
        return null;
    }

    public void applySettings() {
        this.setSize(oSettings.getSize());
        if (oSettings != null)
            this.setLocation(oSettings.getLocation());
        try {
            if (oSettings.isVisible()) {
                if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSettings.getIndex());
                } else {
                    this.getDesktopPane().setLayer(this, this.getLayer(), oSettings.getIndex());
                }
                this.setVisible(true);
                try {
                    this.setSelected(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                } else {
                    this.getDesktopPane().setLayer(this, this.getLayer(), 0);
                }
            }
        } catch (Exception e) {
            this.setVisible(oSettings.isVisible());
        }

        try {
            switch (oSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    if (oSettings != null) {
                        this.setSize(oSettings.getSize());
                        this.setLocation(oSettings.getLocation());
                    }
                    this.setIcon(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (!oSettings.isHeaderVisible()) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                this.revalidate();
            } else {
                updateUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isTitleVisible() {
        return true;
    }

    public void setTitleVisible(boolean status) {

    }

    public void windowClosing() {

    }

    public void printTable() {

    }

    public void refreshHeatValues() {
        heatPanel.updateHeatStore(symbolArray);
        heatPanel.repaint();
        footer.repaint();
    }


    /**
     * Invoked when a internal frame has been opened.
     *
     * @see javax.swing.JInternalFrame#show
     */
    public void internalFrameOpened(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is in the process of being closed.
     * The close operation can be overridden at this point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {
        if (HeatInterface.isInternationalType())
            unRegisterSymbols();
    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {
        if ((HeatInterface.isInternationalType()) && (!registerSymbols())) {
            try {
                setIcon(true); // iconi
            } catch (PropertyVetoException e1) {

            }
            pack();
        }
    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {
        hintPane.setVisible(false);
        oSettings.getSize();
        FRAME_WIDTH = this.getWidth();
        FRAME_HEIGHT = this.getHeight();
    }

    public void mouseDragged(MouseEvent e) {
    }

    /**
     * Invoked when the mouse button has been moved on a component
     * (with no buttons down).
     */
    public void mouseMoved(MouseEvent e) {
        Object obj = e.getSource();
        if (obj == heatPanel) {
            boolean toolTipVisible = heatPanel.updateToolTip(pnlToolTip, e.getX(), e.getY());
            if (toolTipVisible) {
                Point p = e.getPoint();
                SwingUtilities.convertPointToScreen(p, heatPanel);
                SwingUtilities.convertPointFromScreen(p, this.getContentPane());
                setToolTipLocation(p.x, p.y);
            }
            hintPane.setVisible(toolTipVisible);
        } else {
            hintPane.setVisible(false);
        }
    }


    private void setToolTipLocation(int x, int y) {
        int cursorHt = 22;
        int h = pnlToolTip.getHeight();
        int w = pnlToolTip.getWidth();
        int avaH = this.getContentPane().getHeight();
        int avaW = this.getContentPane().getWidth();
        boolean yOK = (y + cursorHt + h <= avaH);
        boolean xOK = (x + w <= avaW);
        if (xOK) {
            if (yOK) {
                y = y + cursorHt;
            } else {
                y = Math.max(0, y - h);
            }
        } else {
            yOK = (y + h <= avaH);
            if (yOK) {
                x = Math.max(0, x - w);
            } else {
                y = Math.max(0, y - h);
                x = Math.max(0, x - w);
            }
        }
        pnlToolTip.setLocation(x, y);
    }

    public String getWatchlistID() {
        return watchlistID;
    }

    public void setWatchlistID(String watchlistID) {
        this.watchlistID = watchlistID;
    }

    public int getHeatMapMode() {
        return heatMapMode;
    }

    public void setHeatMapMode(int mode) {
        this.heatMapMode = mode;
    }

    public boolean isHeatMapValueShowing() {
        return valueMode;

    }

    public void setHeatMapValMode(boolean status) {
        this.valueMode = status;
    }

    public void setSelectedExchange(String exchange) {
        if (exchange != null) {
            selectedExchange = exchange;
            lstbtnExchange.setText(null);
            lstbtnExchange.setSelectedItem(ExchangeStore.getSharedInstance().getExchange(exchange).getDescription());
        } else {
            selectedExchange = "";
            lstbtnExchange.setText("");
            lstbtnMarkets.removeAll();
            lstbtnMarkets.setVisible(false);
        }
    }

    public void setSelectedMarket(String subMarket) {
        if (subMarket != null) {
            lstbtnMarkets.setText(subMarket);
            selectedMarket = subMarket;
        } else {
            lstbtnMarkets.setText(Language.getString("SELECT_MARKET"));
            selectedMarket = Language.getString("SELECT_MARKET");
        }
    }

    public void upadateViewsetting() {
        HeatPanel hp = heatPanel;
        oSettings.putProperty(ViewConstants.VC_MKTMAP_CRITERIA, hp.getCriteria());
        oSettings.putProperty(ViewConstants.VC_MKTMAP_SORTING, hp.getComparator().getComparisonMode());
        oSettings.putProperty(ViewConstants.VC_MKTMAP_THEME, themeID);
        oSettings.putProperty(ViewConstants.VC_MKTMAP_COLUMNS, hp.getColumnCount());
        oSettings.putProperty(ViewConstants.VC_MKTMAP_STYLE, hp.getMapStyle());
        oSettings.putProperty(ViewConstants.VC_MKTMAP_ID, getWatchlistID());
        oSettings.putProperty(ViewConstants.VC_HEATMAP_MODE, getHeatMapMode());
        oSettings.putProperty(ViewConstants.VC_HEATMAP_VALONOFF, isHeatMapValueShowing());
    }

    public void setProperties(String record) {
        StringTokenizer fields = new StringTokenizer(record, ViewConstants.VC_RECORD_DELIMETER);
        DataDisintegrator pair = new DataDisintegrator();
        pair.setSeperator(ViewConstants.VC_FIELD_DELIMETER);

        HeatPanel hp = heatPanel;
        String value;
        while (fields.hasMoreTokens()) {
            try {
                pair.setData(fields.nextToken());
                value = pair.getData();

                switch (SharedMethods.intValue(pair.getTag(), 0)) {
                    case ViewConstants.VC_MKTMAP_CRITERIA:
                        hp.setCriteria(Byte.parseByte(value));
                        break;
                    case ViewConstants.VC_MKTMAP_SORTING:
                        hp.setSorting(Byte.parseByte(value));
                        break;
                    case ViewConstants.VC_MKTMAP_THEME:
                        themeID = Byte.parseByte(value);
                        hp.setTheme(themeUpColors[themeID], themeDownColors[themeID]);
                        break;
                    case ViewConstants.VC_MKTMAP_COLUMNS:
                        hp.setColumnCount(Integer.parseInt(value));
                        break;
                    case ViewConstants.VC_MKTMAP_STYLE:
                        hp.setMapStyle(Byte.parseByte(value));
                        break;
                    case ViewConstants.VC_MKTMAP_ID:
                        setWatchlistID(value);
                        break;
                    case ViewConstants.VC_HEATMAP_MODE:
                        setHeatMapMode(Integer.parseInt(value));
                        break;
                    case ViewConstants.VC_HEATMAP_VALONOFF:
                        setHeatMapValMode(Boolean.parseBoolean(value));
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        fields = null;
        pair = null;
        refreshComboBoxItems();
    }

    public synchronized boolean isSymbolsRegistered() {
        return symbolsRegistered;
    }

    public synchronized void setSymbolsRegistered(boolean symbolsRegistered) {
        this.symbolsRegistered = symbolsRegistered;
    }


    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            heatPanel.updateSelectedIndex(e.getX(), e.getY());
            String s = heatPanel.getSelectedSymbol();
            if (s != null) {
                HeatInterface.showSummaryQuote(s);
            }
        }
        if (SwingUtilities.isRightMouseButton(e)) {
            createRightClickPopup();
            validateRightClickPopup();
            rightClickPopup.show(e.getComponent(), e.getX(), e.getY());
        }
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
        Object obj = e.getSource();
        if (obj == heatPanel) {
            heatPanel.updateSelectedIndex(e.getX(), e.getY());
            heatPanel.repaint();
            footer.repaint();
            hintPane.setVisible(false);
        }
    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
        hintPane.setVisible(false);
    }

    public HeatPanel getHeatPanel() {
        return heatPanel;
    }

    public void setSize(int width, int height) {
        super.setSize(width, height);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void setSize(Dimension d) {
        super.setSize(d);    //To change body of overridden methods use File | Settings | File Templates.
        this.updateUI();
    }

    public int print(Graphics pg, PageFormat pageFormat, int pageIndex) throws PrinterException {
        pg.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());

        int wPage = (int) pageFormat.getImageableWidth();
        int hPage = (int) pageFormat.getImageableHeight();
        int w = heatPanel.getWidth();
        int h = heatPanel.getPrintableHeight();
        if (w == 0 || h == 0)
            return NO_SUCH_PAGE;

        heatPanel.drawPrintFooter((Graphics2D) pg, wPage, hPage);
        if ((wPage < w) || (hPage < h)) {
            float frac = Math.min((float) wPage / w, (float) (hPage) / h);
            ((Graphics2D) pg).scale(frac, frac);
        }
        heatPanel.setWatchlistName(oSettings.getCaption());
        heatPanel.paintOnThePrinterGraphics(pg, w, h, true);
        if (pageIndex == 0) {
            return PAGE_EXISTS;
        } else {
            return NO_SUCH_PAGE;
        }
    }

    public void printMarketMap() {
        PrintPreview p = new PrintPreview(this, "Preview");
    }

    public String getWindowID() {
        return null;
    }

    public void setWindowID(String id) {

    }

    public void applyTheme() {
        try {
            lstbtnTheme.updateUI();
            lstbtnCriteria.updateUI();
            lstbtnExchange.updateUI();
            lstbtnSortBy.updateUI();
            lstbtnColumns.updateUI();
            lstbtnStyles.updateUI();
            lstbtnMarkets.updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        createExchange();
        this.doLayout();
        this.repaint();
        mainPanel.repaint();
        heatPanel.repaint();
        scrollPane.repaint();
    }

    public void setMarkets(String exchange) {
        ArrayList items = new ArrayList();
        ListButtonItem lbi;
        try {
            lstbtnMarkets.removeAll();
            Exchange exch = ExchangeStore.getSharedInstance().getExchange(exchange);
            if (exch.hasSubMarkets() && exch.isUserSubMarketBreakdown()) {
                lstbtnMarkets.setVisible(true);
                Market[] markets = exch.getSubMarkets();
                for (int i = 0; i < markets.length; i++) {
                    lbi = new ListButtonItem(markets[i].getMarketID(), null, markets[i].getDescription().trim(), 0f);
                    items.add(lbi);
                }
                Collections.sort(items);
                lstbtnMarkets.setItems(items.toArray());
                items = null;
                lstbtnMarkets.updateUI();
            } else {
                lstbtnMarkets.removeAll();
                lstbtnMarkets.setVisible(false);
            }
        } catch (Exception e) {
        }
    }

    public void exchangesLoaded() {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private void createExchange() {
        ArrayList items = new ArrayList();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        ListButtonItem lbi;
        boolean exchangesFound = false;
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                lbi = new ListButtonItem(exchange.getSymbol(), null, exchange.getDescription(), 0f);
                items.add(lbi);
                exchangesFound = true;
            }
            exchange = null;
        }
        Collections.sort(items);
        //todo - 21/12/2006
        lstbtnExchange.setItems(items.toArray());
        items = null;
        if (exchangesFound) {
            lstbtnExchange.updateUI();
            lstbtnExchange.setVisible(true);
        } else {
            lstbtnExchange.setVisible(false);
        }
    }

    public JPanel createHideButton() {
        JPanel hideButton = new JPanel();
        hideButton.setOpaque(false);
        hideButton.setPreferredSize(new Dimension(18, 18));
        hideBtn = new ToolBarButton();
        hideBtn.setFocusable(false);
        hideBtn.setPreferredSize(new Dimension(18, 18));
        hideBtn.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        hideBtn.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/showpanel.gif"));
        hideBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                heatPanel.setPreferredBounds();
                heatPanel.repaint();
                footer.repaint();
                createMainUI(false);
            }
        });
        hideBtn.setVisible(true);
        hideBtn.setOpaque(false);
        hideButton.add(hideBtn);
        hideButton.setVisible(true);
        return hideButton;
    }

    public int getHorizontalWidth() {
        heatPanel.setPreferredBounds();
        int width = heatPanel.getPreferredSize().width;
        if (width > Client.getInstance().getDesktop().getWidth()) {
            width = Client.getInstance().getDesktop().getWidth() - 10;
            return width;
        }
        return width + 25;
    }

    public int getHorizontalHeight() {
        heatPanel.setPreferredBounds();
        int height = mainPanel.getPreferredSize().height;
        return height + 25;
    }

    public int getVerticalWidth() {
        heatPanel.setPreferredBounds();
        int width = mainPanel.getPreferredSize().width;
        return width + 25;
    }

    public int getVerticalHeight() {
        heatPanel.setPreferredBounds();
        int width = mainPanel.getPreferredSize().height;
        if (width > Client.getInstance().getDesktop().getHeight() - 50) {
            width = Client.getInstance().getDesktop().getHeight() - 70;
            return width;
        }
        return width + 25;
    }

    public int getMainPanelHeight() {
        heatPanel.setPreferredBounds();
        int frameHeight = heatPanel.getPreferredSize().height + 70;
        if (frameHeight > 452) {
            frameHeight = 452;
            return frameHeight;
        }
        return frameHeight;
    }

    public int getMainPanelWidth() {
        heatPanel.setPreferredBounds();
        int frameWidth = heatPanel.getPreferredSize().width + 70;
        if (frameWidth > 600) {
            frameWidth = 600;
            return frameWidth;
        }
        return frameWidth;
    }

    public void createRightClickPopup() {
        mainView = new TWMenuItem(Language.getString("HEATMAP_MAINVIEW"), imgMain);
        horizontalView = new TWMenuItem(Language.getString("TILE_HORIZONTAL"), imgHr);
        verticalView = new TWMenuItem(Language.getString("TILE_VERTICAL"), imgVr);
        rightClickPopup = new JPopupMenu("Popup2");
        rightClickPopup.add(mainView);
        rightClickPopup.add(horizontalView);
        rightClickPopup.add(verticalView);
        mainView.addActionListener(this);
        horizontalView.addActionListener(this);
        verticalView.addActionListener(this);
        GUISettings.applyOrientation(rightClickPopup);
    }

    public void validateRightClickPopup() {
        if (this.isDetached()) {
            if (HeatPanel.horizontalTile) {
                mainView.setVisible(false);
                horizontalView.setVisible(false);
                verticalView.setVisible(false);
            } else if (HeatPanel.verticalTile) {
                mainView.setVisible(false);
                horizontalView.setVisible(false);
                verticalView.setVisible(false);
            } else {
                mainView.setVisible(false);
                horizontalView.setVisible(false);
                verticalView.setVisible(false);
            }

        } else {
            if (HeatPanel.horizontalTile) {
                mainView.setVisible(true);
                horizontalView.setVisible(false);
                verticalView.setVisible(true);
            } else if (HeatPanel.verticalTile) {
                mainView.setVisible(true);
                horizontalView.setVisible(true);
                verticalView.setVisible(false);
            } else {
                mainView.setVisible(false);
                horizontalView.setVisible(true);
                verticalView.setVisible(true);
            }
        }
    }

    public void setLocation(Point p) {
        super.setLocation(p);
    }

    public void setLocation(int x, int y) {
        super.setLocation(x, y);
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        String exg = exchange.getSymbol();
        if (exg.equals(selectedExchange)) {
            heatPanel.repaint();
            heatPanel.setPreferredBounds();
            if (getHeatMapMode() == 1) {
                HeatPanel.horizontalTile = true;
                HeatPanel.verticalTile = false;
                heatPanel.setPreferredBounds();
                heatPanel.repaint();
                createHorizontalUI(oSettings.getSize());
            } else if (getHeatMapMode() == 2) {
                HeatPanel.horizontalTile = false;
                HeatPanel.verticalTile = true;
                heatPanel.setPreferredBounds();
                heatPanel.repaint();
                createVerticalUI(oSettings.getSize());
            } else if (getHeatMapMode() == 0) {
                HeatPanel.horizontalTile = false;
                HeatPanel.verticalTile = false;
                if (isHeatMapValueShowing()) {
                    setValueOnOffMode(true);
                    setHeatMapValMode(true);
                } else {
                    setValueOnOffMode(false);
                    setHeatMapValMode(false);
                }
                heatPanel.setPreferredBounds();
                createMainUI(oSettings.getSize());
            }
            heatPanel.repaint();
            heatPanel.setSelectedSymbol(null);
            heatPanel.setPreferredBounds();
            getContentPane().doLayout();
            this.updateUI();
        } else {
            //do nothing
        }
    }
}
