package com.isi.csvr.heatmap;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.HeatInterface;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class HeatPanel extends JPanel {


    public static final char charStar = 0x22C6;
    String strStar = Character.toString(charStar);
    String strFiveStar = strStar.concat(strStar).concat(strStar).concat(strStar).concat(strStar);
    public static String[] saSPRank = {"AAA", "AA+", "AA", "AA-", "A+", "A", "A-",
            "BBB", "BB+", "BB", "BB-", "B+", "B", "B-",
            "CCC", "CC+", "CC", "CC-", "C+", "C", "C-",
            "DDD", "DD+", "DD", "DD-", "D+", "D", "D-",
            "NR", "NA"};
    public static boolean horizontalTile = false;
    public static boolean verticalTile = false;
    public static boolean showValues = true;
    final int[] colorCount = new int[HeatInterface.CRITERIA_COUNT];
    final int MAX_COLOR_COUNT = 11;
    Color[] heatColors = new Color[MAX_COLOR_COUNT * 2 + 1];
    final int HALF_GAP = 5;
    final int STAR_RADIUS = 4;
    final int symbolHeight = 12;
    final int valueHeight = 26;
    final int cellGap = 2;
    final int TitleHeight = 1;
    final int FooterHeight = 42;
    final Object lock = new Object();
    final byte MAP_STYLE_NORMAL = 0;
    private byte mapStyle = MAP_STYLE_NORMAL;
    final byte MAP_STYLE_HIVE = 1;
    final byte MAP_HOR_TILE = 2;
    final Font fontPlain = Theme.getDefaultFont(Font.PLAIN, 10);
    final Font fontBold = Theme.getDefaultFont(Font.BOLD, 11);
    final Font fontBoldSmall9 = Theme.getDefaultFont(Font.BOLD, 9);
    final Font fontBoldSmall7 = Theme.getDefaultFont(Font.BOLD, 7);
    final Font fontSmall = Theme.getDefaultFont(Font.BOLD, 9);
    final String NA = Language.getString("NA");
    public HeatComparator hComparator = new HeatComparator();
    public int rows = 0;
    public int horizontalTileHeight = 43;
    public int verticalTileWidth = 43;
    ImageIcon imgStar = new ImageIcon("images/Common/heatmap_star.gif");
    Font starFont = SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 12);
    int RADIUS_A = 25;
    float RADIUS_B = RADIUS_A * (float) Math.sqrt(3) / 2f;
    int cellWidth = RADIUS_A * 2;  //80; //
    int cellHeight = 30;
    String strPercntChange = Language.getString("HEAT_MAP_PERCENTAGE_CHANGE");
    BasicStroke bsBorder = new BasicStroke(3, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    BasicStroke bsNormal = new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    DynamicArray heatStore = new DynamicArray();
    DynamicArray heatBuffer = new DynamicArray();
    TWDecimalFormat numFloatFormatter = new TWDecimalFormat("#,##0.00");
    TWDecimalFormat numFloatSingleFormatter = new TWDecimalFormat("#,##0.0");
    TWDecimalFormat numPcntFormatter = new TWDecimalFormat("###,##0.00'%'");// ###,##0.00  #,##0.00'%'
    TWDecimalFormat numFormatter = numPcntFormatter;
    TWDecimalFormat numIntFormatter = new TWDecimalFormat("  #,##0  ");
    Color upColor = new Color(0, 102, 102);
    Color downColor = new Color(204, 0, 0);
    String watchlistName = "";
    int boxW = 20;
    int boxH = 15;
    int boxTop = 100;
    int boxLeft = 25;
    private String[] saCompareMode = {
            Language.getString("HEAT_MAP_SORTED_DESCENDING"),
            Language.getString("HEAT_MAP_SORTED_ASCENDING"),
            Language.getString("HEAT_MAP_SORTED_BY_SYMBOL")
    };
    private double maxVariation = 0f;
    private double midValue = 0f;
    private double midBuffer = 0f;
    private double maxVariationBuffer = 0f;
    private int columns = 10;
    private byte criteria = HeatInterface.CRITERIA_PERC_CHANGE;
    private int DARK_COUNT = 5;
    private boolean isTwoColor = true;
    private String selectedSymbol = null;

    public HeatPanel() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static float Round(float Rval, int Rpl) {
        float p = (float) Math.pow(10, Rpl);
        Rval = Rval * p;
        float tmp = Math.round(Rval);
        return (float) tmp / p;
    }

    public void setCellWidth(int width) {
        cellWidth = width;
    }

    public void setCellHeight(int height) {
        cellHeight = height;
    }

    void jbInit() throws Exception {
        this.setLayout(null);
        heatStore.setComparator(hComparator);
        heatBuffer.setComparator(hComparator);
        setHeatColorCount();
        setHeatColors();
    }

    public String[] loadQuoteStoreSymbols() {
//		String[] saTemp = new String[QuoteStore.getSymbolCount()];
//		Enumeration enum = QuoteStore.getSymbols();
//		int count = 0;
//		while (enum.hasMoreElements()){
//			saTemp[count] = (String)enum.nextElement();
//			count++;
//			if (count>=saTemp.length) break;
//		}
//		System.out.println("loading symbols to heat panel - count: "+count);
//		return saTemp;

        return null; //HeatInterface.getWatchListSymbols();
    }

    public void updateHeatStore(String[] sa) {
        heatBuffer.clear();
        if (sa != null) {
            HeatInterface.updateHeatStore(sa, heatBuffer);

            calculateHeatValues(heatBuffer);
        }
        synchronized (lock) {
            DynamicArray tmpDA = heatStore;
            heatStore = heatBuffer;
            heatBuffer = tmpDA;
            midValue = midBuffer;
            maxVariation = maxVariationBuffer;
        }
        //--------setPreferredBounds();
    }

    public void paint(Graphics g) {
        super.paint(g);
        synchronized (lock) {
            if (heatStore.size() > 0) {
                //((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                drawBackGround(g);

                switch (mapStyle) {
                    case MAP_STYLE_HIVE:
                        drawHiveCells(g, getWidth(), false);
                        break;
                    case MAP_STYLE_NORMAL:
                    default:
                        drawNormalCells(g, getWidth(), false);
                        break;
                }

                //drawFooter(g, getFooterTop());
            }
        }
    }

    private void drawNormalCells(Graphics g, int w, boolean isPrinting) {
        HeatRecord hR;
        String value;
        double val;
        int x, y, row, col;
        setMaximumStringWidth();
        for (int i = 0; i < heatStore.size(); i++) {

            col = i % columns;
            row = i / columns;
            x = (col + 1) * cellGap + col * cellWidth;
            y = (row + 1) * cellGap + row * cellHeight + TitleHeight;
            if (horizontalTile) {

                col = i % heatStore.size();
                row = 1;
                int width = 16;// (1070 - (heatStore.size())) / heatStore.size();
                if (width <= 16) {
                    width = 16;
                }
                x = (col + 1) * 2 + col * width;
                y = 2;//(row + 1) * 2 + row * 60 + 1;

                hR = (HeatRecord) heatStore.get(i);
                numFormatter = getSymbolSpecificNumFormatter(hR.getKey());
                if (true) {
                    g.setColor(hR.getColor());
                    g.fillRect(x, y, width + 1, horizontalTileHeight + 1);
                }
                g.setColor(Color.BLACK);
                g.drawRect(x - 1, y - 1, width + 2, horizontalTileHeight + 2);
//                System.out.println("Test y : "+horizontalTileHeight);
                if (true) {
                    g.setColor(hR.getFontColor());
                }
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                drawTiledString(g, hR.getSymbol(), x, y, width);
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

            } else if (verticalTile) {
                col = 1;//i % heatStore.size();
                row = i % heatStore.size();// 1;
                cellHeight = 16;
                y = (row + 1) * 2 + row * cellHeight;
                x = 2;//(row + 1) * 2 + row * 60 + 1;
                hR = (HeatRecord) heatStore.get(i);
                numFormatter = getSymbolSpecificNumFormatter(hR.getKey());
                if (true) {
                    g.setColor(hR.getColor());
                    g.fillRect(x, y, verticalTileWidth + 1, 16 + 1);
                }
                g.setColor(Color.BLACK);
                g.drawRect(x - 1, y - 1, verticalTileWidth + 2, 16 + 2);

                if (true) {
                    g.setColor(hR.getFontColor());
                }
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                drawCenteredString(g, hR.getSymbol(), x, y + 12, verticalTileWidth);
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            } else {

                rows = heatStore.size();
                hR = (HeatRecord) heatStore.get(i);
                numFormatter = getSymbolSpecificNumFormatter(hR.getKey());
                if (!isPrinting) {
                    g.setColor(hR.getColor());
                    g.fillRect(x, y, cellWidth + 1, cellHeight + 1);
                }
                g.setColor(Color.BLACK);
                g.drawRect(x - 1, y - 1, cellWidth + 2, cellHeight + 2);

                if (!isPrinting) {
                    g.setColor(hR.getFontColor());
                }
                g.setFont(fontBold);
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                if (showValues) {
                    drawCenteredString(g, hR.getSymbol(), x, y + symbolHeight, cellWidth);
                } else {
                    drawCenteredString(g, hR.getSymbol(), x, y + 14, cellWidth);
                }
                val = hR.getValue(criteria);
//                System.out.println("Checking : "+val);
                value = getDisplayValue(val);
//                System.out.println("Checking2 : "+value);
                if ((criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) && (val > 0) && showValues) {
                    //drawCenteredStars(g, val, x, y+valueHeight-5, cellWidth);
                    g.setFont(starFont);
                    drawCenteredString(g, value, x, y + valueHeight, cellWidth);
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    g.setFont(fontPlain);
                } else if (showValues) {
                    g.setFont(fontPlain);
//                    System.out.println("Printing Value : "+value);
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    drawCenteredString(g, value, x, y + valueHeight, cellWidth);
                }
            }
        }
        if (!isPrinting) {
            drawSelectedNormalCell(g);
        }
        setPreferredBounds();
    }

    private TWDecimalFormat getSymbolSpecificNumFormatter(String key) {
        TWDecimalFormat twoDecimals = new TWDecimalFormat("#,##0.00");
        TWDecimalFormat threeDecimals = new TWDecimalFormat("#,##0.000");
        if ((criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) ||
                (criteria == HeatInterface.CRITERIA_NUM_TRADES) ||
                (criteria == HeatInterface.CRITERIA_VOLUME)) {
            return numIntFormatter;
        } else if ((criteria == HeatInterface.CRITERIA_5_YEAR_GROWTH) ||
                (criteria == HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING) ||
                (criteria == HeatInterface.CRITERIA_PERC_CHANGE) ||
                (criteria == HeatInterface.CRITERIA_MONEY_FLOW) ||
                (criteria == HeatInterface.CRITERIA_PERC_GAIN)) {
            return numPcntFormatter;
        } else if (criteria == HeatInterface.CRITERIA_MARKET_CAP) {
            return numFloatSingleFormatter;
        } else {
            byte decimals = HeatInterface.getStockObject(key).getDecimalCount();
            switch (decimals) {
                case 2:
                    numFloatFormatter = twoDecimals;
                case 3:
                    numFloatFormatter = threeDecimals;
                default:
                    numFloatFormatter = twoDecimals;
            }
            return numFloatFormatter;
        }

    }

    private void drawSelectedNormalCell(Graphics g) {
        int selectedIndex = getSelectedIndex(selectedSymbol);
        if (horizontalTile) {
            if ((selectedIndex > -1) && (selectedIndex < heatStore.size())) {
                int x, y, row, col;
                col = selectedIndex % heatStore.size();
                row = 1;
                int width = 16;// (1070 - (heatStore.size())) / heatStore.size();
                if (width <= 16) {
                    width = 16;
                }
                x = (col + 1) * 2 + col * width;
                y = 2;//(row + 1) * 2 + row * 60 + 1;
                g.setColor(Color.MAGENTA);
                ((Graphics2D) g).setStroke(bsBorder);
                g.drawRect(x - 1, y - 1, width + 2, horizontalTileHeight + 2);
                ((Graphics2D) g).setStroke(bsNormal);
            }
        } else if (verticalTile) {
            if ((selectedIndex > -1) && (selectedIndex < heatStore.size())) {
                int x, y, row, col;
                col = 1;//selectedIndex % heatStore.size();
                row = selectedIndex % heatStore.size();// 1;
                int width = 16;// (1070 - (heatStore.size())) / heatStore.size();
                if (width <= 16) {
                    width = 16;
                }
                x = 2;//(col + 1) * 2 + col * width;
                y = (row + 1) * 2 + row * width;//2;//(row + 1) * 2 + row * 60 + 1;
                g.setColor(Color.MAGENTA);
                ((Graphics2D) g).setStroke(bsBorder);
                g.drawRect(x - 1, y - 1, horizontalTileHeight + 2, width + 2);
                ((Graphics2D) g).setStroke(bsNormal);
            }
        } else {
            if ((selectedIndex > -1) && (selectedIndex < heatStore.size())) {
                int x, y, row, col;
                col = selectedIndex % columns;
                row = selectedIndex / columns;
                x = (col + 1) * cellGap + col * cellWidth;
                y = (row + 1) * cellGap + row * cellHeight + TitleHeight;
                g.setColor(Color.MAGENTA);
                ((Graphics2D) g).setStroke(bsBorder);
                g.drawRect(x - 1, y - 1, cellWidth + 2, cellHeight + 2);
                ((Graphics2D) g).setStroke(bsNormal);
            }
        }
    }

    private void drawHiveCells(Graphics g, int w, boolean isPrinting) {
        if (heatStore.size() <= 0) return;
        HeatRecord hR;
        String value;
        double val;
        int ringCount = getRingLevel(heatStore.size());
        int cellCount = 3 * ringCount * (ringCount + 1);
        //int w = (3*ringCount+2)*RADIUS_A;
        int left = getPreferredSize().width / 2;
        int Top = Math.round((4f * ringCount + 2f) * RADIUS_B) / 2;
        int x, y, level, ringPos;
        int[] xArr = new int[6];
        int[] yArr = new int[6];
        Color fillColor = ((HeatRecord) heatStore.get(heatStore.size() - 1)).getColor();
        for (int i = 0; i < cellCount; i++) {
            level = getRingLevel(i + 1);
            ringPos = getPositionOnRing(level, i + 1);
            Point pt = getMidPoint(level, ringPos);
            x = left + pt.x;
            y = Top + pt.y;
            calculateCellBounds(x, y, xArr, yArr);

            if (i < heatStore.size()) {
                hR = (HeatRecord) heatStore.get(i);
                numFormatter = getSymbolSpecificNumFormatter(hR.getKey());
                if (!isPrinting) {
                    g.setColor(hR.getColor());
                    g.fillPolygon(xArr, yArr, 6);
                }
                g.setColor(Color.BLACK);
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g.drawPolygon(xArr, yArr, 6);

                if (isPrinting) {
                    g.setColor(Color.BLACK);
                } else {
                    g.setColor(hR.getFontColor());
                }
                g.setFont(fontBold);
                int yPos = y - Math.round(2 * RADIUS_B / 3f);
                drawCenteredString(g, hR.getSymbol(), x - RADIUS_A,
                        yPos + symbolHeight, cellWidth);
                //((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                val = hR.getValue(criteria);
                value = getDisplayValue(val);
                g.setFont(fontPlain);
                if ((criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) && (val > 0)) {
                    //drawCenteredStars(g, val, x-RADIUS_A, yPos+valueHeight-5, cellWidth);
                    g.setFont(starFont);
                    drawCenteredString(g, value, x - RADIUS_A, yPos + valueHeight, cellWidth);
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    g.setFont(fontPlain);
                } else {
                    g.setFont(fontPlain);
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    drawCenteredString(g, value, x - RADIUS_A, yPos + valueHeight, cellWidth);
                }
                //drawCenteredString(g, value, x-RADIUS_A, yPos+valueHeight, cellWidth);
            } else {
                if (!isPrinting) {
                    g.setColor(fillColor);
                    g.fillPolygon(xArr, yArr, 6);
                }
                g.setColor(Color.BLACK);
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g.drawPolygon(xArr, yArr, 6);
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            }
        }
        if (!isPrinting) {
            drawSelectedHiveCell(g);
        }
    }

    private void drawSelectedHiveCell(Graphics g) {
        int selectedIndex = getSelectedIndex(selectedSymbol);
        if ((selectedIndex > -1) && (selectedIndex < heatStore.size())) {
            int ringCount = getRingLevel(heatStore.size());
            //int cellCount = 3*ringCount*(ringCount+1);
            int left = getPreferredSize().width / 2;
            int Top = Math.round((4f * ringCount + 2f) * RADIUS_B) / 2;
            int x, y, level, ringPos;
            int[] xArr = new int[6];
            int[] yArr = new int[6];

            level = getRingLevel(selectedIndex + 1);
            ringPos = getPositionOnRing(level, selectedIndex + 1);
            Point pt = getMidPoint(level, ringPos);
            x = left + pt.x;
            y = Top + pt.y;
            calculateCellBounds(x, y, xArr, yArr);

            g.setColor(Color.MAGENTA);
            ((Graphics2D) g).setStroke(bsBorder);
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.drawPolygon(xArr, yArr, 6);
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
            ((Graphics2D) g).setStroke(bsNormal);
        }
    }

    private void calculateCellBounds(int x, int y, int[] xArr, int[] yArr) {
        float angle = 355f / 113f / 3f;
        for (int i = 0; i < xArr.length; i++) {
            xArr[i] = x + (int) Math.round(RADIUS_A * Math.cos(angle * i));
            yArr[i] = y + (int) Math.round(RADIUS_A * Math.sin(angle * i));
        }
    }

    private Point getMidPoint(int level, int ringPos) {
        int id = ringPos % (6 * level);
        int sector = id / level;
        int secterID = id % level;
        Point startP = getSectorBeginPoint(sector, level);
        if (secterID == 0) return startP;
        Point endP = getSectorBeginPoint((sector + 1) % 6, level);
        int x = Math.round((startP.x * (level - (float) secterID) + endP.x * secterID) / (float) level);
        int y = Math.round((startP.y * (level - (float) secterID) + endP.y * secterID) / (float) level);
        return new Point(x, y);
    }

    private Point getSectorBeginPoint(int sector, int level) {
        float angle = (60f * sector - 30f) * 355f / 113f / 180f;
        float radius = 2 * RADIUS_B * level;
        int x = (int) Math.round(radius * Math.cos(angle));
        int y = (int) Math.round(radius * Math.sin(angle));
        return new Point(x, y);
    }

    private int getRingLevel(int cellID) {
        for (int i = 1; i < 100; i++) {
            if (3 * i * (i + 1) >= cellID) {
                return i;
            }
        }
        return 100;
    }

    private int getPositionOnRing(int level, int cellID) {
        int i = level - 1;
        return cellID - 3 * i * (i + 1);
    }

    private String getDisplayValue(double val) {
        if ((val == HeatInterface.DEFAULT_DOUBLE_VALUE) || (Double.isInfinite(val)) || (Double.isNaN(val))) {
            if (criteria == HeatInterface.CRITERIA_BID_ASK_RATIO) {

                if (val == Float.POSITIVE_INFINITY)
                    return Language.getString("BID");
                else if (val == 0)
                    return Language.getString("OFFER");
                else if ((Double.isNaN(val)) || (val == -1))
                    return NA;
            } else if (criteria == HeatInterface.CRITERIA_MONEY_FLOW) {

                if (val == Float.POSITIVE_INFINITY)
                    return Language.getString("BID");
                else if (val == 0)
                    return Language.getString("OFFER");
                else if ((Double.isNaN(val)) || (val == -1))
                    return NA;
            } else {
                return NA;
            }
        }
        if (criteria == HeatInterface.CRITERIA_SNP_RANK) {
            return HeatInterface.getRankForMappedValue(val);
        } else if (criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) {
            int value = (int) Math.max(Math.min(Math.round(val), 5), 1);
            String s = "";
            for (int i = 0; i < value; i++) {
                s = s + Character.toString(charStar);
            }
            //System.out.println("s and value "+s+" and "+value);
            return s;
        } else if ((criteria == HeatInterface.CRITERIA_BID_ASK_RATIO) ||
                (criteria == HeatInterface.CRITERIA_MONEY_FLOW)) {
            if (val == 0)
                return Language.getString("OFFER");
            else if (val == -1)
                return Language.getString("NA");
            else
                return numFormatter.format(val);
        } else if (criteria == HeatInterface.CRITERIA_PERC_CHANGE) {
//            System.out.println("UUUUUUUUUUUUUUUUUUUUUUUUU");
//            System.out.println("Val :"+val);
            return numPcntFormatter.format(val);
//            return ""+Round(val,2);
        } else {
            return numFormatter.format(val);
        }
    }

    private void drawFooter(Graphics g, int footerTop) {
        int w = 0;
        switch (mapStyle) {
            case MAP_STYLE_HIVE:
                int ringCount = getRingLevel(heatStore.size());
                w = getPreferredSize().width;//(3*ringCount+2)*RADIUS_A;
                break;
            case MAP_STYLE_NORMAL:
            default:
                if (horizontalTile) {
                    w = heatStore.size() * cellGap + heatStore.size() * 16;
                    break;
                } else if (verticalTile) {
                    w = heatStore.size() * cellGap + heatStore.size() * 16;
                } else {
                    w = columns * cellGap + columns * cellWidth;
                    break;
                }
        }
        boxW = w / Math.max(10, heatColors.length + 2);
        boxH = 15;
        boxTop = footerTop + 10;
        boxLeft = (cellGap * 2 + w - boxW * heatColors.length) / 2;
        int strTop = footerTop + 38;

        int colorCnt = heatColors.length;
        if (verticalTile) {

            ((Graphics2D) g).translate(1, heatStore.size() * cellGap + heatStore.size() * 16);
            ((Graphics2D) g).rotate(Math.toRadians(270));
//            ((Graphics2D) g).rotate(Math.toRadians(90));
//            ((Graphics2D) g).translate(-10, -10);

//            for (int i = 0; i < colorCnt; i++) {
//            g.setColor(heatColors[i]);
//            g.fillRect(boxTop,boxLeft + boxW * i, boxH,  boxW);
//                System.out.println("Vertical : "+verticalTile);
//                System.out.println("x "+boxTop);
//                System.out.println("y "+(boxLeft +( boxW * i)));
//                System.out.println("w "+boxH);
//                System.out.println("h "+boxW);
//                System.out.println("ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
//
//        }
        }
//        else{
//          for (int i = 0; i < colorCnt; i++) {
//            g.setColor(heatColors[i]);
//            g.fillRect(boxLeft + boxW * i, boxTop, boxW, boxH);
//              System.out.println("Horizontal: "+verticalTile);
//              System.out.println("x "+(boxLeft + (boxW * i)));
//              System.out.println("y "+boxTop);
//              System.out.println("w "+boxW);
//              System.out.println("h "+boxH);
//
//              System.out.println("ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
//
//        }
//        }
        for (int i = 0; i < colorCnt; i++) {
            g.setColor(heatColors[i]);
            g.fillRect(boxLeft + boxW * i, boxTop, boxW, boxH);
        }

        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(Color.WHITE);
        String[] values = getBeginAndEndValues();
        if (criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) {
            g.setFont(starFont);
        } else {
            g.setFont(fontSmall);
        }
        if (maxVariation > 0) {
            g.drawString(values[0], boxLeft, strTop);
        }
        int strW = g.getFontMetrics().stringWidth(values[1]);
        g.drawString(values[1], boxLeft + boxW * heatColors.length - strW, strTop);
        g.setFont(fontSmall);
        drawCenteredString(g, HeatInterface.getCriteriaDescriptions()[criteria], boxLeft, strTop, boxW * heatColors.length);
        if (verticalTile) {
            ((Graphics2D) g).rotate(Math.toRadians(90));
            ((Graphics2D) g).translate(-1, -(heatStore.size() * cellGap + heatStore.size() * 16));
        }
    }

    private String[] getBeginAndEndValues() {
        String[] sa = new String[2];
        switch (criteria) {
            //-0.5<14.5<29.5
            case HeatInterface.CRITERIA_SNP_RANK:
                sa[0] = saSPRank[saSPRank.length - 1];
                sa[1] = saSPRank[0];
                break;
            //0.5<3<5.5
            case HeatInterface.CRITERIA_SNP_STAR_RATING:
                String s = strStar;
                sa[0] = strStar;
                sa[1] = strFiveStar;
                break;
            //-M<0<M
            case HeatInterface.CRITERIA_PE_RATIO:
            case HeatInterface.CRITERIA_5_YEAR_GROWTH:
            case HeatInterface.CRITERIA_PERC_CHANGE:
            case HeatInterface.CRITERIA_PERC_GAIN:
                sa[0] = formatNumber(-maxVariation);
                sa[1] = formatNumber(maxVariation);
                break;
            //-N<1<M
            case HeatInterface.CRITERIA_BETA: // midValue = 1f

            case HeatInterface.CRITERIA_PCT_RANGE:
            case HeatInterface.CRITERIA_RANGE:
            case HeatInterface.CRITERIA_PCT_SPREAD:
            case HeatInterface.CRITERIA_SPREAD:
                sa[0] = formatNumber(midValue - maxVariation);
                sa[1] = formatNumber(midValue + maxVariation);
                break;
            //range - one color
            case HeatInterface.CRITERIA_MARKET_CAP:
            case HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING:
            case HeatInterface.CRITERIA_PER:
            case HeatInterface.CRITERIA_NUM_TRADES:
            case HeatInterface.CRITERIA_VOLUME:
                sa[0] = formatNumber(midValue);
                sa[1] = formatNumber(midValue + maxVariation);
                break;
            case HeatInterface.CRITERIA_BID_ASK_RATIO:
            case HeatInterface.CRITERIA_MONEY_FLOW:
                sa[0] = formatNumber(midValue);
                sa[1] = formatNumber(midValue + maxVariation);
                break;
            //range - two color
            case HeatInterface.CRITERIA_DIVIDEND_YIELD:
            default:
                sa[0] = formatNumber(midValue - maxVariation);
                sa[1] = formatNumber(midValue + maxVariation);
                break;
        }
        return sa;
    }

    private String formatNumber(double value) {
        if ((Double.isInfinite(value)) || (Double.isNaN(value))) {
            return NA;
        } else {
            return numFormatter.format(value);
        }
    }

    private void drawBackGround(Graphics g) {
//		int w;
//		switch (mapStyle){
//			case MAP_STYLE_HIVE:
//				int ringCount = getRingLevel(heatStore.size());
//				w = cellGap*2 + (3*ringCount+2)*RADIUS_A;
//				break;
//			case MAP_STYLE_NORMAL:
//			default:
//				w = (columns+2)*cellGap + columns*cellWidth;
//				break;
//		}
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getFooterTop() + FooterHeight + cellGap);
    }

    public void setPreferredBounds() {
        int prefW = 0;
        int prefH = 0;
//        if(heatStore.size()==0){
//             String[] sa={"TDWL~TDWL","DFM~DFM"};
//             updateHeatStore(sa);
//
//        }
        switch (0) {
            case MAP_STYLE_HIVE:
                int ringCount = getRingLevel(heatStore.size());
                prefW = cellGap * 2 + (3 * ringCount + 2) * RADIUS_A;
                prefH = cellGap * 2 + Math.round((4f * ringCount + 2f) * RADIUS_B) + TitleHeight + FooterHeight;
                break;
            case MAP_HOR_TILE:
//                int ringCount1 = getRingLevel(heatStore.size());
//                prefW = cellGap * 2 + (heatStore.size()) * 16;
//                prefH = cellGap * 2 + Math.round((4f * ringCount1 + 2f) * RADIUS_B) + TitleHeight + FooterHeight;
//                break;
            case MAP_STYLE_NORMAL:
                if (horizontalTile) {
                    int elements = heatStore.size();
//                    if(elements==0){
//                       elements=93;
//                    }
                    prefW = (cellGap * (elements) - 1) + ((elements) * 16);
                    prefH = cellGap * 2 + horizontalTileHeight;  //+ FooterHeight  // Math.round((4f * ringCount1 + 2f) * RADIUS_B) cellGap * 2 +    + TitleHeight
                    break;
                } else if (verticalTile) {
                    int elements = heatStore.size();
//                    if(elements==0){
//                       elements=93;
//                    }
                    prefW = cellGap * 2 + verticalTileWidth;//+ FooterHeight; ; //cellGap * ((heatStore.size())-1) + (heatStore.size()) * 16;        + TitleHeight
                    prefH = cellGap * ((elements) - 1) + (elements) * 16;//cellGap * 2 + 80 + TitleHeight + FooterHeight;    // Math.round((4f * ringCount1 + 2f) * RADIUS_B)
                    break;
                } else {
                    prefW = cellGap * (columns + 1) + cellWidth * columns;
                    int rows = (heatStore.size() - 1) / columns;
//                    if(rows==0){
//                       rows=(93 - 1) / columns;
//                    }
//                    System.out.println("ROWS : "+rows+" further : "+rows+" , "+columns);
                    prefH = cellGap * (rows + 1) + cellHeight * rows + TitleHeight + FooterHeight;
                    break;
                }
            default:
                prefW = cellGap * (columns + 1) + cellWidth * columns;
                int rows = (heatStore.size() - 1) / columns;
                prefH = cellGap * (rows + 1) + cellHeight * rows + TitleHeight + FooterHeight;
                break;
        }
//        prefW = Math.max(prefW, HeatMapFrame.MIN_WIDTH);
        setPreferredSize(new Dimension(prefW, prefH));
    }

    private void drawCenteredString(Graphics g, String str, int left, int y, int w) {
        int strW = g.getFontMetrics().stringWidth(str);
        int x = left + (w - strW) / 2;
        if (((w - strW) / 2) <= 3) { // not enough space. use smaller font
            g.setFont(fontBoldSmall9);
            strW = g.getFontMetrics().stringWidth(str);
            x = left + (w - strW) / 2;
            if (((w - strW) / 2) <= 3) { // not enough space. use smaller font
                g.setFont(fontBoldSmall7);
                strW = g.getFontMetrics().stringWidth(str);
                x = left + (w - strW) / 2;
            }
        }
        g.drawString(str, x, y);
    }

    public void calculateHeatValues(DynamicArray htStore) {
        if (htStore.size() > 0) {
            HeatRecord hR;
            calcMaxVariationAndMidValue(htStore);
            int index = 0;
            for (int i = 0; i < htStore.size(); i++) {
                hR = (HeatRecord) htStore.get(i);
                if (maxVariationBuffer > 0) {
                    index = getClosestColorIndex(hR.getValue(criteria));
                    if (index == HeatInterface.DEFAULT_DOUBLE_VALUE) {
                        hR.setColor(Color.WHITE);
                        hR.setFontColor(Color.GRAY);
                        continue;
                    }
                    index = Math.min(Math.max(0, index), heatColors.length - 1);
                    //(int)Math.max(Math.ceil((hR.getValue(criteria)+maxVariationBuffer)*(2f*colorCount[criteria]+1f)/(2f*maxVariationBuffer))-1, 0);
                    hR.setColor(heatColors[index]);
                    if (((isTwoColor) && (index < DARK_COUNT)) || (index >= heatColors.length - DARK_COUNT)) {
                        if ((criteria == HeatInterface.CRITERIA_SNP_RANK) && (index == 0)) {
                            hR.setFontColor(Color.GRAY);
                        } else {
                            hR.setFontColor(Color.WHITE);
                        }
                    } else {
                        hR.setFontColor(Color.BLACK);
                    }
                } else {
                    hR.setColor(Color.WHITE);
                    hR.setFontColor(Color.GRAY);
                }
            }
        }
    }

    private int getClosestColorIndex(double value) {
        if (value == HeatInterface.DEFAULT_DOUBLE_VALUE) {
            return (int) Math.round(value);
        }
        switch (criteria) {
            //0.5<3<5.5
            case HeatInterface.CRITERIA_SNP_STAR_RATING:
                return (int) Math.round(value) - 1;
            //-M<0<M
            case HeatInterface.CRITERIA_PE_RATIO:
            case HeatInterface.CRITERIA_5_YEAR_GROWTH:
            case HeatInterface.CRITERIA_PERC_CHANGE:
            case HeatInterface.CRITERIA_PERC_GAIN:

                return (int) Math.max(Math.ceil((value + maxVariationBuffer) * (2f * colorCount[criteria] + 1f) / (2f * maxVariationBuffer)) - 1, 0);
            //-N<1<M
            case HeatInterface.CRITERIA_BETA:

            case HeatInterface.CRITERIA_PCT_RANGE:
            case HeatInterface.CRITERIA_RANGE:
            case HeatInterface.CRITERIA_PCT_SPREAD:
            case HeatInterface.CRITERIA_SPREAD:
                if (Double.isInfinite(value) || Double.isNaN(value))
                    value = 0;
                return (int) Math.max(Math.ceil((value - midBuffer + maxVariationBuffer) * (2f * colorCount[criteria] + 1f) / (2f * maxVariationBuffer)) - 1, 0);
            //range - one color
            case HeatInterface.CRITERIA_MARKET_CAP:
            case HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING:
            case HeatInterface.CRITERIA_PER:
            case HeatInterface.CRITERIA_NUM_TRADES:
            case HeatInterface.CRITERIA_VOLUME:
                return (int) Math.max(Math.ceil((value - midBuffer) * (colorCount[criteria] + 1f) / (maxVariationBuffer)) - 1, 0);
            case HeatInterface.CRITERIA_BID_ASK_RATIO:
            case HeatInterface.CRITERIA_MONEY_FLOW:
                //return (int)Math.max(Math.ceil((value-midBuffer+maxVariationBuffer)*(colorCount[criteria]+1f)/(2f*maxVariationBuffer))-1, 0);
                if (value < 1) {
                    return (int) Math.ceil(11 * value) - 1;
                } else {
                    return (int) Math.ceil((((12 * value) - 12) / (maxVariation - 1)) + 12) - 1;
//                    return (int)Math.ceil(((6 * value) + (5*maxVariation) - 12)/(maxVariation -1));
                }
                //range - two color
            case HeatInterface.CRITERIA_SNP_RANK:
            case HeatInterface.CRITERIA_DIVIDEND_YIELD:
            default:
                return (int) Math.max(Math.ceil((value - midBuffer + maxVariationBuffer) * (colorCount[criteria] + 1f) / (2f * maxVariationBuffer)) - 1, 0);

        }
    }

    private void calcMaxVariationAndMidValue(DynamicArray htStore) {
        HeatRecord hR;
        double val, min = Float.MAX_VALUE, max = Float.MIN_VALUE;
        midBuffer = 0;
        maxVariationBuffer = 0;
        if (criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) {
            //1<3<5
            //case CRITERIA_SNP_STAR_RATING:
            midBuffer = 3f;
            maxVariationBuffer = 2.5f;
        } else if (criteria == HeatInterface.CRITERIA_SNP_RANK) {
            //1<3<5
            //case CRITERIA_SNP_STAR_RATING:
            midBuffer = 14.5f;
            maxVariationBuffer = 15f;
        } else {
            for (int i = 0; i < htStore.size(); i++) {
                hR = (HeatRecord) htStore.get(i);
                switch (criteria) {
                    //-M<0<M
                    case HeatInterface.CRITERIA_PE_RATIO:
                    case HeatInterface.CRITERIA_5_YEAR_GROWTH:
                    case HeatInterface.CRITERIA_PERC_CHANGE:
                    case HeatInterface.CRITERIA_PERC_GAIN:
                        val = Math.abs(hR.getValue(criteria));
                        maxVariationBuffer = Math.max(maxVariationBuffer, val);
                        break;
                    //-N<1<M
                    case HeatInterface.CRITERIA_BETA:
                    case HeatInterface.CRITERIA_PCT_RANGE:
                    case HeatInterface.CRITERIA_RANGE:
                    case HeatInterface.CRITERIA_PCT_SPREAD:
                    case HeatInterface.CRITERIA_SPREAD:
                        val = Math.abs(hR.getValue(criteria) - 1f);
                        if (Double.isInfinite(val) || Double.isNaN(val))
                            val = 0;
                        maxVariationBuffer = Math.max(maxVariationBuffer, val);
                        midBuffer = 1f;
                        break;
                    //range - one color
                    case HeatInterface.CRITERIA_MARKET_CAP:
                    case HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING:
                    case HeatInterface.CRITERIA_VOLUME:
                    case HeatInterface.CRITERIA_PER:
                    case HeatInterface.CRITERIA_NUM_TRADES:
                        val = hR.getValue(criteria);
                        max = Math.max(max, val);
                        min = Math.min(min, val);
                        maxVariationBuffer = (max - min);
                        midBuffer = 1;
                        break;
                    case HeatInterface.CRITERIA_BID_ASK_RATIO:
                    case HeatInterface.CRITERIA_MONEY_FLOW:
                        val = hR.getValue(criteria);

                        if ((!Double.isInfinite(val)) && (!Double.isNaN(val)))
                            max = Math.max(max, val);
                        //if (max > 100) max = 100;
                        min = 0;
                        maxVariationBuffer = (max - min);
                        midBuffer = min;
                        break;
                    //range - two color
                    //case CRITERIA_SNP_RANK:
                    case HeatInterface.CRITERIA_DIVIDEND_YIELD:
                    default:
                        val = hR.getValue(criteria);
                        max = Math.max(max, val);
                        min = Math.min(min, val);
                        maxVariationBuffer = (max - min) / 2f;
                        midBuffer = min + maxVariationBuffer;
                        break;
                }
            }
        }
    }

    private void setHeatColorCount() {
        colorCount[HeatInterface.CRITERIA_SNP_RANK] = 7;
        colorCount[HeatInterface.CRITERIA_SNP_STAR_RATING] = 2;
        colorCount[HeatInterface.CRITERIA_PE_RATIO] = 11;
        colorCount[HeatInterface.CRITERIA_DIVIDEND_YIELD] = 11;
        colorCount[HeatInterface.CRITERIA_5_YEAR_GROWTH] = 11;
        colorCount[HeatInterface.CRITERIA_BETA] = 11;
        colorCount[HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING] = 11;
        colorCount[HeatInterface.CRITERIA_PERC_CHANGE] = 11;
        colorCount[HeatInterface.CRITERIA_MARKET_CAP] = 11;
        colorCount[HeatInterface.CRITERIA_PERC_GAIN] = 11;
        colorCount[HeatInterface.CRITERIA_BID_ASK_RATIO] = 11;
        colorCount[HeatInterface.CRITERIA_SPREAD] = 11;
        colorCount[HeatInterface.CRITERIA_RANGE] = 11;
        colorCount[HeatInterface.CRITERIA_PCT_RANGE] = 11;
        colorCount[HeatInterface.CRITERIA_PCT_SPREAD] = 11;
        colorCount[HeatInterface.CRITERIA_PER] = 11;
        colorCount[HeatInterface.CRITERIA_NUM_TRADES] = 11;
        colorCount[HeatInterface.CRITERIA_VOLUME] = 11;
        colorCount[HeatInterface.CRITERIA_MONEY_FLOW] = 11;
    }

    private void setHeatColors() {
        int r, g, b, rm, gm, bm;
        float factor;
        int colorCnt = colorCount[criteria];
        if ((criteria == HeatInterface.CRITERIA_MARKET_CAP) ||
                (criteria == HeatInterface.CRITERIA_PER) ||
                (criteria == HeatInterface.CRITERIA_NUM_TRADES) ||
                (criteria == HeatInterface.CRITERIA_VOLUME) ||
                (criteria == HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING)) {
            heatColors = new Color[colorCnt + 1];
            heatColors[0] = Color.WHITE;
            r = upColor.getRed();
            g = upColor.getGreen();
            b = upColor.getBlue();
            for (int i = 0; i < colorCnt; i++) {
                factor = i / (float) colorCnt;
                rm = Math.round(r + (255 - r) * factor);
                gm = Math.round(g + (255 - g) * factor);
                bm = Math.round(b + (255 - b) * factor);
                heatColors[colorCnt - i] = new Color(rm, gm, bm);
            }
        } else {
            r = downColor.getRed();
            g = downColor.getGreen();
            b = downColor.getBlue();
            heatColors = new Color[2 * colorCnt + 1];
            for (int i = 0; i < colorCnt; i++) {
                factor = i / (float) colorCnt;
                rm = Math.round(r + (255 - r) * factor);
                gm = Math.round(g + (255 - g) * factor);
                bm = Math.round(b + (255 - b) * factor);
                //System.out.println("rm="+rm+", gm="+gm+", bm="+bm+", factor="+factor+", i="+i);
                heatColors[i] = new Color(rm, gm, bm);
            }
            heatColors[colorCnt] = Color.WHITE;
            r = upColor.getRed();
            g = upColor.getGreen();
            b = upColor.getBlue();
            for (int i = 0; i < colorCnt; i++) {
                factor = i / (float) colorCnt;
                rm = Math.round(r + (255 - r) * factor);
                gm = Math.round(g + (255 - g) * factor);
                bm = Math.round(b + (255 - b) * factor);
                heatColors[2 * colorCnt - i] = new Color(rm, gm, bm);
            }
        }
        //setting dark count
        DARK_COUNT = colorCnt / 2;
    }

    public void setTheme(Color uColor, Color dColor) {
        synchronized (lock) {
            upColor = uColor;
            downColor = dColor;
            setHeatColors();
            calculateHeatValues(heatStore);
            repaint();
        }
    }

    public void setSorting(byte mode) {
        synchronized (lock) {
            hComparator.setComparisonMode(mode);
        }
    }

    public void updateSelectedIndex(int xTip, int yTip) {
        synchronized (lock) {
            selectedSymbol = null;
            //     if (yTip < getFooterTop()) {
            int noOfCells = heatStore.size();
            int width = 16;//(1070 - (heatStore.size())) / heatStore.size();
            if (width <= 16) {
                width = 16;
            }
            Rectangle rect;
            switch (mapStyle) {
                case MAP_STYLE_HIVE:
                    int ringCount = getRingLevel(noOfCells);
                    int left = getWidth() / 2;//(3*ringCount+2)*RADIUS_A/2;
                    int Top = Math.round((4f * ringCount + 2f) * RADIUS_B) / 2;
                    int x, y, level, ringPos;
                    Polygon poly;
                    int[] xArr = new int[6];
                    int[] yArr = new int[6];
                    for (int i = 0; i < noOfCells; i++) {
                        level = getRingLevel(i + 1);
                        ringPos = getPositionOnRing(level, i + 1);
                        Point pt = getMidPoint(level, ringPos);
                        x = left + pt.x;
                        y = Top + pt.y;
                        calculateCellBounds(x, y, xArr, yArr);
                        poly = new Polygon(xArr, yArr, 6);
                        if (poly.contains(xTip, yTip)) {
                            selectedSymbol = ((HeatRecord) heatStore.get(i)).getKey();
                            break;
                        }
                        poly = null;
                    }
                    break;
                case MAP_HOR_TILE:
                    int colx, rowx, xx1, yy1;
                    for (int i = 0; i < noOfCells; i++) {
                        colx = i % heatStore.size();
                        rowx = 1;

                        xx1 = (colx + 1) * 2 + colx * width;
                        yy1 = 2;//(row + 1) * 2 + row * 60 + 1;
                        rect = new Rectangle(xx1, yy1, width, 60);
                        if (rect.contains(xTip, yTip)) {
                            selectedSymbol = ((HeatRecord) heatStore.get(i)).getKey();
                            System.out.println("TEST : " + selectedSymbol);
                            break;
                        }
                        rect = null;
                    }
                    break;
                case MAP_STYLE_NORMAL:
                default:
                    int col, row, xx, yy;
                    for (int i = 0; i < noOfCells; i++) {
                        if (horizontalTile) {
                            colx = i % heatStore.size();
                            rowx = 1;

                            xx1 = (colx + 1) * 2 + colx * width;
                            yy1 = 2;//(row + 1) * 2 + row * 60 + 1;
                            rect = new Rectangle(xx1, yy1, width, horizontalTileHeight);
                            if (rect.contains(xTip, yTip)) {
                                selectedSymbol = ((HeatRecord) heatStore.get(i)).getKey();
                                break;
                            }
                            rect = null;
                        } else if (verticalTile) {
                            colx = 1;//i % heatStore.size();
                            rowx = i % heatStore.size();//1;

                            xx1 = 2;//(colx + 1) * 2 + colx * width;
                            yy1 = (rowx + 1) * 2 + rowx * width;// 2;//(row + 1) * 2 + row * 60 + 1;
                            rect = new Rectangle(xx1, yy1, verticalTileWidth, width);
//                                System.out.println("xtip "+xTip);
//                                System.out.println("ytip "+yTip);
//                                System.out.println("x "+xx1);
//                                System.out.println("y "+yy1);
//                                System.out.println("w "+verticalTileWidth);
//                                System.out.println("h "+width);
                            if (rect.contains(xTip, yTip)) {
                                selectedSymbol = ((HeatRecord) heatStore.get(i)).getKey();
                                break;
                            }
                            rect = null;
                        } else {
                            col = i % columns;
                            row = i / columns;
                            xx = (col + 1) * cellGap + col * cellWidth;
                            yy = (row + 1) * cellGap + row * cellHeight + TitleHeight;
                            rect = new Rectangle(xx, yy, cellWidth, cellHeight);
                            if (rect.contains(xTip, yTip)) {
                                selectedSymbol = ((HeatRecord) heatStore.get(i)).getKey();
                                break;
                            }
                            rect = null;
                        }
                    }
                    break;

            }
            //}
        }
    }

    public boolean updateToolTip(HeatToolTip toolTip, int xTip, int yTip) {
        try {
            boolean result = false;
            Rectangle rect;
            synchronized (lock) {
                int noOfCells = heatStore.size();
                int footerTop = getFooterTop();
                if (yTip > footerTop) {
                    for (int i = 0; i < heatColors.length; i++) {
                        rect = new Rectangle(boxLeft + boxW * i, boxTop, boxW, boxH);
                        if (rect.contains(xTip, yTip)) {
                            setHeatTip(toolTip, i);
                            result = true;
                            break;
                        }
                    }
                }
//				else{
//					switch (mapStyle){
//						case MAP_STYLE_HIVE:
//							int ringCount = getRingLevel(noOfCells);
//							int left = getWidth()/2;//(3*ringCount+2)*RADIUS_A/2;
//							int Top = Math.round((4f*ringCount+2f)*RADIUS_B)/2;
//							int x, y, level, ringPos;
//							Polygon poly;
//							int[] xArr = new int[6];
//							int[] yArr = new int[6];
//							for (int i=0; i<noOfCells; i++) {
//								level = getRingLevel(i+1);
//								ringPos = getPositionOnRing(level, i+1);
//								Point pt = getMidPoint(level, ringPos);
//								x = left + pt.x;
//								y = Top + pt.y;
//								calculateCellBounds(x, y, xArr, yArr);
//								poly = new Polygon(xArr, yArr, 6);
//								if (poly.contains(xTip, yTip)){
//									setSymbolTip(toolTip, i);
//									result = true;
//									break;
//								}
//								poly = null;
//							}
//							break;
//						case MAP_STYLE_NORMAL:
//						default:
//							int col, row, xx, yy;
//							for (int i=0; i<noOfCells; i++) {
//								col = i%columns;
//								row = i/columns;
//								xx = (col+1)*cellGap + col*cellWidth;
//								yy = (row+1)*cellGap + row*cellHeight + TitleHeight;
//								rect = new Rectangle(xx, yy, cellWidth, cellHeight);
//								if (rect.contains(xTip, yTip)){
//									setSymbolTip(toolTip, i);
//									result = true;
//									break;
//								}
//								rect = null;
//							}
//							break;
//					}
//				}
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private int getFooterTop() {
        switch (mapStyle) {
            case MAP_STYLE_HIVE:
                int ringCount = getRingLevel(heatStore.size());
                return 2 * cellGap + Math.round((4f * ringCount + 2f) * RADIUS_B) + TitleHeight;
            case MAP_STYLE_NORMAL:
            default:
                int rows = (heatStore.size() - 1) / columns + 1;
                return rows * cellGap + rows * cellHeight + TitleHeight;
        }
    }

    private void setHeatTip(HeatToolTip toolTip, int index) {
        double[] faValues = getLowerAndUpperValues(index);
        toolTip.setHeatTip(faValues[0], faValues[1], criteria);
    }

    private double[] getLowerAndUpperValues(int index) {
        double[] fa = new double[2];
        switch (criteria) {
            //0<14<28
            case HeatInterface.CRITERIA_SNP_RANK:
                fa[0] = index * 2f * maxVariation / (heatColors.length) - maxVariation + midValue;
                fa[1] = (index + 1) * 2f * maxVariation / (heatColors.length) - maxVariation + midValue;
                break;
            //0.5<3<5.5
            case HeatInterface.CRITERIA_SNP_STAR_RATING:
                fa[0] = index + 1f;
                fa[1] = index + 1f;
                break;
            //-M<0<M
            case HeatInterface.CRITERIA_PE_RATIO:
            case HeatInterface.CRITERIA_5_YEAR_GROWTH:
            case HeatInterface.CRITERIA_PERC_CHANGE:
            case HeatInterface.CRITERIA_PERC_GAIN:
                fa[0] = index * 2f * maxVariation / (heatColors.length) - maxVariation;
                fa[1] = (index + 1) * 2f * maxVariation / (heatColors.length) - maxVariation;
                break;
            //-N<1<M
            case HeatInterface.CRITERIA_BETA: // midValue = 1f

            case HeatInterface.CRITERIA_PCT_RANGE:
            case HeatInterface.CRITERIA_RANGE:
            case HeatInterface.CRITERIA_PCT_SPREAD:
            case HeatInterface.CRITERIA_SPREAD:
                fa[0] = index * 2f * maxVariation / (heatColors.length) - maxVariation + midValue;
                fa[1] = (index + 1) * 2f * maxVariation / (heatColors.length) - maxVariation + midValue;
                break;
            //range - one color
            case HeatInterface.CRITERIA_MARKET_CAP:
            case HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING:
            case HeatInterface.CRITERIA_PER:
            case HeatInterface.CRITERIA_NUM_TRADES:
            case HeatInterface.CRITERIA_VOLUME:
                fa[0] = index * maxVariation / (heatColors.length) + midValue;
                fa[1] = (index + 1) * maxVariation / (heatColors.length) + midValue;
                break;
            case HeatInterface.CRITERIA_BID_ASK_RATIO:
            case HeatInterface.CRITERIA_MONEY_FLOW:
                if (index < 11) {
                    fa[0] = index / (float) 11;
                    fa[1] = (index + 1) / (float) 11;
                } else {
                    fa[0] = (((maxVariation - 1) * (index - 11)) / (float) 12) + 1;
                    fa[1] = (((maxVariation - 1) * (index - 10)) / (float) 12) + 1;
                }
                break;
            //range - two color
            case HeatInterface.CRITERIA_DIVIDEND_YIELD:
            default:
                fa[0] = index * 2f * maxVariation / (heatColors.length) - maxVariation + midValue;
                fa[1] = (index + 1) * 2f * maxVariation / (heatColors.length) - maxVariation + midValue;
                break;
        }
        return fa;
    }

    private void setSymbolTip(HeatToolTip toolTip, int index) {
        HeatRecord hR = (HeatRecord) heatStore.get(index);
        String key = hR.getKey();
        toolTip.setSymbolTip(key);
    }

    public Color[] getToolTipColors() {
        try {
            Color[] clArr = new Color[3];
            synchronized (lock) {
                int colorCnt = colorCount[criteria];
                //System.out.println("colorCnt "+colorCnt+" heatColors.length "+heatColors.length);
                if (colorCnt > 3) {
                    clArr[0] = heatColors[colorCnt + 1];
                    clArr[1] = heatColors[colorCnt + 2];
                    clArr[2] = heatColors[heatColors.length - 1];
                } else if (colorCnt > 1) {
                    clArr[0] = heatColors[colorCnt];
                    clArr[1] = heatColors[colorCnt + 1];
                    clArr[2] = heatColors[heatColors.length - 1];
                } else {
                    return null;
                }
            }
            return clArr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public byte getCriteria() {
        return criteria;
    }

    public void setCriteria(byte c) {
        synchronized (lock) {
            criteria = c;
            hComparator.setCriteria(c);
            setIsTwoColor();
            setNumFormatter();
            setHeatColors();
            //Arrays.sort(heatStore.getList().toArray(), hComparator);
            //calculateHeatValues(heatStore);
        }
    }

    private void setNumFormatter() {
        if ((criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) ||
                (criteria == HeatInterface.CRITERIA_NUM_TRADES) ||
                (criteria == HeatInterface.CRITERIA_VOLUME)) {
            numFormatter = numIntFormatter;
        } else if ((criteria == HeatInterface.CRITERIA_5_YEAR_GROWTH) ||
                (criteria == HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING) ||
                (criteria == HeatInterface.CRITERIA_PERC_CHANGE) ||
                (criteria == HeatInterface.CRITERIA_MONEY_FLOW) ||
                (criteria == HeatInterface.CRITERIA_PERC_GAIN)) {
            numFormatter = numPcntFormatter;
        } else if (criteria == HeatInterface.CRITERIA_MARKET_CAP) {
            numFormatter = numFloatSingleFormatter;
        } else {
            numFormatter = numFloatFormatter;
        }
    }

    private void setIsTwoColor() {
        if ((criteria == HeatInterface.CRITERIA_MARKET_CAP) ||
                (criteria == HeatInterface.CRITERIA_NUM_TRADES) ||
                (criteria == HeatInterface.CRITERIA_VOLUME) ||
                (criteria == HeatInterface.CRITERIA_PER) ||
                (criteria == HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING)) {
            isTwoColor = false;
        } else {
            isTwoColor = true;
        }
    }

    public HeatComparator getComparator() {
        return hComparator;
    }

    public byte getMapStyle() {
        return mapStyle;
    }

    public void setMapStyle(byte style) {
        synchronized (lock) {
            mapStyle = style;
            switch (mapStyle) {
                case MAP_STYLE_HIVE:
                    RADIUS_A = 28;
                    RADIUS_B = RADIUS_A * (float) Math.sqrt(3) / 2f;
                    cellWidth = RADIUS_A * 2;
                    break;
                case MAP_STYLE_NORMAL:
                default:
                    RADIUS_A = 25;
                    cellWidth = RADIUS_A * 2;
                    break;
            }
        }
        setPreferredBounds();
        repaint();
    }

    public int getColumnCount() {
        return columns;
    }

    public void setColumnCount(int cols) {
        columns = cols;
        setPreferredBounds();
    }

    public String getSelectedSymbol() {
        return selectedSymbol;
    }

    public void setSelectedSymbol(String s) {
        selectedSymbol = s;
    }

    public int getSelectedIndex(String selectedSymbol) {
        HeatRecord hR;
        if (selectedSymbol != null) {
            if (true) {
                for (int i = 0; i < heatStore.size(); i++) {
                    hR = (HeatRecord) heatStore.get(i);
                    if (selectedSymbol.equals(hR.getKey())) {
                        return i;
                    }
                }
            } else {
                for (int i = 0; i < heatStore.size(); i++) {
                    hR = (HeatRecord) heatStore.get(i);
                    if (selectedSymbol.equals(hR.getKey())) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    private void drawCenteredStars(Graphics g, float count, int x, int y, int w) {
        //System.out.println("countcountcount "+count);
        int cnt = Math.round(count);
        int[] xa = new int[5], ya = new int[5];
        int[] xArr = getCenterArr(cnt, x + w / 2);
        for (int i = 0; i < xArr.length; i++) {
            //calcStarPoints(xArr[i], y, xa, ya);
            //g.setColor(Color.RED);
            //g.fillPolygon(xa, ya, 5);
            //g.drawOval(xArr[i]-4, y-4, 8, 8);
            g.drawImage(imgStar.getImage(), xArr[i] - 4, y, this);
        }
    }

    private void calcStarPoints(int x, int y, int[] xa, int[] ya) {
        for (int i = 0; i < xa.length; i++) {
            xa[i] = x + (int) Math.round(STAR_RADIUS * Math.cos(Math.toRadians(18 + 144 * i)));
            ya[i] = y - (int) Math.round(STAR_RADIUS * Math.sin(Math.toRadians(18 + 144 * i)));
        }
    }

    private int[] getCenterArr(int cnt, int x) {
        int[] iaX = new int[cnt];
        int beginX = x - HALF_GAP * (cnt - 1);
        for (int i = 0; i < cnt; i++) {
            iaX[i] = beginX + HALF_GAP * 2 * i;
        }
        return iaX;
    }

    public void paintOnThePrinterGraphics(Graphics g, int w, int h, boolean isPrinting) {
        g.setColor(Color.black);
        drawPrintHeader(g);
        ((Graphics2D) g).translate(0, 50);
        if (heatStore.size() > 0) {
            //((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            switch (mapStyle) {
                case MAP_STYLE_HIVE:
                    drawHiveCells(g, w, isPrinting);
                    break;
                case MAP_STYLE_NORMAL:
                default:
                    drawNormalCells(g, w, isPrinting);
                    break;
            }

        }
        this.repaint();
    }

    private void drawPrintHeader(Graphics g) {
        g.setFont(Theme.getDefaultFont(Font.BOLD, 16));
        g.drawString(Language.getString("HEATMAP_TITLE"), 0, 15);
        g.setFont(Theme.getDefaultFont(Font.BOLD, 12));
        g.drawString(watchlistName + " - " +
                HeatMapFrame.getStringFromCriteria(criteria) + " (" +
                saCompareMode[hComparator.getComparisonMode()] + ")", 0, 42);
    }

    public void drawPrintFooter(Graphics2D g, int w, int h) {
        //int borderWidth = 0;
        g.setFont(Theme.getDefaultFont(Font.BOLD, 9));
        int footerHeight = 15;
        /*Image imgUniquotes = Toolkit.getDefaultToolkit().getImage("images/common/print_footer_"+Language.getLanguageTag()+".gif");
          try {
              MediaTracker comp = new MediaTracker(this);
              comp.addImage(imgUniquotes, 0);
              comp.waitForID(0);
              //comp.removeImage(imgUniquotes);
              comp = null;
          }
          catch (InterruptedException ex) {
          }*/

        String tag = Language.getString("PRINT_FOOTER_CLIENT_TITLE");
        //int imgLen = imgUniquotes.getWidth(this)+5;
        //int tagLen = imgLen+g.getFontMetrics().stringWidth(tag);
        int tagLeft = 0;//borderWidth;//Math.max((w-tagLen)/2, borderWidth);
//		g.drawImage(imgUniquotes, tagLeft, h-footerHeight, this);
        //g.drawString((Language.getString("PRINT_FOOTER_CLIENT_TITLE"),tagLeft, h-footerHeight);
        g.setColor(Color.GRAY);
        g.drawString(tag, tagLeft, h - 4);
        int availableW = w - (tagLeft + 5);
        //SimpleDateFormat dateFormatter = new SimpleDateFormat ("dd MM yyyy - HH:mm:ss");
        //tag = dateFormatter.format(new Date(Client.getInstance().getMarketTime()));
        tag = ChartInterface.getFormattedPrintingTime();
        int tagLen = g.getFontMetrics().stringWidth(tag);
        if (tagLen <= availableW) {
            g.drawString(tag, w - tagLen, h - 4);
        }
    }

    public int getPrintableHeight() {
        int TITLE_HT = 70; //30 for title, 20 for criteria, 20 for bottom logo
        switch (mapStyle) {
            case MAP_STYLE_HIVE:
                int ringCount = getRingLevel(heatStore.size());
                return 2 * cellGap + Math.round((4f * ringCount + 2f) * RADIUS_B) + TITLE_HT;
            case MAP_STYLE_NORMAL:
            default:
                int rows = (heatStore.size() - 1) / columns + 1;
                return rows * cellGap + rows * cellHeight + TITLE_HT;
        }
    }

    public void setWatchlistName(String name) {
        watchlistName = name;
    }

    public Footer getFooter() {
        Footer footer = new Footer();
        return footer;
    }

    private void drawTiledString(Graphics g, String str, int left, int y, int w) {
//        int strW = g.getFontMetrics().stringWidth(str);
//        int x = left + (w - strW) / 2;
//        if (((w - strW) / 2) <= 3) { // not enough space. use smaller font
//            g.setFont(fontBoldSmall9);
//            strW = g.getFontMetrics().stringWidth(str);
//            x = left + (w - strW) / 2;
//            if (((w - strW) / 2) <= 3) { // not enough space. use smaller font
//                g.setFont(fontBoldSmall7);
//                strW = g.getFontMetrics().stringWidth(str);
//                x = left + (w - strW) / 2;
//            }
//        }
//        System.out.println("Left : "+left+" y : "+y);
//        ((Graphics2D) g).translate(left, y);
//        ((Graphics2D) g).rotate(Math.toRadians(270));
////        g.drawString(str, x, y);
////        System.out.println("X : "+x+" Y : "+y);
//        ((Graphics2D) g).drawString(str, -45 - g.getFontMetrics().stringWidth(str) / 2, 5 + (w / 2));
//        ((Graphics2D) g).rotate(Math.toRadians(90));
//        ((Graphics2D) g).translate(-left, -y);
//        g.drawString(str, x, y);
//
//        if (w <= 16) {
//            ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD, 10));
//            if (g.getFontMetrics().stringWidth(str) >= horizontalTileHeight) {
//                ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD, 10));
//            }
//        } else {
//            ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD, 11));
//            if (g.getFontMetrics().stringWidth(str) >= horizontalTileHeight) {
//                ((Graphics2D) g).setFont(new TWFont("Arial", Font.BOLD, 11));
//            }
//        }
//
//
        int strW = g.getFontMetrics(getFont()).getHeight();
        int x = left + (w - strW) / 2;
        if (((w - strW) / 2) <= 3) { // not enough space. use smaller font
            //g.setFont(fontBoldSmall9);
            strW = g.getFontMetrics(getFont()).getHeight();
            x = left + (w - strW) / 2;
            if (((w - strW) / 2) <= 3) { // not enough space. use smaller font
                // g.setFont(fontBoldSmall7);
                strW = g.getFontMetrics(getFont()).getHeight();
                x = left + (w - strW) / 2;
            }
        }

        int strH = g.getFontMetrics().stringWidth(str);
        int ye = y + (horizontalTileHeight - strH) / 2;
        if (((horizontalTileHeight - strH) / 2) <= 3) { // not enough space. use smaller font
            //g.setFont(fontBoldSmall9);
            strH = g.getFontMetrics().stringWidth(str);
            ye = y + (horizontalTileHeight - strH) / 2;
            if (((w - strH) / 2) <= 3) { // not enough space. use smaller font
                // g.setFont(fontBoldSmall7);
                strH = g.getFontMetrics().stringWidth(str);
                ye = y + (horizontalTileHeight - strW) / 2;
            }
        }

        // Graphics2D g2d = (Graphics2D)g;
        // g2d.translate(x, y);
        //  g2d.rotate(Math.PI/2.0);
        // g.drawString(str, x, y);
        // Graphics2D g2d = (Graphics2D)g;
//        System.out.println("y trsns : "+y);
//        System.out.println("left : "+left+" y : "+y);
        ((Graphics2D) g).translate(left, y);
        ((Graphics2D) g).rotate(Math.toRadians(270));
        //  System.out.println("Drawing string");
        // ((Graphics2D) g).setFont(new TWFont("Helvetica", Font.BOLD,  7));
        ((Graphics2D) g).drawString(str, -(horizontalTileHeight / 2) - g.getFontMetrics().stringWidth(str) / 2, 5 + (w / 2));
//        System.out.println("x : "+(-45 - g.getFontMetrics().stringWidth(str) / 2)+" y :"+5 + (w / 2));
        ((Graphics2D) g).rotate(Math.toRadians(90));
        ((Graphics2D) g).translate(-left, -y);
        //g2d.rotate((3*Math.PI)/2.0);
//        Graphics2D g2 = (Graphics2D)g;
//        AffineTransform af = new AffineTransform();
//        af.translate( w,60 );
//         af.rotate( Math.toRadians(90) );
//        ((Graphics2D)g).setTransform( af );
//        ((Graphics2D)g).drawString(str, x, y);
//        af.rotate( Math.toRadians(270) );
        // ((Graphics2D)g).setTransform( af );

//        int v = g.getFontMetrics(getFont()).getHeight();
//        System.out.println(v);
//        int j = 0;
//        int k = str.length();
//        int l= str.length();
//        while (j < k + 1) {
//            if (j == k)
//                g.drawString(str.substring(j), x, y + (l * 10));
//            else
//                g.drawString(str.substring(j, j + 1), x, y + (l * 10));
//            j++;
//           l--;
//        }
        //      return g;
    }

    public void setMaximumStringWidth() {
        HeatRecord hR;
        int SymbolMaxWidth = 0;
        for (int i = 0; i < heatStore.size(); i++) {
            hR = (HeatRecord) heatStore.get(i);
            SymbolMaxWidth = Math.max(SymbolMaxWidth, getFontMetrics(getFont()).stringWidth(hR.getSymbol()));
//             System.out.println("width :"+SymbolMaxWidth+ "sysmbol : "+hR.getSymbol());
        }
//        System.out.println("Max Width : "+SymbolMaxWidth);
        horizontalTileHeight = SymbolMaxWidth + 15;
        verticalTileWidth = SymbolMaxWidth + 15;

//        System.out.println("h widh : "+horizontalTileHeight);
//        System.out.println("v width : "+verticalTileWidth);
    }

    public int getMaximumStringWidth() {
        HeatRecord hR;
        int SymbolMaxWidth = 0;
        for (int i = 0; i < heatStore.size(); i++) {
            hR = (HeatRecord) heatStore.get(i);
            SymbolMaxWidth = Math.max(SymbolMaxWidth, getFontMetrics(getFont()).stringWidth(hR.getSymbol()));
//             System.out.println("width :"+SymbolMaxWidth+ "sysmbol : "+hR.getSymbol());
        }
        return SymbolMaxWidth;
    }

    class Footer extends JPanel {

        public Footer() {
            this.setPreferredSize(new Dimension(36, 36));
            this.setBackground(Color.BLACK);
        }

        public void paint(Graphics g) {
            super.paint(g);
            drawFooter(g, -5);
        }
    }
}



