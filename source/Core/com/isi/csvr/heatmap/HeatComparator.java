package com.isi.csvr.heatmap;

import com.isi.csvr.HeatInterface;

import java.util.Comparator;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class HeatComparator implements Comparator {

    public final byte COMPARE_BY_VALUE_DESCENDING = 0;
    private byte compareMode = COMPARE_BY_VALUE_DESCENDING;
    public final byte COMPARE_BY_VALUE_ASCENDING = 1;
    public final byte COMPARE_BY_SYMBOL = 2;
    private byte criteria = HeatInterface.CRITERIA_PERC_CHANGE;

    public HeatComparator() {
    }

    public int compare(Object o1, Object o2) {
        HeatRecord hR1 = (HeatRecord) o1;
        HeatRecord hR2 = (HeatRecord) o2;

        double val1, val2;
        val1 = hR1.getValue(criteria);
        val2 = hR2.getValue(criteria);
        if (Double.isNaN(val1)) val1 = 0;
        if (Double.isNaN(val2)) val2 = 0;

        if (compareMode == COMPARE_BY_SYMBOL) {
            return hR1.getSymbol().compareTo(hR2.getSymbol());
        } else if (compareMode == COMPARE_BY_VALUE_ASCENDING) {
            if (val1 > val2) {
                return 1;
            } else if (val1 < val2) {
                return -1;
            } else {
                return hR1.getSymbol().compareTo(hR2.getSymbol());
            }
        } else {
            if (val1 > val2) {
                return -1;
            } else if (val1 < val2) {
                return +1;
            } else {
                return hR1.getSymbol().compareTo(hR2.getSymbol());
            }
        }
    }

    public byte getComparisonMode() {
        return compareMode;
    }

    public void setComparisonMode(byte mode) {
        compareMode = mode;
    }

    public void setCriteria(byte c) {
        criteria = c;
    }
}