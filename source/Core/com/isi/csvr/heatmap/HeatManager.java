package com.isi.csvr.heatmap;

import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public final class HeatManager extends Thread {

    private static HeatManager heatManager = null;
    private ArrayList heatWindowList = new ArrayList();
    private boolean isBusy = false;

    private HeatManager() {
        super("HeatManager");
        System.out.println("heat manager created");
        start();
    }

    public static synchronized HeatManager getSharedInstance() {
        if (heatManager == null) {
            heatManager = new HeatManager();
        }
        return heatManager;
    }

    public Object clone() throws CloneNotSupportedException {
        if (true) {
            throw new CloneNotSupportedException();
        }
        return null;
    }

    public void addHeatWindow(HeatMapFrame hFrame) {
        synchronized (heatWindowList) {
            isBusy = true;
            if (!heatWindowList.contains(hFrame))
                heatWindowList.add(hFrame);
            isBusy = false;
        }
    }

    public void removeHeatWindow(HeatMapFrame hFrame) {
        synchronized (heatWindowList) {
            isBusy = true;
            heatWindowList.remove(hFrame);
            isBusy = false;
        }
    }

    public void run() {
        while (true) {
            try {
                sleep(1000);
                if (!isBusy) {
//					for (int i = 0; i < 4; i++) {
//						refreshHeatValues();
//						sleep(1000);
//					}
//					refreshSymbols();
                    refreshHeatValues();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

//	private void refreshSymbols(){
//		HeatMapFrame hmf;
//		for (int i=heatWindowList.size()-1; i<=0; i++) {
//			hmf = (HeatMapFrame)heatWindowList.get(i);
//			//hmf.refreshSymbols();
//		}
//	}

    private void refreshHeatValues() {
        HeatMapFrame hmf;
        if (heatWindowList.size() == 0) return;

        for (int i = heatWindowList.size() - 1; i <= 0; i++) {
            hmf = (HeatMapFrame) heatWindowList.get(i);
            hmf.refreshHeatValues();
            hmf = null;
        }
    }

    public void updateViewSettings() {
        HeatMapFrame hmf;
        if (heatWindowList.size() == 0) return;

        for (int i = heatWindowList.size() - 1; i <= 0; i++) {
            hmf = (HeatMapFrame) heatWindowList.get(i);
            hmf.upadateViewsetting();
            hmf = null;
        }
    }

}