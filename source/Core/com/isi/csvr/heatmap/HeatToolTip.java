package com.isi.csvr.heatmap;

import com.isi.csvr.HeatInterface;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class HeatToolTip extends JPanel {

    final Object tipLock = new Object();
    final Font fontBold = Theme.getDefaultFont(Font.BOLD, 11);
    final Font fontPlain = Theme.getDefaultFont(Font.PLAIN, 12);
    final String strLowerToUpper = Language.getString("HEAT_MAP_LOWER_TO_UPPER");
    final String strLastTrade = Language.getString("HEAT_MAP_LBL_LAST_TRADE");
    final String strBid = Language.getString("HEAT_MAP_LBL_BID");
    final String strAsk = Language.getString("HEAT_MAP_LBL_ASK");
    final String strHigh = Language.getString("HEAT_MAP_LBL_HIGH");
    final String strLow = Language.getString("HEAT_MAP_LBL_LOW");
    final String strVolume = Language.getString("HEAT_MAP_LBL_VOLUME");
    final String strChange = Language.getString("HEAT_MAP_LBL_CHANGE");
    final String NA = Language.getString("NA");
    final String[] saStarDesc = {
            Language.getString("HEAT_MAP_STAR_ONE"),
            Language.getString("HEAT_MAP_STAR_TWO"),
            Language.getString("HEAT_MAP_STAR_THREE"),
            Language.getString("HEAT_MAP_STAR_FOUR"),
            Language.getString("HEAT_MAP_STAR_FIVE")
    };
    final int TIP_WIDTH_MIN = 130;
    final int GAP = 5;
    boolean isHeatTip = false;
    TWDecimalFormat pcntFormatter = new TWDecimalFormat("#,##0.00'%'");
    TWDecimalFormat floatFormatter = new TWDecimalFormat("#,##0.00");
    TWDecimalFormat longFormatter = new TWDecimalFormat("#,###,###,##0");
    int TIP_WIDTH_LABEL = 50;
    int HeadingHt = 18;
    int LINEGAP = 15;
    Color backColorLt = new Color(235, 235, 250);
    Color backColorDk = new Color(220, 220, 240);
    Color titleColor = new Color(100, 100, 140);

    String upperValue;
    String lowerValue;
    byte criteria;

    String shortDesc;
    String lastTrade;
    String bid;
    String ask;
    String high;
    String low;
    String volume;
    String change;

    public HeatToolTip() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception {
        this.setLayout(null);
        lowerValue = "";
        upperValue = "";
        criteria = HeatInterface.CRITERIA_PERC_CHANGE;
        String shortDesc = "";
        lastTrade = "0";
        bid = "0";
        ask = "0";
        high = "0";
        low = "0";
        volume = "0";
        change = "0";
        TIP_WIDTH_LABEL = getFontMetrics(fontPlain).stringWidth(strLastTrade);
    }

    public void paint(Graphics g) {
        synchronized (tipLock) {
            g.setColor(backColorLt);
            g.fillRect(0, 0, getWidth(), getHeight());
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            if (isHeatTip) {
                g.setFont(fontPlain);
                g.setColor(Color.BLACK);
                g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
                g.drawString(getHeatTip(), GAP, 12);
            } else {
                g.setColor(titleColor);
                g.fillRect(0, 0, getWidth() - 1, HeadingHt);
                g.setFont(fontBold);
                int vPos = 15;
                int hPos = this.getWidth() - GAP;//Math.max(2*GAP+TIP_WIDTH_LABEL, this.getWidth()/2);
                g.setColor(backColorLt);
                g.drawString(shortDesc, GAP, vPos);
                int tmpY;
                g.setColor(backColorDk);
                for (int i = 0; i < 3; i++) {
                    tmpY = HeadingHt + 1 + LINEGAP * (2 * i + 1);
                    g.fillRect(1, tmpY, getWidth() - 3, LINEGAP);
                }
                g.setFont(fontPlain);
                vPos += 16;
                g.setColor(Color.BLACK);
                g.drawString(strLastTrade, GAP, vPos);
                drawRightAligned(g, lastTrade, hPos, vPos);
                vPos += LINEGAP;
                g.drawString(strChange, GAP, vPos);
                drawRightAligned(g, change, hPos, vPos);
                vPos += LINEGAP;
                g.drawString(strAsk, GAP, vPos);
                drawRightAligned(g, ask, hPos, vPos);
                vPos += LINEGAP;
                g.drawString(strBid, GAP, vPos);
                drawRightAligned(g, bid, hPos, vPos);
                vPos += LINEGAP;
                g.drawString(strHigh, GAP, vPos);
                drawRightAligned(g, high, hPos, vPos);
                vPos += LINEGAP;
                g.drawString(strLow, GAP, vPos);
                drawRightAligned(g, low, hPos, vPos);
                vPos += LINEGAP;
                g.drawString(strVolume, GAP, vPos);
                drawRightAligned(g, volume, hPos, vPos);
                g.setColor(Color.BLACK);
                g.drawRect(0, 0, getWidth() - 1, HeadingHt);
                g.drawRect(0, HeadingHt, getWidth() - 1, getHeight() - HeadingHt - 1);
            }
        }
    }

    private String formatNumber(double value, TWDecimalFormat format) {
        if ((Double.isInfinite(value)) || (Double.isNaN(value))) {
            return NA;
        } else {
            return format.format(value);
        }
    }

    public void setHeatTip(double lower, double upper, byte criteria) {
        synchronized (tipLock) {
            this.criteria = criteria;
            if (criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) {
                lowerValue = formatNumber(lower, longFormatter);//.format(lower);
                upperValue = formatNumber(upper, longFormatter);//.format(upper);
            } else if ((criteria == HeatInterface.CRITERIA_5_YEAR_GROWTH) ||
                    (criteria == HeatInterface.CRITERIA_INSTITUTIONAL_HOLDING) ||
                    (criteria == HeatInterface.CRITERIA_PERC_CHANGE) ||
                    //(criteria==HeatInterface.CRITERIA_BID_ASK_RATIO)||
                    (criteria == HeatInterface.CRITERIA_SPREAD) ||
                    (criteria == HeatInterface.CRITERIA_RANGE) ||
                    (criteria == HeatInterface.CRITERIA_PCT_RANGE) ||
                    (criteria == HeatInterface.CRITERIA_PCT_SPREAD) ||
                    (criteria == HeatInterface.CRITERIA_PERC_GAIN)) {
                lowerValue = formatNumber(lower, pcntFormatter);//.format(lower);
                upperValue = formatNumber(upper, pcntFormatter);//.format(upper);
            } else if (criteria == HeatInterface.CRITERIA_SNP_RANK) {
                int low = (int) Math.round(Math.ceil(lower));
                int upp = (int) Math.round(Math.floor(upper));
                lowerValue = HeatInterface.getRankForMappedValue(low);
                upperValue = HeatInterface.getRankForMappedValue(upp);
            } else {
                lowerValue = formatNumber(lower, floatFormatter);//.format(lower);
                upperValue = formatNumber(upper, floatFormatter);//.format(upper);
            }
            isHeatTip = true;
            setToolTipSize();
        }
    }

    public void setSymbolTip(String key) {
        Stock stock = HeatInterface.getStockObject(key);
        synchronized (tipLock) {
            if (stock == null) {
                shortDesc = ""; // stock.getCompanyName();
                lastTrade = floatFormatter.format(0);
                bid = floatFormatter.format(0);
                ask = floatFormatter.format(0);
                high = floatFormatter.format(0);
                low = floatFormatter.format(0);
                volume = longFormatter.format(0);
                change = floatFormatter.format(0);
            } else {
                shortDesc = HeatInterface.getCompanyName(stock); // stock.getCompanyName();
                lastTrade = floatFormatter.format(stock.getLastTradeValue());
                bid = floatFormatter.format(HeatInterface.getBidPrice(stock));
                ask = floatFormatter.format(HeatInterface.getAskPrice(stock));
                high = floatFormatter.format(stock.getHigh());
                low = floatFormatter.format(stock.getLow());
                volume = longFormatter.format(stock.getVolume());
                float ch = (float) HeatInterface.getChange(stock); // stock.getChange()[0];
                if (ch == HeatInterface.DEFAULT_DOUBLE_VALUE) ch = 0;
                change = floatFormatter.format(ch);
            }
            stock = null;
            isHeatTip = false;
            setToolTipSize();
        }
    }

    private void setToolTipSize() {
        FontMetrics fmPlain = getFontMetrics(fontPlain);
        int w = TIP_WIDTH_MIN;
        int h = 16;
        if (isHeatTip) {
            String strTip = getHeatTip();
            w = fmPlain.stringWidth(strTip) + 2 * GAP;
        } else {
            FontMetrics fmBold = getFontMetrics(fontBold);
            w = Math.max(fmBold.stringWidth(shortDesc) + 2 * GAP, w);
            w = Math.max(fmBold.stringWidth(volume) + 3 * GAP + TIP_WIDTH_LABEL, w);
            h = 7 * LINEGAP + 16 + GAP;
            w = Math.min(w, (h * 3) / 2);
        }
        setSize(w, h);
    }

    private String getHeatTip() {
        String result;
        if (criteria == HeatInterface.CRITERIA_SNP_STAR_RATING) {
            result = getStarDescription(lowerValue);  //lowerValue+" - "+
        } else {
            result = strLowerToUpper.replaceFirst("\\[LOWER\\]", lowerValue);
            result = result.replaceFirst("\\[UPPER\\]", upperValue);
        }
        return result;
    }

    private String getStarDescription(String val) {
        int ind = Integer.parseInt(val.trim()) - 1;
        String str = "";
        if ((ind > -1) && (ind < saStarDesc.length)) {
            str = saStarDesc[ind];
        }
        return str;
    }

    private void drawRightAligned(Graphics g, String str, int r, int y) {
        int strW = g.getFontMetrics().stringWidth(str);
        g.drawString(str, r - strW, y);
    }

    public void setToolTipColors(Color[] clArr) {
        if ((clArr != null) && (clArr.length > 2)) {
            backColorLt = clArr[0];
            backColorDk = clArr[1];
            titleColor = clArr[2];
        }
    }
}











