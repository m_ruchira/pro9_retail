package com.isi.csvr.heatmap;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class HeatColorComboItem {

    private Icon img;
    private Color upColor;
    private Color downColor;

    public HeatColorComboItem(Icon img, Color upColor, Color downColor) {
        this.img = img;
        this.upColor = upColor;
        this.downColor = downColor;
    }

    public Icon getImage() {
        return img;
    }

    public Color getUpColor() {
        return upColor;
    }

    public Color getDownColor() {
        return downColor;
    }
}