package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.GraphDataManager;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 4:41:52 PM
 */
public class RSMACD {

    private static byte source = GraphDataManager.INNER_Close;
    private static long lastVisibleTime = 0L;
    private double value;

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSMACD.source = source;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSMACD.lastVisibleTime = lastVisibleTime;
    }

    public void calculateMACD(RadarScreenData radarScreenData) {
        int tempExpMovAvgTimePeriod = RSExpMovingAverage.getExpMovingAvgTimePeriod();
        byte tempSource = RSExpMovingAverage.getSource();
        RSExpMovingAverage.setSource(source);
        RSExpMovingAverage rc26ExpMovingAverage = new RSExpMovingAverage(radarScreenData);
        RSExpMovingAverage.setExpMovingAvgTimePeriod(26);
        rc26ExpMovingAverage.calculateMA();
        RSExpMovingAverage rc12ExpMovingAverage = new RSExpMovingAverage(radarScreenData);
        RSExpMovingAverage.setExpMovingAvgTimePeriod(12);
        rc12ExpMovingAverage.calculateMA();
        RSExpMovingAverage.setExpMovingAvgTimePeriod(tempExpMovAvgTimePeriod);
        RSExpMovingAverage.setSource(tempSource);

        value = rc12ExpMovingAverage.getValue() - rc26ExpMovingAverage.getValue();
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
