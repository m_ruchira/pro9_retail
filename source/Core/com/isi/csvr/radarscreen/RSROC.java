package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.GraphDataManager;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 4:55:48 PM
 */
public class RSROC {

    private static int rocTimePeriod = 7;
    private static byte source = GraphDataManager.INNER_Close;

    private static long lastVisibleTime = 0L;

    private double value;

    public static int getRocTimePeriod() {
        return rocTimePeriod;
    }

    public static void setRocTimePeriod(int rocTimePeriod) {
        RSROC.rocTimePeriod = rocTimePeriod;
    }

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSROC.source = source;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSROC.lastVisibleTime = lastVisibleTime;
    }

    public void calculateROC(RadarScreenData radarScreenData) {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
            if (radarScreenData.getDataArray().getSize() < ((rocTimePeriod + 1) * interval)) {
                radarScreenData.getDataArray().setMaxCapacity((rocTimePeriod + 1) * interval);
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(RadarScreenInterface.getInstance().getInterval());
            }
        } else { //Default
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        if (listToBeUsed.size() > rocTimePeriod) {
            ChartRecord crCurrent = listToBeUsed.get(listToBeUsed.size() - 1);
            ChartRecord crOld = listToBeUsed.get(listToBeUsed.size() - rocTimePeriod - 1);
            if (crOld.getValue(source) == 0) {
                value = 0.0;
            } else {
                value = (crCurrent.getValue(source) - crOld.getValue(source)) / crOld.getValue(source) * 100;
            }
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
