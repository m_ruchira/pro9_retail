package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChaikinOscillator;
import com.isi.csvr.chart.ChartRecord;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 5:00:05 PM
 */
public class RSChaikinOsc {
    private static long lastVisibleTime = 0L;
    private double value;
    private RadarScreenData radarScreenData;

    public RSChaikinOsc(RadarScreenData radarScreenData) {
        this.radarScreenData = radarScreenData;
        if (!radarScreenData.isFullyLoaded()) {
            radarScreenData.getDataArray().setMaxCapacity(-1);
            radarScreenData.loadDataFromFiles();
            radarScreenData.setFullyLoaded(true);
        }
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSChaikinOsc.lastVisibleTime = lastVisibleTime;
    }

    public void calculateChaikinAD() {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) { //Custom intervals
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
        } else { //Default
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        value = ChaikinOscillator.getChaikinAD(listToBeUsed);
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
