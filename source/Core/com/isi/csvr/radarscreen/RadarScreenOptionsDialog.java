package com.isi.csvr.radarscreen;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWFont;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Arrays;

/**
 * User: Pramoda
 * Date: Aug 30, 2006
 * Time: 12:18:57 PM
 */
public class RadarScreenOptionsDialog extends JDialog {

    public static final String[] rsIndicators = {Language.getString("RS_SIMPLE_MOV_AVG"),
            Language.getString("RS_WEIGHTED_MOV_AVG"),
            Language.getString("RS_EXPONENTIAL_MOV_AVG"),
            Language.getString("RS_MACD"),
            Language.getString("RS_IMI"),
            Language.getString("RS_RSI"),
            Language.getString("RS_PRICE_ROC"),
            Language.getString("RS_FAST_STOCHASTIC"),
            Language.getString("RS_SLOW_STOCHASTIC"),
            Language.getString("RS_BOLLINGER_UPPER"),
            Language.getString("RS_BOLLINGER_MIDDLE"),
            Language.getString("RS_BOLLINGER_LOWER"),
            Language.getString("RS_CHAIKIN_AD")};
    public static String[] rsOHLCPriorities = {Language.getString("OPEN"),
            Language.getString("HIGH"), Language.getString("LOW"),
            Language.getString("CLOSE")
    };
    public static String[] rsMAMethods = {Language.getString("METHOD_SIMPLE"),
            Language.getString("METHOD_WEIGHTED"),
            Language.getString("METHOD_EXPONENTIAL")};
    private JComboBox cmbInterval;
    private JList lstIndicator;
    private JPanel propertyPanel;
    private JSpinner spinTimePeriods;
    private JSpinner deviations;
    private JComboBox cmbMethod;
    private JComboBox cmbOhlcPriority;
    private JSpinner slowingPeriod;
    private String prevSelectedValue;

    public RadarScreenOptionsDialog(JFrame source) {
        super(source, true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle(Language.getString("RADAR_SCREEN_DIALOG_TITLE"));
        setSize(250, 250);
        getContentPane().setLayout(new GridBagLayout());
        final GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;

        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"100%", "0"}, new String[]{"25"}));
        JLabel lblInterval = new JLabel(Language.getString("RADAR_SCREEN_INTERVAL"));
        lblInterval.setFont(new TWFont("Arial", Font.BOLD, 12));
        topPanel.add(lblInterval);
        cmbInterval = new JComboBox(getRSIntervals());
        switch (RadarScreenInterface.getInstance().getInterval()) {
            case 1:
                cmbInterval.setSelectedIndex(0);
                break;
            case 5:
                cmbInterval.setSelectedIndex(1);
                break;
            case 10:
                cmbInterval.setSelectedIndex(2);
                break;
            case 15:
                cmbInterval.setSelectedIndex(3);
                break;
            case 30:
                cmbInterval.setSelectedIndex(4);
                break;
            case 60:
                cmbInterval.setSelectedIndex(5);
                break;
        }
        topPanel.add(cmbInterval);

        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.insets = new Insets(10, 10, 10, 10);
        getContentPane().add(topPanel, c);

        JLabel lblIndicator = new JLabel(Language.getString("SELECT_INDICATORS"));
        lblIndicator.setFont(new TWFont("Arial", Font.BOLD, 12));
        c.gridwidth = 1;
        c.gridy = 1;
        c.insets = new Insets(10, 10, 0, 10);
        getContentPane().add(lblIndicator, c);

        JLabel lblIndProperties = new JLabel(Language.getString("PROPERTIES"));
        lblIndProperties.setFont(new TWFont("Arial", Font.BOLD, 12));
        c.gridx = 1;
        c.insets = new Insets(10, 20, 0, 10);
        getContentPane().add(lblIndProperties, c);

        Arrays.sort(rsIndicators);
        lstIndicator = new JList(rsIndicators);
        lstIndicator.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstIndicator.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                String selectedValue = (String) lstIndicator.getSelectedValue();
                boolean changed = false;
                if ((spinTimePeriods != null) && (deviations != null) && (cmbOhlcPriority != null) &&
                        (cmbMethod != null) && (prevSelectedValue != null)) {
                    try {
                        spinTimePeriods.commitEdit();
                        deviations.commitEdit();
                    } catch (ParseException e1) {
                        //Ignore: take the last valid value as current
                    }
                    if (prevSelectedValue.equals(Language.getString("RS_SIMPLE_MOV_AVG"))) {
                        changed = (RSSimpleMovingAverage.getSimpleMovAvgTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSSimpleMovingAverage.getSource() != cmbOhlcPriority.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_WEIGHTED_MOV_AVG"))) {
                        changed = (RSWghtMovingAverage.getWghtMovAvgTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSWghtMovingAverage.getSource() != cmbOhlcPriority.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_EXPONENTIAL_MOV_AVG"))) {
                        changed = (RSExpMovingAverage.getExpMovingAvgTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSExpMovingAverage.getSource() != cmbOhlcPriority.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_MACD"))) {
                        changed = (RSMACD.getSource() != cmbOhlcPriority.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_IMI"))) {
                        changed = (RSIMI.getImiTimePeriod() != (Integer) spinTimePeriods.getValue());
                    } else if (prevSelectedValue.equals(Language.getString("RS_RSI"))) {
                        changed = (RSRSI.getRsiTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSRSI.getSource() != cmbOhlcPriority.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_PRICE_ROC"))) {
                        changed = (RSROC.getRocTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSROC.getSource() != cmbOhlcPriority.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_FAST_STOCHASTIC"))) {
                        changed = (RSFastStochastic.getFastStochasticTimePeriod() != (Integer) spinTimePeriods.getValue());
                    } else if (prevSelectedValue.equals(Language.getString("RS_BOLLINGER_UPPER"))) {
                        changed = (RSBollingerUpper.getBollingerUpperTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSBollingerUpper.getSource() != cmbOhlcPriority.getSelectedIndex()) ||
                                (RSBollingerUpper.getDeviations() != (Double) deviations.getValue()) ||
                                (RSBollingerUpper.getMethod() != cmbMethod.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_BOLLINGER_MIDDLE"))) {
                        changed = (RSBollingerMiddle.getBollingerMiddleTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSBollingerMiddle.getSource() != cmbOhlcPriority.getSelectedIndex()) ||
                                (RSBollingerMiddle.getMethod() != cmbMethod.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_BOLLINGER_LOWER"))) {
                        changed = (RSBollingerLower.getBollingerLowerTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSBollingerLower.getSource() != cmbOhlcPriority.getSelectedIndex()) ||
                                (RSBollingerLower.getDeviations() != (Double) deviations.getValue()) ||
                                (RSBollingerLower.getMethod() != cmbMethod.getSelectedIndex());
                    } else if (prevSelectedValue.equals(Language.getString("RS_SLOW_STOCHASTIC"))) {
                        changed = (RSSlowStochastic.getSlowStochasticTimePeriod() != (Integer) spinTimePeriods.getValue()) ||
                                (RSSlowStochastic.getSlowingPeriod() != (Integer) slowingPeriod.getValue());
                    }
                }

                if (changed) {
                    String message = Language.getString("RS_PROPERTY_DIALOG_MESSAGE");
                    String messagePart1 = "";
                    String messagePart2 = "";
                    try {
                        messagePart1 = message.substring(0, message.indexOf("["));
                        messagePart2 = message.substring(message.indexOf("]") + 1);
                    } catch (IndexOutOfBoundsException e1) {
                        //Ignore
                    }
                    message = messagePart1 + prevSelectedValue + messagePart2;
                    int option = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE,
                            JOptionPane.YES_NO_OPTION);
                    if (option == JOptionPane.YES_OPTION) {
                        if (prevSelectedValue.equals(Language.getString("RS_SIMPLE_MOV_AVG"))) {
                            RSSimpleMovingAverage.setSimpleMovAvgTimePeriod((Integer) spinTimePeriods.getValue());
                            RSSimpleMovingAverage.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_WEIGHTED_MOV_AVG"))) {
                            RSWghtMovingAverage.setWghtMovAvgTimePeriod((Integer) spinTimePeriods.getValue());
                            RSWghtMovingAverage.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_EXPONENTIAL_MOV_AVG"))) {
                            RSExpMovingAverage.setExpMovingAvgTimePeriod((Integer) spinTimePeriods.getValue());
                            RSExpMovingAverage.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_MACD"))) {
                            RSMACD.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_IMI"))) {
                            RSIMI.setImiTimePeriod((Integer) spinTimePeriods.getValue());
                        } else if (prevSelectedValue.equals(Language.getString("RS_RSI"))) {
                            RSRSI.setRsiTimePeriod((Integer) spinTimePeriods.getValue());
                            RSRSI.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_PRICE_ROC"))) {
                            RSROC.setRocTimePeriod((Integer) spinTimePeriods.getValue());
                            RSROC.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_FAST_STOCHASTIC"))) {
                            RSFastStochastic.setFastStochasticTimePeriod((Integer) spinTimePeriods.getValue());
                        } else if (prevSelectedValue.equals(Language.getString("RS_BOLLINGER_UPPER"))) {
                            RSBollingerUpper.setBollingerUpperTimePeriod((Integer) spinTimePeriods.getValue());
                            RSBollingerUpper.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                            RSBollingerUpper.setDeviations((Double) deviations.getValue());
                            RSBollingerUpper.setMethod((byte) cmbMethod.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_BOLLINGER_MIDDLE"))) {
                            RSBollingerMiddle.setBollingerMiddleTimePeriod((Integer) spinTimePeriods.getValue());
                            RSBollingerMiddle.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                            RSBollingerMiddle.setMethod((byte) cmbMethod.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_BOLLINGER_LOWER"))) {
                            RSBollingerLower.setBollingerLowerTimePeriod((Integer) spinTimePeriods.getValue());
                            RSBollingerLower.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                            RSBollingerLower.setDeviations((Double) deviations.getValue());
                            RSBollingerLower.setMethod((byte) cmbMethod.getSelectedIndex());
                        } else if (prevSelectedValue.equals(Language.getString("RS_SLOW_STOCHASTIC"))) {
                            RSSlowStochastic.setSlowStochasticTimePeriod((Integer) spinTimePeriods.getValue());
                            RSSlowStochastic.setSlowingPeriod((Integer) slowingPeriod.getValue());
                        }
                    }
                }

                prevSelectedValue = selectedValue;
                if (propertyPanel != null) {
                    getContentPane().remove(propertyPanel);
                }
                setPropertyPanelForIndicator(selectedValue);
                c.gridx = 1;
                c.gridy = 2;
                c.anchor = GridBagConstraints.FIRST_LINE_START;
                c.insets = new Insets(10, 20, 0, 10);
                getContentPane().add(propertyPanel, c);
                propertyPanel.updateUI();
                getContentPane().repaint();
                pack();
            }
        });
        //-- Added by shanika - Req from Roshini
        lstIndicator.setFont(new TWFont("Arial", Font.PLAIN, 12));
        JScrollPane scrollPane = new JScrollPane(lstIndicator);
        //scrollPane.setSize(200, 200);
        scrollPane.setPreferredSize(new Dimension((int) lstIndicator.getPreferredSize().getWidth() + 20, 200));
        c.gridx = 0;
        c.gridy = 2;
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.insets = new Insets(10, 10, 0, 10);
        getContentPane().add(scrollPane, c);

        propertyPanel = new JPanel();
        propertyPanel.setSize(110, 200);
        propertyPanel.setPreferredSize(new Dimension(110, 200));
        c.gridx = 1;
        c.insets = new Insets(10, 20, 0, 10);
        getContentPane().add(propertyPanel, c);

        JPanel buttonPannel = new JPanel();
        c.gridy = 3;
        c.insets = new Insets(10, 10, 0, 10);
        getContentPane().add(buttonPannel, c);

        JButton btnOk = new JButton(Language.getString("OK"));
        btnOk.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (lstIndicator.getSelectedValue() != null) {
                    String selectedValue = (String) lstIndicator.getSelectedValue();
                    if (selectedValue.equals(Language.getString("RS_SIMPLE_MOV_AVG")) &&
                            spinTimePeriods != null && cmbOhlcPriority != null) {
                        RSSimpleMovingAverage.setSimpleMovAvgTimePeriod((Integer) spinTimePeriods.getValue());
                        RSSimpleMovingAverage.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_WEIGHTED_MOV_AVG")) &&
                            spinTimePeriods != null && cmbOhlcPriority != null) {
                        RSWghtMovingAverage.setWghtMovAvgTimePeriod((Integer) spinTimePeriods.getValue());
                        RSWghtMovingAverage.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_EXPONENTIAL_MOV_AVG")) &&
                            spinTimePeriods != null && cmbOhlcPriority != null) {
                        RSExpMovingAverage.setExpMovingAvgTimePeriod((Integer) spinTimePeriods.getValue());
                        RSExpMovingAverage.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_MACD")) && cmbOhlcPriority != null) {
                        RSMACD.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_IMI")) && spinTimePeriods != null) {
                        RSIMI.setImiTimePeriod((Integer) spinTimePeriods.getValue());
                    } else if (selectedValue.equals(Language.getString("RS_RSI")) &&
                            spinTimePeriods != null && cmbOhlcPriority != null) {
                        RSRSI.setRsiTimePeriod((Integer) spinTimePeriods.getValue());
                        RSRSI.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_PRICE_ROC")) &&
                            spinTimePeriods != null && cmbOhlcPriority != null) {
                        RSROC.setRocTimePeriod((Integer) spinTimePeriods.getValue());
                        RSROC.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_FAST_STOCHASTIC")) && spinTimePeriods != null) {
                        RSFastStochastic.setFastStochasticTimePeriod((Integer) spinTimePeriods.getValue());
                    } else if (selectedValue.equals(Language.getString("RS_BOLLINGER_UPPER")) &&
                            spinTimePeriods != null && cmbOhlcPriority != null &&
                            deviations != null && cmbMethod != null) {
                        RSBollingerUpper.setBollingerUpperTimePeriod((Integer) spinTimePeriods.getValue());
                        RSBollingerUpper.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        RSBollingerUpper.setDeviations((Double) deviations.getValue());
                        RSBollingerUpper.setMethod((byte) cmbMethod.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_BOLLINGER_MIDDLE")) &&
                            spinTimePeriods != null && cmbOhlcPriority != null &&
                            deviations != null && cmbMethod != null) {
                        RSBollingerMiddle.setBollingerMiddleTimePeriod((Integer) spinTimePeriods.getValue());
                        RSBollingerMiddle.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        RSBollingerMiddle.setMethod((byte) cmbMethod.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_BOLLINGER_LOWER")) &&
                            spinTimePeriods != null && cmbOhlcPriority != null &&
                            deviations != null && cmbMethod != null) {
                        RSBollingerLower.setBollingerLowerTimePeriod((Integer) spinTimePeriods.getValue());
                        RSBollingerLower.setSource((byte) cmbOhlcPriority.getSelectedIndex());
                        RSBollingerLower.setDeviations((Double) deviations.getValue());
                        RSBollingerLower.setMethod((byte) cmbMethod.getSelectedIndex());
                    } else if (selectedValue.equals(Language.getString("RS_SLOW_STOCHASTIC")) &&
                            spinTimePeriods != null && slowingPeriod != null) {
                        RSSlowStochastic.setSlowStochasticTimePeriod((Integer) spinTimePeriods.getValue());
                        RSSlowStochastic.setSlowingPeriod((Integer) slowingPeriod.getValue());
                    }
                }
                int selectedInterval = cmbInterval.getSelectedIndex();
                switch (selectedInterval) {
                    case 0:
                        RadarScreenInterface.getInstance().setInterval(1);
                        break;
                    case 1:
                        RadarScreenInterface.getInstance().setInterval(5);
                        break;
                    case 2:
                        RadarScreenInterface.getInstance().setInterval(10);
                        break;
                    case 3:
                        RadarScreenInterface.getInstance().setInterval(15);
                        break;
                    case 4:
                        RadarScreenInterface.getInstance().setInterval(30);
                        break;
                    case 5:
                        RadarScreenInterface.getInstance().setInterval(60);
                        break;
                }
                dispose();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "TabularIndicatorProperties");
            }
        });
        buttonPannel.add(btnOk);

        JButton btnCancel = new JButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        buttonPannel.add(btnCancel);
        lstIndicator.setSelectedIndex(0);
        GUISettings.applyOrientation(this.getContentPane());
        pack();
    }

    public static String[] getRSIntervals() {
        String[] sa = {Language.getString("EVERY_MINUTE"),
                Language.getString("5_MIN"),
                Language.getString("10_MIN"),
                Language.getString("15_MIN"),
                Language.getString("30_MIN"),
                Language.getString("60_MIN")
        };
        return sa;
    }

    private void setPropertyPanelForIndicator(String indicatorName) {
        propertyPanel = new JPanel();
        propertyPanel.setLayout(null);
        propertyPanel.setSize(110, 200);
        propertyPanel.setPreferredSize(new Dimension(110, 200));

        JLabel lblTimePeriods = new JLabel(Language.getString("TIME_PERIODS"));
        lblTimePeriods.setBounds(5, 1, 100, 22);
        SpinnerNumberModel timePeriodmodel = new SpinnerNumberModel(20, 1, 100, 1);
        spinTimePeriods = new JSpinner(timePeriodmodel);
        spinTimePeriods.setBounds(5, 24, 100, 22);

        JLabel lblDeviations = new JLabel(Language.getString("DEVIATIONS"));
        lblDeviations.setBounds(5, 50, 100, 22);
        SpinnerNumberModel deviationsModel = new SpinnerNumberModel(2.0, 0.01, 100.0, 0.01);
        deviations = new JSpinner(deviationsModel);
        deviations.setBounds(5, 73, 100, 22);

        JLabel lblOHLCPrio = new JLabel(Language.getString("PRICE_FIELD"));
        lblOHLCPrio.setBounds(5, 96, 100, 22);
        cmbOhlcPriority = new JComboBox(rsOHLCPriorities);
        cmbOhlcPriority.setBounds(5, 122, 100, 22);

        JLabel lblMethod = new JLabel(Language.getString("METHOD"));
        lblMethod.setBounds(5, 149, 100, 22);
        cmbMethod = new JComboBox(rsMAMethods);
        cmbMethod.setBounds(5, 172, 100, 22);

        JLabel lblSlowingPeriod = new JLabel(Language.getString("SLOWING_PERIOD"));
        lblSlowingPeriod.setBounds(5, 50, 100, 22);
        SpinnerNumberModel slowingPeriodModel = new SpinnerNumberModel(3, 1, 100, 1);
        slowingPeriod = new JSpinner(slowingPeriodModel);
        slowingPeriod.setBounds(5, 73, 100, 22);

        if (indicatorName.equals(Language.getString("RS_SIMPLE_MOV_AVG"))) {
            (spinTimePeriods.getModel()).setValue(RSSimpleMovingAverage.getSimpleMovAvgTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSSimpleMovingAverage.getSource());
            propertyPanel.add(cmbOhlcPriority);
        } else if (indicatorName.equals(Language.getString("RS_WEIGHTED_MOV_AVG"))) {
            (spinTimePeriods.getModel()).setValue(RSWghtMovingAverage.getWghtMovAvgTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSWghtMovingAverage.getSource());
            propertyPanel.add(cmbOhlcPriority);
        } else if (indicatorName.equals(Language.getString("RS_EXPONENTIAL_MOV_AVG"))) {
            (spinTimePeriods.getModel()).setValue(RSExpMovingAverage.getExpMovingAvgTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSExpMovingAverage.getSource());
            propertyPanel.add(cmbOhlcPriority);
        } else if (indicatorName.equals(Language.getString("RS_MACD"))) {
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSMACD.getSource());
            propertyPanel.add(cmbOhlcPriority);
        } else if (indicatorName.equals(Language.getString("RS_IMI"))) {
            (spinTimePeriods.getModel()).setValue(RSIMI.getImiTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
        } else if (indicatorName.equals(Language.getString("RS_RSI"))) {
            (spinTimePeriods.getModel()).setValue(RSRSI.getRsiTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSRSI.getSource());
            propertyPanel.add(cmbOhlcPriority);
        } else if (indicatorName.equals(Language.getString("RS_PRICE_ROC"))) {
            (spinTimePeriods.getModel()).setValue(RSROC.getRocTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSROC.getSource());
            propertyPanel.add(cmbOhlcPriority);
        } else if (indicatorName.equals(Language.getString("RS_FAST_STOCHASTIC"))) {
            (spinTimePeriods.getModel()).setValue(RSFastStochastic.getFastStochasticTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
        } else if (indicatorName.equals(Language.getString("RS_BOLLINGER_UPPER"))) {
            (spinTimePeriods.getModel()).setValue(RSBollingerUpper.getBollingerUpperTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSBollingerUpper.getSource());
            propertyPanel.add(cmbOhlcPriority);
            propertyPanel.add(lblDeviations);
            (deviations.getModel()).setValue(RSBollingerUpper.getDeviations());
            propertyPanel.add(deviations);
            propertyPanel.add(lblMethod);
            cmbMethod.setSelectedIndex(RSBollingerUpper.getMethod());
            propertyPanel.add(cmbMethod);
        } else if (indicatorName.equals(Language.getString("RS_BOLLINGER_MIDDLE"))) {
            (spinTimePeriods.getModel()).setValue(RSBollingerMiddle.getBollingerMiddleTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSBollingerMiddle.getSource());
            propertyPanel.add(cmbOhlcPriority);
            propertyPanel.add(lblMethod);
            cmbMethod.setSelectedIndex(RSBollingerMiddle.getMethod());
            propertyPanel.add(cmbMethod);
        } else if (indicatorName.equals(Language.getString("RS_BOLLINGER_LOWER"))) {
            (spinTimePeriods.getModel()).setValue(RSBollingerLower.getBollingerLowerTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            propertyPanel.add(lblOHLCPrio);
            cmbOhlcPriority.setSelectedIndex(RSBollingerLower.getSource());
            propertyPanel.add(cmbOhlcPriority);
            propertyPanel.add(lblDeviations);
            (deviations.getModel()).setValue(RSBollingerLower.getDeviations());
            propertyPanel.add(deviations);
            propertyPanel.add(lblMethod);
            cmbMethod.setSelectedIndex(RSBollingerLower.getMethod());
            propertyPanel.add(cmbMethod);
        } else if (indicatorName.equals(Language.getString("RS_SLOW_STOCHASTIC"))) {
            (spinTimePeriods.getModel()).setValue(RSSlowStochastic.getSlowStochasticTimePeriod());
            propertyPanel.add(lblTimePeriods);
            propertyPanel.add(spinTimePeriods);
            (slowingPeriod.getModel()).setValue(RSSlowStochastic.getSlowingPeriod());
            propertyPanel.add(lblSlowingPeriod);
            propertyPanel.add(slowingPeriod);
        }
    }


}
