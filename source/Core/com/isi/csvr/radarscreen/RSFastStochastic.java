package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.StocasticOscillator;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 5:03:47 PM
 */
public class RSFastStochastic {

    private static int fastStochasticTimePeriod = 5;

    private static long lastVisibleTime = 0L;

    private double value;

    public static int getFastStochasticTimePeriod() {
        return fastStochasticTimePeriod;
    }

    public static void setFastStochasticTimePeriod(int fastStochasticTimePeriod) {
        RSFastStochastic.fastStochasticTimePeriod = fastStochasticTimePeriod;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSFastStochastic.lastVisibleTime = lastVisibleTime;
    }

    public void calculateFastStochastic(RadarScreenData radarScreenData) {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
            if (radarScreenData.getDataArray().getSize() < (fastStochasticTimePeriod * interval)) {
                radarScreenData.getDataArray().setMaxCapacity(
                        fastStochasticTimePeriod * RadarScreenInterface.getInstance().getInterval());
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(RadarScreenInterface.getInstance().getInterval());
            }
        } else { //Default
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        if (listToBeUsed.size() >= fastStochasticTimePeriod) {
            value = StocasticOscillator.getFastStochastic(listToBeUsed, fastStochasticTimePeriod);
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
