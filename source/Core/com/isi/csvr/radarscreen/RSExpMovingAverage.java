package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.MovingAverage;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 4:23:33 PM
 */
public class RSExpMovingAverage {

    private static int expMovingAvgTimePeriod = 21;
    private static byte source = GraphDataManager.INNER_Close;

    private static long lastVisibleTime = 0L;

    private double value;
    private RadarScreenData radarScreenData;

    public RSExpMovingAverage(RadarScreenData radarScreenData) {
        this.radarScreenData = radarScreenData;
        if (!radarScreenData.isFullyLoaded()) {
            radarScreenData.getDataArray().setMaxCapacity(-1);
            radarScreenData.loadDataFromFiles();
            radarScreenData.setFullyLoaded(true);
        }
    }

    public static int getExpMovingAvgTimePeriod() {
        return expMovingAvgTimePeriod;
    }

    public static void setExpMovingAvgTimePeriod(int expMovingAvgTimePeriod) {
        RSExpMovingAverage.expMovingAvgTimePeriod = expMovingAvgTimePeriod;
    }

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSExpMovingAverage.source = source;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSExpMovingAverage.lastVisibleTime = lastVisibleTime;
    }

    public void calculateMA() {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
        } else { //Default
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        if (listToBeUsed.size() >= expMovingAvgTimePeriod) {
            value = MovingAverage.getMovingAverage(listToBeUsed, expMovingAvgTimePeriod, MovingAverage.METHOD_EXPONENTIAL,
                    source, ChartRecord.STEP_1);
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
