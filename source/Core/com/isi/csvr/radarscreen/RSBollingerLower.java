package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.BollingerBand;
import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.MovingAverage;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Sep 4, 2006
 * Time: 11:01:19 AM
 */
public class RSBollingerLower {
    private static int bollingerLowerTimePeriod = 20;
    private static byte source = GraphDataManager.INNER_Close;
    private static double deviations = 2.0;
    private static byte method = MovingAverage.METHOD_SIMPLE;

    private static long lastVisibleTime = 0L;

    private double value;

    public static int getBollingerLowerTimePeriod() {
        return bollingerLowerTimePeriod;
    }

    public static void setBollingerLowerTimePeriod(int bollingerLowerTimePeriod) {
        RSBollingerLower.bollingerLowerTimePeriod = bollingerLowerTimePeriod;
    }

    public static double getDeviations() {
        return deviations;
    }

    public static void setDeviations(double deviations) {
        RSBollingerLower.deviations = deviations;
    }

    public static byte getMethod() {
        return method;
    }

    public static void setMethod(byte method) {
        RSBollingerLower.method = method;
    }

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSBollingerLower.source = source;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSBollingerLower.lastVisibleTime = lastVisibleTime;
    }

    public void calculateBollingerLower(RadarScreenData radarScreenData) {
        LinkedList<ChartRecord> listToBeUsed;

        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
            if (method == MovingAverage.METHOD_EXPONENTIAL && !radarScreenData.isFullyLoaded()) {
                radarScreenData.getDataArray().setMaxCapacity(-1); //load all
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(interval);
                radarScreenData.setFullyLoaded(true);
            } else if (radarScreenData.getDataArray().getSize() < (bollingerLowerTimePeriod * interval)) {
                radarScreenData.getDataArray().setMaxCapacity(
                        bollingerLowerTimePeriod * RadarScreenInterface.getInstance().getInterval());
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(interval);
            }
        } else {
            listToBeUsed = radarScreenData.getDataArray().getList();
            if (method == MovingAverage.METHOD_EXPONENTIAL && !radarScreenData.isFullyLoaded()) {
                radarScreenData.getDataArray().setMaxCapacity(-1); //load all
                radarScreenData.loadDataFromFiles();
                radarScreenData.setFullyLoaded(true);
            }
        }
        if (listToBeUsed.size() >= bollingerLowerTimePeriod) {
            value = BollingerBand.getBollingerOther(listToBeUsed, bollingerLowerTimePeriod,
                    -deviations, method, source);
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }

}
