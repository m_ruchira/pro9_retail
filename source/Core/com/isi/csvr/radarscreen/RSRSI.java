package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.RSI;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 4:51:41 PM
 */
public class RSRSI {

    private static int rsiTimePeriod = 14;
    private static byte source = GraphDataManager.INNER_Close;

    private static long lastVisibleTime = 0L;

    private double value;
    private RadarScreenData radarScreenData;

    public RSRSI(RadarScreenData radarScreenData) {
        this.radarScreenData = radarScreenData;
        if (!radarScreenData.isFullyLoaded()) {
            radarScreenData.getDataArray().setMaxCapacity(-1);
            radarScreenData.loadDataFromFiles();
            radarScreenData.setFullyLoaded(true);
        }
    }

    public static int getRsiTimePeriod() {
        return rsiTimePeriod;
    }

    public static void setRsiTimePeriod(int rsiTimePeriod) {
        RSRSI.rsiTimePeriod = rsiTimePeriod;
    }

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSRSI.source = source;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSRSI.lastVisibleTime = lastVisibleTime;
    }

    public void calculateRSI() {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
        } else { //Default
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        if (listToBeUsed.size() >= rsiTimePeriod) {
            value = RSI.getRSI(listToBeUsed, rsiTimePeriod, source);
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
