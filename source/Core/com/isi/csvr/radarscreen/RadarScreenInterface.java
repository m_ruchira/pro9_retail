package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartPoint;
import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.downloader.HistoryDownloadListner;
import com.isi.csvr.downloader.HistoryDownloadManager;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.SharedMethods;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * User: Pramoda
 * Date: Aug 14, 2006
 * Time: 11:21:19 AM
 */
public class RadarScreenInterface implements Runnable, HistoryDownloadListner {

    public static final long updateInterval = 10000L;

    public static final byte IND_SIMPLE_MOV_AVG = 0;
    public static final byte IND_WGHT_MOV_AVG = 1;
    public static final byte IND_EXP_MOV_AVG = 2;
    public static final byte IND_MACD = 3;
    public static final byte IND_IMI = 4;
    public static final byte IND_RSI = 5;
    public static final byte IND_ROC = 6;
    public static final byte IND_CHAIKIN_OSC = 7;
    public static final byte IND_FAST_STOCHASTIC = 8;

    private static RadarScreenInterface thisInstance;

    private Hashtable<String, RadarScreenData> symbols;
    private boolean continueState = true;

    private int interval;
    private int dataCapacity;

    private RadarScreenInterface() {
        symbols = new Hashtable<String, RadarScreenData>();
        interval = 1; //Default
        dataCapacity = 40; //Default;
        HistoryDownloadManager.getSharedInstance().addHistoryDownloadListner(this);
    }

    public static RadarScreenInterface getInstance() {
        if (thisInstance == null) {
            thisInstance = new RadarScreenInterface();
        }

        return thisInstance;
    }

    public Hashtable<String, RadarScreenData> getSymbols() {
        return symbols;
    }

    public void run() {
//        enableAll();
        while (continueState) {
            try {
                Enumeration<RadarScreenData> elements = symbols.elements();
                while (elements.hasMoreElements()) {
                    RadarScreenData radarScreenData = elements.nextElement();
                    boolean active = setEnabledVisibleIndicators(radarScreenData);
                    if (active && !radarScreenData.isLoaded()) {
                        radarScreenData.loadDataFromFiles();
                    }
                    if (active && (interval > 1 && radarScreenData.getDataArray().isNewPointInserted())) {
                        radarScreenData.getDataArray().prepareForInterval(interval);
                    }
                    if (active && radarScreenData.isDataArrayUpdatedAfterLastCalculation()) {
                        radarScreenData.recalculateIndicators();
                    }
                    Thread.sleep(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(updateInterval);
            } catch (InterruptedException e) {
            }
        }
    }

    private boolean setEnabledVisibleIndicators(RadarScreenData radarScreenData) {
        boolean active = false;
        if ((System.currentTimeMillis() - RSSimpleMovingAverage.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setSimpleMovingAvgEnabled(false);
        } else {
            RadarScreenData.setSimpleMovingAvgEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSWghtMovingAverage.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setWghtMovingAvgEnabled(false);
        } else {
            RadarScreenData.setWghtMovingAvgEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSExpMovingAverage.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setExpMovingAvgEnabled(false);
        } else {
            RadarScreenData.setExpMovingAvgEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSMACD.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setMacdEnabled(false);
        } else {
            RadarScreenData.setMacdEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSIMI.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setImiEnabled(false);
        } else {
            RadarScreenData.setImiEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSRSI.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setRsiEnabled(false);
        } else {
            RadarScreenData.setRsiEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSROC.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setRocEnabled(false);
        } else {
            RadarScreenData.setRocEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSFastStochastic.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setFastStochasticEnabled(false);
        } else {
            RadarScreenData.setFastStochasticEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSSlowStochastic.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setSlowStochasticEnabled(false);
        } else {
            RadarScreenData.setSlowStochasticEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSBollingerMiddle.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setBollingerMiddleEnabled(false);
        } else {
            RadarScreenData.setBollingerMiddleEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSBollingerUpper.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setBollingerUpperEnabled(false);
        } else {
            RadarScreenData.setBollingerUpperEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSBollingerLower.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setBollingerLowerEnabled(false);
        } else {
            RadarScreenData.setBollingerLowerEnabled(true);
            active = true;
        }
        if ((System.currentTimeMillis() - RSChaikinOsc.getLastVisibleTime()) > (updateInterval * 2) ||
                (System.currentTimeMillis() - radarScreenData.getLastVisibleTime()) > (updateInterval * 2)) {
            RadarScreenData.setChaikinEnabled(false);
        } else {
            RadarScreenData.setChaikinEnabled(true);
            active = true;
        }
        return active;
    }

    public void enableAll() {
        RadarScreenData.setSimpleMovingAvgEnabled(true);
        RadarScreenData.setWghtMovingAvgEnabled(true);
        RadarScreenData.setExpMovingAvgEnabled(true);
        RadarScreenData.setMacdEnabled(true);
        RadarScreenData.setImiEnabled(true);
        RadarScreenData.setRsiEnabled(true);
        RadarScreenData.setRocEnabled(true);
        RadarScreenData.setFastStochasticEnabled(true);
        RadarScreenData.setBollingerMiddleEnabled(true);
        RadarScreenData.setBollingerUpperEnabled(true);
        RadarScreenData.setBollingerLowerEnabled(true);
        RadarScreenData.setSlowStochasticEnabled(true);
        RadarScreenData.setChaikinEnabled(true);
    }

    public int getInterval() {
        return interval;
    }

    public synchronized void setInterval(final int interval) {
        this.interval = interval;
        Runnable worker = new Runnable() {
            public void run() {
                for (String key : symbols.keySet()) {
                    RadarScreenData radarScreenData = symbols.get(key);
                    if (interval > 1) {
                        radarScreenData.getDataArray().prepareForInterval(interval);
                    }
                    //radarScreenData.recalculateIndicators();
                    radarScreenData.forceRecalculateIndicators();

                    try {
                        Thread.sleep(3);
                    } catch (InterruptedException e) {
                    }
                }
            }
        };
        Thread workerThread = new Thread(worker, "RadarScreen Indicator Disable Thread");
        workerThread.start();
    }

    public int getDataCapacity() {
        return dataCapacity;
    }

    public void setDataCapacity(int dataCapacity) {
        this.dataCapacity = dataCapacity;
    }

    public void addFrame(IntraDayOHLC ohlcRecord, String key) {
        ChartPoint chartPoint = new ChartPoint();
        chartPoint.Open = ohlcRecord.getOpen();
        chartPoint.High = ohlcRecord.getHigh();
        chartPoint.Low = ohlcRecord.getLow();
        chartPoint.Close = ohlcRecord.getClose();
        chartPoint.Volume = ohlcRecord.getVolume();
        ChartRecord chartRecord = new ChartRecord(chartPoint, ohlcRecord.getTime());
        chartRecord.setStepSize(1);
        RadarScreenData radarScreenData =
                RadarScreenInterface.getInstance().getSymbols().get(key);
        if (radarScreenData == null) {
            radarScreenData = new RadarScreenData(key);
            RadarScreenInterface.getInstance().getSymbols().put(key, radarScreenData);
        }
        radarScreenData.getDataArray().insert(chartRecord);
    }

    public void historyDownloaded(String exchange) {

    }

    public void historicalIntradayDownload(String exchange) {
        Enumeration<RadarScreenData> elements = symbols.elements();
        while (elements.hasMoreElements()) {
            RadarScreenData radarScreenData = elements.nextElement();
            String ex = SharedMethods.getExchangeFromKey(radarScreenData.getKey());
            if (ex.equals(exchange)) {
                radarScreenData.setLoaded(false);
            }
        }
    }
}
