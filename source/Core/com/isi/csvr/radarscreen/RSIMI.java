package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.IntradayMomentumIndex;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 4:47:49 PM
 */
public class RSIMI {

    private static int imiTimePeriod = 14;

    private static long lastVisibleTime = 0L;

    private double value;

    public static int getImiTimePeriod() {
        return imiTimePeriod;
    }

    public static void setImiTimePeriod(int imiTimePeriod) {
        RSIMI.imiTimePeriod = imiTimePeriod;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSIMI.lastVisibleTime = lastVisibleTime;
    }

    public void calculateIMI(RadarScreenData radarScreenData) {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
            if (radarScreenData.getDataArray().getSize() < (imiTimePeriod * interval)) {
                radarScreenData.getDataArray().setMaxCapacity(
                        imiTimePeriod * RadarScreenInterface.getInstance().getInterval());
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(RadarScreenInterface.getInstance().getInterval());
            }
        } else {
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        if (listToBeUsed.size() >= imiTimePeriod) {
            value = IntradayMomentumIndex.getIMI(listToBeUsed, imiTimePeriod);
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
