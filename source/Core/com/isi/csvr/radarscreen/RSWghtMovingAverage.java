package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.MovingAverage;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 4:34:26 PM
 */
public class RSWghtMovingAverage {

    private static int wghtMovAvgTimePeriod = 21;
    private static byte source = GraphDataManager.INNER_Close;

    private static long lastVisibleTime = 0L;

    private double value;

    public static int getWghtMovAvgTimePeriod() {
        return wghtMovAvgTimePeriod;
    }

    public static void setWghtMovAvgTimePeriod(int wghtMovAvgTimePeriod) {
        RSWghtMovingAverage.wghtMovAvgTimePeriod = wghtMovAvgTimePeriod;
    }

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSWghtMovingAverage.source = source;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSWghtMovingAverage.lastVisibleTime = lastVisibleTime;
    }

    public void calculateMA(RadarScreenData radarScreenData) {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
            if (radarScreenData.getDataArray().getSize() < (wghtMovAvgTimePeriod * interval)) {
                radarScreenData.getDataArray().setMaxCapacity(
                        wghtMovAvgTimePeriod * RadarScreenInterface.getInstance().getInterval());
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(RadarScreenInterface.getInstance().getInterval());
            }
        } else { //Default
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        if (listToBeUsed.size() >= wghtMovAvgTimePeriod) {
            value = MovingAverage.getMovingAverage(listToBeUsed, wghtMovAvgTimePeriod, MovingAverage.METHOD_WEIGHTED,
                    source, ChartRecord.STEP_1);
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
