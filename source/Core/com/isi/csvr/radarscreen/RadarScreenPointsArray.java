package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartPoint;
import com.isi.csvr.chart.ChartRecord;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 15, 2006
 * Time: 2:53:22 PM
 */
public class RadarScreenPointsArray {

    private LinkedList<ChartRecord> list;
    private LinkedList<ChartRecord> preparedList;
    private Comparator<ChartRecord> comparator;
    private int maxCapacity;

    private long dataPointInsertedTime = 0L;
    private long preparedTime = 0L;

    /**
     * @param maxCapacity Maximum number of elements. The internal list never exceeds this size. If this is -1 there is no restriction
     */
    public RadarScreenPointsArray(int maxCapacity) {
        this.maxCapacity = maxCapacity;
        list = new LinkedList<ChartRecord>();
        preparedList = new LinkedList<ChartRecord>();
        comparator = new Comparator<ChartRecord>() {
            public int compare(ChartRecord o1, ChartRecord o2) {
                if (o1.Time > o2.Time) {
                    return 1;
                } else if (o1.Time < o2.Time) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
    }

    public LinkedList<ChartRecord> getList() {
        return list;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public LinkedList<ChartRecord> getPreparedList() {
        return preparedList;
    }

    public synchronized void insert(ChartRecord chartRecord) {
        int location = Collections.binarySearch(list, chartRecord, comparator);
        if (location >= 0) {
            list.set(location, chartRecord); // replace the element
        } else {
            location = ((location + 1) * -1);
            list.add(location, chartRecord);
            if (maxCapacity >= 0 && list.size() > maxCapacity) {
                list.remove(0);
            }
        }
        dataPointInsertedTime = System.currentTimeMillis();
    }

    public int getSize() {
        return list.size();
    }

    public synchronized void clearList() {
        list.clear();
    }

    public ChartRecord get(int index) {
        return list.get(index);
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public synchronized void prepareForInterval(int intervalInMinutes) {
        preparedList.clear();
        for (ChartRecord chartRecord : list) {
            long tmpTime = chartRecord.Time;
            long currentBeginTime = tmpTime / intervalInMinutes;
            currentBeginTime = currentBeginTime * intervalInMinutes;

            ChartPoint cp = new ChartPoint();
            cp.Open = chartRecord.Open;
            cp.High = chartRecord.High;
            cp.Low = chartRecord.Low;
            cp.Close = chartRecord.Close;
            cp.Volume = chartRecord.Volume;
            ChartRecord cr = new ChartRecord(cp, currentBeginTime);
            cr.setStepSize(1);
            insertToPreparedList(cr);
        }
        preparedTime = System.currentTimeMillis();
    }

    private int insertToPreparedList(ChartRecord newRecord) {
        int location = Collections.binarySearch(preparedList, newRecord, comparator);
        if (location >= 0) {
            ChartRecord cr = preparedList.get(location);
            if (newRecord.High > cr.High) {
                cr.High = newRecord.High;
            }
            if (newRecord.Low < cr.Low) {
                cr.Low = newRecord.Low;
            }
            cr.Close = newRecord.Close;
            cr.Volume += newRecord.Volume;
            preparedList.set(location, cr);
        } else {
            location = ((location + 1) * -1);
            preparedList.add(location, newRecord);
        }
        return location;
    }

    public boolean isNewPointInserted() {
        return (dataPointInsertedTime >= preparedTime);
    }

    public long getDataPointInsertedTime() {
        return dataPointInsertedTime;
    }

}

