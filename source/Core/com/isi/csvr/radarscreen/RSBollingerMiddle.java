package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.BollingerBand;
import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.MovingAverage;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Sep 4, 2006
 * Time: 11:06:13 AM
 */
public class RSBollingerMiddle {
    private static int bollingerMiddleTimePeriod = 20;
    private static byte source = GraphDataManager.INNER_Close;
    private static byte method = MovingAverage.METHOD_SIMPLE;

    private static long lastVisibleTime = 0L;

    private double value;

    public static int getBollingerMiddleTimePeriod() {
        return bollingerMiddleTimePeriod;
    }

    public static void setBollingerMiddleTimePeriod(int bollingerMiddleTimePeriod) {
        RSBollingerMiddle.bollingerMiddleTimePeriod = bollingerMiddleTimePeriod;
    }

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSBollingerMiddle.source = source;
    }

    public static byte getMethod() {
        return method;
    }

    public static void setMethod(byte method) {
        RSBollingerMiddle.method = method;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSBollingerMiddle.lastVisibleTime = lastVisibleTime;
    }

    public void calculateBollingerMiddle(RadarScreenData radarScreenData) {
        LinkedList<ChartRecord> listToBeUsed;

        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
            if (method == MovingAverage.METHOD_EXPONENTIAL && !radarScreenData.isFullyLoaded()) {
                radarScreenData.getDataArray().setMaxCapacity(-1); //load all
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(interval);
                radarScreenData.setFullyLoaded(true);
            } else if (radarScreenData.getDataArray().getSize() < (bollingerMiddleTimePeriod * interval)) {
                radarScreenData.getDataArray().setMaxCapacity(
                        bollingerMiddleTimePeriod * RadarScreenInterface.getInstance().getInterval());
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(interval);
            }
        } else {
            listToBeUsed = radarScreenData.getDataArray().getList();
            if (method == MovingAverage.METHOD_EXPONENTIAL && !radarScreenData.isFullyLoaded()) {
                radarScreenData.getDataArray().setMaxCapacity(-1); //load all
                radarScreenData.loadDataFromFiles();
                radarScreenData.setFullyLoaded(true);
            }
        }

        if (listToBeUsed.size() >= bollingerMiddleTimePeriod) {
            value = BollingerBand.getBollingerMiddle(listToBeUsed, bollingerMiddleTimePeriod,
                    method, source, ChartRecord.STEP_1);
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }
}
