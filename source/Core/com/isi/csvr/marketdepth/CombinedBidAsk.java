package com.isi.csvr.marketdepth;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 8, 2006
 * Time: 9:39:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class CombinedBidAsk {
    private double price;
    private long askQuantity;
    private int askSplits;
    private long bidQuantity;
    private int bidSplits;

    public CombinedBidAsk(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getAskQuantity() {
        return askQuantity;
    }

    public void setAskQuantity(long askQuantity) {
        this.askQuantity = askQuantity;
    }

    public int getAskSplits() {
        return askSplits;
    }

    public void setAskSplits(int askSplits) {
        this.askSplits = askSplits;
    }

    public long getBidQuantity() {
        return bidQuantity;
    }

    public void setBidQuantity(long bidQuantity) {
        this.bidQuantity = bidQuantity;
    }

    public int getBidSplits() {
        return bidSplits;
    }

    public void setBidSplits(int bidSplits) {
        this.bidSplits = bidSplits;
    }

    public String getDDEValue(int column) {

        switch (column) {
            case 0:
                return "" + getBidSplits();
            case 1:
                return "" + getBidQuantity();
            case 2:
                return "" + getPrice();
            case 3:
                return "" + getAskQuantity();
            case 4:
                return "" + getAskSplits();
        }
        return "";
    }
}
