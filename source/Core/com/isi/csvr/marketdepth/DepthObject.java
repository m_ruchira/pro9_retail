package com.isi.csvr.marketdepth;

import com.isi.csvr.shared.BidAsk;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWDecimalFormat;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class DepthObject {

    public static final byte BID = 1;
    public static final byte ASK = 2;

    public static final byte TYTE_PRICE = 0;
    public static final byte TYTE_ORDER = 1;
    public static final byte TYTE_SPECIAL = 2;

    private DynamicArray askOrderList = null;
    private DynamicArray bidOrderList = null;
    private DynamicArray askPriceList = null;
    private DynamicArray bidPriceList = null;
    private DynamicArray askSpecialList = null;
    private DynamicArray bidSpecialList = null;
    private DynamicArray comnbinedOrderList = null;

    private int askOrderListSize = 0;
    private int bidOrderListSize = 0;
    private int askPriceListSize = 0;
    private int bidPriceListSize = 0;
    private int askSpecialListSize = 0;
    private int bidSpecialListSize = 0;

    public DepthObject() {
        askOrderList = new DynamicArray(1);
        bidOrderList = new DynamicArray(1);
        askPriceList = new DynamicArray(1);
        bidPriceList = new DynamicArray(1);
        askSpecialList = new DynamicArray(1);
        bidSpecialList = new DynamicArray(1);
        comnbinedOrderList = new DynamicArray(1);
        comnbinedOrderList.setComparator(new CombinedBidAskComparator());
    }

    public void clear() {
        askOrderList.clear();
        bidOrderList.clear();
        askPriceList.clear();
        bidPriceList.clear();
        askSpecialList.clear();
        bidSpecialList.clear();

        askOrderList.trimToSize();
        bidOrderList.trimToSize();
        askPriceList.trimToSize();
        bidPriceList.trimToSize();
        askSpecialList.trimToSize();
        bidSpecialList.trimToSize();

        askOrderListSize = 0;
        bidOrderListSize = 0;
        askPriceListSize = 0;
        bidPriceListSize = 0;
        askSpecialListSize = 0;
        bidSpecialListSize = 0;
    }

    public DynamicArray getOrderList(byte type) {
        if (type == ASK)
            return askOrderList;
        else
            return bidOrderList;
    }

    public DynamicArray getPriceList(byte type) {
        if (type == ASK)
            return askPriceList;
        else
            return bidPriceList;
    }

    public DynamicArray getSpecialList(byte type) {
        if (type == ASK)
            return askSpecialList;
        else
            return bidSpecialList;
    }

    public void setOrderListSize(byte type, int size) {
        if (type == ASK)
            askOrderListSize = size;
        else
            bidOrderListSize = size;
    }

    public void setPriceListSize(byte type, int size) {
        if (type == ASK)
            askPriceListSize = size;
        else
            bidPriceListSize = size;
    }

    public void setSpecialListSize(byte type, int size) {
        if (type == ASK)
            askSpecialListSize = size;
        else
            bidSpecialListSize = size;
    }

    public int getOrderListSize(byte type) {
        if (type == ASK)
            return askOrderListSize;
        else
            return bidOrderListSize;
    }

    public int getPriceListSize(byte type) {
        if (type == ASK)
            return askPriceListSize;
        else
            return bidPriceListSize;
    }

    public int getSpecialListSize(byte type) {
        if (type == ASK)
            return askSpecialListSize;
        else
            return bidSpecialListSize;
    }

    public void rebuildCombinedView() {
        int location = 0;
        comnbinedOrderList.clear();

        CombinedBidAsk total = new CombinedBidAsk(0);

        for (int i = 0; i < getPriceListSize(BID); i++) {
            BidAsk bidAsk = (BidAsk) bidPriceList.get(i);
            CombinedBidAsk combinedBidAsk = new CombinedBidAsk(bidAsk.getPrice());
            combinedBidAsk.setBidQuantity(bidAsk.getQuantity());
            combinedBidAsk.setBidSplits(bidAsk.getSplits());
            total.setBidQuantity(total.getBidQuantity() + bidAsk.getQuantity());
            total.setBidSplits(total.getBidSplits() + bidAsk.getSplits());
            comnbinedOrderList.insert(combinedBidAsk);
        }

        for (int i = 0; i < getPriceListSize(ASK); i++) {
            BidAsk bidAsk = (BidAsk) askPriceList.get(i);
            CombinedBidAsk combinedBidAsk = new CombinedBidAsk(bidAsk.getPrice());
            location = comnbinedOrderList.indexOfContainingValue(combinedBidAsk);
            if (location < 0) {
                combinedBidAsk = new CombinedBidAsk(bidAsk.getPrice());
                combinedBidAsk.setAskQuantity(bidAsk.getQuantity());
                combinedBidAsk.setAskSplits(bidAsk.getSplits());
                comnbinedOrderList.insert(combinedBidAsk);
            } else {
                combinedBidAsk = (CombinedBidAsk) comnbinedOrderList.get(location);
                combinedBidAsk.setAskQuantity(bidAsk.getQuantity());
                combinedBidAsk.setAskSplits(bidAsk.getSplits());
            }
            total.setAskQuantity(total.getAskQuantity() + bidAsk.getQuantity());
            total.setAskSplits(total.getAskSplits() + bidAsk.getSplits());
        }
        comnbinedOrderList.add(total);
    }

    public DynamicArray getComnbinedOrderList() {
        return comnbinedOrderList;
    }

    public int getCombinedViewSize() {
        return comnbinedOrderList.size();
    }

    public String getXML(String key, String depthMode) {
        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = SharedMethods.getExchangeFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);

        StringBuilder buffer = new StringBuilder();
        buffer.append("<DEPTH>");
        buffer.append("<Symbol value=\"").append(symbol).append("\"/>");
        buffer.append("<iType value=\"").append(instrument).append("\"/>");
        buffer.append("<Exchange value=\"").append(exchange).append("\"/>");
        buffer.append("<DepthMode value=\"").append(depthMode).append("\"/>");
        buffer.append("<Data>");
//        buffer.append("<Data   Symbol=\"").append(symbol).append("\"Exchange=").append("\""+exchange+"").append("\" DepthType=").append("\""+depthMode).append("\"/>");
        TWDecimalFormat format = SharedMethods.getDecimalFormatNoComma(key);
        //bid values
        if (depthMode.equals(com.mubasher.priceAPI.Meta.DEPTH_TYPE_PRICE)) {
            //depth by price bid values
            for (int i = 0; i < getPriceListSize(BID); i++) {
                BidAsk bidAsk = (BidAsk) bidPriceList.get(i);
                buffer.append("<Bid    Sequence=\"").append("" + bidAsk.getDepthSequance() + "\"").append("   Price=\"").append("" + format.format(bidAsk.getPrice()) + "\"").append("   qty=\"").append("" + bidAsk.getQuantity() + "\"").append("   splits=\"").append(bidAsk.getSplits()).append("\"/>");
            }
        } else if (depthMode.equals(com.mubasher.priceAPI.Meta.DEPTH_TYPE_ORDER)) {
            //depth by order bid values
            for (int i = 0; i < getOrderListSize(BID); i++) {
                BidAsk bidAsk = (BidAsk) bidOrderList.get(i);
                buffer.append("<Bid    Sequence=\"").append("" + bidAsk.getDepthSequance() + "\"").append("   Price=\"").append("" + format.format(bidAsk.getPrice()) + "\"").append("   qty=\"").append("" + bidAsk.getQuantity() + "\"").append("   splits=\"").append(bidAsk.getSplits()).append("\"/>");
            }
        } else if (depthMode.equals(com.mubasher.priceAPI.Meta.DEPTH_TYPE_SPECIAL)) {
            //special depth bid values
            for (int i = 0; i < getSpecialListSize(BID); i++) {
                BidAsk bidAsk = (BidAsk) bidSpecialList.get(i);
                buffer.append("<Bid    Sequence=\"").append("" + bidAsk.getDepthSequance() + "\"").append("   Price=\"").append("" + format.format(bidAsk.getPrice()) + "\"").append("   qty=\"").append("" + bidAsk.getQuantity() + "\"").append("   splits=\"").append(bidAsk.getSplits()).append("\"/>");
            }
        }
        //ask values
        if (depthMode.equals(com.mubasher.priceAPI.Meta.DEPTH_TYPE_PRICE)) {
            //depth by price ask values
            for (int i = 0; i < getPriceListSize(ASK); i++) {
                BidAsk bidAsk = (BidAsk) askPriceList.get(i);
                buffer.append("<Ask     Sequence=\"").append("" + bidAsk.getDepthSequance() + "\"").append("   Price=\"").append("" + format.format(bidAsk.getPrice()) + "\"").append("  qty=\"").append("" + bidAsk.getQuantity() + "\"").append("   splits=\"").append(bidAsk.getSplits()).append("\"/>");
            }
        } else if (depthMode.equals(com.mubasher.priceAPI.Meta.DEPTH_TYPE_ORDER)) {
            //depth by order ask values
            for (int i = 0; i < getOrderListSize(ASK); i++) {
                BidAsk bidAsk = (BidAsk) askOrderList.get(i);
                buffer.append("<Ask     Sequence=\"").append("" + bidAsk.getDepthSequance() + "\"").append("    Price=\"").append("" + format.format(bidAsk.getPrice()) + "\"").append("   qty=\"").append("" + bidAsk.getQuantity() + "\"").append("   splits=\"").append(bidAsk.getSplits()).append("\"/>");
            }
        } else if (depthMode.equals(com.mubasher.priceAPI.Meta.DEPTH_TYPE_SPECIAL)) {
            //special depth ask values
            for (int i = 0; i < getSpecialListSize(ASK); i++) {
                BidAsk bidAsk = (BidAsk) askSpecialList.get(i);
                buffer.append("<Ask     Sequence=\"").append("" + bidAsk.getDepthSequance() + "\"").append("    Price=\"").append("" + format.format(bidAsk.getPrice()) + "\"").append("   qty=\"").append("" + bidAsk.getQuantity() + "\"").append("   splits=\"").append(bidAsk.getSplits()).append("\"/>");
            }
        }
        buffer.append("</Data>");
        buffer.append("</DEPTH>\n");

        return buffer.toString();
    }
}