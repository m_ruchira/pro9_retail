package com.isi.csvr.marketdepth;

import com.isi.csvr.portfolio.CommissionObject;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TraderProfileDataListener;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author : Bandula
 */
public class MDepthCalculator extends JPanel implements Runnable,
        Themeable, ActionListener, TraderProfileDataListener, TradingConnectionListener {

    private ViewSetting oViewSetting;
    private DepthCalculatorModel dataModel;

    //private TWTextField  txtSymbol   = new TWTextField();
    private ArrayList<TWComboItem> portfolioList;
    private ArrayList<TWComboItem> comissionList;
    private JComboBox cmbPortfolios;
    private JComboBox cmbCommision;
    private TWTextField txtTarget;
    private JRadioButton rbPriceMode;
    private JRadioButton rbQuantityMode;
    private JRadioButton rbBuy;
    private JRadioButton rbSell;
    private TWButton btnCalculate;
    private TWButton btnBuy;
    private TWButton btnSell;

    //    //private boolean isDepthByPrice = true;
    private boolean isActive = true;
    private String exchange = null;
    private String symbol = null;
    private int instrument = -1;
    private String lastSymbol = "";
    private String lastValue = "";
    private long enteredQty = 0;
    private double fWAAP = 0;
    private double fWABP = 0;
    private TWTypes.TradeSides side;
    private boolean portfolioListenerOK = false;
    private Thread thread;
    private boolean isInterrupted = false;
    //change start
    private String selectedPortfolio = null;
    private int selportfolio = 0;

    public MDepthCalculator(String key, DepthCalculatorModel model, TWTypes.TradeSides side) {
        this.side = side;
        Theme.registerComponent(this);
        dataModel = model;
        this.symbol = SharedMethods.getSymbolFromKey(key);
        this.exchange = SharedMethods.getExchangeFromKey(key);
        this.instrument = SharedMethods.getInstrumentTypeFromKey(key);
        createLayout();
        setSide(side);

        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        try {
            synchronized (this) {
                TradingShared.getTrader().addAccountListener(this);
                setPortfolioListenerOK(true);
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
        thread = new Thread(this, "Depth Calculator");
        thread.start();
    }

    public void changeSymbol(String sKey) {
        this.symbol = SharedMethods.getSymbolFromKey(sKey);
        this.exchange = SharedMethods.getExchangeFromKey(sKey);
        this.instrument = SharedMethods.getInstrumentTypeFromKey(sKey);

    }

    private void createLayout() {
        this.setLayout(new BorderLayout());
        // #####################################################################
        //      Create North Panel
        // #####################################################################
        JPanel configPanel = new JPanel();
        String[] widths = {"100%"};
        String[] heights = {"20", "20", "20", "20", "20", "20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(widths, heights, 2, 2);

        configPanel.setLayout(flexGridLayout);

        JLabel lblSide = new JLabel(Language.getString("SIDE"));
        rbBuy = new JRadioButton(Language.getString("BUY"));
        if (side == TWTypes.TradeSides.BUY) {
            rbBuy.setSelected(true);
        }
        rbBuy.addActionListener(this);
        rbSell = new JRadioButton(Language.getString("SELL"));
        if (side == TWTypes.TradeSides.SELL) {
            rbSell.setSelected(true);
        }
        rbSell.addActionListener(this);
        ButtonGroup group1 = new ButtonGroup();
        group1.add(rbSell);
        group1.add(rbBuy);
        configPanel.add(createPanel(lblSide, rbBuy, rbSell));

        JLabel lblQuantity = new JLabel(Language.getString("TARGET"));
        lblQuantity.setHorizontalAlignment(SwingConstants.LEADING);

        txtTarget = new TWTextField();
        txtTarget.setHorizontalAlignment(SwingConstants.RIGHT);
        txtTarget.setDocument(new ValueFormatter(ValueFormatter.LONG));
        txtTarget.setText("0");
        txtTarget.setPreferredSize(new Dimension(90, 18));
        txtTarget.setMinimumSize(new Dimension(90, 18));
        txtTarget.setMaximumSize(new Dimension(90, 18));
        txtTarget.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                if ((e.getKeyCode() == KeyEvent.VK_ENTER) && ((e.getID() & e.KEY_RELEASED) == e.KEY_RELEASED)) {
                    quantityChange();
                }
            }
        });

        btnCalculate = new TWButton(Language.getString("CALCULATE"));
        btnCalculate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quantityChange();
            }
        });

        configPanel.add(createPanel(lblQuantity, txtTarget, btnCalculate));

        JLabel lblType = new JLabel(Language.getString("DEPTH_CALC_TYPE"));
        rbPriceMode = new JRadioButton(Language.getString("VALUE"));
        rbQuantityMode = new JRadioButton(Language.getString("QUANTITY"));
        rbQuantityMode.setSelected(true);
        rbPriceMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quantityChange();
            }
        });
        rbQuantityMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quantityChange();
            }
        });
        ButtonGroup group = new ButtonGroup();
        group.add(rbPriceMode);
        group.add(rbQuantityMode);

        configPanel.add(createPanel(lblType, rbPriceMode, rbQuantityMode));

        JLabel lblPortfolio = new JLabel(Language.getString("PORTFOLIO"));
        portfolioList = new ArrayList<TWComboItem>();
        TWComboModel portfolioModel = new TWComboModel(portfolioList);
        cmbPortfolios = new JComboBox(portfolioModel);
        cmbPortfolios.setEnabled(false);
        cmbPortfolios.addActionListener(this);
        configPanel.add(createPanel(lblPortfolio, cmbPortfolios, new JLabel()));

        JLabel lblComission = new JLabel(Language.getString("COMMISSION_RATE"));
        comissionList = new ArrayList<TWComboItem>();
        TWComboModel comissionModel = new TWComboModel(comissionList);
        cmbCommision = new JComboBox(comissionModel);
        cmbCommision.setEnabled(true);
        cmbCommision.addActionListener(this);
        configPanel.add(createPanel(lblComission, cmbCommision, new JLabel()));


        btnBuy = new TWButton(Language.getString("BUY"));
        btnBuy.setEnabled(TradingShared.isReadyForTrading());
        btnBuy.setGradientDark("BOARD_TABLE_CELL_BID_BGCOLOR1");
        btnBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quantityChange();
                sendOrders(TradeMeta.BUY);
            }
        });
        btnSell = new TWButton(Language.getString("SELL"));
        btnSell.setEnabled(TradingShared.isReadyForTrading());
        btnSell.setGradientDark("BOARD_TABLE_CELL_ASK_BGCOLOR1");
        btnSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quantityChange();
                sendOrders(TradeMeta.SELL);
            }
        });

        configPanel.add(createPanel(new JLabel(), btnBuy, btnSell));

        populatePortfolios();
        populateComissionRates();
        enableButons(TradingShared.isReadyForTrading());
        GUISettings.applyOrientation(this);
        this.add(configPanel, BorderLayout.CENTER);
    }

    /*private void createLayout() {
        this.setLayout(new BorderLayout(5,5));
        // #####################################################################
        //      Create North Panel
        // #####################################################################
        JPanel configPanel = new JPanel();
        String[] widths = {"100%"};
        String[] heights = {"20", "20", "20", "20", "20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(widths, heights, 5,5, true, true);

        configPanel.setLayout(flexGridLayout);
//        configPanel.setBorder(BorderFactory.createEmptyBorder(2,5,2,5));

        JLabel lblSide = new JLabel(Language.getString("SIDE"));
        rbBuy = new JRadioButton(Language.getString("BUY"));
        if (side == TWTypes.TradeSides.BUY) {
            rbBuy.setSelected(true);
        }
        rbBuy.addActionListener(this);
        rbSell = new JRadioButton(Language.getString("SELL"));
        if (side == TWTypes.TradeSides.SELL) {
            rbSell.setSelected(true);
        }
        rbSell.addActionListener(this);
        ButtonGroup group1 = new ButtonGroup();
        group1.add(rbSell);
        group1.add(rbBuy);
        configPanel.add(createPanel(lblSide, rbBuy, rbSell));
//        configPanel.add(rbBuy);
//        configPanel.add(rbSell);

        JLabel lblQuantity = new JLabel(Language.getString("TARGET"));
        lblQuantity.setHorizontalAlignment(SwingConstants.LEADING);
//        lblQuantity.setPreferredSize(new Dimension(80, 16));
//        lblQuantity.setMaximumSize(new Dimension(80, 16));
//        lblQuantity.setMinimumSize(new Dimension(80, 16));
        //lblQuantity.setFont(new java.awt.Font("Dialog", Font.PLAIN, 13));

        txtTarget = new TWTextField();
        txtTarget.setHorizontalAlignment(SwingConstants.RIGHT);
        txtTarget.setDocument(new ValueFormatter(ValueFormatter.LONG));
        txtTarget.setText("0");
        txtTarget.setPreferredSize(new Dimension(90, 18));
        txtTarget.setMinimumSize(new Dimension(90, 18));
        txtTarget.setMaximumSize(new Dimension(90, 18));
        txtTarget.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                if ((e.getKeyCode() == KeyEvent.VK_ENTER) && ((e.getID() & e.KEY_RELEASED) == e.KEY_RELEASED)) {
                    quantityChange();
                }
            }
        });

        btnCalculate = new TWButton(Language.getString("CALCULATE"));
        btnCalculate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quantityChange();
            }
        });

        configPanel.add(createPanel(lblQuantity, txtTarget, btnCalculate));
//        configPanel.add(lblQuantity);
//        configPanel.add(txtTarget);
//        configPanel.add(new JLabel());

        JLabel lblType = new JLabel(Language.getString("DEPTH_CALC_TYPE"));
        rbPriceMode = new JRadioButton(Language.getString("VALUE"));
        rbQuantityMode = new JRadioButton(Language.getString("QUANTITY"));
        rbQuantityMode.setSelected(true);
        rbPriceMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quantityChange();
            }
        });
        rbQuantityMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quantityChange();
            }
        });
        ButtonGroup group = new ButtonGroup();
        group.add(rbPriceMode);
        group.add(rbQuantityMode);

        configPanel.add(createPanel(lblType, rbPriceMode, rbQuantityMode));

        JLabel lblPortfolio = new JLabel(Language.getString("PORTFOLIO"));
        portfolioList = new ArrayList<TWComboItem>();
        TWComboModel portfolioModel = new TWComboModel(portfolioList);
        cmbPortfolios = new JComboBox(portfolioModel);
        cmbPortfolios.setEnabled(false);

        configPanel.add(createPanel(lblPortfolio, cmbPortfolios, new JLabel()));


        btnBuy = new TWButton(Language.getString("BUY"));
        btnBuy.setEnabled(TradingShared.isReadyForTrading());
        btnBuy.setGradientDark("BOARD_TABLE_CELL_BID_BGCOLOR1");
        btnBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendOrders(TradeMeta.BUY);
            }
        });
        btnSell = new TWButton(Language.getString("SELL"));
        btnSell.setEnabled(TradingShared.isReadyForTrading());
        btnSell.setGradientDark("BOARD_TABLE_CELL_ASK_BGCOLOR1");
        btnSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendOrders(TradeMeta.SELL);
            }
        });

        configPanel.add(createPanel(new JLabel(), btnBuy, btnSell));

        populatePortfolios();
        enableButons(TradingShared.isReadyForTrading());
        GUISettings.applyOrientation(this);
        this.add(configPanel, BorderLayout.CENTER);
    }*/

    public boolean isPortfolioListenerOK() {
        synchronized (this) {
            return portfolioListenerOK;
        }
    }

    public void setPortfolioListenerOK(boolean portfolioListenerOK) {
        synchronized (this) {
            this.portfolioListenerOK = portfolioListenerOK;
        }
    }

    public String getPrintingString() {
        String str;
        str = Language.getString("SIDE") + " , ";
        if (rbBuy.isSelected()) {
            str = str + Language.getString("BUY") + " , ";
        } else {
            str = str + Language.getString("SELL") + " , ";
        }
        str = str + Language.getString("TARGET") + " , ";
        str = str + txtTarget.getText().trim() + " , ";
        str = str + Language.getString("DEPTH_CALC_TYPE") + " , ";
        if (rbPriceMode.isSelected()) {
            str = str + Language.getString("VALUE") + " , ";
        } else {
            str = str + Language.getString("QUANTITY") + " , ";
        }
        str = str + Language.getString("PORTFOLIO") + " , ";

        try {
            str = str + ((TWComboItem) cmbPortfolios.getSelectedItem()).getValue();
        } catch (Exception e) {
            str = str + "";
        }
        try {
            str = str + ((TWComboItem) cmbCommision.getSelectedItem()).getValue();
        } catch (Exception e) {
            str = str + "";
        }
        return str;
    }

    private JPanel createPanel(Component component1, Component component2, Component component3) {
        JPanel panel = new JPanel();
        String[] widths = {"100%", "100", "100"};
        String[] heights = {"20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(widths, heights);
        panel.setLayout(flexGridLayout);
        panel.add(component1);
        panel.add(component2);
        panel.add(component3);

        return panel;
    }

    /*private JPanel createPanel(Component component1, Component component2){
        JPanel panel = new JPanel();
        String[] widths = {"150", "150"};
        String[] heights = {"20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(widths, heights);
        panel.setLayout(flexGridLayout);
        panel.add(component1);
        panel.add(component2);

        return panel;
    }*/

    private void quantityChange() {
        int amount = 0;
        try {
            String enteredValue = txtTarget.getText().trim();
            /*if (enteredValue.equals(lastValue)){ //(!parent.isVisible()) ||
                lastValue = "";
                return;
            }*/
            lastValue = enteredValue;

            amount = Integer.parseInt(txtTarget.getText().trim());
            String value = null;
            try {
                value = ((TWComboItem) cmbCommision.getSelectedItem()).getId();
            } catch (Exception e) {
                value = "";
            }
            //  comission = PFStore.getInstance().getCommission(value);

            if (rbPriceMode.isSelected())
                dataModel.setQuantity(amount, DepthCalculatorModel.MODE_PRICE, value);
            else
                dataModel.setQuantity(amount, DepthCalculatorModel.MODE_QUANTITY, value);
//            enableCalcButton(false);
            interruptThread();
            oViewSetting.putProperty(ViewConstants.VC_DEPTH_CALC_VALUE, txtTarget.getText().trim());
            oViewSetting.putProperty(ViewConstants.VC_DEPTH_CALC_TYPE, rbPriceMode.isSelected());
        } catch (Exception e2) {
            //new ShowMessage(Language.getString("MSG_DEPTH_CAL_INVLID_INPUT"),"E");
        }
    }

    private synchronized void populatePortfolios() {
        try {
            if (TradingShared.getTrader() != null) {
                portfolioList.clear();
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());
                    portfolioList.add(item);
                    item = null;
                    record = null;
                }
                Collections.sort(portfolioList);
                cmbPortfolios.updateUI();
                //change start
                if (selectedPortfolio != null) {
                    try {
                        cmbPortfolios.setSelectedIndex(selportfolio);
                    } catch (Exception e) {
                        cmbPortfolios.setSelectedIndex(0);
                    }
                } else {
                    cmbPortfolios.setSelectedIndex(0);
                }
                selportfolio = cmbPortfolios.getSelectedIndex();
                selectedPortfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
                dataModel.setPortfolio(selectedPortfolio);
                //change end
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized void populateComissionRates() {

        comissionList.clear();
        CommissionObject commision = null;
        ArrayList<String> itemlist = PFStore.getInstance().getCommissionList();

        for (int i = 0; i < itemlist.size(); i++) {
            commision = PFStore.getInstance().getCommission(itemlist.get(i));
            TWComboItem item = new TWComboItem(commision.getTimeStamp() + "", commision.getId());
            comissionList.add(item);
            item = null;
        }
        comissionList.add(new TWComboItem(Language.getString("BROKER"), Language.getString("BROKER")));

    }


    public void killThread() {
        isActive = false;
    }

    public void applyTheme() {
    }

    /**
     * Returns the view setting object for this table
     */
    public ViewSetting getViewSettings() {
        return oViewSetting;
    }

    /**
     * Returns the view setting object for this table
     */
    public void setViewSettings(ViewSetting vw) {
        oViewSetting = vw;
        String value = vw.getProperty(ViewConstants.VC_DEPTH_CALC_VALUE);
        if ((value != null) && (!value.trim().equals("null")))
            txtTarget.setText(value);
        value = vw.getProperty(ViewConstants.VC_DEPTH_CALC_TYPE);
        if ((value != null) && (!value.trim().equals("null"))) {
            try {
                rbPriceMode.setSelected(SharedMethods.booleanValue(value, false));
                rbQuantityMode.setSelected(!rbPriceMode.isSelected());
            } catch (Exception e) {
                rbPriceMode.setSelected(false);
                rbQuantityMode.setSelected(true);
            }
        } else {
            rbPriceMode.setSelected(false);
            rbQuantityMode.setSelected(true);
        }
        quantityChange();
        value = null;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == rbBuy) {
            setSide(TWTypes.TradeSides.BUY);
        } else if (e.getSource() == rbSell) {
//            dataModel.setSide(TWTypes.TradeSides.SELL);
//            interruptThread();
            setSide(TWTypes.TradeSides.SELL);
        } else if (e.getSource() == cmbPortfolios) {
            //change start
            selportfolio = cmbPortfolios.getSelectedIndex();
            selectedPortfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
            dataModel.setPortfolio(selectedPortfolio);
            //change end
            interruptThread();
        } else if (e.getSource() == cmbCommision) {
            quantityChange();
        }
    }

    private void setSide(TWTypes.TradeSides side) {
        this.side = side;
        if (side == TWTypes.TradeSides.BUY) {
            dataModel.setSide(TWTypes.TradeSides.BUY);
            btnBuy.setEnabled(TradingShared.isReadyForTrading()); // enable if trade path is open
            btnSell.setEnabled(false);
        } else {
            dataModel.setSide(TWTypes.TradeSides.SELL);
            btnBuy.setEnabled(false);
            btnSell.setEnabled(TradingShared.isReadyForTrading());// enable if trade path is open
        }
        interruptThread();
    }

    private void enableButons(boolean status) {
        cmbPortfolios.setEnabled(status);

        if (status == true) {
            if (side == TWTypes.TradeSides.BUY) {
                btnBuy.setEnabled(true);
                btnSell.setEnabled(false);
            } else {
                btnBuy.setEnabled(false);
                btnSell.setEnabled(true);
            }
        } else {
            btnBuy.setEnabled(false);
            btnSell.setEnabled(false);
        }
    }

    private void sendOrders(int tradeSide) {
        if (TradeMethods.isSymbolAllowedForProtfolio(selectedPortfolio, exchange, true)) {
            if (TradingShared.isShariaTestPassed(exchange, symbol, instrument)) {
                java.util.List<Order> orders = dataModel.getOrders();
                synchronized (orders) {
                    boolean firstOrder = true;
                    if (orders.size() > 0) {
                        String msg;
                        if (tradeSide == TradeMeta.BUY) {
                            msg = Language.getString("MSG_CONFIRM_DEPTHCALC_BUY");
                        } else {
                            msg = Language.getString("MSG_CONFIRM_DEPTHCALC_SELL");
                        }
                        msg = msg.replaceAll("\\[QTY\\]", "" + dataModel.getTotalShares());
                        msg = msg.replaceAll("\\[SYMBOL\\]", symbol);
                        int result = SharedMethods.showConfirmMessage(msg, JOptionPane.INFORMATION_MESSAGE);
                        if (result == JOptionPane.OK_OPTION) {
                            //                        for(int i =0; i < orders.size(); i++) {
                            //                        Order order = orders.get(i);
                            for (Order order : orders) {
                                TradeMethods.getSharedInstance().sendNewOrder(firstOrder, false, 0, TradingShared.getTrader().getPortfolioID(0), exchange, symbol,
                                        tradeSide, TradeMeta.ORDER_TYPE_LIMIT, order.getPrice(), 0, "*",
                                        (int) order.getQuantity(), (short) 0, 0, -1, 0, false, 0, null, Meta.INSTRUMENT_QUOTE, -1, null, -1, null, null, null, -1, null, null, null, 0d, null, 0);
                                firstOrder = false;
                                //                            order = null;
                            }
                        }
                    }
                }
            }
        }
    }

    public void finalizeWinow() {
        TradingConnectionNotifier.getInstance().removeConnectionListener(this);
        if (isPortfolioListenerOK()) {
            try {
                synchronized (this) {
                    TradingShared.getTrader().removeAccountListener(this);
                    setPortfolioListenerOK(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void accountDataChanged(String accountID) {
        populatePortfolios();
        populateComissionRates();
    }

    public void portfolioDataChanged(String portfolioID) {
    }

    public void tradeServerConnected() {
        enableButons(true);
        if (!isPortfolioListenerOK()) {
            try {
                synchronized (this) {
                    TradingShared.getTrader().addAccountListener(this);
                    setPortfolioListenerOK(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        enableButons(false);
        //change start
        dataModel.setPortfolio(null);
        interruptThread();
    }

    public void run() {
        while (isActive) {
            try {
                dataModel.doCalculated();
                if (!isInterrupted) {
                    Thread.sleep(4000);
                } else {
                    isInterrupted = false;
                }
            } catch (Exception e) {
            }
        }
    }

    public void interruptThread() {
        try {
            isInterrupted = true;
            dataModel.setActive(false);
            thread.interrupt();
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void enableCalcButton(boolean enable) {
        btnCalculate.setEnabled(enable);
    }
}