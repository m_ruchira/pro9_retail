package com.isi.csvr.marketdepth;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 9, 2006
 * Time: 8:59:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class CombinedDepthModel extends CommonTable
        implements TableModel, CommonTableInterface, ClipboardOwner, DDELinkInterface {

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private String g_sSymbol;
    private DepthObject depth;
    private CombinedBidAsk bidAsk;
    private Clipboard clip;
    //private Stock  g_oStock;

    /**
     * Constructor
     */
    public CombinedDepthModel(String g_sSymbol) {
        this.g_sSymbol = g_sSymbol;
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {
        this.g_sSymbol = symbol;
    }

    public int getRowCount() {

        try {
            depth = DepthStore.getInstance().getDepthFor(g_sSymbol);
            depth.rebuildCombinedView();
            return depth.getCombinedViewSize();
        } catch (Exception e) {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {

        try {

            depth = DepthStore.getInstance().getDepthFor(g_sSymbol);
            bidAsk = (CombinedBidAsk) depth.getComnbinedOrderList().get(iRow);

            switch (iCol) {
                case -1:
                    if (bidAsk.getBidQuantity() > 0) {
                        return longTrasferObject.setValue(1);
                    } else {
                        return longTrasferObject.setValue(0);
                    }
                case 0:
                    return longTrasferObject.setValue(bidAsk.getBidSplits());
                case 1:
                    return longTrasferObject.setValue(bidAsk.getBidQuantity());
                case 2:
                    return doubleTransferObject.setValue(bidAsk.getPrice());
                case 3:
                    return longTrasferObject.setValue(bidAsk.getAskQuantity());
                case 4:
                    return longTrasferObject.setValue(bidAsk.getAskSplits());
                default:
                    return longTrasferObject.setValue(0);
            }
        } catch (Exception e) {
            return "0";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }

    public void getDDEString(Table table, boolean withHeadings) {

        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (withHeadings)
            buffer.append(copyHeaders(table));
        for (int r = row; r < (rows + row); r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    buffer.append("=MRegionalDdeServer|'");
                    buffer.append("CB");
                    buffer.append(g_sSymbol);
                    buffer.append("'!'");
                    buffer.append(modelIndex + (r * 100));
                    buffer.append("'");
                    buffer.append("*1");

                    if (c != (cols))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        //int row = this.getSelectedRow();
        //int rows= this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        for (int c = 0; c < cols; c++) {
            int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public CustomizerRecord[] getCustomizerRecords() {
        /*CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"), Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));

        customizerRecords[1] = new CustomizerRecord(Language.getString("BID_ROW_1"), FIELD_BID_ROW1, Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_BID_FGCOLOR1"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("BID_ROW_2"), FIELD_BID_ROW2, Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR2"), Theme.getColor("BOARD_TABLE_CELL_BID_FGCOLOR2"));

        customizerRecords[3] = new CustomizerRecord(Language.getString("OFFER_ROW_1"), FIELD_ASK_ROW1, Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_ASK_FGCOLOR1"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("OFFER_ROW_2"), FIELD_ASK_ROW2, Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR2"), Theme.getColor("BOARD_TABLE_CELL_ASK_FGCOLOR2"));

        customizerRecords[5] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        return customizerRecords;*/

        /*CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"), Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));

        customizerRecords[1] = new CustomizerRecord(Language.getString("BID_ROW_1"), FIELD_COMBINED_BID_ROW1, Theme.getColor("COMBINED_DEPTH_TABLE_BID_BGCOLOR1"), Theme.getColor("COMBINED_DEPTH_TABLE_BID_FGCOLOR1"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("BID_ROW_2"), FIELD_COMBINED_BID_ROW2, Theme.getColor("COMBINED_DEPTH_TABLE_BID_BGCOLOR2"), Theme.getColor("COMBINED_DEPTH_TABLE_BID_FGCOLOR2"));

        customizerRecords[3] = new CustomizerRecord(Language.getString("OFFER_ROW_1"), FIELD_COMBINED_ASK_ROW1, Theme.getColor("COMBINED_DEPTH_TABLE_ASK_BGCOLOR1"), Theme.getColor("COMBINED_DEPTH_TABLE_ASK_FGCOLOR1"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("OFFER_ROW_2"), FIELD_COMBINED_ASK_ROW2, Theme.getColor("COMBINED_DEPTH_TABLE_ASK_BGCOLOR2"), Theme.getColor("COMBINED_DEPTH_TABLE_ASK_FGCOLOR2"));

        customizerRecords[5] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_COMBINED_SELECTED_ROW, Theme.getColor("COMBINED_DEPTH_TABLE_SELECTED_BGCOLOR"), Theme.getColor("COMBINED_DEPTH_TABLE_SELECTED_FGCOLOR"));
        return customizerRecords;*/

        CustomizerRecord[] customizerRecords = new CustomizerRecord[8];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));    //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")

        customizerRecords[1] = new CustomizerRecord(Language.getString("BID_ROW_1"), FIELD_COMBINED_BID_ROW1, Theme.getColor("COMBINED_DEPTH_TABLE_BID_BGCOLOR1"), Theme.getColor("COMBINED_DEPTH_TABLE_BID_FGCOLOR1"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("BID_ROW_2"), FIELD_COMBINED_BID_ROW2, Theme.getColor("COMBINED_DEPTH_TABLE_BID_BGCOLOR2"), Theme.getColor("COMBINED_DEPTH_TABLE_BID_FGCOLOR2"));

        customizerRecords[3] = new CustomizerRecord(Language.getString("OFFER_ROW_1"), FIELD_COMBINED_ASK_ROW1, Theme.getColor("COMBINED_DEPTH_TABLE_ASK_BGCOLOR1"), Theme.getColor("COMBINED_DEPTH_TABLE_ASK_FGCOLOR1"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("OFFER_ROW_2"), FIELD_COMBINED_ASK_ROW2, Theme.getColor("COMBINED_DEPTH_TABLE_ASK_BGCOLOR2"), Theme.getColor("COMBINED_DEPTH_TABLE_ASK_FGCOLOR2"));

        customizerRecords[5] = new CustomizerRecord(Language.getString("PRICE_ROW_1"), FIELD_COMBINED_PRICE_ROW1, Theme.getColor("COMBINED_DEPTH_TABLE_PRICE_BGCOLOR1"), Theme.getColor("COMBINED_DEPTH_TABLE_PRICE_FGCOLOR1"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("PRICE_ROW_2"), FIELD_COMBINED_PRICE_ROW2, Theme.getColor("COMBINED_DEPTH_TABLE_PRICE_BGCOLOR2"), Theme.getColor("COMBINED_DEPTH_TABLE_PRICE_FGCOLOR2"));

        customizerRecords[7] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_COMBINED_SELECTED_ROW, Theme.getColor("COMBINED_DEPTH_TABLE_SELECTED_BGCOLOR"), Theme.getColor("COMBINED_DEPTH_TABLE_SELECTED_FGCOLOR"));
        return customizerRecords;
    }
}
