package com.isi.csvr.marketdepth;

import com.isi.csvr.Client;
import com.isi.csvr.RequestManager;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 16, 2006
 * Time: 11:08:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class DepthCalculatorWindow extends InternalFrame implements LinkedWindowListener {//implements Runnable{

    private ViewSettingsManager viewManager;
    private String symbol;
    private Table table;
    private DepthCalculatorModel model;
    private ViewSetting oViewSetting;
    private String selectedSymbol;
    private TWTypes.TradeSides side;
    private JPanel calcStatus;
    private MDepthCalculator configPanel;
    private boolean loadedFromWSP;
    private boolean isLinked = false;

    public DepthCalculatorWindow(WindowDaemon daemon, ViewSettingsManager viewSettings, String sSelectedSymbol,
                                 TWTypes.TradeSides side, boolean fromWSP, boolean isLinked, String linkgroup) {
        super(daemon);
        this.viewManager = viewSettings;
        this.symbol = selectedSymbol;
        this.side = side;
        this.selectedSymbol = sSelectedSymbol;
        loadedFromWSP = fromWSP;
        this.isLinked = isLinked;
        setLinkedGroupID(linkgroup);

        if (oViewSetting == null && loadedFromWSP && isLinked) {
            ViewSetting oSetting = viewManager.getDepthCalView(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedSymbol)) {
                oViewSetting = oSetting;
            }
        }
        if (oViewSetting == null) {
            oViewSetting = viewManager.getDepthCalView(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW
                    + "|" + selectedSymbol);
        }
        if (oViewSetting == null) {
            oViewSetting = viewManager.getSymbolView("DEPTH_CALCULATOR");
            oViewSetting = oViewSetting.getObject(); // clone the object
            //oViewSetting.setID(selectedSymbol);
            oViewSetting.setID(System.currentTimeMillis() + "");
            oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedSymbol);
            oViewSetting.putProperty(ViewConstants.VC_LINKED, false);

            viewManager.putDepthCalView(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW
                    + "|" + selectedSymbol, oViewSetting);
        }

        createTable();

        oViewSetting.setParent(this);
        setShowServicesMenu(true);
        setColumnResizeable(false);
        setDetachable(true);
        setPrintable(true);
        setSize(oViewSetting.getSize());
        setLocation(oViewSetting.getLocation());
        getContentPane().setLayout(new BorderLayout());
        initSupportingObjcts(1);
        configPanel = new MDepthCalculator(selectedSymbol, model, side);
        addSupportingObject(configPanel, 0);
        table.setNorthPanel(configPanel);
        table.repaint();
        table.updateUI();
        this.hideTitleBarMenu();
        this.setTable(table);
        getContentPane().add(table, BorderLayout.CENTER);
        setColumnResizeable(false);
        setDetachable(true);
        setPrintable(true);
        setResizable(true);
        setClosable(true);
        setMaximizable(false);
        setIconifiable(true);
        setLinkGroupsEnabled(true);
        model.setParent(this);
        try {
            model.setDecimalCount(SharedMethods.getDecimalPlaces(selectedSymbol));
        } catch (Exception e) {
        }
        configPanel.setViewSettings(oViewSetting);
        setTitle(Language.getString("TARGET_PRICE_CALC") + " : " +
                DataStore.getSharedInstance().getStockObject(selectedSymbol).getLongDescription());
        table.addTableUpdateListener(model);
        setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
        updateUI();
        if (fromWSP && oViewSetting.isLocationValid()) {
            setLocation(oViewSetting.getLocation());
        } else {
            setLocationRelativeTo(Client.getInstance().getDesktop());
        }
        oViewSetting.setLocation(getLocation());
        this.applySettings();
        Client.getInstance().oTopDesktop.add(this);
        this.show();
        this.setLayer(GUISettings.TOP_LAYER);
        this.setOrientation();
        if (fromWSP && isLinked) {
            Client.getInstance().getViewSettingsManager().setWindow(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" +
                    LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            Client.getInstance().getViewSettingsManager().setWindow(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" +
                    selectedSymbol, this);
        }
        String requestID = Meta.PRICE_DEPTH + "|" + selectedSymbol;
        Client.getInstance().addDepthRequest(requestID, selectedSymbol, Meta.PRICE_DEPTH);
        this.setDataRequestID(DepthStore.getInstance(), selectedSymbol, requestID);

        this.show();
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedSymbol))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedSymbol);
        }
    }


    private void createTable() {
        table = new Table();
        table.hideCustomizer();
        table.getPopup().hideCopywHeadMenu();
        table.setSortingDisabled();
        table.setAutoResize(true);
        table.setWindowType(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW);
        model = new DepthCalculatorModel(side, this);
        model.setSymbol(selectedSymbol);
        model.setViewSettings(oViewSetting);
        table.hideHeader();
        table.setCalcModel(model);
        model.setTable(table);
        table.getTable().getTableHeader().setReorderingAllowed(false);
        model.applyColumnSettings();
        table.getTable().getTableHeader().setFont(table.getTable().getFont());
        createPanel();
        table.setSouthPanel(calcStatus);
        table.updateGUI();
    }

    private void createPanel() {
        calcStatus = new JPanel();
        BoxLayout calcStatusLayout = new BoxLayout(calcStatus, BoxLayout.LINE_AXIS);
        calcStatus.setLayout(calcStatusLayout);
        calcStatus.add(new JLabel(Language.getString("MSG_DEPTH_CALC_FOOTER")));
        calcStatus.add(Box.createHorizontalGlue());
        JLabel lblApprox = new JLabel(Language.getString("MSG_DEPTH_CALC_APPROXYMATED"));
        lblApprox.setFont(lblApprox.getFont().deriveFont(Font.BOLD));
        //  calcStatus.add(lblApprox);
    }

    public String[][] getPanelString() {
        String[][] strArray = new String[4][8];
        String[] str = configPanel.getPrintingString().split(",");
        try {
            strArray[0][0] = "10";
            strArray[0][1] = str[0];
            strArray[0][2] = "4";
            strArray[0][3] = str[1];
            strArray[0][4] = "10";
            strArray[0][5] = "";
            strArray[0][6] = "4";
            strArray[0][7] = "";
            strArray[1][0] = "10";
            strArray[1][1] = str[2];
            strArray[1][2] = "4";
            strArray[1][3] = str[3];
            strArray[1][4] = "10";
            strArray[1][5] = "";
            strArray[1][6] = "4";
            strArray[1][7] = "";
            strArray[2][0] = "10";
            strArray[2][1] = str[4];
            strArray[2][2] = "4";
            strArray[2][3] = str[5];
            strArray[2][4] = "10";
            strArray[2][5] = "";
            strArray[2][6] = "4";
            strArray[2][7] = "";
            if ((str[7] == null) || (str[7].trim().equals(""))) {
                strArray[3][0] = "10";
                strArray[3][1] = "";
                strArray[3][2] = "4";
                strArray[3][3] = "";
            } else {
                strArray[3][0] = "10";
                strArray[3][1] = str[6];
                strArray[3][2] = "4";
                strArray[3][3] = str[7];
            }
            strArray[3][4] = "10";
            strArray[3][5] = "";
            strArray[3][6] = "4";
            strArray[3][7] = "";
            str = null;
            return strArray;
        } catch (Exception e) {
            str = null;
            return null;
        }
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        super.internalFrameClosing(e);
        configPanel.killThread();
    }

    public void symbolChanged(String sKey) {
        selectedSymbol = sKey;
        String oldKey = oViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        String oldreqid = getDataRequestID();
        RequestManager.getSharedInstance().removeRequest(oldreqid);
        setTitle(Language.getString("TARGET_PRICE_CALC") + " : " +
                DataStore.getSharedInstance().getStockObject(selectedSymbol).getLongDescription());


        table.getTable().updateUI();
        model.setActive(false);
        model.setSymbol(selectedSymbol);
        try {
            model.setDecimalCount(SharedMethods.getDecimalPlaces(selectedSymbol));
        } catch (Exception e) {
        }
        model.setActive(true);
        configPanel.changeSymbol(sKey);

        String requestID = Meta.PRICE_DEPTH + "|" + selectedSymbol;
        Client.getInstance().addDepthRequest(requestID, selectedSymbol, Meta.PRICE_DEPTH);
        this.setDataRequestID(DepthStore.getInstance(), selectedSymbol, requestID);
        oViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sKey);

    }
}
