package com.isi.csvr.marketdepth;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 23, 2008
 * Time: 1:17:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class OddLotModel extends CommonTable implements TableModel, CommonTableInterface {


    private String g_sSymbol;

    public OddLotModel(String g_sSymbol) {
        this.g_sSymbol = g_sSymbol;
    }

    public int getRowCount() {
        DynamicArray rows = OddLotStore.getSharedInstance().getOddLotFor(g_sSymbol);  //To change body of implemented methods use File | Settings | File Templates.
        return rows.size();

    }


    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        try {

            DynamicArray oddLotArray = OddLotStore.getSharedInstance().getOddLotFor(g_sSymbol);
            OddLot obj = (OddLot) oddLotArray.get(rowIndex);
            switch (columnIndex) {
                case -1:
                    return obj.getPriceType() + "";
                case 0:
                    return obj.getDescription();
                case 1:
                    return SharedMethods.getSymbolFromKey(g_sSymbol);
                case 2:
                    return obj.getQuantity() + "";
                case 3:
                    double bidp = obj.getPrice(OddLot.BID);
                    String price = "";
                    if (bidp != 0d) {
                        price = bidp + "";
                    }

                    return price;
                case 4:

                    bidp = obj.getPrice(OddLot.ASK);
                    price = "";
                    if (bidp != 0d) {
                        price = bidp + "";
                    }
                    return price;
                default:
                    return "0";
            }

        } catch (Exception e) {
            return "0";
        }

    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }


    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 'B':
            case 1:
                return String.class;
            case 2:
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public void setSymbol(String symbol) {
        this.g_sSymbol = symbol;
    }
}
