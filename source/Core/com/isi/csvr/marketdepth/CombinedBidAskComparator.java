package com.isi.csvr.marketdepth;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 8, 2006
 * Time: 9:41:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class CombinedBidAskComparator implements Comparator {

    public int compare(Object o1, Object o2) {
        CombinedBidAsk c1 = (CombinedBidAsk) o1;
        CombinedBidAsk c2 = (CombinedBidAsk) o2;

        if (c1.getPrice() > c2.getPrice()) {
            return -1;
        } else if (c1.getPrice() < c2.getPrice()) {
            return 1;
        } else {
            return 0;
        }
    }
}
