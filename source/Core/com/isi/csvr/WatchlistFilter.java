package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.SymbolFilter;
import com.isi.csvr.shared.Stock;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 2, 2005
 * Time: 12:18:57 PM
 */
public class WatchlistFilter implements SymbolFilter {

    //    private String exchanges;
    private ArrayList<String> exchangeList;
    //    private String currencies;
    private ArrayList<String> currencyList;

//    public void setFilterCriteria(String exchange, String currency) {
//        this.exchanges = exchange;
//        this.currencies = currency;
//    }

    public void setFilterCriteria(ArrayList<String> exchange, ArrayList<String> currency) {
        this.exchangeList = exchange;
        this.currencyList = currency;
    }

    public String[] getFilteredList(String[] symbols) {
        if ((exchangeList == null) && (currencyList == null)) {
//        if ((exchanges == null) && (currencies == null)) {
            String[] filtered = new String[symbols.length];
            System.arraycopy(symbols, 0, filtered, 0, symbols.length);
            return filtered;
        } else {
            ArrayList<String> tempFilter = new ArrayList<String>();
            for (int i = 0; i < symbols.length; i++) {
                Stock stock = DataStore.getSharedInstance().getStockObject(symbols[i]);
                if (((exchangeList == null) || (exchangeList.size() == 0) || exchangeList.contains(stock.getExchange())) &&
                        ((currencyList == null) || (currencyList.size() == 0) || currencyList.contains(stock.getCurrencyCode())))

//				if (((exchangeList == null) || exchangeList.contains(stock.getExchange())) &&
//						((currencyList == null) || currencyList.contains(stock.getCurrencyCode())))
//				if (((exchanges == null) || (exchanges.equals("")) || (exchanges.indexOf(stock.getExchange()+",") >= 0)) &&
//                        ((currencies == null) || (currencies.equals("")) || (currencies.indexOf(stock.getCurrencyCode()) >= 0)))
                {
                    tempFilter.add(symbols[i]);
                }
                stock = null;
            }
            String[] filtered = tempFilter.toArray(new String[0]);
            return filtered;
        }
    }
}
