package com.isi.csvr;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 4, 2005
 * Time: 3:30:07 PM
 */
public class TWMenu extends JMenu {

    private String iconFile;
    private int id;

    public TWMenu() {
        super();
        getPopupMenu().setLightWeightPopupEnabled(false);
        setOpaque(false);
        setUI(new TWMenuUI());
    }

    public TWMenu(String text, String icon) {
        super(text);
        getPopupMenu().setLightWeightPopupEnabled(false);
        setOpaque(false);
        iconFile = icon;
        setUI(new TWMenuUI());
    }

    public TWMenu(String text, String icon, int id) {
        super(text);
        getPopupMenu().setLightWeightPopupEnabled(false);
        setOpaque(false);
        iconFile = icon;
        this.id = id;
        setUI(new TWMenuUI());
    }

    public TWMenu(String text) {
        super(text);
        getPopupMenu().setLightWeightPopupEnabled(false);
        setOpaque(false);
        iconFile = null;
        setUI(new TWMenuUI());
    }

    public TWMenu(String text, int id) {
        super(text);
        getPopupMenu().setLightWeightPopupEnabled(false);
        setOpaque(false);
        iconFile = null;
        this.id = id;
        setUI(new TWMenuUI());
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public int getId() {
        return id;
    }

    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        dimension.setSize(dimension.getWidth() + Constants.OUTLOOK_MENU_COLUMN_WIDTH, dimension.getHeight());
        return dimension;
    }

    public void paint(Graphics g) {
        g.translate(Constants.OUTLOOK_MENU_COLUMN_WIDTH * Constants.OUTLOOK_MENU_SIDE, 0);
        super.paint(g);
        g.translate(-(Constants.OUTLOOK_MENU_COLUMN_WIDTH * Constants.OUTLOOK_MENU_SIDE), 0);
        g.setColor(Theme.MENU_SELECTION_COLOR);
    }

    public void updateUI() {
        setBorderPainted(false);
        if (iconFile != null)
            super.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/" + iconFile));
        super.updateUI();
        setUI(new TWMenuUI());
        setOpaque(true);
//        setBackground(Color.green);
    }

}
