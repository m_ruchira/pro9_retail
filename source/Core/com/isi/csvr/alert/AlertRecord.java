package com.isi.csvr.alert;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class AlertRecord {

    public static final int SOURCE_DB = 0;
    public static final int SOURCE_SERVER = 1;

    private String symbol;
    private String exchange;
    private int instrument;
    private String ID;
    private int type;
    private int field;
    private float value;
    private float current;
    private boolean active;
    private boolean triggered;
    private int operator;
    private int vlidity;
    private int expiration;
    private short dataType;
    private long date;
    private long triggeredDate;
    private long expirationDate;
    private int sendMode;
    private float triggeredValue = Constants.DEFAULT_DOUBLE_VALUE;
    private int source = -1;

    public String getSymbol() {
        if (symbol.equals("")) {
            return Language.getString("NA");
        } else {
            return symbol;
        }
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getField() {
        if (field < 0) {
            return 0;
        } else {
            return field;
        }
    }

    public void setField(int field) {
        this.field = field;
    }

    public int getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = Integer.parseInt(operator);
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getCurrent() {
        return current;
    }

    public void setCurrent(float current) {
        this.current = current;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean activr) {
        this.active = active;
    }

    public boolean isTriggered() {
        return triggered;
    }

    public void setTriggered(boolean triggered) {
        this.triggered = triggered;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getValidity() {
        return vlidity;
    }

    public void setVlidity(int vlidity) {
        this.vlidity = vlidity;
    }

    public int getExpiration() {
        return expiration;
    }

    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }

    public short getDataType() {
        return dataType;
    }

    public void setDataType(short dataType) {
        this.dataType = dataType;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getTriggeredDate() {
        return triggeredDate;
    }

    public void setTriggeredDate(long triggeredDate) {
        this.triggeredDate = triggeredDate;
    }

    public long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getSendMode() {
        return sendMode;
    }

    public void setSendMode(int sendMode) {
        this.sendMode = sendMode;
    }

    public float getTriggeredValue() {
        return triggeredValue;
    }

    public void setTriggeredValue(float triggeredValue) {

        this.triggeredValue = triggeredValue;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public int getInstrumentType() {
        return instrument;
    }

    public void setInstrumentType(int instrument) {
        this.instrument = instrument;
    }

    public int getDecimalCount() {
        return (DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument).getDecimalCount());
    }
}