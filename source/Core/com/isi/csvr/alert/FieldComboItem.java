package com.isi.csvr.alert;

import com.isi.csvr.shared.Language;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class FieldComboItem {
    private String description;
    private int id;

    public FieldComboItem(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        if (description == null) {
            return Language.getString("NA");
        } else {
            return description;
        }

    }
}