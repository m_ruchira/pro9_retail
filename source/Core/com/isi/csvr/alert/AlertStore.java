package com.isi.csvr.alert;

import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class AlertStore {

    private static ArrayList<AlertRecord> store;
    private static AlertStore self = null;

    private AlertStore() {
        store = new ArrayList<AlertRecord>();
    }

    public synchronized static AlertStore getSharedInstance() {
        if (self == null) {
            self = new AlertStore();
        }
        return self;
    }

    public void setRecord(AlertRecord record) {
        if (getRecord(record.getID()) == null) {
            store.add(record);
        } else {
            updateRecord(record);
        }
    }

    public AlertRecord getRecord(String ID) {
        for (int i = 0; i < store.size(); i++) {
            if ((store.get(i)).getID().equals(ID)) {
                return store.get(i);
            }
        }
        return null;
    }

    public boolean isAlreadyTriggered(String ID) {
        for (int i = 0; i < store.size(); i++) {
            if ((store.get(i)).getID().equals(ID)) {
                if (store.get(i).isTriggered()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void updateRecord(AlertRecord record) {
        for (int i = 0; i < store.size(); i++) {
            if ((store.get(i)).getID().equals(record.getID())) {
                store.set(i, record);
            }
        }
    }

    public void deleteRecord(String id) {
        for (int i = 0; i < store.size(); i++) {
            if ((store.get(i)).getID().equals(id)) {
                store.remove(i);
            }
        }
    }

    public AlertRecord getRecord(int i) {
        return store.get(i);
    }

    public void clear() {
        store.clear();
        store.trimToSize();
    }

    public int size() {
        return store.size();
    }

}