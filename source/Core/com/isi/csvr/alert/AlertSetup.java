package com.isi.csvr.alert;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class AlertSetup extends InternalFrame
        implements ItemListener, ActionListener {
    private JPanel symbolPanel = new JPanel();
    private JPanel operandPanel = new JPanel();
    private JPanel criteriaPanel = new JPanel();
    private JPanel mediaPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();

    private JLabel lblSymbol;
    private int instrument = -1;
    //private JLabel lblCurrent;
    private TWComboBox cmbFields;
    private TWComboBox cmbOperations;
    private TWComboBox cmbValidity;
    private TWComboBox cmbExpiration;

    private TWTextField txtValue;
    private TWTextField soundFilePath;
    private TWButton browse;
    private AlertRecord alertRecord = null;

    private JCheckBox chkPopup;
    private JCheckBox chkSMS;
    private JCheckBox chkEMail;
    private String title;
    private String sKey = "";
    private UIMode mode;

    ;
    public AlertSetup(Frame frame, String title, boolean modal) {
        //super(title, modal);
        this.title = title;
        try {
            jbInit();
            setInitialValues();
            mode = UIMode.New;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public AlertSetup() {
        this(Client.getInstance().getFrame(), Language.getString("SET_ALERT"), true);
    }


    public AlertSetup(AlertRecord record) {
        //super(Client.getInstance().getFrame(), Language.getString("SET_ALERT"), true);
        title = Language.getString("SET_ALERT");
        try {
            jbInit();
            setInitialValues();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        alertRecord = record;
        lblSymbol.setText(SharedMethods.getDisplayKey(record.getExchange(), record.getSymbol()));
        instrument = record.getInstrumentType();
        txtValue.setText("" + record.getValue());

        FieldComboItem item = null;

        for (int i = 0; i < cmbOperations.getItemCount(); i++) {
            item = (FieldComboItem) cmbOperations.getItemAt(i);
            if (item.getId() == record.getOperator()) {
                cmbOperations.setSelectedIndex(i);
            }
        }

        for (int i = 0; i < cmbFields.getItemCount(); i++) {
            item = (FieldComboItem) cmbFields.getItemAt(i);
            if (item.getId() == record.getField()) {
                cmbFields.setSelectedIndex(i);
            }
        }

        for (int i = 0; i < cmbValidity.getItemCount(); i++) {
            item = (FieldComboItem) cmbValidity.getItemAt(i);
            if (item.getId() == record.getValidity()) {
                cmbValidity.setSelectedIndex(i);
            }
        }

        for (int i = 0; i < cmbExpiration.getItemCount(); i++) {
            item = (FieldComboItem) cmbExpiration.getItemAt(i);
            if (item.getId() == record.getExpiration()) {
                cmbExpiration.setSelectedIndex(i);
            }
        }

        mode = UIMode.Edit;
        setAlertMode(record.getSendMode());
        setVisible(true);
    }

    public static void main(String[] args) {
        AlertSetup a = new AlertSetup();
        Client.getInstance().getDesktop().add(a);
        a.show();
    }

    private void jbInit() throws Exception {
        setTitle(title);
        setResizable(false);
        this.setClosable(true);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);

        //this.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        BoxLayout parentLayout = new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS);
        this.getContentPane().setLayout(parentLayout);

        // setup symbol panel
        String[] cols1 = {"40%", "60%"};
        String[] rows1 = {"25"};

        FlexGridLayout symbolLayout = new FlexGridLayout(cols1, rows1, 5, 5);
        symbolPanel.setPreferredSize(new Dimension(250, 30));
        symbolPanel.setLayout(symbolLayout);

        JLabel lblSymbolLabel = new JLabel(Language.getString("SYMBOL"));
        lblSymbol = new JLabel("");
        lblSymbol.setHorizontalAlignment(SwingConstants.TRAILING);
        lblSymbol.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        symbolPanel.add(lblSymbolLabel);
        symbolPanel.add(lblSymbol);

        this.getContentPane().add(symbolPanel);

        /* setup the symbol alert panel */
        JPanel symbolAlertPanel = new JPanel();
        BoxLayout symbolAlertPanelLayout = new BoxLayout(symbolAlertPanel, BoxLayout.Y_AXIS);
        symbolAlertPanel.setLayout(symbolAlertPanelLayout);

        // setup criteria panel
        String[] cols2 = {"40%", "60%"};
        String[] rows2 = {"25", "25", "25", "25", "25"};

        FlexGridLayout criteriaLayout = new FlexGridLayout(cols2, rows2, 5, 5);
        criteriaPanel.setPreferredSize(new Dimension(250, 170));//(250,200));
        criteriaPanel.setLayout(criteriaLayout);

        JLabel lblField = new JLabel(Language.getString("FIELD"));
        cmbFields = new TWComboBox();
        cmbFields.addItemListener(this);
        //JLabel  lblCurrentLabel = new JLabel("Current Value");
        //lblCurrent              = new JLabel("");
        //lblCurrent.setHorizontalAlignment(SwingConstants.TRAILING);
        //lblCurrent.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        JLabel lblOperands = new JLabel(Language.getString("OPERATION"));

        cmbOperations = new TWComboBox();
//        ButtonGroup group   = new ButtonGroup();
//        JRadioButton rdoGt  = new JRadioButton(">");
//        group.add(rdoGt);
//        pnlOperands.add(rdoGt);
//        JRadioButton rdoEq  = new JRadioButton("=");
//        rdoEq.setSelected(true);
//        group.add(rdoEq);
//        pnlOperands.add(rdoEq);
//        JRadioButton rdoLt  = new JRadioButton("<");
//        group.add(rdoLt);
//        pnlOperands.add(rdoLt);
        JLabel lblValueLabel = new JLabel(Language.getString("TRADING_VALUE"));
        txtValue = new TWTextField();
        txtValue.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        JLabel lblValidity = new JLabel(Language.getString("NOTIFICATION"));
        cmbValidity = new TWComboBox();
        JLabel lblExpiration = new JLabel(Language.getString("EXPIRATION"));
        cmbExpiration = new TWComboBox();

        criteriaPanel.add(lblField);
        criteriaPanel.add(cmbFields);
        //criteriaPanel.add(lblCurrentLabel);
        //criteriaPanel.add(lblCurrent);
        criteriaPanel.add(lblOperands);
        criteriaPanel.add(cmbOperations);
        criteriaPanel.add(lblValueLabel);
        criteriaPanel.add(txtValue);
        criteriaPanel.add(lblValidity);
        criteriaPanel.add(cmbValidity);
        criteriaPanel.add(lblExpiration);
        criteriaPanel.add(cmbExpiration);


        symbolAlertPanel.add(criteriaPanel);

        // setup alert mode label panel
        JPanel alertModeLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 5));
        JLabel lblAlertMode = new JLabel(Language.getString("ALERT_MODE"));
        lblAlertMode.setHorizontalAlignment(SwingConstants.LEADING);
        lblAlertMode.setMaximumSize(new Dimension(250, 25));
        alertModeLabelPanel.add(lblAlertMode);
        symbolAlertPanel.add(alertModeLabelPanel);

        // setup alert mode label panel
        String[] cols3 = {"33%", "33%", "33%"};
        String[] rows3 = {"25"};

        FlexGridLayout modeLayout = new FlexGridLayout(cols3, rows3, 5, 5);
        mediaPanel.setPreferredSize(new Dimension(250, 65));
        mediaPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 2));

        chkPopup = new JCheckBox(Language.getString("POPUP"));
        chkPopup.setSelected(true);
        chkPopup.addActionListener(this);
        chkPopup.setActionCommand("MODE");
        chkSMS = new JCheckBox(Language.getString("SMS"));
        chkSMS.addActionListener(this);
        chkSMS.setActionCommand("MODE");
        chkEMail = new JCheckBox(Language.getString("EMAIL"));
        chkEMail.addActionListener(this);
        chkEMail.setActionCommand("MODE");

        mediaPanel.add(chkPopup);
        mediaPanel.add(chkSMS);
        mediaPanel.add(chkEMail);

        JLabel lblSound = new JLabel(Language.getString("PLAY_ALEART"));
        soundFilePath = new TWTextField(6);
        browse = new TWButton(Language.getString("BROWSE"));
        browse.setActionCommand("BROWSE");
        browse.addActionListener(this);

        //     mediaPanel.add(lblSound);
        //   mediaPanel.add(soundFilePath);
        //   mediaPanel.add(browse);

        symbolAlertPanel.add(mediaPanel);

        setAlertModeValidity();

        // setup button panel
        String[] cols4 = {"100%", "80", "80"};
        String[] rows4 = {"25"};

        FlexGridLayout buttonLayout = new FlexGridLayout(cols4, rows4, 5, 5);
        buttonPanel.setPreferredSize(new Dimension(250, 35));
        buttonPanel.setLayout(buttonLayout);

        TWButton btnOK = new TWButton(Language.getString("OK"));
        btnOK.setActionCommand("OK");
        btnOK.addActionListener(this);
        TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.addActionListener(this);

        buttonPanel.add(new JLabel()); // dummy
        buttonPanel.add(btnOK);
        buttonPanel.add(btnCancel);

        symbolAlertPanel.add(buttonPanel);


        JTabbedPane tabPane = new JTabbedPane();
        tabPane.add(Language.getString("SYMBOL_ALERTS"), symbolAlertPanel);
        this.getContentPane().add(tabPane);
        hideTitleBarMenu();
        pack();

        /* Center the window on the screen */
        //Toolkit oToolkit = Toolkit.getDefaultToolkit();
        //Dimension oDm = oToolkit.getScreenSize();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        this.setBounds((int)(oDm.getWidth()/2 - (this.getWidth()/2)),
//            (int)(oDm.getHeight()/2 - (this.getHeight()/2)),
//            this.getWidth(),this.getHeight());
        GUISettings.applyOrientation(this);
        //this.setLocationRelativeTo(Client.getInstance().getDesktop());
        GUISettings.setLocationRelativeTo(this, Client.getInstance().getDesktop());
    }

    private void setInitialValues() {
//        cmbFields.addItem(new FieldComboItem("Last Price",Meta.ALERT_LASTTRADEPRICE));
//        cmbFields.addItem(new FieldComboItem("Last Volume",Meta.ALERT_LASTTRADEVOL));
//        cmbFields.addItem(new FieldComboItem("Bid Price",Meta.ALERT_BID));
//        cmbFields.addItem(new FieldComboItem("Bid Quantity",Meta.ALERT_BIDQTY));
//        cmbFields.addItem(new FieldComboItem("Bid Price",Meta.ALERT_ASK));
//        cmbFields.addItem(new FieldComboItem("Bid Quantity",Meta.ALERT_OFFERQTY));
//        cmbFields.addItem(new FieldComboItem("Volume",Meta.ALERT_CUMVOLUME));
//        cmbFields.addItem(new FieldComboItem("Change",Meta.ALERT_CHANGE));
//        cmbFields.addItem(new FieldComboItem("% Change",Meta.ALERT_PERCENTCHANGE));

//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_LASTTRADEPRICE) ,Meta.ALERT_LASTTRADEPRICE));
//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_LASTTRADEVOL),Meta.ALERT_LASTTRADEVOL));
//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_BID),Meta.ALERT_BID));
//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_BIDQTY),Meta.ALERT_BIDQTY));
//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_ASK),Meta.ALERT_ASK));
//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_OFFERQTY),Meta.ALERT_OFFERQTY));
//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_CUMVOLUME),Meta.ALERT_CUMVOLUME));
//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_CHANGE),Meta.ALERT_CHANGE));
//        cmbFields.addItem(new FieldComboItem(criteriaFormats.format(Meta.ALERT_PERCENTCHANGE),Meta.ALERT_PERCENTCHANGE));

        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_LASTTRADEPRICE));
        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_LASTTRADEVOL));
        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_BID));
        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_BIDQTY));
        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_ASK));
        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_OFFERQTY));
        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_CUMVOLUME));
        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_CHANGE));
        cmbFields.addItem(AlertUtils.getFieldItem(Meta.ALERT_PERCENTCHANGE));

        cmbOperations.addItem(AlertUtils.getConditionItem(Meta.ALERT_GRATERTHAN));
        cmbOperations.addItem(AlertUtils.getConditionItem(Meta.ALERT_GREATERTHANOREQUAL));
        cmbOperations.addItem(AlertUtils.getConditionItem(Meta.ALERT_LESSTHAN));
        cmbOperations.addItem(AlertUtils.getConditionItem(Meta.ALERT_LESSTHANEQUAL));
        cmbOperations.addItem(AlertUtils.getConditionItem(Meta.ALERT_EQUAL));

        cmbValidity.addItem(AlertUtils.getValidityItem(Meta.ALERT_EXPIRED_ONE_TIME));
        cmbValidity.addItem(AlertUtils.getValidityItem(Meta.ALERT_EXPIRED_UNTIL_CANCEL));

        cmbExpiration.addItem(AlertUtils.getExpirationItem(1));
        cmbExpiration.addItem(AlertUtils.getExpirationItem(2));
        cmbExpiration.addItem(AlertUtils.getExpirationItem(3));
    }

    public void showDialog(String key) {
        lblSymbol.setText(SharedMethods.getDisplayKey(key));
        instrument = SharedMethods.getInstrumentTypeFromKey(key);
        setVisible(true);
    }

    /**
     * Invoked when an item has been selected or deselected by the user.
     * The code written for this method performs the operations
     * that need to occur when an item is selected (or deselected).
     */
    public void itemStateChanged(ItemEvent e) {
        FieldComboItem item = (FieldComboItem) e.getItem();
        if (item == null) return;

//        try {
//            switch (item.getId()) {
//                case 1:
//                    Stock stock = QuoteStore.getStockObject(lblSymbol.getText());
//                    lblCurrent.setText("" + stock.getLastTrade()[0]);
//                    break;
//                default:
//                    lblCurrent.setText("N/A");
//            }
//        }
//        catch (Exception ex) {
//        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            sendMessage();
            if (mode == UIMode.New) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Alert, "Set");
            } else {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Alert, "Amend");
            }
        } else if (e.getActionCommand().equals("CANCEL")) {
            cencelMessage();
            dispose();
        } else if (e.getActionCommand().equals("MODE")) {
            validateCheckBoxes(e.getSource());
        } else if (e.getActionCommand().equals("BROWSE")) {
            File file = getSelecedFile();
        }

    }

    private void validateCheckBoxes(Object source) {
        if (!(chkEMail.isSelected() || chkPopup.isSelected() || chkSMS.isSelected())) { // if none is selected
            ((JCheckBox) source).setSelected(true); // tick the last clicked checkbox
        }
    }

    public void sendMessage() {

        if (isValidValue()) {
            String symbol = SharedMethods.getSymbolFromKey(lblSymbol.getText());
            String exchange = SharedMethods.getExchangeFromKey(lblSymbol.getText());
            AlertFrame msg = new AlertFrame();
            msg.setType("" + Meta.ALERT);
            msg.setSymbol(symbol);
            msg.setExchange(exchange);
            msg.setInstrumentType(instrument);

            msg.setField(((FieldComboItem) cmbFields.getSelectedItem()).getId());
            msg.setCriteria(((FieldComboItem) cmbOperations.getSelectedItem()).getId());
            msg.setValue(Float.parseFloat(txtValue.getText()));
            msg.setSend_Method(getAlertMode());
            msg.setExpiration(((FieldComboItem) cmbExpiration.getSelectedItem()).getId()); // DAYS
            msg.setValidity(((FieldComboItem) cmbValidity.getSelectedItem()).getId());
            if (alertRecord == null) { // setting up for the first time
                msg.setClientAlertID(System.currentTimeMillis() + "-" + Settings.getUserID()); // time+user
                msg.setLogingID(Settings.getUserID());
                msg.setDataType((short) 0); // quote type
            } else { // unpading an existing alert
                msg.setClientAlertID(alertRecord.getID());
                msg.setLogingID(Settings.getUserID());
                msg.setDataType(alertRecord.getDataType());
                msg.setType(Meta.ALERT_UPDATE);
            }


            SendQFactory.addData(Constants.PATH_PRIMARY, msg.getMsgString());
            dispose();
        } else {
            new ShowMessage(Language.getString("MSG_INVALID_VALUE"), "E");
        }

    }

    private int getAlertMode() {
        int result = 0;

        if (chkEMail.isSelected()) {
            result += Meta.ALERT_MODE_EMAIL;
        }
        if (chkPopup.isSelected()) {
            result += Meta.ALERT_MODE_TW;
        }
        if (chkSMS.isSelected()) {
            result += Meta.ALERT_MODE_SMS;
        }

        return result;
    }

    private void setAlertMode(int mode) {
        if ((mode & Meta.ALERT_MODE_EMAIL) > 0) {
            chkEMail.setSelected(true);
        } else {
            chkEMail.setSelected(false);
        }
        if ((mode & Meta.ALERT_MODE_TW) > 0) {
            chkPopup.setSelected(true);
        } else {
            chkPopup.setSelected(false);
        }
        if ((mode & Meta.ALERT_MODE_SMS) > 0) {
            chkSMS.setSelected(true);
        } else {
            chkSMS.setSelected(false);
        }
    }

    private void setAlertModeValidity() {
        /*if((mode & Meta.ALERT_MODE_EMAIL)>0){
            chkEMail.setEnabled(true);
        }else{
            chkEMail.setEnabled(false);
        }

        chkPopup.setEnabled(true);
        
        if((mode & Meta.ALERT_MODE_SMS)>0){
            chkSMS.setEnabled(true);
        }else{
            chkSMS.setEnabled(false);
        }*/

        chkEMail.setEnabled(Client.getInstance().isValidSystemWindow(Meta.IT_Alert_EMail, true));
        chkSMS.setEnabled(Client.getInstance().isValidSystemWindow(Meta.IT_Alerts_SMS, true));
    }

    public void deleteAlert() {

    }

    private boolean isValidValue() {

        try {
            int field = ((FieldComboItem) cmbFields.getSelectedItem()).getId();
            double value = Double.parseDouble(txtValue.getText());

            if (((field == Meta.ALERT_CHANGE) || (field == Meta.ALERT_PERCENTCHANGE)) || (value > 0)) {
                return true;
            } else {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
    }
    //MSG=61|Type=REQPATH|ID=190|DTyp=0|SYM=a|Fld=a|LN=a|C==<|V=5|SM=TW
    //|DIR=UP|SRCE=|DEST=|ISEMM=0|TTL=0|VLTY=1

    public void cencelMessage() {
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private File getSelecedFile() {

        SmartFileChooser chooser = new SmartFileChooser(Settings.getAbsolutepath());
        GUISettings.localizeFileChooserHomeButton(chooser);
        chooser.addChoosableFileFilter(new MyFilterTree());
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setDialogTitle(Language.getString("IMPORT_USER_DATA"));
        int option = chooser.showOpenDialog(Client.getInstance().getFrame());
        if (option == JFileChooser.CANCEL_OPTION) {
            return null;
        } else if (option == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile();
        } else {
            return null;
        }

    }

    private enum UIMode {New, Edit}

    class MyFilterTree extends javax.swing.filechooser.FileFilter {
        public boolean accept(File file) {
            String filename = file.getName();
            String ext = getFileExtention();
            if (filename.endsWith(ext)) {
                return true;
            } else if (file.isDirectory()) {
                return true;
            } else {
                return false;
            }
        }

        public String getDescription() {
            return "*" + getFileExtention();
        }

        private String getFileExtention() {

            return ".wav";
        }
    }


}



