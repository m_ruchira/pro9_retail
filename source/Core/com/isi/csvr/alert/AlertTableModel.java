package com.isi.csvr.alert;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Uditha Nagahawatta
 * @version 1.0
 */

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class AlertTableModel extends CommonTable implements TableModel, CommonTableInterface {
    /**
     * Constructor
     */
    public AlertTableModel() {
    }

    /* --- Table Model's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return AlertStore.getSharedInstance().size();
    }

    public Object getValueAt(int iRow, int iCol) {

        AlertRecord record = getRecordAtRow(iRow);

        switch (iCol) {
            case -1:
                return record.getInstrumentType();
            case 0:
                return record.getSymbol();
            case 1:
//                return record.getExchange();
                return ExchangeStore.getSharedInstance().getExchange(record.getExchange().trim()).getDisplayExchange(); //Display Exchnage
            case 2:
                return AlertUtils.getFieldItem(record.getField()).getDescription();
            case 3:
                return AlertUtils.getConditionItem(record.getOperator()).getDescription();
            case 4:
                return "" + record.getValue();
            case 5:
                return "" + Settings.getLocalTimeFor(record.getDate());
            case 6:
                return "" + Settings.getLocalTimeFor(record.getExpirationDate());
            case 7:
                return AlertUtils.getAlertMode(record.getSendMode());
            case 8:
                return AlertUtils.getStatusString(record.isTriggered());
            case 9:
                return "" + Settings.getLocalTimeFor(record.getTriggeredDate());
            case 10:
                return "" + record.getTriggeredValue();
            case 11:
                try {
                    return DataStore.getSharedInstance().getStockObject(record.getExchange(), record.getSymbol(), record.getInstrumentType()).getShortDescription();
                } catch (Exception e) {
                    return record.getSymbol();
                }
            default:
                return "0";
        }
    }

    public AlertRecord getRecordAtRow(int row) {
        try {
            return AlertStore.getSharedInstance().getRecord(row);
        } catch (Exception e) {
            return null;
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        try {
            return getValueAt(0, iCol).getClass();
        } catch (Exception e) {
            return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }

    public void setSymbol(String symbol) {

    }
}