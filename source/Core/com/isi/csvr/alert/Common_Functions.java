package com.isi.csvr.alert;

import java.sql.Date;
//import java.util.Date;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class Common_Functions {
    public static final char STX = (char) 25;
    public static final char ETX = (char) 26;
    public static final char DELM = '|';
    //public static final String ADD_ALERT_DEF_MSG = STX + "Type=A|ID=100|SYM=NSDQ~MSFT|Fld=BID|LN=bimal1|C==<|V=500|TO=EMM" + ETX;

    public static final String STATUS_SEND2AM = "SEND2AM";
    public static final String STATUS_ENABLED = "ENABLED";
    public static final String STATUS_TRIGGERED = "TRIGGERED";
    public static final String STATUS_DELIVERED = "DELIVERED";

    //SEND2AM,ENABLED,TRIGGERED,DELIVERED
    public Common_Functions() {
    }

    public static String getTagValue(String sFrame, char Delm, String sTag) {
        int iStart = 0, iEnd = 0;
        String sResult = "";
        iStart = sFrame.indexOf(sTag);
        if (iStart != -1) {
            iEnd = sFrame.indexOf(Delm, iStart);
            if (iEnd == -1) {
                iEnd = sFrame.length();
            }
            sResult = sFrame.substring(iStart, iEnd).replaceAll(sTag, "");
        }
        return sResult;
    }

    public static int ConvertToInt(String sExpression) {
        try {
            return Integer.parseInt(sExpression);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public static short ConvertToShort(String sExpression) {
        try {
            return Short.parseShort(sExpression);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }


    public static long ConvertToLong(String sExpression) {
        try {
            return Long.parseLong(sExpression);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }


    public static double ConvertToDouble(String sExpression) {
        try {
            return Double.parseDouble(sExpression);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public static float ConvertToFloat(String sExpression) {
        try {
            return Float.parseFloat(sExpression);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public static Date ConvertToDate(String sExpression) {
        Date dt = new Date(ConvertToLong(sExpression));
        try {
            return dt;
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getUniqueValue() {
        java.util.Date dt = new java.util.Date();
        return "" + dt.getTime() + "";
    }

    public static void PrintMsg(String sText) {
        System.out.println(sText);
    }

    public static String Get_Adjusted_Source_Path(String Source, String SCM_ID) {
        //SRCE=CM/SCM1/ACS1/SCM3/TW1
        int iStartPos, iLength;
        iStartPos = Source.indexOf(SCM_ID);
        if (iStartPos != -1) {
            iLength = Source.length();
            return Source.substring(iStartPos, iLength);
        } else {
            return "";
        }

    }

    public static void sleep(int t) {
        try {
            Thread.sleep(t);
        } catch (Exception e) {
        }
    }
}