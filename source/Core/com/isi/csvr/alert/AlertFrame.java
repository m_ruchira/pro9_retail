package com.isi.csvr.alert;


import com.isi.csvr.shared.Meta;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: me
 * Date: Jan 21, 2005
 * Time: 3:32:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlertFrame implements Cloneable {
    protected static final int DEFAULT_EXPIRATION = 0;
    protected static final int DEFAULT_STATUS = 0;
    private String clientAlertID = null;
    private String type = null;
    private String serverAlertID = null;   // this is auto generaeted id by AlertManager
    private short dataType = -1;  // type like trade index
    private String symbol = null;
    private String exchange = null;
    private int instrument = 0;
    private String logingID;
    private String path = null;
    private int field = -1;
    private float Value = -1;
    private int criteria = -1;
    private int notifyMethod = -1;
    private long triggeredDate = -1;
    private int expirationDate = 0;
    private int iValidity = -1;
    private int alertStatus = -1;
    private int enabledStatus = -1;
    private long dInsertDate = -1;

//    private short   isFromAM = FROMAM;
    private float triggeredValue = -1;
    private int source = -1;
    private String alertFrame;

    public AlertFrame() {
    }

    public AlertFrame(String alertFrame) {
        this.alertFrame = alertFrame;
        createAlert();
    }


    public static String getFormattedDate(Date date) {
        return AlertManager.getSharedInstance().getFormattedDate(date);
    }

    public static long getDate(String mktDateTime) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        if (mktDateTime != null) {
            cal.set(Integer.parseInt(mktDateTime.substring(0, 4)),
                    Integer.parseInt(mktDateTime.substring(4, 6)) - 1,
                    Integer.parseInt(mktDateTime.substring(6, 8)),
                    Integer.parseInt(mktDateTime.substring(8, 10)),
                    Integer.parseInt(mktDateTime.substring(10, 12)),
                    Integer.parseInt(mktDateTime.substring(12, 14)));
        }
        return cal.getTime().getTime();
    }

    private void createAlert() {
        String[] tokenizer = null;
        try {
            System.out.println("Alert Raw data : " + alertFrame);
            //Alert Raw data : 1145436260343-125||R|0|4061|TDWL|125||3|10.0|2|1||1|1|||||1
            tokenizer = alertFrame.split("\\" + String.valueOf(Common_Functions.DELM));
            clientAlertID = tokenizer[0];
            serverAlertID = tokenizer[1];
            type = tokenizer[2];
            if (!tokenizer[3].equals(""))
                dataType = Common_Functions.ConvertToShort(tokenizer[3]);
            symbol = tokenizer[4];
            exchange = tokenizer[5];
            if (!tokenizer[6].equals(""))
                logingID = tokenizer[6];
            if (!tokenizer[7].equals(""))
                path = tokenizer[7];
            if (!tokenizer[8].equals(""))
                field = Integer.parseInt(tokenizer[8]);
            if (!tokenizer[9].equals(""))
                Value = Common_Functions.ConvertToFloat(tokenizer[9]);
            if (!tokenizer[10].equals(""))
                criteria = Integer.parseInt(tokenizer[10]);
            if (!tokenizer[11].equals(""))
                notifyMethod = Integer.parseInt(tokenizer[11]);
            if (!tokenizer[12].equals(""))
                triggeredDate = AlertManager.getSharedInstance().getDateFromString(tokenizer[12]).getTime();
            if (!tokenizer[13].equals(""))
                expirationDate = Integer.parseInt(tokenizer[13]);
            if (!tokenizer[14].equals(""))
                iValidity = Integer.parseInt(tokenizer[14]);
            if (!tokenizer[15].equals(""))
                alertStatus = Integer.parseInt(tokenizer[15]);
            if (!tokenizer[16].equals(""))
                enabledStatus = Integer.parseInt(tokenizer[16]);
            if (!tokenizer[17].equals(""))
                dInsertDate = AlertManager.getSharedInstance().getDateFromString(tokenizer[17]).getTime();
            if (!tokenizer[18].equals(""))
                triggeredValue = Common_Functions.ConvertToFloat(tokenizer[18]);
            if (!tokenizer[19].equals(""))
                source = Common_Functions.ConvertToInt(tokenizer[19]);
            //commented by chandika
            // Alert manager old version is not supporting for instrument types....
//            if (!tokenizer[20].equals(""))
//                instrument = Integer.parseInt(tokenizer[19]);
        } catch (Exception e) {
            System.out.println(tokenizer[0]);
            throw new RuntimeException(" In Alert Message " + e.toString());
        }

    }

    public String toString() {
        StringBuilder sMsgBuffer = new StringBuilder();
        sMsgBuffer.append(clientAlertID);
        sMsgBuffer.append(Common_Functions.DELM);
        if (serverAlertID != null)
            sMsgBuffer.append(serverAlertID);
        sMsgBuffer.append(Common_Functions.DELM);
        sMsgBuffer.append(type);
        sMsgBuffer.append(Common_Functions.DELM);
        if (dataType != -1)
            sMsgBuffer.append("" + dataType);
        sMsgBuffer.append(Common_Functions.DELM);
        if (symbol != null)
            sMsgBuffer.append(symbol);
        sMsgBuffer.append(Common_Functions.DELM);
        if (exchange != null)
            sMsgBuffer.append(exchange);
        sMsgBuffer.append(Common_Functions.DELM);
        if (logingID != null)
            sMsgBuffer.append("" + logingID);
        sMsgBuffer.append(Common_Functions.DELM);
        if (path != null)
            sMsgBuffer.append("" + path);
        sMsgBuffer.append(Common_Functions.DELM);
        if (field != -1)
            sMsgBuffer.append("" + field);
        sMsgBuffer.append(Common_Functions.DELM);
        if (Value != -1)
            sMsgBuffer.append("" + Value);
        sMsgBuffer.append(Common_Functions.DELM);
        if (criteria != -1)
            sMsgBuffer.append("" + criteria);
        sMsgBuffer.append(Common_Functions.DELM);
        if (notifyMethod != -1)
            sMsgBuffer.append("" + notifyMethod);
        sMsgBuffer.append(Common_Functions.DELM);
        if (triggeredDate > 0)
            sMsgBuffer.append(AlertManager.getSharedInstance().getFormattedDate(new Date(triggeredDate)));
        sMsgBuffer.append(Common_Functions.DELM);
        if (expirationDate != -1)
            sMsgBuffer.append("" + expirationDate);
        sMsgBuffer.append(Common_Functions.DELM);
        if (iValidity != -1)
            sMsgBuffer.append("" + iValidity);
        sMsgBuffer.append(Common_Functions.DELM);
        if (alertStatus != -1)
            sMsgBuffer.append("" + alertStatus);
        sMsgBuffer.append(Common_Functions.DELM);
        if (enabledStatus != -1)
            sMsgBuffer.append("" + enabledStatus);
        sMsgBuffer.append(Common_Functions.DELM);
        if (dInsertDate > 0)
            sMsgBuffer.append(AlertManager.getSharedInstance().getFormattedDate(new Date(dInsertDate)));
        sMsgBuffer.append(Common_Functions.DELM);
        if (triggeredValue != -1)
            sMsgBuffer.append("" + triggeredValue);
        sMsgBuffer.append(Common_Functions.DELM);
        if (instrument != -1)
            sMsgBuffer.append("" + instrument);
        return sMsgBuffer.toString();
    }

    public String getMsgString() {
        StringBuilder sMsgBuffer = new StringBuilder();
        sMsgBuffer.append(Meta.ALERT_MESSAGE);
        sMsgBuffer.append(Meta.DS);
        sMsgBuffer.append(this.toString());
        sMsgBuffer.append(Meta.EOL);
        return sMsgBuffer.toString();
    }

    public String getAlertFrame() {
        return alertFrame;
    }

    public void setAlertFrame(String alertFrame) {
        this.alertFrame = alertFrame;
    }

    public String getServerAlertID() {
        return serverAlertID;
    }

    public void setServerAlertID(String serverAlertID) {
        this.serverAlertID = serverAlertID;
    }

    public short getDataType() {
        return dataType;
    }

    public void setDataType(short dataType) {
        this.dataType = dataType;
    }

    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }

    public float getValue() {
        return Value;
    }

    public void setValue(float value) {
        Value = value;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getInstrumentType() {
        return instrument;
    }

    public void setInstrumentType(int instrument) {
        this.instrument = instrument;
    }

    public int getCriteria() {
        return criteria;
    }

    public void setCriteria(int criteria) {
        this.criteria = criteria;
    }

    public String getClientAlertID() {
        return clientAlertID;
    }

    public void setClientAlertID(String clientAlertID) {
        this.clientAlertID = clientAlertID;
    }

    public int getSend_Method() {
        return notifyMethod;
    }

    public void setSend_Method(int iSend_Method) {
        this.notifyMethod = iSend_Method;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getExpiration() {
        return expirationDate;
    }

    public void setExpiration(int Expiration_Date) {
        this.expirationDate = Expiration_Date;
    }

    public int getValidity() {
        return iValidity;
    }

    public void setValidity(int iValidity) {
        this.iValidity = iValidity;
    }

    public int getEnable() {
        return enabledStatus;
    }

    public void setEnable(int iEnable) {
        this.enabledStatus = iEnable;
    }

    public long getTriggeredDate() {
        return triggeredDate;
    }

    public void setTriggeredDate(long triggeredDate) {
        this.triggeredDate = triggeredDate;
    }

    public long getInsertDate() {
        return dInsertDate;
    }

    public void setdInsertDate(long dInsertDate) {
        this.dInsertDate = dInsertDate;
    }

    public float getTRValue() {
        return triggeredValue;
    }

    public void setTRValue(float fTRValue) {
        this.triggeredValue = fTRValue;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.getMessage());
        }
    }

    public String getExcahge() {
        return this.exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getLogingID() {
        return logingID;
    }

    public void setLogingID(String logingID) {
        this.logingID = logingID;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getNotifyMethod() {
        return notifyMethod;
    }

    public void setNotifyMethod(int notifyMethod) {
        this.notifyMethod = notifyMethod;
    }

    public int getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(int expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getiValidity() {
        return iValidity;
    }

    public void setiValidity(int iValidity) {
        this.iValidity = iValidity;
    }

    public int getAlertStatus() {
        return alertStatus;
    }

    public void setAlertStatus(int alertStatus) {
        this.alertStatus = alertStatus;
    }

    public int getEnabledStatus() {
        return enabledStatus;
    }

    public void setEnabledStatus(int enabledStatus) {
        this.enabledStatus = enabledStatus;
    }

    public float getTriggeredValue() {
        return triggeredValue;
    }

    public void setTriggeredValue(float triggeredValue) {
        this.triggeredValue = triggeredValue;
    }

    public int getSource() {
        return source;
    }


    public String toDebugString() {
        return "AlertFrame{" +
                "alertFrame='" + alertFrame + "'" +
                ", clientAlertID='" + clientAlertID + "'" +
                ", type='" + type + "'" +
                ", serverAlertID='" + serverAlertID + "'" +
                ", dataType=" + dataType +
                ", symbol='" + symbol + "'" +
                ", instrument Type='" + instrument + "'" +
                ", exchange='" + exchange + "'" +
                ", logingID='" + logingID + "'" +
                ", path='" + path + "'" +
                ", field=" + field +
                ", Value=" + Value +
                ", criteria=" + criteria +
                ", notifyMethod=" + notifyMethod +
                ", triggeredDate=" + triggeredDate +
                ", expirationDate=" + expirationDate +
                ", iValidity=" + iValidity +
                ", alertStatus=" + alertStatus +
                ", enabledStatus=" + enabledStatus +
                ", dInsertDate=" + dInsertDate +
                ", triggeredValue=" + triggeredValue +
                "}";
    }
}