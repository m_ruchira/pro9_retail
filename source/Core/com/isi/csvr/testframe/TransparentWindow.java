package com.isi.csvr.testframe;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 11, 2005
 * Time: 5:23:33 PM
 */

import com.isi.csvr.shared.TWFont;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/*
**TransparentWindow captures the background screen image and displays it.
**As the window drags across the screen, it captures screen areas to be covered
**by it (just before it moves into the new location) and adds them to the image
**already displayed. Thus, the image captures and window updates are incremental.
*/

public class TransparentWindow extends JWindow {
    private Rectangle baseRect;//Intersection rect between the frames in old and new location
    private Rectangle tile[];//Newly covered areas as the window drags from one location to next
    private BufferedImage currentImage;//Full image carried by the window at any given time
    private BufferedImage baseImage;//Image in the intersection rect between the old and new frames
    private Image tileImage[];//Image in the newly covered areas (tiles)
    private Robot robot;//Robot class instance to capture the background image
    private Font boldFont;
    private boolean drawBackgroundOnly;//Flag to exclude the red border from the window image


    public TransparentWindow() {
        super(new Frame());
        tileImage = new Image[2];// There will never be more than two tiles
        tileImage[0] = null;
        tileImage[1] = null;
        pack();
        setSize(100, 100);
        boldFont = new TWFont("SansSerif", Font.BOLD, 16);
        validate();
        try {
            robot = new Robot();
        } catch (AWTException e) {
            System.out.println(e);
        }
    }

    protected void grabCurrentImage() {
/*
**Grab the image curretly displayed by the window (same as the
**image in the background covered by the window)
*/
        currentImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = currentImage.getGraphics();
        drawBackgroundOnly = true;// We do not want to capture the red border that is an artifact
        paint(g);
        drawBackgroundOnly = false;
        g.dispose();
    }

    public void setVisible(boolean flag) {
/*
**Called once in the beginning of the dragging session
*/
        if (flag) {
/*
**Initially, the intersection rectangle is the same as the original bounds,
**and the tiles are empty
*/
            baseRect = getBounds();
/*
**Grab the background image at the initial location from the screen,
**before it gets covered by the window, and prime up "currentImage" var
*/
            baseImage = robot.createScreenCapture(baseRect);
            currentImage = baseImage;
        }
        super.setVisible(flag);
    }

    public void setLocation(Point loc) {
/*
**Called by Controller as the window drags across the screen. To minimize the
**impact of grabbing the screen image, we capture only the pixels under the
**areas to be newly covered (before the window actually moves there); these areas
**(max of 2) are calculated as the difference between the old and new bounds of the
**window. The base rectangle is the area common between the old and new bounds.
**
**Calculate tiled rectangles
*/
        Rectangle oldFrame = getBounds();
        Rectangle newFrame = new Rectangle(loc.x, loc.y, oldFrame.width, oldFrame.height);
        baseRect = oldFrame.intersection(newFrame);
        tile = SwingUtilities.computeDifference(newFrame, oldFrame);
/*
**Get base image from the captured ImageBuffer
*/
        if (baseRect.width > 0 && baseRect.height > 0)
            baseImage = currentImage.getSubimage(baseRect.x - oldFrame.x, baseRect.y - oldFrame.y, baseRect.width, baseRect.height);
        else
            baseImage = null;
/*
**Capture tile images from the screen
*/
        tileImage[0] = null;
        tileImage[1] = null;
        for (int i = 0; i < tile.length; i++) {
            if (!tile[i].isEmpty())
                tileImage[i] = robot.createScreenCapture(tile[i]);
        }
/*
**Translate the rectangles to the origin of the new frame
*/
        baseRect.x -= newFrame.x;
        baseRect.y -= newFrame.y;
        for (int i = 0; i < tile.length; i++) {
            if (!tile[i].isEmpty()) {
                tile[i].x -= newFrame.x;
                tile[i].y -= newFrame.y;
            }
        }
/*
**We now have up to 3 rectangles with images. They are assembled into
**one image by the paint() method.
*/
        super.setVisible(false);//Hide the window
        super.setLocation(loc);//Move it to the new location
        grabCurrentImage();//Save the image in the current location
        super.setVisible(true);//Show the window with the new image
    }

    public void paint(Graphics g) {
/*
**Draw the images (from up to 3 rectangles)
*/
        if (baseImage != null)
            g.drawImage(baseImage, baseRect.x, baseRect.y, null);
        for (int i = 0; i < tile.length; i++) {
            if (tileImage[i] != null)
                g.drawImage(tileImage[i], tile[i].x, tile[i].y, null);
        }
        if (drawBackgroundOnly)
            return;
/*
**Draw the content and red border to show where window is
**and the effects of transparency
*/
        g.setColor(Color.black);
        g.setFont(boldFont);
        g.drawString("Hello there", 5, 50);
        Rectangle rect = getBounds();
        g.setColor(Color.red);
        g.drawRect(0, 0, rect.width - 1, rect.height - 1);
    }
}


