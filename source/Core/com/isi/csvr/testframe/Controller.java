package com.isi.csvr.testframe;

import com.isi.csvr.shared.TWFont;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 11, 2005
 * Time: 5:26:27 PM
 */
class Controller implements DragGestureListener, DragSourceListener, DragSourceMotionListener, Transferable {
    private JFrame frame;
    private Container contentPane;
    private Rectangle windowRect;
    private Font plainFont, boldFont;
    private TransparentWindow cache;
    private DataFlavor[] dummyDataFlavors = {new DataFlavor(String.class, "")};

    public Controller() {
        frame = new JFrame();
        frame.addNotify();
        frame.pack();
        boldFont = new TWFont("SansSerif", Font.BOLD, 18);
        contentPane = frame.getContentPane();
        contentPane.setLayout(null);
        frame.setSize(350, 250);
        frame.validate();
        windowRect = frame.getBounds();
        createViews();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.doLayout();
        cache = new TransparentWindow();
    }

    public static void main(String args[]) {
        new Controller();
    }

    public void createViews() {
/*
**Drag source
*/
        JLabel dragSource = new JLabel("Drag from here...");
        dragSource.setFont(boldFont);
        dragSource.setSize(dragSource.getPreferredSize());
        dragSource.setLocation(20, 20);
        contentPane.add(dragSource);
        DragSource ds = DragSource.getDefaultDragSource();
        ds.addDragSourceMotionListener(this);
        ds.createDefaultDragGestureRecognizer(dragSource, DnDConstants.ACTION_MOVE, this);
    }

    public void dragGestureRecognized(DragGestureEvent dge) {
/*
**Pops the TransparentWindow at the mouse location
*/
        Point location = dge.getDragOrigin();
        SwingUtilities.convertPointToScreen(location, dge.getComponent());
        cache.setLocation(location);
        cache.setVisible(true);
        dge.startDrag(null, this, this);
    }

    public void dragDropEnd(DragSourceDropEvent dsde) {
        cache.setVisible(false);
    }

    public void dropActionChanged(DragSourceDragEvent dsde) {
    }

    public void dragEnter(DragSourceDragEvent dsde) {
    }

    public void dragExit(DragSourceEvent dse) {
    }

    public void dragMouseMoved(DragSourceDragEvent dsde) {
/*
**Reposition the TransparentWindow following the mouse
*/
        Point origin = dsde.getLocation();
        cache.setLocation(origin);
    }

    public void dragOver(DragSourceDragEvent dsde) {
    }

    public Object getTransferData(DataFlavor flavor) {
        return null;
    }

    public DataFlavor[] getTransferDataFlavors() {
        return (dummyDataFlavors);
    }

    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return (true);
    }
}