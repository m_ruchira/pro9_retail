package com.isi.csvr.financialcalender;


import com.isi.csvr.Client;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;

import java.text.ParseException;
import java.util.Date;

public class CABodyProcessor {
    private static CABodyProcessor self;
    private String[] captions;
    private String head = "<html><body bgcolor='#FFFFFF'><table border='0' cellspacing='1'>";
    private String body = "<tr><td ><b>[ID]</b></td><td >&nbsp;</td><td align='[ALIGN]'>[VALUE]</td></tr>";
    private String tail = "</table></body></html>";
    private ViewSetting settings;
    private TWDateFormat dateFormatIn = new TWDateFormat("yyyyMMdd");
    private TWDateFormat dateFormatOut = new TWDateFormat("dd/MM/yyyy");
    private TWDecimalFormat decimalFormat = new TWDecimalFormat("#,##0.00");
//    private CABrowserFrame caBrowserFrame;


    private CABodyProcessor() {
        captions = Language.getList("CA_BODY_COLS");
        settings = ViewSettingsManager.getSummaryView("CA_BODY");
//        caBrowserFrame = new CABrowserFrame();
//        caBrowserFrame.setLayer(GUISettings.POPUP_LAYER);
//        caBrowserFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
//        caBrowserFrame.setClosable(true);
//        Client.getInstance().getDesktop().add(caBrowserFrame);
    }

    public synchronized static CABodyProcessor getInstance() {
        if (self == null) {
            self = new CABodyProcessor();
        }
        return self;
    }

    public void process(String data) {
        StringBuffer text = new StringBuffer();
        String[] caBody = new String[2];

        String[] records = data.split(Meta.FD);
        String[] fields = records[0].split(Meta.ID);

        text.append(head);
        for (int i = 0; i < captions.length; i++) {

            try {
                if (isValidField(fields[i], i)) {
                    String row = body.replaceFirst("\\[ID\\]", captions[i]);
                    row = row.replaceFirst("\\[VALUE\\]", format(fields[i], i));
                    row = row.replaceFirst("\\[ALIGN\\]", getAlignment(i));
                    text.append(row);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        text.append(tail);

        caBody[0] = settings.getCaption();
        caBody[1] = text.toString();

        //Client.getInstance().getBrowserFrame().setText(text.toString());
        //CABrowser.getSharedInstance().updateUI();
//        caBrowserFrame.setSearchedCABody(caBody);
        Client.getInstance().getBrowserFrame().setContentType("text/html");
        Client.getInstance().getBrowserFrame().setText(caBody[1]);
        Client.getInstance().getBrowserFrame().setTitle(caBody[0]);
//        Client.getInstance().getBrowserFrame().show(Constants.DOC_TYPE_COROPARTE_ACTIONS);

//        caBrowserFrame.pack();
//        caBrowserFrame.pack();
        //CABrowser.getSharedInstance().show();
//        try {
//            if (caBrowserFrame.isIcon()){
//                caBrowserFrame.setIcon(false);
//            }else{
//                caBrowserFrame.setVisible(true);
//            }
//        } catch (PropertyVetoException e) {}
//        caBrowserFrame.moveToFront();
//        caBrowserFrame.packFully();
//        GUISettings.setLocationRelativeTo(caBrowserFrame,Client.getInstance().getDesktop());
    }

    private boolean isValidField(String value, int index) {
        int rID = settings.getRendererID(index);
        switch (rID) {
            case 'S':
            case 'D':
            case 'C':
            case 'M':
                if (value.equals("") || value.trim().equalsIgnoreCase("null"))
                    return false;
                else
                    return true;
            case 'F':
                if (SharedMethods.doubleValue(value, 0) == 0)
                    return false;
                else
                    return true;
            default:
                return false;
        }
    }

    private String format(String value, int index) {
        if (value.trim().equalsIgnoreCase("null")) {
            return null;
        }
        int rID = settings.getRendererID(index);
        switch (rID) {
            case 'C':
                return FinancialCalenderStore.getSharedInstance().getCACategory(value);
            case 'M':
//                return value;
                return Settings.getMarketCenter(value, 0);
            case 'S':
                return value;
            case 'F':
                return decimalFormat.format(SharedMethods.doubleValue(value, 0));
            case 'D':
                String dateValue = null;
                try {
                    Date dt = dateFormatIn.parse(value); //new Date(SharedMethods.longValue(value,0));
                    dateValue = dateFormatOut.format(dt);
                    dt = null;
                } catch (ParseException e) {
                    System.out.println(value);
                    e.printStackTrace();
                    dateValue = Language.getString("LBL_NA");
                }
                return dateValue;
            default:
                return "";
        }
    }

    private String getAlignment(int index) {
        int rID = settings.getRendererID(index);
        switch (rID) {
            case 'S':
                return "left";
            case 'F':
                return "left";
            case 'D':
                return "left";
            default:
                return "left";
        }
    }
}

/*        Type          Rend ID
        Category	        C
        EffectiveDate	    D
        Symbol	            S
        PrevSymbol	        S
        CompanyName	        S
        PrevCompanyName     S
        CUSIPNo	            S
        PrevCUSIPNo	        S
        ExchangeName	    S
        MarketCenter	    S
        PrevExchangeName	S
        PrevMarketCenter	M
        IPOSymbol	        S
        DelistedSymbol	    S
        ResumedSymbol	    S
        SuspendedSymbol	    S
        DivAmount	        F
        DivPercentage	    F
        SplitFactor	        F
        SplitRatio	        F
        AdjFactor	        F
        Comment	            S

*/

