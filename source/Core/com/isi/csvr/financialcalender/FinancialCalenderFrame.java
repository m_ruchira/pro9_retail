package com.isi.csvr.financialcalender;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 9, 2007
 * Time: 3:30:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class FinancialCalenderFrame implements ExchangeListener, MouseListener, ActionListener {
    private static InternalFrame announcementFrame;
    private FinancialCalenderModel model;
    private Table table;
    private JPanel panel;

    public FinancialCalenderFrame() {
        table = new Table();
        model = new FinancialCalenderModel();
        model.setDirectionLTR(Language.isLTR());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_FINANCIAL_CALENDER");
        model.setViewSettings(oSetting);
        table.setModel(model);
        //todo
        table.setAutoResize(false);
        table.getTable().addMouseListener(this);
        table.getPopup().setAutoWidthAdjustMenuVisible(false);
        table.setPreferredSize(oSetting.getSize());

        table.setWindowType(ViewSettingsManager.CORPORATE_ACTION_VIEW);
        model.setTable(table);
        model.applyColumnSettings();
        table.updateGUI();

        announcementFrame = new InternalFrame(table);
        announcementFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(announcementFrame);
        announcementFrame.getContentPane().add(table);
        //oTopDesktop.add(announcementFrame);
        announcementFrame.setResizable(true);
        announcementFrame.setClosable(true);
        announcementFrame.setMaximizable(true);
        announcementFrame.setIconifiable(true);
        announcementFrame.setColumnResizeable(true);
        announcementFrame.setDetachable(true);
        announcementFrame.setPrintable(true);
        announcementFrame.setTitle(Language.getString(oSetting.getCaptionID()));
        announcementFrame.updateUI();
        announcementFrame.applySettings();
        announcementFrame.setLayer(GUISettings.TOP_LAYER);
        announcementFrame.setVisible(false);
        announcementFrame.setSize(oSetting.getSize());
        announcementFrame.setLocationRelativeTo(Client.getInstance().getDesktop());
        Client.getInstance().getDesktop().add(announcementFrame);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public static InternalFrame getFinancialCalenderFrame() {
        if (announcementFrame == null) {
            new FinancialCalenderFrame();
        }
        return announcementFrame;
    }

    public void actionPerformed(ActionEvent e) {

    }

    // Exchange Listeners
    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
    }

    public void exchangesLoaded() {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            FinancialCalender calender = (FinancialCalender) table.getTable().getModel().getValueAt(table.getTable().getSelectedRow(), -1);
            FinancialCalenderStore.getSharedInstance().sendBodyRequest(calender.getId(), calender.getType());
        }
//        if (e.getSource() instanceof TWCustomCheckBox){
//            TWCustomCheckBox TWCustomCheckBox = (TWCustomCheckBox)e.getSource();
//            TWCustomCheckBox.setSelected(!TWCustomCheckBox.isSelected());
//            TWCustomCheckBox.repaint();
//        }else{
//        if (!(e.getSource() instanceof TWCustomCheckBox)){
//        StringBuilder buffer = new StringBuilder();
//        Component[] components = panel.getComponents();
//        for (int i = 0; i < components.length; i++) {
//            if (components[i] instanceof TWCustomCheckBox) {
//                TWCustomCheckBox component = (TWCustomCheckBox) components[i];
//                if (component.isSelected()) {
//                    buffer.append(component.getText());
//                    buffer.append(",");
//                }
//            }
//
//        }
//        String filterString = buffer.toString();
//        try {
//            if(filterString.substring(filterString.length()-1).equals(",")){
//                filterString = filterString.substring(0,filterString.length()-1);
//            }
//        } catch (Exception e1) {
//            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
//        if (filterString.equals("")) {
//            filterString = null;
//            announcementFrame.setTitle(Language.getString("ANNOUNCEMENTS"));
//        } else {
//            announcementFrame.setTitle(Language.getString("ANNOUNCEMENTS") + " (" + filterString.trim() + ")");
//        }
////        AnnouncementStore.getSharedInstance().setFilter(filterString);
////        AnnouncementStore.getSharedInstance().applyFilter();
//        table.getPopup().setVisible(false);
//        }
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }


}
