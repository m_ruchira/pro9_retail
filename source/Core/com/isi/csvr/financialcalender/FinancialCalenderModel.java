package com.isi.csvr.financialcalender;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 9, 2007
 * Time: 3:32:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class FinancialCalenderModel extends CommonTable
        implements TableModel {

    /**
     * Constructor
     */
    public FinancialCalenderModel() {
        //init();
    }

    public void setSymbol(String symbol) {

    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return FinancialCalenderStore.getSharedInstance().getCount();
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            FinancialCalender announcement = FinancialCalenderStore.getSharedInstance().getFinancialCalender(iRow);

            switch (iCol) {
                case -1:
                    return announcement;
                case 0:
                    return "" + announcement.getDate();
                case 1:
                    return "" + announcement.getType();
                case 2:
                    return UnicodeUtils.getNativeString((announcement.getName()));
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
//            case 3:
//            case 5:
//            case 6:
//            case 4:
//            case 7:
//            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[5];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEW_ANNOUNCEMENTS"), FIELD_NEW_ANNOUNCEMENT, null, Theme.getColor("ANNOUNCEMENT_NEW_LINE_FGCOLOR"));
        return customizerRecords;
    }

    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }
}