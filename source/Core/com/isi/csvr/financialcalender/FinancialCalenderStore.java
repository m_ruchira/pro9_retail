package com.isi.csvr.financialcalender;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 9, 2007
 * Time: 1:43:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class FinancialCalenderStore {

    private static FinancialCalenderStore self = null;
    private final int FORWARD_SEARCH = 0;
    private int searchDirection = FORWARD_SEARCH;
    private final int BACKWARD_SEARCH = 1;
    String exchangeCode = null;
    String symbol = null;
    int instrument = -1;
    Properties prop;
    //sath
    String sources = "";
    String symbols = "";
    String keywords = "";
    String startdate = "";
    String enddate = "";
    String id = null;
    private ArrayList<String> ids;
    private Hashtable<String, FinancialCalender> dataStore;
    private int count = 0;
    private String language = null;
    private SimpleDateFormat bodyDateFormat = null;
    private Date date = null;
    private String template = null;
    private boolean searchInProgess;
    private TWButton btnNext;
    private TWButton btnPrev;
    private TWButton btnSearch;
    //sath
    private int windowNumber = 0;
    private long nextID = 0;
    private long previouseID = 0;
    private int windowSize = 0;
    private int more = 0;

    //sath
    private int currentCount;
    private long topID = 0;

    private DynamicArray previouseIDs;

    private FinancialCalenderStore() {
        ids = new ArrayList<String>();
        bodyDateFormat = new SimpleDateFormat("dd/MM/yyyy '-' HH:mm");
        date = new Date();
        dataStore = new Hashtable<String, FinancialCalender>();
        //sath
        previouseIDs = new DynamicArray();
        prop = new Properties();
        try {
            FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH + "/category_" + Language.getSelectedLanguage() + ".dll");
            prop.load(oIn);
            oIn.close();

            try {
                File file = new File(Settings.SYSTEM_PATH + "/category.dll");
                if (file.exists()) {
                    oIn = new FileInputStream(file);
                    prop.load(oIn);
                    oIn.close();
                    file.deleteOnExit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {

        }
    }

    public static FinancialCalenderStore getSharedInstance() {
        if (self == null) {
            self = new FinancialCalenderStore();
        }
        return self;
    }

    public boolean isSearchInProgress() {
        return searchInProgess;
    }

    public void setSearchInProgress(boolean searchInProgress) {
        this.searchInProgess = searchInProgress;
    }

    public void clear() {
        ids.clear();
        dataStore.clear();
    }

    public String getCACategory(String type) {
        return prop.getProperty(type);
    }

    public void setData(String response) {
        //sath
        boolean firstItem = true;


        try {
            String[] data = response.split(Meta.FD);
//            exchangeCode = data[0];
//            symbol = data[1];
//            language = data[2];
            if (data.length < 3) {
                new ShowMessage(Language.getString("NO_RESULTS_FOUND"), "I");
            }
            count = Integer.parseInt(data[0]);
            more = Integer.parseInt(data[1]);
            String[] params = null;
            FinancialCalender calender = null;
            for (int i = 2; i < data.length; i++) {
                try {
                    params = data[i].split(Meta.ID);
                    exchangeCode = params[0];
                    calender = addFinancialObject(SharedMethods.intValue(params[2]), SharedMethods.intValue(params[3]));
                    calender.setName(getCACategory(params[3]));                         //UnicodeUtils.getNativeStringFromCompressed(params[1])
                    calender.setDate(SharedMethods.longValue(params[4]));

                    //sath
                    nextID = calender.getId();
                    if (firstItem) {
                        previouseID = topID;
                        setPreviouseID(previouseID);
                        topID = calender.getId();
                        firstItem = false;
                    }


                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

            if (btnPrev != null) {
                if ((windowNumber == 0)) {
                    btnPrev.setEnabled(false);
                } else {
                    btnPrev.setEnabled(true);
                }
            }

            //sath todo set initial next button enablility when window is first createed
            if (btnNext != null) {
                if ((more > 0)) {
                    btnNext.setEnabled(false);
                } else {
                    btnNext.setEnabled(true);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        enableButtons();
    }

    public void setBody(String data) {
        try {
            String[] params = data.split(Meta.FD);
            exchangeCode = params[0];
            String eventType = params[1];
            int eventID = Integer.parseInt(params[2]);
            language = params[3];
            FinancialCalender calender = getFinancialCalender("" + eventID);
//            FinancialCalender calender = addFinancialObject(eventID, Integer.parseInt(eventType));
            calender.setBody(UnicodeUtils.getNativeStringFromCompressed(params[4]));
            showBody(calender);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void showBody(FinancialCalender calender) {
        String body = generateTempFile(calender);
        String company = UnicodeUtils.getNativeString(DataStore.getSharedInstance().getShortDescription(exchangeCode, symbol, instrument));
        Client.getInstance().getBrowserFrame().setContentType("text/html");
        Client.getInstance().getBrowserFrame().setTitle(Language.getString("CORPORATE_ACTIONS") + " " + company);
        Client.getInstance().getBrowserFrame().setText(body);
//        Client.getInstance().getBrowserFrame().setMoreInfoURL(announcement.getUrl());
        Client.getInstance().getBrowserFrame().show(Constants.DOC_TYPE_COROPARTE_ACTIONS);
        Client.getInstance().getBrowserFrame().bringToFont();

    }

    public String generateTempFile(FinancialCalender calender) {

        try {
            loadTemplate(language);
            date.setTime(calender.getDate());
            String title = calender.getName();
            String body = calender.getBody();
            body = body.replaceAll("\r\n", "<br>"); // replace the line break with HTML line break
            body = body.replaceAll("\n", "<br>");   // replace the line break with HTML line break

            String page = replaceString(template, "[NEWS_TITLE]", title);

            if (body == null) {
                page = replaceString(page, "[NEWS_BODY]", title);
            } else {
                page = replaceString(page, "[NEWS_BODY]", body);
            }
            page = replaceString(page, "[NEWS_DATE]", bodyDateFormat.format(date));
            return page;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private String replaceString(String buffer, String tag, String str) {
        int index = buffer.indexOf(tag);
        if (index >= 0) {
            return buffer.substring(0, index) + str + buffer.substring(index + tag.length());
        }
        return buffer;
    }

    private void loadTemplate(String language) {
        StringBuilder buffer = new StringBuilder();
        byte[] temp = new byte[1000];
        int count = 0;

        try {
            if ((language == null) || (language.equals(""))) {
                language = Language.getLanguageTag();
            }
            InputStream in = new FileInputStream(".\\Templates\\FinancialCalendar_" + language + ".htm");
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            template = buffer.toString();
            if (language == null) {
                if (Language.isLTR())
                    template = template.replaceFirst("\\[DIR\\]", "LTR");
                else
                    template = template.replaceFirst("\\[DIR\\]", "RTL");
            } else {
                if (language.toUpperCase().equals("EN"))
                    template = template.replaceFirst("\\[DIR\\]", "LTR");
                else
                    template = template.replaceFirst("\\[DIR\\]", "RTL");
            }
            buffer = null;
            in = null;
            temp = null;
        } catch (Exception ex) {
            template = "";
        }
    }

    public int getCount() {
        return ids.size();
    }

    public FinancialCalender addFinancialObject(int id, int type) {
        FinancialCalender calender = dataStore.get(id);
        if (calender == null) {
            calender = new FinancialCalender(id, type);
            dataStore.put("" + id, calender);
            ids.add("" + id);
        }
        return calender;
    }

    public FinancialCalender getFinancialCalender(String id) {
        return dataStore.get("" + id);
    }

    public FinancialCalender getFinancialCalender(int row) {
        return dataStore.get(ids.get(row));
    }

    public void setButtonReferences(TWButton btnNext, TWButton btnPrev, TWButton btnSearch) {
        this.btnPrev = btnPrev;
        this.btnNext = btnNext;
        this.btnSearch = btnSearch;
    }

    public void disableButtons() {

        try {
            btnPrev.setEnabled(false);
            btnNext.setEnabled(false);
            btnSearch.setEnabled(false);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public int getWindowNumber() {
        return windowNumber;
    }

    public void enableButtons() {
        if (windowNumber == 0) {
            btnPrev.setEnabled(false);
        } else {
            btnPrev.setEnabled(true);
        }

        if (isMoreRecords()) {
            btnNext.setEnabled(true);
        } else {
            btnNext.setEnabled(false);
        }
        btnSearch.setEnabled(true);
    }

    //Meta.SRD + Meta.CA_SEARCH_REQUEST + Meta.FD + id + Meta.FD + sources + Meta.FD + symbols + Meta.FD + keywords + Meta.FD + startdate + Meta.FD + enddate + Meta.FD + nextID + Meta.ERD;
    /*public void sendSymbolRequest(String symbol, String exchange){
        clear();
        setSearchInProgress(true);
//        String request = Meta.CORP_ACTION_SEARCH_REQUEST+ Meta.DS + exchange + Meta.FD + symbol + Meta.FD + "EN"+Meta.EOL;// Language.getSelectedLanguage();
        String request = Meta.CORP_ACTION_SEARCH_REQUEST+ Meta.DS + exchange + Meta.FD + symbol + Meta.FD + -1+Meta.FD+"1970-01-01"+Meta.FD+Meta.FD+0+Meta.FD+0+Meta.EOL;// Language.getSelectedLanguage();
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request,exchange);
    }
*/
    public void sendSymbolRequest(String key) {
        clear();
        String baseSymbol = SharedMethods.getSymbolFromKey(key);
        Stock stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), SharedMethods.getInstrumentTypeFromKey(key));
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            String symbol = SharedMethods.getSymbolFromKey(key);
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                baseSymbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
            }
        }
        setSearchInProgress(true);
        String request = Meta.CORP_ACTION_SEARCH_REQUEST + Meta.DS + SharedMethods.getExchangeFromKey(key) + Meta.FD + baseSymbol + Meta.FD + -1 + Meta.FD + "1970-01-01" + Meta.FD + Meta.FD + 0 + Meta.FD + 0 + Meta.EOL;// Language.getSelectedLanguage();
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request, SharedMethods.getExchangeFromKey(key));
    }

    public void sendSearchRequest(String symbol, String exchange, int instrument, int ytpe, String startDate, String endDate) {
        //Meta.SRD + Meta.CA_SEARCH_REQUEST + Meta.FD + id + Meta.FD + sources + Meta.FD + symbols + Meta.FD + keywords + Meta.FD + startdate + Meta.FD + enddate + Meta.FD + nextID + Meta.ERD;
        clear();
        String baseSymbol = symbol;
        Stock stock = DataStore.getSharedInstance().getStockObject(symbol, exchange, instrument);
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                baseSymbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
            }
        }
        setSearchInProgress(true);
        String request = Meta.CORP_ACTION_SEARCH_REQUEST + Meta.DS + exchange + Meta.FD + baseSymbol + Meta.FD + -1 + Meta.FD + startDate + Meta.FD + endDate + Meta.FD + 0 + Meta.FD + 0 + Meta.EOL;// Language.getSelectedLanguage();
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request, exchange);
    }

    public void sendBodyRequest(int eventID, int eventType) {
        String request = Meta.CORP_ACTION_BODY_REQUEST + Meta.DS + eventID + Meta.EOL;//Language.getSelectedLanguage();
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request, exchangeCode);
    }

    //sath
    public void sendCommonCARequest() {
        clear();
        setSearchInProgress(true);
        String baseSymbol = SharedMethods.getSymbolFromKey(symbols);
        int instrument = SharedMethods.getInstrumentTypeFromKey(symbols);
        Stock stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(symbols), SharedMethods.getSymbolFromKey(symbols), instrument);
        if ((stock != null) && (stock.getMarketCenter() != null)) {
            String symbol = SharedMethods.getSymbolFromKey(symbols);
            if (symbol.endsWith("." + stock.getMarketCenter())) {
                baseSymbol = SharedMethods.getKey(SharedMethods.getExchangeFromKey(symbols), symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter())), instrument);
            }
        }
        String request = Meta.CORP_ACTION_SEARCH_REQUEST + Meta.DS +
                sources + Meta.FD +
                baseSymbol + Meta.FD +
                keywords + Meta.FD +
                startdate + Meta.FD +
                //Meta.FD+
                enddate + Meta.FD +
                nextID + Meta.EOL;

//        String request =  Meta.CORP_ACTION_SEARCH_REQUEST + Meta.FD + id + Meta.FD + sources + Meta.FD +
//		symbols + Meta.FD + keywords + Meta.FD + startdate + Meta.FD +
//    	enddate + Meta.FD + nextID + Meta.ERD;
        System.out.println("sathreq: " + request);
        //SRD + CSPMeta.CA_SEARCH_REQUEST + FD + exchange + FD + symbol + FD + category + FD + startDate + FD + endDate + ERD;
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, request, null);

    }

    //sath
    public void PrepareRequest(String newSources, String newSymbols,
                               String newKeywords, String newStartdate, String newEnddate) {

        String NULL_STRING = "";

        if (newSymbols.trim().equals(NULL_STRING)) {
            newSymbols = "*";
        }
        if (newKeywords.trim().equals(NULL_STRING)) {
            newKeywords = "-1";
        }
        sources = newSources;
        symbols = newSymbols;
        keywords = newKeywords;
        startdate = newStartdate;
        enddate = newEnddate;
    }


    public void goPreviouse() {
        //newsList.clear();
        nextID = Integer.parseInt((String) previouseIDs.get(windowNumber));//previouseID+1;
        windowNumber--;
        //searchDirection = BACKWARD_SEARCH;
        //currentCount -= windowSize;
        //windowSize = 0;
        sendCommonCARequest();

    }

    public void goNext() {
        //newsList.clear();
        windowNumber++;
        //searchDirection = FORWARD_SEARCH;
        //windowSize = 0;

        sendCommonCARequest();
    }


    private void setPreviouseID(long previouseID) {
        if (previouseIDs.size() <= windowNumber) {
            previouseIDs.add("" + previouseID);
        }
    }


    public void reset() {
        topID = 0;
        previouseID = 0;
        nextID = 0;
        previouseIDs.clear();
        windowNumber = 0;
    }

    public boolean isMoreRecords() {
        return Boolean.parseBoolean("" + more);
    }

}
