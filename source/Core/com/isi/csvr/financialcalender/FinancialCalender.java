package com.isi.csvr.financialcalender;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 9, 2007
 * Time: 12:43:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class FinancialCalender {

    private int id = -1;
    private int type = -1;
    private long date = -1;
    private String body = null;
    private String name = null;
    private String lang = null;
    private String symbol = null;
    private String exchange = null;

    public FinancialCalender(int id, int type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
