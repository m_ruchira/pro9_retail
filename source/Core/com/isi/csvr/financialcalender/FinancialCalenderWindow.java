package com.isi.csvr.financialcalender;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

//import com.isi.csvr.news.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 27, 2007
 * Time: 9:51:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class FinancialCalenderWindow extends InternalFrame implements ExchangeListener, ActionListener,
        Themeable {

    private static FinancialCalenderWindow self = null;
    final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
    final String searching = Language.getString("SEARCHING");
    //    private NewsMouseListener selectionListener;
//    ArrayList<TWComboItem> exchangeArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> typeArray = new ArrayList<TWComboItem>();
    private FinancialCalenderModel model;
    private JPanel mainPanel;
    //    private JPanel searchPanel;
    private JPanel nextPanel;
    private Table table;
    //    private JLabel lblexchange;
    private JLabel lblType;
    private JLabel lblNull;
    private JLabel lblNull2;
    //    private TWButton btnSearch;
    private TWButton btnAdvSearch;
    //    private TWButton btnLatest;
    private TWButton btnNext;
    private TWButton btnPrev;
    //    private TWComboBox cmbExchange;
    private TWComboBox cmbType;
    //    private TWComboModel exchangeModel;
    private TWComboModel typeModel;
    private boolean isLatest = true;
    private int width = 800;
    private int height = 400;

    private FinancialCalenderWindow() {
        super(false);
        table = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
//                if(!isLatest){
                if (FinancialCalenderStore.getSharedInstance().isSearchInProgress()) {
                    FontMetrics fontMetrics = table.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                            (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - ((fontMetrics.stringWidth(searching)) / 2) - 165),
                            (getHeight() - icon.getImage().getHeight(this)) / 2 + 15, this);
                    //+ fontMetrics.stringWidth(searching) + 10
                    fontMetrics = null;
                }
//                }
            }
        };
        createUI();
    }

    public static FinancialCalenderWindow getSharedInstance() {
        if (self == null) {
            self = new FinancialCalenderWindow();
        }
        return self;
    }

    private void createUI() {

        String[] mainCols = {"100%"};
//        if (Settings.isTCPMode()) {
//            String[] mainRows = {"35","100%","35"};
//            this.setLayout(new FlexGridLayout(mainCols, mainRows));
//        } else {
        String[] mainRows = {"100%", "35"};    //,"35"
        this.setLayout(new FlexGridLayout(mainCols, mainRows));
//        }

        this.setSize(width, height);

//        String[] searchCols = {"100","100","100","100","120","120"};         //,"120"
//        String[] searchRows = {"100%"};
//        searchPanel = new JPanel();
//        searchPanel.setLayout(new FlexGridLayout(searchCols,searchRows,5,5,true,true));

        String[] bottomPanelWidths = {"0", "50%", "0", "50%", "0"};
        String[] bottonPanelHeights = {"25"};
        nextPanel = new JPanel(new FlexGridLayout(bottomPanelWidths, bottonPanelHeights, 5, 5, true, true));

//        lblexchange = new JLabel(Language.getString("EXCHANGE"));
        lblType = new JLabel(Language.getString("DURATION"));
        lblType.setHorizontalAlignment(JLabel.CENTER);
        lblNull = new JLabel("");
        lblNull2 = new JLabel("");
//        btnSearch = new TWButton(Language.getString("SEARCH"));
//        btnSearch.addActionListener(this);
        btnAdvSearch = new TWButton(Language.getString("ADV_SEARCH"));
        btnAdvSearch.addActionListener(this);
//        btnLatest = new TWButton(Language.getString("LATEST_NEWS"));
//        btnLatest.addActionListener(this);
//        btnClose = new TWButton(Language.getString("CLOSE"));
//        btnClose.addActionListener(this);
        btnNext = new TWButton(Language.getString("NEXT"));
        btnNext.addActionListener(this);
        btnNext.setActionCommand("NEXT");
        btnPrev = new TWButton(Language.getString("PREVIOUS"));
        btnPrev.addActionListener(this);
        btnPrev.setActionCommand("PREVIOUS");
        btnNext.setEnabled(false);
        btnPrev.setEnabled(false);
//        populateDuration();
//        populateExchanges();
//        exchangeModel = new TWComboModel(exchangeArray);
        typeModel = new TWComboModel(typeArray);
//        cmbExchange = new TWComboBox(exchangeModel);
        cmbType = new TWComboBox(typeModel);
        populateDuration();
//        populateExchanges();

//        searchPanel.add(lblexchange);
//        searchPanel.add(cmbExchange);
//        searchPanel.add(lblType);
//        searchPanel.add(cmbType);
//        searchPanel.add(btnSearch);
//        searchPanel.add(btnAdvSearch);
//        searchPanel.add(btnLatest);
//        searchPanel.add(btnClose);

        nextPanel.add(btnPrev);
        nextPanel.add(lblNull);
        nextPanel.add(new JLabel()/*btnAdvSearch*/);
        nextPanel.add(lblNull2);
        nextPanel.add(btnNext);

        this.setTable(table);
        model = new FinancialCalenderModel();
        model.setDirectionLTR(Language.isLTR());
        table.setWindowType(ViewSettingsManager.CORPORATE_ACTION_VIEW);
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_FINANCIAL_CALENDER");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("CORPORATE_ACTION"));
        model.setViewSettings(oSetting);
        table.setModel(model);
        table.setAutoResize(false);
        table.getTable().addMouseListener(this);
        table.getPopup().setAutoWidthAdjustMenuVisible(false);
        table.setPreferredSize(new Dimension(600, 100));

        model.setTable(table);
        model.applyColumnSettings();
        table.updateGUI();

        this.getContentPane().add(table);
        this.getContentPane().add(nextPanel);

        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(this);
        this.setResizable(true);
        this.setColumnResizeable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        this.setSize(oSetting.getSize());
        this.setLocation(oSetting.getLocation());
//        model.applySettings();
        this.updateUI();
        this.applySettings();
        this.setVisible(oSetting.isVisible());
        table.repaint();
        Theme.registerComponent(this);
        this.applyTheme();
        cmbType.addActionListener(this);
        this.hideTitleBarMenu();
        table.getPopup().hideTitleBarMenu();
        this.setLayer(GUISettings.TOP_LAYER);
//        super.applySettings();
        FinancialCalenderStore.getSharedInstance().setButtonReferences(btnNext, btnPrev, btnAdvSearch);

    }

   /* public void populateExchanges(){
        exchangeArray.clear();

        Enumeration exchangeObjects = ExchangeStore.getSharedInstance().getExchanges();
        while (exchangeObjects.hasMoreElements()) {
            Exchange exchanges = (Exchange) exchangeObjects.nextElement();
            exchangeArray.add(new TWComboItem(exchanges.getSymbol(), exchanges.getDescription()));
            exchanges = null;
        }
        exchangeObjects = null;
        Collections.sort(exchangeArray);
        if(exchangeArray.size()>1){
            TWComboItem allItem =  new TWComboItem("*", Language.getString("ALL"));
            exchangeArray.add(0,allItem);
        }
        try {
            cmbExchange.setSelectedIndex(0);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }*/


    public void show() {
        table.repaint();
        super.show();    //To change body of overridden methods use File | Settings | File Templates.
    }


    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            FinancialCalender calender = (FinancialCalender) table.getTable().getModel().getValueAt(table.getTable().getSelectedRow(), -1);
            Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_COROPARTE_ACTIONS);
            Client.getInstance().getBrowserFrame().setTitle(Language.getString("CORPORATE_ACTIONS"));
            FinancialCalenderStore.getSharedInstance().sendBodyRequest(calender.getId(), calender.getType());
        }
    }

    public void applyTheme() {
        try {
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
        }
    }

    public void setDefaultValues() {
//        cmbExchange.setSelectedIndex(0);
//        cmbType.setSelectedIndex(0);
//        cmbExchange.setEnabled(false);
//        btnSearch.setEnabled(false);
    }

    /*public void setSelectedExchange(String exchange){
        for(TWComboItem item: exchangeArray){
            if (exchange.equals(item.getId())){
                cmbExchange.setSelectedItem(item);
                return;
            }
        }
    }*/

    public void setDataStore(boolean isLatestNews) {
//        if(isLatestNews){
//            model.setDataStore(NewsStore.getSharedInstance().getFilteredNewss());
//            table1.repaint();
//        }else{
//            model.setDataStore(SearchedNewsStore.getSharedInstance().getStore());
//            table1.repaint();
//        }
    }

    public void setSearchedDataStore() {
//        model.setDataStore(SearchedNewsStore.getSharedInstance().getStore());
    }

    public void actionPerformed(ActionEvent e) {
//        if(e.getSource()==btnLatest){
//            showLatestNews();
//        }else if(e.getSource()==btnSearch){
//            if (cmbType.getSelectedIndex() == 0){
//                showLatestNews();
//            }else{
//                showSearchedNews();
//            }
//        }else
        if (e.getSource() == btnAdvSearch) {
//            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
            isLatest = false;
            Client.getInstance().showCASearchPanel();
//            NewsSearchWindow nswindow = NewsSearchWindow.getInstance();
//            nswindow.setLocationRelativeTo(Client.getInstance().getDesktop());
//            nswindow.setVisible(true);
//            table.repaint();
//            this.updateUI();
//            btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
        } else if (e.getSource() == btnNext) {
//            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
//                SearchedNewsStore.getSharedInstance().doNextSearch();
//            this.updateUI();
//            btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
//            SearchedNewsStore.getSharedInstance().enableButtons();
        } else if (e.getSource() == btnPrev) {
//            if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
//                SearchedNewsStore.getSharedInstance().doPreviuosSearch();
//            this.updateUI();
//            btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//            btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
//            SearchedNewsStore.getSharedInstance().enableButtons();
        } else if (e.getSource() == cmbType) {
            if (cmbType.getSelectedIndex() == 0) { // latest news selected
//                cmbExchange.setSelectedIndex(0);
//                cmbExchange.setEnabled(false);
                showLatestNews();
//            } else {
//                cmbExchange.setEnabled(true);
            }
        }
    }

    public void disableButtons() {

        try {
            btnPrev.setEnabled(false);
            btnNext.setEnabled(false);
            btnAdvSearch.setEnabled(false);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void enableButtons() {
        if (FinancialCalenderStore.getSharedInstance().getWindowNumber() == 0) {
            btnPrev.setEnabled(false);
        } else {
            btnPrev.setEnabled(true);
        }

        if (FinancialCalenderStore.getSharedInstance().isMoreRecords()) {
            btnNext.setEnabled(true);
        } else {
            btnNext.setEnabled(false);
        }
        btnAdvSearch.setEnabled(true);
    }

    private void showLatestNews() {
        if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;
//        int index = cmbExchange.getSelectedIndex();
//        TWComboItem item = exchangeArray.get(index);
//        String exchangeID = item.getId();
        isLatest = true;
        btnNext.setEnabled(false);
        btnPrev.setEnabled(false);
//        cmbExchange.setSelectedIndex(0);
//        cmbType.setSelectedIndex(0);
//        if(!exchangeID.equals("*")){
//            NewsStore.getSharedInstance().setFilter(exchangeID);
//            NewsStore.getSharedInstance().setFilterType(NewsList.FILTER_TYPE_EXCHANGE);
//            NewsStore.getSharedInstance().applyFilter();
//        }else {
//            NewsStore.getSharedInstance().setFilter(null);
//            NewsStore.getSharedInstance().setFilterType(NewsList.FILTER_TYPE_EXCHANGE);
//            NewsStore.getSharedInstance().applyFilter();
//        }
//        selectionListener.setMode(NEWS_TYPES.ALL);
//        model.setDataStore(NewsStore.getSharedInstance().getFilteredNewss());
        table.repaint();
        this.updateUI();
    }

    private void showSearchedNews() {
        if (!Client.getInstance().isValidSystemWindow(Meta.IT_News, false)) return;

        isLatest = false;
//        selectionListener.setMode(NEWS_TYPES.SEARCHED);
//        model.setDataStore(SearchedNewsStore.getSharedInstance().getStore());
        setSearchString(0);
        table.repaint();
        this.updateUI();
//        btnNext.setEnabled(SearchedNewsStore.getSharedInstance().enableNext());
//        btnPrev.setEnabled(SearchedNewsStore.getSharedInstance().enablePrev());
    }

    private void setSearchString(long sequenceNo) {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
        String keyword = "*";
        String symbol = "*";
//        int index = cmbExchange.getSelectedIndex();
//        TWComboItem item = exchangeArray.get(index);
//        String exchangeID = item.getId();
        String providerID = "*";
        String toDate = dateformat.format(new Date(System.currentTimeMillis()));
        int index1 = cmbType.getSelectedIndex();
        String fromDate = null;
        Calendar cal = Calendar.getInstance();
        if (index1 == 1) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            fromDate = dateformat.format(cal.getTime());
        } else if (index1 == 2) {
            cal.add(Calendar.WEEK_OF_MONTH, -1);
            fromDate = dateformat.format(cal.getTime());
        } else if (index1 == 3) {
            cal.add(Calendar.MONTH, -1);
            fromDate = dateformat.format(cal.getTime());
        } else if (index1 == 4) {
            cal.add(Calendar.YEAR, -1);
            fromDate = dateformat.format(cal.getTime());
        } else {
            fromDate = "19700101";
        }
//        String searchString = Meta.NEWS_SEARCH_ALL + Meta.DS + exchangeID + Meta.FD + symbol + Meta.FD + providerID+ Meta.FD +
//                                keyword + Meta.FD + fromDate + Meta.FD + toDate ;// + Meta.FD + sequenceNo ;

//            SearchedNewsStore.getSharedInstance().initSearch();
//            SearchedNewsStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), sequenceNo,exchangeID);
//            SearchedNewsStore.getSharedInstance().setSearchInProgress(true);
//            SearchedNewsStore.getSharedInstance().disableButtons();
    }

    private void populateDuration() {
        typeArray.clear();
        typeArray.add(new TWComboItem("*", Language.getString("LATEST_NEWS")));
        typeArray.add(new TWComboItem(1, Language.getString("1_DAY")));
        typeArray.add(new TWComboItem(2, Language.getString("1_WEEK")));
        typeArray.add(new TWComboItem(3, Language.getString("1_MONTH")));
        typeArray.add(new TWComboItem(4, Language.getString("1_YEAR")));

        try {
            cmbType.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exchangeAdded(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
       /* cmbExchange.removeActionListener(this);
        exchangeArray.clear();
        TWComboItem allItem =  new TWComboItem("*", Language.getString("ALL"));
        exchangeArray.add(allItem);
        Enumeration exchangeObjects = ExchangeStore.getSharedInstance().getExchanges();
        while (exchangeObjects.hasMoreElements()) {
            Exchange exchanges = (Exchange) exchangeObjects.nextElement();
            exchangeArray.add(new TWComboItem(exchanges.getSymbol(), exchanges.getDescription()));
            exchanges = null;
        }
        exchangeObjects = null;
        try {
            cmbExchange.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbExchange.addActionListener(this);*/
//        populateExchanges();
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

}
