package com.isi.csvr.util;

import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import java.security.PublicKey;

/**
 * Created by ThusharaJ.
 * User: admin
 * Date: Apr 9, 2007
 * Time: 2:21:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class Encripter {
    private PublicKey publicKey = null;
    private String sPpublicKeyXML = null;

    public Encripter(String base64EncPubKey) {
        publicKey = new RSA_Public_Key_Generator(base64EncPubKey).getPublicKey();
    }

    private static byte[] innerEncryption(byte[] message, java.security.Key key) {
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] data = cipher.doFinal(message);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String encrypt(String plainText) throws Exception {
        String encripted = new BASE64Encoder().encode(innerEncryption(plainText.getBytes("UTF-8"), publicKey));
        return encripted;
    }

    public String getPublicKeyXML() {
        sPpublicKeyXML = sPpublicKeyXML.replaceAll("\r", "").replaceAll("\n", "").trim();
        return sPpublicKeyXML;
    }
}
