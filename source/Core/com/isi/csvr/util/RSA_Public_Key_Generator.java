package com.isi.csvr.util;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class RSA_Public_Key_Generator {
    private PublicKey publicKey = null;

    public RSA_Public_Key_Generator(String public_key_X509_base64) {
        try {
            byte[] publicKeyBytes = new sun.misc.BASE64Decoder().decodeBuffer(public_key_X509_base64);
            RSAPublicKey rsaPublicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").
                    generatePublic(new X509EncodedKeySpec(publicKeyBytes));

            this.publicKey = rsaPublicKey;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }
}
