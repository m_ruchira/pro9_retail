package com.isi.csvr.util;


import com.isi.csvr.shared.TWButton;

import javax.swing.*;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class Console extends JTextArea {
    private static Console self = null;
    final int maxExcess = 500;
    JFrame frame;
    int idealSize = 1000;
    JScrollPane scroll;

    private Console(InputStream[] inStreams) {
        for (int i = 0; i < inStreams.length; ++i)
            startConsoleReaderThread(inStreams[i]);
    } // ConsoleTextArea()


    private Console() throws IOException {
        final ConsoleStream ls = new ConsoleStream();


        setFont(java.awt.Font.decode("Courier"));
        setEditable(false);
        setForeground(Color.yellow);
        setBackground(Color.black);
        scroll = new JScrollPane(this);

        // Redirect System.out & System.err.
        PrintStream ps = new PrintStream(ls.getOutputStream());
        System.setOut(ps);
        System.setErr(ps);

        frame = new JFrame("TW Console");
        frame.setDefaultCloseOperation(frame.HIDE_ON_CLOSE);
        frame.getContentPane().add(scroll, java.awt.BorderLayout.CENTER);
        frame.setBounds(50, 50, 300, 300);

        TWButton btnClear = new TWButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setText("");
            }
        });
        frame.getContentPane().add(btnClear, java.awt.BorderLayout.SOUTH);

        frame.addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                idealSize = 5000;
            }

            public void windowClosing(WindowEvent e) {
                idealSize = 1000;
            }
        });

        startConsoleReaderThread(ls.getInputStream());
    } // ConsoleTextArea()

    public synchronized static Console getSharedInstance() {
        if (self == null) {
            try {
                self = new Console();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use Options | File Templates.
            }
        }
        return self;
    }

    private void startConsoleReaderThread(
            InputStream inStream) {
        final BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
        new Thread(new Runnable() {
            public void run() {
                StringBuffer sb = new StringBuffer();
                try {
                    String s;
                    Document doc = getDocument();
                    int excess;
                    while ((s = br.readLine()) != null) {
                        if (frame.isVisible()) {
                            boolean caretAtEnd = false;
                            caretAtEnd = getCaretPosition() == doc.getLength() ? true : false;
                            sb.setLength(0);
                            append(sb.append(s).append('\n').toString());
                            if (caretAtEnd)
                                setCaretPosition(doc.getLength());

                            //Keep the text area down to a certain character size

                            excess = getDocument().getLength() - idealSize;
                            if (excess >= maxExcess) {
                                replaceRange("", 0, excess);
                            }
                            s = null;
                        }
                    }
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(null,
                            "Error reading from BufferedReader: " + e);
                    System.exit(1);
                }
            }
        }, "Console").start();
    } // startConsoleReaderThread()

    public void activate() {

        frame.setVisible(true);
    }
    // startWriterTestThread()
} // ConsoleTextArea


