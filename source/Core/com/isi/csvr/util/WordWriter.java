/*
 * (C) Copyright 2009-2010 Direct FN Technologies Limited. All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * Direct FN Technologies and constitute a TRADE SECRET of Direct FN Technologies Limited.
 *
 * Direct FN Technologies Limited retains all title to and intellectual property rights
 * in these materials.
 */
package com.isi.csvr.util;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Settings;
import com.jniwrapper.win32.automation.Automation;
import com.jniwrapper.win32.automation.OleContainer;
import com.jniwrapper.win32.automation.OleMessageLoop;
import com.jniwrapper.win32.automation.impl.IDispatchImpl;
import com.jniwrapper.win32.automation.types.Variant;
import com.jniwrapper.win32.com.ComException;
import com.jniwrapper.win32.ole.OleFunctions;
import com.mubasher.operations.OfficeFileOperationsHandler;


/**
 * com.isi.csvr.util.WordWriter
 */
public class WordWriter {

    public static final String wdPaperA3 = "6";
    public static final String wdPaperA4 = "7";
    public static final String wdPaperA5 = "9";
    public static final String wdPaperB4 = "10";
    public static final String wdPaperB5 = "11";
    public static final String wdPaperLetter = "2";
    public static final String wdPaperTabloid = "23";
    public static final String wdPaperLegal = "4";
    public static final String wdOrientLandscape = "1";
    public static final String wdOrientPortrait = "0";
    /**
     * progid of word document
     */
    private static final String DOCUMENT_PROGID = "Word.Document";
    private final int wdLineStyleSingle = 1;
    private final int wdHorizontalLineAlignCenter = 1;
    private final int wdSeparateByCommas = 2;
    private OleContainer _container;
    private StringBuffer buffer = new StringBuffer();

    public WordWriter() {
        // initialize OLE
        try {
            OleFunctions.oleInitialize();
            _container = new OleContainer();
            _container.createObject(DOCUMENT_PROGID);

            // Enable open / save operations
            _container.setFileOperationsHandler(new OfficeFileOperationsHandler(OfficeFileOperationsHandler.TYPE_WORD));
        } catch (ComException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        // Enable print / print preview
        // _container.setPrintDocumentHandler(new OfficePrintHandler());
    }

    public static boolean isMSWordAvailable() {
        try {
            WordWriter dummyFile = new WordWriter();
            try {
                dummyFile.saveFile(Settings.getAbsolutepath() + "temp/emptyfile.doc");
            } catch (Exception e) {
                System.out.println("checking for MSWord");
            }
            return true;
        } catch (Exception e) {
            System.out.println("== ***MSWord Not available ===");
            return false;
        }
    }

    public void saveFile(String filepath) {
        try {
            OleMessageLoop.invokeMethod(this, "setSaveFile", new Object[]{filepath});
            Constants.isMSWordAvailable = 1;
        } catch (Exception ex) {
            Constants.isMSWordAvailable = 2;
            ex.printStackTrace();
        }
    }

    public void addColumnNames(String text) {
        try {
            OleMessageLoop.invokeMethod(this, "appendColumnNames", new Object[]{text});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void appendColumnNames(String names) {
        buffer.append(names);
    }

    public void addDetails(String data) {
        try {
            OleMessageLoop.invokeMethod(this, "appendDetails", new Object[]{data});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void appendDetails(String data) {
        buffer.append(data);
    }

    public void writeTable() {
        try {
            OleMessageLoop.invokeMethod(this, "writeToTable");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setPage(String orientation, String size, String leftMargin, String rightMargin, String topMargin, String bottomMargin) {
        try {
            OleMessageLoop.invokeMethod(this, "setPageSetup", new Object[]{orientation, size, leftMargin, rightMargin, topMargin, bottomMargin});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setPageSetup(String orientation, String paperSize, String leftMargin, String rightMargin, String topMargin, String bottomMargin) {

        Automation appAutomationPageSetup = null;
        try {
            IDispatchImpl document = new IDispatchImpl(_container.getOleObject());
            document.setAutoDelete(false);
            Automation automation = new Automation(document);

            IDispatchImpl applicationPageSetup = (IDispatchImpl) automation.getProperty("PageSetup").getPdispVal();
            applicationPageSetup.setAutoDelete(false);
            appAutomationPageSetup = new Automation(applicationPageSetup);
        } catch (ComException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

//        need to set the paper size before setting the orientation

        try {
            Variant varPaperSize = appAutomationPageSetup.getProperty("PaperSize");
            varPaperSize.getIntVal().setValue(Integer.parseInt(paperSize));
            appAutomationPageSetup.setProperty("PaperSize", varPaperSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Variant varWdOrientation = appAutomationPageSetup.getProperty("Orientation");
            varWdOrientation.getIntVal().setValue(Integer.parseInt(orientation));
            appAutomationPageSetup.setProperty("Orientation", varWdOrientation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varLeftMargin = appAutomationPageSetup.getProperty("LeftMargin");
            varLeftMargin.getFltVal().setValue(Double.parseDouble(leftMargin) * (100 / 1.39));
            appAutomationPageSetup.setProperty("LeftMargin", varLeftMargin);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varRightMargin = appAutomationPageSetup.getProperty("RightMargin");
            varRightMargin.getFltVal().setValue(Double.parseDouble(rightMargin) * (100 / 1.39));
            appAutomationPageSetup.setProperty("RightMargin", varRightMargin);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varTopMargin = appAutomationPageSetup.getProperty("TopMargin");
            varTopMargin.getFltVal().setValue(Double.parseDouble(topMargin) * (100 / 1.39));
            appAutomationPageSetup.setProperty("TopMargin", varTopMargin);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varBottomMargin = appAutomationPageSetup.getProperty("BottomMargin");
            varBottomMargin.getFltVal().setValue(Double.parseDouble(bottomMargin) * (100 / 1.39));
            appAutomationPageSetup.setProperty("BottomMargin", varBottomMargin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeToTable() {
        IDispatchImpl document = new IDispatchImpl(_container.getOleObject());
        document.setAutoDelete(false);
        Automation automation = new Automation(document);

        IDispatchImpl application3 = (IDispatchImpl) automation.getProperty("Content").getPdispVal();
        application3.setAutoDelete(false);
        Automation appAutomation3 = new Automation(application3);

        Variant var = appAutomation3.getProperty("Text");
        var.getBstrVal().setValue(buffer.toString());
        appAutomation3.setProperty("Text", var);

        Automation appAutomationTable = null;
        try {
            IDispatchImpl applicationTable = (IDispatchImpl) appAutomation3.invoke("ConvertToTable", new Object[]{2, wdSeparateByCommas}).getPdispVal();
            applicationTable.setAutoDelete(false);
            appAutomationTable = new Automation(applicationTable);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationWholeRange = null;
        try {
            IDispatchImpl applicationWholeRange = (IDispatchImpl) appAutomationTable.getProperty("Range").getPdispVal();
            applicationWholeRange.setAutoDelete(false);
            appAutomationWholeRange = new Automation(applicationWholeRange);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationCells = null;
        try {
            IDispatchImpl applicationCells = (IDispatchImpl) appAutomationWholeRange.getProperty("Cells").getPdispVal();
            applicationCells.setAutoDelete(false);
            appAutomationCells = new Automation(applicationCells);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varVerticalAlign = appAutomationCells.getProperty("VerticalAlignment");
            varVerticalAlign.getIntVal().setValue(1);
            appAutomationCells.setProperty("VerticalAlignment", varVerticalAlign);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationRows = null;
        try {
            IDispatchImpl applicationRows = (IDispatchImpl) appAutomationTable.getProperty("Rows").getPdispVal();
            applicationRows.setAutoDelete(false);
            appAutomationRows = new Automation(applicationRows);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationRow = null;
        try {
            IDispatchImpl applicationRow = (IDispatchImpl) appAutomationRows.invoke("Item", 1).getPdispVal();
            applicationRow.setAutoDelete(false);
            appAutomationRow = new Automation(applicationRow);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationRange = null;
        try {
            IDispatchImpl applicationRange = (IDispatchImpl) appAutomationRow.getProperty("Range").getPdispVal();
            applicationRange.setAutoDelete(false);
            appAutomationRange = new Automation(applicationRange);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationShading = null;
        try {
            IDispatchImpl applicationShading = (IDispatchImpl) appAutomationRange.getProperty("Shading").getPdispVal();
            applicationShading.setAutoDelete(false);
            appAutomationShading = new Automation(applicationShading);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varBackColor = appAutomationShading.getProperty("BackgroundPatternColor");
            varBackColor.getIntVal().setValue(15987699);
            appAutomationShading.setProperty("BackgroundPatternColor", varBackColor);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationFont = null;
        try {
            IDispatchImpl applicationFont = (IDispatchImpl) appAutomationRange.getProperty("Font").getPdispVal();
            applicationFont.setAutoDelete(false);
            appAutomationFont = new Automation(applicationFont);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varBold = appAutomationFont.getProperty("Bold");
            varBold.getBoolVal().setBooleanValue(true);
            appAutomationFont.setProperty("Bold", varBold);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationParaFormat = null;
        try {
            IDispatchImpl applicationParaFormat = (IDispatchImpl) appAutomationRange.getProperty("ParagraphFormat").getPdispVal();
            applicationParaFormat.setAutoDelete(false);
            appAutomationParaFormat = new Automation(applicationParaFormat);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varHorAllignment = appAutomationParaFormat.getProperty("Alignment");
            varHorAllignment.getIntVal().setValue(wdHorizontalLineAlignCenter);
            appAutomationParaFormat.setProperty("Alignment", varHorAllignment);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Automation appAutomationBorders = null;
        try {
            IDispatchImpl applicationBorders = (IDispatchImpl) appAutomationTable.getProperty("Borders").getPdispVal();
            applicationBorders.setAutoDelete(false);
            appAutomationBorders = new Automation(applicationBorders);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varOutLine = appAutomationBorders.getProperty("OutsideLineStyle");
            varOutLine.getIntVal().setValue(wdLineStyleSingle);
            appAutomationBorders.setProperty("OutsideLineStyle", varOutLine);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Variant varInLine = appAutomationBorders.getProperty("InsideLineStyle");
            varInLine.getIntVal().setValue(wdLineStyleSingle);
            appAutomationBorders.setProperty("InsideLineStyle", varInLine);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSaveFile(String filepath) {
        IDispatchImpl document = new IDispatchImpl(_container.getOleObject());
        document.setAutoDelete(false);
        Automation automation = new Automation(document);
        try {
            automation.invoke("SaveAs", new Object[]{filepath});
        } catch (Exception e) {
            e.printStackTrace();
        }
        _container.destroyObject();
    }

    public void setText(String text) {
        try {
            OleMessageLoop.invokeMethod(this, "setWordData", new Object[]{text});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setWordData(String text) {
        IDispatchImpl document = new IDispatchImpl(_container.getOleObject());
        document.setAutoDelete(false);
        Automation automation = new Automation(document);
        IDispatchImpl application3 = (IDispatchImpl) automation.getProperty("Content").getPdispVal();
        application3.setAutoDelete(false);
        Automation appAutomation3 = new Automation(application3);
        Variant var = appAutomation3.getProperty("Text");
        var.getBstrVal().setValue(text + "\n");
        appAutomation3.setProperty("Text", var);
    }

    public void appendText(String text) {
        try {
            OleMessageLoop.invokeMethod(this, "setAppendData", new Object[]{text});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setAppendData(String text) {
        IDispatchImpl document = new IDispatchImpl(_container.getOleObject());
        document.setAutoDelete(false);
        Automation automation = new Automation(document);
        IDispatchImpl application3 = (IDispatchImpl) automation.getProperty("Content").getPdispVal();
        application3.setAutoDelete(false);
        Automation appAutomation3 = new Automation(application3);
        try {
            appAutomation3.invoke("InsertAfter", new Object[]{text + "\n"});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
