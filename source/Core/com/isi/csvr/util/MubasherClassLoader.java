package com.isi.csvr.util;

import com.isi.csvr.shared.Settings;

import java.io.File;
import java.io.FileInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 29, 2007
 * Time: 7:45:00 PM
 */
public class MubasherClassLoader extends ClassLoader {

    private ClassLoader parent;

    public MubasherClassLoader() {
        super();
    }

    public MubasherClassLoader(ClassLoader parent) {
        super(parent);
        this.parent = parent;
    }

    public Class<?> findClass(String name) throws ClassNotFoundException {
        System.out.println("Boo " + name);
        return super.findClass(name);
//        byte[] b = loadClassData(name);
//        return defineClass(name, b, 0, b.length);
    }


    private byte[] loadClassData(String name) {
        try {
            name = name.replaceAll("\\.", "/");
            System.out.println("Loading class at.. " + Settings.getAbsolutepath() + "formula/" + name + ".class");
            File file = new File(Settings.getAbsolutepath() + "formula/" + name + ".class");
            byte[] data = new byte[(int) file.length()];
            FileInputStream in = new FileInputStream(file);
            for (int i = 0; i < data.length; i++) {
                data[i] = (byte) in.read();
            }
            in.close();
            in = null;
            try {
                file.delete();
            } catch (Exception e) {
            }
            return data;
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return null;
    }
}
