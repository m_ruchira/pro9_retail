package com.isi.csvr.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 22, 2005
 * Time: 10:19:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class TestCol<E> implements Collection<E> {

    ArrayList<E> data;

    public TestCol() {
        data = new ArrayList<E>();
    }

    public boolean add(E o) {

        return data.add(o);
    }

    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    public void clear() {

    }

    public boolean contains(Object o) {
        return data.contains(o);
    }

    public boolean containsAll(Collection<?> c) {
        return false;
    }

    public boolean isEmpty() {
        return data.isEmpty();
    }

    public Iterator<E> iterator() {
        return data.iterator();
    }

    public boolean remove(Object o) {
        return false;
    }

    public boolean removeAll(Collection<?> c) {
        return data.removeAll(c);
    }

    public boolean retainAll(Collection<?> c) {
        return data.retainAll(c);
    }

    public int size() {
        return data.size();
    }

    public Object[] toArray() {
        return data.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return data.toArray(a);
    }
}
