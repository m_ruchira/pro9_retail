package com.isi.csvr.util;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.shared.ValueFormatter;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 14, 2009
 * Time: 1:42:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class PageSetup extends JDialog implements ActionListener, Themeable {

    public static final int STATUS_OK = 1;
    public static final int STATUS_CANCEL = 0;
    private int status = STATUS_CANCEL;
    public static String selectedOrientation = WordWriter.wdOrientPortrait;
    public static String selectedPaperSize = WordWriter.wdPaperA4;
    public static String selectedLeftMargin = "1";
    public static String selectedRightMargin = "1";
    public static String selectedTopMargin = "1";
    public static String selectedBottomMargin = "1";
    private Hashtable pageSizes = new Hashtable(0);
    private JComboBox paperSizes;
    private JRadioButton portraitRadio;
    private JRadioButton landscapeRadio;
    private JTextField leftTxt;
    private JTextField rightTxt;
    private JTextField topTxt;
    private JTextField bottomTxt;
    private TWButton okBtn;
    private TWButton cancelBtn;

    public PageSetup(JDialog parent) {
        super(parent, true);
        setPageSizes();
        createWindow();
    }

    public PageSetup(JFrame parent) {
        super(parent, true);
        setPageSizes();
        createWindow();
    }

    private void createWindow() {
        this.setTitle(Language.getString("PAGE_SETUP"));
        this.setSize(328, 225);
        this.setLocation(100, 100);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "32"}));

        JPanel setupPanel = new JPanel();
//        setupPanel.setBackground(Color.white);
        setupPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"45%", "55%"}, 5, 5));

        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"20%", "80%"}, new String[]{"100%"}, 0, 8));
        TitledBorder paperBorder = new TitledBorder(Language.getString("PAPER"));
//        paperBorder.setTitleColor(Color.blue);
        paperBorder.setTitleFont(new Font("Ariel", Font.PLAIN, 12));
        topPanel.setBorder(paperBorder);
//        topPanel.setBackground(Color.white);

        Vector pageNames = new Vector();
        Enumeration en = pageSizes.keys();
        while (en.hasMoreElements()) {
            String page = (String) en.nextElement();
            pageNames.add(page);
        }
        paperSizes = new JComboBox(pageNames);

//        paperSizes.setBackground(Color.white);
//        paperSizes.setForeground(Color.BLACK);
        topPanel.add(new JLabel(Language.getString("SIZE")));
        topPanel.add(paperSizes);

        JPanel bottomPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "60%"}, new String[]{"100%"}));
        JPanel orientationPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"50%", "50%"}));
        TitledBorder orientationBorder = new TitledBorder(Language.getString("ORIENTATION"));
//        orientationBorder.setTitleColor(Color.blue);
        orientationBorder.setTitleFont(new Font("Ariel", Font.PLAIN, 12));
        orientationPanel.setBorder(orientationBorder);
//        orientationPanel.setBackground(Color.white);
        ButtonGroup orientations = new ButtonGroup();
        portraitRadio = new JRadioButton(Language.getString("PORTRAIT"));
//        portraitRadio.setBackground(Color.white);
        portraitRadio.setSelected(true);
        landscapeRadio = new JRadioButton(Language.getString("LANDSCAPE"));
//        landscapeRadio.setBackground(Color.white);
        orientations.add(portraitRadio);
        orientations.add(landscapeRadio);
        orientationPanel.add(portraitRadio);
        orientationPanel.add(landscapeRadio);

        JPanel marginPanel = new JPanel(new FlexGridLayout(new String[]{"25", "40", "100%", "40", "40"}, new String[]{"50%", "50%"}, 2, 2));
        TitledBorder marginBorder = new TitledBorder(Language.getString("MARGINS"));
        marginBorder.setTitleColor(Color.blue);
        marginBorder.setTitleFont(new Font("Ariel", Font.PLAIN, 12));
        marginPanel.setBorder(marginBorder);
//        marginPanel.setBackground(Color.white);
//        SpinnerModel numberSpinnerLeft = new SpinnerNumberModel(1, -22, 22, 0.1);
//        SpinnerModel numberSpinnerRight = new SpinnerNumberModel(1, -22, 22, 0.1);
//        SpinnerModel numberSpinnerTop = new SpinnerNumberModel(1, -22, 22, 0.1);
//        SpinnerModel numberSpinnerBottom = new SpinnerNumberModel(1, -22, 22, 0.1);
        leftTxt = new JTextField("1");
        leftTxt.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        leftTxt.setText("1");
        rightTxt = new JTextField("1");
        rightTxt.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        rightTxt.setText("1");
        topTxt = new JTextField("1");
        topTxt.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        topTxt.setText("1");
        bottomTxt = new JTextField("1");
        bottomTxt.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        bottomTxt.setText("1");
        marginPanel.add(new JLabel(Language.getString("LEFT_MARGIN")));
        marginPanel.add(leftTxt);
        marginPanel.add(new JLabel());
        marginPanel.add(new JLabel(Language.getString("RIGHT_MARGIN")));
        marginPanel.add(rightTxt);
        marginPanel.add(new JLabel(Language.getString("TOP_MARGIN")));
        marginPanel.add(topTxt);
        marginPanel.add(new JLabel());
        marginPanel.add(new JLabel(Language.getString("BOTTOM_MARGIN")));
        marginPanel.add(bottomTxt);

        bottomPanel.add(orientationPanel);
        bottomPanel.add(marginPanel);

        setupPanel.add(topPanel);
        setupPanel.add(bottomPanel);

        JPanel buttonPanel = new JPanel(new FlexGridLayout(new String[]{"100%", "80", "80"}, new String[]{"100%"}, 5, 5));
        okBtn = new TWButton(Language.getString("OK"));
        okBtn.addActionListener(this);
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        cancelBtn.addActionListener(this);
        buttonPanel.add(new JLabel());
        buttonPanel.add(okBtn);
        buttonPanel.add(cancelBtn);

        this.add(setupPanel);
        this.add(buttonPanel);
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
    }

    private void setPageSizes() {
        try {
            pageSizes.put("Letter", WordWriter.wdPaperLetter);
            pageSizes.put("Tabloid", WordWriter.wdPaperTabloid);
            pageSizes.put("Legal", WordWriter.wdPaperLegal);
            pageSizes.put("A3", WordWriter.wdPaperA3);
            pageSizes.put("A4", WordWriter.wdPaperA4);
            pageSizes.put("A5", WordWriter.wdPaperA5);
            pageSizes.put("B4", WordWriter.wdPaperB4);
            pageSizes.put("B5", WordWriter.wdPaperB5);
        } catch (Exception e) {
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == okBtn) {
            try {
                if (landscapeRadio.isSelected()) {
                    selectedOrientation = WordWriter.wdOrientLandscape;
                } else {
                    selectedOrientation = WordWriter.wdOrientPortrait;
                }
                selectedPaperSize = (String) pageSizes.get(paperSizes.getSelectedItem());

                DecimalFormat twoDForm = new DecimalFormat("#.##");
                if ((Double.parseDouble(leftTxt.getText()) > 22) || (Double.parseDouble(leftTxt.getText()) < -22)) {
                    JOptionPane.showMessageDialog(this, Language.getString("MARGIN_ERROR_MSG1"));
                    return;
                } else {
                    selectedLeftMargin = twoDForm.format(Double.parseDouble(leftTxt.getText()));
                }
                if ((Double.parseDouble(rightTxt.getText()) > 22) || (Double.parseDouble(rightTxt.getText()) < -22)) {
                    JOptionPane.showMessageDialog(this, Language.getString("MARGIN_ERROR_MSG1"));
                    return;
                } else {
                    selectedRightMargin = twoDForm.format(Double.parseDouble(rightTxt.getText()));
                }
                if ((Double.parseDouble(topTxt.getText()) > 22) || (Double.parseDouble(topTxt.getText()) < -22)) {
                    JOptionPane.showMessageDialog(this, Language.getString("MARGIN_ERROR_MSG1"));
                    return;
                } else {
                    selectedTopMargin = twoDForm.format(Double.parseDouble(topTxt.getText()));
                }
                if ((Double.parseDouble(bottomTxt.getText()) > 22) || (Double.parseDouble(bottomTxt.getText()) < -22)) {
                    JOptionPane.showMessageDialog(this, Language.getString("MARGIN_ERROR_MSG1"));
                    return;
                } else {
                    selectedBottomMargin = twoDForm.format(Double.parseDouble(bottomTxt.getText()));
                }
                status = STATUS_OK;
            } catch (NumberFormatException e2) {
                JOptionPane.showMessageDialog(this, Language.getString("MARGIN_ERROR_MSG1"));
                return;
            } catch (Exception e1) {
                status = STATUS_CANCEL;
            }
            this.dispose();
        } else if (e.getSource() == cancelBtn) {
            status = STATUS_CANCEL;
            this.dispose();
        }
    }

    public int showDialog() {
        this.show();
        return status;
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(getContentPane());
    }
}
