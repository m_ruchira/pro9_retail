/*
 * (swing1.1beta3)
 *
 */

package com.isi.csvr.util;


import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableHeaderUI;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.*;
import java.util.List;


/**
 * @author BeOnTime
 */
public class GroupableTableHeaderUI extends BasicTableHeaderUI {

    private int headerHeight;

    public GroupableTableHeaderUI() {
        super();
        headerHeight = 0;
    }

    public static ComponentUI createUI(JComponent c) {
        return new GroupableTableHeaderUI();
    }

    public void paint(Graphics g, JComponent c) {
        boolean ltr = header.getComponentOrientation().isLeftToRight();
        TableColumnModel cm = header.getColumnModel();
        if (cm.getColumnCount() > 0) {
            Rectangle clip = g.getClipBounds();
            Point left = new Point(0, 0);// clip.getLocation();
            Point right = new Point(clip.x + clip.width - 1, clip.y);
            int cMin = header.columnAtPoint(ltr ? left : right);
            int cMax = header.columnAtPoint(ltr ? right : left);
            // This should never happen.
            if (cMin == -1) {
                cMin = 0;
            }
            // If the table does not have enough columns to fill the view we'll get -1.
            // Replace this with the index of the last column.
            if (cMax == -1) {
                cMax = cm.getColumnCount() - 1;
            }

            int columnWidth;
            Rectangle cellRect = header.getHeaderRect(ltr ? cMin : cMax);

            Set done = new HashSet();
            if (ltr) {
                for (int column = cMin; column != -1 && column <= cMax; column++) {
                    TableColumn aColumn = cm.getColumn(column);
                    columnWidth = aColumn.getWidth();
                    cellRect.width = columnWidth;
                    paintCell(done, g, cellRect, column);
                    cellRect.x += columnWidth;
                }
            } else {
                for (int column = cMax; column >= cMin; column--) {
                    TableColumn aColumn = cm.getColumn(column);
                    columnWidth = aColumn.getWidth();
                    cellRect.width = columnWidth;
                    paintCell(done, g, cellRect, column);
                    cellRect.x += columnWidth;
                }
            }
            // Remove all components in the rendererPane.
            rendererPane.removeAll();
        }
    }

    /**
     * Paints a column header, plus any (unpainted) group headers that it is a member of
     *
     * @param done        set of group headers already painted
     * @param g           graphic context
     * @param cellRect    bounds to paint header
     * @param columnIndex column index
     */
    private void paintCell(Set done, Graphics g, Rectangle cellRect, int columnIndex) {
        GroupableTableHeader groups = (GroupableTableHeader) header;
        int depth = groups.getDepth();
        int remainingHeaderHeight = getHeaderHeight();

        // get list of headers at this column index

        int totalHeight = 0;
        List headers = groups.getHeaders(columnIndex);
        for (int i = 0; i < headers.size(); i++) {
            Object heading = headers.get(i);
            if (heading instanceof ColumnGroup) {
                ColumnGroup group = (ColumnGroup) heading;

                Component component = getGroupedHeaderRenderer(group);
                int height = component.getPreferredSize().height;

                if (!done.contains(group)) {
                    // group header has not been painted so paint it
                    int width = group.getColumnSpan();
                    rendererPane.paintComponent(g, component, header, cellRect.x,
                            cellRect.y + (totalHeight), width,
                            height, true);
                    totalHeight += height;

                    // and flag it as painted

                    done.add(group);
                } else {
                    totalHeight += height;
                }
            } else {
                // Paint TableColumn header
                Component component = getHeaderRenderer(columnIndex);
                rendererPane.paintComponent(g, component, header, cellRect.x,
                        cellRect.y + totalHeight, cellRect.width,
                        remainingHeaderHeight - totalHeight, true);
            }
        }
    }

    private int viewIndexForColumn(TableColumn aColumn) {
        TableColumnModel cm = header.getColumnModel();
        for (int column = 0; column < cm.getColumnCount(); column++) {
            if (cm.getColumn(column) == aColumn) {
                return column;
            }
        }
        return -1;
    }

    private int getHeaderHeight() {

        if (headerHeight > 0) return headerHeight;
        int height = 0;
        try {
            TableColumnModel columnModel = header.getColumnModel();
            for (int column = 0; column < columnModel.getColumnCount(); column++) {
                TableColumn aColumn = columnModel.getColumn(column);
                TableCellRenderer renderer = header.getDefaultRenderer();
                Component comp = renderer.getTableCellRendererComponent(header.getTable(), aColumn.getHeaderValue(), false, false, -1, column);
                int cHeight = comp.getPreferredSize().height;
                Iterator groups = ((GroupableTableHeader) header).getHeaders(column).iterator();
                if (groups != null) {
                    while (groups.hasNext()) {
                        Object obj = groups.next();
                        if (obj instanceof ColumnGroup) {
                            cHeight += getGroupedHeaderRenderer((ColumnGroup) obj).getPreferredSize().height;
                        } else { // table Column
                            cHeight += getHeaderRenderer(column).getHeight();
                        }
                    }
                }
                height = Math.max(height, cHeight);
            }
            headerHeight = height;
            return height;
        } catch (Exception e) {
            e.printStackTrace();
            return 150;
        }
    }

    private Dimension createHeaderSize(long width) {
        TableColumnModel columnModel = header.getColumnModel();
        if (width > Integer.MAX_VALUE) {
            width = Integer.MAX_VALUE;
        }
        return new Dimension((int) width, getHeaderHeight());
    }

    /*
      * (non-Javadoc)
      *
      * @see javax.swing.plaf.ComponentUI#getPreferredSize(javax.swing.JComponent)
      */
    public Dimension getPreferredSize(JComponent c) {
        long width = 0;
        Enumeration enumeration = header.getColumnModel().getColumns();
        while (enumeration.hasMoreElements()) {
            TableColumn aColumn = (TableColumn) enumeration.nextElement();
            width = width + aColumn.getPreferredWidth();
        }
        return createHeaderSize(width);
    }

    private Component getHeaderRenderer(int colindex) {
        TableColumn column = header.getColumnModel().getColumn(colindex);
        TableCellRenderer renderer = column.getHeaderRenderer();
        if (renderer == null) {
            renderer = header.getDefaultRenderer();
        }
        return renderer.getTableCellRendererComponent(header.getTable(), column
                .getHeaderValue(), false, false, -1, colindex);
    }

    private Component getGroupedHeaderRenderer(ColumnGroup group) {
        TableCellRenderer renderer = header.getDefaultRenderer();
        return renderer.getTableCellRendererComponent(header.getTable(),
                group.getName(), false, false, -1, -1);
    }
}