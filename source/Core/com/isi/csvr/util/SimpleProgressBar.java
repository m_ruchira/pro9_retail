package com.isi.csvr.util;

import javax.swing.*;
import java.awt.*;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 24, 2003
 * Time: 11:15:51 AM
 * To change this template use Options | File Templates.
 */
public class SimpleProgressBar extends JLabel {

    private int maximim = 0;
    private int value = 0;
    private Color progressColor;

    public void paint(Graphics g) {
        super.paint(g);
        g.setXORMode(getBackground());
        g.setColor(progressColor);
        if (maximim > 0) {
            //setOpaque(true);
            g.fillRect(0, 0, getWidth() * value / maximim, getHeight());
        } else {
            //setOpaque(false);
        }
    }

    public void setMaximum(int n) {
        maximim = n;
    }

    public void setValue(int n) {
        value = n;
        repaint();
    }

    public void setProgressColor(Color color) {
        progressColor = color;
    }

    public void setBackgroundColor(Color color) {
        //setOpaque(true);
        setBackground(color);
    }
}