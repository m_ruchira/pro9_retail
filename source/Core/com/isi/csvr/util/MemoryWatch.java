package com.isi.csvr.util;

// Copyright (c) 2000 server

import com.isi.csvr.FrameAnalyser;
import com.isi.csvr.communication.ReceiveQFactory;
import com.isi.csvr.communication.udp.DataListener;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWFont;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DecimalFormat;

/**
 * Shows a window with the Heap size and the available heap %.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class MemoryWatch extends JDialog implements WindowListener, Runnable {
    private GridLayout gridLayout = new GridLayout();
    private JLabel lblHeapLabel = new JLabel();
    private JLabel lblHeapValue = new JLabel();
    private JLabel lblUtilization = new JLabel();
    private JLabel lblUtilizationValue = new JLabel();
    private JLabel lblThreads = new JLabel();
    private JLabel lblThreadsValue = new JLabel();
    private JLabel lblFrames = new JLabel();
    private JLabel lblFramesValue = new JLabel();
    private JLabel lblAnalisedFrames = new JLabel();
    private JLabel lblAnalisedFramesValue = new JLabel();
    private JLabel lblRequestCount = new JLabel();
    private JLabel lblRequestCountValue = new JLabel();
    private MemoryBar memoryBar;


    private double heap = 0;
    //private double  urilization = 0;
    private boolean running = false;
//    private static MemoryWatch self = null;

    private Thread thread;
    private DecimalFormat heapFormat = new DecimalFormat("###,##0.00 'M'");
    private DecimalFormat utilFormat = new DecimalFormat("##0.00 %");
    private DecimalFormat threadFormat = new DecimalFormat("###,##0");

    /**
     * Constructs a new instance.
     */
    public MemoryWatch(JFrame parent) {
        super(parent);
        addWindowListener(this);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        try {
            jbInit();
            //if (canShowGUI()){
            thread = new Thread(this, "TW Memory Watch");
            setRunning(true);
            thread.start();
            this.show();
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public static MemoryWatch getSharedInstance() {
        if (self == null) {
            self = new MemoryWatch(Client.getInstance().getFrame());
        }
        return self;
    }*/

    public void activate() {
        if (!isRunning()) {
            thread = new Thread(this, "TW Memory Watch");
            setRunning(true);
            thread.start();

        }
        this.show();
    }

    private synchronized boolean isRunning() {
        return this.running;
    }

    private synchronized void setRunning(boolean status) {
        this.running = status;
    }

    /**
     * Checks if the envr variable to show the GUI is set.
     * returns true if the varible is set
     */
    private boolean canShowGUI() {
        String value = System.getProperty("MemoryWatch");
        if (value == null)
            return false;
        else
            return true;
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {

        this.getContentPane().setLayout(new BorderLayout(0, 0));
        JPanel table = new JPanel(gridLayout);
        this.setSize(new Dimension(128, 200));
//        this.setSize(new Dimension(128, 105));
        this.setTitle("Memory Watch");
        this.setResizable(false);
        gridLayout.setRows(6);
        gridLayout.setColumns(2);
        setBackground(Color.black);
        lblHeapLabel.setForeground(Color.yellow);
        lblHeapLabel.setFont(new TWFont("Dialog", 1, 10));
        lblHeapLabel.setOpaque(true);
        lblHeapLabel.setBackground(Color.black);
        lblHeapLabel.setText(" Heap Size");
        lblHeapValue.setForeground(Color.yellow);
        lblHeapValue.setFont(new TWFont("Dialog", 1, 10));
        lblHeapValue.setOpaque(true);
        lblHeapValue.setBackground(Color.black);
        lblHeapValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblHeapValue.setText("0");
        lblUtilization.setForeground(Color.yellow);
        lblUtilization.setOpaque(true);
        lblUtilization.setFont(new TWFont("Dialog", 1, 10));
        lblUtilization.setBackground(Color.black);
        lblUtilization.setText(" Utilization");
        lblUtilizationValue.setForeground(Color.yellow);
        lblUtilizationValue.setOpaque(true);
        lblUtilizationValue.setFont(new TWFont("Dialog", 1, 10));
        lblUtilizationValue.setBackground(Color.black);
        lblUtilizationValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblUtilizationValue.setText("0 %");

        lblThreads.setForeground(Color.yellow);
        lblThreads.setOpaque(true);
        lblThreads.setFont(new TWFont("Dialog", 1, 10));
        lblThreads.setBackground(Color.black);
        lblThreads.setText(" Threads");
        lblThreadsValue.setForeground(Color.yellow);
        lblThreadsValue.setOpaque(true);
        lblThreadsValue.setFont(new TWFont("Dialog", 1, 10));
        lblThreadsValue.setBackground(Color.black);
        lblThreadsValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblThreadsValue.setText("0");

        lblFrames.setForeground(Color.yellow);
        lblFrames.setOpaque(true);
        lblFrames.setFont(new TWFont("Dialog", 1, 10));
        lblFrames.setBackground(Color.black);
        lblFrames.setText(" Frames");
        lblFramesValue.setForeground(Color.yellow);
        lblFramesValue.setOpaque(true);
        lblFramesValue.setFont(new TWFont("Dialog", 1, 10));
        lblFramesValue.setBackground(Color.black);
        lblFramesValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblFramesValue.setText("0");

        lblAnalisedFrames.setForeground(Color.yellow);
        lblAnalisedFrames.setOpaque(true);
        lblAnalisedFrames.setFont(new TWFont("Dialog", 1, 10));
        lblAnalisedFrames.setBackground(Color.black);
        lblAnalisedFrames.setText(" Analysed");
        lblAnalisedFramesValue.setForeground(Color.yellow);
        lblAnalisedFramesValue.setOpaque(true);
        lblAnalisedFramesValue.setFont(new TWFont("Dialog", 1, 10));
        lblAnalisedFramesValue.setBackground(Color.black);
        lblAnalisedFramesValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblAnalisedFramesValue.setText("0");

        lblRequestCount.setForeground(Color.yellow);
        lblRequestCount.setOpaque(true);
        lblRequestCount.setFont(new TWFont("Dialog", 1, 10));
        lblRequestCount.setBackground(Color.black);
        lblRequestCount.setText(" Stock Req.");
        lblRequestCountValue.setForeground(Color.yellow);
        lblRequestCountValue.setOpaque(true);
        lblRequestCountValue.setFont(new TWFont("Dialog", 1, 10));
        lblRequestCountValue.setBackground(Color.black);
        lblRequestCountValue.setHorizontalAlignment(SwingConstants.RIGHT);
        lblRequestCountValue.setText("0");

        this.setBackground(SystemColor.control);
        table.add(lblHeapLabel, null);
        table.add(lblHeapValue, null);
        table.add(lblUtilization, null);
        table.add(lblUtilizationValue, null);
        table.add(lblThreads, null);
        table.add(lblThreadsValue, null);
        table.add(lblFrames, null);
        table.add(lblFramesValue, null);
        table.add(lblAnalisedFrames, null);
        table.add(lblAnalisedFramesValue, null);
        table.add(lblRequestCount, null);
        table.add(lblRequestCountValue, null);
        memoryBar = new MemoryBar();
        memoryBar.setPreferredSize(new Dimension(120, 7));
        this.getContentPane().add(table, BorderLayout.CENTER);
        this.getContentPane().add(memoryBar, BorderLayout.SOUTH);

        System.out.println("***************************************");
        System.out.println("* Memory Watch: By Uditha Nagahawatta *");
        System.out.println("***************************************");

        String title = System.getProperty("MemoryWatch.Title", "MemoryWatch");

        if ((title != null) && (!title.trim().equals("")))
            this.setTitle(title);
    }

    /**
     * Update the GUI each 1/2 sec
     */
    public void run() {
        double total = 0;
        double newUtilization = 0;
        double free;
        long max;

        while (isRunning()) {
            max = Runtime.getRuntime().maxMemory();
            total = Runtime.getRuntime().totalMemory();
            free = Runtime.getRuntime().freeMemory();
            newUtilization = (total - free) / total;

            if (heap != total) {
                lblHeapValue.setBackground(Color.yellow);
                lblHeapValue.setForeground(Color.black);
            } else {
                lblHeapValue.setBackground(Color.black);
                lblHeapValue.setForeground(Color.yellow);
            }
            lblHeapValue.setText(heapFormat.format(total / 1048576L));
            lblUtilizationValue.setText(utilFormat.format(newUtilization));
            lblThreadsValue.setText(threadFormat.format(Thread.activeCount()));
            memoryBar.update(max, (long) total, (long) free);
            heap = total;
            if (Settings.isTCPMode()) {
                try {
                    lblFramesValue.setText("" + ReceiveQFactory.getReceiveQueue().getFrameCount());
                } catch (Exception e) {
                }
            } else {
                try {
                    lblFramesValue.setText("" + DataListener.frameCount);
                } catch (Exception e) {
                }
            }
            try {
                lblAnalisedFramesValue.setText("" + FrameAnalyser.frameCount);
            } catch (Exception e) {
            }
            try {
                lblRequestCountValue.setText("" + DataStore.getSharedInstance().requestCount());
            } catch (Exception e) {
            }

            sleepMe();
        }
    }

    /**
     * Sleep the thread
     */
    private void sleepMe() {
        try {
            thread.sleep(1000);
        } catch (Exception e) {
            // ignore
        }
    }

    /**
     * Invoked the first time a window is made visible.
     */
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Invoked when the user attempts to close the window
     * from the window's system menu.  If the program does not
     * explicitly hide or dispose the window while processing
     * this event, the window close operation will be cancelled.
     */
    public void windowClosing(WindowEvent e) {
        setRunning(false);
    }

    /**
     * Invoked when a window has been closed as the result
     * of calling dispose on the window.
     */
    public void windowClosed(WindowEvent e) {

    }

    /**
     * Invoked when a window is changed from a normal to a
     * minimized state. For many platforms, a minimized window
     * is displayed as the icon specified in the window's
     * iconImage property.
     *
     * @see java.awt.Frame#setIconImage
     */
    public void windowIconified(WindowEvent e) {

    }

    /**
     * Invoked when a window is changed from a minimized
     * to a normal state.
     */
    public void windowDeiconified(WindowEvent e) {

    }

    /**
     * Invoked when the Window is set to be the active Window. Only a Frame or
     * a Dialog can be the active Window. The native windowing system may
     * denote the active Window or its children with special decorations, such
     * as a highlighted title bar. The active Window is always either the
     * focused Window, or the first Frame or Dialog that is an owner of the
     * focused Window.
     */
    public void windowActivated(WindowEvent e) {

    }

    /**
     * Invoked when a Window is no longer the active Window. Only a Frame or a
     * Dialog can be the active Window. The native windowing system may denote
     * the active Window or its children with special decorations, such as a
     * highlighted title bar. The active Window is always either the focused
     * Window, or the first Frame or Dialog that is an owner of the focused
     * Window.
     */
    public void windowDeactivated(WindowEvent e) {

    }
}
