package com.isi.csvr.util;

import java.io.Serializable;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 22, 2005
 * Time: 10:28:50 AM
 * To change this template use File | Settings | File Templates.
 */
public class TWList<E> implements Collection<E>, List<E>, Serializable {

    public static final int ARRAY = 0;
    public static final int LIST = 1;

    private int type;
    private ArrayList<E> aList;
    private LinkedList<E> lList;

    public TWList(int type) {
        this.type = type;

        if (type == ARRAY) {
            aList = new ArrayList<E>();
        } else {
            lList = new LinkedList<E>();
        }
    }

    /* -- Collection Methods --*/
    public boolean add(E o) {
        if (type == ARRAY) {
            return aList.add(o);
        } else {
            return lList.add(o);
        }
    }

    public boolean addAll(Collection<? extends E> c) {
        if (type == ARRAY) {
            return aList.addAll(c);
        } else {
            return lList.addAll(c);
        }
    }

    public void clear() {
        if (type == ARRAY) {
            aList.clear();
        } else {
            lList.clear();
        }
    }

    public boolean contains(Object o) {
        if (type == ARRAY) {
            return aList.contains(o);
        } else {
            return lList.contains(o);
        }
    }

    public boolean containsAll(Collection<?> c) {
        if (type == ARRAY) {
            return aList.containsAll(c);
        } else {
            return lList.containsAll(c);
        }
    }

    public boolean isEmpty() {
        if (type == ARRAY) {
            return aList.isEmpty();
        } else {
            return lList.isEmpty();
        }
    }

    public Iterator<E> iterator() {
        if (type == ARRAY) {
            return aList.iterator();
        } else {
            return lList.iterator();
        }
    }

    public boolean remove(Object o) {
        if (type == ARRAY) {
            return aList.remove(o);
        } else {
            return lList.remove(o);
        }
    }

    public boolean removeAll(Collection<?> c) {
        if (type == ARRAY) {
            return aList.removeAll(c);
        } else {
            return lList.removeAll(c);
        }
    }

    public boolean retainAll(Collection<?> c) {
        if (type == ARRAY) {
            return aList.retainAll(c);
        } else {
            return lList.retainAll(c);
        }
    }

    public int size() {
        if (type == ARRAY) {
            return aList.size();
        } else {
            return lList.size();
        }
    }

    public Object[] toArray() {
        if (type == ARRAY) {
            return aList.toArray();
        } else {
            return lList.toArray();
        }
    }

    public <T> T[] toArray(T[] a) {
        if (type == ARRAY) {
            return aList.toArray(a);
        } else {
            return lList.toArray(a);
        }
    }

    /*-- List Methods --*/

    public boolean addAll(int index, Collection<? extends E> c) {
        if (type == ARRAY) {
            return aList.addAll(index, c);
        } else {
            return lList.addAll(index, c);
        }
    }

    public int indexOf(Object o) {
        if (type == ARRAY) {
            return aList.indexOf(o);
        } else {
            return lList.indexOf(o);
        }
    }

    public int lastIndexOf(Object o) {
        if (type == ARRAY) {
            return aList.lastIndexOf(o);
        } else {
            return lList.lastIndexOf(o);
        }
    }

    public ListIterator<E> listIterator() {
        if (type == ARRAY) {
            return aList.listIterator();
        } else {
            return lList.listIterator();
        }
    }

    public ListIterator<E> listIterator(int index) {
        if (type == ARRAY) {
            return aList.listIterator(index);
        } else {
            return lList.listIterator(index);
        }
    }

    public E remove(int index) {
        if (type == ARRAY) {
            return aList.remove(index);
        } else {
            return lList.remove(index);
        }
    }

    public E set(int index, E element) {
        if (type == ARRAY) {
            return aList.set(index, element);
        } else {
            return lList.set(index, element);
        }
    }

    public List<E> subList(int fromIndex, int toIndex) {
        if (type == ARRAY) {
            return aList.subList(fromIndex, toIndex);
        } else {
            return lList.subList(fromIndex, toIndex);
        }
    }

    /*-- Common Methods --*/
    public E get(int index) {
        if (type == ARRAY) {
            return aList.get(index);
        } else {
            return lList.get(index);
        }
    }

    public void add(int index, E element) {
        if (type == ARRAY) {
            aList.add(index, element);
        } else {
            lList.add(index, element);
        }
    }

    public E removeFirst() {
        if (type == ARRAY) {
            return aList.remove(0);
        } else {
            return lList.removeFirst();
        }
    }
}
