package com.isi.csvr.util;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Nov 20, 2008
 * Time: 5:10:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class TWSpinnerNumberModel extends SpinnerNumberModel {
    private Number stepSize, value;
    private Comparable minimum, maximum;
    private int decimalPlaces = -1;

    public TWSpinnerNumberModel(Number value, Comparable minimum, Comparable maximum, Number stepSize) {
        super(value, minimum, maximum, stepSize);
        this.value = value;
        this.minimum = minimum;
        this.maximum = maximum;
        this.stepSize = stepSize;
    }

    public TWSpinnerNumberModel(int value, int minimum, int maximum, int stepSize) {
        this(new Integer(value), new Integer(minimum), new Integer(maximum), new Integer(stepSize));
    }

    public TWSpinnerNumberModel(long value, long minimum, long maximum, long stepSize) {
        this(new Long(value), new Long(minimum), new Long(maximum), new Long(stepSize));
    }

    public TWSpinnerNumberModel(double value, double minimum, double maximum, double stepSize) {
        this(new Double(value), new Double(minimum), new Double(maximum), new Double(stepSize));
    }

    public TWSpinnerNumberModel() {
        this(new Integer(0), null, null, new Integer(1));
    }

    public void setMinimum(Comparable minimum) {
        this.minimum = minimum;
        super.setMinimum(minimum);
    }

    public void setMaximum(Comparable maximum) {
        this.maximum = maximum;
        super.setMaximum(maximum);
    }

    public void setStepSize(Number stepSize) {
        this.stepSize = stepSize;
        super.setStepSize(stepSize);
    }

    public Object getNextValue() {
        return incrValue(+1);
    }

    public Object getPreviousValue() {
        return incrValue(-1);
    }

    public Number getNumber() {
        return value;
    }

    public Object getValue() {
        /*if (decimalPlaces == -1) {
            return value;
		} else {
			String data = SharedMethods.formatToDecimalPlacesNumeric(decimalPlaces, value);
			if(value instanceof Float){
				return Float.parseFloat(data);
			} else if(value instanceof Double){
				return Double.parseDouble(data);
			} else if (value instanceof Long) {
				return Long.parseLong(data);
			} else {
				return Integer.parseInt(data);
			}
		}*/
        return value;
    }

    public void setValue(Object value) {
        this.value = (Number) value;
        super.setValue(value);
    }

    public void setDecimalPlaces(int decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

    private Number incrValue(int dir) {
        Number newValue;
        if ((value instanceof Float) || (value instanceof Double)) {
            long lValue = Math.round(value.doubleValue() * 1000);
            long lStepSize = Math.round(stepSize.doubleValue() * 1000);
            long v = lValue + lStepSize * dir;
            long diff = v % lStepSize;
//			double v = (Math.round(value.doubleValue()*1000)/1000) + ((Math.round(stepSize.doubleValue()*1000)/1000) * (double)dir);
//			double diff = v % (Math.round(stepSize.doubleValue()*1000)/1000);
            if (diff != 0) {
                if (dir > 0) {
                    v = v - diff;
                } else {
                    v = v + lStepSize - diff;
                }
            }
            double v2 = v / 1000D;
            if (value instanceof Double) {
                newValue = new Double(v2);
            } else {
                newValue = new Float(v2);
            }
        } else {
            long v = value.longValue() + (stepSize.longValue() * (long) dir);

            long diff = v % stepSize.longValue();
            if (diff != 0) {
                if (dir > 0) {
                    v = v - diff; //- (stepSize.doubleValue() * (double)dir);
                } else {
                    v = v + stepSize.longValue() - diff; //- (stepSize.doubleValue() * (double)dir);
                }
            }
            if (value instanceof Long) {
                newValue = new Long(v);
            } else if (value instanceof Integer) {
                newValue = new Integer((int) v);
            } else if (value instanceof Short) {
                newValue = new Short((short) v);
            } else {
                newValue = new Byte((byte) v);
            }
        }

//		return newValue;

        try {
            if ((maximum != null) && (maximum.compareTo(newValue) < 0)) {
                return null;
            } else if ((minimum != null) && (minimum.compareTo(newValue) > 0)) {
                return null;
            } else {
                return newValue;
            }
        } catch (Exception e) {
//			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return newValue;
        }
    }
}
