/*
 * (C) Copyright 2009-2010 Direct FN Technologies Limited. All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * Direct FN Technologies and constitute a TRADE SECRET of Direct FN Technologies Limited.
 *
 * Direct FN Technologies Limited retains all title to and intellectual property rights
 * in these materials.
 */
package com.isi.csvr.util;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * com.isi.csvr.util.SplashTest
 */
public class SplashTest extends Frame implements ActionListener {
    public SplashTest() {
        super("SplashScreen demo");
        setSize(500, 300);
        setLayout(new BorderLayout());
        Menu m1 = new Menu("File");
        MenuItem mi1 = new MenuItem("Exit");
        m1.add(mi1);
        mi1.addActionListener(this);

        MenuBar mb = new MenuBar();
        setMenuBar(mb);
        mb.add(m1);
        SplashScreen splash = SplashScreen.getSplashScreen();
        if (splash == null) {
            System.out.println("SplashScreen.getSplashScreen() returned null");
            return;
        }
        Graphics2D g = (Graphics2D) splash.createGraphics();
        if (g == null) {
            System.out.println("g is null");
            return;
        }
        for (int i = 0; i < 100; i++) {
            renderSplashFrame(g, i);
            splash.update();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
            /*if(i==50){
                splash.close();
                splash = SplashScreen.getSplashScreen();
                g = (Graphics2D)splash.createGraphics();
                System.out.println("new splash created");
            }*/
        }
        splash.close();
        setVisible(true);
        toFront();
    }

    static void renderSplashFrame(Graphics2D g, int frame) {
        final String[] comps = {"foo", "bar", "baz"};
        g.setComposite(AlphaComposite.Clear);
//        g.fillRect(0,0,200,60);
        g.setPaintMode();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, (frame * 10) % 200, 20);
        g.setColor(Color.RED);
        g.drawString("Loading " + comps[(frame / 5) % 3] + "...", 10, 50);
        if (frame == 50) {
//            spl
        }
    }

    public static void main(String args[]) {
        SplashTest test = new SplashTest();
    }

    public void actionPerformed(ActionEvent ae) {
        System.exit(0);
    }
}
