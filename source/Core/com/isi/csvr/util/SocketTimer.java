package com.isi.csvr.util;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 25, 2003
 * Time: 4:23:28 PM
 * To change this template use Options | File Templates.
 */
public class SocketTimer implements ActionListener {
    private Timer timer;
    private Socket socket;

    public SocketTimer(Socket socket) {
        this.socket = socket;
        timer = new Timer(30000, this);
        timer.start();
    }

    public void deactivate() {
        try {
            timer.stop();
        } catch (Exception e) {

        }
        timer = null;
    }

    public void actionPerformed(ActionEvent e) {
        try {
            socket.close();
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
        try {
            timer.stop();
        } catch (Exception e1) {

        }
        timer = null;
        System.out.println("timer listner exit");
    }
}
