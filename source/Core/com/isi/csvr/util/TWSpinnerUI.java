package com.isi.csvr.util;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicSpinnerUI;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Nov 10, 2008
 * Time: 1:02:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class TWSpinnerUI extends BasicSpinnerUI {

    public static ComponentUI createUI(JComponent c) {
        return new TWSpinnerUI();
    }

    protected Component createPreviousButton() {
        Icon icon = new ImageIcon("images/Theme" + Theme.getID() + "/down.gif");
//		if(icon == null){
//			icon =  new ImageIcon("images/Theme" + Theme.getID() + "/show.gif");
//		}
        JButton b = new JButton(icon);
//		JButton b = new BasicArrowButton(SwingConstants.SOUTH);
        Border buttonBorder = UIManager.getBorder("Spinner.arrowButtonBorder");
        if (buttonBorder instanceof UIResource) {
            b.setBorder(new CompoundBorder(buttonBorder, null));
        } else {
            b.setBorder(buttonBorder);
        }
        b.setInheritsPopupMenu(true);
        b.setName("Spinner.previousButton");
        installPreviousButtonListeners(b);
        return b;
    }

    protected Component createNextButton() {
//		JButton b = new JButton(new ImageIcon("images/Theme"
//				+ Theme.getID() + "/up.gif"));
        Icon icon = new ImageIcon("images/Theme" + Theme.getID() + "/up.gif");
//		if(icon == null){
//			icon =  new ImageIcon("images/Theme" + Theme.getID() + "/hide.gif");
//		}
        JButton b = new JButton(icon);
//		JButton b = new BasicArrowButton(SwingConstants.NORTH);
        Border buttonBorder = UIManager.getBorder("Spinner.arrowButtonBorder");
        if (buttonBorder instanceof UIResource) {
            b.setBorder(new CompoundBorder(buttonBorder, null));
        } else {
            b.setBorder(buttonBorder);
        }
        b.setInheritsPopupMenu(true);
        b.setName("Spinner.nextButton");
        installNextButtonListeners(b);
        return b;
    }

}
