package com.isi.csvr.util;

class Element {
    int prefix;
    int suffix;

    public Element(int prefix, int suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }
}
