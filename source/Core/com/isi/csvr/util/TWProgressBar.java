package com.isi.csvr.util;

import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TWProgressBar extends JLabel {

    public final byte DISPLAY_TYPE_NONE = 0;
    public final byte DISPLAY_TYPE_RATIO = 1;
    public final byte DISPLAY_TYPE_PERCENT = 2;
    TWDecimalFormat pcntFormatter = new TWDecimalFormat("#,##0'%'");
    private float totalCount;
    private float currentCount;
    private Color beginColor;
    private Color fillColor;
    private Color endColor;
    //private GradientPaint shade;
    private byte displayType;

    public TWProgressBar() {
        displayType = DISPLAY_TYPE_PERCENT;
        totalCount = 1;
        currentCount = 0;
        //setBackground(Color.BLACK);
        setForeground(Color.WHITE);
        beginColor = new Color(0, 128, 0);
        endColor = new Color(200, 0, 0);
        setFillColor();
        //shade = new GradientPaint(0,0, beginColor, 100,0, endColor);
    }

    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
        //shade = new GradientPaint(0,0, beginColor, width,0, endColor);
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        //g.setColor(getBackground());
        //g.fillRect(0,0,getWidth(), getHeight());

        //g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        float percent = Math.min(currentCount / totalCount, 1);
        //g2.setPaint(shade);
        g2.setColor(fillColor);
        g2.fill(new Rectangle2D.Double(0, 0, percent * getWidth(), getHeight()));

        drawText(g);
    }

    private void drawText(Graphics g) {
        if (displayType == DISPLAY_TYPE_NONE) return;
        String text = getDisplayText();
        //g.setColor(getForeground());
        g.setXORMode(getForeground());
        int x = (getWidth() - g.getFontMetrics().stringWidth(text)) / 2;
        int y = getHeight() / 2 + getFont().getSize() / 2;
        g.setFont(Theme.getDefaultFont(Font.BOLD, 12));
        g.drawString(text, x, y);
    }

    public String getDisplayText() {
        return getDisplayText(displayType);
    }

    public String getDisplayText(byte displayType) {
        switch (displayType) {
            case DISPLAY_TYPE_NONE:
                return "";
            case DISPLAY_TYPE_RATIO:
                return "" + Math.round(currentCount) + "/" + Math.round(totalCount);
            case DISPLAY_TYPE_PERCENT:
            default:
                int percent = Math.round(100 * Math.min(currentCount / totalCount, 1));
                return pcntFormatter.format(percent);
        }
    }

    public Color getBeginColor() {
        return beginColor;
    }

    //beginColor
    public void setBeginColor(Color beginColor) {
        this.beginColor = beginColor;
        //shade = new GradientPaint(0,0, beginColor, getWidth(),0, endColor);
        setFillColor();
        repaint();
    }

    public Color getEndColor() {
        return endColor;
    }

    //endColor
    public void setEndColor(Color endColor) {
        this.endColor = endColor;
        //shade = new GradientPaint(0,0, beginColor, getWidth(),0, endColor);
        setFillColor();
        repaint();
    }

    public float getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(float totalCount) {
        if (totalCount <= 0) {
            totalCount = 1;
        }
        this.totalCount = totalCount;
        repaint();
    }

    public float getCurrentCount() {
        setFillColor();
        return currentCount;
    }

    public void setCurrentCount(float currentCount) {
        this.currentCount = currentCount;
        setFillColor();
        repaint();
    }

    public byte getDisplayType() {
        return displayType;
    }

    public void setDisplayType(byte displayType) {
        this.displayType = displayType;
        repaint();
    }

    public Color setFillColor() {
        int r1, r2, g1, g2, b1, b2;
        r1 = beginColor.getRed();
        g1 = beginColor.getGreen();
        b1 = beginColor.getBlue();
        r2 = endColor.getRed();
        g2 = endColor.getGreen();
        b2 = endColor.getBlue();
        float percent = Math.min(currentCount / totalCount, 1);
        r1 = Math.round(r1 * (1f - percent) + r2 * percent);
        g1 = Math.round(g1 * (1f - percent) + g2 * percent);
        b1 = Math.round(b1 * (1f - percent) + b2 * percent);
        fillColor = new Color(r1, g1, b1);
        return fillColor;
    }
}