/*
 * (C) Copyright 2009-2010 Direct FN Technologies Limited. All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * Direct FN Technologies and constitute a TRADE SECRET of Direct FN Technologies Limited.
 *
 * Direct FN Technologies Limited retains all title to and intellectual property rights
 * in these materials.
 */
package com.isi.csvr.util;

import com.isi.csvr.shared.Settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * com.isi.csvr.util.TWFileWriter
 */
public class TWFileWriter {

    String sFilePathRealTime = null;
    File fileRT = null;
    RandomAccessFile raFileRT = null;
    FileChannel rwChannelRT = null;
    MappedByteBuffer wrBufRT = null;

    public TWFileWriter(String filename) {
        sFilePathRealTime = Settings.getAbsolutepath() + "/System/" + filename;
        try {
            fileRT = new File(sFilePathRealTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (fileRT.exists()) {
                fileRT.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            raFileRT = new RandomAccessFile(fileRT, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            rwChannelRT = raFileRT.getChannel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            wrBufRT = raFileRT.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 10);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeData(String record) {
        try {
            StringBuilder sBuff = new StringBuilder();
            sBuff.append(record.trim());
            sBuff.append("\n");
            try {
                raFileRT.writeBytes(sBuff.toString());
            } catch (Exception e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeFile() {
        try {
            rwChannelRT.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            raFileRT.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
