package com.isi.csvr.util;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Decompress {

    final static int MAX_CODES = 4096;
    final static int BYTE_SIZE = 8;
    final static int EXCESS = 4;
    final static int ALPHA = 256;
    final static int MASK = 15;

    private int[] s;
    private int size;
    private Element[] h;
    private int leftOver;
    private boolean bitsLeftOver;
    private FileInputStream in;
    private ByteArrayOutputStream out;

    public ByteArrayOutputStream setFiles(String filename) throws IOException {
        in = new FileInputStream(filename);
//        in = new BufferedInputStream(new FileInputStream(filename));
//        in = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "US-ASCII"));
        out = new ByteArrayOutputStream();
        //PipedInputStream pin = new PipedInputStream(out);
        return out;
    }

    private void output(int code) throws IOException {
        size = -1;
        while (code >= ALPHA) {
            s[++size] = h[code].suffix;
            code = h[code].prefix;
        }
        s[++size] = code;
        for (int i = size; i >= 0; i--)
            out.write(s[i]);
    }

    private int getCode() throws IOException {
        int c = in.read();
        if (c == -1) return -1;

        int code;
        if (bitsLeftOver)
            code = (leftOver << BYTE_SIZE) + c;
        else {
            int d = in.read();
            code = (c << EXCESS) + (d >> EXCESS);
            leftOver = d & MASK;
        }
        bitsLeftOver ^= true;
//        bitsLeftOver = !bitsLeftOver;
        return code;
    }

    public void decompress() throws IOException {
        int codeUsed = ALPHA;
        s = new int[MAX_CODES];
        h = new Element[MAX_CODES];

        int pcode = getCode(), ccode;
        if (pcode >= 0) {
            s[0] = pcode;
            out.write(s[0]);
            size = 0;

            do {
                ccode = getCode();
                if (ccode < 0) break;
                if (ccode < codeUsed) {
                    output(ccode);
                    if (codeUsed < MAX_CODES)
                        h[codeUsed++] = new Element(pcode, s[size]);
                } else {
                    h[codeUsed++] = new Element(pcode, s[size]);
                    output(ccode);
                }
                pcode = ccode;
            } while (true);
        }
        out.close();
        in.close();
        out = null;
        in = null;
    }
}
