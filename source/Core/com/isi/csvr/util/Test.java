package com.isi.csvr.util;

import com.jniwrapper.win32.automation.Automation;
import com.jniwrapper.win32.automation.OleContainer;
import com.jniwrapper.win32.automation.OleMessageLoop;
import com.jniwrapper.win32.automation.impl.IDispatchImpl;
import com.jniwrapper.win32.automation.types.BStr;
import com.jniwrapper.win32.automation.types.Variant;
import com.mubasher.operations.OfficeFileOperationsHandler;
import com.mubasher.operations.OfficePrintHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


//import word.word._Application;
//import word.word.Application;
//import word.word._Document;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 22, 2005
 * Time: 10:22:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class Test extends JFrame {

   /* public static void main(String[] args) {
        *//*TestCol<String> testCol = new TestCol<String>();
        testCol.add("Alo");
        testCol.add("boll");
        for (String s : testCol){
            System.out.println(s);
        }*//*
//        int randomNo = 2034;
        String username = "DilumJa123";
        System.out.println("11="+System.currentTimeMillis());
        for (int randomNo = 1000; randomNo < 9999; randomNo++) {
            String data = SharedMethods.getAuthMD5String(username,randomNo);
            String mD5 = MD5.getHashString(username);
//            System.out.println("MD5 string =="+mD5);
            boolean status = SharedMethods.checkAuthMD5String(data,randomNo, mD5);
//            System.out.println("status =="+status);
        }
        System.out.println("12="+System.currentTimeMillis());
//        byte[] mD5b = MD5.getHash(username);
//        System.out.println("MD% byte[]=="+new String(mD5b));
//        status = SharedMethods.checkAuthMD5String(data,randomNo, new String(mD5b));
//        System.out.println("status =="+status);
    }*/

    private static final Dimension WINDOW_SIZE = new Dimension(720, 480);
    /**
     * progid of word document
     */
    private static final String DOCUMENT_PROGID = "Word.Document";
    private OleContainer _container;

    public Test() {
        super("JNIWrapper - Word Automation");

        _container = new OleContainer();
        _container.createObject(DOCUMENT_PROGID);

        getContentPane().add(_container, BorderLayout.CENTER);

        // Enable open / save operations
        _container.setFileOperationsHandler(new OfficeFileOperationsHandler(OfficeFileOperationsHandler.TYPE_WORD));

        // Enable print / print preview
        _container.setPrintDocumentHandler(new OfficePrintHandler());
    }

    public static void main(String[] args) {
        WordWriter writer = new WordWriter();
//        writer.startDocument();
        writer.setText("This is the first line");
        writer.appendText("This is the second line");
        writer.appendText("This is the third line");
        writer.appendText("<HTML><BODY>This is the fourth line</BODY></HTML>");
        writer.appendText("1 \u064a\u0648\u0645");
        writer.saveFile("C:/WordWriter1.doc");
        ImageIcon icon = new ImageIcon();
    }

    private static void createGUI() {
        final Test app = new Test();
        app.setSize(WINDOW_SIZE);
        app.setLocationRelativeTo(null);
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        app.addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                // show word
//                app._container.doVerb(OleVerbs.HIDE);

                // work with word through automation
                try {
                    OleMessageLoop.invokeMethod(app, "modifyDocument", new Object[]{});
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            public void windowClosing(WindowEvent e) {
                // close document on exit
                app._container.destroyObject();
            }
        });
//        app.setVisible(true);
    }

    public void modifyDocument() {
        // get word document from container object
        IDispatchImpl document = new IDispatchImpl(_container.getOleObject());
        document.setAutoDelete(false);

        try {
            Automation automation = new Automation(document);

            // get word application
            IDispatchImpl application = (IDispatchImpl) automation.getProperty("Application").getPdispVal();
            application.setAutoDelete(false);

            try {
                Automation appAutomation = new Automation(application);

                // print version of word application
                String version = appAutomation.getProperty("Version").getBstrVal().getValue();
                appAutomation.setProperty("Text", new BStr("This is an test message"));
                System.out.println("version = " + version);

                // get word application
//                IDispatchImpl application2 = (IDispatchImpl) automation.getProperty("Range").getPdispVal();
//                Automation appAutomation2 = new Automation(application2);
//                appAutomation2.setProperty("Text", new BStr("this is an test message"));

                IDispatchImpl application3 = (IDispatchImpl) automation.getProperty("Content").getPdispVal();
                Automation appAutomation3 = new Automation(application3);
                Variant var = appAutomation3.getProperty("Text");
                var.getBstrVal().setValue("this is an test messagsdsdfe" + "\u0654" + "\n");
                appAutomation3.setProperty("Text", var);
//                Variant var2=  appAutomation3.getProperty("Text");
//                var2.getBstrVal().setValue("<html><Body>test string</Body></Html>");
//                appAutomation3.setProperty("Text", var2);
                try {
                    appAutomation3.invoke("InsertAfter", new Object[]{"asdfsfsddffsdf"});
                    automation.invoke("SaveAs", new Object[]{"C:\\test123.doc"});
//                    OleMessageLoop.invokeMethod(document, "Content.InsertAfter", new Object[]{"asdfsfsddffsdf"});
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } finally {
                application.release();
            }
        } finally {
            document.release();
        }
    }

   /* public static void main(String[] args)
    {
        // initialize OLE
        OleFunctions.oleInitialize();

        createGUI();
    }*/


}
