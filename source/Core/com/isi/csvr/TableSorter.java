package com.isi.csvr;

import com.isi.csvr.portfolio.VAluationModelAll;
import com.isi.csvr.portfolio.ValuationModel;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.CompanyCodeSorter;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Date;
import java.util.Vector;

public class TableSorter
        extends TableMap
        implements TableModelListener {

    private int indexes[] = new int[0];
    private Vector sortingColumns = new Vector();
    private boolean ascending = true;
    private JTable table = null;
    private TableModel model;

    private byte windowType;

    public TableSorter() {
    }

    public TableSorter(TableModel model, byte type) {
        setModel(model, type);
        this.model = model;
    }

    public void setModel(TableModel model, byte type) {
        super.setModel(model);
        this.model = model;
        windowType = type;
        reallocateIndexes();
        sortByColumn(0);
        fireTableDataChanged();
    }

    public void setTable(JTable table) {
        this.table = table;
    }


    public TableModel getModel() {
        return model;
    }

    public int compareRowsByColumn(int row1, int row2, int column) {
        Class type = model.getColumnClass(column);
        TableModel data = model;

        // Check for nulls

        Object o1 = data.getValueAt(row1, column);
        Object o2 = data.getValueAt(row2, column);

        // If both values are null return 0
        if (o1 == null && o2 == null) {
            return 0;
        } else if (o1 == null) { // Define null less than everything.
            return -1;
        } else if (o2 == null) {
            return 1;
        }

        if (type.equals(Number[].class)) {
            //Number n1 = ((double[])data.getValueAt(row1, column))[0];
            double d1 = ((float[]) data.getValueAt(row1, column))[0];
            //Number n2 = ((Number[])data.getValueAt(row2, column))[0];
            double d2 = ((float[]) data.getValueAt(row2, column))[0];

            if (d1 < d2)
                return -1;
            else if (d1 > d2)
                return 1;
            else
                return 0;
        } else if (type.equals(Float.class)) {
            //Number n1 = ((double[])data.getValueAt(row1, column))[0];
            float d1 = ((Float) data.getValueAt(row1, column)).floatValue();
            //Number n2 = ((Number[])data.getValueAt(row2, column))[0];
            float d2 = ((Float) data.getValueAt(row2, column)).floatValue();

            if (d1 < d2)
                return -1;
            else if (d1 > d2)
                return 1;
            else
                return 0;
        } else if (type.equals(Double.class)) {
            //Number n1 = ((double[])data.getValueAt(row1, column))[0];
            double d1 = ((Double) data.getValueAt(row1, column)).doubleValue();
            //Number n2 = ((Number[])data.getValueAt(row2, column))[0];
            double d2 = ((Double) data.getValueAt(row2, column)).doubleValue();

            if ((Double.isNaN(d1) || Double.isInfinite(d1)) && (Double.isNaN(d2) || Double.isInfinite(d2))) {
                return 0;
            } else if ((Double.isNaN(d1) || Double.isInfinite(d1))) {
                if (ascending) {
                    return 1;
                } else {
                    return -1;
                }
                // return 1;
            } else if ((Double.isNaN(d2) || Double.isInfinite(d2))) {
                if (ascending) {
                    return -1;
                } else {
                    return 1;
                }
                //   return -1;
            }

            if (d1 < d2)
                return -1;
            else if (d1 > d2)
                return 1;
            else
                return 0;
        } else if (type.equals(Number.class)) {
            double d1 = Double.parseDouble((String) data.getValueAt(row1, column));
            double d2 = Double.parseDouble((String) data.getValueAt(row2, column));

            if (d1 < d2)
                return -1;
            else if (d1 > d2)
                return 1;
            else
                return 0;
        } else if (type.equals(Byte.class)) {
            byte d1 = ((Byte) data.getValueAt(row1, column)).byteValue();
            byte d2 = ((Byte) data.getValueAt(row2, column)).byteValue();

            if (d1 < d2)
                return -1;
            else if (d1 > d2)
                return 1;
            else
                return 0;
        } else if (type.equals(Long.class)) {
            //Number n1 = ((double[])data.getValueAt(row1, column))[0];
            long d1 = ((Long) data.getValueAt(row1, column)).longValue();
            //Number n2 = ((Number[])data.getValueAt(row2, column))[0];
            long d2 = ((Long) data.getValueAt(row2, column)).longValue();

            if (d1 < d2)
                return -1;
            else if (d1 > d2)
                return 1;
            else
                return 0;
        } else if (type.equals(String.class)) {
            String s1 = (String) data.getValueAt(row1, column);
            String s2 = (String) data.getValueAt(row2, column);
            int result = s1.compareTo(s2);

            if (result < 0)
                return -1;
            else if (result > 0)
                return 1;
            else
                return 0;
        } else if (type.equals(java.util.Date.class)) {
            Date d1 = (Date) data.getValueAt(row1, column);
            long n1 = d1.getTime();
            Date d2 = (Date) data.getValueAt(row2, column);
            long n2 = d2.getTime();

            if (n1 < n2)
                return -1;
            else if (n1 > n2)
                return 1;
            else
                return 0;
        } else if (type.equals(CompanyCodeSorter.class)) {
            String s1 = (String) o1;
            String s2 = (String) o2;
            double i2 = 0;
            double i1 = 0;
            boolean first = false;
            boolean second = false;
            try {
                i1 = Double.parseDouble(s1);
//                i1 = Integer.parseInt(s1);
                first = true;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                i2 = Double.parseDouble(s2);
//                i2 = Integer.parseInt(s2);
                second = true;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (first && second) {
                if (i1 < i2)
                    return -1;
                else if (i1 > i2)
                    return 1;
                else
                    return 0;
            } else if (first) {
                return -1;
            } else if (second) {
                return 1;
            } else {
                int result = s1.compareTo(s2);
                if (result < 0)
                    return -1;
                else if (result > 0)
                    return 1;
                else
                    return 0;
//                return s1.compareTo(s2);
            }
        } else if (type.equals(Boolean.class)) {
            Boolean bool1 = (Boolean) data.getValueAt(row1, column);
            boolean b1 = bool1.booleanValue();
            Boolean bool2 = (Boolean) data.getValueAt(row2, column);
            boolean b2 = bool2.booleanValue();

            if (b1 == b2)
                return 0;
            else if (b1) // Define false < true
                return 1;
            else
                return -1;
        } else {
            Object v1 = data.getValueAt(row1, column);
            String s1 = v1.toString();
            Object v2 = data.getValueAt(row2, column);
            String s2 = v2.toString();
            int result = s1.compareTo(s2);
            if (result < 0)
                return -1;
            else if (result > 0)
                return 1;
            else
                return 0;
        }
    }

    public int compare(int row1, int row2) {
        for (int level = 0, n = sortingColumns.size(); level < n; level++) {
            Integer column = (Integer) sortingColumns.elementAt(level);
            int result = compareRowsByColumn(row1, row2, column.intValue());
            if (result != 0) {
                return (ascending ? result : -result);
            }
        }
        return 0;
    }

    public void reallocateIndexes() {
        int rowCount = model.getRowCount();

        indexes = new int[rowCount];

        for (int row = 0; row < rowCount; row++) {
            indexes[row] = row;
        }
    }

    public void tableChanged(TableModelEvent tableModelEvent) {
        super.tableChanged(tableModelEvent);
        reallocateIndexes();
        sortByColumn(0);
        //fireTableStructureChanged();
    }

    public void checkModel() {
        //if ((isCustomType) && (indexes.length != (model.getRowCount()-1)))
        //    reallocateIndexes();
        //else
        if (indexes.length != model.getRowCount()) {
            reallocateIndexes();
        }
    }

    public void sort() {
        checkModel();
        switch (windowType) {
            case ViewSettingsManager.PORTFOLIO_TXN_HISTORY:
                shuttlesort((int[]) indexes.clone(), indexes, 0, indexes.length);
                break;
            case ViewSettingsManager.PORTFOLIO_VALUATION:
                if (model != null) {
                    if (model instanceof ValuationModel) {
                        if (((ValuationModel) model).isCashIncluded) {
                            shuttlesort((int[]) indexes.clone(), indexes, 0, indexes.length - 2);
                        } else {
                            shuttlesort((int[]) indexes.clone(), indexes, 0, indexes.length - 1);
                        }
                    } else if (model instanceof VAluationModelAll) {
                        if (((VAluationModelAll) model).isCashIncluded) {
                            shuttlesort((int[]) indexes.clone(), indexes, 0, indexes.length - 4);
                        } else {
                            shuttlesort((int[]) indexes.clone(), indexes, 0, indexes.length - 3);
                        }
                    }
                }
                break;
            default:
                shuttlesort((int[]) indexes.clone(), indexes, 0, indexes.length);
                break;
        }
//        if (isCustomType)
//            shuttlesort( (int[]) indexes.clone(), indexes, 0, indexes.length - 1);
//        else
//            shuttlesort( (int[]) indexes.clone(), indexes, 0, indexes.length);
        fireTableDataChanged();
    }

    public void shuttlesort(int from[], int to[], int low, int high) {
        if (high - low < 2) {
            return;
        }
        int middle = (low + high) / 2;
        shuttlesort(to, from, low, middle);
        shuttlesort(to, from, middle, high);

        int p = low;
        int q = middle;

        for (int i = low; i < high; i++) {
            if (q >= high || (p < middle && compare(from[p], from[q]) <= 0)) {
                to[i] = from[p++];
            } else {
                to[i] = from[q++];
            }
        }
    }

    private void swap(int first, int second) {
        int temp = indexes[first];
        indexes[first] = indexes[second];
        indexes[second] = temp;
    }

    public Object getValueAt(int row, int column) {
        checkModel();
        return model.getValueAt(indexes[row], column);
    }

    public void setValueAt(Object aValue, int row, int column) {
        checkModel();
        model.setValueAt(aValue, indexes[row], column);
    }

    public void sortByColumn(int column) {
        sortByColumn(column, true);
    }

    public int getUnsortedRowFor(int row) {
        return indexes[row];
    }

    public void sortByColumn(int column, boolean ascending) {
        this.ascending = ascending;
        sortingColumns.removeAllElements();
        sortingColumns.addElement(new Integer(column));
        sort();
        super.tableChanged(new TableModelEvent(this));
        /*   if (table != null)
        {
       TableColumnModel colmodel = table.getColumnModel();
        JLabel l = (JLabel)(colmodel.getColumn(column).getHeaderRenderer());
        }*/
    }

    public void unsort() {
        sortingColumns.removeAllElements();
        reallocateIndexes();
    }

}