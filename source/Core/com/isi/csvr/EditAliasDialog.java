package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Jan 3, 2007
 * Time: 6:25:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class EditAliasDialog extends JDialog implements KeyListener, ActionListener {

    Stock stock;
    TWTextField shortDesc;
    JButton enableAlias;
    //    JLabel lblEnableAlias;
    JPanel bottomPanel;

    public EditAliasDialog() {
        super(Client.getInstance().getFrame(), true);
        setTitle(Language.getString("EDIT_ALIAS"));

        stock = DataStore.getSharedInstance().getStockObject(Client.getInstance().getSelectedSymbol());
//        if(!Settings.isShowAlias()){
//            setSize(350,160);
//            setLayout(new FlexGridLayout(new String[]{"350"}, new String[]{"50","30","40"}));
//        }else{
//            setSize(350,120);
//            setLayout(new FlexGridLayout(new String[]{"350"}, new String[]{"50","30"}));
//        }

        setSize(350, 120);
        setLayout(new FlexGridLayout(new String[]{"350"}, new String[]{"50", "30"}));

        String[] inputwidths = {"100", "229"};
        String[] inputheights = {"20", "20"};
        JPanel panel = new JPanel(new FlexGridLayout(inputwidths, inputheights, 5, 5));
        panel.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        panel.registerKeyboardAction(this, "C", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);

        panel.add(new JLabel(Language.getString("SHORT_DESCRIPTION")));
        panel.add(new JLabel(stock.getOriginalShortDescription()));
        panel.add(new JLabel(Language.getString("ALIAS")));
        shortDesc = new TWTextField();
        shortDesc.setText(stock.getAlias());
        panel.add(shortDesc);
        this.addKeyListener(this);
        shortDesc.addKeyListener(this);

        JPanel btnPanel = new JPanel(new FlexGridLayout(new String[]{"100%", "75", "70", "75", "1"}, new String[]{"22"}, 5, 5));
        btnPanel.add(new JLabel(""));
        TWButton okBtn = new TWButton(Language.getString("OK"));
        okBtn.setPreferredSize(new Dimension(70, 20));
        okBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                oK_Action_Performed();
            }
        });

        okBtn.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    oK_Action_Performed();
            }
        });

        TWButton cancelBtn = new TWButton(Language.getString("CANCEL"));
        cancelBtn.setPreferredSize(new Dimension(75, 20));
        cancelBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancel_Action_Performed();

            }
        });
        cancelBtn.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    cancel_Action_Performed();
            }
        });

        TWButton resetBtn = new TWButton(Language.getString("RESET"));
        resetBtn.setPreferredSize(new Dimension(75, 20));
        resetBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                reset_Action_Performed();

            }
        });
        resetBtn.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    reset_Action_Performed();
            }
        });

        btnPanel.add(resetBtn);
        btnPanel.add(okBtn);
        btnPanel.add(cancelBtn);
        btnPanel.add(new JLabel(""));
        this.add(panel);
        this.add(btnPanel);

//        lblEnableAlias = new JLabel(Language.getString("ENABLE_ALIAS_MESSAGE"));
//        lblEnableAlias.setPreferredSize(new Dimension(280,40));
//        lblEnableAlias.setOpaque(true);
        enableAlias = new JButton(Language.getString("ENABLE_ALIAS_MESSAGE"));
        enableAlias.setPreferredSize(new Dimension(350, 40));
        enableAlias.setBorder(BorderFactory.createEmptyBorder());
        enableAlias.setContentAreaFilled(false);
        enableAlias.setOpaque(true);
        enableAlias.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.setShowAlias(true);
                enableAlias.setVisible(false);
                setSize(350, 120);
            }
        });
        enableAlias.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent e) {
                setCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            public void mouseExited(MouseEvent e) {
                setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });

        bottomPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "0", "50%"}, new String[]{"42"}));
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(enableAlias);
        bottomPanel.add(new JLabel(""));
//        if(!Settings.isShowAlias()){
//            this.add(bottomPanel);
//        }

        GUISettings.setLocationRelativeTo(this, Client.getInstance().getFrame());
        GUISettings.applyOrientation(this);
        setResizable(false);
        this.setVisible(true);
    }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent ev) {
        if (ev.getKeyChar() == '\n') {
            ev.consume();
            oK_Action_Performed();
        }
    }

    private void oK_Action_Performed() {
        String text = shortDesc.getText().trim();
        if (text.equals("")) {
            SharedMethods.showNonBlockingMessage(Language.getString("ALIAS_CANNOT_BE_EMPTY"), JOptionPane.ERROR_MESSAGE);
        } else {
            stock.setAlias(text);
            SymbolMaster.addAlias(stock.getKey(), text);
            SymbolMaster.saveSymbolAlias();
            UpdateNotifier.setShapshotUpdated();
            dispose();
        }
    }

    private void cancel_Action_Performed() {
        dispose();
    }

    private void reset_Action_Performed() {
        stock.setAlias("");
        SymbolMaster.removeAlias(stock.getKey());
        UpdateNotifier.setShapshotUpdated();
        dispose();
    }

    public void actionPerformed(ActionEvent e) {
    }
}
