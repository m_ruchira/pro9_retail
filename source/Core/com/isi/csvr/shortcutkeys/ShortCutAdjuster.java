package com.isi.csvr.shortcutkeys;

import com.isi.csvr.TWActions;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWButton;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jun 15, 2009
 * Time: 1:43:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShortCutAdjuster extends JPanel implements ActionListener {

    public static ShortCutAdjuster selfRef;
    public static JComponent parent;
    static ShortCutCombo[] shortcutComboList;
    static MouseGestureCombo[] mgComboList;
    public boolean isValid = false;
    JPanel mainPanel;
    JPanel comboBoxPanel;
    JPanel downPanel;
    private ArrayList<String> configurableFunctions = new ArrayList<String>();


    public ShortCutAdjuster() {
        super();
        this.parent = parent;
        Hashtable gettingHash;
        setOpaque(true);
        loadConfigurableFunctions();
        JLabel lblFunctions = new JLabel("" + Language.getString("FUNCTIONS"));

        lblFunctions.setFont(this.getFont().deriveFont(Font.BOLD, new Float(12)));

        JLabel lblShortCut = new JLabel(Language.getString("SHORTCUTS"));
        lblShortCut.setFont(this.getFont().deriveFont(Font.BOLD, new Float(12)));

        JLabel lblMouseGestures = new JLabel(Language.getString("MOUSE_GESTURES"));
        lblMouseGestures.setFont(this.getFont().deriveFont(Font.BOLD, new Float(12)));

        JPanel instructionPanel = new JPanel(new FlexGridLayout(new String[]{"36%", "32%", "32%"}, new String[]{"10"}, 5, 5));

        instructionPanel.add(lblFunctions);
        instructionPanel.add(lblShortCut);
        instructionPanel.add(lblMouseGestures);

        int fileLength = configurableFunctions.size();
        String[][] height = new String[20][fileLength];

        comboBoxPanel = new JPanel(new GridLayout(0, 1));
//        mainPanel = new JPanel(new FlexGridLayout(new String[]{"60%", "40%"}, new String[]{String.valueOf(20 * fileLength)}, 20, 5));


        downPanel = new JPanel(new FlexGridLayout(new String[]{"68%", "16%", "16%"}, new String[]{"20"}, 5, 5));

        TWButton saveBtn = new TWButton(Language.getString("SAVE"));
        saveBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setData();
            }
        });

        TWButton cancelBtn = new TWButton(Language.getString("CANCEL"));
        cancelBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                resetComboData();
                ShortCutViewer.getInstance().dispose();
            }
        });


        downPanel.add(new JLabel(""));
        downPanel.add(saveBtn);
        downPanel.add(cancelBtn);


        shortcutComboList = new ShortCutCombo[fileLength];
        mgComboList = new MouseGestureCombo[fileLength];

        int comboIndex = 0;

        Enumeration shortcuts = TWActions.keyMappingTable.keys();
        Enumeration mousegestures = TWActions.mgMappingsTable.keys();

        Enumeration functions = Collections.enumeration(configurableFunctions);
        while (functions.hasMoreElements()) {
            String function = (String) functions.nextElement();
            mainPanel = new JPanel();
            mainPanel.setPreferredSize(new Dimension(445, 30));
            mainPanel.setLayout(new FlexGridLayout(new String[]{"36%", "32%", "32%"}, new String[]{"20"}, 5, 5, true, true));
            String langPrefix = "SC_" + function;
            String name = Language.getString(langPrefix);
            mainPanel.add(new JLabel(name));

            shortcutComboList[comboIndex] = new ShortCutCombo(function, name);
            if (TWActions.keyMappingTable.containsKey(function)) {
                String shortcut = TWActions.keyMappingTable.get(function);
                shortcutComboList[comboIndex].addItem(shortcut.replace('_', ' '));
            }
            shortcutComboList[comboIndex].addItem(Language.getString("NONE"));
            shortcutComboList[comboIndex].addItem(Language.getString("DEFINE_SHORT_CUT"));
            mainPanel.add(shortcutComboList[comboIndex]);

            mgComboList[comboIndex] = new MouseGestureCombo(function, name);
            if (TWActions.mgMappingsTable.containsKey(function)) {
                String mouseGesture = TWActions.mgMappingsTable.get(function);
                mgComboList[comboIndex].addItem(mouseGesture);
            }
            mgComboList[comboIndex].addItem(Language.getString("NONE"));
            mgComboList[comboIndex].addItem(Language.getString("DEFINE_MOUSE_GESTURE"));
            mainPanel.add(mgComboList[comboIndex]);
            comboBoxPanel.add(mainPanel);
            comboIndex++;

        }
/*        while (shortcuts.hasMoreElements() || mousegestures.hasMoreElements()) {
            String tag = (String) shortcuts.nextElement();
            String shortKey = TWActions.keyMappingTable.get(tag);

//            String mgtag = (String) mousegestures.nextElement();
//            String gesture = TWActions.mgMappingsTable.get(mgtag);

            mainPanel = new JPanel(new FlexGridLayout(new String[]{"60%", "40%"}, new String[]{"20"}, 20, 5));
            String langPrefix = "SC_" + tag;
            String name = Language.getString(langPrefix);
            mainPanel.add(new JLabel(name));
//            mainPanel.add(new JLabel(tag));

            shortcutComboList[comboIndex] = new ShortCutCombo(tag, name, parent);

            *//*   shortcutComboList[comboIndex].addActionListener(this);*//*
            if (shortKey.equals("")) {
                shortcutComboList[comboIndex].addItem("");
            } else {
                shortcutComboList[comboIndex].addItem(shortKey);
            }
            shortcutComboList[comboIndex].addItem(Language.getString("NONE"));
            shortcutComboList[comboIndex].addItem(Language.getString("DEFINE_SHORT_CUT"));
            mainPanel.add(shortcutComboList[comboIndex]);
            comboBoxPanel.add(mainPanel);
            comboIndex++;
        }*/

//        mainPanel.setBackground(Color.white);
//        JScrollPane scroller = new JScrollPane(mainPanel);
        JScrollPane scroller = new JScrollPane(comboBoxPanel);
        GUISettings.applyOrientation(comboBoxPanel);
        scroller.setPreferredSize(new Dimension(100, 100));
        scroller.setBorder(BorderFactory.createEmptyBorder());
//        setLayout(new BorderLayout());
        setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"7%", "1%", "84%", "8%"}));
        add(instructionPanel/*, BorderLayout.NORTH*/);
        add(new JSeparator());
        add(scroller);
        add(downPanel/*, BorderLayout.SOUTH*/);
        selfRef = this;
        GUISettings.applyOrientation(this);

    }

    public static ShortCutAdjuster getInstance() {
        if (selfRef == null) {
            selfRef = new ShortCutAdjuster();
        }
        return selfRef;
    }

    public void resetComboData() {

        Enumeration functions = Collections.enumeration(configurableFunctions);
        int comboIndex = 0;
        while (functions.hasMoreElements()) {
            String function = (String) functions.nextElement();

            shortcutComboList[comboIndex].removeAllItems();
            if (TWActions.keyMappingTable.containsKey(function)) {
                String shortcut = TWActions.keyMappingTable.get(function);
                shortcutComboList[comboIndex].addItem(shortcut.replace('_', ' '));
            }
            shortcutComboList[comboIndex].addItem(Language.getString("NONE"));
            shortcutComboList[comboIndex].addItem(Language.getString("DEFINE_SHORT_CUT"));


            mgComboList[comboIndex].removeAllItems();
            if (TWActions.mgMappingsTable.containsKey(function)) {
                String mouseGesture = TWActions.mgMappingsTable.get(function);
                mgComboList[comboIndex].addItem(mouseGesture);
            }
            mgComboList[comboIndex].addItem(Language.getString("NONE"));
            mgComboList[comboIndex].addItem(Language.getString("DEFINE_MOUSE_GESTURE"));


            comboIndex++;

        }
    }

    public void setData() {
        boolean valid = true;
        Hashtable<String, String> checkingTable = new Hashtable<String, String>();
        for (int i = 0; i < shortcutComboList.length; i++) {
            String tag = shortcutComboList[i].getTag();
            String shortcut = (String) shortcutComboList[i].getSelectedItem();

            if (!shortcut.equalsIgnoreCase(Language.getString("NONE"))) {

                if (checkingTable.containsValue(shortcut)) {
                    valid = false;
                    JOptionPane.showMessageDialog(this, Language.getString("ERROR_KEY_ALREADY_ASSIGNED") + shortcut,
                            Language.getString("ERROR"), JOptionPane.OK_OPTION);
                    break;
                } else {
                    checkingTable.put(tag, shortcut);
                }
            }
        }

        for (int i = 0; i < mgComboList.length; i++) {
            String tag = mgComboList[i].getTag();
            String mousegesture = (String) mgComboList[i].getSelectedItem();

            if (!mousegesture.equalsIgnoreCase(Language.getString("NONE"))) {
                if (checkingTable.containsValue(mousegesture)) {
                    valid = false;
                    JOptionPane.showMessageDialog(this, Language.getString("ERROR_KEY_ALREADY_ASSIGNED") + mousegesture,
                            Language.getString("ERROR"), JOptionPane.OK_OPTION);
                    break;
                } else {
                    checkingTable.put(tag, mousegesture);
                }
            }
        }
        if (valid) {
            for (int i = 0; i < shortcutComboList.length; i++) {
                String tag = shortcutComboList[i].getTag();
                String shortCutKey = (String) shortcutComboList[i].getSelectedItem();
                if (!shortCutKey.equalsIgnoreCase(Language.getString("NONE"))) {
                    TWActions.keyMappingTable.put(tag, shortCutKey);
                } else {
                    TWActions.keyMappingTable.remove(tag);
                }

            }
            for (int i = 0; i < mgComboList.length; i++) {
                String tag = mgComboList[i].getTag();
                String mouseGesture = (String) mgComboList[i].getSelectedItem();
                if (!mouseGesture.equalsIgnoreCase(Language.getString("NONE"))) {
                    TWActions.mgMappingsTable.put(tag, mouseGesture);
                } else {
                    TWActions.mgMappingsTable.remove(tag);
                }
            }

            try {
                TWActions.getInstance().saveShortCuts();
                TWActions.getInstance().saveMouseGestures();
                ShortCutViewer currentInstance = ShortCutViewer.getInstance();
                resetComboData();
                currentInstance.dispose();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public void actionPerformed(ActionEvent e) {
        System.out.println("============");
    }

    public void checkingSameFunctions() {
        /*    comboBoxText = new Hashtable();
        System.out.println("in checkingSameFunctions");
        for (int s = 0; s < shortcutComboList.length; s++) {
            this.comboBoxText.put("function" + s, (String) shortcutComboList[s].getSelectedItem());
        }

        for (int r = 0; r < comboBoxListSpecial.length; r++) {
            this.comboBoxText.put("function" + (r + 60), (String) comboBoxListSpecial[r].getSelectedItem());
        }*/

    }

    public boolean checkingProgress(String controllers, String key) {

        if (key == null || key.equals("")) {
            isValid = false;
            JOptionPane.showMessageDialog(this, Language.getString("ERROR_EMPTY_KEY"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        } else if (TWActions.keyMappingTable.containsValue(controllers + key)) {
            isValid = false;
            JOptionPane.showMessageDialog(this, Language.getString("ERROR_KEY_ALREADY_ASSIGNED"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        } else {
            isValid = true;
        }
        /* if (TWActions.keyMappingTable.containsValue(textToCheck)) {
            isValid = false;
            JOptionPane.showMessageDialog(this, Language.getString("KEY_ERROR_MESSAGE"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);

        } else {
            this.isValid = true;
        }*/


        return isValid;
    }

    public void settingChoice(String test, ShortCutCombo box) {
//        checkingProgress(test);
        if (isValid) {
            box.removeAllItems();
            box.addItem(test.replace('_', ' '));
            box.addItem(Language.getString("NONE"));
            box.addItem(Language.getString("DEFINE_SHORT_CUT"));
//            TWActions.keyMappingTable.put(box.getTag(),test);
            try {
                TWActions.getInstance().saveShortCuts();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
//            System.out.println("qqqqqqqqqqqqqqqqqqq");
        }
//        checkingSameFunctions();

    }

    private void loadConfigurableFunctions() {
        try {

            BufferedReader reader = new BufferedReader(new FileReader(Settings.CONFIG_DATA_PATH + "/functions.dll"));
//          BufferedReader reader = new BufferedReader(new FileReader("C:/temp1.dll"));
            String str;
            String name = "";
            while ((str = reader.readLine()) != null) {
                configurableFunctions.add(str);
            }

        } catch (Exception e) {

        }
    }
}
