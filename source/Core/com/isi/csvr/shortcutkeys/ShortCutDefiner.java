package com.isi.csvr.shortcutkeys;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.table.TWTextField;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jun 15, 2009
 * Time: 3:47:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShortCutDefiner extends JDialog implements KeyListener {

    static TWTextField txtKey;
    static boolean isSaved = false;
    static String important;
    static boolean isCanceled;
    JPanel keyPanel;
    TWButton cancelButton;
    TWButton okButton;
    TWButton clearButton;
    //    JPanel leftPanel;
    //    JPanel rightPanel;
    JCheckBox shiftBox;
    JCheckBox ctrlBox;
    JCheckBox altBox;
    //    String selected = "";
    boolean isShift;
    boolean isAlt;
    boolean isCtrl;
    char[] charArray = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '/', '?', '+', '=', ';', ':', '<', ',', '|', '{', '[', '}', ']', '\\'};
    int[] specialKeys = {KeyEvent.VK_BACK_SPACE, KeyEvent.VK_SHIFT, KeyEvent.VK_ALT, KeyEvent.VK_CONTROL,
            KeyEvent.VK_CAPS_LOCK, KeyEvent.VK_ENTER};
    String[] array;
    JComboBox choice;
    private JPanel instructionPanel;
    private JPanel middlePanel;
    private JPanel inputPanel;
    private JPanel controlsPanel;
    private JPanel buttonPanel;
    private TWTextField txtSelection;
    private ShortCutCombo box;
    private ShortCutAdjuster panel;
    private int defaultCloseOperation = HIDE_ON_CLOSE;
    private JComponent parent;

//    public static ShortCutDefiner selfRef;
//
//    public ShortCutDefiner() {
//        //To change body of created methods use File | Settings | File Templates.
//    }

//    public static ShortCutDefiner getInstance() {
//        if (selfRef == null) {
//            selfRef = new ShortCutDefiner();
//        }
//        return selfRef;
//    }

    public ShortCutDefiner(ShortCutCombo selectedCombo) {
        super(Client.getInstance().getFrame(), Language.getString("NEW_SHORTCUT") + " - " + Language.getString("SC_" + selectedCombo.getTag()));
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        setModal(true);
        box = selectedCombo;
        important = (String) box.getItemAt(0);
        createUI();
//        selfRef = this;

//        System.out.println("000000000000000000000"+important);
    }


    public ShortCutDefiner(String[] comboText) {
        array = comboText;
    }

    public void createUI() {
        this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        this.requestFocusInWindow();
//        this.setLocation(900, 500);
        this.setSize(300, 190);
        this.setResizable(false);
        this.setFocusable(true);
//        this.setClosable(true);

        // this.addWindowFocusListener((WindowFocusListener)this);
//        this.setVisible(true);

        /*  this.addInternalFrameListener(new InternalFrameAdapter() {//<-----------
        *//*
               public void windowClosing(WindowEvent we) {
                   windowsClosingMethod();

                   if (!KeyFunctionPanel.isValid) {
                       windowsClosingSavingMethod();
                   }

                   if (isCanceled) {
                       windowsClosingCanceledMethod();
                   }

               }*//*
        });*/


        keyPanel = new JPanel(new FlexGridLayout(new String[]{"285"}, new String[]{"20", "1", "80", "1", "30"}, 5, 5));
        instructionPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 0, 0));
        instructionPanel.add(new JLabel(Language.getString("SELECT_SHORTCUT")));
        middlePanel = new JPanel(new ColumnLayout());
        inputPanel = new JPanel(new FlexGridLayout(new String[]{"100", "165", "10"}, new String[]{"20", "20"}, 5, 5));
        controlsPanel = new JPanel(new FlexGridLayout(new String[]{"95", "60", "60", "60"}, new String[]{"20"}, 5, 5));


        txtKey = new TWTextField(10);
        txtKey.addKeyListener(this);

        txtSelection = new TWTextField(20);
        txtSelection.setEditable(false);

        shiftBox = new JCheckBox(Language.getString("SHIFT"));
        shiftBox.setEnabled(false);
        altBox = new JCheckBox(Language.getString("ALT"));
        ctrlBox = new JCheckBox(Language.getString("CTRL"));

        altBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                Object source = e.getItemSelectable();
                if (source == altBox) {
                    isAlt = true;
                    shiftBox.setEnabled(true);
                }
                if (e.getStateChange() == ItemEvent.DESELECTED) {
                    //c = '-';
                    isAlt = false;
                    if (ctrlBox.isSelected()) {
                        shiftBox.setEnabled(true);
                    } else {
                        shiftBox.setSelected(false);
                        shiftBox.setEnabled(false);
                    }
                }
                txtSelection.setText(getSelectedChoice());
            }
        });

        ctrlBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                Object source = e.getItemSelectable();
                if (source == ctrlBox) {
                    isCtrl = true;
                    shiftBox.setEnabled(true);
                }
                if (e.getStateChange() == ItemEvent.DESELECTED) {
                    isCtrl = false;
                    if (altBox.isSelected()) {
                        shiftBox.setEnabled(true);
                    } else {
                        shiftBox.setSelected(false);
                        shiftBox.setEnabled(false);
                    }

                }
                txtSelection.setText(getSelectedChoice());

            }
        });


        shiftBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                Object source = e.getItemSelectable();
                if (source == shiftBox) {
                    isShift = true;
                }
                if (e.getStateChange() == ItemEvent.DESELECTED) {
                    isShift = false;
                }
                txtSelection.setText(getSelectedChoice());
            }
        });


        cancelButton = new TWButton(Language.getString("CANCEL"));
        okButton = new TWButton(Language.getString("OK"));
        clearButton = new TWButton(Language.getString("RESET"));
        choice = new JComboBox();
        choice.addItem("<None>");
        choice.addItem(Language.getString("ALT"));
        choice.addItem(Language.getString("CTRL"));
        choice.addItem(Language.getString("SHIFT"));

        controlsPanel.add(new JLabel());
        controlsPanel.add(ctrlBox);
        controlsPanel.add(shiftBox);
        controlsPanel.add(altBox);

        inputPanel.add(new JLabel(Language.getString("CURRENT_SELECTION")));
        inputPanel.add(txtSelection);
        inputPanel.add(new JLabel());
        inputPanel.add(new JLabel(Language.getString("KEY_COMBINATION")));
        inputPanel.add(txtKey);
        inputPanel.add(new JLabel());

        middlePanel.add(inputPanel);
        middlePanel.add(controlsPanel);

        //Button panel
        buttonPanel = new JPanel(new FlexGridLayout(new String[]{"60", "65", "65", "70"}, new String[]{"20"}, 5, 5));
        buttonPanel.add(new JLabel());
        buttonPanel.add(okButton);
        buttonPanel.add(clearButton);
        buttonPanel.add(cancelButton);


        keyPanel.add(instructionPanel);
        keyPanel.add(new JSeparator());
        keyPanel.add(middlePanel);
        keyPanel.add(new JSeparator());
        keyPanel.add(buttonPanel);

        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                isCanceled = true;
                altBox.setSelected(false);
                ctrlBox.setSelected(false);
                shiftBox.setSelected(false);
                txtKey.setText("");
                txtSelection.setText("");
                dispose();
            }
        });

        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                isSaved = true;
                String retunString = completingText();
                String textFromArea = txtKey.getText();
                boolean valid = ShortCutAdjuster.getInstance().checkingProgress(retunString, textFromArea);
                if (valid) {

                    String choice = retunString + textFromArea;
                    ShortCutAdjuster.getInstance().settingChoice(choice, box);

                }
//                panel = new KeyFunctionPanel(retunString + "+" + textFromArea);
                /* String nullString = "";
                if ((textFromArea.equals(nullString)) && (retunString.equals(nullString))) {
                    ShortCutAdjuster.getInstance().settingChoice("<None>", box);
                } else if (!(textFromArea.equals(nullString)) && (retunString.equals(nullString))) {
                    ShortCutAdjuster.getInstance().settingChoice(textFromArea, box);
                } else if ((textFromArea.equals(nullString)) && !(retunString.equals(nullString))) {
                    ShortCutAdjuster.getInstance().settingChoice(retunString, box);

                } else if (!(textFromArea.equals(nullString)) && !(retunString.equals(nullString))) {
                    ShortCutAdjuster.getInstance().settingChoice(retunString + "+" + textFromArea, box);
                }*/
                if (ShortCutAdjuster.getInstance().isValid) {

                    closeWindow();
                }

            }

        });

        clearButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                altBox.setSelected(false);
                ctrlBox.setSelected(false);
                shiftBox.setSelected(false);
                txtKey.setText("");
                txtSelection.setText("");
            }
        });

        this.add(keyPanel);
//        pack();
//        setResizable(false);
        this.setVisible(true);
        isSaved = false;
    }

    public void keyTyped(KeyEvent e) {
/*
        txtKey.setText("");
        int keyCode = e.getKeyCode();

        int numpandwn = 95;
        int numpanup = 112;

        if ((keyCode > numpandwn) && (keyCode < numpanup)) {
            txtKey.setText("");
        }

        String comparingString = String.valueOf(e.getKeyChar());
        System.out.println("))))))))))))))))))" + comparingString);
        // f8 = 'F8';

        if (keyCode == 59)
            txtKey.setText("");
        if (keyCode == 61)
            txtKey.setText("");
        if (keyCode == 45)
            txtKey.setText("");
        if (keyCode == 47)
            txtKey.setText("");
        if (keyCode == 96)
            txtKey.setText("");
        if (keyCode == 91)
            txtKey.setText("");
        if (keyCode == 92)
            txtKey.setText("");
        if (keyCode == 93)
            txtKey.setText("");
        if (keyCode == 39)
            txtKey.setText("");
        if (keyCode == 44)
            txtKey.setText("");
        if (keyCode == 46)
            txtKey.setText("");
        if (keyCode == 47)


            if (keyCode == 37)
                txtKey.setText("");
        if (keyCode == 38)
            txtKey.setText("");
        if (keyCode == 39)
            txtKey.setText("");
        if (keyCode == 40)
            txtKey.setText("");
        if (keyCode == 45)
            txtKey.setText("");
        if (keyCode == 46)
            txtKey.setText("");
        if (keyCode == 144)
            txtKey.setText("");
        if (keyCode == 145)
            txtKey.setText("");
        if (keyCode == 19)
            txtKey.setText("");
        if (keyCode == 186)
            txtKey.setText("");
        if (keyCode == 187)
            txtKey.setText("");
        if (keyCode == 189)
            txtKey.setText("");
        if (keyCode == 191)
            txtKey.setText("");
        if (keyCode == 192)
            txtKey.setText("");
        if (keyCode == 219)
            txtKey.setText("");
        if (keyCode == 220)
            txtKey.setText("");
        if (keyCode == 222)
            txtKey.setText("");
        if (keyCode == 188)
            txtKey.setText("");
        if (keyCode == 190)
            txtKey.setText("");
        if (keyCode == 191)
            txtKey.setText("");

        char comparingChar = e.getKeyChar();

        for (int r = 0; r < charArray.length; r++) {
            if (comparingChar == charArray[r]) {
                txtKey.setText("");
            }
        }
//        char myChar = '';
        System.out.println("kkkkkkkkkkkkkkkey" + keyCode);
        int numdwn = 47;
        int numup = 58;
        if ((keyCode > numdwn) && (keyCode < numup)) {
            System.out.println("im in------");
            txtKey.setText("");
        }

        if (keyCode == 112)
            txtKey.setText("F1");
        if (keyCode == 113)
            txtKey.setText("F2");
        if (keyCode == 114)
            txtKey.setText("F3");
        if (keyCode == 115)
            txtKey.setText("F4");
        if (keyCode == 116)
            txtKey.setText("F5");
        if (keyCode == 117)
            txtKey.setText("F6");
        if (keyCode == 118)
            txtKey.setText("F7");
        if (keyCode == 119)
            txtKey.setText("F8");
        if (keyCode == 120)
            txtKey.setText("F9");
        if (keyCode == 121)
            txtKey.setText("F10");
        if (keyCode == 122)
            txtKey.setText("F11");
        if (keyCode == 123)
            txtKey.setText("F12");


        if (keyCode == 59)
            txtKey.setText("");
        if (keyCode == 61)
            txtKey.setText("");
        if (keyCode == 45)
            txtKey.setText("");
        if (keyCode == 47)
            txtKey.setText("");
        if (keyCode == 96)
            txtKey.setText("");
        if (keyCode == 91)
            txtKey.setText("");
        if (keyCode == 92)
            txtKey.setText("");
        if (keyCode == 93)
            txtKey.setText("");
        if (keyCode == 39)
            txtKey.setText("");
        if (keyCode == 44)
            txtKey.setText("");
        if (keyCode == 46)
            txtKey.setText("");
        if (keyCode == 47)
            //   65 -90 , 112 -123 ,
       */
/*     if (keyCode == 8)
                txtKey.setText(Language.getString("BCKSPACE"));*/
/*

        if (keyCode == 27)
            txtKey.setText(Language.getString("ESC"));
        if (keyCode == 32)
            txtKey.setText(Language.getString("SPACE"));
        if (keyCode == 33)
            txtKey.setText(Language.getString("PAGE_UP"));
        if (keyCode == 34)
            txtKey.setText(Language.getString("PAGE_DOWN"));
        if (keyCode == 35)
            txtKey.setText(Language.getString("END"));
        if (keyCode == 36)
            txtKey.setText(Language.getString("HOME"));
*/


    }

    public void keyPressed(KeyEvent e) {
/*
        txtKey.setText("");
        char comparingChar = e.getKeyChar();
        String comparingString = String.valueOf(e.getKeyChar());

        for (int r = 0; r < charArray.length; r++) {
            if (comparingChar == charArray[r]) {
                txtKey.setText("");
            }
        }


        int keyCode = e.getKeyCode();

        int numpandwn = 95;
        int numpanup = 112;

        if ((keyCode > numpandwn) && (keyCode < numpanup)) {
            txtKey.setText("");
        }
        if (keyCode == 59)
            txtKey.setText("");
        if (keyCode == 61)
            txtKey.setText("");
        if (keyCode == 45)
            txtKey.setText("");
        if (keyCode == 47)
            txtKey.setText("");
        if (keyCode == 96)
            txtKey.setText("");
        if (keyCode == 91)
            txtKey.setText("");
        if (keyCode == 92)
            txtKey.setText("");
        if (keyCode == 93)
            txtKey.setText("");
        if (keyCode == 39)
            txtKey.setText("");
        if (keyCode == 44)
            txtKey.setText("");
        if (keyCode == 46)
            txtKey.setText("");
        if (keyCode == 47)

            if (keyCode == 37)
                txtKey.setText("");
        if (keyCode == 38)
            txtKey.setText("");
        if (keyCode == 39)
            txtKey.setText("");
        if (keyCode == 40)
            txtKey.setText("");
        if (keyCode == 45)
            txtKey.setText("");
        if (keyCode == 46)
            txtKey.setText("");
        if (keyCode == 144)
            txtKey.setText("");
        if (keyCode == 145)
            txtKey.setText("");
        if (keyCode == 19)
            txtKey.setText("");
        if (keyCode == 186)
            txtKey.setText("");
        if (keyCode == 187)
            txtKey.setText("");
        if (keyCode == 189)
            txtKey.setText("");
        if (keyCode == 191)
            txtKey.setText("");
        if (keyCode == 192)
            txtKey.setText("");
        if (keyCode == 219)
            txtKey.setText("");
        if (keyCode == 220)
            txtKey.setText("");
        if (keyCode == 222)
            txtKey.setText("");
        if (keyCode == 188)
            txtKey.setText("");
        if (keyCode == 190)
            txtKey.setText("");
        if (keyCode == 191)
            txtKey.setText("");

        if (keyCode == 112)
            txtKey.setText("F1");
        if (keyCode == 113)
            txtKey.setText("F2");
        if (keyCode == 114)
            txtKey.setText("F3");
        if (keyCode == 115)
            txtKey.setText("F4");
        if (keyCode == 116)
            txtKey.setText("F5");
        if (keyCode == 117)
            txtKey.setText("F6");
        if (keyCode == 118)
            txtKey.setText("F7");
        if (keyCode == 119)
            txtKey.setText("F8");
        if (keyCode == 120)
            txtKey.setText("F9");
        if (keyCode == 121)
            txtKey.setText("F10");
        if (keyCode == 122)
            txtKey.setText("F11");
        if (keyCode == 123)
            txtKey.setText("F12");
        //   65 -90 , 112 -123 ,
       */
/* if (keyCode == 8)
            txtKey.setText(Language.getString("BCKSPACE"));*/
/*

        if (keyCode == 27)
            txtKey.setText(Language.getString("ESC"));
        if (keyCode == 32)
            txtKey.setText(Language.getString("SPACE"));
        if (keyCode == 33)
            txtKey.setText(Language.getString("PAGE_UP"));
        if (keyCode == 34)
            txtKey.setText(Language.getString("PAGE_DOWN"));
        if (keyCode == 35)
            txtKey.setText(Language.getString("END"));
        if (keyCode == 36)
            txtKey.setText(Language.getString("HOME"));
*/

    }

    public void keyReleased(KeyEvent e) {

        int keyCode = e.getKeyCode();
        boolean isSpecialKey = false;
        for (int i = 0; i < specialKeys.length; i++) {
            if (keyCode == specialKeys[i]) {
                isSpecialKey = true;
            }

        }

        if (isSpecialKey) {
            txtKey.setText("");
            txtSelection.setText(getSelectedChoice());
        } else {
            txtKey.setText(KeyEvent.getKeyText(keyCode).toUpperCase());
            txtSelection.setText(getSelectedChoice());
        }
/*
        char comparingChar = e.getKeyChar();

        for (int r = 0; r < charArray.length; r++) {
            if (comparingChar == charArray[r]) {
                txtKey.setText("");
            }
        }

        int keyCode = e.getKeyCode();
        String comparingString = String.valueOf(e.getKeyChar());

        int numpandwn = 95;
        int numpanup = 112;

        if ((keyCode > numpandwn) && (keyCode < numpanup)) {
            txtKey.setText("");
        }

        char myChar = '';


        if (keyCode == 37)
            txtKey.setText("");
        if (keyCode == 38)
            txtKey.setText("");
        if (keyCode == 39)
            txtKey.setText("");
        if (keyCode == 40)
            txtKey.setText("");
        if (keyCode == 45)
            txtKey.setText("");
        if (keyCode == 46)
            txtKey.setText("");
        if (keyCode == 144)
            txtKey.setText("");
        if (keyCode == 145)
            txtKey.setText("");
        if (keyCode == 19)
            txtKey.setText("");
        if (keyCode == 186)
            txtKey.setText("");
        if (keyCode == 187)
            txtKey.setText("");
        if (keyCode == 189)
            txtKey.setText("");
        if (keyCode == 191)
            txtKey.setText("");
        if (keyCode == 192)
            txtKey.setText("");
        if (keyCode == 219)
            txtKey.setText("");
        if (keyCode == 220)
            txtKey.setText("");
        if (keyCode == 222)
            txtKey.setText("");
        if (keyCode == 188)
            txtKey.setText("");
        if (keyCode == 190)
            txtKey.setText("");
        if (keyCode == 191)
            txtKey.setText("");

        int numdwn = 47;
        int numup = 58;
        if ((keyCode > numdwn) && (keyCode < numup)) {

            txtKey.setText("");
        }


        if (keyCode == 112)
            txtKey.setText("F1");
        if (keyCode == 113)
            txtKey.setText("F2");
        if (keyCode == 114)
            txtKey.setText("F3");
        if (keyCode == 115)
            txtKey.setText("F4");
        if (keyCode == 116)
            txtKey.setText("F5");
        if (keyCode == 117)
            txtKey.setText("F6");
        if (keyCode == 118)
            txtKey.setText("F7");
        if (keyCode == 119)
            txtKey.setText("F8");
        if (keyCode == 120)
            txtKey.setText("F9");
        if (keyCode == 121)
            txtKey.setText("F10");
        if (keyCode == 122)
            txtKey.setText("F11");
        if (keyCode == 123)
            txtKey.setText("F12");
        //   65 -90 , 112 -123 ,
  */
/*      if (keyCode == 8)
            txtKey.setText(Language.getString("BCKSPACE"));*/
/*

        if (keyCode == 27)
            txtKey.setText(Language.getString("ESC"));
        if (keyCode == 32)
            txtKey.setText(Language.getString("SPACE"));
        if (keyCode == 33)
            txtKey.setText(Language.getString("PAGE_UP"));
        if (keyCode == 34)
            txtKey.setText(Language.getString("PAGE_DOWN"));
        if (keyCode == 35)
            txtKey.setText(Language.getString("END"));
        if (keyCode == 36)
            txtKey.setText(Language.getString("HOME"));

        txtSelection.setText(getSelectedChoice());
*/
    }


    public String completingText() {
        String selected = "";
        if (isAlt && isCtrl && isShift) {
//            selected = altBox.getText() + "+" + ctrlBox.getText() + "+" + shiftBox.getText();
            selected = ctrlBox.getText() + "+" + shiftBox.getText() + "+" + altBox.getText() + "+";
        }

        if (isAlt && isCtrl && !isShift) {
            selected = ctrlBox.getText() + "+" + altBox.getText() + "+";
        }

        if (isAlt && !isCtrl && isShift) {
            selected = shiftBox.getText() + "+" + altBox.getText() + "+";
        }
        if (isAlt && !isCtrl && !isShift) {
            selected = altBox.getText() + "+";
        }
        if (!isAlt && isCtrl && isShift) {
            selected = ctrlBox.getText() + "+" + shiftBox.getText() + "+";
        }
        if (!isAlt && isCtrl && !isShift) {
            selected = ctrlBox.getText() + "+";
        }
        if (!isAlt && !isCtrl && isShift) {
            selected = shiftBox.getText() + "+";
        }
        return selected;
    }

    public int getDefaultCloseOperation() {
        return defaultCloseOperation;
    }

    public void settingInitialData() {
        JOptionPane.showMessageDialog(this, Language.getString("SAVE_CHANGES"),
                Language.getString("ERROR"), JOptionPane.OK_OPTION);
    }

    public void windowsClosingMethod() {
        if (!isSaved) {
            settingInitialData();
//            ShortCutAdjuster.getInstance().settingUnSellected(box, important);
        }
    }

    public void windowsClosingSavingMethod() {
//       if(!isSaved){
//              settingInitialData();
        System.out.println("im in windowsClosingSavingMethod");
//        ShortCutAdjuster.getInstance().settingUnSellected(box, important);
//          }
    }

    public void windowsClosingCanceledMethod() {

//        ShortCutAdjuster.getInstance().settingUnSellected(box, important);
//          }
        isCanceled = false;
    }

    public ShortCutCombo sendBox() {
        return box;
    }

    public void closeWindow() {
        this.dispose();
    }

    private String getSelectedChoice() {
        String retunString = completingText();
        String textFromArea = txtKey.getText();
        return retunString + textFromArea;
    }

}
