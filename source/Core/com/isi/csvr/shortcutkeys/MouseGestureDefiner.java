package com.isi.csvr.shortcutkeys;

import com.isi.csvr.Client;
import com.isi.csvr.TWActions;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA.
 * User: sandarekaf
 * Date: Jun 24, 2009
 * Time: 10:32:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class MouseGestureDefiner extends JDialog implements KeyListener {
    private JPanel mainPanel;
    private JPanel instructionPanel;
    private JPanel middlePanel;
    private JPanel keyGuidePanel;
    private JPanel inputPanel;
    private JPanel buttonPanel;


    private JLabel lblInput;
    private TWTextField txtGesture;
    private JFrame parent = Client.getInstance().getFrame();


    private TWButton btnSave;
    private TWButton btnCancel;

    private TWButton btnReset;

    private TWButton btnUP = new TWButton("U");
    private TWButton btnDOWN = new TWButton("D");
    private TWButton btnLEFT = new TWButton("L");
    private TWButton btnRIGHT = new TWButton("R");
    private ArrayList<String> gestureList = new ArrayList<String>();

    private char[] allowedKeys = {'U', 'D', 'R', 'L'};
    private int[] specialKeys = {KeyEvent.VK_SHIFT, KeyEvent.VK_CAPS_LOCK, KeyEvent.VK_TAB, KeyEvent.VK_CONTROL,
            KeyEvent.VK_ESCAPE, KeyEvent.VK_ENTER, KeyEvent.VK_UP, KeyEvent.VK_DOWN, KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT,
            KeyEvent.VK_DELETE, KeyEvent.VK_END, KeyEvent.VK_PAGE_DOWN, KeyEvent.VK_PAGE_UP, KeyEvent.VK_INSERT,
            KeyEvent.VK_HOME, KeyEvent.VK_BACK_SPACE, KeyEvent.VK_ALT};


    private MouseGestureCombo selectedBox;


    public MouseGestureDefiner(MouseGestureCombo selectedBox) {
        super(Client.getInstance().getFrame(), Language.getString("NEW_MOUSEGESTURE") + " - " +
                Language.getString("SC_" + selectedBox.getTag()));
        setModal(true);
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        this.selectedBox = selectedBox;
        createUI();
    }

    private void createUI() {
//        mainPanel = new JPanel(new FlexGridLayout(new String[]{"300"}, new String[]{"25", "50", "25"}, 5, 5));
//        setSize(350,150);
        mainPanel = new JPanel(new FlexGridLayout(new String[]{"300"}, new String[]{"40", "1", "70", "1", "30"}, 1, 0));
//        mainPanel.setSize(350,150);
        instructionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));

        JTextArea lblInstruction = new JTextArea(Language.getString("SELECT_MOUSEGESTURE"));
        lblInstruction.setSize(290, 40);
        lblInstruction.setEditable(false);
        lblInstruction.setBackground(this.getBackground());
        lblInstruction.setLineWrap(true);
        lblInstruction.setWrapStyleWord(true);
        lblInstruction.setBorder(BorderFactory.createEmptyBorder());
        instructionPanel.add(lblInstruction);

        //Middle Panel
        middlePanel = new JPanel(new FlexGridLayout(new String[]{"290"}, new String[]{"25", "30"}, 5, 5));

        //keyGuidePanel

        keyGuidePanel = new JPanel(new FlexGridLayout(new String[]{"100", "40", "40", "40", "40", "30"}, new String[]{"100%"}, 5, 5));
//         keyGuidePanel = new JPanel(new FlexGridLayout(new String[]{"40%", "15%", "15%", "15%", "15%","10%"}, new String[]{"100%"}, 5, 5));
        btnUP.setSize(20, 20);
        btnUP.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                String newGesture = appendGesture("U");
                txtGesture.setText(newGesture);
                btnUP.setEnabled(false);
                btnDOWN.setEnabled(true);
                btnLEFT.setEnabled(true);
                btnRIGHT.setEnabled(true);
            }
        });

        btnDOWN.setSize(20, 20);
        btnDOWN.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String newGesture = appendGesture("D");
                txtGesture.setText(newGesture);
                btnUP.setEnabled(true);
                btnDOWN.setEnabled(false);
                btnLEFT.setEnabled(true);
                btnRIGHT.setEnabled(true);
            }
        });

        btnLEFT.setSize(20, 20);
        btnLEFT.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String newGesture = appendGesture("L");
                txtGesture.setText(newGesture);
                btnUP.setEnabled(true);
                btnDOWN.setEnabled(true);
                btnLEFT.setEnabled(false);
                btnRIGHT.setEnabled(true);
            }
        });

        btnRIGHT.setSize(20, 20);
        btnRIGHT.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String newGesture = appendGesture("R");
                txtGesture.setText(newGesture);
                btnUP.setEnabled(true);
                btnDOWN.setEnabled(true);
                btnLEFT.setEnabled(true);
                btnRIGHT.setEnabled(false);
            }
        });


        keyGuidePanel.add(new JLabel(Language.getString("SELECT_MOTION")));
        keyGuidePanel.add(btnUP);
        keyGuidePanel.add(btnDOWN);
        keyGuidePanel.add(btnLEFT);
        keyGuidePanel.add(btnRIGHT);
        keyGuidePanel.add(new JLabel());

        //inputPanel
        inputPanel = new JPanel(new FlexGridLayout(new String[]{"100", "175", "15"}, new String[]{"20"}, 5, 5));
//        inputPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "50%","10%"}, new String[]{"20"}, 5, 5));
        lblInput = new JLabel(Language.getString("CURRENT_SELECTION"));
        txtGesture = new TWTextField();
        txtGesture.setEditable(false);
        txtGesture.addKeyListener(this);
        inputPanel.add(lblInput);
        inputPanel.add(txtGesture);

        middlePanel.add(inputPanel);
        middlePanel.add(keyGuidePanel);

        //buttonPanel
        buttonPanel = new JPanel(new FlexGridLayout(new String[]{"22%", "25%", "25%", "28%"}, new String[]{"100%"}, 5, 5));
        btnSave = new TWButton(Language.getString("OK"));
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String gesture = txtGesture.getText();
                saveChoice(gesture);
            }
        });
        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                txtGesture.setText("");

                dispose();
            }
        });

        btnReset = new TWButton(Language.getString("RESET"));
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnUP.setEnabled(true);
                btnDOWN.setEnabled(true);
                btnLEFT.setEnabled(true);
                btnRIGHT.setEnabled(true);
                txtGesture.setText("");
                gestureList.clear();
            }
        });

        buttonPanel.add(new JLabel());
        buttonPanel.add(btnSave);
        buttonPanel.add(btnReset);
        buttonPanel.add(btnCancel);

        mainPanel.add(instructionPanel);
        mainPanel.add(new JSeparator());
        mainPanel.add(middlePanel);
        mainPanel.add(new JSeparator());
        mainPanel.add(buttonPanel);

        this.add(mainPanel);
        this.pack();
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
    }

    private void saveChoice(String gesture) {
        if (gesture.equals("") || gesture == null) {
            JOptionPane.showMessageDialog(this, Language.getString("ERROR_EMPTY_KEY"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        } else if (TWActions.mgMappingsTable.containsValue(gesture)) {
            JOptionPane.showMessageDialog(this, Language.getString("ERROR_KEY_ALREADY_ASSIGNED"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        } else {
            boolean isSameAdjecentChars = false;
            boolean isValidChar = false;
            char[] chars = gesture.toCharArray();
            char previousChar = chars[0];
            for (int i = 1; i < chars.length; i++) {
                char currentChar = chars[i];

                if (previousChar == currentChar) {
                    isSameAdjecentChars = true;
                    break;
                } else {
                    previousChar = currentChar;
                }
            }

            for (int i = 0; i < chars.length; i++) {
                for (int j = 0; j < allowedKeys.length; j++) {
                    if (chars[i] == allowedKeys[j]) {
                        isValidChar = true;
                    }
                }
            }


            if (isSameAdjecentChars) {
                JOptionPane.showMessageDialog(this, Language.getString("ERROR_SAME_CHARACTOR"),
                        Language.getString("ERROR"), JOptionPane.OK_OPTION);
            } else if (!isValidChar) {
                JOptionPane.showMessageDialog(this, Language.getString("ERROR_INVALID_CHARACTOR"),
                        Language.getString("ERROR"), JOptionPane.OK_OPTION);
            } else {
                selectedBox.removeAllItems();
                selectedBox.addItem(gesture);
                selectedBox.addItem(Language.getString("NONE"));
                selectedBox.addItem(Language.getString("DEFINE_MOUSE_GESTURE"));
                selectedBox.setSelectedIndex(0);

                //TWActions.mgMappingsTable.put(selectedBox.getTag(),gesture);
                //TWActions.getInstance().saveMouseGestures();
                this.dispose();
            }
        }
    }

    public void keyTyped(KeyEvent e) {
        
        /*boolean valid = false;
        char key = e.getKeyChar();
        for (int i = 0; i < allowedKeys.length; i++) {
            if (allowedKeys[i] == key) {
                valid = true;
                break;
            }
        }
        if (!valid) {

            String text = txtGesture.getText();
            txtGesture.setText(text.substring(0, text.length()));
        }*/

    }

    public void keyPressed(KeyEvent e) {
     /*   boolean valid = false;
        char key = e.getKeyChar();
        for (int i = 0; i < allowedKeys.length; i++) {
            if (allowedKeys[i] == key) {
                valid = true;
                break;
            }
        }
        if (!valid) {
            String text = txtGesture.getText();
            txtGesture.setText(text.substring(0, text.length()));
//           txtGesture.setText("");
        }*/

    }

    public void keyReleased(KeyEvent e) {
    /*    boolean valid = false;
        char key = e.getKeyChar();
        for (int i = 0; i < allowedKeys.length; i++) {
            if (allowedKeys[i] == key) {
                valid = true;
                break;
            }
        }
        if (!valid) {
            boolean isSpecialKey = false;
            for (int i = 0; i < specialKeys.length; i++) {
                if (e.getKeyCode() == specialKeys[i]) {
                    isSpecialKey = true;
                    break;
                }
            }
            if (!isSpecialKey) {
                String text = txtGesture.getText();
                txtGesture.setText(text.substring(0, text.length() - 1));
            }

        }*/
    }

    private String appendGesture(String newGesture) {
        gestureList.add(newGesture);
        String gestureString = "";
        Iterator it = gestureList.iterator();
        while (it.hasNext()) {
            String gesture = (String) it.next();
            gestureString += gesture;
        }
        return gestureString;
    }

    private String clearResentGesture() {
        gestureList.remove(gestureList.size() - 1);
        String gestureString = "";
        Iterator it = gestureList.iterator();
        while (it.hasNext()) {
            String gesture = (String) it.next();
            gestureString += gesture;
        }
        return gestureString;
    }


}