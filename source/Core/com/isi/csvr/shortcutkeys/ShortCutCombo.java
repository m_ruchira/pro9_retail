package com.isi.csvr.shortcutkeys;

import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jun 15, 2009
 * Time: 2:03:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShortCutCombo extends JComboBox implements ActionListener, ItemListener {


    public static boolean isUICreate = true;
    private String name;
    private String tag;
    private JComponent parent;

    public ShortCutCombo(String tag, String name) {
        super();
        this.parent = parent;
        this.setTag(tag);
        this.setName(name);
        addActionListener(this);
        addItemListener(this);
    }

    public ShortCutCombo() {
        addActionListener(this);

    }

    public ShortCutCombo(String[] comboText) {
        //To change body of created methods use File | Settings | File Templates.
    }


    public void actionPerformed(ActionEvent ex) {
       /* int items = this.getItemCount();
        int lastIndex = items-1;*/
        /*if (this.getSelectedItem().equals(Language.getString("DEFINE_SHORT_CUT"))) {
            ShortCutDefiner keyDefiner = new ShortCutDefiner(this, parent);
        }*/
    }

    public void actionMethod() {
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getItem().equals(Language.getString("DEFINE_SHORT_CUT"))
                && e.getStateChange() == ItemEvent.SELECTED) {
            ShortCutDefiner keyDefiner = new ShortCutDefiner(this);
            setSelectedIndex(0);
        }
       /* if (this.getSelectedItem().equals(Language.getString("DEFINE_SHORT_CUT"))) {
            ShortCutDefiner keyDefiner = new ShortCutDefiner(this, parent);
        }*/
    }

    public void settingText(String text) {
        this.addItem(text);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
