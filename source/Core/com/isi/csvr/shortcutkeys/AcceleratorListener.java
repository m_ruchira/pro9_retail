package com.isi.csvr.shortcutkeys;

/**
 * Created by IntelliJ IDEA.
 * User: sandarekaf
 * Date: Jul 8, 2009
 * Time: 3:06:34 PM
 * To change this template use File | Settings | File Templates.
 */
public interface AcceleratorListener {
    public void acceleratorChanged();
}
