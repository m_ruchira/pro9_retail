// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.properties;

/**
 * A class to handle view settings of table views
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.ClientTable;
import com.isi.csvr.DataDisintegrator;
import com.isi.csvr.datastore.PortfolioStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.mist.MISTTable;
import com.isi.csvr.mist.MISTTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.math.BigInteger;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class ViewSetting implements Cloneable, ViewConstants {
    public boolean isDetached = false;
    public boolean temp = false;
    public int layer = -1;
    private char g_chrType;
    private byte g_bytSubType;
    private byte g_bytMainType;
    private int g_iStyle = ViewSettingsManager.STYLE_NORMAL;
    private int g_iIndex = 0;
    private boolean g_bVisible;
    private String g_sID;
    //change start
    private BigInteger g_iColumns;
    private BigInteger g_iFixedColumns;
    private BigInteger g_lHiddenColumns;
    //change end
    private Font g_oFont;
    private Font g_oHeaderFont;
    private String g_sCaptions;
    private String g_sCaptionID = null;
    private Symbols g_oSymbols;
    private String[] g_asColumnHeadings;
    private String[] g_asColumnTooltips;
    private String g_sColumnHeadingsID;
    private String g_sRenderingIDs;
    private String[] g_asRenderingIDs;
    private String[] g_asColumnWidths;
    private Point g_oLocation;
    private Dimension g_oSize;
    private int[][] g_aiColumnSettings;
    private Object g_oParent;
    private Object supportingObject = null;
    private int sortColumn = -1;
    private int sortOrder = 0;
    private boolean headerVisible = true;
    private int splitLocation = -1;
    private String filter = null;
    private String currency = null;
    private int filterType;
    private int tableNumber = 1;
    private String watchlistFilter;
    private byte desktopType = Constants.MAINBOARD_TYPE;
    private String seperatorType = VC_RECORD_DELIMETER;
    private Hashtable propertiesTable = null;

    public ViewSetting() {
        this(VC_RECORD_DELIMETER);
    }

    public ViewSetting(String recordSeperator) {
        seperatorType = recordSeperator;
        propertiesTable = new Hashtable();
        g_iColumns = new BigInteger("0");
        g_iFixedColumns = new BigInteger("0");
        g_lHiddenColumns = new BigInteger("0");
    }

    public void setRecordSeperator(String recordSeperator) {
        seperatorType = recordSeperator;
    }

    public Object getParent() {
        return g_oParent;
    }

    public void setParent(Object oParent) {
        g_oParent = oParent;
    }

    /**
     * Returns the type of the view
     */
    public int getType() {
        return (char) ((g_bytMainType << 8) | (g_bytSubType));
    }

    /**
     * Sets the type of the view
     */
    public void setType(int iType) {
        g_chrType = (char) iType;
        g_bytSubType = (byte) (g_chrType);
        g_bytMainType = (byte) (g_chrType >>> 8);
    }

    /**
     * Returns the main type of the view
     */
    public byte getSubType() {
        //short shtSubType = 0;

        //shtSubType = (short)(g_shtType >>> 8);
        return g_bytSubType;
    }

    /**
     * Set the sub type of the view
     */
    public void setSubType(byte bytType) {
        g_bytSubType = bytType;
    }

    /**
     * Returns the sub type of the view
     */
    public byte getMainType() {
        short shtSubType = 0;

        //shtSubType = (short)(g_shtType & 255);
        return g_bytMainType;
    }

    /**
     * Set the main type of the view
     */
    public void setMainType(byte bytType) {
        g_bytMainType = bytType;
    }

    /**
     * Returns the ID of the view
     */
    public String getID() {
        String key = null;
        if (getSubType() == ViewSettingsManager.DETAIL_QUOTE || getSubType() == ViewSettingsManager.SNAP_QUOTE || getSubType() == ViewSettingsManager.TIMEnSALES_VIEW2
                || getSubType() == ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW || getSubType() == ViewSettingsManager.DEPTH_PRICE_BID_VIEW || getSubType() == ViewSettingsManager.DEPTH_BID_VIEW ||
                getSubType() == ViewSettingsManager.COMBINED_DEPTH_VIEW || getSubType() == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW || getSubType() == ViewSettingsManager.DEPTH_ODDLOT_VIEW
                || getSubType() == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW || getSubType() == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW || getSubType() == ViewSettingsManager.FULL_QUOTE_VIEW) {
            // key= (String)propertiesTable.get(ViewConstants.VC_SYMBOLWISEVIEW_KEY+"");
            key = getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        }
        if (key == null || key.isEmpty()) {
            return g_sID;
        } else {
            return key;
        }

    }

    /**
     * Sets the ID of the view
     */
    public void setID(String sID) {
        g_sID = sID;
    }

    public String getIDForWSP() {
        return g_sID;
    }

    /**
     * Returns the ID of the view
     */
    public int getIntID() throws Exception {
        return Integer.parseInt(g_sID);
    }

    /**
     * Returns selected columns of the view
     */
    public BigInteger getColumns() {
        return g_iColumns;
    }

    /**
     * Sets the selected columns of the view
     */
    public void setColumns(BigInteger iColumns) {
        g_iColumns = iColumns;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getWatchlistFilter() {
        return watchlistFilter;
    }

    public void setWatchlistFilter(String watchlistFilter) {
        this.watchlistFilter = watchlistFilter;
    }

    /**
     * Returns fixed columns of the view
     */
    public BigInteger getFixedColumns() {
        return g_iFixedColumns;
    }

    /**
     * Sets the fixed columns of the view
     */
    public void setFixedColumns(BigInteger iFixedColumns) {
        g_iFixedColumns = iFixedColumns;
        g_iColumns = g_iColumns.and(g_iFixedColumns.not());
    }

    /**
     * Returns Hidden columns of the view
     */
    public BigInteger getHiddenColumns() {
        return g_lHiddenColumns;
    }

    /**
     * Sets the hidden columns of the view
     */
    public void setHiddenColumns(BigInteger lHiddenColumns) {
        g_lHiddenColumns = lHiddenColumns;
    }

    /**
     * Return font of the view
     */
    public Font getFont() {
        return g_oFont;
    }

    /**
     * Set font for the view
     */
    public void setFont(Font oFont) {
        g_oFont = oFont;
    }

    /**
     * Return font of the header
     */
    public Font getHeaderFont() {
        return g_oHeaderFont;
    }

    /**
     * Set font for the header
     */
    public void setHeaderFont(Font oFont) {
        g_oHeaderFont = oFont;
    }

    public String getFontString() {
        String fontString = "";

        if (g_oFont == null)
            fontString = "0";
        else
            fontString = g_oFont.getFamily() + "," + g_oFont.getStyle() + "," + g_oFont.getSize();

        return fontString;
    }

    public String getHeaderFontString() {
        String fontString = "";
        if (g_oHeaderFont == null)
            fontString = "0";
        else
            fontString = g_oHeaderFont.getFamily() + "," + g_oHeaderFont.getStyle() + "," + g_oHeaderFont.getSize();

        return fontString;
    }

    public String getLocationString(WindowWrapper parent) {
        if (parent == null) {
            return ((int) getLocation().getX() + "," + (int) getLocation().getY());
        } else {
            if (parent.isDetached()) {
                JFrame iframe = (JFrame) parent.getDetachedFrame();
                return ((int) iframe.getLocationOnScreen().getX() + "," + (int) iframe.getLocationOnScreen().getY());
            } else {
                return ((int) parent.getLocation().getX() + "," + (int) parent.getLocation().getY());
            }
        }
    }

    public String getSizeString(WindowWrapper oParent) {
        if (oParent == null) {
            return ((int) getSize().getWidth() + "," + (int) getSize().getHeight());
        } else {
            if (oParent.isDetached()) {
                JFrame iframe = (JFrame) oParent.getDetachedFrame();
                return ((int) iframe.getSize().getWidth() + "," + (int) iframe.getSize().getHeight());
            } else {
                return ((int) oParent.getSize().getWidth() + "," + (int) oParent.getSize().getHeight());
            }
        }
    }

    public int getZOrder(WindowWrapper oParent) {
        if (oParent == null) {
            return g_iIndex;
        } else {
            return oParent.getZOrder();
        }
    }

    public boolean isWindowVisibile(WindowWrapper oParent) {
        if (oParent == null) {
            return isVisible();
        } else {
            return oParent.isWindowVisible();
        }
    }

    /**
     * Returns captions of the view
     */
    public String getCaptions() {
        //if (g_sCaptionID == null)
        return g_sCaptions;
    }

    /**
     * Sets captions for the view
     */
    public void setCaptions(String sCaptions) {
        g_sCaptions = sCaptions;
    }

    /**
     * Returns captions of the view
     */
    public String getCaption() {
        if (g_sCaptionID == null) {
            return Language.getLanguageSpecificString(g_sCaptions, ",");
        } else {
            return Language.getString(g_sCaptionID);
        }
    }

    /**
     * Returns captions ID of the view
     */
    public String getCaptionID() {
        return g_sCaptionID;
    }

    /**
     * Sets captions ID for the view
     */
    public void setCaptionID(String sCaptionID) {
        g_sCaptionID = sCaptionID;
    }

    /**
     * Sets symbols for the view
     */
    public void setSymbols(String sSymbols) {
        g_oSymbols = new Symbols(sSymbols, true);
    }

    /**
     * Sets symbols for the view
     */
    public void setSymbols(String[] saSymbols) {
        g_oSymbols = new Symbols();
        g_oSymbols.setSymbols(saSymbols);
    }

    /**
     * Returns symbols of the view
     */
    public Symbols getSymbols() {
        return g_oSymbols;
    }

    /**
     * Sets symbols for the view
     */
    public void setSymbols(Symbols oSymbols) {
        g_oSymbols = oSymbols;
    }

    /**
     * Returns column heading id
     */
    public String getColumnHeadingsID() {
        return g_sColumnHeadingsID;
    }

    /**
     * Sets column heading id
     */
    public void setColumnHeadingsID(String sID) {
        g_sColumnHeadingsID = sID;
    }

    public String[] getColumnTooltips() {
        return g_asColumnTooltips;
    }

    public void setColumnTooltips(String[] g_asColumnTooltips) {
        this.g_asColumnTooltips = g_asColumnTooltips;
    }

    public void restColumnSettings() {
        g_aiColumnSettings = new int[g_asColumnHeadings.length][3];
    }

    public int[][] getColumnSettings() {
        return g_aiColumnSettings;
    }

    public void setColumnSettings(int[][] aiColumnSettings) {
        g_aiColumnSettings = aiColumnSettings;
    }

    /**
     * Returns column Width captions
     */
    public String[] getColumnWidthsx() {
        //Stringokenizer oTokens = Stringokenizer(g_sColumnHeadings,",");
//String[] asCols = new String[oTokens.]
        return g_asColumnWidths;
    }

    /**
     * Sets column width captions
     */
    public void setColumnWidthsx(String[] asColWidths) {
        g_asColumnWidths = asColWidths;
    }

    /**
     * Returns column heading captions
     */
    public String[] getColumnHeadings() {
        //Stringokenizer oTokens = Stringokenizer(g_sColumnHeadings,",");
//String[] asCols = new String[oTokens.]
        return g_asColumnHeadings;
    }

    /**
     * Sets column heading captions
     */
    public void setColumnHeadings(String[] asCols) {
        g_asColumnHeadings = asCols;
    }

    public boolean isSymbolType() {
        return (this.getMainType() == ViewSettingsManager.SYMBOL_VIEW);
    }

    /**
     * Check if this view is custom type
     */
    public boolean isCutomType() {
        return (this.getMainType() == ViewSettingsManager.CUSTOM_VIEW);
    }

    /**
     * Check if this view is custom type
     */
    public boolean isMISTType() {
        return (this.getMainType() == ViewSettingsManager.MIST_VIEW);
    }

    /**
     * Check if this view is custom type
     */
    public boolean isMarketType() {
        return (this.getMainType() == ViewSettingsManager.MARKET_VIEW);
    }

    public boolean isPortfolioType() {
        return (this.getMainType() == ViewSettingsManager.PORTFOLIO_VIEW);
    }

    public boolean isFunctionType() {
        return (this.getMainType() == ViewSettingsManager.FILTERED_VIEW);
    }

    public boolean isVolumeWatcherType() throws Exception {
        return (this.getID().equals("VOLUME_WATCHER_VIEW"));
    }

    public boolean isCashFlowWatcherType() throws Exception {
        return (this.getID().equals("CASHFLOW_WATCHER_VIEW"));
    }

    /**
     * Sets the rendering ID string for the table
     */
    public String getRenderingIDs() {
        return g_sRenderingIDs;
    }

    /**
     * return the rendering ID string for the table
     */
    public void setRenderingIDs(String sRenderingIDs) {
        int i = 0;
        g_asRenderingIDs = new String[sRenderingIDs.length()];
        {
            for (i = 0; i < g_asRenderingIDs.length; i++) {
                g_asRenderingIDs[i] = sRenderingIDs.substring(i, i + 1);
            }
        }
        g_sRenderingIDs = sRenderingIDs;
    }

    /**
     * Return the renderer ID for the given column
     */
    public int getRendererID(int iColumn) {
        try {
            return intValue(g_asRenderingIDs[iColumn]);
        } catch (Exception e) {
            return 0;
        }
    }

    protected int intValue(String sValue) throws Exception {
        try {
            return (Integer.parseInt(sValue.trim()));
        } catch (Exception e) {
            return sValue.toCharArray()[0];
        }
    }

    public boolean isLocationValid() {
        return (g_oLocation.getX() >= 0);
    }

    public Point getLocation() {
        return g_oLocation;
    }

    public void setLocation(Point oLocation) {
        g_oLocation = oLocation;
    }

    public Dimension getSize() {
        return g_oSize;
    }

    public void setSize(Dimension oSize) {
        g_oSize = oSize;
//        if (getID().equals("UNICHART")) {
//            SharedMethods.printLine("Testing chart sizes : "+oSize.getWidth()+" , "+oSize.getHeight(),true);
//        }
    }

    public void setDetach(String value) {
//        WindowWrapper oParent=(WindowWrapper)getParent();
//        System.out.println("Inside setDetach()");
//        try {
//            if (value.equals("1")) {
//                System.out.println("Inside if Condition......");
//                InternalFrame iframe = (InternalFrame) oParent;
//                ((DetachableRootPane)iframe.getRootPane()).detach();
//
//
//            }
//        } catch (Exception e) {
//
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }

        isDetached = value.equals("1");


    }

    public int getStyle() {
        return g_iStyle;
    }

    public void setStyle(int iStyle) {
        g_iStyle = iStyle;
    }

    public int getIndex() {
        return g_iIndex;
    }

    public void setIndex(int iIndex) {
        g_iIndex = iIndex;
    }

    public boolean isVisible() {
        return g_bVisible;
    }

    public void setVisible(boolean bVisible) {
        g_bVisible = bVisible;
    }

    private String getWindowStyle() {
        WindowWrapper oParent = (WindowWrapper) getParent();

        if (oParent != null) {
            try {
                return "" + oParent.getWindowStyle();
            } catch (Exception ex) {
                return "0";
            }
        } else {
            return "" + g_iStyle;
        }
//        return "0";
    }

    /**
     * Returns the string representation of the view
     */
    public String toString() {
        if (getParent() instanceof ClientTable) {
            ClientTable oParent = (ClientTable) getParent();

            switch (getType()) {
                case ViewSettingsManager.CUSTOM_VIEW:
                    return getType() + "|" + getID() + "|" +
                            getHiddenColumns() + "|" +
                            getColumns() + "|" + getFixedColumns() + "|" +
                            getFontString() + "|" + getWindowStyle() + "|" +
                            getCaptions() + "|" + getSymbols();
                case ViewSettingsManager.MARKET_VIEW:
                    return getType() + "|" + getID() + "|" +
                            getHiddenColumns() + "|" +
                            getColumns() + "|" + getFixedColumns() + "|" +
                            getFontString() + "|" + getWindowStyle() + "|" +
                            getCaptionID();
                case ViewSettingsManager.SECTOR_VIEW:
                    return getType() + "|" + getID() + "|" +
                            getHiddenColumns() + "|" +
                            getColumns() + "|" + getFixedColumns() + "|" +
                            getFontString() + "|" + getWindowStyle();
                case ViewSettingsManager.SUMMARY_VIEW:
                    return getType() + "|" + getID() + "|" +
                            getHiddenColumns() + "|" +
                            getColumns() + "|" + getFixedColumns() + "|" +
                            getFontString() + "|0|" +
                            getRenderingIDs() + "|" +
                            getCaptionID() + "|" + getColumnHeadingsID();
                case ViewSettingsManager.FOREX_VIEW:
                    return getType() + "|" + getID() + "|" +
                            getHiddenColumns() + "|" +
                            getColumns() + "|" + getFixedColumns() + "|" +
                            getFontString() + "|0|" +
                            getRenderingIDs() + "|" +
                            getCaptionID() + "|" + getColumnHeadingsID();
                default:
                    return null;
            }
        }
        return null;
    }

    private String getColumnSettings(JTable table) {
        try {
            String sTableSettings = "";
            int iColCount = table.getColumnCount();

            for (int i = 0; i < iColCount; i++) {
                TableColumn oCol = table.getColumn("" + i);
                sTableSettings += "" + i + "," +
                        oCol.getWidth() + "," +
                        table.convertColumnIndexToView(i) + ",";
            }
            return sTableSettings;
        } catch (Exception e) {
            return "0";
        }
    }

    public String toWorkspaceString(boolean isWorkspaceRequest) {
        try {
            WindowWrapper oParent = (WindowWrapper) getParent();
            JFrame detachFrame = null;
            try {
                if (oParent.isDetached()) {
                    detachFrame = (JFrame) oParent.getDetachedFrame();
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            String sTableSettings;
            if (oParent != null) {
                if (isSymbolType()) {
                    if (oParent.getWindowStyle() == ViewSettingsManager.STYLE_ICONIFIED) {
                        return null; // no need to save Iconified Frames. It becomes a nightmare when reopening them uplon load
                    }
                }
                sTableSettings = "";
                if (getTableNumber() == 1) {
                    if (oParent.getTable1() != null) {
                        sTableSettings = getColumnSettings(oParent.getTable1());
                    } else {
                        sTableSettings = getColumnSettings(oParent.getTabFoler(getID()));
                    }
                } else {
                    if (oParent.getTable2() != null) {
                        sTableSettings = getColumnSettings(oParent.getTable2());
                    } else {
                        sTableSettings = getColumnSettings(oParent.getTabFoler(getID()));
                    }
                }
            } else {
                try {
                    sTableSettings = "";
                    for (int i = 0; i < getColumnSettings().length; i++) {
                        sTableSettings = sTableSettings + getColumnSettings()[i][0] + ",";
                        sTableSettings = sTableSettings + getColumnSettings()[i][1] + ",";
                        sTableSettings = sTableSettings + getColumnSettings()[i][2] + ",";
                    }
                    sTableSettings = sTableSettings.substring(0, (sTableSettings.length() - 1));
                } catch (Exception e) {
                    sTableSettings = "";  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            String dataString = "";
            if (isCutomType()) {
                if (isWorkspaceRequest) {
                    dataString = getType() + "|" + getIDForWSP() + Meta.RS;
                }
                if (oParent.isDetached()) {
                    dataString = dataString + VC_COLUMNS + VC_FIELD_DELIMETER +
                            getColumns() + seperatorType +
                            VC_FONT + VC_FIELD_DELIMETER +
                            getFontString() + seperatorType +
                            VC_HEADER_FONT + VC_FIELD_DELIMETER +
                            getHeaderFontString() + seperatorType + VC_WINDOW_LOCATION + VC_FIELD_DELIMETER +
                            (int) detachFrame.getLocationOnScreen().getX() + "," +
                            (int) detachFrame.getLocationOnScreen().getY() + seperatorType + VC_WINDOW_SIZE + VC_FIELD_DELIMETER +
                            (int) detachFrame.getSize().getWidth() + "," +
                            (int) detachFrame.getSize().getHeight() + seperatorType +
                            VC_WINDOW_STYLE + VC_FIELD_DELIMETER +
                            getWindowStyle() + seperatorType +
                            VC_WINDOW_INDEX + VC_FIELD_DELIMETER +
                            oParent.getZOrder() + seperatorType +
                            VC_WINDOW_VISIBLE + VC_FIELD_DELIMETER +
                            oParent.isWindowVisible() + seperatorType +
                            VC_WINDOW_HEADER + VC_FIELD_DELIMETER +
                            isHeaderVisible() + seperatorType +
                            VC_FILTER + VC_FIELD_DELIMETER +
                            getFilter() + seperatorType +
                            VC_FILTER_TYPE + VC_FIELD_DELIMETER +
                            getFilterType() + seperatorType +
                            VC_SORT_COLUMN + VC_FIELD_DELIMETER +
                            getSortColumn() + seperatorType +
                            VC_SORT_ORDER + VC_FIELD_DELIMETER +
                            getSortOrder() + seperatorType +
                            VC_SPLIT_LOCATION + VC_FIELD_DELIMETER +
                            getSplitLocation() + seperatorType +
                            VC_COLUMN_SETTINGS + VC_FIELD_DELIMETER +
                            sTableSettings + seperatorType +
                            VC_CURRENCY + VC_FIELD_DELIMETER +
                            getCurrency() + seperatorType +
                            VC_WATCHLIST_FILTER + VC_FIELD_DELIMETER +
                            getWatchlistFilter() + seperatorType +
                            VC_DESKTOP_TYPE + VC_FIELD_DELIMETER +
                            getDesktopType() + seperatorType +
                            VC_CAPTIONS + VC_FIELD_DELIMETER +
                            UnicodeUtils.getUnicodeString(getCaptions()) + seperatorType +
                            //VC_SYMBOLS + VC_FIELD_DELIMETER +
                            //    getSymbols().toOroginalString() + seperatorType +
                            getPropertyTableData() + seperatorType +
                            getCustomThemeSettings() + seperatorType +
                            VC_DETACH + VC_FIELD_DELIMETER +
                            isDetached();
                    return dataString;
                } else {
                    dataString = dataString + VC_COLUMNS + VC_FIELD_DELIMETER +
                            getColumns() + seperatorType +
                            VC_FONT + VC_FIELD_DELIMETER +
                            getFontString() + seperatorType +
                            VC_HEADER_FONT + VC_FIELD_DELIMETER +
                            getHeaderFontString() + seperatorType + VC_WINDOW_LOCATION + VC_FIELD_DELIMETER +
                            (int) oParent.getLocation().getX() + "," +
                            (int) oParent.getLocation().getY() + seperatorType + VC_WINDOW_SIZE + VC_FIELD_DELIMETER +
                            (int) oParent.getSize().getWidth() + "," +
                            (int) oParent.getSize().getHeight() + seperatorType +
                            VC_WINDOW_STYLE + VC_FIELD_DELIMETER +
                            getWindowStyle() + seperatorType +
                            VC_WINDOW_INDEX + VC_FIELD_DELIMETER +
                            oParent.getZOrder() + seperatorType +
                            VC_WINDOW_VISIBLE + VC_FIELD_DELIMETER +
                            oParent.isWindowVisible() + seperatorType +
                            VC_WINDOW_HEADER + VC_FIELD_DELIMETER +
                            isHeaderVisible() + seperatorType +
                            VC_FILTER + VC_FIELD_DELIMETER +
                            getFilter() + seperatorType +
                            VC_FILTER_TYPE + VC_FIELD_DELIMETER +
                            getFilterType() + seperatorType +
                            VC_SORT_COLUMN + VC_FIELD_DELIMETER +
                            getSortColumn() + seperatorType +
                            VC_SORT_ORDER + VC_FIELD_DELIMETER +
                            getSortOrder() + seperatorType +
                            VC_SPLIT_LOCATION + VC_FIELD_DELIMETER +
                            getSplitLocation() + seperatorType +
                            VC_COLUMN_SETTINGS + VC_FIELD_DELIMETER +
                            sTableSettings + seperatorType +
                            VC_CURRENCY + VC_FIELD_DELIMETER +
                            getCurrency() + seperatorType +
                            VC_WATCHLIST_FILTER + VC_FIELD_DELIMETER +
                            getWatchlistFilter() + seperatorType +
                            VC_DESKTOP_TYPE + VC_FIELD_DELIMETER +
                            getDesktopType() + seperatorType +
                            VC_CAPTIONS + VC_FIELD_DELIMETER +
                            UnicodeUtils.getUnicodeString(getCaptions()) + seperatorType +
                            //VC_SYMBOLS + VC_FIELD_DELIMETER +
                            //    getSymbols().toOroginalString() + seperatorType +
                            getPropertyTableData() + seperatorType +
                            getCustomThemeSettings() + seperatorType +
                            VC_DETACH + VC_FIELD_DELIMETER +
                            isDetached();
                    return dataString;
                }

            } else if (isMISTType()) {
                if (isWorkspaceRequest) {
                    dataString = getType() + "|" + getIDForWSP() + Meta.RS;
                }
                if (oParent.isDetached()) {
                    dataString = dataString + VC_FONT + VC_FIELD_DELIMETER +
                            getFontString() + seperatorType +
                            VC_HEADER_FONT + VC_FIELD_DELIMETER +
                            getHeaderFontString() + seperatorType + VC_WINDOW_LOCATION + VC_FIELD_DELIMETER +
                            (int) detachFrame.getLocationOnScreen().getX() + "," +
                            (int) detachFrame.getLocationOnScreen().getY() + seperatorType + VC_WINDOW_SIZE + VC_FIELD_DELIMETER +
                            (int) detachFrame.getSize().getWidth() + "," +
                            (int) detachFrame.getSize().getHeight() + seperatorType +
                            VC_WINDOW_STYLE + VC_FIELD_DELIMETER +
                            getWindowStyle() + seperatorType +
                            VC_WINDOW_INDEX + VC_FIELD_DELIMETER +
                            oParent.getZOrder() + seperatorType +
                            VC_WINDOW_VISIBLE + VC_FIELD_DELIMETER +
                            oParent.isWindowVisible() + seperatorType +
                            VC_WINDOW_HEADER + VC_FIELD_DELIMETER +
                            isHeaderVisible() + seperatorType +
                            VC_SORT_COLUMN + VC_FIELD_DELIMETER +
                            getSortColumn() + seperatorType +
                            VC_SORT_ORDER + VC_FIELD_DELIMETER +
                            getSortOrder() + seperatorType +
                            VC_CAPTIONS + VC_FIELD_DELIMETER +
                            UnicodeUtils.getUnicodeString(getCaptions()) + seperatorType +
                            VC_SYMBOLS + VC_FIELD_DELIMETER +
                            getSymbols().toOroginalString() +
                            VC_COLUMN_SETTINGS + VC_FIELD_DELIMETER +
                            sTableSettings + seperatorType +
                            VC_CURRENCY + VC_FIELD_DELIMETER +
                            getCurrency() + seperatorType +
                            VC_WATCHLIST_FILTER + VC_FIELD_DELIMETER +
                            getWatchlistFilter() + seperatorType +
                            getPropertyTableData() + seperatorType +
                            VC_DETACH + VC_FIELD_DELIMETER +
                            isDetached();
                    return dataString;
                } else {
                    dataString = dataString + VC_FONT + VC_FIELD_DELIMETER +
                            getFontString() + seperatorType +
                            VC_HEADER_FONT + VC_FIELD_DELIMETER +
                            getHeaderFontString() + seperatorType + VC_WINDOW_LOCATION + VC_FIELD_DELIMETER +
                            (int) oParent.getLocation().getX() + "," +
                            (int) oParent.getLocation().getY() + seperatorType + VC_WINDOW_SIZE + VC_FIELD_DELIMETER +
                            (int) oParent.getSize().getWidth() + "," +
                            (int) oParent.getSize().getHeight() + seperatorType +
                            VC_WINDOW_STYLE + VC_FIELD_DELIMETER +
                            getWindowStyle() + seperatorType +
                            VC_WINDOW_INDEX + VC_FIELD_DELIMETER +
                            oParent.getZOrder() + seperatorType +
                            VC_WINDOW_VISIBLE + VC_FIELD_DELIMETER +
                            oParent.isWindowVisible() + seperatorType +
                            VC_WINDOW_HEADER + VC_FIELD_DELIMETER +
                            isHeaderVisible() + seperatorType +
                            VC_SORT_COLUMN + VC_FIELD_DELIMETER +
                            getSortColumn() + seperatorType +
                            VC_SORT_ORDER + VC_FIELD_DELIMETER +
                            getSortOrder() + seperatorType +
                            VC_CAPTIONS + VC_FIELD_DELIMETER +
                            UnicodeUtils.getUnicodeString(getCaptions()) + seperatorType +
                            VC_SYMBOLS + VC_FIELD_DELIMETER +
                            getSymbols().toOroginalString() +
                            VC_COLUMN_SETTINGS + VC_FIELD_DELIMETER +
                            sTableSettings + seperatorType +
                            VC_CURRENCY + VC_FIELD_DELIMETER +
                            getCurrency() + seperatorType +
                            VC_WATCHLIST_FILTER + VC_FIELD_DELIMETER +
                            getWatchlistFilter() + seperatorType +
                            getPropertyTableData() + seperatorType +
                            VC_DETACH + VC_FIELD_DELIMETER +
                            isDetached();
                    return dataString;
                }

            } else if (isPortfolioType()) {
                if (isWorkspaceRequest) {
                    dataString = getType() + "|" + getIDForWSP() + Meta.RS;
                }
                if (oParent.isDetached()) {
                    dataString = dataString + VC_COLUMNS + VC_FIELD_DELIMETER +
                            getColumns() + seperatorType +
                            VC_FONT + VC_FIELD_DELIMETER +
                            getFontString() + seperatorType +
                            VC_HEADER_FONT + VC_FIELD_DELIMETER +
                            getHeaderFontString() + seperatorType;
                    if (isWorkspaceRequest) {
                        dataString = dataString + VC_WINDOW_LOCATION + VC_FIELD_DELIMETER +
                                (int) detachFrame.getLocationOnScreen().getX() + "," +
                                (int) detachFrame.getLocationOnScreen().getY() + seperatorType;
                    }
                    dataString = dataString + VC_WINDOW_SIZE + VC_FIELD_DELIMETER +
                            (int) detachFrame.getSize().getWidth() + "," +
                            (int) detachFrame.getSize().getHeight() + seperatorType +
                            VC_WINDOW_STYLE + VC_FIELD_DELIMETER +
                            getWindowStyle() + seperatorType +
                            VC_WINDOW_INDEX + VC_FIELD_DELIMETER +
                            oParent.getZOrder() + seperatorType +
                            VC_WINDOW_VISIBLE + VC_FIELD_DELIMETER +
                            oParent.isWindowVisible() + seperatorType +
                            VC_WINDOW_HEADER + VC_FIELD_DELIMETER +
                            isHeaderVisible() + seperatorType +
                            VC_SORT_COLUMN + VC_FIELD_DELIMETER +
                            getSortColumn() + seperatorType +
                            VC_SORT_ORDER + VC_FIELD_DELIMETER +
                            getSortOrder() + seperatorType +
                            VC_COLUMN_SETTINGS + VC_FIELD_DELIMETER +
                            sTableSettings + seperatorType +
                            VC_CAPTIONS + VC_FIELD_DELIMETER +
                            UnicodeUtils.getUnicodeString(getCaptions()) + seperatorType +
                            getPropertyTableData() + seperatorType +
                            VC_DETACH + VC_FIELD_DELIMETER +
                            isDetached();
                    return dataString;
                } else {
                    dataString = dataString + VC_COLUMNS + VC_FIELD_DELIMETER +
                            getColumns() + seperatorType +
                            VC_FONT + VC_FIELD_DELIMETER +
                            getFontString() + seperatorType +
                            VC_HEADER_FONT + VC_FIELD_DELIMETER +
                            getHeaderFontString() + seperatorType;
                    if (isWorkspaceRequest) {
                        dataString = dataString + VC_WINDOW_LOCATION + VC_FIELD_DELIMETER +
                                (int) oParent.getLocation().getX() + "," +
                                (int) oParent.getLocation().getY() + seperatorType;
                    }
                    dataString = dataString + VC_WINDOW_SIZE + VC_FIELD_DELIMETER +
                            (int) oParent.getSize().getWidth() + "," +
                            (int) oParent.getSize().getHeight() + seperatorType +
                            VC_WINDOW_STYLE + VC_FIELD_DELIMETER +
                            getWindowStyle() + seperatorType +
                            VC_WINDOW_INDEX + VC_FIELD_DELIMETER +
                            oParent.getZOrder() + seperatorType +
                            VC_WINDOW_VISIBLE + VC_FIELD_DELIMETER +
                            oParent.isWindowVisible() + seperatorType +
                            VC_WINDOW_HEADER + VC_FIELD_DELIMETER +
                            isHeaderVisible() + seperatorType +
                            VC_SORT_COLUMN + VC_FIELD_DELIMETER +
                            getSortColumn() + seperatorType +
                            VC_SORT_ORDER + VC_FIELD_DELIMETER +
                            getSortOrder() + seperatorType +
                            VC_COLUMN_SETTINGS + VC_FIELD_DELIMETER +
                            sTableSettings + seperatorType +
                            VC_CAPTIONS + VC_FIELD_DELIMETER +
                            UnicodeUtils.getUnicodeString(getCaptions()) + seperatorType +
                            getPropertyTableData() + seperatorType +
                            VC_DETACH + VC_FIELD_DELIMETER +
                            isDetached();
                    return dataString;
                }

            } else {
                if (isWorkspaceRequest) {
                    dataString = getType() + "|" + getIDForWSP() + Meta.RS;
                }
                dataString = dataString + VC_COLUMNS + VC_FIELD_DELIMETER +
                        (getColumns().or(getFixedColumns())) + seperatorType +
                        VC_FONT + VC_FIELD_DELIMETER +
                        getFontString() + seperatorType +
                        VC_HEADER_FONT + VC_FIELD_DELIMETER +
                        getHeaderFontString() + seperatorType + VC_WINDOW_LOCATION + VC_FIELD_DELIMETER +
                        getLocationString(oParent) + seperatorType + VC_WINDOW_SIZE + VC_FIELD_DELIMETER +
                        getSizeString(oParent) + seperatorType +
                        VC_WINDOW_STYLE + VC_FIELD_DELIMETER +
                        getWindowStyle() + seperatorType +
                        VC_WINDOW_INDEX + VC_FIELD_DELIMETER +
                        getZOrder(oParent) + seperatorType +
                        VC_WINDOW_VISIBLE + VC_FIELD_DELIMETER +
                        isWindowVisibile(oParent) + seperatorType +
                        VC_WINDOW_HEADER + VC_FIELD_DELIMETER +
                        isHeaderVisible() + seperatorType +
                        VC_SORT_COLUMN + VC_FIELD_DELIMETER +
                        getSortColumn() + seperatorType +
                        VC_SORT_ORDER + VC_FIELD_DELIMETER +
                        getSortOrder() + seperatorType +
                        VC_SPLIT_LOCATION + VC_FIELD_DELIMETER +
                        getSplitLocation() + seperatorType +
                        VC_COLUMN_SETTINGS + VC_FIELD_DELIMETER +
                        sTableSettings + seperatorType +
                        VC_CURRENCY + VC_FIELD_DELIMETER +
                        getCurrency() + seperatorType +
                        VC_DESKTOP_TYPE + VC_FIELD_DELIMETER +
                        getDesktopType() + seperatorType +
                        VC_WATCHLIST_FILTER + VC_FIELD_DELIMETER +
                        getWatchlistFilter() + seperatorType +
                        getPropertyTableData() + seperatorType +
                        getCustomThemeSettings() + seperatorType +
                        VC_DETACH + VC_FIELD_DELIMETER +
                        isDetached();
                return dataString;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the string representation of the view
     */
    public String toWorkspaceString() {
        return toWorkspaceString(true);
    }

    private String getPropertyTableData() {
        Enumeration keys = propertiesTable.keys();
        StringBuffer record = new StringBuffer("");
        String key;

        while (keys.hasMoreElements()) {
            key = (String) keys.nextElement();
            record.append(key);
            record.append("" + VC_FIELD_DELIMETER);
            record.append((String) propertiesTable.get(key));
            record.append("" + seperatorType);
        }

        return record.toString();
    }

    private String getCustomThemeSettings() {
        try {
            WindowWrapper oParent = (WindowWrapper) getParent();
            if (oParent != null) {
                TableThemeSettings customSettings = ((TableThemeSettings) oParent.getTable1());
                String customSetting = "" +
                        CS_CUSTOMIZED + VC_FIELD_DELIMETER + customSettings.isCuatomThemeEnabled() + seperatorType +
                        CS_ROW1_FG + VC_FIELD_DELIMETER + customSettings.getRow1FGColor().getRGB() + seperatorType +
                        CS_ROW1_BG + VC_FIELD_DELIMETER + customSettings.getRow1BGColor().getRGB() + seperatorType +
                        CS_ROW2_FG + VC_FIELD_DELIMETER + customSettings.getRow2FGColor().getRGB() + seperatorType +
                        CS_ROW2_BG + VC_FIELD_DELIMETER + customSettings.getRow2BGColor().getRGB() + seperatorType +
                        CS_DOWN_FG + VC_FIELD_DELIMETER + customSettings.getDownFGColor().getRGB() + seperatorType +
                        CS_DOWN_BG + VC_FIELD_DELIMETER + customSettings.getDownBGColor().getRGB() + seperatorType +
                        CS_UP_FG + VC_FIELD_DELIMETER + customSettings.getUpFGColor().getRGB() + seperatorType +
                        CS_UP_BG + VC_FIELD_DELIMETER + customSettings.getUpBGColor().getRGB() + seperatorType +
                        CS_SELECTED_FG + VC_FIELD_DELIMETER + customSettings.getHighlighterFGColor().getRGB() + seperatorType +
                        CS_SELECTED_BG + VC_FIELD_DELIMETER + customSettings.getHighlighterBGColor().getRGB() + seperatorType +
                        CS_TRADE_ROW1_FG + VC_FIELD_DELIMETER + customSettings.getTradeFGColor1().getRGB() + seperatorType +
                        CS_TRADE_ROW1_BG + VC_FIELD_DELIMETER + customSettings.getTradeBGColor1().getRGB() + seperatorType +
                        CS_TRADE_ROW2_FG + VC_FIELD_DELIMETER + customSettings.getTradeFGColor2().getRGB() + seperatorType +
                        CS_TRADE_ROW2_BG + VC_FIELD_DELIMETER + customSettings.getTradeBGColor2().getRGB() + seperatorType +
                        CS_BID_ROW1_FG + VC_FIELD_DELIMETER + customSettings.getBidFGColor1().getRGB() + seperatorType +
                        CS_BID_ROW1_BG + VC_FIELD_DELIMETER + customSettings.getBidBGColor1().getRGB() + seperatorType +
                        CS_BID_ROW2_FG + VC_FIELD_DELIMETER + customSettings.getBidFGColor2().getRGB() + seperatorType +
                        CS_BID_ROW2_BG + VC_FIELD_DELIMETER + customSettings.getBidBGColor2().getRGB() + seperatorType +
                        CS_ASK_ROW1_FG + VC_FIELD_DELIMETER + customSettings.getAskFGColor1().getRGB() + seperatorType +
                        CS_ASK_ROW1_BG + VC_FIELD_DELIMETER + customSettings.getAskBGColor1().getRGB() + seperatorType +
                        CS_ASK_ROW2_FG + VC_FIELD_DELIMETER + customSettings.getAskFGColor2().getRGB() + seperatorType +
                        CS_ASK_ROW2_BG + VC_FIELD_DELIMETER + customSettings.getAskBGColor2().getRGB() + seperatorType +
                        CS_CHANGE_UP_FG + VC_FIELD_DELIMETER + customSettings.getChangeUPFGColor().getRGB() + seperatorType +
                        CS_CHANGE_DOWN_FG + VC_FIELD_DELIMETER + customSettings.getChangeDownFGColor().getRGB() + seperatorType +
                        CS_GRID_FG + VC_FIELD_DELIMETER + customSettings.getTableGridColor().getRGB() + seperatorType +
                        CS_BODY_GAP + VC_FIELD_DELIMETER + customSettings.getBodyGap() + seperatorType +
                        CS_GRID_ON + VC_FIELD_DELIMETER + customSettings.getGdidType() + seperatorType;
                try {
                    customSetting += "" + CS_MIN_FG + VC_FIELD_DELIMETER + customSettings.getMinFGColor().getRGB() + seperatorType +  //----- Added by Shanika --
                            CS_MIN_BG + VC_FIELD_DELIMETER + customSettings.getMinBGColor().getRGB() + seperatorType +
                            CS_MAX_FG + VC_FIELD_DELIMETER + customSettings.getMaxFGColor().getRGB() + seperatorType +
                            CS_MAX_BG + VC_FIELD_DELIMETER + customSettings.getMaxBGColor().getRGB() + seperatorType +
                            CS_WEEK52LOW_FG + VC_FIELD_DELIMETER + customSettings.getWeek52LowFGColor().getRGB() + seperatorType +
                            CS_WEEK52LOW_BG + VC_FIELD_DELIMETER + customSettings.getWeek52LowBGColor().getRGB() + seperatorType +
                            CS_WEEK52HIGH_FG + VC_FIELD_DELIMETER + customSettings.getWeek52HighFGColor().getRGB() + seperatorType +
                            CS_WEEK52HIGH_BG + VC_FIELD_DELIMETER + customSettings.getWeek52HighBGColor().getRGB() + seperatorType +
                            CS_MIN_ENABLED + VC_FIELD_DELIMETER + customSettings.isMinEnabled() + seperatorType +
                            CS_MAX_ENABLED + VC_FIELD_DELIMETER + customSettings.isMaxEnabled() + seperatorType +
                            CS_WK52LOW_ENABLED + VC_FIELD_DELIMETER + customSettings.isWk52LowEnabled() + seperatorType +
                            CS_WK52HIGH_ENABLED + VC_FIELD_DELIMETER + customSettings.isWk52HighEnabled();
                } catch (Exception ignored) {

                }
                return customSetting;
            } else {
                String customSetting = "" +
                        CS_CUSTOMIZED + VC_FIELD_DELIMETER + propertiesTable.get(CS_CUSTOMIZED) + seperatorType +
                        CS_ROW1_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_ROW1_FG) + seperatorType +
                        CS_ROW1_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_ROW1_BG) + seperatorType +
                        CS_ROW2_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_ROW2_FG) + seperatorType +
                        CS_ROW2_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_ROW2_BG) + seperatorType +
                        CS_DOWN_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_DOWN_FG) + seperatorType +
                        CS_DOWN_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_DOWN_BG) + seperatorType +
                        CS_UP_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_UP_FG) + seperatorType +
                        CS_UP_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_UP_BG) + seperatorType +
                        CS_SELECTED_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_SELECTED_FG) + seperatorType +
                        CS_SELECTED_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_SELECTED_BG) + seperatorType +
                        CS_TRADE_ROW1_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_TRADE_ROW1_FG) + seperatorType +
                        CS_TRADE_ROW1_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_TRADE_ROW1_BG) + seperatorType +
                        CS_TRADE_ROW2_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_TRADE_ROW2_FG) + seperatorType +
                        CS_TRADE_ROW2_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_TRADE_ROW2_BG) + seperatorType +
                        CS_BID_ROW1_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_BID_ROW1_FG) + seperatorType +
                        CS_BID_ROW1_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_BID_ROW1_BG) + seperatorType +
                        CS_BID_ROW2_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_BID_ROW2_FG) + seperatorType +
                        CS_BID_ROW2_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_BID_ROW2_BG) + seperatorType +
                        CS_ASK_ROW1_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_ASK_ROW1_FG) + seperatorType +
                        CS_ASK_ROW1_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_ASK_ROW1_BG) + seperatorType +
                        CS_ASK_ROW2_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_ASK_ROW2_FG) + seperatorType +
                        CS_ASK_ROW2_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_ASK_ROW2_BG) + seperatorType +
                        CS_CHANGE_UP_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_CHANGE_UP_FG) + seperatorType +
                        CS_CHANGE_DOWN_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_CHANGE_DOWN_FG) + seperatorType +
                        CS_GRID_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_GRID_FG) + seperatorType +
                        CS_BODY_GAP + VC_FIELD_DELIMETER + propertiesTable.get(CS_BODY_GAP) + seperatorType +
                        CS_GRID_ON + VC_FIELD_DELIMETER + propertiesTable.get(CS_GRID_ON) + seperatorType;

                try {
                    customSetting += "" + CS_MIN_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_MIN_FG) + seperatorType +   // ===Added by Shanika
                            CS_MIN_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_MIN_BG) + seperatorType +
                            CS_MAX_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_MAX_FG) + seperatorType +
                            CS_MAX_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_MAX_BG) + seperatorType +
                            CS_WEEK52LOW_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_WEEK52LOW_FG) + seperatorType +
                            CS_WEEK52LOW_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_WEEK52LOW_BG) + seperatorType +
                            CS_WEEK52HIGH_FG + VC_FIELD_DELIMETER + propertiesTable.get(CS_WEEK52HIGH_FG) + seperatorType +
                            CS_WEEK52HIGH_BG + VC_FIELD_DELIMETER + propertiesTable.get(CS_WEEK52HIGH_BG) + seperatorType +
                            CS_MIN_ENABLED + VC_FIELD_DELIMETER + propertiesTable.get(CS_MIN_ENABLED) + seperatorType +
                            CS_MAX_ENABLED + VC_FIELD_DELIMETER + propertiesTable.get(CS_MAX_ENABLED) + seperatorType +
                            CS_WK52LOW_ENABLED + VC_FIELD_DELIMETER + propertiesTable.get(CS_WK52LOW_ENABLED) + seperatorType +
                            CS_WK52HIGH_ENABLED + VC_FIELD_DELIMETER + propertiesTable.get(CS_WK52HIGH_ENABLED);
                } catch (Exception ignored) {

                }
                return customSetting;
            }

        } catch (Exception ex) {
            return "";
        }
    }


    public ViewSetting getObject() {
        try {

            return (ViewSetting) clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public Object clone() throws CloneNotSupportedException {
        propertiesTable = new Hashtable();
        return super.clone();
    }

    public void showColPos() {
        try {
            for (int i = 0; i < getColumnHeadings().length; i++) {
                System.out.println(getColumnSettings()[i][0] + " " + getColumnSettings()[i][2]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object getSupportingObject() {
        return supportingObject;
    }

    public void setSupportingObject(Object newSupportingObject) {
        supportingObject = newSupportingObject;
    }

    public int getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(int newSortColumn) {
        sortColumn = newSortColumn;
        if (temp) {
//            SharedMethods.printLine("Set ZERO "+newSortColumn,true);
        }
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int newSortOrder) {
        sortOrder = newSortOrder;
    }

    public boolean isHeaderVisible() {
        return headerVisible;
    }

    public void setHeaderVisible(boolean newHeaderVisible) {
        headerVisible = newHeaderVisible;
    }

    public int getSplitLocation() {
        return splitLocation;
    }

    public void setSplitLocation(int newSplitLocation) {
        splitLocation = newSplitLocation;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String newFilter) {
        if ((newFilter == null) || (newFilter.toLowerCase().equals("null")))
            filter = null;
        else
            filter = newFilter;
    }

    public int getFilterType() {
        return filterType;
    }

    public void setFilterType(int newFilterType) {
        filterType = newFilterType;
    }

    public String getProperty(int key) {
        return (String) propertiesTable.get("" + key);
    }

    public void putProperty(int key, String property) {
        propertiesTable.put("" + key, property);
    }

    public void putProperty(int key, boolean property) {
        propertiesTable.put("" + key, "" + property);
    }

    public void putProperty(int key, int property) {
        propertiesTable.put("" + key, "" + property);
    }

    public void removeProperty(int key) {
        propertiesTable.remove("" + key);
    }

    public void setWorkspaceRecord(String record) {
        StringTokenizer fields = new StringTokenizer(record, seperatorType);
        DataDisintegrator pair = new DataDisintegrator();
        pair.setSeperator(VC_FIELD_DELIMETER);
        propertiesTable.clear();
        String value;
        while (fields.hasMoreTokens()) {
            try {
                pair.setData(fields.nextToken());
                value = pair.getData();

                switch (SharedMethods.intValue(pair.getTag(), 0)) {
                    case VC_COLUMNS:
//                        System.out.println("column value="+value);
                        setColumns(SharedMethods.bigIntValue(value, BigInteger.valueOf(0)));
                        break;
                    case VC_FONT:
                        setFont(SharedMethods.fontObject(value, Theme.getDefaultFont())); //.DEFAULT_FONT));
                        break;
                    case VC_HEADER_FONT:
                        setHeaderFont(SharedMethods.fontObject(value, Theme.getDefaultFont())); //Theme.DEFAULT_FONT));
                        break;
                    case VC_SORT_COLUMN:
                        setSortColumn(SharedMethods.intValue(value, 0));
                        break;
                    case VC_SORT_ORDER:
                        setSortOrder(SharedMethods.intValue(value, 0));
                        break;
                    case VC_SPLIT_LOCATION:
                        setSplitLocation(SharedMethods.intValue(value, 0));
                        break;
                    case VC_COLUMN_SETTINGS:
                        //setColumnSettings(SharedMethods.getColumnSettings(value));
//                        if(getID().equals("7\u0019KSE\u0019R")){
//                            System.out.println("loaded column settings = "+value);
//                        }
                        GUISettings.setColumnSettings(this, value);
                        break;
                    case VC_WINDOW_LOCATION:
                        setLocation(SharedMethods.getPoint(value));
                        break;
                    case VC_WINDOW_SIZE:
                        setSize(SharedMethods.getDimension(value));
                        break;
                    case VC_WINDOW_STYLE:
                        setStyle(SharedMethods.intValue(value, 0));
                        break;
                    case VC_WINDOW_INDEX:
                        setIndex(SharedMethods.intValue(value, 0));
                        break;
                    case VC_WINDOW_VISIBLE:
                        setVisible(value.equals("true"));
                        break;
                    case VC_WINDOW_HEADER:
                        setHeaderVisible(value.equals("true"));
                        break;
                    case VC_FILTER:
                        setFilter(value);
                        break;
                    case VC_FILTER_TYPE:
                        setFilterType(SharedMethods.intValue(value, 0));
                        break;
                    case VC_PORTFOLIO_DATA:
                        ((PortfolioStore) getSupportingObject()).setData(value);
                        break;
                    case VC_TnS_SUMMARY:
                        propertiesTable.put("" + VC_TnS_SUMMARY, value);
                        break;
                    case VC_CURRENCY:
                        setCurrency(value);
                        break;
                    case VC_WATCHLIST_FILTER:
                        setWatchlistFilter(value);
                        break;
                    case VC_DESKTOP_TYPE:
                        setDesktopType(Byte.parseByte(value));
                        break;
                    case VC_DETACH:
                        setDetach(value);
                        break;
//                     case VC_CAPTIONS:
//                        setCaptions(UnicodeUtils.getNativeString(value));
//                        break;
                    default:
                        if ((pair != null) && (pair.getTag() != null)) {
                            propertiesTable.put(pair.getTag(), value);
                        }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        fields = null;
        pair = null;
    }

    public void updateMISTCustomSettings(MISTTableSettings target) {
        Enumeration keys = propertiesTable.keys();
        String key = null;
        while (keys.hasMoreElements()) {
            key = (String) keys.nextElement();
//            System.out.println(">>> " + key + " = " + (String)propertiesTable.get(key));
            if (Integer.parseInt(key.trim()) == CS_TABLE_SETTINGS) {
                target.setWorkspaceString((String) propertiesTable.get(key));
            } else if (Integer.parseInt(key.trim()) == CS_CUSTOMIZED) {
                target.setGridOn(SharedMethods.booleanValue((String) propertiesTable.get(key), false));
                (((MISTTable) g_oParent).getTable().getSmartTable()).setCustomThemeEnabled(SharedMethods.booleanValue((String) propertiesTable.get(key), false));
            } else if (Integer.parseInt(key.trim()) != VC_CAPTIONS)
                processTableSettings(target, (String) propertiesTable.get(key));
        }
    }

    private void processTableSettings(MISTTableSettings target, String sValue) {
        String sComplexTag = null;
        StringTokenizer st = null;
        StringTokenizer st2 = null;

        try {
            st = new StringTokenizer(sValue, SM_START_REC + "", false);
            st.nextToken();
            sComplexTag = st.nextToken();
            st2 = new StringTokenizer(sComplexTag, SM_FIELD_DELIM + "", false);
            target.setGridColor(new Color(Integer.parseInt(st2.nextToken())));
            target.setSelectedColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setSelectedColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setDescColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setDescColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setPrevClosedColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setPrevClosedColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setVwapColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setVwapColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            sComplexTag = null;

            sComplexTag = st.nextToken();
            st2 = new StringTokenizer(sComplexTag, SM_FIELD_DELIM + "", false);
            target.setLowTradeColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLowTradeColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setHighTradeColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setHighTradeColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setTradesColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setTradesColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setVolumeColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setVolumeColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastTradeQtyColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastTradeQtyColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            sComplexTag = null;

            sComplexTag = st.nextToken();
//            System.out.println("3 >>> = " + sComplexTag + "-");
            st2 = new StringTokenizer(sComplexTag, SM_FIELD_DELIM + "", false);
            target.setLowAskColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLowAskColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setHighAskColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setHighAskColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLowAskQtyColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLowAskQtyColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setTotAskQtyColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setTotAskQtyColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastAskColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastAskColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastAskQtyColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastAskQtyColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            sComplexTag = null;

            sComplexTag = st.nextToken();
//            System.out.println("4 >>> = " + sComplexTag + "-");
            st2 = new StringTokenizer(sComplexTag, SM_FIELD_DELIM + "", false);
            target.setLowBidColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLowBidColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setHighBidColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setHighBidColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setHighBidQtyColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setHighBidQtyColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setTotBidQtyColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setTotBidQtyColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastBidColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastBidColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastBidQtyColumnBG(new Color(Integer.parseInt(st2.nextToken())));
            target.setLastBidQtyColumnFG(new Color(Integer.parseInt(st2.nextToken())));
            sComplexTag = null;

            target.setBodyFont(getFont());
            target.setHeadingFont(getHeaderFont());
        } catch (Exception ex) {
//            ex.printStackTrace();
        }
        sComplexTag = null;
        st = null;
        st2 = null;
    }

    public void updateCustomSettings(TableThemeSettings target) {
        Enumeration keys = propertiesTable.keys();
        String key;
        while (keys.hasMoreElements()) {
            key = (String) keys.nextElement();
            try {
                switch (SharedMethods.intValue(key)) {
                    case CS_CUSTOMIZED:
                        target.setCustomThemeEnabled(SharedMethods.booleanValue((String) propertiesTable.get(key), false));
                        propertiesTable.remove(key);
                        break;
                    case CS_ROW1_FG:
                        target.setRow1FGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_ROW1_BG:
                        target.setRow1BGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_ROW2_FG:
                        target.setRow2FGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_ROW2_BG:
                        target.setRow2BGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_DOWN_FG:
                        target.setDownFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_DOWN_BG:
                        target.setDownBGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_UP_FG:
                        target.setUpFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_UP_BG:
                        target.setUpBGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_SELECTED_FG:
                        target.setHighlighterFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_SELECTED_BG:
                        target.setHighlighterBGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_TRADE_ROW1_FG:
                        target.setTradeFGColor1(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_TRADE_ROW1_BG:
                        target.setTradeBGColor1(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_TRADE_ROW2_FG:
                        target.setTradeFGColor2(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_TRADE_ROW2_BG:
                        target.setTradeBGColor2(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_BID_ROW1_FG:
                        target.setBidFGColor1(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_BID_ROW1_BG:
                        target.setBidBGColor1(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_BID_ROW2_FG:
                        target.setBidFGColor2(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_BID_ROW2_BG:
                        target.setBidBGColor2(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_ASK_ROW1_FG:
                        target.setAskFGColor1(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_ASK_ROW1_BG:
                        target.setAskBGColor1(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_ASK_ROW2_FG:
                        target.setAskFGColor2(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_ASK_ROW2_BG:
                        target.setAskBGColor2(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_CHANGE_UP_FG:
                        target.setChangeUPFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_CHANGE_DOWN_FG:
                        target.setChangeDownFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_GRID_FG:
                        target.setTableGridColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_BODY_GAP:
                        target.setBodyGap(SharedMethods.intValue((String) propertiesTable.get(key)));
                        propertiesTable.remove(key);
                        break;
                    case CS_GRID_ON:
                        target.setGdidOn(SharedMethods.intValue((String) propertiesTable.get(key), TableThemeSettings.GRID_NONE));
                        propertiesTable.remove(key);
                        break;
                    //----Added by Shanika --------
                    case CS_MIN_FG:
                        target.setMinFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_MIN_BG:
                        target.setMinBGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_MAX_FG:
                        target.setMaxFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_MAX_BG:
                        target.setMaxBGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_WEEK52LOW_FG:
                        target.setWeek52LowFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_WEEK52LOW_BG:
                        target.setWeek52LowBGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_WEEK52HIGH_FG:
                        target.setWeek52HighFGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_WEEK52HIGH_BG:
                        target.setWeek52HighBGColor(new Color(SharedMethods.intValue((String) propertiesTable.get(key))));
                        propertiesTable.remove(key);
                        break;
                    case CS_MIN_ENABLED:
                        target.setMinEnabled(SharedMethods.booleanValue((String) propertiesTable.get(key), false));
                        propertiesTable.remove(key);
                        break;
                    case CS_MAX_ENABLED:
                        target.setMaxEnabled(SharedMethods.booleanValue((String) propertiesTable.get(key), false));
                        propertiesTable.remove(key);
                        break;
                    case CS_WK52LOW_ENABLED:
                        target.setWk52LowEnabled(SharedMethods.booleanValue((String) propertiesTable.get(key), false));
                        propertiesTable.remove(key);
                        break;
                    case CS_WK52HIGH_ENABLED:
                        target.setWk52HighEnabled(SharedMethods.booleanValue((String) propertiesTable.get(key), false));
                        propertiesTable.remove(key);
                        break;

                }
            } catch (Exception e) {

            }
            //propertiesTable.remove(key);
        }
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public byte getDesktopType() {
        return desktopType;
    }

    public void setDesktopType(byte desktopType) {
        this.desktopType = desktopType;
    }

    public String isDetached() {
        try {
            WindowWrapper oParent = (WindowWrapper) getParent();
            if (oParent.isDetached()) {
                return "1";
            } else {
                return "0";
            }
        } catch (Exception e) {
            return "0";
        }
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }
}

