package com.isi.csvr.properties;

import com.isi.csvr.Client;
import com.isi.csvr.MultiDialogWindow;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.shared.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 14, 2009
 * Time: 1:41:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class WorkspaceAdjuster {

    private static WorkspaceAdjuster self;
    private ViewSetting fullQuotemain;
    private ViewSetting fullQuoteDepthBid;
    private ViewSetting fullQuoteDepthAsk;
    private ViewSetting fullQuotTamiandsales;

    private WorkspaceAdjuster() {

    }

    public static WorkspaceAdjuster getSharedInstance() {
        if (self == null) {
            self = new WorkspaceAdjuster();
        }
        return self;
    }


    public String createFullQuoteSymbolView(ViewSetting multiDialog) {
        try {
            createSubViewSettings(multiDialog);
            String sSelectedSymbol = (multiDialog.getProperty(ViewConstants.SELECTED_SYMBOL));
            if (!SharedMethods.isFullKey(sSelectedSymbol)) {
                sSelectedSymbol = sSelectedSymbol + "~0";
            }
            try {
                multiDialog.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sSelectedSymbol);
            } catch (Exception e) {
                multiDialog.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, "");
            }
            multiDialog.putProperty(ViewConstants.VC_LINKED, LinkStore.LINK_NONE);
//        try {
//            multiDialog.putProperty(ViewConstants.SECOND_SPLIT_LOCATION,bottomsplitPane.getDividerLocation());
//        } catch (Exception e) {
//            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
            try {
                multiDialog.putProperty(ViewConstants.SUB_WINDOW_TYPES, getSubWindowTypesString());
                multiDialog.putProperty(ViewConstants.SUB_WINDOW_VIEWS, getSubWindowViews());
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            String wspSetting = multiDialog.toWorkspaceString();
            String replacement = Client.getInstance().getViewSettingsManager().getSymbolView("FULL_QUOTE_VIEW").getType() +
                    "|" + System.currentTimeMillis() + Meta.RS;
            String[] data = wspSetting.split(Meta.RS);
            String adjusted = replacement + data[1];
            return adjusted;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }

    private void createSubViewSettings(ViewSetting multiDialog) {
        String subTypes = multiDialog.getProperty(ViewConstants.SUB_WINDOW_TYPES);
        String subViews = multiDialog.getProperty(ViewConstants.SUB_WINDOW_VIEWS);
        fullQuoteDepthAsk = null;
        fullQuoteDepthBid = null;
        fullQuotTamiandsales = null;
        long timestamp = System.currentTimeMillis();
        try {
            if (subTypes != null) {
                String[] subWindowViews = subViews.split(":");
                String[] subWindows = subTypes.split(":");
                for (int i = 0; i < subWindows.length; i++) {
                    String windowViewString = subWindowViews[i];
                    windowViewString = windowViewString.replaceFirst("\\[", "");
                    windowViewString = windowViewString.replaceFirst("\\]", "");
                    if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.DEPTH_PRICE_BID_VIEW)) {

                        String[] views = windowViewString.split("/");

                        fullQuoteDepthBid = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW);
                        if (fullQuoteDepthBid == null) {
                            fullQuoteDepthBid = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_PRICE_BID_VIEW");
                            fullQuoteDepthBid = fullQuoteDepthBid.getObject();
                            fullQuoteDepthBid.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                            fullQuoteDepthBid.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            fullQuoteDepthBid.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            //    fullQuoteDepthBid.setSize(this.getSize());
                            //    Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW, fullQuoteDepthBid);
                            GUISettings.setColumnSettings(fullQuoteDepthBid, TWColumnSettings.getItem("DEPTH_PRICE_COLS"));
                        }
                        try {
                            if (views[0].indexOf("\u001E") > 0) {
                                fullQuoteDepthBid.setWorkspaceRecord(views[0].split("\u001E")[1]);
                            } else {
                                fullQuoteDepthBid.setWorkspaceRecord(views[0]);
                            }
//                            fullQuoteDepthBid.setWorkspaceRecord(views[0].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        fullQuoteDepthAsk = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
                        if (fullQuoteDepthAsk == null) {
                            fullQuoteDepthAsk = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_PRICE_ASK_VIEW");
                            fullQuoteDepthAsk = fullQuoteDepthAsk.getObject();
                            fullQuoteDepthAsk.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                            fullQuoteDepthAsk.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            fullQuoteDepthAsk.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            //  fullQuoteDepthAsk.setSize(this.getSize());
                            //  Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW, fullQuoteDepthAsk);
                            GUISettings.setColumnSettings(fullQuoteDepthAsk, TWColumnSettings.getItem("DEPTH_PRICE_COLS"));
                        }
                        try {
                            if (views[1].indexOf("\u001E") > 0) {
                                fullQuoteDepthAsk.setWorkspaceRecord(views[1].split("\u001E")[1]);
                            } else {
                                fullQuoteDepthAsk.setWorkspaceRecord(views[1]);
                            }
                        } catch (Exception e) {
//                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        fullQuoteDepthBid.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                        fullQuoteDepthAsk.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);


//                        if (fullQuotTamiandsales.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
//                            fullQuoteDepthBid.putProperty(ViewConstants.VC_LINKED, true);
//                        fullQuoteDepthAsk.putProperty(ViewConstants.VC_LINKED, true);
//                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + LinkStore.linked + "_" + fullQuotTamiandsales.getProperty(ViewConstants.VC_LINKED_GROUP), fullQuoteDepthBid);
//                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + LinkStore.linked + "_" + fullQuotTamiandsales.getProperty(ViewConstants.VC_LINKED_GROUP), fullQuoteDepthAsk);
//                        } else {
                        fullQuoteDepthBid.putProperty(ViewConstants.VC_LINKED, false);
                        fullQuoteDepthAsk.putProperty(ViewConstants.VC_LINKED, false);
//                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + fullQuotTamiandsales.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), fullQuoteDepthBid);
//                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + fullQuotTamiandsales.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), fullQuoteDepthAsk);
//                        }


                    } else if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.DEPTH_BID_VIEW)) {
                        String[] views = windowViewString.split("/");
                        fullQuoteDepthBid = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_BID_VIEW);
                        if (fullQuoteDepthBid == null) {
                            fullQuoteDepthBid = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_BID_VIEW");
                            fullQuoteDepthBid = fullQuoteDepthBid.getObject();
                            fullQuoteDepthBid.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                            fullQuoteDepthBid.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            fullQuoteDepthBid.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            // fullQuoteDepthBid.setSize(this.getSize());
                            //   Client.getInstance().getViewSettingsManager().putFullQuoteSubView(""+ViewSettingsManager.DEPTH_BID_VIEW, fullQuoteDepthBid);
                            GUISettings.setColumnSettings(fullQuoteDepthBid, TWColumnSettings.getItem("DEPTH_ORDER_COLS"));
                        }
                        try {
                            if (views[0].indexOf("\u001E") > 0) {
                                fullQuoteDepthBid.setWorkspaceRecord(views[0].split("\u001E")[1]);
                            } else {
                                fullQuoteDepthBid.setWorkspaceRecord(views[0]);
                            }
//                            fullQuoteDepthBid.setWorkspaceRecord(views[0].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        fullQuoteDepthAsk = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_ASK_VIEW);
                        if (fullQuoteDepthAsk == null) {
                            fullQuoteDepthAsk = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_ASK_VIEW");
                            fullQuoteDepthAsk = fullQuoteDepthAsk.getObject();
                            fullQuoteDepthAsk.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                            fullQuoteDepthAsk.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            fullQuoteDepthAsk.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            //  fullQuoteDepthAsk.setSize(this.getSize());
                            Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_ASK_VIEW, fullQuoteDepthAsk);
                            GUISettings.setColumnSettings(fullQuoteDepthAsk, TWColumnSettings.getItem("DEPTH_ORDER_COLS"));
                        }
                        try {
                            if (views[1].indexOf("\u001E") > 0) {
                                fullQuoteDepthAsk.setWorkspaceRecord(views[1].split("\u001E")[1]);
                            } else {
                                fullQuoteDepthAsk.setWorkspaceRecord(views[1]);
                            }
//                            fullQuoteDepthAsk.setWorkspaceRecord(views[1].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    } else if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW)) {
                        String[] views = windowViewString.split("/");
                        fullQuoteDepthBid = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW);
                        if (fullQuoteDepthBid == null) {
                            fullQuoteDepthBid = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_SPECIAL_BID_VIEW");
                            fullQuoteDepthBid = fullQuoteDepthBid.getObject();
                            fullQuoteDepthBid.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                            fullQuoteDepthBid.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            fullQuoteDepthBid.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            //  fullQuoteDepthBid.setSize(this.getSize());
                            Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW, fullQuoteDepthBid);
                            GUISettings.setColumnSettings(fullQuoteDepthBid, TWColumnSettings.getItem("DEPTH_SPECIAL_COLS"));
                        }
                        try {
                            fullQuoteDepthBid.setWorkspaceRecord(views[0].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        fullQuoteDepthAsk = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW);
                        if (fullQuoteDepthAsk == null) {
                            fullQuoteDepthAsk = Client.getInstance().getViewSettingsManager().getSymbolView("DEPTH_SPECIAL_ASK_VIEW");
                            fullQuoteDepthAsk = fullQuoteDepthAsk.getObject();
                            fullQuoteDepthAsk.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                            fullQuoteDepthAsk.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            fullQuoteDepthAsk.setID(MultiDialogWindow.TABLE_ID);  //selectedKey+ "*"+type);
                            //   fullQuoteDepthAsk.setSize(this.getSize());
                            Client.getInstance().getViewSettingsManager().putFullQuoteSubView("" + ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW, fullQuoteDepthAsk);
                            GUISettings.setColumnSettings(fullQuoteDepthAsk, TWColumnSettings.getItem("DEPTH_SPECIAL_COLS"));
                        }
                        try {
                            fullQuoteDepthAsk.setWorkspaceRecord(views[1].split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    } else if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.TIMEnSALES_VIEW2)) {
                        fullQuotTamiandsales = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2);   //sSelectedSymbol + "*"+ type);
                        if (fullQuotTamiandsales == null) {
                            fullQuotTamiandsales = Client.getInstance().getViewSettingsManager().getSymbolView("TIME_AND_SALES");
                            fullQuotTamiandsales = fullQuotTamiandsales.getObject(); // clone the object
                            fullQuotTamiandsales.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                            fullQuotTamiandsales.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            fullQuotTamiandsales.setID(MultiDialogWindow.TABLE_ID);   //sSelectedSymbol+ "*"+type);
                            //   fullQuotTamiandsales.setSize(this.getSize());
                            // Client.getInstance().getViewSettingsManager().putFullQuoteSubView(""+ViewSettingsManager.TIMEnSALES_VIEW2, fullQuotTamiandsales);
                            GUISettings.setColumnSettings(fullQuotTamiandsales, TWColumnSettings.getItem("SYMBOL_TIME_N_SALES_COLS"));
                        }
                        try {
                            if (windowViewString.indexOf("\u001E") > 0) {
                                fullQuotTamiandsales.setWorkspaceRecord(windowViewString.split("\u001E")[1]);
                            } else {
                                fullQuotTamiandsales.setWorkspaceRecord(windowViewString);
                            }
//                            fullQuotTamiandsales.setWorkspaceRecord(windowViewString.split("\u001E")[1]);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        fullQuotTamiandsales.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);


//                        if (fullQuotTamiandsales.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
//                            fullQuotTamiandsales.putProperty(ViewConstants.VC_LINKED, true);
//                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + LinkStore.linked + "_" + this.fullQuotTamiandsales.getProperty(ViewConstants.VC_LINKED_GROUP), fullQuotTamiandsales);
//                        } else {
                        fullQuotTamiandsales.putProperty(ViewConstants.VC_LINKED, false);
//                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + this.fullQuotTamiandsales.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), fullQuotTamiandsales);
//                        }
                    } else if ((Integer.parseInt(subWindows[i]) == ViewSettingsManager.COMBINED_DEPTH_VIEW)) {
                        fullQuotTamiandsales = Client.getInstance().getViewSettingsManager().
                                getFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW);   //sSelectedSymbol + "*"+ type);
                        if (fullQuotTamiandsales == null) {
                            fullQuotTamiandsales = Client.getInstance().getViewSettingsManager().getSymbolView("COMBINED_DEPTH");
                            fullQuotTamiandsales = fullQuotTamiandsales.getObject(); // clone the object
                            fullQuotTamiandsales.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                            fullQuotTamiandsales.setDesktopType(Constants.FULL_QUOTE_TYPE);
                            fullQuotTamiandsales.setID(MultiDialogWindow.TABLE_ID);   //sSelectedSymbol+ "*"+type);
                            //       fullQuotTamiandsales.setSize(this.getSize());
                            //  Client.getInstance().getViewSettingsManager().putFullQuoteSubView(""+ViewSettingsManager.COMBINED_DEPTH_VIEW, fullQuotTamiandsales);
                            GUISettings.setColumnSettings(fullQuotTamiandsales, TWColumnSettings.getItem("COMBINED_ORDER_COLS"));
                        }
                        try {
                            if (windowViewString.indexOf("\u001E") > 0) {
                                fullQuotTamiandsales.setWorkspaceRecord(windowViewString.split("\u001E")[1]);
                            } else {
                                fullQuotTamiandsales.setWorkspaceRecord(windowViewString);
                            }
//                            fullQuotTamiandsales.setWorkspaceRecord(windowViewString.split("\u001E")[1]);
                        } catch (Exception e) {
//                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        fullQuotTamiandsales.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);


//                        if (fullQuotTamiandsales.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
//                            fullQuotTamiandsales.putProperty(ViewConstants.VC_LINKED, true);
//                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + LinkStore.linked + "_" + this.fullQuotTamiandsales.getProperty(ViewConstants.VC_LINKED_GROUP), fullQuotTamiandsales);
//                        } else {
                        fullQuotTamiandsales.putProperty(ViewConstants.VC_LINKED, false);
//                           this.fullQuoteDepthAsk = fullQuoteDepthAsk;
//                           this.fullQuoteDepthBid = fullQuoteDepthBid;
//                           fullQuotTamiandsales = fullQuotTamiandsales;
//                            g_oViewSettingsManager.putFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + this.fullQuotTamiandsales.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), fullQuotTamiandsales);
//                        }
                    }
                }
                // isViewSettingsCreated = true;
            } else {
                //  isViewSettingsCreated = false;
            }
        } catch (Exception e) {
            // isViewSettingsCreated = false;
        }
//        System.out.println("create sub viewsettings ended");

    }

    public String getSubWindowTypesString() {
        //todo added to temparary purpose
        String str = "";
        if (Settings.getFullQuoteMDepthType() == MultiDialogWindow.COMBINED_MDEPTH_TYPE) {
            str = "" + ViewSettingsManager.COMBINED_DEPTH_VIEW;
        } else {
            str = "" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW;
        }
        str = str + ":";
        str = str + ViewSettingsManager.TIMEnSALES_VIEW2;
        return str;

        //todo removed for temparary
//        String str = "";
//        Enumeration<String> keys = subWindows.keys();
//        while(keys.hasMoreElements()){
//            str= str +":";
//            str = str + keys.nextElement();
//        }
//        str = str.replaceFirst(":","");
//        return str;
    }

    public String getSubWindowViews() {
        //todo added to temparary purpose
        String str = "";

        try {
            if (Settings.getFullQuoteMDepthType() == MultiDialogWindow.COMBINED_MDEPTH_TYPE) {
                //  str = "[" + Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.COMBINED_DEPTH_VIEW).toWorkspaceString();
                str = "[" + fullQuoteDepthBid.toWorkspaceString();
                str = str + "]";
            } else {
//                str = "[" + Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_BID_VIEW).toWorkspaceString();
//                str = str + " / " + Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.DEPTH_PRICE_ASK_VIEW).toWorkspaceString() + "]";
                str = "[" + fullQuoteDepthBid.toWorkspaceString();
                str = str + " / " + fullQuoteDepthAsk.toWorkspaceString() + "]";
            }
            str = str + ":";
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        // str = str + "[" + Client.getInstance().getViewSettingsManager().getFullQuoteSubView("" + ViewSettingsManager.TIMEnSALES_VIEW2).toWorkspaceString() + "]";
        str = str + "[" + fullQuotTamiandsales.toWorkspaceString() + "]";
        return str;

        //todo removed for temparary
//        String str = "";
//        Enumeration<String> keys = subWindows.keys();
//        while(keys.hasMoreElements()){
//            int windowType =Integer.parseInt(keys.nextElement());//  subWindows.get(i).getWindowType();
//            str= str +":";
//            if((windowType == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) || (windowType == ViewSettingsManager.DEPTH_BID_VIEW)
//                    || (windowType == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW)){
//                str = str + "[" + marketDepth.getTableComponent().getModel().getViewSettings().toWorkspaceString() +" / "
//                            + marketDepth.getSubTableComponent().getModel().getViewSettings().toWorkspaceString() +"]";
//            }else {
//                str = str + "[" + timeAndSales.getViewSetting().toWorkspaceString() +"]";
//            }
//        }
//        str = str.replaceFirst(":","");
//        return str;
    }
}
