// Copyright (c) 2000 Home
package com.isi.csvr.properties;

import com.isi.csvr.*;
import com.isi.csvr.chart.*;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.PortfolioStore;
import com.isi.csvr.dnd.DraggableTable;
import com.isi.csvr.event.Application;
import com.isi.csvr.forex.ForexBoard;
import com.isi.csvr.heatmap.HeatManager;
import com.isi.csvr.heatmap.HeatMapFrame;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.mist.MISTModel;
import com.isi.csvr.mist.MISTTable;
import com.isi.csvr.mist.MISTTableSettings;
import com.isi.csvr.plugin.PluginStore;
import com.isi.csvr.sectoroverview.SectorOverviewUI;
import com.isi.csvr.sectoroverview.SectorOverviewUpdator;
import com.isi.csvr.shared.*;
import com.isi.csvr.sideBar.RendererComponent;
import com.isi.csvr.sideBar.SideBar;
import com.isi.csvr.sideBar.SideBarOptionDialog;
import com.isi.csvr.sideBar.SymbolTreeRenderer;
import com.isi.csvr.table.IndexPanel;
import com.isi.csvr.table.StretchedIndexPanel;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.advanced.*;
import com.isi.csvr.ticker.custom.Ticker;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.trading.TradeMethods;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class WorkspaceManager implements ViewConstants {

    private static boolean loadingWorkspace = false;
    private static boolean marketViewsAvailable;
    private static boolean workspaceLoaded = false;
    private SmartProperties g_oSettings;
    private ViewSettingsManager g_oViewSettingsManager;
    private Client g_oClient;
    private int g_iType;
    private String g_sID;

    /**
     * Constructor
     */
    public WorkspaceManager(Client oClient, ViewSettingsManager oViewSettingsManager) {
        g_oViewSettingsManager = oViewSettingsManager;
        g_oClient = oClient;
    }

    public synchronized static boolean isLoadingWorkspace() {
        return loadingWorkspace;
    }

    public synchronized void setLoadingWorkspace(boolean status) {
        loadingWorkspace = status;
    }

    public static boolean isMarketViewsAvailable() {
        return marketViewsAvailable;
    }

    /**
     * Alter the view using the workspace settings
     */
    public static void alterView(ViewSetting oSetting,
                                 String sWorkspaceSetting) throws Exception {
        alterView(oSetting, sWorkspaceSetting, null);
    }

    private static void alterView(ViewSetting oSetting,
                                  String sWorkspaceSetting,
                                  PortfolioStore portfolioStore) throws Exception {
        oSetting.setWorkspaceRecord(sWorkspaceSetting);
    }

    /*public void applyInitialWSP() {
        try {
            FileInputStream oIn = new FileInputStream("system/initialwsp.msf");
            SmartProperties settings = new SmartProperties(Meta.RS);
            settings.load(oIn);
            oIn.close();
            oIn = null;

            Vector mainBoardViews = ViewSettingsManager.getMainBoardViews();
            Enumeration keys = settings.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                breakTag(key);
                String workspaceSetting = settings.getProperty(key);
                for (int i = 0; i < mainBoardViews.size(); i++) {
                    ViewSetting setting = (ViewSetting) mainBoardViews.get(i);
                    if (setting.getMainType() == ViewSettingsManager.SECTOR_VIEW) {
                        //if (setting.getID().startsWith(g_sID)) { // found it
                        alterView(setting, workspaceSetting);
                        setting.setVisible(false);
                        ClientTable oTableView = (ClientTable) setting.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        setting.updateCustomSettings((DraggableTable)oTableView.getTable());
                        //}
                    }else if (setting.getMainType() == ViewSettingsManager.MARKET_VIEW) {
//                        if (setting.getID().startsWith(g_sID)) { // found it
                        alterView(setting, workspaceSetting);
                        ClientTable oTableView = (ClientTable) setting.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        setting.updateCustomSettings((DraggableTable)oTableView.getTable());
//                        }
                    }

                }
            }
        } catch (Exception e) {

        }
    }*/

    public static boolean isWorkspaceLoaded() {
        return workspaceLoaded;
    }

    public static void setWorkspaceLoaded(boolean workspaceLoaded) {
        WorkspaceManager.workspaceLoaded = workspaceLoaded;
    }

    /**
     * Load the workspace file to the memory
     */
    public void loadWorkspace(String sFileName, boolean isDefaultWSPAvailable) throws Exception {
        setWorkspaceLoaded(false);
        if (sFileName.endsWith(".wsp")) {
            FileInputStream oIn = new FileInputStream(sFileName);//Settings.getAbsolutepath()+
            g_oSettings = new SmartProperties(Meta.RS);
            g_oSettings.load(oIn);
            oIn.close();
            oIn = null;

            /*****************gettign the workspace Ppth ***************/
            int lastIndex = sFileName.lastIndexOf("\\");
            String workpacePath = sFileName.substring(0, lastIndex);
            /*****************gettign the workspace Ppth ***************/

            applyWorkspace(isDefaultWSPAvailable, workpacePath); //open
        } else { //Its .wspx
            String path = sFileName.substring(0, sFileName.lastIndexOf(File.separator));

            int bufferLength = 2048;
            BufferedOutputStream dest = null;
            FileInputStream fis = new FileInputStream(sFileName);//Settings.getAbsolutepath() +
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
            ZipEntry entry;
            HashMap<String, ZipEntry> zipFileMap = new HashMap<String, ZipEntry>();
            while ((entry = zis.getNextEntry()) != null) {
                String name = entry.getName();
                String type = name.substring(name.indexOf(".") + 1);
                zipFileMap.put(type, entry);
                System.out.println("Extracting: " + entry);
                int count;
                byte data[] = new byte[bufferLength];
                // write the files to the disk
                FileOutputStream fos = new FileOutputStream(path + File.separator + entry.getName());
                dest = new BufferedOutputStream(fos, bufferLength);
                while ((count = zis.read(data, 0, bufferLength)) > -1) {
                    dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
            }
            zis.close();

            File owsFile = null;
            try {
                owsFile = new File(path + File.separator + zipFileMap.get("wsp").getName());
                FileInputStream owsIn = new FileInputStream(owsFile);
                g_oSettings = new SmartProperties(Meta.RS);
                g_oSettings.load(owsIn);
                owsIn.close();
                owsIn = null;
                owsFile.delete();
                /*****************gettign the workspace Ppth ***************/
                int lastIndex = sFileName.lastIndexOf("\\");
                String workpacePath = sFileName.substring(0, lastIndex);
                /*****************gettign the workspace Ppth ***************/
                applyWorkspace(isDefaultWSPAvailable, workpacePath);     //main board charts
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
//            owsFile.delete();

            File chartLayoutFile = null;
            try {
                chartLayoutFile = new File(path + File.separator + zipFileMap.get("mcl").getName());
                Vector<JInternalFrame> selectedFrames = new Vector<JInternalFrame>();
                boolean layoutLoaded = LayoutFactory.LoadLayout(chartLayoutFile, selectedFrames, false, null, false);
                if (layoutLoaded && ChartFrame.getSharedInstance().isWindowVisible()) {
                    ChartFrame.getSharedInstance().addChartsToLayout(selectedFrames, true);
                }
//            for (JInternalFrame internalFrame: selectedFrames) {
//                GraphStream.setBasicChartProperties((String) g_oSettings.get(key), (GraphFrame)internalFrame);
//            }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                chartLayoutFile.delete();
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            /* File dir = new File(path);
            String [] fileList = dir.list();
            for (String file: fileList) {
                if (file.endsWith(".out.mcl")) {
                    File fileObj = new File(path + File.separator + file);
                    fileObj.delete();
                }
            }*/
        }
    }

    private void applyWorkspace(boolean isDefaultWSPAvailable, String workspacePath) throws Exception {
        ViewSetting oSetting = null;
        String sWorkspaceSetting = null;
        boolean chartFrameVisible = false;

        String key;
        int iType;
        byte bytMainType;
        byte bytSubType;
        String sID = null;
        String sValue = null;
        Vector tempViewSettings = new Vector(20);
        marketViewsAvailable = false;

        setLoadingWorkspace(true);

        /* Close all opened wondows */
        JInternalFrame aoFrames[] = g_oClient.getDesktop().getAllFrames();
        for (JInternalFrame aoFrame : aoFrames) {
            if (aoFrame instanceof InternalFrame) {
                if (((InternalFrame) aoFrame).isDetached()) {
                    ((InternalFrame) aoFrame).attach();
                }
                aoFrame.setVisible(false);
            } else if (!(aoFrame instanceof ClientTable)) {
                //shift down
            }
            if (aoFrame instanceof ClientTable) {
                if (((ClientTable) aoFrame).isDetached()) {
                    ((ClientTable) aoFrame).attach();
                }
                if (isDefaultWSPAvailable) {
                    ((ClientTable) aoFrame).hideWindow();
                }
            } else {
                aoFrame.setVisible(false);
            }
        }

        Enumeration windowkeys = g_oViewSettingsManager.getWindowList().keys();
        while (windowkeys.hasMoreElements()) {
            InternalFrame frame = g_oViewSettingsManager.getWindow((String) windowkeys.nextElement());
            if (frame == null) continue;
            frame.finalizeWindow();
            frame.dispose();
            frame = null;
        }

        // close chart windows
        if (!ChartFrame.isNull()) {
            JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
            for (int i = 0; i < frames.length; i++) {
                if (frames[i] instanceof GraphFrame) {
                    try {
                        ChartFrame.getWindowTabs().removeTab(frames[i]);
                        frames[i].dispose();
                    } catch (Exception e) {
//                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
            ChartFrame.getSharedInstance().setActiveGraph(null);
        }

        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i] instanceof GraphFrame) {
                try {
                    frames[i].dispose();
                } catch (Exception e) {
                    //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }

        g_oViewSettingsManager.removeWindows();
        g_oViewSettingsManager.removeChartViews();
        g_oViewSettingsManager.removeTimenSalesViews();
        g_oViewSettingsManager.removeDepthSalesViews();
        g_oViewSettingsManager.removePulseViews();
        g_oViewSettingsManager.removeDTQViews();
        g_oViewSettingsManager.removeSnapViews();
        g_oViewSettingsManager.removeOrderDepthViews();
        g_oViewSettingsManager.removeRQuotesViews();
        g_oViewSettingsManager.removeFD2Views();
        g_oViewSettingsManager.removeOptionChainViews();
        g_oClient.removePortfolioViews();
        SideBar.getSharedInstance().closeSideBar();

        /* Analyse entries in the workspace file */
        adjustFullQuoteSummaryViewToSymbolview();
        Enumeration oKeys = g_oSettings.keys();
        String[] keys = new String[g_oSettings.size()];
        for (int i = 0; i < keys.length; i++) {
            keys[i] = (String) oKeys.nextElement();
        }
        WorkspaceKeySorter sorter = new WorkspaceKeySorter();
        Arrays.sort(keys, sorter);
        oKeys = null;
        sorter = null;

//        System.out.println("Begining for inside applywsp : "+ System.currentTimeMillis());
        for (int i = 0; i < keys.length; i++) {
            try {
                key = keys[i];
                breakTag(key);
                iType = g_iType;
                bytSubType = (byte) (iType);
                bytMainType = (byte) (iType >>> 8);
                sValue = (String) g_oSettings.get(key);
                sID = g_sID;
                switch (iType) {
                /*
                applyTheme() is Commented for optimization
                 */

                    case ViewSettingsManager.THEME:
                        if (!sValue.equals(Settings.getLastThemeID())) {
                            try {
                                g_oClient.applyTheme(sValue);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;

                        /* Select ticker or index panel */
//                    case ViewSettingsManager.TICKER_OR_INDEX:
//
//                        try {
//                            g_oClient.setTickerOrIndexPanel((String) g_oSettings.get(key));
//                        } catch (Exception e) {
//                            g_oClient.setTickerOrIndexPanel("INDICES");
//                        }
//                        break;

                    case ViewSettingsManager.TOOLBAR:
                        try {
                            g_oClient.hideShowToolBar(sValue.toUpperCase().equals("TRUE"));
                        } catch (Exception e) {
                            g_oClient.hideShowToolBar(true);
                        }
                        break;

                    case ViewSettingsManager.SELECTED_MARKET:
                        if ((sValue != null) && (sValue.equalsIgnoreCase("NULL"))) {
                            ExchangeStore.setSelectedExchangeID(null);
                        } else {
                            ExchangeStore.setSelectedExchangeID(sValue);
                        }
                        break;

                    case ViewSettingsManager.TOP_INDEX:
                        String[] params = sValue.split(VC_RECORD_DELIMETER);
                        IndexPanel.setIndexID(params[0]);

                        StretchedIndexPanel.setIndexID(params[0]);
                        ToolBar.getInstance().setToolBar(params[1]);
                        if (params.length == 3) {
                            IndexPanel.getInstance().setCashmapMode(params[2]);
                        }
                        params = null;
                        break;

                    case ViewSettingsManager.INVESTER_WINDOW_VIEW:
                        PluginStore.fireApplyWorkspace("" + ViewSettingsManager.INVESTER_WINDOW_VIEW, sValue);
                        break;

                    case ViewSettingsManager.DETAILED_MARKET_SUMMARY:
                        PluginStore.fireApplyWorkspace("" + ViewSettingsManager.DETAILED_MARKET_SUMMARY, sValue);
                        break;

                    case ViewSettingsManager.MEDIAPLAYER:
                        PluginStore.fireApplyWorkspace("" + ViewSettingsManager.MEDIAPLAYER, sValue);
                        break;

                    case ViewSettingsManager.FULL_QUOTE_MDEPTH_TYPE:
                        Settings.loadFullQuoteProperties(Byte.parseByte(sValue));
                        break;

                    case ViewSettingsManager.RAPID_ORDER_WINDOW:
                        try {
                            g_oClient.showRapidOrderPanel(sValue.toUpperCase().equals("TRUE"));
                        } catch (Exception e) {
                            g_oClient.showRapidOrderPanel(false);
                        }
                        break;

                    case ViewSettingsManager.PORTFOLIO_SIMULATOR_DEFAULT_CURRENCY:
                        break;

                    case ViewSettingsManager.COMMON_TICKER:
                        sWorkspaceSetting = (String) g_oSettings.get("" + ViewSettingsManager.COMMON_TICKER);
                        String indtickerWSString = sWorkspaceSetting.substring(sWorkspaceSetting.indexOf("|") + 1);
                        SharedSettings.loadingFromWorkSpace(indtickerWSString);
                        break;

                    case ViewSettingsManager.TICKER:
                        sWorkspaceSetting = (String) g_oSettings.get("" + ViewSettingsManager.TICKER);
                        String tickerWSString = sWorkspaceSetting.substring(sWorkspaceSetting.indexOf("|") + 1);
                        InternalFrame ticker = g_oClient.getTickerFrame();
                        ViewSetting setting = ticker.getViewSetting();
                        setting.setID("TICKER");
                        setting.setType(ViewSettingsManager.TICKER);
                        try {
                            alterView(setting, sWorkspaceSetting.substring(0, sWorkspaceSetting.indexOf("|"))); //sWorkspaceSetting);
                        } catch (Exception ex) {
                            alterView(setting, sWorkspaceSetting);
                        }
                        if (setting.getProperty(VC_TICKER_CUSTOM_VISIBLE).equalsIgnoreCase("true")) {
                            ticker.setSize(setting.getSize());
                            ticker.setVisible(setting.isVisible());
                            TradeFeeder.setVisible(setting.isVisible());
                        }
                        ticker.setLocation(setting.getLocation());

                        g_oClient.getTicker().setHideShowMenuCaption();
                        g_oClient.getTicker().applyWorkSpace(tickerWSString);
                        ticker.applySettings();
                        g_oClient.adjustTickerSettings();
                        if (setting.isDetached) {
                            ((InternalFrame) setting.getParent()).setVisible(true);
                            ((DetachableRootPane) ((InternalFrame) setting.getParent()).getRootPane()).detach();
                        }

                        ticker.updateUI();
                        if (setting.getProperty(VC_TICKER_FIXED).equalsIgnoreCase("true")) {
                            if (setting.isDetached) {
                                ((InternalFrame) setting.getParent()).setVisible(true);
                                ((DetachableRootPane) ((InternalFrame) setting.getParent()).getRootPane()).detach();
                            } else {

                                if (setting.getProperty(VC_TICKER_FIXED_TO_TOP).equalsIgnoreCase("true")) {
                                    g_oClient.fixTicker();
                                } else {
                                    g_oClient.fixTickerToBottom();
                                }
                            }
                        } else {
                            if (setting.isDetached) {
                                ((InternalFrame) setting.getParent()).setVisible(true);
                                ((DetachableRootPane) ((InternalFrame) setting.getParent()).getRootPane()).detach();
                            } else {
                                if (ticker.isVisible()) {
                                    g_oClient.floatTicker();
                                }
                            }
                        }

                        tickerWSString = null;
                        ticker = null;
                        setting = null;
                        break;

                    case ViewSettingsManager.SIDE_BAR_OP:
                        sWorkspaceSetting = (String) g_oSettings.get("" + ViewSettingsManager.SIDE_BAR_OP);
                        String sidebarWSString = sWorkspaceSetting.substring(sWorkspaceSetting.indexOf("|") + 1);
                        SideBarOptionDialog.loadingFromWorkSpace(sidebarWSString);
                        break;

                    case ViewSettingsManager.CHART_SIDE_BAR_OP:
                        sWorkspaceSetting = (String) g_oSettings.get("" + ViewSettingsManager.CHART_SIDE_BAR_OP);
                        String chartNaviBarWSString = sWorkspaceSetting.substring(sWorkspaceSetting.indexOf("|") + 1);
                        ChartNavibarConfigDialog.loadingFromWorkSpace(chartNaviBarWSString);
                        break;

                    case ViewSettingsManager.UPPER_TICKER:
                        sWorkspaceSetting = (String) g_oSettings.get("" + ViewSettingsManager.UPPER_TICKER);
                        String indtickerWSStringUpperTicker = sWorkspaceSetting.substring(sWorkspaceSetting.indexOf("|") + 1);
                        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                            UpperPanelSettings.loadingFromWorkSpace(indtickerWSStringUpperTicker);
                        }
                        break;

                    case ViewSettingsManager.LOWER_TICKER:
                        sWorkspaceSetting = (String) g_oSettings.get("" + ViewSettingsManager.LOWER_TICKER);
                        String indtickerWSStringLowerTicker = sWorkspaceSetting.substring(sWorkspaceSetting.indexOf("|") + 1);
                        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                            LowerPanelSettings.loadingFromWorkSpace(indtickerWSStringLowerTicker);
                        }
                        break;

                    case ViewSettingsManager.MIDDLE_TICKER:
                        sWorkspaceSetting = (String) g_oSettings.get("" + ViewSettingsManager.MIDDLE_TICKER);
                        String indtickerWSStringMiddleTicker = sWorkspaceSetting.substring(sWorkspaceSetting.indexOf("|") + 1);
                        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                            MiddlePanelSettings.loadingFromWorkSpace(indtickerWSStringMiddleTicker);
                        }
                        break;

                    default:
                        switch (bytMainType) {
                            case ViewSettingsManager.SECTOR_VIEW:
                            case ViewSettingsManager.MARKET_VIEW:
                            case ViewSettingsManager.FILTERED_VIEW:
                            case ViewSettingsManager.CUSTOM_VIEW:
                                if (sID == null) continue; // this is not a table view

                                if (bytMainType == ViewSettingsManager.MARKET_VIEW) {
                                    marketViewsAvailable = true;
                                }
                                oSetting = ViewSettingsManager.getMainBoardView(iType, sID);
                                if (oSetting == null) continue;
                                sWorkspaceSetting = (String) g_oSettings.get(oSetting.getType() + "|" + oSetting.getID());
                                if (sWorkspaceSetting == null) // view is not included in the workspace
                                {
                                    oSetting.setVisible(false);// remove the view from the desktop
                                } else {
                                    alterView(oSetting, sWorkspaceSetting);
                                }
                                try {
                                    ClientTable oTableView = (ClientTable) oSetting.getParent();
                                    oTableView.applySettings();
                                    oTableView.adjustColumns();
                                    oTableView.setSize(oSetting.getSize()); // this is done to avoid a _bug in
                                    // desktop pane which doesnot apply
                                    // the frame size correctly
                                    tempViewSettings.addElement(oSetting);
                                    String lnkgrp = oSetting.getProperty(ViewConstants.VC_LINKED_GROUP);
                                    if (lnkgrp != null && !lnkgrp.isEmpty()) {
                                        oTableView.setLinkedGroupID(lnkgrp);
                                    }

                                    oSetting.updateCustomSettings((DraggableTable) oTableView.getTable());
                                    oTableView.applySettings(); // todo need to optimize and remove this line
                                    ((DraggableTable) oTableView.getTable()).setHeadingFont(oSetting.getHeaderFont());
                                    ((DraggableTable) oTableView.getTable()).updateHeaderUI();
                                    oTableView.getViewSettings().setHeaderFont(oSetting.getHeaderFont());
                                    Object oRend = oTableView.getTable().getColumnModel().getColumn(0).getHeaderRenderer();
                                    if (oRend instanceof SortButtonRenderer) {
                                        ((SortButtonRenderer) oRend).setHeaderFont(oSetting.getHeaderFont());
                                        ((SortButtonRenderer) oRend).updateUI();
                                    }
                                    //commented for optimizing
//                                    oTableView.getTable().getTableHeader().updateUI();
//                                    oTableView.getTable().updateUI();
//                                    oTableView.updateUI();
                                    oTableView = null;

                                } catch (Exception e) {
                                }


                                break;

                            case ViewSettingsManager.PORTFOLIO_VIEW:
                                sWorkspaceSetting = (String) g_oSettings.get(key);
                                if (sWorkspaceSetting != null) // view is not included in the workspace
                                {
                                    if (sID.equals("1")) {
                                        alterView(Client.getInstance().getPortfolioWindow().getPortfolioViewSettings(), sWorkspaceSetting);
                                    }
                                    if (sID.equals("2")) {
                                        alterView(Client.getInstance().getPortfolioWindow().getTransHistorySettings(), sWorkspaceSetting);
                                    }
                                    if (sID.equals("3")) {
                                        alterView(Client.getInstance().getPortfolioWindow().getValuationSettings(), sWorkspaceSetting);
                                    }
                                    if (sID.equals("4")) {
                                        alterView(Client.getInstance().getTradingPortfolio().getValuationSettings(), sWorkspaceSetting);
                                    }
                                    if (sID.equals("5")) {
                                        alterView(Client.getInstance().getTradingPortfolio().getDefcurValuationSettings(), sWorkspaceSetting);
                                    }
                                    if (sID.equals("7")) {
                                        alterView(Client.getInstance().getPortfolioWindow().getValuationAllSettings(), sWorkspaceSetting);
                                    }
                                    if (sID.equals("11")) {
                                        //alterView(IntraDayPositionMoniterUI.getSharedInstance().getSettingAllPf() , sWorkspaceSetting);
                                    }
                                    if (sID.equals("12")) {
                                        //alterView(IntraDayPositionMoniterUI.getSharedInstance().getSettingSinglePf(), sWorkspaceSetting);
                                    }

                                }
                                break;

                            case ViewSettingsManager.MIST_VIEW:
                                if (sID == null) continue; // this is not a table view
                                // Commented on 02-06-2003
                                oSetting = ViewSettingsManager.getMainBoardView(iType, sID);
                                if (oSetting == null) continue;
                                sWorkspaceSetting = (String) g_oSettings.get(oSetting.getType() + "|" + oSetting.getID());
                                if (sWorkspaceSetting == null) // view is not included in the workspace
                                {
                                    oSetting.setVisible(false);// remove the view from the desktop
                                } else {
                                    alterView(oSetting, sWorkspaceSetting);
                                }
                                try {
                                    oSetting.setSortColumn(2); //StockData.LONG_DESC);
                                    MISTTable oTableView = (MISTTable) oSetting.getParent();
                                    oSetting.updateMISTCustomSettings((MISTTableSettings) oTableView.getTable().getSmartTable().getTableSettings());
                                    oTableView.applySettings();
                                    oTableView.updateGUI();
                                    oTableView.setSize(oSetting.getSize()); // this is done to avoid a _bug in
                                    ((MISTModel) oTableView.getModel1()).setCurrency(oSetting.getCurrency());
                                    oTableView.setWindowTitle();
                                    tempViewSettings.addElement(oSetting);
                                    oTableView.updateUI();
                                    oTableView = null;
                                } catch (Exception e) {
                                }
                                break;

                            case ViewSettingsManager.SUMMARY_VIEW:
                                if (sID != null) {
                                    oSetting = ViewSettingsManager.getSummaryView(sID);
                                    sWorkspaceSetting = (String) g_oSettings.get(oSetting.getType() + "|" + oSetting.getID());
                                    if (sWorkspaceSetting == null) // view is not included in the workspace
                                    {
                                        oSetting.setVisible(false);// remove the view from the desktop
                                    } else {
                                        alterView(oSetting, sWorkspaceSetting);
                                        tempViewSettings.addElement(oSetting);
                                    }
                                    if (oSetting.getID().equalsIgnoreCase("side_bar")) {
                                        g_oClient.createSummaryWindow(oSetting.getID());
                                    }
                                    try {
                                        if ((oSetting.getParent() == null) && (oSetting.isVisible() || oSetting.isDetached)) { //||oSetting.isDetached)
                                            g_oClient.createSummaryWindow(oSetting.getID());
                                            System.out.println("summary window created");
                                        }
                                        if ((oSetting.getParent() instanceof ChartFrame)) {
                                            InternalFrame frame = (InternalFrame) oSetting.getParent();
                                            frame.setSize(oSetting.getSize());
                                            frame.setLocation(oSetting.getLocation());
                                            frame.setVisible(oSetting.isVisible());
                                            chartFrameVisible = oSetting.isVisible();
                                            frame.setViewSetting(oSetting);
                                            tempViewSettings.addElement(oSetting);
                                            frame.applySettings();
                                            frame = null;
                                        } else if ((oSetting.getParent() instanceof HeatMapFrame)) {
                                            HeatMapFrame frame = (HeatMapFrame) oSetting.getParent();
                                            frame.setSize(oSetting.getSize());
                                            frame.setLocation(oSetting.getLocation());
                                            tempViewSettings.addElement(oSetting);
                                            frame.setProperties(sWorkspaceSetting);
                                            Client.getInstance().setSymbolsForHeatMap(frame.getWatchlistID());
                                            frame.show(oSetting.getSize());
                                            frame.setVisible(oSetting.isVisible());
                                            frame = null;
                                        } else if ((oSetting.getParent() instanceof com.isi.csvr.iframe.InternalFrame)) {
                                            InternalFrame oTableView = (InternalFrame) oSetting.getParent();
                                            oTableView.applySettings();
                                            try {
                                                oTableView.getModel1().applySettings();
                                            } catch (Exception e) {
                                            }
                                            try {
                                                oTableView.getModelFor(oSetting.getID()).applySettings();
                                            } catch (Exception e) {
                                            }
                                        } else {
                                            WindowWrapper oTableView = (WindowWrapper) oSetting.getParent();
                                            oTableView.applySettings();
                                        }
                                        PluginStore.fireApplyWorkspace(key, sWorkspaceSetting);
                                    } catch (Exception e) {
                                    }
                                    g_oClient.updateTree();

                                }
                                break;

                            case ViewSettingsManager.FOREX_VIEW:
                                if (sID != null) {
                                    try {
                                        oSetting = ViewSettingsManager.getForexView(sID);
                                        sWorkspaceSetting = (String) g_oSettings.get(oSetting.getType() + "|" + oSetting.getID());
                                        if (sWorkspaceSetting == null) // view is not included in the workspace
                                        {
                                            oSetting.setVisible(false);// remove the view from the desktop
                                        } else {
                                            alterView(oSetting, sWorkspaceSetting);
                                            tempViewSettings.addElement(oSetting);
                                        }
                                        try {
                                            if ((oSetting.getParent() == null) && (oSetting.isVisible() || oSetting.isDetached)) { //||oSetting.isDetached)
                                                g_oClient.createForexView((oSetting.getID()));
                                                System.out.println("Forex window created");
                                            }
                                            if ((oSetting.getParent() instanceof ForexBoard)) {
                                                JInternalFrame frame = (JInternalFrame) oSetting.getParent();
                                                frame.setSize(oSetting.getSize());
                                                frame.setLocation(oSetting.getLocation());
                                                frame.setVisible(oSetting.isVisible());
                                                tempViewSettings.addElement(oSetting);
                                                frame = null;
                                            } else {
                                                WindowWrapper oTableView = (WindowWrapper) oSetting.getParent();
                                                oTableView.applySettings();
                                            }

                                        } catch (Exception e) {
                                        }
                                        g_oClient.updateTree();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                break;

                            case ViewSettingsManager.SYMBOL_VIEW: {
                                switch (bytSubType) {
                                    case ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("DEPTH_CALCULATOR").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            // view is not included in the workspace
                                            if (sWorkspaceSetting != null) {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putDepthCalView("" + bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oSetting);
                                                } else {
                                                    g_oViewSettingsManager.putDepthCalView("" + bytSubType + "|" + oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oSetting);
                                                }
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.mnu_MDepth_Calculator(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), TWTypes.TradeSides.BUY, true, true, oSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.mnu_MDepth_Calculator(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), TWTypes.TradeSides.BUY, true, false, LinkStore.LINK_NONE);
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.OUTER_CHART:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("OUTER_CHART").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                g_oViewSettingsManager.putChartView("" + bytSubType + "|" + sID, oSetting);
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) != null && oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.showChartLinkedToSideBar(null, workspacePath, sID);
                                                } else {
                                                    g_oClient.showChartFromWSP(null, workspacePath, sID);
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.TIMEnSALES_VIEW2:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("TIME_AND_SALES").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oSetting.getDesktopType() == Constants.FULL_QUOTE_TYPE) {
                                                    oSetting.setID(sID);
                                                    g_oViewSettingsManager.putTimenSalesView("" + bytSubType + "|" + sID, oSetting);
                                                } else {
                                                    oSetting.setID(sID);
                                                    if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                        g_oViewSettingsManager.putTimenSalesView("" + bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oSetting);
                                                    } else {
                                                        g_oViewSettingsManager.putTimenSalesView("" + bytSubType + "|" + oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oSetting);
                                                    }
                                                }
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getDesktopType() != Constants.FULL_QUOTE_TYPE) {
                                                    if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                        g_oClient.mnu_TimeNSalesSymbol(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oSetting.getDesktopType(), true, true, oSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                    } else {
                                                        g_oClient.mnu_TimeNSalesSymbol(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oSetting.getDesktopType(), true, false, LinkStore.LINK_NONE);
                                                    }
                                                } else {
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("TRADE_SUMMARY").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putTimenSalesView("" + bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oSetting);
                                                } else {
                                                    g_oViewSettingsManager.putTimenSalesView("" + bytSubType + "|" + oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oSetting);
                                                }
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.mnu_TimeNSalesSummaryForSymbol(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, true, oSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.mnu_TimeNSalesSummaryForSymbol(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, false, LinkStore.LINK_NONE);
                                                }

                                            } catch (Exception e) {
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.MARKET_TIMEnSALES_VIEW:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getMarketTimenSalesView(key);
                                            sWorkspaceSetting = (String) g_oSettings.get(key);
                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                tempViewSettings.addElement(oSetting);
                                                ((InternalFrame) oSetting.getParent()).applySettings();
                                                try {
                                                    Client.getInstance().updateMarketTNSTickMode(sID, SharedMethods.intValue(oSetting.getProperty(VC_TICK_MODE)));
                                                } catch (Exception e) {

                                                }
//                                                String exchange = key.split("\\|")[1];
//                                                String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + exchange;
//                                                Client.getInstance().addMarketTradeRequest(requestID, exchange);
                                            }
                                            oSetting = null;
                                        }
                                        break;

                                    case ViewSettingsManager.ANNOUNCEMENT_VIEW:
                                        oSetting = g_oViewSettingsManager.getSymbolView("ANNOUNCEMENT_VIEW").getObject();
                                        sWorkspaceSetting = (String) g_oSettings.get(key);

                                        if (sWorkspaceSetting != null) // view is not included in the workspace
                                        {
                                            alterView(oSetting, sWorkspaceSetting);
                                            oSetting.setID(sID);
                                            if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                if (!SharedMethods.isFullKey(sID)) {
                                                    sID = sID + "~0";
                                                }
                                                oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                            }
                                            g_oViewSettingsManager.putAnnouncementView("" + bytSubType + "|" + sID, oSetting);
                                            tempViewSettings.addElement(oSetting);
                                        }
                                        try {
                                            g_oClient.mnu_AnnouncementsSymbol(sID);
                                        } catch (Exception e) {

                                            e.printStackTrace();
                                        }
                                        break;

                                    case ViewSettingsManager.DETAIL_QUOTE:
                                    case ViewSettingsManager.COMBINED_WINDOW_VIEW:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("DETAIL_QUOTE").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putDTQView("" + bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oSetting);
                                                } else {
                                                    g_oViewSettingsManager.putDTQView("" + bytSubType + "|" + oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oSetting);
                                                }
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.showDetailQuote(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), false, true, true, oSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.showDetailQuote(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), false, true, false, LinkStore.LINK_NONE);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.SNAP_QUOTE:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("SNAP_QUOTE").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putSnapView("" + bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oSetting);
                                                } else {
                                                    g_oViewSettingsManager.putSnapView("" + bytSubType + "|" + oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oSetting);
                                                }
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.showSnapQuote(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), false, true, true, oSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.showSnapQuote(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), false, true, false, LinkStore.LINK_NONE);
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("DETAIL_QUOTE_CASH_FLOW").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putSnapView("" + bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oSetting);
                                                } else {

                                                    g_oViewSettingsManager.putSnapView("" + bytSubType + "|" + oSetting.getProperty(VC_SYMBOLWISEVIEW_KEY), oSetting);
                                                }
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.showCashFlowDetailQuote(oSetting.getProperty(VC_SYMBOLWISEVIEW_KEY), false, true, true, oSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.showCashFlowDetailQuote(oSetting.getProperty(VC_SYMBOLWISEVIEW_KEY), false, true, false, LinkStore.LINK_NONE);
                                                }
                                            } catch (Exception e) {
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.DETAIL_QUOTE_FOREX:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("DETAIL_QUOTE_FOREX").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                g_oViewSettingsManager.putDTQView("" + bytSubType + "|" + sID, oSetting);
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                g_oClient.mnu_DetailQuoteForex(sID, false);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.MINI_TRADE_VIEW:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("MINI_TRADE_VIEW").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putMiniTradeView("" + bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oSetting);
                                                } else {
//                                                    g_oViewSettingsManager.putMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
//                    + "|" + ViewConstants.VC_SYMBOLWISEVIEW_KEY + "|" + oSetting.getID(), oSetting);

                                                    g_oViewSettingsManager.putMiniTradeView(ViewSettingsManager.MINI_TRADE_VIEW
                                                            + "|" + oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY) /*+ "|" + oSetting.getID()*/, oSetting);


                                                }
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.showDetailQuote(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), false, true, true, oSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.doMiniTradeSymbol(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).split(":")[0], oSetting.getID());
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.SECTOR_OVERVIEW:
                                        if (sID != null) {
                                            String sectorKey = key.split("\\|")[1];
                                            oSetting = g_oViewSettingsManager.getSectorOverView(sectorKey);
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) {// view is not included in the workspace
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                g_oViewSettingsManager.putSectorOverView("" + sID, oSetting);
                                                tempViewSettings.addElement(oSetting);
                                                try {
                                                    ((SectorOverviewUI) oSetting.getParent()).setVisible(oSetting.isVisible());
                                                    if (!oSetting.isLocationValid()) {
                                                        ((SectorOverviewUI) oSetting.getParent()).setLocationRelativeTo(Client.getInstance().oTopDesktop);
                                                        ((SectorOverviewUI) oSetting.getParent()).setLocation(oSetting.getLocation());

                                                    } else {
                                                        ((SectorOverviewUI) oSetting.getParent()).setLocation(oSetting.getLocation());
                                                    }
                                                    if (oSetting.isVisible()) {
                                                        SectorOverviewUpdator.getSharedInstance().sectorTempExchangesGatherer(ExchangeStore.getSharedInstance().getExchange(sID));
                                                    }

                                                    ((SectorOverviewUI) oSetting.getParent()).applySettings();
                                                    ((SectorOverviewUI) oSetting.getParent()).updateUI();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.DEPTH_ASK_VIEW:
                                    case ViewSettingsManager.DEPTH_BID_VIEW:
                                        if (sID != null) {


                                            ViewSetting oBidSetting = g_oViewSettingsManager.getSymbolView("DEPTH_BID_VIEW").getObject();
                                            ViewSetting oAskSetting = g_oViewSettingsManager.getSymbolView("DEPTH_ASK_VIEW").getObject();
                                            String sBidWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                    ViewSettingsManager.DEPTH_BID_VIEW) + "|" + sID);
                                            String sAskWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                    ViewSettingsManager.DEPTH_ASK_VIEW) + "|" + sID);

                                            oSetting = g_oViewSettingsManager.getSymbolView("DEPTH_BID_VIEW").getObject();
                                            alterView(oSetting, sBidWorkspaceSetting);
                                            String sKey = "";
                                            sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                                            if (g_oViewSettingsManager.getDepthView(bytSubType + "|" + sKey) != null && (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("false")))
                                                continue;
                                            else if ((g_oViewSettingsManager.getDepthView(bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP)) != null) && oSetting.getProperty(ViewConstants.VC_LINKED) != null
                                                    && oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true"))
                                                continue;

                                            if (sBidWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                oBidSetting.setID(sID);
                                                alterView(oBidSetting, sBidWorkspaceSetting);
                                                if (oBidSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oBidSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oBidSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oBidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_BID_VIEW + "|" + LinkStore.linked + "_" + oBidSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oBidSetting);
                                                } else {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_BID_VIEW + "|" + oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oBidSetting);

                                                }
                                                tempViewSettings.addElement(oBidSetting);
                                            }
                                            if (sAskWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                oAskSetting.setID(sID);
                                                alterView(oAskSetting, sAskWorkspaceSetting);
                                                if (oAskSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oAskSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oAskSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oAskSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_ASK_VIEW + "|" + LinkStore.linked + "_" + oAskSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oAskSetting);

                                                } else {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_ASK_VIEW + "|" + oAskSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oAskSetting);

                                                }
                                                tempViewSettings.addElement(oAskSetting);
                                            }
                                            try {
                                                if (oBidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.depthByOrder(oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, true, oBidSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.depthByOrder(oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, false, LinkStore.LINK_NONE);
                                                }
                                            } catch (Exception e) {
                                            }

                                        }
                                        break;

                                    case ViewSettingsManager.DEPTH_PRICE_ASK_VIEW:
                                    case ViewSettingsManager.DEPTH_PRICE_BID_VIEW:
                                        if (sID != null) {

                                            try {


                                                ViewSetting oBidSetting = g_oViewSettingsManager.getSymbolView("DEPTH_PRICE_BID_VIEW").getObject();
                                                ViewSetting oAskSetting = g_oViewSettingsManager.getSymbolView("DEPTH_PRICE_ASK_VIEW").getObject();
                                                String sBidWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                        ViewSettingsManager.DEPTH_PRICE_BID_VIEW) + "|" + sID);
                                                String sAskWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                        ViewSettingsManager.DEPTH_PRICE_ASK_VIEW) + "|" + sID);


                                                oSetting = g_oViewSettingsManager.getSymbolView("DEPTH_PRICE_BID_VIEW").getObject();
                                                alterView(oSetting, sBidWorkspaceSetting);
                                                String sKey = "";
                                                sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                                                if (g_oViewSettingsManager.getDepthView(bytSubType + "|" + sKey) != null && (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("false")))
                                                    continue;
                                                else if ((g_oViewSettingsManager.getDepthView(bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP)) != null) && oSetting.getProperty(ViewConstants.VC_LINKED) != null
                                                        && oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true"))
                                                    continue;

                                                if (sBidWorkspaceSetting != null) // view is not included in the workspace
                                                {
                                                    oBidSetting.setID(sID);
                                                    alterView(oBidSetting, sBidWorkspaceSetting);
                                                    if (oBidSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                        oBidSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                        if (!SharedMethods.isFullKey(sID)) {
                                                            sID = sID + "~0";
                                                        }
                                                        oBidSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                    }
                                                    if (oBidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true"))
                                                        g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + LinkStore.linked + "_" + oBidSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oBidSetting);
                                                    else {
                                                        g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oBidSetting);

                                                    }
                                                    tempViewSettings.addElement(oBidSetting);
                                                }
                                                if (sAskWorkspaceSetting != null) // view is not included in the workspace
                                                {
                                                    oAskSetting.setID(sID);
                                                    alterView(oAskSetting, sAskWorkspaceSetting);
                                                    if (oAskSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                        oAskSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                        if (!SharedMethods.isFullKey(sID)) {
                                                            sID = sID + "~0";
                                                        }
                                                        oAskSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                    }
                                                    if (oAskSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                        g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + LinkStore.linked + "_" + oAskSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oAskSetting);
                                                    } else {
                                                        g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + oAskSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oAskSetting);
                                                    }
                                                    tempViewSettings.addElement(oAskSetting);
                                                }
                                                try {
                                                    if (oBidSetting.getDesktopType() != Constants.FULL_QUOTE_TYPE) {
                                                        if (oBidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                            g_oClient.depthByPriceNormal(oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, oBidSetting.getDesktopType(), true, oBidSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                        } else {
                                                            g_oClient.depthByPriceNormal(oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, oBidSetting.getDesktopType(), false, LinkStore.LINK_NONE);
                                                        }
                                                    } else {
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW:
                                    case ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW:
                                        if (sID != null) {


                                            ViewSetting oBidSetting = g_oViewSettingsManager.getSymbolView("DEPTH_SPECIAL_BID_VIEW").getObject();
                                            ViewSetting oAskSetting = g_oViewSettingsManager.getSymbolView("DEPTH_SPECIAL_ASK_VIEW").getObject();
                                            String sBidWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                    ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW) + "|" + sID);
                                            String sAskWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                    ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW) + "|" + sID);

                                            oSetting = g_oViewSettingsManager.getSymbolView("DEPTH_SPECIAL_BID_VIEW").getObject();
                                            alterView(oSetting, sBidWorkspaceSetting);
                                            String sKey = "";
                                            sKey = oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
                                            if (g_oViewSettingsManager.getDepthView(bytSubType + "|" + sKey) != null)
                                                continue;
                                            else if ((g_oViewSettingsManager.getDepthView(bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP)) != null) && oSetting.getProperty(ViewConstants.VC_LINKED) != null
                                                    && oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true"))
                                                continue;

                                            if (sBidWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                oBidSetting.setID(sID);
                                                alterView(oBidSetting, sBidWorkspaceSetting);
                                                if (oBidSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oBidSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oBidSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oBidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + LinkStore.linked + "_" + oBidSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oBidSetting);
                                                } else {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW + "|" + oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oBidSetting);
                                                }
                                                tempViewSettings.addElement(oBidSetting);
                                            }
                                            if (sAskWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                oAskSetting.setID(sID);
                                                alterView(oAskSetting, sAskWorkspaceSetting);
                                                if (oAskSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oAskSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oAskSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oBidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + LinkStore.linked + "_" + oAskSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oAskSetting);
                                                } else {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" + oAskSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oAskSetting);

                                                }
                                                tempViewSettings.addElement(oAskSetting);
                                            }
                                            try {
                                                if (oBidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.depthSpecialByOrder(oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, true, oBidSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.depthSpecialByOrder(oBidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, false, LinkStore.LINK_NONE);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.DEPTH_ODDLOT_VIEW:
                                        if (sID != null) {
                                            ViewSetting oddlotSetting = g_oViewSettingsManager.getSymbolView("DEPTH_ODDLOT_VIEW").getObject();
                                            String sOddLotWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                    ViewSettingsManager.DEPTH_ODDLOT_VIEW) + "|" + sID);


                                            if (sOddLotWorkspaceSetting != null) {

                                                oddlotSetting.setID(sID);
                                                alterView(oddlotSetting, sOddLotWorkspaceSetting);
                                                if (oddlotSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oddlotSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oddlotSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oddlotSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + LinkStore.linked + "_" + oddlotSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oddlotSetting);
                                                } else {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" + oddlotSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oddlotSetting);
                                                }
                                                tempViewSettings.addElement(oddlotSetting);

                                            }

                                            try {
                                                if (oddlotSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.createDepthOddLot(oddlotSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, oddlotSetting.getDesktopType(), true, oddlotSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.createDepthOddLot(oddlotSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, oddlotSetting.getDesktopType(), false, LinkStore.LINK_NONE);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;


                                    case ViewSettingsManager.COMBINED_DEPTH_VIEW:
                                        if (sID != null) {
                                            if (g_oViewSettingsManager.getDepthView(bytSubType + "|" + sID) != null)
                                                continue;
                                            ViewSetting bidSetting = g_oViewSettingsManager.getSymbolView("COMBINED_DEPTH").getObject();
                                            String sbidWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                    ViewSettingsManager.COMBINED_DEPTH_VIEW) + "|" + sID);

                                            if (sbidWorkspaceSetting != null) {

                                                bidSetting.setID(sID);
                                                alterView(bidSetting, sbidWorkspaceSetting);
                                                if (bidSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    bidSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    bidSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (bidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + LinkStore.linked + "_" + bidSetting.getProperty(ViewConstants.VC_LINKED_GROUP), bidSetting);
                                                } else {
                                                    g_oViewSettingsManager.putDepthView(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" + bidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), bidSetting);
                                                }
                                                tempViewSettings.addElement(bidSetting);

                                            }

                                            try {
                                                if (bidSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.depthByPriceCombined(bidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, bidSetting.getDesktopType(), true, bidSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.depthByPriceCombined(bidSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, bidSetting.getDesktopType(), false, LinkStore.LINK_NONE);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.FULL_QUOTE_VIEW:
                                        if (sID != null) {
                                            oSetting = g_oViewSettingsManager.getSymbolView("FULL_QUOTE_VIEW").getObject();
                                            sWorkspaceSetting = (String) g_oSettings.get(key);

                                            if (sWorkspaceSetting != null) // view is not included in the workspace
                                            {
                                                alterView(oSetting, sWorkspaceSetting);
                                                oSetting.setID(sID);
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                    oSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                    if (!SharedMethods.isFullKey(sID)) {
                                                        sID = sID + "~0";
                                                    }
                                                    oSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                                }
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oViewSettingsManager.putFullQuoteMainView("" + bytSubType + "|" + LinkStore.linked + "_" + oSetting.getProperty(ViewConstants.VC_LINKED_GROUP), oSetting);
                                                } else {
                                                    g_oViewSettingsManager.putFullQuoteMainView("" + bytSubType + "|" + oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), oSetting);
                                                }
                                                tempViewSettings.addElement(oSetting);
                                            }
                                            try {
                                                if (oSetting.getProperty(ViewConstants.VC_LINKED).equalsIgnoreCase("true")) {
                                                    g_oClient.showFullQuote(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, Constants.FULL_QUOTE_TYPE, true, oSetting.getProperty(ViewConstants.VC_LINKED_GROUP));
                                                } else {
                                                    g_oClient.showFullQuote(oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY), true, Constants.FULL_QUOTE_TYPE, false, LinkStore.LINK_NONE);
                                                }

                                            } catch (Exception e) {
                                            }
                                        }
                                        break;

                                    case ViewSettingsManager.REGIONAL_QUOTE_VIEW:
                                        /* check if the view is already created */
                                        if (g_oViewSettingsManager.getRQuotesView(bytSubType + "|" + sID) != null)
                                            continue;
                                        /* -- */

                                        ViewSetting oBidSetting = g_oViewSettingsManager.getSymbolView("REGIONAL_QUOTES").getObject();
                                        String sBidWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                ViewSettingsManager.REGIONAL_QUOTE_VIEW) + "|" + sID);
                                        if (sBidWorkspaceSetting != null) // view is not included in the workspace
                                        {
                                            oBidSetting.setID(sID);
                                            alterView(oBidSetting, sBidWorkspaceSetting);
                                            if (oBidSetting.getProperty(ViewConstants.VC_LINKED) == null) {
                                                oBidSetting.putProperty(ViewConstants.VC_LINKED, false);
                                                if (!SharedMethods.isFullKey(sID)) {
                                                    sID = sID + "~0";
                                                }
                                                oBidSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                            }
                                            g_oViewSettingsManager.putRQuotesView(ViewSettingsManager.REGIONAL_QUOTE_VIEW + "|" + sID, oBidSetting);
                                            tempViewSettings.addElement(oBidSetting);
                                        }
                                        try {

                                            g_oClient.mnu_RegionalQuoteSymbol(sID, true);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        break;

                                    case ViewSettingsManager.FUNDAMENTAL_DATA_VIEW:
                                        /* check if the view is already created */
                                        if (g_oViewSettingsManager.getFD2View(bytSubType + "|" + sID) != null) continue;
                                        /* -- */

                                        ViewSetting oBidSettingFDV = g_oViewSettingsManager.getSymbolView("FUNDAMENTAL_DATA").getObject();
                                        String sBidWorkspaceSettingFDV = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                ViewSettingsManager.FUNDAMENTAL_DATA_VIEW) + "|" + sID);
                                        if (sBidWorkspaceSettingFDV != null) // view is not included in the workspace
                                        {
                                            oBidSettingFDV.setID(sID);
                                            alterView(oBidSettingFDV, sBidWorkspaceSettingFDV);
                                            if (oBidSettingFDV.getProperty(ViewConstants.VC_LINKED) == null) {
                                                oBidSettingFDV.putProperty(ViewConstants.VC_LINKED, false);
                                                if (!SharedMethods.isFullKey(sID)) {
                                                    sID = sID + "~0";
                                                }
                                                oBidSettingFDV.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, sID);
                                            }
                                            g_oViewSettingsManager.putFD2View(ViewSettingsManager.FUNDAMENTAL_DATA_VIEW + "|" + sID, oBidSettingFDV);
                                            tempViewSettings.addElement(oBidSettingFDV);
                                        }
                                        try {

                                            g_oClient.mnu_FD2Symbol(sID, true);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        break;

                                    case ViewSettingsManager.OPTION_CALLS_VIEW:
                                    case ViewSettingsManager.FUTURES_CALLS_VIEW:
                                        if (sID != null) {
                                            if (g_oViewSettingsManager.getOptionChainView(ViewSettingsManager.OPTION_CALLS_VIEW + "|" + sID) != null)
                                                continue;
                                            ViewSetting oCallsSetting = g_oViewSettingsManager.getSymbolView("OPTIONS").getObject();
                                            ViewSetting oPutsSetting = g_oViewSettingsManager.getSymbolView("FUTURES").getObject();
                                            String sCallsWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                    ViewSettingsManager.OPTION_CALLS_VIEW) + "|" + sID);
                                            String sPutsWorkspaceSetting = (String) g_oSettings.get(((ViewSettingsManager.SYMBOL_VIEW * 256) +
                                                    ViewSettingsManager.FUTURES_CALLS_VIEW) + "|" + sID);

                                            if (sCallsWorkspaceSetting != null) {
                                                oCallsSetting.setID(sID);
                                                alterView(oCallsSetting, sCallsWorkspaceSetting);
                                                g_oViewSettingsManager.putOptionChainView(ViewSettingsManager.OPTION_CALLS_VIEW + "|" + sID, oCallsSetting);
                                                tempViewSettings.addElement(oCallsSetting);
                                            }
                                            if (sPutsWorkspaceSetting != null) {
                                                oPutsSetting.setID(sID);
                                                alterView(oPutsSetting, sPutsWorkspaceSetting);
                                                g_oViewSettingsManager.putOptionChainView(ViewSettingsManager.FUTURES_CALLS_VIEW + "|" + sID, oPutsSetting);
                                                tempViewSettings.addElement(oPutsSetting);
                                            }

                                            try {
                                                g_oClient.mnu_Options(sID, null, true, false);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                        break;

                                    default:

                                        sWorkspaceSetting = (String) g_oSettings.get(key);
                                        PluginStore.fireApplyWorkspace(key, sWorkspaceSetting);
                                        break;


                                }


                                sWorkspaceSetting = (String) g_oSettings.get(key);
                                PluginStore.fireApplyWorkspace(key, sWorkspaceSetting);
                            }

                            break;

                        }
                        break;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//         System.out.println("Ending for inside applywsp : "+ System.currentTimeMillis());
        System.out.println("Begining for inside applywsp : " + System.currentTimeMillis());
        /* To rearrange windows in correct position in the layer */
        try {
            Object[] settings = tempViewSettings.toArray();
            ViewSettingComparator comparator = new ViewSettingComparator();
            comparator.setSortOrder(ViewSettingComparator.DESCENDING);
            /* sort viewsetting in the order of index */
            Arrays.sort(settings, comparator);
            for (int i = 0; i < settings.length; i++) {
                try {
                    ViewSetting setting = (ViewSetting) settings[i];
                    JInternalFrame frame = (JInternalFrame) setting.getParent();
                    /* reapply the index to each window */
                    //System.out.println(frame == null);
                    if (frame != null) {
                        g_oClient.getDesktop().setPosition(frame, setting.getIndex());
                        //System.out.println(frame.getTitle());
                    }
                    setting = null;
                    frame = null;
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            settings = null;
        } catch (Exception x) {
            x.printStackTrace();
        }
        tempViewSettings = null;

        if (chartFrameVisible) {
            Client.getInstance().getChartFrame().setVisible(chartFrameVisible);
        } else if (!ChartFrame.isNull()) {
            Client.getInstance().getChartFrame().setVisible(chartFrameVisible);
        }
        Client.getInstance().getPortfolioWindow().applySettings();
        Client.getInstance().getTradingPortfolio().applySettings();
        //IntraDayPositionMoniterUI.getSharedInstance().applySettings();
        setLoadingWorkspace(false);
        setWorkspaceLoaded(true);
        // Take 6.5 seconds for this operation
        Application.getInstance().fireWorkspaceLoaded();
        System.out.println("Ending for inside applywsp : " + System.currentTimeMillis());
        //((TWDesktop) Client.getInstance().getDesktop()).checkForDetach();
        ((TWDesktop) Client.getInstance().getDesktop()).refreshDesktop();
        //System.out.println("--------------------");
    }

    public void applyInitialWSPForMainboard(ViewSetting mainboardview) {
        try {
            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/initialwsp.msf");
            SmartProperties settings = new SmartProperties(Meta.RS);
            settings.load(oIn);
            oIn.close();
            oIn = null;
            Enumeration keys = settings.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                breakTag(key);
                String workspaceSetting = settings.getProperty(key);
                if ((mainboardview.getType() == g_iType) && (mainboardview.getMainType() == ViewSettingsManager.SECTOR_VIEW)) {
//                    if (mainboardview.getType()== g_iType) { // found it
                    alterView(mainboardview, workspaceSetting);
                    mainboardview.setVisible(false);
                    //todo added by Dilum
                    SharedMethods.applyCustomViewSetting(mainboardview);
                    ClientTable oTableView = (ClientTable) mainboardview.getParent();
                    oTableView.applySettings();
                    oTableView.adjustColumns();
                    oTableView.setSize(mainboardview.getSize());
                    mainboardview.updateCustomSettings((DraggableTable) oTableView.getTable());
                    break;
//                    }
                } else if ((mainboardview.getType() == g_iType) && (mainboardview.getMainType() == ViewSettingsManager.MARKET_VIEW)) {
                    if (mainboardview.getID().startsWith(g_sID)) { // found it         //&& (mainboardview.getID().equals("8"))
                        alterView(mainboardview, workspaceSetting);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(mainboardview);
                        ClientTable oTableView = (ClientTable) mainboardview.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(mainboardview.getSize());
                        mainboardview.updateCustomSettings((DraggableTable) oTableView.getTable());
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applyInitialWSPForMyStocks(ViewSetting setting) {
        try {
            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/initialwsp.msf");
            SmartProperties settings = new SmartProperties(Meta.RS);
            settings.load(oIn);
            oIn.close();
            oIn = null;

            Enumeration keys = settings.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                breakTag(key);
                String workspaceSetting = settings.getProperty(key);
                if (setting.getMainType() == ViewSettingsManager.CUSTOM_VIEW) {
                    if (g_sID.equals("0")) { // found it
                        alterView(setting, workspaceSetting);
                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        ClientTable oTableView = (ClientTable) setting.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.FILTERED_VIEW) {
                    if (g_sID.equals("1792")) { // found it
                        alterView(setting, workspaceSetting);
                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        ClientTable oTableView = (ClientTable) setting.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.MIST_VIEW) {
                    if (g_sID.equals("1536")) { // found it
                        alterView(setting, workspaceSetting);
//                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        MISTTable oTableView = (MISTTable) setting.getParent();
                        oTableView.applySettings();
//                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        System.out.println("initailwsp allied for the new mist view");
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applyInitialWSPForForex(ViewSetting setting) {
        try {
            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/initialwsp.msf");
            SmartProperties settings = new SmartProperties(Meta.RS);
            settings.load(oIn);
            oIn.close();
            oIn = null;

            Enumeration keys = settings.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                breakTag(key);
                String workspaceSetting = settings.getProperty(key);
                if (setting.getMainType() == ViewSettingsManager.CUSTOM_VIEW) {
                    if (g_sID.equals("0")) { // found it
                        alterView(setting, workspaceSetting);
                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        ClientTable oTableView = (ClientTable) setting.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.FILTERED_VIEW) {
                    if (g_sID.equals("1792")) { // found it
                        alterView(setting, workspaceSetting);
                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        ClientTable oTableView = (ClientTable) setting.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.MIST_VIEW) {
                    if (g_sID.equals("1536")) { // found it
                        alterView(setting, workspaceSetting);
//                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        MISTTable oTableView = (MISTTable) setting.getParent();
                        oTableView.applySettings();
//                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        System.out.println("initailwsp allied for the new mist view");
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.FOREX_VIEW) {
                    if (g_sID.equals("2048")) { // found it
                        alterView(setting, workspaceSetting);
//                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        ForexBoard fxb = (ForexBoard) setting.getParent();
                        fxb.applySettings();
//                        oTableView.adjustColumns();
                        fxb.setSize(setting.getSize());
                        System.out.println("initailwsp allied for the new Forex view");
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applyInitialWSPForSectorOverView(ViewSetting setting) {
        try {
            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/initialwsp.msf");
            SmartProperties settings = new SmartProperties(Meta.RS);
            settings.load(oIn);
            oIn.close();
            oIn = null;

            Enumeration keys = settings.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                breakTag(key);
                String workspaceSetting = settings.getProperty(key);
                if (setting.getMainType() == ViewSettingsManager.CUSTOM_VIEW) {
                    if (g_sID.equals("0")) { // found it
                        alterView(setting, workspaceSetting);
                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        ClientTable oTableView = (ClientTable) setting.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.FILTERED_VIEW) {
                    if (g_sID.equals("1792")) { // found it
                        alterView(setting, workspaceSetting);
                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        ClientTable oTableView = (ClientTable) setting.getParent();
                        oTableView.applySettings();
                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.MIST_VIEW) {
                    if (g_sID.equals("1536")) { // found it
                        alterView(setting, workspaceSetting);
//                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        MISTTable oTableView = (MISTTable) setting.getParent();
                        oTableView.applySettings();
//                        oTableView.adjustColumns();
                        oTableView.setSize(setting.getSize());
                        System.out.println("initailwsp allied for the new mist view");
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.FOREX_VIEW) {
                    if (g_sID.equals("2048")) { // found it
                        alterView(setting, workspaceSetting);
//                        setting.setVisible(false);
                        //todo added by Dilum
                        SharedMethods.applyCustomViewSetting(setting);
                        ForexBoard fxb = (ForexBoard) setting.getParent();
                        fxb.applySettings();
//                        oTableView.adjustColumns();
                        fxb.setSize(setting.getSize());
                        System.out.println("initailwsp allied for the new Forex view");
                        break;
                    }
                } else if (setting.getMainType() == ViewSettingsManager.SYMBOL_VIEW) {
                    if (g_sID.equals("63")) {
                        alterView(setting, workspaceSetting);
                        SharedMethods.applyCustomViewSetting(setting);
                        SectorOverviewUI secOVU = (SectorOverviewUI) setting.getParent();
                        secOVU.applySettings();
                        secOVU.setSize(setting.getSize());
                        System.out.println("initailwsp allied for the new Forex view");
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveWorkspace(String sFileName) throws Exception {
        if (sFileName.endsWith(".wspx")) {
            sFileName = sFileName.substring(0, sFileName.lastIndexOf(".")) + ".wsp";
        }

        ViewSetting oSetting = null;
        String sWorkspaceSetting = null;

        FileOutputStream oOut = new FileOutputStream(sFileName);

        /* Splitpanel locations */
        oOut.write((ViewSettingsManager.TICKER_OR_INDEX + Meta.RS + g_oClient.getTickerOrIndexPanel() + "\r\n").getBytes());
//        oOut.write((ViewSettingsManager.SYMBOL_INDEX+ Meta.RS + SymbolPanelStore.getSharedInstance().getSymbols()+ "\r\n").getBytes());
//        if (IndexPanel.getIndexID() != null){
        oOut.write((ViewSettingsManager.TOP_INDEX + Meta.RS + IndexPanel.getIndexID() + VC_RECORD_DELIMETER + ToolBar.getIndexPanelMode() + VC_RECORD_DELIMETER + IndexPanel.getInstance().isCashmapMode() + "\r\n").getBytes());
//        }
        oOut.write((ViewSettingsManager.FULL_QUOTE_MDEPTH_TYPE + Meta.RS + Settings.getFullQuoteMDepthType() + "\r\n").getBytes());
        oOut.write((ViewSettingsManager.THEME + Meta.RS + Theme.getID() + "\r\n").getBytes());
        oOut.write((ViewSettingsManager.TOOLBAR + Meta.RS + g_oClient.isToolBarVisible() + "\r\n").getBytes());
        oOut.write((ViewSettingsManager.SELECTED_MARKET + Meta.RS + ExchangeStore.getSharedInstance().getSelectedExchangeID() + "\r\n").getBytes());
        oOut.write((ViewSettingsManager.RAPID_ORDER_WINDOW + Meta.RS + g_oClient.isRapidOrderPanelVisible() + "\r\n").getBytes());

        oOut.write((ViewSettingsManager.INVESTER_WINDOW_VIEW + Meta.RS + PluginStore.getWorkspaceString("" + ViewSettingsManager.INVESTER_WINDOW_VIEW) + "\r\n").getBytes());
        oOut.write((ViewSettingsManager.DETAILED_MARKET_SUMMARY + Meta.RS + PluginStore.getWorkspaceString("" + ViewSettingsManager.DETAILED_MARKET_SUMMARY) + "\r\n").getBytes());
        oOut.write((ViewSettingsManager.MEDIAPLAYER + Meta.RS + PluginStore.getWorkspaceString("" + ViewSettingsManager.MEDIAPLAYER) + "\r\n").getBytes());

        oOut.write((ViewSettingsManager.SIDE_BAR_OP + Meta.RS + SideBarOptionDialog.isCheckisSymbolOnly() + Meta.RS + SideBarOptionDialog.isCheckisSymbolShortOnly() + Meta.RS + SideBarOptionDialog.isCheckisShortOnly() + Meta.RS + SideBarOptionDialog.isCheckisShorSymboltOnly() +
                Meta.RS + SideBarOptionDialog.isCheckisLongOnly() + Meta.RS + SideBarOptionDialog.isCheckisLongSymbolOnly() +
                Meta.RS + RendererComponent.FONT_TYPE + Meta.RS + RendererComponent.FONT_STYLE + Meta.RS + RendererComponent.FONT_SIZE +
                Meta.RS + SymbolTreeRenderer.sideBarSymbolColor.getRGB() + "\r\n").getBytes());

        oOut.write((ViewSettingsManager.CHART_SIDE_BAR_OP + Meta.RS + ChartNavibarConfigDialog.isCheckisSymbolOnly() + Meta.RS + ChartNavibarConfigDialog.isCheckisSymbolShortOnly() + Meta.RS + ChartNavibarConfigDialog.isCheckisShortOnly() + Meta.RS + ChartNavibarConfigDialog.isCheckisShorSymboltOnly() +
                Meta.RS + ChartNavibarConfigDialog.isCheckisLongOnly() + Meta.RS + ChartNavibarConfigDialog.isCheckisLongSymbolOnly() +
                Meta.RS + ChartRendererComponenet.FONT_TYPE + Meta.RS + ChartRendererComponenet.FONT_STYLE + Meta.RS + ChartRendererComponenet.FONT_SIZE +
                Meta.RS + ChartSymbolTreeRenderer.chartNaviBarSymbolColor.getRGB() + "\r\n").getBytes());

        oOut.write((ViewSettingsManager.COMMON_TICKER + Meta.RS + SharedSettings.custom + Meta.RS + SharedSettings.advanced + Meta.RS +
                SharedSettings.IS_CUSTOM_SELECTED + Meta.RS + SharedSettings.IS_ADVANCED_SELECTED + Meta.RS +
                Client.getInstance().isTickerFixed() + Meta.RS + Ticker.tickerFixedToTopPanel + Meta.RS + SharedSettings.is_Very_First_Time + "\r\n").getBytes());

        if (CommonSettings.IS_INT_SET_LOD) {
            oOut.write((ViewSettingsManager.LOWER_TICKER + Meta.RS + LowerPanelSettings.BIG_FONT_STYLE + Meta.RS + LowerPanelSettings.SMALL_FONT_STYLE + Meta.RS +
                    LowerPanelSettings.FONT_BIG + Meta.RS + LowerPanelSettings.FONT_SMALL + Meta.RS + LowerPanelSettings.FONT_TYPE + Meta.RS +
                    LowerPanelSettings.INDIA_DRAW_HEIGHT + Meta.RS + LowerPanelSettings.INDIA_TICKER_HEIGHT + Meta.RS +
                    LowerPanelSettings.S_EQ_STATUS + Meta.RS + LowerPanelSettings.MODE + Meta.RS + LowerFilterPanel.EXCHANGE_LIST + Meta.RS +
                    LowerFilterPanel.WATCH_LIST + Meta.RS + LowerFilterPanel.ANNOUNCE_LIST + Meta.RS + LowerFilterPanel.NEWS_LIST + Meta.RS +
                    LowerTicker.tickerSpeed + Meta.RS + LowerFilterPanel.showExTraded + Meta.RS + LowerFilterPanel.showExAll + Meta.RS + LowerFilterPanel.runExOnClose +
                    Meta.RS + LowerFilterPanel.showWatchListTraded + Meta.RS + LowerFilterPanel.showWatchListAll + Meta.RS + LowerFilterPanel.runWatchListOnClose +
                    Meta.RS + LowerFilterPanel.isVeryFirstTime +
                    "\r\n").getBytes());
        }


        if (CommonSettings.IS_DERI_SET_LOD) {
            oOut.write((ViewSettingsManager.MIDDLE_TICKER + Meta.RS + MiddlePanelSettings.BIG_FONT_STYLE + Meta.RS + MiddlePanelSettings.SMALL_FONT_STYLE + Meta.RS +
                    MiddlePanelSettings.FONT_BIG + Meta.RS + MiddlePanelSettings.FONT_SMALL + Meta.RS + MiddlePanelSettings.FONT_TYPE + Meta.RS +
                    MiddlePanelSettings.INDIA_DRAW_HEIGHT + Meta.RS + MiddlePanelSettings.INDIA_TICKER_HEIGHT + Meta.RS +
                    MiddlePanelSettings.S_EQ_STATUS + Meta.RS + MiddlePanelSettings.MODE + Meta.RS + MiddleFilterPanel.EXCHANGE_LIST + Meta.RS +
                    MiddleFilterPanel.WATCH_LIST + Meta.RS + MiddleFilterPanel.ANNOUNCE_LIST + Meta.RS + MiddleFilterPanel.NEWS_LIST + Meta.RS +
                    MiddleTicker.tickerSpeed + Meta.RS + MiddleFilterPanel.showExTraded + Meta.RS + MiddleFilterPanel.showExAll + Meta.RS + MiddleFilterPanel.runExOnClose +
                    Meta.RS + MiddleFilterPanel.showWatchListTraded + Meta.RS + MiddleFilterPanel.showWatchListAll + Meta.RS + MiddleFilterPanel.runWatchListOnClose +
                    Meta.RS + MiddleFilterPanel.isVeryFirstTime +
                    "\r\n").getBytes());
        }

        if (CommonSettings.IS_EQ_SET_LOD) {
            oOut.write((ViewSettingsManager.UPPER_TICKER + Meta.RS + UpperPanelSettings.BIG_FONT_STYLE + Meta.RS + UpperPanelSettings.SMALL_FONT_STYLE + Meta.RS +
                    UpperPanelSettings.FONT_BIG + Meta.RS + UpperPanelSettings.FONT_SMALL + Meta.RS + UpperPanelSettings.FONT_TYPE + Meta.RS +
                    UpperPanelSettings.INDIA_DRAW_HEIGHT + Meta.RS + UpperPanelSettings.INDIA_TICKER_HEIGHT + Meta.RS +
                    UpperPanelSettings.S_EQ_STATUS + Meta.RS + UpperPanelSettings.MODE + Meta.RS + UpperFilterPanel.EXCHANGE_LIST + Meta.RS +
                    UpperFilterPanel.WATCH_LIST + Meta.RS + UpperFilterPanel.ANNOUNCE_LIST + Meta.RS + UpperFilterPanel.NEWS_LIST + Meta.RS +
                    UpperTicker.tickerSpeed + Meta.RS + UpperFilterPanel.showExTraded + Meta.RS + UpperFilterPanel.showExAll + Meta.RS + UpperFilterPanel.runExOnClose +
                    Meta.RS + UpperFilterPanel.showWatchListTraded + Meta.RS + UpperFilterPanel.showWatchListAll + Meta.RS + UpperFilterPanel.runWatchListOnClose +
                    Meta.RS + UpperFilterPanel.isVeryFirstTime +
                    "\r\n").getBytes());
        }

//        oOut.write((ViewSettingsManager.PORTFOLIO_SIMULATOR_DEFAULT_CURRENCY + Meta.RS + PFStore.getBaseCurrency() + "\r\n").getBytes());
        InternalFrame ticker = Client.getInstance().getTickerFrame();

        oOut.write((ViewSettingsManager.TICKER + Meta.RS +
                VC_WINDOW_VISIBLE + VC_FIELD_DELIMETER +
                ticker.isVisible() + VC_RECORD_DELIMETER +
                VC_TICKER_FIXED + VC_FIELD_DELIMETER +
                Client.getInstance().isTickerFixed() + VC_RECORD_DELIMETER +
                VC_WINDOW_HEADER + VC_FIELD_DELIMETER +
                ticker.getViewSetting().isHeaderVisible() + VC_RECORD_DELIMETER +
                VC_WINDOW_LOCATION + VC_FIELD_DELIMETER +
                (int) ticker.getLocation().getX() + "," +
                (int) ticker.getLocation().getY() + VC_RECORD_DELIMETER +
                VC_WINDOW_SIZE + VC_FIELD_DELIMETER +
                (int) ticker.getSize().getWidth() + "," +
                (int) ticker.getSize().getHeight() + VC_RECORD_DELIMETER +
                VC_DETACH + VC_FIELD_DELIMETER + ticker.getViewSetting().isDetached() + VC_RECORD_DELIMETER +
                VC_TICKER_FIXED_TO_TOP + VC_FIELD_DELIMETER + Ticker.tickerFixedToTopPanel +
                VC_RECORD_DELIMETER + VC_TICKER_CUSTOM_VISIBLE + VC_FIELD_DELIMETER + SharedSettings.IS_CUSTOM_SELECTED + "|" + g_oClient.getTicker().toString() + "\r\n").getBytes());

        ticker = null;

        HeatManager.getSharedInstance().updateViewSettings(); // apply vindow settings to all heat frames

        // save Main Board views (Market / Sector)
        for (int i = 0; i < ViewSettingsManager.getMainBoardViews().size(); i++) {
            oSetting = (ViewSetting) ViewSettingsManager.getMainBoardViews().elementAt(i);
            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                oOut.write((sWorkspaceSetting + "\r\n").getBytes());
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        // save portfolios
        try {
            oOut.write((Client.getInstance().getPortfolioWindow().getPortfolioViewSettings().toWorkspaceString() + "\r\n").getBytes());
            oOut.write((Client.getInstance().getPortfolioWindow().getTransHistorySettings().toWorkspaceString() + "\r\n").getBytes());
            oOut.write((Client.getInstance().getPortfolioWindow().getValuationSettings().toWorkspaceString() + "\r\n").getBytes());
            oOut.write((Client.getInstance().getPortfolioWindow().getValuationAllSettings().toWorkspaceString() + "\r\n").getBytes());
            oOut.write((Client.getInstance().getTradingPortfolio().getValuationSettings().toWorkspaceString() + "\r\n").getBytes());
            oOut.write((Client.getInstance().getTradingPortfolio().getDefcurValuationSettings().toWorkspaceString() + "\r\n").getBytes());
            //oOut.write((IntraDayPositionMoniterUI.getSharedInstance().getSettingAllPf().toWorkspaceString() + "\r\n").getBytes());
            //oOut.write((IntraDayPositionMoniterUI.getSharedInstance().getSettingSinglePf().toWorkspaceString() + "\r\n").getBytes());
            //System.out.println(sWorkspaceSetting);
        } catch (Exception e) {
            //e.printStackTrace();
        }

        // remove blotter sorting before soting
        int col = TradeMethods.getSharedInstance().getBlotterSortColumn();
        TradeMethods.getSharedInstance().removeBlotterSorting();
        TradeMethods.getSharedInstance().setBlotterSortColumn(col);
        System.out.println(" ------------------- ");
        Enumeration oSummaryViews = ViewSettingsManager.getSummaryViews().elements();

//        ViewSetting tempSetting = (ViewSetting) ViewSettingsManager.getSummaryViews().get("UNICHART");
//        String chartString = tempSetting.toWorkspaceString();
//        System.out.println("chartString : " + chartString);


        while (oSummaryViews.hasMoreElements()) {
            oSetting = (ViewSetting) oSummaryViews.nextElement();

            sWorkspaceSetting = oSetting.toWorkspaceString();
//            System.out.println("Saving " + oSetting.getID());
            /*if (oSetting.getID().startsWith("BTW_AN")){
                if ((sWorkspaceSetting == null))
                    System.out.println(" Setting Not ok");
                else
                    System.out.println(" Setting ok");
            }*/
            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting.contains("CASHFLOWHISTORY_SUMMARY_VIEW")) {
                    System.out.println("Hit");
                }
                oOut.write((sWorkspaceSetting + "\r\n").getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // reapply sorting after save
        TradeMethods.getSharedInstance().setBlotterSortColumn(col);
        System.out.println(" ------------------- ");


        Enumeration oTimeNSalesViews = g_oViewSettingsManager.getTimenSalesViews().elements();
        while (oTimeNSalesViews.hasMoreElements()) {
            oSetting = (ViewSetting) oTimeNSalesViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) continue;
            if (oSetting.getID().equals("TIME_AND_SALES")) continue;

            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                }
            } catch (Exception e) {
            }
        }

        // market time and sales views
        Enumeration oFilteredTimeNSalesViews = g_oViewSettingsManager.getMarketTimeNSalesViews().elements();
        while (oFilteredTimeNSalesViews.hasMoreElements()) {
            oSetting = (ViewSetting) oFilteredTimeNSalesViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) continue;

            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                    //System.out.println(sWorkspaceSetting);
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
        oFilteredTimeNSalesViews = null;

        Enumeration oDQViews = g_oViewSettingsManager.getDTQViews().elements();

        while (oDQViews.hasMoreElements()) {
            oSetting = (ViewSetting) oDQViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) {
                continue;
            } else {
                //System.out.println("null " +(oSetting.getParent() == null));
            }

            sWorkspaceSetting = oSetting.toWorkspaceString();

            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                } else {
                    //System.out.println(sWorkspaceSetting == null);
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

//           --- Added by Shanika - For Sector Overview ----

        Enumeration sectorOverViews = g_oViewSettingsManager.getsecOverViews().elements();

        while (sectorOverViews.hasMoreElements()) {
            oSetting = (ViewSetting) sectorOverViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) {
                continue;
            } else {
                //System.out.println("null " +(oSetting.getParent() == null));
            }
            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                } else {
                    //System.out.println(sWorkspaceSetting == null);
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

//                  ------- End -------------

        Enumeration oSnapViews = g_oViewSettingsManager.getSnapViews().elements();

        while (oSnapViews.hasMoreElements()) {
            oSetting = (ViewSetting) oSnapViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) {
                continue;
            } else {
                //System.out.println("null " +(oSetting.getParent() == null));
            }

            sWorkspaceSetting = oSetting.toWorkspaceString();

            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                } else {
                    //System.out.println(sWorkspaceSetting == null);
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }
        oSnapViews = null;

        Enumeration oDepthViews = g_oViewSettingsManager.getDepthViews().elements();
        while (oDepthViews.hasMoreElements()) {
            oSetting = (ViewSetting) oDepthViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) {
                continue;
            }

            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;

            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                } else {
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        Enumeration oRQuoteViews = g_oViewSettingsManager.getRQuotesViews().elements();
        while (oRQuoteViews.hasMoreElements()) {
            oSetting = (ViewSetting) oRQuoteViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) {
                continue;
            }

            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                } else {
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        Enumeration oFD2Views = g_oViewSettingsManager.getFD2Views().elements();
        while (oFD2Views.hasMoreElements()) {
            oSetting = (ViewSetting) oFD2Views.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) {
                continue;
            }

            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                } else {
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        // Option Chain Views
        Enumeration oOptionsViews = g_oViewSettingsManager.getOptionChainViews().elements();
        while (oOptionsViews.hasMoreElements()) {
            oSetting = (ViewSetting) oOptionsViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) {
                if (oSetting.getParent() == null)
                    //System.out.println("Parent null >---------- " + oSetting.getType());
                    continue;
            }

            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;

            try {
                // view is not included in the workspace
                if (sWorkspaceSetting != null) {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                    //System.out.println(">> " + sWorkspaceSetting);
                } else {
                    //System.out.println(">> null view found" );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // ==== Mini Trades View ===

        Enumeration oMiniTradeViews = g_oViewSettingsManager.getminiTradeViews().elements();

        while (oMiniTradeViews.hasMoreElements()) {
            oSetting = (ViewSetting) oMiniTradeViews.nextElement();
            if ((oSetting == null) || (oSetting.getParent() == null)) {
                continue;
            } else {
                //System.out.println("null " +(oSetting.getParent() == null));
            }
            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                if (sWorkspaceSetting != null) // view is not included in the workspace
                {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                } else {
                    //System.out.println(sWorkspaceSetting == null);
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        /*Enumeration oAnnouncementViews = g_oViewSettingsManager.getAnnouncementViews().elements();
      while (oAnnouncementViews.hasMoreElements()) {
          oSetting = (ViewSetting) oAnnouncementViews.nextElement();
          if ((oSetting == null) || (oSetting.getParent() == null)) {
              continue;
          }

          sWorkspaceSetting = oSetting.toWorkspaceString();
          if (sWorkspaceSetting == null) continue;

          try {
              if (sWorkspaceSetting != null) // view is not included in the workspace
              {
                  oOut.write((sWorkspaceSetting + "\r\n").getBytes());
              } else {
              }
          } catch (Exception e) {
              //e.printStackTrace();
          }
      }
      oAnnouncementViews = null;*/

        /*
            Forex Board Workspace Settings...

        */
        Enumeration fxViews = ViewSettingsManager.getForexViews().elements();
        while (fxViews.hasMoreElements()) {
            oSetting = (ViewSetting) fxViews.nextElement();

            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                oOut.write((sWorkspaceSetting + "\r\n").getBytes());
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        sWorkspaceSetting = null;

        ////////////////////////////////////////////////////////
        Enumeration oTPCalViews = g_oViewSettingsManager.getDepthCalViews().elements();
        while (oTPCalViews.hasMoreElements()) {
            oSetting = (ViewSetting) oTPCalViews.nextElement();

            sWorkspaceSetting = oSetting.toWorkspaceString();
            if (sWorkspaceSetting == null) continue;
            try {
                oOut.write((sWorkspaceSetting + "\r\n").getBytes());
            } catch (Exception e) {
                //e.printStackTrace();
            }
        }

        Enumeration oFullQviews = g_oViewSettingsManager.getFullQuoteViews().elements();
        while (oFullQviews.hasMoreElements()) {
            oSetting = (ViewSetting) oFullQviews.nextElement();
            if (oSetting.getSubType() == ViewSettingsManager.FULL_QUOTE_VIEW) {
                sWorkspaceSetting = oSetting.toWorkspaceString();
                if (sWorkspaceSetting == null) continue;
                try {
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }
        }

        sWorkspaceSetting = null;

        String path = sFileName.substring(0, sFileName.lastIndexOf(File.separator));
        String fileName = sFileName.substring(sFileName.lastIndexOf(File.separator) + 1);
        String fileNameWithoutExtension = fileName.substring(0, fileName.indexOf("."));
        ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(path + File.separator +
                fileNameWithoutExtension + ".wspx"));
        zipOut.setMethod(ZipOutputStream.DEFLATED);

        // Main board charts
        ViewSetting graphSettings = null;
        GraphFrame graph = null;
        long time = System.currentTimeMillis();
        Object[] desktopFrames = ((TWDesktop) Client.getInstance().getDesktop()).getWindows();
        long layoutTime = System.currentTimeMillis();
        for (int i = 0; i < desktopFrames.length; i++) {
            try {
                if (desktopFrames[i] instanceof GraphFrame) {
                    graph = (GraphFrame) desktopFrames[i];
                    graphSettings = graph.getViewSettings();
                    if ((graphSettings == null) || (graphSettings.getParent() == null)) continue;
                    String layoutFileName = layoutTime + "_" + i + ".out.mcl";
//                    String tempPath = Settings.getAbsolutepath() + "workspaces\\" + layoutFileName;
                    String tempPath = layoutFileName;
                    File tempFile = new File(tempPath);
                    //tempPath = tempFile.getCanonicalPath();
                    tempPath = tempPath.replaceAll("\\\\", "/");
                    //graphSettings.putProperty(ViewConstants.VC_CHART_LAYOUT,Settings.getAbsolutepath()+ "workspaces/" + layoutFileName);
                    graphSettings.putProperty(ViewConstants.VC_CHART_LAYOUT, tempPath);
                    sWorkspaceSetting = graphSettings.toWorkspaceString() +
                            GraphStream.getBasicChartString((GraphFrame) graphSettings.getParent());
                    oOut.write((sWorkspaceSetting + "\r\n").getBytes());

                    ZipEntry chartLayout = new ZipEntry(layoutFileName);
                    zipOut.putNextEntry(chartLayout);
                    File layoutFile = new File(path + File.separator + layoutFileName);
                    LayoutFactory.SaveLayout(layoutFile, new GraphFrame[]{graph});
                    int n;
                    byte[] buffer = new byte[2048];
                    FileInputStream fileinputstream = new FileInputStream(layoutFile);
                    while ((n = fileinputstream.read(buffer)) > -1) {
                        zipOut.write(buffer, 0, n);
                    }
                    fileinputstream.close();
                    layoutFile.delete();
                    zipOut.closeEntry();

                    graph = null;
                    graphSettings = null;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("=================== error loading chart layout in workspace manage =============  ");
            }
        }
        desktopFrames = null;

        oOut.close();

        ZipEntry oldWorkSpace = new ZipEntry(fileName);
        zipOut.putNextEntry(oldWorkSpace);
        FileInputStream fileinputstream = new FileInputStream(sFileName);
        int n;
        byte[] buffer = new byte[2048];
        while ((n = fileinputstream.read(buffer)) > -1) {
            zipOut.write(buffer, 0, n);
        }
        fileinputstream.close();
        File tempFile = new File(sFileName);
        tempFile.delete();
        zipOut.closeEntry();

        // Saving Charts
        // Unichart views
        if (!ChartFrame.isNull()) {
            JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
            Vector<GraphFrame> graphFrames = new Vector<GraphFrame>();
            for (int i = 0; i < frames.length; i++) {
                if (frames[i] instanceof GraphFrame) {
                    graphFrames.add((GraphFrame) frames[i]);
                    try {
                        graph = (GraphFrame) frames[i];
                        if (graph.getGraph().getGDMgr().getBaseGraph() != null) {
                            graphSettings = g_oViewSettingsManager.getSymbolView("OUTER_CHART").getObject();
                            graphSettings.setParent(graph);
                            graphSettings.setID("" + SharedMethods.getSerialID());
                            sWorkspaceSetting = graphSettings.toWorkspaceString() +
                                    GraphStream.getBasicChartString((GraphFrame) graphSettings.getParent());
                            oOut.write((sWorkspaceSetting + "\r\n").getBytes());
                            graph = null;
                            graphSettings = null;
                        }
                    } catch (Exception ex) {
                        //ex.printStackTrace();
                    }
                }
            }
            GraphFrame[] gfArray = new GraphFrame[graphFrames.size()];
            for (int i = 0; i < graphFrames.size(); i++) {
                gfArray[i] = graphFrames.get(i);
            }
            String chartLayoutFileName = "chart_layout.mcl";
            File chartLayoutFile = new File(path + File.separator + chartLayoutFileName);
            LayoutFactory.SaveLayout(chartLayoutFile, gfArray);
            frames = null;
            ZipEntry chartLayout = new ZipEntry(chartLayoutFileName);
            zipOut.putNextEntry(chartLayout);
            fileinputstream = new FileInputStream(chartLayoutFile);
            while ((n = fileinputstream.read(buffer)) > -1) {
                zipOut.write(buffer, 0, n);
            }
            fileinputstream.close();
            zipOut.closeEntry();
            chartLayoutFile.delete();
        }

        zipOut.flush();
        zipOut.close();
        Application.getInstance().fireWorkspaceSaved();
    }

    protected void setSortColumn(ViewSetting oSetting, String value) {
        try {
            oSetting.setSortColumn(intValue(value));
        } catch (Exception ex) {
            oSetting.setSortColumn(-1);
        }

    }

    /**
     * Converts the given value to an int
     */
    protected int intValue(String sValue) throws Exception {
        return (Integer.parseInt(sValue.trim()));
    }

    protected Font getFontObject(String sFont) throws Exception {
        StringTokenizer oTokens = new StringTokenizer(sFont, ",");
        if (oTokens.countTokens() != 3)
            return null;
        else
            return new TWFont(oTokens.nextToken(),
                    intValue(oTokens.nextToken()), intValue(oTokens.nextToken()));
    }

    /**
     * Converts the given value to a byte
     */
    protected byte byteValue(String sValue) throws Exception {
        return (Byte.parseByte(sValue.trim()));
    }

    protected long longValue(String sValue) throws Exception {
        return (Long.parseLong(sValue.trim()));
    }

    protected String stringValue(String sValue) throws Exception {
        if (sValue.equals("null"))
            return null;
        else
            return (sValue.trim());
    }

    /**
     * Returns the view setting from the container, for the criteria
     * given
     */
    protected ViewSetting getViewSetting(Vector oContainer, int iType, String sID) {
        ViewSetting oViewSetting;
        for (int i = 0; i < oContainer.size(); i++) {
            oViewSetting = (ViewSetting) oContainer.elementAt(i);
            if ((oViewSetting.getType() == iType) &&
                    (oViewSetting.getID().equals(sID)))
                return oViewSetting;
        }
        return null;
    }

    /**
     * Set window settings values
     */
    protected void setWindowSettings(ViewSetting oSetting, String sSettings) throws Exception {
        StringTokenizer oTokens = new StringTokenizer(sSettings, ",");
        if (oTokens.countTokens() != 10) {
            oSetting.setLocation(new Point(0, 0));
            oSetting.setSize(new Dimension(500, 500));
            oSetting.setStyle(ViewSettingsManager.STYLE_NORMAL);
            oSetting.setIndex(-1);
            oSetting.setVisible(false);
            oSetting.setHeaderVisible(true);
        } else {
            oSetting.setLocation(new Point(intValue(oTokens.nextToken()), intValue(oTokens.nextToken())));
            oSetting.setSize(new Dimension(intValue(oTokens.nextToken()), intValue(oTokens.nextToken())));
            oSetting.setStyle(intValue(oTokens.nextToken()));
            oSetting.setIndex(intValue(oTokens.nextToken()));
            oSetting.setVisible(intValue(oTokens.nextToken()) == 1);
            oSetting.setHeaderVisible(oTokens.nextToken().equals("v"));
            try {
                oSetting.setFilter(stringValue(oTokens.nextToken()));
                oSetting.setFilterType(intValue(oTokens.nextToken()));
                //System.out.println("filter set");
            } catch (Exception e) {
                e.printStackTrace();
            }
            //System.out.println("setting heder " + oSetting.isHeaderVisible());
        }

    }

    protected void setSortOrder(ViewSetting oSetting, String value) throws Exception {
        try {
            oSetting.setSortOrder(intValue(value));
        } catch (Exception ex) {
            oSetting.setSortOrder(0);
        }
    }

    protected void setSplitLocation(ViewSetting oSetting, String value) throws Exception {
        try {
            oSetting.setSplitLocation(intValue(value));
        } catch (Exception ex) {
            oSetting.setSortOrder(0);
        }
    }

    private void breakTag(String sTag) {
        sTag = sTag.trim();
        if (sTag.length() > 0) {

            int iPipe = sTag.indexOf("|");

            if (iPipe == -1) {
                g_iType = Integer.parseInt(sTag);
                g_sID = null;
            } else {
                g_iType = Integer.parseInt(sTag.substring(0, iPipe));
                try {
                    g_sID = sTag.substring(iPipe + 1).trim();
                } catch (Exception e) {
                    g_sID = null;
                }
            }
        }
    }


    private void adjustFullQuoteSummaryViewToSymbolview() {
        String key = ViewSettingsManager.getSummaryView("MULTIWINDOW").getType() + "|" + "MULTIWINDOW";
        try {
            if (g_oSettings.containsKey(key)) {
                String wspString = (String) g_oSettings.get(key);
                ViewSetting oSetting = ViewSettingsManager.getSummaryView("MULTIWINDOW");

                if (wspString == null) // view is not included in the workspace
                {
                    oSetting.setVisible(false);// remove the view from the desktop
                } else {

                    alterView(oSetting, wspString);
                    if (oSetting.isVisible()) {
                        String altered = WorkspaceAdjuster.getSharedInstance().createFullQuoteSymbolView(oSetting);
                        if (altered != null) {
                            g_oSettings.remove(key);
                            oSetting.setVisible(false);
                            String[] data = altered.split(Meta.RS);
                            g_oSettings.put(data[0], data[1]);
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

}

