package com.isi.csvr;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Sep 18, 2007
 * Time: 12:51:55 PM
 */
public class ColumnTemplateStore {
    private static ColumnTemplateStore self = null;
    private Hashtable<String, ColumnTemplate> store;

    private ColumnTemplateStore() {
        store = new Hashtable<String, ColumnTemplate>();
        load();
    }

    public static ColumnTemplateStore getSharedInstance() {
        if (self == null) {
            self = new ColumnTemplateStore();
        }
        return self;
    }

    public ColumnTemplate getColumnTemplate(int id) {
        return store.get("" + id);
    }

    public Enumeration<String> getIds() {
        return store.keys();
    }

    public Enumeration<ColumnTemplate> getTemplates() {
        return store.elements();
    }

    public void load() {
        String data;
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(Settings.SYSTEM_PATH + "/columntemplate_" + Language.getSelectedLanguage() + ".msf"));
            while (true) {
                data = in.readLine();
                if ((data == null) || (data.trim().equals(""))) break;
                try {
                    String[] records = data.split("\\;");
                    ColumnTemplate template = new ColumnTemplate(records[0], UnicodeUtils.getNativeString(records[1]), new BigInteger(records[2]), new BigInteger(records[3]));
                    store.put(template.getId(), template);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
