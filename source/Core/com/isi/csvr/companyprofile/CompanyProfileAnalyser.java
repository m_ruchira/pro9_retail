package com.isi.csvr.companyprofile;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.UnicodeUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 13, 2005
 * Time: 2:18:52 PM
 */
public class CompanyProfileAnalyser {

    public static CompanyProfileAnalyser self = null;
    private String profileTemplate;

    private CompanyProfileAnalyser() {
        loadTemplate(Language.getLanguageTag());
    }

    public static CompanyProfileAnalyser getSharedInstance() {
        if (self == null) {
            self = new CompanyProfileAnalyser();
        }
        return self;
    }

    public void show(String frame) {
        try {
            String page = profileTemplate;

            Client.getInstance().getBrowserFrame().setTitle(Language.getString("COMPANY_PROFILE"));

            String[] fields = frame.split(Meta.FD);
//            Stock stock=DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(fields[0],fields[1], Integer.parseInt(fields[4])));
            page = page.replace("[COMPANY]", UnicodeUtils.getNativeStringFromCompressed(fields[3]));                                    //DataStore.getSharedInstance().getStockObject()
            page = page.replace("[SYMBOL]", UnicodeUtils.getNativeString(fields[1]));
//            page = page.replace("%%ACTIVITIES%%", UnicodeUtils.getNativeString(fields[2]));
            page = page.replace("[BODY]", UnicodeUtils.getNativeStringFromCompressed(fields[4]));
//            page = page.replace("%%DATE_ESTABLISHED%%", UnicodeUtils.getNativeString(fields[7]));
//            page = page.replace("%%FIN_YEAR_END%%", UnicodeUtils.getNativeString(fields[8]));
//            page = page.replace("%%CONTACT%%", UnicodeUtils.getNativeString(fields[10]));
//            page = page.replace("%%ADDRESS%%", UnicodeUtils.getNativeString(fields[12]));
//            page = page.replace("%%TELEPHONE%%", UnicodeUtils.getNativeString(fields[11]));
//            page = page.replace("%%FAX%%", UnicodeUtils.getNativeString(fields[13]));
//            page = page.replace("%%TELEX%%", UnicodeUtils.getNativeString(fields[14]));
//            page = page.replace("%%EMAIL%%", UnicodeUtils.getNativeString(fields[15]));
//            page = page.replace("%%URL%%", UnicodeUtils.getNativeString(fields[16]));
//            page = page.replace("%%AUTH_SHARE_CAP%%", SharedMethods.toDeciamlFoamat(Double.parseDouble(fields[21])));
//            page = page.replace("%%PAID_SHARES%%", SharedMethods.toDeciamlFoamat(Double.parseDouble(fields[22])));
//            page = page.replace("%%PAIDUP_SHARES%%", SharedMethods.toDeciamlFoamat(Double.parseDouble(fields[25])));
//            page = page.replace("%%ISSUED_SHARES%%", SharedMethods.toIntegerFoamat(Long.parseLong(fields[23])));
//            page = page.replace("%%PAR_VALUE%%", SharedMethods.toDeciamlFoamat(Double.parseDouble(fields[24])));
//            page = page.replace("%%CHAIRMAN%%", UnicodeUtils.getNativeString(fields[17]));
//            page = page.replace("%%MANAGER%%", UnicodeUtils.getNativeString(fields[19]));
//            page = page.replace("%%DIRECTORS%%", UnicodeUtils.getNativeString(fields[18]));
//            page = page.replace("%%AUDITORS%%", UnicodeUtils.getNativeString(fields[9]));
            Client.getInstance().getBrowserFrame().setContentType("text/html");
            Client.getInstance().getBrowserFrame().setText(page);
            Client.getInstance().getBrowserFrame().show(Constants.DOC_TYPE_COMPANY_PROFILE);
            Client.getInstance().getBrowserFrame().bringToFont();
//            stock=null;
//            FileOutputStream out = new FileOutputStream("profile.html");
//            out.write(page.getBytes());
//            out.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadTemplate(String language) {
        StringBuffer buffer = new StringBuffer();
        byte[] temp = new byte[1000];
        int count = 0;

        try {
            InputStream in = new FileInputStream("./Templates/Company_Profile_" + language + ".htm");
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            profileTemplate = buffer.toString();
            buffer = null;
            in = null;
            temp = null;
        } catch (IOException ex) {
            profileTemplate = "";
        }
    }
}
