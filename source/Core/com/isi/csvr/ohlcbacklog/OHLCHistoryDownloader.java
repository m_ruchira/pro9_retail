package com.isi.csvr.ohlcbacklog;

import com.isi.csvr.TradeStation.TradeStationManager;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.downloader.HistoryDownloadManager;
import com.isi.csvr.downloader.ZipFileDownloader;
import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 21, 2003
 * Time: 11:28:57 AM
 * To change this template use Options | File Templates.
 */
public class OHLCHistoryDownloader implements Runnable, ProgressListener {

    private static OHLCHistoryDownloader self = null;
    int count = 0;
    private OHLCFileListItem itemInProgress;
    private ProgressListener uiProgressListener;
    private SimpleDateFormat format;

    public OHLCHistoryDownloader() {
        format = new SimpleDateFormat("yyyyMMdd");
    }

    public static OHLCHistoryDownloader getSharedInstance() {
        if (self == null) {
            self = new OHLCHistoryDownloader();
        }
        return self;
    }

    public static void createFolder(String path) {
        try {
            File folder = new File(path);
            if (!folder.exists()) {

                folder.mkdirs();
            }
            folder = null;
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public void clearOlderFiles(String exchange) {
        ArrayList<OHLCFileListItem> lists = OHLCBacklogStore.getSharedInstance().getDataStore(exchange);
        String[] fnames = new String[lists.size()];
        for (int i = 0; i < lists.size(); i++) {
            fnames[i] = lists.get(i).getFileName();

        }
        File dir = new File(Settings.INTRADAY_OHLC_PATH + exchange);
        File[] selectedFiles = dir.listFiles();
        Hashtable filehash = new Hashtable();
        for (int i = 0; i < selectedFiles.length; i++) {
            filehash.put(Integer.parseInt(selectedFiles[i].getName()), selectedFiles[i]);
        }
        Enumeration e = filehash.keys();
        while (e.hasMoreElements()) {
            int filename = (Integer) e.nextElement();
            try {
                if (filename < Integer.parseInt(fnames[lists.size() - 1])) {
                    System.out.println("Success.....");
                    try {
                        deleteOldest((File) filehash.get(filename));
                    } catch (Exception e1) {
                        //                     e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                        System.out.println("File not deleted...");
                    }
                }
            } catch (Exception e1) {
//                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                System.out.println("File not delete....");
            }
        }
        filehash.clear();

    }

    public void startDownload() {

        boolean canProceed = false;

        String exchange = OHLCBacklogStore.getSharedInstance().getCurrentExchange();
        if (ExchangeStore.getSharedInstance().getExchange(exchange).getMarketStatus() == Meta.MARKET_OPEN) {
            int result = SharedMethods.showConfirmMessage(Language.getString("TRADE_HISTORY_DOWNLOAD_WARING"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                canProceed = true;
            }
        } else {
            canProceed = true;
        }

        if (canProceed) {
            Thread thread = new Thread(this, "OHLCHistoryDownloader");
            thread.start();
        }
    }

//    public void removeProgressListener(ProgressListener listener) {
//    }

    public void addProgressListener(ProgressListener listener) {
        uiProgressListener = listener;
    }

    public void run() {
        String exchange = OHLCBacklogStore.getSharedInstance().getCurrentExchange();
        int size = OHLCBacklogStore.getSharedInstance().getDataStore(exchange).size();

        // Queue all
        for (int i = 0; i < size; i++) {
            OHLCFileListItem item = OHLCBacklogStore.getSharedInstance().getDataStore(exchange).get(i);
            if (item.isChecked()) {
                item.setStatus(OHLCFileList.STATUS_QUEUED);
            }
            item = null;
        }
        OHLCBacklogStore.getSharedInstance().fireDataChanged();

        // download queued
        for (int i = 0; i < size; i++) {

            OHLCFileListItem item = OHLCBacklogStore.getSharedInstance().getDataStore(exchange).get(i);
            if (item.getStatus() == OHLCFileList.STATUS_QUEUED) {
                item.setStatus(OHLCFileList.STATUS_DOWNLOADENG);
                if (download(exchange, item) == true) {
                    count = count + 1;
                    item.setStatus(OHLCFileList.STATUS_SAVED);
                    item.setProgress("");
                } else {
                    item.setStatus(OHLCFileList.STATUS_ERROR);
                }
            }
            item = null;
            OHLCBacklogStore.getSharedInstance().fireDataChanged();
        }
        System.out.println("File count : " + count);

    }

    public boolean download(String exchange, OHLCFileListItem item) {
        boolean bSucess = false;
        int count = 0;

        try {
            itemInProgress = item;

            String url = Meta.OHLC_DOWNLOAD + Meta.DS + exchange + Meta.FD + item.getFileName() + "\n";
            ZipFileDownloader downloader = new ZipFileDownloader(url, exchange, item.getFileName());
            downloader.addProgressListener(this);
            downloader.addProgressListener(uiProgressListener);
            File[] files = downloader.downloadFiles();

            if (files[0].length() > 0) {
                try {
                    extractFile(Settings.INTRADAY_OHLC_PATH + exchange + "\\" + item.getFileName(), files[0]);
                } catch (Exception e) {
                    System.out.println("File not extracted....");
                }
                count = count + 1;
                bSucess = true;
            }
            if (!Settings.isConnected()) {
                bSucess = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            bSucess = false;
        }
        itemInProgress = null;
        OHLCStore.getInstance().loadIntradayHistory(exchange);
        TradeStationManager.getInstance().updateIntrdayHistory(exchange, item.getFileName());
        HistoryDownloadManager.getSharedInstance().fireHistoryIntradayDownloaded(exchange);
        return bSucess;
    }

    public void sortDirectory() {
        String exchange = OHLCBacklogStore.getSharedInstance().getCurrentExchange();
        File dir = new File(Settings.INTRADAY_OHLC_PATH + exchange);
        File[] selectedFiles = dir.listFiles();
        if (selectedFiles != null) {
            for (int i = 0; i < selectedFiles.length; i++) {
            }
            Arrays.sort(selectedFiles, new FileComparator());
            for (int i = 0; i < selectedFiles.length; i++) {
            }
        }
    }

    public boolean deleteOldest(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteOldest(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    /* Progress Listener Methods */
    public void setProcessStarted(String id) {
        try {
            itemInProgress.setProgress("0%");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setMax(String id, int max) {

    }

    public void setProcessCompleted(String id, boolean sucess) {
        try {
            itemInProgress.setProgress("");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setProgress(String id, int progress) {
        try {
            itemInProgress.setProgress(progress + "%");
            uiProgressListener.setProgress(null, 0);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private void extractFile(String filepath, File file) {
        synchronized (Constants.FILE_EXTRACT_LOCK) {
            try {
                String path;
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                byte[] bytData = new byte[1000];
                int i;

                while (true) {
                    try {
                        ZipEntry oEntry = zIn.getNextEntry();
                        if (oEntry == null)
                            break;
                        if (oEntry.isDirectory()) {
                            path = filepath + "/" + oEntry.getName();
                            createFolder(path);
                        } else {
                            path = filepath;
                            createFolder(path);
                        }
                        File f = new File(oEntry.getName());
                        FileOutputStream oOut = new FileOutputStream(path + "/" + f.getName());
                        while (true) {
                            i = zIn.read(bytData);
                            if (i == -1)
                                break;
                            oOut.write(bytData, 0, i);
                        }
                        zIn.closeEntry();
                        oOut.close();
                        f = null;
                        oEntry = null;
                        oOut = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Thread.sleep(2);
                }
                zIn.close();
                zIn = null;
                bytData = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();

    }

    private static class FileComparator
            implements Comparator {
        private Collator c = Collator.getInstance();

        public int compare(Object o1,
                           Object o2) {
            if (o1 == o2)
                return 0;

            String f1 = (String) o1;
            String f2 = (String) o2;

//            if (f1.isDirectory() && f2.isFile()     )
//                return -1;
//            if (f1.isFile()      && f2.isDirectory())
//                return 1;

            return c.compare(f1, f2);
        }
    }
}
