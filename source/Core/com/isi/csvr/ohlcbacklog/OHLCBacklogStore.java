package com.isi.csvr.ohlcbacklog;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 21, 2003
 * Time: 9:26:58 AM
 * To change this template use Options | File Templates.
 */
public class OHLCBacklogStore implements ListModel, Comparator, ConnectionListener, FilenameFilter {

    private static OHLCBacklogStore self = null;
    public String currentFile;
    private Hashtable<String, ArrayList<OHLCFileListItem>> dataStore;
    private OHLCDataChangeListener dataListener;
    private SimpleDateFormat dateFormatOut;
    private SimpleDateFormat dateFormatIn;
    private boolean autoDownloadRecent = false;
    private String currentExchange;

    private OHLCBacklogStore() {
        dataStore = new Hashtable<String, ArrayList<OHLCFileListItem>>();
        //dataStore.setComparator(this);
        ConnectionNotifier.getInstance().addConnectionListener(this);
        requestData();
        dateFormatIn = new SimpleDateFormat("yyyyMMdd");
        dateFormatOut = new SimpleDateFormat("dd/MM/yyyy");
        String value = HistorySettings.getProperty("AUTO_DOWNLOAD_LAST_TNS");
        if ((value == null) || (value.equals("")) || (value.equals("0"))) {
            setAutoDownloadRecent(false);
        } else {
            setAutoDownloadRecent(true);
        }
        value = null;
    }

    public static OHLCBacklogStore getSharedInstance() {
        if (self == null) {
            self = new OHLCBacklogStore();
        }
        return self;
    }

    public ArrayList<OHLCFileListItem> getDataStore(String exchange) {
        ArrayList<OHLCFileListItem> lists = dataStore.get(exchange);
        if (lists == null) {
            lists = new ArrayList<OHLCFileListItem>();
            dataStore.put(exchange, lists);
        }
        return lists;
    }

    public void clear() {
        dataStore.clear();
    }

    public synchronized void setData(String data) {
        try {
            String[] tokens = data.split(Meta.FD);
            String exchange = tokens[0];
            System.out.println(" ===>  OHLC History " + exchange + " " + data);
            try {
                File path = new File(Settings.getAbsolutepath() + "ohlc/" + exchange);
                path.mkdirs();
            } catch (Exception e) {
            }
//            OHLCBacklogStore.getSharedInstance().updateListFromFiles(exchange);

            for (int i = 1; i < tokens.length; i++) {
                String[] file = tokens[i].split(Meta.ID);
                OHLCFileListItem item = new OHLCFileListItem();
                try {
                    item.setDescription(dateFormatOut.format(dateFormatIn.parse(file[0].substring(0, 8))));
                    ExchangeStore.getSharedInstance().getExchange(exchange).setLastIntradayHistorydate(file[0].substring(0, 8));
                } catch (Exception e) {
                    item.setDescription(file[i]);
                }
                currentFile = file[0];
                item.setFileName(currentFile);
                item.setSize(Integer.parseInt(file[1]));
                item = updateStore(exchange, item);
                File[] files = getDownloadedFiles(exchange);
                checkAvalability(files, item); // check if the file is already downloaded
                file = null;
                files = null;
                item = null;
            }
            dataListener.listDownloaded();
            if (Settings.isTCPMode()) {
                clearDumpFolder(exchange);
            }
            fireDataChanged();
            OHLCHistoryDownloader.getSharedInstance().clearOlderFiles(exchange);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateListFromFiles(String exchange) {
        try {
            File path = new File(Settings.INTRADAY_OHLC_PATH + exchange);
            File[] files = path.listFiles();
            path = null;

            for (OHLCFileListItem item : getDataStore(exchange)) {
                boolean found = false;
                for (File file : files) {
                    if (item.getFileName().equals(file.getName())) {
                        item.setStatus(OHLCFileList.STATUS_SAVED);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    item.setStatus(OHLCFileList.STATUS_NOT_SAVED);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        fireDataChanged();
    }

    //public void downloadRecentFile(String exchange){
    /*try {
        if (isAutoDownloadRecent() && (Settings.getMarketStatus() != Settings.MARKET_OPEN)){
            checkRecentFile(exchange);
        }
    } catch (Exception e) {
        e.printStackTrace();  //To change body of catch statement use Options | File Templates.
    }*/
    //}

    public synchronized boolean checkRecentFile(String exchange) {
        try {
            currentFile = getRecentFilename(exchange);
            File[] files = getDownloadedFiles(exchange);
            OHLCFileListItem ref = (getDataStore(exchange)).get(0);
            if ((ref.getStatus() != OHLCFileList.STATUS_QUEUED) &&
                    (ref.getStatus() != OHLCFileList.STATUS_DOWNLOADENG) &&
                    (ref.getStatus() != OHLCFileList.STATUS_SAVED) &&
                    (!isDataDownloaded(files, ref))) {
                ref.setStatus(OHLCFileList.STATUS_QUEUED);
                OHLCHistoryDownloader.getSharedInstance().startDownload();
                return true;
            }
            files = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
        return false;
    }

    private void clearDumpFolder(String exchange) {
        File folder;
        File[] files;
        try {
            ArrayList<OHLCFileListItem> items = getDataStore(exchange);
            folder = new File(Settings.INTRADAY_OHLC_PATH + exchange);
            files = folder.listFiles();
            boolean found;
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                found = false;
                for (int j = 0; j < items.size(); j++) {
                    OHLCFileListItem item = items.get(j);
                    if (file.getName().equals(item.getFileName())) {
                        item = null;
                        found = true;
                        break;
                    }
                    item = null;
                }
                if (!found) {
                    file.delete();
                }
                file = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            files = null;
            folder = null;
        }
    }

    public void reValidateRecentFile(String exchange) {
        currentFile = getRecentFilename(exchange);
        File[] files = getDownloadedFiles(exchange);
        OHLCFileListItem item = getRecentFile(exchange);
        checkAvalability(files, item);
        files = null;
    }

    public String getRecentFilename(String exchange) {
        try {
            OHLCFileListItem ref = (getDataStore(exchange)).get(0);
            return ref.getFileName();
        } catch (Exception e) {
            return null;
        }
    }

    public OHLCFileListItem getRecentFile(String exchange) {
        try {
            OHLCFileListItem ref = (getDataStore(exchange)).get(0);
            return ref;
        } catch (Exception e) {
            return null;
        }
    }

    public void fireDataChanged() {
        if (dataListener != null) {
            dataListener.dataChanged();
        }
    }

    public File[] getDownloadedFiles(String exchange) {
        File path = new File(Settings.INTRADAY_OHLC_PATH + exchange);
        File[] files = path.listFiles(this);
        path = null;
        return files;
    }

    private boolean isDataDownloaded(File[] files, OHLCFileListItem item) {
        try {
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.getName().equals(item.getFileName())) { // folder is available
                    return (files[0].list().length > 0); // if files are available in it, seems ok
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public void checkAvalability(File[] files, OHLCFileListItem item) {
        if ((item.getStatus() == OHLCFileList.STATUS_NOT_SAVED) ||
                (item.getStatus() == OHLCFileList.STATUS_SAVED) ||
                (item.getStatus() == OHLCFileList.STATUS_ERROR)) {
            if (isDataDownloaded(files, item)) { // check if the file is downloaded
                item.setStatus(OHLCFileList.STATUS_SAVED);
            } else {
                item.setStatus(OHLCFileList.STATUS_NOT_SAVED);
            }
        }
    }

    private OHLCFileListItem updateStore(String exchange, OHLCFileListItem item) {
        int index = Collections.binarySearch(getDataStore(exchange), item, this);   //indexOfContainingValue(item);
        if (index >= 0) { // item already in. Update fields only
            OHLCFileListItem ref = (getDataStore(exchange)).get(index);
            ref.setSize(item.getSize());
            return ref;
        } else { // insert the item
            (getDataStore(exchange)).add((index + 1) * -1, item);
            return item;
        }
    }

    public void requestData() {
//        if (Settings.isConnected()) {
//            SendQFactory.addContentData(Constants.PATH_PRIMARY, Meta.TRADE_HISTORY_LIST_REQUEST + Meta.DS + "0", null); // time & sales file list
//        }
    }

    public void removeDataListener() {
        dataListener = null;
    }

    public void addDataListener(OHLCDataChangeListener dataListener) {
        this.dataListener = dataListener;
    }

    public boolean isAutoDownloadRecent() {
        return autoDownloadRecent;
    }

    public void setAutoDownloadRecent(boolean autoDownloadRecent) {
        this.autoDownloadRecent = autoDownloadRecent;
    }

    public String getCurrentExchange() {
        return currentExchange;
    }

    public void setCurrentExchange(String currentExchange) {
        this.currentExchange = currentExchange;
    }

    /*---- List Model methods ----*/
    public int getSize() {
        try {
            return (getDataStore(getCurrentExchange())).size();
        } catch (Exception e) {
            return 0;
        }
    }

    public Object getElementAt(int index) {
        return (getDataStore(getCurrentExchange())).get(index);
    }

    public void addListDataListener(ListDataListener l) {
    }

    public void removeListDataListener(ListDataListener l) {
    }

    /*---- Comparator Methods ----*/
    public int compare(Object o1, Object o2) {
        OHLCFileListItem i1 = (OHLCFileListItem) o1;
        OHLCFileListItem i2 = (OHLCFileListItem) o2;

        return i2.getFileName().compareTo(i1.getFileName());
    }

    /*---- Connection Listener methods ----*/
    public void twConnected() {
//        SendQFactory.addContentData(Constants.PATH_PRIMARY, Meta.TRADE_HISTORY_LIST_REQUEST + Meta.DS + "0", null);
    }

    public void twDisconnected() {
    }

    /*---- File Name filter methods ----*/
    public boolean accept(File dir, String name) {
        try {
            if (currentFile.equals(name))
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }
}
