package com.isi.csvr.ohlcbacklog;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 20, 2003
 * Time: 1:57:30 PM
 * To change this template use Options | File Templates.
 */
public class OHLCFileListItem {
    private boolean checked = false;
    private String description = null;
    private String fileName;
    private String progress;
    private byte status = OHLCFileList.STATUS_NOT_SAVED;
    private int size;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte getStatus() {

        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }
}


