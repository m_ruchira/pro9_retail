package com.isi.csvr.ohlcbacklog;

import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 20, 2003
 * Time: 1:31:06 PM
 * To change this template use Options | File Templates.
 */
public class OHLCFileList extends JList implements MouseListener, MouseMotionListener {

    public static byte STATUS_NOT_SAVED = 0;
    public static byte STATUS_SAVED = 1;
    public static byte STATUS_DOWNLOADENG = 2;
    public static byte STATUS_QUEUED = 3;
    public static byte STATUS_ERROR = 4;

    private ImageIcon[] icons;
    private Color foreground;
    private Color backround;

    public OHLCFileList(ListModel dataModel) {
        super(dataModel);
        init();
    }

    public OHLCFileList(final Object[] listData) {
        super(listData);
        init();
    }

    public OHLCFileList(final Vector listData) {
        super(listData);
        init();
    }

    public OHLCFileList() {
        init();
    }

    private void init() {
        addMouseListener(this);
        addMouseMotionListener(this);
        setCellRenderer(new JMultiColumnListRenderer());
        icons = new ImageIcon[5];
        icons[STATUS_SAVED] = new ImageIcon("images/common/tnsSaved.gif");
        icons[STATUS_NOT_SAVED] = new ImageIcon("images/common/tnsNotSaved.gif");
        icons[STATUS_DOWNLOADENG] = new ImageIcon("images/common/tnsDownloading.gif");
        icons[STATUS_QUEUED] = new ImageIcon("images/common/tnsQueued.gif");
        icons[STATUS_ERROR] = new ImageIcon("images/common/tnsError.gif");
        initColors();
    }

    public void initColors() {
        foreground = Theme.getColor("LIST_FGCOLOR");
        backround = Theme.getColor("LIST_BGCOLOR");
    }

    public boolean isSelected(int index) {
        return ((OHLCFileListItem) getModel().getElementAt(index)).isChecked();
    }

    /**
     * Returns an array on indices with checked items in the list
     *
     * @return int[] array of indices with checked items
     */
    public int[] getSelectedIndices() {
        int selectedCount = 0;

        for (int i = 0; i < getModel().getSize(); i++) {
            if (((OHLCFileListItem) getModel().getElementAt(i)).isChecked()) {
                selectedCount++;
            }
        }
        int[] selectedIndices = new int[selectedCount];
        int j = 0;
        for (int i = 0; i < getModel().getSize(); i++) {
            if (((OHLCFileListItem) getModel().getElementAt(i)).isChecked()) {
                selectedIndices[j] = i;
                j++;
            }
        }
        return selectedIndices;
    }

    /**
     * Returns an array on objects with checked items in the list
     *
     * @return Object[] array of objects with checked items
     */
    public Object[] getSelectedValues() {
        int selectedCount = 0;

        for (int i = 0; i < getModel().getSize(); i++) {
            if (((OHLCFileListItem) getModel().getElementAt(i)).isChecked()) {
                selectedCount++;
            }
        }
        Object[] selectedIndices = new Object[selectedCount];
        int j = 0;
        for (int i = 0; i < getModel().getSize(); i++) {
            if (((OHLCFileListItem) getModel().getElementAt(i)).isChecked()) {
                selectedIndices[j] = getModel().getElementAt(i);
                j++;
            }
        }
        return selectedIndices;
    }

    public void unselectAll() {
        for (int i = 0; i < getModel().getSize(); i++) {
            ((OHLCFileListItem) getModel().getElementAt(i)).setChecked(false);
        }
    }

    public void getFileList() {
        for (int i = 0; i < getModel().getSize(); i++) {
            System.out.println("file name : " + (OHLCFileListItem) getModel().getElementAt(i));
        }
    }

    public void selectAll() {
        for (int i = 0; i < getModel().getSize(); i++) {
            ((OHLCFileListItem) getModel().getElementAt(i)).setChecked(true);
        }
    }

    private boolean isFileSizeOkToOpen(String fileName) {
        File file = new File(fileName);
        if (file.length() > (Constants.OPENABLE_TRADE_FILE_SIZE)) {
            int status = SharedMethods.showConfirmMessage(Language.getString("MSG_LARGE_TRADE_FILE"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
            return (status == JOptionPane.YES_OPTION);
//            return false;
        } else {
            return true;
        }
    }

    private int showExtractPopup() {
        ExtractConfirmDialog confirmDialog = new ExtractConfirmDialog();
        return confirmDialog.showDialog();
    }


    public void mouseClicked(MouseEvent e) {
        try {
            String exchange = OHLCBacklogStore.getSharedInstance().getCurrentExchange();
            OHLCFileListItem item = ((OHLCFileListItem) getModel().getElementAt(getSelectedIndex()));
            CellComponent component = (CellComponent) getCellRenderer().getListCellRendererComponent(this, item, getSelectedIndex(), false, false);
            String archive = Settings.INTRADAY_OHLC_PATH + "\\" + exchange + "\\" + item.getFileName();

            if ((item.getStatus() != STATUS_DOWNLOADENG) && (item.getStatus() != STATUS_QUEUED)) {
                item.setChecked(component.isCheckCLieked(e.getPoint()) && !item.isChecked());
                if (component.isOpenClieked(e.getPoint())) {
                    if (isFileSizeOkToOpen(archive)) { // check if the file is safe to open in memory
//                        DetailedTradeStore.getSharedInstance().setFileItem(item);
                        ////////////////////////////////////2222222222222222222222222
//                        Client.getInstance().showDetailedTrades();
                    } else { // no. must export file file
                        int value = showExtractPopup();
                        if ((value == ExtractConfirmDialog.STATUS_EXTRACT) ||
                                (value == ExtractConfirmDialog.STATUS_EXTRACT_OPEN)) {
                            OHLCExporter OHLCExporter = new OHLCExporter(exchange, item.getFileName(), value == ExtractConfirmDialog.STATUS_EXTRACT_OPEN);
                            OHLCExporter.start();
                        }
                    }
                }
            }
            repaint();
            item = null;
            component = null;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
//        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
//        try {
//            int location = locationToIndex(e.getPoint());
//            OHLCFileListItem item = ((OHLCFileListItem) getModel().getElementAt(location));
//            CellComponent component = (CellComponent) getCellRenderer().getListCellRendererComponent(this, item, getSelectedIndex(), false, false);
//            if (component.isOpenClieked(e.getPoint())) {
//                setCursor(new Cursor(Cursor.HAND_CURSOR));
//            } else {
//                setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
//            }
//            component = null;
//            item = null;
//        } catch (Exception e1) {
//
//        }
    }

    class JMultiColumnListRenderer extends CellComponent implements ListCellRenderer {

        public JMultiColumnListRenderer() {
            super();
            setOpaque(true);
            status.setOpaque(false);

        }

        public Component getListCellRendererComponent(JList list, Object value,
                                                      int index, boolean isSelected, boolean cellHasFocus) {

            OHLCFileListItem item = (OHLCFileListItem) value;
            descrLabel.setText(item.getDescription());
            size.setText((item.getSize() / 1024) + "k");
            try {
                imageLabel.setIcon(icons[item.getStatus()]);
            } catch (Exception e) {
            }
            imageLabel.setText(item.getProgress());
            status.setSelected(item.isChecked());
            if ((item.getStatus() == STATUS_QUEUED) || (item.getStatus() == STATUS_DOWNLOADENG)) {
                status.setEnabled(false);
            } else {
                status.setEnabled(true);
            }
            showOpen(item.getStatus() == STATUS_SAVED);
            item = null;

            setBackground(backround);
            setForeground(foreground);// list.getForeground());
            setEnabled(list.isEnabled());
            setFont(list.getFont());

            return this;
        }
    }

    class CellComponent extends JPanel {
        JCheckBox status = new JCheckBox();
        JLabel descrLabel;
        JLabel imageLabel;
        JLabel size;
        JLabel open;
        String openText;

        public CellComponent() {
            descrLabel = new JLabel();
            imageLabel = new JLabel();
            open = new JLabel();
            size = new JLabel();
            JLabel dummy = new JLabel("");
            size.setHorizontalAlignment(JLabel.TRAILING);
            imageLabel.setHorizontalAlignment(JLabel.LEFT);
            openText = Language.getString("DOWNLOADED"); //Language.getString("OPEN_FILE");
            open.setBackground(Color.blue);
            setLayout(new FlowLayout(FlowLayout.LEADING, 8, 2));
            add(status);
            add(descrLabel);
            add(size);
            add(dummy);
            add(imageLabel);
            add(open);
            dummy.setPreferredSize(new Dimension(5, 20));
            descrLabel.setPreferredSize(new Dimension(80, 20));
            size.setPreferredSize(new Dimension(50, 20));
            imageLabel.setPreferredSize(new Dimension(50, 20));
            GUISettings.applyOrientation(this);
        }

        public boolean isOpenClieked(Point point) {
//            if (Language.isLTR()){
//                if ((point.x >= open.getBounds().x) && (point.x <= open.getBounds().x + open.getPreferredSize().getWidth()))
//                    return true;
//                else
//                    return false;
//            }else {
//                if ((point.x <= open.getBounds().x) &&
//                        (open.getPreferredSize().getWidth() > 0))
//                    return true;
//                else
//                    return false;
//            }
            return false;
        }

        public boolean isCheckCLieked(Point point) {
            if ((point.x >= status.getBounds().x) && (point.x <= status.getBounds().x + status.getPreferredSize().getWidth()))
                return true;
            else
                return false;
        }

        public void showOpen(boolean show) {
            if (show) {
                open.setText(openText);
            } else {
                open.setText("");
            }
        }

        public void setForeground(Color fg) {
            try {
                descrLabel.setForeground(fg);
                imageLabel.setForeground(fg);
                size.setForeground(fg);
                open.setForeground(fg);
            } catch (Exception e) {

            }
        }
    }
}
