package com.isi.csvr.topstocks;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: ISI Sri Lanka</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class TopstocksObject {
    private String symbol;
    private int instrument;
    private String symbolCode;
    private double lastTrade;
    private double netChange;
    private double perChange;
    private long volume;
    private long trades;
    private String description;
    private int mode;
    private int decimalCount = 2;
    private int decimalCorrectionFactor = 1;
    private double high_52_week = 0;
    private double low_52_week = 0;

    public TopstocksObject(int decimalCount) {
        this.decimalCount = decimalCount;
    }

    public void setData(String[] data) {
        this.symbol = data[0];
        this.instrument = Integer.parseInt(data[7]);
        try {
            this.lastTrade = SharedMethods.getDouble(data[1]);
            this.netChange = SharedMethods.getDouble(data[2]);
            this.perChange = SharedMethods.getDouble(data[3]);
            this.volume = SharedMethods.getLong(data[4]);
            this.trades = SharedMethods.getLong(data[5]);
            try {
                this.decimalCount = Integer.parseInt(data[6]);
                this.decimalCorrectionFactor = Integer.parseInt(data[8]);
                this.description = data[9];
                setHigh_52_week(SharedMethods.getDouble(data[10]));
                setLow_52_week(SharedMethods.getDouble(data[11]));

            } catch (Exception e) {
//                this.decimalCount = 2;
//                this.decimalCorrectionFactor=1;
                setHigh_52_week(0);
                setLow_52_week(0);

            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public int getInstrument() {
        return instrument;
    }

    public void setInstrument(int instrument) {
        this.instrument = instrument;
    }

    public double getNetChange() {
        return netChange / decimalCorrectionFactor;
    }

    public void setNetChange(float netChange) {
        this.netChange = netChange;
    }

    public double getLastTrade() {
        return lastTrade / decimalCorrectionFactor;
    }

    public void setLastTrade(double lastTrade) {
        this.lastTrade = lastTrade;
    }

    public double getPerChange() {
        return perChange / decimalCorrectionFactor;
    }

    public void setPerChange(float perChange) {
        this.perChange = perChange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbolCode() {
        return symbolCode;
    }

    public void setSymbolCode(String symbolCode) {
        this.symbolCode = symbolCode;
    }

    public long getVolume() {
        return volume;///decimalCorrectionFactor;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public long getTrades() {
        if (trades < 0) {
            trades = 0;
        }
        return trades;///decimalCorrectionFactor;
    }

    public void setTrades(long trades) {
        this.trades = trades;
    }

    public String getDescription() {
        if ((description == null) || (description.equals("")) || (description.equals(Language.getString("NA"))))
            return symbol;
        else
            return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getDecimalCount() {
        return decimalCount;
    }

    public String toString() {
        return symbol + " Last " + lastTrade + " Per " + perChange + " Net " + netChange;
    }

    public double getHigh_52_week() {
        return high_52_week / decimalCorrectionFactor;
    }

    public void setHigh_52_week(double high_52_week) {
        this.high_52_week = high_52_week;
    }

    public double getLow_52_week() {
        return low_52_week / decimalCorrectionFactor;
    }

    public void setLow_52_week(double low_52_week) {
        this.low_52_week = low_52_week;
    }
}