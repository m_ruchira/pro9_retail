package com.isi.csvr.topstocks;

import com.isi.csvr.SymbolMaster;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.shared.*;

import java.util.Arrays;
import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TopStocksStore implements Runnable, Comparator {
    private static final String DEFAULT_MARKET_CODE = "-1";
    private static TopStocksStore self = null;
    private boolean continueLoop;
    private boolean dataReceived;
    private TopStocksWindow parent;
    private int requestType = -1;
    private String exchange;
    private String market;
    private int instrumentType = -1;
    private boolean isDefaultExchange;
    private Symbols symbols;
    private DynamicArray store;
    private boolean isInstrumentFilterEnabled = true;

    public TopStocksStore() {
        store = new DynamicArray();
    }

    public static synchronized TopStocksStore getSharedInstance() {
        if (self == null) {
            self = new TopStocksStore();
        }
        return self;
    }

    public void setInstrumentFilterEnabled(boolean status) {
        this.isInstrumentFilterEnabled = status;
    }

    public void setParent(TopStocksWindow parent) {
        this.parent = parent;
    }

    public void clear() {
        store.clear();
    }

    public DynamicArray getTopStocks() {
        return store;
    }

    public void setTopStocks(TopstocksObject oTops) {
        store.add(oTops);
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public void setSymbols(Symbols symbols) {
        this.symbols = symbols;
    }

    public void run() {
        continueLoop = true;
        while (continueLoop) {
            if (parent.isWindowVisible()) { //Settings.isConnected() && 
                if (isDefaultExchange) {
                    updateData();
                    sleepMe(1000);
                } else {
                    requestTopStocksData();
                    sleepMe(10000);
                }
            } else {
                sleepMe(3000);
            }
        }
    }


    public void requestTopStocksData() {
        String strReq = null;
        try {
            if ((exchange != null) && (requestType >= 0)) {
                if (ExchangeStore.isExpired(exchange)) {
                    return;
                }
                if (ExchangeStore.isInactive(exchange)) {
                    return;
                }
                /*while(!dataReceived){
                    sleepMe(1000);
                }*/
                if (instrumentType == -1) {
                    instrumentType = Meta.INSTRUMENT_QUOTE;
                }
                if (market != null) {
                    strReq = Meta.TOP_STOCKS + Meta.DS + exchange + Meta.FD + requestType + Meta.FD + market + Meta.FD + instrumentType;
                } else {
                    strReq = Meta.TOP_STOCKS + Meta.DS + exchange + Meta.FD + requestType + Meta.FD + DEFAULT_MARKET_CODE + Meta.FD + instrumentType;
                }
                byte path = 0;
                try {
                    path = ExchangeStore.getSharedInstance().getExchange(exchange).getPath();
                } catch (Exception e) {
                    path = Constants.PATH_SECONDARY;
                }
                dataReceived = false;
                SendQFactory.addData(path, strReq);
            }
        } catch (Exception ex) {
        }
        strReq = null;
    }

    private void updateData() {
        try {
            boolean valid = false;
            int index = 0;
            if ((symbols == null) || symbols.equals("")) {
                return;
            }
            Arrays.sort(symbols.getSymbols(), this);
            TopStocksStore.getSharedInstance().clear();
            for (int i = 0; (i < symbols.getSymbols().length) && (index < 10); i++) {
                Stock stock = DataStore.getSharedInstance().getStockObject(symbols.getSymbols()[i]);
                if ((market != null) && (!market.equals(DEFAULT_MARKET_CODE))) {
                    if ((stock.getMarketID().equals(market)) && ((!isInstrumentFilterEnabled) || (instrumentType == -1) || (SharedMethods.getSymbolType(stock.getInstrumentType()) == instrumentType))) {
                        switch (requestType) {
                            case Meta.MOST_ACTIVE_BY_VOLUME:
                                valid = stock.getVolume() > 0;
                                break;
                            case Meta.MOST_ACTIVE_BY_TRADES:
                                valid = stock.getNoOfTrades() > 0;
                                break;
                            case Meta.TOP_GAINERS_BY_PCNTCHANGE:
                                valid = stock.getPercentChange() > 0;
                                break;
                            case Meta.TOP_LOSERS_BY_PCNTCHANGE:
                                valid = stock.getPercentChange() < 0;
                                break;
                            case Meta.TOP_GAINERS_BY_CHANGE:
                                valid = stock.getChange() > 0;
                                break;
                            case Meta.TOP_LOSERS_BY_CHANGE:
                                valid = stock.getChange() < 0;
                                break;
                            case Meta.HIGH_REACHED_52_WEEKS:
                                valid = stock.get52WeekHighLowTracker() == 1;
                                break;
                            case Meta.LOW_REACHED_52_WEEKS:
                                valid = stock.get52WeekHighLowTracker() == -1;
                                break;
                            default:
                                valid = false;
                        }
                        if (valid) {
                            TopstocksObject topstocksObject = new TopstocksObject(stock.getDecimalCount());
                            topstocksObject.setSymbol(stock.getSymbol());
                            topstocksObject.setSymbolCode(stock.getSymbolCode());
                            topstocksObject.setDescription(SymbolMaster.getCompanyName(stock.getSymbolForTopStocks(), stock.getExchange()));
                            topstocksObject.setLastTrade(stock.getLastTradeValue());
                            topstocksObject.setNetChange((float) stock.getChange());
                            topstocksObject.setPerChange((float) stock.getPercentChange());
                            topstocksObject.setVolume(stock.getVolume());
                            topstocksObject.setTrades(stock.getNoOfTrades());
                            topstocksObject.setMode(stock.getMinOrMax());
                            topstocksObject.setHigh_52_week(stock.getHighPriceOf52Weeks());
                            topstocksObject.setLow_52_week(stock.getLowPriceOf52Weeks());
                            setTopStocks(topstocksObject);
                            index = index + 1;
                            topstocksObject = null;
                        }
                    }
                } else {
                    if (!isInstrumentFilterEnabled) {
                        if ((SharedMethods.getSymbolType(stock.getInstrumentType()) != Meta.SYMBOL_TYPE_INDEX)) {
                            switch (requestType) {
                                case Meta.MOST_ACTIVE_BY_VOLUME:
                                    valid = stock.getVolume() > 0;
                                    break;
                                case Meta.MOST_ACTIVE_BY_TRADES:
                                    valid = stock.getNoOfTrades() > 0;
                                    break;
                                case Meta.TOP_GAINERS_BY_PCNTCHANGE:
                                    valid = stock.getPercentChange() > 0;
                                    break;
                                case Meta.TOP_LOSERS_BY_PCNTCHANGE:
                                    valid = stock.getPercentChange() < 0;
                                    break;
                                case Meta.TOP_GAINERS_BY_CHANGE:
                                    valid = stock.getChange() > 0;
                                    break;
                                case Meta.TOP_LOSERS_BY_CHANGE:
                                    valid = stock.getChange() < 0;
                                    break;
                                case Meta.HIGH_REACHED_52_WEEKS:
                                    valid = stock.get52WeekHighLowTracker() == 1;
                                    break;
                                case Meta.LOW_REACHED_52_WEEKS:
                                    valid = stock.get52WeekHighLowTracker() == -1;
                                    break;
                                default:
                                    valid = false;
                            }
                            if (valid) {
                                TopstocksObject topstocksObject = new TopstocksObject(stock.getDecimalCount());
                                topstocksObject.setSymbol(stock.getSymbol());
                                topstocksObject.setSymbolCode(stock.getSymbolCode());
                                topstocksObject.setDescription(SymbolMaster.getCompanyName(stock.getSymbolForTopStocks(), stock.getExchange()));
                                topstocksObject.setLastTrade(stock.getLastTradeValue());
                                topstocksObject.setNetChange((float) stock.getChange());
                                topstocksObject.setPerChange((float) stock.getPercentChange());
                                topstocksObject.setVolume(stock.getVolume());
                                topstocksObject.setTrades(stock.getNoOfTrades());
                                topstocksObject.setMode(stock.getMinOrMax());
                                topstocksObject.setHigh_52_week(stock.getHighPriceOf52Weeks());
                                topstocksObject.setLow_52_week(stock.getLowPriceOf52Weeks());
                                setTopStocks(topstocksObject);
                                index = index + 1;
                                topstocksObject = null;
                            }
                        }
                    } else if ((instrumentType == -1) || (SharedMethods.getSymbolType(stock.getInstrumentType()) == instrumentType)) {
//                    if ((stock.getInstrumentType() == Meta.QUOTE)||(stock.getInstrumentType() == Meta.OPTIONS)||(stock.getInstrumentType() == Meta.FUTURES)) {
                        switch (requestType) {
                            case Meta.MOST_ACTIVE_BY_VOLUME:
                                valid = stock.getVolume() > 0;
                                break;
                            case Meta.MOST_ACTIVE_BY_TRADES:
                                valid = stock.getNoOfTrades() > 0;
                                break;
                            case Meta.TOP_GAINERS_BY_PCNTCHANGE:
                                valid = stock.getPercentChange() > 0;
                                break;
                            case Meta.TOP_LOSERS_BY_PCNTCHANGE:
                                valid = stock.getPercentChange() < 0;
                                break;
                            case Meta.TOP_GAINERS_BY_CHANGE:
                                valid = stock.getChange() > 0;
                                break;
                            case Meta.TOP_LOSERS_BY_CHANGE:
                                valid = stock.getChange() < 0;
                                break;
                            case Meta.HIGH_REACHED_52_WEEKS:
                                valid = stock.get52WeekHighLowTracker() == 1;
                                break;
                            case Meta.LOW_REACHED_52_WEEKS:
                                valid = stock.get52WeekHighLowTracker() == -1;
                                break;
                            default:
                                valid = false;
                        }
                        if (valid) {
                            TopstocksObject topstocksObject = new TopstocksObject(stock.getDecimalCount());
                            topstocksObject.setSymbol(stock.getSymbol());
                            topstocksObject.setSymbolCode(stock.getSymbolCode());
                            topstocksObject.setDescription(SymbolMaster.getCompanyName(stock.getSymbolForTopStocks(), stock.getExchange()));
                            topstocksObject.setLastTrade(stock.getLastTradeValue());
                            topstocksObject.setNetChange((float) stock.getChange());
                            topstocksObject.setPerChange((float) stock.getPercentChange());
                            topstocksObject.setVolume(stock.getVolume());
                            topstocksObject.setTrades(stock.getNoOfTrades());
                            topstocksObject.setMode(stock.getMinOrMax());
                            topstocksObject.setHigh_52_week(stock.getHighPriceOf52Weeks());
                            topstocksObject.setLow_52_week(stock.getLowPriceOf52Weeks());
                            setTopStocks(topstocksObject);
                            index = index + 1;
                            topstocksObject = null;
                        }
                    }
                }
                stock = null;
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void setExchange(String exchange, boolean isDefault) {
        this.exchange = exchange;
        this.isDefaultExchange = isDefault;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    private void sleepMe(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
        }
    }

    public void setRecords(String data) {
        dataReceived = true;
        String[] records = data.split(Meta.FD);
        System.out.println("topstock===" + data);
        TopStocksStore.getSharedInstance().clear();
        int decimalCount = ExchangeStore.getSharedInstance().getExchange(exchange).getPriceDecimalPlaces();
        if (records.length > 4) {
            for (int i = 4; i < records.length; i++) {
                try {
                    String[] fields = records[i].split(Meta.ID);
                    TopstocksObject topstocksObject = new TopstocksObject(decimalCount);
                    topstocksObject.setData(fields);
                    //topstocksObject.setDescription(DataStore.getSharedInstance().getShortDescription(exchange + Constants.KEY_SEPERATOR_CHARACTER + fields[0]));
                    setTopStocks(topstocksObject);
                    fields = null;
                    topstocksObject = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        records = null;
    }

    public void exitFromLoop() {
        continueLoop = false;
    }

    // Comparator methods
    public int compare(Object o1, Object o2) {
        Stock s1 = DataStore.getSharedInstance().getStockObject((String) o1);
        Stock s2 = DataStore.getSharedInstance().getStockObject((String) o2);

        switch (requestType) {
            case Meta.MOST_ACTIVE_BY_VOLUME:
                if (s2.getVolume() == s1.getVolume()) {
                    return s1.getShortDescription().compareTo(s2.getShortDescription());
                } else {
                    return (int) (s2.getVolume() - s1.getVolume());
                }
            case Meta.MOST_ACTIVE_BY_TRADES:
                if (s2.getNoOfTrades() == s1.getNoOfTrades()) {
                    return s1.getShortDescription().compareTo(s2.getShortDescription());
                } else {
                    return (s2.getNoOfTrades() - s1.getNoOfTrades());
                }
            case Meta.TOP_GAINERS_BY_PCNTCHANGE:
            case Meta.HIGH_REACHED_52_WEEKS:
            case Meta.LOW_REACHED_52_WEEKS:
                if (s2.getPercentChange() == s1.getPercentChange()) {
                    return s1.getShortDescription().compareTo(s2.getShortDescription());
                } else {
                    if (s2.getPercentChange() > s1.getPercentChange()) {
                        return 1;
                    } else if (s2.getPercentChange() < s1.getPercentChange()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            case Meta.TOP_LOSERS_BY_PCNTCHANGE:
                if (s2.getPercentChange() == s1.getPercentChange()) {
                    return s1.getShortDescription().compareTo(s2.getShortDescription());
                } else {
                    if (s2.getPercentChange() < s1.getPercentChange()) {
                        return 1;
                    } else if (s2.getPercentChange() > s1.getPercentChange()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            case Meta.TOP_GAINERS_BY_CHANGE:
                if (s2.getChange() == s1.getChange()) {
                    return s1.getShortDescription().compareTo(s2.getShortDescription());
                } else {
                    if (s2.getChange() > s1.getChange()) {
                        return 1;
                    } else if (s2.getChange() < s1.getChange()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            case Meta.TOP_LOSERS_BY_CHANGE:
                if (s2.getChange() == s1.getChange()) {
                    return s1.getShortDescription().compareTo(s2.getShortDescription());
                } else {
                    if (s2.getChange() < s1.getChange()) {
                        return 1;
                    } else if (s2.getChange() > s1.getChange()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            default:
                return 0;
        }
    }
}