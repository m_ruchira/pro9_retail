package com.isi.csvr.topstocks;

import com.isi.csvr.shared.DynamicArray;

import javax.swing.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class ExchangeComboModel extends DefaultComboBoxModel {

    DynamicArray array = null;

    public ExchangeComboModel() {
        array = new DynamicArray();
//		array.setComparator(new ExpirationDateComparator());
    }

    public boolean addItem(Object item) {
        if (!array.contains(item)) {
            array.insert(item);
            return true;
        }
        return false;
    }

    public void clear() {
        array.clear();
    }

    public Object getElementAt(int index) {
        return (array.get(index)); //.getCaption();
    }

    public int getSize() {
        return array.size();
    }

    public String getItemAt(int index) {
        return (String) array.get(index);
    }
}