package com.isi.csvr.topstocks;

import com.isi.csvr.*;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TimeZone;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TopStocksWindow implements InternalFrameListener, ItemListener, MouseListener,
        ActionListener, MenuListener, ExchangeListener, TWDesktopInterface, ApplicationListener {

    private static TopStocksWindow self = null;
    TWMenu mnuDecimalPlaces;
    TWRadioButtonMenuItem mnuNoDecimal = new TWRadioButtonMenuItem();
    TWRadioButtonMenuItem mnuDefaultDecimal = new TWRadioButtonMenuItem();
    TWRadioButtonMenuItem mnuOneDecimal = new TWRadioButtonMenuItem();
    TWRadioButtonMenuItem mnuTwoDecimal = new TWRadioButtonMenuItem();
    TWRadioButtonMenuItem mnuThrDecimal = new TWRadioButtonMenuItem();
    TWRadioButtonMenuItem mnuFouDecimal = new TWRadioButtonMenuItem();
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private int FRAME_WIDTH = 630;
    private int FRAME_HEIGHT = 300;
    private Table table;
    private int selectedTab;
    private boolean justLoaded = true;
    //    private TWButton btnRefresh;
    private JPanel exchangePanel;
    private TopStocksStore dataUpdator;
    private Thread dataUpdatorThread;
    private TWComboBox excgCombo;
    private ArrayList<TWComboItem> exchangeList;
    private ArrayList<TopStockInstrumentType> topStockInstruments;
    private TWComboBox typeCombo;
    private TWComboBox marketCombo;
    private TWComboBox instrumentCombo;
    private String exchange;
    private int instrumentType = 0;
    private String market;
    private int topStockType;
    private ViewSetting viewSetting;
    private InternalFrame frame;
    private byte decimalPlaces = Constants.ONE_DECIMAL_PLACES;

    private TopStocksWindow() {

        topStockInstruments = new ArrayList<TopStockInstrumentType>();
        dataUpdator = TopStocksStore.getSharedInstance();
        createLayout();

        dataUpdator.setParent(this);
        dataUpdatorThread = new Thread(dataUpdator, "TW Top Stocks");
        dataUpdatorThread.setPriority(Thread.NORM_PRIORITY - 2);
        dataUpdatorThread.start();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Application.getInstance().addApplicationListener(this);
    }

    public static synchronized TopStocksWindow getSharedInstance() {
        if (self == null) {
            self = new TopStocksWindow();
//            throw new RuntimeException("TopStocks window not created");
        }
        return self;
    }

    /*public static void init() {
        self = new TopStocksWindow();
    }*/

    public void createLayout() {
        viewSetting = ViewSettingsManager.getSummaryView("TOP_STOCKS");
        createDataPanels();

        frame = new InternalFrame(table);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setPrintable(true);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setRequestFocusEnabled(false);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.addInternalFrameListener(this);
        frame.setTitle(viewSetting.getCaption());

        viewSetting.setParent(frame);

        exchangePanel = new JPanel(); //new FlowLayout(FlowLayout.LEADING, 8, 2));
        exchangePanel.setLayout(new FlexGridLayout(new String[]{"0", "0", "0", "0"}, new String[]{"25"}, 5, 5));

        exchangeList = new ArrayList<TWComboItem>();
        excgCombo = new TWComboBox(new TWComboModel(exchangeList));
        excgCombo.setPreferredSize(new Dimension(150, 25));


        marketCombo = new TWComboBox();
        marketCombo.setPreferredSize(new Dimension(140, 25));
        marketCombo.addItemListener(this);
        marketCombo.setVisible(true);

        instrumentCombo = new TWComboBox();
        instrumentCombo.setPreferredSize(new Dimension(90, 25));
        instrumentCombo.addItemListener(this);
        instrumentCombo.setVisible(true);

        typeCombo = new TWComboBox();
        typeCombo.addItemListener(this);
//        setTypeCombo(((TWComboItem)excgCombo.getItemAt(0)).getId());

        exchangePanel.add(excgCombo);
        exchangePanel.add(marketCombo);
        exchangePanel.add(instrumentCombo);
        exchangePanel.add(typeCombo);
        GUISettings.applyOrientation(exchangePanel);
//        setMarketCombo(null);
//        setInstrumentCombo(null);
        exchangesAdded(false);
        setInitialParams();

        JLabel messageLabel = new JLabel(Language.getString("TOP_STOCKS_DELAY_MESG"));
        messageLabel.setHorizontalAlignment(JLabel.CENTER);
        messageLabel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED, UIManager.getColor("controlLtHighlight"), UIManager.getColor("controlShadow")));

        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setViewSetting(viewSetting);
        Client.getInstance().getDesktop().add(frame);

        frame.getContentPane().add(exchangePanel, BorderLayout.NORTH);
        frame.getContentPane().add(table, BorderLayout.CENTER);

        createDecimalMenu(table.getPopup());
        try {
            decimalPlaces = Byte.parseByte(viewSetting.getProperty(ViewConstants.VC_DECIMAL_PLACES));
        } catch (Exception e) {
            decimalPlaces = Constants.TWO_DECIMAL_PLACES;
        }
        setDecimalPlaces(decimalPlaces);

        frame.setSize(viewSetting.getSize());
        frame.setLocation(viewSetting.getLocation());
        frame.applySettings();
        GUISettings.applyOrientation(frame);
        applyTheme();
    }

    private void setTypeCombo(String exg) {
        typeCombo.removeItemListener(this);
        typeCombo.removeAllItems();
        typeCombo.setLightWeightPopupEnabled(false);
        int[] criteriaTypes = {Meta.MOST_ACTIVE_BY_VOLUME, Meta.MOST_ACTIVE_BY_TRADES, Meta.TOP_GAINERS_BY_PCNTCHANGE, Meta.TOP_LOSERS_BY_PCNTCHANGE
                , Meta.TOP_GAINERS_BY_CHANGE, Meta.TOP_LOSERS_BY_CHANGE, Meta.HIGH_REACHED_52_WEEKS, Meta.LOW_REACHED_52_WEEKS};
        for (int i = 0; i < criteriaTypes.length; i++) {
            if (ExchangeStore.getSharedInstance().isTopstockAvailable(exg)) {
                if (ExchangeStore.getSharedInstance().isTopStockCriteriaTypeAvailable(exg, "" + criteriaTypes[i])) {
                    if (Meta.MOST_ACTIVE_BY_VOLUME == criteriaTypes[i]) {
                        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_MOST_ACTIVE_BY_VOLUME"), Meta.MOST_ACTIVE_BY_VOLUME));

                    } else if (Meta.MOST_ACTIVE_BY_TRADES == criteriaTypes[i]) {
                        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_MOST_ACTIVE_BY_TRADES"), Meta.MOST_ACTIVE_BY_TRADES));

                    } else if (Meta.TOP_GAINERS_BY_PCNTCHANGE == criteriaTypes[i]) {
                        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_TOP_GAINERS"), Meta.TOP_GAINERS_BY_PCNTCHANGE));

                    } else if (Meta.TOP_LOSERS_BY_PCNTCHANGE == criteriaTypes[i]) {
                        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_TOP_LOSERS"), Meta.TOP_LOSERS_BY_PCNTCHANGE));

                    } else if (Meta.TOP_GAINERS_BY_CHANGE == criteriaTypes[i]) {
                        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_TOP_GAINERS_BY_NETCHG"), Meta.TOP_GAINERS_BY_CHANGE));

                    } else if (Meta.TOP_LOSERS_BY_CHANGE == criteriaTypes[i]) {
                        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_TOP_LOSERS_BY_NETCHG"), Meta.TOP_LOSERS_BY_CHANGE));

                    } else if (Meta.HIGH_REACHED_52_WEEKS == criteriaTypes[i]) {
                        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("HIGH_REACHED_52HIGH"), Meta.HIGH_REACHED_52_WEEKS));

                    } else if (Meta.LOW_REACHED_52_WEEKS == criteriaTypes[i]) {
                        typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("LOW_REACHED_52LOW"), Meta.LOW_REACHED_52_WEEKS));

                    }

                }
            }
        }
//                 typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_MOST_ACTIVE_BY_VOLUME"), Meta.MOST_ACTIVE_BY_VOLUME));
//                 typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_MOST_ACTIVE_BY_TRADES"), Meta.MOST_ACTIVE_BY_TRADES));
//                 typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_TOP_GAINERS"), Meta.TOP_GAINERS_BY_PCNTCHANGE));
//                 typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_TOP_LOSERS"), Meta.TOP_LOSERS_BY_PCNTCHANGE));
//                 typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_TOP_GAINERS_BY_NETCHG"), Meta.TOP_GAINERS_BY_CHANGE));
//                 typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("MVIEW_TOP_LOSERS_BY_NETCHG"), Meta.TOP_LOSERS_BY_CHANGE));
//                 typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("HIGH_REACHED_52HIGH"), Meta.HIGH_REACHED_52_WEEKS));
//                 typeCombo.addItem(new TopStocksTypeComboItem(Language.getString("LOW_REACHED_52LOW"), Meta.LOW_REACHED_52_WEEKS));

        typeCombo.setPreferredSize(new Dimension(140, 25));
        try {
//            typeCombo.setSelectedIndex(0);
            TopStocksTypeComboItem item = (TopStocksTypeComboItem) typeCombo.getSelectedItem();
            topStockType = item.getType();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        typeCombo.addItemListener(this);
    }

    private void createDataPanels() {
        createTable();
        table.getModel().setViewSettings(ViewSettingsManager.getSummaryView("TOP_STOCKS"));
        applyTableSettings(table);
        /*gainersToMainboard = new TWMenu(Language.getString("POPUP_ADD_TO_MAINBOARD"), "addtomainboard.gif");
        table.getPopup().setMenu(gainersToMainboard);
        gainersToMainboard.addMenuListener(this);*/
    }

    private void applyTableSettings(Table oTable) {
        try {
            oTable.setBorder(null);
            oTable.setRequestFocusEnabled(false);
            oTable.getModel().applySettings();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void applyTheme() {
        try {
            SwingUtilities.updateComponentTreeUI(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void switchPanels() {
        if (selectedTab < 3) {
            selectedTab++;
        } else {
            selectedTab = 0;
        }
    }

    private void showStartPanel() {
        TopStocksTypeComboItem item = (TopStocksTypeComboItem) typeCombo.getSelectedItem();
        dataUpdator.setRequestType(item.getType());
        item = null;
    }

    public void setRecords(String data) {
        dataUpdator.setRecords(data);
    }

    public String getSelectedExchange() {
        try {
            return ((TWComboItem) excgCombo.getSelectedItem()).getId();
        } catch (Exception ex) {
            return null;
        }
    }

    public String getSelectedMarket() {
        try {
            return ((TWComboItem) marketCombo.getSelectedItem()).getId();
        } catch (Exception ex) {
            return null;
        }
    }

    public String getSelectedInstrument() {
        try {
            return ((TWComboItem) instrumentCombo.getSelectedItem()).getId();
        } catch (Exception ex) {
            return "0";
        }
    }

    public boolean isVisible() {
        return frame.isVisible();
    }

    public void setVisible(boolean status) {
        frame.setVisible(status);
    }

    public boolean isSelected() {
        return frame.isSelected();
    }

    public void setSelected(boolean status) throws Exception {
        frame.setSelected(status);
    }

    public boolean isIcon() {
        return frame.isIcon();
    }

    public void setIcon(boolean status) throws Exception {
        frame.setIcon(status);
    }

    /**
     * Invoked when a internal frame has been opened.
     *
     * @see javax.swing.JInternalFrame#show
     */
    public void internalFrameOpened(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is in the process of being closed.
     * The close operation can be overridden at this point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {
        if (justLoaded) {
            showStartPanel();
            justLoaded = false;
        }
    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {

    }

    private void createTable() {
        try {
            Table oTable = new Table();
            oTable.setHeaderPopupActive(true);
            TopStocksModel model = new TopStocksModel();
            model.setViewSettings(viewSetting);
            oTable.setModel(model);
            model.setTable(oTable);
            oTable.getTable().setDefaultRenderer(Object.class, new TopStocksRenderer(model.getRendIDs()));
            oTable.getTable().setDefaultRenderer(Number.class, new TopStocksRenderer(model.getRendIDs()));
            oTable.getTable().setDefaultRenderer(Boolean.class, new TopStocksRenderer(model.getRendIDs()));
            oTable.getTable().addMouseListener(this);
            if (oTable != null) {
                if (viewSetting.getID().equals("TOP_STOCKS")) {
                    model.setDataStore(TopStocksStore.getSharedInstance().getTopStocks());
                    table = oTable;
                }
            }

            oTable = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int intValue(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void closeWindow() {
        if (frame.getDefaultCloseOperation() == JFrame.HIDE_ON_CLOSE) {
            frame.setVisible(false);
        } else {
            frame.dispose();
        }
    }

    public void invalidateWindow() {
        dataUpdator.exitFromLoop();
    }

    public int getDesktopItemType() {
        return POPUP_TYPE;
    }

    public boolean isWindowVisible() {
        if (frame.isDetached()) {
            return ((JFrame) frame.getDetachedFrame()).isVisible();

        } else {
            return frame.isVisible();
        }

    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == excgCombo) {
            setInstrumentCombo(exchange);
            if (e.getStateChange() == ItemEvent.SELECTED) {
                exchange = getSelectedExchange();
                setMarketCombo(exchange);
                setTypeCombo(exchange);
                setInstrumentCombo(exchange);
                setSymbols(DataStore.getSharedInstance().getTopStocksIndex(exchange));
                if (market != null) {
                    viewSetting.putProperty(ViewConstants.VC_TOP_STOCK_EXCHANGE, (exchange + Constants.MARKET_SEPERATOR_CHARACTER + market));
                } else {
                    viewSetting.putProperty(ViewConstants.VC_TOP_STOCK_EXCHANGE, exchange);
                }
                sendRequest();

            }
        } else if (e.getSource() == typeCombo) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
//                TopStocksStore.getSharedInstance().clear();
                TopStocksTypeComboItem item = (TopStocksTypeComboItem) typeCombo.getSelectedItem();
                topStockType = item.getType();
                viewSetting.putProperty(ViewConstants.VC_TOP_STOCKS_TYPE, item.getType());
                item = null;
                sendRequest();
            }
        } else if (e.getSource() == marketCombo) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
//                TopStocksStore.getSharedInstance().clear();
                market = getSelectedMarket();
                setInstrumentCombo(exchange);
//                setSymbols(DataStore.getSharedInstance().getTopStocksIndex(exchange));
//                dataUpdator.setExchange(exchange, ExchangeStore.getSharedInstance().isDefault(exchange));
//                dataUpdator.setMarket(market);
//                dataUpdator.setInstrumentType(instrumentType);
                if (market != null) {
                    viewSetting.putProperty(ViewConstants.VC_TOP_STOCK_EXCHANGE, (exchange + Constants.MARKET_SEPERATOR_CHARACTER + market));
                } else {
                    viewSetting.putProperty(ViewConstants.VC_TOP_STOCK_EXCHANGE, exchange);
                }
//                dataUpdatorThread.interrupt();
//                if (!ExchangeStore.getSharedInstance().isDefault(exchange)) {
//                    dataUpdator.requestTopStocksData();
//                }
                sendRequest();
            }
        } else if (e.getSource() == instrumentCombo) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
//                TopStocksStore.getSharedInstance().clear();
//                setSymbols(DataStore.getSharedInstance().getTopStocksIndex(exchange));
                instrumentType = Byte.parseByte(getSelectedInstrument());
//                dataUpdator.setExchange(exchange, ExchangeStore.getSharedInstance().isDefault(exchange));
//                dataUpdator.setMarket(market);
//                dataUpdator.setInstrumentType(instrumentType);
                viewSetting.putProperty(ViewConstants.VC_TOP_STOCK_INSTRUMENT, instrumentType);
//                dataUpdatorThread.interrupt();
//                if (!ExchangeStore.getSharedInstance().isDefault(exchange)) {
//                    dataUpdator.requestTopStocksData();
//                }
                sendRequest();
            }
        }
    }

    public void sendRequest() {
        try {
            TopStocksStore.getSharedInstance().clear();
            dataUpdator.setRequestType(topStockType);
            dataUpdator.setExchange(exchange, ExchangeStore.getSharedInstance().isDefault(exchange));
            dataUpdator.setMarket(market);
            dataUpdator.setInstrumentType(instrumentType);
            dataUpdator.setInstrumentFilterEnabled(instrumentCombo.getItemCount() > 0);
            dataUpdatorThread.interrupt();

            if (!ExchangeStore.getSharedInstance().isDefault(exchange)) {
                dataUpdator.requestTopStocksData();
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void setInitialParams() {
        try {
            String exchangesID = viewSetting.getProperty(ViewConstants.VC_TOP_STOCK_EXCHANGE);
            String[] data = new String[1];
            String exchangeID = null;
            try {
                data = exchangesID.split(Constants.MARKET_SEPERATOR_CHARACTER);
                exchangeID = data[0];
            } catch (Exception e) {
                data[0] = exchangesID;
                exchangeID = exchangesID;
            }
            if ((exchangeID != null) && (ExchangeStore.getSharedInstance().isValidExchange(exchangeID))) {
                TopStocksStore.getSharedInstance().clear();
                exchange = exchangeID;
                setMarketCombo(exchange);
                String marketID = null;
                try {
                    marketID = data[1];
                } catch (Exception e) {
                    marketID = null;
                }
                if (marketID != null) {
                    try {
                        for (int i = 0; i < marketCombo.getItemCount(); i++) {
                            if (((TWComboItem) marketCombo.getItemAt(i)).getId().equals(marketID)) {
                                marketCombo.setSelectedIndex(i);
                                market = marketID;
                                break;
                            }
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();  //To change body of catch statement use Options | File Templates.
                    }
                }
                try {
                    setInstrumentCombo(exchange);
                    instrumentType = Integer.parseInt(viewSetting.getProperty(ViewConstants.VC_TOP_STOCK_INSTRUMENT));
                    if (instrumentType != -1) {
                        try {
                            for (int i = 0; i < instrumentCombo.getItemCount(); i++) {
                                if (((TWComboItem) instrumentCombo.getItemAt(i)).getId().equals(marketID)) {
                                    instrumentCombo.setSelectedIndex(i);
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            //e.printStackTrace();  //To change body of catch statement use Options | File Templates.
                        }
                    }
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
//                setTypeCombo();
                setSymbols(DataStore.getSharedInstance().getTopStocksIndex(exchange));
                sendRequest();
            }

            exchangeID = null;
//            marketID = null;
//            exchangesID = null;
//            data = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            for (int i = 0; i < excgCombo.getItemCount(); i++) {
                if (((TWComboItem) excgCombo.getItemAt(i)).getId().equals(exchange)) {
                    excgCombo.setSelectedIndex(i);
                    break;
                }
            }
//            excgCombo.updateUI();
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }

        try {
            int lattType = Integer.parseInt(viewSetting.getProperty(ViewConstants.VC_TOP_STOCKS_TYPE));
            for (int i = 0; i < typeCombo.getItemCount(); i++) {
                if (((TopStocksTypeComboItem) typeCombo.getItemAt(i)).getType() == lattType) {
//                    typeCombo.setSelectedItem(typeCombo.getItemAt(i));
                    typeCombo.setSelectedIndex(i);
                    break;
                }
            }
//            typeCombo.updateUI();
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }

//##############################################################################
//     MouseListener Interface Methods
//##############################################################################

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void showSummaryQuote(String symbol, int instrument) {
        if (ExchangeStore.getSharedInstance().isDefault(exchange))
            Client.getInstance().showDetailQuote(SharedMethods.getKey(exchange, symbol, instrument), true);
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof JTable) {
            if (e.getClickCount() > 1) {
                JTable table = (JTable) e.getSource();
                showSummaryQuote("" + table.getValueAt(table.getSelectedRow(), -1), Integer.parseInt("" + table.getValueAt(table.getSelectedRow(), -3)));
                table = null;
            }
        }
    }

    public void tableFontChanged(Font font) {
        table.getTable().setFont(font);
        table.getTable().repaint();
        viewSetting.setFont(font);
    }

    public void setSymbols(Symbols symbols) {
        dataUpdator.setSymbols(symbols);
    }

    public void headerFontChanged(Font font) {
    }

    public void applySettings() {
        frame.applySettings();
        table.getModel().applySettings();
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        int tableType = Integer.parseInt(command.substring(0, 1));
        command = command.substring(1);
        String symbol = null;
        int row;

        switch (tableType) {
            case 0:
                row = table.getTable().getSelectedRow();
                row = table.getTable().getSelectedRow();
                symbol = (String) table.getTable().getModel().getValueAt(row, 0);
                break;
        }
    }

    private void createWSPopUp(JMenu menu) {
        try {
            String wsName = null;
            JMenuItem wsItemMenu = null;
            int id = 0;

            /*if (menu == gainersToMainboard) {
                id = 0;
            }*/
            Object[] windowList = ((TWDesktop) Client.getInstance().getDesktop()).getWindows();
            if (windowList == null)
                return;
            ViewSetting oSettings = null;
            menu.removeAll();
            for (int i = 0; i < windowList.length; i++) {
                try {
                    JInternalFrame frame = (JInternalFrame) windowList[i];
                    if (!(frame instanceof ClientTable)) {
                        continue;
                    }
                    wsName = frame.getTitle();
                    oSettings = ((ClientTable) frame).getViewSettings();
                    wsItemMenu = new JMenuItem(wsName);
                    wsItemMenu.addActionListener(this);
                    wsItemMenu.setActionCommand(id + oSettings.getID());
                    menu.add(wsItemMenu);
                    frame = null;
                } catch (Exception ex) {
                }
            }
            windowList = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMarketCombo(String exchange) {
        String excg;
        Market[] marArray = null;
        if (exchange == null) {
            excg = getSelectedExchange();
        } else {
            excg = exchange;
        }
        if (excg != null) {
            marketCombo.removeItemListener(this);
            marketCombo.removeAllItems();
            if ((ExchangeStore.getSharedInstance().getExchange(excg).hasSubMarkets()) && ExchangeStore.getSharedInstance().getExchange(excg).isUserSubMarketBreakdown()) {
                marArray = ExchangeStore.getSharedInstance().getExchange(excg).getSubMarkets();
                Market defSubmaket = ExchangeStore.getSharedInstance().getExchange(excg).getDefaultMarket();
                TWComboItem defSubCmb = new TWComboItem(defSubmaket.getMarketID(), defSubmaket.getDescription());
                marketCombo.addItem(defSubCmb);
                for (int j = 0; j < marArray.length; j++) {
                    if (defSubmaket.getMarketID() != marArray[j].getMarketID()) {
                        TWComboItem comboItem = new TWComboItem(marArray[j].getMarketID(), marArray[j].getDescription());
                        marketCombo.addItem(comboItem);
                    }
                }
            }

            if ((marArray != null) && (marArray.length > 0)) {
                marketCombo.setSelectedIndex(0);
                marketCombo.setEnabled(true);
                marketCombo.setVisible(true);
                exchangePanel.removeAll();
                exchangePanel.setLayout(new FlexGridLayout(new String[]{"0", "0", "0"}, new String[]{"25"}, 5, 5));
                exchangePanel.add(excgCombo);
                exchangePanel.add(marketCombo);
                exchangePanel.add(typeCombo);
                exchangePanel.doLayout();
                marketCombo.updateUI();
                marketCombo.repaint();
                exchangePanel.repaint();
            } else {
                market = null;
                marketCombo.setEnabled(false);
                marketCombo.setVisible(false);
                exchangePanel.removeAll();
                exchangePanel.setLayout(new FlexGridLayout(new String[]{"0", "0"}, new String[]{"25"}, 5, 5));
                exchangePanel.add(excgCombo);
                exchangePanel.add(typeCombo);
                exchangePanel.doLayout();
                exchangePanel.repaint();

            }
            marArray = null;

        }
        excg = null;
        try {
            for (int i = 0; i < marketCombo.getItemCount(); i++) {
                if (((TWComboItem) marketCombo.getItemAt(i)).getId().equals(market)) {
                    marketCombo.setSelectedIndex(i);
                    break;
                }
            }
        } catch (Exception e) {
        }
        market = getSelectedMarket();
        marketCombo.addItemListener(this);
    }


    public void setInstrumentCombo(String exchange) {
        instrumentCombo.removeItemListener(this);
        instrumentCombo.removeAllItems();
        String excg;
        if (exchange == null)
            excg = getSelectedExchange();
        else
            excg = exchange;
        if (excg != null) {
            topStockInstruments.clear();
            topStockInstruments = ExchangeStore.getSharedInstance().getTopStockInstruments(excg);
            TopStockInstrumentType instrument = null;
            for (int j = 0; j < topStockInstruments.size(); j++) {
                instrument = topStockInstruments.get(j);
                TWComboItem comboItem = new TWComboItem(instrument.getInstrumentType(), instrument.getDescryption());
                if (market != null) {
                    if (market.equals(instrument.getMarket())) {
                        instrumentCombo.addItem(comboItem);
                    }
                } else {
                    if ("-1".equals(instrument.getMarket())) {
                        instrumentCombo.addItem(comboItem);
                    }
                }
            }
            if (instrumentCombo.getItemCount() > 0) {
                instrumentCombo.setSelectedIndex(0);
                instrumentCombo.setEnabled(true);
                instrumentCombo.setVisible(true);
                exchangePanel.removeAll();
                if (market != null) {
                    exchangePanel.setLayout(new FlexGridLayout(new String[]{"0", "0", "0", "0"}, new String[]{"25"}, 5, 5));
                    exchangePanel.add(excgCombo);
                    exchangePanel.add(marketCombo);
                    exchangePanel.add(instrumentCombo);
                    exchangePanel.add(typeCombo);
                } else {
                    exchangePanel.setLayout(new FlexGridLayout(new String[]{"0", "0", "0"}, new String[]{"25"}, 5, 5));
                    exchangePanel.add(excgCombo);
                    exchangePanel.add(instrumentCombo);
                    exchangePanel.add(typeCombo);
                }
                exchangePanel.doLayout();
                instrumentCombo.repaint();
                exchangePanel.repaint();
            } else {
                instrumentType = 0;
                instrumentCombo.setEnabled(false);
                instrumentCombo.setVisible(false);
                exchangePanel.removeAll();
                if (market != null) {
                    exchangePanel.setLayout(new FlexGridLayout(new String[]{"0", "0", "0"}, new String[]{"25"}, 5, 5));
                    exchangePanel.add(excgCombo);
                    exchangePanel.add(marketCombo);
                    exchangePanel.add(typeCombo);
                } else {
                    exchangePanel.setLayout(new FlexGridLayout(new String[]{"0", "0"}, new String[]{"25"}, 5, 5));
                    exchangePanel.add(excgCombo);
                    exchangePanel.add(typeCombo);
                }
                exchangePanel.doLayout();
                exchangePanel.repaint();
            }

        }
        excg = null;
        try {
            for (int i = 0; i < instrumentCombo.getItemCount(); i++) {
                if (((TWComboItem) instrumentCombo.getItemAt(i)).getId().equals(instrumentType)) {
                    instrumentCombo.setSelectedIndex(i);
                    break;
                }
            }
        } catch (Exception e) {
        }
        instrumentType = Byte.parseByte(getSelectedInstrument());
        instrumentCombo.addItemListener(this);
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        excgCombo.removeItemListener(this);
        exchangeList.clear();

        try {
            for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
                Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
                if (!exchange.isExpired() && !exchange.isInactive()) {
                    TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                    try {
                        if (ExchangeStore.getSharedInstance().isTopstockAvailable(exchange.getSymbol())) {
                            exchangeList.add(comboItem);
                            setTypeCombo(exchange.getSymbol());
                            setInitialParams();
                        }
                    } catch (Exception e) {
//                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                exchange = null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        Collections.sort(exchangeList);
        excgCombo.updateUI();
        if (excgCombo.getSelectedIndex() < 0 && (exchangeList.size() > 0)) {
            excgCombo.setSelectedIndex(0);
        }

        try {
            if (exchange == null) {
                exchange = getSelectedExchange();
            } else if (ExchangeStore.getSharedInstance().isValidExchange(exchange)) {
                try {
                    for (int i = 0; i < excgCombo.getItemCount(); i++) {
                        if (((TWComboItem) excgCombo.getItemAt(i)).getId().equals(exchange)) {
                            excgCombo.setSelectedIndex(i);
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use Options | File Templates.
                }
            } else {
                exchange = null;
                market = null;
                instrumentType = 0;
            }
            if (exchange == null) {
                try {
                    exchange = exchangeList.get(0).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            excgCombo.addItemListener(this);
            setMarketCombo(exchange);
            setInstrumentCombo(exchange);
//            setTypeCombo();
            setSymbols(DataStore.getSharedInstance().getTopStocksIndex(exchange));
            sendRequest();
        } catch (Exception e) {
//            excgCombo.addItemListener(this);
            //e.printStackTrace();
        }


    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public JTable getTable1() {
        return table.getTable();
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        createWSPopUp((JMenu) e.getSource());
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void menuSelected(MenuEvent e) {
        createWSPopUp((JMenu) e.getSource());
    }

    public void menuDeselected(MenuEvent e) {
    }

    public void menuCanceled(MenuEvent e) {
    }

    public String getTitle() {
        return frame.getTitle();
    }

    public int getWindowType() {
        return frame.getWindowType();
    }

    public void setWindowType(int type) {
        frame.setWindowType(type);
    }

    public void applicationExiting() {

    }

    public void applicationLoaded() {

    }

    public void applicationLoading(int percentage) {

    }

    public void applicationReadyForTransactions() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void workspaceLoaded() {
        setInitialParams();
        try {
            decimalPlaces = Byte.parseByte(viewSetting.getProperty(ViewConstants.VC_DECIMAL_PLACES));
        } catch (Exception e) {
            decimalPlaces = Constants.TWO_DECIMAL_PLACES;
        }
        setDecimalPlaces(decimalPlaces);
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void loadOfflineData() {
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    private void createDecimalMenu(JPopupMenu parent) {
        mnuDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        parent.add(mnuDecimalPlaces);

        //transHistoryTable.getPopup().add(mnuDecimalPlaces);
        ButtonGroup oGroup = new ButtonGroup();
        mnuDefaultDecimal = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDecimalPlaces.add(mnuDefaultDecimal);
        mnuDefaultDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuDefaultDecimal);

        mnuNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDecimalPlaces.add(mnuNoDecimal);
        mnuNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuNoDecimal);

        mnuOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDecimalPlaces.add(mnuOneDecimal);
        mnuOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuOneDecimal);
        mnuTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDecimalPlaces.add(mnuTwoDecimal);
        mnuTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuTwoDecimal);
        mnuThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDecimalPlaces.add(mnuThrDecimal);
        mnuThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuThrDecimal);
        mnuFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDecimalPlaces.add(mnuFouDecimal);
        mnuFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });

        oGroup.add(mnuFouDecimal);
        GUISettings.applyOrientation(mnuDecimalPlaces);
    }

    private void setDecimalPlaces(byte places) {
        ((SmartTable) table.getTable()).setDecimalPlaces(places);
        table.updateGUI();
        table.getTable().updateUI();
        viewSetting.putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        if (places == Constants.NO_DECIMAL_PLACES) {
            mnuNoDecimal.setSelected(true);
        } else if (places == Constants.ONE_DECIMAL_PLACES) {
            mnuOneDecimal.setSelected(true);
        } else if (places == Constants.TWO_DECIMAL_PLACES) {
            mnuTwoDecimal.setSelected(true);
        } else if (places == Constants.THREE_DECIMAL_PLACES) {
            mnuThrDecimal.setSelected(true);
        } else if (places == Constants.FOUR_DECIMAL_PLACES) {
            mnuFouDecimal.setSelected(true);
        } else if (places == Constants.UNASSIGNED_DECIMAL_PLACES) {
            mnuDefaultDecimal.setSelected(true);
        }
    }
}
