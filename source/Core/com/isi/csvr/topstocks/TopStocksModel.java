// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.topstocks;

/**
 * A Class class.
 * <P>
 * @author Bandula Priyadarshana
 */

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class TopStocksModel extends CommonTable implements TableModel, CommonTableInterface {

    private DynamicArray g_oList;
    private TopstocksObject stock;

    /**
     * Constructor
     */
    public TopStocksModel() {
    }

    protected void finalize() throws Throwable {
        g_oList = null;
    }

    public void setDataStore(DynamicArray oList) {
        g_oList = oList;
    }

    public void setSymbol(String symbol) {

    }

    /* --- Table Model's methods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if (g_oList != null)
            return g_oList.size();
        else
            return 0;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            stock = null;
            stock = (TopstocksObject) g_oList.get(iRow);

            switch (iCol) {
                case -4:
                    return stock.getDecimalCount();
                case -3:
                    return stock.getInstrument();
                case -2:
                    return stock.getMode();
                case -1:
                    return stock.getSymbolCode();
                case 0:
                    return stock.getSymbol();
                case 1:
                    return stock.getDescription();
                case 2:
                    return "" + stock.getLastTrade();
                case 3:
                    return "" + stock.getNetChange();
                case 4:
                    return "" + stock.getPerChange();
                case 5:
                    return "" + stock.getVolume();
                case 6:
                    return "" + stock.getTrades();
                case 7:
                    return "" + stock.getHigh_52_week();
                case 8:
                    return "" + stock.getLow_52_week();
                default:
                    return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        try {
            return getValueAt(0, iCol).getClass();
        } catch (Exception ex) {
            return Object.class;
            //return Number.class;
        }
        //return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */
}
