package com.isi.csvr;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class TWFileFilter extends FileFilter {
    String description;
    String[] extensions;

    /*public ExtensionFileFilter(String description, String extension)
    {
		String[] array = new String[0];
		array[0] = extension;
        this(description, array);
    }*/

    public TWFileFilter(String description, String[] extensions) {
        if (description == null) {
            // Since no description, use first extension and # of extensions as description
            this.description = extensions[0] + "{" + extensions.length + "}";
        } else {
            this.description = description;
        }
        this.extensions = extensions;
        // Convert array to lowercase
        // Don't alter original entries
        toLower(this.extensions);
    }

    public String[] getExtension() {
        return extensions;
    }

    private void toLower(String array[]) {
        for (int i = 0, n = array.length; i < n; i++) {
            array[i] = array[i].toLowerCase();
        }
    }

    public String getDescription() {
        return description;
    }

    // ignore case, always accept directories
    // character before extension must be a period
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        } else {
            String path = file.getAbsolutePath().toLowerCase();
            for (int i = 0, n = extensions.length; i < n; i++) {
                String extension = extensions[i];
                if ((path.endsWith(extension) &&
                        (path.charAt(path.length() - extension.length() - 1)) == '.')) {
                    return true;
                }
            }
        }
        return false;
    }
}
