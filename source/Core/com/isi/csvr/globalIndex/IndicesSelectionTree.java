package com.isi.csvr.globalIndex;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.util.Decompress;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 25-Mar-2008 Time: 10:12:40 To change this template use File | Settings
 * | File Templates.
 */
public class IndicesSelectionTree extends InternalFrame implements ActionListener {
    public static IndicesSelectionTree self;
    public static Vector newlyAdded = new Vector();
    private static Hashtable<String, GlobalIndex> globalIndexStore;
    private static Hashtable<String, CheckBoxNode> mainStore;
    private JPanel mainPanel;
    private JScrollPane scrollPane;
    private JPanel bottomPanel;
    private int width = 300;
    private int height = 400;
    private Vector rootVector;
    private Vector rootNode;
    private JTree tree;
    private TWButton addButton;
    private TWButton cancelButton;
    private CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();


    private IndicesSelectionTree() {
        globalIndexStore = new Hashtable<String, GlobalIndex>();
        mainStore = new Hashtable<String, CheckBoxNode>();
        rootNode = new Vector();
        loadGlobalIndicesFile();
        createUI();

    }

    public static IndicesSelectionTree getSharedInstance() {
        if (self == null) {
            self = new IndicesSelectionTree();
        }
        return self;
    }

    public static void addNewlyAddedIndex(String key) {
        newlyAdded.add(key);
    }

    private void createUI() {
        mainPanel = new JPanel();
        bottomPanel = new JPanel();
        addButton = new TWButton(Language.getString("ADD"));
        addButton.addActionListener(this);
        cancelButton = new TWButton(Language.getString("CANCEL"));
        cancelButton.addActionListener(this);
        this.setSize(width, height);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        this.add(mainPanel);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle(Language.getString("GLOBAL_INDEX"));
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "32"}, 3, 3));
        bottomPanel.setLayout(new FlexGridLayout(new String[]{"100%", "80", "5", "80"}, new String[]{"100%"}, 3, 3));
        bottomPanel.add(new JLabel());
        bottomPanel.add(addButton);
        bottomPanel.add(new JLabel());
        bottomPanel.add(cancelButton);
        GUISettings.applyOrientation(this);
        loadTree();

        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
        setLocationRelativeTo(Client.getInstance().getDesktop());

    }

    private void loadTree() {
        try {
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/giexchanges_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out;
            out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            do {
                record = in.readLine();
                if (record != null) {
                    try {
                        String[] fields = record.split(Meta.DS);
                        Hashtable<String, CheckBoxNode> indexhash = new Hashtable<String, CheckBoxNode>();
                        Enumeration indexEnum = globalIndexStore.elements();
                        while (indexEnum.hasMoreElements()) {
                            GlobalIndex gi = (GlobalIndex) indexEnum.nextElement();
                            if (gi.getExchange().equals(fields[0])) {
                                CheckBoxNode cbn = new CheckBoxNode(gi.getShortDescription(), SharedMethods.getKey(gi.getExchange(), gi.getSymbol(), Meta.INSTRUMENT_INDEX), false);
                                indexhash.put(SharedMethods.getKey(gi.getExchange() + "~" + gi.getSymbol()), cbn);
                                mainStore.put(SharedMethods.getKey(gi.getExchange() + "~" + gi.getSymbol()), cbn);
                            }
                        }
                        NamedVector tempvector = new NamedVector(fields[1], indexhash);
                        fields = null;
                        rootNode.add(tempvector);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            } while (record != null);
            out = null;
            decompress = null;
            in.close();
            in = null;
            rootVector = new NamedVector("Root", rootNode.toArray());
            tree = new JTree(rootVector);
            tree.putClientProperty("JTree.lineStyle", "Vertical");
            tree.setCellRenderer(renderer);
            tree.setToggleClickCount(1);
            tree.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {

                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent();
                    System.out.println((DefaultMutableTreeNode) tree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent());

                    //(DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                    if (node == null) return;
                    Object nodeInfo = node.getUserObject();
                    if (node.isLeaf()) {
                        CheckBoxNode cbn = (CheckBoxNode) nodeInfo;
                        if (cbn.isSelected()) {
                            cbn.setSelected(false);
                        } else {
                            cbn.setSelected(true);
                        }
                        tree.repaint();
                    } else {
                        //do nothing
                    }

                }
            });
            scrollPane = new JScrollPane(tree);
            mainPanel.add(scrollPane);
            mainPanel.add(bottomPanel);
            setInitialSelections();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadGlobalIndicesFile() {
        try {
            String sLangSpecFileName = Settings.SYSTEM_PATH + "/giindicies_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out;
            out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            do {
                record = in.readLine();
                if (record != null) {
                    try {
                        String[] fields = record.split(Meta.DS);
                        GlobalIndex globalIndex = new GlobalIndex();
                        globalIndex.setExchange(fields[0]);
                        globalIndex.setSymbol(fields[1]);
                        globalIndex.setShortDescription(UnicodeUtils.getNativeString(fields[2]));
                        globalIndexStore.put(SharedMethods.getKey(fields[0], fields[1], Meta.INSTRUMENT_INDEX), globalIndex);
                        fields = null;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            } while (record != null);
            out = null;
            decompress = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showFrame() {
        setInitialSelections();
        setVisible(true);
    }

    public void setSelectedIndices() {
//        resetFlags();
        Enumeration selectedIndices = mainStore.elements();
        while (selectedIndices.hasMoreElements()) {
            CheckBoxNode cbn = (CheckBoxNode) selectedIndices.nextElement();
            if (cbn.isSelected()) {
                GlobalIndexStore.getSharedInstance().addGlobalIndexToStore(cbn.key);
                setVisible(false);
            } else {
                GlobalIndexStore.getSharedInstance().removeGlobalIndexToStore(cbn.key);
            }
        }
    }

    private void setInitialSelections() {
        Enumeration selectedIndices = mainStore.elements();
        while (selectedIndices.hasMoreElements()) {
            CheckBoxNode cbn = (CheckBoxNode) selectedIndices.nextElement();
            cbn.setSelected(GlobalIndexStore.getSharedInstance().contains(cbn.getKey()));
        }
    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource().equals(addButton)) {

            setSelectedIndices();


        } else if (e.getSource().equals(cancelButton)) {
            setVisible(false);

        }
    }

//    public void resetFlags() {
//        Enumeration selectedIndices =GlobalIndexStore.getSharedInstance().getStore().elements();
//        while (selectedIndices.hasMoreElements()) {
//                GlobalIndexObject gio=(GlobalIndexObject)selectedIndices.nextElement();
//                gio.setFlag(false);
//        }
//    }
//
//    private void sendRemoveRequestsForUnticked() {
//        Enumeration selectedIndices =GlobalIndexStore.getSharedInstance().getStore().elements();
//        while (selectedIndices.hasMoreElements()) {
//            GlobalIndexObject gio=(GlobalIndexObject)selectedIndices.nextElement();
//            if(!gio.isFlaged()){
//               GlobalIndexStore.getSharedInstance().sendRermoveSymbolRequests(false,globalIndexStore.get(SharedMethods.getKey(gio.getExchange(),gio.getSymbol())));
//               GlobalIndexStore.getSharedInstance().getStore().remove(SharedMethods.getKey(gio.getExchange(),gio.getSymbol()));
//               GlobalIndexStore.getSharedInstance().getKeys().remove(SharedMethods.getKey(gio.getExchange(),gio.getSymbol()));
//            }
//        }
//    }

}
