package com.isi.csvr.globalIndex;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 25-Mar-2008 Time: 10:41:48 To change this template use File | Settings
 * | File Templates.
 */
class CheckBoxNode implements Comparable {
    String text;
    String key;
    boolean selected;

    public CheckBoxNode(String text, boolean selected) {
        this.text = text;
        this.selected = selected;
    }

    public CheckBoxNode(String text, String key, boolean selected) {
        this.text = text;
        this.key = key;
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean newValue) {
        selected = newValue;
    }

    public String getText() {
        return text;
    }

    public void setText(String newValue) {
        text = newValue;
    }

    public String toString() {
        return getClass().getName() + "[" + text + "/" + selected + "]";
    }

    public String getKey() {
        return key;
    }

    public int compareTo(Object o) {
        return getText().compareTo(((CheckBoxNode) o).getText());
    }
}
