package com.isi.csvr.globalIndex;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 25-Mar-2008 Time: 10:38:53 To change this template use File | Settings
 * | File Templates.
 */
public class CheckBoxNodeRenderer implements TreeCellRenderer, Themeable {
    private static ImageIcon g_oClosedIcon = null;
    private static ImageIcon g_oExpandedIcon = null;
    private static ImageIcon g_oLeafIcon_sub = null;
    private static ImageIcon g_oLeafIcon_unSub = null;
    Color selectionBorderColor, selectionForeground, selectionBackground, textForeground, textBackground;
    private CheckLabel leafRenderer = new CheckLabel();
    private CheckBoxNode lRenderer;
    private DefaultTreeCellRenderer nonLeafRenderer = new DefaultTreeCellRenderer();
    private DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
    private Color backgroundSelectionColor;
    private Color backgroundNonSelectionColor;

    public CheckBoxNodeRenderer() {
        Font fontValue;
        fontValue = UIManager.getFont("Tree.font");
        if (fontValue != null) {
            leafRenderer.setFont(fontValue);
        }
        Boolean booleanValue = (Boolean) UIManager
                .get("Tree.drawsFocusBorderAroundIcon");

        backgroundSelectionColor = defaultRenderer.getBackgroundSelectionColor();
        backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
        selectionBorderColor = UIManager.getColor("Tree.selectionBorderColor");
        selectionForeground = UIManager.getColor("Tree.selectionForeground");
        selectionBackground = UIManager.getColor("Tree.selectionBackground");
        textForeground = UIManager.getColor("Tree.textForeground");
        textBackground = UIManager.getColor("Tree.textBackground");
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/gitick.gif");
            g_oLeafIcon_unSub = new ImageIcon("images/theme" + Theme.getID() + "/giuntick.gif");
        } catch (Exception e) {
            System.out.println("Error");
        }
        reload();
        Theme.registerComponent(this);
    }

    public static void reload() {
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/gitick.gif");
            g_oLeafIcon_unSub = new ImageIcon("images/theme" + Theme.getID() + "/giuntick.gif");

        } catch (Exception e) {

        }
    }

    protected CheckLabel getLeafRenderer() {
        return leafRenderer;
    }

    protected CheckBoxNode getlfRenderer() {
        return lRenderer;
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean selected, boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        Component returnValue = null;

        if (((DefaultMutableTreeNode) value).getUserObject() instanceof NamedVector) {
            NamedVector nv = (NamedVector) ((DefaultMutableTreeNode) value).getUserObject();
            if (expanded) {
                nonLeafRenderer.setText(nv.toString());
                nonLeafRenderer.setIcon(g_oExpandedIcon);
                nonLeafRenderer.setForeground(Color.BLACK);

            } else {
                nonLeafRenderer.setText(nv.toString());
                nonLeafRenderer.setIcon(g_oClosedIcon);
                nonLeafRenderer.setForeground(Color.BLACK);

            }
            nonLeafRenderer.setEnabled(tree.isEnabled());
            returnValue = nonLeafRenderer;
        } else if (((DefaultMutableTreeNode) value).getUserObject() instanceof CheckBoxNode) {
            CheckBoxNode cbn = (CheckBoxNode) ((DefaultMutableTreeNode) value).getUserObject();
            lRenderer = cbn;
            if (cbn.isSelected()) {
                leafRenderer.setIcon(g_oLeafIcon_sub);
                leafRenderer.setText(cbn.getText());
                leafRenderer.setSelected(true);
            } else {
                leafRenderer.setIcon(g_oLeafIcon_unSub);
                leafRenderer.setText(cbn.getText());
                leafRenderer.setSelected(false);
            }
            if (selected) {
                leafRenderer.setBackground(backgroundSelectionColor);
//                leafRenderer.setBackground(Theme.getColor(""));
            } else {
                leafRenderer.setBackground(backgroundNonSelectionColor);
//                leafRenderer.setBackground(Theme.getColor(""));
            }
            leafRenderer.setEnabled(tree.isEnabled());
            returnValue = leafRenderer;
        } else {
            returnValue = nonLeafRenderer.getTreeCellRendererComponent(tree,
                    value, selected, expanded, leaf, row, hasFocus);
        }
        return returnValue;
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        leafRenderer.setBackground(Color.white);
        leafRenderer.setForeground(Color.BLACK);
    }
}
