package com.isi.csvr.globalIndex;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 20-Mar-2008 Time: 12:27:32 To change this template use File | Settings
 * | File Templates.
 */

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

public class GlobalIndexModel extends CommonTable implements TableModel, CommonTableInterface, ClipboardOwner, DDELinkInterface {
    private Clipboard clip;
    private String exchange;

    /**
     * Constructor
     */
    public GlobalIndexModel() {
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        init();
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void setSymbol(String symbol) {

    }


    public void exchageModified() {
    }

    private void init() {

    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        try {
            return GlobalIndexStore.getSharedInstance().size();
        } catch (Exception e) {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {

        try {
            Stock oGIndex = GlobalIndexStore.getSharedInstance().getObject(iRow);
            switch (iCol) {
                case -1:
                    return exchange;
                case 0:
                    return oGIndex.getSymbol();
                case 1:
                    return oGIndex.getShortDescription();
                case 2:
                    return "" + oGIndex.getLastTradeValue();
                case 3:
                    return "" + oGIndex.getChange();
                case 4:
                    return "" + oGIndex.getPercentChange();
                case 5:
                    return "" + oGIndex.getTurnover();
                case 6:
                    return "" + oGIndex.getVolume();
                case 7:
                    return "" + oGIndex.getTodaysOpen();
                case 8:
                    return "" + oGIndex.getHigh();
                case 9:
                    return "" + oGIndex.getLow();
                case 10:
                    return "" + oGIndex.getPreviousClosed();
                case 11:
                    return "" + oGIndex.getAvgTradePrice();
                default:
                    return "0";
            }
        } catch (Exception e) {
            return Constants.NULL_STRING;
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];

    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 11:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    public void getDDEString(Table table, boolean withHeadings) {

        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (withHeadings)
            buffer.append(copyHeaders(table));
        for (int r = row; r < (rows + row); r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    if (c == 1) {
                        buffer.append(getValueAt(r, 1));
                    } else {
                        buffer.append("=MRegionalDdeServer|'");
                        buffer.append("IN");
                        buffer.append(getValueAt(r, 0));
                        buffer.append("'!'");
                        buffer.append(modelIndex);
                        buffer.append("'");
                    }
                    if (c > 1)
                        buffer.append("*1");

                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        for (int c = 0; c < cols; c++) {
            int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }

    public String getDDEString(String id, int col) {
        /*for (int i = 0; i < symbols.length; i++) {
            if (symbols[i].equals(id)) {
                return (String) getValueAt(i, col);
            }
        }*/
        return "0";
    }
}

