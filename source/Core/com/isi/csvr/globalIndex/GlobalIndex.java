package com.isi.csvr.globalIndex;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 20-Mar-2008 Time: 14:05:11 To change this template use File | Settings
 * | File Templates.
 */
public class GlobalIndex {
    private String exchange;
    private String symbol;
    private String shortDescription;
    private String exchangeName;
    private boolean flag = false;
    private String longDescription;

    public GlobalIndex(String exchange, String symbol) {
        this.exchange = exchange;
        this.symbol = symbol;
    }

    public GlobalIndex() {

    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public boolean isFlaged() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String toString() {
        StringBuffer tempString = new StringBuffer();
        tempString.append(exchange);
        tempString.append(",");
        tempString.append(symbol);
        return tempString.toString();
    }
}
