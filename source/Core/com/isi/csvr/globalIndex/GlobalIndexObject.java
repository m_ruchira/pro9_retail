package com.isi.csvr.globalIndex;

import com.isi.csvr.shared.SharedMethods;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 08-Apr-2008 Time: 11:07:35 To change this template use File | Settings
 * | File Templates.
 */
public class GlobalIndexObject implements Serializable {
    boolean flag = false;
    private String exchange;
    private String symbol;
    private String shortDescription;
    private String exchangeName;
    //newly Added
    private double lastTradevalue;
    private double change;
    private double pctChange;
    private double turnover;
    private long volume;
    private double todaysOpen;
    private double high;
    private double low;
    private double previousClossed;
    private double avgTradePrice;
    private String longDescription;

    public GlobalIndexObject(String exchange, String symbol) {
        this.exchange = exchange;
        this.symbol = symbol;
    }

    public void setData(String[] data, int decimalFactor) {
        if (data == null)
            return;

        char tag;
        for (int i = 0; i < data.length; i++) {
            tag = data[i].charAt(0);
            switch (tag) {
                case 'D':
                    setVolume(SharedMethods.getLong(data[i].substring(1)));
                    break;
                case 'K':
                    setLastTradevalue(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case 'L':
                    setHigh(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case 'M':
                    setLow(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case 'N':
                    setChange(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case 'O':
                    setPctChange(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case 'P':
                    setPreviousClossed(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case 'R':
                    setTodaysOpen(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case 'S':
                    setAvgTradePrice(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case 'C':
                    setTurnover(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;

            }
        }
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public double getLastTradevalue() {
        return lastTradevalue;
    }

    public void setLastTradevalue(double lastTradevalue) {
        this.lastTradevalue = lastTradevalue;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public double getPctChange() {
        return pctChange;
    }

    public void setPctChange(double pctChange) {
        this.pctChange = pctChange;
    }

    public double getTurnover() {
        return turnover;
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public double getTodaysOpen() {
        return todaysOpen;
    }

    public void setTodaysOpen(double todaysOpen) {
        this.todaysOpen = todaysOpen;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getPreviousClossed() {
        return previousClossed;
    }

    public void setPreviousClossed(double previousClossed) {
        this.previousClossed = previousClossed;
    }

    public double getAvgTradePrice() {
        return avgTradePrice;
    }

    public void setAvgTradePrice(double avgTradePrice) {
        this.avgTradePrice = avgTradePrice;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public boolean isFlaged() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String toString() {
        StringBuffer tempString = new StringBuffer();
        tempString.append(exchange);
        tempString.append(",");
        tempString.append(symbol);
        return tempString.toString();
    }
}
