package com.isi.csvr.globalIndex;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 25-Mar-2008 Time: 22:23:16 To change this template use File | Settings
 * | File Templates.
 */
public class CheckLabel extends JLabel implements Themeable {
    private boolean isSelected = false;

    public CheckLabel() {
        Theme.registerComponent(this);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        this.setBackground(Color.white);
        this.setForeground(Color.BLACK);
    }
}
