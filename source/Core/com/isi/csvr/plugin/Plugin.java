package com.isi.csvr.plugin;

import com.isi.csvr.plugin.event.PluginEvent;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Feb 28, 2008
 * Time: 12:46:54 PM
 */
public interface Plugin {
    public void load(Object... params);

    public void showUI(Object... params);

    public void save(Object... params);

    public boolean isLoaded();

    public String getCaption();

    public void unload(Object... params);

    public void pluginEvent(PluginEvent event, Object... data);

    public boolean isDataNeeded();

    public void setData(int id, Object... params);

    public void applyWorkspace(String id, String value);

    public String getWorkspaceString(String id);

    public String getViewSettingID();

    public int getWindowTypeID();

    public int getWindowTypeCategory();
}
