// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

/**
 * Main class for the StockNet application
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.communication.udp.SatelliteIPSettings;
import com.isi.csvr.downloader.TWUpdateDownloader;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.security.TWSecurityManager;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

//import com.dfn.mtr.mix.util.ConfigLoader;

public class Mubasher {
    public static boolean DEBUG = true;
    public static boolean isVeryFistTime = false;
    public static ArrayList<String> folders;
    private static String ssoLang = "EN";
    private static String proDataHome = "";

    /**
     * Constructor
     */
    public Mubasher(String[] args, boolean updateFailed) {
        folders = new ArrayList<String>();
//        try {
//            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
//            for (ObjectName name : mbs.queryNames(null, null)) {
//                if (!name.getDomain().equals("JMImplementation"))
//                    mbs.unregisterMBean(name);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            if (args[0].equalsIgnoreCase("/Cache")) {
                OSCache.doCache();
                System.exit(0);
            }
        } catch (Exception e) {

        }

        checkForMetaStockUpdate();

        if (NativeMethods.tooManyInstances())
            System.exit(0);

        try {
            loadFolderNamesToCopy();
            copyFolderstoUserProfile();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Settings.setCurrentZone(TimeZone.getDefault());
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        Settings.init();
        TWControl.init();

        // check for single sign on mode { format - SSOID LANG INSTid Trading}
        try {
            //if (Math.abs(4.5 - args.length) == 0.5) {
            if ((args.length == 4) || (args.length == 5)) {
                String singleSignOnID = TWSecurityManager.getSingleSingOnID(args[0]);
                String tradeToken = null;
                if (args.length == 5) {
                    tradeToken = TWSecurityManager.getTradeToken(args[4]);
                }
                if (singleSignOnID != null) {
                    Settings.setSingleSignOnMode(true);
                    if (tradeToken != null) {
                        Settings.setTradeToken(tradeToken);
                    }
                    //Settings.setUserID(singleSignOnID);
                    Settings.setSingleSignOnID(singleSignOnID);
                    Settings.setInstitutionID(args[2]);
                    Settings.setSsoTradingEnabled(args[3].equalsIgnoreCase("1"));
                    Settings.setUserDetails(singleSignOnID, "*****");
                    TWControl.setWebSSOType();
                }
                ssoLang = args[1];
                System.out.println("Single signon mode ---------");

                if (args.length == 5 && args[4] != null) {
                    Settings.setSsoId(args[4]);
                }
            }

        } catch (Exception e) {
            Settings.setSingleSignOnMode(false);
            e.printStackTrace();
        }

        // check for custom ports
        /*if (args.length == 2) {
            try {
                value1 = SharedMethods.intValue(args[0]);
                value2 = SharedMethods.intValue(args[1]);

                Settings.SERVER_PORT = value1;
                Settings.DOWNLOAD_PORT = value2;
            } catch (Exception ex) {
            }
        }*/

        // end of main methods

        IPSettings i = new IPSettings(); // must load after tw settings
        SatelliteIPSettings.load();
        TWColumnSettings l = new TWColumnSettings();
        HistorySettings h = new HistorySettings();
        new

                Language();

        if (Settings.isSingleSignOnMode())

        {
            Language.setLanguage(ssoLang);
        } else

        {
            if ((Settings.getItem("LANGUAGE") == null) || Settings.getItem("LANGUAGE").equals("")) {
                showLanguageSelection();
            }
            Language.setLanguage(Settings.getItem("LANGUAGE"));
        }

        if (TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web) && (!Settings.isSingleSignOnMode()))

        { // SSO client must only run in SSO mode
            String message = Language.getString("SSO_INVOKE_ERROR_MSG");
            System.out.println(message);
            JOptionPane.showMessageDialog(null, message, Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }

        Settings.systemLocale = Locale.getDefault().

                getCountry()

                + " " + Locale.getDefault().

                getLanguage();

        Locale.setDefault(new

                        Locale("en", "US")

        );

        if (updateFailed)

        {
            Settings.setCouldNotApplyUpdate();
        }

        HelpManager.getSharedInsatance().

                loadHelpItems();

        new

                Client();
        /*SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Client();
            }
        });*/
    }

    private static void doFistTimeProcess() {

        // copy the uninstaller icon
        try {
            File iconDestination = new File("ARPIconPath.txt");
            if (iconDestination.exists()) {
                iconDestination.deleteOnExit();
                DataInputStream in = new DataInputStream(new FileInputStream(iconDestination));
                String clsID = in.readLine();
                NativeMethods.writeRegValue(NativeMethods.HKEY_LOCAL_MACHINE,
                        "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\InstallShield_" + clsID,
                        "DisplayIcon", System.getProperties().get("user.dir") + "\\TWR.ico");
                in = null;
                iconDestination = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void checkUpdateAvailable() {
        try {
//            File file = new File( "autoupdate/restart"); // if this file is available, there is an update available
            if (TWUpdateDownloader.isUpdateDownloaded()) { // must update
                if (TWUpdateDownloader.hasWriteAccess()) { // must have write access to the folder.
//                    file.deleteOnExit();
                    if (TWUpdateDownloader.isUpdateAuthentic()) // update is authentic
                    {
                        Thread t = new Thread("Client-addExitThread") {
                            public void run() {
                                try {
                                    Runtime.getRuntime().exec("start \"" + System.getProperties().get("user.dir") +
                                            "\\AutoUpdate\\AutoUpdate.exe\" \"" + System.getProperties().getProperty("user.dir") +
                                            "\" Pro.exe");
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        };

                        Runtime.getRuntime().addShutdownHook(t);
                        System.exit(0);
                    }
                    TWUpdateDownloader.finishUpdate();
                } else {
                    Settings.setCouldNotApplyUpdate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void copyFolderstoUserProfile() {

//        String userHome = System.getenv("ALLUSERSPROFILE") + "\\Application Data";
        String folderStruct = "";
        folderStruct = System.getenv("APPDATA");

        try {
            BufferedReader in = new BufferedReader(new FileReader("appdata.dat"));
            String str;
            while ((str = in.readLine()) != null) {
                folderStruct = folderStruct + str;
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            folderStruct = folderStruct + "/Mubasher/Mubasher Pro";
        }

        if (!folderStruct.isEmpty()) {
//        if (!folderStruct.isEmpty() && userHome != null && !userHome.isEmpty()) {

            proDataHome = folderStruct;
            proDataHome = proDataHome.trim();
            File file = new File(proDataHome);

            boolean homediretoryexsist = file.exists();
            if (!homediretoryexsist) {
                homediretoryexsist = file.mkdirs();
            }

            File tempfile = null;
            String name = null;
            String excludeList = null;
            if (homediretoryexsist) {
                for (int i = 0; i < folders.size(); i++) {
                    excludeList = null;
                    name = folders.get(i).split("\\|")[0];
                    if (!name.equalsIgnoreCase("workspaces")) {
                        try {
                            excludeList = folders.get(i).split("\\|")[1];
                            excludeList = excludeList.trim().toLowerCase();
                        } catch (Exception e) {
                            //                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        tempfile = new File(proDataHome + "\\" + name);
                        if (!tempfile.exists()) {
                            try {
                                copyFiles(name, proDataHome + "\\" + name);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                if ((excludeList != null) && (excludeList.length() > 0)) {
                                    copyNewFiles(name, proDataHome + "\\" + name, excludeList);
                                } else {
                                    copyNewFiles(name, proDataHome + "\\" + name);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        tempfile = new File(proDataHome + "\\workspaces");
                        if (!tempfile.exists()) {
                            String workspace = processWorkspaces();
                            try {
                                copyFiles(workspace, proDataHome + "\\workspaces");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                }
            }

            /*if (homediretoryexsist) {
                tempfile = new File(proDataHome + "\\history");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("history", proDataHome + "\\history");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        copyNewFiles("history", proDataHome + "\\history");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                tempfile = new File(proDataHome + "\\workspaces");
                if (!tempfile.exists()) {
                    String workspace = processWorkspaces();
                    try {
                        copyFiles(workspace, proDataHome + "\\workspaces");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }*//*else {
                    String workspace = processWorkspaces();
                    try {
                        copyNewFiles(workspace, proDataHome + "\\workspaces");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }*//*

                tempfile = new File(proDataHome + "\\Trades");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("Trades", proDataHome + "\\Trades");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("Trades", proDataHome + "\\Trades");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\DataStore");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("DataStore", proDataHome + "\\DataStore");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }else {
                    try {
                        copyNewFiles("DataStore", proDataHome + "\\DataStore");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\Exchanges");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("Exchanges", proDataHome + "\\Exchanges");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("Exchanges", proDataHome + "\\Exchanges");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\Charts");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("Charts", proDataHome + "\\Charts");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("Charts", proDataHome + "\\Charts");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\customindicators");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("customindicators", proDataHome + "\\customindicators");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("customindicators", proDataHome + "\\customindicators");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\formula");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("formula", proDataHome + "\\formula");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("formula", proDataHome + "\\formula");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\MetaStock");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("MetaStock", proDataHome + "\\MetaStock");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("MetaStock", proDataHome + "\\MetaStock");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\ohlc");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("ohlc", proDataHome + "\\ohlc");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("ohlc", proDataHome + "\\ohlc");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\PortfolioData");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("PortfolioData", proDataHome + "\\PortfolioData");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("PortfolioData", proDataHome + "\\PortfolioData");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\System");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("System", proDataHome + "\\System");
                        setVeryFistTime(true);
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("System", proDataHome + "\\System",true);
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\radarscreen");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("radarscreen", proDataHome + "\\radarscreen");
                        setVeryFistTime(true);
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("radarscreen", proDataHome + "\\radarscreen");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                tempfile = new File(proDataHome + "\\Temp");
                if (!tempfile.exists()) {
                    try {
                        copyFiles("Temp", proDataHome + "\\Temp");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        copyNewFiles("Temp", proDataHome + "\\Temp");
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                tempfile = null;
            }*/

        }
    }

    public static void copyFiles(String strPath, String dstPath) throws IOException {
        File src = new File(strPath);
        File dest = new File(dstPath);
        if (!src.exists()) {
            dest.mkdirs();
        } else if (src.isDirectory()) {
            dest.mkdirs();
            String list[] = src.list();
            for (int i = 0; i < list.length; i++) {
                String dest1 = dest.getAbsolutePath() + "\\" + list[i];
                String src1 = src.getAbsolutePath() + "\\" + list[i];
                copyFiles(src1, dest1);
            }
        } else {
            FileChannel sourceChannel = new FileInputStream(src).getChannel();
            FileChannel targetChannel = new FileOutputStream(dest).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(), targetChannel);
            sourceChannel.close();
            targetChannel.close();
        }
    }

    public static void copyNewFiles(String strPath, String dstPath, String fileList) throws IOException {
        File src = new File(strPath);
        File dest = new File(dstPath);
        File listedFile;
        if (!src.exists()) {
        } else if (src.isDirectory()) {
            /*if (!checkDefaultWSP) {
                String list[] = src.list();
                for (int i = 0; i < list.length; i++) {
                    String dest1 = dest.getAbsolutePath() + "\\" + list[i];
                    listedFile = new File(dest1);
                    if(!listedFile.exists()) {
                        String src1 = src.getAbsolutePath() + "\\" + list[i];
                        copyFiles(src1, dest1);
                    }
                }
            } else {*/
            String[] sList = fileList.split(",");
            ArrayList<String> excludeList = new ArrayList<String>();
            for (String str : sList) {
                excludeList.add(str);
            }
            String list[] = src.list();
            for (int i = 0; i < list.length; i++) {
                String dest1 = dest.getAbsolutePath() + "\\" + list[i];
                listedFile = new File(dest1);
                if (!excludeList.contains(listedFile.getName().toLowerCase()) && !listedFile.exists()) {
                    String src1 = src.getAbsolutePath() + "\\" + list[i];
                    copyFiles(src1, dest1);
                }
                /*if(!listedFile.getName().equalsIgnoreCase("Default.wsp") && !listedFile.exists()) {
                    String src1 = src.getAbsolutePath() + "\\" + list[i];
                    copyFiles(src1, dest1);
                }*/
            }
//            }
        } else {
            if (!dest.exists()) {
                copyFiles(src.getAbsolutePath(), dest.getAbsolutePath());
            }
        }
    }

    public static void copyNewFiles(String strPath, String dstPath) throws IOException {
        File src = new File(strPath);
        File dest = new File(dstPath);
        File listedFile;
        if (!src.exists()) {
        } else if (src.isDirectory()) {
            String list[] = src.list();
            for (int i = 0; i < list.length; i++) {
                String dest1 = dest.getAbsolutePath() + "\\" + list[i];
                listedFile = new File(dest1);
                if (!listedFile.exists()) {
                    String src1 = src.getAbsolutePath() + "\\" + list[i];
                    copyFiles(src1, dest1);
                }
            }
        } else {
            if (!dest.exists()) {
                copyFiles(src.getAbsolutePath(), dest.getAbsolutePath());
            }
        }
    }

    public static String getProDataHome() {
        return proDataHome;
    }

    public static void main(String[] args) {
        // Warning: never use this method to load the application. For Obfuscation only.
        if (true) {
            return;
        }
        new Mubasher(new String[0], false);
    }

    public static String processWorkspaces() {
        // Open the ZIP file
        File tempfile = new File("workspaces/");
        File[] files = tempfile.listFiles();
        String defaultWSP = "workspaces/Default/";
        String current = Toolkit.getDefaultToolkit().getScreenSize().width + "x" + Toolkit.getDefaultToolkit().getScreenSize().height;
        for (File file : files) {
            if (file.isDirectory()) {
                if (file.getName().equals(current)) {
                    return file.getAbsolutePath();
                }
            }
        }
        return defaultWSP;
    }

    public static boolean isVeryFistTime() {
        return isVeryFistTime;
    }

    public static void setVeryFistTime(boolean veryFistTime) {
        isVeryFistTime = veryFistTime;
    }

    private void checkForMetaStockUpdate() {
        try {
            File file = new File("metaAutoUpdate.bat");
            if (file.exists()) {
                String os = System.getProperty("os.name");
                boolean osIs98;
                try {
                    osIs98 = false;
                    if (os == null)
                        osIs98 = false;
                    else if ((os.indexOf("98") >= 0) || (os.toUpperCase().indexOf("WINDOWS ME") >= 0))
                        osIs98 = true;
                } catch (Exception e) {
                    osIs98 = false;
                }

                Process p;
                if (osIs98)
                    p = Runtime.getRuntime().exec("command.com /C .\\metaAutoUpdate.bat");
                else
                    p = Runtime.getRuntime().exec("cmd /C .\\metaAutoUpdate.bat");

                //important
                p.waitFor();

                //-------delete the file-----------
                try {
                    file.delete();
                } catch (Exception e) {
                    if (file != null)
                        file.deleteOnExit();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void loadFolderNamesToCopy() {
        try {
            File file = new File("profile.dll");
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            String str = null;
            while ((str = in.readLine()) != null) {
                if (str.trim().startsWith("#")) continue;
                folders.add(str);
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void showLanguageSelection() {
        String[] languages = Language.getStartMessages();
        String[] languagesIDs = Language.getLanguageIDs();
        Object[] options = new Object[1];
        //JPanel panel = new JPanel(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        if (languages.length == 1) {
            Settings.setItem("LANGUAGE", languagesIDs[0]);
            Language.setLanguage(languagesIDs[0]);
        } else {
            final ImageIcon image = new ImageIcon("images/common/language.jpg");
            if (image.getIconWidth() <= 0) {
                JOptionPane.showMessageDialog(null, "<html>Unable to load Application.<br>Error: Incomplete customization.<br>Contact customer support");
                System.exit(-1);
            }
            JPanel panel = new JPanel() {
                public void paint(Graphics g) {
                    g.drawImage(image.getImage(), 0, 0, this);
                    paintComponents(g);
                }
            };
            panel.setLayout(null);
            panel.setOpaque(false);
            panel.setBackground(Color.WHITE);

            //JLabel image = new JLabel(new ImageIcon("images/common/logo_en.jpg"));
            //panel.add(image);

            final JDialog dialog = new JDialog((JFrame) null, true);
            dialog.setUndecorated(true);

            for (int i = 0; i < languages.length; i++) {
                JButton button = new JButton(UnicodeUtils.getNativeString(languages[i]));
                button.setHorizontalAlignment(SwingConstants.LEFT);
                button.setHorizontalTextPosition(SwingConstants.LEFT);
                int y = image.getIconHeight();
                button.setSize(new Dimension(150, 30));
                button.setLocation(image.getIconWidth() - 150, y + 20 - (30 * (i + 1)) - 30);
                button.setContentAreaFilled(false);
                button.setFocusable(false);
                button.setCursor(new Cursor(Cursor.HAND_CURSOR));
                button.setBorder(BorderFactory.createEmptyBorder());
                button.setActionCommand(languagesIDs[i]);
                button.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Settings.setItem("LANGUAGE", e.getActionCommand());
                        Language.setLanguage(e.getActionCommand());
                        dialog.dispose();
                    }
                });
                panel.add(button);
            }
            panel.setBorder(BorderFactory.createLineBorder(Color.black, 2));

            dialog.add(panel);
            dialog.setSize(image.getIconWidth(), image.getIconHeight());
            dialog.setLocationRelativeTo(null);
            dialog.show();
        }
    }

    public void load(String[] args) {

        /*try {
            if (args[0].equalsIgnoreCase("/Cache")){
                OSCache.doCache();
                System.exit(0);
            }
        } catch (Exception e) {

        }

        if (NativeMethods.tooManyInstances())
            System.exit(0);

        try {
            copyFolderstoUserProfile();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            copyUpdatedProfile();
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkUpdateAvailable();

        Settings.setCurrentZone(TimeZone.getDefault());
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        Settings.init();



        // check for single sign on mode { format - SSOID LANG INSTid Trading}
        try {
            if (args.length == 4){
                String singleSignOnID = TWSecurityManager.getSingleSingOnID(args[0]);
                if (singleSignOnID != null){
                    Settings.setSingleSignOnMode(true);
                    //Settings.setUserID(singleSignOnID);
                    Settings.setSingleSignOnID(singleSignOnID);
                    Settings.setInstitutionID(args[2]);
                    Settings.setSsoTradingEnabled(args[3].equalsIgnoreCase("1"));
                    Settings.setUserDetails(singleSignOnID, "*****");
                }
                ssoLang = args[1];
                System.out.println("Single signon mode ---------");
            }

        } catch (Exception e) {
            Settings.setSingleSignOnMode(false);
            e.printStackTrace();
        }


        // check for custom ports
        *//*if (args.length == 2) {
            try {
                value1 = SharedMethods.intValue(args[0]);
                value2 = SharedMethods.intValue(args[1]);

                Settings.SERVER_PORT = value1;
                Settings.DOWNLOAD_PORT = value2;
            } catch (Exception ex) {
            }
        }*//**/
//        Mubasher mubasher = new Mubasher(args);
    }
}
