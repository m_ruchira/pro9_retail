package com.isi.csvr;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.UpdateableTable;
import com.isi.csvr.table.updator.TableUpdateManager;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 11, 2009
 * Time: 8:42:37 AM
 * To change this template use File | Settings | File Templates.
 */

public class BarChartPanel extends JPanel implements ComponentListener, MouseListener, MouseMotionListener, UpdateableTable, Themeable {

    public static final byte TYPE_BID = 0;
    protected byte chartType = TYPE_BID;
    public static final byte TYPE_ASK = 1;
    private final float BAR_WIDTH_FACTOR = .3f;
    private final float[] PriceVolPitch = {1f, 2f, 4f, 5f, 8f};
    private final float ALPHA_TRANSPARENT = .2f;
    JToolTip toolTip;
    TWDecimalFormat quantityFormatter = new TWDecimalFormat("###,###,###,###");
    private ArrayList<BarInfo> quantities = new ArrayList<BarInfo>();
    private ArrayList<BarInfo> allQantities = new ArrayList<BarInfo>();
    private double[] values;
    private int yAxisWidth = 10;
    private int xAxisHeight = 20;
    private int chartWidth = 450;
    private int chartHeight = 250; //these are the effective width and height of the chart
    private int legendHeight = 10;
    //colors
    private Color gridColor;
    private Color chartBackColor;
    private Color chartOutsideColor;
    private Color chartAxisFontColor;
    private Color barLeftColor;
    private Color barRightColor;
    private Color chartBorderColor;
    private double minY = Double.MAX_VALUE;
    private double maxY = -Double.MIN_VALUE;
    private double xFactor;
    private double yFactor;
    private double bestXPitch = 10;
    private double bestYPitch = 100;
    private double xStart = 1;
    private double yStart = 1;
    private double adj = 10;
    private String legend;
    private int barWidth = 10;
    private boolean isLeftToRight = true;
    private Font axisFont = new Font("Arial", Font.BOLD, 10);
    private TWDecimalFormat formatter = null;
    private TWDecimalFormat pctFormatter = new TWDecimalFormat("###,##0.00");
    private TWDecimalFormat priceFormatter = new TWDecimalFormat("###,###,##0.00");
    private Table infoTableOriginal;
    private Table infoTableOther;
    private Rectangle r = new Rectangle();
    private String htmlString = "<html><head><body></body> </head></html>";
    private String HTML_PRICE = Language.getString("MARKET_DEPATH_CHART_PRICE");
    private String HTML_QUANTITY = Language.getString("MARKET_DEPATH_CHART_QUANTITY");

    public BarChartPanel(double[] barValues, String str) {
        super();
        this.values = barValues;
        this.legend = str;
        this.addComponentListener(this);
    }

    public BarChartPanel(byte type) {
        this.chartType = type;
        this.addComponentListener(this);
        TableUpdateManager.addTable(this);
        Theme.registerComponent(this);
    }

    public BarChartPanel(Table table, byte type) {
        this.chartType = type;
        this.infoTableOriginal = table;
        this.addComponentListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        TableUpdateManager.addTable(this);
        Theme.registerComponent(this);
        this.setToolTipText("");

        gridColor = Theme.getColor("BAR_CHART_GRID_COLOR");
        chartBackColor = Theme.getColor("BAR_CHART_BACK_COLOR");
        chartAxisFontColor = Theme.getColor("BAR_CHART_AXIS_FONT_COLOR");
        chartBorderColor = Theme.getColor("BAR_CHART_BORDER_COLOR");
        chartOutsideColor = Theme.getColor("BAR_CHART_OUTSIDE_COLOR");

        if (chartType == TYPE_ASK) {
            barLeftColor = Theme.getColor("BAR_CHART_ASK_LEFT_COLOR");
            barRightColor = Theme.getColor("BAR_CHART_ASK_RIGHT_COLOR");
        } else {
            barLeftColor = Theme.getColor("BAR_CHART_BID_LEFT_COLOR");
            barRightColor = Theme.getColor("BAR_CHART_BID_RIGHT_COLOR");
        }

        if (!Language.isLTR()) {
            isLeftToRight = false;
        }
    }


    public BarChartPanel(Table originalTable, Table otherTable, byte type) {
        this.chartType = type;
        this.infoTableOriginal = originalTable;
        this.infoTableOther = otherTable;
        this.addComponentListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        TableUpdateManager.addTable(this);
        Theme.registerComponent(this);
        this.setToolTipText("");

        gridColor = Theme.getColor("BAR_CHART_GRID_COLOR");
        chartBackColor = Theme.getColor("BAR_CHART_BACK_COLOR");
        chartAxisFontColor = Theme.getColor("BAR_CHART_AXIS_FONT_COLOR");
        chartBorderColor = Theme.getColor("BAR_CHART_BORDER_COLOR");
        chartOutsideColor = Theme.getColor("BAR_CHART_OUTSIDE_COLOR");

        if (chartType == TYPE_ASK) {
            barLeftColor = Theme.getColor("BAR_CHART_ASK_LEFT_COLOR");
            barRightColor = Theme.getColor("BAR_CHART_ASK_RIGHT_COLOR");
        } else {
            barLeftColor = Theme.getColor("BAR_CHART_BID_LEFT_COLOR");
            barRightColor = Theme.getColor("BAR_CHART_BID_RIGHT_COLOR");
        }

        if (!Language.isLTR()) {
            isLeftToRight = false;
        }
    }

    public ArrayList<BarInfo> getArrayList() {

        TableModel model = infoTableOriginal.getModel();
        int rows = infoTableOriginal.getModel().getRowCount();
        if (rows > 5) {
            rows = 5;
        }
        ArrayList<BarInfo> qtList = new ArrayList<BarInfo>();
        if (chartType == TYPE_ASK) {
            if (Language.isLTR()) {
                for (int i = 0; i < rows; i++) {

                    double price = Double.parseDouble(String.valueOf(model.getValueAt(i, 1)));
                    long quantity = Long.parseLong(String.valueOf(model.getValueAt(i, 2)));
                    BarInfo pq = new BarInfo(price, quantity);
                    qtList.add(pq);
                }
            } else {
                for (int i = rows - 1; i >= 0; i--) {

                    double price = Double.parseDouble(String.valueOf(model.getValueAt(i, 1)));
                    long quantity = Long.parseLong(String.valueOf(model.getValueAt(i, 2)));
                    BarInfo pq = new BarInfo(price, quantity);
                    qtList.add(pq);
                }
            }
        } else if (chartType == TYPE_BID) {
            if (Language.isLTR()) {
                for (int i = rows - 1; i >= 0; i--) {
                    double price = Double.parseDouble(String.valueOf(model.getValueAt(i, 1)));
                    long quantity = Long.parseLong(String.valueOf(model.getValueAt(i, 2)));
                    BarInfo pq = new BarInfo(price, quantity);
                    qtList.add(pq);
                }
            } else {
                for (int i = 0; i < rows; i++) {
                    double price = Double.parseDouble(String.valueOf(model.getValueAt(i, 1)));
                    long quantity = Long.parseLong(String.valueOf(model.getValueAt(i, 2)));
                    BarInfo pq = new BarInfo(price, quantity);
                    qtList.add(pq);
                }
            }
        }

        return qtList;
    }

    public ArrayList<BarInfo> getArrayListAll() {

        TableModel model = infoTableOriginal.getModel();
        int rows = infoTableOriginal.getModel().getRowCount();
        if (rows > 5) {
            rows = 5;
        }
        ArrayList<BarInfo> qtList = new ArrayList<BarInfo>();

        for (int i = 0; i < rows; i++) {

            double price = Double.parseDouble(String.valueOf(model.getValueAt(i, 1)));
            long quantity = Long.parseLong(String.valueOf(model.getValueAt(i, 2)));
            BarInfo pq = new BarInfo(price, quantity);
            qtList.add(pq);
        }


        model = infoTableOther.getModel();
        rows = infoTableOther.getModel().getRowCount();
        if (rows > 5) {
            rows = 5;
        }

        for (int i = 0; i < rows; i++) {

            double price = Double.parseDouble(String.valueOf(model.getValueAt(i, 1)));
            long quantity = Long.parseLong(String.valueOf(model.getValueAt(i, 2)));
            BarInfo pq = new BarInfo(price, quantity);
            qtList.add(pq);
        }

        return qtList;
    }

    public void calculatePixelValues() {

        minY = Double.MAX_VALUE;
        maxY = -Double.MIN_VALUE;
        chartWidth = Math.max(this.getWidth() - yAxisWidth - 10, 100);
        chartHeight = Math.max((this.getHeight() - xAxisHeight - legendHeight), 80);

        if (quantities == null || quantities.size() == 0) {
            return;
        }
        values = new double[quantities.size()];

        for (int i = 0; i < quantities.size(); i++) {
            values[i] = quantities.get(i).getQuantity();
        }

        double[] tempValues = new double[allQantities.size()];
        for (int i = 0; i < allQantities.size(); i++) {
            tempValues[i] = allQantities.get(i).getQuantity();
        }
        for (double value : tempValues) {
            minY = Math.min(minY, value);
            maxY = Math.max(maxY, value);
        }

        adj = (maxY - minY) * 0.2f;
        //minY = minY - adj;  //TODO:
        maxY = maxY + adj;

        minY = (minY > 0) ? 0 : minY;

        yFactor = chartHeight / ((maxY - minY));
        bestYPitch = getBestYInterval(maxY - minY, chartHeight);
        yStart = 0; //hardcoded to zero since y basis must start from zero always.
        if (bestYPitch < 1) {
            formatter = new TWDecimalFormat("###,##0.00");
        } else {
            formatter = new TWDecimalFormat("###,###");
        }
    }

    @Override
    public String getToolTipText(MouseEvent e) {
        super.getToolTipText();

        htmlString = "";
        if (values != null && values.length > 0) {

            int number = values.length + 1;
            int HGAP = chartWidth / number;  //Horizontal gap between two horizonal lines
            int xStart = (int) (yAxisWidth + (HGAP * (1 - BAR_WIDTH_FACTOR)));
            int zeroPos = (int) getPixelForYValue(0);

            for (int i = 0; i < values.length; i++) {

                double value = 0;
                if (Language.isLTR()) {
                    value = values[i];
                } else {
                    value = values[values.length - 1 - i];
                }
                int ht = (int) ((value - 0) * yFactor);
                int barTop = zeroPos - ht;
                r.setBounds(xStart, barTop, barWidth, ht);
                if (r.contains(e.getPoint())) {
                    //System.out.println("******** inside ********* " + i);
                    htmlString = "<html><head><body>" + HTML_PRICE + " " + priceFormatter.format(quantities.get(i).getPrice()) +
                            "<br>" + HTML_QUANTITY + " " + quantityFormatter.format(quantities.get(i).getQuantity()) + "</body></head></html>";
                    this.setToolTipText(htmlString);
                    return String.valueOf(htmlString);
                }

                xStart = xStart + HGAP;
            }
        }

        this.setToolTipText(null);
        return null;
    }

    public Point getToolTipLocation(MouseEvent e) {
        if (isLeftToRight) {
            return new Point(e.getX() + 8, e.getY());
        } else {
            return new Point(e.getX() - 8, e.getY());
        }
    }

    public void paint(Graphics g) {
        super.paint(g);

        if (quantities == null || quantities.size() == 0) {
            return;
        }
        g.setColor(chartOutsideColor);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        g.setColor(chartBackColor);
        g.fillRect(yAxisWidth, legendHeight, chartWidth, chartHeight);

        //drawLegend(g);
        drawXAxisLabels(g);
        drawYAxisLabels(g);
        drawBars(g);
        //drawPercentages(g);

        //drawing the chart outline of the rectangle
        g.setColor(chartBorderColor);
        g.drawRect(yAxisWidth, legendHeight, chartWidth, chartHeight);


    }

    private void drawBars(Graphics g) {

        int number = values.length + 1;
        int HGAP = chartWidth / number;  //Horizontal gap between two horizonal lines
        barWidth = (int) (HGAP * 2.0 * BAR_WIDTH_FACTOR);
        int xStart = (int) (yAxisWidth + (HGAP * (1 - BAR_WIDTH_FACTOR)));
        int zeroPos = (int) getPixelForYValue(0);

        Graphics2D g2 = (Graphics2D) g;

        for (double value : values) {

            if (value > 0) {
                GradientPaint greentowhite = new GradientPaint(xStart, 0, barLeftColor, xStart + barWidth, 0, barRightColor);
                g2.setPaint(greentowhite);

                int ht = (int) ((value - 0) * yFactor);
                int barTop = zeroPos - ht;
                g.fillRect(xStart, barTop, barWidth, ht);

                //drawing the % string
                //g.setFont(axisFont);
                //g.setColor(chartAxisFontColor);
                //String strVal = pctFormatter.format(value) + "%";
                //int labelLength = g.getFontMetrics().stringWidth(strVal);
                //g.drawString(strVal, xStart + ((barWd - labelLength) / 2), barTop - 5);
            } else {
                GradientPaint redtowhite = new GradientPaint(xStart, 0, barLeftColor, xStart + barWidth, 0, barRightColor);
                g2.setPaint(redtowhite);

                int ht = (int) ((0 - value) * yFactor);
                g.fillRect(xStart, zeroPos, barWidth, ht);

                //drawing the % string
                g.setFont(axisFont);
                g.setColor(chartAxisFontColor);
                String strVal = pctFormatter.format(value) + "%";
                int labelLength = g.getFontMetrics().stringWidth(strVal);
                //g.drawString(strVal, xStart + ((barWd - labelLength) / 2), zeroPos + ht + 10);
            }
            xStart = xStart + HGAP;
        }
    }

    private void drawPercentages(Graphics g) {

        int number = values.length + 1;
        int HGAP = chartWidth / number;  //Horizontal gap between two horizonal lines
        int barWd = (int) (HGAP * 2.0 * BAR_WIDTH_FACTOR);
        int xStart = (int) (yAxisWidth + (HGAP * (1 - BAR_WIDTH_FACTOR)));
        int zeroPos = (int) getPixelForYValue(0);

        Graphics2D g2 = (Graphics2D) g;
        g.setFont(new Font("Arial", Font.BOLD, 10));
        g.setColor(chartAxisFontColor);

        for (double value : values) {

            String strVal = pctFormatter.format(value) + "%";
            int labelLength = g.getFontMetrics().stringWidth(strVal);

            if (value > 0) {
                int ht = (int) ((value - 0) * yFactor);
                int barTop = zeroPos - ht;
                //drawing the % string
                g.drawString(strVal, xStart + ((barWd - labelLength) / 2), barTop - 5);
            } else {
                int ht = (int) ((0 - value) * yFactor);
                //drawing the % string
                g.drawString(strVal, xStart + ((barWd - labelLength) / 2), zeroPos + ht + 10);
            }
            xStart = xStart + HGAP;
        }
    }

    private void drawYAxisLabels(Graphics g) {

        float labelLength = 0;
        g.setColor(this.getForeground());
        //double currVal = yStart;    //TODO:
        double currVal = 0;
        float x = yAxisWidth - 5;
        float yAdj = 0;

        Graphics2D g2D = (Graphics2D) g;

        while (currVal <= maxY) {
            float y = getPixelForYValue(currVal);
            g.setColor(gridColor);
            g2D.setStroke(new BasicStroke(.4f));
            g.drawLine(yAxisWidth, (int) y, (yAxisWidth + chartWidth), (int) y);

            g.setColor(chartAxisFontColor);
            String strVal = formatter.format(currVal);
            labelLength = g.getFontMetrics().stringWidth(strVal);
            //g.setColor(Color.BLACK);
            //g.drawString(strVal, (int) (x - labelLength), (int) (y + yAdj));
            //g.drawLine((int) yAxisWidth, (int) y, (int) yAxisWidth - 3, (int) y);
            currVal += bestYPitch;
        }

        //drawing the negative lines and values
        currVal = 0;
        while (currVal >= minY) {
            float y = getPixelForYValue(currVal);
            g.setColor(gridColor);
            g2D.setStroke(new BasicStroke(.4f));
            g.drawLine(yAxisWidth, (int) y, (yAxisWidth + chartWidth), (int) y);

            g.setColor(chartAxisFontColor);
            String strVal = formatter.format(currVal);
            labelLength = g.getFontMetrics().stringWidth(strVal);
            //g.drawString(strVal, (int) (x - labelLength), (int) (y + yAdj));
            //g.drawLine((int) yAxisWidth, (int) y, (int) yAxisWidth - 3, (int) y);
            currVal -= bestYPitch;
        }
    }

    public void drawXAxisLabels(Graphics g) {

        int number = values.length + 1;
        int width = chartWidth / number;
        int xStart = yAxisWidth + width;

        for (int i = 0; i < number - 1; i++) {
            //int index = i + 1;
            int index = 0;
            if (chartType == TYPE_BID) {
                if (Language.isLTR()) {
                    index = values.length - i;
                } else {
                    index = i + 1;
                }
            } else if (chartType == TYPE_ASK) {
                if (Language.isLTR()) {
                    index = i + 1;
                } else {
                    index = values.length - i;
                }
            }
            g.setColor(chartAxisFontColor);
            g.drawLine(xStart, legendHeight + chartHeight, xStart, legendHeight + chartHeight + 8);
            g.drawString(String.valueOf(index), xStart - 2, (legendHeight + chartHeight + 20));

            g.setColor(gridColor);
            g.drawLine(xStart, legendHeight, xStart, (legendHeight + chartHeight));
            g.drawLine(xStart, (legendHeight + chartHeight), xStart, (legendHeight + chartHeight));
            xStart += width;
        }
    }

    public void drawLegend(Graphics g) {
        g.setColor(Color.BLUE);
        g.drawString(legend, yAxisWidth, legendHeight - 15);
    }

    private double getBestYInterval(double val, float pixLen) {

        double HALF_INCH = 72 / 3d;
        val = val / pixLen * HALF_INCH;

        float pitch = PriceVolPitch[0];
        for (long j = 1; j < Long.MAX_VALUE; j *= 10) {
            float Multiplier = ((float) j / 1000000000);
            for (float aPriceVolPitch : PriceVolPitch) {
                if (val >= (double) Multiplier * aPriceVolPitch) {
                    pitch = (Multiplier * aPriceVolPitch);
                } else {
                    return pitch;
                }
            }
            if (j > Long.MAX_VALUE / 10f) {
                return pitch;
            }
        }
        return pitch;
    }

    private float getPixelForYValue(double value) {
        return chartHeight + legendHeight - (float) ((value - minY) * yFactor);
    }

    private double getStart(double min, double bestPitch) {
        return bestPitch * Math.round(min / bestPitch);
    }

    public ArrayList<BarInfo> getQuantities() {
        return quantities;
    }

    public void setQuantities(ArrayList<BarInfo> quantities) {
        this.quantities = quantities;
    }

    public void componentResized(ComponentEvent e) {
        calculatePixelValues();
        repaint();
    }

    public void componentMoved(ComponentEvent e) {

    }

    public void componentShown(ComponentEvent e) {

    }

    public void componentHidden(ComponentEvent e) {

    }

    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

        if (values != null && values.length > 0) {

            int number = values.length + 1;
            int HGAP = chartWidth / number;  //Horizontal gap between two horizonal lines
            int xStart = (int) (yAxisWidth + (HGAP * (1 - BAR_WIDTH_FACTOR)));
            int zeroPos = (int) getPixelForYValue(0);

            for (int i = 0; i < values.length; i++) {
                double value = values[i];
                int ht = (int) ((value - 0) * yFactor);
                int barTop = zeroPos - ht;
                r.setBounds(xStart, barTop, barWidth, ht);
                if (r.contains(e.getPoint())) {
                    int rowNumber = i;
                    if (chartType == TYPE_BID) {
                        if (Language.isLTR()) {
                            rowNumber = values.length - i - 1;
                        } else {
                            rowNumber = i;
                        }
                    } else if (chartType == TYPE_ASK) {
                        if (Language.isLTR()) {
                            rowNumber = i;
                        } else {
                            rowNumber = values.length - i - 1;
                        }
                    }
                    infoTableOriginal.getTable().setRowSelectionInterval(rowNumber, rowNumber);
                    Rectangle r = infoTableOriginal.getTable().getCellRect(rowNumber, 0, true);
                    infoTableOriginal.getTable().scrollRectToVisible(infoTableOriginal.getTable().getCellRect(infoTableOriginal.getTable().getRowCount() - 1, 0, true));
                    infoTableOriginal.getTable().scrollRectToVisible(r);
                    infoTableOriginal.scrollRectToVisible(infoTableOriginal.getTable().getCellRect(rowNumber - 1, 0, true));

                }

                xStart = xStart + HGAP;
            }
        }
    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseDragged(MouseEvent e) {

    }

    public void mouseMoved(MouseEvent e) {


    }

    public void runThread() {

        //System.out.println("********** running thred **********");
        quantities = getArrayList();
        allQantities = getArrayListAll();
        calculatePixelValues();
        repaint();
    }

    public void removeThread() {
        TableUpdateManager.removeTable(this);
    }

    public String getTitle() {
        return "bar chart updator";
    }

    public Constants.ThreadTypes getTableID() {
        return Constants.ThreadTypes.DEFAULT;
    }

    public void applyTheme() {
        gridColor = Theme.getColor("BAR_CHART_GRID_COLOR");
        chartBackColor = Theme.getColor("BAR_CHART_BACK_COLOR");
        chartAxisFontColor = Theme.getColor("BAR_CHART_AXIS_FONT_COLOR");
        chartBorderColor = Theme.getColor("BAR_CHART_BORDER_COLOR");
        chartOutsideColor = Theme.getColor("BAR_CHART_OUTSIDE_COLOR");
        if (chartType == TYPE_ASK) {
            barLeftColor = Theme.getColor("BAR_CHART_ASK_LEFT_COLOR");
            barRightColor = Theme.getColor("BAR_CHART_ASK_RIGHT_COLOR");
        } else {
            barLeftColor = Theme.getColor("BAR_CHART_BID_LEFT_COLOR");
            barRightColor = Theme.getColor("BAR_CHART_BID_RIGHT_COLOR");
        }
    }

    public void setFormatter(byte decimalPlaces) {

        if (decimalPlaces == 2) {
            priceFormatter.applyPattern("###,###,###.00");
        } else if (decimalPlaces == 3) {
            priceFormatter.applyPattern("###,###,###.000");
        } else if (decimalPlaces == 4) {
            priceFormatter.applyPattern("###,###,###.0000");
        }

    }
}
