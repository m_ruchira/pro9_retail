package com.isi.csvr;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.StatusBarLayout;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 6, 2006
 * Time: 10:53:39 PM
 */
public class StatusBar extends JPanel implements Themeable {

    private ImageIcon tile;
    private int imageWidth;

    public StatusBar() {
        StatusBarLayout oStatusLayout = new StatusBarLayout(Language.isLTR());
        setLayout(oStatusLayout);
        Theme.registerComponent(this);
    }

    public void applyTheme() {
        tile = new ImageIcon("images/theme" + Theme.getID() + "/statusbartile.png");
        imageWidth = tile.getIconWidth();
        setBackground(Theme.getColor("TOOLBAR_BGCOLOR"));
        setForeground(Theme.getColor("TOOLBAR_FGCOLOR"));
    }

    public void paint(Graphics g) {

        /*int height = getHeight();

        for (int i = 0; i <= height; i++) {
            g.setColor(new Color((((lightColor.getRed() * (height - i)) + darkColor.getRed() * i) / height),
                    (((lightColor.getGreen() * (height - i)) + darkColor.getGreen() * i) / height),
                    (((lightColor.getBlue() * (height - i)) + darkColor.getBlue() * i) / height)));
            g.drawLine(0, i, getWidth(), i);
        }*/
        if (imageWidth > 0) {
            int width = getWidth();
            for (int i = 0; i <= width; i += imageWidth) {
                g.drawImage(tile.getImage(), i, 0, this);
            }

            super.paintChildren(g);
        } else {
            super.paint(g);
        }

    }
}
