package com.isi.csvr.sectormap;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 3, 2007
 * Time: 9:57:11 PM
 */
public class SectorMapRecord implements Comparable {
    private String id;
    private String caption;
    private int[] map;


    public SectorMapRecord(String id) {
        this.id = id;
        map = new int[5];
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int[] getMap() {
        return map;
    }

    public String getId() {
        return id;
    }

    public int compareTo(Object o) {
        return this.getCaption().compareToIgnoreCase(((SectorMapRecord) o).getCaption());
    }

}
