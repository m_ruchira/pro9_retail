package com.isi.csvr.sectormap;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 3, 2007
 * Time: 9:11:55 PM
 */

public class SectorMapModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private DynamicArray dataStore;

    /**
     * Constructor
     */
    public SectorMapModel() {
        //init();
    }

    public void setSymbol(String symbol) {

    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return SectorMapStore.getSharedInstance().size();
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            SectorMapRecord record = SectorMapStore.getSharedInstance().getRecord(iRow);

            switch (iCol) {
                case 0:
                    return record.getCaption();
                case 1:
                    return record.getMap();


            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return Object.class;
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[5];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEW_ANNOUNCEMENTS"), FIELD_NEW_ANNOUNCEMENT, null, Theme.getColor("ANNOUNCEMENT_NEW_LINE_FGCOLOR"));
        return customizerRecords;
    }

    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }
}
