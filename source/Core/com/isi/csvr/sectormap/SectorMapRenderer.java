package com.isi.csvr.sectormap;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 4, 2007
 * Time: 9:38:03 PM
 */

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class SectorMapRenderer extends TWBasicTableRenderer {
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private SectorMapImage image;
    private Color foreground, background;
    private DefaultTableCellRenderer lblRenderer;

    public SectorMapRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        reload();
        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;

        image = new SectorMapImage();
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void propertyChanged(int property) {
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            lblRenderer.setFont(lblRenderer.getFont().deriveFont(Font.PLAIN));
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                } else {
                    foreground = sett.getRowColor2FG();
                }
            }
            if (isSelected) {
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    background = sett.getRowColor1BG();
                } else {
                    background = sett.getRowColor2BG();
                }
            }
        } else {
            lblRenderer.setFont(lblRenderer.getFont().deriveFont(Font.PLAIN));
            if (isSelected) {
                foreground = g_oSelectedFG;
            } else {
                if (row % 2 == 0) {
                    foreground = g_oFG1;
                } else {
                    foreground = g_oFG2;
                }
            }
            if (isSelected) {
                background = g_oSelectedBG;
            } else {
                if (row % 2 == 0) {
                    background = g_oBG1;
                } else {
                    background = g_oBG2;
                }
            }
        }

        image.setHeight(table.getRowHeight());
        image.setWidth(table.getColumnModel().getColumn(1).getWidth());
        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            switch (iRendID) {
                case 1: // SYMBOL
                    lblRenderer.setText((String) value);
                    lblRenderer.setIcon(null);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 'i': // DESCRIPTION
                    lblRenderer.setText("");
                    image.setValue((int[]) value);
                    lblRenderer.setIcon(image);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lblRenderer;
    }
}
