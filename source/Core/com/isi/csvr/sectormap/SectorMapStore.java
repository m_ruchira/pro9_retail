package com.isi.csvr.sectormap;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Sector;
import com.isi.csvr.datastore.SectorStore;
import com.isi.csvr.shared.Stock;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 3, 2007
 * Time: 9:56:15 PM
 */
public class SectorMapStore implements Runnable {
    private static SectorMapStore self = null;
    private List<SectorMapRecord> store;
    private String exchange;
    private boolean active;

    private SectorMapStore() {
        store = Collections.synchronizedList(new ArrayList<SectorMapRecord>());
        Thread thread = new Thread(this, "SectorMap");
        thread.start();

    }

    public static synchronized SectorMapStore getSharedInstance() {
        if (self == null) {
            self = new SectorMapStore();
        }
        return self;
    }

    public SectorMapRecord getRecord(int index) {
        try {
            return store.get(index);
        } catch (Exception e) {
            return null;
        }
    }


    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public int size() {
        return store.size();
    }

    public void clear() {
        store.clear();
    }


    public void setActive(boolean active) {
        this.active = active;
    }

    public void run() {
        String currentExchange = null;
        Hashtable<String, SectorMapRecord> recordIndex = new Hashtable<String, SectorMapRecord>();
        while (true) {
            if (active) {
                if ((currentExchange == null) || (!currentExchange.equals(exchange))) {
                    currentExchange = exchange;
                    store.clear();
                    if (exchange != null) {
                        Enumeration<Sector> sectors = SectorStore.getSharedInstance().getSectors(currentExchange);
                        while (sectors.hasMoreElements()) {
                            Sector sector = sectors.nextElement();
                            SectorMapRecord record = new SectorMapRecord(sector.getId());
                            record.setCaption(sector.getDescription());
//                            record.setMap("Boo");
                            store.add(record);
                            recordIndex.put(record.getId(), record);
                        }
                        Collections.sort(store);
                    }
                } else {
                    try {
                        populateData(currentExchange, recordIndex);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void populateData(String exchange, Hashtable<String, SectorMapRecord> maps) {
        if (exchange == null) return;

        for (SectorMapRecord record : store) {
            record.getMap()[0] = 0;
            record.getMap()[1] = 0;
            record.getMap()[2] = 0;
            record.getMap()[3] = 0;
            record.getMap()[4] = 0;
        }

        Enumeration<Stock> stocks = DataStore.getSharedInstance().getExchangeSymbolStore(exchange).elements();

        while (stocks.hasMoreElements()) {
            Stock stock = stocks.nextElement();
            if (stock != null) {
                SectorMapRecord map = maps.get(stock.getSectorCode());
                //System.out.println("Stock " + stock.getSymbol() + "Sector " + stock.getSectorCode());
                if (map != null) {
                    if (stock.getPercentChange() <= -2) {
                        map.getMap()[0] += 1;
                        map.getMap()[4] += 1;
                    } else if (stock.getPercentChange() < 0) {
                        map.getMap()[1] += 1;
                        map.getMap()[4] += 1;
                    } else if (stock.getPercentChange() >= 2) {
                        map.getMap()[2] += 1;
                        map.getMap()[4] += 1;
                    } else if (stock.getPercentChange() > 0) {
                        map.getMap()[3] += 1;
                        map.getMap()[4] += 1;
                    }
//                    System.out.println("Sector " + map.getId() + " " + map.getMap()[0] + "," + map.getMap()[1] + ","+ map.getMap()[2] + ","+ map.getMap()[3] + ","+ map.getMap()[4]);
                    //map.setMap(map.getMap()+stock.getLastTradeValue());
                }
            }
        }

    }
}
