package com.isi.csvr;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWControl;
import com.isi.csvr.table.IndexPanel;
import com.isi.csvr.table.StretchedIndexPanel;
import com.isi.csvr.table.updator.SymbolPanel;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexFlowLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 11, 2005
 * Time: 4:09:03 PM
 */
public class ToolBar extends JPanel implements Themeable, ExchangeListener {

    public static PANEL_MODES panelSelection = PANEL_MODES.MainIndex;//0=mainIndex,1=StrechedIndexpanel,2=symbolPanel
    private static ToolBar self = null;
    public boolean set_to_newView = false;
    Client clientMain;
    JPanel toolbar;
    JPanel hideToolbar;
    JPanel newPanel;
    //    private Color lightColor;
//    private Color darkColor;
    private ImageIcon tile;
    private int imageWidth;

    public ToolBar(Client client) {
        this.clientMain = client;
        set_to_newView = false;
//        darkColor = Theme.MENU_GRADIENT2_COLOR;
//        lightColor = Theme.MENU_GRADIENT1_COLOR;
        tile = new ImageIcon();
        setOpaque(true);
        toolbar = clientMain.getMenus().createToolbar();
        hideToolbar = clientMain.getMenus().createHideButton(0);
        if (Settings.symbolPanelOnly) {
            addSymbolPanelOnly();
        } else {
            addToolbarPanel();
            setInitealToolbar(TWControl.getIntItem("TOOL_BAR_INITEAL_MODE", 0));
        }
        self = this;
        applyTheme();
        Theme.registerComponent(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    //Panel selection constants
    /*private final int mainIndexMode=0;
    private final int graphIndexMode=3;
    private final int stretchedIndexMode=1;
    private final int symbolIndexMode=2;*/

    public static ToolBar getInstance() {
        if (self == null) {
            self = new ToolBar(Client.getInstance());
        }
        return self;
    }

    public static PANEL_MODES getIndexPanelMode() {
        return panelSelection;
    }

    public void addSymbolPanelOnly() {
        removeAll();
        String[] toolbarWidths = {"0", "100%"};//,"100%", "0"};
        setLayout(new FlexFlowLayout(toolbarWidths, 0, 0));
        add(toolbar);
        add(SymbolPanel.getInstance());
//        add(new JLabel());
//        add(hideToolbar);
        set_to_newView = false;
        doLayout();
        updateUI();
        applyTheme();
        repaint();
        panelSelection = PANEL_MODES.MainIndex;
    }

    public void addNewIndexPanel() {
//        remove(IndexPanel.getInstance());
//        remove(toolbar);
//        remove(hideToolbar);
        removeAll();
        String[] toolbarWidths = {"0", "100%", "0"};
        setLayout(new FlexFlowLayout(toolbarWidths, 0, 0));
        newPanel = StretchedIndexPanel.getInstance();
        newPanel.setPreferredSize(new Dimension(1020, 35));
        add(newPanel);
        add(new JLabel());
        add(clientMain.getMenus().createHideButton(1));
        doLayout();
        set_to_newView = true;
        updateUI();
        applyTheme();
        repaint();
        panelSelection = PANEL_MODES.StretchedPanel;
    }

    public void addToolbarPanel() {
//        if (set_to_newView) {
        removeAll();
//        }
        //  String[] toolbarWidths = {"0", "0", "100%", "0"};
        String[] toolbarWidths = {"0", "0", "100%"};
        setLayout(new FlexFlowLayout(toolbarWidths, 0, 0));
        add(toolbar);
        add(IndexPanel.getInstance());
        // add(new JLabel());
        add(hideToolbar);
        set_to_newView = false;
        doLayout();
        updateUI();
        applyTheme();
        repaint();
        if (IndexPanel.getInstance().isGraphMode()) {
            panelSelection = PANEL_MODES.GraphMode;
        } else {
            panelSelection = PANEL_MODES.MainIndex;
        }
    }

    public void addSymbolPanel() {
//        if (set_to_newView) {
        removeAll();
//        }
        //  String[] toolbarWidths = {"0", "100%", "0"};
        String[] toolbarWidths = {"0", "100%", "0"};
        setLayout(new FlexFlowLayout(toolbarWidths, 0, 0));
        //  hideToolbar.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        add(toolbar);
        add(SymbolPanel.getInstance());
//        add(new JLabel());
        add(hideToolbar);
        set_to_newView = false;

        doLayout();
        hideToolbar.doLayout();
        updateUI();
        applyTheme();
        repaint();
        panelSelection = PANEL_MODES.SymbolPanel;
    }

    public void setToolBar(String indexpanelMode) {
        try {
            setToolBar(PANEL_MODES.valueOf(indexpanelMode));
        } catch (IllegalArgumentException e) {
        }
    }

    public void setToolBar(PANEL_MODES indexpanelMode) {
        if (Settings.symbolPanelOnly) {
            addSymbolPanelOnly();
        } else {
            if (indexpanelMode == PANEL_MODES.MainIndex) {
                addToolbarPanel();
            } else if (indexpanelMode == PANEL_MODES.StretchedPanel) {
                addNewIndexPanel();
            } else if (indexpanelMode == PANEL_MODES.SymbolPanel) {
                addSymbolPanel();
            } else if (indexpanelMode == PANEL_MODES.GraphMode) {
                // IndexPanel.getInstance().graphMode = true;
                IndexPanel.getInstance().setGraphMode(true);
                panelSelection = PANEL_MODES.GraphMode;
                addToolbarPanel();
            }
        }
    }

    private void setInitealToolbar(int value) {
        if (value == 1) {
            setToolBar(PANEL_MODES.StretchedPanel);
        } else if (value == 2) {
            setToolBar(PANEL_MODES.SymbolPanel);
        } else if (value == 3) {
            setToolBar(PANEL_MODES.GraphMode);
        } else {
            setToolBar(PANEL_MODES.MainIndex);
        }
    }

    public void applyTheme() {
        tile = null;
//        tile = new ImageIcon("images/theme" + Theme.getID() + "/toolbartile.png");
//        imageWidth = tile.getIconWidth();
        if (set_to_newView) {
            tile = new ImageIcon("images/theme" + Theme.getID() + "/indexpaneltile.png");
            imageWidth = tile.getIconWidth();
//            setBackground(Theme.getColor("STRETCHED_INDEX_BACKGROUND"));
//            setForeground(Theme.getColor("STRETCHED_INDEX_FOREGROUND"));
//            hideToolbar.setForeground(Theme.getColor("STRETCHED_INDEX_FOREGROUND"));
//            hideToolbar.setBackground(Theme.getColor("STRETCHED_INDEX_BACKGROUND"));

        } else {
            tile = new ImageIcon("images/theme" + Theme.getID() + "/toolbartile.png");
            imageWidth = tile.getIconWidth();
//            setBackground(Theme.getColor("TOOLBAR_BGCOLOR"));
//            setForeground(Theme.getColor("TOOLBAR_FGCOLOR"));
//            hideToolbar.setForeground(Theme.getColor("TOOLBAR_FGCOLOR"));
//            hideToolbar.setBackground(Theme.getColor("TOOLBAR_BGCOLOR"));
        }
        hideToolbar.updateUI();
    }

    public void paint(Graphics g) {
        try {
            if (tile != null && imageWidth > 0) {
                int width = getWidth();
                for (int i = 0; i <= width; i += imageWidth) {
                    g.drawImage(tile.getImage(), i, 0, this);
                }
                super.paintChildren(g);
            } else {
                super.paint(g);
            }
        } catch (Exception e) {

        }
    }

    public void exchangeAdded(Exchange exchange) {
    }

    // exchange listener

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        if ((ExchangeStore.getSharedInstance().defaultCount() == 0) && (ExchangeStore.getSharedInstance().count() > 0)) {
            setToolBar(PANEL_MODES.SymbolPanel);
            hideToolbar.setVisible(false);
        } else {
            hideToolbar.setVisible(true);
        }
    }

    public void exchangesLoaded() {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public static enum PANEL_MODES {
        MainIndex, StretchedPanel, SymbolPanel, GraphMode
    }
}
