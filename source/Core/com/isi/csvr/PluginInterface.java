package com.isi.csvr;

import com.isi.csvr.properties.ViewSetting;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 7, 2007
 * Time: 12:51:33 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PluginInterface {

    public String saveWorkspace();

    public ViewSetting getViewSetting();

    public void applyWorkspace();

    public int getWindowType();

    public void addToMenu(TWMenuItem menuItem, int index, String section);

    public void addToMenu(TWMenu menuItem, int index, String section);

    public void addToRightClickMenu(TWMenuItem menuItem, int index);

    public void addToRightClickMenu(TWMenu menuItem, int index);
}