package com.isi.csvr.iframe;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public interface TWDesktopInterface {
    public static int UNASSIGNED = -1;
    public static int BOARD_TYPE = 0;
    public static int POPUP_TYPE = 1;
//    public final int PORTFOLIO_TYPE = 2;

    public int getDesktopIndex();

    public void setDesktopIndex(int index);

    public void closeWindow();

    public int getDesktopItemType();

    public int getWindowType();

    public void setWindowType(int type);

    public String getTitle();

}