package com.isi.csvr.iframe;


import com.isi.csvr.*;
import com.isi.csvr.chart.GraphFrame;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.DataStoreInterface;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ProcessListener;
import com.isi.csvr.forex.ForexBoard;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.marketdepth.MDepthCalculator;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.print.PrintPreview;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.sectoroverview.SectorOverviewUI;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.ticker.GlobalSettings;
import com.mubasher.plugin.PFrame;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.*;
import java.awt.event.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

//import com.isi.util.ObjectWatcher;

/**
 * A Class class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class InternalFrame extends PFrame
        implements InternalFrameListener, WindowWrapper,
        DropTargetListener, Printable, MouseListener,
        ActionListener, PropertyChangeListener,
        TWDesktopInterface, ComponentListener,
        ProcessListener, TWTitlePaneInterface, Themeable, ApplicationListener {

    TWDateFormat g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
    TWDateFormat g_oDateTimeFormatHM = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HM_FORMAT"));//" yyyy/MM/dd HH:mm ");
    TWDateFormat g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));//" dd:MM:yyyy '-' HH:mm:ss ");
    TWDateFormat g_oTimeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));//" HH:mm:ss ");
    TWDateFormat g_oDateFormat2 = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));
    TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
    TWDecimalFormat oChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    TWMenuItem linkTosideBar;
    TWMenuItem redmenu;
    TWMenuItem greenmenu;
    TWMenuItem bluemenu;
    TWMenuItem yellowmenu;
    TWMenuItem whitemenu;
    TWMenuItem notlinkedmenu;
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private int windowType;
    private Table g_oTable1;
    private Table[] g_oTable2;
    private Hashtable<String, Table> tables = new Hashtable<String, Table>();
    //    private Table g_oTable2;
    //private DQTable     g_oDQTable;
    private TWMenuItem g_oMenuItem;
    private Menus g_oMenus;
    private boolean g_bActive = true;
    private WindowDaemon g_oDaemon;
    private ThreadGroup g_oThreadGroup = null;
    private String g_sThreadID;
    //    private StaticPriceDepthStore  g_oStaticPriceDepthStore = null;
    //    private StaticDepthStore       g_oStaticDepthStore = null;
    private DropTarget dropTarget = null;
    private String windowID = "";
    private String[] dataRequestID = new String[1];
    private DataStoreInterface[] dataStore = new DataStoreInterface[1];
    private String symbol;
    private JPopupMenu titleBarMenu;
    private ViewSetting oSettings = null;
    private boolean resizeWindowOnTitleToggle = false;
    private JSplitPane splitPane = null;
    // Variables used for Printing
    private int m_maxNumPage = 1;
    private int maximumWidths[];
    private int colXPositions[];
    private int maxWidth = 110;
    private Dimension minimumSize = new Dimension(100, 100);
    private boolean showServicesMenu = false;
    private boolean isLinkGroupsEnabled = false;
    private JPopupMenu servicesMenu;
    private JPopupMenu linkGroupMenu;
    private boolean printable;
    private boolean columnResizeable;
    private boolean detachable;
    private boolean detached;
    private Object detachedFrame;
    private Object[] objects = null;
    private boolean isModel = false;
    private int desktopType = POPUP_TYPE;
    private InternalFrame parent;
    private float aspectRatioReltivetoWd;
    private Dimension origSize = new Dimension();
    private Dimension oldSize = new Dimension();
    private boolean isLodedFromWsp = false;
    private String linkedGroupID = LinkStore.LINK_NONE;
    private int layer = -1;

    public InternalFrame() {
        createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
        this.addInternalFrameListener(this);
        this.addPropertyChangeListener("title", this);

    }

    public InternalFrame(WindowDaemon oDaemon) {
        g_oDaemon = oDaemon;
        createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
        this.addInternalFrameListener(this);
        this.addPropertyChangeListener("title", this);
    }

    public InternalFrame(boolean showTitleBarMenu) {
        if (showTitleBarMenu)
            createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
    }

    public InternalFrame(String title, ViewSetting settings) {
        super.setTitle(title);
        oSettings = settings;
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
        this.addInternalFrameListener(this);
        this.addPropertyChangeListener("title", this);

    }

    /**
     * Constructor
     */
    public InternalFrame(ThreadGroup oThreadGroup, String sThreadID,
                         WindowDaemon oDaemon, Table oTable) {
        g_oThreadGroup = oThreadGroup;
        g_sThreadID = sThreadID;
        g_oTable1 = oTable;
        g_oDaemon = oDaemon;
//setWindowStyle();
        this.addInternalFrameListener(this);
        dropTarget = new DropTarget(this, this);
        createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
//Theme.registerComponent(this);
    }


    public InternalFrame(ThreadGroup oThreadGroup, String sThreadID,
                         WindowDaemon oDaemon, Table oTable, Table[] oTable2) {
        g_oThreadGroup = oThreadGroup;
        g_sThreadID = sThreadID;
        g_oTable1 = oTable;
        g_oTable2 = oTable2;
        g_oDaemon = oDaemon;
        this.addInternalFrameListener(this);
        dropTarget = new DropTarget(this, this);
        createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
    }

    public InternalFrame(String sThreadID,
                         WindowDaemon oDaemon, Table oTable, Table[] oTable2) {
        g_oThreadGroup = null;
        g_sThreadID = sThreadID;
        g_oTable1 = oTable;
        g_oTable2 = oTable2;
        g_oDaemon = oDaemon;
        this.addInternalFrameListener(this);
        dropTarget = new DropTarget(this, this);
        createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
    }

    public InternalFrame(Table oTable) {
        g_oTable1 = oTable;
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        dropTarget = new DropTarget(this, this);
        createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
        this.addInternalFrameListener(this);
    }

    public InternalFrame(Table oTable, WindowDaemon oDaemon) {
        g_oTable1 = oTable;
        g_oDaemon = oDaemon;
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        dropTarget = new DropTarget(this, this);
        createTitlebarManu();
//        createServicesMenu();
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
        this.addInternalFrameListener(this);
    }

    public void setWindowDeamon(WindowDaemon oDaemon) {
        g_oDaemon = oDaemon;
    }

    public void setRootPane(JRootPane root) {
        super.setRootPane(root);
    }

    protected final JRootPane createRootPane() {
        return new DetachableRootPane();
    }

    public boolean isPrintable() {
        return printable;
    }

    public void setPrintable(boolean printable) {
        this.printable = printable;
    }

    public boolean isColumnResizeable() {
        return columnResizeable;
    }

    public void setColumnResizeable(boolean columnResizeable) {
        this.columnResizeable = columnResizeable;
    }

    public boolean isDetachable() {
        return detachable;
    }

    public void setDetachable(boolean detachableable) {
        this.detachable = detachableable;
    }

    public boolean isDetached() {
        return detached;
    }

    public void setDetached(boolean detached) {
        this.detached = detached;
    }

    public Object getDetachedFrame() {
        return detachedFrame;
    }

    public void setDetachedFrame(Object detachedFrame) {
        this.detachedFrame = detachedFrame;
    }

    public void setTickerType() {
        addComponentListener(this);
    }

    public ViewSetting getViewSetting() {
       /* if (oSettings == null) {
            oSettings = g_oTable1.getModel().getViewSettings();
        } else {
            return oSettings;
        }*/
        return oSettings;
    }

    /*public InternalFrame(StaticPriceDepthStore oStore, String sThreadID,
WindowDaemon oDaemon, Table oTable, Table oTable2)
{
g_oStaticPriceDepthStore = oStore;
g_sThreadID         = sThreadID;
g_oTable1           = oTable;
g_oTable2           = oTable2;
g_oDaemon           = oDaemon;
this.addInternalFrameListener(this);
dropTarget = new DropTarget (this, this);
createTitlebarManu();
        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif")));
    }*/

    /*public InternalFrame(StaticDepthStore oStore, String sThreadID,
WindowDaemon oDaemon, Table oTable, Table oTable2)
{
g_oStaticDepthStore = oStore;
g_sThreadID         = sThreadID;
g_oTable1           = oTable;
g_oTable2           = oTable2;
g_oDaemon           = oDaemon;
this.addInternalFrameListener(this);
dropTarget = new DropTarget (this, this);
createTitlebarManu();
        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif")));
    }*/

    public void setViewSetting(ViewSetting oSettings) {
        this.oSettings = oSettings;
        try {
            this.oSettings.setLayer(layer);
//            this.oSettings.setLayer(this.getLayer());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void attach() {
        ((DetachableRootPane) ((JFrame) detachedFrame).getRootPane()).detach();
    }

    public void addTable(String id, Table table) {
        tables.put(id, table);
    }

//    public InternalFrame(DQTable oTable,WindowDaemon oDaemon)
//    {
//        g_oDQTable  = oTable;
//        g_oDaemon    = oDaemon;
//        this.getContentPane().setLayout(new BorderLayout());
//        this.getContentPane().add(oTable,BorderLayout.CENTER);
//        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        this.addInternalFrameListener(this);
//        dropTarget = new DropTarget (this, this);
//        createTitlebarManu();
//		this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif")));
//        //Container np =  (Container)((BasicInternalFrameUI)getUI()).getNorthPane();
//        //np.add(new JButton("hi"));
//        //this.pack();
//        //com.isi.util.ObjectWatcher.Register(this);
//        //((BasicInternalFrameUI)getUI()).setNorthPane(null);
//    }

    public Table getTable(String id) {
        return tables.get(id);
    }

    public void setTable(Table table) {
        g_oTable1 = table;
        createServicesMenu();
        createLinkGroupMenu();
    }

    private void createTitlebarManu() {
        titleBarMenu = new JPopupMenu();
        TWMenuItem hideTitlemenu = new TWMenuItem(Language.getString("HIDE_TITLEBAR"), "hidetitle.gif");
        hideTitlemenu.setActionCommand("HIDE");
        hideTitlemenu.addActionListener(this);
        titleBarMenu.add(hideTitlemenu);
        GUISettings.applyOrientation(titleBarMenu);
        addTitlebarManu();

    }

    private void createServicesMenu() {
        servicesMenu = new JPopupMenu();
        linkTosideBar = new TWMenuItem(Language.getString("LINK_TO_SIDEBAR"), "specialorderbook.gif");
        linkTosideBar.setEnabled(false);
//        if(oSettings.getSubType()==ViewSettingsManager.OUTER_CHART){
//
//        }
        TWMenuItem timeanssalesSummary = null;
        TWMenuItem timeandsales;

        try {
            TWMenuItem mnuGraph = new TWMenuItem(Language.getString("GRAPH"), "graph.gif");
            servicesMenu.add(mnuGraph);
            mnuGraph.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    //ShowGraph_Selected();
                    Client.getInstance().showChart(oSettings.getID(), null, null);
                }
            });

        } catch (Exception e) {

        }
        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_SymbolTimeAndSales)) {
                    TWMenuItem item = new TWMenuItem(Language.getString("TIME_AND_SALES"), "time&sales.gif");
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Client.getInstance().mnu_TimeNSalesSymbol(oSettings.getID(), Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
                        }
                    });
                    if (getWindowType() != ViewSettingsManager.TIMEnSALES_VIEW2)
                        servicesMenu.add(item);
                }
            }
        } catch (Exception e) {
        }

        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_TimeAndSales_Summary)) {
                    TWMenuItem item = new TWMenuItem(Language.getString("TRADE_SUMMARY"), "time&salessum.gif");
                    timeanssalesSummary = item;

                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Client.getInstance().mnu_TimeNSalesSummaryForSymbol(oSettings.getID(), false, false, LinkStore.LINK_NONE);
                        }
                    });
                    if (getWindowType() != ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW)
                        servicesMenu.add(item);
                }
            }
        } catch (Exception e) {
        }

        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                TWMenuItem item = new TWMenuItem(Language.getString("DETAIL_QUOTE"), "detailquote.gif");
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Client.getInstance().showDetailQuote(oSettings.getID(), true);
                        DataStore.getSharedInstance().addSymbolRequest(oSettings.getID());
                    }
                });
                if (getWindowType() != ViewSettingsManager.DETAIL_QUOTE)
                    servicesMenu.add(item);
            }
        } catch (Exception e) {
        }

        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_SnapQuote)) {
                    TWMenuItem item = new TWMenuItem(Language.getString("SNAP_QUOTE"), "snapquote.gif");
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Client.getInstance().showSnapQuote(oSettings.getID(), true, false, false, LinkStore.LINK_NONE);
                            DataStore.getSharedInstance().addSymbolRequest(oSettings.getID());
                        }
                    });
                    if (getWindowType() != ViewSettingsManager.SNAP_QUOTE)
                        servicesMenu.add(item);
                }
            }
        } catch (Exception e) {
        }

        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_MarketDepthByPrice)) {
                    TWMenuItem item = new TWMenuItem(Language.getString("MARKET_DEPTH_BY_PRICE"), "mktby_price.gif");
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Client.getInstance().depthByPrice(oSettings.getID(), false, Constants.MAINBOARD_TYPE);
                        }
                    });
                    if ((getWindowType() != ViewSettingsManager.COMBINED_DEPTH_VIEW) && (getWindowType() != ViewSettingsManager.DEPTH_PRICE_BID_VIEW))
                        servicesMenu.add(item);
                }
            }
        } catch (Exception e) {
        }

        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_SpecialOrderBook)) {
                    TWMenuItem item = new TWMenuItem(Language.getString("SPECIAL_ORDER_BOOK"), "specialorderbook.gif");
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {

                            if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_SpecialOrderBook)) {
                                Client.getInstance().depthSpecialByOrder(null, false, false, LinkStore.LINK_NONE);

                            } else {
                                String message = Language.getString("FEATURE_NOT_AVAILABLE");
                                message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(oSettings.getID())).getDescription());
                                SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                            }
//                            Client.getInstance().depthSpecialByOrder(oSettings.getID(), false, false, LinkStore.LINK_NONE);
                        }
                    });
                    if ((getWindowType() != ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW))
                        servicesMenu.add(item);
                }
            }
        } catch (Exception e) {
        }

        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_MarketDepthByOrder)) {
                    TWMenuItem item = new TWMenuItem(Language.getString("MARKET_DEPTH_BY_ORDER"), "mktby_order.gif");
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Client.getInstance().depthByOrder(oSettings.getID(), false, false, LinkStore.LINK_NONE);
                        }
                    });
                    if (getWindowType() != ViewSettingsManager.DEPTH_BID_VIEW)
                        servicesMenu.add(item);
                }
            }
        } catch (Exception e) {
        }

        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_DepthCalculator)) {
                    TWMenuItem item = new TWMenuItem(Language.getString("PRICE_CALC"), "ordercalc.gif");
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Client.getInstance().mnu_MDepth_Calculator(oSettings.getID(), TWTypes.TradeSides.BUY, false, false, LinkStore.LINK_NONE);
                        }
                    });
                    if (getWindowType() != ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW)
                        servicesMenu.add(item);
                }
            }
        } catch (Exception e) {
        }
        ////////////////////////////////////////////////

        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_CashFlow)) {
                    TWMenuItem mnuCashFQ = new TWMenuItem(Language.getString("CASHFLOW_DQ"), "cashflowdetailquote.gif");
                    mnuCashFQ.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Client.getInstance().showCashFlowDetailQuote(oSettings.getID(), false, false, false, LinkStore.LINK_NONE);
                        }
                    });
                    if (getWindowType() != ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) {
                        servicesMenu.add(mnuCashFQ);
                    }
                }
            }
//         mnuCashFQ.setVisible(Client.getInstance().isValidInfoType(getSelectedSymbol(), Meta.IT_CashFlowDQ, true));
        } catch (Exception e) {

        }
        ////////////////////////////////////////////////
        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_Alerts)) {
                    TWMenuItem item = new TWMenuItem(Language.getString("SET_ALERT"), "alert.gif");
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Client.getInstance().mnu_AlertSetup(oSettings.getID());
                        }
                    });
                    servicesMenu.add(item);
                }
            }
        } catch (Exception e) {
        }
        try {
            if (SharedMethods.checkInstrumentType(oSettings.getID(), Meta.INSTRUMENT_QUOTE)) {
                final InternalFrame frame = this;
                //    if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_SpecialOrderBook)) {
                TWMenuItem item = new TWMenuItem(Language.getString("LINK_TO_SIDEBAR"), "specialorderbook.gif");
                linkTosideBar.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        LinkStore.getSharedInstance().LinkToSidebar(frame);
                    }
                });
                //  if ((getWindowType() != ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW))
                // servicesMenu.add(linkTosideBar);
                linkTosideBar.setEnabled(LinkStore.getSharedInstance().canLinkToSidebar(getViewSetting().getSubType()));
            }
        } catch (Exception e) {
        }


        final TWMenuItem timeanSalesSummary = timeanssalesSummary;

        servicesMenu.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                Exchange ex = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(oSettings.getID()));
                if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_TimeAndSales_Summary)) {
                    if (ex != null && (ex.getMarketStatus() == Meta.MARKET_CLOSE || ex.getMarketStatus() == Meta.MARKET_PRECLOSE)) {
                        timeanSalesSummary.setVisible(SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_TimeAndSales_Summary));
                    } else {
                        timeanSalesSummary.setVisible(SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(oSettings.getID()), Meta.IT_TimeAndSales_Summary));
                        timeanSalesSummary.setEnabled(false);
                    }
                }
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        GUISettings.applyOrientation(servicesMenu);
    }

    private void createLinkGroupMenu() {
        linkGroupMenu = new JPopupMenu();

        redmenu = new TWMenuItem(Language.getString("RED"), "lnk_red.gif");
        redmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                linkGroupSelected(LinkStore.LINK_RED);
                setLinkedGroupID(LinkStore.LINK_RED);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_RED);


            }
        });
        linkGroupMenu.add(redmenu);

        greenmenu = new TWMenuItem(Language.getString("GREEN"), "lnk_green.gif");
        greenmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                linkGroupSelected(LinkStore.LINK_GREEN);
                setLinkedGroupID(LinkStore.LINK_GREEN);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_GREEN);

            }
        });
        linkGroupMenu.add(greenmenu);

        bluemenu = new TWMenuItem(Language.getString("BLUE"), "lnk_blue.gif");
        bluemenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                linkGroupSelected(LinkStore.LINK_BLUE);
                setLinkedGroupID(LinkStore.LINK_BLUE);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_BLUE);

            }
        });
        linkGroupMenu.add(bluemenu);

        yellowmenu = new TWMenuItem(Language.getString("YELLOW"), "lnk_yellow.gif");
        yellowmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                linkGroupSelected(LinkStore.LINK_YELLOW);
                setLinkedGroupID(LinkStore.LINK_YELLOW);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_YELLOW);

            }
        });
        linkGroupMenu.add(yellowmenu);

        whitemenu = new TWMenuItem(Language.getString("WHITE"), "lnk_white.gif");
        whitemenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                linkGroupSelected(LinkStore.LINK_WHITE);
                setLinkedGroupID(LinkStore.LINK_WHITE);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_WHITE);

            }
        });
        linkGroupMenu.add(whitemenu);

        notlinkedmenu = new TWMenuItem(Language.getString("NOT_LINK"), "lnk_notlink.gif");
        notlinkedmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                linkGroupSelected(LinkStore.LINK_NONE);
                setLinkedGroupID(LinkStore.LINK_NONE);
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_NONE);

            }
        });
        linkGroupMenu.add(notlinkedmenu);

        linkGroupMenu.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                if (oSettings != null) {
                    redmenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_RED));
                    greenmenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_GREEN));
                    bluemenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_BLUE));
                    yellowmenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_YELLOW));
                    whitemenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_WHITE));
                }
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        GUISettings.applyOrientation(linkGroupMenu);
    }

    private void linkGroupSelected(String newgroup) {
        String oldGroup = LinkStore.getSharedInstance().getOldGroupID(linkedGroupID);
        this.linkedGroupID = newgroup;

        LinkStore.getSharedInstance().addFrameTolinkedGroup(this, oldGroup);


    }

    /*public void setBuddyFrame(InternalFrame buddyFrame){
               this.buddyFrame = buddyFrame;
            this.addComponentListener(this);
            this.buddyFrame.setTitleVisible(false);
            this.buddyFrame.setSize(new Dimension((int)this.getSize().getWidth(),(int)this.buddyFrame.getSize().getHeight()));
    }*/

    public void updateLinkGroupIconInTitleBar() {
        if (isLinkGroupEnabled()) {
            try {
                ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(linkedGroupID);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void validateLinkToSidebar() {

    }

    public void updateUI() {
        super.updateUI();
        try {
            addTitlebarManu();
            toggleTitleBar();
            if (titleBarMenu != null) {
                SwingUtilities.updateComponentTreeUI(titleBarMenu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addTitlebarManu() {
        Component np = (Component) ((BasicInternalFrameUI) getUI()).getNorthPane();
        try {
            np.removeMouseListener(this); // if existing
        } catch (Exception e) {
        }
        np.addMouseListener(this);
    }

    public void setWindowID(String id) {
        windowID = id;
    }

    public String setWindowID() {
        return windowID;
    }

//    public void setMenuItem(Menus oMenus)
//    {
//        g_oMenus    = oMenus;
//        g_oMenuItem = oMenus.addWindow(this);
//    }

    public void applySettings() {
        //ViewSetting oSettings = null;
        if (g_oTable1 != null) {
            oSettings = g_oTable1.getModel().getViewSettings();
            this.setSize(oSettings.getSize());
            g_oTable1.getModel().applySettings();
        } else if (oSettings != null) {
            this.setSize(oSettings.getSize());
        }

        if (oSettings != null) {
            this.setLocation(oSettings.getLocation());
        }

//        System.out.println("==="+oSettings.getID());

        try {
            if (oSettings == null) {
                if (Settings.isPutAllToSameLayer() && (layer != GUISettings.INTERNAL_DIALOG_LAYER)) {
                    this.setLayer(GUISettings.DEFAULT_LAYER);
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                }

            } else if (oSettings.isVisible()) {
                if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSettings.getIndex());
                } else {
                    this.getDesktopPane().setLayer(this, oSettings.getLayer(), oSettings.getIndex());
                }
                this.setVisible(true);
                try {
                    this.setSelected(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                this.setVisible(false);
                if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                } else {
                    this.getDesktopPane().setLayer(this, oSettings.getLayer(), 0);
                }
//                this.getDesktopPane().setLayer(this, this.getLayer(), 0);
            }
        } catch (Exception e) {
//            e.printStackTrace();
            // ticker may come to this catch since it may not be in a desktop when it is fixed
            this.setVisible(oSettings.isVisible());
        }

        try {
            switch (oSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    if (oSettings != null) {
                        this.setSize(oSettings.getSize());
                        this.setLocation(oSettings.getLocation());
                    }
                    this.setIcon(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (g_oTable1 != null) {
            g_oTable1.updateGUI();
        }

        if (splitPane != null) {
            splitPane.setDividerLocation(oSettings.getSplitLocation());
        }

        try {
            if (!oSettings.isHeaderVisible()) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                this.revalidate();
            } else {
                updateUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        createServicesMenu();
        createLinkGroupMenu();

    }

//    public void setSize(Dimension size){
//        super.setSize(size);
//        SharedMethods.printLine("%%%%%%%%%% " + getTitle() +" " + size.toString(),false );
//    }
//
//    public void setSize(int w, int h){
//        super.setSize(w,h);
//        SharedMethods.printLine("%%%%%%%%%% "  + getTitle() +" " + w + " "+ h,true );
//    }

    public void toggleTitleBar() {
        if (oSettings != null) {
            if (!oSettings.isHeaderVisible()) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                this.revalidate();
            }
        }
    }

    public int getTitlebarHeight() {
        return (int) ((BasicInternalFrameUI) getUI()).getNorthPane().getSize().getHeight();
    }

    public void windowClosing() {

    }

    public void setTitleVisible(boolean setVisible, boolean resizeFrame) {
        if (setVisible) {
            if (oSettings != null) {
                oSettings.setHeaderVisible(true);
            }
            /*if (g_oTable1 != null) {
                g_oTable1.getModel().getViewSettings().setHeaderVisible(true);
            }*/
            if (g_oTable2 != null) {
                for (Table oTable : g_oTable2) {
                    oTable.getModel().getViewSettings().setHeaderVisible(true);
                }
//                    g_oTable2.getModel().getViewSettings().setHeaderVisible(true);
            }
            if (resizeFrame) {
                setSize(getWidth(), getHeight() + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT);
            }
            updateUI();
        } else {
            Component titleBar = ((BasicInternalFrameUI) getUI()).getNorthPane();
            if (titleBar != null) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                revalidate();
                if (oSettings != null) {
                    oSettings.setHeaderVisible(false);
                }
                /*if (g_oTable1 != null) {
                    g_oTable1.getModel().getViewSettings().setHeaderVisible(true);
                }*/
                if (g_oTable2 != null) {
                    for (Table oTable : g_oTable2) {
                        oTable.getModel().getViewSettings().setHeaderVisible(false);
                    }
//                    g_oTable2.getModel().getViewSettings().setHeaderVisible(false);
                }
                if (resizeFrame) {
                    setSize(getWidth(), getHeight() - GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT);
                }
            }
        }
    }

    public boolean isTitleVisible() {
        if (oSettings != null) {
            return oSettings.isHeaderVisible();
        } else {
            return true;
        }
    }

    public void setTitleVisible(boolean setVisible) {
        setTitleVisible(setVisible, isResizeWindowOnTitleToggle());
    }

    public void show() {
        if (isDetached()) {
            JFrame frame = (JFrame) detachedFrame;
            if (frame.getExtendedState() == JFrame.ICONIFIED) {
                frame.setExtendedState(JFrame.NORMAL);
            }
            frame.toFront();
            return;
        }
//        System.out.println("Frame Location = "+Client.getInstance().getFrame().getBounds().getMaxX()+ " , "+Client.getInstance().getFrame().getBounds().getMaxY());
//        System.out.println("This Location = "+this.getBounds().getMaxX()+" , "+this.getBounds().getMaxY());
        if (this.getBounds().getMaxX() > Client.getInstance().getFrame().getBounds().getMaxX()) {
            this.setLocationRelativeTo(Client.getInstance().getDesktop());
        }
        super.show();
        this.setVisible(true);
        toggleTitleBar();
    }

    /*public Table getTable1()
    {
        return g_oTable1;
    }

    public Table getTable2()
    {
        return g_oTable2;
    }*/

    public boolean finalizeWindow() {
        if (getDefaultCloseOperation() == HIDE_ON_CLOSE) {
            if (getWindowType() == ViewSettingsManager.MARKET_TIMEnSALES_VIEW) {
//                RequestManager.getSharedInstance().sendRemoveRequest(dataRequestID[0]);
                RequestManager.getSharedInstance().removeRequest(dataRequestID[0]);
            } else if (getWindowType() == Meta.WT_Ticker) {
                if (Settings.isTradeTickerMode()) {
                    if (!Settings.isShowSummaryTicker()) {
//                        RequestManager.getSharedInstance().sendRemoveRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
                        RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
                    }
                }
            }
            return false;
        }
        removeFromLinkStore();
        if (getWindowType() == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
            MDepthCalculator mDepthCalculator = (MDepthCalculator) getSupportingObject(0);
            mDepthCalculator.finalizeWinow();
            mDepthCalculator = null;
        } else if (getWindowType() == ViewSettingsManager.TIMEnSALES_VIEW2) {
            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
            }
        } else if (getWindowType() == ViewSettingsManager.DETAIL_QUOTE) {
            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
            }
        } else if (getWindowType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) {
            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
            }
        } else if (getWindowType() == ViewSettingsManager.SNAP_QUOTE) {
            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
            }
        } else if (getWindowType() == ViewSettingsManager.REGIONAL_QUOTE_VIEW) {
            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID(), Meta.MESSAGE_TYPE_REGIONAL_QUOTE);
            }
            /*Stock stock = DataStore.getSharedInstance().getStockObject(oSettings.getID());
            if(stock != null) {
                SendQFactory.addRemoveRequest(Constants.PATH_SECONDARY, stock.getExchange(), stock.getSymbol(),
                                              Meta.MESSAGE_TYPE_REGIONAL_QUOTE, stock.getInstrumentType(), stock.getMarketCenter());
            } else {
                SendQFactory.addRemoveRequest(Constants.PATH_SECONDARY,SharedMethods.getExchangeFromKey(oSettings.getID()),
                                                SharedMethods.getSymbolFromKey(oSettings.getID()),Meta.MESSAGE_TYPE_REGIONAL_QUOTE,
                                                DataStore.getSharedInstance().getStockObject(oSettings.getID()).getInstrumentType(), null);
            }*/
        } else if ((getWindowType() == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) || (getWindowType() == ViewSettingsManager.DEPTH_BID_VIEW)
                || (getWindowType() == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) || (getWindowType() == ViewSettingsManager.DEPTH_SPECIAL_BID_VIEW)
                || (getWindowType() == ViewSettingsManager.COMBINED_DEPTH_VIEW)) { //Bug ID <#0015>
//            if(get)
            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
            }
        }

        // highpriority add remove request to mkt depth and trade dialog
        if (g_oTable1 != null) {
            g_oTable1.killThread();
            g_oTable1.getModel().invalidateTable();
            g_oTable1 = null;
        }
        if (g_oTable2 != null) {
            for (Table oTable : g_oTable2) {
                oTable.killThread();
                oTable.getModel().invalidateTable();
            }
//            g_oTable2.killThread();
//            g_oTable2.getModel().invalidateTable();
            g_oTable2 = null;
        }

        if (g_oThreadGroup != null) {
            Thread[] aoList = new Thread[g_oThreadGroup.activeCount()];
            g_oThreadGroup.enumerate(aoList);
            for (int i = 0; i < aoList.length; i++) {

                if (aoList[i].getName().equals(g_sThreadID)) {
                    aoList[i] = null;

                    break;
                }
            }
            aoList = null;
        }

        if (g_oMenuItem != null) {
            g_oMenus.removeFormMenu(g_oMenuItem);
        }
        if (dataRequestID.length > 0) {
            for (int i = 0; i < dataRequestID.length; i++) {
                RequestManager.getSharedInstance().removeRequest(dataRequestID[i]);
                if (dataStore[i] != null)
                    dataStore[i].clear(symbol);
            }

        }
        setInactive();
        try {
            g_oDaemon.notifyThread();

        } catch (Exception e) {
            //e.printStackTrace();
        }


        return true;
    }

    public void removeFromLinkStore() {
        try {
            if (!(getDefaultCloseOperation() == HIDE_ON_CLOSE) && oSettings.getProperty(ViewConstants.VC_LINKED) != null
                    && ((String) oSettings.getProperty(ViewConstants.VC_LINKED)).equalsIgnoreCase("true")) {
                LinkStore.getSharedInstance().removeLinkedWindow(oSettings.getSubType());
                LinkStore.getSharedInstance().removeFromHashTable(getLinkedGroupID(), this);
                LinkStore.getSharedInstance().removeLinkedWindowFromRegister(oSettings.getSubType(), getLinkedGroupID(), this);
            }
        } catch (Exception e) {
        }
    }

    private void setInactive() {
        g_bActive = false;
    }

    public boolean isActive() {
        return g_bActive;
    }

    /*public Point getLocation()
    {
        return this.getLocation();
    }

    public Dimension getSize()
    {
        return this.getSize();
    }*/

    public void setOrientation() {
        if (Language.isLTR())
            GUISettings.applyOrientation(this, ComponentOrientation.LEFT_TO_RIGHT);
        else
            GUISettings.applyOrientation(this, ComponentOrientation.RIGHT_TO_LEFT);
    }

    public String toString() {
        if (this instanceof ForexBoard) {
            String[] arr = this.getTitle().split("-");
            return arr[1];
        } else if (this instanceof SectorOverviewUI) {
            return Language.getString("SECTOR_OVERVIEW");
//            return SectorOverviewUI.getSharedInstance().gettingTitle();
        }
        return g_oTable1.getModel().getViewSettings().getCaption();
        // this.getTitle();
    }

    /* Table wrapper methods */

    public boolean isWindowVisible() {
        if (isDetached()) {
            JFrame frame = (JFrame) detachedFrame;
            return true;
        }
        return isVisible();
    }

    public int getWindowStyle() {
//        if (g_oDQTable!=null){
//            if (g_oDQTable.isMaximim())
//                return  ViewSettingsManager.STYLE_MAXIMIZED;
//            else if (isIcon())
//                return ViewSettingsManager.STYLE_ICONIFIED;
//            else
//                return ViewSettingsManager.STYLE_NORMAL;
//        }else{
        if (isMaximum())
            return ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
//        }
    }

    public JTable getTable1() {
        try {
            return g_oTable1.getTable();
        } catch (Exception e) {
            return null;
        }
    }

    public Table getTableComponent() {
        return g_oTable1;
    }

    public Table getSubTableComponent() {
        if (g_oTable2 != null) {
            return g_oTable2[0];
        } else {
            return null;
        }
    }

    public CommonTable getModel1() {
        try {
            return g_oTable1.getModel();
        } catch (Exception e) {
            return null;
        }
    }

    public CommonTable getModel2() {
        try {
            return g_oTable2[0].getModel();
        } catch (Exception e) {
            return null;
        }
    }

    public CommonTable getModelFor(String id) {
        try {
            return tables.get(id).getModel();
        } catch (Exception e) {
            return null;
        }
    }

    public JTable getTable2() {
        try {
            return g_oTable2[0].getTable();
        } catch (Exception e) {
            return null;
        }
    }

    public void setTable2(Table[] table) {
        g_oTable2 = table;
    }

    public JTable getTabFoler(String id) {
        try {
            return tables.get(id).getTable();
        } catch (Exception e) {
            return null;
        }
    }

    public JTable[] getTablesArray() {
        JTable[] tables = new JTable[g_oTable2.length];
        for (int i = 0; i < g_oTable2.length; i++) {
            tables[i] = g_oTable2[i].getTable();
        }
        return tables;
    }

    public int getZOrder() {
        try {
            return getDesktopPane().getPosition(this);
        } catch (Exception e) {
            return 0; // what if there is not desktop pane
        }
    }

    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent event) {

        /* Check if the drag came form prit button */
        try {
            String key = (String) event.getTransferable().getTransferData(DataFlavor.stringFlavor);
            if (key.equals("Print")) {
                event.getDropTargetContext().dropComplete(true);
            } else {
                event.rejectDrop(); // not from the print button. reject drop
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            event.rejectDrop();
            return;
        }
        printTable();
    }

    public void applyTheme() {
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
    }

    public void printTable() {
        PrintPreview p = null;

        CommonTableRenderer.reloadForPrinting();

        if (getTable1() != null) {
            if ((oSettings.getSubType() == ViewSettingsManager.DETAIL_QUOTE) ||
                    (oSettings.getSubType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) ||
                    (oSettings.getSubType() == ViewSettingsManager.DETAIL_QUOTE_MFUND)) {
                DQTableRenderer.reloadForPrinting();
            } else
                createColMaxWidths(getTable1());
            getTable1().setGridColor(Color.white);
            getTable1().updateUI();
        }
        if (getTable2() != null) {
            createColMaxWidths(getTable2());
            MarketAskDepthRenderer.reloadForPrinting();
            getTable2().setGridColor(Color.white);
            getTable2().updateUI();
        }
        p = new PrintPreview(this, "Preview");

        if ((oSettings.getSubType() == ViewSettingsManager.DETAIL_QUOTE) ||
                (oSettings.getSubType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) ||
                (oSettings.getSubType() == ViewSettingsManager.DETAIL_QUOTE_MFUND))
            DQTableRenderer.reload();
        else
            CommonTableRenderer.reload();

        if (getTable1() != null) {
            getTable1().setGridColor(Theme.getColor("BOARD_TABLE_GRIDCOLOR"));
            getTable1().updateUI();

        }
        if (getTable2() != null) {
            MarketAskDepthRenderer.reload();
            getTable2().setGridColor(Theme.getColor("BOARD_TABLE_GRIDCOLOR"));
            getTable2().updateUI();
        }
        this.updateUI();
        p = null;
    }

    /* Internal frame listener */ //todo
    public void internalFrameOpened(InternalFrameEvent e) {
        /*if(TradeMethods.getSharedInstance().getIsAccountFrameCreated()  && TradeMethods.getSharedInstance().getAccountWindow().isShowing()){
            System.out.println("frame opened");
            TradeMethods.getSharedInstance().getAccountWindow().resizeRowhight();


        }*/

    }

    public void internalFrameClosing(InternalFrameEvent e) {
        try {
            if (this.isDetached()) {
                ((JFrame) getDetachedFrame()).dispose();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        finalizeWindow();
        focusNextWindow();
    }

    /* Printable method */
    public void internalFrameClosed(InternalFrameEvent e) {
        try {
            if (this.isDetached()) {
                ((JFrame) getDetachedFrame()).dispose();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void internalFrameIconified(InternalFrameEvent e) {
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    public void internalFrameActivated(InternalFrameEvent e) {
    }

    public void internalFrameDeactivated(InternalFrameEvent e) {
        try {
            if (g_oTable1.getTable().isEditing()) {
                g_oTable1.getTable().getCellEditor().cancelCellEditing();
            }
        } catch (Exception ex) {
        }
    }

    public int print(Graphics g, PageFormat pageFormat,
                     int pageIndex) throws PrinterException {

        if (getTable2() != null)
            return printDepth(g, pageFormat, pageIndex);
        else {
            if ((oSettings.getSubType() == ViewSettingsManager.DETAIL_QUOTE) ||
                    (oSettings.getSubType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) ||
                    (oSettings.getSubType() == ViewSettingsManager.DETAIL_QUOTE_MFUND)) {
                return printDetailQuote(g, pageFormat, pageIndex);
            } else if (oSettings.getMainType() == ViewSettingsManager.CHART_VIEW) {
                return printTable2(g, pageFormat, pageIndex);
            } else
                return printTable1(g, pageFormat, pageIndex);
        }
    }

    public void printContents() {
        try {
            String title = getTitle();
            int type;

            JTable table1 = getTable1();
            JTable table2 = getTable2();

            if ((g_oTable1.getWindowType() == ViewSettingsManager.DETAIL_QUOTE) ||
                    (oSettings.getSubType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) ||
                    (g_oTable1.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_MFUND))
                type = PrintManager.TABLE_TYPE_DEFAULT_NO_HEAD;
            else if (g_oTable1.getModel().getViewSettings().getID().equals("HISTORY_ANALYZER"))
                type = PrintManager.TABLE_TYPE_WATCHLIST; // ignore last col
            else if ((g_oTable1.getWindowType() == ViewSettingsManager.SNAP_QUOTE) ||
                    (g_oTable1.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW))
                type = PrintManager.TABLE_TYPE_SNAP_QUOTE;
            else if (g_oTable1.getWindowType() == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW)
                type = PrintManager.TABLE_TYPE_DEPTH_CALC;
            else
                type = PrintManager.TABLE_TYPE_DEFAULT;

            if (table2 != null) {
                Object[] tables = new Object[2];
                tables[0] = table1;
                tables[1] = table2;
                SharedMethods.printTable(tables, type, title);
                tables = null;
                table2 = null;
            } else {
                SharedMethods.printTable(getTable1(), type, title);
            }

            title = null;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void showServices() {
        if (showServicesMenu) {
            SwingUtilities.updateComponentTreeUI(servicesMenu);
            Point point = MouseInfo.getPointerInfo().getLocation();
            SwingUtilities.convertPointFromScreen(point, this);
            GUISettings.showPopup(servicesMenu, this, (int) point.getX(), (int) point.getY());
        }
    }

    public void showLinkGroup() {
        if (isLinkGroupsEnabled) {
            if (linkGroupMenu == null) {
                createLinkGroupMenu();
            }

            if ((this instanceof GraphFrame && !(((GraphFrame) this).isGraphDetached()))) {
                return;
            }
            SwingUtilities.updateComponentTreeUI(linkGroupMenu);
            Point point = MouseInfo.getPointerInfo().getLocation();
            SwingUtilities.convertPointFromScreen(point, this);
            GUISettings.showPopup(linkGroupMenu, this, (int) point.getX(), (int) point.getY());
        }
    }

    public String getLinkGroupIDForTitleBar() {
        return linkedGroupID;
    }

    public void setShowServicesMenu(boolean showServicesMenu) {
        this.showServicesMenu = showServicesMenu;
    }

    public boolean isServicesEnabled() {
        return showServicesMenu;
    }

    public boolean isLinkGroupEnabled() {
        return isLinkGroupsEnabled;
    }

    public void setLinkGroupsEnabled(boolean linkGroupsEnabled) {
        isLinkGroupsEnabled = linkGroupsEnabled;
    }

    public int printTable(Graphics g, PageFormat pageFormat,
                          int pageIndex) throws PrinterException {

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black);
        int fontHeight = g2.getFontMetrics().getHeight();
        int fontDesent = g2.getFontMetrics().getDescent();
        int titleGap = 10;
        JTable tableView = getTable1();

        //leave room for page number
        double pageHeight = pageFormat.getImageableHeight() - (fontHeight * 2) - titleGap;
        double pageWidth = pageFormat.getImageableWidth();
        double tableWidth = (double) tableView.getColumnModel().getTotalColumnWidth();
        double scale = 1;
        if (tableWidth >= pageWidth) {
            scale = pageWidth / tableWidth;
        }

        double headerHeightOnPage = tableView.getTableHeader().getHeight() * scale;
        double tableWidthOnPage = tableWidth * scale;

        double oneRowHeight = (tableView.getRowHeight()) * scale;
        int numRowsOnAPage = (int) ((pageHeight - headerHeightOnPage) / oneRowHeight);
        double pageHeightForTable = oneRowHeight * numRowsOnAPage;
        int totalNumPages = (int) Math.ceil(((double) tableView.getRowCount()) / numRowsOnAPage);
        if (pageIndex >= totalNumPages) {
            return NO_SUCH_PAGE;
        }
        g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        // draw title
        if (Language.isLTR()) {
            g2.drawString(getTitle(), 0, (int) (fontHeight - fontDesent));
        } else {
            FontMetrics fm = g2.getFontMetrics();
            int titleWidth = fm.charsWidth(getTitle().toCharArray(), 0, getTitle().length());
            g2.drawString(getTitle(), (int) (pageWidth - titleWidth), (int) (fontHeight - fontDesent));
        }

        // draw page number
        g2.drawString("Page: " + (pageIndex + 1), (int) pageWidth / 2 - 35, (int) (pageHeight
                + fontHeight + fontHeight - fontDesent));// two font heights. onr for header
        // one for footer
        if (Language.isLTR()) {
            g2.translate(0, headerHeightOnPage + fontHeight + titleGap);
        } else {
            g2.translate((int) (pageWidth - tableWidthOnPage), headerHeightOnPage + fontHeight + titleGap);
        }
        g2.translate(0f, -pageIndex * pageHeightForTable);

        //If this piece of the table is smaller
        //than the size available,
        //clip to the appropriate bounds.
        if (pageIndex + 1 == totalNumPages) {
            int lastRowPrinted = numRowsOnAPage * pageIndex;
            int numRowsLeft = tableView.getRowCount() - lastRowPrinted;
            g2.setClip(0,
                    (int) (pageHeightForTable * pageIndex),
                    (int) Math.ceil(tableWidthOnPage),
                    (int) Math.ceil(oneRowHeight *
                            numRowsLeft));
        }
        //else clip to the entire area available.
        else {
            g2.setClip(0,
                    (int) ((pageHeightForTable * pageIndex)),
                    (int) Math.ceil(tableWidthOnPage),
                    (int) Math.ceil(pageHeightForTable));
        }

        g2.scale(scale, scale);
        tableView.paint(g2);
        g2.scale(1 / scale, 1 / scale);
        g2.translate(0f, pageIndex * pageHeightForTable);
        g2.translate(0f, -headerHeightOnPage);
        g2.setClip(0, 0,
                (int) Math.ceil(tableWidthOnPage),
                (int) Math.ceil(headerHeightOnPage));
        g2.scale(scale, scale);
        tableView.getTableHeader().paint(g2);
        //paint header at top

        return Printable.PAGE_EXISTS;
    }

    public int printDepth(Graphics g, PageFormat pageFormat,
                          int pageIndex) throws PrinterException {

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black);
        int fontHeight = g2.getFontMetrics().getHeight();
        int fontDesent = g2.getFontMetrics().getDescent();
        int titleGap = 10;
        int tableGap = 2;
        JTable tableView1 = getTable1();
        JTable tableView2 = getTable2();
        //System.out.println(tableView == null);

        //leave room for page number
        double pageHeight = pageFormat.getImageableHeight() - (fontHeight * 2)
                - titleGap;
        double pageWidth = pageFormat.getImageableWidth();
        double tableWidth1 = (double) tableView1.getColumnModel().getTotalColumnWidth();
        double tableWidth2 = (double) tableView2.getColumnModel().getTotalColumnWidth();
        //System.out.println(tableWidth);
        double scale = 1;
        if ((tableWidth1 + tableWidth2) >= pageWidth) {
            scale = pageWidth / (tableWidth1 + tableWidth2);
        }

        double headerHeightOnPage = tableView1.getTableHeader().getHeight() * scale;
        double tableWidthOnPage = tableGap + (tableWidth1 + tableWidth2) * scale;

        double oneRowHeight1 = (tableView1.getRowHeight()) * scale;
        double oneRowHeight2 = (tableView2.getRowHeight()) * scale;
        int numRowsOnAPage1 = (int) ((pageHeight - headerHeightOnPage) / oneRowHeight1);
        int numRowsOnAPage2 = (int) ((pageHeight - headerHeightOnPage) / oneRowHeight2);
        double pageHeightForTable1 = oneRowHeight1 * numRowsOnAPage1;
        double pageHeightForTable2 = oneRowHeight2 * numRowsOnAPage2;
        int totalNumPages1 = (int) Math.ceil((double) tableView1.getRowCount() / numRowsOnAPage1);
        int totalNumPages2 = (int) Math.ceil((double) tableView2.getRowCount() / numRowsOnAPage2);
        int totalNumPages = Math.max(totalNumPages1, totalNumPages2);
        if (pageIndex >= totalNumPages) {
            return NO_SUCH_PAGE;
        }
        g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        // draw title
        if (Language.isLTR()) {
            g2.drawString(getTitle(), 0, (int) (fontHeight - fontDesent));
        } else {
            FontMetrics fm = g2.getFontMetrics();
            int titleWidth = fm.charsWidth(getTitle().toCharArray(), 0,
                    getTitle().length());
            g2.drawString(getTitle(), (int) (pageWidth - titleWidth),
                    (int) (fontHeight - fontDesent));
        }

        // draw page number
        g2.drawString("Page " + (pageIndex + 1), (int) pageWidth / 2 - 35, (int) (pageHeight
                + fontHeight + fontHeight - fontDesent));// two font heights. onr for header
        // one for footer
        if (Language.isLTR()) {
            g2.translate(0, headerHeightOnPage + fontHeight + titleGap);
        } else {
            g2.translate((int) (pageWidth - tableWidthOnPage),
                    headerHeightOnPage + fontHeight + titleGap);
        }
        g2.translate(0f, -pageIndex * pageHeightForTable1);

        //If this piece of the table is smaller
        //than the size available,
        //clip to the appropriate bounds.
        boolean haveMoreRowsToPrint = false;
        int lastRowPrinted = numRowsOnAPage1 * pageIndex;
        int numRowsLeft = tableView1.getRowCount() - lastRowPrinted;

        haveMoreRowsToPrint = (numRowsLeft > 0);
        if (haveMoreRowsToPrint) {
            if (pageIndex + 1 == totalNumPages1) {
                g2.setClip(0,
                        (int) (pageHeightForTable1 * pageIndex),
                        (int) Math.ceil(tableWidthOnPage / 2),
                        (int) Math.ceil(oneRowHeight1 * numRowsLeft));
            }
            //else clip to the entire area available.
            else {
                g2.setClip(0,
                        (int) ((pageHeightForTable1 * pageIndex)),
                        (int) Math.ceil(tableWidthOnPage / 2),
                        (int) Math.ceil(pageHeightForTable1));
            }
            g2.scale(scale, scale);
            tableView1.paint(g2);
            g2.scale(1 / scale, 1 / scale);
            g2.translate(0f, pageIndex * pageHeightForTable1);
            g2.translate(0f, -headerHeightOnPage);
            g2.setClip(0, 0,
                    (int) Math.ceil(tableWidthOnPage / 2),
                    (int) Math.ceil(headerHeightOnPage));
            g2.scale(scale, scale);
            tableView1.getTableHeader().paint(g2);
            g2.scale(1 / scale, 1 / scale);
            // undo translate
            g2.translate(0f, headerHeightOnPage);
            g2.translate(0f, -pageIndex * pageHeightForTable1);
        }

        // print table 2
        haveMoreRowsToPrint = false;
        //g2.translate(0f,-pageIndex*pageHeightForTable);
        g2.translate(tableWidth1 * scale + tableGap, 0);
        lastRowPrinted = numRowsOnAPage2 * pageIndex;
        numRowsLeft = tableView2.getRowCount() - lastRowPrinted;
        haveMoreRowsToPrint = (numRowsLeft > 0);
        if (haveMoreRowsToPrint) {
            if (pageIndex + 1 == totalNumPages2) {
                g2.setClip(0,
                        (int) (pageHeightForTable2 * pageIndex),
                        (int) Math.ceil(tableWidthOnPage / 2),
                        (int) Math.ceil(oneRowHeight2 * numRowsLeft));
            }
            //else clip to the entire area available.
            else {
                g2.setClip(0,
                        (int) ((pageHeightForTable2 * pageIndex)),
                        (int) Math.ceil(tableWidthOnPage / 2),
                        (int) Math.ceil(pageHeightForTable2));
            }
            g2.scale(scale, scale);
            tableView2.paint(g2);
            g2.scale(1 / scale, 1 / scale);
            g2.translate(0f, pageIndex * pageHeightForTable2);
            g2.translate(0f, -headerHeightOnPage);
            g2.setClip(0, 0,
                    (int) Math.ceil(tableWidthOnPage / 2),
                    (int) Math.ceil(headerHeightOnPage));
            g2.scale(scale, scale);
            tableView2.getTableHeader().paint(g2);
        }
        //paint header at top

        return Printable.PAGE_EXISTS;
    }

    private int printDetailQuote(Graphics pg, PageFormat pageFormat,
                                 int pageIndex) throws PrinterException {
        String printTitle = null;

        if (pageIndex >= m_maxNumPage)
            return NO_SUCH_PAGE;

        // Calculate the initial details
        pg.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
        int wPage = 0;
        int hPage = 0;
        int noOfRows = 0;

        wPage = (int) pageFormat.getImageableWidth();
        hPage = (int) pageFormat.getImageableHeight(); // + 200;
        int columnWidth = wPage / 4;

        // Draw the Title and the header
        int y = 0;
        pg.setFont(new TWFont("Arial", Font.PLAIN, 10)); // table.getFont());
        pg.setColor(Color.black);
        Font fn = pg.getFont();
        FontMetrics fm = pg.getFontMetrics();
        FontMetrics fMetrics = pg.getFontMetrics(); //getTable1().getFontMetrics(table.getFont());

        pg.fillRect(0, 0, wPage, 1);              // Draws the top outer line
        y += fMetrics.getAscent() + 5;
        if (Language.isLTR())
            pg.drawString(getTitle(), 0, y);
        else
            pg.drawString(getTitle(), wPage - 50, y);

        printTitle = Language.getString("UNIQUOTES");
        if (Language.isLTR()) {
            //priority
            //printTitle += " : " + g_oDateFormat2.format(new Date(TradeStore.getLastTradedTime()));
            pg.drawString(printTitle, wPage - fm.stringWidth(printTitle), y);
        } else {
            //priority
            //printTitle = g_oDateFormat2.format(new Date(TradeStore.getLastTradedTime())) + " : " + printTitle;
            pg.drawString(printTitle, 10, y);
        }

        y += 15; // space between title and table headers

        pg.fillRect(0, y - 5, wPage, 2);          // Draws the 2 nd line

        pg.setFont(new TWFont("Arial", Font.PLAIN, 10)); // table.getFont());
        //Font headerFont = fn.deriveFont(Font.BOLD); //table.getFont().deriveFont(Font.BOLD);
        //pg.setFont(headerFont);
        fm = pg.getFontMetrics();

        int header = y;
        int h = fm.getAscent();
        y += h + 5; // add ascent of header font because of baseline positioning

        int strWidth = 0;
        String dataStr = null;
        if (Language.isLTR()) {
            dataStr = Language.getString("PRICE");
            strWidth = fm.stringWidth(dataStr);
            pg.drawString(dataStr, columnWidth * 2 - strWidth - 5, y);

            dataStr = Language.getString("CHANGE");
            strWidth = fm.stringWidth(dataStr);
            pg.drawString(dataStr, columnWidth * 3 - strWidth - 5, y);

            dataStr = Language.getString("QUANTITY");
            strWidth = fm.stringWidth(dataStr);
            pg.drawString(dataStr, columnWidth * 4 - strWidth - 5, y);
        } else {
            dataStr = Language.getString("PRICE");
            strWidth = fm.stringWidth(dataStr);
            pg.drawString(dataStr, columnWidth * 3 - strWidth, y);

            dataStr = Language.getString("CHANGE");
            strWidth = fm.stringWidth(dataStr);
            pg.drawString(dataStr, columnWidth * 2 - strWidth, y);

            dataStr = Language.getString("QUANTITY");
            strWidth = fm.stringWidth(dataStr);
            pg.drawString(dataStr, columnWidth * 1 - strWidth, y);
        }
        dataStr = null;
        y += fm.getDescent();

        // Draws the Seperator Line
        if (Language.isLTR())
            pg.drawLine(columnWidth, y, wPage, y);
        else
            pg.drawLine(0, y, columnWidth * 3, y);
        y += 3; //fm.getAscent();

        TableModel tblModel = getTable1().getModel();
        noOfRows = tblModel.getRowCount();
        h = fm.getHeight();
        int rowH = Math.max((int) (h * 1.2), 10);
        int rowPerPage = (hPage - header) / rowH - 1; // - 8;
        int iniRow = pageIndex * rowPerPage;
        int endRow = Math.min(getTable1().getRowCount(), iniRow + rowPerPage);
        int curRow = iniRow;
        //int strWidth    = 0;
        int colType = 0;
        JLabel label = null;
        //String  dataStr = null;
        boolean isEmptyColumn = false;
        boolean isHeaderRow = false;

        for (int row = 0; row < (endRow - iniRow); row++) {
            if ((row == 4)) { // || (row == 9)) {
                y += 3; //fm.getDescent();
                // Draws the Seperator Line
                if (Language.isLTR())
                    pg.drawLine(columnWidth, y, wPage, y);
                else
                    pg.drawLine(0, y, columnWidth * 3, y);
            } else if ((row == 3) || (row == 9)) {
                y += 3; //fm.getDescent();
                pg.fillRect(0, y, wPage, 2);          // Draws the 2 nd line
                y += 3;
            }

            y += fm.getAscent();
            //pg.drawString((String)tblModel.getValueAt(curRow, 0), 0*columnWidth, y)
            for (int curCol = 0; curCol < 4; curCol++) {
                if (curRow == 3) {
                    if ((curCol == 1) || (curCol == 2) || (curCol == 3))
                        isHeaderRow = true;
                    else
                        isHeaderRow = false;
                } else {
                    isHeaderRow = false;
                }
                if (isEmptyColumn) {
                    if (Language.isLTR())
                        pg.drawString("", curCol * columnWidth, y);
                    else
                        pg.drawString("", (6 - curCol) * columnWidth, y);
                } else {
                    label = (JLabel) getTable1().getCellRenderer(curRow,
                            curCol).getTableCellRendererComponent(getTable1(),
                            tblModel.getValueAt(curRow, curCol), false, false, curRow, curCol);
                    dataStr = label.getText();
                    if (label.getHorizontalAlignment() == JLabel.RIGHT) {
                        strWidth = fm.stringWidth(dataStr);
                        if (Language.isLTR())
                            pg.drawString(dataStr, ((curCol + 1) * columnWidth - 2 - strWidth), y);
                        else
                            pg.drawString(dataStr, (((6 - curCol) - 2) * columnWidth - 2 - strWidth), y);
                    } else {
                        if (isHeaderRow) {
                            strWidth = fm.stringWidth(dataStr);

                            if (Language.isLTR())
                                pg.drawString(dataStr, (curCol + 1) * columnWidth - strWidth - 5, y);
                            else
                                pg.drawString(dataStr, (6 - curCol) * columnWidth - strWidth, y);
                        } else {
                            if (Language.isLTR())
                                pg.drawString(dataStr, (curCol) * columnWidth + 2, y);
                            else
                                pg.drawString(dataStr, ((6 - curCol) - 1) * columnWidth + 2, y);
                        }

                        dataStr = null;
                        colType = 0;
                    }
                }
            }
            curRow++;
            y += fm.getDescent();
        }

        String str = null; //obj.toString();
        y += 3; //fm.getDescent();

        if (pageIndex == (m_maxNumPage - 1))
            pg.fillRect(0, y, wPage - 2, 2);

        // draw page number
        if (Language.isLTR())
            pg.drawString(Language.getString("PAGE") + " : " + (pageIndex + 1), (int) wPage / 2 - 35, (int) (hPage) - fm.getDescent());
        else
            pg.drawString((pageIndex + 1) + " : " + Language.getString("PAGE"), (int) wPage / 2 - 35, (int) (hPage) - fm.getDescent());

        System.gc();
        return PAGE_EXISTS;
    }

    private void createColMaxWidths(JTable table) {
        int maxColWidth = 0;
        int curColWidth = 0;
        String dataStr = null;
        Date dDate = null;
        TableColumn oColumn = null;
        Font font = new TWFont("Arial", Font.PLAIN, 10);
        FontMetrics fMetrics = table.getFontMetrics(font); //table.getFont());

        maximumWidths = new int[table.getColumnCount()];
        colXPositions = new int[table.getColumnCount()];

        int startIdx = 0;
        int endIdx = table.getColumnCount();
        if (!Language.isLTR()) {
            startIdx = -table.getColumnCount() + 1;
            endIdx = 1;
        }

        for (int column = startIdx; column < endIdx; column++) {
            int col = table.convertColumnIndexToModel(Math.abs(column)); // oColumn.getModelIndex();
            oColumn = table.getColumn(col + "");
            if (oColumn.getWidth() > 0) {
                maxColWidth = fMetrics.stringWidth((String) oColumn.getHeaderValue()) + 10;
                for (int row = 0; row < table.getRowCount(); row++) {
//System.out.println("---- " + row + " - " + column + " - " + g_oViewSettings.getRendererID(column));
                    curColWidth = 0;
                    try {
                        switch (oSettings.getRendererID(col)) { //  table.getColumnClass(column) == Number[].class) {
                            case 0: // DEFAULT
                            case 1: // SYMBOL
                            case 2: // DESCRIPTION
                            case 9: // TICK
                            case 'B': // Broker ID
                            case 'S': // Trade Session
                                //curColWidth = fMetrics.stringWidth((String)(table.getModel().getValueAt(row, col)));
                                dataStr = (String) (table.getModel().getValueAt(row, col));
                                if ((col == 2) || (col == 2)) {
                                    int strWidth = fMetrics.stringWidth(dataStr);
                                    if (strWidth > maxWidth) {
                                        String holder = "";
                                        char[] chars = dataStr.toCharArray();
                                        for (int count = 0; count < dataStr.length(); count++) {
                                            if ((fMetrics.stringWidth(holder + chars[count] + "") >= (maxWidth - 8))) {
                                                holder += "...";
                                                break;
                                            }
                                            holder += chars[count] + "";
                                        }
                                        dataStr = holder;
                                        holder = null;
                                        chars = null;
                                    }
                                }
                                curColWidth = fMetrics.stringWidth(dataStr);
                                break;
                            case 'C': // CUM_RETURNOF_52_WK
                            case 'P': // PRICE
                                curColWidth = fMetrics.stringWidth(oPriceFormat.format(((double[]) (table.getModel().getValueAt(row, col)))[0]));
                                break;
                            case 7:
                                dDate = new Date((Long.parseLong((String) (table.getModel().getValueAt(row, col)))));
                                curColWidth = fMetrics.stringWidth(g_oDateFormat.format(dDate));
                                dDate = null;
                                break;
                            case 8:
                                dDate = new Date((Long.parseLong((String) (table.getModel().getValueAt(row, col)))));
                                curColWidth = fMetrics.stringWidth(g_oDateTimeFormatHM.format(dDate));
                                dDate = null;
                                break;
                            case 'A': // TIME
                                dDate = new Date((Long.parseLong((String) (table.getModel().getValueAt(row, col)))));
                                curColWidth = fMetrics.stringWidth(g_oTimeFormat.format(dDate));
                                dDate = null;
                                break;
                            case 'D':
                                dDate = new Date((Long.parseLong((String) (table.getModel().getValueAt(row, col)))));
                                curColWidth = fMetrics.stringWidth(g_oDateTimeFormatHMS.format(dDate));
                                dDate = null;
                                break;
                                /*case  7  :
                                case  8  :
                                    Date dDate = new Date((Long.parseLong((String)(table.getModel().getValueAt(row, col)))));
                                    curColWidth = fMetrics.stringWidth(g_oDateTimeFormat.format(dDate));
                                    dDate = null;
                                    break; */
                            case 'Q': // Quantity with coloured bg
                                curColWidth = fMetrics.stringWidth(oQuantityFormat.format(((long[]) (table.getModel().getValueAt(row, col)))[0]));
                                break;
                            case 4: // QUANTITY
                                curColWidth = fMetrics.stringWidth(oQuantityFormat.format(Long.parseLong((String) (table.getModel().getValueAt(row, col)))));
                                break;
                            default:
                                curColWidth = fMetrics.stringWidth(oPriceFormat.format(Double.parseDouble((String) (table.getModel().getValueAt(row, col)))));
                                break;
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        curColWidth = 0;
                    }
                    if (maxColWidth < curColWidth)
                        maxColWidth = curColWidth;
                }
                maximumWidths[Math.abs(column)] = maxColWidth;
                if (Language.isLTR()) {
                    if (Math.abs(column) < (table.getColumnCount() - 1)) {
                        colXPositions[Math.abs(column) + 1] = colXPositions[Math.abs(column)] + maxColWidth + 5;
                    }
                } else {
                    if (Math.abs(column) <= (table.getColumnCount() - 1)) {
                        if ((Math.abs(column) - 1) > -1)
                            colXPositions[Math.abs(column) - 1] = colXPositions[Math.abs(column)] + maxColWidth + 5;
                    }
                }
            } else {
                //maximumWidths[column][0] = i2;
                maximumWidths[Math.abs(column)] = 0;
                if (Language.isLTR()) {
                    if (Math.abs(column) < (table.getColumnCount() - 1)) {
                        colXPositions[Math.abs(column) + 1] = colXPositions[Math.abs(column)];
                    }
                } else {
                    if (Math.abs(column) <= (table.getColumnCount() - 1)) {
                        if ((Math.abs(column) - 1) > -1)
                            colXPositions[Math.abs(column) - 1] = colXPositions[Math.abs(column)];
                    }
                }
            }
        }
    }

    public int printTable1(Graphics pg, PageFormat pageFormat, int pageIndex) throws PrinterException {
        int curColWidth = 0;
        String dataStr = null;
        String printTitle = null;

        if (pageIndex >= m_maxNumPage)
            return NO_SUCH_PAGE;

//System.out.println("====== " + pageIndex + " - " + m_maxNumPage);
        pg.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
        int wPage = 0;
        int hPage = 0;
        wPage = (int) pageFormat.getImageableWidth();
        hPage = (int) pageFormat.getImageableHeight(); // + 200;

        int y = 0;
        JTable table = getTable1();
        pg.setFont(new TWFont("Arial", Font.PLAIN, 10)); // table.getFont());
        pg.setColor(Color.black);
        Font fn = pg.getFont();
        FontMetrics fm = pg.getFontMetrics();
        FontMetrics fMetrics = pg.getFontMetrics(); //getTable1().getFontMetrics(table.getFont());

        y += fMetrics.getAscent();

        if (Language.isLTR())
            pg.drawString(getTitle(), 0, y);
        else
            //pg.drawString(getTitle(), wPage-80, y);
            pg.drawString(getTitle(), wPage - fMetrics.stringWidth(getTitle()), y); // colXPositions[0]+30, y);

        printTitle = Language.getString("UNIQUOTES");
        if (Language.isLTR()) {
            //priority
            //printTitle += " : " + g_oDateFormat2.format(new Date(TradeStore.getLastTradedTime()));
            pg.drawString(printTitle, wPage - fm.stringWidth(printTitle), y);
        } else {
            //priority
            //printTitle = g_oDateFormat2.format(new Date(TradeStore.getLastTradedTime())) + " : " + printTitle;
            pg.drawString(printTitle, 10, y);
        }

        y += 20; // space between title and table headers

        pg.fillRect(0, y - 5, wPage, 2);

        Font headerFont = fn.deriveFont(Font.BOLD); //table.getFont().deriveFont(Font.BOLD);
        pg.setFont(headerFont);
        fm = pg.getFontMetrics();

        TableColumnModel colModel = table.getColumnModel();
        int nColumns = colModel.getColumnCount();

        int h = fm.getAscent();
        y += h + 5; // add ascent of header font because of baseline
        // positioning

        pg.drawLine(0, y + 2, wPage, y + 2);

        int nRow, nCol;
        for (nCol = 0; nCol < nColumns; nCol++) { //Language.getString("MSG_INVALID_PASSWORD")
            TableColumn tk = colModel.getColumn(nCol);
            int width = tk.getWidth();

            if (colXPositions[nCol] > wPage) { //+width+30) > wPage) {
                String sMesg = "";
                if (pageFormat.getOrientation() == pageFormat.PORTRAIT) {
                    sMesg = "<html>" + Language.getString("MSG_TOO_MANY_COLUMNS") + "<br>" +
                            Language.getString("MSG_LANDSCAPE_OR_REDUCE_COLS") + "</html>";
                    new ShowMessage(Client.getInstance(), sMesg, "I", false);
                } else {
                    sMesg = "<html>" + Language.getString("MSG_TOO_MANY_COLUMNS") + "<br>" +
                            Language.getString("LBL_PRINT_HINT_REDUCE_COLUMNS") + "</html>";
                    new ShowMessage(Client.getInstance(), sMesg, "I", false);
                }
                sMesg = null;
                return NO_SUCH_PAGE;
            }
            String title = ((String) tk.getHeaderValue()).trim(); //(String)tk.getIdentifier();
            if (maximumWidths[nCol] > 0) {
                int strWidth = 0;
                int drawPos = 0;
//System.out.println("-----" + title + " - " + nCol + " - " + colXPositions[nCol] + " - " + maximumWidths[nCol]);
//                    if (nCol < (nColumns-1))
//                        drawPos = colXPositions[nCol+1] - fMetrics.stringWidth(title); // + 10;  // + maximumWidths[nCol]
//                    else
                drawPos = colXPositions[nCol] + maximumWidths[nCol] - fMetrics.stringWidth(title); // + 20;  // + maximumWidths[nCol]
                switch (oSettings.getRendererID(nCol)) {
                    case 0: // DEFAULT
                    case 1: // SYMBOL
                    case 2: // DESCRIPTION
                    case 9: // TICK
                    case 'B': // Broker ID
                    case 'D': // Date & Time
                    case 'S': // Trade Session
                        strWidth = fm.stringWidth(title);
                        if (Language.isLTR()) {
                            //pg.drawString(title, colXPositions[nCol]-7, y-5);
                            pg.drawString(title, colXPositions[nCol] + 10, y - 5);
                        } else {
                            //pg.drawString(title, drawPos-7, y-5);
                            pg.drawString(title, drawPos, y - 5);
                        }
                        //pg.drawString(title, wPage-colXPositions[nCol]-maximumWidths[nCol], y);
                        break;
                    default:
                        //if (Language.isLTR()) {
                        //pg.drawString(title, colXPositions[nCol]-7, y-5);
                        //    pg.drawString(title, colXPositions[nCol], y-5);
                        //} else {
                        //pg.drawString(title, drawPos-7, y-5);
                        pg.drawString(title, drawPos, y - 5);
                        //}
                        /*strWidth = fm.stringWidth(title);
                        //if (Language.isLTR())
                            pg.drawString(title, drawPos, y-5);
                            //pg.drawString(title, drawPos-7, y-5);
                            //pg.drawString(title, drawPos-strWidth, y-5);
                        //else
                        //    pg.drawString(title, wPage-drawPos-maximumWidths[nCol], y); */
                        break;
                }
            }
        }

        pg.setFont(fn.deriveFont(Font.PLAIN)); // table.getFont());
        fm = pg.getFontMetrics();

        int header = y;
        h = fm.getHeight();
        int rowH = Math.max((int) (h * 1.1), 10);
        //int rowH = Math.max((int)(h*1.5), 10);
        //int rowPerPage = (hPage)/rowH;
        int rowPerPage = (hPage - header) / rowH;
        //System.out.println("hPage=" + hPage + " wPage=" + wPage + " header=" + header + " rowH=" + rowH + " rowPerPage=" + rowPerPage);
        m_maxNumPage = Math.max((int) Math.ceil(table.getRowCount() / (double) rowPerPage), 1);

        TableModel tblModel = table.getModel();
        int iniRow = pageIndex * rowPerPage;
        int endRow = Math.min(table.getRowCount(), iniRow + rowPerPage);
        String str = null; //obj.toString();

        for (nRow = iniRow; nRow < endRow; nRow++) {
            y += h;
            int startIdx = 0;
            Date dDate = null;
            int endIdx = nColumns;
            if (!Language.isLTR()) {
                startIdx = -nColumns + 1;
                endIdx = 1;
            }
            for (nCol = startIdx; nCol < endIdx; nCol++) {
                int col = table.convertColumnIndexToModel(Math.abs(nCol)); // .getColumnModel().getColumn(Math.abs(nCol)).getModelIndex();
                //if (colModel.getColumn(nCol).getWidth() == 0) continue;
                if (maximumWidths[Math.abs(nCol)] == 0) continue;
                //Object obj = oDataModel.getValueAt(nRow, col);

                try {
                    switch (oSettings.getRendererID(col)) { //  table.getColumnClass(column) == Number[].class) {
                        case 0: // DEFAULT
                        case 1: // SYMBOL
                        case 2: // DESCRIPTION
                        case 9: // TICK
                        case 'B': // Broker ID
                        case 'S': // Trade Session
                            str = (String) (table.getModel().getValueAt(nRow, col));
                            if (col == 2) {
                                int strWidth = fMetrics.stringWidth(str);
                                if (strWidth > maxWidth) {
                                    String holder = "";
                                    char[] chars = str.toCharArray();
                                    for (int count = 0; count < str.length(); count++) {
                                        if ((fMetrics.stringWidth(holder + chars[count] + "") >= (maxWidth - 8))) {
                                            holder += "...";
                                            break;
                                        }
                                        holder += chars[count] + "";
                                    }
                                    str = holder;
                                    holder = null;
                                    chars = null;
                                }
                            }

                            //curColWidth = fMetrics.stringWidth((String)(table.getModel().getValueAt(row, column)));
                            break;
                        case 'C': // CUM_RETURNOF_52_WK
                        case 'P': // PRICE
                            str = oPriceFormat.format(((double[]) (table.getModel().getValueAt(nRow, col)))[0]);
                            //curColWidth = fMetrics.stringWidth(((double[])(table.getModel().getValueAt(row, column)))[0]+"");
                            break;
                        case 7:
                            dDate = new Date((Long.parseLong((String) (table.getModel().getValueAt(nRow, col)))));
                            str = g_oDateFormat.format(dDate);
                            dDate = null;
                            break;
                        case 8:
                            dDate = new Date((Long.parseLong((String) (table.getModel().getValueAt(nRow, col)))));
                            str = g_oDateTimeFormatHM.format(dDate);
                            dDate = null;
                            break;
                        case 'A': // TIME
                            dDate = new Date((Long.parseLong((String) (table.getModel().getValueAt(nRow, col)))));
                            str = g_oTimeFormat.format(dDate);
                            dDate = null;
                            break;
                        case 'D':
                            dDate = new Date((Long.parseLong((String) (table.getModel().getValueAt(nRow, col)))));
                            str = g_oDateTimeFormatHMS.format(dDate);
                            dDate = null;
                            break;
                        case 'Q': // Quantity with coloured bg
                            str = oQuantityFormat.format(((long[]) (table.getModel().getValueAt(nRow, col)))[0]);
                            //curColWidth = fMetrics.stringWidth(((long[])(table.getModel().getValueAt(row, column)))[0]+"");
                            break;
                        case 4: // QUANTITY
                            str = oQuantityFormat.format(Long.parseLong((String) (table.getModel().getValueAt(nRow, col))));
                            break;
                        default:
                            str = oPriceFormat.format(Double.parseDouble((String) (table.getModel().getValueAt(nRow, col))));
                            //curColWidth = fMetrics.stringWidth((String)(table.getModel().getValueAt(row, column)));
                            break;
                    }
                    //  curColWidth = fMetrics.stringWidth((String)table.getModel().getValueAt(row, column));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    str = "";
                }
                //String str = obj.toString();
                pg.setColor(Color.black);
                int drawPos = 0;
                drawPos = colXPositions[Math.abs(nCol)] + maximumWidths[Math.abs(nCol)] - fMetrics.stringWidth(str) + 10;  // + maximumWidths[nCol]
                switch (oSettings.getRendererID(col)) {
                    case 0: // DEFAULT
                    case 1: // SYMBOL
                    case 2: // DESCRIPTION
                    case 'B': // Broker ID
                    case 'S': // Trade Session
                        if (Language.isLTR())
                            pg.drawString(str, colXPositions[Math.abs(nCol)] + 5, y);
                        else
                            pg.drawString(str, drawPos, y);
                        //    pg.drawString(str, wPage-colXPositions[Math.abs(nCol)]-maximumWidths[Math.abs(nCol)], y);
                        break;
                    case 9: // TICK
                        int iValue = 0;
                        try {
                            iValue = Integer.parseInt(str);
                        } catch (Exception e) {
                            iValue = Settings.TICK_NOCHANGE;
                        }
                        switch (iValue) {
                            case Settings.TICK_UP:
                                str = "+";
                                break;
                            case Settings.TICK_DOWN:
                                str = "-";
                                break;
                            default:
                                str = "=";
                        }
                        if (Language.isLTR())
                            pg.drawString(str, colXPositions[Math.abs(nCol)] + 5, y);
                        else
                            pg.drawString(str, drawPos, y);
                        break;
                    default:
                        pg.drawString(str, drawPos, y);
                        break;
                }
            }
        }
        pg.fillRect(0, y + 5, wPage, 2);

        // draw page number
        pg.drawString(Language.getString("PAGE") + " : " + (pageIndex + 1), (int) wPage / 2 - 35, (int) (hPage) - fm.getDescent());

        System.gc();
        return PAGE_EXISTS;
    }

    public int printTable2(Graphics g, PageFormat pageFormat,
                           int pageIndex) throws PrinterException {

        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black);
        int fontHeight = g2.getFontMetrics().getHeight();
        int fontDesent = g2.getFontMetrics().getDescent();
        int titleGap = 10;
        JTable tableView = getTable1();

        //leave room for page number
        double pageHeight = pageFormat.getImageableHeight() - (fontHeight * 2) - titleGap;
        double pageWidth = pageFormat.getImageableWidth();
        double tableWidth = (double) tableView.getColumnModel().getTotalColumnWidth();
        double scale = 1;
        if (tableWidth >= pageWidth) {
            scale = pageWidth / tableWidth;
        }

        double headerHeightOnPage = tableView.getTableHeader().getHeight() * scale;
        double tableWidthOnPage = tableWidth * scale;

        double oneRowHeight = (tableView.getRowHeight()) * scale;
        int numRowsOnAPage = (int) ((pageHeight - headerHeightOnPage) / oneRowHeight);
        double pageHeightForTable = oneRowHeight * numRowsOnAPage;
        int totalNumPages = (int) Math.ceil(((double) tableView.getRowCount()) / numRowsOnAPage);
        if (pageIndex >= totalNumPages) {
            return NO_SUCH_PAGE;
        }
        g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        // draw title
        if (Language.isLTR()) {
            g2.drawString(getTitle(), 0, (int) (fontHeight - fontDesent));
        } else {
            FontMetrics fm = g2.getFontMetrics();
            int titleWidth = fm.charsWidth(getTitle().toCharArray(), 0, getTitle().length());
            g2.drawString(getTitle(), (int) (pageWidth - titleWidth), (int) (fontHeight - fontDesent));
        }

        // draw page number
        g2.drawString("Page: " + (pageIndex + 1), (int) pageWidth / 2 - 35, (int) (pageHeight
                + fontHeight + fontHeight - fontDesent));// two font heights. onr for header
        // one for footer
        if (Language.isLTR()) {
            g2.translate(0, headerHeightOnPage + fontHeight + titleGap);
        } else {
            g2.translate((int) (pageWidth - tableWidthOnPage), headerHeightOnPage + fontHeight + titleGap);
        }
        g2.translate(0f, -pageIndex * pageHeightForTable);

        //If this piece of the table is smaller
        //than the size available,
        //clip to the appropriate bounds.
        if (pageIndex + 1 == totalNumPages) {
            int lastRowPrinted = numRowsOnAPage * pageIndex;
            int numRowsLeft = tableView.getRowCount() - lastRowPrinted;
            g2.setClip(0,
                    (int) (pageHeightForTable * pageIndex),
                    (int) Math.ceil(tableWidthOnPage),
                    (int) Math.ceil(oneRowHeight *
                            numRowsLeft));
        }
        //else clip to the entire area available.
        else {
            g2.setClip(0,
                    (int) ((pageHeightForTable * pageIndex)),
                    (int) Math.ceil(tableWidthOnPage),
                    (int) Math.ceil(pageHeightForTable));
        }

        g2.scale(scale, scale);
        tableView.paint(g2);
        g2.scale(1 / scale, 1 / scale);
        g2.translate(0f, pageIndex * pageHeightForTable);
        g2.translate(0f, -headerHeightOnPage);
        g2.setClip(0, 0,
                (int) Math.ceil(tableWidthOnPage),
                (int) Math.ceil(headerHeightOnPage));
        g2.scale(scale, scale);
        tableView.getTableHeader().paint(g2);
        //paint header at top

        return Printable.PAGE_EXISTS;
    }

    /*public static void disableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(false);
    }*/

    /**
     * Re-enables double buffering globally.
     */
    /*public static void enableDoubleBuffering(Component c) {
        RepaintManager currentManager = RepaintManager.currentManager(c);
        currentManager.setDoubleBufferingEnabled(true);
    }*/
    public void setDataRequestID(DataStoreInterface store, String symbol, String id) {
        dataRequestID = new String[1];
        dataStore = new DataStoreInterface[1];
        dataRequestID[0] = id;
        dataStore[0] = store;
        this.symbol = symbol;
    }

    public void setDataRequestID(DataStoreInterface[] store, String symbol, String[] id) {

        if (dataRequestID != null && dataRequestID.length > 0) {
            for (int i = 0; i < dataRequestID.length; i++) {
                try {
                    if (getWindowType() == ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW) {
                        MDepthCalculator mDepthCalculator = (MDepthCalculator) getSupportingObject(0);
                        mDepthCalculator.finalizeWinow();
                        mDepthCalculator = null;
                    } else if (getWindowType() == ViewSettingsManager.TIMEnSALES_VIEW2) {
                        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                            DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
                        }
                    } else if (getWindowType() == ViewSettingsManager.DETAIL_QUOTE) {
                        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                            DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
                        }
                    } else if ((getWindowType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) && (getLinkedGroupID() == LinkStore.LINK_NONE)) {
                        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                            DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
                        }
                    } else if (getWindowType() == ViewSettingsManager.SNAP_QUOTE) {
                        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                            DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
                        }
                    } else if ((getWindowType() == ViewSettingsManager.DEPTH_PRICE_BID_VIEW) || (getWindowType() == ViewSettingsManager.DEPTH_BID_VIEW)) { //Bug ID <#0015>
                        //            if(get)
                        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oSettings.getID()))) {
                            DataStore.getSharedInstance().removeSymbolRequest(oSettings.getID());
                        }
                    }
                    RequestManager.getSharedInstance().removeRequest(dataRequestID[i]);
                    if (dataStore[i] != null)
                        dataStore[i].clear(this.symbol);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            dataRequestID = new String[1];
            dataStore = new DataStoreInterface[1];
            dataRequestID = id;
            dataStore = store;
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.symbol = symbol;
    }

    public String getDataRequestID() {
        return dataRequestID[0];
    }

    public void hideTitleBarMenu() {
        titleBarMenu = null;
    }

    public void mouseClicked(MouseEvent e) {
        if ((SwingUtilities.isRightMouseButton(e)) && (titleBarMenu != null)) {
            GUISettings.showPopup(titleBarMenu, this, e.getX(), e.getY());
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("HIDE")) {
            /*((BasicInternalFrameUI)getUI()).setNorthPane(null);
            if (oSettings!= null){
                oSettings.setHeaderVisible(false);
            }
            this.revalidate();*/
            setTitleVisible(false);
        }
    }

    public JSplitPane getSplitPane() {
        return splitPane;
    }

    public void setSplitPane(JSplitPane newSplitPane) {
        splitPane = newSplitPane;
        splitPane.addPropertyChangeListener("dividerLocation", this);
        if (g_oTable1 != null) {
            int location = g_oTable1.getModel().getViewSettings().getSplitLocation();
            if (location >= 0)
                splitPane.setDividerLocation(location);
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("title")) {
            TWDesktop.updateTabTitles(this);
        } else {
            if (g_oTable1 != null) {
                g_oTable1.getModel().getViewSettings().setSplitLocation(splitPane.getDividerLocation());
                //g_oTable1.getModel().applySettings();
            }
            if (g_oTable2 != null) {
                g_oTable2[0].getModel().getViewSettings().setSplitLocation(splitPane.getDividerLocation());
                //g_oTable2.getModel().applySettings();
            }
        }

    }

    public boolean isResizeWindowOnTitleToggle() {
        return resizeWindowOnTitleToggle;
    }

    public void setResizeWindowOnTitleToggle(boolean resizeWindowOnTitleToggle) {
        this.resizeWindowOnTitleToggle = resizeWindowOnTitleToggle;
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void closeWindow() {
//		if (finalizeWindow()){
//			dispose();
//		}else{
//			setVisible(false);
//		}
        finalizeWindow();
//dispose();
        TWDesktop.doCloseWindow(this);
        focusNextWindow();
    }

    public void doDefaultCloseAction() {
        //finalizeWindow();
        //GUISettings.doCloseWindow(this);
        super.doDefaultCloseAction();
        focusNextWindow();
    }

//     public void doCloseWindow(){
//        switch (getDefaultCloseOperation()) {
//             case DO_NOTHING_ON_CLOSE:
//                 break;
//             case HIDE_ON_CLOSE:
//                 setVisible(false);
//                 if (isSelected())
//                     try {
//                         setSelected(false);
//                     }
//                     catch (PropertyVetoException pve) {}
//                 break;
//             case DISPOSE_ON_CLOSE:
//                 dispose();
//             default:
//                 break;
//        }
//
//     }

//     public void dispose(){
//         super.dispose();
//         SharedMethods.printLine("Disposed " ,true);
//     }

    private void focusNextWindow() {

        try {
            JDesktopPane desk = getDesktopPane();
            TWDesktop desktop = (TWDesktop) Client.getInstance().getDesktop();
            if (desk.equals(desktop)) {
                desktop.focusNextWindow(desktop.getListPosition(this, true), true);
                desktop = null;
            }
        } catch (Exception e) {
        }
    }

    public int getDesktopItemType() {
        return desktopType;
    }

    public void setDesktopItemType(int type) {
        desktopType = type;
    }

    public int getWindowType() {
        try {
            return g_oTable1.getWindowType();
        } catch (Exception e) {
            return windowType;
        }
    }

    public void setWindowType(int type) {
        windowType = type;
    }

    public void resizeFrame() {
        try {
            Container np = ((BasicInternalFrameUI) getUI()).getNorthPane();
            if (np != null) {
                setSize(getWidth(), GlobalSettings.STOCK_TICKER_HEIGHT + np.getHeight() + getBorder().getBorderInsets(this).top * 2);
            } else {
                setSize(getWidth(), GlobalSettings.STOCK_TICKER_HEIGHT + getBorder().getBorderInsets(this).top * 2);
            }
            np = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Invoked when the component's size changes.
     */
    public void componentResized(ComponentEvent e) {
        resizeFrame();
    }

    public void processCompleted(boolean sucessfull, Object object) {
        resizeFrame();
    }

    public void processSarted(Object object) {

    }

    /**
     * Invoked when the component's position changes.
     */
    public void componentMoved(ComponentEvent e) {
        //buddyFrame.setLocation(this.getX(),this.getY()+this.getHeight());
    }

    /**
     * Invoked when the component has been made visible.
     */
    public void componentShown(ComponentEvent e) {

    }

    /**
     * Invoked when the component has been made invisible.
     */
    public void componentHidden(ComponentEvent e) {

    }

    public void setLocationRelativeTo(Component c) {
        Dimension parentSize = c.getSize();

        if (Settings.isDualScreenMode()) {
            if (Settings.isLeftAlignPopups()) {
                this.setBounds((int) ((parentSize.getWidth() / 2 - this.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                        this.getWidth(), this.getHeight());
            } else {
                this.setBounds((int) ((parentSize.getWidth() / 2) + (parentSize.getWidth() / 2 - this.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                        this.getWidth(), this.getHeight());
            }
        } else {


            this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
                    (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                    this.getWidth(), this.getHeight());
        }

//        this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
//                (int) ((parentSize.getHeight() - this.getHeight()) / 2),
//                this.getWidth(), this.getHeight());
    }

    private void ensureVisibility() {
        try {
            if (this instanceof NonTabbable) return;

//            Dimension parentSize = Toolkit.getDefaultToolkit().getScreenSize();
            Dimension parentSize = Client.getInstance().getDesktop().getSize();
            if (Settings.isDualScreenMode() && (Settings.isMultiScreenEnvr())) {
                this.setBounds((int) this.getLocation().getX(), (int) this.getLocation().getY(), (int) this.getSize().getWidth(), (int) this.getSize().getHeight());
            }
//            Dimension parentSize = this.getDesktopPane().getSize();
            Dimension mySize = this.getSize();
            Dimension myNewSize = this.getSize();

            Point myLocation = this.getLocation();
            Point myNewLocation = this.getLocation();

            boolean resized = false;
            boolean repositioned = false;

            if (mySize.height > parentSize.height) {
                myNewSize.height = (int) (parentSize.getHeight() * .75);
                resized = true;
            } else {
                myNewSize.height = mySize.height;
            }

            if (mySize.width > parentSize.width) {
                if (this.getTitle().equals(Language.getString("TICKER"))) {
                    System.out.println("----ticker");
                } else {
                    myNewSize.width = (int) (parentSize.getWidth() * .75);
                }
                resized = true;
            } else {
                myNewSize.width = mySize.width;
            }
            if (resized) {
                this.setSize(myNewSize);
                if (this.getTitle().equals(Language.getString("TICKER"))) {
                    this.setLocation(myNewLocation);
                } else {
                    this.setBounds((int) ((parentSize.getWidth() - this.getWidth()) / 2),
                            (int) ((parentSize.getHeight() - this.getHeight()) / 2),
                            this.getWidth(), this.getHeight());
                }
            }
            if ((myLocation.y < 0) || (myLocation.y > parentSize.height - 50)) {
                myNewLocation.y = 0;
                repositioned = true;
            } else {
                myNewLocation.y = myLocation.y;
            }
            //if ((myLocation.x < 0) || (myLocation.x > parentSize.width - 50)) {
            if ((Language.isLTR() && myLocation.x < 0) || ((!Language.isLTR()) && (myLocation.x + mySize.width < 100)) || (myLocation.x > parentSize.width - 50)) {

                myNewLocation.x = 0;
                repositioned = true;
            } else {
                myNewLocation.x = myLocation.x;
            }
            if (repositioned) {
                this.setLocation(myNewLocation);
            }

        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public void initSupportingObjcts(int size) {
        objects = new Object[size];
    }

    public void addSupportingObject(Object object, int index) {
        objects[index] = object;
    }

    private Object getSupportingObject(int index) {
        return objects[index];
    }

    public String[][] getPanelString() {
        return null;
    }

    public void setModelStatus(boolean setModel) {
        isModel = setModel;
    }

    public void setModel(InternalFrame parent) {
        isModel = true;
        this.parent = parent;
        final JPanel glass = new JPanel();
        glass.setOpaque(false);
        glass.setLayout(null);

        // Attach mouse listeners
        MouseInputAdapter adapter = new MouseInputAdapter() {
            // implements java.awt.event.MouseListener
            public void mouseClicked(MouseEvent e) {
                Toolkit.getDefaultToolkit().beep();
            }
        };
        glass.addMouseListener(adapter);
        glass.addMouseMotionListener(adapter);

        Client.getInstance().getDesktop().validate();
        try {
            setSelected(true);
        } catch (PropertyVetoException ignored) {
        }

        // Add modal internal frame to glass pane
//        this.setLocationRelativeTo(parent);
        glass.add(this);
        this.setLocationRelativeTo(Client.getInstance().getDesktop().getRootPane());

        // Change glass pane to our panel
        Client.getInstance().getDesktop().getRootPane().setGlassPane(glass);
//        glass.setLocation(parent.getLocation());

        // Show glass pane, then modal dialog
        glass.setVisible(true);

    }

    public Dimension getMinimumSize() {
        return minimumSize;
    }

    public void setMinimumSize(Dimension minimumSize) {
        this.minimumSize = minimumSize;
        super.setMinimumSize(minimumSize);
    }

    public void setVisible(boolean value) {
        super.setVisible(value);
        if (isModel) {
            if (value) {
                startModal();
            } else {
                stopModal();
            }
        }

        if (!TWDesktop.isInTabbedPanel(this)) {
            if (value) {
                TWDesktop.addWindowTab(this);
            }
        }
        if (TWDesktop.isInTabbedPanel(this)) {
            if (!value) {
                TWDesktop.removeWindowtab(this);
            }
        }

        if (!TWDesktop.checkInternalFrameAvailability(this)) {
            if (value) {
                TWDesktop.removeWindowtab(this);
            }
        }
        if (TWDesktop.checkInternalFrameAvailability(this) && !TWDesktop.isInTabbedPanel(this)) {
            if (value) {
                TWDesktop.addWindowTab(this);
            }
        }

        if (value) {
            ensureVisibility();
        } else {
            Settings.removeFormFocusFlowList(this);
            TWDesktop.setTabForSelectedWindow(Settings.getFormFocusFlowList(Settings.getFocusFlowList().size() - 1));
        }
    }

    private synchronized void startModal() {
        try {
            if (SwingUtilities.isEventDispatchThread()) {
                EventQueue theQueue = getToolkit().getSystemEventQueue();
                while (isVisible()) {
                    AWTEvent event = theQueue.getNextEvent();
                    Object source = event.getSource();
                    if (event instanceof ActiveEvent) {
                        ((ActiveEvent) event).dispatch();
                    } else if (source instanceof Component) {
                        ((Component) source).dispatchEvent(event);
                    } else if (source instanceof MenuComponent) {
                        ((MenuComponent) source).dispatchEvent(event);
                    } else {
                        System.err.println("Unable to dispatch: " + event);
                    }
                }
            } else {
                while (isVisible()) {
                    wait();
                }
            }
        } catch (InterruptedException ignored) {
        }
    }

    private synchronized void stopModal() {
        //Client.getInstance().getDesktop().getRootPane().getGlassPane().setVisible(false);
//        parent.getRootPane().setGlassPane(null);
        //notifyAll();
    }

    /**
     * public ModalInternalFrame(JRootPane rootPane, Component desktop, Component pane) {
     * <p/>
     * // create opaque glass pane final JPanel glass = new JPanel(); glass.setOpaque(false);
     * <p/>
     * // Attach mouse listeners MouseInputAdapter adapter = new MouseInputAdapter() { };
     * glass.addMouseListener(adapter); glass.addMouseMotionListener(adapter);
     * <p/>
     * // Add in option pane getContentPane().add(pane, BorderLayout.CENTER);
     * <p/>
     * // Define close behavior PropertyChangeListener pcl = new PropertyChangeListener() { public void
     * propertyChange(PropertyChangeEvent event) { if (isVisible() && (event.getPropertyName().equals(
     * JOptionPane.VALUE_PROPERTY) || event.getPropertyName().equals( JOptionPane.INPUT_VALUE_PROPERTY))) { try {
     * setClosed(true); } catch (PropertyVetoException ignored) { } setVisible(false); glass.setVisible(false); } } };
     * pane.addPropertyChangeListener(pcl);
     * <p/>
     * // Change frame border putClientProperty("JInternalFrame.frameType", "optionDialog");
     * <p/>
     * // Size frame //        Dimension size = getPreferredSize(); //        Dimension rootSize = desktop.getSize();
     * <p/>
     * //        setBounds((rootSize.width - size.width) / 2, //                (rootSize.height - size.height) / 2, //
     * size.width, size.height); desktop.validate(); try { setSelected(true); } catch (PropertyVetoException ignored) {
     * }
     * <p/>
     * // Add modal internal frame to glass pane glass.add(this);
     * <p/>
     * // Change glass pane to our panel rootPane.setGlassPane(glass);
     * <p/>
     * // Show glass pane, then modal dialog glass.setVisible(true); }
     */

    public Dimension reSizeSpecial(Dimension newSz, Dimension oldSz) {

        if (((newSz != null) && (!newSz.equals(oldSz)))) {
            int oldWidth = oldSz.width;
            int oldHeight = oldSz.height;
            int newWidth = newSz.width;
            int newHeight = newSz.height;
            Dimension dim;
            if ((oldWidth != newWidth) && (oldHeight != newHeight)) {
//                System.out.println("11111111");
                float widthRatio = ((float) newWidth / oldWidth);
                float heightRatio = ((float) newHeight / oldHeight);
                float widthDecimal = Math.abs(widthRatio - (Math.round(widthRatio)));
                float heightDecimal = Math.abs(heightRatio - (Math.round(heightRatio)));
                if (widthDecimal == 0f && heightDecimal == 0f) {
//                    System.out.println("00000000");
                    dim = new Dimension(newWidth, newHeight);
                    oldSize.setSize(dim.getSize());
                    return dim;
                } else if (widthDecimal >= heightDecimal) {
//                    System.out.println("0000000011");
                    newWidth = (int) (newHeight / aspectRatioReltivetoWd);
                    dim = new Dimension(newWidth, newHeight);
                    oldSize.setSize(dim.getSize());
                    return dim;
                } else if (widthDecimal < heightDecimal) {
//                    System.out.println("000000022");
                    newHeight = (int) (newWidth * aspectRatioReltivetoWd);
                    dim = new Dimension(newWidth, newHeight);
                    oldSize.setSize(dim.getSize());
                    return dim;
                }
            } else if ((oldWidth == newWidth) && (oldHeight != newHeight)) {
//                System.out.println("22222222");
                newWidth = (int) (newHeight / aspectRatioReltivetoWd);
                dim = new Dimension(newWidth, newHeight);
                oldSize.setSize(dim.getSize());
                return dim;
            } else if ((oldWidth != newWidth) && (oldHeight == newHeight)) {
//                System.out.println("3333333333333");
                newHeight = (int) (newWidth * aspectRatioReltivetoWd);
                dim = new Dimension(newWidth, newHeight);
                oldSize.setSize(dim.getSize());
                return dim;
            } else {
//                System.out.println("aaaaaaaaaaaa");
                dim = new Dimension(newWidth, newHeight);
                oldSize.setSize(dim.getSize());
                return dim;
//                return oldSize;
            }
        }
//        System.out.println("end of the method ="+oldSize.toString());
        return oldSize;
    }


    public Dimension getOrigDimension() {
        return origSize;
    }


    public void setOrigDimention(Dimension dim) {
        origSize.setSize(dim);
        if (oldSize.getSize().height <= 0 && oldSize.getSize().width <= 0) {
            oldSize.setSize(dim);
        }
        aspectRatioReltivetoWd = (((float) origSize.height) / origSize.width);
    }


    public Dimension getOldSize() {
        return oldSize;
    }

    public void setOldSize(Dimension dim) {
        oldSize.setSize(dim);
    }

    public boolean getisLoadedFromWsp() {
        return isLodedFromWsp;
    }

    public void setisLoadedFromWsp(boolean isWsp) {
        isLodedFromWsp = isWsp;
    }

    public Dimension resizeToFont(Dimension newSz, Dimension oldSz) {
        Dimension dim = new Dimension(newSz);

        int oldHeight = oldSz.height;

        int newHeight = newSz.height;
        if (oldHeight == newHeight) {
            int nww = (int) (newHeight / aspectRatioReltivetoWd);
            if (nww < newSz.width) {
                nww = newSz.width;
            }
            dim = new Dimension(nww, newHeight);
            aspectRatioReltivetoWd = (((float) dim.height) / dim.width);
            oldSize.setSize(dim);
        }
        return dim;
    }


    public void applicationLoading(int percentage) {

    }

    public void applicationLoaded() {

    }

    public void applicationReadyForTransactions() {

    }

    public void applicationExiting() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void workspaceWillLoad() {

    }

    public void workspaceLoaded() {

    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void selectedExchangeChanged(Exchange exchange) {

    }


    public synchronized void setSelected(boolean selected) throws PropertyVetoException {
        super.setSelected(selected);    //To change body of overridden methods use File | Settings | File Templates.
        if (selected) {
            TWDesktop.setTabForSelectedWindow(this);
            Settings.addToFocusFlowList(this);
        }
    }

    public String getLinkedGroupID() {
        return linkedGroupID;
    }

    public void setLinkedGroupID(String linkedGroupID) {
        this.linkedGroupID = linkedGroupID;
    }

    @Override
    public void setIcon(boolean b) throws PropertyVetoException {
        try {
            if (b) {
                this.setLayer(GUISettings.DESKTOP_ICON_LAYER);
            } else {
                if (Settings.isPutAllToSameLayer()) {
                    this.setLayer(GUISettings.DEFAULT_LAYER);
                    if (getViewSetting() != null) {
                        this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, getViewSetting().getIndex());
                    } else {
                        this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                    }
                } else {
                    if (getViewSetting() != null) {
                        this.setLayer(getViewSetting().getLayer());
                        this.getDesktopPane().setLayer(this, getViewSetting().getLayer(), getViewSetting().getIndex());
                    } else {
                        this.setLayer(getFrameLayer());
                        this.getDesktopPane().setLayer(this, getFrameLayer(), 0);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        super.setIcon(b);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setMaximum(boolean b) throws PropertyVetoException {
        super.setMaximum(b);    //To change body of overridden methods use File | Settings | File Templates.
        try {
            if (b) {
                ((SmartTable) getTable1()).adjustColumnWidthsToFit(10);
                if (getTable2() != null) {
                    ((SmartTable) getTable2()).adjustColumnWidthsToFit(10);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void checkForDetach() {
        if (oSettings != null && oSettings.isDetached && Settings.detachOnWspLoad) {
            try {
                ((DetachableRootPane) this.getRootPane()).detach();
                this.setSize(oSettings.getSize());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int getFrameLayer() {
        return layer;
    }

    @Override
    public void setLayer(int layer) {
        if (this.layer == -1) {
            this.layer = layer;
        }
        try {
            if (getViewSetting() == null) {
                setViewSetting(g_oTable1.getModel().getViewSettings());
            }
            if ((getViewSetting().getLayer() == -1))
                getViewSetting().setLayer(layer);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        super.setLayer(layer);    //To change body of overridden methods use File | Settings | File Templates.
    }

    /* public void setLayer(Integer layer) {
        if (this.layer == -1) {
            this.layer = layer;
        }
        try {
            if((oSettings.getLayer() == -1))
                oSettings.setLayer(layer);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        super.setLayer(layer);    //To change body of overridden methods use File | Settings | File Templates.
    }*/

//     public boolean isVisible() {
//        if (TWDesktop.hidden){
//            return false;
//        } else {
//            return super.isVisible();    //To change body of overridden methods use File | Settings | File Templates.
//        }
//    }
/*
    public void setIcon(boolean b) throws PropertyVetoException {
        SharedMethods.printLine("Icon " + b + " " + super.getTitle(), true);
        super.setIcon(b);
    }*/
}

