package com.isi.csvr.iframe;
// Copyright (c) 2000 Home

import javax.swing.*;
import java.awt.*;

public interface WindowWrapper {

    /**
     * Constructor
     */
    public Point getLocation();

    public Dimension getSize();

    public int getWindowStyle();

    public int getZOrder();

    public boolean isWindowVisible();

    public JTable getTable1();

    public JTable getTable2();

    public JTable getTabFoler(String id);

    public void applySettings();

    public void printTable();

    public boolean isTitleVisible();

    public void setTitleVisible(boolean status);

    public void windowClosing();

    public boolean isDetached();

    public Object getDetachedFrame();
}