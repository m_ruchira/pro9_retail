package com.isi.csvr.chart;

import com.isi.csvr.tabbedpane.TWTabbedPane;

/**
 * User: Pramoda
 * Date: Jul 20, 2007
 * Time: 11:26:06 AM
 */
public class PropertyDialogTabPane extends TWTabbedPane {

    private ChartProperties sourceCP;
    private WindowPanel rect;

    public PropertyDialogTabPane() {
        super();
    }

    /*public PropertyDialogTabPane(int tabPlacement) {
        super(tabPlacement);
    }

    public PropertyDialogTabPane(int tabPlacement, int tabLayoutPolicy) {
        super(tabPlacement, tabLayoutPolicy);
    }*/


    public ChartProperties getSourceCP() {
        return sourceCP;
    }

    public void setSourceCP(ChartProperties sourceCP) {
        this.sourceCP = sourceCP;
    }


    public WindowPanel getRect() {
        return rect;
    }

    public void setRect(WindowPanel rect) {
        this.rect = rect;
    }
}
