package com.isi.csvr.chart;

import com.isi.csvr.shared.Constants;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class SymbolObject {
    String symbol;

    public SymbolObject(String sym) {
        symbol = sym;
    }

    public String toString() {
        return extractSymbol();
    }

    private String extractSymbol() {
        String[] keyData;
        try {
            keyData = symbol.split(Constants.KEY_SEPERATOR_CHARACTER);
            if ((keyData != null) && (keyData.length > 1))
                return keyData[1];
            else
                return symbol;
        } catch (Exception e) {

        }
        return " ";
    }

    public String getSymbol() {
        return symbol;
    }

}