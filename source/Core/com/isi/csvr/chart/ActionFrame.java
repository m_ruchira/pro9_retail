package com.isi.csvr.chart;

import com.isi.csvr.chart.customindicators.CustomIndicatorListener;
import com.isi.csvr.chart.customindicators.IndicatorConfigInfo;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.IndicatorMenuObject;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Apr 16, 2009
 * Time: 2:40:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class ActionFrame extends JDialog implements ActionListener, MouseListener, WindowListener, Themeable {

    JList indicatorList;
    TWButton btnClose;
    InternalFrame parent;
    IndicatorBuilderBasePanel indicatorBuilderPanel;
    //Buttons
    private JButton btnNew;
    private JButton btnEdit;
    private JButton btnDelete;
    private JButton btnCopy;
    private JSeparator topHorizontalSeperator;
    private JSeparator bottomHorizontalSeperator;
    private JSeparator topHorizontalShadowTopSeperator;
    private JSeparator topHorizontalShadowBottomSeperator;
    private JSeparator bottomHorizontalShadowSeperator;
    private JScrollPane scrollPane;

    public ActionFrame(IndicatorBuilderBasePanel builderPanel, JFrame owner) {

        super(owner);
        this.indicatorBuilderPanel = builderPanel;
        setSize(350, 275);

        setTitle(Language.getString("CUSTOM_INDICATOR_SELECT_ACTION_WINDOW_TITLE"));
        setLayout(new BorderLayout());
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        //shashika
        getContentPane().setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"35", "1", "1", "1", "25", "1", "1", "100%"}, 1, 1));
        JPanel buttonPanel = new JPanel(new FlexGridLayout(new String[]{"75", "75", "75", "75", "100%"}, new String[]{"100%"}, 1, 1));

        btnNew = new JButton(Language.getString("NEW"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/new.gif"));
        btnEdit = new JButton(Language.getString("EDIT"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/edit.gif"));
        btnCopy = new JButton(Language.getString("COPY"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/copy.gif"));
        btnDelete = new JButton(Language.getString("DELETE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/delete.gif"));

        btnNew.setBorder(BorderFactory.createEmptyBorder());
        btnEdit.setBorder(BorderFactory.createEmptyBorder());
        btnCopy.setBorder(BorderFactory.createEmptyBorder());
        btnDelete.setBorder(BorderFactory.createEmptyBorder());

        btnNew.setContentAreaFilled(false);
        btnEdit.setContentAreaFilled(false);
        btnCopy.setContentAreaFilled(false);
        btnDelete.setContentAreaFilled(false);

        btnNew.addMouseListener(this);
        btnEdit.addMouseListener(this);
        btnCopy.addMouseListener(this);
        btnDelete.addMouseListener(this);

        btnNew.addActionListener(this);
        btnEdit.addActionListener(this);
        btnCopy.addActionListener(this);
        btnDelete.addActionListener(this);

        btnNew.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnEdit.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnCopy.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 11));

        buttonPanel.add(btnNew);
        buttonPanel.add(btnEdit);
        buttonPanel.add(btnCopy);
        buttonPanel.add(btnDelete);

        getContentPane().add(buttonPanel);

        topHorizontalShadowTopSeperator = new JSeparator(JSeparator.HORIZONTAL);
        topHorizontalShadowTopSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        getContentPane().add(topHorizontalShadowTopSeperator);
        topHorizontalSeperator = new JSeparator(JSeparator.HORIZONTAL);
        topHorizontalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        getContentPane().add(topHorizontalSeperator);
        topHorizontalShadowBottomSeperator = new JSeparator(JSeparator.HORIZONTAL);
        topHorizontalShadowBottomSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        getContentPane().add(topHorizontalShadowBottomSeperator);


        JPanel namePanel = new JPanel(new FlexGridLayout(new String[]{"10", "150", "100%"}, new String[]{"100%"}, 1, 1));
        namePanel.add(new JLabel(""));
        namePanel.add(new JLabel(Language.getString("CUSTOM_INDICATORS")));
        getContentPane().add(namePanel);

        bottomHorizontalSeperator = new JSeparator(JSeparator.HORIZONTAL);
        bottomHorizontalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        getContentPane().add(bottomHorizontalSeperator);

        bottomHorizontalShadowSeperator = new JSeparator(JSeparator.HORIZONTAL);
        bottomHorizontalShadowSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        getContentPane().add(bottomHorizontalShadowSeperator);


        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
        btnCopy.setEnabled(false);

        String[] indicatorNames = IndicatorFactory.getIndicatorNames();
        indicatorList = new JList();
        if (indicatorNames != null) {
            indicatorList.setListData(indicatorNames);
            if (indicatorList.getLastVisibleIndex() != -1) {
                indicatorList.setSelectedIndex(0);
            }
        }
        DefaultListSelectionModel defaultListSelectionModel = new DefaultListSelectionModel();
        defaultListSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        defaultListSelectionModel.clearSelection();
        indicatorList.setSelectionModel(defaultListSelectionModel);
        indicatorList.addMouseListener(this);
        scrollPane = new JScrollPane(indicatorList);
        scrollPane.setPreferredSize(new Dimension(280, 275));

        scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")),
                BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"))));

        //Border indicatorBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),Language.getString("CUSTOM_INDICATORS"));
        //scrollPane.setBorder(indicatorBorder);
        JPanel centerpanel = new JPanel(new BorderLayout());
        centerpanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        centerpanel.add(scrollPane, BorderLayout.CENTER);
        getContentPane().add(centerpanel);

        addWindowListener(this);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        applyTheme();
    }

    public void showWindow() {

        setLocationRelativeTo(ChartFrame.getSharedInstance().getDesktop());
        ChartFrame.getSharedInstance().getDesktop().setLayer(this, GUISettings.INTERNAL_DIALOG_LAYER + 1);
        String[] indicatorNames = IndicatorFactory.getIndicatorNames();
        if (indicatorNames != null) {
            indicatorList.setListData(indicatorNames);
            if (indicatorList.getLastVisibleIndex() != -1) {
                indicatorList.setSelectedIndex(0);
            }
            indicatorList.updateUI();
        }
        setModal(true);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == btnNew && btnNew.isEnabled()) {
            setVisible(false);
            indicatorBuilderPanel.isEditing = false;
            indicatorBuilderPanel.indicatorTextPane.setText("");
            indicatorBuilderPanel.txtIndicatorName.requestFocus();
            indicatorBuilderPanel.txtIndicatorName.setText("New Custom Indicator");
            indicatorBuilderPanel.txtIndicatorName.setEditable(true);
            indicatorBuilderPanel.txtIndicatorName.setSelectionStart(0);
            indicatorBuilderPanel.txtIndicatorName.setSelectionEnd(indicatorBuilderPanel.txtIndicatorName.getText().trim().length());
            clearMessagePanes();
        } else if (source == btnEdit && btnEdit.isEnabled()) {
            edit();
        } else if (source == btnDelete && btnDelete.isEnabled()) {
            int option = JOptionPane.showConfirmDialog(indicatorBuilderPanel, Language.getString("CI_DELETE_CONFIRM_MESSAGE"),
                    Language.getString("CI_DELETE_CONFIRM_MESSAGE_TITLE"), JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.NO_OPTION) return;
            indicatorBuilderPanel.isEditing = false;
            String indicatorName = (String) indicatorList.getSelectedValue();
            if (indicatorName != null) {
                IndicatorFactory.removeIndicator(indicatorName);

                //this is not executing now
                for (CustomIndicatorListener cil : indicatorBuilderPanel.ciListeners) {
                    cil.customIndicatorDeleted(indicatorName);
                    ChartFrame.getSharedInstance().refreshCustomIndicatorsMenu();
                }
                File classFile = new File(IndicatorFactory.classPath + indicatorName + ".ind");
                classFile.delete();
                String[] indicatorNames = IndicatorFactory.getIndicatorNames();
                indicatorList.setListData(indicatorNames);

                IndicatorMenuObject indicatorMenuObject = null;
                for (IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
                    if (imo.getIndicatorName().equals(indicatorName)) {
                        indicatorMenuObject = imo;
                    }
                }
                if (indicatorMenuObject != null) {
                    ChartFrame.getSharedInstance().customIndicators.remove(indicatorMenuObject);
                    IndicatorDetailStore.CustomIndicators.remove(indicatorMenuObject);
                    ChartFrame.getSharedInstance().refreshCustomIndicatorsMenu();
                }

                if (indicatorList.getLastVisibleIndex() == -1) { //if no indicators presense
                    disableButtons();
                } else {
                    indicatorList.setSelectedIndex(0);
                }

                indicatorList.updateUI();
            }
            indicatorBuilderPanel.txtIndicatorName.setEditable(true);

        } else if (source == btnCopy) {
            String indicatorName = (String) indicatorList.getSelectedValue();
            if (indicatorName != null) {
                indicatorBuilderPanel.isEditing = false;
                indicatorBuilderPanel.editingIndicatorName = "New Custom Indicator";
                IndicatorConfigInfo ici = IndicatorFactory.getIndicatoInfoForName(indicatorName);
                indicatorBuilderPanel.txtIndicatorName.setText("New Custom Indicator");
                indicatorBuilderPanel.txtIndicatorName.setEditable(true);
                indicatorBuilderPanel.indicatorTextPane.setText("");
                indicatorBuilderPanel.indicatorTextPane.setText(ici.Grammer);
                indicatorBuilderPanel.markSyntax();
            }
            enableButtons();
            indicatorBuilderPanel.dirty = true;
            setVisible(false);
        } else if (source == btnClose) {
            indicatorBuilderPanel.indicatorTextPane.setText("");
            indicatorBuilderPanel.txtIndicatorName.setEditable(true);
//                parent.dispose();
            indicatorBuilderPanel.isEditing = false;
            this.dispose();
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == indicatorList) {
            if (indicatorList.getLastVisibleIndex() != -1) {
                enableButtons();
            }
            if (e.getClickCount() == 2) {
                edit();
            }
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {

        if (e.getSource().equals(btnNew) && btnNew.isEnabled()) {
            btnNew.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnEdit) && btnEdit.isEnabled()) {
            btnEdit.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnCopy) && btnCopy.isEnabled()) {
            btnCopy.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnDelete) && btnDelete.isEnabled()) {
            btnDelete.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        }
    }

    public void mouseExited(MouseEvent e) {

        if (e.getSource().equals(btnNew)) {
            btnNew.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnEdit)) {
            btnEdit.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnCopy)) {
            btnCopy.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnDelete)) {
            btnDelete.setBorder(BorderFactory.createEmptyBorder());
        }
    }

    protected void edit() {
        indicatorBuilderPanel.isEditing = true;
        String indicatorName = (String) indicatorList.getSelectedValue();
        if (indicatorName != null) {
            IndicatorConfigInfo ici = IndicatorFactory.getIndicatoInfoForName(indicatorName);
            indicatorBuilderPanel.txtIndicatorName.setText(indicatorName);
            indicatorBuilderPanel.txtIndicatorName.setEditable(false);
            indicatorBuilderPanel.indicatorTextPane.setText("");
            indicatorBuilderPanel.indicatorTextPane.setText(ici.Grammer);
            indicatorBuilderPanel.markSyntax();
        }
        //((IndicatorBuilder) parent).enableButtons(); // TODO:
        indicatorBuilderPanel.enableButtons();
        indicatorBuilderPanel.dirty = false;
        setVisible(false);
        clearMessagePanes();
    }

    protected void clearMessagePanes() {
        indicatorBuilderPanel.messagesPane.setText("");
        indicatorBuilderPanel.errorsPane.setText("");
        indicatorBuilderPanel.warningsPane.setText("");
    }

    public void enableButtons() {
        btnEdit.setEnabled(true);
        btnCopy.setEnabled(true);
        btnDelete.setEnabled(true);
    }

    public void disableButtons() {
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
        btnCopy.setEnabled(false);
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);

        btnNew.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/new.gif"));
        btnEdit.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/edit.gif"));
        btnCopy.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/copy.gif"));
        btnDelete.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/delete.gif"));

        topHorizontalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        bottomHorizontalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        topHorizontalShadowTopSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        topHorizontalShadowBottomSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        bottomHorizontalShadowSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));

        scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")),
                BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"))));

    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
        indicatorBuilderPanel.indicatorTextPane.setText("");
        indicatorBuilderPanel.dirty = false;
//            parent.dispose();
//            tabbedPanel.setVisible(false);
        this.dispose();
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public JList getIndicatorList() {
        return indicatorList;
    }


}
