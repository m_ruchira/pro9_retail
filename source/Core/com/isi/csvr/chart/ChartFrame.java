package com.isi.csvr.chart;

import com.isi.csvr.*;
import com.isi.csvr.chart.analysistechniques.AnalysisTechWindow;
import com.isi.csvr.chart.backtesting.BackTestingWindow;
import com.isi.csvr.chart.customindicators.CustomIndicatorListener;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.IndicatorMenuObject;
import com.isi.csvr.chart.options.ChartOptions;
import com.isi.csvr.chart.options.ChartOptionsWindow;
import com.isi.csvr.chart.options.ThemeData;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.*;

//import com.thoughtworks.xstream.XStream;
//import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class ChartFrame extends InternalFrame implements ActionListener, Themeable,
        ComponentListener, TWDesktopInterface, MenuListener, ViewConstants, WindowWrapper,
        ApplicationListener, DataWindowListener, CustomIndicatorListener, TradingConnectionListener,
        TWTabbedPaleListener, MouseListener, PopupMenuListener, ChartNavigationBarListner {

    public static final byte TILE_NORMALLY = 0;
    private byte currentArrangeMode = TILE_NORMALLY;
    public static final byte TABBED = 1;
    public static final byte TILE_HORIZONTALLY = 2;
    public static final byte TILE_VERTICALLY = 3;
    private static ChartFrame self = null;
    private static int currentLayout = 1;
    private static TWTabbedPane windowTabs;
    public GraphToolBar topToolBar = null;
    public BottomToolBar bottomToolBar = null;
    public JPanel naviBarPanel = null;
    public boolean isNavigationBarVisible = false;
    protected ArrayList<IndicatorMenuObject> customIndicators = new ArrayList<IndicatorMenuObject>();
    protected ArrayList<IndicatorMenuObject> alStrategies = new ArrayList<IndicatorMenuObject>();
    protected ArrayList<IndicatorMenuObject> alPatterns = new ArrayList<IndicatorMenuObject>();
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private int windowType;
    private JDesktopPane desktop;
    private JPanel pnlDesktopContainer;
    private GraphFrame activeGraph = null;
    private boolean dataWinVisible = false;
    private DataWindow dataWindow = null;
    private Hashtable<String, Vector> layouts = new Hashtable<String, Vector>();
    private TWMenuBar menuBar;
    private JMenu mnuFile;
    private TWMenu mnuNew;
    private TWMenuItem miNewChart;
    private TWMenuItem miNewLayout;
    private TWMenuItem miOpenChart;
    private TWMenuItem miSaveChart;
    private TWMenuItem miSaveChartAs;
    private TWMenuItem miSetDefaults;
    private TWMenuItem miPrintChart;
    private TWMenuItem miDetach;
    private TWMenuItem miCloseChart;
    private TWMenuItem miCloseAllCharts;
    private JMenu mnuInsert;
    private TWMenuItem itmIndicatorsDialog;
    private TWMenu mnuBaseSymbol;
    private TWMenu mnuCompareSymbol;
    private TWMenuItem miChartNaviBar;
    private TWMenuItem miNewBase;
    private TWMenuItem miNewCompare;
    private JMenu mnuHelp;
    private TWMenuItem helpContent;
    private JMenu mnuProperties;
    private JMenu mnuIndicators;
    private JMenu mnuStratergies;
    private JMenu mnuPatterns;
    private TWMenu favPatternsMenu;
    private TWMenu favStrMenu;
    private TWMenu favIndMenu;
    private TWMenu mnuCI2;
    private JMenu mnuWindows;
    private TWMenuItem miWindowTile;
    private TWMenuItem miWindowTabbed;
    private TWMenuItem miTileHorizontal;
    private TWMenuItem miTileVertical;
    private TWMenu mnuMode;
    private TWMenu mnuPeriod;
    private TWMenu mnuInterval;
    private TWMenu mnuStyle;
    private TWCheckBoxMenuItem chkmiShowVolume;
    private TWCheckBoxMenuItem chkmiShowVolumeByPrice;
    private TWCheckBoxMenuItem chkmiShowTurnOver;
    private TWCheckBoxMenuItem chkmiIndexed;
    private TWMenuItem mnuZooming;
    private TWMenuItem miSemiLogScale;
    private TWCheckBoxMenuItem chkmiShowCurrentPriceLine;
    private TWCheckBoxMenuItem chkmiShowPreviousCloseLine;
    private TWCheckBoxMenuItem chkmiShowOrderLines;
    private TWCheckBoxMenuItem chkmiShowBidAskTags;
    private TWCheckBoxMenuItem chkmiShowMinMaxPriceLines;
    private TWCheckBoxMenuItem chkmiVWAP;
    private JMenu mnuTools;
    private TWMenuItem miCustomIndicatorBuilder;
    private TWMenuItem miChartOptions;
    private TWMenuItem miIAT;
    private TWMenuItem miSystememTester;
    private boolean showCrossHairs = false;
    private boolean showsnapBall = false;
    private JPanel topToolTabBar;
    private JPanel tabBar;
    private JPanel tabContainer;
    private JPanel testSideBarPnl;
    private boolean bIsTabbedMode = false;
    private JPopupMenu mnuTabPopup = new JPopupMenu();
    private TWMenuItem mnuCloseTab;
    private TWMenuItem mnuCloseAllButThis;
    private TWMenuItem mnuCloseAllTabs;
    private ViewSetting oSetting;

    private int totalChartCount = 0;
    private int proChartCount = 0;
    private int maxChartCount = 1000;

    private ChartFrame() {
        super();

        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        IndicatorBuilder.getInstance().addCustomIndicatorListener(this);
        long entryTime = System.currentTimeMillis();
        IndicatorFactory.rebuildAssembly();
        //System.out.println("**** chartframe Calc time " + (entryTime - System.currentTimeMillis()));
        long entryTimecreateui = System.currentTimeMillis();
        createUI();
        //System.out.println("**** chartframecreateui Calc time " + (entryTimecreateui - System.currentTimeMillis()));
        ChartInterface.setFirstTimeChartLoading(false);
        this.setBorder(GUISettings.getTWFrameBorder());
        Theme.registerComponent(this);
        long entryTimeaplytheme = System.currentTimeMillis();
        applyTheme();
        //System.out.println("**** chartframe Calc time " + (entryTimeaplytheme - System.currentTimeMillis()));
        currentArrangeMode = getDefaultArrangeMode();

        oSetting = ViewSettingsManager.getSummaryView("UNICHART");
        if (oSetting != null) {
            this.setSize(oSetting.getSize());
        } else {
            this.setSize(500, 500);
        }
        //this.setLocation(-1000, -1000); // invalidate the location so that it can be centered

        Point p = oSetting.getLocation();
        this.setLocation((int) p.getX(), (int) p.getY());
        oSetting.setParent(this);

        self = this;
        this.maxChartCount = ChartInterface.getMaximumChartCount();
    }

    public static ChartFrame getSharedInstance() {
        if (self == null) {
            self = new ChartFrame();
        }
        return self;
    }

    public static void setSemiLogScales(StockGraph g, Component aParent) {
        ArrayList panels = g.panels;
        g.refreshSemiLogStatus();
        Hashtable htResult = new Hashtable();
        CustomDialog dlg = CustomDialogs.getSemiLogScaleDialog(panels, htResult);
        GUISettings.applyOrientation(dlg);
        dlg.setLocationRelativeTo(aParent);
        dlg.setModal(true);
        dlg.pack();
        dlg.setResizable(false);
        dlg.setVisible(true);
        if (dlg.getModalResult()) {
            for (int i = 0; i < g.panels.size(); i++) {
                WindowPanel wp = (WindowPanel) g.panels.get(i);
                if (htResult.containsKey(wp)) {
                    wp.setSemiLog(((Boolean) htResult.get(wp)).booleanValue());
                }
            }
        }
        g.GDMgr.reCalculateGraphParams();
        g.repaint();
    }

    public static void insertBaseSymbol(GraphFrame gf) {
        String[] sa = ChartInterface.searchSymbols();
        if ((sa != null) && (sa.length > 0)) {

            // Modified 30-10-2007
            gf.setGraphBaseSymbol(sa[0]);
        }
    }

    public static void insertCompareSymbol(GraphFrame gf) {
        String[] sa = ChartInterface.searchSymbols();
        if ((sa != null) && (sa.length > 0)) {
            String sKey = sa[0];
            GraphFrame.insertToSymbolsEx(gf.g_oComparedSymbols, sKey);
            gf.setComparisonSymbol(sKey);
        }
        sa = null;
    }

    public static int getIndexOftheMenuItem(JMenu mnu, JMenuItem mi) {
        for (int i = 0; i < mnu.getItemCount(); i++) {
            JMenuItem mii = mnu.getItem(i);
            if (mii == mi) return i;
        }
        return -1;
    }

    public static int getIndexOftheMenuItem(TWMenu mnu, JMenuItem mi) {
        return getIndexOftheMenuItem((JMenu) mnu, mi);
    }

    public static TWTabbedPane getWindowTabs() {
        return windowTabs;
    }

    public static boolean isNull() {
        return self == null;
    }

    public int getMaxChartCount() {
        return maxChartCount;
    }

    public void setMaxChartCount(int maxChartCount) {
        this.maxChartCount = maxChartCount;
    }

    public int getTotalChartCount() {
        return totalChartCount;
    }

    public void setTotalChartCount(int totalChartCount) {
        this.totalChartCount = totalChartCount;
    }

    public int getProChartCount() {
        return proChartCount;
    }

    public void setProChartCount(int proChartCount) {
        this.proChartCount = proChartCount;
    }

    public boolean isDataWinVisible() {
        return dataWinVisible;
    }

    public void setDataWinVisible(boolean isVisible) {
        dataWinVisible = isVisible;
    }

    public void createUI() {

        pnlDesktopContainer = new JPanel();
        pnlDesktopContainer.setLayout(new BorderLayout());
        desktop = new JDesktopPane();
        desktop.addComponentListener(this);

        createMainMenu();
        setDetachable(true);
        setJMenuBar(menuBar);

        topToolBar = new GraphToolBar(this);
        bottomToolBar = new BottomToolBar(this);

        windowTabs = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Simulated, "dt");
        windowTabs.addTabPanelListener(this);
        windowTabs.addTabMouseListenerToTab(this);

        createTabPopup();

        GUISettings.applyOrientation(windowTabs);
        topToolTabBar = new JPanel(new BorderLayout());
        tabBar = new JPanel(new BorderLayout());
        tabContainer = new JPanel(new BorderLayout());
        tabContainer.add(windowTabs, BorderLayout.SOUTH);
        tabBar.add(tabContainer, BorderLayout.CENTER);

        tabBar.addMouseListener(this);
        topToolTabBar.add(topToolBar, BorderLayout.NORTH);


        getContentPane().setLayout(new BorderLayout());
        pnlDesktopContainer.add(tabBar, BorderLayout.NORTH);
        pnlDesktopContainer.add(desktop, BorderLayout.CENTER);
        getContentPane().add(pnlDesktopContainer, BorderLayout.CENTER);

        getContentPane().add(topToolTabBar, BorderLayout.NORTH);
        getContentPane().add(bottomToolBar, BorderLayout.SOUTH);


        testSideBarPnl = new JPanel();
        testSideBarPnl.setPreferredSize(new Dimension(100, desktop.getHeight()));

        if (Language.isLTR()) {
            getContentPane().add(ChartNavigationBar.getSharedInstance().createSideBar(), BorderLayout.WEST);
        } else {
            getContentPane().add(ChartNavigationBar.getSharedInstance().createSideBar(), BorderLayout.EAST);
        }


        setTitle(Language.getString("UNICHARTS"));
        GUISettings.applyOrientation(menuBar);
        Application.getInstance().addApplicationListener(this);
    }

    private void createMainMenu() {
        menuBar = new TWMenuBar();
        //File menu
        mnuFile = new JMenu(Language.getString("FILE"));
        mnuNew = new TWMenu(Language.getString("NEW"), "graph_New.GIF");
        miNewChart = new TWMenuItem(Language.getString("CHART"), "graph_Newchart.GIF");
        miNewLayout = new TWMenuItem(Language.getString("MENUITEM_LAYOUT"), "graph_Newlayout.GIF");
        miOpenChart = new TWMenuItem(Language.getString("OPEN_CHART"), "graph_Open.gif");
        miSaveChart = new TWMenuItem(Language.getString("SAVE_CHART"), "graph_Save.GIF");
        miSaveChartAs = new TWMenuItem(Language.getString("SAVE_CHART_AS"), "graph_Save As.GIF");
        miSetDefaults = new TWMenuItem(Language.getString("CHART_SET_DEFAULTS"), "graph_set_defaults.GIF");
        miPrintChart = new TWMenuItem(Language.getString("PRINT_CHART"), "graph_Print.GIF");
        miDetach = new TWMenuItem(Language.getString("DETACH"), "graph_Detach.GIF");
        miCloseChart = new TWMenuItem(Language.getString("CLOSE_CHART"), "graph_close.GIF");
        miCloseAllCharts = new TWMenuItem(Language.getString("CLOSE_ALL_CHARTS"), "graph_closeall.GIF");
        //Insert menu
        mnuInsert = new JMenu(Language.getString("INSERT"));
        mnuBaseSymbol = new TWMenu(Language.getString("BASE_SYMBOL"), "graph_base_Symbol.gif");
        mnuCompareSymbol = new TWMenu(Language.getString("COMPARE"), "graph_compare_Symbol.gif");
        miNewBase = new TWMenuItem(Language.getString("SEARCH_3_DOTS"));
        miNewCompare = new TWMenuItem(Language.getString("SEARCH_3_DOTS"));

        //Properties menu
        mnuProperties = new JMenu(Language.getString("PROPERTIES"));
        mnuMode = new TWMenu(Language.getString("GRAPH_MODE"), "graph_Mode.GIF");
        mnuPeriod = new TWMenu(Language.getString("PERIOD"), "graph_Period.GIF");
        mnuInterval = new TWMenu(Language.getString("INTERVAL"), "graph_Interval.GIF");
        mnuStyle = new TWMenu(Language.getString("STYLE"), "graph_Style.GIF");
        chkmiShowVolume = new TWCheckBoxMenuItem(Language.getString("SHOW_VOLUME"), "graph_show volume.GIF");
        chkmiShowVolumeByPrice = new TWCheckBoxMenuItem(Language.getString("SHOW_VOLUME_BY_PRICE"), "graph_show_volume_by_price.GIF");
        chkmiShowTurnOver = new TWCheckBoxMenuItem(Language.getString("SHOW_TURNOVER"), "graph_show volume.GIF");
        chkmiIndexed = new TWCheckBoxMenuItem(Language.getString("INDEXED"), "graph_Indexed.GIF");
        mnuZooming = new TWMenuItem(Language.getString("GRAPH_RESET_ZOOM"), "graph_Semi_Log.GIF");
        miSemiLogScale = new TWMenuItem(Language.getString("GRAPH_SEMI_LOG_SCALE"), "graph_Semi_Log.GIF");
        itmIndicatorsDialog = new TWMenuItem(Language.getString("INSERT_INDICATOR"), "graph_Indicators.GIF");

        chkmiShowCurrentPriceLine = new TWCheckBoxMenuItem(Language.getString("SHOW_CURRENT_PRICE_LINE"), "graph_current_price_line.GIF");
        chkmiShowPreviousCloseLine = new TWCheckBoxMenuItem(Language.getString("SHOW_PREVIOUS_CLOSE_LINE"), "graph_previous_close_line.GIF");
        chkmiShowOrderLines = new TWCheckBoxMenuItem(Language.getString("SHOW_ORDER_LINES"), "graph_order_lines.GIF");
        chkmiShowOrderLines.setEnabled(false);
        chkmiShowBidAskTags = new TWCheckBoxMenuItem(Language.getString("SHOW_BID_ASK_TAGS"), "graph_bidAsk_tags.GIF");
        chkmiShowMinMaxPriceLines = new TWCheckBoxMenuItem(Language.getString("SHOW_MIN_MAX_LINES"), "graph_minMax_lines.GIF");
        chkmiVWAP = new TWCheckBoxMenuItem(Language.getString("CHART_VWAP_SHOW"), "graph_vwap.GIF"); //TODO: crate image and put into the folder

        createIndicatorsMenu();

        addActionListeners();

        mnuHelp = new JMenu(Language.getString("HELP"));

        helpContent = new TWMenuItem(Language.getString("HELP_CONTENTS"), "helpcontents.gif");
        helpContent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_CHART"));
            }
        });


        miChartNaviBar = new TWMenuItem(Language.getString("SHOW_NAVIGATION_BAR"), "sidebar.gif");

        miChartNaviBar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                isNavigationBarVisible ^= true;
//                isNavigationBarVisible = !isNavigationBarVisible;
                refreshToolBars();
                ChartNavigationBar.getSharedInstance().controlSideBar();
            }
        });
        mnuWindows = new JMenu(Language.getString("WINDOWS"));

        miWindowTile = new TWMenuItem(Language.getString("TILE"), "tile.gif");
        miWindowTile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //removeTabIndexPanel();
                //bIsTabbedMode = false;
                currentArrangeMode = TILE_NORMALLY;
                tileWindows();

            }
        });

        miWindowTabbed = new TWMenuItem(Language.getString("TABBED"), "graph_tabbed.gif");
        miWindowTabbed.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //bIsTabbedMode = true;
                //addTabIndexpanel(bIsTabbedMode);
                //doMaxWindows();
                currentArrangeMode = TABBED;
                tileWindows();
            }
        });

        miTileHorizontal = new TWMenuItem(Language.getString("CO_TILE_HOR"), "tileH.gif");
        miTileHorizontal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                //tileWindowsHorizontally();
                currentArrangeMode = TILE_HORIZONTALLY;
                tileWindows();
            }
        });

        miTileVertical = new TWMenuItem(Language.getString("CO_TILE_VER"), "tileV.gif");
        miTileVertical.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                //tileWindowsVertically();
                currentArrangeMode = TILE_VERTICALLY;
                tileWindows();
            }
        });

        mnuTools = new JMenu(Language.getString("TOOLS"));
        miCustomIndicatorBuilder = new TWMenuItem(Language.getString("CUSTOM_INDICATOR_BUILDER"), "graph_custom_indicators.GIF");
        miCustomIndicatorBuilder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                IndicatorBuilder ciw = IndicatorBuilder.getInstance();
                ciw.showWindow();
                //ciw.getIndicatorBuilderPanel().getActionFrame().showWindow();
                //ActionFrame frame = ciw.getIndicatorBuilderPanel().getActionFrame();
                //ActionFrame frame = new ActionFrame(IndicatorBuilder.getInstance().getIndicatorBuilderPanel(), ChartInterface.getChartFrameParentComponent());
                //frame.showWindow();

            }
        });

        //added by charithn- chart options windows
        miChartOptions = new TWMenuItem(Language.getString("CHART_OPTIONS"), "graph_set_defaults.GIF");
        miChartOptions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ChartOptionsWindow.getSharedInstance(0).setVisible(true);
            }
        });

        miIAT = new TWMenuItem(Language.getString("INSERT_ANALYSIS_TECHNIQUE"));
        miIAT.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_IAT.gif"));
        miIAT.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AnalysisTechWindow.getSharedInstance().setVisible(true);
            }
        });

        miSystememTester = new TWMenuItem("Mubasher System Tester", "graph_custom_indicators.GIF");
        //miSystememTester.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/graph_custom_indicators.GIF"));
        miSystememTester.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BackTestingWindow window = BackTestingWindow.getSharedInstance();
                window.showWindow();
            }
        });

        mnuFile.add(mnuNew);
        mnuNew.add(miNewChart);
        mnuNew.add(miNewLayout);
        mnuFile.add(miOpenChart);
        mnuFile.add(miSaveChart);
        mnuFile.add(miSaveChartAs);
        mnuFile.add(new TWSeparator());
        mnuFile.add(miPrintChart);
        mnuFile.add(new TWSeparator());
        mnuFile.add(miDetach);
        mnuFile.add(new TWSeparator());
        mnuFile.add(miCloseChart);
        mnuFile.add(miCloseAllCharts);

        mnuBaseSymbol.add(miNewBase);
        mnuCompareSymbol.add(miNewCompare);
        mnuInsert.add(mnuBaseSymbol);

        mnuInsert.add(mnuCompareSymbol);
        mnuInsert.add(new TWSeparator());
        //mnuInsert.add(itmIndicatorsDialog); //removed the indicator dialog and added  insert analysis techniques window
        mnuInsert.add(miChartNaviBar);
        mnuTools.add(miCustomIndicatorBuilder);
        mnuTools.add(miChartOptions);
        mnuTools.add(miIAT);
        //mnuTools.add(miSystememTester);

        mnuProperties.add(mnuMode);
        mnuProperties.add(mnuPeriod);
        mnuProperties.add(mnuInterval);
        mnuProperties.add(new TWSeparator());
        mnuProperties.add(mnuStyle);
        mnuProperties.add(chkmiShowVolume);
        mnuProperties.add(chkmiShowVolumeByPrice);
        mnuProperties.add(chkmiShowTurnOver);
        mnuProperties.add(chkmiIndexed);
        mnuProperties.add(new TWSeparator());
        mnuProperties.add(miSemiLogScale);
        mnuProperties.add(new TWSeparator());
        mnuProperties.add(chkmiShowCurrentPriceLine);
        mnuProperties.add(chkmiShowPreviousCloseLine);
        mnuProperties.add(chkmiShowOrderLines);
        mnuProperties.add(chkmiShowBidAskTags);
        if (ChartInterface.isMinMaxEnabled()) mnuProperties.add(chkmiShowMinMaxPriceLines);
        if (ChartInterface.isVWAPEnabled()) mnuProperties.add(chkmiVWAP);

        mnuHelp.add(helpContent);

        mnuWindows.add(miWindowTile);
        mnuWindows.add(miWindowTabbed);
        mnuWindows.add(miTileHorizontal);
        mnuWindows.add(miTileVertical);

        menuBar.add(mnuFile);
        menuBar.add(mnuInsert);
        menuBar.add(mnuTools);
        menuBar.add(mnuProperties);

        menuBar.add(mnuIndicators);
        menuBar.add(mnuStratergies);
        menuBar.add(mnuPatterns);

        menuBar.add(mnuWindows);
        menuBar.add(mnuHelp);
    }

    public void refreshPatternsFavouritesMenu() {

        mnuPatterns.remove(favPatternsMenu);

        String iconPath = "images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif";
        favPatternsMenu = new TWMenu(Language.getString("FAVOURITE"), iconPath);
        mnuPatterns.add(favPatternsMenu, 0);
        for (final String stratName : IndicatorDetailStore.getalFaviouritePatterns()) {
            TWMenuItem miInd = new TWMenuItem(stratName);
            miInd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Class stratClass = activeGraph.getIndicatorClassByName(stratName, IndicatorDetailStore.alPatterns);
                    activeGraph.graph.GDMgr.addCustomIndicator(stratName, stratClass, false);
                }
            });
            miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            favPatternsMenu.add(miInd);
        }

        GUISettings.applyOrientation(favPatternsMenu);
    }

    public void refreshStratergiesFavouriteMenu() {

        mnuStratergies.remove(favStrMenu);

        String iconPath = "images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif";
        favStrMenu = new TWMenu(Language.getString("FAVOURITE"), iconPath);
        mnuStratergies.add(favStrMenu, 0);
        for (final String stratName : IndicatorDetailStore.getalFavouriteStrategies()) {
            TWMenuItem miInd = new TWMenuItem(stratName);
            miInd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Class stratClass = activeGraph.getIndicatorClassByName(stratName, IndicatorDetailStore.alStrategies);
                    activeGraph.graph.GDMgr.addCustomIndicator(stratName, stratClass, false);
                }
            });
            miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            favStrMenu.add(miInd);
        }
        GUISettings.applyOrientation(favStrMenu);

    }

    public void refreshIndicatorsFavouriteMenu() {

        mnuIndicators.remove(favIndMenu);

        String iconPath = "images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif";
        favIndMenu = new TWMenu(Language.getString("FAVOURITE"), iconPath);
        //if (!Language.isLTR()) {
        favIndMenu.setHorizontalTextPosition(JButton.TRAILING);
        favIndMenu.setHorizontalAlignment(JButton.LEADING);
        // }

        mnuIndicators.add(favIndMenu, 0);
        //add faviourite normal indicators
        for (int i = 0; i < IndicatorDetailStore.getalFavouiteIndicators().size(); i++) {
            final String indicatorName = IndicatorDetailStore.getalFavouiteIndicators().get(i);
            TWMenuItem item = new TWMenuItem(indicatorName);
            favIndMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = activeGraph.indicatorMap.get(indicatorName);
                    activeGraph.indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        //add faviourite custom indicators
        for (int i = 0; i < IndicatorDetailStore.getalFaviouriteCustIndicators().size(); i++) {
            final String custIndicatorName = IndicatorDetailStore.getalFaviouriteCustIndicators().get(i);
            //Todo if custom indicators include a separator
            TWMenuItem item = new TWMenuItem(custIndicatorName);
            favIndMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Class custIndClass = activeGraph.getIndicatorClassByName(custIndicatorName, IndicatorDetailStore.CustomIndicators);
                    activeGraph.graph.GDMgr.addCustomIndicator(custIndicatorName, custIndClass, false);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        GUISettings.applyOrientation(favIndMenu);
    }

    public void refreshCustomIndicatorsMenu() {

        mnuIndicators.remove(mnuCI2);
        String iconPath = "images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif";

        //custom indicators
        mnuCI2 = new TWMenu(Language.getString("CUSTOM_INDICATORS"), iconPath);
        mnuIndicators.add(mnuCI2, 4);
        for (final IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
            TWMenuItem miInd = new TWMenuItem(imo.getIndicatorName());
            miInd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    activeGraph.graph.GDMgr.addCustomIndicator(imo.getIndicatorName(), imo.getIndicatorClass(), false);
                }
            });
            miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            mnuCI2.add(miInd);
        }

        GUISettings.applyOrientation(mnuCI2);
    }

    public void refreshMainMenus(boolean graphMode) {

        mnuIndicators.setEnabled(graphMode);
        mnuStratergies.setEnabled(graphMode && ChartInterface.isSystemStrategiesEnabled());
        mnuPatterns.setEnabled(graphMode && ChartInterface.isSystemPatternsEnabled());
        mnuInsert.setEnabled(graphMode);
        mnuTools.setEnabled(graphMode);
        mnuProperties.setEnabled(graphMode);
    }

    private void createIndicatorsMenu() {
        mnuIndicators = new JMenu(Language.getString("INDICATORS"));
        String iconPath = "images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif";
        /************************************Add Favourite indicators menu ******************/
        favIndMenu = new TWMenu(Language.getString("FAVOURITE"), iconPath);

        for (int i = 0; i < IndicatorDetailStore.getalFavouiteIndicators().size(); i++) {
            final String indicatorName = IndicatorDetailStore.getalFavouiteIndicators().get(i);
            TWMenuItem item = new TWMenuItem(indicatorName);
            favIndMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = activeGraph.indicatorMap.get(indicatorName);
                    activeGraph.indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        //add faviourite custom indicators
        for (int i = 0; i < IndicatorDetailStore.getalFaviouriteCustIndicators().size(); i++) {
            final String custIndicatorName = IndicatorDetailStore.getalFaviouriteCustIndicators().get(i);
            //Todo if custom indicators include a separator
            TWMenuItem item = new TWMenuItem(custIndicatorName);
            favIndMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Class custIndClass = activeGraph.getIndicatorClassByName(custIndicatorName, IndicatorDetailStore.CustomIndicators);
                    activeGraph.graph.GDMgr.addCustomIndicator(custIndicatorName, custIndClass, false);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        mnuIndicators.add(favIndMenu);
        /************************************Add Averages indictor menu ******************/
        TWMenu averagesMenu = new TWMenu(Language.getString("AVERAGES"), iconPath);
        mnuIndicators.add(averagesMenu);
        Arrays.sort(GraphFrame.averageIndicatorNames);
        for (int i = 0; i < GraphFrame.averageIndicatorNames.length; i++) {
            final String indicatorName = GraphFrame.averageIndicatorNames[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            averagesMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = activeGraph.indicatorMap.get(indicatorName);
                    activeGraph.indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        TWMenu bandsMenu = new TWMenu(Language.getString("BANDS"), iconPath);
        mnuIndicators.add(bandsMenu);
        Arrays.sort(GraphFrame.bandsIndicatorNames);
        for (int i = 0; i < GraphFrame.bandsIndicatorNames.length; i++) {
            final String indicatorName = GraphFrame.bandsIndicatorNames[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            bandsMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = activeGraph.indicatorMap.get(indicatorName);
                    activeGraph.indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

//        TWMenu marketIndMenu = new TWMenu(Language.getString("BANDS"), iconPath);
        TWMenu marketIndMenu = new TWMenu(Language.getString("MARKET_INDICATOR"), iconPath);
        mnuIndicators.add(marketIndMenu);
        Arrays.sort(GraphFrame.marketIndicatorNames);
        for (int i = 0; i < GraphFrame.marketIndicatorNames.length; i++) {
            final String indicatorName = GraphFrame.marketIndicatorNames[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            marketIndMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = activeGraph.indicatorMap.get(indicatorName);
                    activeGraph.indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        TWMenu indicatorsMenu = new TWMenu(Language.getString("OTHER_INDICATORS"), iconPath);
        int menuRows = GraphFrame.otherIndicators.length / 2;
        VerticalGridLayout menuGrid = new VerticalGridLayout(menuRows, 2);
        indicatorsMenu.getPopupMenu().setLayout(menuGrid);
        mnuIndicators.add(indicatorsMenu);
        Arrays.sort(GraphFrame.otherIndicators);
        for (int i = 0; i < GraphFrame.otherIndicators.length; i++) {
            if (GraphFrame.otherIndicators[i].trim().equals("")) continue;
            if (GraphFrame.otherIndicators[i].trim().equals(Language.getString("IND_NET_ORDERS").trim()) || GraphFrame.otherIndicators[i].trim().equals(Language.getString("IND_NET_TURNOVER").trim())) {
                continue;
            }
            final String indicatorName = GraphFrame.otherIndicators[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            indicatorsMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = activeGraph.indicatorMap.get(indicatorName);
                    activeGraph.indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        //custom indicators
        mnuCI2 = new TWMenu(Language.getString("CUSTOM_INDICATORS"), iconPath);
        for (final IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
            TWMenuItem miInd = new TWMenuItem(imo.getIndicatorName());
            miInd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    activeGraph.graph.GDMgr.addCustomIndicator(imo.getIndicatorName(), imo.getIndicatorClass(), false);
                }
            });
            miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            mnuCI2.add(miInd);
        }

        mnuIndicators.add(mnuCI2);

        TWMenu allMenu = new TWMenu(Language.getString("ALL"), iconPath);
        String[] sa = GraphFrame.getIndicators();
        menuRows = sa.length / 2;
        menuGrid = new VerticalGridLayout(menuRows, 2);
        allMenu.getPopupMenu().setLayout(menuGrid);
        mnuIndicators.add(allMenu);
        if (sa != null) {
            for (int i = 0; i < sa.length; i++) {
                if (sa[i].trim().equals("")) continue;
                if (sa[i].trim().equals(Language.getString("IND_NET_ORDERS").trim()) || sa[i].trim().equals(Language.getString("IND_NET_TURNOVER").trim())) {
                    continue;
                }
                TWMenuItem item = new TWMenuItem(sa[i]);
                allMenu.add(item);
                final String indicatorName = sa[i]; //Get the indicator name to search in hashmap(need for sorting) - Pramoda
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Integer ObjInt = activeGraph.indicatorMap.get(indicatorName); // get the original index after sorting - Pramoda
                        activeGraph.indicatorClicked(e.getSource(), ObjInt);
                    }
                });
                item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            }
        }

        mnuStratergies = new JMenu(Language.getString("STRATEGIES"));

        //stratergy favourites
        favStrMenu = new TWMenu(Language.getString("FAVOURITE"), iconPath);
        for (final String stratName : IndicatorDetailStore.getalFavouriteStrategies()) {
            TWMenuItem miInd = new TWMenuItem(stratName);
            miInd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Class stratClass = activeGraph.getIndicatorClassByName(stratName, IndicatorDetailStore.alStrategies);
                    activeGraph.graph.GDMgr.addCustomIndicator(stratName, stratClass, false);
                }
            });
            miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            favStrMenu.add(miInd);
        }

        mnuStratergies.add(favStrMenu);

        for (final IndicatorMenuObject imo : IndicatorDetailStore.alStrategies) {
            TWMenuItem miInd = new TWMenuItem(imo.getIndicatorName());
            miInd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    activeGraph.graph.GDMgr.addCustomIndicator(imo.getIndicatorName(), imo.getIndicatorClass(), false);
                }
            });
            miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            mnuStratergies.add(miInd);
        }


        mnuPatterns = new JMenu(Language.getString("PATTERNS"));

        //patterns favourites
        favPatternsMenu = new TWMenu(Language.getString("FAVOURITE"), iconPath);
        for (final String stratName : IndicatorDetailStore.getalFaviouritePatterns()) {
            TWMenuItem miInd = new TWMenuItem(stratName);
            miInd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Class stratClass = activeGraph.getIndicatorClassByName(stratName, IndicatorDetailStore.alPatterns);
                    activeGraph.graph.GDMgr.addCustomIndicator(stratName, stratClass, false);
                }
            });
            miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            favPatternsMenu.add(miInd);
        }

        mnuPatterns.add(favPatternsMenu);

        for (final IndicatorMenuObject imo : IndicatorDetailStore.alPatterns) {
            TWMenuItem miInd = new TWMenuItem(imo.getIndicatorName());
            miInd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    activeGraph.graph.GDMgr.addCustomIndicator(imo.getIndicatorName(), imo.getIndicatorClass(), false);
                }
            });
            miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            mnuPatterns.add(miInd);
        }
    }

    private void addActionListeners() {
        //File Menu
        miNewChart.addActionListener(this);
        miNewLayout.addActionListener(this);
        miOpenChart.addActionListener(this);
        miSaveChart.addActionListener(this);
        miSaveChartAs.addActionListener(this);
        miSetDefaults.addActionListener(this);
        miPrintChart.addActionListener(this);
        miDetach.addActionListener(this);
        miCloseChart.addActionListener(this);
        miCloseAllCharts.addActionListener(this);
        //Insert Menu
        miNewBase.addActionListener(this);
        miNewCompare.addActionListener(this);
        //Properties Menu
        chkmiShowVolume.addActionListener(this);
        chkmiShowVolumeByPrice.addActionListener(this);
        chkmiShowTurnOver.addActionListener(this);
        chkmiIndexed.addActionListener(this);
        miSemiLogScale.addActionListener(this);
        //PopupmenuListeners
        mnuFile.addMenuListener(this);
        mnuInsert.addMenuListener(this);
        mnuProperties.addMenuListener(this);
        chkmiShowCurrentPriceLine.addActionListener(this);
        chkmiShowPreviousCloseLine.addActionListener(this);
        chkmiShowOrderLines.addActionListener(this);
        chkmiShowBidAskTags.addActionListener(this);
        chkmiShowMinMaxPriceLines.addActionListener(this);
        chkmiVWAP.addActionListener(this);

        itmIndicatorsDialog.addActionListener(this);
    }

    public void applyCustomColors(ThemeData optionData) {
        JInternalFrame[] frames = desktop.getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) { //uditha
                try {
                    ((GraphFrame) frames[i]).
                            applyCustomColors(optionData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void doMaxWindows() {
        JInternalFrame[] frames = desktop.getAllFrames();

        // count frames that aren't iconized
        int frameCount = 0;
        int len = windowTabs.getTabCount();
        for (int i = 0; i < len; i++) {
            windowTabs.removeTab(0);
        }
        try {
            for (int i = 0; i < frames.length; i++) {
                if (!(frames[i] instanceof GraphFrame)) {
                    continue;
                }
                if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) { //uditha
                    //becuae this happens in tabbed pane
                    //frames[i].setSize(desktop.getWidth(), desktop.getHeight());

                    if (isBIsTabbedMode() && (!((GraphFrame) frames[i]).isGraphDetached())) {
                        ((GraphFrame) frames[i]).setTitleVisible(false);
                    }
                    //frames[i].setBounds(desktop.getBounds());
                    //frames[i].setBounds(getDesktop().getBounds());
                    frames[i].setMaximum(true);

                } else if (frames[i].isIcon()) {
                    frames[i].setMaximum(true);
                    ((GraphFrame) frames[i]).setTitleVisible(false);
                }

                windowTabs.addTab(frames[i].getTitle(), frames[i]);
                ((GraphFrame) frames[i]).setGraphTitle();
            }
            windowTabs.setSelectedIndex(0);
            windowTabs.repaint();
            windowTabs.updateUI();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHeadersVisible() {
        JInternalFrame[] frames = desktop.getAllFrames();

        // count frames that aren't iconized
        int frameCount = 0;

        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) { //uditha
                try {
                    //becuae this happens in tabbed pane
                    if (isBIsTabbedMode() && (!((GraphFrame) frames[i]).isGraphDetached())) {
                        ((GraphFrame) frames[i]).setTitleVisible(false);
                    }

                    frames[i].setMaximum(true);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void tileWindows() {

        bIsTabbedMode = false;
        if (currentArrangeMode == TILE_NORMALLY) {
            addTabIndexpanel(false);
            tileWindowsNormally();
        } else if (currentArrangeMode == TABBED) {
            bIsTabbedMode = true;
            addTabIndexpanel(true);
            //desktop.setLocation(0, 0);
            doMaxWindows();
        } else if (currentArrangeMode == TILE_HORIZONTALLY) {
            addTabIndexpanel(false);
            tileWindowsHorizontally();
        } else if (currentArrangeMode == TILE_VERTICALLY) {
            addTabIndexpanel(false);
            tileWindowsVertically();
        }
    }

    public byte getDefaultArrangeMode() {
        if (ChartOptions.getCurrentOptions().chart_window_tile == ChartOptions.WindowTileMode.TILE_NORMAL) {
            return TILE_NORMALLY;
        } else if (ChartOptions.getCurrentOptions().chart_window_tile == ChartOptions.WindowTileMode.TABBED) {
            return TABBED;
        } else if (ChartOptions.getCurrentOptions().chart_window_tile == ChartOptions.WindowTileMode.TILE_HORIZONTAL) {
            return TILE_HORIZONTALLY;
        } else if (ChartOptions.getCurrentOptions().chart_window_tile == ChartOptions.WindowTileMode.TILE_VERTICAL) {
            return TILE_VERTICALLY;
        }
        return TILE_NORMALLY;
    }

    public void tileWindowsNormally() {

        JInternalFrame[] frames = desktop.getAllFrames();

        // count frames that aren't iconized
        int frameCount = 0;
        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) { //uditha
                frameCount++;
            }
        }

        if (frameCount <= 0) return;

        int rows = (int) Math.sqrt(frameCount);
        int cols = frameCount / rows;
        int extra = frameCount % rows;
        // number of columns with an extra row

        int width = desktop.getWidth() / cols;
        int height = desktop.getHeight() / rows;
        int r = 0;
        int c = 0;
        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) { //uditha
                try {
                    frames[i].setMaximum(false);
                    ((GraphFrame) frames[i]).setTitleVisible(true);
                    frames[i].reshape(c * width,
                            r * height, width, height);
                    r++;
                    if (r == rows) {
                        r = 0;
                        c++;
                        if (c == cols - extra) {  // start adding an extra row
                            rows++;
                            height = desktop.getHeight() / rows;
                        }
                    }
                } catch (PropertyVetoException e) {
                }
            }
        }
    }

    public void tileWindowsVertically() {

        JInternalFrame[] frames = desktop.getAllFrames();

        // count frames that aren't iconized
        int frameCount = 0;
        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) { //uditha
                frameCount++;
            }
        }

        if (frameCount <= 0) return;

        int width = desktop.getWidth() / frameCount;
        int height = desktop.getHeight();
        int c = 0;
        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) { //uditha
                try {
                    frames[i].setMaximum(false);
                    ((GraphFrame) frames[i]).setTitleVisible(true);
                    frames[i].reshape(c * width, 0, width, height);
                    c++;
                } catch (PropertyVetoException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void tileWindowsHorizontally() {

        JInternalFrame[] frames = desktop.getAllFrames();

        // count frames that aren't iconized
        int frameCount = 0;
        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) {
                frameCount++;
            }
        }

        if (frameCount <= 0) return;

        int width = desktop.getWidth();
        int height = desktop.getHeight() / frameCount;

        int r = 0;
        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) {
                try {
                    frames[i].setMaximum(false);
                    ((GraphFrame) frames[i]).setTitleVisible(true);
                    frames[i].reshape(0, r * height, width, height);
                    r++;
                } catch (PropertyVetoException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setHeaderVisible() {
        GraphFrame[] frames = (GraphFrame[]) desktop.getAllFrames();
        // count frames that aren't iconized
        int frameCount = 0;
        for (int i = 0; i < frames.length; i++) {
            if ((!frames[i].isIcon()) && (!(frames[i] instanceof NonResizeable))) { //uditha
                frameCount++;
            }
        }
    }

    public void show() {

        super.show();
        bottomToolBar.assignCardLengths();
        bottomToolBar.setTextSymbolIcons();
        refreshToolBars();
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == miNewChart) {
            if (this.totalChartCount >= this.maxChartCount) {
                String message = "Graph limit exceeded. Maximum %s graphs allowed.";
                message = String.format(message, this.maxChartCount);
                JOptionPane.showConfirmDialog(this, message, "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            } else {
                addNewGraph(true, false);
                tileWindows();
            }
        } else if (obj == miNewLayout) {
            createLayout();
        } else if (obj == miOpenChart) {
            openChart();
        } else if (obj == miCloseAllCharts) {
            closeAll();
        } else if (obj == miSetDefaults) {
            setDefaults();
        } else if (activeGraph != null) {
            //File Menu
            if (obj == miSaveChart)
                activeGraph.saveClicked(false);
            else if (obj == miSaveChartAs)
                activeGraph.saveClicked(true);
            else if (obj == miPrintChart)
                activeGraph.printChart();
            else if (obj == miDetach)
                activeGraph.toggleDetach();
            else if (obj == miCloseChart)
                closeActiveChart();
                //Insert Menu
            else if (obj == miNewBase)
                insertBaseSymbol(activeGraph);
            else if (obj == miNewCompare)
                insertCompareSymbol(activeGraph);
                //Properties Menu
            else if (obj == chkmiShowVolume)
                activeGraph.toggleShowVolume();
            else if (obj == chkmiShowVolumeByPrice)
                activeGraph.toggleShowVolumeByPrice();
            else if (obj == chkmiShowTurnOver)
                activeGraph.toggleShowTurnOver();
            else if (obj == chkmiIndexed)
                activeGraph.toggleIndexed();
            else if (obj == miSemiLogScale) {
                setSemiLogScales(activeGraph.graph, desktop);
            } else if (obj == chkmiShowCurrentPriceLine) {
                activeGraph.graph.GDMgr.toggleShowCurrentPriceLine();
            } else if (obj == chkmiShowPreviousCloseLine) {
                activeGraph.graph.GDMgr.toggleShowPreviousCloseLine();
            } else if (obj == chkmiShowOrderLines) {
                activeGraph.graph.GDMgr.toggleShowOrderLines();
            } else if (obj == chkmiShowBidAskTags) {
                activeGraph.graph.GDMgr.toggleShowBidAskTags();
            } else if (obj == chkmiShowMinMaxPriceLines) {
                activeGraph.graph.GDMgr.toggleShowMinMaxPriceLines();
            } else if (obj == chkmiVWAP) {
                activeGraph.graph.GDMgr.toggleShowVWAP();
            } else if (obj == itmIndicatorsDialog) {
                if (!ChartInterface.isIndicatorsEnabled()) {
                    new ShowMessage(Language.getString("MSG_NOT_SUBSCRIBED_FOR_INDICATORS"), "I");
                    return;
                }
                CustomDialog cdl = CustomDialogs.getInsertIndicatorsDialog(GraphFrame.getIndicators(),
                        GraphFrame.getIndicatorStringAndID(), activeGraph.graph.GDMgr.getSources(),
                        activeGraph.graph.isShowingVolumeGrf(), activeGraph.graph.volumeRect);
                cdl.setModal(true);
                cdl.pack();
                cdl.setResizable(false);
                cdl.setVisible(true);
                Object result = cdl.result;
                if (result != null) {
                    byte[] selectedIndicator = (byte[]) result;
                    activeGraph.graph.GDMgr.addIndicator(selectedIndicator[0],
                            selectedIndicator[1], selectedIndicator[2]);
                }
            }
        }
        refreshToolBars();
    }

    public void createLayout() {
        JInternalFrame[] graphFrames = desktop.getAllFrames();
        CustomDialog dlg = CustomDialogs.getCreateLayoutDialog(graphFrames);
        GUISettings.applyOrientation(dlg);
        dlg.setModal(true);
        //dlg.pack();
        dlg.setSize(660, 400);
        dlg.setVisible(true);
        if (dlg.getModalResult() && (dlg.result != null)) {
            Vector<JInternalFrame> selectedFrames = (Vector<JInternalFrame>) dlg.result;
            addChartsToLayout(selectedFrames, true);
        }

    }

    public void addChartsToLayout(Vector<JInternalFrame> selectedFrames, boolean isNew) {
        if ((selectedFrames != null) && (selectedFrames.size() > 0)) {
            String uniqueID = "L" + System.currentTimeMillis();
            int randNum = 15 + (int) Math.round(80 * Math.random());
            uniqueID += randNum;
            layouts.put(uniqueID, selectedFrames);
            for (int i = 0; i < selectedFrames.size(); i++) {
                GraphFrame gf = (GraphFrame) selectedFrames.get(i);
                if (isNew) {
                    gf.addLayout(uniqueID, currentLayout);
                } else {
                    gf.addLayout(uniqueID, -1);
                }
            }
            if (isNew) currentLayout++;
        }
    }

    public Hashtable<String, Vector> getLayouts() {
        return layouts;
    }

    public Vector getLayoutForID(String uniqueID) {
        return layouts.get(uniqueID);
    }

    public void closeAll() {
        JInternalFrame[] frames = desktop.getAllFrames();
        if (frames != null)
            for (int i = 0; i < frames.length; i++) {
                //((GraphFrame) frames[i]).closeWindow();
                if (frames[i] instanceof GraphFrame) {
                    ((GraphFrame) frames[i]).closeWindow();
                } else {
                    frames[i].doDefaultCloseAction();
                }
            }
    }

    public void closeGivenChart(Component comp) {
        int tabCount = windowTabs.getTabCount();
        for (int i = 0; i < tabCount; i++) {
            Component frame = windowTabs.getTabComponentAt(i);
            if (frame != null) {
                if (frame.equals(comp)) {
                    ((GraphFrame) frame).closeWindow();
                }
            }
        }
    }

    public void closeActiveChart() {
        activeGraph.closeWindow();
    }

    public GraphFrame addNewGraph(boolean show, boolean showBlankGraph) {
        //sathchartTab
        String[] sa = new String[0];
        String sKey = null;
        if (!showBlankGraph) {
            sa = ChartInterface.searchSymbols();
            if ((sa != null) && (sa.length > 0)) {
                sKey = sa[0];
            } else {
                return null;
            }
        }

        activeGraph = new GraphFrame(this);
        ViewSetting viewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("OUTER_CHART").getObject();
        viewSetting.setID("" + System.currentTimeMillis());
        activeGraph.setResizable(true);
        activeGraph.setMaximizable(true);
        activeGraph.setIconifiable(true);
        activeGraph.setClosable(true);
        activeGraph.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        activeGraph.addDataWindowListener(this);
        activeGraph.setLayer(GUISettings.TOP_LAYER);
        getDesktop().setLayer(activeGraph, GUISettings.TOP_LAYER);
        desktop.add(activeGraph);

        desktop.validate();
        GUISettings.setColumnSettings(viewSetting, ChartInterface.getColumnSettings("CHART_DATA_COLS"));
        activeGraph.setViewSettings(viewSetting);
//        windowTabs.addTab(activeGraph.getTitle(), activeGraph);
        GUISettings.applyOrientation(activeGraph);
        /*if (bIsTabbedMode) {
            doMaxWindows();
        } else {
            removeTabIndexPanel();
            tileWindows();
        }*/

        viewSetting = null;
        if (!showBlankGraph) {
            GraphFrame.insertToSymbolsEx(activeGraph.g_oBaseSymbols, sKey);
            TemplateFactory.LoadTemplate(activeGraph, new File(TemplateFactory.DEFAULT_TEMPLATE_FILE), sKey,
                    activeGraph.isGraphDetached());
        }
        //patch
        Graphics g = activeGraph.graph.getGraphics();
        if (g != null) {
            //activeGraph.graph.titleHeight = g.getFontMetrics(activeGraph.graph.graphTitleFont).getHeight();
            //activeGraph.graph.GDMgr.titleHeight = g.getFontMetrics(activeGraph.graph.graphTitleFont).getHeight();
        }
        activeGraph.graph.reDrawAll();
        //tileWindows(); //TODO: important here.
        this.proChartCount++;
        if (this.isVisible()) {
            this.totalChartCount++;
        }
        return activeGraph;
    }

    public GraphFrame addNewGraph(boolean show, boolean showBlankGraph, String sKey) {
        if (this.totalChartCount >= this.maxChartCount) {
            String message = "Graph limit exceeded. Maximum %s graphs allowed.";
            message = String.format(message, this.maxChartCount);
            JOptionPane.showConfirmDialog(this, message, "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            return null;
        } else {
            //sathchartTab
            activeGraph = new GraphFrame(this);
            ViewSetting viewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("OUTER_CHART").getObject();
            viewSetting.setID("" + System.currentTimeMillis());
            activeGraph.setResizable(true);
            activeGraph.setMaximizable(true);
            activeGraph.setIconifiable(true);
            activeGraph.setClosable(true);
            activeGraph.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            activeGraph.addDataWindowListener(this);
            activeGraph.setLayer(GUISettings.TOP_LAYER);
            getDesktop().setLayer(activeGraph, GUISettings.TOP_LAYER);
            desktop.add(activeGraph);

            desktop.validate();
            GUISettings.setColumnSettings(viewSetting, ChartInterface.getColumnSettings("CHART_DATA_COLS"));
            activeGraph.setViewSettings(viewSetting);
            GUISettings.applyOrientation(activeGraph);

            viewSetting = null;

            if (!showBlankGraph) {
                GraphFrame.insertToSymbolsEx(activeGraph.g_oBaseSymbols, sKey);
                TemplateFactory.LoadTemplate(activeGraph, new File(TemplateFactory.DEFAULT_TEMPLATE_FILE), sKey,
                        activeGraph.isGraphDetached());
            }
            tileWindows();
            this.proChartCount++;
            this.totalChartCount++;
            return activeGraph;
        }
    }

    public void toggleShowDataWindow() {
        dataWinVisible ^= true;
//        dataWinVisible = !dataWinVisible;
        if (dataWinVisible) {
            if ((dataWindow == null) || (!dataWindow.isDisplayable())) {
                dataWindow = new DataWindow(this);

                ChartFrame.getSharedInstance().getDesktop().add(dataWindow);
                ChartFrame.getSharedInstance().getDesktop().setLayer(dataWindow, 1000);
            }
            dataWindow.setVisible(true);
        } else {
            if (dataWindow != null) {
                Rectangle r = dataWindow.getBounds();
                dataWindow.dispose();
                dataWindow = null;
                this.repaint(r);
            }
        }
        refreshTopToolbar();
    }

    public void toggleCrossHair() {
        if (showCrossHairs) {
            showCrossHairs = false;
            for (JInternalFrame frame : desktop.getAllFrames()) {
                if (frame instanceof GraphFrame) {
                    GraphFrame graphFrame = (GraphFrame) frame;
                    graphFrame.graph.GDMgr.crossHairX = -1;
                    graphFrame.graph.GDMgr.crossHairY = -1;
                    graphFrame.graph.GDMgr.setLegendPriceDisplayempty();
                    graphFrame.repaint();
                }
            }
        } else {
            showCrossHairs = true;
        }

    }

    public void toggleSnapBall() {
        if (showsnapBall) {
            showsnapBall = false;
            for (JInternalFrame frame : desktop.getAllFrames()) {
                if (frame instanceof GraphFrame) {
                    GraphFrame graphFrame = (GraphFrame) frame;
                    graphFrame.graph.GDMgr.pinBallX = -1;
                    graphFrame.graph.GDMgr.setLegendPriceDisplayempty();

                    graphFrame.repaint();
                }
            }
        } else {
            showsnapBall = true;
        }

    }

    public void setDefaults() {
        //IMPORTANT
        ChartProperties cp = ChartSettings.getSettings().getDefaultCP(null, "", GraphDataManager.ID_BASE, Color.BLACK, null);
        CustomDialog cdlg = CustomDialogs.getDefaultPropertiesDialog(cp, activeGraph);
        cdlg.setLocationRelativeTo(this);
        cdlg.setModal(true);
        cdlg.setVisible(true);
    }

    /**
     * *****************************************************************************************************************************
     */
    public void openChart() {
        GraphStream gs = null;
        //String filePath = GraphFrame.openChartFile(this);//"tmpSave.txt";
        String filePath = GraphFrame.openChartFile(ChartInterface.getChartFrameParentComponent());//"tmpSave.txt";
        if (filePath == null) return;
        filePath = filePath.trim();
        File f = new File(filePath);
        if (this.totalChartCount >= this.maxChartCount) {
            String message = "Graph limit exceeded. Maximum %s graphs allowed.";
            message = String.format(message, this.maxChartCount);
            JOptionPane.showConfirmDialog(this, message, "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
        } else {
            if (filePath.endsWith(".cht")) {
                Vector<JInternalFrame> gfs = new Vector<JInternalFrame>();
                LayoutFactory.openChtFile(filePath, gfs, false, null);
                tileWindows();
            } else {  // ".mcl"
                Vector<JInternalFrame> selectedFrames = new Vector<JInternalFrame>();
                if (LayoutFactory.LoadLayout(f, selectedFrames, false, null, false)) {
                    //adding new layout to layouts
                    addChartsToLayout(selectedFrames, false);
                } else {
                    JOptionPane.showMessageDialog(this, Language.getString("MSG_INVALID_CHART_LAYOUT"),
                            Language.getString("OPEN_CHART"), JOptionPane.WARNING_MESSAGE);
                }
            }
        }


    }

    public GraphFrame getActiveGraph() {
        if (activeGraph == null) {
            if (getDesktop().getAllFrames().length > 0) {

                if (getDesktop().getAllFrames()[0] instanceof GraphFrame) {
                    activeGraph = (GraphFrame) getDesktop().getAllFrames()[0];
                }
            }
        }
        return activeGraph;
    }

    public void setActiveGraph(GraphFrame gf) {
        activeGraph = gf;
        topToolBar.setActiveGraph(gf);
        bottomToolBar.setActiveGraph(gf);
        refreshToolBars();
    }

    public void updateUI() {
        super.updateUI();
        this.setBorder(GUISettings.getTWFrameBorder());
    }

    /**
     * Invoked when the component's size changes.
     */
    public void componentResized(ComponentEvent e) {

        this.getDesktopPane().repaint();
        if (!isBIsTabbedMode()) {
            tileWindows();
        } else {

        }
    }

    /**
     * Invoked when the component's position changes.
     */
    public void componentMoved(ComponentEvent e) {
    }

    /**
     * Invoked when the component has been made visible.
     */
    public void componentShown(ComponentEvent e) {
    }

    /**
     * Invoked when the component has been made invisible.
     */
    public void componentHidden(ComponentEvent e) {
    }

    public String toString() {
        return Language.getString("GRAPH");
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void closeWindow() {

        TWDesktop.doCloseWindow(this);
        focusNextWindow();
    }

    public void doDefaultCloseAction() {

        TWDesktop.doCloseWindow(this);
        focusNextWindow();
        this.totalChartCount -= this.proChartCount;
    }

    private void focusNextWindow() {
        TWDesktop desktop = (TWDesktop) Client.getInstance().getDesktop();
        desktop.focusNextWindow(desktop.getListPosition(this, true), true);
        desktop = null;
    }

    //############################################################
    //implementing MenuListener

    public JDesktopPane getDesktop() {
        return desktop;
    }

    public int getDesktopItemType() {
        return BOARD_TYPE;
    }

    public int getWindowType() {
        return windowType;
    }
    //############################################################
    //end implementing MenuListener

    public void setWindowType(int type) {
        windowType = type;
    }

    /**
     * Invoked when a menu is selected.
     *
     * @param e a MenuEvent object
     */
    public void menuSelected(MenuEvent e) {
        Object obj = e.getSource();
        if (obj == mnuFile)
            updateFileMenu();
        else if (obj == mnuInsert)
            updateInsertMenu();
        else if (obj == mnuProperties) updatePropertiesMenu();
    }

    /**
     * Invoked when the menu is deselected.
     *
     * @param e a MenuEvent object
     */
    public void menuDeselected(MenuEvent e) {
    }

    /**
     * Invoked when the menu is canceled.
     *
     * @param e a MenuEvent object
     */
    public void menuCanceled(MenuEvent e) {
    }

    private void updateFileMenu() {

        if (activeGraph == null) {
            miSaveChart.setEnabled(false);
            miSaveChartAs.setEnabled(false);
            miPrintChart.setEnabled(false);
            miCloseChart.setEnabled(false);
            miDetach.setEnabled(false);
        } else {
            miSaveChart.setVisible(ChartInterface.isIndicatorsEnabled());
            miSaveChartAs.setVisible(ChartInterface.isIndicatorsEnabled());
            miCloseChart.setEnabled(true);
            boolean baseAdded = activeGraph.graph.GDMgr.getBaseGraph() != null;
            miPrintChart.setEnabled(baseAdded);
            miDetach.setEnabled(baseAdded);
        }
        boolean chartsAdded = (desktop.getComponentCount() > 0);
        miCloseAllCharts.setEnabled(chartsAdded);
        miNewLayout.setEnabled(chartsAdded);
    }

    private void updateInsertMenu() {

        if (activeGraph == null) {
            mnuBaseSymbol.setEnabled(false);
            mnuCompareSymbol.setEnabled(false);
            itmIndicatorsDialog.setEnabled(false);
        } else {
            int ind = getIndexOftheMenuItem(mnuInsert, mnuBaseSymbol);
            mnuBaseSymbol.remove(miNewBase);
            mnuInsert.remove(mnuBaseSymbol);
            mnuBaseSymbol = null;
            mnuBaseSymbol = activeGraph.getBaseSymbolSubMenu();
            mnuBaseSymbol.setIconFile("graph_base_Symbol.gif");
            mnuBaseSymbol.updateUI();
            mnuBaseSymbol.add(miNewBase);
            mnuInsert.insert(mnuBaseSymbol, ind);
            activeGraph.markBaseGraph(mnuBaseSymbol);

            ind = getIndexOftheMenuItem(mnuInsert, mnuCompareSymbol);
            mnuCompareSymbol.remove(miNewCompare);
            mnuInsert.remove(mnuCompareSymbol);
            mnuCompareSymbol = null;
            mnuCompareSymbol = activeGraph.getCompareSymbolSubMenu();
            mnuCompareSymbol.setIconFile("graph_compare_Symbol.gif");
            mnuCompareSymbol.updateUI();
            mnuCompareSymbol.add(miNewCompare);
            mnuInsert.insert(mnuCompareSymbol, ind);
            activeGraph.markCompareGraphs(mnuCompareSymbol);

            ind = getIndexOftheMenuItem(mnuInsert, itmIndicatorsDialog);
            mnuInsert.remove(itmIndicatorsDialog);
            itmIndicatorsDialog.setIconFile("graph_Indicators.GIF");
            itmIndicatorsDialog.setAutoscrolls(true);
            itmIndicatorsDialog.updateUI();
            //mnuInsert.insert(itmIndicatorsDialog, ind);

            mnuBaseSymbol.setEnabled(true);
            boolean baseNotNull = activeGraph.graph.GDMgr.getBaseGraph() != null;
            mnuCompareSymbol.setEnabled(baseNotNull);
            itmIndicatorsDialog.setEnabled(baseNotNull);
        }
    }

    private void updatePropertiesMenu() {

        if (activeGraph == null) {
            mnuMode.setEnabled(false);
            mnuPeriod.setEnabled(false);
            mnuInterval.setEnabled(false);
            mnuStyle.setEnabled(false);
            chkmiShowVolume.setEnabled(false);
            chkmiShowVolumeByPrice.setEnabled(false);
            chkmiShowTurnOver.setEnabled(false);
            chkmiIndexed.setEnabled(false);
            miSemiLogScale.setEnabled(false);
            chkmiShowCurrentPriceLine.setEnabled(false);
            chkmiShowPreviousCloseLine.setEnabled(false);
            chkmiShowOrderLines.setEnabled(false);
            chkmiShowBidAskTags.setEnabled(false);
            chkmiShowMinMaxPriceLines.setEnabled(false);
            chkmiVWAP.setEnabled(false);
        } else {
            int ind = getIndexOftheMenuItem(mnuProperties, mnuMode);
            mnuProperties.remove(mnuMode);
            mnuMode = null;
            mnuMode = activeGraph.getModeMenu();
            mnuMode.setIconFile("graph_Mode.GIF");
            mnuMode.updateUI();
            mnuProperties.insert(mnuMode, ind);
            activeGraph.markMode(mnuMode);

            ind = getIndexOftheMenuItem(mnuProperties, mnuPeriod);
            mnuProperties.remove(mnuPeriod);
            mnuPeriod = null;
            mnuPeriod = activeGraph.getPeriodSubMenu();
            mnuPeriod.setIconFile("graph_Period.GIF");
            mnuPeriod.updateUI();
            mnuProperties.insert(mnuPeriod, ind);
            activeGraph.markPeriod(mnuPeriod);

            ind = getIndexOftheMenuItem(mnuProperties, mnuInterval);
            mnuProperties.remove(mnuInterval);
            mnuInterval = null;
            mnuInterval = activeGraph.getIntervalSubMenu();
            mnuInterval.setIconFile("graph_Interval.GIF");
            mnuInterval.updateUI();
            mnuProperties.insert(mnuInterval, ind);
            activeGraph.markInterval(mnuInterval);

            ind = getIndexOftheMenuItem(mnuProperties, mnuStyle);
            mnuProperties.remove(mnuStyle);
            mnuStyle = null;
            mnuStyle = activeGraph.getStyleSubMenu(GraphToolBar.styleIcon, GraphToolBar.styleActiveIcon);
            mnuStyle.setIconFile("graph_Style.GIF");
            mnuStyle.updateUI();
            mnuProperties.insert(mnuStyle, ind);
            activeGraph.markStyle(mnuStyle, true);

            activeGraph.markShowVolume(chkmiShowVolume);
            activeGraph.markShowVolumeByPrice(chkmiShowVolumeByPrice);
            activeGraph.markShowTurnOver(chkmiShowTurnOver);
            activeGraph.markIndexed(chkmiIndexed);

            chkmiShowCurrentPriceLine.setState(activeGraph.graph.GDMgr.isShowCurrentPriceLine());
            chkmiShowPreviousCloseLine.setState(activeGraph.graph.GDMgr.isshowPreviousCloseLine());
            chkmiShowOrderLines.setState(activeGraph.graph.GDMgr.isShowOrderLines());
            chkmiShowBidAskTags.setState(activeGraph.graph.GDMgr.isShowBidAskTags());
            chkmiShowMinMaxPriceLines.setState(activeGraph.graph.GDMgr.isShowMinMaxPriceLines());

            /* if (activeGraph.graph.isCurrentMode()) {
                chkmiVWAP.setState(false);
                chkmiVWAP.setEnabled(false);
            } else {
                chkmiVWAP.setState(activeGraph.graph.GDMgr.isVWAP());
                chkmiVWAP.setEnabled(true);
            }*/
            //commented by charithn- VWAP should not be  enabled in intraday mode. enabled only for history mode.

            chkmiVWAP.setState(activeGraph.graph.GDMgr.isVWAP());
            chkmiVWAP.setEnabled(true);
            mnuMode.setEnabled(true);
            mnuPeriod.setEnabled(true);
            mnuInterval.setEnabled(true);
            mnuStyle.setEnabled(true);
            chkmiShowVolume.setEnabled(true);
            chkmiShowVolumeByPrice.setEnabled(true);
            chkmiShowTurnOver.setEnabled(true);
            chkmiIndexed.setEnabled(true);
            miSemiLogScale.setEnabled(true);
            chkmiShowCurrentPriceLine.setEnabled(true);
            chkmiShowPreviousCloseLine.setEnabled(true);
            //chkmiShowOrderLines.setEnabled(ChartInterface.isTradingConnected());
            chkmiShowOrderLines.setEnabled(ChartInterface.isTradingConnected(SharedMethods.getExchangeFromKey(getActiveGraph().getBaseKey())));
            chkmiShowBidAskTags.setEnabled(true);
            chkmiShowMinMaxPriceLines.setEnabled(true/*activeGraph.graph.isCurrentMode()*/);
        }
    }

    public void modeClicked(java.lang.Object source, java.lang.Integer index) {
        if (activeGraph != null) {
            activeGraph.modeClicked(source, index);
        }
    }

    public void refreshTopToolbar() {
        if (activeGraph != null) {
            activeGraph.refreshTopToolBar(topToolBar);
        } else {
            topToolBar.setNullTarget();
        }
    }

    public void refreshBottomToolbar() {
        bottomToolBar.assignCardLengths();
        if (activeGraph != null) {
            activeGraph.refreshBottomToolBar(bottomToolBar);
        } else {
            bottomToolBar.setNullTarget();
        }
    }

    public void refreshToolBars() {
        refreshTopToolbar();
        refreshBottomToolbar();
        setTitle();
    }

    public void setTitle() {
        if ((activeGraph != null) && (activeGraph.graph.GDMgr.getBaseGraph() != null)) {
            String grfName = StockGraph.extractSymbolFromStr(activeGraph.graph.GDMgr.getBaseGraph());
            if (this.isDetached()) {
                JFrame frame = (JFrame) getDetachedFrame();
                frame.setTitle(Language.getString("UNICHARTS") + " (" + grfName + ")");
            } else {
                setTitle(Language.getString("UNICHARTS") + " (" + grfName + ")");
            }
        } else {
            //setTitle(Language.getString("UNICHARTS"));
            if (this.isDetached()) {
                JFrame frame = (JFrame) getDetachedFrame();
                frame.setTitle(Language.getString("UNICHARTS"));
            } else {
                setTitle(Language.getString("UNICHARTS"));
            }
        }

    }

    public void redrawAllGraphs(TimeZone zone) {

        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i] instanceof GraphFrame) {
                GraphFrame graph = (GraphFrame) frames[i];
                graph.getGraph().getGDMgr().applyTimeZoneAdjustment(zone);
                graph = null;
            }
        }
        frames = null;

        // charts in chartframe
        frames = getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i] instanceof GraphFrame) {
                GraphFrame graph = (GraphFrame) frames[i];
                //graph.getGraph().GDMgr.refreshGraphs();
                graph.getGraph().getGDMgr().applyTimeZoneAdjustment(zone);
                graph = null;
            }
        }
        frames = null;

        ChartInterface.setPrevTimeZone(zone);
    }

    public boolean isShowCrossHairs() {
        return showCrossHairs;
    }

    public void setShowCrossHairs(boolean showCrossHairs) {
        this.showCrossHairs = showCrossHairs;
    }

    public boolean isShowSnapBall() {
        return showsnapBall;
    }

    public void setShowSnapBall(boolean showsnapBall) {
        this.showsnapBall = showsnapBall;
    }

    public int getWindowStyle() {
        return 0;
    }

    public int getZOrder() {
        return 0;
    }

    public boolean isWindowVisible() {
        return isVisible();
    }

    public JTable getTable1() {
        return null;
    }

    public JTable getTable2() {
        return null;
    }

    public boolean isTitleVisible() {
        return true;
    }

    public void setTitleVisible(boolean status) {

    }

    public void windowClosing() {

    }

    public void printTable() {

    }

    public String getWindowID() {
        return null;
    }

    public void setWindowID(String id) {

    }

    public void applyTheme() {

        //--------------newly added-----------------------------

        Theme.applyFGColor(mnuFile, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuInsert, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuTools, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuProperties, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuIndicators, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuStratergies, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuPatterns, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuWindows, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuHelp, "MAIN_MENU_FGCOLOR");

        //------------------------------------------------------

        bottomToolBar.setIcons();
        bottomToolBar.applyTheme();
        topToolBar.setStaticIcons();
        topToolBar.setButtonIcons();
        topToolBar.applyTheme();
        //appalyThemeForMenuBar();

        PropertyDialogFactory.updateTheme();
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(mnuTabPopup); //tab right click menu
        SwingUtilities.updateComponentTreeUI(menuBar);
    }

    public void applicationExiting() {

    }

    public void applicationLoaded() {

    }

    public void applicationLoading(int percentage) {

    }

    public void applicationReadyForTransactions() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        redrawAllGraphs(zone);
    }

    public void workspaceLoaded() {

        try {
            if (oSetting != null) {
                currentArrangeMode = Byte.parseByte(oSetting.getProperty(ViewConstants.VC_CHART_WINDOW_ARRANGE_MODE));
                tileWindows();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void workspaceWillLoad() {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    public void workspaceWillSave() {

        try {
            if (oSetting != null) {
                oSetting.putProperty(ViewConstants.VC_CHART_WINDOW_ARRANGE_MODE, currentArrangeMode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void workspaceSaved() {

    }

    public void dataPointChanged(MouseEvent e, Point CurrentMousePos, StockGraph graph) {
        int x, y;
        x = e.getX();
        y = e.getY();
        GraphDataManager GDMgr = graph.GDMgr;
        DynamicArray graphStore = GDMgr.getStore();
        Object[] singlePoint;
        if (dataWindow != null) {
            String strSymbol = "-";
            long longTime = Long.MIN_VALUE;
            double strValue = Double.MIN_VALUE;
            double strOpen = Double.MIN_VALUE;
            double strHigh = Double.MIN_VALUE;
            double strLow = Double.MIN_VALUE;
            double strClose = Double.MIN_VALUE;
            long strVolume = Long.MIN_VALUE;
            double strTurnover = Double.MIN_VALUE;
            int mode = (graph.isCurrentMode()) ? 1 : 0;
            long interval = graph.getInterval();
            if (GDMgr.getSources().size() > 0) {
                ChartProperties cp = (ChartProperties) GDMgr.getSources().get(0);
                strSymbol = StockGraph.extractSymbolFromStr(cp.getSymbol());
            }
            int pnlID = graph.getPanelIDatPoint(x, y);
            strValue = GDMgr.getYValueForthePixel(y, pnlID);
            int index = Math.round(GDMgr.getIndexForthePixel(x));
            if ((index >= GDMgr.getBeginIndex()) && (index < graphStore.size())) {
                singlePoint = (Object[]) (graphStore.get(index));
                if (singlePoint != null) {
                    longTime = (Long) singlePoint[0];
                    //if (objLong != null) longTime = objLong.longValue();
                    if (singlePoint.length > 1) {
                        ChartPoint fa = (ChartPoint) singlePoint[1];
                        if (fa != null) {
                            strOpen = fa.Open;
                            strHigh = fa.High;
                            strLow = fa.Low;
                            strClose = fa.Close;
                            strVolume = (long) fa.Volume;
                            strTurnover = (long) fa.TurnOver;
                        }
                    }
                }
            }
            if (dataWindow.isWindowMode()) {
                dataWindow.setData(strSymbol, mode, interval, longTime, strValue, strOpen, strHigh, strLow, strClose, strVolume, strTurnover);
                dataWindow.addIndicatorValues(graph.GDMgr, index);
            }
        }
    }

    public void customIndicatorCreated(final String name, final Class indicatorClass, String grammer) {

        customIndicators.add(new IndicatorMenuObject(indicatorClass, name));
        sortCustomIndicators();
    }

    public void strategyCreated(String name, Class indicatorClass) {
        alStrategies.add(new IndicatorMenuObject(indicatorClass, name));
    }

    public void patternCreated(String name, Class indicatorClass) {
        alPatterns.add(new IndicatorMenuObject(indicatorClass, name));
    }

    public void customIndicatorDeleted(String name) {
        IndicatorMenuObject indicatorMenuObject = null;
        for (IndicatorMenuObject imo : customIndicators) {
            if (imo.getIndicatorName().equals(name)) {
                indicatorMenuObject = imo;
            }
        }
        if (indicatorMenuObject != null) {
            customIndicators.remove(indicatorMenuObject);
            IndicatorDetailStore.CustomIndicators.remove(indicatorMenuObject);
        }
        sortCustomIndicators();

        //remove from the faviourite list
        for (String custIndname : IndicatorDetailStore.alFaviouriteCustIndicators) {
            if (custIndname.equals(name)) {
                IndicatorDetailStore.alFaviouriteCustIndicators.remove(custIndname);
            }
        }


    }

    public void tradeServerConnected() {
        if (activeGraph != null) {
            chkmiShowOrderLines.setEnabled(true);
            topToolBar.getBtnShowOrderLines().setEnabled(true);
        }
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        if (activeGraph != null) {
            chkmiShowOrderLines.setEnabled(false);
            topToolBar.getBtnShowOrderLines().setEnabled(false);
        }
    }

    private void sortCustomIndicators() {
        Collections.sort(customIndicators, new Comparator<IndicatorMenuObject>() {
            public int compare(IndicatorMenuObject o1, IndicatorMenuObject o2) {
                String name1 = o1.getIndicatorName().toUpperCase();
                String name2 = o2.getIndicatorName().toUpperCase();
                if (name1.compareTo(name2) > 0) {
                    return 1;
                } else if (name1.compareTo(name2) < 0) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
    }

    public boolean isResizeWindowOnTitleToggle() {
        return super.isResizeWindowOnTitleToggle();
    }

    public JPanel getTabBar() {

        return tabBar;
    }

    public void internalFrameIconified(InternalFrameEvent e) {

    }

    public JPanel getTopToolTabBar() {

        return topToolTabBar;

    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {        //hideGlassPane();

    }

    public void removeTabIndexPanel() {
        pnlDesktopContainer.removeAll();
        pnlDesktopContainer.setLayout(new BorderLayout());
        pnlDesktopContainer.add(desktop, BorderLayout.CENTER);
        pnlDesktopContainer.doLayout();
        this.getContentPane().doLayout();

    }

    public void internalFrameClosed(InternalFrameEvent e) {

    }

    public void internalFrameClosing(InternalFrameEvent e) {

    }

    public void addTabIndexpanel(boolean status) {
        try { // todo enable tab here
            if (status) {

                pnlDesktopContainer.removeAll();
                pnlDesktopContainer.setLayout(new BorderLayout());
                pnlDesktopContainer.add(desktop, BorderLayout.CENTER);
                pnlDesktopContainer.add(tabBar, BorderLayout.NORTH);
                pnlDesktopContainer.doLayout();
                this.getContentPane().doLayout();
            } else {
                removeTabIndexPanel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doNavigationBarLayout(int barWidth) {
        tabBar.doLayout();
        tabBar.updateUI();
        this.getContentPane().doLayout();
    }

    public void addWindowTab(InternalFrame iframe) {
        if ((iframe.getDesktopPane() != null) && (!iframe.isDetached()) && !(iframe instanceof NonTabbable)) {
            windowTabs.addTab(iframe.getTitle(), iframe);
            windowTabs.setSelectedComponent(iframe);
            windowTabs.repaint();
            if (Settings.isTabIndex()) {
                addTabIndexpanel(windowTabs.getTabCount() > 0);
            }
        }
    }

    public void tabStareChanged(TWTabEvent event) {
        try {
            if (event.getState() == TWTabEvent.STATE_SELECTED) {
                JInternalFrame iframe = (JInternalFrame) windowTabs.getSelectedComponent();
                if (iframe.isIcon()) {
                    iframe.setIcon(false);
                    if (!iframe.isSelected()) {
                        iframe.setSelected(true);
                        iframe.setMaximum(true);
                    }
                } else if (!iframe.isSelected()) {
                    iframe.setSelected(true);
                    iframe.setMaximum(true);
                }
            }
        } catch (PropertyVetoException e1) {
            System.out.println("InternalFrame selection failed......");
            e1.printStackTrace();                                             //To change body of catch statement use File | Settings | File Templates.
        }

        /*if (e.getSource() == windowTabs) {
           Enumeration ee = TWDesktop.tabHashTable.keys();
            while (ee.hasMoreElements()) {
                InternalFrame iframe = (InternalFrame) ee.nextElement();
                if (windowTabs.getSelectedComponent().equals(TWDesktop.tabHashTable.get(iframe))) {
                    //SharedMethods.printLine("Change Event " + iframe.getTitle(), true);
                    try {
                        if (iframe.isIcon()) {
                            //iframe.setIcon(false);
                            iframe.setSelected(true);
                        }else if (!iframe.isSelected()) {
                            iframe.setSelected(true);
                        }
                    } catch (PropertyVetoException e1) {
                        System.out.println("InternalFrame selection failed......");
                        e1.printStackTrace();                                             //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }*/
    }

    public boolean isInTabbedPanel(InternalFrame frame) {
        return windowTabs.contains(frame);
    }

    public boolean isBIsTabbedMode() {
        return bIsTabbedMode;
    }

    private void createTabPopup() {
        mnuCloseTab = new TWMenuItem(Language.getString("CLOSE_TAB"), "");       //COLMUBMKT-144
        mnuCloseTab.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                windowTabs.removeTab(windowTabs.getSelectedComponent());
                closeActiveChart();
            }
        });
        mnuCloseTab.setVisible(true);

        mnuCloseAllButThis = new TWMenuItem(Language.getString("CLOSE_ALL_BUT_THIS"), "");  //graph_Detach.gif
        mnuCloseAllButThis.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeAllButThisTab();
            }
        });
        mnuCloseAllButThis.setVisible(true);

        mnuCloseAllTabs = new TWMenuItem(Language.getString("CLOSE_ALL_TABS"), "");  //graph_Detach.gif
        mnuCloseAllTabs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeAllTabs();
            }
        });
        mnuCloseAllTabs.setVisible(true);

        mnuTabPopup.add(mnuCloseTab);
        mnuTabPopup.add(mnuCloseAllButThis);
        mnuTabPopup.add(mnuCloseAllTabs);

        mnuTabPopup.setVisible(false);
        mnuTabPopup.addPopupMenuListener(this);
        GUISettings.applyOrientation(mnuTabPopup);
    }

    public void closeSelectedTab() {
        int selTab = windowTabs.getSelectedIndex();
        Component comp = windowTabs.getSelectedComponent();
        closeGivenChart(comp);
        windowTabs.removeTab(selTab);
    }

    public void closeAllButThisTab() {

        JInternalFrame[] frames = desktop.getAllFrames();
        int selTab = windowTabs.getSelectedIndex();
        int tabcount = windowTabs.getTabCount();

        for (int i = 0; i < tabcount; i++) {
            if (selTab != 0) {
                ((GraphFrame) windowTabs.getComponentAt(0)).closeWindow();

                selTab = selTab - 1;
            } else if (selTab == 0 && (windowTabs.getTabCount() > 1)) {
                ((GraphFrame) windowTabs.getComponentAt(1)).closeWindow();
            }
        }
    }

    public void closeAllTabs() {

        int tabCount = windowTabs.getTabCount();

        for (int i = 0; i < tabCount; i++) {
            windowTabs.removeTab(0);
        }

        closeAll();
    }

    public void mouseClicked(MouseEvent e) {
        if ((e.getModifiers() & e.BUTTON3_MASK) == e.BUTTON3_MASK) {

            if (e.getSource() == windowTabs) {
                System.out.println("Event source is Tabbed pane");
                GUISettings.showPopup(mnuTabPopup, e.getComponent(), e.getX(), e.getY());
            }
        }
    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

    }

    public void popupMenuCanceled(PopupMenuEvent e) {

    }

    public void chartNavigationBarChanged() {
        getContentPane().doLayout();
    }

    public DataWindow getDataWindow() {
        return dataWindow;
    }

    public void setDataWindow(DataWindow win) {
        dataWindow = win;
    }

    public boolean isChatNavigationBarVisible() {
        return isNavigationBarVisible;
    }

    public void setChartNavigationBarVisible(boolean visible) {
        this.isNavigationBarVisible = visible;
    }

    public byte getCurrentArrangeMode() {
        return currentArrangeMode;
    }

    public void setCurrentArrangeMode(byte currentArrangeMode) {
        this.currentArrangeMode = currentArrangeMode;
    }
}