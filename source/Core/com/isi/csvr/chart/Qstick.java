package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 29, 2006
 * Time: 11:56:17 AM
 */
public class Qstick extends ChartProperties implements Indicator {

    private static final long serialVersionUID = UID_QSTICK;
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public Qstick(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_QSTICK") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_QSTICK");
        timePeriods = 8;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte destIndex) {
        int startIndex = bIndex + t_im_ePeriods - 1;
        if (al.size() - 1 < startIndex)
            return;
        double subSum = 0;
        ChartRecord crNow = null;
        if (al.size() < startIndex || t_im_ePeriods == 0) {
            for (int i = 0; i < al.size(); i++) {
                crNow = (ChartRecord) al.get(i);
                crNow.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        for (int j = 0; j <= startIndex; j++) {
            crNow = ((ChartRecord) al.get(j));
            subSum += crNow.Close - crNow.Open;
        }

        //Set The start point

        crNow = (ChartRecord) al.get(startIndex);
        crNow.setValue(destIndex, subSum / t_im_ePeriods);

        /*    how calculation going on
        *     = = = = =
        *       = = = = =     (remove first in current list as it not belong to period)
        *         = = = = =    (add next one)
        *     remove first one and add next one.
        */

        ChartRecord crRem = null;

        for (int i = startIndex + 1; i < al.size(); i++) {
            //add current values
            crNow = (ChartRecord) al.get(i);
            subSum += crNow.Close - crNow.Open;

            //remove point outside the period
            crRem = (ChartRecord) al.get(i - t_im_ePeriods);
            subSum -= crRem.Close - crRem.Open;
            crNow.setValue(destIndex, subSum / t_im_ePeriods);
        }
    }

    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 8;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof Qstick) {
            Qstick qstick = (Qstick) cp;
            this.timePeriods = qstick.timePeriods;
            this.innerSource = qstick.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_QSTICK") + " " + parent;
    }

//############################################
//implementing Indicator

    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int timePeriods) {
        this.timePeriods = timePeriods;
    }

    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        if (al.size() < 2) return;
        long entryTime = System.currentTimeMillis();
        double sum = 0f;
        ChartRecord cr, crOld;

        for (int i = 0; i < timePeriods; i++) {
            cr = (ChartRecord) al.get(i);
            sum += (cr.Close - cr.Open);

            if (i == timePeriods - 1) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(sum / timePeriods);
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }

        for (int i = timePeriods; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - timePeriods);
            sum += (cr.Close - cr.Open);
            sum -= (crOld.Close - crOld.Open);

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(sum / timePeriods);
            }
        }

        //System.out.println("**** Qstick Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_QSTICK");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
