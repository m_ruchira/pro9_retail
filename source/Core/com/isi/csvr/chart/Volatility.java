package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Mar 31, 2005 - Time: 7:34:47 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeTwoParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class Volatility extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_Volatility;
    //Fields
    private int timePeriods;
    private int rateOfChange;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public Volatility(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_VOLATILITY") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_VOLATILITY");
        this.timePeriods = 10;
        this.rateOfChange = 10;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeTwoParameterProperty idp = (IndicatorTypeTwoParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getParam1());
            this.setRateOfChange((int) idp.getParam2());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte stepHminusL, byte stepEMA,
                                          byte rateOfChange, byte destIndex) {

        double currVal, f1, f2;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;

        //calculating diff
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(stepHminusL, cr.High - cr.Low);
        }

        //EMA
        MovingAverage.getMovingAverage(al, 0, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, stepHminusL, stepEMA);

        int loopBegin = bIndex + t_im_ePeriods + rateOfChange - 1;
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - rateOfChange);
            cr = (ChartRecord) al.get(i);
            f1 = crOld.getStepValue(stepEMA);
            f2 = cr.getStepValue(stepEMA);
            currVal = (f1 == f2) ? 0 : (f1 == 0) ? 10000f : (f2 - f1) * 100f / f1;  // here 10000f represents the infinity

            /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(currVal);
            }*/
            cr.setValue(destIndex, currVal);
        }
        //System.out.println("**** Volatality Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 2;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 10;
        this.rateOfChange = 10;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.rateOfChange = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/RateOfChange"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "RateOfChange", Integer.toString(this.rateOfChange));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof Volatility) {
            Volatility vol = (Volatility) cp;
            this.timePeriods = vol.timePeriods;
            this.rateOfChange = vol.rateOfChange;
            this.innerSource = vol.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_VOLATILITY") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    //rateOfChange
    public int getRateOfChange() {
        return rateOfChange;
    }
    //############################################

    public void setRateOfChange(int roc) {
        rateOfChange = roc;
    }

    // this contains 2 intermediate steps: HminusL and EMA
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double currVal, f1, f2;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;
        //setting step size 2
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(2);
        }
        // steps involved
        byte stepHminusL = ChartRecord.STEP_1;
        byte stepEMA = ChartRecord.STEP_2;

        //calculating diff
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(stepHminusL, cr.High - cr.Low);
        }

        //EMA
        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_EXPONENTIAL, stepHminusL, stepEMA);

        int loopBegin = timePeriods + rateOfChange - 1;
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - rateOfChange);
            cr = (ChartRecord) al.get(i);
            f1 = crOld.getStepValue(stepEMA);
            f2 = cr.getStepValue(stepEMA);
            currVal = (f1 == f2) ? 0 : (f1 == 0) ? 10000f : (f2 - f1) * 100f / f1;  // here 10000f represents the infinity

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(currVal);
            }
        }
        //System.out.println("**** Volatality Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_VOLATILITY");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}