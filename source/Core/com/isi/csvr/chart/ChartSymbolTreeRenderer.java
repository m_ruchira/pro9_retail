package com.isi.csvr.chart;

import com.isi.csvr.chart.options.ChartOptions;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Jun 16, 2008
 * Time: 4:58:16 PM
 * To change this template use File | Settings | File Templates.
 */
//public class SymbolTreeRenderer implements TreeCellRenderer {

public class ChartSymbolTreeRenderer implements TreeCellRenderer, Themeable {
    public static Color chartNaviBarSymbolColor = Color.BLUE;
    public static ChartRendererComponenet nonLeafRenderer = new ChartRendererComponenet();
    //    private JLabel leafRenderer = new JLabel();
    //    private DefaultTreeCellRenderer nonLeafRenderer = new DefaultTreeCellRenderer();
    private static DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
    //    Color selectionBorderColor, selectionForeground, selectionBackground, textForeground, textBackground;
    private static ImageIcon g_oClosedIcon = null;
    private static ImageIcon g_oExpandedIcon = null;
    private static ImageIcon g_oLeafIcon_sub = null;
    //    private static ImageIcon g_oLeafIcon_unSub = null;
    private static Color backgroundSelectionColor;
    private static Color backgroundNonSelectionColor;
    ChartOptions.SymbolNameOptions optionSymbolName;
    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");

    public ChartSymbolTreeRenderer() {

//        GUISettings.applyOrientation(nonLeafRenderer);
        /*Boolean booleanValue = (Boolean) UIManager.get("Tree.drawsFocusBorderAroundIcon");
        selectionBorderColor = UIManager.getColor("Tree.selectionBorderColor");
        selectionForeground = UIManager.getColor("Tree.selectionForeground");
        selectionBackground = UIManager.getColor("Tree.selectionBackground");
        textForeground = UIManager.getColor("Tree.textForeground");
        textBackground = UIManager.getColor("Tree.textBackground");*/
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
//            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
            backgroundSelectionColor = Theme.getColor("SIDEBAR_SELECTED_BGCOLOR");

        } catch (Exception e) {

        }

        reload();
        Theme.registerComponent(this);
        optionSymbolName = ChartOptions.getCurrentOptions().misc_symbol_Name;

    }

    public static void reload() {

        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
//            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
//            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
            backgroundSelectionColor = Theme.getColor("SIDEBAR_SELECTED_BGCOLOR");

        } catch (Exception e) {

        }
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean selected, boolean expanded, boolean leaf, int row,
                                                  boolean hasFocus) {

        Component returnValue = null;
        //ChartRendererComponenet nonLeafRenderer = new ChartRendererComponenet();
        try {
            if (((ChartExchangeNode) value) instanceof ChartExchangeNode) {
                ChartExchangeNode nv = (ChartExchangeNode) value;
                if (nv.isExchangeType()) {
                    if (expanded) {
                        try {
                            if (nv.getMarketStatusType() == Meta.MARKET_OPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_CLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PREOPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PREOPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PRECLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PRECLOSED_COLOR"));
                            } else {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            }
//                            nonLeafRenderer.setMode(0);
                            nonLeafRenderer.setLeftString(nv.toString());
                            nonLeafRenderer.setRightString(nv.getStatus());
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        nonLeafRenderer.setImage(g_oExpandedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    } else {
                        try {
                            if (nv.getMarketStatusType() == Meta.MARKET_OPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_CLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PREOPEN) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PREOPEN_COLOR"));
                            } else if (nv.getMarketStatusType() == Meta.MARKET_PRECLOSE) {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_PRECLOSED_COLOR"));
                            } else {
                                nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
                            }
//                            nonLeafRenderer.setMode(0);
                            nonLeafRenderer.setLeftString(nv.toString());
                            nonLeafRenderer.setRightString(nv.getStatus());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        nonLeafRenderer.setImage(g_oClosedIcon);
                        nonLeafRenderer.setToolTipText(null);
                    }
                    nonLeafRenderer.setSelectedColor(backgroundNonSelectionColor);
                    if (selected) {
                        nonLeafRenderer.setSelectedColor(backgroundSelectionColor);
                    } else {
                        nonLeafRenderer.setUnselected();
//                        nonLeafRenderer.setSelectedColor(backgroundNonSelectionColor);

                    }
                    returnValue = nonLeafRenderer;
                } else {
                    ChartSymbolNode sto = (ChartSymbolNode) value;
                    nonLeafRenderer.setImage(g_oLeafIcon_sub);
                    nonLeafRenderer.setMode(1);
                    nonLeafRenderer.setColor(0, chartNaviBarSymbolColor, Theme.getColor("SIDEBAR_SYMBOL_COLOR"));

                    //System.out.println("");
                    if (selected) {
                        nonLeafRenderer.setSelectedColor(backgroundSelectionColor);
                        if (ChartNavibarConfigDialog.isCheckisShortOnly()) {
                            nonLeafRenderer.setLeftString(sto.getShortDescription());
                            if (ChartNavibarConfigDialog.isCheckisShorSymboltOnly()) {
                                nonLeafRenderer.setRightString(sto.getSymbol());
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else if (ChartNavibarConfigDialog.isCheckisLongOnly()) {
                            nonLeafRenderer.setLeftString(sto.getDescription());
                            if (ChartNavibarConfigDialog.isCheckisLongSymbolOnly()) {
                                nonLeafRenderer.setRightString(sto.getSymbol());
                                if (sto.getDescription().equals("")) {
                                    nonLeafRenderer.setLeftString(sto.getShortDescription());
                                }
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else if (ChartNavibarConfigDialog.isCheckisSymbolOnly()) {
                            nonLeafRenderer.setLeftString(sto.getSymbol());

                            if (ChartNavibarConfigDialog.isCheckisSymbolShortOnly()) {
                                nonLeafRenderer.setRightString(sto.getShortDescription());
                                if (sto.getDescription().equals("")) {
                                    nonLeafRenderer.setLeftString(sto.getShortDescription());
                                }
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
//                            nonLeafRenderer.setRightString("");
                        } else {
                            nonLeafRenderer.setLeftString(sto.getShortDescription());
                            nonLeafRenderer.setRightString(sto.getSymbol());
                        }
                    } else {
                        nonLeafRenderer.setUnselected();
                        if (ChartNavibarConfigDialog.isCheckisShortOnly()) {
                            nonLeafRenderer.setLeftString(sto.getShortDescription());
                            if (ChartNavibarConfigDialog.isCheckisShorSymboltOnly()) {
                                nonLeafRenderer.setRightString(sto.getSymbol());
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else if (ChartNavibarConfigDialog.isCheckisLongOnly()) {
                            nonLeafRenderer.setLeftString(sto.getDescription());
                            if (sto.getDescription().equals("")) {
                                nonLeafRenderer.setLeftString(sto.getShortDescription());
                            }
                            if (ChartNavibarConfigDialog.isCheckisLongSymbolOnly()) {
                                nonLeafRenderer.setRightString(sto.getSymbol());
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else if (ChartNavibarConfigDialog.isCheckisSymbolOnly()) {
                            nonLeafRenderer.setLeftString(sto.getSymbol());

                            if (ChartNavibarConfigDialog.isCheckisSymbolShortOnly()) {
                                nonLeafRenderer.setRightString(sto.getShortDescription());
                                if (sto.getDescription().equals("")) {
                                    nonLeafRenderer.setLeftString(sto.getShortDescription());
                                }
                            } else {
                                nonLeafRenderer.setRightString("");
                            }
                        } else {
                            nonLeafRenderer.setLeftString(sto.getShortDescription());
                            nonLeafRenderer.setRightString(sto.getSymbol());
                        }
//                        nonLeafRenderer.setText("<HTML><font color=#" + Theme.getSideBarExchangeColor() + ">" + " " + sto.getDescription() + " - " + sto.getSymbol()+ "</font>");

                    } //woai
                    returnValue = nonLeafRenderer;
                }

            } else {
                returnValue = new ChartRendererComponenet();
            }
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return new JLabel("root");
        }
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        try {
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");
//            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();
            backgroundSelectionColor = Theme.getColor("SIDEBAR_SELECTED_BGCOLOR");
            nonLeafRenderer.setColor(0, Theme.getColor("SIDEBAR_EXCHANGE_COLOR"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
            chartNaviBarSymbolColor = Theme.getColor("SIDEBAR_DESCR_COLOR");

        } catch (Exception e) {

        }
    }
}
