package com.isi.csvr.chart;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.TWCheckBoxMenuItem;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class GraphToolBar extends JPanel implements ActionListener, MouseListener {
    public static Icon[] styleIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_line.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_bar.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_hlc.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_ohlc.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_candle.gif")
    };
    static Icon[] modeIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_history.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_intraday.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_history_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_intraday_act.gif")
    };
    static Icon[] styleActiveIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_line_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_bar_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_hlc_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_ohlc_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_candle_act.gif")
    };
    static Icon[] gridIcon = {//h b v n
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_horiz.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_both.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_verti.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_none.gif"),
    };
    static Icon[] gridActiveIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_horiz_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_both_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_verti_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_none_act.gif"),
    };
    static Icon[] dragZoomIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_dragZoom.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_dragZoom_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_boxZoom.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_boxZoom.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_boxZoom_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_boxZoom_act.gif")
    };
    static Icon[] showTurnOverIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_turnover.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_turnover.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_turnover_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_turnover_act.gif")
    };
    static Icon[] showVolumeIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_vol.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_vol_act.gif")
    };
    static Icon[] showVolumeByPriceIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol_by_price.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_vol_by_price.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol_by_price_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_vol_by_price_act.gif")
    };
    static Icon[] showDataWinIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_dw.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_dw.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_dw_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_dw_act.gif")
    };
    static Icon[] indexedIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_indexed.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_indexed.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_indexed_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_indexed_act.gif")
    };
    static Icon[] dataModeIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_dataMode.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dataMode.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_dataMode_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dataMode_act.gif"),

    };
    static Icon[] showDescIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_desc.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_desc.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_desc_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_desc_act.gif")
    };
    static Icon[] splitAdjustedIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_split_adjusted.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_split_adjusted.gif"),
    };
    static Icon[] splitAdjustedActiveIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_split_adjusted_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_split_adjusted_act.gif")
    };
    static Icon[] showSplitsIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_splits.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_splits.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_splits_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_splits_act.gif")
    };
    static Icon[] showAnnouncementsIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_announs.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_announs.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_announs_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_announs_act.gif")
    };
    static Icon[] showCurrentPriceIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_current_price.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_current_price.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_current_price_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_current_price_act.gif")
    };
    static Icon[] showPreviousCloseIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_previous_close_line.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_previous_close_line.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_previous_close_line_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_previous_close_line_act.gif")
    };
    static Icon[] showResistanceIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_supRes_lines.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_supRes_lines.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_supRes_lines_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_supRes_lines_act.gif")
    };
    static Icon[] showOrderLinesIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_order_lines.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_order_lines.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_order_lines_vol_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_order_lines_act.gif")
    };
    static Icon[] showBidAskTagsIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_bidAsk_tags.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_bidAsk_tags.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_bidAsk_tags_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_bidAsk_tags_act.gif")
    };
    static Icon[] showMinMaxLinesIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_min_max.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_min_max.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_min_max_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_min_max_act.gif")
    };
    static Icon[] snapToPriceIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_price.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_disable_snap_price.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_price_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_disable_snap_price_act.gif")
    };
    static Icon[] snapBallIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_ball.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_disable_snap_ball.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_ball_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_disable_snap_ball_act.gif")
    };
    static Icon[] showHIdeISPIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_ball.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_disable_snap_ball.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_ball_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_disable_snap_ball_act.gif")
    };
    static Icon[] showCrossHairsIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_cross_hair.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_cross_hair.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_cross_hair_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_cross_hair_act.gif")
    };
    static Icon[] fibCalcIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_enable_fib_calculator.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_disable_fib_calculator.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_enable_fib_calculator_roll.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_disable_fib_calculator_roll.gif")
    };
    static Icon[] recSelectionIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_enable_rect_selection.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_disable_rect_selection.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_enable_rect_selection_roll.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_disable_rect_selection_roll.gif")
    };
    static Icon[] erasorIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_enable_eraser.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_disable_eraser.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_enable_eraser_roll.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_disable_eraser_roll.gif")
    };
    static Icon[] navigationBarIcon = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_sidebar_show.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_sidebar_hide.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_sidebar_show_act.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_sidebar_hide_act.gif")
    };
    private final byte ICON_HISTORY = 0;
    private final byte ICON_CURRENT = 1;
    public ChartFrame parent = null;
    private GraphFrame activeGraph = null;
    private FlowLayout flowToolBarLayout = new FlowLayout(FlowLayout.LEADING, 1, 1);
    private FlowLayout flowToolPnlLayout = new FlowLayout(FlowLayout.TRAILING, 1, 1);
    private JPanel pnlShowHideISP = new JPanel(flowToolPnlLayout);
    private JPanel pnlPeriod = new JPanel(flowToolPnlLayout);
    private JPanel pnlInterval = new JPanel(flowToolPnlLayout);
    private JPanel pnlStyle = new JPanel(flowToolPnlLayout);
    private JButton btnSaveChart = new JButton();
    private JButton btnOpenChart = new JButton();
    private JButton btnNew = new JButton();
    private Component component1;
    private JButton btnPrintChart = new JButton();
    private Component component2;
    private JButton btnDragZoom = new JButton();
    private JButton btnShowVolume = new JButton();
    private JButton btnShowVolumeByPrice = new JButton();
    private JButton btnShowTurnOver = new JButton();
    private JButton btnShowDataWin = new JButton();
    private JButton btnIndexed = new JButton();
    private JButton btnSplitAdjusted = new JButton();
    private JButton btnShowSplits = new JButton();
    private JButton btnShowAnnouncements = new JButton();
    private JButton btnChartOrData = new JButton();
    private JButton btnShowCurrentPriceLine = new JButton();
    private JButton btnShowPreviousCloseLine = new JButton();
    private JButton btnShowOrderLines = new JButton();
    private JButton btnShowBidAskTags = new JButton();
    private JButton btnShowMinMaxLines = new JButton();
    private JButton btnSnapToPrice = new JButton();
    private JButton btnSnapBall = new JButton();
    private JLabel btnShowHideISP = new JLabel();
    private JButton lblShowHideISP = new JButton();
    private JButton btnShowResistanceLines = new JButton();
    private JButton btnRectangualrSelection = new JButton();
    private JButton btnErasor = new JButton();
    private JButton btnFibCalculator = new JButton();
    private JButton btnShowCrossHairs = new JButton();
    private Component component3;
    private JButton btnMode = new JButton();
    private JButton btnNavigationBar = new JButton();
    private JLabel btnPeriods = new JLabel();
    private Border borderEmpty = BorderFactory.createEmptyBorder();
    private JLabel lblPeriods = new JLabel();
    private JLabel btnIntervals = new JLabel();
    private JLabel lblIntervals = new JLabel();
    private JLabel btnStyle = new JLabel();
    private JButton lblStyle = new JButton();
    private JPopupMenu popNewCharts = null;
    private JPopupMenu popIndicators = null;
    private JPopupMenu popStrategies = null;
    private JPopupMenu popPatterns = null;
    private JPopupMenu popPeriods = null;
    private JPopupMenu popIntervals = null;
    private JPopupMenu popStyles = null;
    private JPopupMenu popShowHideISP = null;
    private JButton btnShowDescription = new JButton();
    private JButton btnGrid = new JButton();
    private TWMenuItem miNewChart = null;
    private TWMenuItem miNewLayout = null;
    private JPopupMenu popZoom = null;
    private JPopupMenu popSnapToPrice = null;
    private JPopupMenu popcrossHair = null;
    private JPopupMenu popVolByPrice = null;
    private JPopupMenu popSplitAdjust = null;
    private JLabel lblZoom = new JLabel();
    private JLabel lblSnapToPrice = new JLabel();
    private JLabel lblCrossHair = new JLabel();
    private JLabel lblVolByPrice = new JLabel();
    private JLabel lblSplitAdjust = new JLabel();

    private boolean isDragZoomSelected = true;


    public GraphToolBar(ChartFrame cf) {
        try {
            parent = cf;
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception {
        component1 = Box.createVerticalStrut(20);
        component2 = Box.createVerticalStrut(20);
        component3 = Box.createVerticalStrut(20);
        this.setMinimumSize(new Dimension(10, 26));
        this.setPreferredSize(new Dimension(100, 26));
        this.setRequestFocusEnabled(false);
        this.setLayout(flowToolBarLayout);

        setCaptions();
        setPreferredSizes();
        addActionListeners();
        setButtonIcons();
        setEmptyBorders();
        setToolTipText();
        pnlPeriod.add(lblPeriods, null);
        pnlPeriod.add(btnPeriods, null);
        pnlInterval.add(lblIntervals, null);
        pnlInterval.add(btnIntervals, null);
        pnlStyle.add(lblStyle, null);
        pnlStyle.add(btnStyle, null);
        this.add(btnNew, null);
        this.add(btnOpenChart, null);
        this.add(btnSaveChart, null);
        this.add(component1, null);
        this.add(btnPrintChart, null);
        this.add(component2, null);
        this.add(btnNavigationBar, null);
        this.add(btnMode, null);

        this.add(pnlPeriod, null);
        this.add(pnlInterval, null);
        this.add(pnlStyle, null);
        this.add(component3, null);
        this.add(btnShowVolume, null);
        this.add(btnShowVolumeByPrice, null);
        this.add(lblVolByPrice, null);
        this.add(btnShowTurnOver, null);
        this.add(btnShowDataWin, null);
        this.add(btnShowCrossHairs, null);
        this.add(lblCrossHair, null);
        this.add(btnIndexed, null);
        if (ChartInterface.isSplitAdujstmentsEnabled()) this.add(btnSplitAdjusted, null);
        if (ChartInterface.isSplitAdujstmentsEnabled()) this.add(lblSplitAdjust, null);
        if (ChartInterface.isSplitsEnabled()) this.add(btnShowSplits, null);
        if (ChartInterface.isAnnoucmentsEnabled()) this.add(btnShowAnnouncements, null);
        this.add(btnShowDescription, null);
        this.add(btnGrid, null);
        this.add(btnChartOrData, null);
        this.add(btnShowPreviousCloseLine, null);
        this.add(btnShowCurrentPriceLine, null);
        this.add(btnShowResistanceLines, null);
        if (ChartInterface.isBuySellEnabled()) this.add(btnShowOrderLines, null);
        btnShowOrderLines.setEnabled(false);
        this.add(btnShowBidAskTags, null);
        if (ChartInterface.isMinMaxEnabled()) this.add(btnShowMinMaxLines, null);
        this.add(btnSnapToPrice, null);
        this.add(lblSnapToPrice, null);
        this.add(btnRectangualrSelection, null);
        this.add(btnErasor, null);
        this.add(btnFibCalculator, null);
        this.add(btnSnapBall, null);
        this.add(pnlShowHideISP, null);
        pnlShowHideISP.add(lblShowHideISP, null);
        pnlShowHideISP.add(btnShowHideISP, null);


        this.setOpaque(true);

    }

    private void setCaptions() {
        lblPeriods.setText(Language.getString("PERIOD"));
        lblIntervals.setText(Language.getString("INTERVAL"));
    }

    private void setPreferredSizes() {
        btnSaveChart.setPreferredSize(new Dimension(24, 24));
        btnOpenChart.setPreferredSize(new Dimension(24, 24));
        btnNew.setPreferredSize(new Dimension(24, 24));
        btnPrintChart.setPreferredSize(new Dimension(24, 24));

        btnDragZoom.setPreferredSize(new Dimension(24, 24));
        btnShowTurnOver.setPreferredSize(new Dimension(24, 24));
        btnShowVolume.setPreferredSize(new Dimension(24, 24));
        btnShowVolumeByPrice.setPreferredSize(new Dimension(24, 24));
        btnShowDataWin.setPreferredSize(new Dimension(24, 24));
        btnIndexed.setPreferredSize(new Dimension(24, 24));
        btnSplitAdjusted.setPreferredSize(new Dimension(24, 24));
        btnShowSplits.setPreferredSize(new Dimension(24, 24));
        btnShowAnnouncements.setPreferredSize(new Dimension(24, 24));
        btnChartOrData.setPreferredSize(new Dimension(24, 24));
        btnShowDescription.setPreferredSize(new Dimension(24, 24));

        btnMode.setPreferredSize(new Dimension(24, 24));
        btnNavigationBar.setPreferredSize(new Dimension(24, 24));
        btnGrid.setPreferredSize(new Dimension(24, 24));

        btnShowCurrentPriceLine.setPreferredSize(new Dimension(24, 24));
        btnShowPreviousCloseLine.setPreferredSize(new Dimension(24, 24));
        btnShowResistanceLines.setPreferredSize(new Dimension(24, 24));
        btnRectangualrSelection.setPreferredSize(new Dimension(24, 24));
        btnErasor.setPreferredSize(new Dimension(24, 24));
        btnShowOrderLines.setPreferredSize(new Dimension(24, 24));
        btnShowBidAskTags.setPreferredSize(new Dimension(24, 24));
        btnShowMinMaxLines.setPreferredSize(new Dimension(24, 24));
        btnSnapToPrice.setPreferredSize(new Dimension(24, 24));
        btnSnapBall.setPreferredSize(new Dimension(24, 24));

        btnShowCrossHairs.setPreferredSize(new Dimension(24, 24));

        pnlPeriod.setPreferredSize(new Dimension(70, 24));
        btnPeriods.setPreferredSize(new Dimension(10, 22));

        btnIntervals.setPreferredSize(new Dimension(10, 22));
        pnlInterval.setPreferredSize(new Dimension(65, 24));

        btnStyle.setPreferredSize(new Dimension(10, 22));
        lblStyle.setPreferredSize(new Dimension(24, 24));
        pnlStyle.setPreferredSize(new Dimension(40, 24));
        lblZoom.setPreferredSize(new Dimension(10, 22));
        lblSnapToPrice.setPreferredSize(new Dimension(10, 22));
        lblCrossHair.setPreferredSize(new Dimension(10, 22));
        lblVolByPrice.setPreferredSize(new Dimension(10, 22));
        lblSplitAdjust.setPreferredSize(new Dimension(10, 22));
        lblShowHideISP.setPreferredSize(new Dimension(24, 22));
        btnShowHideISP.setPreferredSize(new Dimension(10, 22));

    }

    private void addActionListeners() {
        btnNew.addActionListener(this);
        btnOpenChart.addActionListener(this);
        btnSaveChart.addActionListener(this);
        btnPrintChart.addActionListener(this);
        btnMode.addActionListener(this);
        btnNavigationBar.addActionListener(this);
        lblStyle.addActionListener(this);
        btnDragZoom.addActionListener(this);
        btnShowTurnOver.addActionListener(this);
        btnShowVolume.addActionListener(this);
        btnShowVolumeByPrice.addActionListener(this);
        btnShowDataWin.addActionListener(this);
        btnIndexed.addActionListener(this);
        btnSplitAdjusted.addActionListener(this);
        btnShowSplits.addActionListener(this);
        btnShowAnnouncements.addActionListener(this);
        btnChartOrData.addActionListener(this);
        btnShowDescription.addActionListener(this);
        btnGrid.addActionListener(this);
        btnShowCurrentPriceLine.addActionListener(this);
        btnShowPreviousCloseLine.addActionListener(this);
        btnShowResistanceLines.addActionListener(this);
        btnRectangualrSelection.addActionListener(this);
        btnErasor.addActionListener(this);
        btnFibCalculator.addActionListener(this);
        btnShowOrderLines.addActionListener(this);
        btnShowBidAskTags.addActionListener(this);
        btnShowMinMaxLines.addActionListener(this);
        btnSnapToPrice.addActionListener(this);
        btnSnapBall.addActionListener(this);
        btnShowCrossHairs.addActionListener(this);
        lblShowHideISP.addActionListener(this);
        //MouseListeners
        btnPeriods.addMouseListener(this);
        btnIntervals.addMouseListener(this);
        btnStyle.addMouseListener(this);
        lblPeriods.addMouseListener(this);
        lblIntervals.addMouseListener(this);
        lblZoom.addMouseListener(this);
        lblSnapToPrice.addMouseListener(this);
        lblCrossHair.addMouseListener(this);
        lblVolByPrice.addMouseListener(this);
        lblSplitAdjust.addMouseListener(this);
        btnShowHideISP.addMouseListener(this);


    }

    public void setButtonIcons() {

        btnNew.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_new.gif"));
        btnOpenChart.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_open.gif"));
        btnSaveChart.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_save.gif"));
        btnPrintChart.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_print.gif"));

        btnMode.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_history.gif"));
        btnNavigationBar.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_sidebar_show.gif"));
        btnPeriods.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        btnIntervals.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        btnStyle.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        lblStyle.setIcon(styleIcon[0]); // for chart loading with no graphs
        lblZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        //btnDragZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom.gif"));
        btnShowVolume.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol.gif"));
        btnShowVolumeByPrice.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol_by_price.gif"));
        btnShowTurnOver.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_turnover.gif"));
        btnShowDataWin.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_dw.gif"));
        btnIndexed.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_indexed.gif"));
        //btnSplitAdjusted.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_split_adjusted.gif"));
        btnShowSplits.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_splits.gif"));
        btnShowAnnouncements.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_announs.gif"));
        btnShowDescription.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_desc.gif"));
        btnGrid.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_verti.gif"));
        btnShowCurrentPriceLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_current_price.gif"));
        btnShowPreviousCloseLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_previous_close_line.gif"));
        btnShowOrderLines.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_order_lines.gif"));
        btnShowBidAskTags.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_bidAsk_tags.gif"));
        btnShowMinMaxLines.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_min_max.gif"));
        btnSnapToPrice.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_price.gif"));
        btnSnapBall.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_ball.gif"));
        btnShowHideISP.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        lblShowHideISP.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_showhide_isp.gif"));

        //lblZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        lblSnapToPrice.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        lblCrossHair.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        lblVolByPrice.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        lblSplitAdjust.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        btnShowCrossHairs.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_cross_hair.gif"));
        btnShowResistanceLines.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_supRes_lines.gif"));
        btnRectangualrSelection.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_enable_rect_selection.gif"));
        btnChartOrData.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_dataMode.gif"));

        btnNew.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_new_act.gif"));
        btnOpenChart.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_open_act.gif"));
        btnSaveChart.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_save_act.gif"));
        btnPrintChart.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_print_act.gif"));

        //TODO : need to remove - no need of this codes- charithn

        /*
        btnMode.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_intraday_act.gif"));
        btnDragZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom_act.gif"));
        btnShowVolume.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol_act.gif"));
        btnShowDataWin.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_dw_act.gif"));
        btnIndexed.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_indexed_act.gif"));
        btnSplitAdjusted.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_split_adjusted_act.gif"));
        btnShowSplits.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_splits_act.gif"));
        btnShowAnnouncements.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_announs_act.gif"));
        btnShowDescription.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_desc_act.gif"));
        btnGrid.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_verti_act.gif"));
        btnShowCurrentPriceLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_current_price_act.gif"));
        btnRectangualrSelection.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_enable_rect_selection_roll.gif"));


        btnShowResistanceLines.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_supRes_lines_act.gif"));
        btnShowOrderLines.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_order_lines_act.gif"));
        btnShowBidAskTags.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_bidAsk_tags_act.gif"));
        btnShowMinMaxLines.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_min_max_act.gif"));
        btnSnapToPrice.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_price_act.gif"));
        btnShowCrossHairs.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_cross_hair_act.gif"));

        btnMode.setRolloverEnabled(true);
        btnNavigationBar.setRolloverEnabled(true);
        btnNew.setRolloverEnabled(true);
        btnOpenChart.setRolloverEnabled(true);
        btnSaveChart.setRolloverEnabled(true);
        btnPrintChart.setRolloverEnabled(true);
        btnDragZoom.setRolloverEnabled(true);
        btnGrid.setRolloverEnabled(true);
        */

    }

    private void setEmptyBorders() {
        btnNew.setBorder(borderEmpty);
        btnOpenChart.setBorder(borderEmpty);
        btnSaveChart.setBorder(borderEmpty);
        btnPrintChart.setBorder(borderEmpty);
        btnMode.setBorder(borderEmpty);
        btnNavigationBar.setBorder(borderEmpty);
        btnPeriods.setBorder(borderEmpty);
        btnIntervals.setBorder(borderEmpty);
        btnStyle.setBorder(borderEmpty);
        btnDragZoom.setBorder(borderEmpty);
        lblZoom.setBorder(borderEmpty);
        lblSnapToPrice.setBorder(borderEmpty);
        lblCrossHair.setBorder(borderEmpty);
        lblVolByPrice.setBorder(borderEmpty);
        lblSplitAdjust.setBorder(borderEmpty);
        btnShowTurnOver.setBorder(borderEmpty);
        btnShowVolume.setBorder(borderEmpty);
        btnShowVolumeByPrice.setBorder(borderEmpty);
        btnShowDataWin.setBorder(borderEmpty);
        btnIndexed.setBorder(borderEmpty);
        btnSplitAdjusted.setBorder(borderEmpty);
        btnShowSplits.setBorder(borderEmpty);
        btnShowAnnouncements.setBorder(borderEmpty);
        btnChartOrData.setBorder(borderEmpty);
        btnShowDescription.setBorder(borderEmpty);
        lblStyle.setBorder(borderEmpty);
        btnGrid.setBorder(borderEmpty);
        btnShowCurrentPriceLine.setBorder(borderEmpty);
        btnShowPreviousCloseLine.setBorder(borderEmpty);
        btnShowResistanceLines.setBorder(borderEmpty);
        btnRectangualrSelection.setBorder(borderEmpty);
        btnErasor.setBorder(borderEmpty);
        btnFibCalculator.setBorder(borderEmpty);
        btnShowOrderLines.setBorder(borderEmpty);
        btnShowBidAskTags.setBorder(borderEmpty);
        btnShowMinMaxLines.setBorder(borderEmpty);
        btnSnapToPrice.setBorder(borderEmpty);
        btnSnapBall.setBorder(borderEmpty);
        btnShowCrossHairs.setBorder(borderEmpty);
        lblShowHideISP.setBorder(borderEmpty);
        btnShowHideISP.setBorder(borderEmpty);
    }

    private void setToolTipText() {

        btnNew.setToolTipText(Language.getString("NEW"));
        btnOpenChart.setToolTipText(Language.getString("OPEN_CHART"));
        btnSaveChart.setToolTipText(Language.getString("SAVE_CHART"));
        btnPrintChart.setToolTipText(Language.getString("PRINT_CHART"));
        lblPeriods.setToolTipText(Language.getString("PERIOD"));
        btnPeriods.setToolTipText(Language.getString("PERIOD"));
        btnIntervals.setToolTipText(Language.getString("INTERVAL"));
        lblIntervals.setToolTipText(Language.getString("INTERVAL"));
        btnStyle.setToolTipText(Language.getString("STYLE"));
        lblStyle.setToolTipText(Language.getString("STYLE"));
        lblShowHideISP.setToolTipText(Language.getString("SHOW_HIDE_ISP"));
        btnShowHideISP.setToolTipText(Language.getString("SHOW_HIDE_ISP"));
        btnGrid.setToolTipText(Language.getString("GRID_TYPE"));
        //TODO : need to remove - no need of this codes- charithn

        /*
        btnMode.setToolTipText(Language.getString("HISTORY_MODE"));
        btnNavigationBar.setToolTipText(Language.getString("SHOW_CHART_NAVIGATION_BAR"));
        btnDragZoom.setToolTipText(Language.getString("DRAG_ZOOM_ON"));
        lblZoom.setToolTipText(Language.getString("ZOOM"));
        lblSnapToPrice.setToolTipText(Language.getString("SNAP_TO_PRICE_ON"));
        lblCrossHair.setToolTipText(Language.getString("CROSS_HAIR_ENABLE"));
        btnShowVolume.setToolTipText(Language.getString("SHOW_VOLUME"));
        btnShowDataWin.setToolTipText(Language.getString("SHOW_DATA_WINDOW"));
        btnIndexed.setToolTipText(Language.getString("INDEXED_OFF"));
        btnSplitAdjusted.setToolTipText(Language.getString("SPLIT_ADJUSTED_OFF"));
        btnShowSplits.setToolTipText(Language.getString("HIDE_SPLITS"));
        btnShowAnnouncements.setToolTipText(Language.getString("HIDE_ANNOUNCEMENTS"));
        btnShowDescription.setToolTipText(Language.getString("SHOW_DESCRIPTION"));
        lblPeriods.setToolTipText(Language.getString("PERIOD"));
        lblIntervals.setToolTipText(Language.getString("INTERVAL"));
        lblStyle.setToolTipText(Language.getString("STYLE"));
        btnGrid.setToolTipText(Language.getString("GRID_TYPE"));
        btnShowCurrentPriceLine.setToolTipText(Language.getString("SHOW_CURRENT_PRICE_LINE"));
        btnShowResistanceLines.setToolTipText(Language.getString("SHOW_RESISTANCE_LINE"));
        btnShowOrderLines.setToolTipText(Language.getString("SHOW_ORDER_LINES"));
        btnShowBidAskTags.setToolTipText(Language.getString("SHOW_BID_ASK_TAGS"));
        btnShowMinMaxLines.setToolTipText(Language.getString("SHOW_MIN_MAX_LINES"));
        btnSnapToPrice.setToolTipText(Language.getString("ENABLE_SNAP_TO_PRICE"));

        btnShowCrossHairs.setToolTipText(Language.getString("SHOW_CROSS_HAIR"));
        btnRectangualrSelection.setToolTipText(Language.getString("GRAPH_RECTANGUALR_SELECTION"));
        btnFibCalculator.setToolTipText(Language.getString("GRAPH_FIB_CALCULATOR"));
        */
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == btnNew) newChartPopClicked();
        else if (obj == miNewChart) {
            ChartFrame cf = ChartFrame.getSharedInstance();
            if (cf.getTotalChartCount() >= cf.getMaxChartCount()) {
                String message = "Graph limit exceeded. Maximum %s graphs allowed.";
                message = String.format(message, cf.getMaxChartCount());
                JOptionPane.showConfirmDialog(cf, message, "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            } else {
                parent.addNewGraph(true, false);
                parent.tileWindows();
            }
        } else if (obj == miNewLayout) parent.createLayout();
        else if (obj == btnOpenChart) parent.openChart();
        else if (obj == btnShowDataWin) parent.toggleShowDataWindow();
        else if (obj == btnNavigationBar) {
            boolean isNavigationBarVisible = !ChartFrame.getSharedInstance().isChatNavigationBarVisible();
            ChartFrame.getSharedInstance().setChartNavigationBarVisible(isNavigationBarVisible);
            ChartNavigationBar.getSharedInstance().controlSideBar();
        } else if (activeGraph != null) {
            if (obj == btnSaveChart) activeGraph.saveClicked(false);
            else if (obj == btnPrintChart) activeGraph.printChart();
            else if (obj == btnMode) activeGraph.toggleGraphMode();
            else if (obj == lblStyle) stylePopClicked();
            else if (obj == btnDragZoom) {
                if (isDragZoomSelected) {
                    activeGraph.dragZoomEnabledClicked();
                    if (activeGraph.isZoomDragEnabled()) {
                        btnDragZoom.setToolTipText(Language.getString("DRAG_ZOOM_OFF"));
                    } else {
                        btnDragZoom.setToolTipText(Language.getString("DRAG_ZOOM_ON"));
                    }
                } else {
                    activeGraph.boxZoomEnabledClick();
                    if (activeGraph.isBoxZoomEnabled()) {
                        btnDragZoom.setToolTipText(Language.getString("BOX_ZOOM_OFF"));
                    } else {
                        btnDragZoom.setToolTipText(Language.getString("BOX_ZOOM_ON"));
                    }
                }
            } else if (obj == btnShowVolume) activeGraph.toggleShowVolume();
            else if (obj == btnShowVolumeByPrice) activeGraph.toggleShowVolumeByPrice();
            else if (obj == btnShowTurnOver) activeGraph.toggleShowTurnOver();
            else if (obj == btnIndexed) activeGraph.toggleIndexed();
            else if (obj == btnSplitAdjusted) activeGraph.toggleSplitAdjusted();
            else if (obj == btnShowSplits) activeGraph.toggleShowSplits();
            else if (obj == btnShowAnnouncements) activeGraph.toggleShowAnnouncements();
            else if (obj == btnShowDescription) activeGraph.toggleLegendType();
            else if (obj == btnGrid) activeGraph.toggleGridType();
            else if (obj == btnChartOrData) activeGraph.switchPanel();
            else if (obj == btnShowCurrentPriceLine) activeGraph.graph.GDMgr.toggleShowCurrentPriceLine();
            else if (obj == btnShowPreviousCloseLine) activeGraph.graph.GDMgr.toggleShowPreviousCloseLine();
            else if (obj == btnShowOrderLines) activeGraph.graph.GDMgr.toggleShowOrderLines();
            else if (obj == btnShowBidAskTags) activeGraph.graph.GDMgr.toggleShowBidAskTags();
            else if (obj == btnShowMinMaxLines) activeGraph.graph.GDMgr.toggleShowMinMaxPriceLines();
            else if (obj == btnShowResistanceLines) activeGraph.graph.GDMgr.toggleshowResistanceLines();
            else if (obj == btnShowCrossHairs) parent.toggleCrossHair();
            else if (obj == btnSnapToPrice) activeGraph.graph.GDMgr.toggleSnapToPrice();
            else if (obj == btnSnapBall) parent.toggleSnapBall();
            else if (obj == lblShowHideISP) showHIdeISPPopClicked();
                //TODO: is this the correct way-need to finalize
                //Todo: better to shift the implementation in to parent (ChartFrame) class
            else if (obj == btnRectangualrSelection)
                toggleRectangularSelection();       //rectangular selection tool - added by charithn
            else if (obj == btnFibCalculator)
                toggleFiboCalculator(); //fibonacci price retracement calculator - added by charithn
            else if (obj == btnErasor) {        //erasor mode
                toggleErasorMode();
            }

        }
        ChartFrame.getSharedInstance().refreshToolBars();
    }

    public void toggleErasorMode() {
        //setErasorMode(!isErasorMode);
        JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
        if (frames != null) {
            for (JInternalFrame frame : frames) {
                if (frame instanceof GraphFrame) {
                    ((GraphFrame) frame).setErasorMode(!((GraphFrame) frame).isErasorMode());
                    // ((GraphFrame) frame).setRectangularSelection(false);

                    if (((GraphFrame) frame).isErasorMode()) {
                        BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_ERASOR;
                        ((GraphFrame) frame).graph.setCursor(BottomToolBar.createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_Eraser.gif"));
                    } else {
                        ((GraphFrame) frame).graph.setCursor(Cursor.getDefaultCursor());
                        BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_NONE;
                    }
                    parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
                    ((GraphFrame) frame).isRectangularSelection = false;
                    ((GraphFrame) frame).isFibCalMode = false;
                }
            }
        }
    }

    private void toggleFiboCalculator() {
        try {
            if (activeGraph != null) {
                parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
                BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_NONE;
                activeGraph.isRectangularSelection = false;
                activeGraph.isErasorMode = false;
                if (!activeGraph.isFibPriceCalcEnabled()) {
                    //TODO: need to finalize the requirement further
                    /* JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
                    if (frames != null) {
                        for (JInternalFrame frame : frames) {
                            if (frame instanceof GraphFrame) {
                                //((GraphFrame) frame).graph.setCursor(BottomToolBar.createCursor("images/Theme" + Theme.getID() + "/graph_obj_fibRetrc.gif"));
                                if(!(((GraphFrame) frame).isActive())){
                                    ((GraphFrame) frame).setFibPriceCalcEnabled(false);
                                }
                            }
                        }
                    }*/
                    activeGraph.graph.setCursor(BottomToolBar.createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fib_calc.gif"));
                    activeGraph.fibPriceCalEnabledClick();
                    BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_FIB_PRICE;
                } else {
                    activeGraph.graph.setCursor(Cursor.getDefaultCursor());
                    BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_NONE;
                    activeGraph.setFibPriceCalcEnabled(false);
                    activeGraph.clickCount = 0;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void toggleRectangularSelection() {
        if (parent.bottomToolBar.ObjectTobeDrawn != AbstractObject.INT_RECT_SELECTION) {
            parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_RECT_SELECTION;
            BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_REC_SELECTION;

            JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
            if (frames != null) {
                for (JInternalFrame frame : frames) {
                    if (frame instanceof GraphFrame) {
                        ((GraphFrame) frame).isRectangularSelection = true;
                        ((GraphFrame) frame).isErasorMode = false;
                        ((GraphFrame) frame).isFibCalMode = false;
                        ((GraphFrame) frame).graph.setCursor(BottomToolBar.createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_rect_selection.gif"));
                    }
                }
            }

        } else {
            parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
            BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_NONE;
            JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
            if (frames != null) {
                for (JInternalFrame frame : frames) {
                    if (frame instanceof GraphFrame) {
                        ((GraphFrame) frame).isRectangularSelection = false;
                        ((GraphFrame) frame).setFibPriceCalcEnabled(false);
                        ((GraphFrame) frame).graph.setCursor(Cursor.getDefaultCursor());
                    }
                }
            }
        }
    }

    public void newChartPopClicked() {
        if (!btnNew.isEnabled()) return;

        popNewCharts = new JPopupMenu();
        miNewChart = new TWMenuItem(Language.getString("CHART"));
        miNewChart.addActionListener(this);
        miNewChart.setIconFile("graph_Newchart.GIF");
        miNewChart.setToolTipText(Language.getString("NEW_CHART"));
        popNewCharts.add(miNewChart);

        boolean chartsAdded = (parent.getDesktop().getComponentCount() > 0);
        miNewLayout = new TWMenuItem(Language.getString("MENUITEM_LAYOUT"));
        miNewLayout.addActionListener(this);
        miNewLayout.setIconFile("graph_Newlayout.GIF");
        miNewLayout.setEnabled(chartsAdded);
        miNewLayout.setToolTipText(Language.getString("NEW_LAYOUT"));
        popNewCharts.add(miNewLayout);

        int popW;
        if (!Language.isLTR()) {
            popW = (int) popNewCharts.getPreferredSize().getWidth();
            popNewCharts.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            miNewChart.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            miNewLayout.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        } else {
            popW = (int) btnNew.getPreferredSize().getWidth();
        }
        popNewCharts.show(btnNew, btnNew.getWidth() - popW, btnNew.getHeight());
    }

    private void periodPopClicked() {
        popPeriods = null;
        //if (popPeriods==null)
        popPeriods = activeGraph.getPeriodPopup();
        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popPeriods.getPreferredSize().getWidth();
        }
        popPeriods.addPopupMenuListener(activeGraph);
        popPeriods.show(btnPeriods, btnPeriods.getWidth() - popW, btnPeriods.getHeight());
    }

    private void intervalPopClicked() {
        popIntervals = null;
        //if (popIntervals==null)
        popIntervals = activeGraph.getIntervalPopup();
        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popIntervals.getPreferredSize().getWidth();
        }
        popIntervals.addPopupMenuListener(activeGraph);
        popIntervals.show(btnIntervals, btnIntervals.getWidth() - popW, btnIntervals.getHeight());
    }

    private void stylePopClicked() {
        popStyles = null;
        //if (popStyles==null)
        popStyles = activeGraph.getStylePopup(styleIcon, styleActiveIcon);
        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popStyles.getPreferredSize().getWidth();
        }
        popStyles.addPopupMenuListener(activeGraph);
        popStyles.show(btnStyle, btnStyle.getWidth() - popW, btnStyle.getHeight());
    }

    private void showHIdeISPPopClickedold() {

        popShowHideISP = new JPopupMenu();
        //name array
        final ArrayList<ChartProperties> sources = activeGraph.graph.GDMgr.getSources();
        String[] actISPNameArr = null;
        if (sources.size() > 0) {
            actISPNameArr = new String[sources.size()];
            int i = 0;
            for (java.util.Iterator it = sources.iterator(); it.hasNext(); ) {
                final ChartProperties cp = (ChartProperties) it.next();
                if (cp.getID() > activeGraph.graph.GDMgr.ID_COMPARE && cp.getID() != activeGraph.graph.GDMgr.ID_VOLUME && cp.getID() != activeGraph.graph.GDMgr.ID_TURNOVER) {

                    final TWMenuItem itemISP = new TWMenuItem(cp.getShortName(), "");
                    if (cp.isHidden()) {
                        itemISP.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
                    } else {
                        itemISP.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
                    }

                    itemISP.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            cp.setHidden(!cp.isHidden());
                            if (cp.isHidden()) {
                                itemISP.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));

                                if ((cp.getRect() != activeGraph.graph.mainRect)) {
                                    Rectangle rect = cp.getRect();
                                    boolean rectEmpty = true;
                                    for (int i = sources.size() - 1; i >= 0; i--) {
                                        ChartProperties cpEXt = (ChartProperties) sources.get(i);
                                        if (cpEXt.getRect() == rect && !cpEXt.isHidden()) {
                                            rectEmpty = false;
                                            break;
                                        }
                                    }
                                    if (rectEmpty) {
                                        //int index = activeGraph.graph.panels.lastIndexOf(rect);
                                        int indexCp = activeGraph.graph.GDMgr.getIndexOfTheRect(activeGraph.graph.panels, rect);
                                        if (indexCp > -1) {
                                            activeGraph.graph.deleteISPPanel(activeGraph.graph.GDMgr.getIndexOfTheRect(activeGraph.graph.panels, cp.getRect()));
                                        }
                                    }
                                }
                            } else {
                                itemISP.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
                                if (cp.getRect() != activeGraph.graph.mainRect) {
                                    activeGraph.graph.addNewPanel(false, cp.getRect());
                                    activeGraph.graph.reDrawAll();

                                }
                            }
                            activeGraph.repaint();
                        }
                    });
                    i++;
                    popShowHideISP.add(itemISP);
                }

            }

        } else {

        }

        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popShowHideISP.getPreferredSize().getWidth();
        }
        GUISettings.applyOrientation(popShowHideISP);
//        popShowHideISP.addPopupMenuListener(activeGraph);
        popShowHideISP.show(btnShowHideISP, btnShowHideISP.getWidth() - popW, btnShowHideISP.getHeight());


    }

    private void showHIdeISPPopClicked() {

        if (activeGraph != null && activeGraph.graph.GDMgr.getIndicators().size() == 0) {
            return;
        }
        popShowHideISP = activeGraph.getShowHideISPPopUP();
        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popShowHideISP.getPreferredSize().getWidth();
        }
        GUISettings.applyOrientation(popShowHideISP);
//        popShowHideISP.addPopupMenuListener(activeGraph);
        popShowHideISP.show(btnShowHideISP, btnShowHideISP.getWidth() - popW, btnShowHideISP.getHeight());


    }

    private void zoomPopClicked() {
        popZoom = new JPopupMenu();
        TWMenuItem itmDragZoom;
        if (activeGraph.isZoomDragEnabled()) {
            itmDragZoom = new TWMenuItem(Language.getString("DRAG_ZOOM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_dragZoom.gif"));
        } else {
            itmDragZoom = new TWMenuItem(Language.getString("DRAG_ZOOM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom.gif"));
        }
        itmDragZoom.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                activeGraph.dragZoomEnabledClicked();
                if (activeGraph.isZoomDragEnabled()) {
                    btnDragZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_dragZoom.gif"));
                    btnDragZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_dragZoom_act.gif"));
                    btnDragZoom.setToolTipText(Language.getString("DRAG_ZOOM_OFF"));
                    activeGraph.setBoxZoomEnabled(false);
                } else {
                    btnDragZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom.gif"));
                    btnDragZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom_act.gif"));
                    btnDragZoom.setToolTipText(Language.getString("DRAG_ZOOM_ON"));
                }
                isDragZoomSelected = true;
            }
        });
        popZoom.add(itmDragZoom);
        TWMenuItem itmBoxZoom;
        if (activeGraph.isBoxZoomEnabled()) {
            itmBoxZoom = new TWMenuItem(Language.getString("BOX_ZOOM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_boxZoom.gif"));
        } else {
            itmBoxZoom = new TWMenuItem(Language.getString("BOX_ZOOM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_boxZoom.gif"));
        }
        itmBoxZoom.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                activeGraph.boxZoomEnabledClick();
                if (activeGraph.isBoxZoomEnabled()) {
                    btnDragZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_boxZoom.gif"));
                    btnDragZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_boxZoom_act.gif"));
                    btnDragZoom.setToolTipText(Language.getString("BOX_ZOOM_OFF"));
                    activeGraph.setDragZoomEnabled(false);
                } else {
                    btnDragZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_boxZoom.gif"));
                    btnDragZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_boxZoom_act.gif"));
                    btnDragZoom.setToolTipText(Language.getString("BOX_ZOOM_ON"));
                }
                isDragZoomSelected = false;
            }
        });
        popZoom.add(itmBoxZoom);
        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popZoom.getPreferredSize().getWidth();
        }
        GUISettings.applyOrientation(popZoom);
        popZoom.show(lblZoom, lblZoom.getWidth() - popW, lblZoom.getHeight());
    }

    //snap values
    private void snapToPricePopClicked() {
        popSnapToPrice = new JPopupMenu();
        String[] ohlcStringArr = {Language.getString("AUTOMATIC"), Language.getString("OPEN"), Language.getString("HIGH"),
                Language.getString("LOW"), Language.getString("CLOSE")};
        final TWMenuItem[] itemOHLC = new TWMenuItem[ohlcStringArr.length];
        for (int i = 0; i < ohlcStringArr.length; i++) {
            itemOHLC[i] = new TWMenuItem(ohlcStringArr[i], "");
            itemOHLC[i].setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            final int index = i;
            itemOHLC[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    final String s = (new Integer(index)).toString();
                    activeGraph.graph.GDMgr.setSnapToPricePriority(Byte.parseByte(s));
                }
            });
            popSnapToPrice.add(itemOHLC[i]);
        }

        int currentSnapPriority = activeGraph.graph.GDMgr.getSnapToPricePriority();
        itemOHLC[currentSnapPriority].setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popSnapToPrice.getPreferredSize().getWidth();
        }
        GUISettings.applyOrientation(popSnapToPrice);
        popSnapToPrice.show(lblSnapToPrice, lblSnapToPrice.getWidth() - popW, lblSnapToPrice.getHeight());
    }


    private void crossHairPopClicked() {
        popcrossHair = new JPopupMenu();
        String[] ohlcStringArr = {Language.getString("NORMAL_CROSS_HAIR"), Language.getString("SNAP_BALL"),
                Language.getString("HIGH_LOW")};
        final TWMenuItem[] itemCrossHair = new TWMenuItem[ohlcStringArr.length];
        for (int i = 0; i < ohlcStringArr.length; i++) {
            itemCrossHair[i] = new TWMenuItem(ohlcStringArr[i], "");
            itemCrossHair[i].setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            final int index = i;
            itemCrossHair[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    final String s = (new Integer(index)).toString();
                    activeGraph.graph.GDMgr.setCrossHairType(Byte.parseByte(s));
                    ChartFrame.getSharedInstance().setShowCrossHairs(true);
                    ChartFrame.getSharedInstance().refreshTopToolbar();

                    //patch

                }
            });
            popcrossHair.add(itemCrossHair[i]);
        }

        int currentCrossHairType = activeGraph.graph.GDMgr.getCrossHairType();
        itemCrossHair[currentCrossHairType].setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popcrossHair.getPreferredSize().getWidth();
        }
        GUISettings.applyOrientation(popcrossHair);
        popcrossHair.show(lblCrossHair, lblCrossHair.getWidth() - popW, lblCrossHair.getHeight());

    }

    private void volumeByPricePopClicked() {

        popVolByPrice = new JPopupMenu();
        String[] types = {Language.getString("VOL_BY_PRICE_NET_CHANGE"), Language.getString("GRAPH_BY_PCT_CHANGE"), Language.getString("TOTAL")};
        final TWMenuItem[] itemTypes = new TWMenuItem[types.length];
        for (int i = 0; i < types.length; i++) {
            itemTypes[i] = new TWMenuItem(types[i], "");
            itemTypes[i].setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            final int index = i;
            itemTypes[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    final String s = (new Integer(index)).toString();
                    activeGraph.graph.GDMgr.setVolumeByPriceType(Byte.parseByte(s));
                    activeGraph.graph.GDMgr.setShowVolumeByPrice(true);
                    ChartFrame.getSharedInstance().refreshTopToolbar();
                }
            });
            popVolByPrice.add(itemTypes[i]);
        }

        int type = activeGraph.graph.GDMgr.getVolumeByPriceType();
        itemTypes[type].setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
        int popW = 0;
        if (Language.isLTR()) {
            popW = (int) popVolByPrice.getPreferredSize().getWidth();
        }
        GUISettings.applyOrientation(popVolByPrice);
        popVolByPrice.show(lblVolByPrice, lblVolByPrice.getWidth() - popW, lblVolByPrice.getHeight());

    }

    //added charithn
    private void splitAdjustPopClicked() {

        try {
            popSplitAdjust = new JPopupMenu();
            /*String[] types = {Language.getString("TIP_STOCK_DIVIDEND"), Language.getString("TIP_CASH_DIVIDEND"),
                    Language.getString("GRAPH_RIGHT_ISSUE"), Language.getString("TIP_STOCK_SPLIT")};*/
            String[] types = {Language.getString("GRAPH_ADJUST_SD"), Language.getString("GRAPH_ADJUST_CD"),
                    Language.getString("GRAPH_ADJUST_RI"), Language.getString("GRAPH_ADJUST_SS")};
            /*String[] types = {Language.getString("GRAPH_ADJUST_SD"),
                    Language.getString("GRAPH_ADJUST_RI"), Language.getString("GRAPH_ADJUST_SS")};*/
            final TWCheckBoxMenuItem[] itemTypes = new TWCheckBoxMenuItem[types.length];

            for (int i = 0; i < types.length; i++) {
                itemTypes[i] = new TWCheckBoxMenuItem(types[i], "");
                if (activeGraph.graph.GDMgr.getSplitAdjustMethod() == GraphDataManager.METHOD_NONE) {
                    itemTypes[i].setSelected(true);
                } else {
                    itemTypes[i].setSelected(activeGraph.graph.GDMgr.adjustTypes[i]);
                }

                final int index = i;
                itemTypes[i].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        activeGraph.graph.GDMgr.setAdjustMethod(GraphDataManager.METHOD_UNADJUSTED);
                        activeGraph.graph.GDMgr.adjustTypes[index] = itemTypes[index].isSelected();
                        if (isAllChecked(activeGraph.graph.GDMgr.adjustTypes)) {
                            activeGraph.graph.GDMgr.setAdjustMethod(GraphDataManager.METHOD_NONE);
                        }
                        if (activeGraph.graph != null) {
                            activeGraph.graph.reDrawAll();
                        }
                        ChartFrame.getSharedInstance().refreshTopToolbar();
                        activeGraph.setGraphTitle();
                    }
                });
                popSplitAdjust.add(itemTypes[i]);
            }
            int popW = 0;
            if (Language.isLTR()) {
                popW = (int) popSplitAdjust.getPreferredSize().getWidth();
            }
            GUISettings.applyOrientation(popSplitAdjust);
            popSplitAdjust.show(lblSplitAdjust, lblSplitAdjust.getWidth() - popW, lblSplitAdjust.getHeight());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean isAllChecked(boolean[] types) {

        for (int i = 0; i < types.length; i++) {
            if (!types[i]) {
                return false;
            }
        }
        return true;
    }

    private int getSplitTypeForMenuItem(int index) {

        switch (index) {
            case 0:
                System.out.println("************* stock dividned");
                return ChartSplitPoints.TYPE_STOCK_DIVIDEND;
            case 1:
                System.out.println("************* cash dividned");
                return ChartSplitPoints.TYPE_CASH_DIVIDEND;
            case 2:
                System.out.println("************* right issue");
                return ChartSplitPoints.TYPE_RIGHT_ISSUE;
            case 3:
                System.out.println("************* stock split");
                return ChartSplitPoints.TYPE_STOCK_SPLIT;
        }
        return ChartSplitPoints.TYPE_STOCK_DIVIDEND;
    }

    //activeGraph
    public GraphFrame getActiveGraph() {
        return activeGraph;
    }

    public void setActiveGraph(GraphFrame gf) {
        activeGraph = gf;
    }
    //#########################################

    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {
        Object obj = e.getSource();
        if ((activeGraph != null) && (((Component) e.getSource()).isEnabled())) {

            if (obj == lblPeriods) periodPopClicked();
            else if (obj == lblIntervals) intervalPopClicked();
            else if (obj == btnPeriods) periodPopClicked();
            else if (obj == btnIntervals) intervalPopClicked();
            else if (obj == btnStyle) stylePopClicked();
            else if (obj == lblZoom) zoomPopClicked();
            else if (obj == lblSnapToPrice) snapToPricePopClicked();
            else if (obj == lblCrossHair) crossHairPopClicked();
            else if (obj == lblVolByPrice) volumeByPricePopClicked();
            else if (obj == lblSplitAdjust) splitAdjustPopClicked();
            else if (obj == btnShowHideISP) showHIdeISPPopClicked();

            ChartFrame.getSharedInstance().refreshToolBars();
        }
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
    }
    //#########################################

    public void refreshToolBar(boolean detached, boolean histroyEnabled,
                               boolean indicatorsEnabled, boolean currentMode, String period,
                               String interval, int style, boolean zoomEnabled, boolean boxZoomEnabled,
                               boolean showVolume, boolean showVolumeByPrice, boolean showTurnover, boolean indexed, int legendType, boolean showSplits, byte gridType,
                               boolean tableMode, boolean staticChart, boolean showingDataWin,
                               boolean splitAdjusted, boolean showAnnouncements,
                               boolean showCurrentPriceLine, boolean showPreviousCloseLine, boolean showOrderLines, boolean showBidAskTags, boolean showMinMaxLines,
                               boolean showCrossHair, boolean showResistanceLines, boolean snapToPrice, boolean fibCalulator,
                               boolean rectangularSelection, boolean snapBall, boolean chartNavigationBarVisible, boolean erasorEnabled, byte splitAdjustMethod) {

        setButtonsEnabled(true);
        if (currentMode) {
            btnMode.setIcon(modeIcon[ICON_HISTORY]);
            btnMode.setRolloverIcon(modeIcon[ICON_HISTORY + 2]);
            btnMode.setToolTipText(Language.getString("HISTORY_MODE"));
        } else {
            btnMode.setIcon(modeIcon[ICON_CURRENT]);
            btnMode.setRolloverIcon(modeIcon[ICON_CURRENT + 2]);
            btnMode.setToolTipText(Language.getString("CURRENT_MODE"));
        }

        refreshGridButton(gridType);
        refreshSplitAdjustButton(splitAdjustMethod);

        btnMode.setEnabled(!tableMode);
        //btnNavigationBar.setEnabled(!tableMode);
        btnNavigationBar.setEnabled(true);

        lblPeriods.setEnabled(!tableMode);
        btnPeriods.setEnabled(!tableMode);
        lblIntervals.setEnabled(!tableMode);
        btnIntervals.setEnabled(!tableMode);

        lblStyle.setEnabled(!tableMode);
        btnStyle.setEnabled(!tableMode);
        btnDragZoom.setEnabled(!tableMode);
        lblZoom.setEnabled(!tableMode);
        btnShowTurnOver.setEnabled(!tableMode);
        btnShowVolume.setEnabled(!tableMode);
        btnShowVolumeByPrice.setEnabled(!tableMode);
        btnShowDataWin.setEnabled(!tableMode);
        btnIndexed.setEnabled(!tableMode);
        btnSplitAdjusted.setEnabled(!tableMode);
        btnShowSplits.setEnabled(!tableMode);
        btnShowAnnouncements.setEnabled(!tableMode);
        btnShowDescription.setEnabled(!tableMode);
        btnGrid.setEnabled(!tableMode);
        btnShowCurrentPriceLine.setEnabled(!tableMode);
        btnShowPreviousCloseLine.setEnabled(!tableMode);

        //btnShowResistanceLines.setEnabled(!tableMode && currentMode);
        btnShowResistanceLines.setEnabled(!tableMode);
        btnRectangualrSelection.setEnabled(!tableMode);
        btnErasor.setEnabled(!tableMode);
        btnFibCalculator.setEnabled(!tableMode);
        btnShowOrderLines.setEnabled(!tableMode && ChartInterface.isTradingConnected(SharedMethods.getExchangeFromKey(parent.getActiveGraph().getBaseKey())));
        btnShowBidAskTags.setEnabled(!tableMode);
        btnShowMinMaxLines.setEnabled(!tableMode);
        btnSnapToPrice.setEnabled(!tableMode);
        btnSnapBall.setEnabled(!tableMode);
        lblShowHideISP.setEnabled(!tableMode);
        btnShowHideISP.setEnabled(!tableMode);
        lblSnapToPrice.setEnabled(!tableMode);
        lblCrossHair.setEnabled(!tableMode);
        lblVolByPrice.setEnabled(!tableMode);
        lblSplitAdjust.setEnabled(!tableMode);
        btnShowCrossHairs.setEnabled(!tableMode);
        btnRectangualrSelection.setEnabled(!tableMode);
        btnFibCalculator.setEnabled(!tableMode);

        setTextOnLbl(lblPeriods, pnlPeriod, period);
        setTextOnLbl(lblIntervals, pnlInterval, interval);
        setStyleImage(style);

        boolean showDesc = legendType > StockGraph.INT_LEGEND_SYMBOL;

        setTooltipForBtn(btnShowVolume, showVolume, "HIDE_VOLUME",
                "SHOW_VOLUME");
        setTooltipForBtn(btnShowVolumeByPrice, showVolumeByPrice, "HIDE_VOLUME_BY_PRICE",
                "SHOW_VOLUME_BY_PRICE");
        setTooltipForBtn(btnShowTurnOver, showTurnover, "HIDE_TURNOVER",
                "SHOW_TURNOVER");
        setTooltipForBtn(btnShowDataWin, showingDataWin, "HIDE_DATA_WINDOW",
                "SHOW_DATA_WINDOW");
        setTooltipForBtn(btnIndexed, indexed, "INDEXED_OFF",
                "INDEXED_ON");
        /*setTooltipForBtn(btnSplitAdjusted, splitAdjusted, "SPLIT_ADJUSTED_ON",
                "SPLIT_ADJUSTED_OFF");*/
        setTooltipForBtn(btnShowSplits, showSplits, "HIDE_SPLITS",
                "SHOW_SPLITS");
        setTooltipForBtn(btnShowAnnouncements, showAnnouncements, "HIDE_ANNOUNCEMENTS",
                "SHOW_ANNOUNCEMENTS");
        setTooltipForBtn(btnChartOrData, tableMode, "DATAMODE_OFF",
                "DATAMODE_ON");
        setTooltipForBtn(btnShowDescription, showDesc, "HIDE_DESCRIPTION",
                "SHOW_DESCRIPTION");
        setTooltipForBtn(btnShowCurrentPriceLine, showCurrentPriceLine, "HIDE_CURRENT_PRICE_LINE",
                "SHOW_CURRENT_PRICE_LINE");
        setTooltipForBtn(btnShowPreviousCloseLine, showPreviousCloseLine, "HIDE_PREVIOUS_CLOSE_LINE",
                "SHOW_PREVIOUS_CLOSE_LINE");
        setTooltipForBtn(btnShowResistanceLines, showResistanceLines, "HIDE_RESISTANCE_LINE",
                "SHOW_RESISTANCE_LINE");
        setTooltipForBtn(btnShowOrderLines, showOrderLines, "HIDE_ORDER_LINES",
                "SHOW_ORDER_LINES");
        setTooltipForBtn(btnShowBidAskTags, showBidAskTags, "HIDE_BID_ASK_TAGS",
                "SHOW_BID_ASK_TAGS");
        setTooltipForBtn(btnShowMinMaxLines, showMinMaxLines, "HIDE_MIN_MAX_LINES",
                "SHOW_MIN_MAX_LINES");
        setTooltipForBtn(btnSnapToPrice, snapToPrice, "ENABLE_SNAP_TO_PRICE", "DISABLE_SNAP_TO_PRICE");

        setTooltipForBtn(btnSnapBall, snapBall, "DISABLE_SNAP_BALL",
                "ENABLE_SNAP_BALL");
        setTooltipForBtn(btnShowCrossHairs, showCrossHair, "HIDE_CROSS_HAIR",
                "SHOW_CROSS_HAIR");
        setTooltipForBtn(btnRectangualrSelection, rectangularSelection, "GRAPH_DISABLE_RECT_SELECTION",
                "GRAPH_ENABLE_RECT_SELECTION");
        setTooltipForBtn(btnErasor, erasorEnabled, "GRAPH_DISABLE_ERASOR",
                "GRAPH_ENABLE_ERASOR");
        setTooltipForBtn(btnFibCalculator, fibCalulator, "GRAPH_HIDE_FIBCAL",
                "GRAPH_SHOW_FIBCAL");
        setTooltipForBtn(btnNavigationBar, chartNavigationBarVisible, "HIDE_CHART_NAVIGATION_BAR",
                "SHOW_CHART_NAVIGATION_BAR");

        setIconForZoomBtn(btnDragZoom, zoomEnabled, boxZoomEnabled, dragZoomIcon);
        setIconForBtn(btnShowVolume, showVolume, showVolumeIcon);
        setIconForBtn(btnShowVolumeByPrice, showVolumeByPrice, showVolumeByPriceIcon);
        setIconForBtn(btnShowTurnOver, showTurnover, showTurnOverIcon);
        setIconForBtn(btnShowDataWin, showingDataWin, showDataWinIcon);
        setIconForBtn(btnIndexed, indexed, indexedIcon);
        //setIconForBtn(btnSplitAdjusted, splitAdjusted, splitAdjustedIcon);
        setIconForBtn(btnShowSplits, showSplits, showSplitsIcon);
        setIconForBtn(btnShowAnnouncements, showAnnouncements, showAnnouncementsIcon);
        setIconForBtn(btnChartOrData, tableMode, dataModeIcon);
        setIconForBtn(btnShowDescription, showDesc, showDescIcon);
        setIconForBtn(btnShowResistanceLines, showResistanceLines, showResistanceIcon);//icon
        setIconForBtn(btnShowCurrentPriceLine, showCurrentPriceLine, showCurrentPriceIcon);
        setIconForBtn(btnShowPreviousCloseLine, showPreviousCloseLine, showPreviousCloseIcon);
        setIconForBtn(btnShowOrderLines, showOrderLines, showOrderLinesIcon);
        setIconForBtn(btnShowBidAskTags, showBidAskTags, showBidAskTagsIcon);
        setIconForBtn(btnShowMinMaxLines, showMinMaxLines, showMinMaxLinesIcon);
        setIconForBtn(btnSnapToPrice, snapToPrice, snapToPriceIcon);
        setIconForBtn(btnSnapBall, snapBall, snapBallIcon);

        setIconForBtn(btnShowCrossHairs, showCrossHair, showCrossHairsIcon);
        setIconForBtn(btnFibCalculator, fibCalulator, fibCalcIcon);
        setIconForBtn(btnRectangualrSelection, rectangularSelection, recSelectionIcon);
        setIconForBtn(btnErasor, erasorEnabled, erasorIcon);
        setIconForBtn(btnNavigationBar, chartNavigationBarVisible, navigationBarIcon);

        setRollIconForZoomBtn(btnDragZoom, zoomEnabled, boxZoomEnabled, dragZoomIcon);
        setRollIconForBtn(btnShowVolume, showVolume, showVolumeIcon);
        setRollIconForBtn(btnShowVolumeByPrice, showVolumeByPrice, showVolumeByPriceIcon);
        setRollIconForBtn(btnShowTurnOver, showTurnover, showTurnOverIcon);
        setRollIconForBtn(btnShowDataWin, showingDataWin, showDataWinIcon);
        setRollIconForBtn(btnIndexed, indexed, indexedIcon);
        //setRollIconForBtn(btnSplitAdjusted, splitAdjusted, splitAdjustedIcon);
        setRollIconForBtn(btnShowSplits, showSplits, showSplitsIcon);
        setRollIconForBtn(btnShowAnnouncements, showAnnouncements, showAnnouncementsIcon);
        setRollIconForBtn(btnChartOrData, tableMode, dataModeIcon);
        setRollIconForBtn(btnShowDescription, showDesc, showDescIcon);
        setRollIconForBtn(btnShowResistanceLines, showResistanceLines, showResistanceIcon);
        setRollIconForBtn(btnShowCurrentPriceLine, showCurrentPriceLine, showCurrentPriceIcon);
        setRollIconForBtn(btnShowPreviousCloseLine, showPreviousCloseLine, showPreviousCloseIcon);
        setRollIconForBtn(btnShowOrderLines, showOrderLines, showOrderLinesIcon);
        setRollIconForBtn(btnShowBidAskTags, showBidAskTags, showBidAskTagsIcon);
        setRollIconForBtn(btnShowMinMaxLines, showMinMaxLines, showMinMaxLinesIcon);
        setRollIconForBtn(btnSnapToPrice, snapToPrice, snapToPriceIcon);
        setRollIconForBtn(btnSnapBall, snapBall, snapBallIcon);
        setRollIconForBtn(btnShowCrossHairs, showCrossHair, showCrossHairsIcon);
        setRollIconForBtn(btnFibCalculator, fibCalulator, fibCalcIcon);
        setRollIconForBtn(btnRectangualrSelection, rectangularSelection, recSelectionIcon);
        setRollIconForBtn(btnErasor, erasorEnabled, erasorIcon);
        setRollIconForBtn(btnNavigationBar, chartNavigationBarVisible, navigationBarIcon);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void setTooltipForBtn(AbstractButton btn, boolean pressed, String ttTrue,
                                 String ttFalse) {
        if (pressed) {
            btn.setToolTipText(Language.getString(ttTrue));
        } else {
            btn.setToolTipText(Language.getString(ttFalse));
        }
    }

    public void setIconForBtn(AbstractButton btn, boolean pressed, Icon[] ia) {
        if (pressed) {
            btn.setIcon(ia[1]);
        } else {
            btn.setIcon(ia[0]);
        }
    }

    public void setRollIconForBtn(AbstractButton btn, boolean pressed, Icon[] ia) {
        if (pressed) {
            btn.setRolloverIcon(ia[3]);
        } else {
            btn.setRolloverIcon(ia[2]);
        }
    }

    public void setIconForZoomBtn(AbstractButton btn, boolean pressed, boolean boxPressed, Icon[] ia) {
        if (isDragZoomSelected) {
            if (pressed) {
                btn.setIcon(ia[1]);
            } else {
                btn.setIcon(ia[0]);
            }
        } else {
            if (boxPressed) {
                btn.setIcon(ia[5]);
            } else {
                btn.setIcon(ia[4]);
            }
        }
    }

    public void setRollIconForZoomBtn(AbstractButton btn, boolean pressed, boolean boxPressed, Icon[] ia) {
        if (isDragZoomSelected) {
            if (pressed) {
                btn.setRolloverIcon(ia[3]);
            } else {
                btn.setRolloverIcon(ia[2]);
            }
        } else {
            if (boxPressed) {
                btn.setRolloverIcon(ia[7]);
            } else {
                btn.setRolloverIcon(ia[6]);
            }
        }
    }

    private void setTextOnLbl(JLabel lbl, JPanel pnl, String txt) {
        lbl.setText(txt);
        int lblW = pnl.getFontMetrics(lbl.getFont()).stringWidth(txt) + 5;
        lbl.setPreferredSize(new Dimension(lblW, 24));
        pnl.setPreferredSize(new Dimension(lblW + 20, 26));
    }

    private void setStyleImage(int style) {
        lblStyle.setIcon(styleIcon[style]);
        lblStyle.setRolloverIcon(styleActiveIcon[style]);
    }

    private void refreshGridButton(byte gridType) {
        btnGrid.setIcon(gridIcon[gridType]);
        btnGrid.setRolloverIcon(gridActiveIcon[gridType]);
    }

    private void refreshSplitAdjustButton(byte method) {
        btnSplitAdjusted.setIcon(splitAdjustedIcon[method]);
        btnSplitAdjusted.setRolloverIcon(splitAdjustedActiveIcon[method]);
        if (method == GraphDataManager.METHOD_UNADJUSTED) {
            btnSplitAdjusted.setToolTipText(Language.getString("GRAPH_SPLIT_ADJUSTED"));
        } else if (method == GraphDataManager.METHOD_NONE) {
            btnSplitAdjusted.setToolTipText(Language.getString("GRAPH_SPLIT_UNADJUSTED"));
        }
    }

    public void applyTheme() {
        if (popIndicators != null)
            SwingUtilities.updateComponentTreeUI(popIndicators);
        if (popPatterns != null)
            SwingUtilities.updateComponentTreeUI(popPatterns);
        if (popStrategies != null)
            SwingUtilities.updateComponentTreeUI(popStrategies);
        if (popStyles != null)
            SwingUtilities.updateComponentTreeUI(popStyles);
        if (popShowHideISP != null)
            SwingUtilities.updateComponentTreeUI(popShowHideISP);
        applyBGColor(this, Theme.getColor("GRPH_TOP_TOOLBAR_BGCOLOR"));
    }

    public void applyBGColor(Component c, Color color) {
        try {

            if (c instanceof JComponent) {
                ((JComponent) c).setOpaque(true);
            }
            if (c instanceof JComboBox) {
                // do nothing
            } else if (c instanceof java.awt.Container) {
                c.setBackground(color);
                java.awt.Container container = (java.awt.Container) c;
                int ncomponents = container.getComponentCount();
                for (int i = 0; i < ncomponents; ++i) {
                    applyBGColor(container.getComponent(i), color);
                }
            } else {
                c.setBackground(color);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setNullTarget() {
        setButtonsEnabled(false);
    }

    private void setButtonsEnabled(boolean enabled) {
        btnSaveChart.setEnabled(enabled);
        btnPrintChart.setEnabled(enabled);
        btnMode.setEnabled(enabled);
        lblPeriods.setEnabled(enabled);
        btnPeriods.setEnabled(enabled);
        lblIntervals.setEnabled(enabled);
        btnIntervals.setEnabled(enabled);
        lblStyle.setEnabled(enabled);
        btnStyle.setEnabled(enabled);
        btnDragZoom.setEnabled(enabled);
        lblZoom.setEnabled(enabled);
        lblSnapToPrice.setEnabled(enabled);
        lblCrossHair.setEnabled(enabled);
        lblVolByPrice.setEnabled(enabled);
        lblSplitAdjust.setEnabled(enabled);
        btnShowTurnOver.setEnabled(enabled);
        btnShowVolume.setEnabled(enabled);
        btnShowVolumeByPrice.setEnabled(enabled);
        btnShowDataWin.setEnabled(enabled);
        btnIndexed.setEnabled(enabled);
        btnSplitAdjusted.setEnabled(enabled);
        btnShowAnnouncements.setEnabled(enabled);
        btnShowSplits.setEnabled(enabled);
        btnChartOrData.setEnabled(enabled);
        btnShowDescription.setEnabled(enabled);
        btnGrid.setEnabled(enabled);
        btnShowCurrentPriceLine.setEnabled(enabled);
        btnShowPreviousCloseLine.setEnabled(enabled);
        btnShowResistanceLines.setEnabled(enabled);
        btnRectangualrSelection.setEnabled(enabled);
        btnErasor.setEnabled(enabled);
        btnFibCalculator.setEnabled(enabled);
        btnShowOrderLines.setEnabled(enabled && ChartInterface.isTradingConnected(SharedMethods.getExchangeFromKey(parent.getActiveGraph().getBaseKey())));
        btnShowBidAskTags.setEnabled(enabled);
        btnShowMinMaxLines.setEnabled(enabled);
        btnSnapToPrice.setEnabled(enabled);
        btnSnapBall.setEnabled(enabled);
        btnShowCrossHairs.setEnabled(enabled);
        lblShowHideISP.setEnabled(enabled);
        btnShowHideISP.setEnabled(enabled);
    }

    public void setStaticIcons() {
        modeIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_history.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_intraday.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_history_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_intraday_act.gif")
        };
        styleIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_line.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_bar.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_hlc.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_ohlc.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_candle.gif")
        };
        styleActiveIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_line_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_bar_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_hlc_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_ohlc_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_style_candle_act.gif")
        };
        gridIcon = new Icon[]{//h b v n
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_horiz.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_both.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_verti.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_none.gif"),
        };
        gridActiveIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_horiz_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_both_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_verti_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_grid_none_act.gif"),
        };
        dragZoomIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_dragZoom.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_dragZoom_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_dragZoom_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_boxZoom.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_boxZoom.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_boxZoom_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_no_boxZoom_act.gif")
        };
        showVolumeIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_vol.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_vol_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_vol_act.gif")
        };
        indexedIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_indexed.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_indexed.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_indexed_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_indexed_act.gif")
        };
        showDescIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_desc.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_desc.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_desc_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_desc_act.gif")
        };
        splitAdjustedIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_split_adjusted.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_split_adjusted.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_split_adjusted_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_not_split_adjusted_act.gif")
        };
        showSplitsIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_splits.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_splits.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_splits_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_splits_act.gif")
        };
        showAnnouncementsIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_announs.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_announs.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_announs_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_announs_act.gif")
        };

        showCurrentPriceIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_current_price.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_current_price.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_current_price_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_current_price_act.gif")
        };

        showPreviousCloseIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_previous_close_line.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_previous_close_line.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_previous_close_line_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_previous_close_line_act.gif")
        };

        showResistanceIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_supRes_lines.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_supRes_lines.gif "),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_supRes_lines_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_supRes_lines_act.gif")
        };
        showOrderLinesIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_order_lines.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_order_lines.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_order_lines_vol_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_order_lines_act.gif")
        };
        showBidAskTagsIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_bidAsk_tags.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_bidAsk_tags.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_bidAsk_tags_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_bidAsk_tags_act.gif")
        };
        showMinMaxLinesIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_min_max.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_min_max.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_min_max_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_min_max_act.gif")
        };

        snapToPriceIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_price.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_disable_snap_price.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_enable_snap_price_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_disable_snap_price_act.gif")
        };

        showCrossHairsIcon = new Icon[]{
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_cross_hair.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_cross_hair.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_show_cross_hair_act.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_hide_cross_hair_act.gif")
        };
    }


    public JButton getBtnShowOrderLines() {
        return btnShowOrderLines;
    }
}



