package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 23, 2006
 * Time: 3:05:29 PM
 */
public class ADXR extends ChartProperties implements Indicator {

    private static final long serialVersionUID = UID_ADXR;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public ADXR(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_ADXR") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_ADXR");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof ADXR) {
            ADXR adxr = (ADXR) cp;
            this.timePeriods = adxr.timePeriods;
            this.innerSource = adxr.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_ADXR") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // This needs 5 intermediate steps in total to keep (ADX)WildersSmoothing, DX, PlusDI, MinusDI and SumOfTR
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        if ((al.size() < timePeriods) || (timePeriods < 2)) return;

        long entryTime = System.currentTimeMillis();

        //setting step size 5
        ChartRecord cr, crOld;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(5);
        }
        // steps involved
        byte stepSumOfTR = ChartRecord.STEP_1;
        byte stepPlusDI = ChartRecord.STEP_2;
        byte stepMinusDI = ChartRecord.STEP_3;
        byte stepDX = ChartRecord.STEP_4;
        byte stepADX = ChartRecord.STEP_5;
        ADX.getADX(al, 0, timePeriods, stepADX, stepDX, stepPlusDI, stepMinusDI, stepSumOfTR);

        for (int i = 1; i < timePeriods * 3; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        for (int i = (timePeriods * 3); i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - timePeriods);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(Math.round((cr.getStepValue(stepADX) + crOld.getStepValue(stepADX)) / 2f));
        }

        //System.out.println("**** ADXR Calc time " + (entryTime - System.currentTimeMillis()));
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public String getShortName() {
        return Language.getString("IND_ADXR");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
