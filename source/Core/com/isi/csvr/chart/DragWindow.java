package com.isi.csvr.chart;

import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.NonResizeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Aug 21, 2008
 * Time: 3:06:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class DragWindow extends JWindow implements NonResizeable {

    private static DragWindow window = null;
    private final int HGAP = 5;
    private final int LABEL_GAP = 10;
    private int WIDTH1 = 120;  //jpanel width for horizonal line-both intraday and history common
    private int HEIGHT = 55;
    private JLabel lblSymbolName;
    private JLabel lblDatePriceValue;
    private JLabel lblDatePriceStr;
    private JLabel lblLineType;
    private JLabel lblSymbol;

    private JPanel pnlBottom;

    private String strEmpty = "-";

    private SimpleDateFormat monthFormat = new SimpleDateFormat("MMM yyyy");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd-HH:mm");
    private DecimalFormat priceFormat = new DecimalFormat("###,##0.00");

    //common layout for both intraday and hitory-->horizontal lines
    private FlexGridLayout commonLayout = new FlexGridLayout(new String[]{"45%", "55%"}, new String[]{"50%", "50%"}, 1, 1);

    private GraphFrame graphFrame;

    public DragWindow(GraphFrame frame) {
        super();
        createUI();
        this.graphFrame = frame;
    }

    public DragWindow() {
        super();
        createUI();
    }

    public static DragWindow getSharedInstance() {
        if (window == null) {
            window = new DragWindow();
        }
        return window;
    }

    public void createUI() {

        JPanel panelAll = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"17", "100%"}, 1, 2));
        panelAll.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
        this.setPreferredSize(new Dimension(WIDTH1, HEIGHT));

        JPanel pnlUp = new JPanel(new BorderLayout(0, 3));
        Color temp = new Color(153, 153, 153);
        pnlUp.setBackground(temp);

        Font font1 = new Font("Arial", Font.PLAIN, 11);
        Font font2 = new Font("Arial", Font.BOLD, 11);

        lblLineType = new JLabel(Language.getString("HIRIZONTAL_LINE"), JLabel.CENTER);
        lblLineType.setFont(font1);

        pnlUp.add(lblLineType, BorderLayout.CENTER);

        pnlBottom = new JPanel(commonLayout);
        pnlBottom.setBorder(BorderFactory.createEmptyBorder(1, 2, 1, 1));

        lblSymbol = new JLabel(Language.getString("STRATEGY_SYMBOL"), JLabel.LEADING);
        lblSymbol.setFont(font1);

        lblSymbolName = new JLabel(strEmpty, JLabel.LEADING);
        lblSymbolName.setFont(font2);

        lblDatePriceStr = new JLabel(Language.getString("PRICE"), JLabel.LEADING);
        lblDatePriceStr.setFont(font1);

        lblDatePriceValue = new JLabel(strEmpty, JLabel.LEADING);
        lblDatePriceValue.setFont(font2);

        pnlBottom.add(lblSymbol);
        pnlBottom.add(lblSymbolName);
        pnlBottom.add(lblDatePriceStr);
        pnlBottom.add(lblDatePriceValue);

        panelAll.add(pnlUp);
        panelAll.add(pnlBottom);
        this.add(panelAll);
        this.pack();
        GUISettings.applyOrientation(this);
    }

    public void setDataValues(String name, boolean currentMode, int type, String price, long interval, long date, Point2D.Float e) {

        lblSymbolName.setText(name);
        price = priceFormat.format(Double.parseDouble(price));

        FontMetrics metrics = lblSymbolName.getFontMetrics(lblSymbolName.getFont());

        int width1 = metrics.stringWidth(lblSymbolName.getText()) + LABEL_GAP;
        int width2 = 10;
        int width3 = metrics.stringWidth(Language.getString("STRATEGY_SYMBOL")) + LABEL_GAP;
        int width4 = 10;

        switch (type) {

            case AbstractObject.INT_LINE_HORIZ:

                lblLineType.setText(Language.getString("HIRIZONTAL_LINE"));
                lblDatePriceStr.setText(Language.getString("MVIEW_MOST_ACTIVE_BY_VALUE"));
                lblDatePriceValue.setText(price);

                width2 = metrics.stringWidth(price) + LABEL_GAP;
                width4 = metrics.stringWidth(Language.getString("MVIEW_MOST_ACTIVE_BY_VALUE")) + LABEL_GAP;

                int width11 = Math.max(width1, width2); //343,343.40 vs 1010
                int width22 = Math.max(width3, width4); //"Symbol" vs "Value"

                int maxWidth = Math.max(width11 + width22, metrics.stringWidth(lblLineType.getText()) + 4 * LABEL_GAP);
                int W1 = ((width1) / (width1 + width2)) * 100;
                int W2 = 100 - W1;
                commonLayout = new FlexGridLayout(new String[]{String.valueOf(W1), String.valueOf(W2)}, new String[]{"50%", "50%"}, HGAP, 1);
                this.setSize(maxWidth, HEIGHT);
                pnlBottom.setLayout(commonLayout);

                break;

            case AbstractObject.INT_LINE_VERTI:

                lblLineType.setText(Language.getString("VERTICAL_LINE"));
                boolean isTimeValid = date != Long.MIN_VALUE;

                if (currentMode) {  //intraday mode
                    lblDatePriceStr.setText(Language.getString("TIME"));
                    lblDatePriceValue.setText(isTimeValid ? timeFormat.format(new Date(date)) : strEmpty);
                } else { //history mode
                    lblDatePriceStr.setText(Language.getString("DATE"));
                    SimpleDateFormat sdtf = (interval == StockGraph.MONTHLY) ? monthFormat : dateFormat;
                    lblDatePriceValue.setText(isTimeValid ? sdtf.format(new Date(date)) : strEmpty);
                }

                width2 = metrics.stringWidth(lblDatePriceValue.getText()) + LABEL_GAP;
                width4 = metrics.stringWidth(lblDatePriceStr.getText()) + LABEL_GAP;

                width11 = Math.max(width1, width2); // 1010 vs 1992.03.23
                width22 = Math.max(width3, width4); // "Symbol" vs "Time/Date"

                maxWidth = Math.max(width11 + width22, metrics.stringWidth(lblLineType.getText()) + 4 * LABEL_GAP);
                W1 = ((width1) / (width1 + width2)) * 100;
                W2 = 100 - W1;

                commonLayout = new FlexGridLayout(new String[]{String.valueOf(W1), String.valueOf(W2)}, new String[]{"50%", "50%"}, HGAP, 1);
                this.setSize(maxWidth, HEIGHT);
                pnlBottom.setLayout(commonLayout);
                break;
        }
        if (isInsideGraph(e)) {
            int x = (int) (graphFrame.getLocationOnScreen().getX() + e.getX() + 33);
            int y = (int) (graphFrame.getLocationOnScreen().getY() + e.getY() + 30);
            this.setLocation(x, y);
            this.setVisible(true);
        } else {
            this.setVisible(false);
        }

    }

    public boolean isInsideGraph(Point2D.Float e) {

        int x = graphFrame.getWidth() - 20;
        int y = graphFrame.getHeight() - 60;

        if ((e.getX() > 0 && e.getX() < x) && (e.getY() > 0 && e.getY() < y)) {
            return true;
        }
        return false;

    }
}
