package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 7:15:13 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class ChaikinMoneyFlow extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_CHAIKINS_MONEY_FLOW;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public ChaikinMoneyFlow(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_CHAIKINS_MONEY_FLOW") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_CHAIKINS_MONEY_FLOW");
        this.timePeriods = 21;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_i_mePeriods,
                                          byte destIndex) {

        double accmVal = 0f, currVal, prevVal, volume;
        ChartRecord cr, crOld;

        int loopBegin = t_i_mePeriods + bIndex;

        if (loopBegin > al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }
        //tmp var for sys.out.print
        long entryTime = System.currentTimeMillis();

        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        for (int i = bIndex; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High == cr.Low) ? 0 : (2 * cr.Close - cr.High - cr.Low) * cr.Volume / (cr.High - cr.Low);
            accmVal = accmVal + currVal;
            if (i == loopBegin - 1) {
//                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, destIndex, getID());
//                if (aPoint != null)
//                    aPoint.setIndicatorValue(accmVal);
                cr.setValue(destIndex, accmVal);
            } else {
//                GDM.removeIndicatorPoint(cr.Time, destIndex, getID());
                cr.setValue(destIndex, Indicator.NULL);
            }
        }

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High == cr.Low) ? 0 : (2 * cr.Close - cr.High - cr.Low) * cr.Volume / (cr.High - cr.Low);

            crOld = (ChartRecord) al.get(i - t_i_mePeriods);
            prevVal = (crOld.High == crOld.Low) ? 0 : (2 * crOld.Close - crOld.High - crOld.Low) * crOld.Volume / (crOld.High - crOld.Low);

            accmVal = accmVal + currVal - prevVal;

            cr.setValue(destIndex, accmVal);
        }

        //System.out.println("**** Chaikin Money Flow Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 21;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof ChaikinMoneyFlow) {
            ChaikinMoneyFlow arn = (ChaikinMoneyFlow) cp;
            this.timePeriods = arn.timePeriods;
            this.innerSource = arn.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_CHAIKINS_MONEY_FLOW") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }
    //############################################

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double accmVal = 0f, currVal, prevVal, volume;
        ChartRecord cr, crOld;

        int loopBegin = timePeriods;
        if (loopBegin > al.size()) return;
        //tmp var for sys.out.print
        long entryTime = System.currentTimeMillis();

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High == cr.Low) ? 0 : (2 * cr.Close - cr.High - cr.Low) * cr.Volume / (cr.High - cr.Low);
            accmVal = accmVal + currVal;
            if (i == loopBegin - 1) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null)
                    aPoint.setIndicatorValue(accmVal);
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High == cr.Low) ? 0 : (2 * cr.Close - cr.High - cr.Low) * cr.Volume / (cr.High - cr.Low);

            crOld = (ChartRecord) al.get(i - timePeriods);
            prevVal = (crOld.High == crOld.Low) ? 0 : (2 * crOld.Close - crOld.High - crOld.Low) * crOld.Volume / (crOld.High - crOld.Low);

            accmVal = accmVal + currVal - prevVal;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(accmVal);
        }
        //System.out.println("**** Chaikin Money Flow Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_CHAIKINS_MONEY_FLOW");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

}
