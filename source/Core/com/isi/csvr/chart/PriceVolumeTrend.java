package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorDefaultProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 8:31:11 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class PriceVolumeTrend extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_PRICE_VOLUME_TREND;
    //Fields
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public PriceVolumeTrend(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_PRICE_VOLUME_TREND") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_PRICE_VOLUME_TREND");
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorDefaultProperty idp = (IndicatorDefaultProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setOHLCPriority(idp.getOHLCPriority());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, byte srcIndex, byte destIndex) {

        //tmp var for sop
        if (al.size() < 2) return;
        long entryTime = System.currentTimeMillis();
        double preClose, pvt;
        ChartRecord cr;

        cr = (ChartRecord) al.get(0);
        preClose = cr.getValue(srcIndex);
        pvt = 0;

        for (int i = 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if (preClose > 0) {
                pvt = (cr.Close - preClose) * cr.Volume / preClose + pvt;
            }
           /* ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(pvt);
            }*/
            cr.setValue(destIndex, pvt);
            preClose = cr.Close;
        }
    }

    public static int getAuxStepCount() {
        return 0;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof PriceVolumeTrend) {
            PriceVolumeTrend co = (PriceVolumeTrend) cp;
            this.innerSource = co.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        return Language.getString("IND_PRICE_VOLUME_TREND") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }
    //############################################

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        //tmp var for sop
        if (al.size() < 2) return;
        long entryTime = System.currentTimeMillis();
        double preClose, pvt;
        ChartRecord cr;

        cr = (ChartRecord) al.get(0);
        preClose = cr.getValue(getOHLCPriority());
        pvt = 0;

        for (int i = 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if (preClose > 0) {
                pvt = (cr.Close - preClose) * cr.Volume / preClose + pvt;
            }
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(pvt);
            }
            preClose = cr.Close;
        }
        //System.out.println("**** Price Volume Trend Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_PRICE_VOLUME_TREND");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
