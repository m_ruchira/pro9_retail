package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 2:33:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorStochasticMomentumIndexProperty extends IndicatorDefaultProperty {

    private final String TIME_PERIODS = "timePeriods";
    private final String SMOOTHING_PERIOD = "smoothingPeriod";
    private final String DOUBLE_SMOOTHING_PERIOD = "doubleSmoothingPeriod";
    private int timePeriods;    //:TODO Enter the default values
    private int smoothingPeriod;
    private int doubleSmoothingPeriod;

    public IndicatorStochasticMomentumIndexProperty() {
        super();
    }

    public IndicatorStochasticMomentumIndexProperty(int objectId) {
        super(objectId);
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public IndicatorDataItem setTimePeriods(int timePeriods) {
        this.timePeriods = timePeriods;
        return new IndicatorDataItem(TIME_PERIODS, timePeriods);
    }

    public int getSmoothingPeriod() {
        return smoothingPeriod;
    }

    public IndicatorDataItem setSmoothingPeriod(int sp) {
        this.smoothingPeriod = sp;
        return new IndicatorDataItem(SMOOTHING_PERIOD, sp);
    }

    public int getDoubleSmoothingPeriod() {
        return doubleSmoothingPeriod;
    }

    public IndicatorDataItem setDoubleSmoothingPeriod(int dsp) {
        this.doubleSmoothingPeriod = dsp;
        return new IndicatorDataItem(DOUBLE_SMOOTHING_PERIOD, dsp);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(TIME_PERIODS)) {
            this.timePeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(SMOOTHING_PERIOD)) {
            this.smoothingPeriod = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(DOUBLE_SMOOTHING_PERIOD)) {
            this.doubleSmoothingPeriod = Integer.parseInt(propertyValue.toString());
        }
    }
}
