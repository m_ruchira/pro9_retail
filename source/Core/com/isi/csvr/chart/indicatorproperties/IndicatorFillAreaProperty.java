package com.isi.csvr.chart.indicatorproperties;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Jun 8, 2011
 * Time: 5:46:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorFillAreaProperty extends IndicatorDefaultProperty {

    private final String FILL_BANDS = "fillBands";
    private final String FILL_COLOR = "fillColor";
    private final String TRANSPARENCY = "transparency";
    private boolean fillBands;
    private Color fillColor;
    private int transparency;

    public IndicatorFillAreaProperty() {

    }

    public IndicatorFillAreaProperty(int objectId) {
        super(objectId);
    }

    public boolean isFillBands() {
        return fillBands;
    }

    public IndicatorDataItem setFillBands(boolean fillBands) {
        this.fillBands = fillBands;
        return new IndicatorDataItem(FILL_BANDS, fillBands);
    }

    public Color getFillColor() {
        return fillColor;
    }

    public IndicatorDataItem setFillColor(Color fillColor) {
        this.fillColor = fillColor;
        return new IndicatorDataItem(FILL_COLOR, fillColor.getRGB());
    }

    public int getTransparency() {
        return transparency;
    }

    public IndicatorDataItem setTransparency(int transparency) {
        this.transparency = transparency;
        return new IndicatorDataItem(TRANSPARENCY, transparency);
    }

    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {
        if (propertyName.equals(FILL_BANDS)) {
            this.fillBands = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(FILL_COLOR)) {
            this.fillColor = new Color(Integer.parseInt(propertyValue.toString()));
        } else if (propertyName.equals(TRANSPARENCY)) {
            this.transparency = Integer.parseInt(propertyValue.toString());
        }
    }

}
