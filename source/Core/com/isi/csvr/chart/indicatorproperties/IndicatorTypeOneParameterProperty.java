package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 3, 2009
 * Time: 3:33:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorTypeOneParameterProperty extends IndicatorDefaultProperty {

    private final String TIME_PERIODS = "timePeriods";
    private int timePeriods;

    public IndicatorTypeOneParameterProperty() {
        super();
    }

    public IndicatorTypeOneParameterProperty(int objectId) {
        super(objectId);
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public IndicatorDataItem setTimePeriods(int timePeriods) {
        this.timePeriods = timePeriods;
        return new IndicatorDataItem(TIME_PERIODS, timePeriods);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(TIME_PERIODS)) {
            this.timePeriods = Integer.parseInt(propertyValue.toString());
        }
    }
}
