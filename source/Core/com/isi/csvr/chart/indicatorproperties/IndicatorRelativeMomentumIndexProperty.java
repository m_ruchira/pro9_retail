package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 1:04:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorRelativeMomentumIndexProperty extends IndicatorDefaultProperty {

    private final String TIME_PERIODS = "timePeriods";
    private final String MOMENTUM_PARAM = "momentumParam";
    private int timePeriods;    //:TODO Enter the default values
    private int momentumParam;


    public IndicatorRelativeMomentumIndexProperty() {
        super();
    }

    public IndicatorRelativeMomentumIndexProperty(int objectId) {
        super(objectId);
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public IndicatorDataItem setTimePeriods(int tp) {
        this.timePeriods = tp;
        return new IndicatorDataItem(TIME_PERIODS, tp);
    }

    public int getMomentumParam() {
        return momentumParam;
    }

    public IndicatorDataItem setMomentumParam(int mp) {
        this.momentumParam = mp;
        return new IndicatorDataItem(MOMENTUM_PARAM, mp);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(TIME_PERIODS)) {
            this.timePeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(MOMENTUM_PARAM)) {
            this.momentumParam = Integer.parseInt(propertyValue.toString());
        }
    }
}
