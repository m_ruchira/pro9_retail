package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 1:15:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorBollingerBandsProperty extends IndicatorFillAreaProperty {

    private final String TIME_PERIODS = "timePeriods";
    private final String METHOD = "method";
    private final String DEVIATIONS = "deviations";
    private int timePeriods;    //:TODO Enter the default values
    private byte method;
    private float deviations;

    public IndicatorBollingerBandsProperty() {
        super();
    }

    public IndicatorBollingerBandsProperty(int objectId) {
        super(objectId);
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public IndicatorDataItem setTimePeriods(int timePeriods) {
        this.timePeriods = timePeriods;
        return new IndicatorDataItem(TIME_PERIODS, timePeriods);
    }

    public byte getMethod() {
        return method;
    }

    public IndicatorDataItem setMethod(byte method) {
        this.method = method;
        return new IndicatorDataItem(METHOD, method);
    }

    public float getDeviations() {
        return deviations;
    }

    public IndicatorDataItem setDeviations(float deviations) {
        this.deviations = deviations;
        return new IndicatorDataItem(DEVIATIONS, deviations);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {
        super.setValues(propertyName, propertyValue);
        if (propertyName.equals(TIME_PERIODS)) {
            this.timePeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(METHOD)) {
            this.method = Byte.parseByte(propertyValue.toString());
        } else if (propertyName.equals(DEVIATIONS)) {
            this.deviations = Float.parseFloat(propertyValue.toString());
        }
    }
}
