package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 3:59:02 PM
 * To change this template use File | Settings | File Templates.
 */

public class IndicatorIchimokuKinkoHyoProperty extends IndicatorDefaultProperty {


    private final String TENKANSEN_PERIODS = "tenkanSenPeriods";
    private final String KIJUNSEN_PERIODS = "kijunSenPeriods";
    private final String SENKOU_SPAN_PERIODS = "senkouSpanPeriods";
    private final String CHIKOU_SPAN_PERIODS = "chikouSpanPeriods";
    private int tenkanSenPeriods; //:TODO Enter the default values
    private int kijunSenPeriods;
    private int senkouSpanPeriods;
    private int chikouSpanPeriods;


    public IndicatorIchimokuKinkoHyoProperty() {
        super();
    }

    public IndicatorIchimokuKinkoHyoProperty(int objectId) {
        super(objectId);
    }

    public int getTenkanSenPeriods() {
        return tenkanSenPeriods;
    }

    public IndicatorDataItem setTenkanSenPeriods(int timePeriods) {
        this.tenkanSenPeriods = timePeriods;
        return new IndicatorDataItem(TENKANSEN_PERIODS, timePeriods);
    }

    public int getKijunSenPeriods() {
        return kijunSenPeriods;
    }

    public IndicatorDataItem setKijunSenPeriods(int sp) {
        this.kijunSenPeriods = sp;
        return new IndicatorDataItem(KIJUNSEN_PERIODS, sp);
    }

    public int getSenkouSpanPeriods() {
        return senkouSpanPeriods;
    }

    public IndicatorDataItem setSenkouSpanPeriods(int dsp) {
        this.senkouSpanPeriods = dsp;
        return new IndicatorDataItem(SENKOU_SPAN_PERIODS, dsp);
    }

    public int getChikouSpanPeriods() {
        return chikouSpanPeriods;
    }

    public IndicatorDataItem setChikouSpanPeriods(int sp) {
        this.chikouSpanPeriods = sp;
        return new IndicatorDataItem(CHIKOU_SPAN_PERIODS, sp);
    }

    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(TENKANSEN_PERIODS)) {
            this.tenkanSenPeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(KIJUNSEN_PERIODS)) {
            this.kijunSenPeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(SENKOU_SPAN_PERIODS)) {
            this.senkouSpanPeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(CHIKOU_SPAN_PERIODS)) {
            this.chikouSpanPeriods = Integer.parseInt(propertyValue.toString());
        }
    }
}
