package com.isi.csvr.chart.indicatorproperties;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 3:41:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorStochasticOscillatorProperty extends IndicatorDefaultProperty {


    private final String TIME_PERIODS = "timePeriods";
    private final String SLOWING_PERIODS = "slowingPeriods";
    private final String METHOD = "method";
    private final String SIGNAL_TIME_PERIOD = "signalTimePeriod";
    private final String SIGNAL_STYLE = "signalStyle";
    private final String LINE_COLOR = "lineColor";
    protected int slowingPeriods;
    protected byte method;
    private int timePeriods;    //:TODO Enter the default values
    private int signalTimePeriods;
    private byte signalStyle;
    private Color lineColor;

    public IndicatorStochasticOscillatorProperty() {
        super();
    }

    public IndicatorStochasticOscillatorProperty(int objectId) {
        super(objectId);
    }

    public Color getLineColor() {
        return lineColor;
    }

    public IndicatorDataItem setLineColor(Color lineColor) {
        this.lineColor = lineColor;
        return new IndicatorDataItem(LINE_COLOR, lineColor.getRGB());
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public IndicatorDataItem setTimePeriods(int timePeriods) {
        this.timePeriods = timePeriods;
        return new IndicatorDataItem(TIME_PERIODS, timePeriods);
    }

    public int getSlowingPeriod() {
        return slowingPeriods;
    }

    public IndicatorDataItem setSlowingPeriod(int sp) {
        this.slowingPeriods = sp;
        return new IndicatorDataItem(SLOWING_PERIODS, sp);
    }

    public byte getMethod() {
        return method;
    }

    public IndicatorDataItem setMethod(byte dsp) {
        this.method = dsp;
        return new IndicatorDataItem(METHOD, dsp);
    }

    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public IndicatorDataItem setSignalTimePeriods(int sp) {
        this.signalTimePeriods = sp;
        return new IndicatorDataItem(SIGNAL_TIME_PERIOD, sp);
    }

    public byte getSignalStyle() {
        return signalStyle;
    }

    public IndicatorDataItem setSignalStyle(byte sp) {
        this.signalStyle = sp;
        return new IndicatorDataItem(SIGNAL_STYLE, sp);
    }

    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(TIME_PERIODS)) {
            this.timePeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(SLOWING_PERIODS)) {
            this.slowingPeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(METHOD)) {
            this.method = Byte.parseByte(propertyValue.toString());
        } else if (propertyName.equals(SIGNAL_TIME_PERIOD)) {
            this.signalTimePeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(SIGNAL_STYLE)) {
            this.signalStyle = Byte.parseByte(propertyValue.toString());
        } else if (propertyName.equals(LINE_COLOR)) {
            this.lineColor = new Color(Integer.parseInt(propertyValue.toString()));
        }
    }
}
