package com.isi.csvr.chart.indicatorproperties;

import com.isi.csvr.chart.*;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 3, 2009
 * Time: 10:40:08 AM
 * To change this template use File | Settings | File Templates.
 */

public class IndicatorPropertyFactory {

    //save methods

    public static void saveDefaultIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorDefaultProperty op = new IndicatorDefaultProperty(cp.getID());

        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveSingleParamIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        //IndicatorDefaultProperty op = new IndicatorDefaultProperty(cp.getID());
        IndicatorTypeOneParameterProperty op = new IndicatorTypeOneParameterProperty(cp.getID());

        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));
        //items.add(null);

        if (cp instanceof Aroon) {

            Aroon os = (Aroon) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof AverageTrueRange) {

            AverageTrueRange os = (AverageTrueRange) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof ChaikinMoneyFlow) {

            ChaikinMoneyFlow os = (ChaikinMoneyFlow) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof ChandeMomentumOscillator) {

            ChandeMomentumOscillator os = (ChandeMomentumOscillator) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof CommodityChannelIndex) {
            CommodityChannelIndex os = (CommodityChannelIndex) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof DEMA) {

            DEMA os = (DEMA) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof DetrendPriceOscillator) {

            DetrendPriceOscillator os = (DetrendPriceOscillator) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof DX) {

            DX os = (DX) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof PlusDI) {
            PlusDI os = (PlusDI) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof MinusDI) {

            MinusDI os = (MinusDI) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof ADX) {

            ADX os = (ADX) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof ADXR) {

            ADXR os = (ADXR) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof EaseOfMovement) {

            EaseOfMovement os = (EaseOfMovement) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof ForcastOscillator) {

            ForcastOscillator os = (ForcastOscillator) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof IntradayMomentumIndex) {

            IntradayMomentumIndex os = (IntradayMomentumIndex) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof LinearRegression) {

            LinearRegression os = (LinearRegression) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof MassIndex) {

            MassIndex os = (MassIndex) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof Momentum) {

            Momentum os = (Momentum) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof MoneyFlowIndex) {

            MoneyFlowIndex os = (MoneyFlowIndex) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof PriceChannel) {

            PriceChannel os = (PriceChannel) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof PriceROC) {

            PriceROC os = (PriceROC) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof Qstick) {

            Qstick os = (Qstick) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof RSI) {

            RSI os = (RSI) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof RelativeVolatilityIndex) {

            RelativeVolatilityIndex os = (RelativeVolatilityIndex) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof StandardError) {

            StandardError os = (StandardError) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof SwingIndex) {

            SwingIndex os = (SwingIndex) cp;

            items.add(op.setTimePeriods(os.getLimitMove()));

        } else if (cp instanceof TEMA) {

            TEMA os = (TEMA) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof TimeSeriesForcast) {

            TimeSeriesForcast os = (TimeSeriesForcast) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof TRIX) {

            TRIX os = (TRIX) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof VerticalHorizontalFilter) {

            VerticalHorizontalFilter os = (VerticalHorizontalFilter) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof VolumeROC) {

            VolumeROC os = (VolumeROC) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof WildersSmoothing) {

            WildersSmoothing os = (WildersSmoothing) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof WilliamsR) {

            WilliamsR os = (WilliamsR) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));

        } else if (cp instanceof SwingIndex) {

            SwingIndex os = (SwingIndex) cp;

            items.add(op.setTimePeriods(os.getLimitMove()));
        } else if (cp instanceof CoppockCurve) {

            CoppockCurve os = (CoppockCurve) cp;

            items.add(op.setTimePeriods(os.getPeriod()));
        } else if (cp instanceof DisparityIndex) {

            DisparityIndex os = (DisparityIndex) cp;

            items.add(op.setTimePeriods(os.getTimePeriods()));
        }

        //IndicatorPropertyXMLService.saveObjectProperties(cp.getID(), items);
        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveTypeTwoParameterIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorTypeTwoParameterProperty op = new IndicatorTypeTwoParameterProperty(cp.getID());

        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        if (cp instanceof Volatility) {

            Volatility os = (Volatility) cp;

            items.add(op.setParam1(os.getTimePeriods()));
            items.add(op.setParam2((byte) os.getRateOfChange()));

        } else if (cp instanceof Envelope) {

            Envelope os = (Envelope) cp;

            items.add(op.setParam1(os.getTimePeriods()));
            items.add(op.setParam2(os.getMethod()));

        } else if (cp instanceof MovingAverage) {

            MovingAverage os = (MovingAverage) cp;

            items.add(op.setParam1(os.getTimePeriods()));
            items.add(op.setParam2(os.getMethod()));

        }

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveZigZagIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorZigZagProperty op = new IndicatorZigZagProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        ZigZag zz = (ZigZag) cp;
        items.add(op.setReversalAmount(zz.getReversalAmount()));
        items.add(op.setMethod(zz.getMethod()));
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveRelMomentumIndexProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorRelativeMomentumIndexProperty op = new IndicatorRelativeMomentumIndexProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        RelativeMomentumIndex rmi = (RelativeMomentumIndex) cp;
        items.add(op.setTimePeriods(rmi.getTimePeriods()));
        items.add(op.setMomentumParam(rmi.getMomentumParam()));
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveParabolicSARProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorParabolicSARProperties op = new IndicatorParabolicSARProperties(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        ParabolicSAR ipsp = (ParabolicSAR) cp;
        items.add(op.setReversalAmount(ipsp.getStep()));
        items.add(op.setMethod(ipsp.getMax()));
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void savePriceChannelsIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();
        IndicatorPriceChannelsProperty op = new IndicatorPriceChannelsProperty(cp.getID());

        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        PriceChannel ipsp = (PriceChannel) cp;
        items.add(op.setTimePeriods(ipsp.getTimePeriods()));
        items.add(op.setFillBands(ipsp.isFillBands()));
        items.add(op.setFillColor(ipsp.getFillColor()));
        items.add(op.setTransparency(ipsp.getTransparency()));

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveEnvelopesIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();
        IndicatorEnvelopesProperty op = new IndicatorEnvelopesProperty(cp.getID());

        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        Envelope ipsp = (Envelope) cp;
        items.add(op.setParam1(ipsp.getTimePeriods()));
        items.add(op.setParam2(ipsp.getMethod()));

        items.add(op.setFillBands(ipsp.isFillBands()));
        items.add(op.setFillColor(ipsp.getFillColor()));
        items.add(op.setTransparency(ipsp.getTransparency()));

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveBollingerBandsIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorBollingerBandsProperty op = new IndicatorBollingerBandsProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        BollingerBand ipsp = (BollingerBand) cp;
        items.add(op.setTimePeriods(ipsp.getTimePeriods()));
        items.add(op.setMethod(ipsp.getMethod()));
        items.add(op.setDeviations(ipsp.getDeviations()));
        items.add(op.setFillBands(ipsp.isFillBands()));
        items.add(op.setFillColor(ipsp.getFillColor()));
        items.add(op.setTransparency(ipsp.getTransparency()));
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveKeltnerChannelsIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();
        IndicatorKeltnerChannelsProperties op = new IndicatorKeltnerChannelsProperties(cp.getID());

        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        KeltnerChannels ipsp = (KeltnerChannels) cp;
        items.add(op.setTimePeriods(ipsp.getTimePeriods()));
        items.add(op.setMethod(ipsp.getDeviations()));
        items.add(op.setFillBands(ipsp.isFillBands()));
        items.add(op.setFillColor(ipsp.getFillColor()));
        items.add(op.setTransparency(ipsp.getTransparency()));

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveStandardDeviationIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorStandardDeviationProperty op = new IndicatorStandardDeviationProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        StandardDeviation sd = (StandardDeviation) cp;
        items.add(op.setTimePeriods(sd.getTimePeriods()));
        items.add(op.setMethod(sd.getDeviations()));

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveStochasticMomentumInProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorStochasticMomentumIndexProperty op = new IndicatorStochasticMomentumIndexProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        StochasticMomentumIndex smi = (StochasticMomentumIndex) cp;
        items.add(op.setTimePeriods(smi.getTimePeriods()));
        items.add(op.setSmoothingPeriod(smi.getSmoothingPeriod()));
        items.add(op.setDoubleSmoothingPeriod(smi.getDoubleSmoothingPeriod()));
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveOscillatorIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorOscillatorProperty op = new IndicatorOscillatorProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        if (cp instanceof VolumeOscillator) {

            VolumeOscillator vo = (VolumeOscillator) cp;
            items.add(op.setShortPeriod(vo.getShortPeriods()));
            items.add(op.setLongPeriod(vo.getLongPeriods()));
            items.add(op.setMethod(vo.getMethod()));
            items.add(op.setgetCalcMethod(vo.getCalcMethod()));

        } else if (cp instanceof PriceOscillator) {

            PriceOscillator vo = (PriceOscillator) cp;

            items.add(op.setShortPeriod(vo.getShortPeriods()));
            items.add(op.setLongPeriod(vo.getLongPeriods()));
            items.add(op.setMethod(vo.getMethod()));
            items.add(op.setgetCalcMethod(vo.getCalcMethod()));
        }
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveTypeThreeParameterIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorTypeThreeParameterProperty op = new IndicatorTypeThreeParameterProperty(cp.getID());

        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        if (cp instanceof MACDVolume) {

            MACDVolume os = (MACDVolume) cp;

            items.add(op.setTimePeriods(os.getSignalTimePeriods()));
            items.add(op.setLineColor(os.getSignalColor()));
            items.add(op.setLineStyle2(os.getSignalStyle()));

        } else if (cp instanceof KlingerOscillator) {

            KlingerOscillator os = (KlingerOscillator) cp;

            items.add(op.setTimePeriods(os.getSignalTimePeriods()));
            items.add(op.setLineColor(os.getSignalColor()));
            items.add(op.setLineStyle2(os.getSignalStyle()));

        }

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveIchimokuKinkoHyoIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorIchimokuKinkoHyoProperty op = new IndicatorIchimokuKinkoHyoProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        IchimokuKinkoHyo ik = (IchimokuKinkoHyo) cp;
        items.add(op.setTenkanSenPeriods(ik.getTenkanSenPeriods()));
        items.add(op.setKijunSenPeriods(ik.getKijunSenPeriods()));
        items.add(op.setSenkouSpanPeriods(ik.getSenkouSpanPeriods()));
        items.add(op.setChikouSpanPeriods(ik.getChikouSpanPeriods()));
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveStochasticOscillatorIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorStochasticOscillatorProperty op = new IndicatorStochasticOscillatorProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        // didnt use the instanceof test.
        StocasticOscillator so = (StocasticOscillator) cp;
        items.add(op.setTimePeriods(so.getTimePeriods()));
        items.add(op.setLineColor(so.getSignalColor()));
        items.add(op.setSlowingPeriod(so.getSlowingPeriods()));
        items.add(op.setSignalTimePeriods(so.getSignalTimePeriods()));
        items.add(op.setSignalStyle(so.getSignalStyle()));
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    public static void saveMACDIndicatorProperties(ChartProperties cp) {

        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();

        IndicatorMACDProperty op = new IndicatorMACDProperty(cp.getID());


        items.add(op.setObjectID());
        items.add(op.setUpColor(cp.getColor()));
        items.add(op.setDownColor(cp.getWarningColor()));
        items.add(op.setLineStyle(cp.getPenStyle()));
        items.add(op.setLineThickness(cp.getPenWidth()));
        items.add(op.setChartStyle(cp.getChartStyle()));
        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
        items.add(op.setUseSameColor(cp.isUsingSameColor()));

        MACD so = (MACD) cp;
        items.add(op.setMACD_TimePeriods_High(so.getMACD_TimePeriods_High()));
        items.add(op.setMACD_TimePeriods_Low(so.getMACD_TimePeriods_Low()));
        items.add(op.setSignalTimePeriods(so.getSignalTimePeriods()));
        items.add(op.setSignalStyle(so.getSignalStyle()));
        items.add(op.setSignalColor(so.getSignalColor()));
        //items.add(null);

        IndicatorPropertyXMLService.saveIndicatorProperties(cp.getID(), items);
    }

    //Load methods

    public static IndicatorDefaultProperty loadDefaultIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorDefaultProperty op = new IndicatorDefaultProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorTypeOneParameterProperty loadSingleParamIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorTypeOneParameterProperty op = new IndicatorTypeOneParameterProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorTypeTwoParameterProperty loadTypeTwoParameterIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorTypeTwoParameterProperty op = new IndicatorTypeTwoParameterProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorZigZagProperty loadZigZagIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorZigZagProperty op = new IndicatorZigZagProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorRelativeMomentumIndexProperty loadRelMomentumIndexProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorRelativeMomentumIndexProperty op = new IndicatorRelativeMomentumIndexProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorParabolicSARProperties loadParabolicSARProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorParabolicSARProperties op = new IndicatorParabolicSARProperties();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorBollingerBandsProperty loadBollingerBandsIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorBollingerBandsProperty op = new IndicatorBollingerBandsProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorPriceChannelsProperty loadPriceChannelsIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorPriceChannelsProperty op = new IndicatorPriceChannelsProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorEnvelopesProperty loadEnvelopesIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorEnvelopesProperty op = new IndicatorEnvelopesProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorKeltnerChannelsProperties loadKeltnerChannelsProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorKeltnerChannelsProperties op = new IndicatorKeltnerChannelsProperties();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorStandardDeviationProperty loadStandardDeviationIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorStandardDeviationProperty op = new IndicatorStandardDeviationProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorStochasticMomentumIndexProperty loadStochasticMomentumInProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorStochasticMomentumIndexProperty op = new IndicatorStochasticMomentumIndexProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorOscillatorProperty loadOscillatorIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorOscillatorProperty op = new IndicatorOscillatorProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorTypeThreeParameterProperty loadTypeThreeParameterIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorTypeThreeParameterProperty op = new IndicatorTypeThreeParameterProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorIchimokuKinkoHyoProperty loadIchimokuKinkoHyoIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorIchimokuKinkoHyoProperty op = new IndicatorIchimokuKinkoHyoProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorStochasticOscillatorProperty loadStochasticOscillatorIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorStochasticOscillatorProperty op = new IndicatorStochasticOscillatorProperty();
        op.assignvaluesFrom(items);
        return op;
    }

    public static IndicatorMACDProperty loadMACDIndicatorProperties(ArrayList<IndicatorDataItem> items) {

        IndicatorMACDProperty op = new IndicatorMACDProperty();
        op.assignvaluesFrom(items);
        return op;
    }

}
