package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 6, 2009
 * Time: 11:09:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorTypeOneProperty extends IndicatorDefaultProperty {

    private final String TIME_PERIODS = "timePeriods";
    private int timePeriods;    //:TODO Enter the default values


    public IndicatorTypeOneProperty() {
        super();
    }

    public IndicatorTypeOneProperty(int objectId) {
        super(objectId);
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public IndicatorDataItem setParam1(int param1) {
        this.timePeriods = param1;
        return new IndicatorDataItem(TIME_PERIODS, param1);
    }

    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(TIME_PERIODS)) {
            this.timePeriods = Integer.parseInt(propertyValue.toString());
        }
    }
}
