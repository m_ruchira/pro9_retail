package com.isi.csvr.chart.indicatorproperties;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.GraphDataManager;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Aug 3, 2009
 * Time: 10:21:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorPropertyStore {

    private static Hashtable<Byte, Object> propertyStore = new Hashtable<Byte, Object>();
    private static IndicatorPropertyStore self;

    public static IndicatorPropertyStore getSharedInstance() {
        if (self == null) {
            self = new IndicatorPropertyStore();
            reloadPropertyStore();
        }
        return self;
    }

    public static void reloadPropertyStore() {
        propertyStore.clear();
        ArrayList<Byte> idList = IndicatorPropertyXMLService.getSavedIndicators();

        for (int i = 0; i < idList.size(); i++) {
            byte key = idList.get(i);
            ArrayList<IndicatorDataItem> items = IndicatorPropertyXMLService.loadProperties(key);

            switch (key) {
                case GraphDataManager.ID_ACCU_DISTRI://no param
                case GraphDataManager.ID_CHAIKINS_OSCILL:
                case GraphDataManager.ID_MEDIAN_PRICE:
                case GraphDataManager.ID_MFI:
                case GraphDataManager.ID_NVI:
                case GraphDataManager.ID_ON_BALANCE_VOLUME:
                case GraphDataManager.ID_PERFORMANCE:
                case GraphDataManager.ID_PVI:
                case GraphDataManager.ID_PRICE_VOLUME_TREND:
                case GraphDataManager.ID_TYPICAL_PRICE:
                case GraphDataManager.ID_WEIGHTED_CLOSE:
                case GraphDataManager.ID_WILLS_ACCU_DISTRI:
                case GraphDataManager.ID_ALLIGATOR:
                    IndicatorDefaultProperty ip = IndicatorPropertyFactory.loadDefaultIndicatorProperties(items);
                    propertyStore.put(key, ip);
                    break;

                case GraphDataManager.ID_AROON://time peroids only
                case GraphDataManager.ID_AVG_TRUE_RANGE:
                case GraphDataManager.ID_CHAIKIN_MONEY_FLOW:
                case GraphDataManager.ID_CMO:
                case GraphDataManager.ID_CCI:
                case GraphDataManager.ID_DEMA:
                    // added by Mevan
                case GraphDataManager.ID_COPP_CURVE:
                case GraphDataManager.ID_DPO:
                case GraphDataManager.ID_DX:
                case GraphDataManager.ID_PLUS_DI:
                case GraphDataManager.ID_MINUS_DI:
                case GraphDataManager.ID_ADX:
                case GraphDataManager.ID_ADXR:
                case GraphDataManager.ID_EASE_OF_MOVEMENT:
                case GraphDataManager.ID_FO:
                case GraphDataManager.ID_INTRADAY_MOMENTUM_INDEX:
                case GraphDataManager.ID_LRI:
                case GraphDataManager.ID_MASS_INDEX:
                case GraphDataManager.ID_MOMENTUM:
                case GraphDataManager.ID_MONEY_FLOW_INDEX:
                case GraphDataManager.ID_ROC:
                case GraphDataManager.ID_QSTICK:
                case GraphDataManager.ID_RSI:
                case GraphDataManager.ID_RVI:
                case GraphDataManager.ID_STD_ERR:
                case GraphDataManager.ID_TEMA:
                case GraphDataManager.ID_TSF:
                case GraphDataManager.ID_TRIX:
                case GraphDataManager.ID_VERT_HORI_FILTER:
                case GraphDataManager.ID_VOLUME_ROC:
                case GraphDataManager.ID_WILDERS_SMOOTHING:
                case GraphDataManager.ID_WILLS_R:
                case GraphDataManager.ID_SWING_INDEX://limit move
                case GraphDataManager.ID_DISPARITY_INDEX:
                case GraphDataManager.ID_HULL_MOVING_AVG:
                    IndicatorTypeOneParameterProperty ipo = IndicatorPropertyFactory.loadSingleParamIndicatorProperties(items);
                    propertyStore.put(key, ipo);
                    break;

                case GraphDataManager.ID_KLINGER_OSCILL://time period,line color,line style
                case GraphDataManager.ID_MACD_VOLUME:
                    IndicatorTypeThreeParameterProperty ipt = IndicatorPropertyFactory.loadTypeThreeParameterIndicatorProperties(items);
                    propertyStore.put(key, ipt);
                    break;

                case GraphDataManager.ID_PARABOLIC_SAR://step,maximum
                    IndicatorParabolicSARProperties ips = IndicatorPropertyFactory.loadParabolicSARProperties(items);
                    propertyStore.put(key, ips);
                    break;

                //todo important
                case GraphDataManager.ID_MACD://time period,line color,line style
                    IndicatorMACDProperty imp = IndicatorPropertyFactory.loadMACDIndicatorProperties(items);
                    propertyStore.put(key, imp);
                    break;

                case GraphDataManager.ID_PRICE_OSCILL://many
                case GraphDataManager.ID_VOLUME_OSCILL:
                    IndicatorOscillatorProperty iop = IndicatorPropertyFactory.loadOscillatorIndicatorProperties(items);
                    propertyStore.put(key, iop);
                    break;

                case GraphDataManager.ID_MOVING_AVERAGE:
                case GraphDataManager.ID_VOLATILITY:
                    IndicatorTypeTwoParameterProperty itt = IndicatorPropertyFactory.loadTypeTwoParameterIndicatorProperties(items);
                    propertyStore.put(key, itt);
                    break;

                case GraphDataManager.ID_ICHIMOKU_KINKO_HYO://diffrent
                    IndicatorIchimokuKinkoHyoProperty iik = IndicatorPropertyFactory.loadIchimokuKinkoHyoIndicatorProperties(items);
                    propertyStore.put(key, iik);
                    break;

                case GraphDataManager.ID_STOCHAST_OSCILL://many
                    IndicatorStochasticOscillatorProperty iso = IndicatorPropertyFactory.loadStochasticOscillatorIndicatorProperties(items);
                    propertyStore.put(key, iso);
                    break;

                case GraphDataManager.ID_BOLLINGER_BANDS://time period,method,deviations
                    IndicatorBollingerBandsProperty ibb = IndicatorPropertyFactory.loadBollingerBandsIndicatorProperties(items);
                    propertyStore.put(key, ibb);
                    break;

                case GraphDataManager.ID_PRICE_CHANNELS:
                    IndicatorPriceChannelsProperty ipc = IndicatorPropertyFactory.loadPriceChannelsIndicatorProperties(items);
                    propertyStore.put(key, ipc);
                    break;

                case GraphDataManager.ID_ENVILOPES:
                    IndicatorEnvelopesProperty epc = IndicatorPropertyFactory.loadEnvelopesIndicatorProperties(items);
                    propertyStore.put(key, epc);
                    break;

                case GraphDataManager.ID_SDI://time period,deviations
                    IndicatorStandardDeviationProperty isd = IndicatorPropertyFactory.loadStandardDeviationIndicatorProperties(items);
                    propertyStore.put(key, isd);
                    break;

                case GraphDataManager.ID_RMI://time period,Momentum parameter
                    IndicatorRelativeMomentumIndexProperty irm = IndicatorPropertyFactory.loadRelMomentumIndexProperties(items);
                    propertyStore.put(key, irm);
                    break;

                case GraphDataManager.ID_SMO://time period,Momentum parameter
                    IndicatorStochasticMomentumIndexProperty ism = IndicatorPropertyFactory.loadStochasticMomentumInProperties(items);
                    propertyStore.put(key, ism);
                    break;

                case GraphDataManager.ID_ZIGZAG://reversal amount,method
                    IndicatorZigZagProperty izz = IndicatorPropertyFactory.loadZigZagIndicatorProperties(items);
                    propertyStore.put(key, izz);
                    break;

                case GraphDataManager.ID_KELTNER_CHANNELS://time periods, deviations
                    IndicatorKeltnerChannelsProperties ikcp = IndicatorPropertyFactory.loadKeltnerChannelsProperties(items);
                    propertyStore.put(key, ikcp);
                    break;

            }
        }

    }

    public Hashtable<Byte, Object> getPropertyStore() {
        return propertyStore;
    }

    public void saveDefaultProperties(ChartProperties cp) {

        byte objType = cp.getID();

        switch (objType) {
            case GraphDataManager.ID_ACCU_DISTRI://no param
            case GraphDataManager.ID_CHAIKINS_OSCILL:
            case GraphDataManager.ID_MEDIAN_PRICE:
            case GraphDataManager.ID_MFI:
            case GraphDataManager.ID_NVI:
            case GraphDataManager.ID_ON_BALANCE_VOLUME:
            case GraphDataManager.ID_PERFORMANCE:
            case GraphDataManager.ID_PVI:
            case GraphDataManager.ID_PRICE_VOLUME_TREND:
            case GraphDataManager.ID_TYPICAL_PRICE:
            case GraphDataManager.ID_WEIGHTED_CLOSE:
            case GraphDataManager.ID_WILLS_ACCU_DISTRI:
            case GraphDataManager.ID_ALLIGATOR:
                IndicatorPropertyFactory.saveDefaultIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_AROON://time peroids only
            case GraphDataManager.ID_AVG_TRUE_RANGE:
            case GraphDataManager.ID_CHAIKIN_MONEY_FLOW:
            case GraphDataManager.ID_CMO:
            case GraphDataManager.ID_CCI:
            case GraphDataManager.ID_DEMA:
                // added by Mevan
            case GraphDataManager.ID_COPP_CURVE:
            case GraphDataManager.ID_DPO:
            case GraphDataManager.ID_DX:
            case GraphDataManager.ID_PLUS_DI:
            case GraphDataManager.ID_MINUS_DI:
            case GraphDataManager.ID_ADX:
            case GraphDataManager.ID_ADXR:
            case GraphDataManager.ID_EASE_OF_MOVEMENT:
            case GraphDataManager.ID_FO:
            case GraphDataManager.ID_INTRADAY_MOMENTUM_INDEX:
            case GraphDataManager.ID_LRI:
            case GraphDataManager.ID_MASS_INDEX:
            case GraphDataManager.ID_MOMENTUM:
            case GraphDataManager.ID_MONEY_FLOW_INDEX:
            case GraphDataManager.ID_ROC:
            case GraphDataManager.ID_QSTICK:
            case GraphDataManager.ID_RSI:
            case GraphDataManager.ID_RVI:
            case GraphDataManager.ID_STD_ERR:
            case GraphDataManager.ID_TEMA:
            case GraphDataManager.ID_TSF:
            case GraphDataManager.ID_TRIX:
            case GraphDataManager.ID_VERT_HORI_FILTER:
            case GraphDataManager.ID_VOLUME_ROC:
            case GraphDataManager.ID_WILDERS_SMOOTHING:
            case GraphDataManager.ID_WILLS_R:
            case GraphDataManager.ID_SWING_INDEX://limit move
            case GraphDataManager.ID_DISPARITY_INDEX://limit move
            case GraphDataManager.ID_HULL_MOVING_AVG:
                IndicatorPropertyFactory.saveSingleParamIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_KLINGER_OSCILL://time period,line color,line style
            case GraphDataManager.ID_MACD_VOLUME:
                IndicatorPropertyFactory.saveTypeThreeParameterIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_PARABOLIC_SAR://step,maximum
                IndicatorPropertyFactory.saveParabolicSARProperties(cp);
                break;

            case GraphDataManager.ID_MACD://time period,line color,line style
                IndicatorPropertyFactory.saveMACDIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_PRICE_OSCILL://many
            case GraphDataManager.ID_VOLUME_OSCILL:
                IndicatorPropertyFactory.saveOscillatorIndicatorProperties(cp);
                break;
            //moving avrg,rate of change

            case GraphDataManager.ID_MOVING_AVERAGE:
            case GraphDataManager.ID_VOLATILITY:
                IndicatorPropertyFactory.saveTypeTwoParameterIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_ICHIMOKU_KINKO_HYO://diffrent
                IndicatorPropertyFactory.saveIchimokuKinkoHyoIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_STOCHAST_OSCILL://many
                IndicatorPropertyFactory.saveStochasticOscillatorIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_BOLLINGER_BANDS://time period,method,deviations
                IndicatorPropertyFactory.saveBollingerBandsIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_PRICE_CHANNELS:
                IndicatorPropertyFactory.savePriceChannelsIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_ENVILOPES:
                IndicatorPropertyFactory.saveEnvelopesIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_SDI://time period,deviations
                IndicatorPropertyFactory.saveStandardDeviationIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_RMI://time period,Momentum parameter
                IndicatorPropertyFactory.saveRelMomentumIndexProperties(cp);
                break;

            case GraphDataManager.ID_SMO://time period,Momentum parameter
                IndicatorPropertyFactory.saveStochasticMomentumInProperties(cp);
                break;

            case GraphDataManager.ID_ZIGZAG://reversal ammount,method
                IndicatorPropertyFactory.saveZigZagIndicatorProperties(cp);
                break;

            case GraphDataManager.ID_KELTNER_CHANNELS://timeperiods, deviations
                IndicatorPropertyFactory.saveKeltnerChannelsIndicatorProperties(cp);
                break;

        }
        //refrshes the property store for new changes
        reloadPropertyStore();
    }

    public boolean hasDefauiltProperties(byte objectID) {
        Object op = propertyStore.get(new Byte(objectID));
        if (op == null) {
            return false;
        }
        return true;
    }

    public void deleteDefaultProperties(ChartProperties cp) {
        IndicatorPropertyXMLService.deleteIndicatorProperties((int) cp.getID());
        reloadPropertyStore();
    }
}


