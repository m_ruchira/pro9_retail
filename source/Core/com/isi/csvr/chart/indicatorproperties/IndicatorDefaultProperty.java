package com.isi.csvr.chart.indicatorproperties;

import java.awt.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Aug 3, 2009
 * Time: 10:41:03 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorDefaultProperty {

    protected static final String OBJECT_ID = "objectID";
    protected final String UP_COLOR = "upColor";
    protected final String DOWN_COLOR = "downColor";
    protected final String LINE_STYLE = "lineStyle";
    protected final String LINE_THICKNESS = "lineThickness";
    protected final String USE_SAME_COLOR = "useSameColor";
    protected final String OHLC_PRIORITY = "OHLCPriority";
    protected final String CHART_STYLE = "chartStyle";
    protected int objectId;
    protected Color upColor;
    protected Color downColor;
    protected byte lineStyle;
    protected float lineThickness;
    protected boolean useSameColor;
    protected byte OHLCPriority;
    protected byte chartStyle;

    public IndicatorDefaultProperty(int objectId) {
        this.objectId = objectId;
    }

    public IndicatorDefaultProperty() {

    }

    public IndicatorDataItem setObjectID() {
        return new IndicatorDataItem(OBJECT_ID, objectId);
    }


    public IndicatorDataItem setOHLCPriority(byte priority) {
        this.OHLCPriority = priority;
        return new IndicatorDataItem(OHLC_PRIORITY, OHLCPriority);
    }

    public Color getUpColor() {
        return upColor;
    }

    public IndicatorDataItem setUpColor(Color lineColor) {
        this.upColor = lineColor;
        return new IndicatorDataItem(UP_COLOR, lineColor.getRGB());
    }

    public boolean isUsingSameColor() {
        return useSameColor;
    }

    public IndicatorDataItem setUseSameColor(boolean useSameClr) {
        this.useSameColor = useSameClr;
        return new IndicatorDataItem(USE_SAME_COLOR, useSameClr);
    }

    public Color getDownColor() {
        return downColor;
    }

    public IndicatorDataItem setDownColor(Color lineColor) {
        this.downColor = lineColor;
        return new IndicatorDataItem(DOWN_COLOR, lineColor.getRGB());
    }

    public byte getLineStyle() {
        return lineStyle;
    }

    public IndicatorDataItem setLineStyle(byte lineStyle) {
        this.lineStyle = lineStyle;
        return new IndicatorDataItem(LINE_STYLE, lineStyle);
    }

    public float getLineThickness() {
        return lineThickness;
    }

    public IndicatorDataItem setLineThickness(float lineThickness) {
        this.lineThickness = lineThickness;
        return new IndicatorDataItem(LINE_THICKNESS, lineThickness);
    }

    public byte getChartStyle() {
        return chartStyle;
    }

    public IndicatorDataItem setChartStyle(byte chartStyle) {
        this.chartStyle = chartStyle;
        return new IndicatorDataItem(CHART_STYLE, chartStyle);
    }

    public byte getOHLCPriority() {
        return OHLCPriority;
    }


    private void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(OBJECT_ID)) {
            this.objectId = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(UP_COLOR)) {
            this.upColor = new Color(Integer.parseInt(propertyValue.toString()));
        } else if (propertyName.equals(DOWN_COLOR)) {
            this.downColor = new Color(Integer.parseInt(propertyValue.toString()));
        } else if (propertyName.equals(LINE_STYLE)) {
            this.lineStyle = Byte.parseByte(propertyValue.toString());
        } else if (propertyName.equals(LINE_THICKNESS)) {
            this.lineThickness = Float.parseFloat(propertyValue.toString());
        } else if (propertyName.equals(USE_SAME_COLOR)) {
            this.useSameColor = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(CHART_STYLE)) {
            this.chartStyle = Byte.parseByte(propertyValue.toString());
        } else if (propertyName.equals(OHLC_PRIORITY)) {
            this.OHLCPriority = Byte.parseByte(propertyValue.toString());
        }
    }

    protected String convertFontToString(Font font) {
        return (font.getName() + "," + font.getSize() + "," + font.getStyle());
    }

    protected Font getFontFromString(String strFont) {
        StringTokenizer tokenizer = new StringTokenizer(strFont, ","); //tokenize from the " , "

        String name = tokenizer.nextToken();
        int size = Integer.parseInt(tokenizer.nextToken());
        int style = Integer.parseInt(tokenizer.nextToken());

        return new Font(name, style, size);
    }

    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {

        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }
}
