package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 2:58:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorOscillatorProperty extends IndicatorDefaultProperty {

    private final String SHORT_PERIODS = "shortPeriod";
    private final String LONG_PERIODS = "longPeriod";
    private final String METHOD = "method";
    private final String CALC_METHOD = "calcMethod";
    private int shortPeriods;
    private int longPeriods;
    private byte method;
    private int calcMethod;

    public IndicatorOscillatorProperty() {
        super();
    }

    public IndicatorOscillatorProperty(int objectId) {
        super(objectId);
    }

    public int getShortPeriod() {
        return shortPeriods;
    }

    public IndicatorDataItem setShortPeriod(int timePeriods) {
        this.shortPeriods = timePeriods;
        return new IndicatorDataItem(SHORT_PERIODS, timePeriods);
    }

    public int getLongPeriod() {
        return longPeriods;
    }

    public IndicatorDataItem setLongPeriod(int sp) {
        this.longPeriods = sp;
        return new IndicatorDataItem(LONG_PERIODS, sp);
    }

    public byte getMethod() {
        return method;
    }

    public IndicatorDataItem setMethod(byte dsp) {
        this.method = dsp;
        return new IndicatorDataItem(METHOD, dsp);
    }

    public int getCalcMethod() {
        return calcMethod;
    }

    public IndicatorDataItem setgetCalcMethod(int dsp) {
        this.calcMethod = dsp;
        return new IndicatorDataItem(CALC_METHOD, dsp);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(SHORT_PERIODS)) {
            this.shortPeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(LONG_PERIODS)) {
            this.longPeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(METHOD)) {
            this.method = Byte.parseByte(propertyValue.toString());
        } else if (propertyName.equals(CALC_METHOD)) {
            this.calcMethod = Integer.parseInt(propertyValue.toString());
        }
    }
}
