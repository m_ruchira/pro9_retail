package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Dec 15, 2009
 * Time: 10:40:01 AM
 * Version: 0.1
 * To change this template use File | Settings | File Templates.
 */


public class CoppockCurve extends ChartProperties implements Indicator, Serializable {

    // period constants
    public final static byte PERIOD_MONTHLY = 0;
    private byte period = CoppockCurve.PERIOD_MONTHLY;
    public final static byte PERIOD_DAILY = 1;
    // Monthly constants
    private static final int WMA_MONTHLY = 10;
    //    private byte period = CoppockCurve.PERIOD_DAILY;
    private int wmaPeriod = WMA_MONTHLY;
    private static final int ROC_A_MONTHLY = 14;
    private int rocAPeriod = ROC_A_MONTHLY;
    private static final int ROC_B_MONTHLY = 11;
    private int rocBPeriod = ROC_B_MONTHLY;
    // Daily Constants
    private static final int WMA_DAILY = 210;
    private static final int ROC_A_DAILY = 294;
    private static final int ROC_B_DAILY = 231;
    private static final long serialVersionUID = UID_COPPOCK_CURVE;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public CoppockCurve(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_COPPOCK_CURVE") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_COPPOCK_CURVE");

        isIndicator = true;
        isMarketIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.period = (byte) idp.getTimePeriods();

        }
    }

    public static String[] getPeriodArray() {
        return new String[]{Language.getString("PERIOD_MONTHLY"),
                Language.getString("PERIOD_DAILY")};
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, CopCurvePeriod method, byte stepROC14, byte stepROC11, byte stepROC_Added, byte destIndex) {

        byte period = (byte) method.ordinal();
        int wmaPeriod = WMA_MONTHLY, rocAPeriod = ROC_A_MONTHLY, rocBPeriod = ROC_B_MONTHLY;

        byte ohlcPrio = GraphDataManager.INNER_Close;
        ChartRecord cr;

        // get the adjusted dates
        ArrayList tempArray = new ArrayList();
        Calendar cal = new GregorianCalendar();
        int currMonth = Integer.MAX_VALUE, tempMonth;

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(4);
        }

        if (period == CoppockCurve.PERIOD_MONTHLY) {
            wmaPeriod = WMA_MONTHLY;
            rocAPeriod = ROC_A_MONTHLY;
            rocBPeriod = ROC_B_MONTHLY;

            for (int i = al.size(); i > 0; i--) {
                if (i == al.size()) {
                    cr = (ChartRecord) al.get(i - 1);
                    cal.setTimeInMillis(cr.Time);
                    currMonth = cal.get(Calendar.YEAR) * 100 + cal.get(Calendar.MONTH) + 1;
                    tempArray.add(al.get(i - 1));
                    continue;
                }
                cr = (ChartRecord) al.get(i - 1);

                cal.setTimeInMillis(cr.Time);
                tempMonth = cal.get(Calendar.YEAR) * 100 + cal.get(Calendar.MONTH) + 1;

                if (tempMonth < currMonth) {
                    tempArray.add(0, al.get(i - 1));
                    currMonth = tempMonth;
                    continue;
                }
            }
            if (rocAPeriod + wmaPeriod > tempArray.size()) {
                return;
            }
        } else {
            wmaPeriod = WMA_DAILY;
            rocAPeriod = ROC_A_DAILY;
            rocBPeriod = ROC_B_DAILY;

            for (int i = al.size(); i > 0; i--) {
                if (i == al.size()) {
                    cr = (ChartRecord) al.get(i - 1);
                    cal.setTimeInMillis(cr.Time);
                    currMonth = cal.get(Calendar.YEAR) * 10000 + cal.get(Calendar.MONTH) * 100 + cal.get(Calendar.DAY_OF_MONTH);
                    tempArray.add(al.get(i - 1));
                    continue;
                }
                cr = (ChartRecord) al.get(i - 1);
                cal.setTimeInMillis(cr.Time);
                tempMonth = cal.get(Calendar.YEAR) * 10000 + cal.get(Calendar.MONTH) * 100 + cal.get(Calendar.DAY_OF_MONTH);

                if (tempMonth < currMonth) {
                    tempArray.add(0, al.get(i - 1));
                    currMonth = tempMonth;
                    continue;
                }
            }
            if (rocAPeriod + wmaPeriod > tempArray.size()) {
                return;
            }
        }


        CoppockCurve.getRateOfChange(tempArray, (rocAPeriod - rocBPeriod), rocBPeriod, GDM, ohlcPrio, stepROC11);
        CoppockCurve.getRateOfChange(tempArray, 0, rocAPeriod, GDM, ohlcPrio, stepROC14);

        for (int i = 0; i < tempArray.size(); i++) {
            cr = (ChartRecord) tempArray.get(i);
            cr.setStepValue(stepROC_Added, cr.getStepValue(stepROC11) + cr.getStepValue(stepROC14));
        }

        MovingAverage.getMovingAverage(tempArray, 0, wmaPeriod, MovingAverage.METHOD_WEIGHTED, stepROC_Added, destIndex);
    }

    public static void getCoppockCurve(ArrayList al, int bIndex, int timePeriods, byte destIndex) {

    }

    public static int getAuxStepCount() {
        return 2;
    }

    public static void getRateOfChange(ArrayList al, int beginIndex, int timePeriods, GraphDataManagerIF GDM, byte srcIndex, byte destIndex) {
        ChartRecord cr, crOld;
        int loopBegin = beginIndex + timePeriods;

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - timePeriods);
            if (cr != null && crOld != null) {
                cr.setStepValue(destIndex, 100f * (cr.Close - crOld.Close) / crOld.Close);
            } else {
                cr.setStepValue(destIndex, Double.MIN_VALUE);
            }
        }
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof CoppockCurve) {
            CoppockCurve cc = (CoppockCurve) cp;
            this.innerSource = cc.innerSource;
            this.period = cc.period;
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.period = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/PeriodSpan"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "PeriodSpan", Byte.toString(this.period));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        return Language.getString("IND_COPPOCK_CURVE") + " " + parent;
    }

    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public byte getPeriod() {
        return this.period;
    }

    public void setPeriod(byte p) {
        this.period = p;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        byte ohlcPrio = GraphDataManager.INNER_Close;
        ChartRecord cr;

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
            cr.setStepSize(4);
        }

        byte stepROC14 = ChartRecord.STEP_1;
        byte stepROC11 = ChartRecord.STEP_2;
        byte stepWMA = ChartRecord.STEP_3;
        byte stepROC_Added = ChartRecord.STEP_4;

        // get the adjusted dates
        ArrayList tempArray = new ArrayList();
        Calendar cal = new GregorianCalendar();
        int currMonth = Integer.MAX_VALUE, tempMonth;

        if (period == CoppockCurve.PERIOD_MONTHLY) {
            wmaPeriod = WMA_MONTHLY;
            rocAPeriod = ROC_A_MONTHLY;
            rocBPeriod = ROC_B_MONTHLY;

            for (int i = al.size(); i > 0; i--) {
                if (i == al.size()) {
                    cr = (ChartRecord) al.get(i - 1);
                    cal.setTimeInMillis(cr.Time);
                    currMonth = cal.get(Calendar.YEAR) * 100 + cal.get(Calendar.MONTH) + 1;
                    tempArray.add(al.get(i - 1));
                    continue;
                }
                cr = (ChartRecord) al.get(i - 1);

                cal.setTimeInMillis(cr.Time);
                tempMonth = cal.get(Calendar.YEAR) * 100 + cal.get(Calendar.MONTH) + 1;

                if (tempMonth < currMonth) {
                    tempArray.add(0, al.get(i - 1));
                    currMonth = tempMonth;
                    continue;
                }
            }
            if (rocAPeriod + wmaPeriod > tempArray.size()) {
                return;
            }
        } else {
            wmaPeriod = WMA_DAILY;
            rocAPeriod = ROC_A_DAILY;
            rocBPeriod = ROC_B_DAILY;

            for (int i = al.size(); i > 0; i--) {
                if (i == al.size()) {
                    cr = (ChartRecord) al.get(i - 1);
                    cal.setTimeInMillis(cr.Time);
                    currMonth = cal.get(Calendar.YEAR) * 10000 + cal.get(Calendar.MONTH) * 100 + cal.get(Calendar.DAY_OF_MONTH);
                    tempArray.add(al.get(i - 1));
                    continue;
                }
                cr = (ChartRecord) al.get(i - 1);
                cal.setTimeInMillis(cr.Time);
                tempMonth = cal.get(Calendar.YEAR) * 10000 + cal.get(Calendar.MONTH) * 100 + cal.get(Calendar.DAY_OF_MONTH);

                if (tempMonth < currMonth) {
                    tempArray.add(0, al.get(i - 1));
                    currMonth = tempMonth;
                    continue;
                }
            }
            if (rocAPeriod + wmaPeriod > tempArray.size()) {
                return;
            }
        }

        getRateOfChange(tempArray, (rocAPeriod - rocBPeriod), rocBPeriod, GDM, ohlcPrio, stepROC11);
        getRateOfChange(tempArray, 0, rocAPeriod, GDM, ohlcPrio, stepROC14);

        for (int i = 0; i < tempArray.size(); i++) {
            cr = (ChartRecord) tempArray.get(i);
            cr.setStepValue(stepROC_Added, cr.getStepValue(stepROC11) + cr.getStepValue(stepROC14));
        }

        MovingAverage.getMovingAverage(tempArray, 0, wmaPeriod, MovingAverage.METHOD_WEIGHTED, stepROC_Added, stepWMA);

        // insert into gdm
        for (int j = 0; j < tempArray.size(); j++) {
            cr = (ChartRecord) tempArray.get(j);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(cr.getStepValue(stepWMA));
            }
        }

        for (int i = (rocAPeriod + wmaPeriod - 1); i >= 0; i--) {
            cr = (ChartRecord) tempArray.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());  // remove the first few values that are not needed.
        }
    }

    public String getShortName() {
        return Language.getString("IND_COPPOCK_CURVE");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
