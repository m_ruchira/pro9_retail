package com.isi.csvr.chart;

import java.io.Serializable;
import java.util.Comparator;

/**
 * User: Udaka
 * Date: Aug 21, 2005
 * Time: 11:59:19 PM
 */
public class SplitPointComparator implements Comparator, Serializable {
    public int compare(Object o1, Object o2) {
        SplitPointRecord spr1 = (SplitPointRecord) o1;
        SplitPointRecord spr2 = (SplitPointRecord) o2;

        if (spr1.time > spr2.time) {
            return 1;
        } else if (spr1.time < spr2.time) {
            return -1;
        } else {
            if (spr1.group != spr2.group) {
                return -1;
            } else {
                // this part is merely to be used by comparison points later
                // not for insertion
                if (spr1.sequence > spr2.sequence)
                    return 1;
                else
                    return -1;
                /*if (spr1.factor > spr2.factor) {
                    return 1;
                }else if (spr1.factor<spr2.factor){
                    return -1;
                }else{
                    return 1;
                }*/
            }
        }
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}