package com.isi.csvr.chart;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */


import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
//import com.isi.csvr.Themeable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class ComboRenderer extends GraphLabel
        implements ListCellRenderer {
    ComboImage icon;

    public ComboRenderer() {
        setOpaque(true);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        ComboItem item = (ComboItem) value;

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        setText(item.getDescription());
        if (isEnabled()) {
            setIcon(item.getImage());
        } else {
            setIcon(null);
        }
        setDisabledIcon(item.getDisabledImage());
        setFont(Theme.getDefaultFont(Font.BOLD, 10));

        return this;
    }
}
