package com.isi.csvr.chart;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author unascribed
 * @version 1.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorBollingerBandsProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;

public class BollingerBand extends MovingAverage implements FillArea {
    public final static byte BT_UPPER = -1;
    public final static byte BT_CENTER = 0;
    public final static byte BT_LOWER = 1;

    private static final long serialVersionUID = UID_BollingerBand;

    //Fields
    private byte bandType;
    private float deviations;
    // used only by BT_CENTER
    private BollingerBand upper = null, lower = null;

    private boolean fillBands;
    private Color fillColor;
    private int transparency;
    private String tableColumnHeading;

    public BollingerBand(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_BOLLINGER_BANDS");
        setTimePeriods(20);
        deviations = 2;
        this.bandType = BT_CENTER;

        //fill bands related
        fillBands = true;
        fillColor = Color.blue;
        transparency = 30;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorBollingerBandsProperty idp = (IndicatorBollingerBandsProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());

            this.setTimePeriods(idp.getTimePeriods());
            this.setDeviations(idp.getDeviations());
            this.setMethod(idp.getMethod());

            this.setFillBands(idp.isFillBands());
            this.setFillColor(idp.getFillColor());
            this.setTransparency(idp.getTransparency());
        }
    }

    protected static String getName() {
        return Language.getString("BOLLINGER_BANDS");
    }

    public static double getBollingerMiddle(LinkedList<ChartRecord> ll, int timePeriods,
                                            byte method, byte srcIndex, byte destIndex) {
        return MovingAverage.getMovingAverage(ll, timePeriods, method, srcIndex, destIndex);
    }

    /**
     * @param ll
     * @param timePeriods
     * @param deviations  Pass negative value for lower bollinger value. Pass positive value for upper bollinger value
     * @param method
     * @param srcIndex
     * @return Bollinger Value
     */
    public static double getBollingerOther(LinkedList<ChartRecord> ll, int timePeriods, double deviations,
                                           byte method, byte srcIndex) {
        double zigmaX = 0, zigmaXX = 0;
        double xi, stdDev, myu, xBase = 0;
        ChartRecord cr, crOld;
        int loopBegin;

        byte stepMA = ChartRecord.STEP_1;
        double val = MovingAverage.getMovingAverage(ll, timePeriods, method, srcIndex, stepMA);
        double zigmaXminusM, xj;

        loopBegin = timePeriods;
        stdDev = 0;
        for (int i = loopBegin; i < ll.size(); i++) {
            cr = ll.get(i);
            zigmaXminusM = 0;
            for (int j = 0; j < timePeriods; j++) {
                crOld = ll.get(i - j);
                xj = cr.getValue(stepMA) - crOld.getValue(srcIndex);
                zigmaXminusM += xj * xj;
            }

            stdDev = Math.sqrt(zigmaXminusM / timePeriods);

            if (Double.isNaN(stdDev)) {
                stdDev = 0;
            }
        }

        return val + (stdDev * deviations * Math.abs(deviations));

        /*switch (method) {
            case METHOD_SIMPLE:
            loopBegin = timePeriods;
            stdDev = 0;

            for (int i = loopBegin; i < ll.size(); i++) {
                cr = ll.get(i);
                zigmaXminusM = 0;
                for (int j = 0; j < timePeriods; j++) {
                    crOld = ll.get(i - j);
                    xj = cr.getValue(stepMA) - crOld.getValue(srcIndex);
                    zigmaXminusM += xj * xj;
                        }

                stdDev = Math.sqrt(zigmaXminusM / timePeriods);

                if (Double.isNaN(stdDev)) {
                    stdDev = 0;
                    }
                }

            return val + (stdDev * deviations * Math.abs(deviations));

            case METHOD_WEIGHTED:
            loopBegin = timePeriods;
            stdDev = 0;
            for (int i = loopBegin; i < ll.size(); i++) {
                cr = ll.get(i);
                zigmaXminusM = 0;
                for (int j = 0; j < timePeriods; j++) {
                    crOld = ll.get(i - j);
                    xj = cr.getValue(stepMA) - crOld.getValue(srcIndex);
                    zigmaXminusM += xj * xj;
                }

                stdDev = Math.sqrt(zigmaXminusM / timePeriods);

                if (Double.isNaN(stdDev)) {
                    stdDev = 0;
                }
            }

            return val + (stdDev * deviations * Math.abs(deviations));

        *//*int weightSum = Math.round((timePeriods + 1f) * timePeriods / 2f);
                myu = getWeightedMovAvg(ll, 0, ll.size() - 1, weightSum, timePeriods, srcIndex);
                zigmaXX = 0;
                for (int i = (ll.size() - timePeriods); i < ll.size(); i++) {
             cr = ll.get(i);
                    if (cr != null) {
                        xi = cr.getValue(srcIndex);
                        zigmaXX += (xi - myu) * (xi - myu);

                    }
                }
                stdDev = deviations * Math.sqrt(zigmaXX / timePeriods);
                if (Double.isNaN(stdDev) || Double.isInfinite(stdDev)) {
                    stdDev = 0;
                }
         return myu + stdDev;*//*

            case METHOD_EXPONENTIAL:
                *//*loopBegin = timePeriods - 1;
                myu = 0;
                float expPercentage = 2f / (timePeriods + 1f);
                myu = getExpMovAvgExt(ll, myu, loopBegin - 1, expPercentage, srcIndex);

                for (int i = loopBegin; i < ll.size(); i++) {
                    cr = ll.get(i);
                    if (cr != null) {
                        myu = getExpMovAvgExt(ll, myu, i, expPercentage, srcIndex);
                        zigmaXX = 0;
                        for (int j = i - loopBegin; j <= i; j++) {
                            ChartRecord crLoop = ll.get(j);
                            xi = crLoop.getValue(srcIndex);
                            zigmaXX += (xi - myu) * (xi - myu);
                        }
                        stdDev = deviations * Math.sqrt(zigmaXX / timePeriods);
                        if (Double.isNaN(stdDev) || Double.isInfinite(stdDev)) {
                            stdDev = 0;
                        }
                        cr.setStepValue(ChartRecord.STEP_1, myu + stdDev);
                    }
                }
                return ll.get(ll.size() - 1).getStepValue(ChartRecord.STEP_1);
                *//*

                loopBegin = timePeriods;
                stdDev = 0;
                for (int i = loopBegin; i < ll.size(); i++) {
                    cr = ll.get(i);
                    zigmaXminusM = 0;
                    for (int j = 0; j < timePeriods; j++) {
                        crOld = ll.get(i - j);
                        xj = cr.getValue(stepMA) - crOld.getValue(srcIndex);
                        zigmaXminusM += xj * xj;
                    }

                    stdDev = Math.sqrt(zigmaXminusM / timePeriods);

                    if (Double.isNaN(stdDev)) {
                        stdDev = 0;
                    }
                }

                return val + (stdDev * deviations * Math.abs(deviations));

        }

        return 0.0;
        */
    }

    public static void getBollingerBand(ArrayList al, int bIndex, int timePeriods, byte method, float deviations,
                                        int bollingerBandType, GraphDataManagerIF GDM, byte stepReserved1, byte stepReserved2, byte stepMA, byte srcIndex, byte destIndex) {
        ChartRecord cr, crOld;
        float sd = deviations;

        switch (bollingerBandType) {
            case BT_CENTER:
                MovingAverage.getVariableMovingAverage(al, bIndex, timePeriods, method, GDM, stepReserved1, stepReserved2, srcIndex, destIndex);
                return;
            case BT_UPPER:
                sd = deviations;
                break;
            case BT_LOWER:
                sd = -deviations;
                break;
        }

        long entryTime = System.currentTimeMillis();

        //int loopBegin = bIndex + 2 * timePeriods - 2; // for MA and Std Dev
        int loopBegin = bIndex + timePeriods - 1; // for MA and Std Dev
        if (loopBegin >= al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        MovingAverage.getVariableMovingAverage(al, bIndex, timePeriods, method, GDM, stepReserved1, stepReserved2, srcIndex, stepMA);

        int baseIndex = bIndex + timePeriods - 1; // after MA
        for (int i = 0; i < baseIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        ///////////// this is done to eleminate all the null points ////////////
        ArrayList alNotNull = new ArrayList();
        for (int i = baseIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(stepMA);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                alNotNull.add(cr);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        ////////////////////////////////////////////////////////////////////////

        if (alNotNull.size() < timePeriods) {
            for (int i = 0; i < alNotNull.size(); i++) {
                cr = (ChartRecord) alNotNull.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }


        double zigmaXX = 0;
        double xi, xPi, stdDev;
        // first point
        for (int i = 0; i < timePeriods; i++) {
            cr = (ChartRecord) alNotNull.get(i);
            xi = cr.getValue(stepMA) - cr.getValue(srcIndex);
            zigmaXX += xi * xi;
            if (i == timePeriods - 1) {
                stdDev = sd * Math.sqrt(zigmaXX / timePeriods);
                if (Double.isNaN(stdDev)) {
                    stdDev = 0;
                }
                cr.setValue(destIndex, cr.getValue(stepMA) + stdDev);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        /*
        // rest of the points
        for (int i = timePeriods; i < alNotNull.size(); i++) {
            cr = (ChartRecord) alNotNull.get(i);
            crOld = (ChartRecord) alNotNull.get(i - timePeriods);
            xi = cr.getValue(stepMA) - cr.getValue(srcIndex);
            xPi = crOld.getValue(stepMA) - crOld.getValue(srcIndex);

            zigmaXX = zigmaXX + xi * xi - xPi * xPi;
            stdDev = sd * Math.sqrt(zigmaXX / timePeriods);
            if (Double.isNaN(stdDev)) {
                stdDev = 0;
            }
            cr.setValue(destIndex, cr.getValue(stepMA) + stdDev);
        }
        */
        double zigmaXminusM = 0, xj = 0;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            zigmaXminusM = 0;
            for (int j = 0; j < timePeriods; j++) {
                crOld = (ChartRecord) al.get(i - j);
                xj = cr.getValue(stepMA) - crOld.getValue(srcIndex);
                zigmaXminusM += xj * xj;
            }
            // cr = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (cr != null) {
                stdDev = Math.sqrt(zigmaXminusM / timePeriods);

                if (Double.isNaN(stdDev)) {
                    stdDev = 0;
                }
                cr.setValue(destIndex, cr.getValue(stepMA) + (stdDev * sd));
                //cr.setValue(destIndex, cr.getValue(stepMA) + (stdDev * sd * deviations));
                //aPoint.setIndicatorValue(cr.getValue(stepMA) + (stdDev * sd * deviations));
            }
        }

        //System.out.println("**** Inter Bollinger Calc time "  + (entryTime - System.currentTimeMillis()));

    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, MAMethod mamethod, double deviations, int bollingerBandType,
                                          byte stepReserved1, byte stepReserved2, byte stepMA, byte srcIndex, byte destIndex) {


        byte method = (byte) mamethod.ordinal();

        ///////////// this is done to avoid null points ////////////
        ChartRecord cr;
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        ////////////////////////////////////////////////////////////

        getBollingerBand(al, bIndex, t_im_ePeriods, method, (float) deviations, bollingerBandType,
                GDM, stepReserved1, stepReserved2, stepMA, srcIndex, destIndex);
    }

    public static int getAuxStepCount() {
        return 3;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 20;
        this.deviations = 2;
        this.fillBands = true;
        this.fillColor = Color.blue;
        this.transparency = 30;

        if (upper != null && lower != null) {
            upper.assignDefaultValues();
            lower.assignDefaultValues();
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        //this.timePeriods -- loaded in the super class
        this.bandType = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/BandType"));
        this.deviations = Float.parseFloat(TemplateFactory.loadProperty(xpath, document, preExpression + "/Deviations"));
        this.fillBands = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/fillBands"));
        this.transparency = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/transparency"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/fillColor").split(",");
        this.fillColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        //this.timePeriods -- saved in the super class
        TemplateFactory.saveProperty(chart, document, "BandType", Byte.toString(this.bandType));
        TemplateFactory.saveProperty(chart, document, "Deviations", Float.toString(this.deviations));
        TemplateFactory.saveProperty(chart, document, "fillBands", Boolean.toString(this.fillBands));
        TemplateFactory.saveProperty(chart, document, "transparency", Integer.toString(this.transparency));
        String strColor = this.fillColor.getRed() + "," + this.fillColor.getGreen() + "," + this.fillColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "fillColor", strColor);
    }

    //bandType

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof BollingerBand) {
            BollingerBand bb = (BollingerBand) cp;
            this.bandType = bb.bandType;
            this.deviations = bb.deviations;
            this.fillBands = bb.fillBands;
            this.fillColor = bb.fillColor;
            this.transparency = bb.transparency;

            if ((((BollingerBand) cp).getBandType() == BT_CENTER)) {
                this.upper = bb.upper;
                this.lower = bb.lower;
            }

            if (upper != null && lower != null) {
                this.upper.fillBands = bb.fillBands;
                this.upper.fillColor = bb.fillColor;
                this.upper.transparency = bb.transparency;

                this.lower.fillBands = bb.fillBands;
                this.lower.fillColor = bb.fillColor;
                this.lower.transparency = bb.transparency;
            }
        }
        /*if (upper != null) {
            upper.assignValuesFrom(cp);
            upper.setBandType(BollingerBand.BT_UPPER);
            upper.setSelected(false);
        }
        if (lower != null) {
            lower.assignValuesFrom(cp);
            lower.setBandType(BollingerBand.BT_LOWER);
            lower.setSelected(false);
        }*/
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

//        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return getName() + parent + getClose_Periods_MethodStr(sa.length != 2);
    }

    //deviations

    public boolean hasItsOwnScale() {
//        if (super.getInnerSource() instanceof Indicator){
//            return ((Indicator)super.getInnerSource()).hasItsOwnScale();
//        }
//        return false;
        return true;
    }

    protected String getClose_Periods_MethodStr(boolean isIndicator) {
        if (bandType == BT_CENTER) {
            return "(" + getPriorityStr(isIndicator) + ", " + getTimePeriods() + ", "
                    + getMethodString() + ")";
        } else {
            return "(" + getPriorityStr(isIndicator) + ", " + getTimePeriods() + ", "
                    + getMethodString() + ", " + getDeviationString() + ")";
        }
    }

    protected String getDeviationString() {
        DecimalFormat numformat = new DecimalFormat("##0.0###");
        return numformat.format(deviations);
    }

    public byte getBandType() {
        return bandType;
    }

    public void setBandType(byte bType) {
        bandType = bType;
    }

    public float getDeviations() {
        return deviations;
    }

    public void setDeviations(float d) {
        deviations = d;
        if (bandType == BT_CENTER) {
            try {
                if (upper != null) {
                    upper.setDeviations(d);
                }
                if (lower != null) {
                    lower.setDeviations(d);
                }
            } catch (Exception ex) {
            }
        }
    }

    public boolean isFillBands() {
        return this.fillBands;
    }

    public void setFillBands(boolean fillBands) {
        this.fillBands = fillBands;
    }

    //upper

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    //lower

    public Color getFillTransparentColor() {
        return new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(), transparency);
    }

    public int getTransparency() {
        return transparency;
    }

    //method

    public void setTransparency(int transparency) {
        this.transparency = transparency;
    }

    //############################################

    public BollingerBand getUpper() {
        return upper;
    }
    //############################################

    public void setUpper(ChartProperties cp) {
        if (cp instanceof BollingerBand)
            upper = (BollingerBand) cp;
    }


    //NOTE: Followings are for RadarScreen

    public BollingerBand getLower() {
        return lower;
    }

    public void setLower(ChartProperties cp) {
        if (cp instanceof BollingerBand)
            lower = (BollingerBand) cp;
    }

    public void setMethod(byte tp) {
        super.setMethod(tp);
        if (bandType == BT_CENTER) {
            try {
                if (upper != null) {
                    upper.setMethod(tp);
                }
                if (lower != null) {
                    lower.setMethod(tp);
                }
            } catch (Exception ex) {
            }
        }
    }
    /* public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                  int t_im_ePeriods, MAMethod mamethod, double deviations, int bollingerBandType,
                  byte stepReserved1, byte stepReserved2, byte stepMA, byte srcIndex, byte destIndex) {
    */

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        ChartRecord cr, crOld;
        float sd = this.deviations;

        switch (bandType) {
            case BT_CENTER:
                super.insertIndicatorToGraphStore(al, GDM, index);
                return;
            case BT_UPPER:
                sd = deviations;
                break;
            case BT_LOWER:
                sd = -deviations;
                break;
        }

        if (this.timePeriods > al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
            return;
        }

        //setting step size 3 (reserved)
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }
        // steps involved
        byte stepReserved1 = ChartRecord.STEP_1;
        byte stepReserved2 = ChartRecord.STEP_2;
        byte stepMA = ChartRecord.STEP_3;

        MovingAverage.getVariableMovingAverage(al, 0, this.timePeriods, this.method, GDM, stepReserved1, stepReserved2, getOHLCPriority(), stepMA);
        ///////////// this is done to avoid null points ////////////
        int tmpIndex = Math.max(al.size() - 1, 0);
        int bIndex = this.timePeriods - 1;
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(stepMA);
            if (!Double.isNaN(val) && !Double.isInfinite(val) && Indicator.NULL != val) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        ////////////////////////////////////////////////////////////

        int loopBegin = bIndex;// + timePeriods;

        ChartPoint aPoint;
        double zigmaXX = 0;
        double xi, xPi, stdDev;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        double zigmaXminusM = 0, xj = 0;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            zigmaXminusM = 0;
            for (int j = 0; j < timePeriods; j++) {
                crOld = (ChartRecord) al.get(i - j);
                xj = cr.getValue(stepMA) - crOld.getValue(getOHLCPriority());
                zigmaXminusM += xj * xj;
            }
            aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                stdDev = Math.sqrt(zigmaXminusM / timePeriods);

                if (Double.isNaN(stdDev)) {
                    stdDev = 0;
                }
                aPoint.setIndicatorValue(cr.getValue(stepMA) + (stdDev * sd));
            }
        }

    }

    public String getShortName() {
        return getName();
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }


}
