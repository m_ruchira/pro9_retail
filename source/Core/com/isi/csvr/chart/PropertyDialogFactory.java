package com.isi.csvr.chart;

import com.isi.csvr.Client;
import com.isi.csvr.FontChooser;
import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.chart.cashflowcharts.NetOrdersInd;
import com.isi.csvr.chart.cashflowcharts.NetTurnOverInd;
import com.isi.csvr.chart.chartobjects.*;
import com.isi.csvr.chart.options.UIHelper;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.DefaultFormatter;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class
        PropertyDialogFactory {

    public final static int PEN_SOLID = 0;
    public final static int PEN_DOTTED = 1;
    public final static int PEN_DASH = 2;
    public final static int PEN_DASH_DOT = 3;
    public final static int PEN_DASH_DOT_DOT = 4;

    final static Dimension DIM_DIALOG = new Dimension(320, 260);
    final static Dimension DIM_FIB_RETR_DIALOG = new Dimension(415, 590);
    final static Dimension DIM_FIB_EXT_DIALOG = new Dimension(415, 555);
    final static int CMB_HEIGHT = 22;
    final static int LBL_HEIGHT = 20;
    final static int GAP = 1;
    final static int LEFT = 5;
    final static int VADJ = 8;
    final static String METHOD_SET_COLOR = "setColor";
    final static String METHOD_WARNING_COLOR = "setWarningColor";
    final static String METHOD_SIGNAL_COLOR = "setSignalColor";
    final static String METHOD_SET_FONT_COLOR = "setFontColor";
    final static String METHOD_SET_FILL_COLOR = "setFillColor";
    final static String METHOD_SET_IS_FILLING = "isFilling";
    final static String METHOD_SET_STYLE = "setStyle";
    final static String METHOD_SET_PEN_STYLE = "secreateVLineDataSettingsPaneltPenStyle";
    final static String METHOD_SET_SIGNAL_STYLE = "setSignalStyle";
    final static String METHOD_SET_WIDTH = "setPenWidth";
    public static String COLOR_SEP = ",";
    static String[] saPriority = {Language.getString("OPEN"),
            Language.getString("HIGH"), Language.getString("LOW"),
            Language.getString("CLOSE")
    };
    static ImageIcon icon = new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif");
    static Color[] colors = {Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY,
            Color.GRAY, Color.LIGHT_GRAY, Color.GREEN, Color.MAGENTA, Color.ORANGE, Color.PINK, Color.RED,
            Color.WHITE, Color.YELLOW};
    static String[] colorNames = extractColorNames();
    //static String[] lineStyles = {"Solid", "Dot", "Dash", "DashDot", "DashDotDot"};
    static String[] lineStyles = {Language.getString("CS_SOLID"), Language.getString("CS_DOT"), Language.getString("CS_DASH")
            , Language.getString("CS_DASH_DOT"), Language.getString("CS_DASH_DOT_DOT")};
    static Icon[] penStyles = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_solid.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dot.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash_dot.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash_dot_dot.gif")
    };
    static Icon[] penWidths = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_0p5.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_1p0.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_1p5.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_2p0.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_3p0.gif")
    };

    static String[] lineWidths = {"1.0", "1.5", "2.0", "3.0", "5.0"};

    public static TWTabbedPane createDefaultTabbedPane(final ChartProperties origin, final ChartProperties target,
                                                       final StockGraph graph) {
        TWTabbedPane tabbedPane = null;
        tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //color/style panel (GENERAL)
        JPanel panel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), panel,
                Language.getString("SELECT_COLORS_STYLES"));
        tabbedPane.setSelectedIndex(0);

        return tabbedPane;
    }

    public static TWTabbedPane createTabbedPane(byte Indicator_ID, ChartProperties origin,
                                                ChartProperties target, int tabIndex, StockGraph graph, boolean showSources) {
        switch (Indicator_ID) {
            case GraphDataManager.ID_MOVING_AVERAGE:
                return createMovingAveragePanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_MACD:
                return createMACDPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_RSI:
                return createRSIPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_MOMENTUM:
                return createMomentumPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_LRI:
                return createLRIPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_SDI:
                return createSDIPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_BOLLINGER_BANDS:
                return createBollingerPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_ACCU_DISTRI:
                return createAccDistriPanes(origin, target, graph);
            case GraphDataManager.ID_WILLS_R:
                return createWillsRPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_STOCHAST_OSCILL:
                return createStocasicOscillPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_AVG_TRUE_RANGE:
                return createAvgTrueRangePanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_MASS_INDEX:
                return createMassIndexPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_MEDIAN_PRICE:
            case GraphDataManager.ID_ALLIGATOR:
                return createMedianPricePanes(origin, target, graph);
            case GraphDataManager.ID_MONEY_FLOW_INDEX:
                return createMoneyFlowIndexPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_TRIX:
                return createTrixPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_VOLATILITY:
                return createVolatilityPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_ZIGZAG:
                return createZigZagPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_AROON:
                return createAroonPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_CHAIKINS_OSCILL:
                return createChakinOscillatorPanes(origin, target, graph);
            case GraphDataManager.ID_CCI:
                return createCCIPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_CHAIKIN_MONEY_FLOW:
                return createChaikinMoneyFlowPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_EASE_OF_MOVEMENT:
                return createEaseOfMovementPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_ICHIMOKU_KINKO_HYO:
                return createIchimokuKinkoHyoPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_KLINGER_OSCILL:
                return createKlingerOscillatorPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_PARABOLIC_SAR:
                return createParabolicSARPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_PRICE_VOLUME_TREND:
                return createPriceVolumeTrendPanes(origin, target, graph);
            case GraphDataManager.ID_VOLUME_OSCILL:
                return createVolumeOscillatorPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_WILLS_ACCU_DISTRI:
                return createWillsAccumDistribPanes(origin, target, graph);
            case GraphDataManager.ID_RMI:
                return createRMIPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_ENVILOPES:
                return createEnvelopePanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_PRICE_OSCILL:
                return createPriceOscillatorPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_INTRADAY_MOMENTUM_INDEX:
                return createIMIPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_ON_BALANCE_VOLUME:
                return createOBVPane(origin, target, graph);
            case GraphDataManager.ID_ROC:
                return createROCPanes(origin, target, tabIndex, graph);//
            case GraphDataManager.ID_WILDERS_SMOOTHING:
                return createWildersSmoothingPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_PLUS_DI:
            case GraphDataManager.ID_MINUS_DI:
            case GraphDataManager.ID_DX:
            case GraphDataManager.ID_ADX:
            case GraphDataManager.ID_ADXR:
                return createDMIPanes(origin, target, tabIndex, graph, Indicator_ID, showSources);
            case GraphDataManager.ID_DPO:
                return createDPIPanes(origin, target, tabIndex, graph);//
            case GraphDataManager.ID_SWING_INDEX:
                return createSwingIndexPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_RVI:
                return createRVIPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_QSTICK:
                return createQstickPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_VERT_HORI_FILTER:
                return createVertHorizFilterPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_VOLUME_ROC:
                return createVolumeROCPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_DEMA:
                return createDEMAPanes(origin, target, tabIndex, graph, showSources);
            // added by Mevan
            case GraphDataManager.ID_COPP_CURVE:
                return createCoppockCurvePanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_PRICE_CHANNELS:
                return createPriceChannelPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_TSF:
                return createTSFPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_FO:
                return createFOPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_CMO:
                return createCMOPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_SMO:
                return createSMOPanes(origin, target, tabIndex, graph); //TODO
            case GraphDataManager.ID_TEMA:
                return createTEMAPanes(origin, target, tabIndex, graph, showSources);
            case GraphDataManager.ID_STD_ERR:
                return createStdErrPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_MACD_VOLUME:
                return createMACDVolumePanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_NET_ORDERS:
                return createNetOrderPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_NET_TURNOVER:
                return createNetTurnOverPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_DISPARITY_INDEX:
                return createDisparityIdxPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_HULL_MOVING_AVG:
                return createHullMovingAveragePanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_KELTNER_CHANNELS:
                return createKeltnerChannelPanes(origin, target, tabIndex, graph);
            case GraphDataManager.ID_CUSTOM_INDICATOR:
                return createCIPanes(origin, target, graph);
            default:
                return createDefaultTabbedPane(origin, target, graph);
        }
    }

    private static TWTabbedPane createCIPanes(final ChartProperties origin, final ChartProperties target,
                                              final StockGraph graph) {
        return new CustomIndicatorPropertiesPane(origin, target, graph);
    }

    private static TWTabbedPane createStdErrPanes(final ChartProperties origin, final ChartProperties target,
                                                  int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((StandardError) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((StandardError) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        GUISettings.applyOrientation(tabbedPane);
        return tabbedPane;
    }

    private static TWTabbedPane createTEMAPanes(final ChartProperties origin, final ChartProperties target,
                                                int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((TEMA) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((TEMA) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createSMOPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"52%"}, new String[]{"18", "20", "18", "20", "18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((StochasticMomentumIndex) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((StochasticMomentumIndex) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblSmoothingTimePeriods = new GraphLabel(Language.getString("SMOOTHING_PERIODS"));
        SpinnerNumberModel model1 = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinSmoothingTimePeriods = new JSpinner(model1);
        ((SpinnerNumberModel) spinSmoothingTimePeriods.getModel()).setValue(
                new Integer(((StochasticMomentumIndex) target).getSmoothingPeriod()));
        spinSmoothingTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinSmoothingTimePeriods.getModel()).getNumber().intValue();
                    ((StochasticMomentumIndex) target).setSmoothingPeriod(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblDoubleSmoothingTimePeriods = new GraphLabel(Language.getString("DOUBLE_SMOOTHING_PERIODS"));
        SpinnerNumberModel model2 = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinDoubleSmoothingTimePeriods = new JSpinner(model2);
        ((SpinnerNumberModel) spinDoubleSmoothingTimePeriods.getModel()).setValue(
                new Integer(((StochasticMomentumIndex) target).getDoubleSmoothingPeriod()));
        spinDoubleSmoothingTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinDoubleSmoothingTimePeriods.getModel()).getNumber().intValue();
                    ((StochasticMomentumIndex) target).setDoubleSmoothingPeriod(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);
        paramPanel.add(lblSmoothingTimePeriods);
        paramPanel.add(spinSmoothingTimePeriods);
        paramPanel.add(lblDoubleSmoothingTimePeriods);
        paramPanel.add(spinDoubleSmoothingTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }


    private static TWTabbedPane createCMOPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((ChandeMomentumOscillator) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((ChandeMomentumOscillator) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createFOPanes(final ChartProperties origin, final ChartProperties target,
                                              int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((ForcastOscillator) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((ForcastOscillator) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createTSFPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((TimeSeriesForcast) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((TimeSeriesForcast) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createPriceChannelPanes(final ChartProperties origin, final ChartProperties target,
                                                        int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "5%", "45%"}, new String[]{"100%"}, 0, 5));
        paramPanel.setSize(DIM_DIALOG);
        final JComboBox cmbFillColor = getColorCombo(origin, target, graph, METHOD_SET_FILL_COLOR,
                ((PriceChannel) target).getFillColor(), LEFT + 130, 3 * GAP + CMB_HEIGHT, 60, CMB_HEIGHT);

        final JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 150, ((PriceChannel) target).getTransparency());
        slider.setEnabled(true);
        cmbFillColor.setEnabled(true);
        final JCheckBox cbFillBand = new JCheckBox(Language.getString("FILL_BAND"));
        cbFillBand.setFont(Theme.getDefaultFont());
        cbFillBand.setSelected(((PriceChannel) target).isFillBands());
        cbFillBand.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (cbFillBand.isSelected()) {
                    slider.setEnabled(true);
                    cmbFillColor.setEnabled(true);
                    ((PriceChannel) target).setFillBands(true);
                    dynamicChangeChartProperties(origin, target, graph);
                } else {
                    slider.setEnabled(false);
                    cmbFillColor.setEnabled(false);
                    ((PriceChannel) target).setFillBands(false);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });


        GraphLabel lblFillColor = new GraphLabel(Language.getString("FILL_COLOR"));
        GraphLabel lblTransparency = new GraphLabel(Language.getString("TRANSPARENCY"));
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                int val = (int) source.getValue();
                ((PriceChannel) target).setTransparency(val);
                dynamicChangeChartProperties(origin, target, graph);
            }
        });
        slider.setMajorTickSpacing(10);
        slider.setMinorTickSpacing(1);
        slider.setPaintTicks(false);

        JPanel rightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "30"}, 10, 5));
        rightPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("BAND_FILL"), TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        rightPanel.add(cbFillBand);
        rightPanel.add(lblFillColor);
        rightPanel.add(cmbFillColor);
        rightPanel.add(lblTransparency);
        rightPanel.add(slider);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((PriceChannel) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((PriceChannel) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        JPanel leftPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20"}, 10, 5));
        leftPanel.add(lblTimePeriods);
        leftPanel.add(spinTimePeriods);

        paramPanel.add(leftPanel);
        paramPanel.add(new JLabel());
        if ((((PriceChannel) target).getBandType() == PriceChannel.PC_UPPER) || (((PriceChannel) target).getBandType() == PriceChannel.PC_LOWER)) {
            paramPanel.add(rightPanel);
        }

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;

    }

    private static TWTabbedPane createDEMAPanes(final ChartProperties origin, final ChartProperties target,
                                                int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((DEMA) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((DEMA) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createCoppockCurvePanes(final ChartProperties origin, final ChartProperties target,
                                                        int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblDataPoints = new GraphLabel(Language.getString("DATA_POINTS"));

        JComboBox cmbDataPeriod = new JComboBox();
        cmbDataPeriod.setEditable(false);
        //cmbDataPeriod.setFont(StockGraph.font_BOLD_10);
        String[] sa = CoppockCurve.getPeriodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbDataPeriod.addItem(sa[i]);
            if (((CoppockCurve) target).getPeriod() == i) {
                cmbDataPeriod.setSelectedIndex(i);
            }
        }
        cmbDataPeriod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((CoppockCurve) target).setPeriod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });


        paramPanel.add(lblDataPoints);
        paramPanel.add(cmbDataPeriod);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createVolumeROCPanes(final ChartProperties origin, final ChartProperties target,
                                                     int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((VolumeROC) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((VolumeROC) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createVertHorizFilterPanes(final ChartProperties origin, final ChartProperties target,
                                                           int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((VerticalHorizontalFilter) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((VerticalHorizontalFilter) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createQstickPanes(final ChartProperties origin, final ChartProperties target,
                                                  int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((Qstick) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((Qstick) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createSwingIndexPanes(final ChartProperties origin, final ChartProperties target,
                                                      int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("LIMIT_MOVE"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((SwingIndex) target).getLimitMove()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((SwingIndex) target).setLimitMove(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createRVIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((RelativeVolatilityIndex) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((RelativeVolatilityIndex) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createDPIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(
                new Integer(((DetrendPriceOscillator) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((DetrendPriceOscillator) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    // Added - Pramoda (21/03/2006)
    private static TWTabbedPane createDMIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph, final byte indicator_id, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        switch (indicator_id) {
            case GraphDataManager.ID_PLUS_DI:
                ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((PlusDI) target).getTimePeriods()));
                break;
            case GraphDataManager.ID_MINUS_DI:
                ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((MinusDI) target).getTimePeriods()));
                break;
            case GraphDataManager.ID_DX:
                ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((DX) target).getTimePeriods()));
                break;
            case GraphDataManager.ID_ADX:
                ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((ADX) target).getTimePeriods()));
                break;
            case GraphDataManager.ID_ADXR:
                ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((ADXR) target).getTimePeriods()));
                break;
        }
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    switch (indicator_id) {
                        case GraphDataManager.ID_PLUS_DI:
                            ((PlusDI) target).setTimePeriods(val);
                            break;
                        case GraphDataManager.ID_MINUS_DI:
                            ((MinusDI) target).setTimePeriods(val);
                            break;
                        case GraphDataManager.ID_DX:
                            ((DX) target).setTimePeriods(val);
                            break;
                        case GraphDataManager.ID_ADX:
                            ((ADX) target).setTimePeriods(val);
                            break;
                        case GraphDataManager.ID_ADXR:
                            ((ADXR) target).setTimePeriods(val);
                            break;
                    }
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createDestinationPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    public static TWTabbedPane createTabbedPane(AbstractObject origin, AbstractObject target, StockGraph graph) {
        TWTabbedPane tabbedPane = null;
        tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        if (target.getObjType() == AbstractObject.INT_SYMBOL) {
            JPanel txtPnl = createObjectSymbolPanel((ObjectSymbol) origin, (ObjectSymbol) target, graph);
            tabbedPane.addTab(Language.getString("SYMBOL"), txtPnl,
                    Language.getString("CHANGE_COLOR_n_SIZE"));

        } else if (target.getObjType() == AbstractObject.INT_FIBONACCI_RETRACEMENTS) {
            JPanel customLinesTab = createFibonacciRetracLinesTab(origin, target, graph);
            tabbedPane.addTab(Language.getString("SETTINGS"), customLinesTab,
                    Language.getString("SET_FIBONACCI_PROPERTIES"));
        } else if (target.getObjType() == AbstractObject.INT_FIBONACCI_EXTENSIONS) {
            JPanel customLinesTab = createFibonacciExtensionsTab(origin, target, graph);
            tabbedPane.addTab(Language.getString("SETTINGS"), customLinesTab,
                    Language.getString("SET_FIBONACCI_PROPERTIES"));
        } else if (target.getObjType() == AbstractObject.INT_LINE_HORIZ) {
            JPanel panel = createObjectGeneralPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("GENERAL"), panel,
                    Language.getString("SELECT_COLORS_AND_STYLES"));

            //aded by charithn
            JPanel panelData = createObjectDataSettingsPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("DATA"), panelData,
                    Language.getString("DATA_SETTINGS"));
            GUISettings.applyOrientation(tabbedPane);//TODO: need to complete for all the objects

        } else if (target.getObjType() == AbstractObject.INT_LINE_VERTI) {
            JPanel panel = createObjectGeneralPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("GENERAL"), panel,
                    Language.getString("SELECT_COLORS_AND_STYLES"));

            //aded by charithn
            JPanel panelData = createVLineDataSettingsPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("DATA"), panelData,
                    Language.getString("DATA_SETTINGS"));
        } else if (target.getObjType() == AbstractObject.INT_ANDREWS_PITCHFORK) {
            JPanel panel = createObjectGeneralPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("GENERAL"), panel,
                    Language.getString("SELECT_COLORS_AND_STYLES"));

            //added by Mevan @ 20 August 2009
            JPanel panelData = createAndrewPitchforkDataSettingsPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("PARAMETERS"), panelData,
                    Language.getString("SET_PARAMETERS"));
        } else if (target.getObjType() == AbstractObject.INT_CYCLE_LINE) {
            // added by Mevan @ 30 June 2009
            JPanel panel = createObjectGeneralPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("GENERAL"), panel,
                    Language.getString("SELECT_COLORS_AND_STYLES"));

            JPanel panelData = createCycleLineSettingsPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("GRAPH_CYCLE_TAB_MODE"), panelData,
                    Language.getString("DATA_SETTINGS"));

        } else if ((target.getObjType() == AbstractObject.INT_ELLIPSE) || (target.getObjType() == AbstractObject.INT_RECT)) {
            JPanel rectPnl = createObjectRectPanel((LineStudy) origin, (LineStudy) target, graph);
            tabbedPane.addTab(Language.getString("GENERAL"), rectPnl,
                    Language.getString("SELECT_COLORS_AND_STYLES"));
        } else {
            JPanel panel = createObjectGeneralPanel(origin, target, graph);
            tabbedPane.addTab(Language.getString("GENERAL"), panel,
                    Language.getString("SELECT_COLORS_AND_STYLES"));
        }


        if (target.getObjType() == AbstractObject.INT_TEXT) {
            JPanel txtPnl = createObjectTextPanel((ObjectTextNote) origin, (ObjectTextNote) target, graph);
            tabbedPane.addTab(Language.getString("TEXT"), txtPnl,
                    Language.getString("EDIT_TEXT_PROPERTIES"));
            tabbedPane.setSelectedIndex(0);
        } else if (target.getObjType() == AbstractObject.INT_REGRESSION) {
            JPanel extensionsTab = createExtensionsTab(origin, target, graph);
            tabbedPane.addTab(Language.getString("PARAMETERS"), extensionsTab,
                    Language.getString("SET_PARAMETERS"));
            tabbedPane.setSelectedIndex(0);
        } else if (target.getObjType() == AbstractObject.INT_LINE_SLOPE) {
            JPanel extensionsTab = createLineSlopeExtensionsTab(origin, target, graph);
            tabbedPane.addTab(Language.getString("PARAMETERS"), extensionsTab,
                    Language.getString("GRAPH_TREND_TOOL_TIP"));
            //alert tab-shashika
            JPanel alarmTab = createAlarmTab(origin, target, graph);
            tabbedPane.addTab(Language.getString("TREND_LINE_ALERT"), alarmTab, Language.getString("TREND_LINE_ALERT_TOOLTIP"));
            tabbedPane.setSelectedIndex(0);
        } else if (target.getObjType() == AbstractObject.INT_ARROW_LINE_SLOPE) { //TODO : ALS create a separate property dialog if need ? 02-02-2009
            JPanel extensionsTab = createArrowLineSlopeExtensionsTab(origin, target, graph);
            tabbedPane.addTab(Language.getString("PARAMETERS"), extensionsTab,
                    Language.getString("GRAPH_TREND_TOOL_TIP"));
            //alert tab-shashika
            JPanel alarmTab = createAlarmTab(origin, target, graph);
            tabbedPane.addTab(Language.getString("TREND_LINE_ALERT"), alarmTab, Language.getString("TREND_LINE_ALERT_TOOLTIP"));
            tabbedPane.setSelectedIndex(0);
        } else if (target.getObjType() == AbstractObject.INT_ZIG_ZAG) {
            //alert tab-shashika
            JPanel alarmTab = createAlarmTab(origin, target, graph);
            tabbedPane.addTab(Language.getString("TREND_LINE_ALERT"), alarmTab, Language.getString("TREND_LINE_ALERT_TOOLTIP"));
            tabbedPane.setSelectedIndex(0);
        } else if (target.getObjType() == AbstractObject.INT_STD_ERROR_CHANNEL) { // Pramoda
            JPanel parametersTab = createStdErrParametersTab((ObjectStandardError) origin, (ObjectStandardError) target,
                    graph);
            tabbedPane.addTab(Language.getString("PARAMETERS"), parametersTab,
                    Language.getString("SET_PARAMETERS"));
            tabbedPane.setSelectedIndex(0);
        } else if (target.getObjType() == AbstractObject.INT_RAFF_REGRESSION) {
            JPanel parametersTab = createRaffRegParametersTab((ObjectRaffRegression) origin, (ObjectRaffRegression) target,
                    graph);
            tabbedPane.addTab(Language.getString("PARAMETERS"), parametersTab,
                    Language.getString("SET_PARAMETERS"));
            tabbedPane.setSelectedIndex(0);
        } else if (target.getObjType() == AbstractObject.INT_STD_DEV_CHANNEL) {
            JPanel parametersTab = createStdDevParametersTab((ObjectStandardDeviationChannel) origin,
                    (ObjectStandardDeviationChannel) target, graph);
            tabbedPane.addTab(Language.getString("PARAMETERS"), parametersTab,
                    Language.getString("SET_PARAMETERS"));
            tabbedPane.setSelectedIndex(0);
        } else {
            tabbedPane.setSelectedIndex(0);
        }

        return tabbedPane;
    }

    private static JPanel createStdDevParametersTab(final ObjectStandardDeviationChannel origin,
                                                    final ObjectStandardDeviationChannel target, final StockGraph graph) {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%",}, new String[]{"40", "20"}, 10, 5));
        JPanel panelTop = new JPanel(new FlexGridLayout(new String[]{"35%", "10%"}, new String[]{"20", "20"}, 0, 0));
        JPanel panelBottom = new JPanel(new FlexGridLayout(new String[]{"28%", "13%"}, new String[]{"20"}, 0, 0));
        panel.setSize(DIM_DIALOG);

        GraphLabel lblExtendLeft = new GraphLabel(Language.getString("EXTEND_LEFT"));
        //lblExtendLeft.setBounds(LEFT, 2, 100, LBL_HEIGHT);
        final JCheckBox cbExtendLeft = new JCheckBox();
        //cbExtendLeft.setBounds(LEFT + 100, 2, 100, LBL_HEIGHT);
        cbExtendLeft.setSelected(target.isExtendedLeft());
        cbExtendLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendLeft.isSelected();
                target.setExtendedLeft(checked);
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        GraphLabel lblExtendRight = new GraphLabel(Language.getString("EXTEND_RIGHT"));
        //lblExtendRight.setBounds(LEFT, 4 + LBL_HEIGHT, 100, LBL_HEIGHT);
        final JCheckBox cbExtendRight = new JCheckBox();
        //cbExtendRight.setBounds(LEFT + 100, 4 + LBL_HEIGHT, 100, LBL_HEIGHT);
        cbExtendRight.setSelected(target.isExtendedRight());
        cbExtendRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendRight.isSelected();
                target.setExtendedRight(checked);
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        GraphLabel lblUnits = new GraphLabel(Language.getString("UNITS"));
        //lblUnits.setBounds(LEFT, 6 + LBL_HEIGHT * 2, 100, LBL_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(2, 1, 100, 1);
        final JSpinner spinUnits = new JSpinner(model);
        //spinUnits.setBounds(LEFT + 100, 6 + LBL_HEIGHT * 2, 50, LBL_HEIGHT);
        spinUnits.getModel().setValue(target.getUnits());
        spinUnits.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinUnits.getModel()).getNumber().intValue();
                    target.setUnits(val);
                    dynamicChangeObjectProperties(origin, target, graph);
                }
            }
        });

        panelTop.add(lblExtendLeft);
        panelTop.add(cbExtendLeft);
        panelTop.add(lblExtendRight);
        panelTop.add(cbExtendRight);
        panelBottom.add(lblUnits);
        panelBottom.add(spinUnits);
        panel.add(panelTop);
        panel.add(panelBottom);

        return panel;
    }

    private static JPanel createRaffRegParametersTab(final ObjectRaffRegression origin,
                                                     final ObjectRaffRegression target, final StockGraph graph) {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"30%", "10%", "65%",}, new String[]{"20", "20"}, 10, 5));
        panel.setSize(DIM_DIALOG);
        //panel.setLayout(null);

        GraphLabel lblExtendLeft = new GraphLabel(Language.getString("EXTEND_LEFT"));
        //lblExtendLeft.setBounds(LEFT, 2, 100, LBL_HEIGHT);
        final JCheckBox cbExtendLeft = new JCheckBox();
        //cbExtendLeft.setBounds(LEFT + 100, 2, 100, LBL_HEIGHT);
        cbExtendLeft.setSelected(target.isExtendedLeft());
        cbExtendLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendLeft.isSelected();
                target.setExtendedLeft(checked);
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        GraphLabel lblExtendRight = new GraphLabel(Language.getString("EXTEND_RIGHT"));
        //lblExtendRight.setBounds(LEFT, 4 + LBL_HEIGHT, 100, LBL_HEIGHT);
        final JCheckBox cbExtendRight = new JCheckBox();
        //cbExtendRight.setBounds(LEFT + 100, 4 + LBL_HEIGHT, 100, LBL_HEIGHT);
        cbExtendRight.setSelected(target.isExtendedRight());
        cbExtendRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendRight.isSelected();
                target.setExtendedRight(checked);
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        panel.add(lblExtendLeft);
        panel.add(cbExtendLeft);
        panel.add(new JLabel(""));
        panel.add(lblExtendRight);
        panel.add(cbExtendRight);

        return panel;
    }

    //added by charithn
    private static JPanel createFibonacciRetracLinesTab(final AbstractObject origin, final Object target, final StockGraph graph) {

        JPanel pnlAll = new JPanel();

        int hGap = 5, vGap = 5;

        pnlAll.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"79%", "5%", "5%", "10%"}, hGap, 0));

        JPanel panelUp = new JPanel();
        panelUp.setSize(DIM_FIB_RETR_DIALOG);

        String[] widths = new String[]{"20", "65", "20", "85", "85", "85"};
        String[] heights = new String[]{"18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18"};

        FlexGridLayout flexlayout = new FlexGridLayout(widths, heights, hGap, vGap);
        panelUp.setLayout(flexlayout);

        //contains header values
        JLabel lblPercentageHeader = new JLabel(Language.getString("PERCENTAGE"), JLabel.CENTER);
        JLabel lblColorHeader = new JLabel(Language.getString("FB_LINE_COLOR"), JLabel.CENTER);
        JLabel lblStyleHeader = new JLabel(Language.getString("FB_LINE_STYLE"), JLabel.CENTER);
        JLabel lblThicknessHeader = new JLabel(Language.getString("LINE_THICKNESS"), JLabel.CENTER);

        //add components to up panel
        panelUp.add(new JLabel(""));
        panelUp.add(lblPercentageHeader);
        panelUp.add(new JLabel(""));
        panelUp.add(lblColorHeader);
        panelUp.add(lblStyleHeader);
        panelUp.add(lblThicknessHeader);

        final JCheckBox chkSameProperties = new JCheckBox(Language.getString("FIB_USE_SAME"));
        chkSameProperties.setSelected(((ObjectFibonacciRetracements) origin).isUsingSameProperties());

        //get the current fibonnacci lines retuenrs from the ObjectFibonacciRetracements class
        final ObjectFibonacciRetracements fibonacciRetracements = (ObjectFibonacciRetracements) target;
        final Hashtable hashRowInfo = fibonacciRetracements.getFibonacciLines();

        Integer[] keys = (Integer[]) hashRowInfo.keySet().toArray(new Integer[0]);
        Arrays.sort(keys);

        final Hashtable<Integer, FibonacciRowGUI> hashRowUI = new Hashtable<Integer, FibonacciRowGUI>();

        for (final int key : keys) {

            final FibonacciRetracementiLine line = (FibonacciRetracementiLine) hashRowInfo.get(key);

            //append ui components row by row
            final JCheckBox chkEnable = new JCheckBox("", line.isEnabled());
            final JTextField txtPercentage = new JTextField();
            final CustomColorLabel lblColor = new CustomColorLabel(line.getLineColor());
            final JComboBox cmbLineStyle = UIHelper.getPenStyleCombo();
            final JComboBox cmbLineWidth = UIHelper.getPenWidthCombo();


            //set lins style combobox values
            for (int i = 0; i < cmbLineStyle.getItemCount(); i++) {
                if (i == (int) line.getPenStyle()) {
                    cmbLineStyle.setSelectedIndex(i);
                    break;
                }
            }

            //set lins width combobox values
            int equiW1 = getEquivalentWidth(line.getPenWidth());
            for (int i = 0; i < cmbLineWidth.getItemCount(); i++) {
                Object obj = ((ComboItem) cmbLineWidth.getItemAt(i)).getValue();
                float width = Float.parseFloat((String) obj);
                int equiW2 = getEquivalentWidth(width);
                if (equiW1 == equiW2) {
                    cmbLineWidth.setSelectedIndex(i);
                    break;
                }
            }

            //set check box values
            chkEnable.setSelected(line.isEnabled());

            //disable the trendline, 0% and 100% check boxes
            if ((line.getPercentageValue() == 0 || line.getPercentageValue() == 1 || Double.compare(line.getPercentageValue(), Double.POSITIVE_INFINITY) == 0)) {
                chkEnable.setEnabled(false);
            }

            double percentage = line.getPercentageValue();
            percentage = (percentage * 1000) / 10; //for roundup purposes
            String percent = String.valueOf(percentage);

            //detect for the trnd line
            if (Double.compare(percentage, Double.POSITIVE_INFINITY) == 0) {
                percent = Language.getString("TREND_LINE");
                txtPercentage.setHorizontalAlignment(JTextField.LEADING);
                txtPercentage.setEnabled(false);

            } else {
                txtPercentage.setHorizontalAlignment(JTextField.RIGHT);
                txtPercentage.setEnabled(true);
            }
            //detect for the 0% and 100% line
            if ((Double.compare(percentage, 0.0) == 0) || (Double.compare(percentage, 100.0) == 0)) {
                txtPercentage.setHorizontalAlignment(JTextField.RIGHT);
                txtPercentage.setEnabled(false);
            }
            //set the percentage value
            txtPercentage.setText(percent);

            if (!(line.getPercentageValue() == 0 || line.getPercentageValue() == 1 || Double.compare(line.getPercentageValue(), Double.POSITIVE_INFINITY) == 0)) {
                txtPercentage.setEnabled(line.isEnabled());
            }

            //enable color,style and width values
            lblColor.setEnabled(line.isEnabled());
            cmbLineStyle.setEnabled(line.isEnabled());
            cmbLineWidth.setEnabled(line.isEnabled());

            lblColor.setEnabled(!chkSameProperties.isSelected());
            cmbLineStyle.setEnabled(!chkSameProperties.isSelected());
            cmbLineWidth.setEnabled(!chkSameProperties.isSelected());

            //construct the hash table which hols the ui components with its properties
            FibonacciRowGUI rowUI = new FibonacciRowGUI(chkEnable, txtPercentage, lblColor, cmbLineStyle, cmbLineWidth);
            hashRowUI.put(key, rowUI);

            //add action listeners  4 components in each row.
            chkEnable.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    if (!(line.getPercentageValue() == 0 || line.getPercentageValue() == 1 || Double.compare(line.getPercentageValue(), Double.POSITIVE_INFINITY) == 0)) {
                        txtPercentage.setEnabled(chkEnable.isSelected());
                        lblColor.setEnabled(chkEnable.isSelected());
                        cmbLineStyle.setEnabled(chkEnable.isSelected());
                        cmbLineWidth.setEnabled(chkEnable.isSelected());
                        line.setEnabled(chkEnable.isSelected());
                    }
                    if (chkSameProperties.isSelected()) {
                        lblColor.setEnabled(false);
                        cmbLineStyle.setEnabled(false);
                        cmbLineWidth.setEnabled(false);
                    }
                    line.setEnabled(chkEnable.isSelected());
                    txtPercentage.setEnabled(chkEnable.isSelected());
                    fibonacciRetracements.setFibonacciLines(hashRowInfo);
                    dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    //ComponentRegistry.fireUpdate();
                }
            });

            cmbLineStyle.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    if (target instanceof ObjectFibonacciRetracements) {
                        byte id = (byte) cmbLineStyle.getSelectedIndex();
                        line.setPenStyle(id);
                        fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    }
                }
            });

            cmbLineWidth.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    if (target instanceof ObjectFibonacciRetracements) {
                        Object obj = ((ComboItem) cmbLineWidth.getSelectedItem()).getValue();
                        float width = Float.parseFloat((String) obj);
                        line.setPenWidth(width);
                        fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    }
                }
            });

            lblColor.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {

                    if (target instanceof ObjectFibonacciRetracements && !chkSameProperties.isSelected()) {
                        Color pickColor = lblColor.getBackground();
                        line.setLineColor(pickColor);
                        fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    }
                }

                public void mousePressed(MouseEvent e) {
                    //System.out.println("mouse pressed");
                }

                public void mouseReleased(MouseEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void mouseEntered(MouseEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void mouseExited(MouseEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }
            });

            txtPercentage.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    try {
                        double value = Double.parseDouble(txtPercentage.getText().trim());
                        value = (value * 10) / 1000;  //round up purposes
                        line.setPercentageValue(value);
                        fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        double val = (line.getPercentageValue() * 1000) / 10;
                        txtPercentage.setText("");
                        //txtPercentage.setText(String.valueOf(val));
                        //fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        //dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    }
                }
            });

            panelUp.add(chkEnable);
            panelUp.add(txtPercentage);
            if (Double.compare(line.getPercentageValue(), Double.POSITIVE_INFINITY) == 0) {
                panelUp.add(new JLabel(""));   // % sign not required for the trend line
            } else {
                panelUp.add(new JLabel("%"));
            }

            panelUp.add(lblColor);
            panelUp.add(cmbLineStyle);
            panelUp.add(cmbLineWidth);
        }
        pnlAll.add(panelUp);

        JPanel pnlSecond = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18"}, 5, 5));

        final CustomColorLabel lblAll = new CustomColorLabel(Color.RED);
        final JComboBox cmbStyleAll = UIHelper.getPenStyleCombo();
        final JComboBox cmbWidthAll = UIHelper.getPenWidthCombo();

        lblAll.setEnabled(chkSameProperties.isSelected());
        cmbStyleAll.setEnabled(chkSameProperties.isSelected());
        cmbWidthAll.setEnabled(chkSameProperties.isSelected());

        FibonacciRowGUI row = hashRowUI.get(0);
        lblAll.setBackground(row.getLblColor().getBackground());
        cmbStyleAll.setSelectedIndex(row.getCmbLineStyle().getSelectedIndex());
        cmbWidthAll.setSelectedIndex(row.getCmLineWidth().getSelectedIndex());

        chkSameProperties.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                ((ObjectFibonacciRetracements) target).setUsingSameProperties(chkSameProperties.isSelected());
                //lblColor.
                lblAll.setEnabled(chkSameProperties.isSelected());
                cmbStyleAll.setEnabled(chkSameProperties.isSelected());
                cmbWidthAll.setEnabled(chkSameProperties.isSelected());

                Hashtable temp = hashRowInfo;
                if (chkSameProperties.isSelected()) {
                    int size = hashRowUI.size();
                    for (int i = 0; i < size; i++) {
                        FibonacciRowGUI rowUI = hashRowUI.get(i);
                        JComboBox cmbStyle = rowUI.getCmbLineStyle();
                        JComboBox cmbWidth = rowUI.getCmLineWidth();
                        CustomColorLabel lblCol = rowUI.getLblColor();

                        cmbStyle.setEnabled(false);
                        cmbWidth.setEnabled(false);
                        lblCol.setEnabled(false);
                        //lblCol.setBackground(Color.DARK_GRAY);
                    }
                    int size2 = temp.size();
                    for (int i = 0; i < size2; i++) {

                        FibonacciRetracementiLine line = (FibonacciRetracementiLine) temp.get(i);

                        if (line.isEnabled()) { //apply only for the lines which are currently enabled.(check box enabled)
                            line.setLineColor(lblAll.getBackground());
                            line.setPenStyle((byte) cmbStyleAll.getSelectedIndex());

                            Object obj = ((ComboItem) cmbWidthAll.getSelectedItem()).getValue();
                            float width = Float.parseFloat((String) obj);
                            line.setPenWidth(width);
                        }
                    }
                    fibonacciRetracements.setFibonacciLines(temp);
                    dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    //ComponentRegistry.fireUpdate();

                } else {
                    int size = hashRowUI.size();
                    for (int i = 0; i < size; i++) {
                        FibonacciRowGUI rowUI = hashRowUI.get(i);
                        JComboBox cmbStyle = rowUI.getCmbLineStyle();
                        JComboBox cmbWidth = rowUI.getCmLineWidth();
                        CustomColorLabel lblCol = rowUI.getLblColor();

                        FibonacciRetracementiLine line = (FibonacciRetracementiLine) hashRowInfo.get(i);
                        line.setLineColor(lblCol.getBackground());
                        line.setPenStyle((byte) cmbStyle.getSelectedIndex());
                        Object obj = ((ComboItem) cmbWidth.getSelectedItem()).getValue();
                        float width = Float.parseFloat((String) obj);
                        line.setPenWidth(width);

                        cmbStyle.setEnabled(line.isEnabled());
                        cmbWidth.setEnabled(line.isEnabled());
                        lblCol.setEnabled(line.isEnabled());
                    }
                    fibonacciRetracements.setFibonacciLines(hashRowInfo);
                    dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    //ComponentRegistry.fireUpdate();
                }
            }
        });

        cmbStyleAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Hashtable temp = hashRowInfo;
                int size2 = temp.size();
                for (int i = 0; i < size2; i++) {

                    FibonacciRetracementiLine line = (FibonacciRetracementiLine) temp.get(i);

                    if (line.isEnabled()) { //apply only for the lines which are currently enabled.(check box enabled)
                        line.setLineColor(lblAll.getBackground());
                        line.setPenStyle((byte) cmbStyleAll.getSelectedIndex());
                        Object obj = ((ComboItem) cmbWidthAll.getSelectedItem()).getValue();
                        float width = Float.parseFloat((String) obj);
                        line.setPenWidth(width);
                    }
                }
                fibonacciRetracements.setFibonacciLines(temp);
                dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                //ComponentRegistry.fireUpdate();
            }
        });

        cmbWidthAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Hashtable temp = hashRowInfo;
                int size2 = temp.size();
                for (int i = 0; i < size2; i++) {

                    FibonacciRetracementiLine line = (FibonacciRetracementiLine) temp.get(i);

                    if (line.isEnabled()) { //apply only for the lines which are currently enabled.(check box enabled)
                        line.setLineColor(lblAll.getBackground());
                        line.setPenStyle((byte) cmbStyleAll.getSelectedIndex());
                        Object obj = ((ComboItem) cmbWidthAll.getSelectedItem()).getValue();
                        float width = Float.parseFloat((String) obj);
                        line.setPenWidth(width);
                    }
                }
                fibonacciRetracements.setFibonacciLines(temp);
                dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                //ComponentRegistry.fireUpdate();
            }
        });


        lblAll.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                if (chkSameProperties.isSelected()) { //done only when checkbox is enabled
                    int size2 = hashRowInfo.size();
                    for (int i = 0; i < size2; i++) {

                        FibonacciRetracementiLine line = (FibonacciRetracementiLine) hashRowInfo.get(i);

                        if (line.isEnabled()) { //apply only for the lines which are currently enabled.(check box enabled)
                            line.setLineColor(lblAll.getBackground());
                            line.setPenStyle((byte) cmbStyleAll.getSelectedIndex());
                            Object obj = ((ComboItem) cmbWidthAll.getSelectedItem()).getValue();
                            float width = Float.parseFloat((String) obj);
                            line.setPenWidth(width);
                        }
                    }
                    fibonacciRetracements.setFibonacciLines(hashRowInfo);
                    dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                }
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });


        pnlSecond.add(chkSameProperties);
        pnlAll.add(pnlSecond);

        JPanel pnlBottom = new JPanel();
        pnlBottom.setLayout(new FlexGridLayout(new String[]{"70", "20", "85", "85", "85"}, new String[]{"18"}, hGap, 2));

        final JCheckBox chkSetDefault = new JCheckBox(Language.getString("SET_AS_DEFAULT2"));

        ((AbstractObject) target).setUsingUserDefault(false);
        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);

        chkSetDefault.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ((AbstractObject) target).setUsingUserDefault(chkSetDefault.isSelected());
                dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
            }
        });


        final JCheckBox chkFreeStyle = new JCheckBox(Language.getString("GRAPH_FREE_STYLE"));

        //TODO: charitjh. if needs in future
        chkFreeStyle.setSelected(origin.isFreeStyle());
        //chkFreeStyle.setEnabled(!graph.GDMgr.isSnapToPrice());

        chkFreeStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ((AbstractObject) target).setFreeStyle(chkFreeStyle.isSelected());
                dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
            }
        });


        pnlBottom.add(new JLabel(""));
        pnlBottom.add(new JLabel(""));

        pnlBottom.add(lblAll);
        pnlBottom.add(cmbStyleAll);
        pnlBottom.add(cmbWidthAll);
        pnlAll.add(pnlBottom);
        JPanel pnlDef = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "10", "18"}, hGap, 0));
        pnlDef.add(chkFreeStyle);
        pnlDef.add(new JPanel());
        pnlDef.add(chkSetDefault);

        pnlAll.add(pnlDef);

        Integer[] UIkeys = (Integer[]) hashRowUI.keySet().toArray(new Integer[0]);
        Arrays.sort(UIkeys);

        GUISettings.applyOrientation(pnlAll);
        return pnlAll;
    }

    //added by charithn
    private static JPanel createFibonacciExtensionsTab(final AbstractObject origin, final Object target, final StockGraph graph) {

        double TEMP_VALUE = 9999999900D;
        JPanel pnlAll = new JPanel();

        int hGap = 5, vGap = 5;

        //pnlAll.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"80%", "8%", "6%", "6%"}, hGap, vGap));
        pnlAll.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"78%", "5%", "6%", "10%"}, hGap, 0));

        JPanel panelUp = new JPanel();
        panelUp.setSize(new Dimension(415, 590));

        String[] widths = new String[]{"20", "75", "15", "85", "85", "85"};
        String[] heights = new String[]{"18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18", "18"};

        FlexGridLayout flexlayout = new FlexGridLayout(widths, heights, hGap, vGap);
        panelUp.setLayout(flexlayout);

        //contains header values
        JLabel lblPercentageHeader = new JLabel(Language.getString("PERCENTAGE"), JLabel.CENTER);
        JLabel lblColorHeader = new JLabel(Language.getString("FB_LINE_COLOR"), JLabel.CENTER);
        JLabel lblStyleHeader = new JLabel(Language.getString("FB_LINE_STYLE"), JLabel.CENTER);
        JLabel lblThicknessHeader = new JLabel(Language.getString("LINE_THICKNESS"), JLabel.CENTER);

        //add components to up panel
        panelUp.add(new JLabel(""));
        panelUp.add(lblPercentageHeader);
        panelUp.add(new JLabel(""));
        panelUp.add(lblColorHeader);
        panelUp.add(lblStyleHeader);
        panelUp.add(lblThicknessHeader);

        final JCheckBox chkSameProperties = new JCheckBox(Language.getString("FIB_USE_SAME"));
        chkSameProperties.setSelected(((ObjectFibonacciExtensions) origin).isUsingSameProperties());

        //get the current fibonnacci lines retuenrs from the ObjectFibonacciRetracements class
        final ObjectFibonacciExtensions fibonacciRetracements = (ObjectFibonacciExtensions) target;
        final Hashtable hashRowInfo = fibonacciRetracements.getFibonacciLines();

        Integer[] keys = (Integer[]) hashRowInfo.keySet().toArray(new Integer[0]);
        Arrays.sort(keys);

        final Hashtable<Integer, FibonacciRowGUI> hashRowUI = new Hashtable<Integer, FibonacciRowGUI>();

        for (final int key : keys) {

            final FibonacciExtensionLine line = (FibonacciExtensionLine) hashRowInfo.get(key);

            //append ui components row by row
            final JCheckBox chkEnable = new JCheckBox("", line.isEnabled());
            final JTextField txtPercentage = new JTextField();
            final CustomColorLabel lblColor = new CustomColorLabel(line.getLineColor());
            final JComboBox cmbLineStyle = UIHelper.getPenStyleCombo();
            final JComboBox cmbLineWidth = UIHelper.getPenWidthCombo();

            //set lins style combobox values
            for (int i = 0; i < cmbLineStyle.getItemCount(); i++) {
                if (i == (int) line.getPenStyle()) {
                    cmbLineStyle.setSelectedIndex(i);
                    break;
                }
            }

            //set lins width combobox values
            int equiW1 = getEquivalentWidth(line.getPenWidth());
            for (int i = 0; i < cmbLineWidth.getItemCount(); i++) {
                Object obj = ((ComboItem) cmbLineWidth.getItemAt(i)).getValue();
                float width = Float.parseFloat((String) obj);
                int equiW2 = getEquivalentWidth(width);
                if (equiW1 == equiW2) {
                    cmbLineWidth.setSelectedIndex(i);
                    break;
                }
            }

            //set check box values
            chkEnable.setSelected(line.isEnabled());

            //disable the trendline, 0% and 100% check boxes
            if ((line.getPercentageValue() == 0 || line.getPercentageValue() == 1 || Double.compare(line.getPercentageValue(), Double.NEGATIVE_INFINITY) == 0) ||
                    Double.compare(line.getPercentageValue(), Double.POSITIVE_INFINITY) == 0 || Double.compare(line.getPercentageValue(), TEMP_VALUE / 100) == 0) {
                chkEnable.setEnabled(false);
            }

            double percentage = line.getPercentageValue();
            percentage = (percentage * 1000) / 10; //for roundup purposes
            String percent = "";

            //detect for the trnd line
            if (Double.compare(percentage, Double.POSITIVE_INFINITY) == 0) {
                percent = Language.getString("TREND_LINE");
                txtPercentage.setHorizontalAlignment(JTextField.LEADING);
                txtPercentage.setEnabled(false);
            }
            //detect for the retrt line
            else if (Double.compare(percentage, Double.NEGATIVE_INFINITY) == 0) {
                percent = Language.getString("GRAPH_RETRACEMENT");
                txtPercentage.setHorizontalAlignment(JTextField.LEADING);
                txtPercentage.setEnabled(false);
            }
            //detect for the base line
            else if (Double.compare(percentage, TEMP_VALUE) == 0) {
                percent = Language.getString("GRAPH_BASE_LINE");
                txtPercentage.setHorizontalAlignment(JTextField.LEADING);
                txtPercentage.setEnabled(false);

            } else {
                percent = String.valueOf(percentage);
                txtPercentage.setHorizontalAlignment(JTextField.RIGHT);
                txtPercentage.setEnabled(true);
            }

            //set the percentage value
            txtPercentage.setText(percent);

            //detect for the 0% and 100%, trend, retr and base line
            if ((Double.compare(percentage, 0.0) == 0) || (Double.compare(percentage, 100.0) == 0) ||
                    (Double.compare(percentage, Double.POSITIVE_INFINITY) == 0) || (Double.compare(percentage, Double.NEGATIVE_INFINITY) == 0) ||
                    (Double.compare(percentage, TEMP_VALUE) == 0)) {
                txtPercentage.setHorizontalAlignment(JTextField.RIGHT);
                txtPercentage.setEnabled(false);
            }

            if (!(line.getPercentageValue() == 0 || line.getPercentageValue() == 1 || Double.compare(line.getPercentageValue(), Double.POSITIVE_INFINITY) == 0
                    || Double.compare(line.getPercentageValue(), Double.NEGATIVE_INFINITY) == 0 || Double.compare(line.getPercentageValue(), TEMP_VALUE / 100) == 0)) {
                txtPercentage.setEnabled(line.isEnabled());
            }

            //enable color,style and width values
            lblColor.setEnabled(line.isEnabled());
            cmbLineStyle.setEnabled(line.isEnabled());
            cmbLineWidth.setEnabled(line.isEnabled());

            //construct the hash table which hols the ui components with its properties
            FibonacciRowGUI rowUI = new FibonacciRowGUI(chkEnable, txtPercentage, lblColor, cmbLineStyle, cmbLineWidth);
            hashRowUI.put(key, rowUI);

            //add action listeners  4 components in each row.
            chkEnable.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    if (!(line.getPercentageValue() == 0 || line.getPercentageValue() == 1 || Double.compare(line.getPercentageValue(), Double.POSITIVE_INFINITY) == 0)) {
                        txtPercentage.setEnabled(chkEnable.isSelected());
                        lblColor.setEnabled(chkEnable.isSelected());
                        cmbLineStyle.setEnabled(chkEnable.isSelected());
                        cmbLineWidth.setEnabled(chkEnable.isSelected());
                        line.setEnabled(chkEnable.isSelected());
                    }
                    if (chkSameProperties.isSelected()) {
                        lblColor.setEnabled(false);
                        cmbLineStyle.setEnabled(false);
                        cmbLineWidth.setEnabled(false);
                    }
                    line.setEnabled(chkEnable.isSelected());
                    txtPercentage.setEnabled(chkEnable.isSelected());
                    fibonacciRetracements.setFibonacciLines(hashRowInfo);
                    dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    //ComponentRegistry.fireUpdate();
                }
            });

            cmbLineStyle.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    if (target instanceof ObjectFibonacciExtensions) {
                        byte id = (byte) cmbLineStyle.getSelectedIndex();
                        line.setPenStyle(id);
                        fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    }
                }
            });

            cmbLineWidth.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    if (target instanceof ObjectFibonacciExtensions) {
                        Object obj = ((ComboItem) cmbLineWidth.getSelectedItem()).getValue();
                        float width = Float.parseFloat((String) obj);
                        line.setPenWidth(width);
                        fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    }
                }
            });

            lblColor.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {

                    if (target instanceof ObjectFibonacciExtensions && !chkSameProperties.isSelected()) {
                        Color pickColor = lblColor.getBackground();
                        line.setLineColor(pickColor);
                        fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    }
                }

                public void mousePressed(MouseEvent e) {
                    //System.out.println("mouse pressed");
                }

                public void mouseReleased(MouseEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void mouseEntered(MouseEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void mouseExited(MouseEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }
            });

            txtPercentage.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    try {
                        double value = Double.parseDouble(txtPercentage.getText().trim());
                        value = (value * 10) / 1000;  //round up purposes
                        line.setPercentageValue(value);
                        fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        double val = (line.getPercentageValue() * 1000) / 10;
                        txtPercentage.setText("");
                        //txtPercentage.setText(String.valueOf(val));
                        //fibonacciRetracements.setFibonacciLines(hashRowInfo);
                        //dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    }
                }
            });

            panelUp.add(chkEnable);
            panelUp.add(txtPercentage);
            if (Double.compare(line.getPercentageValue(), Double.POSITIVE_INFINITY) == 0 || Double.compare(line.getPercentageValue(), Double.NEGATIVE_INFINITY) == 0
                    || Double.compare(line.getPercentageValue(), TEMP_VALUE / 100) == 0) {
                panelUp.add(new JLabel(""));   // % sign not required for the trend line
            } else {
                panelUp.add(new JLabel("%"));
            }

            panelUp.add(lblColor);
            panelUp.add(cmbLineStyle);
            panelUp.add(cmbLineWidth);
        }
        pnlAll.add(panelUp);

        JPanel pnlSecond = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18"}, 5, 5));

        final CustomColorLabel lblAll = new CustomColorLabel(Color.RED);
        final JComboBox cmbStyleAll = UIHelper.getPenStyleCombo();
        final JComboBox cmbWidthAll = UIHelper.getPenWidthCombo();

        lblAll.setEnabled(chkSameProperties.isSelected());
        cmbStyleAll.setEnabled(chkSameProperties.isSelected());
        cmbWidthAll.setEnabled(chkSameProperties.isSelected());

        FibonacciRowGUI row = hashRowUI.get(0);
        lblAll.setBackground(row.getLblColor().getBackground());
        cmbStyleAll.setSelectedIndex(row.getCmbLineStyle().getSelectedIndex());
        cmbWidthAll.setSelectedIndex(row.getCmLineWidth().getSelectedIndex());

        chkSameProperties.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                ((ObjectFibonacciExtensions) target).setUsingSameProperties(chkSameProperties.isSelected());
                //lblColor.
                lblAll.setEnabled(chkSameProperties.isSelected());
                cmbStyleAll.setEnabled(chkSameProperties.isSelected());
                cmbWidthAll.setEnabled(chkSameProperties.isSelected());

                Hashtable temp = hashRowInfo;
                if (chkSameProperties.isSelected()) {
                    int size = hashRowUI.size();
                    for (int i = 0; i < size; i++) {
                        FibonacciRowGUI rowUI = hashRowUI.get(i);
                        JComboBox cmbStyle = rowUI.getCmbLineStyle();
                        JComboBox cmbWidth = rowUI.getCmLineWidth();
                        CustomColorLabel lblCol = rowUI.getLblColor();

                        cmbStyle.setEnabled(false);
                        cmbWidth.setEnabled(false);
                        lblCol.setEnabled(false);
                        //lblCol.setBackground(Color.DARK_GRAY);
                    }
                    int size2 = temp.size();
                    for (int i = 0; i < size2; i++) {

                        FibonacciExtensionLine line = (FibonacciExtensionLine) temp.get(i);

                        if (line.isEnabled()) { //apply only for the lines which are currently enabled.(check box enabled)
                            line.setLineColor(lblAll.getBackground());
                            line.setPenStyle((byte) cmbStyleAll.getSelectedIndex());

                            Object obj = ((ComboItem) cmbWidthAll.getSelectedItem()).getValue();
                            float width = Float.parseFloat((String) obj);
                            line.setPenWidth(width);
                        }
                    }
                    fibonacciRetracements.setFibonacciLines(temp);
                    dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    //ComponentRegistry.fireUpdate();

                } else {
                    int size = hashRowUI.size();
                    for (int i = 0; i < size; i++) {
                        FibonacciRowGUI rowUI = hashRowUI.get(i);
                        JComboBox cmbStyle = rowUI.getCmbLineStyle();
                        JComboBox cmbWidth = rowUI.getCmLineWidth();
                        CustomColorLabel lblCol = rowUI.getLblColor();

                        FibonacciExtensionLine line = (FibonacciExtensionLine) hashRowInfo.get(i);
                        line.setLineColor(lblCol.getBackground());
                        line.setPenStyle((byte) cmbStyle.getSelectedIndex());
                        Object obj = ((ComboItem) cmbWidth.getSelectedItem()).getValue();
                        float width = Float.parseFloat((String) obj);
                        line.setPenWidth(width);

                        cmbStyle.setEnabled(line.isEnabled());
                        cmbWidth.setEnabled(line.isEnabled());
                        lblCol.setEnabled(line.isEnabled());
                    }
                    fibonacciRetracements.setFibonacciLines(hashRowInfo);
                    dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    //ComponentRegistry.fireUpdate();
                }
            }
        });

        cmbStyleAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Hashtable temp = hashRowInfo;
                int size2 = temp.size();
                for (int i = 0; i < size2; i++) {

                    FibonacciExtensionLine line = (FibonacciExtensionLine) temp.get(i);

                    if (line.isEnabled()) { //apply only for the lines which are currently enabled.(check box enabled)
                        line.setLineColor(lblAll.getBackground());
                        line.setPenStyle((byte) cmbStyleAll.getSelectedIndex());
                        Object obj = ((ComboItem) cmbWidthAll.getSelectedItem()).getValue();
                        float width = Float.parseFloat((String) obj);
                        line.setPenWidth(width);
                    }
                }
                fibonacciRetracements.setFibonacciLines(temp);
                dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                //ComponentRegistry.fireUpdate();
            }
        });

        cmbWidthAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Hashtable temp = hashRowInfo;
                int size2 = temp.size();
                for (int i = 0; i < size2; i++) {

                    FibonacciExtensionLine line = (FibonacciExtensionLine) temp.get(i);

                    if (line.isEnabled()) { //apply only for the lines which are currently enabled.(check box enabled)
                        line.setLineColor(lblAll.getBackground());
                        line.setPenStyle((byte) cmbStyleAll.getSelectedIndex());
                        Object obj = ((ComboItem) cmbWidthAll.getSelectedItem()).getValue();
                        float width = Float.parseFloat((String) obj);
                        line.setPenWidth(width);
                    }
                }
                fibonacciRetracements.setFibonacciLines(temp);
                dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                //ComponentRegistry.fireUpdate();
            }
        });

        lblAll.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                if (chkSameProperties.isSelected()) { //done only when checkbox is enabled
                    int size2 = hashRowInfo.size();
                    for (int i = 0; i < size2; i++) {

                        FibonacciExtensionLine line = (FibonacciExtensionLine) hashRowInfo.get(i);

                        if (line.isEnabled()) { //apply only for the lines which are currently enabled.(check box enabled)
                            line.setLineColor(lblAll.getBackground());
                            line.setPenStyle((byte) cmbStyleAll.getSelectedIndex());
                            Object obj = ((ComboItem) cmbWidthAll.getSelectedItem()).getValue();
                            float width = Float.parseFloat((String) obj);
                            line.setPenWidth(width);
                        }
                    }
                    fibonacciRetracements.setFibonacciLines(hashRowInfo);
                    dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
                    //ComponentRegistry.fireUpdate();
                }
            }

            public void mousePressed(MouseEvent e) {

            }

            public void mouseReleased(MouseEvent e) {

            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });

        final JCheckBox chkSetDefault = new JCheckBox(Language.getString("SET_AS_DEFAULT2"));

        ((AbstractObject) target).setUsingUserDefault(false);
        dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);

        chkSetDefault.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ((AbstractObject) target).setUsingUserDefault(chkSetDefault.isSelected());
                dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
            }
        });

        final JCheckBox chkFreeStyle = new JCheckBox(Language.getString("GRAPH_FREE_STYLE"));

        //TODO: charith. if needs in future
        chkFreeStyle.setSelected(origin.isFreeStyle());
        //chkFreeStyle.setEnabled(!graph.GDMgr.isSnapToPrice());

        chkFreeStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ((AbstractObject) target).setFreeStyle(chkFreeStyle.isSelected());
                dynamicChangeObjectProperties(origin, (AbstractObject) target, graph);
            }
        });

        pnlSecond.add(chkSameProperties);
        pnlAll.add(pnlSecond);

        JPanel pnlBottom = new JPanel();
        pnlBottom.setLayout(new FlexGridLayout(new String[]{"20", "65", "20", "85", "85", "85"}, new String[]{"18"}, hGap, vGap));
        pnlBottom.add(new JLabel(""));
        pnlBottom.add(new JLabel(""));
        pnlBottom.add(new JLabel(""));

        pnlBottom.add(lblAll);
        pnlBottom.add(cmbStyleAll);
        pnlBottom.add(cmbWidthAll);
        pnlAll.add(pnlBottom);
        JPanel pnlDef = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "10", "18"}, hGap, 0));
        pnlDef.add(chkFreeStyle);
        pnlDef.add(new JPanel());
        pnlDef.add(chkSetDefault);
        pnlAll.add(pnlDef);

        Integer[] UIkeys = (Integer[]) hashRowUI.keySet().toArray(new Integer[0]);
        Arrays.sort(UIkeys);

        GUISettings.applyOrientation(pnlAll);
        return pnlAll;
    }


    public static Hashtable<Integer, FibonacciRetracementiLine> sortHashTable(Hashtable table) {
        Integer[] UIkeys = (Integer[]) table.keySet().toArray(new Integer[0]);
        Arrays.sort(UIkeys);

        Hashtable<Integer, FibonacciRetracementiLine> tableSort = new Hashtable<Integer, FibonacciRetracementiLine>();
        for (int key : UIkeys) {
            FibonacciRetracementiLine line = (FibonacciRetracementiLine) table.get(key);
            tableSort.put(key, line);
        }

        return tableSort;
    }

    public static JPanel createGeneralPanel(final ChartProperties origin, final ChartProperties target,
                                            final StockGraph graph) {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"30%", "65%"}, 10, 5));
        panel.setSize(DIM_DIALOG);

        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"35%", "22%", "33%", "10%"}, new String[]{"20", "20"}, 2, 0));
        JPanel middlePanel = new JPanel(new FlexGridLayout(new String[]{"55%", "2%", "43%"}, new String[]{"100%"}, 0, 0));
        //JPanel bottomPanel = new JPanel(new FlexGridLayout(new String[]{"55%", "45%"}, new String[]{"100%"}, 0, 0));

        JPanel pnlColor = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"65%", "30%"}, 0, 0));
        JPanel pnlTopColor = new JPanel(new FlexGridLayout(new String[]{"35%", "60%"}, new String[]{"20", "20"}, 0, 0));
        JPanel pnlBottomColor = new JPanel(new FlexGridLayout(new String[]{"80%"}, new String[]{"20"}, 0, 0));

        JPanel middleRightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "20", "20", "20", "20"}, 0, 3));

        pnlColor.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("CHART_COLOR"), TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        //pnlColor.setBounds(LEFT, VADJ + 3 * GAP + 2 * CMB_HEIGHT, 160, 4 * CMB_HEIGHT + 5 * GAP + 5);

        GraphLabel lblUpColor = new GraphLabel(Language.getString("UP"));
        //lblUpColor.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 40, CMB_HEIGHT);
        JComboBox cmbUpColor = getColorCombo(origin, target, graph, METHOD_SET_COLOR, target.getColor(),
                LEFT + 40, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);

        final GraphLabel lblDownColor = new GraphLabel(Language.getString("DOWN_COLON"));
        //lblDownColor.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 40, CMB_HEIGHT);
        final JComboBox cmbDownColor = getColorCombo(origin, target, graph, METHOD_WARNING_COLOR,
                target.getWarningColor(), LEFT + 40, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);

        final JCheckBox chkUseSameColor = new JCheckBox(Language.getString("USE_SAME_COLOR"));
        //chkUseSameColor.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 130, CMB_HEIGHT);
        //chkUseSameColor.setBounds(LEFT, 3 * GAP + 3 * CMB_HEIGHT, 150, CMB_HEIGHT);
        chkUseSameColor.setSelected(target.isUsingSameColor());
        if (target.isUsingSameColor()) {
            lblDownColor.setEnabled(false);
            cmbDownColor.setEnabled(false);
        }
        chkUseSameColor.setFont(Theme.getDefaultFont(Font.PLAIN, 12));
        chkUseSameColor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean useSame = chkUseSameColor.isSelected();
                target.setUseSameColor(useSame);
                lblDownColor.setEnabled(!useSame);
                cmbDownColor.setEnabled(!useSame);
                dynamicChangeChartProperties(origin, target, graph);
                //cmbDownColor.getEditor().getEditorComponent().setEnabled(!useSame);
            }
        });

        GraphLabel lblLineStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        //lblLineStyle.setBounds(GAP + 170, VADJ + 8 * GAP + 2 * CMB_HEIGHT, 120, CMB_HEIGHT);
        JComboBox cmbLineStyle = getPenStyleCombo(origin, target, graph, GAP + 170, VADJ + 9 * GAP + 3 * CMB_HEIGHT, 130, CMB_HEIGHT);

        GraphLabel lblLineWeight = new GraphLabel(Language.getString("LINE_WEIGHT"));
        //lblLineWeight.setBounds(GAP + 170, VADJ + 10 * GAP + 4 * CMB_HEIGHT, 120, CMB_HEIGHT);
        JComboBox cmbLineWidth = getPenWidthCombo(origin, target, graph, METHOD_SET_WIDTH,
                target.getPenWidth(), GAP + 170, VADJ + 11 * GAP + 5 * CMB_HEIGHT, 130, CMB_HEIGHT);


        //added by charithn-set as deualt
        /*final JCheckBox chkSetDefault = new JCheckBox(Language.getString("SET_AS_DEFAULT2"));
        chkSetDefault.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setUsingUserDefault(chkSetDefault.isSelected());
                dynamicChangeChartProperties(origin,target,graph);
                System.out.println("***clicked");
            }
        });*/

        byte ID = target.getID();
        boolean isPrioVisible = (ID != GraphDataManager.ID_VOLUME) && (ID != GraphDataManager.ID_TURNOVER) && !((ID > GraphDataManager.ID_MULTI_LOW) && (ID < GraphDataManager.ID_MULTI_HIGH));
        if (isPrioVisible && (target instanceof Indicator)) {
            ChartProperties cp = ((Indicator) target).getInnerSource();
            if (cp.getID() > GraphDataManager.ID_COMPARE) {
                isPrioVisible = false;
            }
        }
        if (ID == GraphDataManager.ID_PRICE_VOLUME_TREND || ID == GraphDataManager.ID_ON_BALANCE_VOLUME) {
            isPrioVisible = true;
        }


        if (isPrioVisible) {
            GraphLabel lblStyle = new GraphLabel(Language.getString("CHART_STYLE"));
            //lblStyle.setBounds(GAP + 170, GAP, 80, CMB_HEIGHT);
            JComponent[] ca = {lblLineStyle, cmbLineStyle, lblLineWeight, cmbLineWidth};
            JComboBox cmbStyle = getGraphStyleCombo(origin, target, graph, ca, GAP + 170, CMB_HEIGHT + 2 * GAP, 100, CMB_HEIGHT);
            //panel.add(lblStyle);
            //panel.add(cmbStyle);

            GraphLabel lblPriority = new GraphLabel(Language.getString("PRICE_FIELD"));
            //lblPriority.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
            JComboBox cmbPriority = getPriorityCombo(origin, target, graph, saPriority, LEFT, CMB_HEIGHT + 2 * GAP, 100, CMB_HEIGHT);
            //panel.add(lblPriority);
            //panel.add(cmbPriority);
            topPanel.add(lblPriority);
            topPanel.add(new JLabel(""));
            topPanel.add(lblStyle);
            topPanel.add(new JLabel(""));
            topPanel.add(cmbPriority);
            topPanel.add(new JLabel(""));
            topPanel.add(cmbStyle);
            topPanel.add(new JLabel(""));
        } else {
            GraphLabel lblStyle = new GraphLabel(Language.getString("CHART_STYLE"));
            //lblStyle.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
            JComponent[] ca = {lblLineStyle, cmbLineStyle, lblLineWeight, cmbLineWidth};
            JComboBox cmbStyle = getGraphStyleCombo(origin, target, graph, ca, LEFT, CMB_HEIGHT + 2 * GAP, 100, CMB_HEIGHT);
            //panel.add(lblStyle);
            //panel.add(cmbStyle);
            topPanel.setLayout((new FlexGridLayout(new String[]{"35%", "65%"}, new String[]{"20", "20"}, 0, 0)));
            topPanel.add(lblStyle);
            topPanel.add(new JLabel(""));
            topPanel.add(cmbStyle);
            topPanel.add(new JLabel(""));
        }


        pnlTopColor.add(lblUpColor);
        pnlTopColor.add(cmbUpColor);
        pnlTopColor.add(lblDownColor);
        pnlTopColor.add(cmbDownColor);
        pnlBottomColor.add(chkUseSameColor);
        pnlColor.add(pnlTopColor);
        pnlColor.add(pnlBottomColor);
        middleRightPanel.add(new JLabel(""));
        middleRightPanel.add(lblLineStyle);
        middleRightPanel.add(cmbLineStyle);
        middleRightPanel.add(lblLineWeight);
        middleRightPanel.add(cmbLineWidth);
        middlePanel.add(pnlColor);
        middlePanel.add(new JPanel());
        middlePanel.add(middleRightPanel);
        //bottomPanel.add(chkSetDefault);
        //bottomPanel.add(new JPanel());

        panel.add(topPanel);
        panel.add(middlePanel);
        //panel.add(bottomPanel);

        GUISettings.applyOrientation(panel);
        return panel;
    }

    public static JPanel createGeneralPanel(final ChartProperties origin, final ChartProperties target,
                                            final StockGraph graph, boolean isPrioVisible) {
        JPanel panel = new JPanel();
        panel.setSize(DIM_DIALOG);
        panel.setLayout(null);

        JPanel pnlColor = new JPanel();
        pnlColor.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("CHART_COLOR"), TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        pnlColor.setLayout(null);
        pnlColor.setBounds(LEFT, VADJ + 3 * GAP + 2 * CMB_HEIGHT, 160, 4 * CMB_HEIGHT + 5 * GAP + 5);

        GraphLabel lblUpColor = new GraphLabel(Language.getString("UP"));
        lblUpColor.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 40, CMB_HEIGHT);
        JComboBox cmbUpColor = getColorCombo(origin, target, graph, METHOD_SET_COLOR, target.getColor(), LEFT + 40, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);

        final GraphLabel lblDownColor = new GraphLabel(Language.getString("DOWN_COLON"));
        lblDownColor.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 40, CMB_HEIGHT);
        final JComboBox cmbDownColor = getColorCombo(origin, target, graph, METHOD_WARNING_COLOR, target.getWarningColor(), LEFT + 40, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);

        final JCheckBox chkUseSameColor = new JCheckBox(Language.getString("USE_SAME_COLOR"));
        chkUseSameColor.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 130, CMB_HEIGHT);
        chkUseSameColor.setSelected(target.isUsingSameColor());
        if (target.isUsingSameColor()) {
            lblDownColor.setEnabled(false);
            cmbDownColor.setEnabled(false);
        }
        chkUseSameColor.setFont(Theme.getDefaultFont(Font.PLAIN, 12));
        chkUseSameColor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean useSame = chkUseSameColor.isSelected();
                target.setUseSameColor(useSame);
                lblDownColor.setEnabled(!useSame);
                cmbDownColor.setEnabled(!useSame);
                //cmbDownColor.getEditor().getEditorComponent().setEnabled(!useSame);
            }
        });

        GraphLabel lblLineStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        lblLineStyle.setBounds(GAP + 170, VADJ + 3 * GAP + 2 * CMB_HEIGHT, 120, CMB_HEIGHT);
        JComboBox cmbLineStyle = getPenStyleCombo(origin, target, graph, GAP + 170, VADJ + 4 * GAP + 3 * CMB_HEIGHT, 130, CMB_HEIGHT);

        GraphLabel lblLineWeight = new GraphLabel(Language.getString("LINE_WEIGHT"));
        lblLineWeight.setBounds(GAP + 170, VADJ + 5 * GAP + 4 * CMB_HEIGHT, 120, CMB_HEIGHT);
        JComboBox cmbLineWidth = getPenWidthCombo(origin, target, graph, METHOD_SET_WIDTH,
                target.getPenWidth(), GAP + 170, VADJ + 6 * GAP + 5 * CMB_HEIGHT, 130, CMB_HEIGHT);

        if (isPrioVisible) {
            GraphLabel lblStyle = new GraphLabel(Language.getString("CHART_STYLE"));
            lblStyle.setBounds(GAP + 170, GAP, 80, CMB_HEIGHT);
            JComponent[] ca = {lblLineStyle, cmbLineStyle, lblLineWeight, cmbLineWidth};
            JComboBox cmbStyle = getGraphStyleCombo(origin, target, graph, ca, GAP + 170, CMB_HEIGHT + 2 * GAP, 100, CMB_HEIGHT);
            panel.add(lblStyle);
            panel.add(cmbStyle);

            GraphLabel lblPriority = new GraphLabel(Language.getString("PRICE_FIELD"));
            lblPriority.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
            JComboBox cmbPriority = getPriorityCombo(origin, target, graph, saPriority, LEFT, CMB_HEIGHT + 2 * GAP, 100, CMB_HEIGHT);
            panel.add(lblPriority);
            panel.add(cmbPriority);
        } else {
            GraphLabel lblStyle = new GraphLabel(Language.getString("CHART_STYLE"));
            lblStyle.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
            JComponent[] ca = {lblLineStyle, cmbLineStyle, lblLineWeight, cmbLineWidth};
            JComboBox cmbStyle = getGraphStyleCombo(origin, target, graph, ca, LEFT, CMB_HEIGHT + 2 * GAP, 100, CMB_HEIGHT);
            panel.add(lblStyle);
            panel.add(cmbStyle);
        }

        pnlColor.add(lblUpColor);
        pnlColor.add(cmbUpColor);
        pnlColor.add(lblDownColor);
        pnlColor.add(cmbDownColor);
        pnlColor.add(chkUseSameColor);
        panel.add(pnlColor);
        panel.add(lblLineStyle);
        panel.add(cmbLineStyle);
        panel.add(lblLineWeight);
        panel.add(cmbLineWidth);

        return panel;
    }

    public static void createIndicatorSourceDestPanel(StockGraph graph, final PropertyDialogTabPane tabbedPane, ImageIcon icon) {
        JPanel panel = new JPanel();
        panel.setSize(DIM_DIALOG);

        Vector<ChartProperties> allowedCurves = new Vector<ChartProperties>();
        ArrayList sources = graph.GDMgr.getSources();
        tabbedPane.setSourceCP((ChartProperties) sources.get(0));
        for (java.util.Iterator it = sources.iterator(); it.hasNext(); ) {
            ChartProperties cp = (ChartProperties) it.next();
            if (cp.getRect() == graph.volumeRect && !graph.showVolumeGrf) {
                continue;
            }
            if (cp.getRect() == graph.turnOverRect && !graph.showTurnOverGrf) {
                continue;
            }
            allowedCurves.add(cp);
        }
        final JList allowedList = new JList(allowedCurves);
        allowedList.setPreferredSize(new Dimension(290, 200));
        allowedList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        allowedList.setSelectedIndex(0);
        JScrollPane scrollPane = new JScrollPane(allowedList);
        panel.add(scrollPane);
        tabbedPane.addTab(Language.getString("SOURCES"), panel, Language.getString("SET_SOURCE"));

        JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());
        panel1.setSize(DIM_DIALOG);
        Vector<WindowPanel> availableRects = new Vector<WindowPanel>();
        for (java.util.Iterator it = graph.panels.iterator(); it.hasNext(); ) {
            WindowPanel rect = (WindowPanel) it.next();
            availableRects.add(rect);
        }
        final JList destinations = new JList(availableRects);
        final JCheckBox chkNewWindow = new JCheckBox(Language.getString("INDICATOR_ON_NEW_WINDOW"));
        destinations.setPreferredSize(new Dimension(290, 200));
        destinations.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        final WindowPanel newWindow = new WindowPanel();
        destinations.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (chkNewWindow.isSelected()) {
                    tabbedPane.setRect(newWindow);
                } else {
                    tabbedPane.setRect((WindowPanel) destinations.getSelectedValue());
                }
            }
        });
        chkNewWindow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (chkNewWindow.isSelected()) {
                    destinations.setEnabled(false);
                    tabbedPane.setRect(newWindow);
                } else {
                    destinations.setEnabled(true);
                    tabbedPane.setRect((WindowPanel) destinations.getSelectedValue());
                }

            }
        });
        JScrollPane scrollPane1 = new JScrollPane(destinations);
        panel1.add(scrollPane1, BorderLayout.CENTER);
        JPanel checkBoxPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        checkBoxPanel.add(chkNewWindow);
        panel1.add(checkBoxPanel, BorderLayout.SOUTH);
        tabbedPane.addTab(Language.getString("DESTINATION"), panel1, Language.getString("SET_DESTINATION"));

        allowedList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                tabbedPane.setSourceCP((ChartProperties) allowedList.getSelectedValue());
                destinations.setSelectedValue(((ChartProperties) allowedList.getSelectedValue()).getRect(), true);
            }
        });
    }

    public static void createDestinationPanel(StockGraph graph, final PropertyDialogTabPane tabbedPane, ImageIcon icon) {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setSize(DIM_DIALOG);
        Vector<WindowPanel> availableRects = new Vector<WindowPanel>();
        for (java.util.Iterator it = graph.panels.iterator(); it.hasNext(); ) {
            WindowPanel rect = (WindowPanel) it.next();
            availableRects.add(rect);
        }
        final JList destinations = new JList(availableRects);
        final JCheckBox chkNewWindow = new JCheckBox(Language.getString("INDICATOR_ON_NEW_WINDOW"));
        destinations.setPreferredSize(new Dimension(290, 200));
        destinations.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        final WindowPanel newWindow = new WindowPanel();
        destinations.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (chkNewWindow.isSelected()) {
                    tabbedPane.setRect(newWindow);
                } else {
                    tabbedPane.setRect((WindowPanel) destinations.getSelectedValue());
                }
            }
        });
        chkNewWindow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (chkNewWindow.isSelected()) {
                    destinations.setEnabled(false);
                    tabbedPane.setRect(newWindow);
                } else {
                    destinations.setEnabled(true);
                    tabbedPane.setRect((WindowPanel) destinations.getSelectedValue());
                }

            }
        });
        JScrollPane scrollPane1 = new JScrollPane(destinations);
        panel.add(scrollPane1, BorderLayout.CENTER);
        panel.add(chkNewWindow, BorderLayout.SOUTH);
        tabbedPane.addTab(Language.getString("DESTINATION"), panel, Language.getString("SET_DESTINATION"));
    }

    public static JPanel createObjectGeneralPanel(final AbstractObject origin, final AbstractObject target,
                                                  final StockGraph graph) {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"45%", "50%"}, new String[]{"20", "20", "20", "20", "20", "20"}, 5, 5));
        panel.setSize(DIM_DIALOG);
        //panel.setLayout(null);

        GraphLabel lblColor = new GraphLabel(Language.getString("LINE_COLOR"));
        //lblColor.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        JComboBox cmbColor = getColorCombo(origin, target, graph, METHOD_SET_COLOR, target.getColor(),
                LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblLineStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        //lblLineStyle.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 120, CMB_HEIGHT);
        JComboBox cmbLineStyle = getPenStyleCombo(origin, target, graph,
                LEFT, 4 * GAP + 3 * CMB_HEIGHT, 130, CMB_HEIGHT);

        GraphLabel lblLineWeight = new GraphLabel(Language.getString("LINE_WEIGHT"));
        //lblLineWeight.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, 120, CMB_HEIGHT);
        JComboBox cmbLineWidth = getPenWidthCombo(origin, target, graph, METHOD_SET_WIDTH,
                target.getPenWidth(), LEFT, 6 * GAP + 5 * CMB_HEIGHT, 130, CMB_HEIGHT);

        final JCheckBox chkSetDefault = new JCheckBox(Language.getString("SET_AS_DEFAULT2"));
        //chkSetDefault.setBounds(LEFT + 140, 7 * GAP + 5 * CMB_HEIGHT, 120, CMB_HEIGHT);

        //target.setUsingUserDefault(chkSetDefault.isSelected());


        chkSetDefault.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setUsingUserDefault(chkSetDefault.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        final JCheckBox chkFreeStyle = new JCheckBox(Language.getString("GRAPH_FREE_STYLE"));
        //chkFreeStyle.setBounds(LEFT + 140, 7 * GAP + 4 * CMB_HEIGHT, 120, CMB_HEIGHT);

        //TODO: charitjh. if needs in future
        chkFreeStyle.setSelected(origin.isFreeStyle());
        //chkFreeStyle.setEnabled(!graph.GDMgr.isSnapToPrice());

        chkFreeStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setFreeStyle(chkFreeStyle.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });


        panel.add(lblColor);
        panel.add(new JLabel(""));
        panel.add(cmbColor);
        panel.add(new JLabel(""));
        panel.add(lblLineStyle);
        panel.add(new JLabel(""));
        panel.add(cmbLineStyle);
        panel.add(new JLabel(""));
        panel.add(lblLineWeight);
        if (!(origin instanceof ObjectLineHorizontal || origin instanceof ObjectGannLine ||
                origin instanceof ObjectGannFan || origin instanceof ObjectGannGrid ||
                origin instanceof ObjectFibonacciZones || origin instanceof ObjectRegression ||
                origin instanceof ObjectRaffRegression || origin instanceof ObjectStandardDeviationChannel
                || origin instanceof ObjectStandardError || origin instanceof ObjectEquiDistantChannel)) {    //skip unnecessary objects
            panel.add(chkFreeStyle);
        } else
            panel.add(new JLabel(""));

        panel.add(cmbLineWidth);
        panel.add(chkSetDefault);

        //panel.add(chkFreeStyle);

        GUISettings.applyOrientation(panel);
        return panel;
    }

    public static JPanel createObjectRectPanel(final LineStudy origin, final LineStudy target,
                                               final StockGraph graph) {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 10, 5));
        JPanel leftPanel = new JPanel(new FlexGridLayout(new String[]{"90%"}, new String[]{"18", "20", "18", "20", "18", "20"}, 0, 3));
        JPanel rightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"42", "20", "20", "20", "20"}, 0, 3));
        JPanel rightTopPanel = new JPanel(new FlexGridLayout(new String[]{"90%"}, new String[]{"50%", "50%"}, 0, 0));

        panel.setSize(DIM_DIALOG);
        int horizAdj = 145;

        GraphLabel lblColor = new GraphLabel(Language.getString("LINE_COLOR"));
        JComboBox cmbColor = getColorCombo(origin, target, graph, METHOD_SET_COLOR, target.getColor(),
                LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblLineStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        JComboBox cmbLineStyle = getPenStyleCombo(origin, target, graph,
                LEFT, 4 * GAP + 3 * CMB_HEIGHT, 130, CMB_HEIGHT);

        GraphLabel lblLineWeight = new GraphLabel(Language.getString("LINE_WEIGHT"));
        JComboBox cmbLineWidth = getPenWidthCombo(origin, target, graph, METHOD_SET_WIDTH,
                target.getPenWidth(), LEFT, 6 * GAP + 5 * CMB_HEIGHT, 130, CMB_HEIGHT);

        GraphLabel lblFillColor = new GraphLabel(Language.getString("BACK_COLOR"));
        JComboBox cmbFillColor = getColorCombo(origin, target, graph, METHOD_SET_FILL_COLOR,
                target.getFillColor(), LEFT + horizAdj, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);

        final JCheckBox chkTransparent = new JCheckBox(Language.getString("TRANSPARENT"));
        chkTransparent.setSelected(target.isTransparent());
        chkTransparent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setTransparent(chkTransparent.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        final JCheckBox chkSendBack = new JCheckBox(Language.getString("SEND_BEHIND_CHARTS"));
        chkSendBack.setSelected(target.isDrawingOnBackground());
        chkSendBack.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setDrawingOnBackground(chkSendBack.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        final JCheckBox chkSetDefault = new JCheckBox(Language.getString("SET_AS_DEFAULT2"));

        chkSetDefault.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setUsingUserDefault(chkSetDefault.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        final JCheckBox chkFreeStyle = new JCheckBox(Language.getString("GRAPH_FREE_STYLE"));

        //TODO: charith. if needs in future
        chkFreeStyle.setSelected(origin.isFreeStyle());
        //chkFreeStyle.setEnabled(!graph.GDMgr.isSnapToPrice());

        chkFreeStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setFreeStyle(chkFreeStyle.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        leftPanel.add(lblColor);
        leftPanel.add(cmbColor);
        leftPanel.add(lblLineStyle);
        leftPanel.add(cmbLineStyle);
        leftPanel.add(lblLineWeight);
        leftPanel.add(cmbLineWidth);

        rightTopPanel.add(lblFillColor);
        rightTopPanel.add(cmbFillColor);
        rightPanel.add(rightTopPanel);
        rightPanel.add(chkTransparent);
        rightPanel.add(chkSendBack);
        rightPanel.add(chkFreeStyle);
        rightPanel.add(chkSetDefault);

        panel.add(leftPanel);
        panel.add(rightPanel);

        return panel;
    }

    //added by charithn
    public static JPanel createObjectDataSettingsPanel(final AbstractObject origin, final AbstractObject target,
                                                       final StockGraph graph) {
        JPanel panel = new JPanel();
        if (Language.isLTR()) {
            panel.setLayout(new FlexGridLayout(new String[]{"1", "40", "3", "70"}, new String[]{"22"}, 10, 10));
        } else {
            panel.setLayout(new FlexGridLayout(new String[]{"1", "60", "3", "70"}, new String[]{"22"}, 10, 10));
        }

        panel.setSize(DIM_DIALOG);

        GraphLabel lblColor = new GraphLabel(Language.getString("GRAPH_HOR_VALUE"));
        JTextField txtPrice = getDataSettingsTextField(origin, target, graph);

        panel.add(new JLabel(""));
        panel.add(lblColor);
        panel.add(new JLabel(""));
        panel.add(txtPrice);
        GUISettings.applyOrientation(panel);
        return panel;
    }

    public static JPanel createVLineDataSettingsPanel(final AbstractObject origin, final AbstractObject target,
                                                      final StockGraph graph) {
        JPanel panel = new JPanel();
        if (Language.isLTR()) {
            panel.setLayout(new FlexGridLayout(new String[]{"65", "70", "20", "70"}, new String[]{"22"}, 5, 10));
        } else {
            panel.setLayout(new FlexGridLayout(new String[]{"70", "80", "20", "70"}, new String[]{"22"}, 5, 10));
        }

        final JLabel lblDateStr = new JLabel(Language.getString("SET_DATE"));
        final JLabel lblDateValue = new JLabel("");
        final JButton btnDate = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));
        final DatePicker datePicker = new DatePicker(ChartFrame.getSharedInstance(), true);
        final TimePickerPanel pnlTime = new TimePickerPanel();
        final JTextField txtHours = pnlTime.getTxtHours();
        final JTextField txtMins = pnlTime.getTxtMins();

        int height = 22;
        int leftX = 10;
        int y = 10;
        pnlTime.setVisible(false);

        try {
            //load data settings
            long[] xArr = new long[0];

            if (target instanceof AbstractObject) {

                xArr = target.getXArr();
                SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                String yValue = timeFormatter.format(new Date(xArr[0]));

                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(xArr[0]);
                //System.out.println("===================== TEMP =============== : " + c);
                int index = yValue.lastIndexOf("-");
                //String date = yValue.substring(0, index);

                String date = String.valueOf(c.get(Calendar.YEAR)) + "-" + String.valueOf(c.get(Calendar.MONTH) + 1) + "-" + String.valueOf(c.get(Calendar.DATE));
                lblDateValue.setText(date);
                datePicker.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                if (graph.isCurrentMode()) {
                    pnlTime.setVisible(true);
                    lblDateStr.setText(Language.getString("SET_TIME"));
                    xArr = target.getXArr();
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(xArr[0]);

                    int ind = yValue.lastIndexOf("-");
                    String temp = yValue.substring(ind + 1, yValue.length());
                    String[] vals = temp.split(":");

                    if (vals.length == 2) {
                        //pnlTime.setHours(vals[0]);
                        //pnlTime.setMins(vals[1]);
                        pnlTime.setHours(String.valueOf(c.get(Calendar.HOUR_OF_DAY)));
                        String min = String.valueOf(c.get(Calendar.MINUTE));
                        if (min.length() == 1) {
                            min = "0" + min; // if mins is 7 sets it to the 07
                        }
                        pnlTime.setMins(min);
                    }
                }
            }

            datePicker.getCalendar().setSelfDispose(false);
            datePicker.getCalendar().addDateSelectedListener(new DateSelectedListener() {


                public void dateSelected(Object source, int iYear, int iMonth, int iDay) {


                    datePicker.setVisible(false);
                    Calendar cal = Calendar.getInstance();
                    int hours = 0;
                    int mins = 0;
                    if (graph.isCurrentMode()) {
                        hours = Integer.parseInt(txtHours.getText());
                        mins = Integer.parseInt(txtMins.getText());
                    }
                    cal.set(iYear, iMonth, iDay, hours, mins, 0);

                    if (target instanceof AbstractObject) {

                        //finding the closest marlet open day for a given day.
                        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{cal.getTimeInMillis()});
                        int x = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                        float index = graph.GDMgr.getIndexForthePixel(x);
                        long time = graph.GDMgr.getTimeMillisec((int) index);
                        //setting the sidplay date in labels
                        cal.setTimeInMillis(time);
                        String dateStr = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal.get(Calendar.DATE));
                        lblDateValue.setText(dateStr);

                        target.setXArr(new long[]{time});
                        target.setIndexArray(new float[]{index});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }

            });

            txtHours.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.

                    String[] vals = lblDateValue.getText().split("-");
                    int year = Integer.parseInt(vals[0]);
                    int month = Integer.parseInt(vals[1]) - 1;
                    int date = Integer.parseInt(vals[2]);
                    if (txtMins.getText().trim().length() == 0 || txtHours.getText().trim().length() == 0) {
                        return;
                    }
                    int hours = Integer.parseInt(txtHours.getText());
                    int mins = Integer.parseInt(txtMins.getText());

                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, date, hours, mins, 0);  //sets the

                    int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                    long time = graph.GDMgr.getTimeMillisec(index);

                    //graph.GDMgr.convertXArrayTimeToIndex();

                    cal.setTimeInMillis(time);
                    //System.out.println("===== time ===== : " + cal);

                    if (target instanceof AbstractObject) {
                        //long[] xar = new long[]{cal.getTimeInMillis()};
                        long[] xar = new long[]{time};
                        target.setXArr(xar);
                        target.setIndexArray(new float[]{index});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            txtMins.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                    String[] vals = lblDateValue.getText().split("-");
                    int year = Integer.parseInt(vals[0]);
                    int month = Integer.parseInt(vals[1]) - 1;
                    int date = Integer.parseInt(vals[2]);
                    if (txtMins.getText().trim().length() == 0 || txtHours.getText().trim().length() == 0) {
                        return;
                    }
                    int hours = Integer.parseInt(txtHours.getText());
                    int mins = Integer.parseInt(txtMins.getText());

                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, date, hours, mins, 0);
                    int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                    long time = graph.GDMgr.getTimeMillisec(index);

                    cal.setTimeInMillis(time);

                    if (target instanceof AbstractObject) {
                        //long[] xar = new long[]{cal.getTimeInMillis()};
                        long[] xar = new long[]{time};
                        target.setXArr(xar);
                        target.setIndexArray(new float[]{index});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            btnDate.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    int x = (int) btnDate.getLocationOnScreen().getX();
                    int y = (int) btnDate.getLocationOnScreen().getY();
                    datePicker.setLocation(x, y);
                    datePicker.setVisible(true);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            return new JPanel();
        }

        panel.add(lblDateStr);
        panel.add(lblDateValue);
        panel.add(btnDate);
        panel.add(pnlTime);
        return panel;
    }

    // added by Mevan @ 2009-08-21
    public static JPanel createAndrewPitchforkDataSettingsPanel(final AbstractObject origin, final AbstractObject target,
                                                                final StockGraph graph) {
        JPanel panel = new JPanel();
        panel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"22", "150"}, 0, 0));

        JPanel topPanel = new JPanel();
        topPanel.setLayout(new FlexGridLayout(new String[]{"80", "40", "1", "88", "68"}, new String[]{"22"}, 5, 0));

        JPanel historyPanel = new JPanel();
        historyPanel.setLayout(new FlexGridLayout(new String[]{"75", "55", "1", "66", "18", "67"}, new String[]{"22", "22", "22"}, 4, 12));

        final JLabel lblStartDateStr = new JLabel(Language.getString("GRAPH_APDATE"), JLabel.TRAILING);
        lblStartDateStr.setFont(new Font("Arial", Font.BOLD, 12));
        final JLabel lblStartTimeStr = new JLabel(Language.getString("GRAPH_APTIME"), JLabel.TRAILING);
        lblStartTimeStr.setVisible(false);
        lblStartTimeStr.setFont(new Font("Arial", Font.BOLD, 12));
        final JLabel lblStartPriceStr = new JLabel(Language.getString("GRAPH_APPRICE"), JLabel.TRAILING);
        lblStartPriceStr.setFont(new Font("Arial", Font.BOLD, 12));

        topPanel.add(new JPanel());
        topPanel.add(lblStartPriceStr);
        topPanel.add(new JPanel());
        topPanel.add(lblStartDateStr);
        topPanel.add(lblStartTimeStr);

        //Strings
        final JLabel lblLineHandleStr = new JLabel(Language.getString("GRAPH_HANDLE_LINE"));
        final JLabel lblLine1Str = new JLabel(Language.getString("GRAPH_APLINE_1"));
        final JLabel lblLine2Str = new JLabel(Language.getString("GRAPH_APLINE_2"));

        // Date Labels
        final JLabel lblLineHandleDate = new JLabel("");
        final JLabel lblLine1Date = new JLabel("");
        final JLabel lblLine2Date = new JLabel("");

        // initial price values
        final DecimalFormat priceFormat = new DecimalFormat("###,##0.00");
        String handleLinePrice = priceFormat.format(origin.getYArr()[0]);
        String line1Price = priceFormat.format(origin.getYArr()[1]);
        String line2Price = priceFormat.format(origin.getYArr()[2]);

        // price textfields
        final DatePicker datePickerHandle = new DatePicker(ChartFrame.getSharedInstance(), true);
        final DatePicker datePickerLine1 = new DatePicker(ChartFrame.getSharedInstance(), true);
        final DatePicker datePickerLine2 = new DatePicker(ChartFrame.getSharedInstance(), true);
        final TimePickerPanel handleLinePnlTime = new TimePickerPanel();
        final TimePickerPanel line1PnlTime = new TimePickerPanel();
        final TimePickerPanel line2PnlTime = new TimePickerPanel();
        final TWTextField txtLineHandlePrice = new TWTextField();
        final TWTextField txtLine1Price = new TWTextField();
        final TWTextField txtLine2Price = new TWTextField();
        txtLineHandlePrice.setText(handleLinePrice);
        txtLine1Price.setText(line1Price);
        txtLine2Price.setText(line2Price);

        // redundant hour & mins text fields
        final JTextField txtLineHandleHoursStart = handleLinePnlTime.getTxtHours();
        final JTextField txtLine1HoursStart = line1PnlTime.getTxtHours();
        final JTextField txtLine2HoursStart = line2PnlTime.getTxtHours();
        final JTextField txtLineHandleMinsStart = handleLinePnlTime.getTxtMins();
        final JTextField txtLine1MinsStart = line1PnlTime.getTxtMins();
        final JTextField txtLine2MinsStart = line2PnlTime.getTxtMins();

        // calendar buttons
        final JButton btnHandleDate = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));
        final JButton btnLine1Date = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));
        final JButton btnLine2Date = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));


        historyPanel.add(lblLineHandleStr);
        historyPanel.add(txtLineHandlePrice);
        historyPanel.add(new JPanel());
        historyPanel.add(lblLineHandleDate);
        historyPanel.add(btnHandleDate);
        historyPanel.add(handleLinePnlTime);

        historyPanel.add(lblLine1Str);
        historyPanel.add(txtLine1Price);
        historyPanel.add(new JPanel());
        historyPanel.add(lblLine1Date);
        historyPanel.add(btnLine1Date);
        historyPanel.add(line1PnlTime);

        historyPanel.add(lblLine2Str);
        historyPanel.add(txtLine2Price);
        historyPanel.add(new JPanel());
        historyPanel.add(lblLine2Date);
        historyPanel.add(btnLine2Date);
        historyPanel.add(line2PnlTime);


        // hide the time panels by default
        handleLinePnlTime.setVisible(false);
        line1PnlTime.setVisible(false);
        line2PnlTime.setVisible(false);

        try {
            //load data settings for handle line
            long[] xArr = new long[0];

            if (target instanceof AbstractObject) {

                xArr = target.getXArr();
                SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                String yValue = timeFormatter.format(new Date(xArr[0]));

                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(xArr[0]);
                int index = yValue.lastIndexOf("-");

                String date = String.valueOf(c.get(Calendar.YEAR)) + "-" + String.valueOf(c.get(Calendar.MONTH) + 1) + "-" + String.valueOf(c.get(Calendar.DATE));
                lblLineHandleDate.setText(date);
                datePickerHandle.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                if (graph.isCurrentMode()) {
                    handleLinePnlTime.setVisible(true);
                    handleLinePnlTime.setTime(origin.getXArr()[0]);
                    lblStartTimeStr.setVisible(true);
                    xArr = target.getXArr();
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(xArr[0]);

                    int ind = yValue.lastIndexOf("-");
                    String temp = yValue.substring(ind + 1, yValue.length());
                    String[] vals = temp.split(":");

                    if (vals.length == 2) {
                        handleLinePnlTime.setHours(vals[0]);
                        handleLinePnlTime.setMins(vals[1]);
                        handleLinePnlTime.setHours(String.valueOf(c.get(Calendar.HOUR_OF_DAY)));
                        String min = String.valueOf(c.get(Calendar.MINUTE));
                        if (min.length() == 1) {
                            min = "0" + min; // if mins is 7 sets it to the 07
                        }
                        handleLinePnlTime.setMins(min);
                    }
                }
            }

            datePickerHandle.getCalendar().setSelfDispose(false);
            datePickerHandle.getCalendar().addDateSelectedListener(new DateSelectedListener() {

                public void dateSelected(Object source, int iYear, int iMonth, int iDay) {

                    datePickerHandle.setVisible(false);
                    Calendar cal = Calendar.getInstance();
                    int hours = 0;
                    int mins = 0;
                    if (graph.isCurrentMode()) {
                        //hours = Integer.parseInt(txtHours.getText());
                        //mins = Integer.parseInt(txtMins.getText());
                    }
                    cal.set(iYear, iMonth, iDay, hours, mins, 0);

                    if (target instanceof AbstractObject) {

                        //finding the closest marlet open day for a given day.
                        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{cal.getTimeInMillis()});
                        int x = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                        float index = graph.GDMgr.getIndexForthePixel(x);
                        long time = graph.GDMgr.getTimeMillisec((int) index);
                        //setting the sidplay date in labels
                        cal.setTimeInMillis(time);
                        String date = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal.get(Calendar.DATE));
                        lblLineHandleDate.setText(date);
                        target.setXArr(new long[]{time, target.getXArr()[1], target.getXArr()[2]});
                        target.setIndexArray(new float[]{index, target.getIndexArray()[1], target.getIndexArray()[2]});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);

                    }
                }
            });

            txtLineHandlePrice.addKeyListener(new KeyAdapter() {

                public void keyReleased(KeyEvent e) {

                    try {
                        double startPrice = Double.parseDouble(txtLineHandlePrice.getText());
                        target.setYArr(new double[]{startPrice, target.getYArr()[1], target.getYArr()[2]});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    } catch (NumberFormatException e1) {
                        double start = origin.getYArr()[0];
                        txtLineHandlePrice.setText(priceFormat.format(start));
                    }
                }
            });

            txtLineHandleHoursStart.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.

                    String[] vals = lblLineHandleDate.getText().split("-");
                    int year = Integer.parseInt(vals[0]);
                    int month = Integer.parseInt(vals[1]) - 1;
                    int date = Integer.parseInt(vals[2]);
                    if (txtLineHandleMinsStart.getText().trim().length() == 0 || txtLineHandleHoursStart.getText().trim().length() == 0) {
                        return;
                    }
                    int hours = Integer.parseInt(txtLineHandleHoursStart.getText());
                    int mins = Integer.parseInt(txtLineHandleMinsStart.getText());

                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, date, hours, mins, 0);  //sets the

                    int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                    long time = graph.GDMgr.getTimeMillisec(index);
                    cal.setTimeInMillis(time);

                    if (target instanceof AbstractObject) {
                        target.setXArr(new long[]{time, origin.getXArr()[1], origin.getXArr()[2]});
                        target.setIndexArray(new float[]{index, target.getIndexArray()[1], target.getIndexArray()[2]});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            txtLineHandleMinsStart.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                    String[] vals = lblLineHandleDate.getText().split("-");
                    int year = Integer.parseInt(vals[0]);
                    int month = Integer.parseInt(vals[1]) - 1;
                    int date = Integer.parseInt(vals[2]);
                    if (txtLineHandleMinsStart.getText().trim().length() == 0 || txtLineHandleHoursStart.getText().trim().length() == 0) {
                        return;
                    }
                    int hours = Integer.parseInt(txtLineHandleHoursStart.getText());
                    int mins = Integer.parseInt(txtLineHandleMinsStart.getText());

                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, date, hours, mins, 0);
                    int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                    long time = graph.GDMgr.getTimeMillisec(index);

                    cal.setTimeInMillis(time);

                    if (target instanceof AbstractObject) {
                        long[] xar = new long[]{time};
                        target.setXArr(new long[]{time, origin.getXArr()[1], origin.getXArr()[2]});
                        target.setIndexArray(new float[]{index, target.getIndexArray()[1], target.getIndexArray()[2]});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            btnHandleDate.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    int x = (int) btnHandleDate.getLocationOnScreen().getX();
                    int y = (int) btnHandleDate.getLocationOnScreen().getY();
                    datePickerHandle.setLocation(x, y);
                    datePickerHandle.setVisible(true);
                }
            });
        } catch (Exception ex) {
            return new JPanel();
        }

        try {
            //load data settings for line1
            long[] xArr = new long[0];

            if (target instanceof AbstractObject) {

                xArr = target.getXArr();
                SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                String yValue = timeFormatter.format(new Date(xArr[1]));

                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(xArr[1]);
                int index = yValue.lastIndexOf("-");

                String date = String.valueOf(c.get(Calendar.YEAR)) + "-" + String.valueOf(c.get(Calendar.MONTH) + 1) + "-" + String.valueOf(c.get(Calendar.DATE));
                lblLine1Date.setText(date);
                datePickerLine1.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                if (graph.isCurrentMode()) {
                    line1PnlTime.setVisible(true);
                    line1PnlTime.setTime(origin.getXArr()[1]);
                    lblStartTimeStr.setVisible(true);
                    xArr = target.getXArr();
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(xArr[1]);

                    int ind = yValue.lastIndexOf("-");
                    String temp = yValue.substring(ind + 1, yValue.length());
                    String[] vals = temp.split(":");

                    if (vals.length == 2) {
                        line1PnlTime.setHours(vals[0]);
                        line1PnlTime.setMins(vals[1]);
                        line1PnlTime.setHours(String.valueOf(c.get(Calendar.HOUR_OF_DAY)));
                        String min = String.valueOf(c.get(Calendar.MINUTE));
                        if (min.length() == 1) {
                            min = "0" + min; // if mins is 7 sets it to the 07
                        }
                        line1PnlTime.setMins(min);
                    }
                }
            }

            datePickerLine1.getCalendar().setSelfDispose(false);
            datePickerLine1.getCalendar().addDateSelectedListener(new DateSelectedListener() {

                public void dateSelected(Object source, int iYear, int iMonth, int iDay) {

                    datePickerLine1.setVisible(false);
                    Calendar cal = Calendar.getInstance();
                    int hours = 0;
                    int mins = 0;
                    if (graph.isCurrentMode()) {
                        //hours = Integer.parseInt(txtHours.getText());
                        //mins = Integer.parseInt(txtMins.getText());
                    }
                    cal.set(iYear, iMonth, iDay, hours, mins, 0);

                    if (target instanceof AbstractObject) {

                        //finding the closest marlet open day for a given day.
                        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{cal.getTimeInMillis()});
                        int x = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                        float index = graph.GDMgr.getIndexForthePixel(x);
                        long time = graph.GDMgr.getTimeMillisec((int) index);
                        //setting the sidplay date in labels
                        cal.setTimeInMillis(time);
                        String date = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal.get(Calendar.DATE));
                        lblLine1Date.setText(date);
                        target.setXArr(new long[]{target.getXArr()[0], time, target.getXArr()[2]});
                        target.setIndexArray(new float[]{target.getIndexArray()[0], index, target.getIndexArray()[2]});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            txtLine1Price.addKeyListener(new KeyAdapter() {

                public void keyReleased(KeyEvent e) {

                    try {
                        double startPrice = Double.parseDouble(txtLine1Price.getText());
                        target.setYArr(new double[]{target.getYArr()[0], startPrice, target.getYArr()[2]});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    } catch (NumberFormatException e1) {
                        double start = origin.getYArr()[1];
                        txtLine1Price.setText(priceFormat.format(start));
                    }
                }
            });

            txtLine1HoursStart.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.

                    String[] vals = lblLine1Date.getText().split("-");
                    int year = Integer.parseInt(vals[0]);
                    int month = Integer.parseInt(vals[1]) - 1;
                    int date = Integer.parseInt(vals[2]);
                    if (txtLine1MinsStart.getText().trim().length() == 0 || txtLine1HoursStart.getText().trim().length() == 0) {
                        return;
                    }
                    int hours = Integer.parseInt(txtLine1HoursStart.getText());
                    int mins = Integer.parseInt(txtLine1MinsStart.getText());

                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, date, hours, mins, 0);  //sets the

                    int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                    long time = graph.GDMgr.getTimeMillisec(index);
                    cal.setTimeInMillis(time);

                    if (target instanceof AbstractObject) {
                        target.setXArr(new long[]{origin.getXArr()[0], time, origin.getXArr()[2]});
                        target.setIndexArray(new float[]{target.getIndexArray()[0], index, target.getIndexArray()[2]});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            txtLine1MinsStart.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                    String[] vals = lblLine1Date.getText().split("-");
                    int year = Integer.parseInt(vals[0]);
                    int month = Integer.parseInt(vals[1]) - 1;
                    int date = Integer.parseInt(vals[2]);
                    if (txtLine1MinsStart.getText().trim().length() == 0 || txtLine1HoursStart.getText().trim().length() == 0) {
                        return;
                    }
                    int hours = Integer.parseInt(txtLine1HoursStart.getText());
                    int mins = Integer.parseInt(txtLine1MinsStart.getText());

                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, date, hours, mins, 0);
                    int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                    long time = graph.GDMgr.getTimeMillisec(index);

                    cal.setTimeInMillis(time);

                    if (target instanceof AbstractObject) {
                        long[] xar = new long[]{time};
                        target.setXArr(new long[]{origin.getXArr()[0], time, origin.getXArr()[2]});
                        target.setIndexArray(new float[]{target.getIndexArray()[0], index, target.getIndexArray()[2]});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            btnLine1Date.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    int x = (int) btnLine1Date.getLocationOnScreen().getX();
                    int y = (int) btnLine1Date.getLocationOnScreen().getY();
                    datePickerLine1.setLocation(x, y);
                    datePickerLine1.setVisible(true);
                }
            });
        } catch (Exception ex) {
            return new JPanel();
        }

        // TODO : Update Labels on change... Mevan
        try {
            //load data settings for line2
            long[] xArr = new long[0];

            if (target instanceof AbstractObject) {

                xArr = target.getXArr();
                SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                String yValue = timeFormatter.format(new Date(xArr[2]));

                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(xArr[2]);
                int index = yValue.lastIndexOf("-");

                String date = String.valueOf(c.get(Calendar.YEAR)) + "-" + String.valueOf(c.get(Calendar.MONTH) + 1) + "-" + String.valueOf(c.get(Calendar.DATE));
                lblLine2Date.setText(date);
                datePickerLine2.setDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                if (graph.isCurrentMode()) {
                    line2PnlTime.setVisible(true);
                    line2PnlTime.setTime(origin.getXArr()[2]);
                    lblStartTimeStr.setVisible(true);
                    xArr = target.getXArr();
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(xArr[2]);

                    int ind = yValue.lastIndexOf("-");
                    String temp = yValue.substring(ind + 1, yValue.length());
                    String[] vals = temp.split(":");

                    if (vals.length == 2) {
                        line2PnlTime.setHours(vals[0]);
                        line2PnlTime.setMins(vals[1]);
                        line2PnlTime.setHours(String.valueOf(c.get(Calendar.HOUR_OF_DAY)));
                        String min = String.valueOf(c.get(Calendar.MINUTE));
                        if (min.length() == 1) {
                            min = "0" + min; // if mins is 7 sets it to the 07
                        }
                        line2PnlTime.setMins(min);
                    }
                }
            }

            datePickerLine2.getCalendar().setSelfDispose(false);
            datePickerLine2.getCalendar().addDateSelectedListener(new DateSelectedListener() {

                public void dateSelected(Object source, int iYear, int iMonth, int iDay) {

                    datePickerLine2.setVisible(false);
                    Calendar cal = Calendar.getInstance();
                    int hours = 0;
                    int mins = 0;
                    if (graph.isCurrentMode()) {
                        //hours = Integer.parseInt(txtHours.getText());
                        //mins = Integer.parseInt(txtMins.getText());
                    }
                    cal.set(iYear, iMonth, iDay, hours, mins, 0);

                    if (target instanceof AbstractObject) {

                        //finding the closest marlet open day for a given day.
                        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{cal.getTimeInMillis()});
                        int x = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                        float index = graph.GDMgr.getIndexForthePixel(x);
                        long time = graph.GDMgr.getTimeMillisec((int) index);
                        //setting the sidplay date in labels
                        cal.setTimeInMillis(time);
                        String date = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal.get(Calendar.DATE));
                        lblLine2Date.setText(date);
                        target.setXArr(new long[]{target.getXArr()[0], target.getXArr()[1], time});
                        target.setIndexArray(new float[]{target.getIndexArray()[0], target.getIndexArray()[1], index});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            txtLine2Price.addKeyListener(new KeyAdapter() {

                public void keyReleased(KeyEvent e) {

                    try {
                        double startPrice = Double.parseDouble(txtLine2Price.getText());
                        target.setYArr(new double[]{target.getYArr()[0], target.getYArr()[1], startPrice});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    } catch (NumberFormatException e1) {
                        double start = origin.getYArr()[2];
                        txtLine2Price.setText(priceFormat.format(start));
                    }
                }
            });

            txtLine2HoursStart.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.

                    String[] vals = lblLine2Date.getText().split("-");
                    int year = Integer.parseInt(vals[0]);
                    int month = Integer.parseInt(vals[1]) - 1;
                    int date = Integer.parseInt(vals[2]);
                    if (txtLine2MinsStart.getText().trim().length() == 0 || txtLine2HoursStart.getText().trim().length() == 0) {
                        return;
                    }
                    int hours = Integer.parseInt(txtLine2HoursStart.getText());
                    int mins = Integer.parseInt(txtLine2MinsStart.getText());

                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, date, hours, mins, 0);  //sets the

                    int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                    long time = graph.GDMgr.getTimeMillisec(index);
                    cal.setTimeInMillis(time);

                    if (target instanceof AbstractObject) {
                        target.setXArr(new long[]{origin.getXArr()[0], origin.getXArr()[1], time});
                        target.setIndexArray(new float[]{target.getIndexArray()[0], target.getIndexArray()[1], index});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            txtLine2MinsStart.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyPressed(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                }

                public void keyReleased(KeyEvent e) {
                    //To change body of implemented methods use File | Settings | File Templates.
                    String[] vals = lblLine2Date.getText().split("-");
                    int year = Integer.parseInt(vals[0]);
                    int month = Integer.parseInt(vals[1]) - 1;
                    int date = Integer.parseInt(vals[2]);
                    if (txtLine2MinsStart.getText().trim().length() == 0 || txtLine2HoursStart.getText().trim().length() == 0) {
                        return;
                    }
                    int hours = Integer.parseInt(txtLine2HoursStart.getText());
                    int mins = Integer.parseInt(txtLine2MinsStart.getText());

                    Calendar cal = Calendar.getInstance();
                    cal.set(year, month, date, hours, mins, 0);
                    int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                    long time = graph.GDMgr.getTimeMillisec(index);

                    cal.setTimeInMillis(time);

                    if (target instanceof AbstractObject) {
                        long[] xar = new long[]{time};
                        target.setXArr(new long[]{origin.getXArr()[0], origin.getXArr()[1], time});
                        target.setIndexArray(new float[]{target.getIndexArray()[0], target.getIndexArray()[1], index});
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                }
            });

            btnLine2Date.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    int x = (int) btnLine2Date.getLocationOnScreen().getX();
                    int y = (int) btnLine2Date.getLocationOnScreen().getY();
                    datePickerLine2.setLocation(x, y);
                    datePickerLine2.setVisible(true);
                }
            });
        } catch (Exception ex) {
            return new JPanel();
        }

        panel.add(topPanel);
        panel.add(historyPanel);
        return panel;
    }

    private static JPanel createObjectSymbolPanel(final ObjectSymbol origin,
                                                  final ObjectSymbol target, final StockGraph graph) {
        //JPanel panel = new JPanel();
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"35%", "50%"}, new String[]{"20", "20", "20", "20"}, 5, 5));
        panel.setSize(DIM_DIALOG);
        //panel.setLayout(null);

        GraphLabel lblSymblColor = new GraphLabel(Language.getString("COLOR"));
        //lblSymblColor.setBounds(LEFT, GAP, 120, CMB_HEIGHT);
//        JComboBox cmbSymblColor = getColorCombo(origin, target, graph, METHOD_SET_FONT_COLOR,
//                target.getFontColor(), LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT); //changed by sathyajith

        JComboBox cmbSymblColor = getColorCombo(origin, target, graph, METHOD_SET_FONT_COLOR,
                target.getColor(), LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblSize = new GraphLabel(Language.getString("SIZE"));
        //lblSize.setBounds(LEFT, GAP * 3 + 2 * CMB_HEIGHT, 120, CMB_HEIGHT);
        JComboBox cmbSize = new JComboBox();
        //cmbSize.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbSize.setEditable(false);
        cmbSize.setFont(StockGraph.font_BOLD_10);
        //String[] sa = {"Small", "Medium", "Large"};
        String[] sa = {Language.getString("CHART_SIGN_SMALL"), Language.getString("CHART_SIGN_MEDIUM"), Language.getString("CHART_SIGN_LARGE")};
        for (int i = 0; i < sa.length; i++) {
            cmbSize.addItem(sa[i]);
            if (target.getFont().getSize() / 6 - 2 == i) {
                cmbSize.setSelectedIndex(i);
            }
        }
        cmbSize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    target.setFont(SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf",
                            cmb.getSelectedIndex() * 6 + 12));
                    dynamicChangeObjectProperties(origin, target, graph);
                }
            }
        });

        panel.add(lblSymblColor);
        panel.add(new JLabel(""));
        panel.add(cmbSymblColor);
        panel.add(new JLabel(""));
        panel.add(lblSize);
        panel.add(new JLabel(""));
        panel.add(cmbSize);
        return panel;
    }

    // added - Pramoda
    private static JPanel createExtensionsTab(final AbstractObject origin, final AbstractObject targetObj,
                                              final StockGraph graph) {
        final Extendible target = (Extendible) targetObj;
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"30%", "10%", "65%",}, new String[]{"20", "20"}, 10, 5));
        panel.setSize(DIM_DIALOG);
        //panel.setLayout(null);

        GraphLabel lblExtendLeft = new GraphLabel(Language.getString("EXTEND_LEFT"));
        //lblExtendLeft.setBounds(LEFT, GAP * 2, 100, LBL_HEIGHT);
        final JCheckBox cbExtendLeft = new JCheckBox();
        //cbExtendLeft.setBounds(LEFT + 100, GAP * 2, 100, LBL_HEIGHT);
        cbExtendLeft.setSelected(target.isExtendedLeft());
        cbExtendLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendLeft.isSelected();
                target.setExtendedLeft(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        GraphLabel lblExtendRight = new GraphLabel(Language.getString("EXTEND_RIGHT"));
        //lblExtendRight.setBounds(LEFT, GAP * 4 + LBL_HEIGHT, 100, LBL_HEIGHT);
        final JCheckBox cbExtendRight = new JCheckBox();
        //cbExtendRight.setBounds(LEFT + 100, GAP * 4 + LBL_HEIGHT, 100, LBL_HEIGHT);
        cbExtendRight.setSelected(target.isExtendedRight());
        cbExtendRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendRight.isSelected();
                target.setExtendedRight(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        panel.add(lblExtendLeft);
        panel.add(cbExtendLeft);
        panel.add(new JLabel(""));
        panel.add(lblExtendRight);
        panel.add(cbExtendRight);

        return panel;
    }

    private static JPanel createAlarmTab(final AbstractObject origin, final AbstractObject targetObj,
                                         final StockGraph graph) {
        //final InfoObjectDisplayble target = (InfoObjectDisplayble) targetObj;
        final Alarmable target = (Alarmable) targetObj;
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "80%"}, 2, 2));
        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        JPanel bottomPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "100%"}, 5, 0));
        panel.setSize(DIM_DIALOG);

        GraphLabel lblAlarmMessage = new GraphLabel(Language.getString("TREND_LINE_ALERT_MSG"));
        final JTextArea txtMessage = new JTextArea();
        txtMessage.setText(target.getAlarmMessage());
        txtMessage.setLineWrap(true);
        txtMessage.setEnabled(target.isAlarmEnabled());
        txtMessage.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                target.setAlarmMessage(txtMessage.getText());
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        final JCheckBox cbEnableAlarm = new JCheckBox(Language.getString("TREND_LINE_TRIGGER_ALERT"));
        cbEnableAlarm.setFont(lblAlarmMessage.getFont());
        cbEnableAlarm.setSelected(target.isAlarmEnabled());
        cbEnableAlarm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbEnableAlarm.isSelected();
                txtMessage.setEnabled(checked);
                target.setAlarmEnabled(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        topPanel.add(cbEnableAlarm);
        bottomPanel.add(lblAlarmMessage);
        bottomPanel.add(txtMessage);
        panel.add(topPanel);
        panel.add(bottomPanel);
        return panel;

    }

    private static JPanel createLineSlopeExtensionsTab(final AbstractObject origin, final AbstractObject targetObj,
                                                       final StockGraph graph) {
        final InfoObjectDisplayble target = (InfoObjectDisplayble) targetObj;
        final ObjectLineSlope lineSlope = (ObjectLineSlope) target;
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"40%", "60%"}, 2, 2));
        panel.setSize(DIM_DIALOG);

        /*  begining of advance settings */
        GraphLabel lblAngle = new GraphLabel(Language.getString("GRAPH_TREND_ANGLE"));

        SpinnerModel model = new SpinnerNumberModel(45, 0, 359, 1);
        final JSpinner spnAngle = new JSpinner(model);

        final JCheckBox chkAngle = new JCheckBox();
        chkAngle.setSelected(target.isbarCount());

        //left hand side
        final JLabel lblStart = new JLabel(Language.getString("GRAPH_TREND_START"), JLabel.TRAILING);
        lblStart.setFont(new Font("Arial", Font.BOLD, 12));

        final JLabel lblEnd = new JLabel(Language.getString("GRAPH_TREND_END"), JLabel.TRAILING);
        lblEnd.setFont(new Font("Arial", Font.BOLD, 12));

        GraphLabel lblDateStr = new GraphLabel(Language.getString("GRAPH_TREND_DATE"));
        GraphLabel lblTimeStr = new GraphLabel(Language.getString("GRAPH_TREND_TIME"));
        GraphLabel lblPriceStr = new GraphLabel(Language.getString("GRAPH_TREND_PRICE"));
        final GraphLabel lblStartDate = new GraphLabel("");

        final TWButton btnStartDate = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));

        final TimePickerPanel startTime = new TimePickerPanel();

        final ValueFormatter formatter = new ValueFormatter(ValueFormatter.DECIMAL, 6, 4);
        final TWTextField txtStartPrice = new TWTextField();
        txtStartPrice.setDocument(formatter);

        // right hand side
        final GraphLabel lblEndDate = new GraphLabel("");
        final TWButton btnEndDate = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));
        final TimePickerPanel endTime = new TimePickerPanel();

        ValueFormatter formatter2 = new ValueFormatter(ValueFormatter.DECIMAL, 6, 4);
        final TWTextField txtEndPrice = new TWTextField();
        txtEndPrice.setDocument(formatter2);

        //shashikaw
        JPanel pnlTop = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"40", "20"}, 2, 2));
        JPanel pnlTopFirst = new JPanel(new FlexGridLayout(new String[]{"120", "20", "15", "120", "20"}, new String[]{"20", "20"}, 2, 0));
        JPanel pnlTopSecond = new JPanel(new FlexGridLayout(new String[]{"120", "20", "10", "20", "5", "65", "50"}, new String[]{"20"}, 2, 0));

        JPanel pnlAdvance = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20"}, 2, 2));

        JPanel pnlFirst = new JPanel(new FlexGridLayout(new String[]{"140", "158"}, new String[]{"20"}, 2, 0));
        pnlFirst.add(lblStart);
        pnlFirst.add(lblEnd);
        pnlAdvance.add(pnlFirst);

        JPanel pnlSecond = new JPanel(new FlexGridLayout(new String[]{"40", "75", "20", "55", "80", "20"}, new String[]{"20"}, 2, 0));
        pnlSecond.add(lblDateStr);
        pnlSecond.add(lblStartDate);
        pnlSecond.add(btnStartDate);
        pnlSecond.add(new JPanel());
        pnlSecond.add(lblEndDate);
        pnlSecond.add(btnEndDate);
        pnlAdvance.add(pnlSecond);

        JPanel pnlThird = new JPanel(new FlexGridLayout(new String[]{"72", "70", "88", "70"}, new String[]{"20"}, 2, 0));
        pnlThird.add(lblTimeStr);
        pnlThird.add(startTime);
        pnlThird.add(new JPanel());
        pnlThird.add(endTime);
        //no need to add starttime and end time in history mode
        if (graph.isCurrentMode()) {
            pnlAdvance.add(pnlThird);
        }

        JPanel pnlFourth = new JPanel(new FlexGridLayout(new String[]{"74", "63", "95", "63"}, new String[]{"20"}, 2, 0));
        pnlFourth.add(lblPriceStr);
        pnlFourth.add(txtStartPrice);
        pnlFourth.add(new JPanel());
        pnlFourth.add(txtEndPrice);
        pnlAdvance.add(pnlFourth);
        /*  end of advance settings */

        float[] xArr = null;
        if (origin.isFreeStyle()) {
            xArr = origin.getIndexArray();  //get the original index array
        } else {
            xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
        }
        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

        int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
        int x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);

        int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);
        int y2 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[1], pnlID);

        final double length = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));

        GraphLabel lblExtendLeft = new GraphLabel(Language.getString("EXTEND_LEFT"));
        final JCheckBox cbExtendLeft = new JCheckBox();
        cbExtendLeft.setSelected(target.isExtendedLeft());

        cbExtendLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendLeft.isSelected();
                target.setExtendedLeft(checked);
                txtStartPrice.setEnabled(!checked && !chkAngle.isSelected());
                btnStartDate.setEnabled(!checked && !chkAngle.isSelected());
                startTime.controlComponent(!checked);
                //lblStartDate.setEnabled(!checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);

                if (!cbExtendLeft.isSelected()) {
                    return;
                }
                float[] xArr = null;
                if (origin.isFreeStyle()) {
                    xArr = origin.getIndexArray();  //get the original index array
                } else {
                    xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
                }
                int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

                int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
                int x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]); //get the pixel from the index
                int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);
                int y2 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[1], pnlID);

                double ang = Math.toRadians(((ObjectLineSlope) origin).getAngle());
                if (ang != 0) {
                    x2 = (int) (x1 + length * Math.cos(ang));
                    y2 = (int) (y1 - length * Math.sin(ang));
                } else {
                    x1 = (int) origin.getRect().getX();
                    y2 = y1;
                }
                float index1 = graph.GDMgr.getIndexForthePixel(x1);
                float index2 = graph.GDMgr.getIndexForthePixel(x2);
                float[] faX0 = new float[]{index1, index2};
                double[] faY0 = new double[]{graph.GDMgr.getYValueForthePixel(y1, pnlID), graph.GDMgr.getYValueForthePixel(y2, pnlID)};

                targetObj.setXArr(graph.GDMgr.convertXArrayIndexToTime(faX0));
                targetObj.setYArr(faY0);
                targetObj.setIndexArray(faX0);
            }
        });

        GraphLabel lblExtendRight = new GraphLabel(Language.getString("EXTEND_RIGHT"));
        final JCheckBox cbExtendRight = new JCheckBox();
        cbExtendRight.setSelected(target.isExtendedRight());
        cbExtendRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendRight.isSelected();
                target.setExtendedRight(checked);
                txtEndPrice.setEnabled(!checked && !chkAngle.isSelected());
                btnEndDate.setEnabled(!checked && !chkAngle.isSelected());
                endTime.controlComponent(!checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);

                if (!cbExtendRight.isSelected()) {
                    return;
                }
                float[] xArr = null;
                if (origin.isFreeStyle()) {
                    xArr = origin.getIndexArray();  //get the original index array
                } else {
                    xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
                }
                int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

                int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
                int x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]); //get the pixel from the index
                int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);
                int y2 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[1], pnlID);

                double ang = Math.toRadians(((ObjectLineSlope) origin).getAngle());
                if (ang != 0) {
                    x2 = (int) (x1 + length * Math.cos(ang));
                    y2 = (int) (y1 - length * Math.sin(ang));
                } else {
                    x2 = (int) (origin.getRect().getX() + origin.getRect().getWidth());
                    y2 = y1;
                }
                float index1 = graph.GDMgr.getIndexForthePixel(x1);
                float index2 = graph.GDMgr.getIndexForthePixel(x2);
                float[] faX0 = new float[]{index1, index2};
                double[] faY0 = new double[]{graph.GDMgr.getYValueForthePixel(y1, pnlID), graph.GDMgr.getYValueForthePixel(y2, pnlID)};

                targetObj.setXArr(graph.GDMgr.convertXArrayIndexToTime(faX0));
                targetObj.setYArr(faY0);
                targetObj.setIndexArray(faX0);
            }
        });

        GraphLabel lblShowNetChange = new GraphLabel(Language.getString("SHOW_NET_CHANGE"));
        final JCheckBox cbShowNetChange = new JCheckBox();
        cbShowNetChange.setSelected(target.isnetChange());
        cbShowNetChange.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbShowNetChange.isSelected();
                target.setnetChange(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        GraphLabel lblShowPctchange = new GraphLabel(Language.getString("SHOW_PCT_CHANGE"));

        final JCheckBox cbShowPctChnge = new JCheckBox();
        cbShowPctChnge.setSelected(target.ispctChange());
        cbShowPctChnge.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbShowPctChnge.isSelected();
                target.setpctChange(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });
        GraphLabel lblBarCount = new GraphLabel(Language.getString("SHOW_BAR_COUNT"));
        final JCheckBox cbShowBarCount = new JCheckBox();
        cbShowBarCount.setSelected(target.isbarCount());
        cbShowBarCount.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbShowBarCount.isSelected();
                target.setbarCount(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        //loading time value settings
        SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String startDate = timeFormatter.format(origin.getXArr()[0]);
        String endDate = timeFormatter.format(origin.getXArr()[1]);
        lblStartDate.setText(startDate);
        lblEndDate.setText(endDate);

        if (graph.isCurrentMode()) {
            //timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
            startTime.setTime(origin.getXArr()[0]);
            endTime.setTime(origin.getXArr()[1]);
        }
        startTime.controlComponent(graph.isCurrentMode());
        endTime.controlComponent(graph.isCurrentMode());

        //load price values
        final DecimalFormat priceFormat = new DecimalFormat("###,##0.00");
        String beginPrice = priceFormat.format(origin.getYArr()[0]);
        String endPrice = priceFormat.format(origin.getYArr()[1]);
        txtStartPrice.setText(beginPrice);
        txtEndPrice.setText(endPrice);

        final ObjectLineSlope line = (ObjectLineSlope) target;
        chkAngle.setSelected(line.isAngle() && !graph.GDMgr.isSnapToPrice());
        chkAngle.setEnabled(!graph.GDMgr.isSnapToPrice());
        txtStartPrice.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft() && !graph.GDMgr.isSnapToPrice());
        txtEndPrice.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight() && !graph.GDMgr.isSnapToPrice());
        spnAngle.setEnabled(chkAngle.isSelected());
        btnStartDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft());
        btnEndDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());
        lblStartDate.setEnabled(!chkAngle.isSelected());
        lblEndDate.setEnabled(!chkAngle.isSelected());

        startTime.controlComponent(graph.isCurrentMode() && !chkAngle.isSelected() && !target.isExtendedLeft());
        endTime.controlComponent(graph.isCurrentMode() && !chkAngle.isSelected() && !target.isExtendedRight());

        chkAngle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                txtStartPrice.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft());
                txtEndPrice.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());
                spnAngle.setEnabled(chkAngle.isSelected());
                btnStartDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft());
                startTime.controlComponent(!chkAngle.isSelected() && !target.isExtendedLeft());
                btnEndDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());
                endTime.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());
                lblStartDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft());
                lblEndDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());

                startTime.controlComponent(graph.isCurrentMode() && !chkAngle.isSelected());
                endTime.controlComponent(graph.isCurrentMode() && !chkAngle.isSelected());

                spnAngle.setValue((int) (((ObjectLineSlope) origin).getAngle()));
                line.setAngle(chkAngle.isSelected());
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        if ((int) (((ObjectLineSlope) origin).getAngle()) > 0) {
            spnAngle.setValue((int) (int) (((ObjectLineSlope) origin).getAngle()));
        }

        spnAngle.getEditor().addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                //System.out.println("***");
            }
        });

        spnAngle.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {

                double angle = Double.parseDouble(spnAngle.getValue().toString());

                float[] xArr = null;
                if (origin.isFreeStyle()) {
                    xArr = origin.getIndexArray();  //get the original index array
                } else {
                    xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
                }
                int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

                int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
                int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);

                double ang = Math.toRadians(angle);
                int x2 = (int) (x1 + length * Math.cos(ang));
                int y2 = (int) (y1 - length * Math.sin(ang));

                float index1 = graph.GDMgr.getIndexForthePixel(x1);
                float index2 = graph.GDMgr.getIndexForthePixel(x2);
                float[] faX0 = new float[]{index1, index2};
                double[] faY0 = new double[]{graph.GDMgr.getYValueForthePixel(y1, pnlID), graph.GDMgr.getYValueForthePixel(y2, pnlID)};

                targetObj.setXArr(graph.GDMgr.convertXArrayIndexToTime(faX0));
                targetObj.setYArr(faY0);
                targetObj.setIndexArray(faX0);

                dynamicChangeObjectProperties(origin, targetObj, graph);

                //TODO: charith
                ObjectLineSlope line = (ObjectLineSlope) targetObj;
                //System.out.println(((ObjectLineSlope) origin).getAngle());
                if (Math.abs(line.getAngle() - Integer.parseInt(spnAngle.getValue().toString())) > 1) {
                    // spnAngle.setValue((int) (((ObjectLineSlope) origin).getAngle()));
                }
            }
        });

        final JFormattedTextField field = ((JSpinner.DefaultEditor) spnAngle.getEditor()).getTextField();
        ((DefaultFormatter) field.getFormatter()).setAllowsInvalid(false);//disable invalid characters.

        field.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);

                double angle = Double.parseDouble(field.getText());
                if (angle < 0 || angle > 359) {
                    field.setText(String.valueOf(lineSlope.getAngle()));
                }

                float[] xArr = null;
                if (origin.isFreeStyle()) {
                    xArr = origin.getIndexArray();  //get the original index array
                } else {
                    xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
                }
                int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

                int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
                int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);

                double ang = Math.toRadians(angle);
                int x2 = (int) (x1 + length * Math.cos(ang));
                int y2 = (int) (y1 - length * Math.sin(ang));

                float index1 = graph.GDMgr.getIndexForthePixel(x1);
                float index2 = graph.GDMgr.getIndexForthePixel(x2);
                float[] faX0 = new float[]{index1, index2};
                double[] faY0 = new double[]{graph.GDMgr.getYValueForthePixel(y1, pnlID), graph.GDMgr.getYValueForthePixel(y2, pnlID)};

                targetObj.setXArr(graph.GDMgr.convertXArrayIndexToTime(faX0));
                targetObj.setYArr(faY0);
                targetObj.setIndexArray(faX0);

                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        final DatePicker datePickerStart = new DatePicker(ChartFrame.getSharedInstance(), true);
        final DatePicker datePickerEnd = new DatePicker(ChartFrame.getSharedInstance(), true);

        btnStartDate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String[] delims = lblStartDate.getText().trim().split("-", 3);
                int date = Integer.parseInt(delims[2]);
                int month = Integer.parseInt(delims[1]) - 1; // 0 based index
                int year = Integer.parseInt(delims[0]);
                datePickerStart.setDate(year, month, date);

                int x = (int) btnStartDate.getLocationOnScreen().getX();
                int y = (int) btnStartDate.getLocationOnScreen().getY();
                datePickerStart.setLocation(x, y);
                datePickerStart.setVisible(true);
            }
        });

        btnEndDate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {


                String[] delims = lblEndDate.getText().trim().split("-", 3);
                int date = Integer.parseInt(delims[2]);
                int month = Integer.parseInt(delims[1]) - 1; // 0 based index
                int year = Integer.parseInt(delims[0]);
                datePickerEnd.setDate(year, month, date);

                int x = (int) btnEndDate.getLocationOnScreen().getX();
                int y = (int) btnEndDate.getLocationOnScreen().getY();
                datePickerEnd.setLocation(x, y);
                datePickerEnd.setVisible(true);
            }
        });

        final JTextField txtHoursStart = startTime.getTxtHours();
        final JTextField txtMinsStart = startTime.getTxtMins();
        datePickerStart.getCalendar().addDateSelectedListener(new DateSelectedListener() {

            public void dateSelected(Object source, int iYear, int iMonth, int iDay) {

                datePickerStart.setVisible(false);
                Calendar cal = Calendar.getInstance();
                int hours = 0;
                int mins = 0;
                if (graph.isCurrentMode()) {
                    hours = Integer.parseInt(txtHoursStart.getText().trim());
                    mins = Integer.parseInt(txtMinsStart.getText().trim());
                }
                cal.set(iYear, iMonth, iDay, hours, mins, 0);

                if (target instanceof AbstractObject) {

                    //finding the closest marlet open day for a given day.
                    float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{cal.getTimeInMillis()});
                    int x = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                    float index = graph.GDMgr.getIndexForthePixel(x);
                    long time = graph.GDMgr.getTimeMillisec((int) index);
                    //setting the sidplay date in labels
                    cal.setTimeInMillis(time);
                    String dateStr = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal.get(Calendar.DATE));
                    lblStartDate.setText(dateStr);
                    lineSlope.setXArr(new long[]{time, lineSlope.getXArr()[1]});
                    lineSlope.setIndexArray(new float[]{index, lineSlope.getIndexArray()[1]});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) targetObj, graph);

                    String beginPrice = priceFormat.format(origin.getYArr()[0]);
                    txtStartPrice.setText(beginPrice);
                }
            }

        });

        final JTextField txtHoursEnd = endTime.getTxtHours();
        final JTextField txtMinsEnd = endTime.getTxtMins();
        datePickerEnd.getCalendar().addDateSelectedListener(new DateSelectedListener() {

            public void dateSelected(Object source, int iYear, int iMonth, int iDay) {

                datePickerEnd.setVisible(false);
                Calendar cal = Calendar.getInstance();
                int hours = 0;
                int mins = 0;
                if (graph.isCurrentMode()) {
                    hours = Integer.parseInt(txtHoursEnd.getText().trim());
                    mins = Integer.parseInt(txtMinsEnd.getText().trim());
                }
                cal.set(iYear, iMonth, iDay, hours, mins, 0);

                if (target instanceof AbstractObject) {

                    //finding the closest marlet open day for a given day.
                    float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{cal.getTimeInMillis()});
                    int x = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                    float index = graph.GDMgr.getIndexForthePixel(x);
                    long time = graph.GDMgr.getTimeMillisec((int) index);
                    //setting the sidplay date in labels
                    cal.setTimeInMillis(time);
                    String dateStr = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal.get(Calendar.DATE));
                    lblEndDate.setText(dateStr);

                    lineSlope.setXArr(new long[]{lineSlope.getXArr()[0], time});
                    lineSlope.setIndexArray(new float[]{lineSlope.getIndexArray()[0], index});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) targetObj, graph);

                    String endPrice = priceFormat.format(origin.getYArr()[1]);
                    txtEndPrice.setText(endPrice);
                }
            }

        });

        txtStartPrice.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent e) {

                try {
                    double startPrice = Double.parseDouble(txtStartPrice.getText());

                    lineSlope.setYArr(new double[]{startPrice, lineSlope.getYArr()[1]});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) targetObj, graph);
                } catch (NumberFormatException e1) {
                    //e1.printStackTrace();
                    double start = origin.getYArr()[0];
                    txtStartPrice.setText(priceFormat.format(start));
                }
            }
        });

        txtEndPrice.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent e) {

                try {
                    double endPrice = Double.parseDouble(txtEndPrice.getText());

                    lineSlope.setYArr(new double[]{lineSlope.getYArr()[0], endPrice});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) targetObj, graph);
                } catch (NumberFormatException e1) {
                    //e1.printStackTrace();
                    double start = origin.getYArr()[1];
                    txtEndPrice.setText(priceFormat.format(start));

                }
            }
        });

        txtHoursStart.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.

                String[] vals = lblStartDate.getText().split("-");
                int year = Integer.parseInt(vals[0]);
                int month = Integer.parseInt(vals[1]) - 1;
                int date = Integer.parseInt(vals[2]);
                if (txtMinsStart.getText().trim().length() == 0 || txtHoursStart.getText().trim().length() == 0) {
                    return;
                }
                int hours = Integer.parseInt(txtHoursStart.getText().trim());
                int mins = Integer.parseInt(txtMinsStart.getText().trim());

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, date, hours, mins, 0);  //sets the

                int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                long time = graph.GDMgr.getTimeMillisec(index);

                //graph.GDMgr.convertXArrayTimeToIndex();

                cal.setTimeInMillis(time);

                if (target instanceof AbstractObject) {
                    long[] xar = new long[]{time};
                    lineSlope.setXArr(new long[]{time, origin.getXArr()[1]});
                    lineSlope.setIndexArray(new float[]{index, lineSlope.getIndexArray()[1]});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                }
            }
        });

        txtMinsStart.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
                String[] vals = lblEndDate.getText().split("-");
                int year = Integer.parseInt(vals[0]);
                int month = Integer.parseInt(vals[1]) - 1;
                int date = Integer.parseInt(vals[2]);
                if (txtMinsStart.getText().trim().length() == 0 || txtHoursStart.getText().trim().length() == 0) {
                    return;
                }
                int hours = Integer.parseInt(txtHoursStart.getText().trim());
                int mins = Integer.parseInt(txtMinsStart.getText().trim());

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, date, hours, mins, 0);
                int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                long time = graph.GDMgr.getTimeMillisec(index);

                cal.setTimeInMillis(time);

                if (target instanceof AbstractObject) {
                    long[] xar = new long[]{time};
                    lineSlope.setXArr(new long[]{time, origin.getXArr()[1]});
                    lineSlope.setIndexArray(new float[]{index, lineSlope.getIndexArray()[1]});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                }
            }
        });

        txtHoursEnd.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.

                String[] vals = lblEndDate.getText().split("-");
                int year = Integer.parseInt(vals[0]);
                int month = Integer.parseInt(vals[1]) - 1;
                int date = Integer.parseInt(vals[2]);
                if (txtMinsEnd.getText().trim().length() == 0 || txtHoursEnd.getText().trim().length() == 0) {
                    return;
                }
                int hours = Integer.parseInt(txtHoursEnd.getText().trim());
                int mins = Integer.parseInt(txtMinsEnd.getText().trim());

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, date, hours, mins, 0);  //sets the

                int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                long time = graph.GDMgr.getTimeMillisec(index);

                //graph.GDMgr.convertXArrayTimeToIndex();

                cal.setTimeInMillis(time);

                if (target instanceof AbstractObject) {
                    long[] xar = new long[]{time};
                    lineSlope.setXArr(new long[]{origin.getXArr()[0], time});
                    lineSlope.setIndexArray(new float[]{lineSlope.getIndexArray()[0], index});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                }
            }
        });

        txtMinsEnd.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
                String[] vals = lblEndDate.getText().split("-");
                int year = Integer.parseInt(vals[0]);
                int month = Integer.parseInt(vals[1]) - 1;
                int date = Integer.parseInt(vals[2]);
                if (txtMinsEnd.getText().trim().length() == 0 || txtHoursEnd.getText().trim().length() == 0) {
                    return;
                }
                int hours = Integer.parseInt(txtHoursEnd.getText().trim());
                int mins = Integer.parseInt(txtMinsEnd.getText().trim());

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, date, hours, mins, 0);
                int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                long time = graph.GDMgr.getTimeMillisec(index);

                cal.setTimeInMillis(time);

                if (target instanceof AbstractObject) {
                    long[] xar = new long[]{time};
                    lineSlope.setXArr(new long[]{origin.getXArr()[0], time});
                    lineSlope.setIndexArray(new float[]{lineSlope.getIndexArray()[0], index});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                }
            }
        });

        pnlTopFirst.add(lblExtendLeft);
        pnlTopFirst.add(cbExtendLeft);
        pnlTopFirst.add(new JPanel());
        pnlTopFirst.add(lblShowPctchange);
        pnlTopFirst.add(cbShowPctChnge);

        pnlTopFirst.add(lblExtendRight);
        pnlTopFirst.add(cbExtendRight);
        pnlTopFirst.add(new JPanel());
        pnlTopFirst.add(lblBarCount);
        pnlTopFirst.add(cbShowBarCount);

        pnlTopSecond.add(lblShowNetChange);
        pnlTopSecond.add(cbShowNetChange);
        pnlTopSecond.add(new JPanel());
        pnlTopSecond.add(chkAngle);
        pnlTopSecond.add(new JPanel());
        pnlTopSecond.add(lblAngle);
        pnlTopSecond.add(spnAngle);

        pnlTop.add(pnlTopFirst);
        pnlTop.add(pnlTopSecond);
        panel.add(pnlTop);
        panel.add(pnlAdvance);

        GUISettings.applyOrientation(pnlAdvance);
        GUISettings.applyOrientation(pnlTop);
        return panel;
    }

    private static JPanel createArrowLineSlopeExtensionsTab(final AbstractObject origin, final AbstractObject targetObj,
                                                            final StockGraph graph) {

        final InfoObjectDisplayble target = (InfoObjectDisplayble) targetObj;
        final ObjectArrowLineSlope lineSlope = (ObjectArrowLineSlope) target;
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"40%", "60%"}, 2, 2));
        panel.setSize(DIM_DIALOG);

        /*  begining of advance settings */
        GraphLabel lblAngle = new GraphLabel(Language.getString("GRAPH_TREND_ANGLE"));

        SpinnerModel model = new SpinnerNumberModel(45, 0, 359, 1);
        final JSpinner spnAngle = new JSpinner(model);

        final JCheckBox chkAngle = new JCheckBox();
        chkAngle.setSelected(target.isbarCount());

        //left hand side
        final JLabel lblStart = new JLabel(Language.getString("GRAPH_TREND_START"), JLabel.TRAILING);
        lblStart.setFont(new Font("Arial", Font.BOLD, 12));

        final JLabel lblEnd = new JLabel(Language.getString("GRAPH_TREND_END"), JLabel.TRAILING);
        lblEnd.setFont(new Font("Arial", Font.BOLD, 12));

        GraphLabel lblDateStr = new GraphLabel(Language.getString("GRAPH_TREND_DATE"));
        GraphLabel lblTimeStr = new GraphLabel(Language.getString("GRAPH_TREND_TIME"));
        GraphLabel lblPriceStr = new GraphLabel(Language.getString("GRAPH_TREND_PRICE"));
        final GraphLabel lblStartDate = new GraphLabel("");

        final TWButton btnStartDate = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));

        final TimePickerPanel startTime = new TimePickerPanel();

        final ValueFormatter formatter = new ValueFormatter(ValueFormatter.DECIMAL, 6, 4);
        final TWTextField txtStartPrice = new TWTextField();
        txtStartPrice.setDocument(formatter);

        // right hand side
        final GraphLabel lblEndDate = new GraphLabel("");
        final TWButton btnEndDate = new TWButton(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));
        final TimePickerPanel endTime = new TimePickerPanel();

        ValueFormatter formatter2 = new ValueFormatter(ValueFormatter.DECIMAL, 6, 4);
        final TWTextField txtEndPrice = new TWTextField();
        txtEndPrice.setDocument(formatter2);

        //shashikaw
        JPanel pnlTop = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"40", "20"}, 2, 2));
        JPanel pnlTopFirst = new JPanel(new FlexGridLayout(new String[]{"120", "20", "15", "120", "20"}, new String[]{"20", "20"}, 2, 0));
        JPanel pnlTopSecond = new JPanel(new FlexGridLayout(new String[]{"120", "20", "10", "20", "5", "65", "50"}, new String[]{"20"}, 2, 0));

        JPanel pnlAdvance = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20"}, 2, 2));

        JPanel pnlFirst = new JPanel(new FlexGridLayout(new String[]{"140", "158"}, new String[]{"20"}, 2, 0));
        pnlFirst.add(lblStart);
        pnlFirst.add(lblEnd);
        pnlAdvance.add(pnlFirst);

        JPanel pnlSecond = new JPanel(new FlexGridLayout(new String[]{"40", "75", "20", "55", "80", "20"}, new String[]{"20"}, 2, 0));
        pnlSecond.add(lblDateStr);
        pnlSecond.add(lblStartDate);
        pnlSecond.add(btnStartDate);
        pnlSecond.add(new JPanel());
        pnlSecond.add(lblEndDate);
        pnlSecond.add(btnEndDate);
        pnlAdvance.add(pnlSecond);

        JPanel pnlThird = new JPanel(new FlexGridLayout(new String[]{"72", "70", "88", "70"}, new String[]{"20"}, 2, 0));
        pnlThird.add(lblTimeStr);
        pnlThird.add(startTime);
        pnlThird.add(new JPanel());
        pnlThird.add(endTime);
        //no need to add starttime and end time in history mode
        if (graph.isCurrentMode()) {
            pnlAdvance.add(pnlThird);
        }

        JPanel pnlFourth = new JPanel(new FlexGridLayout(new String[]{"74", "63", "95", "63"}, new String[]{"20"}, 2, 0));
        pnlFourth.add(lblPriceStr);
        pnlFourth.add(txtStartPrice);
        pnlFourth.add(new JPanel());
        pnlFourth.add(txtEndPrice);
        pnlAdvance.add(pnlFourth);
        /*  end of advance settings */

        float[] xArr = null;
        if (origin.isFreeStyle()) {
            xArr = origin.getIndexArray();  //get the original index array
        } else {
            xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
        }
        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

        int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
        int x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);

        int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);
        int y2 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[1], pnlID);

        final double length = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));

        GraphLabel lblExtendLeft = new GraphLabel(Language.getString("EXTEND_LEFT"));
        lblExtendLeft.setBounds(LEFT, GAP * 2, 120, LBL_HEIGHT);
        final JCheckBox cbExtendLeft = new JCheckBox();
        cbExtendLeft.setSelected(target.isExtendedLeft());
        cbExtendLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendLeft.isSelected();
                target.setExtendedLeft(checked);
                txtStartPrice.setEnabled(!checked && !chkAngle.isSelected());
                btnStartDate.setEnabled(!checked && !chkAngle.isSelected());
                startTime.controlComponent(!checked);
                //lblStartDate.setEnabled(!checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);

                if (!cbExtendLeft.isSelected()) {
                    return;
                }
                float[] xArr = null;
                if (origin.isFreeStyle()) {
                    xArr = origin.getIndexArray();  //get the original index array
                } else {
                    xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
                }
                int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

                int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
                int x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]); //get the pixel from the index
                int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);
                int y2 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[1], pnlID);

                double ang = Math.toRadians(((ObjectArrowLineSlope) origin).getAngle());
                if (ang != 0) {
                    x2 = (int) (x1 + length * Math.cos(ang));
                    y2 = (int) (y1 - length * Math.sin(ang));
                } else {
                    x1 = (int) origin.getRect().getX();
                    y2 = y1;
                }
                float index1 = graph.GDMgr.getIndexForthePixel(x1);
                float index2 = graph.GDMgr.getIndexForthePixel(x2);
                float[] faX0 = new float[]{index1, index2};
                double[] faY0 = new double[]{graph.GDMgr.getYValueForthePixel(y1, pnlID), graph.GDMgr.getYValueForthePixel(y2, pnlID)};

                targetObj.setXArr(graph.GDMgr.convertXArrayIndexToTime(faX0));
                targetObj.setYArr(faY0);
                targetObj.setIndexArray(faX0);
            }
        });

        GraphLabel lblExtendRight = new GraphLabel(Language.getString("EXTEND_RIGHT"));
        final JCheckBox cbExtendRight = new JCheckBox();
        cbExtendRight.setBounds(LEFT + 120, GAP * 4 + LBL_HEIGHT, 20, LBL_HEIGHT);
        cbExtendRight.setSelected(target.isExtendedRight());
        cbExtendRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendRight.isSelected();
                target.setExtendedRight(checked);
                txtEndPrice.setEnabled(!checked && !chkAngle.isSelected());
                btnEndDate.setEnabled(!checked && !chkAngle.isSelected());
                endTime.controlComponent(!checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);

                if (!cbExtendRight.isSelected()) {
                    return;
                }
                float[] xArr = null;
                if (origin.isFreeStyle()) {
                    xArr = origin.getIndexArray();  //get the original index array
                } else {
                    xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
                }
                int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

                int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
                int x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]); //get the pixel from the index
                int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);
                int y2 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[1], pnlID);

                double ang = Math.toRadians(((ObjectArrowLineSlope) origin).getAngle());
                if (ang != 0) {
                    x2 = (int) (x1 + length * Math.cos(ang));
                    y2 = (int) (y1 - length * Math.sin(ang));
                } else {
                    x2 = (int) (origin.getRect().getX() + origin.getRect().getWidth());
                    y2 = y1;
                }
                float index1 = graph.GDMgr.getIndexForthePixel(x1);
                float index2 = graph.GDMgr.getIndexForthePixel(x2);
                float[] faX0 = new float[]{index1, index2};
                double[] faY0 = new double[]{graph.GDMgr.getYValueForthePixel(y1, pnlID), graph.GDMgr.getYValueForthePixel(y2, pnlID)};

                targetObj.setXArr(graph.GDMgr.convertXArrayIndexToTime(faX0));
                targetObj.setYArr(faY0);
                targetObj.setIndexArray(faX0);
            }
        });

        GraphLabel lblShowNetChange = new GraphLabel(Language.getString("SHOW_NET_CHANGE"));
        final JCheckBox cbShowNetChange = new JCheckBox();
        cbShowNetChange.setSelected(target.isnetChange());
        cbShowNetChange.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbShowNetChange.isSelected();
                target.setnetChange(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        GraphLabel lblShowPctchange = new GraphLabel(Language.getString("SHOW_PCT_CHANGE"));

        final JCheckBox cbShowPctChnge = new JCheckBox();
        cbShowPctChnge.setSelected(target.ispctChange());
        cbShowPctChnge.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbShowPctChnge.isSelected();
                target.setpctChange(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });
        GraphLabel lblBarCount = new GraphLabel(Language.getString("SHOW_BAR_COUNT"));

        final JCheckBox cbShowBarCount = new JCheckBox();
        cbShowBarCount.setSelected(target.isbarCount());
        cbShowBarCount.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbShowBarCount.isSelected();
                target.setbarCount(checked);
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        //loading time value settings
        SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String startDate = timeFormatter.format(origin.getXArr()[0]);
        String endDate = timeFormatter.format(origin.getXArr()[1]);
        lblStartDate.setText(startDate);
        lblEndDate.setText(endDate);

        if (graph.isCurrentMode()) {
            //timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
            startTime.setTime(origin.getXArr()[0]);
            endTime.setTime(origin.getXArr()[1]);
        }
        startTime.controlComponent(graph.isCurrentMode());
        endTime.controlComponent(graph.isCurrentMode());

        //load price values
        final DecimalFormat priceFormat = new DecimalFormat("###,##0.00");
        String beginPrice = priceFormat.format(origin.getYArr()[0]);
        String endPrice = priceFormat.format(origin.getYArr()[1]);
        txtStartPrice.setText(beginPrice);
        txtEndPrice.setText(endPrice);

        final ObjectArrowLineSlope line = (ObjectArrowLineSlope) target;
        chkAngle.setSelected(line.isAngle() && !graph.GDMgr.isSnapToPrice());
        chkAngle.setEnabled(!graph.GDMgr.isSnapToPrice());
        txtStartPrice.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft() && !graph.GDMgr.isSnapToPrice());
        txtEndPrice.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight() && !graph.GDMgr.isSnapToPrice());
        spnAngle.setEnabled(chkAngle.isSelected());
        btnStartDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft());
        btnEndDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());
        lblStartDate.setEnabled(!chkAngle.isSelected());
        lblEndDate.setEnabled(!chkAngle.isSelected());

        startTime.controlComponent(graph.isCurrentMode() && !chkAngle.isSelected() && !target.isExtendedLeft());
        endTime.controlComponent(graph.isCurrentMode() && !chkAngle.isSelected() && !target.isExtendedRight());

        chkAngle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                txtStartPrice.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft());
                txtEndPrice.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());
                spnAngle.setEnabled(chkAngle.isSelected());
                btnStartDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft());
                startTime.controlComponent(!chkAngle.isSelected() && !target.isExtendedLeft());
                btnEndDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());
                endTime.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());
                lblStartDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedLeft());
                lblEndDate.setEnabled(!chkAngle.isSelected() && !target.isExtendedRight());

                startTime.controlComponent(graph.isCurrentMode() && !chkAngle.isSelected());
                endTime.controlComponent(graph.isCurrentMode() && !chkAngle.isSelected());

                spnAngle.setValue((int) (((ObjectArrowLineSlope) origin).getAngle()));
                line.setAngle(chkAngle.isSelected());
                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        if ((int) (((ObjectArrowLineSlope) origin).getAngle()) > 0) {
            spnAngle.setValue((int) (int) (((ObjectArrowLineSlope) origin).getAngle()));
        }

        spnAngle.getEditor().addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                System.out.println("***");
            }
        });


        spnAngle.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {

                double angle = Double.parseDouble(spnAngle.getValue().toString());

                float[] xArr = null;
                if (origin.isFreeStyle()) {
                    xArr = origin.getIndexArray();  //get the original index array
                } else {
                    xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
                }
                int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

                int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
                int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);

                double ang = Math.toRadians(angle);
                int x2 = (int) (x1 + length * Math.cos(ang));
                int y2 = (int) (y1 - length * Math.sin(ang));

                float index1 = graph.GDMgr.getIndexForthePixel(x1);
                float index2 = graph.GDMgr.getIndexForthePixel(x2);
                float[] faX0 = new float[]{index1, index2};
                double[] faY0 = new double[]{graph.GDMgr.getYValueForthePixel(y1, pnlID), graph.GDMgr.getYValueForthePixel(y2, pnlID)};

                targetObj.setXArr(graph.GDMgr.convertXArrayIndexToTime(faX0));
                targetObj.setYArr(faY0);
                //targetObj.setIndexArray(faX0);

                dynamicChangeObjectProperties(origin, targetObj, graph);

                //TODO: charith
                ObjectArrowLineSlope line = (ObjectArrowLineSlope) targetObj;
                //System.out.println(((ObjectLineSlope) origin).getAngle());
                if (Math.abs(line.getAngle() - Integer.parseInt(spnAngle.getValue().toString())) > 1) {
                    // spnAngle.setValue((int) (((ObjectLineSlope) origin).getAngle()));
                }
            }
        });

        final JFormattedTextField field = ((JSpinner.DefaultEditor) spnAngle.getEditor()).getTextField();
        ((DefaultFormatter) field.getFormatter()).setAllowsInvalid(false);//disable invalid characters.

        field.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);

                double angle = Double.parseDouble(field.getText());
                if (angle < 0 || angle > 359) {
                    field.setText(String.valueOf(lineSlope.getAngle()));
                }

                float[] xArr = null;
                if (origin.isFreeStyle()) {
                    xArr = origin.getIndexArray();  //get the original index array
                } else {
                    xArr = graph.GDMgr.convertXArrayTimeToIndex(origin.getXArr());  //get the rounded index array from time array
                }
                int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, origin.getRect());

                int x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]); //get the pixel from the index
                int y1 = graph.GDMgr.getPixelFortheYValue(origin.getYArr()[0], pnlID);

                double ang = Math.toRadians(angle);
                int x2 = (int) (x1 + length * Math.cos(ang));
                int y2 = (int) (y1 - length * Math.sin(ang));

                float index1 = graph.GDMgr.getIndexForthePixel(x1);
                float index2 = graph.GDMgr.getIndexForthePixel(x2);
                float[] faX0 = new float[]{index1, index2};
                double[] faY0 = new double[]{graph.GDMgr.getYValueForthePixel(y1, pnlID), graph.GDMgr.getYValueForthePixel(y2, pnlID)};

                targetObj.setXArr(graph.GDMgr.convertXArrayIndexToTime(faX0));
                targetObj.setYArr(faY0);
                //targetObj.setIndexArray(faX0);

                dynamicChangeObjectProperties(origin, targetObj, graph);
            }
        });

        final DatePicker datePickerStart = new DatePicker(ChartFrame.getSharedInstance(), true);
        final DatePicker datePickerEnd = new DatePicker(ChartFrame.getSharedInstance(), true);

        btnStartDate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String[] delims = lblStartDate.getText().trim().split("-", 3);
                int date = Integer.parseInt(delims[2]);
                int month = Integer.parseInt(delims[1]) - 1; // 0 based index
                int year = Integer.parseInt(delims[0]);
                datePickerStart.setDate(year, month, date);

                int x = (int) btnStartDate.getLocationOnScreen().getX();
                int y = (int) btnStartDate.getLocationOnScreen().getY();
                datePickerStart.setLocation(x, y);
                datePickerStart.setVisible(true);
            }
        });

        btnEndDate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {


                String[] delims = lblEndDate.getText().trim().split("-", 3);
                int date = Integer.parseInt(delims[2]);
                int month = Integer.parseInt(delims[1]) - 1; // 0 based index
                int year = Integer.parseInt(delims[0]);
                datePickerEnd.setDate(year, month, date);

                int x = (int) btnEndDate.getLocationOnScreen().getX();
                int y = (int) btnEndDate.getLocationOnScreen().getY();
                datePickerEnd.setLocation(x, y);
                datePickerEnd.setVisible(true);
            }
        });

        final JTextField txtHoursStart = startTime.getTxtHours();
        final JTextField txtMinsStart = startTime.getTxtMins();
        datePickerStart.getCalendar().addDateSelectedListener(new DateSelectedListener() {

            public void dateSelected(Object source, int iYear, int iMonth, int iDay) {

                datePickerStart.setVisible(false);
                Calendar cal = Calendar.getInstance();
                int hours = 0;
                int mins = 0;
                if (graph.isCurrentMode()) {
                    hours = Integer.parseInt(txtHoursStart.getText().trim());
                    mins = Integer.parseInt(txtMinsStart.getText().trim());
                }
                cal.set(iYear, iMonth, iDay, hours, mins, 0);

                if (target instanceof AbstractObject) {

                    //finding the closest marlet open day for a given day.
                    float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{cal.getTimeInMillis()});
                    int x = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                    float index = graph.GDMgr.getIndexForthePixel(x);
                    long time = graph.GDMgr.getTimeMillisec((int) index);
                    //setting the sidplay date in labels
                    cal.setTimeInMillis(time);
                    String dateStr = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal.get(Calendar.DATE));
                    lblStartDate.setText(dateStr);
                    lineSlope.setXArr(new long[]{time, lineSlope.getXArr()[1]});
                    //lineSlope.setIndexArray(new float[]{index, lineSlope.getIndexArray()[1]});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) targetObj, graph);

                    String beginPrice = priceFormat.format(origin.getYArr()[0]);
                    txtStartPrice.setText(beginPrice);
                }
            }

        });

        final JTextField txtHoursEnd = endTime.getTxtHours();
        final JTextField txtMinsEnd = endTime.getTxtMins();
        datePickerEnd.getCalendar().addDateSelectedListener(new DateSelectedListener() {

            public void dateSelected(Object source, int iYear, int iMonth, int iDay) {

                datePickerEnd.setVisible(false);
                Calendar cal = Calendar.getInstance();
                int hours = 0;
                int mins = 0;
                if (graph.isCurrentMode()) {
                    hours = Integer.parseInt(txtHoursEnd.getText().trim());
                    mins = Integer.parseInt(txtMinsEnd.getText().trim());
                }
                cal.set(iYear, iMonth, iDay, hours, mins, 0);

                if (target instanceof AbstractObject) {

                    //finding the closest marlet open day for a given day.
                    float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{cal.getTimeInMillis()});
                    int x = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                    float index = graph.GDMgr.getIndexForthePixel(x);
                    long time = graph.GDMgr.getTimeMillisec((int) index);
                    //setting the sidplay date in labels
                    cal.setTimeInMillis(time);
                    String dateStr = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + String.valueOf(cal.get(Calendar.DATE));
                    lblEndDate.setText(dateStr);

                    lineSlope.setXArr(new long[]{lineSlope.getXArr()[0], time});
                    //lineSlope.setIndexArray(new float[]{lineSlope.getIndexArray()[0], index});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) targetObj, graph);

                    String endPrice = priceFormat.format(origin.getYArr()[1]);
                    txtEndPrice.setText(endPrice);
                }
            }

        });

        txtStartPrice.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent e) {

                try {
                    double startPrice = Double.parseDouble(txtStartPrice.getText());

                    lineSlope.setYArr(new double[]{startPrice, lineSlope.getYArr()[1]});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) targetObj, graph);
                } catch (NumberFormatException e1) {
                    //e1.printStackTrace();
                    double start = origin.getYArr()[0];
                    txtStartPrice.setText(priceFormat.format(start));
                }
            }
        });

        txtEndPrice.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent e) {

                try {
                    double endPrice = Double.parseDouble(txtEndPrice.getText());

                    lineSlope.setYArr(new double[]{lineSlope.getYArr()[0], endPrice});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) targetObj, graph);
                } catch (NumberFormatException e1) {
                    //e1.printStackTrace();
                    double start = origin.getYArr()[1];
                    txtEndPrice.setText(priceFormat.format(start));

                }
            }
        });

        txtHoursStart.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.

                String[] vals = lblStartDate.getText().split("-");
                int year = Integer.parseInt(vals[0]);
                int month = Integer.parseInt(vals[1]) - 1;
                int date = Integer.parseInt(vals[2]);
                if (txtMinsStart.getText().trim().length() == 0 || txtHoursStart.getText().trim().length() == 0) {
                    return;
                }
                int hours = Integer.parseInt(txtHoursStart.getText().trim());
                int mins = Integer.parseInt(txtMinsStart.getText().trim());

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, date, hours, mins, 0);  //sets the

                int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                long time = graph.GDMgr.getTimeMillisec(index);

                //graph.GDMgr.convertXArrayTimeToIndex();

                cal.setTimeInMillis(time);

                if (target instanceof AbstractObject) {
                    long[] xar = new long[]{time};
                    lineSlope.setXArr(new long[]{time, origin.getXArr()[1]});
                    //lineSlope.setIndexArray(new float[]{index, lineSlope.getIndexArray()[1]});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                }
            }
        });

        txtMinsStart.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
                String[] vals = lblEndDate.getText().split("-");
                int year = Integer.parseInt(vals[0]);
                int month = Integer.parseInt(vals[1]) - 1;
                int date = Integer.parseInt(vals[2]);
                if (txtMinsStart.getText().trim().length() == 0 || txtHoursStart.getText().trim().length() == 0) {
                    return;
                }
                int hours = Integer.parseInt(txtHoursStart.getText().trim());
                int mins = Integer.parseInt(txtMinsStart.getText().trim());

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, date, hours, mins, 0);
                int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                long time = graph.GDMgr.getTimeMillisec(index);

                cal.setTimeInMillis(time);

                if (target instanceof AbstractObject) {
                    long[] xar = new long[]{time};
                    lineSlope.setXArr(new long[]{time, origin.getXArr()[1]});
                    //lineSlope.setIndexArray(new float[]{index, lineSlope.getIndexArray()[1]});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                }
            }
        });

        txtHoursEnd.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.

                String[] vals = lblEndDate.getText().split("-");
                int year = Integer.parseInt(vals[0]);
                int month = Integer.parseInt(vals[1]) - 1;
                int date = Integer.parseInt(vals[2]);
                if (txtMinsEnd.getText().trim().length() == 0 || txtHoursEnd.getText().trim().length() == 0) {
                    return;
                }
                int hours = Integer.parseInt(txtHoursEnd.getText().trim());
                int mins = Integer.parseInt(txtMinsEnd.getText().trim());

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, date, hours, mins, 0);  //sets the

                int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                long time = graph.GDMgr.getTimeMillisec(index);

                //graph.GDMgr.convertXArrayTimeToIndex();

                cal.setTimeInMillis(time);

                if (target instanceof AbstractObject) {
                    long[] xar = new long[]{time};
                    lineSlope.setXArr(new long[]{origin.getXArr()[0], time});
                    //lineSlope.setIndexArray(new float[]{lineSlope.getIndexArray()[0], index});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                }
            }
        });

        txtMinsEnd.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
                String[] vals = lblEndDate.getText().split("-");
                int year = Integer.parseInt(vals[0]);
                int month = Integer.parseInt(vals[1]) - 1;
                int date = Integer.parseInt(vals[2]);
                if (txtMinsEnd.getText().trim().length() == 0 || txtHoursEnd.getText().trim().length() == 0) {
                    return;
                }
                int hours = Integer.parseInt(txtHoursEnd.getText().trim());
                int mins = Integer.parseInt(txtMinsEnd.getText().trim());

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, date, hours, mins, 0);
                int index = graph.GDMgr.getClosestIndexFortheTime(cal.getTimeInMillis());
                long time = graph.GDMgr.getTimeMillisec(index);

                cal.setTimeInMillis(time);

                if (target instanceof AbstractObject) {
                    long[] xar = new long[]{time};
                    lineSlope.setXArr(new long[]{origin.getXArr()[0], time});
                    //lineSlope.setIndexArray(new float[]{lineSlope.getIndexArray()[0], index});
                    dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                }
            }
        });

        pnlTopFirst.add(lblExtendLeft);
        pnlTopFirst.add(cbExtendLeft);
        pnlTopFirst.add(new JPanel());
        pnlTopFirst.add(lblShowPctchange);
        pnlTopFirst.add(cbShowPctChnge);

        pnlTopFirst.add(lblExtendRight);
        pnlTopFirst.add(cbExtendRight);
        pnlTopFirst.add(new JPanel());
        pnlTopFirst.add(lblBarCount);
        pnlTopFirst.add(cbShowBarCount);

        pnlTopSecond.add(lblShowNetChange);
        pnlTopSecond.add(cbShowNetChange);
        pnlTopSecond.add(new JPanel());
        pnlTopSecond.add(chkAngle);
        pnlTopSecond.add(new JPanel());
        pnlTopSecond.add(lblAngle);
        pnlTopSecond.add(spnAngle);

        pnlTop.add(pnlTopFirst);
        pnlTop.add(pnlTopSecond);
        panel.add(pnlTop);
        panel.add(pnlAdvance);

        GUISettings.applyOrientation(pnlAdvance);
        GUISettings.applyOrientation(pnlTop);
        return panel;
    }

    // added by Mevan @ 2009-06-30
    private static JPanel createCycleLineSettingsPanel(final AbstractObject origin, final AbstractObject target,
                                                       final StockGraph graph) {
        JPanel panel = new JPanel();
        panel.setSize(DIM_DIALOG);
        panel.setLayout(null);

        GraphLabel lblCycleMode = new GraphLabel(Language.getString("GRAPH_CYCLE_SELECT_MODE"));
        lblCycleMode.setBounds(LEFT, 2, 100, LBL_HEIGHT);

        JComboBox cmbCycleMode = new JComboBox(new String[]{Language.getString("GRAPH_MODE_CYCLE"), Language.getString("GRAPH_MODE_VERTICAL")});
        cmbCycleMode.setBounds(LEFT, 32, 100, CMB_HEIGHT);
        cmbCycleMode.setEditable(false);
        cmbCycleMode.setSelectedIndex(((ObjectCycleLine) target).getDefaultType());

        cmbCycleMode.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();

                    byte type = (byte) cmb.getSelectedIndex();
                    ((ObjectCycleLine) target).setDefaultType(type);
                }

                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        panel.add(lblCycleMode);
        panel.add(cmbCycleMode);


        return panel;
    }

    // added - Pramoda (2006-03-02)
    private static JPanel createStdErrParametersTab(final ObjectStandardError origin, final ObjectStandardError target,
                                                    final StockGraph graph) {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%",}, new String[]{"40", "20"}, 10, 5));
        JPanel panelTop = new JPanel(new FlexGridLayout(new String[]{"35%", "10%"}, new String[]{"20", "20"}, 0, 0));
        JPanel panelBottom = new JPanel(new FlexGridLayout(new String[]{"28%", "13%"}, new String[]{"20"}, 0, 0));
        panel.setSize(DIM_DIALOG);
        //panel.setLayout(null);

        GraphLabel lblExtendLeft = new GraphLabel(Language.getString("EXTEND_LEFT"));
        //lblExtendLeft.setBounds(LEFT, 2, 100, LBL_HEIGHT);
        final JCheckBox cbExtendLeft = new JCheckBox();
        //cbExtendLeft.setBounds(LEFT + 100, 2, 100, LBL_HEIGHT);
        cbExtendLeft.setSelected(target.isExtendedLeft());
        cbExtendLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendLeft.isSelected();
                target.setExtendedLeft(checked);
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        GraphLabel lblExtendRight = new GraphLabel(Language.getString("EXTEND_RIGHT"));
        //lblExtendRight.setBounds(LEFT, 4 + LBL_HEIGHT, 100, LBL_HEIGHT);
        final JCheckBox cbExtendRight = new JCheckBox();
        //cbExtendRight.setBounds(LEFT + 100, 4 + LBL_HEIGHT, 100, LBL_HEIGHT);
        cbExtendRight.setSelected(target.isExtendedRight());
        cbExtendRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean checked = cbExtendRight.isSelected();
                target.setExtendedRight(checked);
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        GraphLabel lblUnits = new GraphLabel(Language.getString("UNITS"));
        //lblUnits.setBounds(LEFT, 6 + LBL_HEIGHT * 2, 100, LBL_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(2, 1, 100, 1);
        final JSpinner spinUnits = new JSpinner(model);
        //spinUnits.setBounds(LEFT + 100, 6 + LBL_HEIGHT * 2, 50, LBL_HEIGHT);
        spinUnits.getModel().setValue(target.getUnits());
        spinUnits.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinUnits.getModel()).getNumber().intValue();
                    target.setUnits(val);
                    dynamicChangeObjectProperties(origin, target, graph);
                }
            }
        });

        panelTop.add(lblExtendLeft);
        panelTop.add(cbExtendLeft);
        panelTop.add(lblExtendRight);
        panelTop.add(cbExtendRight);
        panelBottom.add(lblUnits);
        panelBottom.add(spinUnits);
        panel.add(panelTop);
        panel.add(panelBottom);

        return panel;
    }

    private static JPanel createObjectTextPanel(final ObjectTextNote origin, final ObjectTextNote target,
                                                final StockGraph graph) {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"40%", "60%"}, 10, 5));
        JPanel bottomPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "10%", "40%", "10%"}, new String[]{"18", "20", "20", "20"}, 0, 5));
        panel.setSize(DIM_DIALOG);

        int textH = 80;
        JTextArea text = getTextArea(target, LEFT, LEFT, DIM_DIALOG.width - 2 * LEFT, textH);

        GraphLabel lblTextColor = new GraphLabel(Language.getString("TEXT_COLOR"));
        JComboBox cmbTextColor = getColorCombo(origin, target, graph, METHOD_SET_FONT_COLOR,
                target.getFontColor(), LEFT, 3 * GAP + CMB_HEIGHT + textH, 100, CMB_HEIGHT);

        GraphLabel lblFillColor = new GraphLabel(Language.getString("BACK_COLOR"));
        JComboBox cmbFillColor = getColorCombo(origin, target, graph, METHOD_SET_FILL_COLOR,
                target.getFillColor(), LEFT + 130, 3 * GAP + CMB_HEIGHT + textH, 100, CMB_HEIGHT);

        JButton btnFont = getFontSelectButton(origin, target, graph, LEFT, LEFT + 4 * GAP + 2 * CMB_HEIGHT + textH, 100, CMB_HEIGHT);

        final JCheckBox chkTransparent = new JCheckBox(Language.getString("TRANSPARENT"));
        chkTransparent.setSelected(target.isTransparent());
        chkTransparent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setTransparent(chkTransparent.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });

        final JCheckBox chkShowBorder = new JCheckBox(Language.getString("SHOW_BORDER"));
        chkShowBorder.setSelected(target.isShowBorder());
        chkShowBorder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setShowBorder(chkShowBorder.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });


        panel.add(text);

        bottomPanel.add(lblTextColor);
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(lblFillColor);
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(cmbTextColor);
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(cmbFillColor);
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(btnFont);
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(chkTransparent);
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(new JLabel(""));
        bottomPanel.add(chkShowBorder);

        panel.add(bottomPanel);
        return panel;
    }

    private static JButton getFontSelectButton(final TextContainer origin, final TextContainer target,
                                               final StockGraph graph, int x, int y, int w, int h) {
        JButton btnFont = new JButton(Language.getString("FONT"));
        btnFont.setBounds(x, y, w, h);
        btnFont.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                FontChooser oFontChooser = new FontChooser(Client.getInstance().getFrame(), true);
                if (target instanceof AbstractObject) {
                    Font oFont = oFontChooser.showDialog(Language.getString("SELECT_FONT"), target.getFont());
                    if (oFont != null) {
                        target.setFont(oFont);
                    }
                    oFont = null;
                }
                oFontChooser = null;
                dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
            }
        });
        return btnFont;
    }

    private static JTextArea getTextArea(final TextContainer target, int x, int y, int w, int h) {
        final JTextArea scroll = new JTextArea();
        scroll.setBounds(x, y, w, h);
        scroll.setText(target.getText());
        //Object[] msg = {Language.getString("ENTER_TEXT"), scroll};
        scroll.setLineWrap(true);
        scroll.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                target.setText(scroll.getText());
            }
        });
        return scroll;
    }

    private static TWTabbedPane createMovingAveragePanes(final ChartProperties origin, final ChartProperties target,
                                                         int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"20", "20", "20", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((MovingAverage) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((MovingAverage) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMethod = new GraphLabel(Language.getString("METHOD"));
        //lblMethod.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbMethod = new JComboBox();
        //cmbMethod.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        String[] sa = MovingAverage.getMethodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbMethod.addItem(sa[i]);
            if (((MovingAverage) target).getMethod() == i) {
                cmbMethod.setSelectedIndex(i);
            }
        }
        cmbMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((MovingAverage) target).setMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        /*final JCheckBox chkSetDefault = new JCheckBox(Language.getString("SET_AS_DEFAULT2"));
        chkSetDefault.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                target.setUsingUserDefault(chkSetDefault.isSelected());
                dynamicChangeObjectProperties(origin, target, graph);
            }
        });*/

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);
        paramPanel.add(lblMethod);
        paramPanel.add(cmbMethod);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createMACDPanes(final ChartProperties origin, final ChartProperties target,
                                                int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"50%"}, new String[]{"20", "20", "90"}, 10, 2));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("SIGNAL_TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 130, CMB_HEIGHT);


        // ToDo : add 2 more spinners for signal time perjiods high & low

        /********************************************spinner 12 - Low ************************************/
        SpinnerNumberModel modelLow_12 = new SpinnerNumberModel(20, 2, 26, 1);
        final JSpinner spinTimePeriodsLow_12 = new JSpinner(modelLow_12);
        //spinTimePeriodsLow_12.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 50, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriodsLow_12.getModel()).setValue(new Integer(((MACD) target).getMACD_TimePeriods_Low()));
        spinTimePeriodsLow_12.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriodsLow_12.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((MACD) target).setMACD_TimePeriods_Low(val);
                    ((MACD) origin).getHistogram().setMACD_TimePeriods_Low(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        /********************************************************************************/
        /********************************************spinner 26 - High  ************************************/
        SpinnerNumberModel modelHigh_26 = new SpinnerNumberModel(27, 26, 1000, 1);
        final JSpinner spinTimePeriodsHIgh_26 = new JSpinner(modelHigh_26);
        //spinTimePeriodsHIgh_26.setBounds(LEFT + 50, 2 * GAP + CMB_HEIGHT, 50, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();t
        ((SpinnerNumberModel) spinTimePeriodsHIgh_26.getModel()).setValue(new Integer(((MACD) target).getMACD_TimePeriods_High()));
        spinTimePeriodsHIgh_26.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriodsHIgh_26.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((MACD) target).setMACD_TimePeriods_High(val);
                    ((MACD) origin).getHistogram().setMACD_TimePeriods_High(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        /********************************************************************************/

        /********************************************spinner 12 - signal time period   9************************************/
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT + 50 + 50, 2 * GAP + CMB_HEIGHT, 50, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((MACD) target).getSignalTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((MACD) target).setSignalTimePeriods(val);
                    ((MACD) origin).getHistogram().setSignalTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        /********************************************************************************/

        Color sigColor;
        byte sigStyle;
        if (((MACD) target).getSignalLine() != null) {
            sigColor = ((MACD) target).getSignalLine().getColor();
            sigStyle = ((MACD) target).getSignalLine().getPenStyle();
        } else {
            sigColor = ((MACD) target).getSignalColor();
            sigStyle = ((MACD) target).getSignalStyle();
        }
        GraphLabel lblSignalColor = new GraphLabel(Language.getString("LINE_COLOR"));
        //lblSignalColor.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbSignalColor = getColorCombo(origin, target, graph, METHOD_SIGNAL_COLOR, sigColor, LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblSignalStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        //lblSignalStyle.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, 80, CMB_HEIGHT);
        JComboBox cmbSignalStyle = getPenStyleCombo(origin, target, graph, sigStyle, LEFT, 6 * GAP + 5 * CMB_HEIGHT, 100, CMB_HEIGHT);

        paramPanel.add(lblTimePeriods);

        JPanel timePanel = new JPanel(new FlexGridLayout(new String[]{"30%", "30%", "30%"}, new String[]{"20"}, 0, 0));
        timePanel.add(spinTimePeriodsLow_12);
        timePanel.add(spinTimePeriodsHIgh_26);
        timePanel.add(spinTimePeriods);
        paramPanel.add(timePanel);

        JPanel bottomPanel = new JPanel(new FlexGridLayout(new String[]{"70%"}, new String[]{"20", "20", "20", "20"}, 0, 2));
        bottomPanel.add(lblSignalColor);
        bottomPanel.add(cmbSignalColor);
        bottomPanel.add(lblSignalStyle);
        bottomPanel.add(cmbSignalStyle);

        paramPanel.add(bottomPanel);
        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createMACDVolumePanes(final ChartProperties origin, final ChartProperties target,
                                                      int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20", "18", "20", "18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("SIGNAL_TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 130, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((MACDVolume) target).getSignalTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((MACDVolume) target).setSignalTimePeriods(val);
                    ((MACDVolume) origin).getHistogram().setSignalTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        Color sigColor;
        byte sigStyle;
        if (((MACDVolume) target).getSignalLine() != null) {
            sigColor = ((MACDVolume) target).getSignalLine().getColor();
            sigStyle = ((MACDVolume) target).getSignalLine().getPenStyle();
        } else {
            sigColor = ((MACDVolume) target).getSignalColor();
            sigStyle = ((MACDVolume) target).getSignalStyle();
        }
        GraphLabel lblSignalColor = new GraphLabel(Language.getString("LINE_COLOR"));
        //lblSignalColor.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbSignalColor = getColorCombo(origin, target, graph, METHOD_SIGNAL_COLOR, sigColor, LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblSignalStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        //lblSignalStyle.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, 80, CMB_HEIGHT);
        JComboBox cmbSignalStyle = getPenStyleCombo(origin, target, graph, sigStyle, LEFT, 6 * GAP + 5 * CMB_HEIGHT, 100, CMB_HEIGHT);

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);
        paramPanel.add(lblSignalColor);
        paramPanel.add(cmbSignalColor);
        paramPanel.add(lblSignalStyle);
        paramPanel.add(cmbSignalStyle);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createRSIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((RSI) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((RSI) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }//craete RSI Panes

    private static TWTabbedPane createMomentumPanes(final ChartProperties origin, final ChartProperties target,
                                                    int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"20", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((Momentum) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((Momentum) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }// create Momemntum Panes

    private static TWTabbedPane createLRIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"20", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((LinearRegression) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((LinearRegression) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }// create LinearRegression panes

    private static TWTabbedPane createSDIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"20", "20", "20", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((StandardDeviation) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((StandardDeviation) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblDeviations = new GraphLabel(Language.getString("DEVIATIONS"));
        //lblDeviations.setBounds(GAP, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        SpinnerNumberModel modelDev = new SpinnerNumberModel(2d, 0.01d, 100d, 0.01d);
        final JSpinner spinDeviations = new JSpinner(modelDev);
        //spinDeviations.setBounds(GAP, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinDeviations.getModel()).setValue(new Float(((StandardDeviation) target).getDeviations()));
        spinDeviations.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    float val = ((SpinnerNumberModel) spinDeviations.getModel()).getNumber().floatValue();
                    //System.out.println("spinDeviations stateChanged "+val);
                    ((StandardDeviation) target).setDeviations(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);
        paramPanel.add(lblDeviations);
        paramPanel.add(spinDeviations);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }// create SDI panes

    private static TWTabbedPane createBollingerPanes(final ChartProperties origin, final ChartProperties target,
                                                     int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "5%", "45%"}, new String[]{"100%"}, 0, 5));
        paramPanel.setSize(DIM_DIALOG);
        final JComboBox cmbFillColor = getColorCombo(origin, target, graph, METHOD_SET_FILL_COLOR,
                ((BollingerBand) target).getFillColor(), LEFT + 130, 3 * GAP + CMB_HEIGHT, 60, CMB_HEIGHT);
        final JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 150, ((BollingerBand) target).getTransparency());
        cmbFillColor.setEnabled(true);
        slider.setEnabled(true);
        final JCheckBox cbFillBand = new JCheckBox(Language.getString("FILL_BAND"));
        cbFillBand.setFont(Theme.getDefaultFont());
        cbFillBand.setSelected(((BollingerBand) target).isFillBands());
        cbFillBand.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (cbFillBand.isSelected()) {
                    cmbFillColor.setEnabled(true);
                    slider.setEnabled(true);
                    // ((BollingerBand) target).setFillBands(cbFillBand.isSelected());
                    //dynamicChangeChartProperties(origin, target, graph);
                } else {
                    cmbFillColor.setEnabled(false);
                    // ((BollingerBand) target).setFillBands(cbFillBand.isSelected());
                    slider.setEnabled(false);
                    //dynamicChangeChartProperties(origin, target, graph);

                }

                ((BollingerBand) target).setFillBands(cbFillBand.isSelected());
                dynamicChangeChartProperties(origin, target, graph);
            }
        });

        GraphLabel lblFillColor = new GraphLabel(Language.getString("FILL_COLOR"));


        GraphLabel lblTransparency = new GraphLabel(Language.getString("TRANSPARENCY"));
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                int val = (int) source.getValue();
                ((BollingerBand) target).setTransparency(val);
                dynamicChangeChartProperties(origin, target, graph);
            }
        });
        slider.setMajorTickSpacing(10);
        slider.setMinorTickSpacing(1);
        slider.setPaintTicks(false);

        JPanel rightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "30"}, 10, 5));
        rightPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("BAND_FILL"), TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        rightPanel.add(cbFillBand);
        rightPanel.add(lblFillColor);
        rightPanel.add(cmbFillColor);
        rightPanel.add(lblTransparency);
        rightPanel.add(slider);

        //leftPanel panel
        JPanel leftPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "20", "20"}, 10, 5));

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((BollingerBand) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((BollingerBand) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMethod = new GraphLabel(Language.getString("METHOD"));
        //lblMethod.setBounds(GAP, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbMethod = new JComboBox();
        //cmbMethod.setBounds(GAP, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbMethod.setEditable(false);
        String[] sa = MovingAverage.getMethodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbMethod.addItem(sa[i]);
            if (((MovingAverage) target).getMethod() == i) {
                cmbMethod.setSelectedIndex(i);
            }
        }
        cmbMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((MovingAverage) target).setMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblDeviations = new GraphLabel(Language.getString("DEVIATIONS"));
        //lblDeviations.setBounds(GAP, 5 * GAP + 4 * CMB_HEIGHT, 100, CMB_HEIGHT);
        SpinnerNumberModel modelDev = new SpinnerNumberModel(2d, 0.01d, 100d, 0.01d);
        final JSpinner spinDeviations = new JSpinner(modelDev);
        //spinDeviations.setBounds(GAP, 6 * GAP + 5 * CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinDeviations.getModel()).setValue(new Float(((BollingerBand) target).getDeviations()));
        spinDeviations.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    float val = ((SpinnerNumberModel) spinDeviations.getModel()).getNumber().floatValue();
                    //System.out.println("spinDeviations stateChanged "+val);
                    ((BollingerBand) target).setDeviations(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        leftPanel.add(lblTimePeriods);
        leftPanel.add(spinTimePeriods);
        leftPanel.add(lblMethod);
        leftPanel.add(cmbMethod);
        leftPanel.add(lblDeviations);
        leftPanel.add(spinDeviations);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        paramPanel.add(leftPanel);
        paramPanel.add(new JLabel());
        if ((((BollingerBand) target).getBandType() == BollingerBand.BT_CENTER) ||
                (((BollingerBand) target).getBandType() == BollingerBand.BT_LOWER) ||
                (((BollingerBand) target).getBandType() == BollingerBand.BT_UPPER)) {
            paramPanel.add(rightPanel);
        }

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }//Bollinger panes

    // Keltner Channels Panes
    private static TWTabbedPane createKeltnerChannelPanes(final ChartProperties origin, final ChartProperties target,
                                                          int tabIndex, final StockGraph graph) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel leftPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "20", "18", "20"}, 10, 5));
        leftPanel.setSize(DIM_DIALOG);

        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "5%", "45%"}, new String[]{"100%"}, 0, 5));
        paramPanel.setSize(DIM_DIALOG);
        final JComboBox cmbFillColor = getColorCombo(origin, target, graph, METHOD_SET_FILL_COLOR,
                ((KeltnerChannels) target).getFillColor(), LEFT + 130, 3 * GAP + CMB_HEIGHT, 60, CMB_HEIGHT);
        final JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 150, ((KeltnerChannels) target).getTransparency());
        cmbFillColor.setEnabled(true);
        slider.setEnabled(true);
        final JCheckBox cbFillBand = new JCheckBox(Language.getString("FILL_BAND"));
        cbFillBand.setFont(Theme.getDefaultFont());
        cbFillBand.setSelected(((KeltnerChannels) target).isFillBands());
        cbFillBand.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (cbFillBand.isSelected()) {
                    cmbFillColor.setEnabled(true);
                    slider.setEnabled(true);
                    ((KeltnerChannels) target).setFillBands(cbFillBand.isSelected());
                    dynamicChangeChartProperties(origin, target, graph);
                } else {
                    cmbFillColor.setEnabled(false);
                    slider.setEnabled(false);
                    ((KeltnerChannels) target).setFillBands(false);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblFillColor = new GraphLabel(Language.getString("FILL_COLOR"));


        GraphLabel lblTransparency = new GraphLabel(Language.getString("TRANSPARENCY"));
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                int val = (int) source.getValue();
                ((KeltnerChannels) target).setTransparency(val);
                dynamicChangeChartProperties(origin, target, graph);
            }
        });
        slider.setMajorTickSpacing(10);
        slider.setMinorTickSpacing(1);
        slider.setPaintTicks(false);

        JPanel rightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "30"}, 10, 5));
        rightPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("BAND_FILL"), TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        rightPanel.add(cbFillBand);
        rightPanel.add(lblFillColor);
        rightPanel.add(cmbFillColor);
        rightPanel.add(lblTransparency);
        rightPanel.add(slider);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((KeltnerChannels) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((KeltnerChannels) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblDeviations = new GraphLabel(Language.getString("DEVIATIONS"));
        //lblDeviations.setBounds(GAP, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        SpinnerNumberModel modelDev = new SpinnerNumberModel(2d, 0.01d, 100d, 0.01d);
        final JSpinner spinDeviations = new JSpinner(modelDev);
        //spinDeviations.setBounds(GAP, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinDeviations.getModel()).setValue(new Float(((KeltnerChannels) target).getDeviations()));
        spinDeviations.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    float val = ((SpinnerNumberModel) spinDeviations.getModel()).getNumber().floatValue();
                    //System.out.println("spinDeviations stateChanged "+val);
                    ((KeltnerChannels) target).setDeviations(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        leftPanel.add(lblTimePeriods);
        leftPanel.add(spinTimePeriods);
        leftPanel.add(lblDeviations);
        leftPanel.add(spinDeviations);

        paramPanel.add(leftPanel);
        paramPanel.add(new JLabel());
        if ((((KeltnerChannels) origin).getKeltnerChannelType() == KeltnerChannels.KC_CENTER) ||
                (((KeltnerChannels) origin).getKeltnerChannelType() == KeltnerChannels.KC_LOWER) ||
                (((KeltnerChannels) origin).getKeltnerChannelType() == KeltnerChannels.KC_UPPER)) {
            paramPanel.add(rightPanel);
        }

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
//        if (showSources) {
//            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
//        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    //AccumulationDistribution
    private static TWTabbedPane createAccDistriPanes(final ChartProperties origin, final ChartProperties target,
                                                     final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.setSelectedIndex(0);

        return tabbedPane;
    }// AccumulationDistribution Panes

    private static TWTabbedPane createStocasicOscillPanes(final ChartProperties origin, final ChartProperties target,
                                                          int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"45%", "5%", "45%", "5%"}, new String[]{"18", "20", "18", "20", "18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(14, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((StocasticOscillator) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((StocasticOscillator) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMethod = new GraphLabel(Language.getString("METHOD"));
        lblMethod.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbMethod = new JComboBox();
        cmbMethod.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        String[] sa = StocasticOscillator.getMethodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbMethod.addItem(sa[i]);
            if (((StocasticOscillator) target).getMethod() == i) {
                cmbMethod.setSelectedIndex(i);
            }
        }
        cmbMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((StocasticOscillator) target).setMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        //shas
        //paramPanel.add(lblTimePeriods);
        //paramPanel.add(spinTimePeriods);
//        paramPanel.add(lblMethod);
//        paramPanel.add(cmbMethod);

        // Slowing periods - Pramoda
        GraphLabel lblSlownessPeriods = new GraphLabel(Language.getString("SLOWING_PERIOD"));
        lblSlownessPeriods.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        SpinnerNumberModel slownessModel = new SpinnerNumberModel(3, 1, 1000, 1);
        final JSpinner slownessSpinTimePeriods = new JSpinner(slownessModel);
        slownessSpinTimePeriods.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) slownessSpinTimePeriods.getModel()).setValue(
                new Integer(((StocasticOscillator) target).getSlowingPeriods()));
        slownessSpinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) slownessSpinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinSlownessPeriods stateChanged " + val);
                    ((StocasticOscillator) target).setSlowingPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        //shas
        //paramPanel.add(lblSlownessPeriods);
        //paramPanel.add(slownessSpinTimePeriods);

        //signal line
        GraphLabel lblSigTimePeriods = new GraphLabel(Language.getString("SIGNAL_TIME_PERIODS"));
        lblSigTimePeriods.setBounds(GAP + 170, GAP, 130, CMB_HEIGHT);
        SpinnerNumberModel modelSig = new SpinnerNumberModel(3, 2, 1000, 1);
        final JSpinner spinSigTimePeriods = new JSpinner(modelSig);
        spinSigTimePeriods.setBounds(GAP + 170, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinSigTimePeriods.getModel()).setValue(new Integer(((StocasticOscillator) target).getSignalTimePeriods()));
        spinSigTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinSigTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((StocasticOscillator) target).setSignalTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        Color sigColor;
        byte sigStyle;
        if (((StocasticOscillator) target).getSignalLine() != null) {
            sigColor = ((StocasticOscillator) target).getSignalLine().getColor();
            sigStyle = ((StocasticOscillator) target).getSignalLine().getPenStyle();
        } else {
            sigColor = ((StocasticOscillator) target).getSignalColor();
            sigStyle = ((StocasticOscillator) target).getSignalStyle();
        }
        GraphLabel lblSignalColor = new GraphLabel(Language.getString("LINE_COLOR"));
        lblSignalColor.setBounds(GAP + 170, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbSignalColor = getColorCombo(origin, target, graph, METHOD_SIGNAL_COLOR, sigColor, GAP + 170, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblSignalStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        lblSignalStyle.setBounds(GAP + 170, 5 * GAP + 4 * CMB_HEIGHT, 80, CMB_HEIGHT);
        JComboBox cmbSignalStyle = getPenStyleCombo(origin, target, graph, sigStyle, GAP + 170, 6 * GAP + 5 * CMB_HEIGHT, 100, CMB_HEIGHT);

        paramPanel.add(lblTimePeriods);
        paramPanel.add(new JLabel());
        paramPanel.add(lblSigTimePeriods);
        paramPanel.add(new JLabel());
        paramPanel.add(spinTimePeriods);
        paramPanel.add(new JLabel());
        paramPanel.add(spinSigTimePeriods);
        paramPanel.add(new JLabel());
        paramPanel.add(lblSlownessPeriods);
        paramPanel.add(new JLabel());
        paramPanel.add(lblSignalColor);
        paramPanel.add(new JLabel());
        paramPanel.add(slownessSpinTimePeriods);
        paramPanel.add(new JLabel());
        paramPanel.add(cmbSignalColor);
        paramPanel.add(new JLabel());
        paramPanel.add(new JLabel());
        paramPanel.add(new JLabel());
        paramPanel.add(lblSignalStyle);
        paramPanel.add(new JLabel());
        paramPanel.add(new JLabel());
        paramPanel.add(new JLabel());
        paramPanel.add(cmbSignalStyle);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //AccumulationDistribution

    //Stocastic oscillator

    private static TWTabbedPane createWillsRPanes(final ChartProperties origin, final ChartProperties target,
                                                  int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"20", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(14, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((WilliamsR) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((WilliamsR) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //Stocastic oscillator

    //William % R

    private static TWTabbedPane createAvgTrueRangePanes(final ChartProperties origin, final ChartProperties target,
                                                        int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(14, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((AverageTrueRange) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((AverageTrueRange) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }//AvgTrueRange
    //William % R

    //AvgTrueRange

    private static TWTabbedPane createMassIndexPanes(final ChartProperties origin, final ChartProperties target,
                                                     int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(25, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((MassIndex) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((MassIndex) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //AvgTrueRange

    //MassIndex

    private static TWTabbedPane createMedianPricePanes(final ChartProperties origin, final ChartProperties target,
                                                       final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.setSelectedIndex(0);

        return tabbedPane;
    }
    //MassIndex

    //MedianPrice

    private static TWTabbedPane createMoneyFlowIndexPanes(final ChartProperties origin, final ChartProperties target,
                                                          int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(14, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((MoneyFlowIndex) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((MoneyFlowIndex) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //MedianPrice

    //MoneyFlowIndex

    private static TWTabbedPane createTrixPanes(final ChartProperties origin, final ChartProperties target,
                                                int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"30%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(12, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((TRIX) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((TRIX) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //MoneyFlowIndex

    //TRIX

    private static TWTabbedPane createVolatilityPanes(final ChartProperties origin, final ChartProperties target,
                                                      int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"18", "20", "18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("VOL_MOVING_AVG"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(10, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((Volatility) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((Volatility) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblRoc = new GraphLabel(Language.getString("VOL_ROC"));
        //lblRoc.setBounds(GAP, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        SpinnerNumberModel modelRoc = new SpinnerNumberModel(10, 1, 100, 1);
        final JSpinner spinRoc = new JSpinner(modelRoc);
        //spinRoc.setBounds(GAP, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinRoc.getModel()).setValue(new Integer(((Volatility) target).getRateOfChange()));
        spinRoc.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinRoc.getModel()).getNumber().intValue();
                    ((Volatility) target).setRateOfChange(val);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);
        paramPanel.add(lblRoc);
        paramPanel.add(spinRoc);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //Trix

    //Volatility

    private static TWTabbedPane createZigZagPanes(final ChartProperties origin, final ChartProperties target,
                                                  int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"18", "20", "18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        //GraphLabel lblAmount = new GraphLabel(Language.getString("TIME_PERIODS"));
        GraphLabel lblAmount = new GraphLabel(Language.getString("CI_REVERSALAMOUNT"));
        //lblAmount.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(5.0f, 0.001f, 1000f, 1.0f);
        final JSpinner spinAmount = new JSpinner(model);
        //spinAmount.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinAmount.getModel()).setValue(new Float(((ZigZag) target).getReversalAmount()));
        spinAmount.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    float val = ((SpinnerNumberModel) spinAmount.getModel()).getNumber().floatValue();
                    ((ZigZag) target).setReversalAmount(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblMethod = new GraphLabel(Language.getString("METHOD"));
        //lblMethod.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbMethod = new JComboBox();
        //cmbMethod.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        String[] sa = ZigZag.getMethodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbMethod.addItem(sa[i]);
            if (((ZigZag) target).getMethod() == i) {
                cmbMethod.setSelectedIndex(i);
            }
        }
        cmbMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((ZigZag) target).setMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblAmount);
        paramPanel.add(spinAmount);
        paramPanel.add(lblMethod);
        paramPanel.add(cmbMethod);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //Volatility

    //ZigZag

    private static TWTabbedPane createAroonPanes(final ChartProperties origin, final ChartProperties target,
                                                 int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(14, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((Aroon) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((Aroon) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //ZigZag

    //Aroon

    private static TWTabbedPane createChakinOscillatorPanes(final ChartProperties origin, final ChartProperties target,
                                                            final StockGraph graph) {
        return createDefaultTabbedPane(origin, target, graph);
    }
    //Aroon

    //Chakin Oscillator

    private static TWTabbedPane createCCIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(14, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((CommodityChannelIndex) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((CommodityChannelIndex) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //Chakin Oscillator

    //CCI

    private static TWTabbedPane createChaikinMoneyFlowPanes(final ChartProperties origin, final ChartProperties target,
                                                            int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(14, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((ChaikinMoneyFlow) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((ChaikinMoneyFlow) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //CCI

    //ChaikinMoneyFlow

    private static TWTabbedPane createEaseOfMovementPanes(final ChartProperties origin, final ChartProperties target,
                                                          int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((EaseOfMovement) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((EaseOfMovement) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMethod = new GraphLabel(Language.getString("METHOD"));
        //lblMethod.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbMethod = new JComboBox();
        //cmbMethod.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        String[] sa = MovingAverage.getMethodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbMethod.addItem(sa[i]);
            if (((EaseOfMovement) target).getMethod() == i) {
                cmbMethod.setSelectedIndex(i);
            }
        }
        cmbMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((EaseOfMovement) target).setMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);
        paramPanel.add(lblMethod);
        paramPanel.add(cmbMethod);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //ChaikinMoneyFlow

    //EaseOfMovement

    private static TWTabbedPane createEnvelopePanes(final ChartProperties origin, final ChartProperties target,
                                                    int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel leftPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "20", "18", "20"}, 10, 5));
        leftPanel.setSize(DIM_DIALOG);

        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "5%", "45%"}, new String[]{"100%"}, 0, 5));
        paramPanel.setSize(DIM_DIALOG);
        final JComboBox cmbFillColor = getColorCombo(origin, target, graph, METHOD_SET_FILL_COLOR,
                ((Envelope) target).getFillColor(), LEFT + 130, 3 * GAP + CMB_HEIGHT, 60, CMB_HEIGHT);
        final JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 150, ((Envelope) target).getTransparency());
        cmbFillColor.setEnabled(true);
        slider.setEnabled(true);
        final JCheckBox cbFillBand = new JCheckBox(Language.getString("FILL_BAND"));
        cbFillBand.setFont(Theme.getDefaultFont());
        cbFillBand.setSelected(((Envelope) target).isFillBands());
        cbFillBand.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (cbFillBand.isSelected()) {
                    cmbFillColor.setEnabled(true);
                    slider.setEnabled(true);
                    ((Envelope) target).setFillBands(cbFillBand.isSelected());
                    dynamicChangeChartProperties(origin, target, graph);
                } else {
                    cmbFillColor.setEnabled(false);
                    slider.setEnabled(false);
                    ((Envelope) target).setFillBands(false);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblFillColor = new GraphLabel(Language.getString("FILL_COLOR"));


        GraphLabel lblTransparency = new GraphLabel(Language.getString("TRANSPARENCY"));
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                int val = (int) source.getValue();
                ((Envelope) target).setTransparency(val);
                dynamicChangeChartProperties(origin, target, graph);
            }
        });
        slider.setMajorTickSpacing(10);
        slider.setMinorTickSpacing(1);
        slider.setPaintTicks(false);

        JPanel rightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "30"}, 10, 5));
        rightPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("BAND_FILL"), TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        rightPanel.add(cbFillBand);
        rightPanel.add(lblFillColor);
        rightPanel.add(cmbFillColor);
        rightPanel.add(lblTransparency);
        rightPanel.add(slider);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((Envelope) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((Envelope) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMethod = new GraphLabel(Language.getString("METHOD"));
        //lblMethod.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbMethod = new JComboBox();
        //cmbMethod.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        String[] sa = MovingAverage.getMethodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbMethod.addItem(sa[i]);
            if (((Envelope) target).getMethod() == i) {
                cmbMethod.setSelectedIndex(i);
            }
        }
        cmbMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((Envelope) target).setMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        leftPanel.add(lblTimePeriods);
        leftPanel.add(spinTimePeriods);
        leftPanel.add(lblMethod);
        leftPanel.add(cmbMethod);

        paramPanel.add(leftPanel);
        paramPanel.add(new JLabel());
        if ((((Envelope) target).getStyle() == Envelope.ENVELOPE_TOP) || (((Envelope) target).getStyle() == Envelope.ENVELOPE_BOTTOM)) {
            paramPanel.add(rightPanel);
        }

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //EaseOfMovement

    //Envelope

    private static TWTabbedPane createIchimokuKinkoHyoPanes(final ChartProperties origin, final ChartProperties target,
                                                            int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "20", "18", "20"}, 10, 5));
        JPanel firstPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 0, 0));
        JPanel secondPanel = new JPanel(new FlexGridLayout(new String[]{"35%", "15%", "35%", "15%"}, new String[]{"100%"}, 0, 0));
        JPanel thirdPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 0, 0));
        JPanel forthPanel = new JPanel(new FlexGridLayout(new String[]{"35%", "15%", "35%", "15%"}, new String[]{"100%"}, 0, 0));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTenkanPeriods = new GraphLabel(Language.getString("TENKAN_PERIODS"));
        //lblTenkanPeriods.setBounds(GAP, GAP, 140, CMB_HEIGHT);
        SpinnerNumberModel model1 = new SpinnerNumberModel(26, 2, 1000, 1);
        final JSpinner spinTenkanPeriods = new JSpinner(model1);
        //spinTenkanPeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTenkanPeriods.getModel()).setValue(new Integer(((IchimokuKinkoHyo) target).getTenkanSenPeriods()));
        spinTenkanPeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTenkanPeriods.getModel()).getNumber().intValue();
                    ((IchimokuKinkoHyo) target).setTenkanSenPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblKijunPeriods = new GraphLabel(Language.getString("KIJUN_PERIODS"));
        //lblKijunPeriods.setBounds(GAP, 3 * GAP + 2 * CMB_HEIGHT, 140, CMB_HEIGHT);
        SpinnerNumberModel model2 = new SpinnerNumberModel(26, 2, 1000, 1);
        final JSpinner spinKijunPeriods = new JSpinner(model2);
        //spinKijunPeriods.setBounds(GAP, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinKijunPeriods.getModel()).setValue(new Integer(((IchimokuKinkoHyo) target).getKijunSenPeriods()));
        spinKijunPeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinKijunPeriods.getModel()).getNumber().intValue();
                    ((IchimokuKinkoHyo) target).setKijunSenPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblSenkouPeriods = new GraphLabel(Language.getString("SENKOU_PERIODS"));
        //lblSenkouPeriods.setBounds(GAP + 145, GAP, 140, CMB_HEIGHT);
        SpinnerNumberModel model3 = new SpinnerNumberModel(52, 2, 1000, 1);
        final JSpinner spinSenkouPeriods = new JSpinner(model3);
        //spinSenkouPeriods.setBounds(GAP + 145, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinSenkouPeriods.getModel()).setValue(new Integer(((IchimokuKinkoHyo) target).getSenkouSpanPeriods()));
        spinSenkouPeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinSenkouPeriods.getModel()).getNumber().intValue();
                    ((IchimokuKinkoHyo) target).setSenkouSpanPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblChikouPeriods = new GraphLabel(Language.getString("CHIKOU_PERIODS"));
        //lblChikouPeriods.setBounds(GAP + 145, 3 * GAP + 2 * CMB_HEIGHT, 140, CMB_HEIGHT);
        SpinnerNumberModel model4 = new SpinnerNumberModel(26, 2, 1000, 1);
        final JSpinner spinChikouPeriods = new JSpinner(model4);
        //spinChikouPeriods.setBounds(GAP + 145, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinChikouPeriods.getModel()).setValue(new Integer(((IchimokuKinkoHyo) target).getChikouSpanPeriods()));
        spinChikouPeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinChikouPeriods.getModel()).getNumber().intValue();
                    ((IchimokuKinkoHyo) target).setChikouSpanPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        firstPanel.add(lblTenkanPeriods);
        //firstPanel.add(new JLabel(""));
        firstPanel.add(lblKijunPeriods);
        //firstPanel.add(new JLabel(""));

        secondPanel.add(spinTenkanPeriods);
        secondPanel.add(new JLabel(""));
        secondPanel.add(spinKijunPeriods);
        secondPanel.add(new JLabel(""));

        thirdPanel.add(lblSenkouPeriods);
        //thirdPanel.add(new JLabel(""));
        thirdPanel.add(lblChikouPeriods);
        //thirdPanel.add(new JLabel(""));

        forthPanel.add(spinSenkouPeriods);
        forthPanel.add(new JLabel(""));
        forthPanel.add(spinChikouPeriods);

        paramPanel.add(firstPanel);
        paramPanel.add(secondPanel);
        paramPanel.add(thirdPanel);
        paramPanel.add(forthPanel);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //Envelope

    //IchimokuKinkoHyo

    private static TWTabbedPane createKlingerOscillatorPanes(final ChartProperties origin, final ChartProperties target,
                                                             int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"18", "20", "18", "20", "18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("SIGNAL_TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 130, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((KlingerOscillator) target).getSignalTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((KlingerOscillator) target).setSignalTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        Color sigColor;
        byte sigStyle;
        if (((KlingerOscillator) target).getSignalLine() != null) {
            sigColor = ((KlingerOscillator) target).getSignalLine().getColor();
            sigStyle = ((KlingerOscillator) target).getSignalLine().getPenStyle();
        } else {
            sigColor = ((KlingerOscillator) target).getSignalColor();
            sigStyle = ((KlingerOscillator) target).getSignalStyle();
        }
        GraphLabel lblSignalColor = new GraphLabel(Language.getString("LINE_COLOR"));
        //lblSignalColor.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbSignalColor = getColorCombo(origin, target, graph, METHOD_SIGNAL_COLOR, sigColor, LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblSignalStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        //lblSignalStyle.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, 80, CMB_HEIGHT);
        JComboBox cmbSignalStyle = getPenStyleCombo(origin, target, graph, sigStyle, LEFT, 6 * GAP + 5 * CMB_HEIGHT, 100, CMB_HEIGHT);

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);
        paramPanel.add(lblSignalColor);
        paramPanel.add(cmbSignalColor);
        paramPanel.add(lblSignalStyle);
        paramPanel.add(cmbSignalStyle);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //IchimokuKinkoHyo

    //Klinger Oscillator

    private static TWTabbedPane createParabolicSARPanes(final ChartProperties origin, final ChartProperties target,
                                                        int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"35%"}, new String[]{"18", "20", "18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblStep = new GraphLabel(Language.getString("PARABOLIC_SAR_STEP"));
        //lblStep.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        DecimalFormat stepFormat = new DecimalFormat("0.000000");
        final JFormattedTextField stepField = new JFormattedTextField(stepFormat);
        stepField.setValue(new Float(((ParabolicSAR) target).getStep()));
        stepField.setColumns(10);
        //stepField.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        stepField.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                if (target != null) {
                    float val = ((Number) stepField.getValue()).floatValue();
                    ((ParabolicSAR) target).setStep(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMax = new GraphLabel(Language.getString("PARABOLIC_SAR_MAX"));
        //lblMax.setBounds(GAP, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        final JFormattedTextField maxField = new JFormattedTextField(stepFormat);
        maxField.setValue(new Float(((ParabolicSAR) target).getMax()));
        maxField.setColumns(10);
        //maxField.setBounds(GAP, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        maxField.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                if (target != null) {
                    float val = ((Number) maxField.getValue()).floatValue();
                    ((ParabolicSAR) target).setMax(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblStep);
        paramPanel.add(stepField);
        paramPanel.add(lblMax);
        paramPanel.add(maxField);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //Klinger Oscillator

    //Parabolic SAR

    private static TWTabbedPane createPriceVolumeTrendPanes(final ChartProperties origin, final ChartProperties target,
                                                            final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //color/style panel
        //JPanel stylePanel = createGeneralPanel(origin, target, graph, true);
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.setSelectedIndex(0);

        return tabbedPane;
    }
    //Parabolic SAR

    //PriceVolumeTrend

    private static TWTabbedPane createPriceOscillatorPanes(final ChartProperties origin, final ChartProperties target,
                                                           int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "10%", "50%"}, new String[]{"100%"}, 10, 5));
        JPanel leftPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "20", "18", "20", "18", "20"}, 0, 2));
        JPanel rightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"55", "100%"}, 5, 2));
        JPanel rightTopPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "20"}, 0, 2));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblShortPeriods = new GraphLabel(Language.getString("PRICE_OSCILL_SHORT_TERM"));
        //lblShortPeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel modelS = new SpinnerNumberModel(1, 1, 1000, 1);
        final JSpinner spinShortPeriods = new JSpinner(modelS);
        //spinShortPeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinShortPeriods.getModel()).setValue(new Integer(((PriceOscillator) target).getShortPeriods()));
        spinShortPeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinShortPeriods.getModel()).getNumber().intValue();
                    ((PriceOscillator) target).setShortPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblLongPeriods = new GraphLabel(Language.getString("PRICE_OSCILL_LONG_TERM"));
        //lblLongPeriods.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        SpinnerNumberModel modelP = new SpinnerNumberModel(25, 2, 1000, 1);
        final JSpinner spinLongPeriods = new JSpinner(modelP);
        //spinLongPeriods.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinLongPeriods.getModel()).setValue(new Integer(((PriceOscillator) target).getLongPeriods()));
        spinLongPeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinLongPeriods.getModel()).getNumber().intValue();
                    ((PriceOscillator) target).setLongPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMethod = new GraphLabel(Language.getString("METHOD"));
        //lblMethod.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbMethod = new JComboBox();
        //cmbMethod.setBounds(LEFT, 6 * GAP + 5 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        String[] sa = MovingAverage.getMethodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbMethod.addItem(sa[i]);
            if (((PriceOscillator) target).getMethod() == i) {
                cmbMethod.setSelectedIndex(i);
            }
        }
        cmbMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((PriceOscillator) target).setMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblCalcMethod = new GraphLabel(Language.getString("METHOD"));
        //lblCalcMethod.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        JComboBox cmbCalcMethod = new JComboBox();
        //cmbCalcMethod.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbCalcMethod.setEditable(false);
        cmbCalcMethod.setFont(StockGraph.font_BOLD_10);
        String[] saCalc = ZigZag.getMethodArray();
        for (int i = 0; i < saCalc.length; i++) {
            cmbCalcMethod.addItem(saCalc[i]);
            if (((PriceOscillator) target).getCalcMethod() == i) {
                cmbCalcMethod.setSelectedIndex(i);
            }
        }
        cmbCalcMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((PriceOscillator) target).setCalcMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        /*JPanel pnlMethod = new JPanel();
        pnlMethod.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "", TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        pnlMethod.setLayout(null);
        pnlMethod.setBounds(LEFT + 145, GAP, 150, 2 * CMB_HEIGHT + 5 * GAP + 5);*/

        rightTopPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "", TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));

        leftPanel.add(lblShortPeriods);
        leftPanel.add(spinShortPeriods);
        leftPanel.add(lblLongPeriods);
        leftPanel.add(spinLongPeriods);
        leftPanel.add(lblMethod);
        leftPanel.add(cmbMethod);
        rightTopPanel.add(lblCalcMethod);
        rightTopPanel.add(cmbCalcMethod);
        rightPanel.add(rightTopPanel);
        paramPanel.add(leftPanel);
        paramPanel.add(new JPanel());
        paramPanel.add(rightPanel);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //PriceVolumeTrend

    //PriceOscillator

    private static TWTabbedPane createVolumeOscillatorPanes(final ChartProperties origin, final ChartProperties target,
                                                            int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%", "10%", "50%"}, new String[]{"100%"}, 10, 5));
        JPanel leftPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "20", "18", "20", "18", "20"}, 0, 2));
        JPanel rightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"55", "100%"}, 5, 2));
        JPanel rightTopPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "20"}, 0, 2));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblShortPeriods = new GraphLabel(Language.getString("PRICE_OSCILL_SHORT_TERM"));
        //lblShortPeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel modelS = new SpinnerNumberModel(1, 1, 1000, 1);
        final JSpinner spinShortPeriods = new JSpinner(modelS);
        //spinShortPeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinShortPeriods.getModel()).setValue(new Integer(((VolumeOscillator) target).getShortPeriods()));
        spinShortPeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinShortPeriods.getModel()).getNumber().intValue();
                    ((VolumeOscillator) target).setShortPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblLongPeriods = new GraphLabel(Language.getString("PRICE_OSCILL_LONG_TERM"));
        //lblLongPeriods.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        SpinnerNumberModel modelP = new SpinnerNumberModel(25, 2, 1000, 1);
        final JSpinner spinLongPeriods = new JSpinner(modelP);
        //spinLongPeriods.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinLongPeriods.getModel()).setValue(new Integer(((VolumeOscillator) target).getLongPeriods()));
        spinLongPeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinLongPeriods.getModel()).getNumber().intValue();
                    ((VolumeOscillator) target).setLongPeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMethod = new GraphLabel(Language.getString("METHOD"));
        //lblMethod.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbMethod = new JComboBox();
        //cmbMethod.setBounds(LEFT, 6 * GAP + 5 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        String[] sa = MovingAverage.getMethodArray();
        for (int i = 0; i < sa.length; i++) {
            cmbMethod.addItem(sa[i]);
            if (((VolumeOscillator) target).getMethod() == i) {
                cmbMethod.setSelectedIndex(i);
            }
        }
        cmbMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((VolumeOscillator) target).setMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblCalcMethod = new GraphLabel(Language.getString("METHOD"));
        //lblCalcMethod.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        JComboBox cmbCalcMethod = new JComboBox();
        //cmbCalcMethod.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbCalcMethod.setEditable(false);
        cmbCalcMethod.setFont(StockGraph.font_BOLD_10);
        String[] saCalc = ZigZag.getMethodArray();
        for (int i = 0; i < saCalc.length; i++) {
            cmbCalcMethod.addItem(saCalc[i]);
            if (((VolumeOscillator) target).getCalcMethod() == i) {
                cmbCalcMethod.setSelectedIndex(i);
            }
        }
        cmbCalcMethod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((VolumeOscillator) target).setCalcMethod((byte) cmb.getSelectedIndex());
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        /*JPanel pnlMethod = new JPanel();
        pnlMethod.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "", TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        pnlMethod.setLayout(null);
        pnlMethod.setBounds(LEFT + 145, GAP, 150, 2 * CMB_HEIGHT + 5 * GAP + 5);*/

        rightTopPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "", TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));

        leftPanel.add(lblShortPeriods);
        leftPanel.add(spinShortPeriods);
        leftPanel.add(lblLongPeriods);
        leftPanel.add(spinLongPeriods);
        leftPanel.add(lblMethod);
        leftPanel.add(cmbMethod);
        rightTopPanel.add(lblCalcMethod);
        rightTopPanel.add(cmbCalcMethod);
        rightPanel.add(rightTopPanel);
        paramPanel.add(leftPanel);
        paramPanel.add(new JPanel());
        paramPanel.add(rightPanel);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //PriceOscillator

    //VolumeOscillator

    private static TWTabbedPane createWillsAccumDistribPanes(final ChartProperties origin, final ChartProperties target,
                                                             final StockGraph graph) {
        return createDefaultTabbedPane(origin, target, graph);
    }
    //VolumeOscillator

    //WillsAccumDistrib

    private static TWTabbedPane createRMIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18", "20", "18", "20"}, 10, 5));
        JPanel firstPanel = new JPanel(new FlexGridLayout(new String[]{"60%"}, new String[]{"18"}, 0, 0));
        JPanel secondPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"20"}, 0, 0));
        JPanel thirdPanel = new JPanel(new FlexGridLayout(new String[]{"60%"}, new String[]{"18"}, 0, 0));
        JPanel forthPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"20"}, 0, 0));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(GAP, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(14, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(GAP, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        //double value = model.getNumber().doubleValue();
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((RelativeMomentumIndex) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    ((RelativeMomentumIndex) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        GraphLabel lblMomentum = new GraphLabel(Language.getString("RMI_MOMENTUM_PARAMETER"));
        //lblMomentum.setBounds(GAP, 3 * GAP + 2 * CMB_HEIGHT, 190, CMB_HEIGHT);
        SpinnerNumberModel modelM = new SpinnerNumberModel(5, 1, 1000, 1);
        final JSpinner spinMomentum = new JSpinner(modelM);
        //pinMomentum.setBounds(GAP, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinMomentum.getModel()).setValue(new Integer(((RelativeMomentumIndex) target).getMomentumParam()));
        spinMomentum.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinMomentum.getModel()).getNumber().intValue();
                    ((RelativeMomentumIndex) target).setMomentumParam(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        firstPanel.add(lblTimePeriods);
        secondPanel.add(spinTimePeriods);
        thirdPanel.add(lblMomentum);
        forthPanel.add(spinMomentum);

        paramPanel.add(firstPanel);
        paramPanel.add(secondPanel);
        paramPanel.add(thirdPanel);
        paramPanel.add(forthPanel);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //WillsAccumDistrib

    //Relative Momentum Index

    private static TWTabbedPane createIMIPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((IntradayMomentumIndex) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((IntradayMomentumIndex) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    //Relative Momentum Index

    // Intraday Momentum Index - Added by Pramoda

    private static TWTabbedPane createOBVPane(final ChartProperties origin, final ChartProperties target,
                                              final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //JPanel stylePanel = createGeneralPanel(origin, target, graph, true);
        JPanel stylePanel = createGeneralPanel(origin, target, graph);
        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));

        return tabbedPane;
    }
    // Intraday Momentum Index

    // OBV - Pramoda

    private static TWTabbedPane createROCPanes(final ChartProperties origin, final ChartProperties target,
                                               int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((PriceROC) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((PriceROC) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    // OBV

    // Price ROC - Pramoda

    private static TWTabbedPane createWildersSmoothingPanes(final ChartProperties origin, final ChartProperties target,
                                                            int tabIndex, final StockGraph graph, boolean showSources) {
        PropertyDialogTabPane tabbedPane = new PropertyDialogTabPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        //lblTimePeriods.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        //spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((WildersSmoothing) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((WildersSmoothing) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        if (showSources) {
            createIndicatorSourceDestPanel(graph, tabbedPane, icon);
        }
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }
    // Price ROC

    // Wilder's Smoothing - Pramoda

    private static TWTabbedPane createNetOrderPanes(final ChartProperties origin, final ChartProperties target,
                                                    int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);
        /********genaral Tab*****************/
//        JPanel stylePanel = createGeneralPanel(origin, target, graph);
//        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));

        /******GraphType Tabpanell*/
        JPanel graphTypePanel = new JPanel();
        FlexGridLayout flexlayout = new FlexGridLayout(new String[]{"5", "100", "5", "100", "100%"}, new String[]{"15", "25"}, 0, 0);
        graphTypePanel.setLayout(flexlayout);

        JLabel lblGraphType = new JLabel(Language.getString("GRAPH_TYPE"));
        final JComboBox cmbGraphType = UIHelper.getGraphTypeCombo();
        cmbGraphType.setSelectedIndex(((NetOrdersInd) target).getCalcType());
        cmbGraphType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

//                        Object obj = ((ComboItem) cmbGraphType .getSelectedItem()).getValue();
//                        float width = (String) obj);
                if (target != null) {
                    int selIndex = cmbGraphType.getSelectedIndex();
                    ((NetOrdersInd) target).setCalcType(selIndex);
                    dynamicChangeChartProperties(origin, target, graph);
                }

            }
        });
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(new JLabel(""));

        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(lblGraphType);
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(cmbGraphType);
        graphTypePanel.add(new JLabel(""));
        tabbedPane.addTab(Language.getString("GRAPH_TYPE"), graphTypePanel, Language.getString("SELECT_GRAPH_TYPE"));
        tabbedPane.setSelectedIndex(0);

        return tabbedPane;
    }

    private static TWTabbedPane createNetTurnOverPanes(final ChartProperties origin, final ChartProperties target,
                                                       int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);
        /********genaral Tab*****************/
//        JPanel stylePanel = createGeneralPanel(origin, target, graph);
//        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));

        /******GraphType Tabpanell*/
        JPanel graphTypePanel = new JPanel();
        FlexGridLayout flexlayout = new FlexGridLayout(new String[]{"5", "100", "5", "100", "100%"}, new String[]{"15", "25"}, 0, 0);
        graphTypePanel.setLayout(flexlayout);

        JLabel lblGraphType = new JLabel(Language.getString("GRAPH_TYPE"));
        final JComboBox cmbGraphType = UIHelper.getGraphTypeCombo();
        cmbGraphType.setSelectedIndex(((NetTurnOverInd) target).getCalcType());
        cmbGraphType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                        Object obj = ((ComboItem) cmbGraphType .getSelectedItem()).getValue();
//                        float width = (String) obj);
                int selIndex = cmbGraphType.getSelectedIndex();
                ((NetTurnOverInd) target).setCalcType(selIndex);
                dynamicChangeChartProperties(origin, target, graph);

            }
        });
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(new JLabel(""));

        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(lblGraphType);
        graphTypePanel.add(new JLabel(""));
        graphTypePanel.add(cmbGraphType);
        graphTypePanel.add(new JLabel(""));
//        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("GRAPH_TYPE"), graphTypePanel, Language.getString("SELECT_GRAPH_TYPE"));
//        tabbedPane.setSelectedIndex(1);

        return tabbedPane;
    }

    private static TWTabbedPane createDisparityIdxPanes(final ChartProperties origin, final ChartProperties target,
                                                        int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((DisparityIndex) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((DisparityIndex) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    private static TWTabbedPane createHullMovingAveragePanes(final ChartProperties origin, final ChartProperties target,
                                                             int tabIndex, final StockGraph graph) {
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"18", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("TIME_PERIODS"));
        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(((HullMovingAverage) target).getTimePeriods()));
        spinTimePeriods.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                if (target != null) {
                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                    System.out.println("spinTimePeriods stateChanged " + val);
                    ((HullMovingAverage) target).setTimePeriods(val);
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblTimePeriods);
        paramPanel.add(spinTimePeriods);

        //color/style panel
        JPanel stylePanel = createGeneralPanel(origin, target, graph);

        tabbedPane.addTab(Language.getString("GENERAL"), stylePanel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("PARAMETERS"), paramPanel, Language.getString("SET_PARAMETERS"));
        tabbedPane.setSelectedIndex(tabIndex);

        return tabbedPane;
    }

    public static BasicStroke getBasicStroke(byte styleID, float penWidth) {
        switch (styleID) {
            case PEN_SOLID:
                return new BasicStroke(penWidth);
            case PEN_DOTTED:
                float[] dashArr = {penWidth, penWidth + 2, penWidth, penWidth + 2};
                return new BasicStroke(penWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
                        10f, dashArr, 0);
            case PEN_DASH:
                float[] dashArr1 = {penWidth * 2 + 4, penWidth + 4};
                return new BasicStroke(penWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
                        10f, dashArr1, 0);
            case PEN_DASH_DOT:
                float[] dashArr2 = {penWidth * 2 + 4, penWidth + 2, penWidth, penWidth + 2};
                return new BasicStroke(penWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
                        10f, dashArr2, 0);
            case PEN_DASH_DOT_DOT:
                float[] dashArr3 = {penWidth * 2 + 4, penWidth + 2, penWidth, penWidth + 2, penWidth, penWidth + 2};
                return new BasicStroke(penWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER,
                        10f, dashArr3, 0);
            default:
                return new BasicStroke(penWidth);
        }
    }

    public static String[] extractColorNames() {
        String[] sa = null;
        try {
            sa = Language.getString("COLOR_NAMES").split(COLOR_SEP);
        } catch (Exception ex) {
        }
        return sa;
    }

    private static void setCompsEnabled(JComponent[] ca, boolean enabled) {
        for (int i = 0; i < ca.length; i++) {
            ca[i].setEnabled(enabled);
        }
    }

    private static JComboBox getGraphStyleCombo(final ChartProperties origin, final ChartProperties target,
                                                final StockGraph graph, final JComponent[] subComps, int x, int y, int w, int h) {
        JComboBox cmbStyle = new JComboBox();
        cmbStyle.setBounds(x, y, w, h);
        cmbStyle.setEditable(false);
        String[] sa = GraphFrame.getGraphStyles();
        Icon[] ia = GraphToolBar.styleIcon;
        cmbStyle.setRenderer(new ComboRenderer());
        cmbStyle.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        int breakIndex = 4;
        if (target.getID() > GraphDataManager.ID_COMPARE) {
            breakIndex = 0;
            if (target.getID() == GraphDataManager.ID_VOLUME || target.getID() == GraphDataManager.ID_TURNOVER || target.getID() == GraphDataManager.ID_MACD_HISTOGRAM || target.getID() == GraphDataManager.ID_DISPARITY_INDEX) {
                breakIndex = 1;
            }
        }
        for (int i = 0; i < sa.length; i++) {
            ci = new ComboItem(sa[i], sa[i], ia[i], ComboItem.ITEM_NORMAL);
            cmbStyle.addItem(ci);
            if (i == target.getChartStyle()) {
                cmbStyle.setSelectedIndex(i);
            }
            if (i >= breakIndex) break;
        }
        if (cmbStyle.getSelectedIndex() == StockGraph.INT_GRAPH_STYLE_LINE) {
            setCompsEnabled(subComps, true);
        } else {
            setCompsEnabled(subComps, false);
        }
        cmbStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    if (cmb.getSelectedIndex() >= 0) {
                        target.setChartStyle((byte) cmb.getSelectedIndex());
                    }
                    if (cmb.getSelectedIndex() == StockGraph.INT_GRAPH_STYLE_LINE) {
                        setCompsEnabled(subComps, true);
                    } else {
                        setCompsEnabled(subComps, false);
                    }
                    dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        return cmbStyle;
    }

    private static JComboBox getPriorityCombo(final ChartProperties origin, final ChartProperties target,
                                              final StockGraph graph, final String[] items, int x, int y, int w, int h) {
        JComboBox cmbPrio = new JComboBox();
        cmbPrio.setBounds(x, y, w, h);
        cmbPrio.setEditable(false);
        cmbPrio.setFont(StockGraph.font_BOLD_10);
        if (items.length == 1) {
            cmbPrio.addItem(items[0]);
            cmbPrio.setSelectedIndex(0);
        } else {
            for (int i = 0; i < items.length; i++) {
                cmbPrio.addItem(items[i]);
                if (i == target.getOHLCPriority()) {
                    cmbPrio.setSelectedIndex(i);
                }
            }
        }
        cmbPrio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    if ((cmb.getSelectedIndex() >= 0) && (items.length > 1)) {
                        target.setOHLCPriority((byte) cmb.getSelectedIndex());
                        if (target instanceof MACD) {
                            ((MACD) origin).getHistogram().setOHLCPriority((byte) cmb.getSelectedIndex());
                        } else if (target instanceof MACDVolume) {
                            ((MACDVolume) origin).getHistogram().setOHLCPriority((byte) cmb.getSelectedIndex());
                        }
                        dynamicChangeChartProperties(origin, target, graph);
                    }
                }
            }
        });
        return cmbPrio;
    }

    private static JComboBox getColorCombo(final Object origin, final Object target, final StockGraph graph,
                                           final String method, Color oldColor, int x, int y, int w, int h) {
        JComboBox cmbColor = new JComboBox();
        cmbColor.setBounds(x, y, w, h);
        cmbColor.setEditable(false);
        cmbColor.setRenderer(new ComboRenderer());
        cmbColor.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        boolean customColor = true;
        for (int i = 0; i < colors.length; i++) {
            ci = new ComboItem(colorNames[i], colors[i], new ComboImage(colors[i]), ComboItem.ITEM_NORMAL);
            //ci.setDisabledImage(ComboImage.getDisabledImage());
            cmbColor.addItem(ci);
            if (colors[i].equals(oldColor)) {
                cmbColor.setSelectedIndex(i);
                customColor = false;
            }
        }
        ci = new ComboItem(Language.getString("CUSTOM"), oldColor, new ComboImage(oldColor), ComboItem.ITEM_CUSTOM_COLOR);
        cmbColor.addItem(ci);
        if (customColor) {
            cmbColor.setSelectedItem(ci);
        }
        cmbColor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    try {
                        Color color = (Color) (((ComboItem) cmb.getSelectedItem()).getValue());

                        if (target instanceof ChartProperties) {
                            if (method.equals(METHOD_SET_COLOR)) {
                                ((ChartProperties) target).setColor(color);
                            } else if (method.equals(METHOD_WARNING_COLOR)) {
                                ((ChartProperties) target).setWarningColor(color);
                            } else if (method.equals(METHOD_SIGNAL_COLOR)) {
                                ((ChartProperties) target).setSignalColor(color);
                            } else if (method.equals(METHOD_SET_FILL_COLOR)) {
                                if (target instanceof FillArea) {
                                    ((FillArea) target).setFillColor(color);
                                }
                            }
                            dynamicChangeChartProperties((ChartProperties) origin, (ChartProperties) target, graph);
                        } else if (target instanceof AbstractObject) {
                            if (method.equals(METHOD_SET_COLOR)) {
                                ((AbstractObject) target).setColor(color);
                            }
                            if (target instanceof ObjectSymbol) {
                                if (method.equals(METHOD_SET_FONT_COLOR)) {
//                                    ((ObjectSymbol) target).setFontColor(color);
                                    ((ObjectSymbol) target).setColor(color); //change to make symbols  themable
                                }
                            } else if (target instanceof LineStudy) {
                                if (method.equals(METHOD_SET_FILL_COLOR)) {
                                    ((LineStudy) target).setFillColor(color);
                                } else if (method.equals(METHOD_SET_FONT_COLOR)) {
                                    ((LineStudy) target).setFontColor(color);
                                }
                            }
                            dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                            //ComponentRegistry.fireUpdate();
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        return cmbColor;
    }

    private static JComboBox getPenStyleCombo(final Object origin, final Object target,
                                              final StockGraph graph, int x, int y, int w, int h) {
        byte penStyle = 0;
        if (target instanceof ChartProperties) {
            penStyle = ((ChartProperties) target).getPenStyle();
        } else if (target instanceof AbstractObject) {
            penStyle = ((AbstractObject) target).getPenStyle();
        }
        return getPenStyleCombo(origin, target, graph, penStyle, x, y, w, h);
    }

    private static JTextField getDataSettingsTextField(final Object origin, final Object target, final StockGraph graph) {

        double yArray[] = new double[]{20.0};
        if (target instanceof AbstractObject) {
            yArray = ((AbstractObject) target).getYArr();
        }

        return getTextField(origin, target, yArray, graph);
    }

    private static JTextField getTextField(final Object origin, final Object target, double yArray[], final StockGraph graph) {

        double value = yArray[0];

        DecimalFormat priceFormat = new DecimalFormat("##0.00");
        String priceValue = priceFormat.format(value);

        final JTextField txtData = new JTextField(String.valueOf(priceValue));

        txtData.addKeyListener(new KeyListener() {
            double[] yarr = new double[]{20.0};

            public void keyTyped(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void keyPressed(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void keyReleased(KeyEvent e) {
                try {
                    yarr[0] = Double.parseDouble(txtData.getText());
                    if (target instanceof AbstractObject) {
                        ((AbstractObject) target).setYArr(yarr);
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                    }
                } catch (Exception ex) {
                    txtData.setText("");
                    ex.printStackTrace();
                }
            }
        });

        return txtData;

    }

    private static JComboBox getPenStyleCombo(final Object origin, final Object target, final StockGraph graph,
                                              byte penStyle, int x, int y, int w, int h) {
        final String method = METHOD_SET_STYLE;
        JComboBox cmbLineStyle = new JComboBox();
        cmbLineStyle.setBounds(x, y, w, h);
        cmbLineStyle.setEditable(false);
        cmbLineStyle.setRenderer(new ComboRenderer());
        cmbLineStyle.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        //int penWidth = getEquivalentWidth(target.getPenWidth());
        for (int i = 0; i < lineStyles.length; i++) {
            ci = new ComboItem(lineStyles[i], lineStyles[i], penStyles[i], ComboItem.ITEM_NORMAL);
            cmbLineStyle.addItem(ci);
            if (i == penStyle) {
                cmbLineStyle.setSelectedItem(ci);
            }
        }
        cmbLineStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    if (target instanceof ChartProperties) {
                        if (method.equals(METHOD_SET_STYLE)) {
                            ((ChartProperties) target).setPenStyle((byte) cmb.getSelectedIndex());
                        } else if (method.equals(METHOD_SET_SIGNAL_STYLE)) {
                            ((ChartProperties) target).setSignalStyle((byte) cmb.getSelectedIndex());
                        }
                        dynamicChangeChartProperties((ChartProperties) origin, (ChartProperties) target, graph);


                    } else if (target instanceof AbstractObject) {
                        ((AbstractObject) target).setPenStyle((byte) cmb.getSelectedIndex());
                        dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                        //ComponentRegistry.fireUpdate();
                    }
                }
            }
        });
        return cmbLineStyle;
    }

    private static JComboBox getPenWidthCombo(final Object origin, final Object target, final StockGraph graph,
                                              final String method, float penWidth, int x, int y, int w, int h) {
        JComboBox cmbLineWidth = new JComboBox();
        cmbLineWidth.setBounds(x, y, w, h);
        cmbLineWidth.setEditable(false);
        cmbLineWidth.setRenderer(new ComboRenderer());
        cmbLineWidth.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        //int penWidth = getEquivalentWidth(target.getPenWidth());
        int equiW = getEquivalentWidth(penWidth);
        for (int i = 0; i < lineWidths.length; i++) {
            ci = new ComboItem(lineWidths[i], lineWidths[i], penWidths[i], ComboItem.ITEM_NORMAL);
            cmbLineWidth.addItem(ci);
            if (equiW == i) {
                cmbLineWidth.setSelectedItem(ci);
            }
        }
        cmbLineWidth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    Object obj = ((ComboItem) cmb.getSelectedItem()).getValue();
                    if (method.equals(METHOD_SET_WIDTH)) {
                        if (target instanceof ChartProperties) {
                            ((ChartProperties) target).setPenWidth(Float.parseFloat((String) obj));
                            dynamicChangeChartProperties((ChartProperties) origin, (ChartProperties) target, graph);
                        } else if (target instanceof AbstractObject) {
                            ((AbstractObject) target).setPenWidth(Float.parseFloat((String) obj));
                            dynamicChangeObjectProperties((AbstractObject) origin, (AbstractObject) target, graph);
                            //ComponentRegistry.fireUpdate();
                        }
                    }
                }
            }
        });
        return cmbLineWidth;
    }

    private static int getEquivalentWidth(float pw) {
        if (pw <= 1.1f) {
            return 0;
        } else if (pw <= 1.6f) {
            return 1;
        } else if (pw <= 2.1f) {
            return 2;
        } else if (pw <= 3.1f) {
            return 3;
        } else return 4;
    }

    public static void updateTheme() {
        icon = new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif");
        Icon[] penStylesTmp = {
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_solid.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dot.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash_dot.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash_dot_dot.gif")
        };
        penStyles = penStylesTmp;
        Icon[] penWidthsTmp = {
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_0p5.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_1p0.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_1p5.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_2p0.gif"),
                new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_3p0.gif")
        };
        penWidths = penWidthsTmp;
    }

    /**
     * **Do we need to update all the datae sources here ..isnt it enough to update corroponding panel data sources ? - sathyajith(suggetion) ***********************
     * to improve the performance. may need to modify the update data sources method ..So that only required sources can be modified *
     */

    public static void dynamicChangeChartProperties(ChartProperties origin, ChartProperties target, StockGraph graph) {
        if (graph != null) {
            origin.assignValuesFrom(target);
            WindowPanel r = origin.getRect();
            if (r != null) {
                if (origin.getID() > GraphDataManager.ID_COMPARE) {
                    Indicator ind = (Indicator) origin;
                    if (ind.getInnerSource() == graph.GDMgr.getBaseCP()) {
                        r.setTitle(origin.toString());
                    }
                }
            }
            if (origin == graph.GDMgr.getBaseCP()) {
                graph.setGraphStyle(origin.getChartStyle());
                ChartFrame.getSharedInstance().refreshTopToolbar();
            }
            if (origin.getID() > GraphDataManager.ID_COMPARE) {
                synchronized (graph.GDMgr.getGraphLock()) {
                    origin.setRecordSize(0);
                    ChartProperties inner;
                    for (int i = 0; i < graph.GDMgr.getSources().size(); i++) {
                        inner = (ChartProperties) graph.GDMgr.getSources().get(i);
                        if (inner instanceof Indicator) {
                            inner.setRecordSize(0);
                        }
                    }
                }
                graph.GDMgr.updateDataSources();
            }
            graph.reDrawAll();
        }
    }

    public static void dynamicChangeObjectProperties(AbstractObject origin, AbstractObject target, StockGraph graph) {
        if (graph != null) {
            origin.assignValuesFrom(target);
            graph.repaint();
        }
    }

    public static class FibonacciRowGUI {
        JCheckBox chkEnable;
        JTextField txtPercentage;
        CustomColorLabel lblColor;
        JComboBox cmbLineStyle;
        JComboBox cmLineWidth;

        public FibonacciRowGUI(JCheckBox chkEnable, JTextField txtPercentage, CustomColorLabel lblColor, JComboBox cmbLineStyle, JComboBox cmLineWidth) {
            this.chkEnable = chkEnable;
            this.txtPercentage = txtPercentage;
            this.lblColor = lblColor;
            this.cmbLineStyle = cmbLineStyle;
            this.cmLineWidth = cmLineWidth;
        }

        public JCheckBox getChkEnable() {
            return chkEnable;
        }

        public JTextField getTxtPercentage() {
            return txtPercentage;
        }

        public CustomColorLabel getLblColor() {
            return lblColor;
        }

        public JComboBox getCmbLineStyle() {
            return cmbLineStyle;
        }

        public JComboBox getCmLineWidth() {
            return cmLineWidth;
        }
    }

    public static class CustomColorLabel extends JLabel implements MouseMotionListener, MouseListener {

        ActionListener okActionListener = new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                //System.out.println("OK Button");
                setLabelVisuals(colorChooser.getColor());
            }
        };
        ActionListener cancelActionListener = new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                //System.out.println("Cancel Button");
            }
        };
        private JColorChooser colorChooser = new JColorChooser(Color.BLUE);

        public CustomColorLabel(Color col) {

            super("", JLabel.CENTER);
            this.addMouseMotionListener(this);
            this.addMouseListener(this);
            this.setOpaque(true);
            this.setBackground(col);
            this.setLabelText(col);
            this.setForeground(getInverseColor(col));
            this.setPreferredSize(new Dimension(50, 10));
            this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        }

        public void mouseDragged(MouseEvent e) {

        }

        public void mouseMoved(MouseEvent e) {

        }

        private void setLabelVisuals(Color color) {

            this.setBackground(color);
            this.setForeground(getInverseColor(color));
            setLabelText(color);
            this.updateUI();
        }

        private void setLabelText(Color color) {

            int rin = color.getRed();
            int gin = color.getGreen();
            int bin = color.getBlue();

            String text = rin + ", " + gin + ", " + bin;
            this.setText(text);
        }

        private Color getInverseColor(Color color) {

            int rin = color.getRed();
            int gin = color.getGreen();
            int bin = color.getBlue();
            int rout = 255 - rin;
            int gout = 255 - gin;
            int bout = 255 - bin;

            return new Color(rout, gout, bout);
        }

        public void mouseClicked(MouseEvent e) {

            if (isEnabled()) {
                JDialog colorDialog = JColorChooser.createDialog(this, Language.getString("FIB_PICK_COLOR"), true, colorChooser, okActionListener, cancelActionListener);
                colorDialog.setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
                int x = (int) this.getLocationOnScreen().getX() + 40;
                int y = (int) this.getLocationOnScreen().getY() + 20;

                colorDialog.setLocationRelativeTo(ChartFrame.getSharedInstance());
                colorDialog.setVisible(true);
            }
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }

        public void mouseExited(MouseEvent e) {
        }
    }

    /*public static class ComponentRegistry {

        private static ArrayList<Component> components = new ArrayList<Component>();
        private static AbstractObject target;

        public static void registerComponent(Component c, AbstractObject ao) {
            components.clear();
            components.add(c);
            target = ao;
        }

        public static void registerComponent(Component[] copms, AbstractObject ao) {
            components.clear();
            for (int i = 0; i < components.size(); i++) {
                Component component = components.get(i);

                components.add(component);
            }
            target = ao;
        }

        public static void fireUpdate() {
            try {
                for (int i = 0; i < components.size(); i++) {
                    Component component = components.get(i);

                    if (component instanceof JCheckBox) {
                        JCheckBox chkDefault = (JCheckBox) component;
                        chkDefault.setEnabled(true);
                        chkDefault.setSelected(false);
                        target.setUsingUserDefault(false);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/
}

