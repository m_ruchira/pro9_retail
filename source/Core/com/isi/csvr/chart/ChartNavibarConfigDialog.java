package com.isi.csvr.chart;

import com.isi.csvr.Client;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;


/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Apr 1, 2009
 * Time: 2:07:49 PM
 * To change this template use File | Settings | File Templates.
 */

public class ChartNavibarConfigDialog extends JDialog implements ActionListener, NonNavigatable, MouseListener, Themeable, ItemListener {

    public static boolean isLongOnly;
    public static boolean isSymbolOnly;
    public static boolean isShortOnly;
    public static boolean isLongSymbolOnly;
    public static boolean isShorSymboltOnly;
    public static boolean isSymbolShortOnly;
    public static boolean checkisLongOnly;
    public static boolean checkisSymbolOnly;
    public static boolean checkisShortOnly;
    public static boolean checkisLongSymbolOnly;
    public static boolean checkisShorSymboltOnly;
    public static boolean checkisSymbolShortOnly;
    TWEXComboBox fontSelect;
    ComboRenderer fontRenderer;
    Font oldFont;
    Font newFont;
    TWComboBox textSizeSelect;
    TWComboBox textStyleSelect;
    TWButton fontColorSelector;
    String[] sizeList = new String[]
            {"2", "4", "6", "8", "10", "12", "14", "16", "18", "20", "22", "24", "30", "36", "48", "72"};
    String[] fontStyle = new String[]{Language.getString("SIDE_BAR_PLAIN"), Language.getString("SIDE_BAR_BOLD"), Language.getString("SIDE_BAR_ITALIC")};
    private JPanel mainPanel;
    private JPanel buttonPanel;
    private JPanel symbolPanel;
    private JPanel shortPanel;
    private JPanel longPanel;
    private JPanel fontPanel;
    private JPanel fontLeftPanel;
    private JPanel fontRightPanel;
    private JRadioButton showShort;
    private JRadioButton showLong;
    private JRadioButton showsymbol;
    private TWButton okButton;
    private TWButton cancelButton;
    private TWCustomCheckBox checkSymbol;
    private TWCustomCheckBox checkShortDec;
    private TWCustomCheckBox checkLongDec;
    private JDialog colorDialog;
    private JColorChooser colorChooser;
    private Color oldColor;
    private Color newColor;
    private TWButton source;
    private String lastCommand = "";

    public ChartNavibarConfigDialog(JFrame frame, String title, boolean b) {
        super(frame, title, b);
        createUI();
        applyTheme();
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
    }

    public static boolean isLongOnly() {
        return isLongOnly;
    }

    public static void setLongOnly(boolean longOnly) {
        isLongOnly = longOnly;
    }

    public static boolean isSymbolOnly() {
        return isSymbolOnly;
    }

    public static void setSymbolOnly(boolean symbolOnly) {
        isSymbolOnly = symbolOnly;
    }

    public static boolean isShortOnly() {
        return isShortOnly;
    }

    public static void setShortOnly(boolean shortOnly) {
        isShortOnly = shortOnly;
    }

    public static boolean isShorSymboltOnly() {
        return isShorSymboltOnly;
    }

    public static void setShorSymboltOnly(boolean shorSymboltOnly) {
        isShorSymboltOnly = shorSymboltOnly;
    }

    public static boolean isLongSymbolOnly() {
        return isLongSymbolOnly;
    }

    public static void setLongSymbolOnly(boolean longSymbolOnly) {
        isLongSymbolOnly = longSymbolOnly;
    }

    public static boolean isSymbolShortOnly() {
        return isSymbolShortOnly;
    }

    public static void setSymbolShortOnly(boolean symbolShortOnly) {
        isSymbolShortOnly = symbolShortOnly;
    }

    public static void loadingFromWorkSpace(String indtickerWSString) {
        String loadString = indtickerWSString;
        ArrayList<String> savedData = new ArrayList<String>();
        StringTokenizer fields = new StringTokenizer(loadString, Meta.RS);
        while (fields.hasMoreElements()) {
            savedData.add(fields.nextToken());
        }
        isSymbolOnly = Boolean.parseBoolean(savedData.get(0));
        isSymbolShortOnly = Boolean.parseBoolean(savedData.get(1));
        isShortOnly = Boolean.parseBoolean(savedData.get(2));
        isShorSymboltOnly = Boolean.parseBoolean(savedData.get(3));
        isLongOnly = Boolean.parseBoolean(savedData.get(4));
        isLongSymbolOnly = Boolean.parseBoolean(savedData.get(5));

        ChartRendererComponenet.FONT_TYPE = savedData.get(6);
        ChartRendererComponenet.FONT_STYLE = Integer.parseInt(savedData.get(7));
        ChartRendererComponenet.FONT_SIZE = Integer.parseInt(savedData.get(8));

        ChartSymbolTreeRenderer.chartNaviBarSymbolColor = new Color(Integer.parseInt(savedData.get(9)));
//        SymbolTreeRenderer.sideBarSymbolColor. = Color.decode(savedData.get(9));

        ChartSymbolTreeRenderer.nonLeafRenderer.setFontSttings(new Font(ChartRendererComponenet.FONT_TYPE, ChartRendererComponenet.FONT_STYLE, ChartRendererComponenet.FONT_SIZE));

        setCheckisSymbolOnly(isSymbolOnly());
        setCheckisShortOnly(isShortOnly());
        setCheckisShorSymboltOnly(isShorSymboltOnly());
        setCheckisLongOnly(isLongOnly());
        setCheckisLongSymbolOnly(isLongSymbolOnly());
        setCheckisSymbolShortOnly(isSymbolShortOnly());
    }

    public static boolean isCheckisShorSymboltOnly() {
        return checkisShorSymboltOnly;
    }

    public static void setCheckisShorSymboltOnly(boolean checkisShorSymboltOnly) {
        ChartNavibarConfigDialog.checkisShorSymboltOnly = checkisShorSymboltOnly;
    }

    public static boolean isCheckisLongOnly() {
        return checkisLongOnly;
    }

    public static void setCheckisLongOnly(boolean checkisLongOnly) {
        ChartNavibarConfigDialog.checkisLongOnly = checkisLongOnly;
    }

    public static boolean isCheckisSymbolOnly() {
        return checkisSymbolOnly;
    }

    public static void setCheckisSymbolOnly(boolean checkisSymbolOnly) {
        ChartNavibarConfigDialog.checkisSymbolOnly = checkisSymbolOnly;
    }

    public static boolean isCheckisShortOnly() {
        return checkisShortOnly;
    }

    public static void setCheckisShortOnly(boolean checkisShortOnly) {
        ChartNavibarConfigDialog.checkisShortOnly = checkisShortOnly;
    }

    public static boolean isCheckisLongSymbolOnly() {
        return checkisLongSymbolOnly;
    }

    public static void setCheckisLongSymbolOnly(boolean checkisLongSymbolOnly) {
        ChartNavibarConfigDialog.checkisLongSymbolOnly = checkisLongSymbolOnly;
    }

    public static boolean isCheckisSymbolShortOnly() {
        return checkisSymbolShortOnly;
    }

    public static void setCheckisSymbolShortOnly(boolean checkisSymbolShortOnly) {
        ChartNavibarConfigDialog.checkisSymbolShortOnly = checkisSymbolShortOnly;
    }

    private void createUI() {
        oldFont = ChartSymbolTreeRenderer.nonLeafRenderer.getFontSetting();
        oldColor = ChartSymbolTreeRenderer.chartNaviBarSymbolColor;
        colorChooser = new JColorChooser(Color.red);
        JLabel previewPane = new JLabel(Language.getString("WINDOW_TITLE_CLIENT"));
        colorChooser.setPreviewPanel(previewPane);
        colorDialog = JColorChooser.createDialog(Client.getInstance().getFrame(), Language.getString("SELECT_COLOR"), true, colorChooser, this, this);
        mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("SIDEBAR_FORMAT")));
        buttonPanel = new JPanel();
        symbolPanel = new JPanel();
        shortPanel = new JPanel();
        longPanel = new JPanel();

        checkSymbol = new TWCustomCheckBox(Language.getString("INCLUDE_SHORT_DEC"), SwingConstants.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
        checkSymbol.addMouseListener(this);
        checkSymbol.setEnabled(isCheckisSymbolOnly());
        checkSymbol.setSelected(isCheckisSymbolShortOnly());

        checkShortDec = new TWCustomCheckBox(Language.getString("INCLUDE_SYMBOL"), SwingConstants.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
        checkShortDec.addMouseListener(this);
        checkShortDec.setEnabled(isCheckisShortOnly());
        checkShortDec.setSelected(isCheckisShorSymboltOnly());

        checkLongDec = new TWCustomCheckBox(Language.getString("INCLUDE_SYMBOL"), SwingConstants.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
        checkLongDec.addMouseListener(this);
        checkLongDec.setEnabled(isCheckisLongOnly());
        checkLongDec.setSelected(isCheckisLongSymbolOnly());

        okButton = new TWButton(Language.getString("OK"));
        okButton.addActionListener(this);
        cancelButton = new TWButton(Language.getString("CANCEL"));
        cancelButton.addActionListener(this);

        showShort = new JRadioButton(Language.getString("SHOW_SHORT_DEC"));
        showShort.setSelected(isCheckisShortOnly());
        showShort.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (showShort.isSelected()) {
                    setShortOnly(true);
                    showLong.setSelected(false);
                    setLongOnly(false);
                    setSymbolOnly(false);
                    showsymbol.setSelected(false);
                    checkShortDec.setEnabled(isShortOnly());
                    checkSymbol.setEnabled(isSymbolOnly());

                    checkLongDec.setSelected(isCheckisLongSymbolOnly());
                    checkLongDec.setEnabled(isLongOnly());
                } else if (!showShort.isSelected()) {
                    setShortOnly(false);
                    checkShortDec.setEnabled(isShortOnly());
                }
            }
        });
        showLong = new JRadioButton(Language.getString("SHOW_LONG_DEC"));
        showLong.setSelected(isCheckisLongOnly());
        showLong.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (showLong.isSelected()) {
                    setLongOnly(true);
                    showShort.setSelected(false);
                    setShortOnly(false);
                    showsymbol.setSelected(false);
                    checkLongDec.setEnabled(isLongOnly());
                    setSymbolOnly(false);
                    checkShortDec.setSelected(isCheckisShorSymboltOnly());
                    checkShortDec.setEnabled(isShortOnly());
                    checkSymbol.setEnabled(isSymbolOnly());
                } else if (!showLong.isSelected()) {
                    setLongOnly(false);
                    checkLongDec.setEnabled(isLongOnly());
                }
            }
        });
        showsymbol = new JRadioButton(Language.getString("SHOW_SIDEBAR_SYMBOL"));
        showsymbol.setSelected(isCheckisSymbolOnly());
        showsymbol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (showsymbol.isSelected()) {
                    setSymbolOnly(true);
                    setLongOnly(false);
                    setShortOnly(false);
                    showLong.setSelected(false);
                    showShort.setSelected(false);
                    checkLongDec.setEnabled(isLongOnly());
                    checkShortDec.setEnabled(isShortOnly());
                    checkSymbol.setSelected(isCheckisSymbolShortOnly());
                    checkSymbol.setEnabled(isSymbolOnly());
                    checkLongDec.setSelected(isCheckisLongSymbolOnly());
                    checkShortDec.setSelected(isCheckisShorSymboltOnly());
                } else if (!showsymbol.isSelected()) {
                    setSymbolOnly(false);
                    checkSymbol.setEnabled(isSymbolOnly());
                }
            }
        });

        //Group the radio buttons.
        ButtonGroup group = new ButtonGroup();
        group.add(showsymbol);
        group.add(showLong);
        group.add(showShort);

        fontPanel = new JPanel();
        fontPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("SIDEBAR_FONT_SETTINGS")));
        fontPanel.setLayout(new FlexGridLayout(new String[]{"40%", "10%", "50%"}, new String[]{"100%"}, 5, 2));
        fontPanel.setSize(400, 200);

        fontLeftPanel = new JPanel();
        fontLeftPanel.setLayout(new FlexGridLayout(new String[]{"120"}, new String[]{"25%", "25%", "25%", "25%"}, 5, 2));
//        fontLeftPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20%", "20%", "20%", "20%", "20%"}, 5, 2));

        fontRightPanel = new JPanel();
        fontRightPanel.setLayout(new FlexGridLayout(new String[]{"70"}, new String[]{"25%", "25%", "25%", "25%"}, 5, 2));

        JLabel labelType = new JLabel(Language.getString("SIDE_BAR_FONT_TYPE"));
        fontLeftPanel.add(labelType);

        createFontSelectorCombo();
        fontLeftPanel.add(fontSelect);
        fontLeftPanel.add(new JLabel(Language.getString("SIDE_BAR_FONT_STYLE")));
        createFontStyleSelector();
        fontLeftPanel.add(textStyleSelect);

//        fontLeftPanel.add(new JLabel());

        fontPanel.add(fontLeftPanel);

        fontPanel.add(new JLabel());
        JLabel fontSize = new JLabel(Language.getString("SIDE_BAR_FONT_SIZE"));
        fontRightPanel.add(fontSize);

        createFontSizeSelector();
        fontRightPanel.add(textSizeSelect);
//        fontRightPanel.add(textStyleSelect);
//        fontLeftPanel.add(textSizeSelect);

        JLabel fontColor = new JLabel(Language.getString("SIDE_BAR_FONT_COLOR"));
        createfFontColorSelector();


        fontRightPanel.add(fontColor);
        fontRightPanel.add(fontColorSelector);
        fontPanel.add(fontRightPanel);


        Container contentPane = getContentPane();
        contentPane.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"42%", "48%", "10%"}, 5, 2));
//        symbolPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 5, 2));
        symbolPanel.setLayout(new FlexGridLayout(new String[]{"40%", "10%", "50%"}, new String[]{"100%"}, 5, 2));
        shortPanel.setLayout(new FlexGridLayout(new String[]{"40%", "10%", "50%"}, new String[]{"100%"}, 5, 2));
        longPanel.setLayout(new FlexGridLayout(new String[]{"40%", "10%", "50%"}, new String[]{"100%"}, 5, 2));
        symbolPanel.setSize(400, 30);
        shortPanel.setSize(400, 30);
        longPanel.setSize(400, 30);
        buttonPanel.setSize(400, 20);
        buttonPanel.setLayout(new FlexGridLayout(new String[]{"50%", "25%", "25%"}, new String[]{"20"}, 5, 2));
        mainPanel.setSize(400, 170);
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"33%", "33%", "34%"}, 5, 2));
        symbolPanel.add(showsymbol);
        symbolPanel.add(new JLabel(""));
        symbolPanel.add(checkSymbol);
        shortPanel.add(showShort);
        shortPanel.add(new JLabel(""));
        shortPanel.add(checkShortDec);
        longPanel.add(showLong);
        longPanel.add(new JLabel(""));
        longPanel.add(checkLongDec);
        mainPanel.add(symbolPanel);
        mainPanel.add(shortPanel);
        mainPanel.add(longPanel);
        buttonPanel.add(new Label(""));
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);
        GUISettings.applyOrientation(mainPanel);
        GUISettings.applyOrientation(buttonPanel);
        GUISettings.applyOrientation(fontPanel);
        contentPane.add(mainPanel);
        contentPane.add(fontPanel);
        contentPane.add(buttonPanel);
        setSize(400, 320);
        setLocationRelativeTo(getOwner());
        setVisible(true);
    }

    private void createfFontColorSelector() {
        fontColorSelector = new TWButton();
        fontColorSelector.setBackground(ChartSymbolTreeRenderer.chartNaviBarSymbolColor);
        fontColorSelector.setPreferredSize(new Dimension(44, 19));
        fontColorSelector.addActionListener(this);
//        fontColorSelector.setBorder(null);
        fontColorSelector.setGradientPainted(false);
        fontColorSelector.setFocusable(false);
//        fontColorSelector.setArrowButtonIconColor(fontColorSelector.getForeground());

//        TWButton btnExportLocation = new TWButton(new DownArrow());
//        btnExportLocation.setBorder(null);
//        btnExportLocation.setGradientPainted(false);
//        btnExportLocation.addActionListener(this);
//        btnExportLocation.setPreferredSize(new Dimension(20, 20));
//        btnExportLocation.setActionCommand("DESTINATION");

    }

    private void createFontStyleSelector() {

        textStyleSelect = new TWComboBox(fontStyle);
        textStyleSelect.setPreferredSize(new Dimension(44, 19));
//        textSizeSelect.setToolTipText(Language.getString("STYLE"));
        textStyleSelect.setSelectedItem(getBodyStyle(oldFont.getStyle()));

        textStyleSelect.addItemListener(this);
        textStyleSelect.setMaximumRowCount(16);
        textStyleSelect.setFocusable(false);
//        textSizeSelect.setBackground(bgColor);
//        textSizeSelect.setForeground(fgColor);
        textStyleSelect.setArrowButtonIconColor(textStyleSelect.getForeground());
    }

    private void createFontSizeSelector() {
        textSizeSelect = new TWComboBox(sizeList);
        textSizeSelect.setPreferredSize(new Dimension(44, 19));
//        textSizeSelect.setToolTipText(Language.getString("SIZE"));
        textSizeSelect.setSelectedItem("" + oldFont.getSize());
        textSizeSelect.addItemListener(this);
        textSizeSelect.setMaximumRowCount(16);
        textSizeSelect.setFocusable(false);
//        textSizeSelect.setBackground(bgColor);
//        textSizeSelect.setForeground(fgColor);
        textSizeSelect.setArrowButtonIconColor(textSizeSelect.getForeground());
//        textSizeSelect.setBorder(BorderFactory.createLineBorder(comboBorderColor));
    }

    private void createFontSelectorCombo() {
        fontRenderer = new ComboRenderer();
        Integer p[] = {1, 2, 3, 4,};
        fontSelect = new TWEXComboBox(getFontList());
        fontSelect.setSelectedItem(oldFont.getFamily());
        fontSelect.setExtendedMode(false);
        fontSelect.setPreferredSize(new Dimension(80, 19));
        fontSelect.setExtendedMode(true);
        fontSelect.setToolTipText(Language.getString("FONT"));

        fontSelect.addItemListener(this);
        fontSelect.setMaximumRowCount(15);
        fontSelect.setRenderer(fontRenderer);
        fontSelect.setFocusable(false);
//        fontSelect.setBackground(bgColor);
//        fontSelect.setForeground(fgColor);
        fontSelect.setArrowButtonIconColor(fontSelect.getForeground());
//        fontSelect.setBorder(BorderFactory.createLineBorder(comboBorderColor));
        GUISettings.clearBorders(fontSelect);
        SwingUtilities.updateComponentTreeUI(fontSelect);
    }

    public Vector getFontList() {
        //  new TWComboModel() ;
        //  new TWComboBox()
        String[] fontList = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

        //  String[] comboList = new String[fontList.length];
        Vector comboList = new Vector();
        char trackingChar = Language.getString("LANG").toCharArray()[0];
        for (int i = 0; i < fontList.length; i++) {
            Font fnt = new Font(fontList[i], Font.PLAIN, 12);
            // String temp = getHtmlString(fontList[i]);
            if (fnt.canDisplay(trackingChar)) {


                comboList.add(fontList[i]);
            }

        }

        // return comboList;
        return comboList;
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(checkShortDec)) {
            if (checkShortDec.isSelected()) {
                setShorSymboltOnly(true);
            } else {
                setShorSymboltOnly(false);
            }
        } else if (e.getSource().equals(checkLongDec)) {
            if (checkLongDec.isSelected()) {
                setLongSymbolOnly(true);
            } else {
                setLongSymbolOnly(false);
            }
        } else if (e.getSource().equals(checkSymbol)) {
            if (checkSymbol.isSelected()) {
                setSymbolShortOnly(true);
            } else {
                setSymbolShortOnly(false);
            }
        }

    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(okButton)) {
            setCheckisSymbolOnly(isSymbolOnly());
            setCheckisShortOnly(isShortOnly());
            setCheckisShorSymboltOnly(isShorSymboltOnly());
            setCheckisLongOnly(isLongOnly());
            setCheckisLongSymbolOnly(isLongSymbolOnly());
            setCheckisSymbolShortOnly(isSymbolShortOnly());
            ChartSymbolTreeRenderer.nonLeafRenderer.setFontSttings(oldFont);
            ChartSymbolTreeRenderer.chartNaviBarSymbolColor = oldColor;
            ChartSymbolNavigator.getSharedInstance().searchTree("");
            this.dispose();
        } else if (e.getSource().equals(cancelButton)) {

            this.dispose();
        } else if (e.getSource().equals(fontColorSelector)) {
            source = (TWButton) e.getSource();
            oldColor = source.getBackground();
            colorChooser.setColor(oldColor);
            lastCommand = e.getActionCommand();
            colorDialog.show();


        } else if (e.getActionCommand().equalsIgnoreCase("OK")) {
            newColor = colorChooser.getColor();
            fontColorSelector.setBackground(newColor);
            oldColor = newColor;

        }

    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == fontSelect) {
            if (e.getStateChange() == e.SELECTED) {
                String type = fontSelect.getSelectedItem().toString();
                if (!oldFont.getFamily().equals(type)) {
                    newFont = new Font(type, oldFont.getStyle(), oldFont.getSize());
                    if (!(newFont.canDisplayUpTo("TEST") == -1)) {
                        JOptionPane.showMessageDialog(this, Language.getString("FONT_WARNING_MSG"), Language.getString("WARNING"), JOptionPane.OK_OPTION);
                    }
                    oldFont = newFont;
                }
            }
        } else if (e.getSource() == textSizeSelect) {
            if (e.getStateChange() == e.SELECTED) {
                int size = Integer.parseInt(textSizeSelect.getSelectedItem().toString());
                if (size != oldFont.getSize()) {
                    newFont = new Font(oldFont.getName(), oldFont.getStyle(), size);
                    oldFont = newFont;
                }
            }
        } else if (e.getSource() == textStyleSelect) {
            if (e.getStateChange() == e.SELECTED) {
                int style = getBodyStyleNumber(textStyleSelect.getSelectedItem().toString());
                if (style != oldFont.getStyle()) {
                    newFont = new Font(oldFont.getName(), style, oldFont.getSize());
                    oldFont = newFont;
                }
            }
        }

        //---

    }


    public String getBodyStyle(int style) {
        switch (style) {
            case 0:
                return "Plain";
            case 1:
                return "Bold";
            case 2:
                return "Italic";
            default:
                return "Bold";
        }
    }

    public int getBodyStyleNumber(String style) {
        int number = 0;
        if (style.equals("Plain")) {
            number = 0;
        } else if (style.equals("Bold")) {
            number = 1;
        } else if (style.equals("Italic")) {
            number = 2;
        }
        /* switch (style) {
            case 0:
                return "Plain";
            case 1:
                return "Bold";
            case 2:
                return "Italic";
            default:
                return "Bold";
        }*/
        return number;
    }

    public class ComboRenderer extends JLabel implements ListCellRenderer {
        //ComboImage icon ;

        public ComboRenderer() {
            setOpaque(true);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String item = (String) value;
            setText(item);
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setFont(new TWFont(item, Font.PLAIN, 20));

            return this;
        }
    }

    public class TWEXComboBox extends TWComboBox {
        int yp = 0;
        int height = 4;

        public TWEXComboBox(Vector<?> items) {
            super(items);
            // setExtendedMode(true);
        }

        public Dimension getPopupSize() {
            int rows = getModel().getSize();
            int width = 0;
            for (int i = 0; i < rows; i++) {
                String item = getModel().getElementAt(i).toString();
                Font fnt = new Font(item, Font.PLAIN, 20);

                width = Math.max(width, super.getFontMetrics(fnt).stringWidth(item));
                height = Math.max(height, super.getFontMetrics(fnt).getHeight());
                item = null;
            }

            if (width < super.getSize().getWidth()) {
                return new Dimension((int) super.getSize().getWidth(), (int) super.getSize().getHeight());
            } else {
                return new Dimension(width + 20, (int) super.getSize().getHeight());
            }
        }

        public void calculateMaxNoOfRowsToDisplay() {

            int h = 25;
            Toolkit oToolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = oToolkit.getScreenSize();

            int scrh = screenSize.height;
            int avhight = scrh - (yp - 3);
            int displayablecount = avhight / height;

            int rows = getModel().getSize();
            if (displayablecount > rows) {

                setMaximumRowCount(rows);
            } else {
                setMaximumRowCount(displayablecount);
            }
        }
    }
}
