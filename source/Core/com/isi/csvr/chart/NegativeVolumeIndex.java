package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorDefaultProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Apr 4, 2006
 * Time: 4:45:07 PM
 */
public class NegativeVolumeIndex extends ChartProperties implements Indicator {
    private static final long serialVersionUID = UID_NVI;
    //Fields
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public NegativeVolumeIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_NVI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_NVI");
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorDefaultProperty idp = (IndicatorDefaultProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setOHLCPriority(idp.getOHLCPriority());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          byte destIndex) {
        if (al.size() < 2) return;
        long entryTime = System.currentTimeMillis();
        double previousNVI = 1000f;
        double prevClose = 0f;
        double prevVol = -100000d;
        double nvi;
        ChartRecord cr;


        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);

            if (cr.Volume < prevVol) {
                nvi = previousNVI * cr.Close / prevClose;
                previousNVI = nvi;
            } else {
                nvi = previousNVI;
            }
            prevClose = cr.Close;
            prevVol = cr.Volume;

            cr.setValue(destIndex, nvi);
        }

        //System.out.println("**** NVI Calc time " + (entryTime - System.currentTimeMillis()));

    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof NegativeVolumeIndex) {
            NegativeVolumeIndex nvi = (NegativeVolumeIndex) cp;
            this.innerSource = nvi.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
    }

//############################################
//implementing Indicator

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        return Language.getString("IND_NVI") + " " + parent;
    }

    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        if (al.size() < 2) return;
        long entryTime = System.currentTimeMillis();
        double previousNVI = 1000f;
        double prevClose = 0f;
        double prevVol = -100000d;
        double nvi;
        ChartRecord cr;

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);

            if (cr.Volume < prevVol) {
                nvi = previousNVI * cr.Close / prevClose;
                previousNVI = nvi;
            } else {
                nvi = previousNVI;
            }
            prevClose = cr.Close;
            prevVol = cr.Volume;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(nvi);
            }
        }

        //System.out.println("**** NVI Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_NVI");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }


}
