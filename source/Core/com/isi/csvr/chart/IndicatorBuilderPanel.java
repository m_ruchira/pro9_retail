package com.isi.csvr.chart;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.chart.customindicators.*;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.ToolBarButton;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Apr 16, 2009
 * Time: 1:44:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorBuilderPanel extends IndicatorBuilderBasePanel {

    private JSeparator verticalSeperator;
    private JSeparator topHorizontalSeperator;
    private JSeparator bottomHorizontalSeperator;
    private JSeparator verticalShadowSeperator;
    private JSeparator topHorizontalShadowTopSeperator;
    private JSeparator topHorizontalShadowBottomSeperator;
    private JSeparator bottomHorizontalShadowSeperator;
    private JPanel indicatorPanel;
    private JPanel indicatorNamePanel;
    private JScrollPane scrollPane;
    private JScrollPane messageScrollPane;
    private JScrollPane errorsScrollPane;

    public IndicatorBuilderPanel(InternalFrame p) {

        super(p);
        setConstants();

        JPanel topPanel, topLeftPanel, topRightPanel;
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"35", "1", "1", "1", "25", "1", "1", "25", "100%"}, 0, 0));
        topPanel = new JPanel(new FlexGridLayout(new String[]{"100%", "1", "1", "80"}, new String[]{"35"}, 0, 0));
        topRightPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"33"}, 0, 0));
        topLeftPanel = new JPanel(new FlexGridLayout(new String[]{"70", "60", "3", "5", "5", "65", "3", "5", "5", "1", "1", "100", "100", "90"}, new String[]{"33"}, 0, 0));

        indicatorPanel = new JPanel(new FlexGridLayout(new String[]{"10", "120", "200", "100%", "20", "10"}, new String[]{"23"}, 0, 0));
        indicatorPanel.setBackground(Theme.getColor("INDICATOR_BUILDER_INDICATOR_PANEL_COLOR"));
        indicatorPanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        JLabel lblIndicatorName = new JLabel(Language.getString("CUSTOM_INDICATOR_NAME"));
        indicatorPanel.add(new JLabel(""));
        indicatorNamePanel = new JPanel(new BorderLayout());
        indicatorNamePanel.setBorder(BorderFactory.createEmptyBorder(2, 1, 2, 1));
        indicatorNamePanel.setBackground(Theme.getColor("INDICATOR_BUILDER_INDICATOR_PANEL_COLOR"));
        indicatorNamePanel.add(txtIndicatorName, BorderLayout.CENTER);
        indicatorPanel.add(lblIndicatorName);
        indicatorPanel.add(indicatorNamePanel);
        indicatorPanel.add(new JLabel(""));
        indicatorPanel.add(btnHelp);
        txtIndicatorName.addKeyListener(this);

        topLeftPanel.add(btnNew);
        topLeftPanel.add(btnEdit);
        topLeftPanel.add(new JLabel());
        topLeftPanel.add(lblEdit);
        topLeftPanel.add(new JLabel());
        topLeftPanel.add(btnDelete);
        topLeftPanel.add(new JLabel());
        topLeftPanel.add(lblDelete);
        topLeftPanel.add(new JLabel());

        verticalSeperator = new JSeparator(JSeparator.VERTICAL);
        verticalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        topLeftPanel.add(verticalSeperator);
        verticalShadowSeperator = new JSeparator(JSeparator.VERTICAL);
        verticalShadowSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        topLeftPanel.add(verticalShadowSeperator);
        topLeftPanel.add(btnLoadText);
        topLeftPanel.add(btnSaveText);
        topLeftPanel.add(btnFunctions);

        topRightPanel.add(btnTest);
        topRightPanel.add(btnBuild);

        popEdit = new JPopupMenu();
        popDelete = new JPopupMenu();

        topLeftPanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        topRightPanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        //topMiddlePanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));

        topPanel.add(topLeftPanel);
        verticalSeperator = new JSeparator(JSeparator.VERTICAL);
        verticalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        topPanel.add(verticalSeperator);
        verticalShadowSeperator = new JSeparator(JSeparator.VERTICAL);
        verticalShadowSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        topPanel.add(verticalShadowSeperator);
        topPanel.add(topRightPanel);
        this.add(topPanel);

        //seperators
        topHorizontalShadowTopSeperator = new JSeparator(JSeparator.HORIZONTAL);
        topHorizontalShadowTopSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        this.add(topHorizontalShadowTopSeperator);
        topHorizontalSeperator = new JSeparator(JSeparator.HORIZONTAL);
        topHorizontalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        this.add(topHorizontalSeperator);
        topHorizontalShadowBottomSeperator = new JSeparator(JSeparator.HORIZONTAL);
        topHorizontalShadowBottomSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        this.add(topHorizontalShadowBottomSeperator);
        //////////

        this.add(indicatorPanel);
        //seperators
        bottomHorizontalSeperator = new JSeparator(JSeparator.HORIZONTAL);
        bottomHorizontalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        this.add(bottomHorizontalSeperator);
        bottomHorizontalShadowSeperator = new JSeparator(JSeparator.HORIZONTAL);
        bottomHorizontalShadowSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        this.add(bottomHorizontalShadowSeperator);
        //////////

        JPanel indicatorFormularPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        indicatorFormularPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 0));
        indicatorFormularPanel.add(new JLabel(Language.getString("CUSTOM_INDICATOR_FORMULA")));
        this.add(indicatorFormularPanel);

        doc = indicatorTextPane.getStyledDocument();

        scrollPane = new JScrollPane(indicatorTextPane);
        scrollPane.setSize(new Dimension(100, 400));
        scrollPane.setPreferredSize(new Dimension(100, 400));

        //scrollPane.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")));
        scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")),
                BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"))));
        centerPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT) {

            public void updateUI() {
                super.updateUI(); //To change body of overridden methods use File | Settings | File Templates.
                if (divider != null) {
                    //setHideButton();
                    toggleShowHidebutton();
                }
            }
        };
        centerPanel.setBorder(BorderFactory.createEmptyBorder());
        centerPanel.setDividerSize(25);
        centerPanel.setTopComponent(scrollPane);

        setStyles();

        //actionFrame = new ActionFrame(this);
        functionDialog = new FunctionDialog(this);

        tabbedPanel = new JPanel();
        tabbedPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")),
                BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"))));
        String[] PanelWidths_lowerCenterPanel = {"100%"};
        String[] PanelHeights_lowerCenterPanel = {"100%"};
        FlexGridLayout flexGridLayout_lowerCenterPanel = new FlexGridLayout(PanelWidths_lowerCenterPanel, PanelHeights_lowerCenterPanel, 0, 0);
        tabbedPanel.setLayout(flexGridLayout_lowerCenterPanel);

        createMessagePanel();
        messageScrollPane = new JScrollPane(messagesPane);
        errorsScrollPane = new JScrollPane(errorsPane);
        messageScrollPane.setBorder(BorderFactory.createEmptyBorder());
        errorsScrollPane.setBorder(BorderFactory.createEmptyBorder());
        //messageScrollPane.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")));
        //errorsScrollPane.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")));

        tabbedPane.addTab(Language.getString("CUSTOM_INDICATORS_MESSAGES_TAB_TITLE"), messageScrollPane);
        tabbedPane.addTab(Language.getString("CUSTOM_INDICATORS_ERRORS_TAB_TITLE"), errorsScrollPane);
        //tabbedPane.addTab(Language.getString("CUSTOM_INDICATORS_WARNINGS_TAB_TITLE"), new JScrollPane(warningsPane));

        btnErrorPanelClose = new ToolBarButton();
        btnErrorPanelClose.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/show.gif"));
        btnErrorPanelClose.updateUI();
        btnErrorPanelClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tabbedPanel.setVisible(false);
                show = false;
                toggleShowHidebutton();
                centerPanel.updateUI();
            }
        });
        btnErrorPanelShow = new ToolBarButton();
        btnErrorPanelShow.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/hide.gif"));
        btnErrorPanelShow.updateUI();
        btnErrorPanelShow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tabbedPanel.setVisible(true);
                show = true;
                toggleShowHidebutton();
                centerPanel.setDividerLocation(0.6);
                centerPanel.updateUI();
            }
        });

        //show hide button Panel
        JPanel centerClose = new JPanel();
        String[] PanelWidths_centerClose = {"100%", "15"};
        String[] PanelHeights_centerClose = {"15"};
        FlexGridLayout flexGridLayout_centerClose = new FlexGridLayout(PanelWidths_centerClose, PanelHeights_centerClose, 0, 0);
        centerClose.setLayout(flexGridLayout_centerClose);
        centerClose.add(new JLabel());
        centerClose.add(new JLabel());

        tabbedPanel.add(tabbedPane);
        centerPanel.setBottomComponent(tabbedPanel);
        tabbedPanel.setVisible(false);
        divider = ((BasicSplitPaneUI) centerPanel.getUI()).getDivider();

        toggleShowHidebutton();

        centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.add(centerPanel);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(topPanel);
        GUISettings.applyOrientation(indicatorNamePanel);
        GUISettings.applyOrientation(indicatorPanel);
        GUISettings.applyOrientation(indicatorFormularPanel);
        GUISettings.applyOrientation(tabbedPanel);
        applyTheme();

        clip = this.getToolkit().getSystemClipboard();
        addRightClickPopupMenu();

        indicatorTextPane.setSelectedTextColor(Color.LIGHT_GRAY);
        indicatorTextPane.setSelectionColor(Color.BLUE);

        registerKeyboardAction(parent, "DEL", KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    public void applyTheme() {
        super.applyTheme();


        verticalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        topHorizontalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));
        bottomHorizontalSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"));

        verticalShadowSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        topHorizontalShadowTopSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        topHorizontalShadowBottomSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));
        bottomHorizontalShadowSeperator.setForeground(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR"));

        indicatorPanel.setBackground(Theme.getColor("INDICATOR_BUILDER_INDICATOR_PANEL_COLOR"));
        indicatorNamePanel.setBackground(Theme.getColor("INDICATOR_BUILDER_INDICATOR_PANEL_COLOR"));

        //scrollPane.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")));
        scrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")),
                BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"))));

        tabbedPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_SHADOW_COLOR")),
                BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_SEPERATOR_COLOR"))));

    }

    public void setConstants() {

        keyWords = IndicatorConstants.keyWords;
        literals = IndicatorConstants.literals;
        indicators = IndicatorConstants.indicators;
    }

    public void actionPerformed(ActionEvent e) {
        Object actionObject = e.getSource();
        if (actionObject == btnClose) {
            if (dirty) {
                int option = JOptionPane.showConfirmDialog(this, Language.getString("CI_UNSAVED_WORK_CONFIRM_MESSAGE"),
                        Language.getString("CI_UNSAVED_WORK_CONFIRM_MESSAGE_TITLE"), JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.CANCEL_OPTION) {
                    return;
                }
            }
            indicatorTextPane.setText("");
            tabbedPanel.setVisible(false);
            isEditing = false;
            if (actionFrame != null) actionFrame.dispose();
            //dispose();          /TODO :
        } else if (actionObject == btnSelectAction) {
            if (dirty) {
                int option = JOptionPane.showConfirmDialog(this, Language.getString("CI_UNSAVED_WORK_CONFIRM_MESSAGE"),
                        Language.getString("CI_UNSAVED_WORK_CONFIRM_MESSAGE_TITLE"), JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.CANCEL_OPTION) {
                    return;
                }
            }
            txtIndicatorName.setText("New Custom Indicator");
            indicatorTextPane.setText("");
            actionFrame = new ActionFrame(this, ChartInterface.getChartFrameParentComponent());
            if (IndicatorFactory.getIndicatorNames() != null) {
                actionFrame.indicatorList.setListData(IndicatorFactory.getIndicatorNames());
                if (actionFrame.indicatorList.getLastVisibleIndex() != -1) {
                    actionFrame.indicatorList.setSelectedIndex(0);
                }

            }
            actionFrame.indicatorList.getSelectionModel().clearSelection();
            //actionFrame.setVisible(true);
            //actionFrame.showWindow();
        } else if (actionObject == btnLoadText) {
            if (loadText()) {
                markSyntax();
                enableButtons();
            }
        } else if (actionObject == btnSaveText) {
            saveText();
        } else if (actionObject == btnFunctions) {
            functionDialog.showWindow();
        } else if (actionObject == btnHelp) {
            NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_CUSTOM_INDICATOR"));
        } else if (actionObject == btnTest) {
            checkName();
            IndicatorConfigInfo info = new IndicatorConfigInfo(
                    "", indicatorTextPane.getText(), getNewClassName(), "true",
                    txtIndicatorName.getText().substring(0, Math.min(ShortNameLength, txtIndicatorName.getText().length())),
                    txtIndicatorName.getText(), false, Color.BLUE, Color.BLUE, (byte) StockGraph.INT_GRAPH_STYLE_LINE, false,
                    GraphDataManager.STYLE_SOLID, 1.0f);
            testCode(info);
        } else if (actionObject == btnBuild) {
            IndicatorConfigInfo info;
            checkName();

            if (isNewIndicatorName(txtIndicatorName.getText().trim())) {
                info = new IndicatorConfigInfo(
                        "", indicatorTextPane.getText(), getNewClassName(), "true",
                        txtIndicatorName.getText().trim().substring(0, Math.min(ShortNameLength, txtIndicatorName.getText().trim().length())),
                        txtIndicatorName.getText().trim(), false, Color.BLUE, Color.BLUE, (byte) StockGraph.INT_GRAPH_STYLE_LINE, false,
                        GraphDataManager.STYLE_SOLID, 1.0f);
            } else {
                info = IndicatorFactory.getIndicatoInfoForName(txtIndicatorName.getText().trim());
                /*Class indClass = null;
                try {
                    indClass = Class.forName(info.ClassName, false, new IndicatorLoader());
                    *//*for (int i = 0; i < IndicatorDetailStore.CustomIndicators.size(); i++) {
                        if (IndicatorDetailStore.CustomIndicators.get(i).getIndicatorName().equals(info.LongName)) {
                            System.out.println("++++++++++++++removed"+IndicatorDetailStore.CustomIndicators.get(i).getIndicatorName());
                            IndicatorDetailStore.CustomIndicators.remove(i);
                        }
                    }*//*
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }*/
                info.Grammer = indicatorTextPane.getText();
            }


            IndicatorConfigInfo newInfo = showInitialPropertyDialog(info);
            if (newInfo == null) return;
            buildIndicator(newInfo);

        } else if (e.getActionCommand().equals("DEL")) {
        } else if (actionObject == btnEdit) {
            tooglePopupEdit();
        } else if (actionObject == btnDelete) {
            tooglePopupDelete();
        } else if (actionObject == btnNew) {
            if (dirty) {
                int option = JOptionPane.showConfirmDialog(this, Language.getString("CI_UNSAVED_WORK_CONFIRM_MESSAGE"),
                        Language.getString("CI_UNSAVED_WORK_CONFIRM_MESSAGE_TITLE"), JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.CANCEL_OPTION) {
                    return;
                }
            }
            this.isEditing = false;
            this.editingIndicatorName = "New Custom Indicator";
            this.indicatorTextPane.setText("");
            this.txtIndicatorName.requestFocus();
            this.txtIndicatorName.setText("New Custom Indicator");
            this.txtIndicatorName.setEditable(true);
            this.txtIndicatorName.setSelectionStart(0);
            this.txtIndicatorName.setSelectionEnd(this.txtIndicatorName.getText().trim().length());
            clearMessagePanes();
            enableButtons();
        }
    }

    private boolean isNewIndicatorName(String name) {
        isNewIndicator = true;
        String[] indicatorNames = IndicatorFactory.getIndicatorNames();
        if (indicatorNames != null && indicatorNames.length > 0) {
            for (int i = 0; i < indicatorNames.length; i++) {
                if (name.equals(indicatorNames[i])) {
                    isNewIndicator = false;
                    return false;
                }
            }
        }
        return true;
    }

    public void tooglePopupDelete() {
        popDelete.removeAll();
        Point p = btnDelete.getLocation();
        String[] indicatorNames = IndicatorFactory.getIndicatorNames();
        if (indicatorNames != null && indicatorNames.length > 0) {
            for (int i = 0; i < indicatorNames.length; i++) {
                final String indicatorName = indicatorNames[i];
                TWMenuItem item = new TWMenuItem(indicatorName);
                item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        delete(indicatorName);
                    }
                });
                popDelete.add(item);
            }

            GUISettings.applyOrientation(popDelete);
            popDelete.show(btnDelete, 0, btnDelete.getHeight());
        }

    }

    public void tooglePopupEdit() {
        popEdit.removeAll();
        Point p = btnEdit.getLocation();
        final String[] indicatorNames = IndicatorFactory.getIndicatorNames();
        if (indicatorNames.length > 0) {
            for (int i = 0; i < indicatorNames.length; i++) {
                final String indicatorName = indicatorNames[i];
                TWMenuItem item = new TWMenuItem(indicatorName);
                //if (txtIndicatorName.getText().equals(indicatorName)) {
                if (editingIndicatorName.equals(indicatorName)) {
                    item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
                } else {
                    item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
                }
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        edit(indicatorName);
                    }
                });
                popEdit.add(item);
            }


            GUISettings.applyOrientation(popEdit);
            popEdit.show(btnEdit, 0, btnEdit.getHeight());
        }
    }

    public Class buildIndicator(IndicatorConfigInfo info) {

        if (!validateName()) {
            tabbedPane.setSelectedIndex(1);
            errorsPane.setText(Language.getString("CI_ERROR_MSG_INVALID_INDICATOR_NAME"));
            messagesPane.setText(Language.getString("CI_MSG_BUILD_FAILD"));
            return null;
        }


        String[] indNames = IndicatorFactory.getIndicatorNames();
        if (indNames != null) {
            for (String indName : indNames) {
                if ((info.LongName.equals(indName) ||
                        info.LongName.replace(" ", "_").equals(indName.replace(" ", "_"))) && (!editingIndicatorName.equals(indName))) {

                    String message = Language.getString("CI_NAME_ALREADY_EXISTS_MESSAGE");
                    message = message.replaceFirst("\\[NAME\\]", indName);
                    JOptionPane.showMessageDialog(this, message, Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return null;

                }
            }
        }


        int result = testCode(info);
        if (result == 0) {
            if (!isNewIndicator) {
                IndicatorFactory.alterIndicator(info);
            } else {
                IndicatorFactory.createIndicator(info);
            }
            // Loading the created class
            if (ciListeners != null) {
                try {
                    Class indClass = Class.forName(info.ClassName, false, new IndicatorLoader());
                    if (!isNewIndicator) {
                        for (CustomIndicatorListener cil : ciListeners) {
                            cil.customIndicatorDeleted(info.LongName);
                        }
                    }
                    for (CustomIndicatorListener cil : ciListeners) {
                        cil.customIndicatorCreated(info.LongName, indClass, info.Grammer);
                    }

                    //if (isEditing && !isNewIndicatorName(txtIndicatorName.getText().trim())) {
                    if (!isNewIndicator) {
                        String message = Language.getString("CI_REBUILD_SUCCESS_MESSAGE");
                        message = message.replaceFirst("\\[INDICATOR_NAME\\]", info.LongName);
                        JOptionPane.showMessageDialog(this, message);

                        //remove existing and add new one
                        for (int i = 0; i < IndicatorDetailStore.CustomIndicators.size(); i++) {
                            if (IndicatorDetailStore.CustomIndicators.get(i).getIndicatorName().equals(info.LongName)) {
                                IndicatorDetailStore.CustomIndicators.remove(i);
                            }
                        }
                        IndicatorDetailStore.CustomIndicators.add(new IndicatorMenuObject(indClass, info.LongName));
                        ChartFrame.getSharedInstance().refreshCustomIndicatorsMenu();
                    } else {
                        String message = Language.getString("CI_BUILD_SUCCESS_MESSAGE");
                        message = message.replaceFirst("\\[INDICATOR_NAME\\]", info.LongName);
                        JOptionPane.showMessageDialog(this, message);

                        IndicatorDetailStore.CustomIndicators.add(new IndicatorMenuObject(indClass, info.LongName));
                        ChartFrame.getSharedInstance().refreshCustomIndicatorsMenu();
                    }
                    editingIndicatorName = info.LongName;
                    enableButtons();
                    dirty = false;
                    //isEditing = false;
                    return indClass;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            tabbedPane.setSelectedIndex(0);
            messagesPane.setText(Language.getString("CI_MSG_BUILD_FAILD"));
        }
        return null;
    }

    public void mouseEntered(MouseEvent e) {

        if (e.getSource().equals(btnSelectAction) && btnSelectAction.isEnabled()) {
            btnSelectAction.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnLoadText) && btnLoadText.isEnabled()) {
            btnLoadText.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnSaveText) && btnSaveText.isEnabled()) {
            btnSaveText.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnFunctions) && btnFunctions.isEnabled()) {
            btnFunctions.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnHelp) && btnHelp.isEnabled()) {
            btnHelp.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnTest) && btnTest.isEnabled()) {
            btnTest.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnBuild) && btnBuild.isEnabled()) {
            btnBuild.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnNew) && btnNew.isEnabled()) {
            btnNew.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnEdit) && btnEdit.isEnabled()) {
            btnEdit.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        } else if (e.getSource().equals(btnDelete) && btnDelete.isEnabled()) {
            btnDelete.setBorder(BorderFactory.createLineBorder(Theme.getColor("INDICATOR_BUILDER_BUTTON_BORDER_COLOR")));
        }
    }


    public void mouseExited(MouseEvent e) {

        if (e.getSource().equals(btnSelectAction)) {
            btnSelectAction.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnLoadText)) {
            btnLoadText.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnSaveText)) {
            btnSaveText.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnFunctions)) {
            btnFunctions.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnHelp)) {
            btnHelp.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnTest)) {
            btnTest.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnBuild)) {
            btnBuild.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnNew)) {
            btnNew.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnEdit)) {
            btnEdit.setBorder(BorderFactory.createEmptyBorder());
        } else if (e.getSource().equals(btnDelete)) {
            btnDelete.setBorder(BorderFactory.createEmptyBorder());
        }
    }

    protected void edit(String indicatorName) {
        this.isEditing = true;
        this.editingIndicatorName = indicatorName;
        if (indicatorName != null) {
            IndicatorConfigInfo ici = IndicatorFactory.getIndicatoInfoForName(indicatorName);
            this.txtIndicatorName.setText(indicatorName);
            this.txtIndicatorName.setEditable(true);
            this.indicatorTextPane.setText("");
            this.indicatorTextPane.setText(ici.Grammer);
            markSyntax();
        }
        enableButtons();
        this.dirty = false;
        clearMessagePanes();
    }

    protected void delete(String indicatorName) {
        int option = JOptionPane.showConfirmDialog(this, Language.getString("CI_DELETE_CONFIRM_MESSAGE"),
                Language.getString("CI_DELETE_CONFIRM_MESSAGE_TITLE"), JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.NO_OPTION) return;
        if (indicatorName != null) {
            IndicatorFactory.removeIndicator(indicatorName);

            //this is not executing now
            for (CustomIndicatorListener cil : this.ciListeners) {
                cil.customIndicatorDeleted(indicatorName);
                ChartFrame.getSharedInstance().refreshCustomIndicatorsMenu();
            }
            File classFile = new File(IndicatorFactory.classPath + indicatorName + ".ind");
            classFile.delete();

            IndicatorMenuObject indicatorMenuObject = null;
            for (IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
                if (imo.getIndicatorName().equals(indicatorName)) {
                    indicatorMenuObject = imo;
                }
            }
            if (indicatorMenuObject != null) {
                ChartFrame.getSharedInstance().customIndicators.remove(indicatorMenuObject);
                IndicatorDetailStore.CustomIndicators.remove(indicatorMenuObject);
                ChartFrame.getSharedInstance().refreshCustomIndicatorsMenu();
            }
            if (txtIndicatorName.getText().equals(indicatorName)) {
                indicatorTextPane.setText("");
                txtIndicatorName.setText("New Custom Indicator");
                editingIndicatorName = "New Custom Indicator";
                isEditing = false;
            }
        }
        this.txtIndicatorName.setEditable(true);
        enableButtons();
    }


    protected void clearMessagePanes() {
        messagesPane.setText("");
        errorsPane.setText("");
        warningsPane.setText("");
    }

    public void mouseClicked(MouseEvent e) {
        Object obj = e.getSource();
        if (obj == lblDelete) {
            tooglePopupDelete();
        } else if (obj == lblEdit) {
            tooglePopupEdit();
        }
    }

    public TWTextField getTxtIndicatorName() {
        return txtIndicatorName;
    }

    public JScrollPane getScrollPane() {
        return scrollPane;
    }
}
