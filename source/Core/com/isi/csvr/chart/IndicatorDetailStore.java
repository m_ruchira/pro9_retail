package com.isi.csvr.chart;

import com.isi.csvr.chart.analysistechniques.IATXmlSerializeService;
import com.isi.csvr.chart.customindicators.IndicatorMenuObject;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jan 9, 2009
 * Time: 11:31:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorDetailStore {

    public static ArrayList<IndicatorMenuObject> alStrategies = new ArrayList<IndicatorMenuObject>();
    public static ArrayList<IndicatorMenuObject> alPatterns = new ArrayList<IndicatorMenuObject>();
    public static ArrayList<IndicatorMenuObject> CustomIndicators = new ArrayList<IndicatorMenuObject>();


    public static ArrayList<String> alFavouiteIndicators;
    public static ArrayList<String> alFavouriteStrategies;
    public static ArrayList<String> alFaviouritePatterns;
    public static ArrayList<String> alFaviouriteCustIndicators;

    public static ArrayList<String> getalFavouiteIndicators() {

        IATXmlSerializeService iatXmlService = new IATXmlSerializeService();
        alFavouiteIndicators = iatXmlService.getFaviouriteList(ChartConstants.TYPE_INDICATOR);

        return alFavouiteIndicators;
    }

    public static ArrayList<String> getalFavouriteStrategies() {

        IATXmlSerializeService iatXmlService = new IATXmlSerializeService();
        alFavouriteStrategies = iatXmlService.getFaviouriteList(ChartConstants.TYPE_STRATEGY);

        return alFavouriteStrategies;
    }

    public static ArrayList<String> getalFaviouritePatterns() {

        IATXmlSerializeService iatXmlService = new IATXmlSerializeService();
        alFaviouritePatterns = iatXmlService.getFaviouriteList(ChartConstants.TYPE_PATTERN);

        return alFaviouritePatterns;
    }

    public static ArrayList<String> getalFaviouriteCustIndicators() {

        IATXmlSerializeService iatXmlService = new IATXmlSerializeService();
        alFaviouriteCustIndicators = iatXmlService.getFaviouriteList(ChartConstants.TYPE_CUSTOM);

        return alFaviouriteCustIndicators;
    }
}
