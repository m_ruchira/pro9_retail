package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorDefaultProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 6:22:23 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class ChaikinOscillator extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_CHAIKINS_OSCILL;
    //Fields
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public ChaikinOscillator(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_CHAIKINS_OSCILL") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_CHAIKINS_OSCILL");
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorDefaultProperty idp = (IndicatorDefaultProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setOHLCPriority(idp.getOHLCPriority());
        }
    }

    public static double getChaikinAD(LinkedList<ChartRecord> ll) {
        if (ll.size() < 10) return 0.0;

        ChartRecord cr, newCr;

        // 3 steps are involved: AccumDistrb, MA3, MA10
        byte stepAccDist = ChartRecord.STEP_1;
        byte stepMA3 = ChartRecord.STEP_2;
        byte stepMA10 = ChartRecord.STEP_3;

        ArrayList al = new ArrayList();
        for (int i = 0; i < ll.size(); i++) {
            cr = ll.get(i);

            newCr = new ChartRecord();
            newCr.Open = cr.Open;
            newCr.Close = cr.Close;
            newCr.High = cr.High;
            newCr.Low = cr.Low;
            newCr.Volume = cr.Volume;
            newCr.Time = cr.Time;
            newCr.setStepSize(3);
            al.add(newCr);
        }

        //calculating accum. distrib.
        AccumulationDistribution.getAccumulationDistribution(al, stepAccDist);
        //calculating exp. mov. avgs
        MovingAverage.getMovingAverage(al, 0, 3, MovingAverage.METHOD_EXPONENTIAL, stepAccDist, stepMA3);
        MovingAverage.getMovingAverage(al, 0, 10, MovingAverage.METHOD_EXPONENTIAL, stepAccDist, stepMA10);

        cr = (ChartRecord) al.get(al.size() - 1);
        newCr = null;
        al = null;
        return cr.getStepValue(stepMA3) - cr.getStepValue(stepMA10);
    }

    public static void calculateIndicatorOld(GraphDataManager GDM, ArrayList al, int bIndex,
                                             byte stepAccDist, byte stepMA3, byte stepMA10, byte destIndex) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        //calculating accum. distrib.
        AccumulationDistribution.getAccumulationDistribution(al, stepAccDist);
        //calculating exp. mov. avgs
        MovingAverage.getMovingAverage(al, bIndex, 3, MovingAverage.METHOD_EXPONENTIAL, stepAccDist, stepMA3);
        MovingAverage.getMovingAverage(al, bIndex, 10, MovingAverage.METHOD_EXPONENTIAL, stepAccDist, stepMA10);
        //final calculation
        double currVal;
        ChartRecord cr;
        for (int i = 0; i < bIndex + 9; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = bIndex + 9; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(stepMA3) - cr.getValue(stepMA10);
            cr.setValue(destIndex, currVal);
        }
        //System.out.println("**** Inner Chaikin Oscillator Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          byte stepAccDist, byte stepMA3, byte stepMA10, byte destIndex) {
        //calculating accum. distrib.
        AccumulationDistribution.getAccumulationDistribution(al, stepAccDist);

        //calculating exp. mov. avgs
        MovingAverage.getMovingAverage(al, bIndex, 3, MovingAverage.METHOD_EXPONENTIAL, stepAccDist, stepMA3);
        MovingAverage.getMovingAverage(al, bIndex, 10, MovingAverage.METHOD_EXPONENTIAL, stepAccDist, stepMA10);

        //final calculation
        double currVal;
        ChartRecord cr;
        for (int i = 0; i < bIndex + 9; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = bIndex + 9; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(stepMA3) - cr.getValue(stepMA10);
            cr.setValue(destIndex, currVal);
        }

    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 3;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof ChaikinOscillator) {
            ChaikinOscillator co = (ChaikinOscillator) cp;
            this.innerSource = co.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        return Language.getString("IND_CHAIKINS_OSCILL") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        if (innerSource instanceof Indicator) {
            return ((Indicator) innerSource).hasItsOwnScale();
        }
        return false;
    }
    //############################################

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //This calculation uses 3 intermediate steps: AccumDistrb, MA3, MA10
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        ChartRecord cr;

        //setting the steps
        // 3 steps are involved: AccumDistrb, MA3, MA10
        byte stepAccDist = ChartRecord.STEP_1;
        byte stepMA3 = ChartRecord.STEP_2;
        byte stepMA10 = ChartRecord.STEP_3;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }

        //calculating accum. distrib.
        AccumulationDistribution.getAccumulationDistribution(al, stepAccDist);
        //calculating exp. mov. avgs
        MovingAverage.getMovingAverage(al, 0, 3, MovingAverage.METHOD_EXPONENTIAL, stepAccDist, stepMA3);
        MovingAverage.getMovingAverage(al, 0, 10, MovingAverage.METHOD_EXPONENTIAL, stepAccDist, stepMA10);

        //final calculation
        double currVal;
        for (int i = 9; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getStepValue(stepMA3) - cr.getStepValue(stepMA10);

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(currVal);
        }
        //System.out.println("**** Chaikin Oscillator Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_CHAIKINS_OSCILL");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
