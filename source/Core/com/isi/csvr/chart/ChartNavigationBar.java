package com.isi.csvr.chart;

import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: june 16, 2008
 * Time: 10:36:11 AM
 * To change this template use File | Settings | File Templates.
 */


public class ChartNavigationBar extends JPanel {
    private static ChartNavigationBar self;
    public JPanel leftTriggerMainPan;
    private JPanel mainPanel;
    private JPanel centerPanel;
    private JButton clickButton;
    private JButton dragButton;
    private ViewSetting viewSettings;
    private JPanel leftSideBarPan;
    private String selectedKey;
    private ViewSetting oSetting;
    private Dimension oldSize;

    public ChartNavigationBar() {
        mainPanel = new JPanel(new BorderLayout());
        centerPanel = new JPanel(new BorderLayout());
        clickButton = new JButton();
        dragButton = new JButton();
        oldSize = new Dimension(120, 1);
//        createSideBar();
    }

    public static ChartNavigationBar getSharedInstance() {
        if (self == null) {
            self = new ChartNavigationBar();

        }
        return self;
    }

    public boolean isNeedsToDisplay() {
        return viewSettings.isVisible();
    }

    public JPanel createSideBar() {
        oSetting = ViewSettingsManager.getSummaryView("CHART_NAVIGATION_BAR");
        //oSetting.setParent(this);
        final JButton leftTriggerBtn1 = new JButton();
        final JButton leftTriggerBtn2 = new JButton();
        leftSideBarPan = new JPanel(new BorderLayout());
        leftSideBarPan.add(ChartSymbolNavigator.getSharedInstance(), BorderLayout.CENTER);
        leftTriggerBtn1.setBackground(Color.RED);
        leftTriggerBtn1.setPreferredSize(new Dimension(2, 1));
        leftTriggerBtn2.setPreferredSize(new Dimension(2, 1));
        leftTriggerMainPan = new JPanel(new BorderLayout());
        leftTriggerMainPan.setPreferredSize(new Dimension(2, 1));
        leftTriggerBtn2.addMouseMotionListener(new MouseMotionListener() {
            public void mouseDragged(MouseEvent e) {
                if (ChartSymbolNavigator.getSharedInstance().isVisible()) {
                    Point point = SwingUtilities.convertPoint(leftTriggerBtn2, e.getX(), e.getY(), leftTriggerBtn1);
                    if (!Language.isLTR()) {
                        point = new Point(Math.abs(point.x), Math.abs(point.y));
                    }
                    if (point.x < 120) {
                        leftTriggerMainPan.setPreferredSize(new Dimension(120, 1));
                        leftTriggerMainPan.doLayout();
                        leftTriggerMainPan.updateUI();
                        leftSideBarPan.doLayout();
                        leftSideBarPan.updateUI();

//                        window.getContentPane().doLayout();
//                        cNBlistener.chartNavigationBarChanged();
                    } else {
                        leftTriggerMainPan.setPreferredSize(new Dimension(point.x, 1));
                        leftTriggerMainPan.doLayout();
                        leftTriggerMainPan.updateUI();
                        leftSideBarPan.doLayout();
                        leftSideBarPan.updateUI();
//                        window.getContentPane().doLayout();
//                        cNBlistener.chartNavigationBarChanged();
                    }

                }

            }

            public void mouseMoved(MouseEvent e) {
                leftTriggerBtn2.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
                leftTriggerBtn2.setBackground(Color.GRAY);
            }

            public void mouseReleased(MouseEvent e) {


            }
        });
        leftTriggerMainPan.setBorder(BorderFactory.createEmptyBorder());
        leftTriggerBtn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controlSideBar();
            }
        });
        if (Language.isLTR()) {
            leftTriggerMainPan.add(leftTriggerBtn1, BorderLayout.WEST);
            leftTriggerMainPan.add(leftSideBarPan, BorderLayout.CENTER);
            leftTriggerMainPan.add(leftTriggerBtn2, BorderLayout.EAST);
            // window.getContentPane().add(leftTriggerMainPan, BorderLayout.WEST);
        } else {
            leftTriggerMainPan.add(leftTriggerBtn1, BorderLayout.EAST);
            leftTriggerMainPan.add(leftSideBarPan, BorderLayout.CENTER);
            leftTriggerMainPan.add(leftTriggerBtn2, BorderLayout.WEST);
            // window.getContentPane().add(leftTriggerMainPan, BorderLayout.EAST);
        }
        return leftTriggerMainPan;

//JPanel pnl = new JPanel();
//        pnl.setPreferredSize(new Dimension(100,200));
//return pnl;
    }

    public void closeSideBar() {

        ChartSymbolNavigator.getSharedInstance().setVisible(false);
        ChartFrame.getSharedInstance().doNavigationBarLayout(0);

    }

    public void setMainPainPreferedsize() {
        if (leftTriggerMainPan != null && leftTriggerMainPan.getPreferredSize().width >= 120) {

            oldSize = leftTriggerMainPan.getPreferredSize();
            leftTriggerMainPan.setPreferredSize(new Dimension(2, 1));
//        window.getContentPane().doLayout();
//        cNBlistener.chartNavigationBarChanged();
            oSetting.setSize(oldSize);
            oSetting.putProperty(99, oldSize.width);
        }
    }

    public String getSelectedKey() {
        return selectedKey;
    }

    public void setSideBar() {
        leftTriggerMainPan.setPreferredSize(new Dimension(ChartSymbolNavigator.getSharedInstance().getSavedSize().width + 4, ChartSymbolNavigator.getSharedInstance().getSavedSize().height));
        ChartSymbolNavigator.getSharedInstance().applySettings();
        leftTriggerMainPan.doLayout();
        leftTriggerMainPan.updateUI();
        leftSideBarPan.doLayout();
        leftSideBarPan.updateUI();
//        window.getContentPane().doLayout();
//        cNBlistener.chartNavigationBarChanged();
    }

    public void controlSideBar() {
        if (ChartSymbolNavigator.getSharedInstance().isVisible() && leftTriggerMainPan.getPreferredSize().width >= 120) {
            closeSideBar();
        } else {
            int barwidth;
            if (oSetting.getSize().width < 120) {

                if (oldSize.width <= 120) {
                    try {
                        barwidth = Integer.parseInt(oSetting.getProperty(99)) + 4;
                        leftTriggerMainPan.setPreferredSize(new Dimension(Integer.parseInt(oSetting.getProperty(99)) + 4, 1));
                    } catch (Exception e) {
                        barwidth = 200 + 4;
                        leftTriggerMainPan.setPreferredSize(new Dimension(200 + 4, 1));
                    }

                } else {
                    barwidth = (int) oldSize.getWidth();
                    leftTriggerMainPan.setPreferredSize(oldSize);
                }
            } else {
                barwidth = (int) oSetting.getSize().getWidth();
                leftTriggerMainPan.setPreferredSize(oSetting.getSize());
            }

//            ChartFrame.getSharedInstance().doNavigationBarLayout(barwidth);
            ChartSymbolNavigator.getSharedInstance().setVisible(true);
            leftTriggerMainPan.doLayout();
            leftTriggerMainPan.updateUI();
            leftSideBarPan.doLayout();
            leftSideBarPan.updateUI();


        }

    }
}
