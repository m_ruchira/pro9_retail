package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Mar 31, 2005 - Time: 7:39:22 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorZigZagProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class ZigZag extends ChartProperties implements Indicator, Serializable {
    //Fields
    public static final int METHOD_PERCENT = 0;
    public static final int METHOD_POINTS = 1;

    //state
    final static int STATE_NEGATIVE = -1;
    final static int STATE_ZERO = 0;
    final static int STATE_POSITIVE = 1;
    private static final long serialVersionUID = UID_ZigZag;
    protected float reversalAmount;
    protected int method;
    protected ChartProperties innerSource;
    protected String tableColumnHeading;

    public ZigZag(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_ZIGZAG") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_ZIGZAG");
        this.reversalAmount = 5.0f;
        this.method = METHOD_PERCENT;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorZigZagProperty idp = (IndicatorZigZagProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());

            this.setReversalAmount(idp.getReversalAmount());
            this.setMethod(idp.getMethod());
        }
    }

    public static String[] getMethodArray() {
        return new String[]{Language.getString("PERCENT"), Language.getString("POINTS")};
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          double reversalAmount, CalcMethod calculationMethod, byte srcIndex, byte destIndex) {

        ChartRecord cr;
        int calcMethod = calculationMethod.equals(CalcMethod.PERCENT) ? 0 : 1;
        long entryTime = System.currentTimeMillis();

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////

        if (bIndex >= al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        int dynIndex, minIndex, maxIndex;
        int state = STATE_ZERO, tmpState;
        double currVal, dynVal, minVal, maxVal;

        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        cr = (ChartRecord) al.get(bIndex);
        currVal = cr.getValue(srcIndex);
        cr.setValue(destIndex, currVal);
        minVal = currVal;
        maxVal = currVal;
        minIndex = bIndex;
        maxIndex = bIndex;
        dynVal = currVal;
        dynIndex = bIndex;


        for (int i = bIndex + 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(srcIndex);
            cr.setValue(destIndex, Indicator.NULL);
            switch (state) {
                case STATE_ZERO:
                    state = compareVals(currVal, maxVal, calcMethod, reversalAmount);
                    if (state == STATE_ZERO) {
                        state = compareVals(currVal, minVal, calcMethod, reversalAmount);
                        if (state == STATE_ZERO) {
                            if (currVal > maxVal) {
                                maxVal = currVal;
                                maxIndex = i;
                            } else if (currVal < minVal) {
                                minVal = currVal;
                                minIndex = i;
                            }
                        } else if (minIndex != bIndex) {
                            setIndicatorValue(al, destIndex, minIndex, minVal);
                            dynVal = currVal;
                            dynIndex = i;
                        }
                    } else if (maxIndex != bIndex) {
                        setIndicatorValue(al, destIndex, maxIndex, maxVal);
                        dynVal = currVal;
                        dynIndex = i;
                    }
                    break;
                case STATE_POSITIVE:
                    tmpState = compareVals(currVal, dynVal, calcMethod, reversalAmount);
                    switch (tmpState) {
                        case STATE_ZERO:
                            if (currVal > dynVal) {
                                dynVal = currVal;
                                dynIndex = i;
                            }
                            break;
                        case STATE_POSITIVE:
                            dynVal = currVal;
                            dynIndex = i;
                            break;
                        case STATE_NEGATIVE:
                            setIndicatorValue(al, destIndex, dynIndex, dynVal);
                            dynVal = currVal;
                            dynIndex = i;
                            state = STATE_NEGATIVE;
                            break;
                    }
                    break;
                case STATE_NEGATIVE:
                    tmpState = compareVals(currVal, dynVal, calcMethod, reversalAmount);
                    switch (tmpState) {
                        case STATE_ZERO:
                            if (currVal < dynVal) {
                                dynVal = currVal;
                                dynIndex = i;
                            }
                            break;
                        case STATE_POSITIVE:
                            setIndicatorValue(al, destIndex, dynIndex, dynVal);
                            dynVal = currVal;
                            dynIndex = i;
                            state = STATE_POSITIVE;
                            break;
                        case STATE_NEGATIVE:
                            dynVal = currVal;
                            dynIndex = i;
                            break;
                    }
                    break;
            }
            if (i == al.size() - 1) {
                setIndicatorValue(al, destIndex, dynIndex, dynVal);
                if ((dynIndex != i) && (dynIndex != 0)) {
                    setIndicatorValue(al, destIndex, i, currVal);
                }
            }
        }
        //System.out.println("**** Inner ZigZag Calc time " + (entryTime - System.currentTimeMillis()));
    }

    protected static int compareVals(double currVal, double prevVal, int calcMethod, double reversalAmount) {
        int result = STATE_ZERO;
        switch (calcMethod) {
            case METHOD_PERCENT:
                if (currVal > prevVal * (1 + reversalAmount / 100f)) {
                    result = STATE_POSITIVE;
                } else if (currVal < prevVal * (1 - reversalAmount / 100f)) {
                    result = STATE_NEGATIVE;
                }
                break;
            case METHOD_POINTS:
                if (currVal > prevVal + reversalAmount) {
                    result = STATE_POSITIVE;
                } else if (currVal < prevVal - reversalAmount) {
                    result = STATE_NEGATIVE;
                }
                break;
        }
        return result;
    }

    protected static void setIndicatorValue(ArrayList al, byte srcIndex, int timeIndex, double value) {
        ChartRecord cr = (ChartRecord) al.get(timeIndex);
        cr.setValue(srcIndex, value);
    }

    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.reversalAmount = 5.0f;
        this.method = METHOD_PERCENT;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.method = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/Method"));
        this.reversalAmount = Float.parseFloat(TemplateFactory.loadProperty(xpath, document, preExpression + "/ReversalAmount"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "Method", Integer.toString(this.method));
        TemplateFactory.saveProperty(chart, document, "ReversalAmount", Float.toString(this.reversalAmount));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof ZigZag) {
            ZigZag zig = (ZigZag) cp;
            this.reversalAmount = zig.reversalAmount;
            this.method = zig.method;
            this.innerSource = zig.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += "(" + reversalAmount + ")";
        return Language.getString("IND_ZIGZAG") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        if (innerSource instanceof Indicator) {
            return ((Indicator) innerSource).hasItsOwnScale();
        }
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public float getReversalAmount() {
        return reversalAmount;
    }

    public void setReversalAmount(float ra) {
        reversalAmount = ra;
    }

    //method
    public int getMethod() {
        return method;
    }
    //############################################

    public void setMethod(int m) {
        method = m;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        long dynTime;
        byte state = 0, tmpState;
        double currVal, dynVal;
        ChartRecord cr;

        cr = (ChartRecord) al.get(0);
        currVal = cr.getValue(getOHLCPriority());
        addIndicatorPoint(GDM, index, cr.Time, currVal);
        dynVal = currVal;
        dynTime = cr.Time;

        for (int i = 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(getOHLCPriority());
            GDM.removeIndicatorPoint(cr.Time, index, getID());
            switch (state) {
                case 0:
                    state = compareVals(currVal, dynVal);
                    if (state != 0) {
                        dynVal = currVal;
                        dynTime = cr.Time;
                    }
                    break;
                case 1:
                    tmpState = compareVals(currVal, dynVal);
                    switch (tmpState) {
                        case 0:
                            if (currVal > dynVal) {
                                dynVal = currVal;
                                dynTime = cr.Time;
                            }
                            break;
                        case 1:
                            //addIndicatorPoint(GDM, index, dynTime, dynVal);
                            dynVal = currVal;
                            dynTime = cr.Time;
                            break;
                        case -1:
                            addIndicatorPoint(GDM, index, dynTime, dynVal);
                            dynVal = currVal;
                            dynTime = cr.Time;
                            state = -1;
                            break;
                    }
                    break;
                case -1:
                    tmpState = compareVals(currVal, dynVal);
                    switch (tmpState) {
                        case 0:
                            if (currVal < dynVal) {
                                dynVal = currVal;
                                dynTime = cr.Time;
                            }
                            break;
                        case 1:
                            addIndicatorPoint(GDM, index, dynTime, dynVal);
                            dynVal = currVal;
                            dynTime = cr.Time;
                            state = 1;
                            break;
                        case -1:
                            //addIndicatorPoint(GDM, index, dynTime, dynVal);
                            dynVal = currVal;
                            dynTime = cr.Time;
                            break;
                    }
                    break;
            }
            if (i == al.size() - 1) {
                addIndicatorPoint(GDM, index, dynTime, dynVal);
                if (dynTime != cr.Time) {
                    addIndicatorPoint(GDM, index, cr.Time, currVal);
                }
            }
        }
        //System.out.println("**** ZigZag Calc time " + (entryTime - System.currentTimeMillis()));
    }

    protected void addIndicatorPoint(GraphDataManagerIF GDM, int index, long time, double value) {
        ChartPoint aPoint = GDM.getIndicatorPoint(time, index, getID());
        if (aPoint != null) {
            aPoint.setIndicatorValue(value);
        }
    }

    private byte compareVals(double currVal, double prevVal) {
        byte result = 0;
        switch (method) {
            case METHOD_PERCENT:
                if (currVal > prevVal * (1 + reversalAmount / 100f)) {
                    result = 1;
                } else if (currVal < prevVal * (1 - reversalAmount / 100f)) {
                    result = -1;
                }
                break;
            case METHOD_POINTS:
                if (currVal > prevVal + reversalAmount) {
                    result = 1;
                } else if (currVal < prevVal - reversalAmount) {
                    result = -1;
                }
                break;
        }
        return result;
    }

    public String getShortName() {
        return Language.getString("IND_ZIGZAG");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}


