package com.isi.csvr.chart;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.Client;
import com.isi.csvr.TWFileFilter;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.chart.customindicators.CustomIndicatorListener;
import com.isi.csvr.chart.customindicators.IndicatorConfigInfo;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.IndicatorParser;
import com.isi.csvr.chart.customindicators.statements.CIProgram;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.io.*;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Apr 16, 2009
 * Time: 11:29:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorBuilderBasePanel extends JPanel implements Themeable, KeyListener, ClipboardOwner, ActionListener, MouseListener {

    //data
    public static String[] keyWords = {};
    public static String[] literals = {};
    public static String[] indicators = {};
    protected static boolean show = false;
    protected final JPopupMenu popupMenu = new JPopupMenu("test");
    //added for parsing
    protected final int ShortNameLength = 8;
    public TWTabbedPane tabbedPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Bottom, TWTabbedPane.CONTENT_PLACEMENT.Absolute, TWTabbedPane.COMPONENT_PLACEMENT.Right, TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout, "sc", false) {
        public void updateUI() {
            super.updateUI();
        }
    };
    //ui
    protected TWTextField txtIndicatorName;
    protected TWButton btnClose;
    protected JTextPane indicatorTextPane;
    protected JTextPane messagesPane;
    protected JTextPane errorsPane;
    protected JTextPane warningsPane;
    protected TWMenuItem itemCut = new TWMenuItem(Language.getString("IND_CUT"));
    protected TWMenuItem itemCopy = new TWMenuItem(Language.getString("COPY"));
    protected TWMenuItem itemPaste = new TWMenuItem(Language.getString("IND_PASTE"));
    protected TWMenuItem itemDelete = new TWMenuItem(Language.getString("DELETE"));
    //buttons
    protected JButton btnSelectAction;
    protected JButton btnLoadText;
    protected JButton btnSaveText;
    protected JButton btnFunctions;
    protected JButton btnHelp;
    protected JButton btnTest;
    protected JButton btnBuild;
    protected JButton btnNew;
    protected JButton btnEdit;
    protected JButton btnDelete;
    protected JButton btnCopy;
    protected JPopupMenu popEdit;
    protected JPopupMenu popDelete;
    protected JLabel lblEdit;
    protected JLabel lblDelete;
    protected Clipboard clip;
    protected JPanel tabbedPanel;
    protected JSplitPane centerPanel;
    protected ToolBarButton btnErrorPanelClose;
    protected ToolBarButton btnErrorPanelShow;
    protected StyledDocument doc;
    protected StyledDocument messageDoc;
    protected StyledDocument errorDoc;
    protected StyledDocument warningDdoc;
    protected ActionFrame actionFrame;
    protected FunctionDialog functionDialog;
    protected IndicatorParser parser;
    protected boolean testFailed;
    protected boolean dirty = false;
    protected boolean isEditing = false;
    protected String editingIndicatorName = "New Custom Indicator";
    protected boolean isNewIndicator = false;
    // to communicate the results to main form
    protected Vector<CustomIndicatorListener> ciListeners = null;
    protected String copyText = "";
    protected InternalFrame parent;
    BasicSplitPaneDivider divider = null;

    public IndicatorBuilderBasePanel(InternalFrame p) {

        this.parent = p;
        ciListeners = new Vector<CustomIndicatorListener>();

        btnSelectAction = new JButton(Language.getString("CUSTOM_INDICATOR_SELECT_ACTION"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/select_action.gif"));
        btnLoadText = new JButton(Language.getString("CUSTOM_INDICATOR_LOAD_TEXT"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/load_text.gif"));
        btnSaveText = new JButton(Language.getString("CUSTOM_INDICATOR_SAVE_TEXT"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/save_text.gif"));
        btnFunctions = new JButton(Language.getString("CUSTOM_INDICATOR_FUNCTIONS"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/functions.gif"));
        //btnHelp = new JButton(Language.getString("HELP"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/help.gif"));
        btnHelp = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/charts/indicator_help.gif"));
        btnTest = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/charts/build.gif"));
        btnBuild = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/charts/test.gif"));
        btnNew = new JButton(Language.getString("NEW"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/new.gif"));
        btnEdit = new JButton(Language.getString("EDIT"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/edit.gif"));
        btnCopy = new JButton(Language.getString("COPY"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/copy.gif"));
        btnDelete = new JButton(Language.getString("DELETE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/delete.gif"));

        lblEdit = new JLabel(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        lblDelete = new JLabel(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));

        lblEdit.addMouseListener(this);
        lblDelete.addMouseListener(this);

        btnSelectAction.setHorizontalAlignment(SwingConstants.CENTER);
        btnLoadText.setHorizontalAlignment(SwingConstants.CENTER);
        btnSaveText.setHorizontalAlignment(SwingConstants.CENTER);
        btnFunctions.setHorizontalAlignment(SwingConstants.CENTER);
        btnHelp.setHorizontalAlignment(SwingConstants.CENTER);
        btnNew.setHorizontalAlignment(SwingConstants.CENTER);
        btnEdit.setHorizontalAlignment(SwingConstants.CENTER);
        btnCopy.setHorizontalAlignment(SwingConstants.CENTER);
        btnDelete.setHorizontalAlignment(SwingConstants.CENTER);

        //set fonts
        btnSelectAction.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnLoadText.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnSaveText.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnFunctions.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnHelp.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnTest.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnBuild.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnNew.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnEdit.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnCopy.setFont(new Font("Tahoma", Font.PLAIN, 11));
        btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 11));


        txtIndicatorName = new TWTextField("New Custom Indicator", 25);
        Dimension d = new Dimension(70, 20);
        txtIndicatorName.setPreferredSize(d);

        indicatorTextPane = new JTextPane();
        indicatorTextPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        indicatorTextPane.setFont(new TWFont("Courier New", Font.PLAIN, 12));
        indicatorTextPane.addKeyListener(this);

        btnTest.setToolTipText(Language.getString("CUSTOM_INDICATOR_TEST"));
        btnBuild.setToolTipText(Language.getString("CUSTOM_INDICATOR_BUILD"));

        //remove the button look
        btnSelectAction.setBorder(BorderFactory.createEmptyBorder());
        btnLoadText.setBorder(BorderFactory.createEmptyBorder());
        btnSaveText.setBorder(BorderFactory.createEmptyBorder());
        btnFunctions.setBorder(BorderFactory.createEmptyBorder());
        btnHelp.setBorder(BorderFactory.createEmptyBorder());
        btnTest.setBorder(BorderFactory.createEmptyBorder());
        btnBuild.setBorder(BorderFactory.createEmptyBorder());
        btnNew.setBorder(BorderFactory.createEmptyBorder());
        btnEdit.setBorder(BorderFactory.createEmptyBorder());
        btnCopy.setBorder(BorderFactory.createEmptyBorder());
        btnDelete.setBorder(BorderFactory.createEmptyBorder());

        btnSelectAction.setContentAreaFilled(false);
        btnLoadText.setContentAreaFilled(false);
        btnSaveText.setContentAreaFilled(false);
        btnFunctions.setContentAreaFilled(false);
        btnHelp.setContentAreaFilled(false);
        btnTest.setContentAreaFilled(false);
        btnBuild.setContentAreaFilled(false);
        btnNew.setContentAreaFilled(false);
        btnEdit.setContentAreaFilled(false);
        btnCopy.setContentAreaFilled(false);
        btnDelete.setContentAreaFilled(false);

        btnSelectAction.addMouseListener(this);
        btnLoadText.addMouseListener(this);
        btnSaveText.addMouseListener(this);
        btnFunctions.addMouseListener(this);
        btnHelp.addMouseListener(this);
        btnTest.addMouseListener(this);
        btnBuild.addMouseListener(this);
        btnNew.addMouseListener(this);
        btnEdit.addMouseListener(this);
        btnCopy.addMouseListener(this);
        btnDelete.addMouseListener(this);

        btnSelectAction.addActionListener(this);
        btnLoadText.addActionListener(this);
        btnSaveText.addActionListener(this);
        btnFunctions.addActionListener(this);
        btnHelp.addActionListener(this);
        btnTest.addActionListener(this);
        btnBuild.addActionListener(this);
        btnNew.addActionListener(this);
        btnEdit.addActionListener(this);
        btnCopy.addActionListener(this);
        btnDelete.addActionListener(this);

    }

    public static String getNewClassName() {
        return "_CUI_" + System.currentTimeMillis();
    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT ||
                e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN) {
            return;
        }

        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            System.out.println("**************");
        }
        if (getIndicatorTextPane().hasFocus()) {
            markSyntax();
        }
        dirty = true;
        enableButtons();
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }

    protected void enableButtons() {

        boolean txtNotEmpty = (indicatorTextPane.getText() != null) && (indicatorTextPane.getText().length() > 0);
        boolean nameNotEmpty = (txtIndicatorName.getText() != null) && (txtIndicatorName.getText().trim().length() > 0);
        String[] indicatorNames = IndicatorFactory.getIndicatorNames();

        boolean hasIndicators = false;
        if (indicatorNames != null && indicatorNames.length > 0) {
            hasIndicators = true;
        }
        btnEdit.setEnabled(hasIndicators);
        btnDelete.setEnabled(hasIndicators);
        lblEdit.setEnabled(hasIndicators);
        lblDelete.setEnabled(hasIndicators);
        btnBuild.setEnabled(txtNotEmpty && nameNotEmpty);
        btnTest.setEnabled(txtNotEmpty);
        btnSaveText.setEnabled(txtNotEmpty);
    }

    public void markSyntax() {
        //First initialize all
        String fullText = getIndicatorTextPane().getText();
        int caretPos = getIndicatorTextPane().getCaretPosition();
        fullText = fullText.replace("\r", ""); //remove all carriage return characters from the context
        getIndicatorTextPane().setText(fullText);
        getIndicatorTextPane().setCaretPosition(caretPos);
        doc.setCharacterAttributes(0, getIndicatorTextPane().getText().length(), getIndicatorTextPane().getStyle("BLACK"), true);

        int charAt = 0;
        String[] lines = getIndicatorTextPane().getText().split("\n");
        for (int i = 0; i < lines.length; i++) {
            StringTokenizer st = new StringTokenizer(lines[i], " \t();,+-/><=*", true);
            while (st.hasMoreTokens()) {
                String word = st.nextToken();
                charAt = getIndicatorTextPane().getText().indexOf(word, charAt);
                if (arrayContains(keyWords, word)) { // Check for keywords
                    doc.setCharacterAttributes(charAt, word.length(), getIndicatorTextPane().getStyle("BLUE"), true);
                } else if (arrayContains(literals, word)) { //check for literals
                    doc.setCharacterAttributes(charAt, word.length(), getIndicatorTextPane().getStyle("PINK"), true);
                } else if (arrayContains(indicators, word)) { //check for indicators
                    doc.setCharacterAttributes(charAt, word.length(), getIndicatorTextPane().getStyle("VIOLET"), true);
                } else { //others
                    doc.setCharacterAttributes(charAt, word.length(), getIndicatorTextPane().getStyle("BLACK"), true);
                }
                charAt += word.length();
            }
        }
        markComments();
    }

    protected void markComments() {
        String[] lines = getIndicatorTextPane().getText().split("\n");
        boolean blockComment = false;
        int preCharCount = 0;
        for (String line : lines) {
            int startIndex = 0;
            if (blockComment) {
                int tmpIndex = line.indexOf("*/", startIndex);
                if (tmpIndex >= 0) {
                    int endIndex = tmpIndex + 1;
                    startIndex = endIndex;
                    // paint the half comment
                    doc.setCharacterAttributes(preCharCount, endIndex + 1, getIndicatorTextPane().getStyle("GREEN"), true);
                    blockComment = false;
                } else {
                    // paint the comment
                    doc.setCharacterAttributes(preCharCount, line.length(), getIndicatorTextPane().getStyle("GREEN"), true);
                    preCharCount += line.length() + 1;
                    continue;
                }
            }
            while (startIndex < line.length() - 1) {
                int blockCmntIndex = line.indexOf("/*", startIndex);
                int lineCmntIndex = line.indexOf("//", startIndex);
                if ((lineCmntIndex >= 0) || (blockCmntIndex >= 0)) {
                    int index;
                    if ((lineCmntIndex >= 0) && (blockCmntIndex >= 0)) {
                        index = Math.min(lineCmntIndex, blockCmntIndex);
                        blockComment = lineCmntIndex > blockCmntIndex;
                    } else if (lineCmntIndex >= 0) {
                        index = lineCmntIndex;
                    } else { // Block comment
                        index = blockCmntIndex;
                        blockComment = true;
                    }
                    int endIndex = line.length() - 1;
                    if (blockComment) {
                        int tmpIndex = line.indexOf("*/", startIndex);
                        if (tmpIndex > index) {
                            endIndex = tmpIndex + 1;
                            startIndex = endIndex;
                            blockComment = false;
                        } else {
                            startIndex = line.length();
                        }
                    } else {
                        startIndex = line.length();
                    }
                    // paint the comment
                    doc.setCharacterAttributes(preCharCount + index, endIndex - index + 1,
                            getIndicatorTextPane().getStyle("GREEN"), true);
                } else {
                    startIndex = line.length();
                }
            }
            preCharCount += line.length() + 1;
        }
    }

    protected boolean arrayContains(String[] syntaxKeyWords, String word) {
        boolean result = false;
        for (String syntaxKeyWord : syntaxKeyWords) {
            if (syntaxKeyWord.equals(word)) return true;
        }
        return result;
    }

    public void copyToClipboard() {
        String data = getIndicatorTextPane().getSelectedText();
        //StringSelection text = new StringSelection(UnicodeUtils.getUnicodeString(data));
        StringSelection text = new StringSelection(data);
        clip.setContents(text, this);
        data = null;

        copyText = getIndicatorTextPane().getSelectedText();
    }

    public void cutToClipboard() {

        try {
            String data = getIndicatorTextPane().getSelectedText();
            //StringSelection text = new StringSelection(UnicodeUtils.getUnicodeString(data));
            StringSelection text = new StringSelection(data);
            clip.setContents(text, this);
            data = null;

            copyText = getIndicatorTextPane().getSelectedText();

            String current = getIndicatorTextPane().getText();

            int startPos = getIndicatorTextPane().getSelectionStart();
            current = current.substring(0, startPos) + current.substring(startPos + copyText.length(), current.length());

            getIndicatorTextPane().setText(current);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void pasteText() {

        try {

            String dstData = "";
            Transferable content = clip.getContents(this);
            if (content != null && content.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                dstData = (String) content.getTransferData(DataFlavor.stringFlavor);
            }

            if (dstData != null && !dstData.equals("")) {

                getIndicatorTextPane().paste();
                /*int pos = getIndicatorTextPane().getCaretPosition();
                String current = getIndicatorTextPane().getText();
                current = current.substring(0, pos) + dstData + current.substring(pos, current.length());
                getIndicatorTextPane().setText(current);
                getIndicatorTextPane().setCaretPosition(pos + dstData.length());*/
                enableButtons();
            }
        } catch (Exception e) {
            e.printStackTrace();
            copyText = "";
        }
    }

    protected void deleteText() {

        String selectedText = getIndicatorTextPane().getSelectedText();
        String current = getIndicatorTextPane().getText();

        int startPos = getIndicatorTextPane().getSelectionStart();
        if (selectedText != null && startPos >= 0 && current.length() > 0) {
            current = current.substring(0, startPos) + current.substring(startPos + selectedText.length(), current.length());
            getIndicatorTextPane().setText(current);
            enableButtons();
        }
    }

    public void addRightClickPopupMenu() {

        itemCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                copyToClipboard();
                markSyntax();

            }
        });

        itemCut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cutToClipboard();
                markSyntax();

            }
        });

        itemPaste.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                pasteText();
                markSyntax();
            }
        });

        itemDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteText();
                markSyntax();
            }
        });

        popupMenu.add(itemCut);
        popupMenu.add(itemCopy);
        popupMenu.add(itemPaste);
        popupMenu.add(itemDelete);
        GUISettings.applyOrientation(popupMenu);

        getIndicatorTextPane().addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                if (!SwingUtilities.isLeftMouseButton(e)) {
                    popupMenu.show(getIndicatorTextPane(), e.getX(), e.getY());
                }
            }

            public void mousePressed(MouseEvent e) {

            }

            public void mouseReleased(MouseEvent e) {

            }

            public void mouseEntered(MouseEvent e) {

            }

            public void mouseExited(MouseEvent e) {

            }
        });


    }

    public void setIndicatorFormulaText(String syntax) {
        getIndicatorTextPane().setText(syntax);
    }

    protected void createMessagePanel() {
        messagesPane = new JTextPane();
        messagesPane.setEditable(false);
        errorsPane = new JTextPane();
        errorsPane.setEditable(false);
        warningsPane = new JTextPane();
        warningsPane.setEditable(false);
    }

    protected void toggleShowHidebutton() {
        divider = ((BasicSplitPaneUI) centerPanel.getUI()).getDivider();
        if (!show) {
            if (divider != null) {
                divider.setLayout(new BorderLayout(2, 2));
                divider.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                JPanel panel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 5, 2));
                panel.add(btnErrorPanelShow);
                divider.add(panel, BorderLayout.CENTER);
                GUISettings.applyOrientation(divider);
            }
        } else {
            if (divider != null) {
                divider.setLayout(new BorderLayout(2, 2));
                divider.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                JPanel panel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 5, 2));
                panel.add(btnErrorPanelClose);
                divider.add(panel, BorderLayout.CENTER);
                GUISettings.applyOrientation(divider);
            }
        }
    }

    public void setStyles() {
        Style style = getIndicatorTextPane().addStyle("PINK", null);

        StyleConstants.setForeground(style, new Color(255, 0, 255));
        StyleConstants.setBold(style, false);
        style = getIndicatorTextPane().addStyle("BLUE", null);
        StyleConstants.setBold(style, true);
        StyleConstants.setForeground(style, new Color(0, 0, 255));
        style = getIndicatorTextPane().addStyle("BLACK", null);
        StyleConstants.setBold(style, false);
        StyleConstants.setForeground(style, new Color(0, 0, 0));
        style = getIndicatorTextPane().addStyle("VIOLET", null);
        StyleConstants.setBold(style, true);
        StyleConstants.setForeground(style, new Color(128, 0, 128));
        style = getIndicatorTextPane().addStyle("GREEN", null);
        StyleConstants.setBold(style, false);
        StyleConstants.setForeground(style, new Color(0, 250, 0));
        markSyntax();
    }

    protected void checkName() {
        // check for non-english characters
        String indicatorName = txtIndicatorName.getText().trim();
        char ch;
        for (int i = 0; i < indicatorName.length(); i++) {
            ch = indicatorName.charAt(i);
            if (!(ch >= 0 && ch <= 255)) {
                JOptionPane.showMessageDialog(this, Language.getString("CI_INVALID_NAME_MESSAGE"));
                throw new RuntimeException("Invalid Indicator Name");
            }
        }


    }

    protected void deleteSource(String className) { //high priority
        String srcPath = (IndicatorFactory.classPath) + className + ".java";
        File file = new File(srcPath);
        if (file.exists()) file.delete();
    }

    public Vector<CustomIndicatorListener> getCiListeners() {
        return ciListeners;
    }

    protected boolean validateName() {
        String name = txtIndicatorName.getText().trim();
        Pattern pattern = Pattern.compile("^[a-zA-Z][a-zA-Z0-9 ]*$");
        Matcher matcher = pattern.matcher(name);
        if (!matcher.matches()) {
            String message = Language.getString("CI_NAME_VALIDATION_MESSAGE");
            message = message.replaceFirst("\\[NAME\\]", name);
            //JOptionPane.showMessageDialog(this, message);
            JOptionPane.showMessageDialog(this, message, Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (name.length() > 25) {
            //JOptionPane.showMessageDialog(this, Language.getString("CI_NAME_TOO_LONG"));
            JOptionPane.showMessageDialog(this, Language.getString("CI_NAME_TOO_LONG"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        /*String[] indNames = IndicatorFactory.getIndicatorNames();
        if (indNames != null) {
            for (String indName : indNames) {
                if (name.equals(indName) ||
                        name.replace(" ", "_").equals(indName.replace(" ", "_"))) {
                    String message = Language.getString("CI_NAME_ALREADY_EXISTS_MESSAGE");
                    message = message.replaceFirst("\\[NAME\\]", indName);
                    //JOptionPane.showMessageDialog(this, message);
                    JOptionPane.showMessageDialog(this, message, Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
        }*/
        return true;
    }

    protected int testCode(IndicatorConfigInfo info) {
        initTest();
        loadParser();
        String code = parseAndGenerateCode(info);
        if (!testFailed && code != null) {
            return testCompile(code, info.ClassName);
        } else {
            return -1;
        }
    }

    protected void initTest() {
        messagesPane.setText("");
        errorsPane.setText("");
        warningsPane.setText("");
        testFailed = false;
    }

    public void clearMessages() {
        messagesPane.setText("");
        errorsPane.setText("");
        warningsPane.setText("");
    }

    protected void loadParser() {
        if (parser == null) {
            parser = new IndicatorParser(Settings.CHART_DATA_PATH + "/CustomIndicator.cgt", errorsPane);
        }
    }

    protected String parseAndGenerateCode(IndicatorConfigInfo info) {
        CIProgram program = parser.parse(info.Grammer);
        String code = null;
        if (program != null) {
            if (!testFailed) {
                try {
                    code = IndicatorFactory.GenerateCode(info, program);
                    info.IsStrategy = program.isStrategy;
                } catch (Exception ex) {
                    // TODO: handle and show errorsPane
                    /*
                    if (txtErrors.Lines.Length > 0) {
                        txtErrors.Text += "\n";
                    }
                    txtErrors.Text += "Compilation error: "+ex.Message;
                    errCount++;
                    */
                    testFailed = true;
                    ex.printStackTrace();
                    tabbedPane.setSelectedIndex(1);
                    errorsPane.setText(Language.getString("CI_ERROR_MSG_GRAMMAR_RSLT_FAILD") + ex.getMessage());
                }
            }
        } else {
            testFailed = true;
            tabbedPane.setSelectedIndex(0);
            messagesPane.setText(Language.getString("CI_ERROR_MSG_GRAMMAR_RSLT_FAILD"));
        }
        if (!testFailed) {
            tabbedPane.setSelectedIndex(0);
            messagesPane.setText(Language.getString("CI_MSG_GRAMMAR_RSLT_SUCCESS"));
        }

        tabbedPanel.setVisible(true);

        centerPanel.setDividerLocation(0.60);

        show = true;
        toggleShowHidebutton();
        centerPanel.updateUI();
        return code;
    }

    public int testCompile(String source, String className) {
        int result = IndicatorFactory.compileCode(source, className);
        System.out.println("########### result : " + result);
        if (result != 0) {
            tabbedPane.setSelectedIndex(0);
            messagesPane.setText(Language.getString("CI_ERROR_MSG_TEST_FAILED"));
        }
        deleteSource(className);
        return result;
    }

    protected boolean loadText() {
        JFileChooser fc = new JFileChooser(".\\Charts");
        GUISettings.applyOrientation(fc);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setDialogTitle(Language.getString("CI_FILE_OPEN_DIALOG_TITLE"));
        String[] extensions = {"txt"};
        TWFileFilter filter = new TWFileFilter(Language.getString("CI_FILE_FILTER"), extensions);
        fc.setFileFilter(filter);

        //int status = fc.showOpenDialog(Client.getInstance().getDesktop());
        int status = fc.showOpenDialog(ChartInterface.getChartFrameParentComponent());
        if (status == JFileChooser.APPROVE_OPTION) {
            try {
                FileReader fr = new FileReader(fc.getSelectedFile());
                getIndicatorTextPane().setText("");
                char[] buffer = new char[1024];
                int n;
                StringBuffer bf = new StringBuffer();
                while ((n = fr.read(buffer)) > -1) {
                    bf.append(new String(buffer, 0, n));
                }
                getIndicatorTextPane().setText(bf.toString());
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    protected void saveText() {
        JFileChooser fc = new JFileChooser(".\\Charts");
        GUISettings.applyOrientation(fc);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setDialogTitle(Language.getString("CI_FILE_SAVE_DIALOG_TITLE"));
        String[] extensions = {"txt"};
        TWFileFilter filter = new TWFileFilter(Language.getString("CI_FILE_FILTER"), extensions);
        fc.setFileFilter(filter);

        //int status = fc.showSaveDialog(Client.getInstance().getDesktop());
        int status = fc.showSaveDialog(ChartInterface.getChartFrameParentComponent());
        if (status == JFileChooser.APPROVE_OPTION) {
            try {
                String filePath = fc.getSelectedFile().getAbsolutePath();
                if (!filePath.endsWith(".txt")) {
                    filePath = filePath + ".txt";
                }
                File newFile = new File(filePath);
                if (newFile.exists()) {
                    int respond = SharedMethods.showConfirmMessage(Language.getString("REPLACE_FILE_MSG"), JOptionPane.YES_NO_OPTION);
                    if (!(respond == JOptionPane.YES_OPTION)) {
                        return;
                    }
                }
                FileWriter fw = new FileWriter(filePath);
                fw.write(getIndicatorTextPane().getText().trim());
                fw.close();
                dirty = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);

        this.updateUI();
        if (divider != null) {
            divider = ((BasicSplitPaneUI) centerPanel.getUI()).getDivider();
            toggleShowHidebutton();
        }
        if (tabbedPane != null) {
            tabbedPane.updateUI();
        }

        btnSelectAction.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/select_action.gif"));
        btnLoadText.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/load_text.gif"));
        btnSaveText.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/save_text.gif"));
        btnFunctions.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/functions.gif"));
        btnHelp.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/indicator_help.gif"));
        btnTest.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/build.gif"));
        btnBuild.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/test.gif"));

        btnNew.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/new.gif"));
        btnEdit.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/edit.gif"));
        btnDelete.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/delete.gif"));

        lblDelete.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));
        lblEdit.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_down_pop.gif"));

        btnErrorPanelClose.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/show.gif"));
        btnErrorPanelShow.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/hide.gif"));

        SwingUtilities.updateComponentTreeUI(popupMenu);

    }

    protected IndicatorConfigInfo showInitialPropertyDialog(IndicatorConfigInfo info) {
        InitialPropertyDialog dlg = new InitialPropertyDialog(info);
        dlg.setModal(true);
        dlg.setLocationRelativeTo(this);
        dlg.setVisible(true);

        if (dlg.result == InitialPropertyDialog.RESULT_OK) {
            IndicatorConfigInfo newInfo = dlg.getConfigInfo();
            dlg.dispose();
            return newInfo;
        } else {
            dlg.dispose();
            return null;
        }
    }

    public void showWindow() {
        enableButtons();
        Client.getInstance().getDesktop().add(this);
        parent.setLocationRelativeTo(Client.getInstance().getDesktop());
        indicatorTextPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
//        Client.getInstance().getDesktop().setLayer(this, GUISettings.INTERNAL_DIALOG_LAYER);
        Client.getInstance().getDesktop().setLayer(this, 1000);
        setVisible(true);
    }

    public void closeWindow() {
        indicatorTextPane.setText("");
        dirty = false;
        tabbedPanel.setVisible(false);
        if (actionFrame != null) actionFrame.dispose();
        parent.dispose();
    }

    //getters and setters

    public StyledDocument getDoc() {
        return doc;
    }

    public void setDoc(StyledDocument doc) {
        this.doc = doc;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public TWTabbedPane getTabbedPane() {
        return tabbedPane;
    }

    public FunctionDialog getFunctionDialog() {
        return functionDialog;
    }

    public JTextPane getIndicatorTextPane() {
        return indicatorTextPane;
    }

    public void setIndicatorTextPane(JTextPane indicatorTextPane) {
        this.indicatorTextPane = indicatorTextPane;
    }

    public ActionFrame getActionFrame() {
        return actionFrame;
    }

    public void setIndicatorName(String name) {
        txtIndicatorName.setText(name);
    }

    public boolean isEditing() {
        return isEditing;
    }

    public void setEditing(boolean b) {
        isEditing = b;
    }

    public void setEditingIndictor(String indi) {
        editingIndicatorName = indi;
    }

    public String getEditingIndicatorName() {
        return editingIndicatorName;
    }

}
