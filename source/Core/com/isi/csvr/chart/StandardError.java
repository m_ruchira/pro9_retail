package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: May 9, 2006
 * Time: 3:27:48 PM
 */
public class StandardError extends ChartProperties implements Indicator {

    private static final long serialVersionUID = UID_STD_ERR;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public StandardError(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_STD_ERR") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_STD_ERR");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof StandardError) {
            StandardError se = (StandardError) cp;
            this.timePeriods = se.timePeriods;
            this.innerSource = se.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_STD_ERR") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long currentBeginTime;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods;
        if ((loopBegin >= al.size()) || (loopBegin <= 2)) return;
        double zigmaX = 0, zigmaY = 0, zigmaXX = 0, zigmaXY = 0, zigmaYY = 0, yi, yPi, Sxx, Sxy;
        double xi, xPi, b1;
        ChartRecord cr, crOld;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            xi = GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getValue(getOHLCPriority());
            zigmaX += xi;
            zigmaY += yi;
            zigmaXX += xi * xi;
            zigmaXY += xi * yi;
            zigmaYY += yi * yi;
            if (i >= loopBegin - 1) {
                Sxx = zigmaXX - zigmaX * zigmaX / timePeriods;
                Sxy = zigmaXY - zigmaX * zigmaY / timePeriods;
                b1 = (float) (Sxy / Sxx);
                double tmpExprn = zigmaYY - zigmaY * zigmaY / timePeriods - b1 * Sxy;
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null)
                    if (tmpExprn < 0) {
                        aPoint.setIndicatorValue(0);
                    } else {
                        aPoint.setIndicatorValue(Math.sqrt(tmpExprn / (timePeriods - 2)));
                    }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }

        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - loopBegin);
            cr = (ChartRecord) al.get(i);
            xi = GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getValue(getOHLCPriority());
            xPi = GDM.getClosestIndexFortheTime(crOld.Time);
            yPi = crOld.getValue(getOHLCPriority());

            zigmaX = zigmaX + xi - xPi;
            zigmaY = zigmaY + yi - yPi;
            zigmaXX = zigmaXX + xi * xi - xPi * xPi;
            zigmaXY = zigmaXY + xi * yi - xPi * yPi;
            zigmaYY = zigmaYY + yi * yi - yPi * yPi;

            Sxx = zigmaXX - zigmaX * zigmaX / timePeriods;
            Sxy = zigmaXY - zigmaX * zigmaY / timePeriods;
            b1 = (float) (Sxy / Sxx);
            double tmpExprn = zigmaYY - zigmaY * zigmaY / timePeriods - b1 * Sxy;
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                if (tmpExprn < 0) {
                    aPoint.setIndicatorValue(0);
                } else {
                    aPoint.setIndicatorValue(Math.sqrt(tmpExprn / (timePeriods - 2)));
                }
        }
        //System.out.println("**** Standard Error Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

    public String getShortName() {
        return Language.getString("IND_STD_ERR");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
