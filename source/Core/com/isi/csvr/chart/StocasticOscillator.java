package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Mar 25, 2005 - Time: 2:33:05 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorStochasticOscillatorProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;


public class StocasticOscillator extends ChartProperties implements Indicator, Serializable {
    public final static byte METHOD_FAST = 0;
    public final static byte METHOD_SLOW = 1;
    private static final long serialVersionUID = 1072600473330203819L;
    //Fields
    protected int timePeriods;
    protected int slowingPeriods;
    protected byte method;
    private int signalTimePeriods;
    private byte signalStyle;
    private Color signalColor;
    private ChartProperties innerSource;
    private MovingAverage signalLine;
    private String tableColumnHeading;

    public StocasticOscillator(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("STOCASTIC_OSCILLATOR") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_STOCHAS_OSCIL");
        timePeriods = 5;
        slowingPeriods = 3;
        method = METHOD_FAST;
        signalTimePeriods = 3;
        signalStyle = GraphDataManager.STYLE_DASH;
        signalColor = Color.DARK_GRAY;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorStochasticOscillatorProperty idp = (IndicatorStochasticOscillatorProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());

            this.setTimePeriods(idp.getTimePeriods());
            this.setSlowingPeriods(idp.getSlowingPeriod());
            this.setSignalColor(idp.getLineColor());
            this.setSignalTimePeriods(idp.getSignalTimePeriods());
            this.setSignalStyle(idp.getSignalStyle());
        }
    }

    //############################################
    public static String[] getMethodArray() {
        String[] methodArr = {Language.getString("SPEED_FAST"), Language.getString("SPEED_SLOW")};
        return methodArr;
    }

    public static double getFastStochastic(LinkedList<ChartRecord> ll, int timePeriod) {
        double highestH = -10000000d;
        double lowestL = Double.MAX_VALUE;
        for (int i = ll.size() - timePeriod; i < ll.size(); i++) {
            ChartRecord cr = ll.get(i);
            highestH = Math.max(highestH, cr.High);
            lowestL = Math.min(lowestL, cr.Low);
        }
        ChartRecord currentCr = ll.get(ll.size() - 1);

        return ((highestH - lowestL) == 0) ? 100.0 : 100d * (currentCr.Close - lowestL) / (highestH - lowestL);
    }

    //TODO
    public static double getSlowStochastic(LinkedList<ChartRecord> ll, int timePeriod, int slowingPeriod) {
        byte stepHighestHigh = ChartRecord.STEP_1;
        byte stepLowestLow = ChartRecord.STEP_2;
        ArrayList<ChartRecord> al = new ArrayList<ChartRecord>();

        for (int i = ll.size() - slowingPeriod; i < ll.size(); i++) {
            double highestH = -10000000d;
            double lowestL = Double.MAX_VALUE;
            ChartRecord cr;
            for (int j = 0; j < timePeriod; j++) {
                cr = ll.get(i - j);
                highestH = Math.max(highestH, cr.High);
                lowestL = Math.min(lowestL, cr.Low);
            }
            cr = ll.get(i);
            ChartRecord crNew = new ChartRecord();
            crNew.Open = cr.Open;
            crNew.Close = cr.Close;
            crNew.High = cr.High;
            crNew.Low = cr.Low;
            crNew.Volume = cr.Volume;
            crNew.Time = cr.Time;
            crNew.setStepSize(2);
            crNew.setStepValue(stepHighestHigh, highestH);
            crNew.setStepValue(stepLowestLow, lowestL);
            al.add(crNew);
        }

        double zigmaCminusL = 0d, zigmaHminusL = 0d;
        for (int j = 0; j < al.size(); j++) {
            ChartRecord cr = al.get(j);
            zigmaCminusL += (cr.Close - cr.getStepValue(stepLowestLow));
            zigmaHminusL += (cr.getStepValue(stepHighestHigh) - cr.getStepValue(stepLowestLow));
        }
        double val = 100d * zigmaCminusL / zigmaHminusL;
        if (Double.isNaN(val) || Double.isInfinite(val)) val = 50d;

        return val;
    }

    // two intermediate steps
    // stepHighestHigh, stepLowestLow
    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, int slowingPeriods, byte stepHighestHigh, byte stepLowestLow, byte destIndex) {
        getStochasticOscillator(al, bIndex, t_im_ePeriods, slowingPeriods, GDM,
                stepHighestHigh, stepLowestLow, destIndex);
    }

    public static void getStochasticOscillator(ArrayList al, int bIndex, int timePeriods, int slowingPeriods,
                                               GraphDataManagerIF GDM, byte stepHighestHigh, byte stepLowestLow, byte destIndex) {

        ChartRecord cr;
        long entryTime = System.currentTimeMillis();

        /////////// no need to avoid null points as there is no srcIndex ////////////

        int loopBegin = bIndex + timePeriods - 1 + slowingPeriods - 1;
        if ((loopBegin >= al.size()) || (timePeriods < 1)) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        for (int i = bIndex + timePeriods - 1; i < al.size(); i++) {
            double highestH = -10000000d;
            double lowestL = Double.MAX_VALUE;
            for (int j = 0; j < timePeriods; j++) {
                cr = (ChartRecord) al.get(i - j);
                highestH = Math.max(highestH, cr.High);
                lowestL = Math.min(lowestL, cr.Low);
            }
            cr = (ChartRecord) al.get(i);
            cr.setValue(stepHighestHigh, highestH);
            cr.setValue(stepLowestLow, lowestL);
        }

        double currVal;
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        if (slowingPeriods == 1) {  // fast stochastic
            for (int i = loopBegin; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                currVal = 100d * (cr.Close - cr.getValue(stepLowestLow)) / (cr.getValue(stepHighestHigh) - cr.getValue(stepLowestLow));
                if (Double.isNaN(currVal)) currVal = 50d;
                cr.setValue(destIndex, currVal);
            }
        } else {  // slow stochastic
            for (int i = loopBegin; i < al.size(); i++) {
                double zigmaCminusL = 0d, zigmaHminusL = 0d;
                for (int j = 0; j < slowingPeriods; j++) {
                    cr = (ChartRecord) al.get(i - j);
                    zigmaCminusL += (cr.Close - cr.getValue(stepLowestLow));
                    zigmaHminusL += (cr.getValue(stepHighestHigh) - cr.getValue(stepLowestLow));
                }
                cr = (ChartRecord) al.get(i);
                currVal = 100d * zigmaCminusL / zigmaHminusL;
                if (Double.isNaN(currVal)) currVal = 50d;
                cr.setValue(destIndex, currVal);
            }
        }
        //System.out.println("**** Inter Stochastic Oscil Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public static int getAuxStepCount() {
        return 2;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        timePeriods = 5;
        slowingPeriods = 3;
        signalTimePeriods = 3;
        signalStyle = GraphDataManager.STYLE_DASH;
        signalColor = Color.DARK_GRAY;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.slowingPeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SlowingPeriods"));
        this.signalTimePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalTimePeriods"));
        this.signalStyle = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalStyle"));
        this.method = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/Method"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalColor").split(",");
        this.signalColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "SlowingPeriods", Integer.toString(this.slowingPeriods));
        TemplateFactory.saveProperty(chart, document, "SignalTimePeriods", Integer.toString(this.signalTimePeriods));
        TemplateFactory.saveProperty(chart, document, "SignalStyle", Byte.toString(this.signalStyle));
        TemplateFactory.saveProperty(chart, document, "Method", Byte.toString(this.method));
        String strColor = this.signalColor.getRed() + "," + this.signalColor.getGreen() + "," + this.signalColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "SignalColor", strColor);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof StocasticOscillator) {
            StocasticOscillator stoc = (StocasticOscillator) cp;
            this.signalTimePeriods = stoc.signalTimePeriods;
            this.slowingPeriods = stoc.slowingPeriods;
            this.timePeriods = stoc.timePeriods;
            this.method = stoc.method;
            this.signalStyle = stoc.signalStyle;
            this.signalColor = stoc.signalColor;
            this.innerSource = stoc.innerSource;
            if ((this.signalLine != null) && (stoc.signalLine != null)) {
                this.signalLine.assignValuesFrom(stoc.signalLine);
            }
        }
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // this includes 2 intermediate steps HighestHigh, LowestLow
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();
        int loopBegin = timePeriods + slowingPeriods - 2;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;

        double currVal;
        ChartRecord cr;
        //setting step size 2
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(2);
        }
        // steps involved
        byte stepHighestHigh = ChartRecord.STEP_1;
        byte stepLowestLow = ChartRecord.STEP_2;

        for (int i = timePeriods - 1; i < al.size(); i++) {
            double highestH = -Double.MAX_VALUE;
            double lowestL = Double.MAX_VALUE;
            for (int j = 0; j < timePeriods; j++) {
                cr = (ChartRecord) al.get(i - j);
                highestH = Math.max(highestH, cr.High);
                lowestL = Math.min(lowestL, cr.Low);
            }
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(stepHighestHigh, highestH);
            cr.setStepValue(stepLowestLow, lowestL);
        }

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        if (slowingPeriods == 1) {  // fast stochastic
            for (int i = loopBegin; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                currVal = 100d * (cr.Close - cr.getStepValue(stepLowestLow)) / (cr.getStepValue(stepHighestHigh) - cr.getStepValue(stepLowestLow));
                if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 50d;
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(currVal);
                }
            }
        } else {  // slow stochastic
            for (int i = loopBegin; i < al.size(); i++) {
                double zigmaCminusL = 0d, zigmaHminusL = 0d;
                for (int j = 0; j < slowingPeriods; j++) {
                    cr = (ChartRecord) al.get(i - j);
                    zigmaCminusL += (cr.Close - cr.getStepValue(stepLowestLow));
                    zigmaHminusL += (cr.getStepValue(stepHighestHigh) - cr.getStepValue(stepLowestLow));
                }
                cr = (ChartRecord) al.get(i);
                currVal = 100d * zigmaCminusL / zigmaHminusL;
                if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 50d;
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(currVal);
                }
            }
        }

        //System.out.println("**** Stocastic Oscillator Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("STOCASTIC_OSCILLATOR") + " " + parent;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    //slowingPeriods
    public int getSlowingPeriods() {
        return slowingPeriods;
    }

    public void setSlowingPeriods(int slowingPeriods) {
        this.slowingPeriods = slowingPeriods;
    }

    //signalTimePeriods
    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public void setSignalTimePeriods(int tp) {
        signalTimePeriods = tp;
        if (signalLine != null)
            signalLine.setTimePeriods(tp);
    }

    //signalStyle;
    public byte getSignalStyle() {
        return signalStyle;
    }

    public void setSignalStyle(Byte ss) {
        setSignalStyle(ss.byteValue());
    }

    public void setSignalStyle(byte ss) {
        signalStyle = ss;
        if (signalLine != null)
            signalLine.setPenStyle(ss);
    }

    //signalColor;
    public Color getSignalColor() {
        return signalColor;
    }

    public void setSignalColor(Color c) {
        signalColor = c;
        if (signalLine != null)
            signalLine.setColor(c);
    }

    //signalLine
    public MovingAverage getSignalLine() {
        return signalLine;
    }

    public void setSignalLine(MovingAverage ma) {
        signalLine = ma;
    }

    //method
    public byte getMethod() {
        return method;
    }

    public void setMethod(byte tp) {
        method = tp;
    }

    public String getShortName() {
        return Language.getString("STOCASTIC_OSCILLATOR");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}