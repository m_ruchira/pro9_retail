package com.isi.csvr.chart;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Udaka
 * Date: Aug 14, 2005
 * Time: 12:39:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class SplitPointRecord implements Serializable {
    public long time = 0;
    public float factor = 1f;
    public int group = 0;
    public int type = 0;
    public int sequence = 0;

    public SplitPointRecord(long time, float factor, int group, int type, int sequence) {
        this.time = time;
        this.factor = factor;
        this.group = group;
        this.type = type;
        this.sequence = sequence;
    }
}
