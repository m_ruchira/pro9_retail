package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorDefaultProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 28, 2006
 * Time: 4:37:09 PM
 */
public class MarketFacilitationIndex extends ChartProperties implements Indicator {
    private static final long serialVersionUID = UID_MFI;
    //Fields
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public MarketFacilitationIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_MFI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_MFI");
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorDefaultProperty idp = (IndicatorDefaultProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setOHLCPriority(idp.getOHLCPriority());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_i_mePeriods, byte stepMA,
                                          byte srcIndex, byte destIndex) {

        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crNew;

        MovingAverage.getMovingAverage(al, 0, t_i_mePeriods, MovingAverage.METHOD_SIMPLE, srcIndex, stepMA);

        int step = t_i_mePeriods / 2 + 1;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if ((i + step >= t_i_mePeriods - 1) && (i + step < al.size())) {
                crNew = (ChartRecord) al.get(i + step);
                /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(cr.getValue(srcIndex) - crNew.getStepValue(stepMA));
                }*/
                cr.setValue(destIndex, cr.getValue(srcIndex) - crNew.getStepValue(stepMA));
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }

        //System.out.println("**** DPO Calc time " + (entryTime - System.currentTimeMillis()));

    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 1;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof MarketFacilitationIndex) {
            MarketFacilitationIndex mfi = (MarketFacilitationIndex) cp;
            this.innerSource = mfi.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        return Language.getString("IND_MFI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        ChartRecord cr;

        long entryTime = System.currentTimeMillis();

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(cr.Volume == 0 ? 0 : (cr.High - cr.Low) / cr.Volume);
            }
        }

        //System.out.println("**** MFI Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_MFI");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
