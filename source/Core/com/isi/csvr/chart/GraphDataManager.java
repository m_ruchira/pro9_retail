package com.isi.csvr.chart;
/**
 * <p>Title: TW International</p>
 * <p>Description: Data structuer for StockGraph </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

import com.isi.csvr.ChartInterface;
import com.isi.csvr.Client;
import com.isi.csvr.cashFlowWatch.CashFlowHistorySummaryUI;
import com.isi.csvr.chart.cashflowcharts.GraphCalcTypeApplicable;
import com.isi.csvr.chart.cashflowcharts.NetOrdersInd;
import com.isi.csvr.chart.cashflowcharts.NetTurnOverInd;
import com.isi.csvr.chart.chartobjects.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.options.ChartOptions;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.shared.OrderStatusListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.lang.reflect.Constructor;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class GraphDataManager implements Runnable, TradingConnectionListener, OrderStatusListener, GraphDataManagerIF {

    public static final byte INNER_RECORD_COUNT = 5;
    public static final byte INDICATOR_RECORD_COUNT = 1;
    public final static byte INDICATOR = 0;
    public final static byte INNER_Open = 0;
    public final static byte INNER_High = 1;
    public final static byte INNER_Low = 2;
    public final static byte INNER_Close = 3;
    private byte snapToPricePriority = INNER_Close;
    public final static byte INNER_Volume = 4;
    public final static byte INNER_CHANGE = 5;
    public final static byte INNER_PERCENT_CHANGE = 6;
    public final static byte INNER_TurnOver = 7;
    //general IDs
    public final static byte ID_MULTI_LOW = -118;
    public final static byte ID_MULTI_HIGH = -9;
    public final static byte ID_BASE = -120;
    public final static byte ID_COMPARE = -119;
    public final static byte ID_CUSTOM_INDICATOR = 0;
    public final static byte ID_VOLUME = 10;
    //Single ohlc field Indicators - use either o,h,l or c
    public final static byte ID_MOVING_AVERAGE = 11;
    public final static byte ID_RSI = 12;
    public final static byte ID_MACD = 13;
    public final static byte ID_MOMENTUM = 14;
    public final static byte ID_LRI = 15;
    public final static byte ID_SDI = 16;
    public final static byte ID_BOLLINGER_BANDS = 17;
    public final static byte ID_TRIX = 18;
    public final static byte ID_ZIGZAG = 19;
    public final static byte ID_ENVILOPES = 20;
    public final static byte ID_PRICE_OSCILL = 21;
    public final static byte ID_RMI = 22;
    public final static byte ID_ROC = 23;
    public final static byte ID_WILDERS_SMOOTHING = 24;
    public final static byte ID_DPO = 25;
    public static final byte ID_PERFORMANCE = 26;
    public static final byte ID_VERT_HORI_FILTER = 27;
    public static final byte ID_DEMA = 28;
    public static final byte ID_TSF = 29;
    public static final byte ID_FO = 30;
    public static final byte ID_CMO = 31;
    public static final byte ID_TEMA = 32;
    public static final byte ID_STD_ERR = 33;
    public static final byte ID_MACD_HISTOGRAM = 34;
    public static final byte ID_MACD_VOLUME = 35;
    public static final byte ID_TURNOVER = 36;
    //Multi ohlc field Indicatoes - use an expresion containing o, h l and c (more than one of them)
    public final static byte ID_ACCU_DISTRI = -11;
    public final static byte ID_WILLS_R = -12;
    public final static byte ID_STOCHAST_OSCILL = -13;
    public final static byte ID_AVG_TRUE_RANGE = -14;
    public final static byte ID_MASS_INDEX = -15;
    public final static byte ID_MEDIAN_PRICE = -16;
    public final static byte ID_MONEY_FLOW_INDEX = -17;
    public final static byte ID_VOLATILITY = -18;
    public final static byte ID_AROON = -19;
    public final static byte ID_CHAIKINS_OSCILL = -20;
    public final static byte ID_CCI = -21;
    public final static byte ID_CHAIKIN_MONEY_FLOW = -22;
    public final static byte ID_EASE_OF_MOVEMENT = -23;
    public final static byte ID_ICHIMOKU_KINKO_HYO = -24;
    public final static byte ID_KLINGER_OSCILL = -25;
    public final static byte ID_PARABOLIC_SAR = -26;
    public final static byte ID_PRICE_VOLUME_TREND = -27;
    public final static byte ID_VOLUME_OSCILL = -28;
    public final static byte ID_WILLS_ACCU_DISTRI = -29;
    public final static byte ID_INTRADAY_MOMENTUM_INDEX = -30;
    public final static byte ID_ON_BALANCE_VOLUME = -31;
    public final static byte ID_PLUS_DI = -32;
    public final static byte ID_MINUS_DI = -33;
    public final static byte ID_DX = -34;
    public final static byte ID_ADX = -35;
    public final static byte ID_ADXR = -36;
    public final static byte ID_SWING_INDEX = -37;
    public final static byte ID_RVI = -38;
    public final static byte ID_MFI = -39;
    public static final byte ID_PVI = -40;
    public static final byte ID_QSTICK = -41;
    public static final byte ID_TYPICAL_PRICE = -42;
    public static final byte ID_WEIGHTED_CLOSE = -43;
    public static final byte ID_VOLUME_ROC = -44;
    public static final byte ID_PRICE_CHANNELS = -45;
    public static final byte ID_NVI = -46;
    public static final byte ID_SMO = -47;
    public final static byte ID_NET_ORDERS = -48; //todo check this place corret ,id value either + or -
    public final static byte ID_NET_TURNOVER = -49; //todo check this place corret ,id value either + or -
    public static final byte ID_ALLIGATOR = -50;
    public static final byte ID_COPP_CURVE = -51;
    public static final byte ID_DISPARITY_INDEX = -52;
    public static final byte ID_HULL_MOVING_AVG = -53;
    public static final byte ID_KELTNER_CHANNELS = -54;
    public final static byte STYLE_SOLID = 0;
    public final static byte STYLE_DOT = 1;
    public final static byte STYLE_DASH = 2;
    public final static byte STYLE_DASH_DOT = 3;
    public final static byte STYLE_DASH_DOT_DOT = 4;
    public final static byte GRID_TYPE_NONE = 0;
    public final static byte GRID_TYPE_NORMAL = 1;
    //boolean inThousands = false;
    private byte verticalGridType = GRID_TYPE_NORMAL;
    private byte horizontalGridType = GRID_TYPE_NORMAL;
    public final static byte GRID_TYPE_SUBDIVIDED = 2;
    public final static byte GRID_TYPE_SPACED = 3;
    public final static byte GRID_TYPE_HORIZONTAL = 1;
    public final static byte GRID_TYPE_BOTH = 2;
    private byte gridType = GRID_TYPE_BOTH;
    public final static byte GRID_TYPE_VERTICAL = 3;
    //for Announcements and split types
    public final static int ALERT_NONE = 0;
    public final static int ALERT_ANNOUNCEMENT = 1;
    public final static int ALERT_STOCK_SPLIT = 2;
    public final static int ALERT_STOCK_DIVIDEND = 4;
    public final static int ALERT_RIGHTS_ISSUE = 8;
    public final static int ALERT_REVERSE_SPLIT = 16;
    public final static int ALERT_CAPITAL_REDUCTION = 32;
    public final static int ALERT_CASH_DIVIDEND = 64;
    /**
     * cross hair tpes constants *******************
     */
    public final static byte NORMAL_CROSS_HAIR = 0;
    private byte crossHairType = NORMAL_CROSS_HAIR;
    public final static byte SNAP_BALL_CROSS_HAIR = 1;
    public final static byte HIGH_LOW_CROSS_HAIR = 2;
    public final static byte SNAP_BALL_ALONE = 2;
    /**
     * cross hair tpes *******************
     */

    public final static byte VOL_BY_PRICE_NET_CHANGE = 0;
    private byte volumeByPriceType = VOL_BY_PRICE_NET_CHANGE;
    public final static byte VOL_BY_PRICE_PCT_CHANGE = 1;
    public final static byte VOL_BY_PRICE_NORMAL = 2;
    //String[] saSymbol = { "�", "�", "C", "D", "J", "L", "O", "M", "l", "u" };
    public static final char[] saSymbol = {
            //0x00E9, 0x00EA, 0x0043, 0x0044, 0x004A,
            0x21D1, 0x21D3, 0x25B2, 0x25BC, 0x263A,
            0x263B, 0x2302, 0x25D9, 0x25CF, 0x25C6
    };
    static final Object graphLock = new GraphLock();
    //private static final String repaintLock = "RepaintLock";
    final static int Miliseconds_Per_Day = 3600 * 24 * 1000;
    static final float XaxisPitch = 0.8f;
    static final float YaxisPitch = 0.4f;
    static final byte PIXELS_DESIRED = 40;
    private static final int EDIT_PROPERTY = 0;
    private static final int INSERT_PROPERTY = 1;
    private static final int NO_OF_RESITANCE_LINES = 7;
    private ObjectHorizontalPriceLine resistanceLines[] = new ObjectHorizontalPriceLine[NO_OF_RESITANCE_LINES];
    private static final int PriceVolPitchLength = 5;
    private static final double HORIZ_LINE_INITIAL_VALUE = -9999.0;
    //adjustable constants
    public static int borderWidth = StockGraph.borderWidth;
    public static int clearance = StockGraph.clearance;
    public int barExt = Math.max(clearance / 2, 3);
    public static int heightXaxis = StockGraph.heightXaxis;
    public static int titleHeight = StockGraph.titleHeight;
    public static int sliderHeight = StockGraph.sliderHeight;
    public static int legendHeight = StockGraph.legendHeight;
    //added by charithn for incase of future requirements.
    protected static byte METHOD_ADJUSTED = 2;
    protected static byte METHOD_UNADJUSTED = 1;
    //private byte adjustMethod = METHOD_NONE;
    private byte adjustMethod = METHOD_UNADJUSTED;
    protected static byte METHOD_NONE = 0;
    static DecimalFormat numformat = new DecimalFormat("###,###,##0.00");
    static DecimalFormat thousandformat = new DecimalFormat("###,###,###,##0");
    static DecimalFormat miliformat = new DecimalFormat("0.000000");
    static DecimalFormat microformat = new DecimalFormat("0.000000000");
    static byte baseGraphStyle = StockGraph.INT_GRAPH_STYLE_LINE;  // This stores the last used style..
    private static String NA = Language.getString("NA");
    private static DecimalFormat decfmt = new DecimalFormat("###,###,##0.000000000");
    public final float HALF_INCH = Toolkit.getDefaultToolkit().getScreenResolution() / 2;
    final float[] dashArr = {3.0f, 3.0f};
    final BasicStroke BS_1p0f_2by2_dashed = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 1.0f, dashArr, 1.0f);
    private final long PRICE_UPDATE_MINIMUM_WAIT = 20000;
    private final float[] PriceVolPitch = {1f, 2f, 2.5f, 4f, 5f};
    private final int[] HistoryTimePitch = {1, 2, 3, 4, 5, 7, 30, 60, 90, 120,
            365, 365 * 2, 365 * 4, 365 * 5, 365 * 10, 365 * 20};
    private final int[] CurrentTimePitch = {1, 2, 3, 4, 5, 6, 10, 12, 15, 20, 30, 60, //13 nos
            90, 120, 180, 240, 300, 360, 480, 600, 1440, 2880, 4320, 5760, 7200}; //13 nos
    public float beginValue = Integer.MAX_VALUE;
    public Object activeObject = null;
    public ArrayList<Color> graphColors = new ArrayList<Color>();
    public int periodBeginIndex = 0;
    public int sourceHiLighted = -1;
    public int objectHiLighted = -1;
    public float xFactor;
    public int currentCustomPeriod = ChartSettings.getSettings().getCustomPeriod();
    public int currentCustomRange = ChartSettings.getSettings().getCustomRange();
    public long currentCustomIntervalType = ChartSettings.getSettings().getCustomIntervalType();
    public long currentCustomIntervalFactor = ChartSettings.getSettings().getCustomIntervalFactor();
    public IntraDayOHLC dynamicLastCandle = new IntraDayOHLC("", 0, 0, 0, 0, 0, 0, 0, 0);
    public long dynamicCandleLastUpdateTime = 0;
    public boolean dynamicCandleIsFirstMinute = true;
    public boolean isAfterDynamicCandleFirstMinute = true;
    public long dynamicCandleStockVolume = 0;
    public double dynamicCandleStockTurnover = 0;
    public long previousStockLastTradeTime = 0;
    protected long tmpLatestTime = 0;
    protected Point[] fibCalBounds = null;
    Image imgIconWindow = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_window.gif");
    //for announcements and splits
    Image imgAnnouncement = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_Announcement.gif");
    Image imgStockSplit = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_StockSplit.gif");
    Image imgStockDividend = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_StockDividend.gif");
    Image imgRightsIssue = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_RightsIssue.gif");
    Image imgReverseSplit = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_ReverseSplit.gif");
    Image imgCapitalReduction = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_CapitalReduction.gif");
    Image imgCashDividend = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_CashDividend.gif");
    Image imgAnnouncementPlus = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_AnnouncementPlus.gif");
    Image imgStockSplitPlus = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_StockSplitPlus.gif");
    Image imgStockDividendPlus = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_StockDividendPlus.gif");
    Image imgRightsIssuePlus = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_RightsIssuePlus.gif");
    Image imgReverseSplitPlus = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_ReverseSplitPlus.gif");
    Image imgCapitalReductionPlus = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_CapitalReductionPlus.gif");
    Image imgCashDividendPlus = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_CashDividendPlus.gif");
    //transient private static GregorianCalendar calendar = new GregorianCalendar(); //calen = Calendar.getInstance();
    SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
    TWDateFormat dateFormatter_ddMMMyyyy = new TWDateFormat("dd MMM yyyy");
    TWDateFormat dateFormatter_yyyy = new TWDateFormat("yyyy");
    TWDateFormat dateFormatter_dd = new TWDateFormat("dd");
    TWDateFormat dateFormatter_MMM = new TWDateFormat("MMM");
    TWDateFormat dateFormatter_dd_MMM = new TWDateFormat("dd MMM");
    //fields
    Thread dynamicUpdater;
    Object[] faTime = null;
    Object[] faAnnTime = null; //For announcements
    Object[] faAnnGrp = null; //For announcements
    Object[] faAnnTimeIndex = null; //For announcements
    Object[] faOpen = null;
    Object[] faHigh = null;
    Object[] faLow = null;
    Object[] faClose = null;
    boolean objectMoving = true;
    double[] minY = new double[20];
    double[] maxY = new double[20];
    double[] minCustomY = new double[20];
    double[] maxCustomY = new double[20];
    double[] yFactor = new double[20];
    float[] indexingFactors;
    int crossHairX = -1;
    int verticalX = -1;
    int crossHairY = -1;
    int dragStartX = -1;
    int dragEndX = -1;
    int pinBallX = -1;
    int highLowCrossX = -1;//keep a common cross hair x in the future
    int highLowCrossY = -1;
    //strategy related
    Font f1 = SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 18);
    boolean isIntradayDirty = false;
    private int MinimumUnit = 60;
    private long interval = MinimumUnit;
    private boolean active = true;
    private StockGraph graph;
    private DynamicArray graphStore;
    private ArrayList<ChartProperties> Sources;
    private ArrayList<HighlightedArea> highLightedSources = new ArrayList<HighlightedArea>();
    private ArrayList<AbstractObject> ObjectArray = new ArrayList<AbstractObject>();
    private ArrayList<AbstractObject> StaticObjectArray = new ArrayList<AbstractObject>();
    private GraphPointComparator gpComparator;
    private boolean isBusyNow = false;
    private boolean isBusyPaintingNow = false;
    private long lastPriceUpdateTime = System.currentTimeMillis();
    //graph fields
    private int sourceCount = 0;
    private int instantSourceCount = 0;
    private float endIndex, beginIndex;
    private int rMarginWidth = 12;
    private int rMarginIntraday = 25;
    private long latestTime = 0;
    private double maxPriceValue = 10.01;
    private double tmpPriceValue = 0;
    private int[] midNights;
    private boolean indexed = true;
    private boolean splitAdjust = false;
    private boolean showSplits = true;
    private boolean showAnnouncements = true;
    transient private int bestPixelPitch;
    private float dashThickness = 0.5f;
    private int barThickness = 3;
    private int dashLength = 1;
    private Stroke dashPen = null;
    private DynamicArray baseSymbolPoints; //records of Data mode
    private Object[] prePeriodRecord;
    private boolean tableMode;
    private boolean isVWAP = ChartSettings.getSettings().isShowVWAP();
    private String legendDisplayTime = "";
    private String legendDisplaySnapPrice = "";
    private ObjectHorizontalPriceLine lastPriceLine;
    private ObjectHorizontalPriceLine previousCloseLine;
    private boolean showCurrentPriceLine = ChartSettings.getSettings().isShowLastPriceLine();
    private boolean showPreviousCloseLine = ChartSettings.getSettings().isShowPreviousCloseLine();
    private boolean showResistanceLine = false;
    private boolean showVolumeByPrice = false;
    private boolean showOrderLines = ChartSettings.getSettings().isShowOrderLines();
    private ObjectBidAskTag[] bidAskTags;
    private boolean showBidAskTags = ChartSettings.getSettings().isShowBidAskTags();
    private long lastBidAskTagsUpdateTime = System.currentTimeMillis();
    private ObjectHorizontalPriceLine minPriceLine;
    private ObjectHorizontalPriceLine maxPriceLine;
    private boolean showMinMaxPriceLines = ChartSettings.getSettings().isShowMinMaxLines();
    private long lastMinMaxUpdateTime = System.currentTimeMillis();
    private boolean snapToPrice = false;
    private TWDecimalFormat cashFlowformatter = new TWDecimalFormat("#############.00");
    private TWDecimalFormat volumeformatter = new TWDecimalFormat("###,###,###,###,###");
    private TWDecimalFormat pctFormatter = new TWDecimalFormat("#0.00");
    // added to keep a constant values while painting
    private int graphStoreSize = 0;
    private long beginTimeMillisec = 0;
    //default set to true
    private boolean sd_adjusted = false;
    private boolean ss_adjusted = false;
    private boolean cd_adjusted = false;
    private boolean ri_adjusted = false;
    protected boolean[] adjustTypes = new boolean[]{sd_adjusted, cd_adjusted, ri_adjusted, ss_adjusted};
    //cross hair formatter : for Y values
    private DecimalFormat yValueFormat = new DecimalFormat("###,##0.00");
    public GraphDataManager(StockGraph sg) {
        isBusyNow = true;
        loadSettings();
        graph = sg;
        graphStore = new DynamicArray();
        baseSymbolPoints = new DynamicArray();
        gpComparator = new GraphPointComparator();
        graphStore.setComparator(gpComparator);
        baseSymbolPoints.setComparator(gpComparator);
        Sources = new ArrayList<ChartProperties>();
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        OrderStore.getSharedInstance().addOrderStatusListener(this);
        dynamicUpdater = new Thread(this, "TW Graph Data Manager");
        dynamicUpdater.start();
        isBusyNow = false;
    }

    public static long getMidnight(long time, boolean isCurrMode) {
        GregorianCalendar calendar = new GregorianCalendar();
        if (isCurrMode) {
            return (time / Miliseconds_Per_Day + 1) * Miliseconds_Per_Day;
        } else {
            calendar.setTimeInMillis(time);
            int DaysInThisYear = 365;
            if (calendar.isLeapYear(calendar.get(calendar.YEAR))) DaysInThisYear++;
            time = (time / Miliseconds_Per_Day - calendar.get(calendar.DAY_OF_YEAR) + 1 + DaysInThisYear) * Miliseconds_Per_Day;
            return time;
        }
    }

    public static String getFormattedValue(double value, int decimals) {
        if (value == 0) return "0.00";
        double logVal = Math.log10(Math.abs(value));
        if (logVal > 4) {
            decfmt.applyPattern(ChartConstants.NUM_FORMAT_LONG);
        } else if (logVal > 0) {
            decfmt.applyPattern("#,##0.00");
        } else {
            int noOfDecimals = (int) Math.round(Math.abs(logVal)) + decimals;
            StringBuilder sb = new StringBuilder();
            sb.append("0.");
            for (int i = 0; i < noOfDecimals / 3; i++) {
                sb.append("000,");
            }
            sb.append("000");
            decfmt.applyPattern(sb.toString());
        }
        return decfmt.format(value);
    }

    public static DecimalFormat getNumberFormat(double allowedPitch) {
        // microformat here must have the pattern "0.000000000"
        decfmt.applyPattern("###,###,##0.000000000");
        String strNum = decfmt.format(allowedPitch);
        int decimalCount = 9;
        while (strNum.lastIndexOf("0") == strNum.length() - 1) {
            strNum = strNum.substring(0, strNum.length() - 1);
            decimalCount--;
        }
        String pattern = "###,###,##0";
        if (decimalCount > 0) {
            pattern = "###,##0.00";
            for (int i = 2; i < decimalCount; i++) {
                pattern += "0";
            }
        }
        decfmt.applyPattern(pattern);

        return decfmt;
    }

    public Object getGraphLock() {
        return graphLock;
    }

    private void loadSettings() {
        boolean isGraphAdjusted = Settings.isGraphAdjusted();
        sd_adjusted = isGraphAdjusted;
        ss_adjusted = isGraphAdjusted;
        cd_adjusted = isGraphAdjusted;
        ri_adjusted = isGraphAdjusted;
        adjustMethod = isGraphAdjusted ? METHOD_NONE : METHOD_UNADJUSTED;
    }

    public void run() {
        while (active) {
            if (Sources.size() > 0) {
                try {
                    updateDataSources();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            try {
                Thread.sleep(800);
            } catch (InterruptedException ex) {
            }
        }
    }

    public void killThread() {
        active = false;
    }

    public DynamicArray getStore() {
        return graphStore;
    }

    public double getMaxPriceValue() {
        return maxPriceValue;
    }

    public void updateMaxPriceValue() {
        maxPriceValue = tmpPriceValue;
    }

    public double getTmpPriceValue() {
        return tmpPriceValue;
    }

    public int getGraphStoreSize() {
        if (graphStore != null) return graphStore.size();
        return 0;
    }

    // this is the method used to add a chart data or
    // an Indicator data to the graphStore
    public ChartProperties addDataSource(ArrayList sourceList, String symbol, Color c, byte ID, WindowPanel r, ChartProperties cp) {
        if (sourceList != null) {
            synchronized (graphLock) {
                isBusyNow = true;
                sourceCount++;
                refreshMinMaxFactorVars();
                graphColors.add(c);
                cp.setOHLCPriority(INNER_Close);
                Sources.add(cp);

                if (cp.getID() == ID_BASE) {

                    long[] xArr1 = new long[1];
                    double[] yArr1 = new double[1];
                    yArr1[0] = HORIZ_LINE_INITIAL_VALUE;
                    previousCloseLine = new ObjectHorizontalPriceLine(xArr1, yArr1, cp, cp.getRect(), graph);
                    previousCloseLine.setType(ObjectHorizontalPriceLine.PREVIOUS_CLOSE_LINE);
                    previousCloseLine.setColor(Theme.getColor("CHART_PREVIOUS_CLOSE_LINE_COLOR"));
                    previousCloseLine.setPenStyle((byte) PropertyDialogFactory.PEN_DASH);
                    previousCloseLine.setDescript(Language.getString("PREVIOUS_CLOSE_LINE"));
                    previousCloseLine.setFillPattern(false);
                    addToStaticObjectArray(previousCloseLine);

                    long[] xArr = new long[1];
                    double[] yArr = new double[1];
                    yArr[0] = HORIZ_LINE_INITIAL_VALUE;
                    lastPriceLine = new ObjectHorizontalPriceLine(xArr, yArr, cp, cp.getRect(), graph);
                    lastPriceLine.setType(ObjectHorizontalPriceLine.CURRENT_PRICE_LINE);
                    lastPriceLine.setColor(Theme.getColor("CHART_CURRENT_PRICE_LINE_COLOR"));
                    lastPriceLine.setPenStyle((byte) PropertyDialogFactory.PEN_DASH);
                    lastPriceLine.setFillPattern(false);
                    addToStaticObjectArray(lastPriceLine);

                    double[] minY = new double[1];
                    minY[0] = HORIZ_LINE_INITIAL_VALUE;
                    minPriceLine = new ObjectHorizontalPriceLine(null, minY, cp, cp.getRect(), graph);
                    minPriceLine.setType(ObjectHorizontalPriceLine.MIN_MAX_PRICE_LINE);
                    minPriceLine.setColor(Theme.getColor("CHART_MIN_PRICE_LINE_COLOR"));
                    minPriceLine.setPenStyle((byte) PropertyDialogFactory.PEN_DASH_DOT);
                    minPriceLine.setDescript(Language.getString("MIN_PRICE"));
                    minPriceLine.setFillPattern(true);
                    addToStaticObjectArray(minPriceLine);

                    double[] maxY = new double[1];
                    maxY[0] = HORIZ_LINE_INITIAL_VALUE;
                    maxPriceLine = new ObjectHorizontalPriceLine(null, maxY, cp, cp.getRect(), graph);
                    maxPriceLine.setType(ObjectHorizontalPriceLine.MIN_MAX_PRICE_LINE);
                    maxPriceLine.setColor(Theme.getColor("CHART_MAX_PRICE_LINE_COLOR"));
                    maxPriceLine.setPenStyle((byte) PropertyDialogFactory.PEN_DASH_DOT);
                    maxPriceLine.setDescript(Language.getString("MAX_PRICE"));
                    maxPriceLine.setFillPattern(true);
                    addToStaticObjectArray(maxPriceLine);

                    //adding 7 resitance lines //added by sathyajith

                    double[][] resDY = new double[NO_OF_RESITANCE_LINES][1];
                    for (int i = 0; i < NO_OF_RESITANCE_LINES; i++) {

                        resDY[i][0] = HORIZ_LINE_INITIAL_VALUE;
                        resistanceLines[i] = new ObjectHorizontalPriceLine(null, resDY[i], cp, cp.getRect(), graph);
                        resistanceLines[i].setType(ObjectHorizontalPriceLine.RESISTANCE_PRICE_LINE);
                        //resistanceLine.setColor(Theme.getColor("CHART_MIN_PRICE_LINE_COLOR"));

                        /*****************************************************************enabled***************/
                        resistanceLines[i].setPenStyle((byte) PropertyDialogFactory.PEN_DASH_DOT);
                        if (i == 0) {
                            resistanceLines[i].setColor(Theme.getColor("GRAPH_PIVOT_LINE_COLOR")); //Theme.getColor("PIVOT_COLOR)
                            resistanceLines[i].setDescript(Language.getString("PIVOT_LINE") + "=");
                            resistanceLines[i].setLineType(Language.getString("PIVOT") + "=");
                        } else if ((i % 2 == 0)) {//s1,s2,s3
                            resistanceLines[i].setColor(Theme.getColor("GRAPH_SUPPORT_LINE_COLOR"));
                            resistanceLines[i].setDescript(Language.getString("SUPPORT_LINE") + (i / 2) + "=");
                            resistanceLines[i].setLineType("S" + (i / 2) + "=");
                        } else { // R1,R2,R3
                            resistanceLines[i].setColor(Theme.getColor("GRAPH_RESISTANCE_LINE_COLOR"));
                            resistanceLines[i].setDescript(Language.getString("RESISTANCE_LINE") + ((i + 1) / 2) + "=");
                            resistanceLines[i].setLineType("R" + ((i + 1) / 2) + "=");
                        }
                        /*****************************************************************enabled***************/

                        resistanceLines[i].setFillPattern(true);
                        addToStaticObjectArray(resistanceLines[i]);
                    }


                    if (showBidAskTags) {
                        bidAskTags = ChartInterface.getBidTagAndAskTag(cp);
                        addToStaticObjectArray(bidAskTags[0]);
                        addToStaticObjectArray(bidAskTags[1]);
                    }

                    ArrayList<ObjectOrderLine> orderLines = ChartInterface.getOrderLines(cp);
                    for (ObjectOrderLine objectOrderLine : orderLines) {
                        removeFromStaticObjectArray(objectOrderLine);
                        if (showOrderLines && !objectOrderLine.isRemove()) {
                            addToStaticObjectArray(objectOrderLine);
                        }
                    }
                }

                insertDataSource(cp);
                isBusyNow = false;
            }
        }
        return cp;
    }

    private WindowPanel getParentWindowForIndicator(byte ID, ChartProperties cp) {
        switch (ID) {
            case ID_VOLUME:
                return graph.volumeRect;
            case ID_TURNOVER:
                return graph.turnOverRect;

            case ID_MOVING_AVERAGE:
            case ID_LRI:
            case ID_BOLLINGER_BANDS:
            case ID_MEDIAN_PRICE:
            case ID_ZIGZAG:
            case ID_PARABOLIC_SAR:
            case ID_ENVILOPES:
            case ID_WILDERS_SMOOTHING:
            case ID_TYPICAL_PRICE:
            case ID_WEIGHTED_CLOSE:
            case ID_DEMA:
            case ID_PRICE_CHANNELS:
            case ID_TSF:
            case ID_TEMA:
            case ID_ALLIGATOR:
            case ID_HULL_MOVING_AVG:
            case ID_KELTNER_CHANNELS:
                return cp.getRect();

            case ID_MACD:
            case ID_RSI:
            case ID_MOMENTUM:
            case ID_SDI:
            case ID_ACCU_DISTRI:
            case ID_WILLS_R:
            case ID_STOCHAST_OSCILL:
            case ID_AVG_TRUE_RANGE:
            case ID_MASS_INDEX:
            case ID_MONEY_FLOW_INDEX:
            case ID_TRIX:
            case ID_VOLATILITY:
            case ID_AROON:
            case ID_CHAIKINS_OSCILL:
            case ID_CCI:
            case ID_CHAIKIN_MONEY_FLOW:
            case ID_EASE_OF_MOVEMENT:
            case ID_ICHIMOKU_KINKO_HYO:
            case ID_KLINGER_OSCILL:
            case ID_PRICE_VOLUME_TREND:
            case ID_VOLUME_OSCILL:
            case ID_WILLS_ACCU_DISTRI:
            case ID_RMI:
            case ID_PRICE_OSCILL:
            case ID_INTRADAY_MOMENTUM_INDEX:
            case ID_ON_BALANCE_VOLUME:
            case ID_ROC:
            case ID_PLUS_DI:
            case ID_MINUS_DI:
            case ID_DX:
            case ID_ADX:
            case ID_ADXR:
            case ID_DPO:
            case ID_SWING_INDEX:
            case ID_RVI:
            case ID_MFI:
            case ID_PERFORMANCE:
            case ID_PVI:
            case ID_QSTICK:
            case ID_VERT_HORI_FILTER:
            case ID_VOLUME_ROC:
            case ID_FO:
            case ID_NVI:
            case ID_CMO:
            case ID_SMO:
            case ID_STD_ERR:
            case ID_MACD_VOLUME:
            case ID_NET_ORDERS:
            case ID_NET_TURNOVER:
                // added by Mevan
            case ID_COPP_CURVE:
            case ID_DISPARITY_INDEX:
                return new WindowPanel();
            default:
                return null;
        }
    }

    public ChartProperties addIndicator(byte ID) {
        if (Sources.size() <= 0) return null;
        int sourceActivated = 0;
        ChartProperties cp = (ChartProperties) Sources.get(sourceActivated);
        WindowPanel r = getParentWindowForIndicator(ID, cp);
        return addIndicator(ID, false, cp, r, true);
    }

    public ChartProperties addCustomIndicator(String name, Class indicatorClass, boolean isAutomatic) {
        if (Sources.size() <= 0) return null;
        int sourceActivated = 0;
        ChartProperties cp = (ChartProperties) Sources.get(sourceActivated);
        try {
            return addCustomIndicator(name, indicatorClass, isAutomatic, cp, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ChartProperties addCustomIndicator(String name, Class indicatorClass, boolean isAutomatic, ChartProperties cp, WindowPanel r) {
        //add this random number bcoz there may be get the same time when loading from the workspace
        String grpID = System.currentTimeMillis() + "_" + (int) (Math.random() * 100000000) % 1000000;
        try {
            Constructor constructor = indicatorClass.getConstructor(ArrayList.class, String.class, Color.class, WindowPanel.class);
            ChartProperties indicator = (ChartProperties) constructor.newInstance(cp.getDataSource(), cp.getSymbol(), Color.red, r);

            if (indicator == null) return null;

            ((IndicatorBase) indicator).setPlotID(0);
            if (r == null) {
                r = ((IndicatorBase) indicator).hasItsOwnScale() ? new WindowPanel() : cp.getRect();
                indicator.setRect(r);
            }

            ((Indicator) indicator).setInnerSource(cp);
            if (cp instanceof Indicator) {
                indicator.setOHLCPriority(INDICATOR);
            } else {
                indicator.setOHLCPriority(INNER_Close);
            }

            if (!isAutomatic) {
                PropertyDialog pd = new PropertyDialog(ChartInterface.getChartFrameParentComponent(), indicator, INSERT_PROPERTY, graph, false);//TODO:whats correct here
                pd.setLocationRelativeTo(graph);
                pd.setModal(true);
                pd.setVisible(true);
                if (pd.getModalResult()) {
                    pd.setVisible(false);
                    if (graph.isMaximized() && (graph.getIDforPanel(r) != graph.getMaximizedPanelID())) {
                        graph.setMaximized(false);
                    }
                    indicator.assignValuesFrom(pd.getTargetCP());
                    if ((r != null) && (graph.getIDforPanel(r) < 0)) {

                        // Modified on 21-10-2007
                        String bg = StockGraph.extractSymbolFromStr(getBaseGraph());
                        r.setTitle(indicator.getShortName() + " (" + bg + ")");
                        graph.addNewPanel(false, r);
                    }
                    synchronized (graphLock) {
                        isBusyNow = true;
                        sourceCount++;
                        Sources.add(indicator);
                        ((IndicatorBase) indicator).setGroupID(grpID);
                        insertDataSource(indicator);
                        isBusyNow = false;
                    }
                    //adding multiple plots
                    int plotCount = ((IndicatorBase) indicator).getPlotCount();
                    if (plotCount > 1) {
                        ChartProperties srcCP = ((IndicatorBase) indicator).IsStrategy() && !((IndicatorBase) indicator).isDrawSymbol() ? indicator : cp;
                        for (int i = 1; i < plotCount; i++) { // i=0 is already added
                            ChartProperties plot = (ChartProperties) constructor.newInstance(cp.getDataSource(),
                                    cp.getSymbol(), Color.red, r);
                            plot.assignValuesFrom(indicator);
                            ((IndicatorBase) plot).setPlotID(i);
                            ((IndicatorBase) plot).setGroupID(grpID);
                            if (((IndicatorBase) indicator).IsStrategy() && ((IndicatorBase) plot).isDrawSymbol()) {
                                ((IndicatorBase) plot).setTargetCP(((IndicatorBase) plot).isPattern() ? cp : srcCP);
                            }
                            synchronized (graphLock) {
                                isBusyNow = true;
                                sourceCount++;
                                Sources.add(plot);
                                insertDataSource(plot);
                                isBusyNow = false;
                            }
                            srcCP = ((IndicatorBase) indicator).IsStrategy() && !((IndicatorBase) plot).isDrawSymbol() ? plot : srcCP;
                        }
                    }
                } else {
                    indicator.setDataSource(null);
                    r = null;
                    indicator = null;
                }
                pd.setTargetCP(null);
                pd.dispose();
                pd = null;
                graph.reDrawAll();
            } else {//Used when indicators added programmetically (Like in Apply Template)
                if ((r != null) && (graph.getIDforPanel(r) < 0)) {
                    // Modified on 21-10-2007
                    String bg = StockGraph.extractSymbolFromStr(getBaseGraph());
                    r.setTitle(indicator.getShortName() + " (" + bg + ")");
                    graph.addNewPanel(false, r);
                }
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    insertDataSource(indicator);
                    isBusyNow = false;
                }
            }
            refreshDataWindow();
            //String IndicatorName = "-" + indicator.getShortName();
            if (indicator != null && indicator.getShortName() != null && !indicator.getShortName().isEmpty()) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.ChartStrategy, indicator.getShortName());
            }
            return indicator;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ChartProperties addIndicator(byte indId, byte targetId, byte windowStatus) {
        if (Sources.size() <= 0) return null;
        ChartProperties cp = (ChartProperties) Sources.get(targetId);
        WindowPanel r = null;
        if (windowStatus == 1) {
            r = new WindowPanel();
        } else if (windowStatus == 4) {
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cpTemp = (ChartProperties) Sources.get(i);
                if (cpTemp.getID() == GraphDataManager.ID_PLUS_DI ||
                        cpTemp.getID() == GraphDataManager.ID_MINUS_DI) {
                    r = cpTemp.getRect();
                }
            }
            if (r == null) {
                r = new WindowPanel();
            }
        } else {
            r = getParentWindowForIndicator(indId, cp);
        }
        return addIndicator(indId, false, cp, r, false);
    }

    public ChartProperties addIndicator(byte ID, boolean isAutomatic, ChartProperties cp, WindowPanel r, boolean showSources) {
        //if ((sourceHiLighted>sourceCount-1)||(sourceHiLighted<0))
        ArrayList alHilighted = cp.getDataSource();
        String symbol = cp.getSymbol();
        ChartProperties indicator = null;
        switch (ID) {
            case ID_VOLUME:
                indicator = new Volume(alHilighted, symbol, ID, Color.GRAY, r);
                break;
            case ID_MOVING_AVERAGE:
                indicator = new MovingAverage(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MACD:
                indicator = new MACD(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_RSI:
                indicator = new RSI(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MOMENTUM:
                indicator = new Momentum(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_LRI:
                indicator = new LinearRegression(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_SDI:
                indicator = new StandardDeviation(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_BOLLINGER_BANDS:
                indicator = new BollingerBand(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ACCU_DISTRI:
                indicator = new AccumulationDistribution(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_WILLS_R:
                indicator = new WilliamsR(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_STOCHAST_OSCILL:
                indicator = new StocasticOscillator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_AVG_TRUE_RANGE:
                indicator = new AverageTrueRange(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MASS_INDEX:
                indicator = new MassIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MEDIAN_PRICE:
                indicator = new MedianPrice(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MONEY_FLOW_INDEX:
                indicator = new MoneyFlowIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_TRIX:
                indicator = new TRIX(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_VOLATILITY:
                indicator = new Volatility(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ZIGZAG:
                indicator = new ZigZag(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_AROON:
                indicator = new Aroon(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_CHAIKINS_OSCILL:
                indicator = new ChaikinOscillator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_CCI:
                indicator = new CommodityChannelIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_CHAIKIN_MONEY_FLOW:
                indicator = new ChaikinMoneyFlow(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_EASE_OF_MOVEMENT:
                indicator = new EaseOfMovement(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ICHIMOKU_KINKO_HYO:
                indicator = new IchimokuKinkoHyo(alHilighted, symbol, ID, Color.MAGENTA, r);
                break;
            case ID_KLINGER_OSCILL:
                indicator = new KlingerOscillator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_PARABOLIC_SAR:
                indicator = new ParabolicSAR(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_PRICE_VOLUME_TREND:
                indicator = new PriceVolumeTrend(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_VOLUME_OSCILL:
                indicator = new VolumeOscillator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_WILLS_ACCU_DISTRI:
                indicator = new WillsAccumDistribution(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_RMI:
                indicator = new RelativeMomentumIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ENVILOPES:
                indicator = new Envelope(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_PRICE_OSCILL:
                indicator = new PriceOscillator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_INTRADAY_MOMENTUM_INDEX:
                indicator = new IntradayMomentumIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ON_BALANCE_VOLUME:
                indicator = new OnBalanceVolume(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ROC:
                indicator = new PriceROC(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_WILDERS_SMOOTHING:
                indicator = new WildersSmoothing(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_PLUS_DI:
                indicator = new PlusDI(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MINUS_DI:
                indicator = new MinusDI(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_DX:
                indicator = new DX(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ADX:
                indicator = new ADX(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ADXR:
                indicator = new ADXR(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_DPO:
                indicator = new DetrendPriceOscillator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_SWING_INDEX:
                indicator = new SwingIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_RVI:
                indicator = new RelativeVolatilityIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MFI:
                indicator = new MarketFacilitationIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_PERFORMANCE:
                indicator = new Performance(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_PVI:
                indicator = new PositiveVolumeIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_QSTICK:
                indicator = new Qstick(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_TYPICAL_PRICE:
                indicator = new TypicalPrice(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_VERT_HORI_FILTER:
                indicator = new VerticalHorizontalFilter(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_WEIGHTED_CLOSE:
                indicator = new WeightedClose(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_VOLUME_ROC:
                indicator = new VolumeROC(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_DEMA:
                indicator = new DEMA(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_PRICE_CHANNELS:
                indicator = new PriceChannel(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_TSF:
                indicator = new TimeSeriesForcast(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_FO:
                indicator = new ForcastOscillator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_NVI:
                indicator = new NegativeVolumeIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_CMO:
                indicator = new ChandeMomentumOscillator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_SMO:
                indicator = new StochasticMomentumIndex(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_TEMA:
                indicator = new TEMA(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_ALLIGATOR:
                indicator = new Alligator(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_STD_ERR:
                indicator = new StandardError(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MACD_HISTOGRAM:
                indicator = new MACDHistogram(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_MACD_VOLUME:
                indicator = new MACDVolume(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_NET_ORDERS:
                r.setClosable(false);
                indicator = new NetOrdersInd(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_NET_TURNOVER:
                r.setClosable(false);
                indicator = new NetTurnOverInd(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_COPP_CURVE:
//                r.setClosable(false);
                indicator = new CoppockCurve(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_DISPARITY_INDEX:
                indicator = new DisparityIndex(alHilighted, symbol, ID, Color.GRAY, r);
                break;
            case ID_HULL_MOVING_AVG:
                indicator = new HullMovingAverage(alHilighted, symbol, ID, Color.red, r);
                break;
            case ID_KELTNER_CHANNELS:
                indicator = new KeltnerChannels(alHilighted, symbol, ID, Color.GREEN, r);
                break;
            default:
                return null;
        }
        if (indicator == null) return null;
        ((Indicator) indicator).setInnerSource(cp);
        if (cp instanceof Indicator) {
            indicator.setOHLCPriority(INDICATOR);
        } else {
            indicator.setOHLCPriority(INNER_Close);
        }

        if (!isAutomatic) { //User adds an indicator and sets parameters
            //grouping related indicators
            String gID = System.currentTimeMillis() + "_" + (int) (Math.random() * 100000000) % 1000000;
            PropertyDialog pd = new PropertyDialog(ChartInterface.getChartFrameParentComponent(), indicator, INSERT_PROPERTY, graph, showSources);
            pd.setLocationRelativeTo(graph);
            pd.setModal(true);
            pd.setVisible(true);
            if (pd.getModalResult()) {
                pd.setVisible(false);

                if (graph.isMaximized() && (graph.getIDforPanel(r) != graph.getMaximizedPanelID())) {
                    graph.setMaximized(false);
                }
                indicator.assignValuesFrom(pd.getTargetCP());
                if (indicator.isUsingUserDefault()) {
                    IndicatorPropertyStore.getSharedInstance().saveDefaultProperties(indicator);
                }
                //Following block of code is to add indicator into another indicator from the PropertyDialog Window itself
                TWTabbedPane tabbedPane = pd.getTabPane();
                if (tabbedPane instanceof PropertyDialogTabPane) {
                    ChartProperties sourceCP = ((PropertyDialogTabPane) tabbedPane).getSourceCP();
                    if (sourceCP != null) {
                        ((Indicator) indicator).setInnerSource(sourceCP);
                        indicator.setDataSource(sourceCP.getDataSource());
                        if (sourceCP instanceof Indicator) {
                            indicator.setOHLCPriority(INDICATOR);
                        } else {
                            indicator.setOHLCPriority(INNER_Close);
                        }
                    }
                    WindowPanel tempRect = ((PropertyDialogTabPane) tabbedPane).getRect();

                    if (tempRect != null) {
                        indicator.setRect(((PropertyDialogTabPane) tabbedPane).getRect());
                        r = tempRect;
                        String tempGID = getGroupForRect(r);
                        if (!tempGID.equals("")) {
                            gID = tempGID;
                        }
                    }
                }

                if ((r != null) && (graph.getIDforPanel(r) < 0)) {
                    // Modified on 21-10-2007
                    String bg = StockGraph.extractSymbolFromStr(getBaseGraph());
                    r.setTitle(indicator.getShortName() + " (" + bg + ")");

                    graph.addNewPanel(false, r);
                }
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    insertDataSource(indicator);
                    isBusyNow = false;
                }
                addSignalIndicators(indicator, gID);
                addSignalLines(indicator);
                indicator.setGroupID(gID);//group ID added by shashika


            } else {
                indicator.setDataSource(null);
                r = null;
                indicator = null;
            }
            //added by shash
            if (pd.getModalDialgResult() == PropertyDialog.DIALOG_RESULT_RESET) {       //user selects reset button
                ChartProperties target = pd.getTempCP();
                IndicatorPropertyStore.getSharedInstance().deleteDefaultProperties(target);
            }

            pd.setTargetCP(null);
            pd.dispose();
            pd = null;
            graph.reDrawAll();
        } else { //Used when indicators added programmetically (Like in Apply Template)
            if ((r != null) && (graph.getIDforPanel(r) < 0)) {
                // Modified on 21-10-2007
                String bg = StockGraph.extractSymbolFromStr(getBaseGraph());
                r.setTitle(indicator.getShortName() + " (" + bg + ")");

                graph.addNewPanel(false, r);
            }
            synchronized (graphLock) {
                isBusyNow = true;
                sourceCount++;
                Sources.add(indicator);
                insertDataSource(indicator);
                isBusyNow = false;
            }
        }


        if (indicator != null && indicator.getShortName() != null && !indicator.getShortName().isEmpty()) {
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.ChartIndicator, indicator.getShortName());
        }
        return indicator;
    }

    public void addSignalLines(ChartProperties cp) {
        float[] faVals = null;
        switch (cp.getID()) {
            case ID_RSI:
            case ID_INTRADAY_MOMENTUM_INDEX:
            case ID_RVI:
            case ID_AROON:
                faVals = new float[]{30f, 70f};
                break;
            case ID_WILLS_R:
                faVals = new float[]{-20f, -80f};
                break;
            case ID_STOCHAST_OSCILL:
            case ID_MONEY_FLOW_INDEX:
                faVals = new float[]{20f, 80f};
                break;
            case ID_VOLATILITY:
            case ID_TRIX:
            case ID_CHAIKINS_OSCILL:
            case ID_CCI:
            case ID_CHAIKIN_MONEY_FLOW:
            case ID_KLINGER_OSCILL:
            case ID_PRICE_OSCILL:
            case ID_PRICE_VOLUME_TREND:
            case ID_ROC:
            case ID_VOLUME_OSCILL:
            case ID_PLUS_DI:
            case ID_MINUS_DI:
            case ID_DX:
            case ID_ADX:
            case ID_ADXR:
            case ID_MACD:
            case ID_DPO:
            case ID_SWING_INDEX:
            case ID_QSTICK:
            case ID_VOLUME_ROC:
            case ID_FO:
            case ID_MACD_VOLUME:
            case ID_COPP_CURVE:
            case ID_DISPARITY_INDEX:
                faVals = new float[]{0f};
                break;
            case ID_CMO:
                faVals = new float[]{50f, 0f, -50f};
                break;
            default:
                return;
        }
        for (int i = 0; i < faVals.length; i++) {
            long[] xArr = new long[1];
            double[] yArr = new double[1];
            xArr[0] = Math.round(faVals[i]);
            yArr[0] = faVals[i];
            AbstractObject oR = new ObjectLineHorizontal(xArr, yArr, cp, cp.getRect(), graph);
            oR.isVisible = !cp.isHidden();
            oR.setColor(new Color(150, 50, 50));
            addToStaticObjectArray(oR);
        }
    }

    public void addSignalIndicators(ChartProperties cp, String grpID) { // to add a second line to an indicator
        ArrayList alHilighted = cp.getDataSource();
        String symbol = cp.getSymbol();
        ChartProperties indicator = null;
        byte ID = ID_MOVING_AVERAGE;
        switch (cp.getID()) {
            case ID_MACD:
            case ID_MACD_VOLUME:
                ID = ID_MOVING_AVERAGE;
                cp.setSignalParent(true);
                Color sigColorMc = (cp instanceof MACD) ? ((MACD) cp).getSignalColor() : ((MACDVolume) cp).getSignalColor();
                indicator = new MovingAverage(alHilighted, symbol, ID, sigColorMc, cp.getRect());
                ((MovingAverage) indicator).setMethod(MovingAverage.METHOD_EXPONENTIAL);
                ((MovingAverage) indicator).setTimePeriods((cp instanceof MACD) ? ((MACD) cp).getSignalTimePeriods() :
                        ((MACDVolume) cp).getSignalTimePeriods());
                ((MovingAverage) indicator).setPenStyle((cp instanceof MACD) ? ((MACD) cp).getSignalStyle() :
                        ((MACDVolume) cp).getSignalStyle());
                ((MovingAverage) indicator).setPenWidth(1f);
                indicator.setSignalIndicator(true);
                indicator.setSignalSourceID(cp.getID());
                ((MovingAverage) indicator).setTableColumnHeading(
                        Language.getString("IND_TABLE_HEADING_MOVING_AVERAGE_MACD"));
                //sourceHiLighted = sourceCount-1;
                ((Indicator) indicator).setInnerSource(cp);
                indicator.setOHLCPriority(INDICATOR);
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    indicator.setGroupID(grpID);//group ID added by shashika
                    insertDataSource(indicator);
                    isBusyNow = false;
                }
                if (cp instanceof MACD) ((MACD) cp).setSignalLine((MovingAverage) indicator);
                else ((MACDVolume) cp).setSignalLine((MovingAverage) indicator);

                ID = ID_MACD_HISTOGRAM;
                indicator = new MACDHistogram(alHilighted, symbol, ID, sigColorMc, cp.getRect());
                ((MACDHistogram) indicator).setMaCalcMethod(
                        (cp instanceof MACD) ? MovingAverage.METHOD_EXPONENTIAL : MovingAverage.METHOD_VOLUME_ADJUSTED);
                indicator.setSignalIndicator(true);
                indicator.setSignalSourceID(cp.getID());
                indicator.setOHLCPriority(cp.getOHLCPriority());
                ((MACDHistogram) indicator).setTableColumnHeading(Language.getString("IND_TABLE_HEADING_HISTOGRAM_MACD"));
                ((Indicator) indicator).setInnerSource(((Indicator) cp).getInnerSource()/*cp*/);

                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    indicator.setGroupID(grpID);//group ID added by shashika
                    insertDataSource(indicator);
                    isBusyNow = false;
                }
                if (cp instanceof MACD) ((MACD) cp).setHistogram((MACDHistogram) indicator);
                else ((MACDVolume) cp).setHistogram((MACDHistogram) indicator);
                break;
            case ID_BOLLINGER_BANDS:
                byte[] ia = {BollingerBand.BT_LOWER, BollingerBand.BT_UPPER};
                ID = ID_BOLLINGER_BANDS;
                cp.setSignalParent(true);
                synchronized (graphLock) {
                    isBusyNow = true;

                    //addding the lower band
                    ChartProperties bollingerLower = new BollingerBand(((Indicator) cp).getInnerSource().getDataSource(), symbol, ID, Color.RED, cp.getRect());
                    bollingerLower.assignValuesFrom(cp);
                    bollingerLower.setSignalIndicator(true);
                    bollingerLower.setSignalSourceID(cp.getID());
                    ((BollingerBand) bollingerLower).setBandType(ia[0]);
                    ((BollingerBand) bollingerLower).setTableColumnHeading(Language.getString("IND_TABLE_HEADING_BOLLINGER_BANDS_LOW"));
                    bollingerLower.setGroupID(grpID);
                    sourceCount++;
                    Sources.add(bollingerLower);
                    insertDataSource(bollingerLower);
                    ((BollingerBand) cp).setLower(bollingerLower);

                    //addding the upper band
                    ChartProperties bollingerUpper = new BollingerBand(((Indicator) cp).getInnerSource().getDataSource(), symbol, ID, Color.RED, cp.getRect());
                    bollingerUpper.assignValuesFrom(cp);
                    bollingerUpper.setSignalIndicator(true);
                    bollingerUpper.setSignalSourceID(cp.getID());
                    ((BollingerBand) bollingerUpper).setBandType(ia[1]);
                    ((BollingerBand) bollingerUpper).setTableColumnHeading(Language.getString("IND_TABLE_HEADING_BOLLINGER_BANDS_UP"));
                    bollingerUpper.setGroupID(grpID);
                    sourceCount++;
                    Sources.add(bollingerUpper);
                    insertDataSource(bollingerUpper);
                    ((BollingerBand) cp).setUpper(bollingerUpper);

                    //HighlightedArea bollingerBands = new HighlightedArea(bollingerUpper, bollingerLower,  new Color(239, 241, 255));
                    HighlightedArea bollingerBands = new HighlightedArea(bollingerUpper, bollingerLower, new Color(0, 0, 255));
                    highLightedSources.add(bollingerBands);

                    isBusyNow = false;
                }
                break;
            case ID_KELTNER_CHANNELS:
                byte[] ia1 = {KeltnerChannels.KC_LOWER, KeltnerChannels.KC_UPPER};
                ID = ID_KELTNER_CHANNELS;
                cp.setSignalParent(true);
                synchronized (graphLock) {
                    isBusyNow = true;

                    //addding the lower channel
                    ChartProperties kcLower = new KeltnerChannels(((Indicator) cp).getInnerSource().getDataSource(), symbol, ID, Color.BLUE, cp.getRect());
                    kcLower.assignValuesFrom(cp);
                    kcLower.setColor(Color.BLUE);
                    kcLower.setUseSameColor(true);
                    kcLower.setSignalIndicator(true);
                    kcLower.setSignalSourceID(cp.getID());
                    ((KeltnerChannels) kcLower).setKeltnerChannelType(ia1[0]);
                    ((KeltnerChannels) kcLower).setTableColumnHeading(Language.getString("IND_TABLE_HEADING_KELTNER_CHANNELS_DOWN"));
                    kcLower.setGroupID(grpID);
                    sourceCount++;
                    Sources.add(kcLower);
                    insertDataSource(kcLower);
                    ((KeltnerChannels) cp).setLower(kcLower);

                    //addding the upper channel
                    ChartProperties kcUpper = new KeltnerChannels(((Indicator) cp).getInnerSource().getDataSource(), symbol, ID, Color.BLUE, cp.getRect());
                    kcUpper.assignValuesFrom(cp);
                    kcUpper.setColor(Color.BLUE);
                    kcUpper.setUseSameColor(true);
                    kcUpper.setSignalIndicator(true);
                    kcUpper.setSignalSourceID(cp.getID());
                    ((KeltnerChannels) kcUpper).setKeltnerChannelType(ia1[1]);
                    ((KeltnerChannels) kcUpper).setTableColumnHeading(Language.getString("IND_TABLE_HEADING_KELTNER_CHANNELS_UP"));
                    kcUpper.setGroupID(grpID);
                    sourceCount++;
                    Sources.add(kcUpper);
                    insertDataSource(kcUpper);
                    ((KeltnerChannels) cp).setUpper(kcUpper);

                    //HighlightedArea keltnerChannels = new HighlightedArea(kcUpper, kcLower,  new Color(239, 241, 255));
                    HighlightedArea keltnerChannels = new HighlightedArea(kcUpper, kcLower, new Color(0, 0, 255));
                    highLightedSources.add(keltnerChannels);

                    isBusyNow = false;
                }
                break;
            case ID_ALLIGATOR:
                byte[] iall = {Alligator.ALLIGATOR_LIPS, Alligator.ALLIGATOR_JAW};
                ID = ID_ALLIGATOR;
                cp.setSignalParent(true);
                synchronized (graphLock) {
                    isBusyNow = true;

                    //addding the lower band
                    ChartProperties alligatorLower = new Alligator(((Indicator) cp).getInnerSource().getDataSource(), symbol, ID, null, cp.getRect());
                    alligatorLower.assignValuesFrom(cp);
                    alligatorLower.setColor(Color.GREEN);
                    alligatorLower.setUseSameColor(true);
                    alligatorLower.setSignalIndicator(true);
                    alligatorLower.setSignalSourceID(cp.getID());
                    ((Alligator) alligatorLower).setAlligatorType(iall[0]);
                    ((Alligator) alligatorLower).setTableColumnHeading("Alligator Lower-lips");
                    alligatorLower.setGroupID(grpID);
                    ((Alligator) cp).setLower(alligatorLower);
                    sourceCount++;
                    Sources.add(alligatorLower);
                    insertDataSource(alligatorLower);

                    //addding the upper band
                    ChartProperties alligatorUpper = new Alligator(((Indicator) cp).getInnerSource().getDataSource(), symbol, ID, null, cp.getRect());
                    alligatorUpper.assignValuesFrom(cp);
                    alligatorUpper.setColor(new Color(102, 102, 255));
                    alligatorUpper.setUseSameColor(true);
                    alligatorUpper.setSignalIndicator(true);
                    alligatorUpper.setSignalSourceID(cp.getID());
                    ((Alligator) alligatorUpper).setAlligatorType(iall[1]);
                    ((Alligator) alligatorUpper).setTableColumnHeading("Alligator Lower-jows");
                    alligatorUpper.setGroupID(grpID);
                    ((Alligator) cp).setUpper(alligatorUpper);
                    sourceCount++;
                    Sources.add(alligatorUpper);
                    insertDataSource(alligatorUpper);

                    isBusyNow = false;
                }
                break;
            case ID_STOCHAST_OSCILL:
                ID = ID_MOVING_AVERAGE;
                cp.setSignalParent(true);
                Color sigColorSto = ((StocasticOscillator) cp).getSignalColor();
                indicator = new MovingAverage(alHilighted, symbol, ID, sigColorSto, cp.getRect());
                ((MovingAverage) indicator).setMethod(MovingAverage.METHOD_SIMPLE);
                ((MovingAverage) indicator).setTimePeriods(((StocasticOscillator) cp).getSignalTimePeriods());
                ((MovingAverage) indicator).setPenStyle(((StocasticOscillator) cp).getSignalStyle());
                ((MovingAverage) indicator).setPenWidth(1f);
                ((Indicator) indicator).setInnerSource(cp);
                indicator.setOHLCPriority(INDICATOR);
                indicator.setSignalIndicator(true);
                indicator.setSignalSourceID(cp.getID());
                ((MovingAverage) indicator).setTableColumnHeading(
                        Language.getString("IND_TABLE_HEADING_MOVING_AVERAGE_STOCH"));
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    //group ID added by shashika
                    indicator.setGroupID(grpID);
                    insertDataSource(indicator);
                    isBusyNow = false;
                }
                ((StocasticOscillator) cp).setSignalLine((MovingAverage) indicator);
                break;
            case ID_AROON:
                cp.setSignalParent(true);
                indicator = new Aroon(alHilighted, symbol, ID, Color.GRAY, cp.getRect());
                indicator.assignValuesFrom(cp);
                ((Aroon) indicator).setStyle(Aroon.AROON_DOWN);
                ((Aroon) indicator).setColor(Color.GRAY);
                ((Aroon) indicator).setWarningColor(Color.GRAY);
                ((Aroon) indicator).setNeutralColor(Color.GRAY);
                indicator.setSignalIndicator(true);
                indicator.setSignalSourceID(cp.getID());
                ((Aroon) indicator).setTableColumnHeading(
                        Language.getString("IND_TABLE_HEADING_AROON_DWN"));
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    indicator.setGroupID(grpID);//group ID added by shashika
                    insertDataSource(indicator);
                    isBusyNow = false;
                }
                break;
            case ID_ENVILOPES:
                cp.setSignalParent(true);
                indicator = new Envelope(alHilighted, symbol, ID, cp.getColor(), cp.getRect());
                indicator.assignValuesFrom(cp);
                ((Envelope) indicator).setStyle(Envelope.ENVELOPE_BOTTOM);
                indicator.setSignalIndicator(true);
                indicator.setSignalSourceID(cp.getID());
                ((Envelope) indicator).setTableColumnHeading(
                        Language.getString("IND_TABLE_HEADING_ENVILOPES_DWN"));
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    indicator.setGroupID(grpID);
                    insertDataSource(indicator);
                    //HighlightedArea haEnvelope = new HighlightedArea(cp, indicator, new Color(239, 241, 255));
                    HighlightedArea haEnvelope = new HighlightedArea(cp, indicator, new Color(0, 0, 255));
                    highLightedSources.add(haEnvelope);
                    isBusyNow = false;
                }
                break;
            case ID_ICHIMOKU_KINKO_HYO:
                cp.setSignalParent(true);
                byte[] baICH = {IchimokuKinkoHyo.KIJUN_SEN, IchimokuKinkoHyo.SENKOU_SPAN_A, IchimokuKinkoHyo.SENKOU_SPAN_B,
                        IchimokuKinkoHyo.CHIKOU_SPAN, IchimokuKinkoHyo.CLOSE};
                Color[] caICH = {Color.RED, new Color(0, 255, 255), new Color(0, 255, 0), new Color(128, 0, 0), Color.GRAY};
                ID = ID_ICHIMOKU_KINKO_HYO;
                synchronized (graphLock) {
                    isBusyNow = true;
                    for (int j = 0; j < baICH.length; j++) {
                        indicator = new IchimokuKinkoHyo(alHilighted, symbol, ID, caICH[j], cp.getRect());
                        indicator.assignValuesFrom(cp);
                        ((IchimokuKinkoHyo) indicator).setStyle(baICH[j]);
                        indicator.setColor(caICH[j]);
                        indicator.setWarningColor(caICH[j]);
                        indicator.setNeutralColor(caICH[j]);
                        indicator.setSignalIndicator(true);
                        indicator.setSignalSourceID(cp.getID());
                        switch (j) {
                            case 0:
                                ((IchimokuKinkoHyo) indicator).setTableColumnHeading(
                                        Language.getString("IND_TABLE_HEADING_ICHIMOKU_KINKO_HYO_KIJUN"));
                                break;
                            case 1:
                                ((IchimokuKinkoHyo) indicator).setTableColumnHeading(
                                        Language.getString("IND_TABLE_HEADING_ICHIMOKU_KINKO_HYO_SPAN_A"));
                                break;
                            case 2:
                                ((IchimokuKinkoHyo) indicator).setTableColumnHeading(
                                        Language.getString("IND_TABLE_HEADING_ICHIMOKU_KINKO_HYO_SPAN_B"));
                                break;
                            case 3:
                                ((IchimokuKinkoHyo) indicator).setTableColumnHeading(
                                        Language.getString("IND_TABLE_HEADING_ICHIMOKU_KINKO_HYO_CHIKOU"));
                                break;
                            case 4:
                                ((IchimokuKinkoHyo) indicator).setTableColumnHeading(
                                        Language.getString("IND_TABLE_HEADING_ICHIMOKU_KINKO_HYO_CLOSE"));
                                break;
                        }

                        sourceCount++;
                        Sources.add(indicator);
                        indicator.setGroupID(grpID);
                        insertDataSource(indicator);
                    }
                    isBusyNow = false;
                }
                break;
            case ID_KLINGER_OSCILL:
                cp.setSignalParent(true);
                ID = ID_MOVING_AVERAGE;
                Color sigColorKVO = ((KlingerOscillator) cp).getSignalColor();
                indicator = new MovingAverage(alHilighted, symbol, ID, sigColorKVO, cp.getRect());
                ((MovingAverage) indicator).setMethod(MovingAverage.METHOD_SIMPLE);
                ((MovingAverage) indicator).setTimePeriods(((KlingerOscillator) cp).getSignalTimePeriods());
                ((MovingAverage) indicator).setPenStyle(((KlingerOscillator) cp).getSignalStyle());
                ((MovingAverage) indicator).setPenWidth(1f);
                ((Indicator) indicator).setInnerSource(cp);
                indicator.setOHLCPriority(INDICATOR);
                indicator.setSignalIndicator(true);
                indicator.setSignalSourceID(cp.getID());
                ((MovingAverage) indicator).setTableColumnHeading(
                        Language.getString("IND_TABLE_HEADING_MOVING_AVERAGE_KLNG"));
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    indicator.setGroupID(grpID);
                    insertDataSource(indicator);
                    isBusyNow = false;
                }
                ((KlingerOscillator) cp).setSignalLine((MovingAverage) indicator);
                break;
            case ID_PRICE_CHANNELS:
                cp.setSignalParent(true);
                indicator = new PriceChannel(alHilighted, symbol, ID, Color.red, cp.getRect());
                indicator.assignValuesFrom(cp);
                ((PriceChannel) indicator).setBandType(PriceChannel.PC_LOWER);
                ((PriceChannel) indicator).setColor(Color.red);
                indicator.setSignalIndicator(true);
                indicator.setSignalSourceID(cp.getID());
                ((PriceChannel) indicator).setTableColumnHeading(Language.getString("IND_TABLE_HEADING_PC_LOWER"));
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    indicator.setGroupID(grpID);
                    insertDataSource(indicator);
                    //HighlightedArea haPriceChannel = new HighlightedArea(cp, indicator, new Color(239, 241, 255));
                    HighlightedArea haPriceChannel = new HighlightedArea(cp, indicator, new Color(0, 0, 255));
                    highLightedSources.add(haPriceChannel);
                    isBusyNow = false;
                }
                break;
            case ID_FO:
                cp.setSignalParent(true);
                ID = ID_MOVING_AVERAGE;
                Color sigColorFo = ((ForcastOscillator) cp).getSignalColor();
                indicator = new MovingAverage(alHilighted, symbol, ID, sigColorFo, cp.getRect());
                ((MovingAverage) indicator).setMethod(MovingAverage.METHOD_EXPONENTIAL);
                ((MovingAverage) indicator).setTimePeriods(((ForcastOscillator) cp).getSignalTimePeriods());
                indicator.setPenStyle(((ForcastOscillator) cp).getSignalStyle());
                indicator.setPenWidth(1f);
                ((Indicator) indicator).setInnerSource(cp);
                indicator.setOHLCPriority(INDICATOR);
                indicator.setSignalIndicator(true);
                indicator.setSignalSourceID(cp.getID());
                ((MovingAverage) indicator).setTableColumnHeading(
                        Language.getString("IND_TABLE_HEADING_MOVING_AVERAGE_FO"));
                synchronized (graphLock) {
                    isBusyNow = true;
                    sourceCount++;
                    Sources.add(indicator);
                    indicator.setGroupID(grpID);
                    insertDataSource(indicator);
                    isBusyNow = false;
                }
                ((ForcastOscillator) cp).setSignalLine((MovingAverage) indicator);
                break;
            default:
                isBusyNow = false;
                return;
        }
        isBusyNow = false;
    }

    public void updateTheme() {
        ChartProperties cp;
        imgIconWindow = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_window.gif");
        for (int i = 0; i < Sources.size(); i++) {
            cp = (ChartProperties) Sources.get(i);
            if ((cp.getID() == ID_BASE)) {
                cp.setColor(graph.graphUpColor);
                cp.setWarningColor(graph.graphDownColor);
            } else if (cp.getID() == ID_VOLUME) {
                cp.setColor(graph.graphVolumeUpColor);
                cp.setWarningColor(graph.graphVolumeDownColor);
            } else if (cp.getID() == ID_TURNOVER) {
                cp.setColor(graph.graphTurnOverUpColor);
                cp.setWarningColor(graph.graphTurnOverDownColor);
            }
        }

        //change the colors of the dynamic objects   (:-) etc)  //SATHYAJITH
        ArrayList alOblects = getObjArray();
        for (int j = 0; j < alOblects.size(); j++) {
            AbstractObject ao = (AbstractObject) alOblects.get(j);
            ao.setColor(Theme.getColor("GRAPH_SYMBOL_COLOR"));
            if (ao.getObjType() == AbstractObject.INT_SYMBOL) {
                //System.out.println("*******************************int symbol abstract objects ****************************************************");
            }
        }

    }

    public void addVolumeToSources() {
        if (Sources.size() < 1) return;
        ChartProperties cp = (ChartProperties) Sources.get(0);
        ArrayList alHilighted = cp.getDataSource();
        String symbol = cp.getSymbol();
        WindowPanel r = graph.volumeRect;
        ChartProperties indicator = new Volume(alHilighted, symbol, ID_VOLUME, Color.GRAY, r);
        indicator.setColor(Theme.getColor("GRAPH_VOLUME_UP_COLOR"));
        indicator.setWarningColor(Theme.getColor("GRAPH_VOLUME_DOWN_COLOR"));
        indicator.setChartStyle((byte) StockGraph.INT_GRAPH_STYLE_BAR);
        ((Indicator) indicator).setInnerSource(cp);
        indicator.setOHLCPriority(INNER_Volume);

        // Modified on 21-10-2007
        String bg = StockGraph.extractSymbolFromStr(getBaseGraph());
        r.setTitle(indicator.getShortName() + " (" + bg + ")");

        if (graph.isShowingVolumeGrf()) {
            graph.addNewPanel(false, r);
        }
        synchronized (graphLock) {
            isBusyNow = true;
            sourceCount++;
            Sources.add(indicator);
            insertDataSource(indicator);
            isBusyNow = false;
        }
        graph.repaint();
    }

    public void addTurnOverToSources() {
        if (Sources.size() < 1) return;
        ChartProperties cp = (ChartProperties) Sources.get(0);
        ArrayList alHilighted = cp.getDataSource();
        String symbol = cp.getSymbol();
        WindowPanel r = graph.turnOverRect;
        ChartProperties indicator = new TurnOver(alHilighted, symbol, ID_TURNOVER, Color.GRAY, r);
        indicator.setColor(Theme.getColor("GRAPH_TURNOVER_UP_COLOR"));
        indicator.setWarningColor(Theme.getColor("GRAPH_TURNOVER_DOWN_COLOR"));
        indicator.setChartStyle((byte) StockGraph.INT_GRAPH_STYLE_BAR);
        ((Indicator) indicator).setInnerSource(cp);
        indicator.setOHLCPriority(INNER_TurnOver);

        String bg = StockGraph.extractSymbolFromStr(getBaseGraph());
        r.setTitle(indicator.getShortName() + " (" + bg + ")");

        if (graph.isShowingTurnOverGraph()) {
            graph.addNewPanel(false, r);
        }
        synchronized (graphLock) {
            isBusyNow = true;
            sourceCount++;
            Sources.add(indicator);
            insertDataSource(indicator);
            isBusyNow = false;
        }
        graph.repaint();
    }

    private void refreshMinMaxFactorVars() {
        int count = graph.panels.size();
        if ((count > 0) && (count > minY.length)) {
            minY = new double[count];
            maxY = new double[count];
            minCustomY = new double[count];
            maxCustomY = new double[count];
            yFactor = new double[count];
            resetCustomYBounds();
        }
    }

    public void reloadDataFromSource(ChartProperties cp) {
        byte ID = cp.getID();
        ArrayList sourceList;
        if (ID > ID_COMPARE) {
            sourceList = ((Indicator) cp).getInnerSource().getDataSource();
        } else {
            sourceList = cp.getDataSource();
        }
        int dataSize = sourceList.size();
        if (dataSize > 0) {
            if (ID > ID_COMPARE) {
                updateIndicatorRecords(sourceCount - 1, ((Indicator) cp).getInnerSourceIndex(Sources));
            } else {
                updateSymbolRecords(sourceCount - 1, cp, 0, dataSize);
            }
            cp.setRecordSize(dataSize);
        }
    }

    public void requestLastRecord() {
        lastPriceUpdateTime = System.currentTimeMillis() - 2 * PRICE_UPDATE_MINIMUM_WAIT;
    }

    public void insertDataSource(ChartProperties cp) {
        byte ID = cp.getID();
        ArrayList sourceList;
        if (ID > ID_COMPARE) {
            sourceList = ((Indicator) cp).getInnerSource().getDataSource();
        } else {
            sourceList = cp.getDataSource();
        }
        int dataSize = sourceList.size();
        if (dataSize > 0) {
            if (ID > ID_COMPARE) {
                updateIndicatorRecords(sourceCount - 1, ((Indicator) cp).getInnerSourceIndex(Sources));
            } else {
                lastPriceUpdateTime = System.currentTimeMillis() - 2 * PRICE_UPDATE_MINIMUM_WAIT;
                updateSymbolRecords(sourceCount - 1, cp, 0, dataSize);
            }

            cp.setRecordSize(dataSize);
            insertDummyPoints(tmpLatestTime);

            setPeriodBeginIndex(graph.getPeriod(), false);
            if ((cp.getID() == ID_BASE) || (graph.zSlider.getZoom() >= 99.5f)) {
                beginIndex = periodBeginIndex;
                endIndex = Math.max(graphStore.size() - 1, 0);
            }

            if (cp.getID() == ID_BASE && showCurrentPriceLine && lastPriceLine != null) {
                double[] yArr = lastPriceLine.getYArr();
                yArr[0] = ((IntraDayOHLC) cp.getDataSource().get(cp.getDataSource().size() - 1)).getClose();
            }

            if (cp.getID() == ID_BASE && showPreviousCloseLine && previousCloseLine != null) {
                double[] yArr = previousCloseLine.getYArr();
                yArr[0] = ChartInterface.getPreviousCloseValue(cp);
            }

            if (cp.getID() == ID_BASE && showMinMaxPriceLines && minPriceLine != null && maxPriceLine != null) {
                String key = getBaseCP().getSymbol();
                if (getBaseCP().getSymbol().split("~").length == 2) {
                    key += "~0";
                }
                double[] minY = minPriceLine.getYArr();
                minY[0] = ChartInterface.getStockObject(key).getMinPrice();
                double[] maxY = maxPriceLine.getYArr();
                maxY[0] = ChartInterface.getStockObject(key).getMaxPrice();
            }

            //adding 7 resistance lines
            if (cp.getID() == ID_BASE && showResistanceLine && resistanceLines != null) {

                double[][] resDYArr = new double[NO_OF_RESITANCE_LINES][0];
                for (int i = 0; i < NO_OF_RESITANCE_LINES; i++) {
                    if (resistanceLines[i] != null) {
                        resDYArr[i] = resistanceLines[i].getYArr();

                        IntraDayOHLC intOHlc = ((IntraDayOHLC) cp.getDataSource().get(cp.getDataSource().size() - 1));
                        double pivotPoint = (intOHlc.getClose() + intOHlc.getHigh() + intOHlc.getLow()) / 3;
                        switch (i) {

                            case 0:  //pivot =  PP = (High + Low + Close)/3
                                resDYArr[i][0] = (intOHlc.getClose() + intOHlc.getHigh() + intOHlc.getLow()) / 3;
                                break;
                            case 1:  //R1 = (2 x PP) � Low
                                resDYArr[i][0] = (2 * pivotPoint) - intOHlc.getLow();
                                break;
                            case 2:  //S1 = (2 x PP) � High
                                resDYArr[i][0] = (2 * pivotPoint) - intOHlc.getHigh();
                                break;
                            case 3:  //R2 = PP + (High � Low)
                                resDYArr[i][0] = pivotPoint + (intOHlc.getHigh() - intOHlc.getLow());
                                break;
                            case 4: //S2 = PP � (High � Low)
                                resDYArr[i][0] = pivotPoint - (intOHlc.getHigh() - intOHlc.getLow());
                                break;
                            case 5://R3 = High + 2 x (PP � Low)
                                resDYArr[i][0] = intOHlc.getHigh() + 2 * (pivotPoint - intOHlc.getLow());
                                break;
                            case 6://S3 = Low � 2 x (High � PP)
                                resDYArr[i][0] = intOHlc.getLow() - (2 * (intOHlc.getHigh() - pivotPoint));
                                break;

                        }
                    }
                }
            }
            resetSlider();
            reCalculateGraphParams();
        }
    }

    public void resetSlider() {
        graph.zSlider.resetSlider(graphStore.size() - periodBeginIndex, endIndex - beginIndex + 1, beginIndex - periodBeginIndex);
    }

    public void removeDataSource(String smbl) {
        float z = graph.zSlider.getZoom();
        float p = graph.zSlider.getBeginPos();
        synchronized (graphLock) {
            isBusyNow = true;
            for (int i = Sources.size() - 1; i >= 0; i--) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp.getSymbol().equals(smbl)) {
                    removeRelatedObjects(cp);
                    removeIndicatorTree(cp);
                    Sources.remove(i);
                    ChartInterface.removeOHLCRequest(graph.isCurrentMode(), smbl);
                    if (!(cp instanceof Indicator)) {
                        removeColor(cp.getColor());
                    }
                    break;
                }
            }
            reConstructGraphStore();
            refreshMinMaxFactorVars();
            isBusyNow = false;
        }
        graph.zSlider.setZoomAndPos(z, p);
        setZoomAndPos(z, p);
        graph.reDrawAll();
    }

    private void removeRelatedIndicators(ChartProperties cpInd) {

        try {
            if (cpInd instanceof Indicator) {
                String groupID = (cpInd).getGroupID();
                for (int i = Sources.size() - 1; i >= 0; i--) {
                    ChartProperties cp = Sources.get(i);
                    String id = cp.getGroupID();
                    if (id.equals(groupID)) {
                        Sources.remove(i);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void removeIndicator(ChartProperties cpInd) {
        float z = graph.zSlider.getZoom();
        float p = graph.zSlider.getBeginPos();
        synchronized (graphLock) {
            isBusyNow = true;
            removeRelatedObjects(cpInd);
            try {
                if (cpInd instanceof IndicatorBase) { //remove all the relateed indicators in strategies.. added by charithn
                    String groupID = ((IndicatorBase) (cpInd)).getGroupID();
                    for (int i = Sources.size() - 1; i >= 0; i--) {
                        ChartProperties cp = (ChartProperties) Sources.get(i);
                        if (!(cp instanceof IndicatorBase)) {  //ex : volume , turnover etc.
                            continue;
                        }
                        String id = ((IndicatorBase) (cp)).getGroupID();
                        if (id.equals(groupID)) {
                            Sources.remove(i);
                        }
                    }
                } else {
                    Sources.remove(cpInd);
//                    removeRelatedIndicators(cpInd); //added by charithn-2010-02-15 to remove related indicators like BB, Alligator
                }

                Rectangle rect = cpInd.getRect();
                boolean rectEmpty = true;
                for (int i = Sources.size() - 1; i >= 0; i--) {
                    ChartProperties cp = Sources.get(i);
                    if (cp.getRect() == rect) {
                        rectEmpty = false;
                        break;
                    }
                }
                if (rectEmpty) {
                    int index = graph.panels.lastIndexOf(rect);
                    int baseIndex = getIndexOfTheRect(graph.panels, getBaseCP().getRect());
                    if (index > -1 && index != baseIndex) {
                        graph.deletePanel(index);
                    }
                }

                for (int i = Sources.size() - 1; i >= 0; i--) {
                    ChartProperties cp = Sources.get(i);
                    if ((cp.getID() <= ID_COMPARE) || (cp.getID() == ID_VOLUME)) continue;

                    if (((Indicator) cp).getInnerSource() == cpInd) {
                        removeIndicator(cp);
                    }
                }

                if (cpInd instanceof BollingerBand) {
                    if (((BollingerBand) cpInd).getBandType() == BollingerBand.BT_CENTER) {
                        if (((BollingerBand) cpInd).getUpper() != null) {
                            removeIndicator(((BollingerBand) cpInd).getUpper());
                        }
                        if (((BollingerBand) cpInd).getLower() != null) {
                            removeIndicator(((BollingerBand) cpInd).getLower());
                        }
                    }
                }
                if (cpInd instanceof KeltnerChannels) {
                    if (((KeltnerChannels) cpInd).getKeltnerChannelType() == KeltnerChannels.KC_CENTER) {
                        if (((KeltnerChannels) cpInd).getUpper() != null) {
                            removeIndicator(((KeltnerChannels) cpInd).getUpper());
                        }
                        if (((KeltnerChannels) cpInd).getLower() != null) {
                            removeIndicator(((KeltnerChannels) cpInd).getLower());
                        }
                    }
                }

                removeHighlightedSource(cpInd);
                reConstructGraphStore();
                refreshMinMaxFactorVars();
                isBusyNow = false;

                graph.zSlider.setZoomAndPos(z, p);
                setZoomAndPos(z, p);
                graph.reDrawAll();

                //todo: need to add listener
                refreshDataWindow();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void removeIndicatorOld(ChartProperties cpInd) {
        float z = graph.zSlider.getZoom();
        float p = graph.zSlider.getBeginPos();
        synchronized (graphLock) {
            isBusyNow = true;
            removeRelatedObjects(cpInd);
            try {
                if (cpInd instanceof IndicatorBase) { //remove all the relateed indicators in strategies.. added by charithn
                    String groupID = ((IndicatorBase) (cpInd)).getGroupID();
                    for (int i = Sources.size() - 1; i >= 0; i--) {
                        ChartProperties cp = (ChartProperties) Sources.get(i);
                        if (!(cp instanceof IndicatorBase)) {  //ex : volume , turnover etc.
                            continue;
                        }
                        String id = ((IndicatorBase) (cp)).getGroupID();
                        if (id.equals(groupID)) {
                            Sources.remove(i);
                        }
                    }
                } else {
                    Sources.remove(cpInd);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Rectangle rect = cpInd.getRect();
            boolean rectEmpty = true;
            for (int i = Sources.size() - 1; i >= 0; i--) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp.getRect() == rect) {
                    rectEmpty = false;
                    break;
                }
            }
            if (rectEmpty) {
                int index = graph.panels.lastIndexOf(rect);
                int baseIndex = getIndexOfTheRect(graph.panels, getBaseCP().getRect());
                if (index > -1 && index != baseIndex) {
                    graph.deletePanel(index);
                }
            }

            for (int i = Sources.size() - 1; i >= 0; i--) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if ((cp.getID() <= ID_COMPARE) || (cp.getID() == ID_VOLUME)) continue;

                if (((Indicator) cp).getInnerSource() == cpInd) {
                    removeIndicator(cp);
                }
            }

            if (cpInd instanceof BollingerBand) {
                if (((BollingerBand) cpInd).getBandType() == BollingerBand.BT_CENTER) {
                    if (((BollingerBand) cpInd).getUpper() != null) {
                        removeIndicator(((BollingerBand) cpInd).getUpper());
                    }
                    if (((BollingerBand) cpInd).getLower() != null) {
                        removeIndicator(((BollingerBand) cpInd).getLower());
                    }
                }
            }

            removeHighlightedSource(cpInd);

            reConstructGraphStore();
            refreshMinMaxFactorVars();
            isBusyNow = false;
        }
        graph.zSlider.setZoomAndPos(z, p);
        setZoomAndPos(z, p);
        graph.reDrawAll();

        //todo: need to add listener
        refreshDataWindow();
    }

    private void removeHighlightedSource(ChartProperties cp) {

        for (int i = 0; i < highLightedSources.size(); i++) {
            HighlightedArea cpHA = highLightedSources.get(i);
            ChartProperties cpTop = cpHA.getTopCP();
            ChartProperties cpBottom = cpHA.getBottomCP();

            if (cp == cpTop || cp == cpBottom) { //removes if one of the bands are deleted(upper OR lower)
                highLightedSources.remove(i);
                break;
            }
        }
    }

    private void removeColor(Color c) {
        try {
            for (int i = graphColors.size() - 1; i >= 0; i--) {
                if (((Color) graphColors.get(i)).equals(c)) {
                    graphColors.remove(i);
                }
            }
        } catch (Exception ex) {
        }
    }

    private void removeRelatedObjects(ChartProperties cp) {
        if (ObjectArray != null) {
            for (int i = ObjectArray.size() - 1; i >= 0; i--) {
                AbstractObject oR = (AbstractObject) ObjectArray.get(i);
                if (oR.getTarget() == cp) {
                    removeFromObjectArray(oR);
                }
            }
        }
        if (StaticObjectArray != null) {
            for (int i = StaticObjectArray.size() - 1; i >= 0; i--) {
                AbstractObject oR = (AbstractObject) StaticObjectArray.get(i);
                if (oR.getTarget() == cp) {
                    removeFromStaticObjectArray(oR);
                }
            }
        }
    }

    public void removePanel(Rectangle r) {
        boolean dirty = false;
        synchronized (graphLock) {
            isBusyNow = true;
            for (int i = Sources.size() - 1; i >= 0; i--) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp.getRect() == r) {
                    dirty = true;
                    if (cp instanceof Indicator) {
                        removeIndicatorTree(cp);
                        Sources.remove(i);
                    } else {
                        Sources.remove(i);
                        removeColor(cp.getColor());
                    }
                }
            }
            for (int i = ObjectArray.size() - 1; i >= 0; i--) {
                AbstractObject oR = (AbstractObject) ObjectArray.get(i);
                if (oR.getRect() == r) {
                    ObjectArray.remove(i);
                    oR = null;
                }
            }
            for (int i = StaticObjectArray.size() - 1; i >= 0; i--) {
                AbstractObject oR = (AbstractObject) StaticObjectArray.get(i);
                if (oR.getRect() == r) {
                    StaticObjectArray.remove(i);
                    oR = null;
                }
            }
            if (dirty) {
                reConstructGraphStore();
                refreshMinMaxFactorVars();
            }
            isBusyNow = false;
        }
        //todo: need to add listener
        refreshDataWindow();
    }

    private void removeIndicatorTree(ChartProperties cp) {
        for (int i = Sources.size() - 1; i >= 0; i--) {
            ChartProperties superInd = (ChartProperties) Sources.get(i);
            if ((superInd instanceof Indicator) && (((Indicator) superInd).getInnerSource() == cp)) {
                removeIndicatorTree(superInd);
                removeRelatedObjects(superInd);
                if (cp.getRect() == superInd.getRect()) {
                    Sources.remove(superInd);
                } else {
                    removeIndicator(superInd);
                }
            }
        }
    }

    public void reConstructGraphStore() {
        synchronized (graphLock) {
            boolean tmpIsBusy = isBusyNow;
            isBusyNow = true;
            clearEntireGraphStore();
            sourceCount = 0;
            if ((Sources != null) && (Sources.size() > 0))
                for (int i = 0; i < Sources.size(); i++) {
                    ChartProperties cp = (ChartProperties) Sources.get(i);

                    sourceCount++;
                    insertDataSource(cp);
                }
            isBusyNow = tmpIsBusy;
        }
    }

    public void resetGraphStore() {
        synchronized (graphLock) {
            isBusyNow = true;
            clearEntireGraphStore();
            sourceCount = 0;
            if ((Sources != null) && (Sources.size() > 0))
                for (int i = 0; i < Sources.size(); i++) {
                    ChartProperties cp = (ChartProperties) Sources.get(i);
                    cp.setRecordSize(0);
                    sourceCount++;
                    //cp.getDataSource().clear();
                    insertDataSource(cp);
                }
            isBusyNow = false;
        }
    }

    public void reloadGraphStoreData() {
        synchronized (graphLock) {
            isBusyNow = true;
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                cp.setRecordSize(0);
            }
            isBusyNow = false;
        }
    }

    //this only clears the current data for the perticular symbol
    //must be used before repopulating when split table changed
    public void clearSymbolData(int j) {
        j = j + 1;
        int lastIndex = graphStore.size() - 1;
        for (int i = lastIndex; i >= 0; i--) {
            Object[] objArr = (Object[]) graphStore.get(i);
            if ((objArr != null) && (objArr.length > j)) {
                objArr[j] = null;
            }
        }
    }

    public void clearEntireGraphStore() {
        if ((graphStore != null) && (graphStore.size() > 0)) {
            int lastIndex = graphStore.size() - 1;
            for (int i = lastIndex; i >= 0; i--) {
                Object[] objArr = (Object[]) graphStore.get(i);
                if ((objArr != null) && (objArr.length > 0)) {
                    for (int j = 0; j < objArr.length; j++) {
                        objArr[j] = null;
                    }
                }
                graphStore.remove(i);
                objArr = null;
            }
            graphStore.trimToSize();
        }
    }

    public void resetEntireGraphStore() {
        Sources.clear();
        ObjectArray.clear();
        StaticObjectArray.clear();
        sourceCount = 0;
        clearEntireGraphStore();
    }

    public long getCurrentBeginTime(long aTime) {
        GregorianCalendar calendar = new GregorianCalendar();
        if (graph.isCurrentMode()) {
            aTime = aTime / (interval * 1000);
            return (aTime) * interval * 1000;
        } else {
            if (interval == StockGraph.MONTHLY) { // monthly
                calendar.setTimeInMillis(aTime);
                int days = calendar.get(Calendar.DAY_OF_MONTH); //-1
                calendar.roll(Calendar.DAY_OF_MONTH, -days);
                long lastDayOfMonth = calendar.getTimeInMillis();
                lastDayOfMonth -= lastDayOfMonth % Miliseconds_Per_Day;
                return lastDayOfMonth;
            } else if (interval == StockGraph.WEEKLY) {  // weekly
                aTime = (aTime - graph.getBeginDayOfWeekAdj() * 1000) / (interval * 1000);
                long result = (aTime) * interval * 1000 + graph.getBeginDayOfWeekAdj() * 1000 + 6L * 24L * 3600L * 1000L;
                return result;
            } else if(interval == StockGraph.YEARLY){ // This else-if block added to rectify issue in calculating one year period
                calendar.setTimeInMillis(aTime);      //Used in Yearly interval
                int year = calendar.get(GregorianCalendar.YEAR);
                calendar.set(--year, GregorianCalendar.DECEMBER, 31);
                return calendar.getTimeInMillis();
            } else { // daily
                aTime = aTime / (interval * 1000);
                return (aTime) * interval * 1000L;
            }
        }
    }

    //dynamic update
    public void updateDataSources() {
        boolean dirty = false;
        synchronized (graphLock) {
            dirty = updateCurrentPrice();
            int dsCount = Sources.size();
            if (dsCount > 0)
                for (int i = 0; i < dsCount; i++) {
                    ChartProperties cp = (ChartProperties) Sources.get(i);
                    int oldSize = cp.getRecordSize();
                    int newSize = cp.getDataSource().size();
                    if (oldSize < newSize) {
                        dirty = true;
                        boolean tmpIsBusy = isBusyNow;
                        isBusyNow = true;
                        if (cp.getID() > ID_COMPARE) {
                            updateIndicatorRecords(i, ((Indicator) cp).getInnerSourceIndex(Sources));
                        } else {
                            // uditha 29 / Jan / 2004
                            updateSymbolRecords(i, cp, 0, newSize);
                            //updateSymbolRecords(i, cp.getDataSource(), oldSize, newSize);
                        }
                        cp.setRecordSize(newSize);
                        isBusyNow = tmpIsBusy;
                        if (//((oldSize+1<newSize)&&(cp.getID()==ID_BASE))||
                                (graph.zSlider.getZoom() >= 99.5f) ||
                                        ((newSize == oldSize + 1) && (endIndex >= graphStore.size() - 2))) {
                            setEndIndex(graphStore.size() - 1);
                        }

                        updateBaseSymbolRecords(oldSize, newSize);

                    } else if (oldSize > newSize) {
                        resetGraphStore();
                    }
                    // functionality of the "else block" is moved to method updateCurrentPrice() (called on 1st line)
                    /*else{
                        //lastPriceUpdateTime is added to reduce the frequency of current price update
                        //constant PRICE_UPDATE_MINIMUM_WAIT can be used to chnage the frequency
                        long timeNow = System.currentTimeMillis();
                        if (lastPriceUpdateTime+PRICE_UPDATE_MINIMUM_WAIT<timeNow){
                            lastPriceUpdateTime = timeNow;
                            dirty = true;
                            boolean tmpIsBusy = isBusyNow;
                            isBusyNow = true;
                            if(cp.getID()>ID_COMPARE){
                                if (cp.getID()==ID_VOLUME){
                                    updateCurrentPriceForIndicatorRecords(i,
                                            ((Indicator)cp).getInnerSourceIndex(Sources), cp.getSymbol());
                                }
                            }
                            else{
                                updateCurrntPriceForHistoryChart(i, cp.getSymbol());
                            }
                            isBusyNow = tmpIsBusy;
                        }
                        // Added by uditha 14 Feb 2004 (Valentines Day Special)
                        if (graph.zSlider.getZoom()>=99.5f){
                            setEndIndex(graphStore.size()-1);
                        }
                    }*/
                }
            if (dirty || isIntradayDirty) {
                insertDummyPoints(tmpLatestTime);
                setPeriodBeginIndex(graph.getPeriod(), false);

                ChartProperties baseCp = getBaseCP();
                if (lastPriceLine != null) {
                    double[] yArr = lastPriceLine.getYArr();
                    if (showCurrentPriceLine && baseCp.getDataSource().size() > 0) {
                        yArr[0] = ((IntraDayOHLC) baseCp.getDataSource().get(baseCp.getDataSource().size() - 1)).getClose();
                    } else {
                        yArr[0] = HORIZ_LINE_INITIAL_VALUE;
                    }
                }

                if (previousCloseLine != null) {
                    double[] yArr = previousCloseLine.getYArr();
                    if (showPreviousCloseLine && baseCp.getDataSource().size() > 0) {
                        yArr[0] = ChartInterface.getPreviousCloseValue(baseCp);
                    } else {
                        yArr[0] = HORIZ_LINE_INITIAL_VALUE;
                    }
                }

                //updating 7 resistance lines
                if (resistanceLines != null) {
                    double[][] resDYArr = new double[NO_OF_RESITANCE_LINES][0];
                    for (int i = 0; i < NO_OF_RESITANCE_LINES; i++) {
                        resDYArr[i] = resistanceLines[i].getYArr();
                        if (resistanceLines[i] != null) {
                            if (showResistanceLine && baseCp.getDataSource().size() > 0) {
                                IntraDayOHLC intOHlc = ((IntraDayOHLC) baseCp.getDataSource().get(baseCp.getDataSource().size() - 1));

                                double pivotPoint = (intOHlc.getClose() + intOHlc.getHigh() + intOHlc.getLow()) / 3;
                                switch (i) {

                                    case 0:  //pivot =  PP = (High + Low + Close)/3
                                        resDYArr[i][0] = (intOHlc.getClose() + intOHlc.getHigh() + intOHlc.getLow()) / 3;
                                        break;
                                    case 1:  //R1 = (2 x PP) � Low
                                        resDYArr[i][0] = (2 * pivotPoint) - intOHlc.getLow();
                                        break;
                                    case 2:  //S1 = (2 x PP) � High
                                        resDYArr[i][0] = (2 * pivotPoint) - intOHlc.getHigh();
                                        break;
                                    case 3:  //R2 = PP + (High � Low)
                                        resDYArr[i][0] = pivotPoint + (intOHlc.getHigh() - intOHlc.getLow());
                                        break;
                                    case 4: //S2 = PP � (High � Low)
                                        resDYArr[i][0] = pivotPoint - (intOHlc.getHigh() - intOHlc.getLow());
                                        break;
                                    case 5://R3 = High + 2 x (PP � Low)
                                        resDYArr[i][0] = intOHlc.getHigh() + 2 * (pivotPoint - intOHlc.getLow());
                                        break;
                                    case 6://S3 = Low � 2 x (High � PP)
                                        resDYArr[i][0] = intOHlc.getLow() - (2 * (intOHlc.getHigh() - pivotPoint));
                                        break;
                                    default:
                                        resDYArr[i][0] = HORIZ_LINE_INITIAL_VALUE;
                                        break;

                                }
                            } else {
                                resDYArr[i][0] = HORIZ_LINE_INITIAL_VALUE;
                            }
                        }

                    }

                }
                //End of updating 7 resistance lines

                resetSlider();
                reCalculateGraphParams();
                checkAndFireAlarms();
                graph.setImageWdHt(); //TODO
                graph.repaint();
            }

        }
    }

    private void checkAndFireAlarms() {

        ChartProperties cp = getBaseCP();

        if (getLastIndexForAlarm(0) <= 0) return;
        Object[] newSinglePoint = (Object[]) graphStore.get(getLastIndexForAlarm(0));
        Object[] oldSinglePoint = (Object[]) graphStore.get(getLastIndexForAlarm(0) - 1);
        long newTime = (Long) newSinglePoint[0];
        ChartPoint newPoint = (ChartPoint) newSinglePoint[1];
        long oldTime = (Long) oldSinglePoint[0];
        ChartPoint oldPoint = (ChartPoint) oldSinglePoint[1];

        if (newPoint == null) return;

        long[] xArr = {oldTime, newTime};//long time
        float[] xIndexArr1 = convertXArrayTimeToIndex(xArr);

        double[] yArr = null;
        yArr = new double[]{oldPoint.getValue(cp.getOHLCPriority()), newPoint.getValue(cp.getOHLCPriority())};

        int[] yPixelArr1 = convertYValueArrayToPixelArr(yArr, getIndexOfTheRect(graph.panels, cp.getRect()));
        int[] xPixelArr1 = {getPixelFortheIndex(xIndexArr1[0]), getPixelFortheIndex(xIndexArr1[1])};

        Line2D priceTrendLine = new Line2D.Double(new Point(xPixelArr1[0], yPixelArr1[0]), new Point(xPixelArr1[1], yPixelArr1[1]));

        TWDecimalFormat formatter = new TWDecimalFormat("#############.00");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date lineBreakDate = new Date(newTime);
        Date lineStart = new Date(oldTime);

        ArrayList<AbstractObject> objects = this.getObjArray();
        for (AbstractObject obj : objects) {
            if ((obj instanceof Alarmable) && ((Alarmable) obj).isAlarmEnabled()) {
                Alarmable line = (Alarmable) obj;
                if (line.isIntesectingLine(priceTrendLine)) {
                    final String msgPrvs = line.getAlarmMessage() + "\nSymbol : " + StockGraph.extractSymbolFromStr(cp.getSymbol()) +
                            "\nPrice : " + formatter.format(yArr[0]) + "\nTime : " + dateFormatter.format(lineBreakDate);
                    line.setAlarmEnabled(false);
                    showAlertTriggeredMsg(msgPrvs);
                }
            }
        }
    }

    public Object[] getPrePeriodRecord() {
        return prePeriodRecord;
    }

    public void setPrePeriodRecord(Object[] prePeriodRecord) {
        this.prePeriodRecord = prePeriodRecord;
    }

    /**
     * ****************editing sathyajith****************
     */
    public void updateBaseSymbolRecords(int oldSize, int newSize) {

        try {
            if (tableMode) {
                //added by shash for datawindow
                baseSymbolPoints.clear();
                int bIndex = Math.round(Math.max(beginIndex, 0));
                int eIndex = Math.round(Math.min(endIndex, graphStore.size() - 1));
                for (int i = bIndex; i <= eIndex; i++) {
                    Object[] oa = (Object[]) graphStore.get(i);
                    if (oa[1] != null) {
                        baseSymbolPoints.insert(oa);
                    }
                }

                //to calcualte net change and % change of last point    //added by sathyajith
                if (graphStore.size() > 0 && bIndex == 0) {  //base symbol begin index
                    prePeriodRecord = (Object[]) graphStore.get(bIndex);
                } else if (bIndex > 0 && graphStore.size() > (bIndex - 1)) {  //>0
                    prePeriodRecord = (Object[]) graphStore.get(bIndex - 1);
                }
            } else {
                baseSymbolPoints.clear();
            }
        } catch (Exception ex) {
            System.out.println(" ==========  updateBaseSymbolRecords(int oldSize, int newSize) ======= ");
        }
    }

    /**
     * ***********************************************
     */
    public ChartPoint getIndicatorPoint(long timeIndex, int graphIndex, byte indicatorID) {
        // here INDICATOR_RECORD_COUNT is hard coded to 0
        // but it can be changed depending on indicatorID
        Object[] singlePoint;
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        graphIndex = graphIndex + 1;
        longArr[0] = new Long(timeIndex);
        searchIndex = graphStore.indexOfContainingValue(longArr);
        if (searchIndex >= 0) {
            singlePoint = (Object[]) (graphStore.get(searchIndex));
            if (singlePoint.length > graphIndex) {
                if (singlePoint[graphIndex] == null) {
                    ChartPoint fa = new ChartPoint();
                    fa.setValue(GraphDataManager.INDICATOR, 0d);
                    singlePoint[graphIndex] = fa;
                }
                return (ChartPoint) singlePoint[graphIndex];
            } else {
                Object[] arrObjNew = new Object[sourceCount + 1];
                for (int k = 0; k < singlePoint.length; k++) {
                    arrObjNew[k] = singlePoint[k];
                    singlePoint[k] = null;
                }
                singlePoint = null;
                ChartPoint fa = new ChartPoint();
                fa.setValue(GraphDataManager.INDICATOR, 0d);
                arrObjNew[graphIndex] = fa;
                graphStore.set(searchIndex, arrObjNew);
                return (ChartPoint) arrObjNew[graphIndex];
            }
        } else {
            return null;
        }
    }

    public void removeIndicatorPoint(long timeIndex, int graphIndex, byte indicatorID) {
        // here INDICATOR_RECORD_COUNT is hard coded to 0
        // but it can be changed depending on indicatorID
        Object[] singlePoint;
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        graphIndex = graphIndex + 1;
        longArr[0] = new Long(timeIndex);
        searchIndex = graphStore.indexOfContainingValue(longArr);
        if (searchIndex >= 0) {
            singlePoint = (Object[]) (graphStore.get(searchIndex));
            if (singlePoint.length > graphIndex) {
                singlePoint[graphIndex] = null;
            }
        }
    }

    public void insertDummyPoints(long timeIndex) {
        int searchIndex = 0;
        Object[] longArr = new Object[1];
        float tmpZoom = graph.zSlider.getZoom();
        if (timeIndex == 0) return;
        removeDummyPoints(latestTime);
        latestTime = timeIndex;
        int count = (graph.isCurrentMode()) ? rMarginIntraday : rMarginWidth;
        for (int i = 0; i < count + 1; i++) {
            longArr[0] = new Long(timeIndex);
            searchIndex = graphStore.indexOfContainingValue(longArr);
            if (searchIndex < 0) {
                Object[] arrObjNew = new Object[sourceCount + 1];
                arrObjNew[0] = new Long(timeIndex);
                graphStore.add(-searchIndex - 1, arrObjNew);
            }
            timeIndex += interval * 1000;
            //System.out.println("inerval: " + interval);
        }
        //revealing newly added points
        if (tmpZoom >= 99.5f) {
            setEndIndex(graphStore.size() - 1);
        }
    }

    public void removeDummyPoints(long timeIndex) {
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        if (timeIndex == 0) return;
        int count = (graph.isCurrentMode()) ? rMarginIntraday : rMarginWidth;
        for (int i = 0; i < count + 1; i++) {
            longArr[0] = new Long(timeIndex);
            searchIndex = graphStore.indexOfContainingValue(longArr);
            if ((searchIndex >= 0) && (isPointEmpty(searchIndex))) {
                graphStore.remove(searchIndex);
            }
            timeIndex += interval * 1000;
        }
    }

    public boolean isPointEmpty(int Index) {
        Object[] singlePoint;
        singlePoint = (Object[]) (graphStore.get(Index));
        for (int j = 1; j < singlePoint.length; j++) {
            if (singlePoint[j] != null) {
                return false;
            }
        }
        return true;
    }

    private ChartPoint getChartPoint(long timeIndex, int graphIndex) {
        Object[] singlePoint;
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        graphIndex = graphIndex + 1;
        longArr[0] = new Long(timeIndex);
        searchIndex = graphStore.indexOfContainingValue(longArr);
        if (searchIndex >= 0) {
            singlePoint = (Object[]) (graphStore.get(searchIndex));
            if (singlePoint.length > graphIndex) {
                if (singlePoint[graphIndex] == null) {
                    ChartPoint point = new ChartPoint();
                    point.Open = 0;
                    point.High = 0;
                    point.Low = Float.MAX_VALUE;
                    point.Close = 0;
                    point.Volume = 0;
                    point.TurnOver = 0;
                    singlePoint[graphIndex] = point;
                }
                return (ChartPoint) singlePoint[graphIndex];
            } else {
                Object[] arrObjNew = new Object[sourceCount + 1];
                for (int k = 0; k < singlePoint.length; k++) {
                    arrObjNew[k] = singlePoint[k];
                    singlePoint[k] = null;
                }
                singlePoint = null;
                ChartPoint point = new ChartPoint();
                point.Open = 0;
                point.High = 0;
                point.Low = Float.MAX_VALUE;
                point.Close = 0;
                point.Volume = 0;
                point.TurnOver = 0;
                arrObjNew[graphIndex] = point;
                graphStore.set(searchIndex, arrObjNew);
                return (ChartPoint) arrObjNew[graphIndex];
            }
        } else {
            Object[] arrObjNew = new Object[sourceCount + 1];
            ChartPoint point = new ChartPoint();
            point.Open = 0;
            point.High = 0;
            point.Low = Float.MAX_VALUE;
            point.Close = 0;
            point.Volume = 0;
            point.TurnOver = 0;
            arrObjNew[0] = new Long(timeIndex);
            arrObjNew[graphIndex] = point;
            graphStore.add(-searchIndex - 1, arrObjNew);
            return (ChartPoint) arrObjNew[graphIndex];
        }
    }

    public ChartRecord getChartRecordForTable(int index, int graphIndex) {
        Object[] singlePoint;
        ChartRecord chartRecord = null;
        graphIndex = graphIndex + 1;
        try {
            singlePoint = (Object[]) (baseSymbolPoints.get(index));
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
        if (singlePoint.length > graphIndex) {
            if (singlePoint[graphIndex] != null) {
                //chartRecord = new ChartRecord((ChartPoint) singlePoint[graphIndex], getTimeMillisec(index));
                //changed by sathyajith to make DataMode compatible with time range of chartView
                chartRecord = new ChartRecord((ChartPoint) singlePoint[graphIndex], (Long) singlePoint[0]);
            }
            return chartRecord;
        } /*else {  //TODO
            Object[] arrObjNew = new Object[sourceCount+1];
            for(int k=0; k<singlePoint.length; k++){
                arrObjNew[k] = singlePoint[k];
                singlePoint[k] = null;
            }
            singlePoint = null;
            ChartPoint point = new ChartPoint();
            point.Open = 0;
            point.High = 0;
            point.Low = Float.MAX_VALUE;
            point.Close = 0;
            point.Volume = 0;
            arrObjNew[graphIndex] = point;
            graphStore.set(index, arrObjNew);
            ChartRecord chartRecord = new ChartRecord((ChartPoint)arrObjNew[graphIndex], getTimeMillisec(index));
            return chartRecord;
        }*/
        return chartRecord;
    }

    public int getLastNotNullIndexOfGraphStore() {
        int index = getGraphStoreSize();
        for (int i = getGraphStoreSize() - 1; i >= 0; i--) {
            Object[] singlePoint = (Object[]) (graphStore.get(i));
            if (singlePoint[1] != null) { //for base graph
                return i;
            }
        }
        return index;
    }

    //coz..base symbol graphpoint may different from the indicator lastpoint
    //or can grossly use base char
    public int getLastNotNullIndexOfIndicatorInGraphStore(int graphIndex) {
        int index = 0;
        try {
            index = getGraphStoreSize();
            for (int i = getGraphStoreSize() - 1; i >= 0; i--) {
                Object[] singlePoint = (Object[]) (graphStore.get(i));

                if ((singlePoint != null) && singlePoint[graphIndex] != null) { //for base graph
                    return i;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return index;
    }

    public int getLastNotNullIndexOfBaseSymbolPoints() {
        int index = baseSymbolPoints.size();
        for (int i = index - 1; i >= 0; i--) {
            Object[] singlePoint = (Object[]) (baseSymbolPoints.get(i));

            if (singlePoint[1] != null) { //for base graph
                return i;
            }
        }
        //  return index - 1 ;  // doesn't return = all values are null  = -1
        return -1;  // doesn't return = all values are null  = -1 then not showing the values - sathyajith
    }

    public ChartPoint getChartPointForDataWindow(int timeIndex, int graphIndex) {
        ChartPoint chartPoint = null;
        graphIndex = graphIndex + 1;
        Object[] singlePoint = new Object[0];
        try {
            singlePoint = (Object[]) (graphStore.get(timeIndex));
        } catch (IndexOutOfBoundsException e) {
            //Ignore
        }
        if (singlePoint.length > graphIndex && singlePoint[graphIndex] != null) {
            chartPoint = (ChartPoint) singlePoint[graphIndex];
        }
        return chartPoint;
    }

    public ChartPoint readChartPoint(long timeIndex, int graphIndex) {
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        longArr[0] = new Long(timeIndex);
        searchIndex = graphStore.indexOfContainingValue(longArr);
        longArr[0] = null;
        return readChartPointAtIndex(searchIndex, graphIndex);
    }

    private ChartPoint readChartPointAtIndex(int index, int graphIndex) {
        Object[] singlePoint;
        graphIndex = graphIndex + 1;
        if (index >= 0 && index < graphStore.size()) {
            singlePoint = (Object[]) (graphStore.get(index));
            if (singlePoint.length > graphIndex) {
                if (singlePoint[graphIndex] != null)
                    return (ChartPoint) singlePoint[graphIndex];
            }
        }
        return null;
    }

    private void updateIndicatorRecords(int index, int sourceIndex) {
        ChartProperties cp = (ChartProperties) Sources.get(index);

        try {

            /*****************internal sources  - sources which are in the graphstore **********************************************/
            if ((cp.getID() == GraphDataManager.ID_NET_ORDERS) || (cp.getID() == GraphDataManager.ID_NET_TURNOVER)) {   //check whether internal or extennal sources
                if (cp instanceof Indicator) {
                    ((Indicator) cp).insertIndicatorToGraphStore(ChartInterface.getCashFlowHistoryDetailArr(), this, index);
                }
            } else {

                ArrayList<ChartRecord> al = new ArrayList<ChartRecord>();
                long timeInd;
                ChartPoint fa;

                int baseIndex = getBaseGraphIndex();
                for (int i = 0; i < graphStore.size(); i++) {
                    timeInd = getTimeMillisec(i);
                    fa = readChartPoint(timeInd, sourceIndex);
                    if (fa != null) {
                        ChartRecord cr = new ChartRecord(fa, timeInd);
                        if (cp.getID() == GraphDataManager.ID_MOVING_AVERAGE) {
                            cr.Volume = readChartPoint(timeInd, baseIndex).Volume;
                        }

                        al.add(cr);
                    }
                }

                // update graphStore
                if (cp instanceof Indicator) {
                    ((Indicator) cp).insertIndicatorToGraphStore(al, this, index);
                }

                al.clear();
                al = null;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateSymbolRecords(int index, ChartProperties cp, int oldSize, int newSize) {

        ArrayList<StockFactorType> alStockfactors = new ArrayList<StockFactorType>();
        float currFactor = -1;
        int type = ChartSplitPoints.TYPE_NONE;
        ArrayList sourceList = cp.getDataSource();
        long currentBeginTime;
        long lastCurrentBeginTime = 0;
        long tmpTime;

        try {
            if (adjustMethod == METHOD_UNADJUSTED) {  //for unadjusted graph

                for (int i = newSize - 1; i >= oldSize; i--) {
                    Object tmpO = sourceList.get(i);
                    if ((tmpO != null) && (tmpO instanceof IntraDayOHLC)) {
                        IntraDayOHLC dR = (IntraDayOHLC) tmpO;
                        if (dR.getTime() == 0) {
                            continue;
                        } else {
                            currFactor = -1;
                            type = -1;
                            tmpTime = dR.getTime() * 60000L;
                            tmpTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, tmpTime, graph.isCurrentMode());
                            currentBeginTime = getCurrentBeginTime(tmpTime);
                            ChartPoint aPoint = getChartPoint(currentBeginTime, index);

                            if (tmpLatestTime < currentBeginTime) {
                                tmpLatestTime = currentBeginTime;
                            }

                            if (alStockfactors.size() < 1) {  //if all the split types are disabled do this
                                aPoint.Open = dR.getOpen();
                                if (aPoint.Close == 0) aPoint.Close = dR.getClose();
//                                if (aPoint.High < dR.getHigh()) aPoint.High = dR.getHigh();
//                                if (aPoint.Low > dR.getLow()) aPoint.Low = dR.getLow();

                                /**
                                 * Above codes were commented off on 20/04/2015 to fix "N/A" values appearing in
                                 * High and Low values of "Unadjusted graph" table data. Modified code appears
                                 * below that captures infinity and minus values for chart points.
                                 * **/

                                if(aPoint.High == Double.POSITIVE_INFINITY){
                                    aPoint.High = dR.getHigh();
                                }else if(aPoint.High < dR.getHigh()){
                                    aPoint.High = dR.getHigh();
                                }

                                if(aPoint.Low == -1){
                                    aPoint.Low = dR.getLow();
                                }else if(aPoint.Low > dR.getLow()){
                                    aPoint.Low = dR.getLow();
                                }

                                /**
                                 * End Modification
                                 */

                                if (currentBeginTime != lastCurrentBeginTime) {
                                    aPoint.Volume = dR.getVolume();
                                    aPoint.TurnOver = dR.getTurnOver();
                                    //lastCurrentBeginTime = currentBeginTime; //corrected for accumilating volume
                                } else {
                                    aPoint.Volume += dR.getVolume();
                                    aPoint.TurnOver += dR.getTurnOver();
                                }
                            }

                            //make a temporoty vaiable for adjusment in big intervals
                            double bigIntervalAdjVol = 0d;

                            //********************** go through factor order ************************/
                            for (int j = 0; j < alStockfactors.size(); j++) {
                                StockFactorType sFT = alStockfactors.get(j);
                                if (j == 0) {
                                    if (sFT.type == ChartSplitPoints.TYPE_CASH_DIVIDEND) {
                                        aPoint.Close = dR.getClose() + sFT.factor;
                                        aPoint.Open = dR.getOpen() + sFT.factor;
                                        aPoint.High = dR.getHigh() + sFT.factor;
                                        aPoint.Low = dR.getLow() + sFT.factor;
                                        //corrected for accumilating volume and turnover
                                        if (currentBeginTime != lastCurrentBeginTime) {
                                            aPoint.Volume = dR.getVolume();
                                        } else {
                                            bigIntervalAdjVol = dR.getVolume();
                                        }
                                    } else {
                                        aPoint.Close = dR.getClose() * sFT.factor;
                                        aPoint.Open = dR.getOpen() * sFT.factor;
                                        aPoint.High = dR.getHigh() * sFT.factor;
                                        aPoint.Low = dR.getLow() * sFT.factor;
                                        //corrected for accumilating volume and turnover
                                        if (currentBeginTime != lastCurrentBeginTime) {
                                            aPoint.Volume = Math.round(dR.getVolume() / sFT.factor);
                                            //aPoint.TurnOver = dR.getTurnOver();
                                        } else { //
                                            bigIntervalAdjVol = Math.round(dR.getVolume() / sFT.factor);
                                        }
                                    }
                                } else { //j!=0
                                    if (sFT.type == ChartSplitPoints.TYPE_CASH_DIVIDEND) {
                                        aPoint.Close = aPoint.Close + sFT.factor;
                                        aPoint.Open = aPoint.Open + sFT.factor;
                                        aPoint.High = aPoint.High + sFT.factor;
                                        aPoint.Low = aPoint.Low + sFT.factor;
                                        //no correction for volume and turnover here

                                    } else {                //ss etc
                                        aPoint.Close = aPoint.Close * sFT.factor;
                                        aPoint.Open = aPoint.Open * sFT.factor;
                                        aPoint.High = aPoint.High * sFT.factor;
                                        aPoint.Low = aPoint.Low * sFT.factor;
                                        if (currentBeginTime != lastCurrentBeginTime) {
                                            aPoint.Volume = Math.round(aPoint.Volume / sFT.factor);
                                        } else {
                                            bigIntervalAdjVol = Math.round(bigIntervalAdjVol / sFT.factor);
                                        }
                                    }
                                }
                            }

                            //when there are additonal interals (5 min, month etc)
                            if (currentBeginTime != lastCurrentBeginTime) {
                                aPoint.TurnOver = dR.getTurnOver();
                            } else {
                                aPoint.Volume += bigIntervalAdjVol;
                                //aPoint.TurnOver += aPoint.TurnOver;
                                aPoint.TurnOver += dR.getTurnOver();
                            }

                            //************************ end of go through factor order ******************/

                            /******** End Do the SS,CD ,RI adjustments according to the order ******/
                            if (graph.isCurrentMode() && interval == StockGraph.EVERYMINUTE && currFactor != -1) {
                                aPoint.Open = dR.getOpen() * currFactor;
                                aPoint.High = dR.getHigh() * currFactor;
                                aPoint.Low = dR.getLow() * currFactor;
                            }

                            //time adjustments
                            if (currentBeginTime != lastCurrentBeginTime) {
                                lastCurrentBeginTime = currentBeginTime;
                            }

                            ArrayList<SplitPointRecord> splitRecords = cp.getSplitPointRecrod(tmpTime);
                            int count = splitRecords.size();
                            //if (count > 0) System.out.println("******* count ********** " + count);

                            /********Do the SS,CD ,RI adjustments according to the order ******/

                            for (int m = 0; m < count; m++) {

                                SplitPointRecord point2 = splitRecords.get(m);
                                currFactor = point2.factor;
                                type = point2.type;

                                /* if (type == ChartSplitPoints.TYPE_STOCK_DIVIDEND && !adjustTypes[0]) {
                                    System.out.println(" SD factor : " + currFactor);
                                } else if (type == ChartSplitPoints.TYPE_CASH_DIVIDEND && !adjustTypes[1]) {
                                    System.out.println(" CD factor : " + currFactor);
                                } else if (type == ChartSplitPoints.TYPE_RIGHT_ISSUE && !adjustTypes[2]) {
                                    System.out.println(" RI factor : " + currFactor);
                                } else if (type == ChartSplitPoints.TYPE_STOCK_SPLIT && !adjustTypes[3]) {
                                    System.out.println(" SS factor : " + currFactor);
                                }*/

                                //add only if the perticular factor is enabled
                                if (type > ChartSplitPoints.TYPE_NONE) {
                                    if (type == ChartSplitPoints.TYPE_STOCK_DIVIDEND && !adjustTypes[0] ||
                                            (type == ChartSplitPoints.TYPE_CASH_DIVIDEND && !adjustTypes[1]) ||
                                            (type == ChartSplitPoints.TYPE_RIGHT_ISSUE && !adjustTypes[2]) ||
                                            (type == ChartSplitPoints.TYPE_STOCK_SPLIT && !adjustTypes[3])) {

                                        alStockfactors.add(new StockFactorType(type, currFactor));
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (adjustMethod == METHOD_NONE) {

                for (int i = oldSize; i < newSize; i++) {
                    Object tmpO = sourceList.get(i);
                    if ((tmpO != null) && (tmpO instanceof IntraDayOHLC)) {
                        IntraDayOHLC dR = (IntraDayOHLC) tmpO;
                        if (dR.getTime() == 0) {
                            continue;
                        } else {

                            tmpTime = dR.getTime() * 60000L;
                            tmpTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, tmpTime, graph.isCurrentMode());

                            if(interval != StockGraph.YEARLY){
                                currentBeginTime = getCurrentBeginTime(tmpTime);
                                if (tmpLatestTime < currentBeginTime) {
                                    tmpLatestTime = currentBeginTime;
                                }

                                ChartPoint aPoint = getChartPoint(currentBeginTime, index);
                                if (aPoint.Open == 0) aPoint.Open = dR.getOpen();
                                if (aPoint.High < dR.getHigh()) aPoint.High = dR.getHigh();
                                if (aPoint.Low > dR.getLow()) aPoint.Low = dR.getLow();
                                if (graph.isCurrentMode() && interval == StockGraph.EVERYMINUTE) {
                                    aPoint.Open = dR.getOpen();
                                    aPoint.High = dR.getHigh();
                                    aPoint.Low = dR.getLow();
                                }
                                if (!isVWAP) {
                                    aPoint.Close = dR.getClose();
                                } else {
                                    aPoint.Close = dR.getVwap();
                                }
                                if (currentBeginTime != lastCurrentBeginTime) {
                                    aPoint.Volume = dR.getVolume();
                                    aPoint.TurnOver = dR.getTurnOver();
                                    lastCurrentBeginTime = currentBeginTime; //corrected for accumilating volume
                                } else {
                                    aPoint.Volume += dR.getVolume();
                                    aPoint.TurnOver += dR.getTurnOver();
                                }
                            }else if(graph.isCurrentMode()==false && interval == StockGraph.YEARLY){//Added to fix issue in history data shown with 1 yea interval
                                currentBeginTime = getCurrentBeginTime(tmpTime);                    // Added 23/04/2015. Separate else-if block added to minimize the
                                if (tmpLatestTime < currentBeginTime) {                             // the impact on existing code
                                    tmpLatestTime = currentBeginTime;
                                }

                                if(!(tmpTime>getYearBeginTime())){
                                    ChartPoint aPoint = getChartPoint(getYearLastTime(tmpTime), index);

                                    if (aPoint.Open == 0) aPoint.Open = dR.getOpen();
                                    if (aPoint.High < dR.getHigh()) aPoint.High = dR.getHigh();
                                    if (aPoint.Low > dR.getLow()) aPoint.Low = dR.getLow();
                                    if (graph.isCurrentMode() && interval == StockGraph.EVERYMINUTE) {
                                        aPoint.Open = dR.getOpen();
                                        aPoint.High = dR.getHigh();
                                        aPoint.Low = dR.getLow();
                                    }
                                    if (!isVWAP) {
                                        aPoint.Close = dR.getClose();
                                    } else {
                                        aPoint.Close = dR.getVwap();
                                    }
                                    if (currentBeginTime != lastCurrentBeginTime) {
                                        aPoint.Volume = dR.getVolume();
                                        aPoint.TurnOver = dR.getTurnOver();
                                        lastCurrentBeginTime = currentBeginTime; //corrected for accumilating volume
                                    } else {
                                        aPoint.Volume += dR.getVolume();
                                        aPoint.TurnOver += dR.getTurnOver();
                                    }
                                }
                            }
                        }//End Else
                    }
                }
            }

            Exchange exg = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(cp.getSymbol()));
            //update last candle
            //if (cp.getID() == ID_BASE && graph.isCurrentMode() && isIntradayDirty && (exg.getMarketStatus() == Meta.MARKET_OPEN || exg.getMarketStatus() == Meta.MARKET_PRECLOSE)) {
            if (cp.getID() == ID_BASE && graph.isCurrentMode() && isIntradayDirty && ChartInterface.isExchangeOpenedOrPreClosed(cp.getSymbol())) {

                updateDynamicLastcandle(index, lastCurrentBeginTime);
                //resetLastCandle();
                isIntradayDirty = false;
            }
            cp.reCalcSplitFactors(cp.getSymbol(), graph.isCurrentMode());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     *  Added on 23/04/2015
     *  Return the December 31 epoch time to accumilate values for that year in charts
     */
    public long getYearLastTime(long aTime) {
        GregorianCalendar calendar = new GregorianCalendar();
        GregorianCalendar cal_Current_Year = new GregorianCalendar();
        calendar.setTimeInMillis(aTime);
        int year = calendar.get(GregorianCalendar.YEAR);
        cal_Current_Year.setTime(new Date());
        int thisYear = cal_Current_Year.get(GregorianCalendar.YEAR);
        int thisMonth = cal_Current_Year.get(GregorianCalendar.MONTH);
        int thisDate = cal_Current_Year.get(GregorianCalendar.DATE);
        if(year != thisYear){
            calendar.set(year, GregorianCalendar.DECEMBER, 31);
            return calendar.getTimeInMillis();
        }else {
            cal_Current_Year.clear();
            cal_Current_Year.set(thisYear, thisMonth, thisDate);
            cal_Current_Year.add(GregorianCalendar.DATE, -1);
            return cal_Current_Year.getTimeInMillis();
        }
    }

    /**
     * Return the beginning epoch time for the given  year
     * Added on 23/04/2015
     */
    public long getYearBeginTime() {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        int year = calendar.get(GregorianCalendar.YEAR);
        calendar.clear();
        calendar.set(year, GregorianCalendar.JANUARY, 1);
        return calendar.getTimeInMillis();
    }

    public void updateChartPointsForSplit(ChartProperties cp, int graphIndex, int endIndex, float factor, int type) {
        ArrayList sourceList = cp.getDataSource();
        long currentBeginTime;
        long tmpTime;
        try {
            for (int i = endIndex; i >= 0; i--) {
                Object tmpO = sourceList.get(i);
                if ((tmpO != null) && (tmpO instanceof IntraDayOHLC)) {
                    IntraDayOHLC dR = (IntraDayOHLC) tmpO;
                    if (dR.getTime() == 0) {
                        continue;
                    } else {
                        tmpTime = dR.getTime() * 60000L;
                        tmpTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, tmpTime, graph.isCurrentMode());

                        currentBeginTime = getCurrentBeginTime(tmpTime);
                        //if (tmpLatestTime < currentBeginTime) tmpLatestTime = currentBeginTime;

                        ChartPoint aPoint = getChartPoint(currentBeginTime, graphIndex);
                        if (aPoint.Close == 0) {
                            aPoint.Close = dR.getClose() * factor;
                        } else {
                            //aPoint.Close = aPoint.Close * factor;
                        }
                        //System.out.println(factor);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void resetLastCandle() {
        dynamicLastCandle.setTime(0);
        dynamicLastCandle.setClose(0);
        dynamicLastCandle.setOpen(0);
        dynamicLastCandle.setLow(0);
        dynamicLastCandle.setHigh(0);
        dynamicLastCandle.setTurnOver(0);
        dynamicLastCandle.setVolume(0);

        //seet the gtrun over and others as wel
        //dynamicCandleLastUpdateTime = timeStockLastTraded;
    }

    private void updateDynamicLastcandle(int index, long lastCurrentBeginTime) {
        long tmpTime;
        long currentBeginTime;
        tmpTime = dynamicLastCandle.getTime() * 60000L;
        tmpTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, tmpTime, graph.isCurrentMode());

        currentBeginTime = getCurrentBeginTime(tmpTime);
        if (tmpLatestTime < currentBeginTime) tmpLatestTime = currentBeginTime;
        ChartPoint aPoint = getChartPoint(currentBeginTime, index);
        if (aPoint.Open == 0) aPoint.Open = dynamicLastCandle.getOpen();
        if (aPoint.High < dynamicLastCandle.getHigh()) aPoint.High = dynamicLastCandle.getHigh();
        if (aPoint.Low > dynamicLastCandle.getLow()) aPoint.Low = dynamicLastCandle.getLow();
        if (!isVWAP) {
            aPoint.Close = dynamicLastCandle.getClose();
        } else {
            aPoint.Close = dynamicLastCandle.getVwap();
        }
        if (currentBeginTime != lastCurrentBeginTime) {
            aPoint.Volume = dynamicLastCandle.getVolume();
            aPoint.TurnOver = dynamicLastCandle.getTurnOver();
            lastCurrentBeginTime = currentBeginTime; //corrected for accumilating volume
        } else {
            aPoint.Volume += dynamicLastCandle.getVolume();
            aPoint.TurnOver += dynamicLastCandle.getTurnOver();

        }

    }

    private boolean updateCurrentPrice() {
        //lastPriceUpdateTime is added to reduce the frequency of current price update
        //constant PRICE_UPDATE_MINIMUM_WAIT can be used to chnage the frequency
        long timeNow = System.currentTimeMillis();

        if ((lastBidAskTagsUpdateTime + PRICE_UPDATE_MINIMUM_WAIT < timeNow) &&
                showBidAskTags && bidAskTags != null && bidAskTags.length > 0) {
            lastBidAskTagsUpdateTime = timeNow;
            removeFromStaticObjectArray(bidAskTags[0]);
            removeFromStaticObjectArray(bidAskTags[1]);
            bidAskTags = ChartInterface.getBidTagAndAskTag(getBaseCP());
            addToStaticObjectArray(bidAskTags[0]);
            addToStaticObjectArray(bidAskTags[1]);
        }

        if ((lastMinMaxUpdateTime + PRICE_UPDATE_MINIMUM_WAIT < timeNow) &&
                showMinMaxPriceLines && minPriceLine != null && maxPriceLine != null) {
            lastMinMaxUpdateTime = timeNow;
            double[] minY = minPriceLine.getYArr();
            double[] maxY = maxPriceLine.getYArr();
            //added to fix compatibility issues
            String key = getBaseCP().getSymbol();
            if (getBaseCP().getSymbol().split("~").length == 2) {
                key += "~0";
            }
            minY[0] = ChartInterface.getStockObject(key).getMinPrice();
            maxY[0] = ChartInterface.getStockObject(key).getMaxPrice();

        }

        /******************intraday last candle  chck whether new stock has come *************/

        if ((graph.isCurrentMode())) { //evert 800 mil sedconds s
            lastPriceUpdateTime = timeNow;
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                //trend line alarm - shashika
                ArrayList<AbstractObject> objects = this.getObjArray();
                ArrayList<Alarmable> alarmLines = new ArrayList<Alarmable>();
                for (AbstractObject obj : objects) {
                    /*if (obj.getObjType() == AbstractObject.INT_LINE_SLOPE && ((ObjectLineSlope) obj).isAlarmEnabled()) {
                        alarmLines.add((ObjectLineSlope)obj);
                    }*/
                    if ((obj instanceof Alarmable) && ((Alarmable) obj).isAlarmEnabled()) {
                        alarmLines.add((Alarmable) obj);
                    }
                }
                //////////////////////////////////
                if (cp.getID() <= GraphDataManager.ID_BASE) { //compare or base symbol records
                    String key = cp.getSymbol();
                    Stock stock = ChartInterface.getStockObject(key);
                    if ((stock != null) && (stock.getLastTradeTime() > 0)) {
                        if ((stock.getTodaysOpen() > 0) && (stock.getLastTradeTime() > 0)) {

                            long timeStockLastTraded = (stock.getLastTradeTime()) / (60 * 1000); // done to round off to nearest miniute
                            //check whether new recors has come
                            ArrayList<IntraDayOHLC> al = cp.getDataSource();
                            if (al.size() > 0) {
                                IntraDayOHLC ohlc = (IntraDayOHLC) al.get(al.size() - 1);

                                if (stock.getLastTradeTime() > ohlc.getTime() * 60000 && stock.getLastTradeTime() > dynamicCandleLastUpdateTime) { //toc ehck whether a new stock

                                    //need to ignore the first time (when dynamicCandleLastUpdateTime = 0)
                                    if ((dynamicCandleLastUpdateTime / 60000L < stock.getLastTradeTime() / 60000L) && dynamicCandleLastUpdateTime != 0) {
                                        dynamicCandleIsFirstMinute = false;
                                    }

                                    isIntradayDirty = true;

                                    if (dynamicCandleIsFirstMinute) {
                                        //-----------------ignoring the first minute---------------------
                                        dynamicLastCandle.setClose((float) stock.getLastTradeValue());
                                        dynamicLastCandle.setOpen(-1.0f);
                                        dynamicLastCandle.setLow(-1.0f);
                                        dynamicLastCandle.setHigh(Float.POSITIVE_INFINITY);
                                        dynamicLastCandle.setTurnOver(-1);
                                        dynamicLastCandle.setVolume(-1);

                                    } else {

                                        long tradedVolume = stock.getVolume() - dynamicCandleStockVolume;
                                        double tradedTurnTurnover = stock.getTurnover() - dynamicCandleStockTurnover;

                                        if (dynamicLastCandle.getTime() == timeStockLastTraded) { //same miniute

                                            dynamicLastCandle.setClose((float) stock.getLastTradeValue());
                                            if (stock.getLastTradeValue() > dynamicLastCandle.getHigh())
                                                dynamicLastCandle.setHigh((float) stock.getLastTradeValue());
                                            if (stock.getLastTradeValue() < dynamicLastCandle.getLow())
                                                dynamicLastCandle.setLow((float) stock.getLastTradeValue());

                                            dynamicLastCandle.setTurnOver((dynamicLastCandle.getTurnOver() + tradedTurnTurnover) / ChartInterface.getTurnoverDivisionFactor());
                                            dynamicLastCandle.setVolume(dynamicLastCandle.getVolume() + tradedVolume);

                                        } else { //another
                                            resetLastCandle();
                                            dynamicLastCandle.setClose((float) stock.getLastTradeValue());
                                            dynamicLastCandle.setOpen((float) stock.getLastTradeValue());
                                            dynamicLastCandle.setLow((float) stock.getLastTradeValue());
                                            dynamicLastCandle.setHigh((float) stock.getLastTradeValue());
                                            dynamicLastCandle.setTurnOver(tradedTurnTurnover / ChartInterface.getTurnoverDivisionFactor());
                                            dynamicLastCandle.setVolume(tradedVolume);
                                        }

                                        //trend line alarm - shashika--------------------------------------------
/*
                                        //previous ohlc point
                                        IntraDayOHLC ohlcPrevious = (IntraDayOHLC) al.get(al.size() - 2);
                                        long ohlcPreviousTime = ohlcPrevious.getTime() * 60000L;
                                        ohlcPreviousTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, ohlcPreviousTime, graph.isCurrentMode());

                                    //ohlc = last point   dynamicLastCandle = new point
                                    long oldTime = ohlc.getTime() * 60000L;
                                    oldTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, oldTime, graph.isCurrentMode());


                                    //long newTime = stock.getLastTradeTime();
                                    long newTime = timeStockLastTraded * 60000L;
                                    newTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, newTime, graph.isCurrentMode());

                                    long lineBreakTime = stock.getLastTradeTime();
                                    lineBreakTime += ChartInterface.getTimeZoneAdjustment(graph.Symbol, newTime, graph.isCurrentMode());

                                    Date lineBreakDate = new Date(lineBreakTime);
                                        //Date lineBreakDatePrvs = new Date(ohlcPreviousTime);
                                        Date lineBreakDatePrvs = new Date(oldTime);

                                        long[] xArr = {oldTime, newTime, ohlcPreviousTime};//long time
                                    float[] xIndexArr1 = convertXArrayTimeToIndex(xArr);

                                    double[] yArr = null;
                                    switch (cp.getOHLCPriority()) {
                                        case GraphDataManager.INNER_Open:
                                                yArr = new double[]{ohlc.getOpen(), dynamicLastCandle.getOpen(), ohlcPrevious.getOpen()};
                                            break;
                                        case GraphDataManager.INNER_High:
                                                yArr = new double[]{ohlc.getHigh(), dynamicLastCandle.getHigh(), ohlcPrevious.getHigh()};
                                            break;
                                        case GraphDataManager.INNER_Low:
                                                yArr = new double[]{ohlc.getLow(), dynamicLastCandle.getLow(), ohlcPrevious.getLow()};
                                            break;
                                        default:
                                                yArr = new double[]{ohlc.getClose(), dynamicLastCandle.getClose(), ohlcPrevious.getClose()};
                                            break;
                                    }

                                    int[] yPixelArr1 = convertYValueArrayToPixelArr(yArr, getIndexOfTheRect(graph.panels, cp.getRect()));
                                        int[] xPixelArr1 = {getPixelFortheIndex(xIndexArr1[0]), getPixelFortheIndex(xIndexArr1[1]), getPixelFortheIndex(xIndexArr1[2])};

                                    Line2D priceTrendLine = new Line2D.Double(new Point(xPixelArr1[0], yPixelArr1[0]), new Point(xPixelArr1[1], yPixelArr1[1]));
                                        Line2D priceTrendLinePrvs = new Line2D.Double(new Point(xPixelArr1[2], yPixelArr1[2]), new Point(xPixelArr1[0], yPixelArr1[0]));

                                    TWDecimalFormat formatter = new TWDecimalFormat("#############.00");
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                        for (Alarmable line : alarmLines) {

                                            //old-code
                                            *//*int[] yPixelArr2 = convertYValueArrayToPixelArr(line.getYArr(), getIndexOfTheRect(graph.panels, cp.getRect()));
                                        float[] xIndexArr2 = line.getIndexArray();
                                        int[] xPixelArr2 = {getPixelFortheIndex(xIndexArr2[0]), getPixelFortheIndex(xIndexArr2[1])};
                                        Line2D lineSlope = new Line2D.Double(new Point(xPixelArr2[0], yPixelArr2[0]), new Point(xPixelArr2[1], yPixelArr2[1]));

                                            if (isAfterDynamicCandleFirstMinute && priceTrendLinePrvs.intersectsLine(lineSlope)) {
                                                final String msgPrvs = line.getAlarmMessage() + "\nSymbol : " + StockGraph.extractSymbolFromStr(cp.getSymbol()) +
                                                        "\nPrice : " + formatter.format(yArr[0]) + "\nTime : " + dateFormatter.format(lineBreakDatePrvs);
                                                line.setAlarmEnabled(false);
                                                showAlertTriggeredMsg(msgPrvs);
                                            }
                                            else if (priceTrendLine.intersectsLine(lineSlope)) {
                                        final String msg = line.getAlarmMessage() + "\nSymbol : " + StockGraph.extractSymbolFromStr(cp.getSymbol()) +
                                                "\nPrice : " + formatter.format(yArr[1]) + "\nTime : " + dateFormatter.format(lineBreakDate);
                                            line.setAlarmEnabled(false);
                                            showAlertTriggeredMsg(msg);
                                            }*//*

                                            if (isAfterDynamicCandleFirstMinute && line.isIntesectingLine(priceTrendLinePrvs)) {
                                                final String msgPrvs = "*******" + line.getAlarmMessage() + "\nSymbol : " + StockGraph.extractSymbolFromStr(cp.getSymbol()) +
                                                        "\nPrice : " + formatter.format(yArr[0]) + "\nTime : " + dateFormatter.format(lineBreakDatePrvs);
                                                line.setAlarmEnabled(false);
                                                showAlertTriggeredMsg(msgPrvs);
                                        }
                                            if (line.isIntesectingLine(priceTrendLine) && line.isAlarmEnabled()) {
                                                final String msg = line.getAlarmMessage() + "\nSymbol : " + StockGraph.extractSymbolFromStr(cp.getSymbol()) +
                                                        "\nPrice : " + formatter.format(yArr[1]) + "\nTime : " + dateFormatter.format(lineBreakDate);
                                                line.setAlarmEnabled(false);
                                                showAlertTriggeredMsg(msg);
                                    }

                                    }

                                        isAfterDynamicCandleFirstMinute = false;*/
                                        //end trend line alarm-shashika--------------------------------------------------------------------------------

                                    }

                                    dynamicLastCandle.setTime(timeStockLastTraded);
                                    dynamicCandleLastUpdateTime = stock.getLastTradeTime();
                                    dynamicCandleStockVolume = stock.getVolume();
                                    dynamicCandleStockTurnover = stock.getTurnover();

                                }
                            }

                        }
                    }
                }
            }

            if (isIntradayDirty) { //new records has come after miniute data ..So need to  set new size > oldsize

                reloadGraphStoreData();
            }

        }
        /**************intraday last cancle  end *****************/

        //hisotry only
        boolean isDirty = (!graph.isCurrentMode()) && (lastPriceUpdateTime + PRICE_UPDATE_MINIMUM_WAIT < timeNow);
        if (isDirty) {
            lastPriceUpdateTime = timeNow;

            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp.getID() <= GraphDataManager.ID_COMPARE) {
                    String key = cp.getSymbol();
                    Stock stock = ChartInterface.getStockObject(key);
                    if ((stock != null) && (stock.getLastTradeTime() > 0)) {
                        if ((stock.getTodaysOpen() > 0) && (stock.getLastTradeTime() > 0)) {

                            long timeInd = stock.getLastTradeTime() / (24 * 60 * 60 * 1000); // done to round off to nearest day
                            timeInd = timeInd * 24 * 60;  //same as divided by 60000 ( 60 and 1000 omitted here)
                            float open = (float) stock.getTodaysOpen();
                            float high = (float) stock.getHigh();
                            float low = (float) stock.getLow();
                            float vwap = (float) stock.getAvgTradePrice();
                            float close;
                            if (!isVWAP) {
                                close = (float) stock.getLastTradeValue();
                            } else {
                                close = (float) stock.getAvgTradePrice();
                            }
                            if (close == 0) {
                                close = (float) stock.getPreviousClosed();
                            }
                            long volume = stock.getVolume();
                            double turnOver = stock.getTurnover();
                            IntraDayOHLC ohlcRecord = new IntraDayOHLC(key, timeInd, open, high, low, close, volume, vwap, turnOver);
                            ArrayList<IntraDayOHLC> al = cp.getDataSource();
                            if (al.size() > 0) {
                                IntraDayOHLC ohlc = (IntraDayOHLC) al.get(al.size() - 1);
                                if (ohlc.getTime() == timeInd) {
                                    al.set(al.size() - 1, ohlcRecord);
                                } else {
                                    al.add(ohlcRecord);
                                }
                            } else {
                                al.add(ohlcRecord);
                            }
                        }
                    }
                }
            }
            reloadGraphStoreData();
        }
        return isDirty;
    }

    //do through the dataSource and calculate todays volume
    private double[] getVolumeTurnoverOfTheLastCandle(ArrayList<IntraDayOHLC> al) {

        double[] arr = new double[2];
        long volume = 0;
        double turnOver = 0;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        int count = 0;
        Date dt;
        for (int i = al.size() - 1; i >= 0; i--) {
            IntraDayOHLC ohlc = al.get(i);
            if (ohlc.getTime() * 60000L >= cal.getTimeInMillis()) {
                dt = new Date(ohlc.getTime() * 60000L);
                volume += ohlc.getVolume();
                turnOver += ohlc.getTurnOver();
                count++;
                //System.out.println(timeFormatter.format(dt)+"\t"+(long)ohlc.getTurnOver());
            }
        }

        System.out.println(">>>>>>>>>>>>  Count " + count);
        arr[0] = volume;
        arr[1] = turnOver;
        return arr;
    }

    private void showAlertTriggeredMsg(String msg) {

        if (msg != null && !msg.isEmpty()) {
            ImageIcon icon = new ImageIcon("images/Theme" + Theme.getID() + "/charts/alert_triggered.gif");
            final JButton[] options = new JButton[1];
            options[0] = new TWButton(Language.getString("OK"));
            final JOptionPane optionPane = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, icon, options, options[0]);
            final JDialog dialog = optionPane.createDialog(Client.getInstance().getFrame(), Language.getString("TREND_LINE_POPUP_MSG"));
            dialog.setContentPane(optionPane);
            dialog.setModal(false);
            dialog.setResizable(false);
            dialog.pack();
            dialog.setLocationRelativeTo(Client.getInstance().getFrame());
            options[0].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialog.dispose();
                }
            });
            dialog.setVisible(true);

        }

    }

    public void calculateMinMaxBounds() {
        if ((graphStore.size() < 1) || (Sources.size() <= 0)) {
            if ((minY != null) && (maxY != null))
                for (int i = 0; i < minY.length; i++) {
                    minY[i] = 0;
                    maxY[i] = 1; //to fix the refresh issue
                }
            midNights = null;
            return;
        }
        indexingFactors = new float[instantSourceCount];
        calculateIndexingFactors(indexingFactors);

        if (minY != null)
            for (int i = 0; i < minY.length; i++) {
                minY[i] = Float.MAX_VALUE;
                maxY[i] = -Float.MAX_VALUE;
            }

        int bIndex = Math.round(Math.max(beginIndex, 0));
        int eIndex = Math.round(Math.min(endIndex, graphStore.size() - 1));

        Object[] singlePoint;
        ChartPoint singleSource;

        try {
            for (int i = bIndex; i <= eIndex; i++) {
                singlePoint = (Object[]) graphStore.get(i);
                for (int j = 1; j <= instantSourceCount; j++) {
                    if ((singlePoint.length <= j) || (singlePoint[j] == null)) continue;
                    ChartProperties cp = (ChartProperties) Sources.get(j - 1);
                    int panelIndex = getIndexOfTheRect(graph.panels, cp.getRect());
                    if (panelIndex < 0) continue;
                    singleSource = (ChartPoint) singlePoint[j];
                    if (cp.getID() > ID_COMPARE) {

                        if (((cp.getID() == ID_NET_TURNOVER) && (((NetTurnOverInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) || (cp.getID() == ID_NET_ORDERS) && (((NetOrdersInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) {
                            minY[panelIndex] = 0d;
                            maxY[panelIndex] = 100d;
                        } else {
                            minY[panelIndex] = Math.min(minY[panelIndex], singleSource.getValue(INDICATOR) * indexingFactors[j - 1]);
                            maxY[panelIndex] = Math.max(maxY[panelIndex], singleSource.getValue(INDICATOR) * indexingFactors[j - 1]);
                        }
                        continue;
                    }
                    if ((cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE) || (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR)) {
                        if (singleSource.getValue(cp.getOHLCPriority()) <= 0) continue;
                        minY[panelIndex] = Math.min(minY[panelIndex], singleSource.getValue(cp.getOHLCPriority()) * indexingFactors[j - 1]);
                        maxY[panelIndex] = Math.max(maxY[panelIndex], singleSource.getValue(cp.getOHLCPriority()) * indexingFactors[j - 1]);
                    } else {
                        if (singleSource.getValue(INNER_Low) <= 0) continue;
                        minY[panelIndex] = Math.min(minY[panelIndex], singleSource.getValue(INNER_Low) * indexingFactors[j - 1]);
                        maxY[panelIndex] = Math.max(maxY[panelIndex], singleSource.getValue(INNER_High) * indexingFactors[j - 1]);
                    }
                }
            }

            if (showMinMaxPriceLines && minPriceLine != null && maxPriceLine != null && minPriceLine.getYArr()[0] > 0 &&
                    maxPriceLine.getYArr()[0] > 0) {
                ChartProperties cp = getBaseCP();
                int panelIndex = getIndexOfTheRect(graph.panels, cp.getRect());
                minY[panelIndex] = Math.min(minPriceLine.getYArr()[0], minY[panelIndex]);
                maxY[panelIndex] = Math.max(maxPriceLine.getYArr()[0], maxY[panelIndex]);
            }

            for (int i = 0; i < minY.length; i++) {
                if (minCustomY[i] != maxCustomY[i]) {
                    minY[i] = minCustomY[i];
                    maxY[i] = maxCustomY[i];
                }
                if (minY[i] > maxY[i]) {
                    maxY[i] = minY[i] = 0;
                }
                if (minY[i] == maxY[i]) {
                    /**avoiding volume becomes - values*/

                    if (getVolumeCP() != null) {
                        ChartProperties volCp = getVolumeCP();
                        int volpanelIndex = getIndexOfTheRect(graph.panels, volCp.getRect());
                        if (i == volpanelIndex) {
                            minY[i] = Math.max(minY[i] - 1, 0);
                            if (maxY[i] <= 0) {
                                maxY[i] = 1;
                            } else {
                                maxY[i] += 1;
                            }
                        } else {
                            minY[i] -= 1;
                            maxY[i] += 1;
                        }
                    } else {
                        minY[i] -= 1;
                        maxY[i] += 1;
                    }
                }
            }

            if (showAnnouncements || showSplits) {
                adjustMaxYForSplits();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        calcPriceMaxValue();
        extractMidnights(bIndex, eIndex);
    }

    public void adjustMaxYForSplits() {

        for (int j = 0; j < Sources.size(); j++) {

            if ((faTime == null) || (faTime.length < (j + 1))) return;
            if ((faTime[j] == null) || (((int[]) faTime[j]).length < 1)) continue;

            ChartProperties cp = Sources.get(j);
            if (cp.getRect() != graph.mainRect) { //calculation do for the main rectangle
                continue;
            }

            int panelIndex = getIndexOfTheRect(graph.panels, cp.getRect());
            int imageHeight = 0;

            int[] iaSplits = (int[]) faAnnTime[j];
            DynamicArray daSplits = cp.getDaSplits();

            /*if ((iaSplits != null) && (iaSplits.length > 0)) {
                for (int i = 0; i < iaSplits.length; i++) {
            */
            if ((daSplits != null) && (daSplits.size() > 0)) {
                for (int i = 0; i < daSplits.size(); i++) {
                    try {
                        /* int splitIndex = iaSplits[i];
                           Object[] singlePoint = (Object[]) graphStore.get(splitIndex);
                         */

                        SplitPointRecord sp = (SplitPointRecord) daSplits.get(i);
                        long time = sp.time;
                        time = getCurrentBeginTime(time);
                        Long[] longArr = new Long[]{time};
                        int searchIndex = graphStore.indexOfContainingValue(longArr);
                        ChartPoint point = null;
                        if (searchIndex >= 0) {
                            Object[] singlePoint = (Object[]) (graphStore.get(searchIndex));
                            point = (ChartPoint) singlePoint[1];    //base chart index is always 1
                            if (point == null || (searchIndex < beginIndex || searchIndex > endIndex)) {
                                continue;
                            }
                        }

                        double priceTop = 0;
                        if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR || cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE) {  //for line and bar style
                            priceTop = point.getValue(cp.getOHLCPriority());
                        } else {   // for CANDLE, HLC, and OHLC
                            priceTop = point.getValue(GraphDataManager.INNER_High);
                        }

                        imageHeight = 27; //hardcoded

                        double pricePerPixel = (maxY[panelIndex] - minY[panelIndex]) / graph.mainRect.getHeight();
                        double priceForSplitHeight = imageHeight * pricePerPixel;

                        if (priceTop + priceForSplitHeight > maxY[panelIndex]) {
                            maxY[panelIndex] = priceTop + priceForSplitHeight;
                        }

                    } catch (Exception ex) {
                        //System.out.println("************ exception in adjustMaxYForSplits **********");
                    }
                }
            }
        }
    }

    private void calcPriceMaxValue() {

        tmpPriceValue = 0;
        for (int i = 0; i < graph.panels.size(); i++) {
            if (graph.isMaximized() && (i != graph.getMaximizedPanelID())) {
                continue;
            }
            tmpPriceValue = Math.max(tmpPriceValue, maxY[i]);
            tmpPriceValue = Math.max(tmpPriceValue, -minY[i]);
        }
    }

    public void resetCustomYBounds() {
        for (int i = 0; i < minCustomY.length; i++) {
            minCustomY[i] = 0f;
            maxCustomY[i] = 0f;
        }
    }

    private void calculateIndexingFactors(final float[] indexingFactors) {
        if (indexed) {
            double base = getFirstNonZeroValue(0);
            ChartProperties cp;
            for (int i = 0; i < instantSourceCount; i++) {
                cp = (ChartProperties) Sources.get(i);
                if (cp.getID() <= ID_COMPARE) {
                    indexingFactors[i] = (float) (base / getFirstNonZeroValue(i));
                } else {
                    if (((Indicator) cp).hasItsOwnScale()) {
                        indexingFactors[i] = 1;
                    } else {
                        int ind = ((Indicator) cp).getInnerSourceIndex(Sources);
                        indexingFactors[i] = (float) (base / getFirstNonZeroValue(ind));
                    }
                }
            }
        } else {
            for (int i = 0; i < instantSourceCount; i++) {
                indexingFactors[i] = 1;
            }
        }
    }

    private double getFirstNonZeroValue(int graphIndex) {
        double base = 1;
        ChartPoint chartPoint;
        for (int i = periodBeginIndex; i < graphStore.size(); i++) {
            long timeInd = getTimeMillisec(i);
            chartPoint = readChartPoint(timeInd, graphIndex);
            if (chartPoint != null) {
                ChartProperties cp = (ChartProperties) Sources.get(graphIndex);
                if (cp.getID() > ID_COMPARE) {
                    base = chartPoint.getValue(INDICATOR);
                } else {
                    base = chartPoint.getValue(cp.getOHLCPriority());
                }
                if (base > 0) break;
            }
        }
        if (base <= 0) base = 1;
        return base;
    }

    public void extractMidnights(int bIndex, int eIndex) {
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        long bTime = getTimeMillisec(bIndex);
        long eTime = getTimeMillisec(eIndex);
        long timeGap;
        GregorianCalendar calendar = new GregorianCalendar();
        if (graph.isCurrentMode()) {
            int NoOfDays = (int) ((getMidnight(eTime, graph.isCurrentMode()) - getMidnight(bTime, graph.isCurrentMode())) / Miliseconds_Per_Day) + 1;
            if (NoOfDays > 0) {
                midNights = new int[NoOfDays];
                bTime = getMidnight(bTime - 24L * 3600L * 1000L, graph.isCurrentMode()); // to get the yesterday midnight
                for (int i = 0; i < NoOfDays; i++) {
                    timeGap = Miliseconds_Per_Day * (long) i;
                    eTime = bTime + timeGap;
                    longArr[0] = new Long(eTime);
                    try {
                        searchIndex = graphStore.indexOfContainingValue(longArr);
                    } catch (Exception ex) {
                    }
                    if (searchIndex < 0) {
                        searchIndex = -searchIndex - 1;
                    }
                    midNights[i] = searchIndex;
                }
            } else {
                midNights = null;
            }
        } else {
            calendar.setTimeInMillis(eTime);
            int NoOfYears = calendar.get(calendar.YEAR);
            calendar.setTimeInMillis(bTime);
            NoOfYears = NoOfYears - calendar.get(calendar.YEAR) + 1;

            if (NoOfYears > 0) {
                midNights = new int[NoOfYears];
                // to get the last year end
                bTime = getMidnight(bTime, false) - 400 * (long) Miliseconds_Per_Day;
                for (int i = 0; i < NoOfYears; i++) {
                    eTime = getMidnight(bTime, false);
                    longArr[0] = new Long(eTime);
                    try {
                        searchIndex = graphStore.indexOfContainingValue(longArr);
                    } catch (Exception ex) {
                    }
                    if (searchIndex < 0) {
                        searchIndex = -searchIndex - 1;
                    }
                    midNights[i] = searchIndex;
                    bTime = eTime + 10 * (long) Miliseconds_Per_Day;
                }
            } else {
                midNights = null;
            }
        }
    }

    public void calculateXYFactors() {
        if (endIndex > beginIndex) {
            xFactor = ((float) graph.mainRect.width - 2 * clearance) / ((float) endIndex - beginIndex);
        } else {
            xFactor = 1f;
        }

        for (int i = 0; i < graph.panels.size(); i++) {
            if (maxY[i] > minY[i]) {
                WindowPanel r = (WindowPanel) graph.panels.get(i);
                int clientHt = (r.height - titleHeight - 2 * clearance);
                if ((clientHt < 2) && (clientHt > -2 * clearance)) {
                    clientHt = 2;
                }
                if (r.isSemiLog() && (minY[i] > 0)) {
                    yFactor[i] = Math.max(clientHt / (Math.log(maxY[i]) - Math.log(minY[i])), 0);
                } else {
                    yFactor[i] = Math.max(clientHt / (maxY[i] - minY[i]), 0);
                }
            } else {
                yFactor[i] = 1f;
            }
        }
        setBarThickness();
    }

    private void extractPointArrays() {
        faTime = null;
        faAnnTime = null;
        faAnnGrp = null;
        faOpen = null;
        faHigh = null;
        faLow = null;
        faClose = null;
        int sCount = instantSourceCount;
        faTime = new Object[sCount];
        faAnnTime = new Object[sCount];
        faAnnGrp = new Object[sCount];
        faAnnTimeIndex = new Object[sCount];
        faOpen = new Object[sCount];
        faHigh = new Object[sCount];
        faLow = new Object[sCount];
        faClose = new Object[sCount];
        ChartPoint fa;
        ArrayList<ChartPoint> al = new ArrayList<ChartPoint>();
        ArrayList<Integer> alTime = new ArrayList<Integer>();
        ArrayList<Integer> alAnnTime = new ArrayList<Integer>();
        ArrayList<Integer> alAnnGrp = new ArrayList<Integer>();
        ArrayList<Integer> alAnnTimeIndex = new ArrayList<Integer>();
        int bIndex = Math.round(Math.max(beginIndex, 0));
        int eIndex = Math.round(Math.min(endIndex, graphStore.size() - 1));
        Graphics g;
        Point sz = null;
        ChartProperties cpBase = getBaseCP();
        int baseIndex = getIndexForSourceCP(cpBase);
        ArrayList<Integer> alBase = new ArrayList<Integer>();
        for (int j = 0; j < sCount; j++) {
            al.clear();
            alTime.clear();
            alAnnTime.clear();
            alAnnGrp.clear();
            alAnnTimeIndex.clear();
            ChartProperties cp = (ChartProperties) Sources.get(j);
            int tmpCnt = 0;

            for (int i = bIndex; i <= eIndex; i++) {
                fa = readChartPointAtIndex(i, j);
                if (fa != null) {
                    al.add(fa);
                    alTime.add(new Integer(i));
                    if (cp.getID() <= GraphDataManager.ID_COMPARE) {

                        //to remove the last point in the first minute
                        if (fa.Open < 0) {
                            al.remove(fa);
                            alTime.remove(new Integer(i));
                        }

                        int annGrp = hasAnnouncementsAtIndex(i, j);
                        if (annGrp > 0) {
                            alAnnTime.add(new Integer(tmpCnt)); // here I am using the exact index of pixel array  //i-bIndex  //i
                            alAnnGrp.add(new Integer(annGrp));
                            alAnnTimeIndex.add(new Integer(i));
                        }
                    }
                    //this is incremented everytime a value is added to alTime
                    tmpCnt++;
                }
            }

            //// temp patch for first candle always green issue - Mevan @ 2009-12-10
            if (bIndex >= 0) {
                ChartPoint tempCPPrevious, tempCPAfter;
                if (bIndex == 0) {
                    cp.isFirstCandleGreen = true;
                } else {
                    tempCPPrevious = readChartPointAtIndex(bIndex - 1, j);
                    tempCPAfter = readChartPointAtIndex(bIndex, j);
                    if (tempCPPrevious != null && tempCPAfter != null) {
                        cp.isFirstCandleGreen = (tempCPAfter.getValue(cp.getOHLCPriority()) >= tempCPPrevious.getValue(cp.getOHLCPriority()));
                    } else if (tempCPPrevious == null) {
                        cp.isFirstCandleGreen = true;
                    }
                }
            }

            if ((j == 0) || (cp == cpBase)) {
                alBase.clear();
                alBase.addAll(alTime);
            }
            int loopCnt = al.size();
            faTime[j] = new int[loopCnt];
            faOpen[j] = new int[loopCnt];
            faClose[j] = new int[loopCnt];
            if (cp.getID() <= ID_COMPARE) {
                faHigh[j] = new int[loopCnt];
                faLow[j] = new int[loopCnt];

                faAnnTime[j] = new int[alAnnTime.size()];
                faAnnGrp[j] = new int[alAnnTime.size()];
                faAnnTimeIndex[j] = new int[alAnnTime.size()];
                for (int i = 0; i < alAnnTime.size(); i++) {
                    ((int[]) faAnnTime[j])[i] = alAnnTime.get(i).intValue();  // here I am using the exact index of pixel array
                    ((int[]) faAnnGrp[j])[i] = alAnnGrp.get(i).intValue();
                    ((int[]) faAnnTimeIndex[j])[i] = alAnnTimeIndex.get(i).intValue();
                }
            }
            int pnlID = getIndexOfTheRect(graph.panels, cp.getRect());
            float iF = indexingFactors[j];
            //special calc for strategies
            if ((cp.getID() == GraphDataManager.ID_CUSTOM_INDICATOR) && (((IndicatorBase) cp).isDrawSymbol()) && (((IndicatorBase) cp).getSymbolID() < 1000)) {
                g = graph.getGraphics();
                IndicatorBase ib = (IndicatorBase) cp;
                String symbl = new String(saSymbol, ib.getSymbolID() - 1, 1);
                FontMetrics fm = g.getFontMetrics(f1);
                sz = new Point(fm.stringWidth(symbl), fm.getHeight());
            }
            for (int i = 0; i < loopCnt; i++) {

                ((int[]) faTime[j])[i] = getPixelFortheIndex(alTime.get(i));
                if ((cp.getID() == GraphDataManager.ID_CUSTOM_INDICATOR) && (((IndicatorBase) cp).isDrawSymbol())) {
                    IndicatorBase ib = (IndicatorBase) cp;
                    ////////////////
                    if (ib.getSymbolID() >= 1000) {
                        int ind = Collections.binarySearch(alBase, alTime.get(i));
                        ((int[]) faOpen[j])[i] = ind;
                        continue;
                    }
                    ////////////////
                    float vAdj = 0;
                    switch (ib.getPosition()) {
                        case -1:
                            vAdj = 2 + (float) sz.getY() / 2f;
                            break;
                        case 1:
                            vAdj = -(float) sz.getY() / 2f - 2;
                            break;
                        default:
                            vAdj = 0;
                            break;
                    }
                    float y = getPixelFortheYValue(al.get(i).Open * iF, pnlID) + vAdj;
                    if (minCustomY[pnlID] == maxCustomY[pnlID]) {
                        Rectangle r = cp.getRect();
                        y = (float) Math.min(y, r.getY() + r.getHeight() - sz.getY() / 2);
                        y = (float) Math.max(y, r.getY() + titleHeight + sz.getY() / 2f);
                    }
                    ((int[]) faOpen[j])[i] = Math.round(y);

                } else {

                    if (((cp.getID() == ID_NET_TURNOVER) && (((NetTurnOverInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) || (cp.getID() == ID_NET_ORDERS) && (((NetOrdersInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) {
                        ((int[]) faOpen[j])[i] = (int) (al.get(i).Open * 100);
                    } else {
                        ((int[]) faOpen[j])[i] = getPixelFortheYValue(al.get(i).Open * iF, pnlID);
                    }
                    //if cpge4t id cashflow && percnetage mode
                    //with out converting to pixels penwana dasamasthana gannen wadi karanna
                    //constant 100   two decimals
                    // 75 .33 = 7533
                    // wena dekakata percenata  open  == percentage
                    //define a new chart - cp.boolean .balueneeded

                }
                if (cp.getID() <= ID_COMPARE) {
                    ((int[]) faHigh[j])[i] = getPixelFortheYValue(al.get(i).High * iF, pnlID);
                    ((int[]) faLow[j])[i] = getPixelFortheYValue(al.get(i).Low * iF, pnlID);
                    ((int[]) faClose[j])[i] = getPixelFortheYValue(al.get(i).Close * iF, pnlID);
                }
            }
        }
    }

    private int hasAnnouncementsAtIndex(int i, int j) {
        ChartProperties cp = (ChartProperties) Sources.get(j);
        DynamicArray daSplits = cp.getDaSplits();
        long timeBegin = getTimeMillisec(i) - 1;
        long timeEnd = timeBegin + interval * 1000L;
        int searchIndex1 = daSplits.indexOfContainingValue(new SplitPointRecord(timeBegin, 2f, 0, -1, 0));
        int searchIndex2 = daSplits.indexOfContainingValue(new SplitPointRecord(timeEnd, 2f, 0, -1, 0));
        if (searchIndex1 == searchIndex2) {
            // this means there are no points in between
            // so no announcements in the region (for the specific index in this case)
            return 0;
        } else {
            // if it comes here that means search indexes are different and there lies a point in between
            // serach indicies obtained must be (-) as timeBegin has -1 millisecond adjustment
            int result = 0;
            for (int n = -searchIndex1 - 1; n < -searchIndex2 - 1; n++) {
                SplitPointRecord spr = (SplitPointRecord) daSplits.get(n);
                if (spr.group == 0) {
                    result = result | GraphDataManager.ALERT_ANNOUNCEMENT;
                } else {
                    switch (spr.type) {
                        case 1: //Stock Split
                            result = result | GraphDataManager.ALERT_STOCK_SPLIT;
                            break;
                        case 2: //Stock Dividend
                            result = result | GraphDataManager.ALERT_STOCK_DIVIDEND;
                            break;
                        case 3: //Rights Issue
                            result = result | GraphDataManager.ALERT_RIGHTS_ISSUE;
                            break;
                        case 4: //Reverse Split
                            result = result | GraphDataManager.ALERT_REVERSE_SPLIT;
                            break;
                        case 5: //Capital Reduction
                            result = result | GraphDataManager.ALERT_CAPITAL_REDUCTION;
                            break;
                        case 6: //Cash dividend
                            result = result | GraphDataManager.ALERT_CASH_DIVIDEND;
                            break;
                    }
                }
            }
            return result;
        }
    }

    private void setBarThickness() {
        switch (Math.round(xFactor)) {
            case 0:
                dashLength = 1;
                dashThickness = 0.5f;
                break;
            case 1:
                dashLength = 1;
                dashThickness = 0.5f;
                break;
            case 2:
                dashLength = 1;
                dashThickness = 0.8f;
                break;
            case 3:
                dashLength = 1;
                dashThickness = 0.8f;
                break;
            case 4:
                dashLength = 1;
                dashThickness = 1f;
                break;
            case 5:
                dashLength = 2;
                dashThickness = 1f;
                break;
            case 6:
                dashLength = 2;
                dashThickness = 1.2f;
                break;
            case 7:
                dashLength = 2;
                dashThickness = 1.2f;
                break;
            default:
                if (xFactor <= 10) {
                    dashLength = 3;
                    dashThickness = 1.4f;
                } else if (xFactor <= 15) {
                    dashLength = 3;
                    dashThickness = 1.8f;
                } else if (xFactor <= 20) {
                    dashLength = 4;
                    dashThickness = 2f;
                } else if (xFactor <= 25) {
                    dashLength = 5;
                    dashThickness = 2.5f;
                } else if (xFactor <= 30) {
                    dashLength = 6;
                    dashThickness = 3f;
                } else {
                    dashLength = Math.round(xFactor / 5);//(byte)Math.min(Math.round(xFactor/5),50);
                    dashThickness = 4f;
                }
                break;
        }
        barThickness = 2 * dashLength;
        dashPen = getDashPenWidth();
    }

    private Stroke getDashPenWidth() {
        if (dashThickness <= 0.8f) {
            return graph.BS_0p8f_solid;
        } else if (dashThickness <= 1f) {
            return graph.BS_1p0f_solid;
        } else if (dashThickness <= 1.2f) {
            return graph.BS_1p2f_solid;
        } else if (dashThickness <= 1.4f) {
            return graph.BS_1p4f_solid;
        } else if (dashThickness <= 1.8f) {
            return graph.BS_1p8f_solid;
        } else if (dashThickness <= 2f) {
            return graph.BS_2p0f_solid;
        } else if (dashThickness <= 2.5f) {
            return graph.BS_2p5f_solid;
        } else if (dashThickness <= 3f) {
            return graph.BS_3p0f_solid;
        } else { //4f
            return graph.BS_4p0f_solid;
        }
    }

    //added by charithn
    private void drawHighlightedObjects(Graphics g, boolean isPrinting) {

        try {
            Graphics2D gg = ((Graphics2D) g);
            for (int j = 0; j < highLightedSources.size(); j++) {

                HighlightedArea cp = highLightedSources.get(j);
                ChartProperties topCP = cp.getTopCP();
                ChartProperties bottomCP = cp.getBottomCP();

                if (topCP == null || bottomCP == null) continue;
                //get the source indexes for top and bottom sources
                int graphIndexTop = getIndexForSourceCP(topCP);
                int graphIndexBottom = getIndexForSourceCP(bottomCP);

                // no need to contniue in these scenarios.
                if (topCP.getRect() != bottomCP.getRect()) continue;
                if ((faTime[graphIndexTop] == null) || (((int[]) faTime[graphIndexTop]).length < 1)) continue;
                if ((faTime[graphIndexBottom] == null) || (((int[]) faTime[graphIndexBottom]).length < 1)) continue;
                if (topCP.isHidden() || bottomCP.isHidden()) continue;

                int length = ((int[]) faTime[graphIndexTop]).length;
                //polygon bounded by the upper band
                int[] x_upperPoloygon = new int[length + 2];
                int[] y_upperPoloygon = new int[length + 2];

                //polygon bounded by the lower band
                int[] x_lowerPoloygon = new int[length + 2];
                int[] y_lowerPoloygon = new int[length + 2];

                for (int i = 0; i < length; i++) {
                    x_upperPoloygon[i] = ((int[]) faTime[graphIndexTop])[i];
                    y_upperPoloygon[i] = ((int[]) faOpen[graphIndexTop])[i];

                    x_lowerPoloygon[i] = ((int[]) faTime[graphIndexBottom])[i];
                    y_lowerPoloygon[i] = ((int[]) faOpen[graphIndexBottom])[i];
                }

                //sets the upper band -- right hand side bottom most point and end chart point
                x_upperPoloygon[x_upperPoloygon.length - 2] = ((int[]) faTime[graphIndexTop])[length - 1];
                y_upperPoloygon[y_upperPoloygon.length - 2] = topCP.getRect().y + topCP.getRect().height;
                //sets the  upper band -- left hand side bottom point and begining chart point
                x_upperPoloygon[x_upperPoloygon.length - 1] = ((int[]) faTime[graphIndexTop])[0];
                y_upperPoloygon[y_upperPoloygon.length - 1] = topCP.getRect().y + topCP.getRect().height;

                //sets the lower band -- right hand side top most point and end chart point
                x_lowerPoloygon[x_lowerPoloygon.length - 2] = ((int[]) faTime[graphIndexBottom])[length - 1];
                y_lowerPoloygon[y_lowerPoloygon.length - 2] = topCP.getRect().y;
                //sets the  lower band -- left hand side bottom most point and begining chart point
                x_lowerPoloygon[x_lowerPoloygon.length - 1] = ((int[]) faTime[graphIndexBottom])[0];
                y_lowerPoloygon[y_lowerPoloygon.length - 1] = topCP.getRect().y;

                //fill the area between the upper and lower lines of Bollinger Bands
                g.setColor(((FillArea) topCP).getFillTransparentColor());
                gg.setStroke(topCP.getPen());

                //important- uppser clip should come first before lower clip
                gg.setClip(new Polygon(x_upperPoloygon, y_upperPoloygon, x_upperPoloygon.length));
                gg.clip(new Polygon(x_lowerPoloygon, y_lowerPoloygon, x_lowerPoloygon.length));

                //fills the entire rectangle from the fill color.
                //clip will paint the necessary region(intersects by the regions above) to fill.

                if (((FillArea) topCP).isFillBands() && (((FillArea) bottomCP).isFillBands())) {
                    gg.fill(topCP.getRect());
                }
                gg.setClip(null);
            }
        } catch (Exception e) {

        }
    }

    private void drawPointsOnGraphics(Graphics g, boolean isPrinting) {
        if (graphStoreSize == 0) return;
        int topGrf_Top = borderWidth + legendHeight + titleHeight;
        int botGrf_Bot = graph.getBottomPanelBottom();//graph.getHeight()-borderWidth-sliderHeight-heightXaxis;
        if (isPrinting) {
            g.setColor(Color.darkGray);
        } else {
            g.setColor(Color.orange);
        }
        ((Graphics2D) g).setStroke(graph.BS_0p8f_solid);
        g.setClip(graph.mainRect.x, topGrf_Top, graph.mainRect.width, botGrf_Bot - topGrf_Top);

        // draw year separator lines
        g.setColor(graph.getyearSeparatorColor());

        if ((midNights != null) && (midNights.length > 0)) {
            for (int i = 0; i < midNights.length; i++) {
                if ((i == 0) && (!isPrinting)) continue;
                int x = Math.round((midNights[i] - beginIndex) * xFactor) + graph.mainRect.x + clearance;
                g.drawLine(x, topGrf_Top, x, botGrf_Bot);
            }
        }

        g.setClip(null);
        //////////////////// drawing candlestick pattern markings //////////////////////////////////////////
        ChartProperties cpBase = getBaseCP();
        int baseIndex = getIndexForSourceCP(cpBase);
        int dLen = Math.max(barThickness, 7);
        for (int j = 0; j < sourceCount; j++) {
            if ((faTime != null) && (faTime.length >= (j + 1))) {
                if ((faTime[j] == null) || (((int[]) faTime[j]).length < 1)) continue;
                ChartProperties cp = Sources.get(j);
                if ((cp.getID() == GraphDataManager.ID_CUSTOM_INDICATOR) && (((IndicatorBase) cp).isDrawSymbol()) && (((IndicatorBase) cp).getSymbolID() >= 1000) && (!cp.isHidden())) {
                    Rectangle r = cp.getRect();
                    g.setClip(r);
                    int len = ((int[]) faTime[j]).length;
                    int[] yInd = (int[]) faOpen[j];
                    int tmpID = ((IndicatorBase) cp).getSymbolID() - 1000;
                    //Pen.Width = 0.5f;
                    ((Graphics2D) g).setStroke(new BasicStroke(0.5f));
                    for (int i = 0; i < len; i++) {
                        int xInd = yInd[i];
                        if (xInd < tmpID) continue;
                        int x1, x2, H, L;
                        x1 = ((int[]) faTime[baseIndex])[xInd - tmpID];
                        x2 = ((int[]) faTime[baseIndex])[xInd];
                        switch (cpBase.getChartStyle()) {
                            case StockGraph.INT_GRAPH_STYLE_LINE:
                                int[] yPrio = getPriotityArray(cp.getOHLCPriority(), baseIndex);
                                H = yPrio[xInd - tmpID];
                                L = yPrio[xInd - tmpID];
                                if (tmpID > 0) {
                                    for (int k = 0; k < tmpID; k++) {
                                        H = Math.min(H, yPrio[xInd - k]);
                                        L = Math.max(L, yPrio[xInd - k]);
                                    }
                                }
                                break;
                            case StockGraph.INT_GRAPH_STYLE_BAR:
                                int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
                                //int bottom = Math.min(zero, r.y+r.height-clearance+barExt);
                                int[] yPrio1 = getPriotityArray(cp.getOHLCPriority(), baseIndex);
                                H = Math.min(yPrio1[xInd - tmpID], zero);
                                L = Math.max(yPrio1[xInd - tmpID], zero);
                                if (tmpID > 0) {
                                    for (int k = 0; k < tmpID; k++) {
                                        H = Math.min(H, yPrio1[xInd - k]);
                                        L = Math.max(L, yPrio1[xInd - k]);
                                    }
                                }
                                break;
                            default: // HLC, OHLC, CANDLE
                                H = ((int[]) faHigh[baseIndex])[xInd - tmpID];
                                L = ((int[]) faLow[baseIndex])[xInd - tmpID];
                                if (tmpID > 0) {
                                    for (int k = 0; k < tmpID; k++) {
                                        H = Math.min(H, ((int[]) faHigh[baseIndex])[xInd - k]);
                                        L = Math.max(L, ((int[]) faLow[baseIndex])[xInd - k]);
                                    }
                                }
                                break;
                        }
                        g.setColor(cp.getColor());
                        g.fillRect(x1 - dLen, H - dLen, 2 * dLen + x2 - x1, L - H + 2 * dLen);
                        g.setColor(cp.getWarningColor());
                        g.drawRect(x1 - dLen, H - dLen, 2 * dLen + x2 - x1, L - H + 2 * dLen);
                        // drawing label
                        int pos = ((IndicatorBase) cp).getPosition();
                        if ((pos == 1) || (pos == -1)) {
                            g.setColor(cp.getWarningColor());
                            String s = cp.getShortName().substring(0, 2);
                            FontMetrics fm = g.getFontMetrics(StockGraph.font_PLAIN_9);
                            int sLen = fm.stringWidth(s);
                            int y = (pos == 1) ? H - dLen - 2 : L + dLen + fm.getHeight() - 2;
                            g.setFont(StockGraph.font_PLAIN_9);
                            g.drawString(s, (x1 + x2 - sLen) / 2 + 1, y);
                        }
                        //draw selected
                        if (cp.isSelected()) {
                            g.setColor(cp.getWarningColor());
                            g.drawRect(x1 - dLen - 1, H - dLen - 1, 2 * dLen + x2 - x1 + 2, L - H + 2 * dLen + 2);
                        }
                    }
                }
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        for (int j = 0; j < sourceCount; j++) {
            if ((faTime == null) || (faTime.length < (j + 1))) return;
            if ((faTime[j] == null) || (((int[]) faTime[j]).length < 1)) continue;
            ChartProperties cp = Sources.get(j);
            if ((cp.getRect() == graph.volumeRect) && !graph.isShowingVolumeGrf()) {
                continue;
            }
            if ((cp.getRect() == graph.turnOverRect) && !graph.isShowingTurnOverGraph()) {
                continue;
            }
            int len = ((int[]) faTime[j]).length;
            Rectangle r = cp.getRect();
            g.setClip(r);

            g.setColor(cp.getColor());


            if ((cp.getID() > ID_COMPARE) || (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE)) {
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            } else {
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            }
            ((Graphics2D) g).setStroke(cp.getPen());
            if (cp.getID() > ID_COMPARE && (!cp.isHidden())) {
                int[] yInd = (int[]) faOpen[j];
                int[] yIndNetBidValues = (int[]) faClose[j];
                if ((cp.getID() == GraphDataManager.ID_CUSTOM_INDICATOR) && (((IndicatorBase) cp).isDrawSymbol())) {
                    if (((IndicatorBase) cp).getSymbolID() >= 1000) continue; // Candlestick pattern
                    IndicatorBase ib = (IndicatorBase) cp;
                    String symbl = new String(saSymbol, ib.getSymbolID() - 1, 1);//saSymbol[ib.getSymbolID() - 1];
                    g.setColor(cp.getColor());
                    FontMetrics fm = g.getFontMetrics(f1);
                    int sw = fm.stringWidth(symbl);
                    for (int i = 0; i < len; i++) {
                        g.setFont(f1);
                        g.drawString(symbl, ((int[]) faTime[j])[i] - sw / 2, yInd[i] + fm.getDescent() + 3);  //+ -fm.getHeight() / 2 + 1
                    }
                } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR) {

                    if (cp.getID() == ID_VOLUME) {

                        int prevY = Integer.MAX_VALUE;
                        int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
                        for (int i = 0; i < len; i++) {
                            if (prevY >= yInd[i] && (yInd[i] < zero)) {
                                int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                                g.fillRect(((int[]) faTime[j])[i] - dashLength, Math.min(yInd[i], zero),
                                        barThickness, Math.abs(bottom - yInd[i]));
                            }
                            prevY = yInd[i];
                        }

                        //changed by charithn. to fix the not showing volume graph in black theme
                        /* if (cp.isUserSaved()) {
                            if (!cp.isUsingSameColor()) {
                                g.setColor(cp.getWarningColor());
                            } else {
                                g.setColor(cp.getColor());
                            }

                        } else {
                            g.setColor(Theme.getColor("GRAPH_DOWN_COLOR"));
                        }*/

                        if (!cp.isUsingSameColor()) {
                            g.setColor(cp.getWarningColor());
                        }

                        prevY = Integer.MAX_VALUE;
                        for (int i = 0; i < len; i++) {
                            if (prevY < yInd[i] && yInd[i] < zero) {
                                int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                                g.fillRect(((int[]) faTime[j])[i] - dashLength, Math.min(yInd[i], zero),
                                        barThickness, Math.abs(bottom - yInd[i]));
                            }
                            prevY = yInd[i];
                        }

                    } else if (cp.getID() == ID_TURNOVER) {   //added by charithn

                        int prevY = Integer.MAX_VALUE;
                        int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
                        for (int i = 0; i < len; i++) {
                            if (prevY >= yInd[i] && (yInd[i] < zero)) {
                                int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                                g.fillRect(((int[]) faTime[j])[i] - dashLength, Math.min(yInd[i], zero), barThickness, Math.abs(bottom - yInd[i]));
                            }
                            prevY = yInd[i];
                        }

                        if (!cp.isUsingSameColor()) {
                            g.setColor(cp.getWarningColor());
                        }
                        prevY = Integer.MAX_VALUE;
                        for (int i = 0; i < len; i++) {
                            if (prevY < yInd[i] && yInd[i] < zero) {
                                int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                                g.fillRect(((int[]) faTime[j])[i] - dashLength, Math.min(yInd[i], zero), barThickness, Math.abs(bottom - yInd[i]));
                            }
                            prevY = yInd[i];
                        }

                    } else {
                        int prevY = Integer.MAX_VALUE;
                        int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
                        for (int i = 0; i < len; i++) {
                            if (prevY >= yInd[i]) {
                                int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                                if (faTime != null && yInd != null) {
                                    g.fillRect(((int[]) faTime[j])[i] - dashLength, Math.min(yInd[i], zero),
                                            barThickness, Math.abs(bottom - yInd[i]));
                                }
                            }
                            prevY = yInd[i];
                        }

                        //changed by charithn. to fix the not showing volume graph in black theme
                        /* if (cp.isUserSaved()) {
                            if (!cp.isUsingSameColor()) {
                                g.setColor(cp.getWarningColor());
                            } else {
                                g.setColor(cp.getColor());
                            }

                        } else {
                            g.setColor(Theme.getColor("GRAPH_DOWN_COLOR"));
                        }*/
                        if (!cp.isUsingSameColor()) {
                            g.setColor(cp.getWarningColor());
                        }
                        //MACD etc. goes here.
                        prevY = Integer.MAX_VALUE;
                        for (int i = 0; i < len; i++) {
                            if (prevY < yInd[i]) {
                                int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                                g.fillRect(((int[]) faTime[j])[i] - dashLength, Math.min(yInd[i], zero),
                                        barThickness, Math.abs(bottom - yInd[i]));
                            }
                            prevY = yInd[i];
                        }
                    }
                    /***************************************************************NetOrders *****************************/


                } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_PERCET) {

                    int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
                    int hundred = getPixelFortheYValue(100d, graph.getIDforPanel(r));
                    //+ values
                    GraphCalcTypeApplicable netCP = (GraphCalcTypeApplicable) cp;


                    if (netCP.getCalcType() == NetTurnOverInd.POINTS) {
                        g.setColor(Color.green);
                        for (int i = 0; i < len; i++) {
                            if (yInd[i] < zero) {   //real y value greater than zero
                                int bottom = Math.min(zero, r.y + r.height - clearance + barExt);

                                g.fillRect(((int[]) faTime[j])[i] - dashLength, Math.min(yInd[i], zero),
                                        barThickness, Math.abs(bottom - yInd[i]));
                            }
                        }

                        g.setColor(Color.red);
                        //- values
                        for (int i = 0; i < len; i++) {
                            if (yInd[i] > zero) { //real y value less than zero
                                int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                                g.fillRect(((int[]) faTime[j])[i] - dashLength, Math.min(yInd[i], zero),
                                        barThickness, Math.abs(bottom - yInd[i]));
                            }

                        }
                    } else {//calc type NetTurnOverInd.PERCENT

                        for (int i = 0; i < len; i++) {
                            int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                            g.setColor(Color.red);
                            int bidPercent = getPixelFortheYValue(yInd[i] / 100d, graph.getIDforPanel(r));
                            g.fillRect(((int[]) faTime[j])[i] - dashLength, hundred,
                                    barThickness, bottom - hundred);
                            g.setColor(Color.green);
                            g.fillRect(((int[]) faTime[j])[i] - dashLength, bidPercent,
                                    barThickness, Math.abs(bottom - bidPercent));

                            double percent = yInd[i] / 100d;
                            Graphics2D gg = (Graphics2D) g;
                            gg.translate(r.x, r.y);
                            gg.rotate(Math.toRadians(90));
                            g.setColor(Color.black);
                            int fifty = getPixelFortheYValue(62d, graph.getIDforPanel(r));
                            gg.drawString(cashFlowformatter.format(percent) + "%", -(r.y - fifty), r.x - (((int[]) faTime[j])[i]));
                            /************************************sofat corrct one*/
                            gg.rotate(Math.toRadians(270));
                            gg.translate(-r.x, -r.y);
                        }
                    }

                } else if ((len > 1) && (yInd != null) && (yInd.length > 1)) {
                    if (cp.isUsingSameColor() || (cp.getColor().equals(cp.getWarningColor()))) {
                        g.drawPolyline((int[]) faTime[j], yInd, len);

                    } else {
                        g.setColor(cp.getWarningColor());
                        int prevY = Integer.MAX_VALUE;
                        for (int i = 0; i < len; i++) {
                            if (prevY < yInd[i]) {
                                g.drawLine(((int[]) faTime[j])[i], yInd[i],
                                        ((int[]) faTime[j])[i - 1], yInd[i - 1]);
                            }
                            prevY = yInd[i];
                        }
                        g.setColor(cp.getColor());
                        prevY = Integer.MIN_VALUE;
                        for (int i = 0; i < len; i++) {
                            if (prevY >= yInd[i]) {
                                g.drawLine(((int[]) faTime[j])[i], yInd[i],
                                        ((int[]) faTime[j])[i - 1], yInd[i - 1]);
                            }
                            prevY = yInd[i];
                        }
                    }
                }
                ///////////////////////////////////////////////
                //Ichimoku Kinko Hyo Cloud
                if (cp.getID() == ID_ICHIMOKU_KINKO_HYO) {
                    try {
                        IchimokuKinkoHyo ich = (IchimokuKinkoHyo) cp;
                        if (ich.getStyle() == IchimokuKinkoHyo.SENKOU_SPAN_B) {
                            int spanAIndex = getSenkouSpanA(ich);
                            int[] timeA = (int[]) faTime[spanAIndex];
                            int[] timeB = (int[]) faTime[j];
                            int[] yIndA = (int[]) faOpen[spanAIndex];
                            int[] yIndB = (int[]) faOpen[j];
                            float[] dashArr = {1, 1, 1, 1};
                            BasicStroke bs = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER,
                                    10f, dashArr, 0);
                            ((Graphics2D) g).setStroke(bs);
                            for (int i = 0; i < timeB.length; i++) {
                                int k = timeB.length - 1 - i;
                                int m = timeA.length - 1 - i;
                                g.setColor((yIndA[m] > yIndB[k]) ? ich.getDownCloudColor() : ich.getUpCloudColor());
                                g.drawLine(timeA[m], yIndA[m], timeB[k], yIndB[k]);
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE) {
                int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
                if ((len > 1) && (yPrio != null) && (yPrio.length > 1)) {
                    if (cp.isUsingSameColor() || (cp.getColor().equals(cp.getWarningColor()))) {
                        g.drawPolyline((int[]) faTime[j], yPrio, len);
                    } else {
                        g.setColor(cp.getWarningColor());
                        int prevY = Integer.MAX_VALUE;
                        for (int i = 0; i < len; i++) {
                            if (prevY < yPrio[i]) {
                                g.drawLine(((int[]) faTime[j])[i], yPrio[i],
                                        ((int[]) faTime[j])[i - 1], yPrio[i - 1]);
                            }
                            prevY = yPrio[i];
                        }
                        g.setColor(cp.getColor());
                        prevY = Integer.MIN_VALUE;
                        for (int i = 0; i < len; i++) {
                            if (prevY >= yPrio[i]) {
                                g.drawLine(((int[]) faTime[j])[i], yPrio[i],
                                        ((int[]) faTime[j])[i - 1], yPrio[i - 1]);
                            }
                            prevY = yPrio[i];
                        }

                    }
                }
            } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR) {
                int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
//                int prevY = Integer.MAX_VALUE;
                int prevY = cp.isFirstCandleGreen ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
                for (int i = 0; i < len; i++) {
                    if (prevY >= yPrio[i]) {
                        int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                        g.fillRect(((int[]) faTime[j])[i] - dashLength, yPrio[i],
                                barThickness, bottom - yPrio[i]);
                    }
                    prevY = yPrio[i];
                }
                if (!cp.isUsingSameColor())
                    g.setColor(cp.getWarningColor());
//                prevY = Integer.MAX_VALUE;
                prevY = cp.isFirstCandleGreen ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                for (int i = 0; i < len; i++) {
                    if (prevY < yPrio[i]) {
                        int bottom = Math.min(zero, r.y + r.height - clearance + barExt);
                        g.fillRect(((int[]) faTime[j])[i] - dashLength, yPrio[i],
                                barThickness, bottom - yPrio[i]);
                    }
                    prevY = yPrio[i];
                }
            } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_HLC) {
                int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
                int x, prevY;
//                prevY = Integer.MAX_VALUE;
                prevY = cp.isFirstCandleGreen ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                g.setColor(cp.getColor());
                ((Graphics2D) g).setStroke(dashPen); //graph.BS_0p8f_solid
                for (int i = 0; i < len; i++) {
                    if (prevY >= yPrio[i]) {
                        x = ((int[]) faTime[j])[i];
                        g.drawLine(x, ((int[]) faHigh[j])[i], x, ((int[]) faLow[j])[i]);
                        g.drawLine(x, ((int[]) faClose[j])[i], x + dashLength, ((int[]) faClose[j])[i]);
                    }
                    prevY = yPrio[i];
                }
//                prevY = Integer.MAX_VALUE;
                prevY = cp.isFirstCandleGreen ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                if (!cp.isUsingSameColor())
                    g.setColor(cp.getWarningColor());
                for (int i = 0; i < len; i++) {
                    if (prevY < yPrio[i]) {
                        x = ((int[]) faTime[j])[i];
                        g.drawLine(x, ((int[]) faHigh[j])[i], x, ((int[]) faLow[j])[i]);
                        g.drawLine(x, ((int[]) faClose[j])[i], x + dashLength, ((int[]) faClose[j])[i]);
                    }
                    prevY = yPrio[i];
                }
            } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_OHLC) {
                int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
                int x, prevY;

//                prevY = Integer.MAX_VALUE;

                prevY = cp.isFirstCandleGreen ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                g.setColor(cp.getColor());
                ((Graphics2D) g).setStroke(dashPen); //graph.BS_0p8f_solid
                for (int i = 0; i < len; i++) {
                    if (prevY >= yPrio[i]) {
                        x = ((int[]) faTime[j])[i];
                        g.drawLine(x, ((int[]) faHigh[j])[i], x, ((int[]) faLow[j])[i]);
                        g.drawLine(x, ((int[]) faClose[j])[i], x + dashLength, ((int[]) faClose[j])[i]);
                        g.drawLine(x - dashLength, ((int[]) faOpen[j])[i], x, ((int[]) faOpen[j])[i]);
                    }
                    prevY = yPrio[i];
                }
//                prevY = Integer.MAX_VALUE;
                prevY = cp.isFirstCandleGreen ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                if (!cp.isUsingSameColor())
                    g.setColor(cp.getWarningColor());
                for (int i = 0; i < len; i++) {
                    if (prevY < yPrio[i]) {
                        x = ((int[]) faTime[j])[i];
                        g.drawLine(x, ((int[]) faHigh[j])[i], x, ((int[]) faLow[j])[i]);
                        g.drawLine(x, ((int[]) faClose[j])[i], x + dashLength, ((int[]) faClose[j])[i]);
                        g.drawLine(x - dashLength, ((int[]) faOpen[j])[i], x, ((int[]) faOpen[j])[i]);
                    }
                    prevY = yPrio[i];
                }
            } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_CANDLE) {

                int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
                int x, O, H, L, C;
                int prevY = Integer.MAX_VALUE;
                ((Graphics2D) g).setStroke(dashPen); //graph.BS_0p8f_solid
                /*((Graphics2D) g).setRenderingHint(RenderingHints.KEY_STROKE_CONTROL,
                        RenderingHints.VALUE_STROKE_PURE);*/
                for (int i = 0; i < len; i++) {

                    if (i == 0) {
                        prevY = yPrio[0];
                    } else {
//                        prevY = yPrio[i-1];

                    }
                    if (!cp.isUsingSameColor()) {
                        if (prevY < yPrio[i]) {
                            g.setColor(cp.getWarningColor());
                        } else {
                            g.setColor(cp.getColor());
                        }

                        // temp patch for first candle always green issue - Mevan
                        if (i == 0) {
                            g.setColor(cp.isFirstCandleGreen ? cp.getColor() : cp.getWarningColor());
                        }
                    }
                    if (faTime[j] == null) continue;
                    x = ((int[]) faTime[j])[i];
                    O = ((int[]) faOpen[j])[i];
                    H = ((int[]) faHigh[j])[i];
                    L = ((int[]) faLow[j])[i];
                    C = ((int[]) faClose[j])[i];
                    if (O == C) {
                        g.drawLine(x, H, x, L);
                        g.drawLine(x - dashLength, O, x - dashLength + barThickness, O);
                    } else if (O > C) {
                        g.drawLine(x, H, x, C);
                        g.drawLine(x, O, x, L);
                        g.drawRect(x - dashLength, C, barThickness, O - C);
                    } else {
                        g.drawLine(x, H, x, O);
                        g.drawLine(x, C, x, L);
                        g.fillRect(x - dashLength, O, barThickness, C - O);
                        g.drawRect(x - dashLength, O, barThickness, C - O);
                    }
                    prevY = yPrio[i];
                }
            }
            //################## Announcements #########################
            if (showAnnouncements || showSplits) {
                if (cp.getID() <= GraphDataManager.ID_COMPARE) {
                    int[] iaSplits = (int[]) faAnnTime[j];
                    if ((iaSplits != null) && (iaSplits.length > 0)) {
                        for (int i = 0; i < iaSplits.length; i++) {
                            int split = -99999;
                            try {

                                //int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
                                int[] yPrio = null;
                                if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR ||
                                        cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE) {  //for line and bar style
                                    yPrio = getPriotityArray(cp.getOHLCPriority(), j);

                                } else {
                                    yPrio = getPriotityArray(GraphDataManager.INNER_High, j); // for CANDLE, HLC, and OHLC
                                }
                                split = iaSplits[i];
                                int x = ((int[]) faTime[j])[split];
                                int y = yPrio[iaSplits[i]];
                                int type = ((int[]) faAnnGrp[j])[i];
                                drawAnnouncement(g, x, y, type);
                            } catch (Exception ex) {
//                                System.out.println("error causing split index: " + i + " split: " + split);
                            }
                        }
                    }
                }
            }
            //################## End Announcements #####################

            g.setClip(null);

            if (cp.isSelected() && !cp.isHidden()) {
                drawChartSelected(g, j, cp);
            }
        }
    }

    private void drawAnnouncement(Graphics g, int x, int y, int type) {

        int ht = 16, gap = 2; //TODO:make this 4 if needed
        Image img = getAlertImgForType(type);
        g.drawImage(img, x - img.getWidth(graph) / 2, y - gap - ht, graph);
    }

    private Image getAlertImgForType(int type) {
        try {
            boolean hasAnn = (type & GraphDataManager.ALERT_ANNOUNCEMENT) == GraphDataManager.ALERT_ANNOUNCEMENT;
            if ((type == GraphDataManager.ALERT_ANNOUNCEMENT) || (hasAnn && !showSplits)) {
                return showAnnouncements ? imgAnnouncement : null;
            }
            if (!showAnnouncements) type = type & (~GraphDataManager.ALERT_ANNOUNCEMENT);
            if (showSplits && (type > 1)) {  //hasSplits
                Image[] imgs = {imgStockSplit, imgStockDividend, imgRightsIssue, imgReverseSplit, imgCapitalReduction, imgCashDividend};
                Image[] plusImags = {imgStockSplitPlus, imgStockDividendPlus, imgRightsIssuePlus, imgReverseSplitPlus, imgCapitalReductionPlus, imgCashDividendPlus};
                for (int i = 1; i < 7; i++) {
                    Image img = getImageForType(type, 1 << i, imgs[i - 1], plusImags[i - 1]);
                    if (img != null) return img;
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getStackTrace());
        }
        return null;
    }

    private Image getImageForType(int type, int checkType, Image img, Image imgPlus) {
        if ((type & checkType) == checkType) {
            return (type == checkType) ? img : imgPlus;
        }
        return null;
    }

    private int getSenkouSpanA(IchimokuKinkoHyo ich) {
        long grpID = ich.getGrpID();
        for (int j = 0; j < sourceCount; j++) {
            ChartProperties cp = (ChartProperties) Sources.get(j);
            if (cp.getID() == ID_ICHIMOKU_KINKO_HYO) {
                IchimokuKinkoHyo ich2 = (IchimokuKinkoHyo) cp;
                if ((ich2.getGroupID().equals(ich.getGroupID())) && (ich2.getStyle() == IchimokuKinkoHyo.SENKOU_SPAN_A)) {
                    return j;
                }
            }
        }
        return -1;
    }

    private int[] getPriotityArray(byte p, int graphInd) {
        switch (p) {
            case INNER_Open:
                return (int[]) faOpen[graphInd];
            case INNER_High:
                return (int[]) faHigh[graphInd];
            case INNER_Low:
                return (int[]) faLow[graphInd];
            default:
                return (int[]) faClose[graphInd];
        }
    }

    private void drawChartSelected(Graphics g, int j, ChartProperties cp) {
        int[] xArr = (int[]) faTime[j];
        int[] yArr = null;
        int len = xArr.length;
        if (cp.getID() > ID_COMPARE) {
            yArr = (int[]) faOpen[j];
        } else {
            yArr = getPriotityArray(cp.getOHLCPriority(), j);
        }
        if ((len > 1) && (yArr != null) && (yArr.length > 1)) {
            int lastX = -1000;
            g.setClip(cp.getRect());
            ((Graphics2D) g).setStroke(new BasicStroke(1f));
            if ((cp.getID() == GraphDataManager.ID_CUSTOM_INDICATOR) && ((IndicatorBase) cp).isDrawSymbol()) {
                for (int i = 0; i < len; i++) {
                    g.setColor(cp.getColor());
                    g.drawRect(xArr[i] - 8, yArr[i] - 8, 16, 16);
                }
            } else {
                g.setXORMode(Color.WHITE);
                for (int i = 0; i < len; i++) {
                    if (lastX + 40 <= xArr[i]) {
                        lastX = xArr[i];
                        g.setColor(cp.getColor());
                        g.fillRect(xArr[i] - 2, yArr[i] - 2, 5, 5);
                        g.setColor(graph.chartSelectedColor);
                        g.drawRect(xArr[i] - 3, yArr[i] - 3, 6, 6);
                    }
                }
            }
            g.setClip(null);
            g.setPaintMode();
        }
    }

    private void drawObjects(Graphics g, boolean isPrinting, boolean isOnbackGround) {
        if (ObjectArray != null) {
            for (int i = 0; i < ObjectArray.size(); i++) {
                AbstractObject oR = (AbstractObject) ObjectArray.get(i);
                if (oR.isDrawingOnBackground() != isOnbackGround) continue;
                if ((oR.getRect() == graph.volumeRect) && !graph.isShowingVolumeGrf() && !oR.isShownOnAllPanels())
                    continue;
                if ((oR.getRect() == graph.turnOverRect) && !graph.isShowingTurnOverGraph() && !oR.isShownOnAllPanels())
                    continue;
                try {
                    float[] xArr = null;
                    if (oR.isFreeStyle()) {
                        xArr = oR.getIndexArray();
                    } else {
                        xArr = convertXArrayTimeToIndex(oR.getXArr());
                    }
                    if (oR.isShownOnAllPanels()) {
                        oR.drawOnGraphics(g, xArr, isPrinting, beginIndex, 0, xFactor, 0, endIndex);
                    } else {
                        int pnlID = getIndexOfTheRect(graph.panels, oR.getRect())/*oR.pnlID*/; //TODO
                        oR.drawOnGraphics(g, xArr, isPrinting, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], endIndex);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        if (StaticObjectArray != null) {
            for (int i = 0; i < StaticObjectArray.size(); i++) {
                AbstractObject oR = (AbstractObject) StaticObjectArray.get(i);
                if (oR.isDrawingOnBackground() != isOnbackGround) continue;
                if ((oR.getRect() == graph.volumeRect) && !graph.isShowingVolumeGrf()) continue;
                int pnlID = getIndexOfTheRect(graph.panels, oR.getRect());
                try {
                    float[] xArr = convertXArrayTimeToIndex(oR.getXArr());

                    if ((sourceCount >= 1) && (graphStoreSize >= 1) && (sourceCount >= Sources.size())) { //BUG fix for showing -9999.0 lines
                        double[] yArr = oR.getYArr();
                        if (yArr[0] != HORIZ_LINE_INITIAL_VALUE) {  //to fix the bug showing -9999 value - sathyajith //draw only when the lines should shown
                            oR.drawOnGraphics(g, xArr, isPrinting, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], endIndex);
                        }
                    }
                } catch (Exception ex) {
                }
            }
        }
    }

    public void setActiveObjectIndex(int x, int y, boolean unselect) {
        objectHiLighted = -1;
        sourceHiLighted = -1;
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            cp.setSelected(false);
        }
        //Checking for symbols
        if ((!unselect) && isOnaLine(x, y, true, true)) {
            unselect = true;
        }
        //Checking for objects
        if (ObjectArray != null) {
            for (int i = ObjectArray.size() - 1; i >= 0; i--) {
                AbstractObject oR = (AbstractObject) ObjectArray.get(i);
                int pnlID = getIndexOfTheRect(graph.panels, oR.getRect());
                oR.setSelected(false);
                if (!unselect) {
                    //float[] xArr = convertXArrayTimeToIndex(oR.getXArr());
                    float[] xArr = null;
                    if (oR.isFreeStyle()) {
                        xArr = oR.getIndexArray();
                    } else {
                        xArr = convertXArrayTimeToIndex(oR.getXArr());
                    }
                    boolean isOnObj = false;
                    if (oR.isShownOnAllPanels()) {
                        isOnObj = oR.isCursorOnObject(x, y, xArr, true, beginIndex, 0, xFactor, 0, true);
                    } else if (pnlID > -1) {
                        isOnObj = oR.isCursorOnObject(x, y, xArr, true, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], true);
                    }
                    if (isOnObj) {
                        objectHiLighted = i;
                        unselect = true;
                    }
                }
            }
        }
        graph.repaint();
    }

    public void setActiveObjectIndexTest(int x, int y, boolean unselect) {

        objectHiLighted = -1;

        //Checking for symbols
        if ((!unselect) && isOnaLine(x, y, true, false)) {
            unselect = true;
        }
        //Checking for objects
        if (ObjectArray != null) {
            for (int i = ObjectArray.size() - 1; i >= 0; i--) {
                AbstractObject oR = (AbstractObject) ObjectArray.get(i);
                int pnlID = getIndexOfTheRect(graph.panels, oR.getRect());
                //oR.setSelected(false);

                if (!unselect) {
                    //float[] xArr = convertXArrayTimeToIndex(oR.getXArr());
                    float[] xArr = null;
                    if (oR.isFreeStyle()) {
                        xArr = oR.getIndexArray();
                    } else {
                        xArr = convertXArrayTimeToIndex(oR.getXArr());
                    }
                    boolean isOnObj = false;
                    if (oR.isShownOnAllPanels()) {
                        isOnObj = oR.isCursorOnObject(x, y, xArr, true, beginIndex, 0, xFactor, 0, false);
                    } else if (pnlID > -1) {
                        isOnObj = oR.isCursorOnObject(x, y, xArr, true, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], false);
                    }
                    if (isOnObj) {
                        if (oR.isSelected()) {
                            oR.setSelected(false);
                        } else {
                            oR.setSelected(true);
                        }
                    }
                }
            }
        }
        graph.repaint();
    }

    public int isOnAnAnnouncement(int xT, int yT, boolean isMove) {
        if ((showAnnouncements || showSplits) && faAnnTime != null) {
            for (int j = 0; j < Sources.size(); j++) {
                ChartProperties cp = (ChartProperties) Sources.get(j);
                if (cp.getID() <= ID_COMPARE) {
                    int[] iaSplits = (int[]) faAnnTime[j];
                    if (iaSplits != null) {
                        for (int i = 0; i < iaSplits.length; i++) {
                            int type = ((int[]) faAnnGrp[j])[i];
                            if ((type == 1) && (!showAnnouncements)) continue;
                            if ((type == 2) && (!showSplits)) continue;
                            //int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
                            int[] yPrio = null;
                            if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR ||
                                    cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE) {  //for line and bar style
                                yPrio = getPriotityArray(cp.getOHLCPriority(), j);

                            } else {
                                yPrio = getPriotityArray(GraphDataManager.INNER_High, j); // for CANDLE, HLC, and OHLC
                            }
                            int x = ((int[]) faTime[j])[iaSplits[i]];
                            int y = yPrio[iaSplits[i]];
                            if (isOnAnnFlag(xT, yT, x, y, type) && showAnnouncements) {
                                int xIndex = Math.min(Math.round(getIndexForthePixel(x)), graphStore.size() - 1);
                                long time = getTimeMillisec(xIndex);
                                graph.setTimeValue(time);
                                if (isMove) {
                                    graph.updateToolTip(true, 1, null, isMove, "");
                                } else {
                                    graph.updateToolTip(false, 1, null, !isMove, "");
                                }
                                return j;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }

    public int isOnASplit(int xT, int yT) {
        try {
            if (showSplits) {
                for (int j = 0; j < Sources.size(); j++) {
                    ChartProperties cp = (ChartProperties) Sources.get(j);
                    if (cp.getID() <= ID_COMPARE) {
                        int[] iaSplits = (int[]) faAnnTime[j];
                        for (int i = 0; i < iaSplits.length; i++) {
                            int type = ((int[]) faAnnGrp[j])[i];
                            if (type < 2) continue;
                            int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
                            int x = ((int[]) faTime[j])[iaSplits[i]];
                            int y = yPrio[iaSplits[i]];
                            if (isOnSplitFlag(xT, yT, x, y, type)) {
                                return j;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return -1;
    }

    public long getAnnouncementDate(int xT, int yT, int j) {
        int indAnn = -1;
        ChartProperties cp = (ChartProperties) Sources.get(j);
        int[] iaSplits = (int[]) faAnnTime[j];
        for (int i = 0; i < iaSplits.length; i++) {
            //int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);//earlier
            int[] yPrio = null;
            if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR ||
                    cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE) {  //for line and bar style
                yPrio = getPriotityArray(cp.getOHLCPriority(), j);

            } else {
                yPrio = getPriotityArray(GraphDataManager.INNER_High, j); // for CANDLE, HLC, and OHLC
            }
            int x = ((int[]) faTime[j])[iaSplits[i]];
            int y = yPrio[iaSplits[i]];
            int type = ((int[]) faAnnGrp[j])[i];
            if (isOnAnnFlag(xT, yT, x, y, type)) {
                indAnn = ((int[]) faAnnTimeIndex[j])[i];
                break;
            }
        }
        if (indAnn < 0) {
            return -1;
        } else {
            return getTimeMillisec(indAnn);
        }
    }

    private boolean isOnAnnFlag(int xT, int yT, int x, int y, int type) {

        /*if (type == 2) return false;  //stock split
        if (type == 4) return false;   //stock dividend
        if (type == 64) return false; // cach dividend
        if (type == 8) return false; // Right issue*/

        if ((type & GraphDataManager.ALERT_ANNOUNCEMENT) != 1) return false;

        //for delta
        int len = 5, ht = 15, gap = 4;
        int[] xPoints = {x, x - len, x + len};
        int[] yPoints = {y - gap, y - gap - ht, y - gap - ht};
        Polygon p = new Polygon(xPoints, yPoints, 3);
        if (p.contains(xT, yT)) {
            return true;
        }

        return false;
    }

    private boolean isOnSplitFlag(int xT, int yT, int x, int y, int type) {
        //for img
        Image img = imgStockSplit;
        int ht = 16, gap = 2, len = img.getWidth(graph) / 2;
        int[] xPoints = {x, x - len, x - len, x + len, x + len};
        int[] yPoints = {y - gap, y - gap - ht / 2, y - gap - ht, y - gap - ht, y - gap - ht / 2};
        Polygon p = new Polygon(xPoints, yPoints, 5);
        if (p.contains(xT, yT)) return true;
        return false;
    }

    public long getSplitDate(int xT, int yT, int j) {
        int indSplit = -1;
        ChartProperties cp = (ChartProperties) Sources.get(j);
        int[] iaSplits = (int[]) faAnnTime[j];
        for (int i = 0; i < iaSplits.length; i++) {
            int[] yPrio = getPriotityArray(cp.getOHLCPriority(), j);
            int x = ((int[]) faTime[j])[iaSplits[i]];
            int y = yPrio[iaSplits[i]];
            int type = ((int[]) faAnnGrp[j])[i];
            if (isOnSplitFlag(xT, yT, x, y, type)) {
                indSplit = ((int[]) faAnnTimeIndex[j])[i];
                break;
            }
        }
        if (indSplit < 0) {
            return -1;
        } else {
            return getTimeMillisec(indSplit);
        }
    }

    public void popActiveObjectProperties() {

        //no need to show the property dialog for the net orders and net turnover. hence skip those two
        if ((sourceHiLighted >= 0) && (sourceHiLighted < Sources.size())) {

            ChartProperties cp = (ChartProperties) Sources.get(sourceHiLighted);
            PropertyDialog pd = new PropertyDialog(ChartInterface.getChartFrameParentComponent(), cp, EDIT_PROPERTY, graph, false);

            pd.setLocationRelativeTo(graph);
            pd.setModal(true);
            pd.setVisible(true);
            if (pd.getModalResult()) {
                pd.setVisible(false);
                cp.assignValuesFrom(pd.getTargetCP());
                WindowPanel r = cp.getRect();
                if (r != null) {
                    if (cp.getID() > GraphDataManager.ID_COMPARE) {
                        Indicator ind = (Indicator) cp;
                        if (ind.getInnerSource() == getBaseCP()) {
                            r.setTitle(cp.toString());
                        }
                    }
                }
                if (cp == getBaseCP()) {
                    graph.setGraphStyle(cp.getChartStyle());
                    ChartFrame.getSharedInstance().refreshTopToolbar();
                }
                if (cp.getID() > ID_COMPARE) {
                    synchronized (graphLock) {
                        cp.setRecordSize(0);
                        ChartProperties inner;
                        for (int i = 0; i < Sources.size(); i++) {
                            inner = (ChartProperties) Sources.get(i);
                            if (inner instanceof Indicator) {
                                inner.setRecordSize(0);
                            }
                        }
                    }
                    updateDataSources();
                }
                if (cp.isIndicator && cp.isUsingUserDefault) {
                    IndicatorPropertyStore.getSharedInstance().saveDefaultProperties(cp);
                }
            } else {
                ChartProperties target = pd.getTempCP();
                PropertyDialogFactory.dynamicChangeChartProperties(cp, target, graph);
            }

            if (pd.getModalDialgResult() == PropertyDialog.DIALOG_RESULT_RESET) {       //user selects reset button
                ChartProperties target = pd.getTempCP();
                target.assignDefaultValues();
                PropertyDialogFactory.dynamicChangeChartProperties(cp, target, graph);
                IndicatorPropertyStore.getSharedInstance().deleteDefaultProperties(target);
            }

            pd.setTargetCP(null);
            pd.dispose();
            pd = null;
            graph.reDrawAll();
        } else if ((objectHiLighted > -1) && (objectHiLighted < ObjectArray.size())) {
            AbstractObject oR = (AbstractObject) ObjectArray.get(objectHiLighted);
            PropertyDialog pd = new PropertyDialog(ChartInterface.getChartFrameParentComponent(), oR, graph);
            pd.setLocationRelativeTo(graph);
            Hashtable tempHash = new Hashtable();

            if (oR.getObjType() == AbstractObject.INT_FIBONACCI_RETRACEMENTS) {
                ObjectFibonacciRetracements fibonacciRetracements = (ObjectFibonacciRetracements) oR;
                for (int i = 0; i < fibonacciRetracements.getFibonacciLines().size(); i++) {
                    FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciRetracements.getFibonacciLines().get(i);
                    FibonacciRetracementiLine tmpLine = new FibonacciRetracementiLine();
                    tmpLine.setEnabled(line.isEnabled());
                    tmpLine.setLineColor(line.getLineColor());
                    tmpLine.setPenStyle(line.getPenStyle());
                    tmpLine.setPenWidth(line.getPenWidth());
                    tmpLine.setPercentageValue(line.getPercentageValue());
                    tempHash.put(i, tmpLine);
                }
            } else if (oR.getObjType() == AbstractObject.INT_FIBONACCI_EXTENSIONS) {
                ObjectFibonacciExtensions fibonacciExts = (ObjectFibonacciExtensions) oR;
                for (int i = 0; i < fibonacciExts.getFibonacciLines().size(); i++) {
                    FibonacciExtensionLine line = (FibonacciExtensionLine) fibonacciExts.getFibonacciLines().get(i);
                    FibonacciExtensionLine tmpLine = new FibonacciExtensionLine();
                    tmpLine.setEnabled(line.isEnabled());
                    tmpLine.setLineColor(line.getLineColor());
                    tmpLine.setPenStyle(line.getPenStyle());
                    tmpLine.setPenWidth(line.getPenWidth());
                    tmpLine.setPercentageValue(line.getPercentageValue());
                    tempHash.put(i, tmpLine);
                }
            }

            if (oR.DISPLAY_TYPE != AbstractObject.DISPLAY_SATATIC) {
                pd.setModal(true);
                pd.setVisible(true);

                if (pd.getModalDialgResult() == PropertyDialog.DIALOG_RESULT_OK) {
                    pd.setVisible(false);
                    oR.assignValuesFrom(pd.getTargetOR());
                    pd.setTargetCP(null);
                    pd.dispose();
                    pd = null;
                    graph.repaint();

                    if (oR.isUsingUserDefault()) {
                        ObjectPropertyStore.getSharedInstance().saveDefaultProperties(oR);
                    }

                } else if (pd.getModalDialgResult() == PropertyDialog.DIALOG_RESULT_CANCEL) {       //user selects cancel button
                    AbstractObject target = pd.getTempOR();

                    if (target.getObjType() == AbstractObject.INT_FIBONACCI_RETRACEMENTS) {

                        ObjectFibonacciRetracements fibonacciRetracements = (ObjectFibonacciRetracements) target;
                        fibonacciRetracements.getFibonacciLines().clear();
                        fibonacciRetracements.setFibonacciLines(tempHash);
                    } else if (target.getObjType() == AbstractObject.INT_FIBONACCI_EXTENSIONS) {

                        ObjectFibonacciExtensions fibonacciExts = (ObjectFibonacciExtensions) target;
                        fibonacciExts.getFibonacciLines().clear();
                        fibonacciExts.setFibonacciLines(tempHash);
                    }

                    PropertyDialogFactory.dynamicChangeObjectProperties(oR, target, graph);
                } else if (pd.getModalDialgResult() == PropertyDialog.DIALOG_RESULT_RESET) {       //user selects reset button
                    ObjectPropertyStore.getSharedInstance().deleteDefaultProperties(oR);
                    AbstractObject target = pd.getTempOR();
                    target.assignDefaultValues();
                    PropertyDialogFactory.dynamicChangeObjectProperties(oR, target, graph);
                }
            }
        }/* else {
            if (!isDetached) {  //cahs flow is already detached
                graph.getPanelIDatPoint()
                AnalysisTechWindow.getSharedInstance().setVisible(true);
            }
        }*/
    }

    public String getVolumeGraphTitle() {
        String bg = StockGraph.extractSymbolFromStr(getBaseGraph());
        String result = Language.getString("VOLUME") + " ";
        if (bg != null) result += "(" + bg + ") ";
        return result;
    }

    public String getTurnOverGraphTitle() {
        String bg = StockGraph.extractSymbolFromStr(getBaseGraph());
        String result = Language.getString("GRAPH_TURNOVER") + " ";
        if (bg != null) result += "(" + bg + ") ";
        return result;
    }

    public String getMainGraphTitle() {

        //changed by charithn
        boolean isMode = ChartOptions.getCurrentOptions().chart_main_title[0];
        boolean isPeriod = ChartOptions.getCurrentOptions().chart_main_title[1];
        boolean isInterval = ChartOptions.getCurrentOptions().chart_main_title[2];

        return getMainGraphTitle(isMode, isPeriod, isInterval);
    }

    public String getMainGraphTitle(boolean isMode, boolean isPeriod, boolean isInterval) {
        String bg = getBaseGraph();
        if ((bg == null) || (bg.trim().equals(""))) {
            return " ";
        }

        long beginTime = getTimeMillisec(Math.round(getBeginIndex()));
        long endTime = getTimeMillisec(Math.min(getLastNonZeroIndex(0), Math.round(getEndIndex())));

        String result = ChartInterface.getTitleStr(bg, graph.isCurrentMode(),
                interval, getGraphStyle(), beginTime, endTime, isMode, isPeriod, isInterval);

        return result;
    }

    public void drawGraphsOnGraphics(Graphics g, boolean isPrinting) {

        try {
            if ((sourceCount < 1) || (sourceCount < Sources.size())) return;
            if (sourceCount > Sources.size()) sourceCount = Sources.size();
            drawVolumeByPrice(g, isPrinting);
            //corresponds to filling the regions bounded by the indicators with upper and lower bands
            drawHighlightedObjects(g, isPrinting);
            drawObjects(g, isPrinting, true);
            drawPointsOnGraphics(g, isPrinting);
            drawObjects(g, isPrinting, false);
            drawPriceBall(g);
            drawVerticalLine(g);
            drawFibCalcPoints(g);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private int getIndexForPrice(double min, double max, int bars, double priceValue) {

        double gap = (max - min) / bars;

        int index = 0;
        for (double i = min; i < max; i = i + gap) {

            if (priceValue >= i && priceValue < (i + gap)) {
                break;
            }

            index++;
        }
        if (index > bars - 1) {
            return (bars - 1);
        }
        return index;
    }

    //added by charithn
    public void drawVolumeByPrice(Graphics g, boolean isPrinting) {

        if (showVolumeByPrice) {

            if (volumeByPriceType == VOL_BY_PRICE_NORMAL) {
                drawVolumeByPriceTotal(g, isPrinting);
            } else if (volumeByPriceType == VOL_BY_PRICE_NET_CHANGE || volumeByPriceType == VOL_BY_PRICE_PCT_CHANGE) {
                drawVolumeByPriceByChange(g, isPrinting);
            }
        }
    }

    public void drawVolumeByPriceByChange(Graphics g, boolean isPrinting) {

        Color barFillColor = new Color(250, 128, 100, 80);
        Color barOutLineColor = new Color(250, 128, 10, 200);
        Font labelFont = new Font("Arial", Font.BOLD, 10);
        WindowPanel r = graph.mainRect;
        int panelID = getIndexOfTheRect(graph.panels, r);

        g.setClip(r.x, r.y, r.width, r.height);
        g.setFont(labelFont);

        double[] semiLogPitch = new double[1];
        float height = r.height - titleHeight;
        double bestPitch = getBestYInterval(panelID, height - 2 * clearance, semiLogPitch);
        double allowedPitch = ((r.isSemiLog()) || (yFactor[panelID] == 0)) ? bestPitch : getAllowedPitch(bestPitch, height - 2 * clearance, yFactor[panelID]);
        double beginVal = calculateBeginValue(minY[panelID], allowedPitch);
        double endVal = maxY[panelID] + allowedPitch;
        //int BARS = (int) ((endVal - beginVal) / allowedPitch);
        int BARS = (int) Math.ceil((endVal - beginVal) / allowedPitch);
        double[] volumeArray = new double[BARS];
        double[] positiveVolumeArray = new double[BARS];
        double maxVolume = -Double.MIN_VALUE;
        double minVolume = Double.MAX_VALUE;

        if (graphStore != null && graphStore.size() == 0) return;
        //splitting the volumes in to different price ranges
        if (graphStore != null && graphStore.size() > 0) {

            for (int i = (int) beginIndex; i < endIndex; i++) {
                Object[] objects = (Object[]) graphStore.get(i);
                ChartPoint point = (ChartPoint) objects[1];
                if (point != null) {
                    double price = point.getValue(getBaseCP().getOHLCPriority());
                    double volume = point.getValue(INNER_Volume);
                    int index = getIndexForPrice(beginVal, endVal, BARS, price);

                    double newVolume = Double.parseDouble(String.valueOf(volumeArray[index])) + volume;
                    volumeArray[index] = newVolume;

                    //calculating the positive volumes -corresponding to + change..
                    Object[] preObjects = null;
                    if (i < graphStore.size() && i > 0) {
                        preObjects = (Object[]) graphStore.get(i - 1);

                        if (preObjects != null) {
                            ChartPoint prePoint = (ChartPoint) preObjects[1];

                            if ((point != null && prePoint != null) && point.getValue(getBaseCP().getOHLCPriority()) > prePoint.getValue(getBaseCP().getOHLCPriority())) {
                                volume = point.getValue(INNER_Volume);
                                newVolume = Double.parseDouble(String.valueOf(positiveVolumeArray[index])) + volume;
                                positiveVolumeArray[index] = newVolume;
                            }
                        }
                    } else if (i == 0) {  //always this is a positive volume
                        volume = point.getValue(INNER_Volume);
                        newVolume = Double.parseDouble(String.valueOf(positiveVolumeArray[index])) + volume;
                        positiveVolumeArray[index] = newVolume;
                    }
                }
            }
        }

        double[] negativeVolumeArray = new double[BARS];
        for (int i = 0; i < negativeVolumeArray.length; i++) {
            negativeVolumeArray[i] = volumeArray[i] - positiveVolumeArray[i];
        }

        //drawing the bars in horizontal direction
        for (int i = 0; i < volumeArray.length; i++) {

            maxVolume = Math.max(maxVolume, volumeArray[i]);
            minVolume = Math.min(minVolume, volumeArray[i]);
        }

        double adj = (maxVolume - minVolume) * 0.02f;
        //adj = 1;
        maxVolume = maxVolume + adj;
        minVolume = minVolume - adj;
        if (minVolume < 0) {
            minVolume = 0;
        }

        double pixelPerVolume = (r.width) / (maxVolume - minVolume);
        //double barHeight = getPixelFortheYValue(beginVal, panelID) - getPixelFortheYValue(beginVal + allowedPitch, panelID);
        double barHeight = 0;
        int index = 0;
        float y = 0;
        double current = beginVal;
        while ((current <= endVal) && (index < BARS)) {

            y = getPixelFortheYValue(current, panelID);
            //drawing the bars in horizontal direction
            double barWidth = pixelPerVolume * (negativeVolumeArray[index] - minVolume);
            barHeight = getPixelFortheYValue(current, panelID) - getPixelFortheYValue(current + allowedPitch, panelID);
            g.setColor(barFillColor);
            g.fillRect(r.x, (int) (y - barHeight), (int) barWidth, (int) barHeight);

            //drawing the vertical outline border of each bar.
            double bottomLine = pixelPerVolume * (negativeVolumeArray[index] - minVolume);
            g.setColor(barOutLineColor);
            g.drawLine((int) (r.x + bottomLine), (int) y, (int) (r.x + bottomLine), (int) (y - barHeight));

            //drawing the horizontal outline border of each bar.
            if (index < BARS - 1) {
                bottomLine = pixelPerVolume * (negativeVolumeArray[index] - minVolume);
                double topLine = pixelPerVolume * (negativeVolumeArray[index + 1] - minVolume);

                double max = Math.max(bottomLine, topLine);
                g.setColor(barOutLineColor);
                g.drawLine(r.x, (int) (y - barHeight), (int) (r.x + max), (int) (y - barHeight));
            }

            //drawing the volume value
            if (negativeVolumeArray[index] > 0) {
                g.setColor(Theme.getColor("VOLUME_BY_PRICE_NET_CHANGE_NEGATIVE_FONT_COLOR"));
                String strValue = volumeformatter.format(negativeVolumeArray[index]);
                if (volumeByPriceType == VOL_BY_PRICE_PCT_CHANGE) {
                    strValue = pctFormatter.format(negativeVolumeArray[index] * 100 / (negativeVolumeArray[index] + positiveVolumeArray[index])) + " %";
                }
                int length = g.getFontMetrics().stringWidth(strValue);
                int x = (int) barWidth - length - 4;
                int yPos = (int) (y - (barHeight - g.getFontMetrics().getHeight()) / 2);
                if (yPos > (r.y + r.height)) {
                    yPos = r.y + r.height;
                }
                if (length < barWidth) {
                    g.drawString(strValue, r.x + x, yPos);
                } else {
                    g.drawString(strValue, r.x + (int) barWidth + 4, yPos);
                }
            }
            index++;
            current += allowedPitch;
        }

        //drawing the horizontal outline border of top most  bar.
        double width1 = pixelPerVolume * (negativeVolumeArray[BARS - 1] - minVolume);
        y = y - (float) barHeight;
        g.drawLine(r.x, (int) y, (int) (r.x + width1), (int) y);

        //drawing the horizontal outline border of bottom most bar.
        y = getPixelFortheYValue(beginVal, panelID);
        width1 = pixelPerVolume * (negativeVolumeArray[0] - minVolume);
        g.drawLine(r.x, (int) y, (int) (r.x + width1), (int) y);

        //drawing the positive volumes//TODO:
        current = beginVal;
        index = 0;
        barFillColor = new Color(60, 250, 80, 80);
        barOutLineColor = new Color(0, 250, 50, 200);

        while ((current <= endVal) && (index < BARS)) {

            y = getPixelFortheYValue(current, panelID);
            //drawing the bars in horizontal direction
            double barWidth = pixelPerVolume * (positiveVolumeArray[index] - minVolume);
            barHeight = getPixelFortheYValue(current, panelID) - getPixelFortheYValue(current + allowedPitch, panelID);
            g.setColor(barFillColor);
            g.fillRect(r.x + r.width - (int) barWidth, (int) (y - barHeight), (int) barWidth, (int) barHeight);

            //drawing the vertical outline border of each bar.
            double bottomLine = pixelPerVolume * (positiveVolumeArray[index] - minVolume);
            g.setColor(barOutLineColor);
            g.drawLine((int) (r.x + r.width - bottomLine), (int) y, (int) (r.x + r.width - bottomLine), (int) (y - barHeight));

            //drawing the horizontal outline border of each bar.
            if (index < BARS - 1) {
                bottomLine = pixelPerVolume * (positiveVolumeArray[index] - minVolume);
                double topLine = pixelPerVolume * (positiveVolumeArray[index + 1] - minVolume);

                double max = Math.max(bottomLine, topLine);
                g.setColor(barOutLineColor);
                g.drawLine(r.x + r.width, (int) (y - barHeight), (int) (r.x + r.width - max), (int) (y - barHeight));
            }

            //drawing the volume value
            if (positiveVolumeArray[index] > 0) {
                g.setColor(Theme.getColor("VOLUME_BY_PRICE_NET_CHANGE_POSITIVE_FONT_COLOR"));
                String strValue = volumeformatter.format(positiveVolumeArray[index]);
                if (volumeByPriceType == VOL_BY_PRICE_PCT_CHANGE) {
                    strValue = pctFormatter.format(positiveVolumeArray[index] * 100 / (negativeVolumeArray[index] + positiveVolumeArray[index])) + " %";
                }
                int length = g.getFontMetrics().stringWidth(strValue);
                int x = (int) barWidth - length - 4;
                int yPos = (int) (y - (barHeight - g.getFontMetrics().getHeight()) / 2);
                if (yPos > (r.y + r.height)) {
                    yPos = r.y + r.height;
                }
                if (length < barWidth) {
                    g.drawString(strValue, r.x + r.width - (int) barWidth, yPos);
                } else {
                    g.drawString(strValue, r.x + r.width - (int) barWidth - length - 4, yPos);
                }
            }
            index++;
            current += allowedPitch;
        }

        //drawing the horizontal outline border of top most  bar.
        width1 = pixelPerVolume * (positiveVolumeArray[BARS - 1] - minVolume);
        y = y - (float) barHeight;
        g.drawLine(r.x + r.width, (int) y, (int) (r.x + r.width - width1), (int) y);

        //drawing the horizontal outline border of bottom most bar.
        y = getPixelFortheYValue(beginVal, panelID);
        width1 = pixelPerVolume * (positiveVolumeArray[0] - minVolume);
        g.drawLine(r.x + r.width, (int) y, (int) (r.x + r.width - width1), (int) y);
    }

    public void drawVolumeByPriceTotal(Graphics g, boolean isPrinting) {

        Color barFillColor = new Color(100, 100, 250, 80);
        Color barOutLineColor = new Color(51, 51, 255, 200);
        Font labelFont = new Font("Arial", Font.BOLD, 10);
        WindowPanel r = graph.mainRect;
        int panelID = getIndexOfTheRect(graph.panels, r);

        g.setClip(r.x, r.y, r.width, r.height);
        g.setFont(labelFont);

        double[] semiLogPitch = new double[1];
        float height = r.height - titleHeight;
        double bestPitch = getBestYInterval(panelID, height - 2 * clearance, semiLogPitch);
        double allowedPitch = ((r.isSemiLog()) || (yFactor[panelID] == 0)) ? bestPitch : getAllowedPitch(bestPitch, height - 2 * clearance, yFactor[panelID]);
        double beginVal = calculateBeginValue(minY[panelID], allowedPitch);
        double endVal = maxY[panelID] + allowedPitch;
        //int BARS = (int) ((endVal - beginVal) / allowedPitch);
        int BARS = (int) Math.ceil((endVal - beginVal) / allowedPitch);
        double[] volumeArray = new double[BARS];
        double[] positiveVolumeArray = new double[BARS];
        double maxVolume = -Double.MIN_VALUE;
        double minVolume = Double.MAX_VALUE;

        if (graphStore != null && graphStore.size() == 0) return;
        //splitting the volumes in to different price ranges
        if (graphStore != null && graphStore.size() > 0) {

            for (int i = (int) beginIndex; i < endIndex; i++) {
                Object[] objects = (Object[]) graphStore.get(i);
                ChartPoint point = (ChartPoint) objects[1];
                if (point != null) {
                    double price = point.getValue(getBaseCP().getOHLCPriority());
                    double volume = point.getValue(INNER_Volume);
                    int index = getIndexForPrice(beginVal, endVal, BARS, price);

                    double newVolume = Double.parseDouble(String.valueOf(volumeArray[index])) + volume;
                    volumeArray[index] = newVolume;

                    //calculating the positive volumes -corresponding to + change..
                    Object[] preObjects = null;
                    if (i < graphStore.size() && i > 0) {
                        preObjects = (Object[]) graphStore.get(i - 1);
                    }
                    if (preObjects != null) {
                        ChartPoint prePoint = (ChartPoint) preObjects[1];

                        if ((point != null && prePoint != null) && point.getValue(getBaseCP().getOHLCPriority()) > prePoint.getValue(getBaseCP().getOHLCPriority())) {
                            volume = point.getValue(INNER_Volume);
                            newVolume = Double.parseDouble(String.valueOf(positiveVolumeArray[index])) + volume;
                            positiveVolumeArray[index] = newVolume;
                        }
                    }
                }
            }
        }

        //drawing the bars in horizontal direction
        for (int i = 0; i < volumeArray.length; i++) {

            maxVolume = Math.max(maxVolume, volumeArray[i]);
            minVolume = Math.min(minVolume, volumeArray[i]);
        }

        double adj = (maxVolume - minVolume) * 0.02f;
        maxVolume = maxVolume + adj;
        minVolume = minVolume - adj;
        if (minVolume < 0) {
            minVolume = 0;
        }

        double pixelPerVolume = (r.width) / (maxVolume - minVolume);
        //double barHeight = getPixelFortheYValue(beginVal, panelID) - getPixelFortheYValue(beginVal + allowedPitch, panelID);
        double barHeight = 0;
        int index = 0;
        float y = 0;
        double current = beginVal;
        while ((current <= endVal) && (index < BARS)) {

            y = getPixelFortheYValue(current, panelID);
            //drawing the bars in horizontal direction
            double barWidth = pixelPerVolume * (volumeArray[index] - minVolume);
            barHeight = getPixelFortheYValue(current, panelID) - getPixelFortheYValue(current + allowedPitch, panelID);
            g.setColor(barFillColor);
            g.fillRect(r.x, (int) (y - barHeight), (int) barWidth, (int) barHeight);

            //drawing the vertical outline border of each bar.
            double bottomLine = pixelPerVolume * (volumeArray[index] - minVolume);
            g.setColor(barOutLineColor);
            g.drawLine((int) (r.x + bottomLine), (int) y, (int) (r.x + bottomLine), (int) (y - barHeight));

            //drawing the horizontal outline border of each bar.
            if (index < BARS - 1) {
                bottomLine = pixelPerVolume * (volumeArray[index] - minVolume);
                double topLine = pixelPerVolume * (volumeArray[index + 1] - minVolume);

                double max = Math.max(bottomLine, topLine);
                g.setColor(barOutLineColor);
                g.drawLine(r.x, (int) (y - barHeight), (int) (r.x + max), (int) (y - barHeight));
            }

            //drawing the volume value
            if (volumeArray[index] > 0) {
                g.setColor(Theme.getColor("VOLUME_BY_PRICE_TOTAL_FONT_COLOR"));
                String strValue = volumeformatter.format(volumeArray[index]);
                int length = g.getFontMetrics().stringWidth(strValue);
                int x = (int) barWidth - length - 4;
                int yPos = (int) (y - (barHeight - g.getFontMetrics().getHeight()) / 2);
                if (yPos > (r.y + r.height)) {
                    yPos = r.y + r.height;
                }
                if (length < barWidth) {
                    g.drawString(strValue, r.x + x, yPos);
                } else {
                    g.drawString(strValue, r.x + (int) barWidth + 4, yPos);
                }
            }
            index++;
            current += allowedPitch;
        }

        //drawing the horizontal outline border of top most  bar.
        double width1 = pixelPerVolume * (volumeArray[BARS - 1] - minVolume);
        y = y - (float) barHeight;
        g.drawLine(r.x, (int) y, (int) (r.x + width1), (int) y);

        //drawing the horizontal outline border of bottom most bar.
        y = getPixelFortheYValue(beginVal, panelID);
        width1 = pixelPerVolume * (volumeArray[0] - minVolume);
        g.drawLine(r.x, (int) y, (int) (r.x + width1), (int) y);
    }

    public void drawCrossHairs(Graphics g) {
        if (crossHairX > -1 && crossHairY > -1) {
            Graphics2D gg = (Graphics2D) g;
            //gg.setColor(graph.getCrossHairColor());
            gg.setStroke(PropertyDialogFactory.getBasicStroke((byte) PropertyDialogFactory.PEN_SOLID, 1.0f));
            for (int i = 0; i < graph.panels.size(); i++) {
                WindowPanel rect = (WindowPanel) graph.panels.get(i);

                /******************draw cross hair labels*****************************************************/
                //if (graph.yAxisPos == SplitPanel.AXIS_LEFT_ONLY || graph.yAxisPos == SplitPanel.AXIS_BOTH_ENDS) {
                if (true) {
                    Font font = graph.getAxisFont();
                    g.setFont(font);
                    //*****Region******************************** formatting y value*****************************************/
                    int pnlheight = rect.height - titleHeight;
                    double[] semiLogPitch = new double[1];
                    double bestPitch = getBestYInterval(i, pnlheight - 2 * clearance, semiLogPitch);
                    double allowedPitch = ((rect.isSemiLog()) || (yFactor[i] == 0)) ? bestPitch : getAllowedPitch(bestPitch, pnlheight - 2 * clearance, yFactor[i]);
                    DecimalFormat priceFormatter = getNumberFormatter(allowedPitch);
                    //*****Region******************************** formatting y value*****************************************/

                    double priceYVal = getYValueForthePixel(crossHairY, i);

                    int rectSize = 7;
                    Color borderColor = Theme.getColor("GRAPH_CROSSHAIR_BORDER_COLOR");
                    Color fontColor = Theme.getColor("GRAPH_CROSSHAIR_FONT_COLOR");
                    Color fillColor = Theme.getColor("GRAPH_CROSSHAIR_FILL_COLOR");

                    Rectangle tmpRect = new Rectangle(rect.x, rect.y + StockGraph.titleHeight, rect.width, rect.height - StockGraph.titleHeight);

                    Point p = new Point(crossHairX, crossHairY);

                    if ((isYValueInsidePanel(priceYVal, i)) && tmpRect.contains(p)) {
                        //*****Region******************************** calculating y value*****************************************/
                        double priceTempYVal = priceYVal;
                        int[] xCordsY = {borderWidth, borderWidth + graph.widthYaxis_1 - 2, graph.widthYaxis_1 - 2, borderWidth};
                        int[] YCordsY = {crossHairY - rectSize, crossHairY - rectSize, crossHairY + rectSize, crossHairY + rectSize};
                        if (graph.yAxisPos == SplitPanel.AXIS_LEFT_ONLY || graph.yAxisPos == SplitPanel.AXIS_BOTH_ENDS) {
                            g.setColor(fillColor);
                            g.fillPolygon(xCordsY, YCordsY, 4);
                            g.setColor(borderColor);
                            g.drawPolygon(xCordsY, YCordsY, 4);
                        }
                        //*End of Region******************************** calculating y value*****************************************/

                        //*****Region******************************** calculating x value*****************************************/

                        Rectangle newRect = new Rectangle(rect.x, graph.getHeight() - sliderHeight - borderWidth - heightXaxis,
                                rect.width, heightXaxis);
                        gg.setClip(newRect);
                        int[] xCords = {crossHairX - 15, crossHairX - 15, crossHairX + 15, crossHairX + 15};
                        int xdrawStartY = graph.getHeight() - sliderHeight - borderWidth - heightXaxis;
                        int[] YCords = {xdrawStartY + 3, xdrawStartY + 3 + 2 * rectSize, xdrawStartY + 3 + 2 * rectSize, xdrawStartY + 3};

                        g.setColor(fillColor);
                        g.fillPolygon(xCords, YCords, 4);
                        g.setColor(borderColor);
                        g.drawPolygon(xCords, YCords, 4);
                        float xIndex = getIndexForthePixel(crossHairX);
                        long xTime = getTimeMillisec(Math.round(xIndex));
                        String xLable = "";
                        if (graph.isCurrentMode()) {
                            xLable = timeFormatter.format(xTime);
                        } else {
                            xLable = dateFormatter_dd_MMM.format(xTime);
                        }
                        g.setColor(fontColor); //font color
                        g.drawString(xLable, crossHairX - 15 + 1, xdrawStartY + 3 + 10 - 1);//x
                        gg.setClip(null);
                        //*End of Region ******************************** calculating x value**************************************/
                        String strPriceTempVal = "";
                        if (rect == graph.turnOverRect) {
                            strPriceTempVal = thousandformat.format(priceTempYVal);
                        } else if (rect == graph.volumeRect) {
                            strPriceTempVal = thousandformat.format(priceTempYVal);
                        } else {
                            strPriceTempVal = formatPriceField(priceTempYVal, rect.isInThousands());
                        }

                        int lableLength = g.getFontMetrics(font).stringWidth(strPriceTempVal);

                        if (graph.yAxisPos == SplitPanel.AXIS_LEFT_ONLY || graph.yAxisPos == SplitPanel.AXIS_BOTH_ENDS) {
                            g.setColor(fontColor);
                            g.drawString(strPriceTempVal, borderWidth + graph.widthYaxis_1 - lableLength - 9, crossHairY + 4);
                        }

                        if (graph.yAxisPos == SplitPanel.AXIS_RIGHT_ONLY || graph.yAxisPos == SplitPanel.AXIS_BOTH_ENDS) {
                            //drawing the right hand side cross hair labels
                            gg.setColor(fillColor);
                            g.fillRect(rect.x + rect.width + 1, crossHairY - rectSize, graph.widthYaxis_2 - 2, 2 * rectSize);
                            gg.setColor(borderColor);
                            g.drawRect(rect.x + rect.width + 1, crossHairY - rectSize, graph.widthYaxis_2 - 2, 2 * rectSize);

                            gg.setColor(fontColor);
                            g.drawString(strPriceTempVal, rect.width + borderWidth + graph.widthYaxis_1 + 9, crossHairY + 4);
                        }
                    }
                }

                /*End of Region#*****************draw cross hair labels***************************/

                /*Region *********************draw cross hair**************************************************************/
                //changed by charithn-cross hari color

                Point p = new Point(crossHairX, crossHairY);
                if (isCrossHairOnChart(p)) {
                    gg.setXORMode(Color.yellow);
                    gg.setColor(graph.getCrossHairColor());
                    Rectangle newRect = new Rectangle(rect.x, rect.y + StockGraph.titleHeight, rect.width, rect.height - StockGraph.titleHeight);
                    gg.setClip(newRect);
                    gg.drawLine(crossHairX, rect.y + StockGraph.titleHeight, crossHairX, rect.y + rect.height);//verticle
                    gg.drawLine(rect.x, crossHairY, rect.x + rect.width, crossHairY);//horizantal
                    gg.setClip(null);
                    gg.setPaintMode();
                }

                /*End of Region*********************draw cross hair**************************************************************/

            }
        }
    }

    public boolean isCrossHairOnChart(Point p) {
        boolean isCrossHairOnChart = false;
        for (int i = 0; i < graph.panels.size(); i++) {
            WindowPanel rect = (WindowPanel) graph.panels.get(i);
            Rectangle newRect = new Rectangle(rect.x, rect.y + StockGraph.titleHeight, rect.width, rect.height - StockGraph.titleHeight);
            if (newRect.contains(p)) {
                isCrossHairOnChart = true;
            }
        }
        return isCrossHairOnChart;
    }

    /**
     * *** Draw the last value of indicator in y axis when they are in visible range *********************************
     */
    public void drawLastValueOfIndicator(Graphics g) {

        if (!ChartOptions.getCurrentOptions().gen_is_shw_last_ind_val) return;
        if (graph.yAxisPos != SplitPanel.AXIS_LEFT_ONLY) { //only for right axis
            ChartProperties cp;
            ArrayList<Integer> overLapAssist = new ArrayList<Integer>();
            for (int i = 0; i < sourceCount; i++) {
                cp = (ChartProperties) Sources.get(i);
                if (cp.getID() > ID_COMPARE && (cp.getID() != ID_TURNOVER) && (cp.getID() != ID_VOLUME && (!cp.isHidden()))) { //indicator //containt vaolume ?
                    int indLastIndex = getLastNotNullIndexOfIndicatorInGraphStore(i + 1);
                    if (graphStoreSize < 1) return;

                    Object[] singlePoint = (Object[]) (graphStore.get(indLastIndex));

                    //if (singlePoint!=null &&  singlePoint[i + 1] != null){
                    double lastIndValue = ((ChartPoint) singlePoint[i + 1]).Open; //indicator price value
                    int pnlID = graph.getIDforPanel(cp.getRect());
                    int indicatorPix = getPixelFortheYValue(lastIndValue, pnlID); //pixel value

                    /*Region*********************overlap adujstment ************/
                    for (int j = 0; j < overLapAssist.size(); j++) {
                        int prePix = overLapAssist.get(j);
                        int diff = indicatorPix - prePix;

                        //update the indicator pixel
                        if ((Math.abs(diff) < 12) && (indicatorPix != prePix)) {
                            int adj = 12 - Math.abs(diff);
                            if (diff > 0) { //indPix > prePix
                                indicatorPix = indicatorPix + adj;
                            } else {
                                indicatorPix = indicatorPix - adj;
                            }

                        }
                    }
                    overLapAssist.add(indicatorPix);
                    /*end of Region*********************overlap adustment ************/

                    /*Region ********************calculating pitch ******************/
                    int pnlheight = cp.getRect().height - titleHeight;
                    double[] semiLogPitch = new double[1];
                    double bestPitch = getBestYInterval(pnlID, pnlheight - 2 * clearance, semiLogPitch);
                    double allowedPitch = ((cp.getRect().isSemiLog()) || (yFactor[pnlID] == 0)) ? bestPitch : getAllowedPitch(bestPitch, pnlheight - 2 * clearance, yFactor[pnlID]);
                    DecimalFormat formatter = getNumberFormatter(allowedPitch);
                    /*Region ********************calculating pitch ******************/

                    Font font = graph.getAxisFont();
                    g.setFont(font);
                    String StrIndVal = formatter.format(lastIndValue);
                    int lableLengthInd = g.getFontMetrics(font).stringWidth(StrIndVal);
                    int left = cp.getRect().x + cp.getRect().width + 2;
                    int[] xCords = {left, left + graph.widthYaxis_2 - 3, left + graph.widthYaxis_2 - 3, left};
                    int[] lowYCords = {indicatorPix - 6, indicatorPix - 6, indicatorPix + 6, indicatorPix + 6};

                    /*check visibility ************************/
                    boolean isInVisibleRange = ((endIndex >= indLastIndex) && beginIndex <= indLastIndex) ? true : false;
                    /*check visibility ************************/

                    if (isInVisibleRange) { //TODO: can move this to upwards .coz any of the calculation need not to be done if this's nt in visible range

                        Color fillColor = cp.getColor();
                        g.setColor(new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(), 255));
                        g.fillPolygon(xCords, lowYCords, 4);

                        g.setColor(Color.black);

                        g.drawPolygon(xCords, lowYCords, 4);
                        /*int red = Math.abs(fillColor.getRed() - 255);
                        int green = Math.abs(fillColor.getGreen() - 255);
                        int blue = Math.abs(fillColor.getBlue() - 255);
                        Color fontColor = new Color(red, green, blue);
                        if (red == 127 && green == 127 && blue == 127) {    //avoid gray color problem
                            g.setColor(Color.BLUE);
                        } else {
                            g.setColor(fontColor);
                        }*/

                        g.setColor(getInversionColor(fillColor));
                        g.setFont(new Font("Tahoma", Font.PLAIN, 9));
                        g.drawString(StrIndVal, left + 10, indicatorPix + 6 - 3);
                        g.setPaintMode();

                    }
                }

            }
            overLapAssist.clear();
        }
        //then only show //other wise show in Dim (like in tradestation)
    }

    public void drawVerticalLine(Graphics g) {
        if (verticalX > -1) {
            Graphics2D gg = (Graphics2D) g;
            gg.setColor(graph.getCrossHairColor());
            gg.setStroke(PropertyDialogFactory.getBasicStroke((byte) PropertyDialogFactory.PEN_DASH, 2.0f)); //TODO: EARLIER CODE

            for (int i = 0; i < graph.panels.size(); i++) {
                //changed by charithn-cross hari color
                gg.setXORMode(Color.CYAN);
                WindowPanel rect = (WindowPanel) graph.panels.get(i);
                Rectangle newRect = new Rectangle(rect.x, rect.y + StockGraph.titleHeight,
                        rect.width, rect.height - StockGraph.titleHeight);
                gg.setClip(newRect);
                gg.drawLine(verticalX, rect.y + StockGraph.titleHeight, verticalX, rect.y + rect.height);
                gg.setClip(null);
                gg.setPaintMode();
            }
        }
    }

    public void drawFibCalcPoints(Graphics g) {
        if (fibCalBounds != null && fibCalBounds.length > 0) {
            for (int i = 0; i < fibCalBounds.length; i++) {
                if (fibCalBounds[i] != null) {
                    int width = 30;
                    int x = (int) fibCalBounds[i].getX();
                    int y = (int) fibCalBounds[i].getY();

                    Graphics2D gg = (Graphics2D) g;
                    gg.setColor(Color.BLUE);
                    gg.setStroke(PropertyDialogFactory.getBasicStroke((byte) PropertyDialogFactory.PEN_SOLID, 2.0f));
                    gg.drawLine(x - width / 2, y, x + width / 2, y);
                }
            }
        }
    }

    public void drawPriceBall(Graphics g) {
        if (pinBallX > -1) {
            Graphics2D gg = (Graphics2D) g;
            gg.setColor(graph.getCrossHairColor());
            gg.setStroke(PropertyDialogFactory.getBasicStroke((byte) PropertyDialogFactory.PEN_SOLID, 1.0f));
            for (int i = 0; i < graph.panels.size(); i++) {
//                gg.setXORMode(Color.CYAN);

                WindowPanel rect = (WindowPanel) graph.panels.get(i);
                Rectangle newRect = new Rectangle(rect.x, rect.y + StockGraph.titleHeight,
                        rect.width, rect.height - StockGraph.titleHeight);
                gg.setClip(newRect);

                float fXIndex = getIndexForthePixel(Math.round(pinBallX));
                float[] faXIndexPint = {fXIndex}; //indexes

                double[] faYRelOHLcPin = graph.GDMgr.convertXArrayIndexTopriorityOHLC(faXIndexPint);

                setPinLegendOHLCPriceDisplay(faYRelOHLcPin[0]);

                //get pixel values for the close value
                long pinSnapTime = getTimeMillisec(Math.round(faXIndexPint[0]));
                setPinLegendOHLCDisplay(pinSnapTime);
                int ixVal = getPixelFortheIndex(Math.round(faXIndexPint[0]));
                int tempBaseChartPanelID = 0;
                int[] ohlcPixArrPint = graph.GDMgr.convertYValueArrayToPixelArr(faYRelOHLcPin, tempBaseChartPanelID);///base chart panel

                gg.fillOval(ixVal - 3, ohlcPixArrPint[0] - 3, 6, 6);
                gg.setColor(Color.black);
                gg.drawOval(ixVal - 3, ohlcPixArrPint[0] - 3, 6, 6);

                gg.setClip(null);
                gg.setPaintMode();
            }
        }
    }

    public void drawHighLowCrossHair(Graphics g) {

        Color borderColor = Theme.getColor("GRAPH_CROSSHAIR_BORDER_COLOR");
        Color fontColor = Theme.getColor("GRAPH_CROSSHAIR_FONT_COLOR");
        Color fillColor = Theme.getColor("GRAPH_CROSSHAIR_FILL_COLOR");

        if (highLowCrossX > -1) {
            Graphics2D gg = (Graphics2D) g;
            gg.setColor(graph.getCrossHairColor());
            for (int i = 0; i < graph.panels.size(); i++) {
                gg.setStroke(PropertyDialogFactory.getBasicStroke((byte) PropertyDialogFactory.PEN_SOLID, 1f)); //TODO: EARLIER CODE
                WindowPanel rect = (WindowPanel) graph.panels.get(i);

                float fXIndex = getIndexForthePixel(Math.round(highLowCrossX));
                float[] faXIndexPint = {fXIndex}; //indexes
                long pinSnapTime = getTimeMillisec(Math.round(faXIndexPint[0]));

                double faYHigh = graph.GDMgr.getOHLCValueForIndex(Math.round(fXIndex), INNER_High);
                double faYLow = graph.GDMgr.getOHLCValueForIndex(Math.round(fXIndex), INNER_Low);

                int ixVal = getPixelFortheIndex(Math.round(faXIndexPint[0]));
                int tempBaseChartPanelID = 0;
                int highPixel = getPixelFortheYValue(faYHigh, tempBaseChartPanelID);
                int lowPixel = getPixelFortheYValue(faYLow, tempBaseChartPanelID);

                boolean isSameHighLow = false;
                if (highPixel == lowPixel) {
                    isSameHighLow = true;
                }
                if (graph.yAxisPos == SplitPanel.AXIS_LEFT_ONLY) {
                    WindowPanel r = (WindowPanel) graph.panels.get(i);
                    int top = r.y + titleHeight;
                    int height = r.height - titleHeight;
                    g.setClip(borderWidth, top, graph.widthYaxis_1, height + SplitPanel.heightXaxis);
                } else if (graph.yAxisPos == SplitPanel.AXIS_RIGHT_ONLY) {
                    WindowPanel r = (WindowPanel) graph.panels.get(i);
                    int left = r.x + r.width;
                    int top = r.y + titleHeight;
                    int height = r.height - titleHeight;
                    g.setClip(left, top, graph.widthYaxis_2, height + SplitPanel.heightXaxis + graph.zSlider.getHeight());
                }

                //ToDo : satyajith : check whether this only this is needed if chart style is candle only ?
                /*************************draw high low lablels *****************************************/
                //if (graph.yAxisPos == SplitPanel.AXIS_LEFT_ONLY || graph.yAxisPos == SplitPanel.AXIS_BOTH_ENDS) {
                if (true) {
                    /****************pitch calculation area ************************************************************/

                    Rectangle tmpRect = new Rectangle(rect.x, rect.y + StockGraph.titleHeight, rect.width, rect.height - StockGraph.titleHeight);
                    Point p = new Point(highLowCrossX, highLowCrossY);

                    if (tmpRect.contains(p)) {

                        int pnlheight = graph.mainRect.height - titleHeight;
                        double[] semiLogPitch = new double[1];

                        int mainRectId = graph.getIDforPanel(graph.mainRect);
                        double bestPitch = getBestYInterval(mainRectId, pnlheight - 2 * clearance, semiLogPitch);
                        double allowedPitch = ((graph.mainRect.isSemiLog()) || (yFactor[mainRectId] == 0)) ? bestPitch : getAllowedPitch(bestPitch, pnlheight - 2 * clearance, yFactor[mainRectId]);
                        DecimalFormat formatter = getNumberFormatter(allowedPitch);
                        /****************pitch calculation area ************************************************************/

                        int adj = 2;
                        int[] xCordsLeft = {borderWidth + adj, rect.x - adj - 2, rect.x - adj - 2, borderWidth + adj};
                        //int[] xCordsLeft = {rect.x - graph.widthYaxis_1 - adj, rect.x - adj, rect.x - adj, rect.x - graph.widthYaxis_1 - adj};
                        int[] xCordsRight = {rect.x + rect.width + adj, rect.width + rect.x + graph.widthYaxis_2 - adj, rect.x + rect.width + graph.widthYaxis_2 - adj, rect.x + rect.width + adj};
                        //ToDo : restrict y values above and below visible range
                        int[] highYCords = {highPixel - 6, highPixel - 6, highPixel + 6, highPixel + 6};


                        Font font = graph.getAxisFont();
                        g.setFont(font);
                        g.setColor(fillColor);
                        g.fillPolygon(xCordsLeft, highYCords, 4);
                        g.fillPolygon(xCordsRight, highYCords, 4);
                        g.setColor(borderColor);
                        g.drawPolygon(xCordsLeft, highYCords, 4);
                        g.drawPolygon(xCordsRight, highYCords, 4);

                        //adjustments
                        if (!isSameHighLow) {
                            if ((lowPixel - 6) < (highPixel + 6)) {
                                int diff = (highPixel + 6) - (lowPixel - 6);
                                int[] lowYCords = {lowPixel - 6 + diff, lowPixel - 6 + diff, lowPixel + 6 + diff, lowPixel + 6 + diff};
                                g.setColor(fillColor);
                                g.fillPolygon(xCordsLeft, lowYCords, 4);
                                g.fillPolygon(xCordsRight, lowYCords, 4);
                                g.setColor(borderColor);
                                g.drawPolygon(xCordsLeft, lowYCords, 4);
                                g.drawPolygon(xCordsRight, lowYCords, 4);
                            } else {
                                int[] lowYCords = {lowPixel - 6, lowPixel - 6, lowPixel + 6, lowPixel + 6};
                                g.setColor(fillColor);
                                g.fillPolygon(xCordsLeft, lowYCords, 4);
                                g.fillPolygon(xCordsRight, lowYCords, 4);
                                g.setColor(borderColor);
                                g.drawPolygon(xCordsLeft, lowYCords, 4);
                                g.drawPolygon(xCordsRight, lowYCords, 4);
                            }
                        }

                        /***************************************************x values********************************************************************/

                        Rectangle newRect = new Rectangle(rect.x, graph.getHeight() - sliderHeight - borderWidth - heightXaxis,
                                rect.width, heightXaxis);
                        gg.setClip(newRect);
                        int[] xCordsX = {highLowCrossX - 15, highLowCrossX - 15, highLowCrossX + 15, highLowCrossX + 15};
                        int xdrawStartY = graph.getHeight() - sliderHeight - borderWidth - heightXaxis;
                        int[] YCordsY = {xdrawStartY + 3, xdrawStartY + 3 + 12, xdrawStartY + 3 + 12, xdrawStartY + 3};

                        g.setColor(fillColor);
                        g.fillPolygon(xCordsX, YCordsY, 4);
                        g.setColor(borderColor);
                        g.drawPolygon(xCordsX, YCordsY, 4);

                        float xIndex = getIndexForthePixel(highLowCrossX);
                        long xTime = getTimeMillisec(Math.round(xIndex));
                        String xLable = "";
                        if (graph.isCurrentMode()) {
                            xLable = timeFormatter.format(xTime);
                        } else {
                            xLable = dateFormatter_dd_MMM.format(xTime);
                        }
                        g.setColor(fontColor);
                        g.drawString(xLable, highLowCrossX - 15 + 1, xdrawStartY + 3 + 10 - 1);
                        gg.setClip(null);
                        /***************************************************x values********************************************************************/

                        int xGap = 10;
                        gg.setColor(fontColor);
                        String strHigh = formatter.format(faYHigh);
                        int lableLengthHIgh = g.getFontMetrics(font).stringWidth(strHigh);
                        g.drawString(strHigh, borderWidth + graph.widthYaxis_1 - lableLengthHIgh - 8, highPixel + 4);
                        g.drawString(strHigh, rect.x + rect.width + xGap, highPixel + 4);
                        String strLow = formatter.format(faYLow);
                        int lableLengthLow = g.getFontMetrics(font).stringWidth(strLow);

                        if (!isSameHighLow) {

                            if ((lowPixel - 6) < (highPixel + 6)) {
                                int diff = (highPixel + 6) - (lowPixel - 6);
                                g.drawString(strLow, borderWidth + graph.widthYaxis_1 - lableLengthLow - 8, lowPixel + 6 + diff - 2);
                                g.drawString(strLow, rect.x + rect.width + xGap, lowPixel + 6 + diff - 2);
                            } else {
                                g.drawString(strLow, borderWidth + graph.widthYaxis_1 - lableLengthLow - 8, lowPixel + 4);
                                g.drawString(strLow, rect.x + rect.width + xGap, lowPixel + 4);

                            }
                        }
                    }

                }

                /*************************draw high low lablels *****************************************/

                Point p = new Point(highLowCrossX, highLowCrossY);
                if (isCrossHairOnChart(p)) {
                    Rectangle newHighLowRect = new Rectangle(rect.x, rect.y + StockGraph.titleHeight,
                            rect.width, rect.height - StockGraph.titleHeight);
                    gg.setClip(newHighLowRect);
                    gg.setColor(graph.getCrossHairColor());
                    gg.setXORMode(Color.CYAN);
                    gg.setStroke(PropertyDialogFactory.getBasicStroke((byte) PropertyDialogFactory.PEN_DASH, 1.4f));
                    //verticla line
                    gg.drawLine(highLowCrossX, rect.y + StockGraph.titleHeight, highLowCrossX, rect.y + rect.height);

                    //horizenatl lines
                    gg.drawLine(rect.x, highPixel, rect.x + rect.width, highPixel);
                    if (!isSameHighLow) {
                        gg.drawLine(rect.x, lowPixel, rect.x + rect.width, lowPixel);
                    }
                    gg.setClip(null);
                    gg.setPaintMode();
                }


            }
        }
    }

    public void setPinLegendOHLCDisplay(long time) {
        TWDateFormat formatter;
        if (graph.isCurrentMode()) {
            formatter = new TWDateFormat("dd MMM HH:mm");
        } else {
            formatter = new TWDateFormat("dd MMM yyyy");
        }
        legendDisplayTime = formatter.format(time);
    }

    public void setPinLegendOHLCPriceDisplay(double dprice) {
        TWDecimalFormat formatter = new TWDecimalFormat("#############.00");

        if (dprice == 0.0d) {
            legendDisplaySnapPrice = "";
        } else {
            legendDisplaySnapPrice = " Price: " + formatter.format(dprice);
        }
    }

    public void setHighLowCorssLegend(double high, double low) {
        TWDecimalFormat formatter = new TWDecimalFormat("#############.00");
        String sHigh = "";
        String sLow = "";
        if (high == 0.0d && low == 0.0d) {
            legendDisplaySnapPrice = "";
        } else {
            if (high != 0.0d) {
                sHigh = formatter.format(high);
                sHigh = "High: " + sHigh + "  ";
            }
            if (low != 0.0d) {
                sLow = formatter.format(low);
                sLow = "Low: " + sLow;
            }
            legendDisplaySnapPrice = sHigh + sLow;
        }

    }

    public void drawDragZoomArea(Graphics g) {
        if (dragStartX > -1 && dragEndX > -1) {
            g.setXORMode(Color.orange);
            int GRAPH_TOP_TOP = StockGraph.borderWidth + StockGraph.legendHeight + StockGraph.titleHeight;
            int GRAPH_TOTAL_HEIGHT = Math.max(graph.getHeight() - StockGraph.borderWidth -
                    StockGraph.sliderHeight - StockGraph.heightXaxis - GRAPH_TOP_TOP, 0);
            if (dragStartX > dragEndX) {
                g.fillRect(dragEndX, GRAPH_TOP_TOP, dragStartX - dragEndX, GRAPH_TOTAL_HEIGHT);
            } else {
                g.fillRect(dragStartX, GRAPH_TOP_TOP, dragEndX - dragStartX, GRAPH_TOTAL_HEIGHT);
            }
            g.setPaintMode();
        }
    }

    public void reCalculateGraphParams() {
        /*if ((sourceCount < 1) || (graphStore.size() < 1) || (sourceCount < Sources.size())) {
            return;
        }*/
        /***  changed to fix the refresh problem 22 April 2009 - sathyajith****************/
        if ((sourceCount < 1) || (sourceCount < Sources.size())) {
            return;
        }
        boolean tmpIsBusy = isBusyNow;
        isBusyNow = true;
        try {
            instantSourceCount = Sources.size();
            if (sourceCount > Sources.size()) sourceCount = Sources.size();
            graphStoreSize = getGraphStoreSize();
            beginTimeMillisec = getTimeMillisec(Math.round(beginIndex));
            calculateMinMaxBounds();
            calculateXYFactors();
            extractPointArrays();
        } catch (Exception e) {
            System.out.println("%%%%%%%% Exception inside reCalculateGraphParams @ " + System.currentTimeMillis());
        }
        isBusyNow = tmpIsBusy;
    }

    public void drawLegendOnGraphics(Graphics gg, boolean isPrinting, int x, int y, int w, int h) {
        if (sourceCount < 1) return;
        ChartProperties cp;
        Graphics2D gP = (Graphics2D) gg;
        String currSymb;

        int LegendLength = 80;
        int ohlcDiplayLength = 80;
        int totalLeft = 0;

        gP.setColor(Theme.getColor("GRAPH_LEGEND_COLOR"));
        gP.setClip(0, 0, graph.getWidth(), legendHeight);
        gP.fillRect(0, 0, graph.getWidth(), legendHeight);

        gP.setClip(x, y, w, h);

        for (int i = 0; i < sourceCount; i++) {
            cp = (ChartProperties) Sources.get(i);
            if (cp.getID() > ID_COMPARE) continue;
            if (graph.getLegendType() > 0) {
                currSymb = ChartInterface.getCompanyName(cp.getSymbol());
            } else {
                currSymb = StockGraph.extractSymbolFromStr(cp.getSymbol());
            }

            gP.setStroke(new BasicStroke(1.0f));
            if (currSymb != null) {
                if (isPrinting) {
                    gP.setFont(graph.getLegendFont());
                    LegendLength = gP.getFontMetrics(graph.getLegendFont()).stringWidth(currSymb) + 25;
                    ohlcDiplayLength = gP.getFontMetrics(graph.getLegendFont()).stringWidth(legendDisplayTime + " " + legendDisplaySnapPrice) + 10;

                    gP.setColor(Color.GRAY);
                    gP.drawRect(totalLeft + x, y, LegendLength, h - 1);
                    gP.setColor(cp.getColor());
                    int rectSize = 6;
                    int rectYPos = (h - rectSize) / 2;
                    int legendHeight = gP.getFontMetrics().getHeight();

                    //gP.fillRect(totalLeft + 3 + x, 2 + y, 6, 6);
                    gP.fillRect(totalLeft + 3 + x, rectYPos, rectSize, rectSize); //changed by  sathyajith
                    gP.setColor(graph.getTitleForeGroundColor());
                    gP.drawString(currSymb, totalLeft + 15 + x, rectYPos + legendHeight / 2 + y);
                    gP.drawString(legendDisplayTime + " " + legendDisplaySnapPrice, w - ohlcDiplayLength, rectYPos + legendHeight / 2 + y);
                    //gP.drawString(currSymb, totalLeft + 15 + x, h - 4 + y);
                    //gP.drawString(legendDisplayTime + " " + legendDisplaySnapPrice, w - ohlcDiplayLength, h - 4 + y);

                    if (getNoOfCompareGraphs() > 0) {
                        gP.setColor(Theme.getColor("GRAPH_LEGEND_SEP_CENTER_COLOR"));
                        int xx = totalLeft + x + LegendLength;
                        gP.drawLine(xx, 0, xx, legendHeight);
                        gP.setColor(Theme.getColor("GRAPH_LEGEND_SEP_LEFT_COLOR"));
                        gP.drawLine(xx - 1, 0, xx - 1, legendHeight);
                        gP.setColor(Theme.getColor("GRAPH_LEGEND_SEP_RIGHT_COLOR"));
                        gP.drawLine(xx + 1, 0, xx + 1, legendHeight);
                    }
                    totalLeft = totalLeft + LegendLength + 3;
                } else {
                    gP.setFont(graph.getLegendFont());
                    LegendLength = gP.getFontMetrics(graph.getLegendFont()).stringWidth(currSymb) + 25;
                    ohlcDiplayLength = gP.getFontMetrics(graph.getLegendFont()).stringWidth(legendDisplayTime + " " + legendDisplaySnapPrice) + 10;
                    gP.setColor(graph.backColor);
                    h = gP.getFontMetrics(graph.getLegendFont()).getHeight();
                    //gP.fill3DRect(totalLeft + x, y, LegendLength, h - 1, true);  //TODO: now removed. add if required in future
                    gP.setColor(cp.getColor());
                    int rectSize = 6;
                    int rectYPos = (h - rectSize) / 2;
                    int legendHeight = gP.getFontMetrics().getHeight();
                    gP.fillRect(totalLeft + 3 + x, rectYPos, rectSize, rectSize); //changed by  sathyajith
                    gP.setColor(graph.getTitleForeGroundColor());
                    gP.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    gP.drawString(currSymb, totalLeft + 15 + x, rectYPos + legendHeight / 2 + y);
                    //gP.drawString(legendDisplayTime + " " + legendDisplaySnapPrice, w - ohlcDiplayLength, h - 4 + y);
                    gP.drawString(legendDisplayTime + " " + legendDisplaySnapPrice, w - ohlcDiplayLength, rectYPos + legendHeight / 2 + y);
                    gP.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);

                    if (getNoOfCompareGraphs() > 0) {
                        gP.setColor(Theme.getColor("GRAPH_LEGEND_SEP_CENTER_COLOR"));
                        int xx = totalLeft + x + LegendLength;
                        gP.drawLine(xx, 0, xx, legendHeight);
                        gP.setColor(Theme.getColor("GRAPH_LEGEND_SEP_LEFT_COLOR"));
                        gP.drawLine(xx - 1, 0, xx - 1, legendHeight);
                        gP.setColor(Theme.getColor("GRAPH_LEGEND_SEP_RIGHT_COLOR"));
                        gP.drawLine(xx + 1, 0, xx + 1, legendHeight);
                    }

                    totalLeft = totalLeft + LegendLength + 3;
                }
            }
        }
        gP.setClip(0, legendHeight - 3, graph.getWidth(), 3);

        gP.setColor(Theme.getColor("GRAPH_LEGEND_TITLE_SEP_1_COLOR"));
        gP.drawLine(0, legendHeight - 3, graph.getWidth(), legendHeight - 3);
        gP.setColor(Theme.getColor("GRAPH_LEGEND_TITLE_SEP_2_COLOR"));
        gP.drawLine(0, legendHeight - 2, graph.getWidth(), legendHeight - 2);
        gP.setColor(Theme.getColor("GRAPH_LEGEND_TITLE_SEP_3_COLOR"));
        gP.drawLine(0, legendHeight - 1, graph.getWidth(), legendHeight - 1);

        gP.setClip(null);
    }

    private double getDisplayPitch(double allowedPitch, int[] multiplier) {
        if ((allowedPitch >= 1000000) && (allowedPitch % 1000000 == 0)) {
            multiplier[0] = 1000000;
            return allowedPitch / 1000000;
        } else if ((allowedPitch >= 1000) && (allowedPitch % 1000 == 0)) {
            multiplier[0] = 1000;
            return allowedPitch / 1000;
        } else {
            multiplier[0] = 1;
            return allowedPitch;
        }
    }

    // drawing the Y-axis on Graphics
    public void drawYaxis1OnGraphics(Graphics g, boolean isPrinting) {
        Rectangle r;
        WindowPanel panel;

        int top, height, left = borderWidth;

        for (int i = 0; i < graph.panels.size(); i++) {
            panel = (WindowPanel) graph.panels.get(i);
            r = panel;
            top = r.y + titleHeight;
            height = r.height - titleHeight;
            Graphics2D g2D = (Graphics2D) g;
            //g2D.setClip(left, top, graph.widthYaxis_1, height + titleHeight + SplitPanel.heightXaxis);
            g2D.setClip(left, top, graph.widthYaxis_1, height + SplitPanel.heightXaxis);
            if (isPrinting) {
                GradientPaint redtowhite = new GradientPaint(left + graph.widthYaxis_1, top, graph.getAxisBGOuterColor(), left, top, graph.getAxisBGInnerColor());
                g2D.setPaint(redtowhite);
                g2D.fillRect(left, top, graph.widthYaxis_1, height + titleHeight + SplitPanel.heightXaxis);

                g2D.setColor(Color.BLACK);
                g2D.fillRect(r.x - 1, top, 1, height + 1);
            } else {
                g2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                GradientPaint redtowhite = new GradientPaint(left + graph.widthYaxis_1, top, graph.getAxisBGOuterColor(), left, top, graph.getAxisBGInnerColor());
                g2D.setPaint(redtowhite);
                g2D.fillRect(left, top, graph.widthYaxis_1, height + titleHeight + SplitPanel.heightXaxis);
            }
            int labelLength;
            double[] semiLogPitch = new double[1];
            double bestPitch = getBestYInterval(i, height - 2 * clearance, semiLogPitch);
            if (bestPitch > 0) {
                double allowedPitch = ((panel.isSemiLog()) || (yFactor[i] == 0)) ? bestPitch : getAllowedPitch(bestPitch, height - 2 * clearance, yFactor[i]);
                int[] multiplier = {1};
                double displayPitch = getDisplayPitch(allowedPitch, multiplier);
                DecimalFormat numFormat = getNumberFormatter(displayPitch);
                DecimalFormat formatter = getNumberFormatter(allowedPitch);
                int count = 0;
                double beginVal = calculateBeginValue(minY[i], allowedPitch);
                double val = beginVal;
                int currPos = getPixelFortheYValue(val, i);//(int)Math.round(height+top-clearance -(val-minY[i])*yFactor[i]);
                int lastPos = Integer.MAX_VALUE;
                g2D.setStroke(graph.BS_1p0f_solid);
                Font aFont = graph.getAxisFont();
                g2D.setFont(aFont);
                //graph y aixs color
                g2D.setColor(graph.getAxisColor());
                g2D.drawLine(r.x - 1, top, r.x - 1, top + height);
                String label;
                /* removed to fix the refresh problem
                 *Here though graphstoreSize > 0 is checked graphstore is not used inside in this block .So that doesn't affect to the loop

                if (graphStoreSize > 0) {
                */
                while (currPos > top) {
                    if ((currPos <= height + top) && !(panel.isSemiLog() && ((val == 0) || (lastPos - HALF_INCH / 4 < currPos)))) {
                        g2D.setColor(graph.getAxisColor());
                        g2D.drawLine(r.x - 6, currPos, r.x - 1, currPos);
                        label = numFormat.format(val / multiplier[0]);
                        g2D.setColor(graph.fontColor);
                        labelLength = g2D.getFontMetrics(aFont).stringWidth(label);
                        g2D.drawString(label, r.x - 8 - labelLength, currPos + 4); //bug here
                        lastPos = currPos;
                    }
                    if ((count >= 10000) || (yFactor[i] == 0) || (val >= maxY[i] + allowedPitch)) {
                        break;
                    }
                    count++;
                    val = beginVal + count * allowedPitch;
                    currPos = getPixelFortheYValue(val, i);
                }
                if (multiplier[0] > 1) {
                    drawMultiplier(g, r.x - graph.widthYaxis_1 + 2, r.y + r.height - 14, multiplier[0]);
                }
            }
            g2D.setClip(null);
            panel = null;
        }
    }

    private void drawMultiplier(Graphics g, int x, int y, int multiplier) {
        //Font aFont = graph.getAxisFont();
        String displayText = (multiplier == 1000) ? "x1K" : "x1M";
        int rectW = (multiplier == 1000) ? 20 : 22;

        g.setColor(Theme.getColor("GRAPH_MULTIPLIER_FILL_COLOR"));
        g.fillRect(x, y, rectW, 10);
        g.setColor(Theme.getColor("GRAPH_MULTIPLIER_BORDER_COLOR"));
        g.drawRect(x, y, rectW, 10);
        g.setColor(Theme.getColor("GRAPH_MULTIPLIER_FONT_COLOR"));
        g.setFont(new Font("Tahoma", Font.PLAIN, 9));
        g.drawString(displayText, x + 2, y + 9);
    }

    public int getMaxYAxisWidth() {
        int labelLength = 0;
        if (getBaseGraph() == null) {
            labelLength = 38;
        } else {
            for (int i = 0; i < graph.panels.size(); i++) {
                WindowPanel r = (WindowPanel) graph.panels.get(i);
                if ((r.isMinimized()) || (graph.isMaximized() && (i != graph.getMaximizedPanelID()))) continue;
                int height = r.height - titleHeight;
                double[] semiLogPitch = new double[1];
                double bestPitch = getBestYInterval(i, height - 2 * clearance, semiLogPitch);
                double allowedPitch = ((r.isSemiLog()) || (yFactor[i] == 0)) ? bestPitch : getAllowedPitch(bestPitch, height - 2 * clearance, yFactor[i]);
                int[] multiplier = {1};
                double displayPitch = getDisplayPitch(allowedPitch, multiplier);
                DecimalFormat numFormat = getNumberFormatter(displayPitch);

                double maxYVal = Math.max(Math.abs(minY[i]), Math.abs(maxY[i]));
                double maxVal = calculateBeginValue(maxYVal, allowedPitch) + allowedPitch;
                //DecimalFormat formatter = getNumberFormatter(allowedPitch);
                //String label = formatter.format(maxVal);
                String label = numFormat.format(maxVal / multiplier[0]);
                Graphics g = graph.getGraphics();
                if (g != null) {
                    labelLength = Math.max(g.getFontMetrics(graph.getAxisFont()).stringWidth(label), labelLength);
                }
            }
        }
        return labelLength;
    }

    private DecimalFormat getNumberFormatter(double allowedPitch) {
        String pattern = "###,###,##0";
        // microformat here must have the pattern "0.000000000"
        String strNum = microformat.format(allowedPitch);
        int decimalCount = 9;
        while (strNum.lastIndexOf("0") == strNum.length() - 1) {
            strNum = strNum.substring(0, strNum.length() - 1);
            decimalCount--;
        }
        if (decimalCount > 0) {
            pattern = "###,##0.00";
            for (int i = 2; i < decimalCount; i++) {
                pattern += "0";
            }
        }
        return new DecimalFormat(pattern);
    }

    public double getAllowedPitch(double bestPitch, float height, double yfactor) {
        double minGap = getMinimumGap(height);
        double result = bestPitch;
        double currGap = result * yfactor;
        while (currGap < minGap) {
            if (currGap > height) break;
            result += bestPitch;
            currGap = result * yfactor;
        }
        return result;
    }

    public void drawYaxis2OnGraphics(Graphics g, boolean isPrinting) {
        Rectangle r;
        WindowPanel panel;
        //boolean firstValueSmall = true;

        int top, height, left;

        for (int i = 0; i < graph.panels.size(); i++) {
            panel = (WindowPanel) graph.panels.get(i);
            r = panel;
            left = r.x + r.width;
            top = r.y + titleHeight;
            height = r.height - titleHeight;
            Graphics2D g2D = (Graphics2D) g;
            g2D.setClip(left, top, graph.widthYaxis_2, height + SplitPanel.heightXaxis + graph.zSlider.getHeight());
            if (isPrinting) {
                g2D.setColor(Color.white);

                GradientPaint redtowhite = new GradientPaint(left, top, graph.getAxisBGOuterColor(), left + graph.widthYaxis_2, top, graph.getAxisBGInnerColor());
                g2D.setPaint(redtowhite);
                g2D.fillRect(left, top, graph.widthYaxis_2, height + titleHeight + SplitPanel.heightXaxis);

                g2D.setColor(Color.BLACK);
                g2D.fillRect(left, top, 1, height + 1);
            } else {
                g2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                GradientPaint redtowhite = new GradientPaint(left, top, graph.getAxisBGOuterColor(), left + graph.widthYaxis_2, top, graph.getAxisBGInnerColor());
                g2D.setPaint(redtowhite);
                g2D.fillRect(left, top, graph.widthYaxis_2, height + titleHeight + SplitPanel.heightXaxis);

            }
            int labelLength;
            double[] semiLogPitch = new double[1];
            double bestPitch = getBestYInterval(i, height - 2 * clearance, semiLogPitch);
            if (bestPitch > 0) {
                double allowedPitch = ((panel.isSemiLog()) || (yFactor[i] == 0)) ? bestPitch : getAllowedPitch(bestPitch, height - 2 * clearance, yFactor[i]);
                int[] multiplier = {1};
                double displayPitch = getDisplayPitch(allowedPitch, multiplier);
                DecimalFormat numFormat = getNumberFormatter(displayPitch);

                DecimalFormat formatter = getNumberFormatter(allowedPitch);
                int count = 0;
                double beginVal = calculateBeginValue(minY[i], allowedPitch);
                double val = beginVal;

                int currPos = getPixelFortheYValue(val, i);
                int lastPos = Integer.MAX_VALUE;
                String label;
                g2D.setColor(graph.fontColor);
                g2D.setStroke(graph.BS_1p0f_solid);
                java.awt.Font aFont = graph.getAxisFont();

                g2D.setFont(aFont);
                //graph y aixs color
                g2D.setColor(graph.getAxisColor());
                g2D.drawLine(left, top, left, top + height);
                /* removed to fix the refresh problem
                 *Here though graphstoreSize > 0 is checked graphstore is not used inside in this block .So that doesn't affect to the loop

               if (graphStoreSize > 0) {
               */
                while (currPos > top) {
                    if ((currPos <= height + top) && !(panel.isSemiLog() && ((val == 0) || (lastPos - HALF_INCH / 4 < currPos)))) {
                        g2D.setColor(graph.getAxisColor());
                        g2D.drawLine(left + 5, currPos, left, currPos);
                        //label = formatter.format(val);
                        label = numFormat.format(val / multiplier[0]);
                        g2D.setColor(graph.fontColor);
                        //labelLength = g2D.getFontMetrics(aFont).stringWidth(label);
                        //g2D.drawString(label, left + graph.widthYaxis_2 - labelLength, currPos + 4);
                        g2D.drawString(label, left + 10, currPos + 4);
                        lastPos = currPos;
                    }
                    if ((count >= 10000) || (yFactor[i] == 0) || (val >= maxY[i] + allowedPitch)) {
                        break;
                    }
                    count++;
                    val = beginVal + count * allowedPitch;
                    currPos = getPixelFortheYValue(val, i);
                }
                if (multiplier[0] > 1) {
                    drawMultiplier(g, r.x + r.width + graph.widthYaxis_2 - 24, r.y + r.height - 14, multiplier[0]);
                }
            }
            g2D.setClip(null);
            panel = null;
        }
    }

    public void drawYaxis1MarginOnGraphics(Graphics g, boolean isPrinting) {
        Rectangle r;
        int top, height;

        g.setColor(graph.getAxisColor());
        for (int i = 0; i < graph.panels.size(); i++) {
            r = (WindowPanel) graph.panels.get(i);
            top = r.y + titleHeight;
            height = r.height - titleHeight;
            g.drawLine(0, top, 0, top + height);
        }
    }

    public String formatPriceField(double num, boolean inThousands) {
        return formatPriceField(num, inThousands, false);
    }

    public String formatPriceField(double num, boolean inThousands, boolean noDecimals) {
        DecimalFormat numberformat = getNumberFormat();
        if (inThousands) {
            return numberformat.format(num / 1000);
        } else if (noDecimals) {
            return thousandformat.format(num);
        } else {
            if ((num < 1) && (num > -1) && (num != 0)) {
                if ((num < 0.0001) && (num > -0.0001)) {
                    return microformat.format(num);
                } else {
                    return miliformat.format(num);
                }
            }
            return numberformat.format(num);
        }
    }

    public DecimalFormat getNumberFormat() {
        byte decimalPlaces = SharedMethods.getDecimalPlaces(graph.Symbol);
        DecimalFormat numformat = new DecimalFormat("###,###,##0.00");
        switch (decimalPlaces) {
            case Constants.ONE_DECIMAL_PLACES:
                numformat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case Constants.TWO_DECIMAL_PLACES:
                numformat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case Constants.THREE_DECIMAL_PLACES:
                numformat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                numformat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
            case Constants.FIVE_DECIMAL_PLACES:
                numformat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                break;
            case Constants.SIX_DECIMAL_PLACES:
                numformat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                break;
            case Constants.SEVEN_DECIMAL_PLACES:
                numformat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL);
                break;
            case Constants.EIGHT_DECIMAL_PLACES:
                numformat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL);
                break;
            case Constants.ONE_DECIMAL_PLACES_NOZERO:
                numformat.applyPattern(Constants.PATTERN_ONE_DECIMAL_NOZERO);
                break;
            case Constants.TWO_DECIMAL_PLACES_NOZERO:
                numformat.applyPattern(Constants.PATTERN_TWO_DECIMAL_NOZERO);
                break;
            case Constants.THREE_DECIMAL_PLACES_NOZERO:
                numformat.applyPattern(Constants.PATTERN_THREE_DECIMAL_NOZERO);
                break;
            case Constants.FOUR_DECIMAL_PLACES_NOZERO:
                numformat.applyPattern(Constants.PATTERN_FOUR_DECIMAL_NOZERO);
                break;
            case Constants.FIVE_DECIMAL_PLACES_NOZERO:
                numformat.applyPattern(Constants.PATTERN_FIVE_DECIMAL_NOZERO);
                break;
            case Constants.SIX_DECIMAL_PLACES_NOZERO:
                numformat.applyPattern(Constants.PATTERN_SIX_DECIMAL_NOZERO);
                break;
            case Constants.SEVEN_DECIMAL_PLACES_NOZERO:
                numformat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL_NOZERO);
                break;
            case Constants.EIGHT_DECIMAL_PLACES_NOZERO:
                numformat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL_NOZERO);
                break;
        }
        return numformat;
    }

    /**
     * calculates the best pitch for Y-axis
     * actualLen : (maxY - minY) for panel in terms of actual chart values
     * pixLen : corresponding pixel length (for val)
     */
    private double getBestYInterval(int panelID, float pixLen, double[] semiLogPitch) {
        boolean isSemiLog = ((WindowPanel) graph.panels.get(panelID)).isSemiLog();
        double actualLen = maxY[panelID] - minY[panelID];
        if (sourceCount < 1) return 40;
        if (pixLen <= 0) return actualLen * 10; // to avoild a second line

        double valPerQuarterInch = actualLen / pixLen * HALF_INCH / 2f;

        if (isSemiLog) {
            double noOfMarks = Math.max(pixLen / HALF_INCH, 2d);
            double totalMultiplication = maxY[panelID] / minY[panelID];
            double factorPerMark = Math.pow(totalMultiplication, 1d / noOfMarks);
            semiLogPitch[0] = factorPerMark;

        }
        return getPitchForCurrentValue(valPerQuarterInch);
    }

    private double getPitchForCurrentValue(double currVal) {
        double pitch = PriceVolPitch[0];
        for (long j = 1; j < Long.MAX_VALUE; j *= 10) {
            double Multiplier = (double) j / 1000000000d;
            for (int i = 0; i < PriceVolPitchLength; i++) {
                if (currVal >= Multiplier * PriceVolPitch[i]) {
                    pitch = Multiplier * PriceVolPitch[i];
                } else {
                    return pitch;
                }
            }
            if (j > Long.MAX_VALUE / 10) {
                return pitch;
            }
        }
        return pitch;
    }

    // calculates the best pitch for X-axis
    private int getBestTimeInterval(float val, float pixLen) {
        Date dd = new Date(beginTimeMillisec);//new Date(getTimeMillisec(Math.round(beginIndex)));
        if (graph.isCurrentMode()) {
            String label = timeFormatter.format(dd);//Long.toString(dd.getTime()) ;
            bestPixelPitch = graph.getGraphics().getFontMetrics(graph.font_PLAIN_9).stringWidth(label) + 12;
            val = (val / pixLen * bestPixelPitch) * interval / 60;
            return getTheRoundedTimeValue(val);
        } else {
            String label = dateFormatter_dd.format(dd);//Long.toString(dd.getTime()) ;
            bestPixelPitch = graph.getGraphics().getFontMetrics(graph.font_PLAIN_9).stringWidth(label) + 12;
            val = (val / pixLen * bestPixelPitch) * interval / (3600 * 24);
            return getTheRoundedTimeValue(val);
        }
    }

    private int getTheRoundedTimeValue(float val) {
        int pitch = 1;
        if (graph.isCurrentMode()) {
            pitch = CurrentTimePitch[0];
            for (int i = 0; i < CurrentTimePitch.length; i++) {
                pitch = CurrentTimePitch[i];
                if (val <= pitch) {
                    return pitch;
                }
            }
            return pitch;
        } else {
            pitch = HistoryTimePitch[0];
            for (int i = 0; i < HistoryTimePitch.length; i++) {
                pitch = HistoryTimePitch[i];
                if (val <= pitch) {
                    return pitch;
                }
            }
            return pitch;
        }
    }

    private int[] getMajorPointArray(int bestMinutePitch) {
        if (!graph.isCurrentMode()) {
            if (bestMinutePitch < 30) {
                return getMonthsArray();
            } else if (bestMinutePitch < 360) {
                return midNights;
            } else {
                return null;
            }
        }
        return midNights;
    }

    private int[] getMonthsArray() {
        int size = (int) Math.round((getTimeMillisec(Math.round(endIndex)) -
                getTimeMillisec(Math.round(beginIndex))) / (28L * (long) Miliseconds_Per_Day)) + 10;
        if (size <= 0) return null;

        int[] tmp = new int[size];
        int count = 0;
        int preMonth = -1;
        int currMonth = -1;
        GregorianCalendar calendar = new GregorianCalendar();
        for (int i = Math.round(beginIndex); i <= endIndex; i++) {
            calendar.setTimeInMillis(getTimeMillisec(i));
            currMonth = calendar.get(calendar.YEAR) * 100 + calendar.get(calendar.MONTH);
            if (currMonth > preMonth) {
                tmp[count] = i;
                count++;
                if (count == size) break;
                preMonth = currMonth;
            }
        }
        if (count > 0) {
            int[] fullArr = new int[count];
            for (int i = 0; i < count; i++) {
                fullArr[i] = tmp[i];
            }
            return fullArr;
        }
        return null;
    }

    public void drawXaxisOnGraphics(Graphics g, boolean isPrinting, int x,
                                    int y, int XImgWd, int XImgHt) {
        g.setClip(x, y, XImgWd, XImgHt + graph.zSlider.getHeight());
        Graphics2D gX = (Graphics2D) g;
        if (isPrinting) {
            gX.setColor(Color.white);

            GradientPaint redtowhite = new GradientPaint(x, y, graph.getAxisBGOuterColor(), x, y + XImgHt, graph.getAxisBGInnerColor());
            gX.setPaint(redtowhite);
            gX.fillRect(x, y, XImgWd, XImgHt + graph.zSlider.getHeight());//changed to themable the zslider
            gX.setColor(Color.BLACK);
            gX.fillRect(x, y, XImgWd, 1);
        } else {
            g.setClip(x, y, XImgWd, XImgHt);
            gX.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

            GradientPaint redtowhite = new GradientPaint(x, y, graph.getAxisBGOuterColor(), x, y + XImgHt, graph.getAxisBGInnerColor());
            gX.setPaint(redtowhite);
            gX.fillRect(x, y, XImgWd, XImgHt);
        }

        Date dd = new Date();
        String label;

        java.awt.Font aFont;
        gX.setColor(graph.fontColor);
        if (isPrinting) {
            aFont = graph.getAxisFont();
            //aFont = graph.font_PLAIN_8;
        } else {
            //aFont = graph.font_PLAIN_9;
            aFont = graph.getAxisFont();
        }

        gX.setFont(aFont);
        gX.setColor(graph.getAxisColor());
        gX.drawLine(x, y, x + XImgWd, y);

        if (graphStoreSize < 1) {
            g.setClip(null);
            return;
        }

        int xTmp = 0;
        int preX = -1000;
        int lastLabelLength = 0;
        g.setColor(graph.fontColor);
        ((Graphics2D) g).setStroke(graph.BS_0p8f_solid);

        int bestMinutePitch = getBestTimeInterval(endIndex - beginIndex, XImgWd - 2 * clearance);

        try {
            int[] majorPoints = getMajorPointArray(bestMinutePitch);
            if ((majorPoints != null) && (majorPoints.length > 0)) {
                for (int i = 0; i < majorPoints.length; i++) {
                    xTmp = Math.round((majorPoints[i] - beginIndex) * xFactor) + x + clearance;
                    dd.setTime(getTimeMillisec(majorPoints[i]));
                    if (graph.isCurrentMode()) {

                        label = dateFormatter_ddMMMyyyy.format(dd);
                    } else {
                        if ((bestMinutePitch < 30) && (interval != StockGraph.MONTHLY)) {
                            label = SharedMethods.getMonthLocaleForDate(dd);
                        } else {
                            label = dateFormatter_yyyy.format(dd);
                        }
                    }
                    if (preX + lastLabelLength < xTmp) {
                        g.drawLine(xTmp, y, xTmp, y + 23);
                        gX.drawString(label, xTmp + 2, 23 + y);
                        lastLabelLength = gX.getFontMetrics(aFont).stringWidth(label) + 5;
                        preX = xTmp;
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        lastLabelLength = 0;
        if (bestMinutePitch > 0) {
            long nextTime = calculateBeginTime(getTimeMillisec(Math.round(beginIndex)), bestMinutePitch);
            long currTime;
            int currPos = Math.round(beginIndex);
            int prevPixVal = Math.round((currPos - beginIndex) * xFactor) + x + clearance - 1000;
            int currPixVal;

            while (currPos <= endIndex) {  // - clearance
                currTime = getTimeMillisec(currPos);

                if (currTime >= nextTime) {
                    nextTime = getNextPlottingTime(currTime, bestMinutePitch);
                    currPixVal = Math.round((currPos - beginIndex) * xFactor) + x + clearance;
                    if (currPixVal >= prevPixVal + lastLabelLength) {
                        dd.setTime(currTime);
                        if (graph.isCurrentMode()) {
                            label = timeFormatter.format(dd);
                        } else {
                            if (getSuitableDateFormatter(bestMinutePitch) == dateFormatter_MMM) {
                                label = SharedMethods.getMonthLocaleForDate(dd);
                            } else {
                                label = getSuitableDateFormatter(bestMinutePitch).format(dd);
                            }
                        }
                        lastLabelLength = gX.getFontMetrics(aFont).stringWidth(label) + 5;

                        if (!isExceedingDateBoundary(currPixVal + lastLabelLength, x + clearance, currTime)) {
                            gX.setColor(graph.getAxisColor());
                            gX.drawLine(currPixVal, 0 + y, currPixVal, 10 + y);
                            gX.setColor(graph.fontColor);
                            gX.drawString(label, currPixVal + 2, 11 + y);
                            prevPixVal = currPixVal;
                        }
                    }
                }
                currPos++;
            }
        }
        g.setClip(null);
    }

    public void drawPanelTitles(Graphics gg, boolean isPrinting) {
        for (int i = 0; i < graph.panels.size(); i++) {
            if (graph.isMaximized() && (i != graph.getMaximizedPanelID())) continue;
            int mytitle = titleHeight;
            WindowPanel r = (WindowPanel) graph.panels.get(i);
            if (r == graph.mainRect) {
                //changed by charithn
                boolean isMode = ChartOptions.getCurrentOptions().chart_main_title[0];
                boolean isPeriod = ChartOptions.getCurrentOptions().chart_main_title[1];
                boolean isInterval = ChartOptions.getCurrentOptions().chart_main_title[2];
                r.setTitle(getMainGraphTitle(isMode, isPeriod, isInterval));

            } else if (r == graph.volumeRect) {
                r.setTitle(getVolumeGraphTitle());
            } else if (r == graph.turnOverRect) {
                r.setTitle(getTurnOverGraphTitle());
            }

            int adj = 3;
            gg.setClip(borderWidth, r.y, graph.widthYaxis_1 + graph.widthYaxis_2 + r.width + adj, titleHeight);
            gg.setFont(graph.graphTitleFont);
            if (isPrinting) {
                Graphics2D g2 = (Graphics2D) gg;
                GradientPaint pnlColor = new GradientPaint(0, 0, graph.graphPnltitle_LeftColor, graph.widthYaxis_1 + graph.widthYaxis_2 + r.width + adj, titleHeight, graph.graphPnltitle_RightColor);
                g2.setPaint(pnlColor);
                gg.fillRect(borderWidth, r.y, graph.widthYaxis_1 + graph.widthYaxis_2 + r.width + adj, titleHeight);
                gg.setColor(graph.getTitleForeGroundColor());
                //gg.drawString(r.getTitle(), borderWidth + titleHeight + 2 + 5, r.y + titleHeight - 2);
                int titleFontHeight = gg.getFontMetrics().getHeight();
                gg.drawString(r.getTitle(), r.x, r.y + (titleHeight - titleFontHeight / 2) + 2);
            } else {
                if (graph.titleDragged && graph.sourcePanelID > -1 && graph.destPanelID > -1) {
                    if ((graph.below && graph.destPanelID > graph.sourcePanelID && graph.destPanelID == i) ||
                            (!graph.below && graph.destPanelID > graph.sourcePanelID && graph.destPanelID - 1 == i) ||
                            (graph.below && graph.destPanelID < graph.sourcePanelID && graph.destPanelID + 1 == i) ||
                            (!graph.below && graph.destPanelID < graph.sourcePanelID && graph.destPanelID == i) ||
                            (graph.destPanelID == graph.sourcePanelID && graph.destPanelID == i)) {
                        gg.setColor(new Color(120, 120, 120));
                    } else {
                        Graphics2D g2 = (Graphics2D) gg;
                        GradientPaint pnlColor = new GradientPaint(0, 0, graph.graphPnltitle_LeftColor, graph.widthYaxis_1 + graph.widthYaxis_2 + r.width + adj, titleHeight, graph.graphPnltitle_RightColor);
                        g2.setPaint(pnlColor);
                    }
                } else {
                    Graphics2D g2 = (Graphics2D) gg;
                    GradientPaint pnlColor = new GradientPaint(0, 0, graph.graphPnltitle_LeftColor, graph.widthYaxis_1 + graph.widthYaxis_2 + r.width + adj, titleHeight, graph.graphPnltitle_RightColor);
                    g2.setPaint(pnlColor);
                }
                int titleFontHeight = gg.getFontMetrics().getHeight();
                adj = 0;
                gg.fillRect(borderWidth, r.y, graph.widthYaxis_1 + graph.widthYaxis_2 + r.width + adj, titleHeight);
                gg.setColor(graph.getTitleForeGroundColor());
                //gg.drawString(r.getTitle(), borderWidth + titleHeight + 2 + 5, r.y + titleHeight - (titleHeight / 6));
                gg.drawString(r.getTitle(), 13, r.y + (titleHeight - titleFontHeight / 2) + 2);
                //gg.drawImage(imgIconWindow, borderWidth + 2, r.y, graph);//TODO: if required
                gg.setColor(graph.getAxisColor());
                gg.drawLine(r.x, r.y + titleHeight - 1, r.x + r.width, r.y + titleHeight - 1);
            }
            gg.setClip(null);
        }
    }

    private TWDateFormat getSuitableDateFormatter(int bestMinutePitch) {
        if ((bestMinutePitch < 30) && (interval != StockGraph.MONTHLY)) {
            return dateFormatter_dd;
        } else if (bestMinutePitch < 360) {
            return dateFormatter_MMM;
        } else {
            return dateFormatter_yyyy;
        }
    }

    private long getNextPlottingTime(long currTime, int bestMinutePitch) {
        if (graph.isCurrentMode()) {
            return getImmediateRoundedTime(currTime, bestMinutePitch) + bestMinutePitch * (long) MinimumUnit * 1000L;
        } else {
            if (bestMinutePitch < 30) {
                return getImmediateRoundedTime(currTime, bestMinutePitch) + bestMinutePitch * (long) MinimumUnit * 1000L;
            } else if (bestMinutePitch < 360) {
                return getBeginingOfNextMonthsSet(currTime, bestMinutePitch);
            } else {
                return getBeginingOfNextYearsSet(currTime, bestMinutePitch);
            }
        }
    }

    private long getBeginingOfNextMonthsSet(long currTime, int bestMinutePitch) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(currTime);
        int days = calendar.get(calendar.DAY_OF_MONTH) - 1;
        long firstDayOfMonth = currTime - days * (long) Miliseconds_Per_Day;
        firstDayOfMonth -= (firstDayOfMonth % Miliseconds_Per_Day);
        calendar.setTimeInMillis(firstDayOfMonth);
        int NoOfMonthsInSet = getNoOfPeriodsInSet(bestMinutePitch);
        int adj = NoOfMonthsInSet - calendar.get(calendar.MONTH) % NoOfMonthsInSet;
        calendar.add(calendar.MONTH, adj);
        return calendar.getTimeInMillis();
    }

    private long getBeginingOfNextYearsSet(long currTime, int bestMinutePitch) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(currTime);
        int days = calendar.get(calendar.DAY_OF_YEAR) - 1;
        long firstDayOfYear = currTime - days * (long) Miliseconds_Per_Day;
        firstDayOfYear -= (firstDayOfYear % Miliseconds_Per_Day);
        calendar.setTimeInMillis(firstDayOfYear);
        int NoOfYearsInSet = getNoOfPeriodsInSet(bestMinutePitch);
        int adj = NoOfYearsInSet - calendar.get(calendar.YEAR) % NoOfYearsInSet;
        calendar.add(calendar.YEAR, adj);
        return calendar.getTimeInMillis();
    }

    private int getNoOfPeriodsInSet(int bestMinutePitch) {
        switch (bestMinutePitch) {
            case 30:
                return 1;
            case 60:
                return 2;
            case 90:
                return 3;
            case 120:
                return 4;
            case 365:
                return 1;
            case 365 * 2:
                return 2;
            case 365 * 4:
                return 4;
            case 365 * 5:
                return 5;
            case 365 * 10:
                return 10;
            case 365 * 20:
                return 20;
            default:
                return 1;
        }
    }

    long getImmediateRoundedTime(long currTime, int bestMinPitch) {
        long tmp = (currTime / (bestMinPitch * (long) MinimumUnit * 1000L));
        return tmp * bestMinPitch * MinimumUnit * 1000L;
    }

    private boolean isExceedingDateBoundary(int pixPos, int Left, long currTime) {
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        longArr[0] = new Long(getMidnight(currTime, graph.isCurrentMode()));
        searchIndex = graphStore.indexOfContainingValue(longArr);
        if (searchIndex < 0) searchIndex = -searchIndex - 1;
        int nextPixVal = Math.round((searchIndex - beginIndex) * xFactor) + Left;
        if (nextPixVal < pixPos) return true;
        return false;
    }

    public long getTimeMillisec(int index) {
        try {
            if (index < 0) {
                index = 0;
            } else if (graphStore.size() <= index) {
                index = graphStore.size() - 1;
            }
            Object[] objArr = (Object[]) graphStore.get(index);
            if ((objArr != null) && (objArr.length > 0) && (objArr[0] != null)) {
                return (Long) objArr[0];
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    private int getLastIndexForAlarm(int graphIndex) {
        int baseIndex = graphStore.size() - 1;
        double base = 0;
        ChartPoint chartPoint;
        for (int i = (graphStore.size() - 1); i >= periodBeginIndex; i--) {
            long timeInd = getTimeMillisec(i);
            chartPoint = readChartPoint(timeInd, graphIndex);
            if (chartPoint != null) {
                ChartProperties cp = (ChartProperties) Sources.get(graphIndex);
                if (cp.getID() > ID_COMPARE) {
                    base = chartPoint.getValue(INDICATOR);
                } else {
                    base = chartPoint.getValue(cp.getOHLCPriority());
                    base = Math.min(base, chartPoint.Volume);  //here open,high,low,volume,turnover also should be > 0
                }
                if (base > 0) {
                    baseIndex = i;
                    break;
                }
            }
        }

        return baseIndex;
    }

    private int getLastNonZeroIndex(int graphIndex) {
        int baseIndex = graphStore.size() - 1;
        double base = 0;
        ChartPoint chartPoint;
        for (int i = (graphStore.size() - 1); i >= periodBeginIndex; i--) {
            long timeInd = getTimeMillisec(i);
            chartPoint = readChartPoint(timeInd, graphIndex);
            if (chartPoint != null) {
                ChartProperties cp = (ChartProperties) Sources.get(graphIndex);
                if (cp.getID() > ID_COMPARE) {
                    base = chartPoint.getValue(INDICATOR);
                } else {
                    base = chartPoint.getValue(cp.getOHLCPriority());
                }
                if (base > 0) {
                    baseIndex = i;
                    break;
                }
            }
        }

        return baseIndex;
    }

    private double calculateBeginValue(double minYp, double bestPitch) {
        return bestPitch * Math.floor(minYp / bestPitch);
    }

    private long calculateBeginTime(long beginIndex, int bestPitch) {
        return (long) (Math.floor(beginIndex / (bestPitch * (long) MinimumUnit * 1000L)) * bestPitch * MinimumUnit * 1000L);
    }

    public void setZoomAndPos(float sZoom, float bgnPos) {
        isBusyNow = true;
        beginIndex = Math.max(bgnPos * (graphStore.size() - periodBeginIndex) / 100 + periodBeginIndex, periodBeginIndex);
        endIndex = Math.min(beginIndex + sZoom * (graphStore.size() - periodBeginIndex) / 100 - 1, graphStore.size() - 1);
        if (graphStore.size() > 1) {
            if (endIndex - beginIndex < 1) {
                beginIndex = Math.max((float) Math.floor(beginIndex), periodBeginIndex);
                endIndex = Math.min(beginIndex + 1, graphStore.size() - 1);
                if (beginIndex == endIndex) {
                    beginIndex = Math.max(endIndex - 1, periodBeginIndex);
                }
            }
        }
        isBusyNow = false;
        graph.reDrawAll();
    }

    private void refreshZoomAndPos() {
        float bgnPos = graph.zSlider.getBeginPos();
        float sZoom = graph.zSlider.getZoom();
        beginIndex = Math.max(bgnPos * (graphStore.size() - periodBeginIndex) / 100 + periodBeginIndex, periodBeginIndex);
        endIndex = Math.min(beginIndex + sZoom * (graphStore.size() - periodBeginIndex) / 100, graphStore.size() - 1);
        reCalculateGraphParams();
    }

    public float getIndexForthePixel(int pixel) {

        if (xFactor > 0)
            return (pixel - graph.mainRect.x - clearance) / xFactor + beginIndex;
        return 0;
    }

    public int getPixelFortheIndex(float index) {
        if (xFactor > 0)
            return Math.round((index - beginIndex) * xFactor) + graph.mainRect.x + clearance;
        return 0;
    }

    public double getYValueForthePixel(int pixel, int pnlID) {
        double val = 0;
        if (pnlID >= 0) {
            WindowPanel r = (WindowPanel) graph.panels.get(pnlID);
            if (r.isSemiLog() && (minY[pnlID] > 0)) {
                val = Math.exp((r.y + r.height - clearance - pixel) / yFactor[pnlID] + Math.log(minY[pnlID]));
            } else {
                val = (r.y + r.height - clearance - pixel) / yFactor[pnlID] + minY[pnlID];
            }
        }
        return val;
    }

    public int getPixelFortheYValue(double value, int pnlID) {
        int pixel = 0;
        if (pnlID >= 0) {
            WindowPanel r = (WindowPanel) graph.panels.get(pnlID);
            if (r.isSemiLog() && (minY[pnlID] > 0)) {
                pixel = r.y + r.height - clearance - (int) Math.round((Math.log(value) - Math.log(minY[pnlID])) * yFactor[pnlID]);
            } else {
                pixel = r.y + r.height - clearance - (int) Math.round((value - minY[pnlID]) * yFactor[pnlID]);
            }
        }
        return pixel;
    }

    public int getNearestWholeIndex(int timeIndex, int chartIndex, boolean searchUp) {
        int i = timeIndex;
        for (; ; ) {
            Object[] oa;
            if (i < graphStore.size()) {
                oa = (Object[]) graphStore.get(i);
            } else {
                i = graphStore.size() - 1;
                break;
            }
            if ((oa.length > chartIndex) && (oa[chartIndex] != null)) {
                return i;
            }
            if (searchUp) {
                if (i >= graphStore.size() - 1) break;
                i++;
            } else {
                if (i <= 0) break;
                i--;
            }
        }
        return i;
    }

    public boolean isaYValueMatching(int indX_1, int indX_2, int X, int Y, boolean select, boolean isMove) {
        boolean result = false;
        boolean isPointX1 = true;
        int currY1, currY2, currX1, currX2;
        int H1, L1, H2, L2;
        Object[] secondPoint;
        ChartPoint secondSource;
        Line2D.Float Line;
        Rectangle Rect;
        int dataLines = 6;
        int smblWd = 14;

        float indX = getIndexForthePixel(X);
        int indX1, indX2;
        Object[] singlePoint;
        ChartPoint singleSource;

        if (graphStore.size() > 0)
            if ((indX_1 >= 0) && (indX_2 <= graphStore.size() - 1)) {
                int left, top, height;
                for (int j = sourceCount; j > 0; j--) {
                    ChartProperties cp = (ChartProperties) Sources.get(j - 1);
                    if ((cp.getID() == ID_VOLUME) && (!graph.isShowingVolumeGrf())) continue;
                    if (cp.isHidden()) continue;
                    indX1 = getNearestWholeIndex(indX_1, j, false);
                    indX2 = getNearestWholeIndex(indX_2, j, true);
                    singlePoint = (Object[]) graphStore.get(indX1);
                    secondPoint = (Object[]) graphStore.get(indX2);
                    if ((singlePoint.length <= j) || (singlePoint[j] == null)) {
                        continue;
                    }
                    if ((secondPoint.length <= j) || (secondPoint[j] == null)) {
                        continue;
                    }
                    Rectangle r = cp.getRect();
                    if (!r.contains(X, Y)) {
                        continue;
                    }
                    int pnlID = getIndexOfTheRect(graph.panels, r);
                    left = r.x + clearance;
                    top = r.y + titleHeight;
                    height = r.height - titleHeight;
                    singleSource = (ChartPoint) singlePoint[j];
                    secondSource = (ChartPoint) secondPoint[j];

                    if (cp.getID() <= ID_COMPARE && (singleSource.Open < 0 || secondSource.Open < 0)) {
                        continue;
                    }

                    currX1 = Math.round((indX1 - beginIndex) * xFactor) + left;
                    currX2 = Math.round((indX2 - beginIndex) * xFactor) + left;
                    if (cp.getID() > ID_COMPARE) {
                        currY1 = getPixelFortheYValue(singleSource.getValue(INDICATOR) * indexingFactors[j - 1], pnlID);
                        currY2 = getPixelFortheYValue(secondSource.getValue(INDICATOR) * indexingFactors[j - 1], pnlID);

                        if ((cp.getID() == GraphDataManager.ID_CUSTOM_INDICATOR) && (((IndicatorBase) cp).isDrawSymbol())) {
                            IndicatorBase ib = (IndicatorBase) cp;
                            if (ib.getSymbolID() >= 1000) continue; // Candlestick pattern - Checking Later
                            //calculate x and y
                            String symbl = new String(saSymbol, ib.getSymbolID() - 1, 1);//saSymbol[ib.getSymbolID() - 1];
                            Graphics g = graph.getGraphics();
                            FontMetrics fm = g.getFontMetrics(f1);
                            int sw = fm.stringWidth(symbl);
                            float vAdj = 0;
                            switch (ib.getPosition()) {
                                case -1:
                                    vAdj = 2 + fm.getHeight() / 2f;
                                    break;
                                case 1:
                                    vAdj = -fm.getHeight() / 2f - 2;
                                    break;
                                default:
                                    vAdj = 0;
                                    break;
                            }
                            //point 1
                            float y = currY1 + vAdj;
                            if (minCustomY[pnlID] == maxCustomY[pnlID]) {
                                y = (float) Math.min(y, r.getY() + r.getHeight() - fm.getHeight() / 2f);
                                y = (float) Math.max(y, r.getY() + titleHeight + fm.getHeight() / 2f);
                            }
                            Rect = new Rectangle(currX1 - smblWd / 2, (int) y - smblWd / 2, smblWd, smblWd);
                            if (Rect.contains(X, Y)) {
                                result = true;
                                isPointX1 = true;
                            } else {
                                //point 2
                                y = currY2 + vAdj;
                                if (minCustomY[pnlID] == maxCustomY[pnlID]) {
                                    y = (float) Math.min(y, r.getY() + r.getHeight() - fm.getHeight() / 2f);
                                    y = (float) Math.max(y, r.getY() + titleHeight + fm.getHeight() / 2f);
                                }
                                Rect = new Rectangle(currX2 - smblWd / 2, (int) y - smblWd / 2, smblWd, smblWd);
                                if (Rect.contains(X, Y)) {
                                    result = true;
                                    isPointX1 = false;
                                }
                            }
                        } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR) {
                            int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r)); //
                            if (currX1 == currX2) {
                                if ((currY1 - Y) * (zero - Y) <= 0) {
                                    result = true;
                                    isPointX1 = true;
                                } else if ((currY2 - Y) * (zero - Y) <= 0) {
                                    result = true;
                                    isPointX1 = false;
                                }
                            } else {
                                Rect = new Rectangle(currX1 - dashLength, Math.min(currY1, zero), barThickness, Math.abs(currY1 - zero));
                                if (Rect.contains(X, Y)) {
                                    result = true;
                                    isPointX1 = true;
                                } else {
                                    Rect.setBounds(currX2 - dashLength, Math.min(currY2, zero), barThickness, Math.abs(currY2 - zero));
                                    if (Rect.contains(X, Y)) {
                                        result = true;
                                        isPointX1 = false;
                                    }
                                }
                            }
                        } else if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_PERCET) {
                            if (((cp.getID() == ID_NET_TURNOVER) && (((NetTurnOverInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) || (cp.getID() == ID_NET_ORDERS) && (((NetOrdersInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) {
                                int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
                                int hundred = getPixelFortheYValue(100d, graph.getIDforPanel(r));
//
                                if (currX1 == currX2) {
                                    if ((hundred - Y) * (zero - Y) <= 0) {
                                        result = true;
                                        isPointX1 = true;
                                    } else if ((hundred - Y) * (zero - Y) <= 0) {
                                        result = true;
                                        isPointX1 = false;
                                    }
                                } else {
                                    Rect = new Rectangle(currX1 - dashLength, Math.min(hundred, zero), barThickness, Math.abs(hundred - zero));
                                    if (Rect.contains(X, Y)) {
                                        result = true;
                                        isPointX1 = true;
                                    } else {
                                        Rect.setBounds(currX2 - dashLength, Math.min(hundred, zero), barThickness, Math.abs(hundred - zero));
                                        if (Rect.contains(X, Y)) {
                                            result = true;
                                            isPointX1 = false;
                                        }
                                    }
                                }
                            } else { //calc type == point - just like bar calculation for yvalue matching
                                int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
//
                                if (currX1 == currX2) {
                                    if ((currY1 - Y) * (zero - Y) <= 0) {
                                        result = true;
                                        isPointX1 = true;
                                    } else if ((currY2 - Y) * (zero - Y) <= 0) {
                                        result = true;
                                        isPointX1 = false;
                                    }
                                } else {
                                    Rect = new Rectangle(currX1 - dashLength, Math.min(currY1, zero), barThickness, Math.abs(currY1 - zero));
                                    if (Rect.contains(X, Y)) {
                                        result = true;
                                        isPointX1 = true;
                                    } else {
                                        Rect.setBounds(currX2 - dashLength, Math.min(currY2, zero), barThickness, Math.abs(currY2 - zero));
                                        if (Rect.contains(X, Y)) {
                                            result = true;
                                            isPointX1 = false;
                                        }
                                    }
                                }

                            }
                        } else {
                            if (currX1 == currX2) {
                                if ((currY1 < Y + 2) && (currY1 > Y - 2)) {
                                    result = true;
                                    isPointX1 = true;
                                } else if ((currY2 < Y + 2) && (currY2 > Y - 2)) {
                                    result = true;
                                    isPointX1 = false;
                                }
                            } else {
                                Line = new Line2D.Float(currX1, currY1, currX2, currY2);
                                if (Line.ptLineDist(X, Y) < 2) {
                                    if (((currX1 - X) * (currX2 - X) <= 0) &&
                                            ((currY1 - Y) * (currY2 - Y) <= 0)) {
                                        result = true;
                                        isPointX1 = (indX - indX1 <= indX2 - indX);
                                    }
                                }
                            }
                        }
                    } else {

                        if (cp.getChartStyle() > StockGraph.INT_GRAPH_STYLE_BAR) {
                            L1 = getPixelFortheYValue(singleSource.Low * indexingFactors[j - 1], pnlID);
                            H1 = getPixelFortheYValue(singleSource.High * indexingFactors[j - 1], pnlID);
                            L2 = getPixelFortheYValue(secondSource.Low * indexingFactors[j - 1], pnlID);
                            H2 = getPixelFortheYValue(secondSource.High * indexingFactors[j - 1], pnlID);
                            if (currX1 == currX2) {
                                if ((L1 >= Y) && (H1 <= Y)) {
                                    result = true;
                                    isPointX1 = true;
                                } else if ((L2 >= Y) && (H2 <= Y)) {
                                    result = true;
                                    isPointX1 = false;
                                }
                            } else {
                                if (L1 == H1 || L2 == H2) {//if the high and low are equal
                                    L1 += dashLength;
                                    L2 += dashLength;
                                    H1 -= dashLength;
                                    H2 -= dashLength;
                                }
                                Rect = new Rectangle(currX1 - dashLength, H1, barThickness, L1 - H1);
                                if (Rect.contains(X, Y)) {
                                    result = true;
                                    isPointX1 = true;
                                } else {
                                    Rect.setBounds(currX2 - dashLength, H2, barThickness, L2 - H2);
                                    if (Rect.contains(X, Y)) {
                                        result = true;
                                        isPointX1 = false;
                                    }
                                }
                            }
                        } else {

                            currY1 = getPixelFortheYValue(singleSource.getValue(cp.getOHLCPriority()) * indexingFactors[j - 1], pnlID);
                            currY2 = getPixelFortheYValue(secondSource.getValue(cp.getOHLCPriority()) * indexingFactors[j - 1], pnlID);
                            if (cp.getChartStyle() == StockGraph.INT_GRAPH_STYLE_BAR) {
                                if (currX1 == currX2) {
                                    if ((height + top - clearance + barExt >= Y) && (currY1 <= Y)) {
                                        result = true;
                                        isPointX1 = true;
                                    } else if ((height + top - clearance + barExt >= Y) && (currY2 <= Y)) {
                                        result = true;
                                        isPointX1 = false;
                                    }
                                } else {
                                    Rect = new Rectangle(currX1 - dashLength, currY1, barThickness, height + top - clearance + barExt - currY1);
                                    if (Rect.contains(X, Y)) {
                                        result = true;
                                        isPointX1 = true;
                                    } else {
                                        Rect.setBounds(currX2 - dashLength, currY2, barThickness, height + top - clearance + barExt - currY2);
                                        if (Rect.contains(X, Y)) {
                                            result = true;
                                            isPointX1 = false;
                                        }
                                    }
                                }
                            } else {
                                if (currX1 == currX2) {
                                    if ((currY1 < Y + 2) && (currY1 > Y - 2)) {
                                        result = true;
                                        isPointX1 = true;
                                    } else if ((currY2 < Y + 2) && (currY2 > Y - 2)) {
                                        result = true;
                                        isPointX1 = false;
                                    }
                                } else {
                                    Line = new Line2D.Float(currX1, currY1, currX2, currY2);
                                    if (Line.ptLineDist(X, Y) < 2) {
                                        if (((currX1 - X) * (currX2 - X) <= 0) &&
                                                ((currY1 - Y) * (currY2 - Y) <= 0)) {
                                            result = true;
                                            isPointX1 = (indX - indX1 <= indX2 - indX);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (result) {
                        Object[] aPoint;
                        ChartPoint aSource;
                        if (isPointX1) {  //indX-indX1<0.5
                            aPoint = singlePoint;
                            aSource = singleSource;
                        } else {
                            aPoint = secondPoint;
                            aSource = secondSource;
                        }
                        if (cp.getID() > ID_COMPARE) {
                            if ((cp instanceof IndicatorBase) && ((IndicatorBase) cp).isDrawSymbol()) {
                                graph.priceOpenValue = "";
                                dataLines = 1;
                            } else if (cp.getID() == ID_VOLUME) {
                                graph.priceOpenValue = thousandformat.format(aSource.getValue(INDICATOR));
                                dataLines = 2;
                            } else if (((cp.getID() == ID_NET_TURNOVER) && (((NetTurnOverInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) || (cp.getID() == ID_NET_ORDERS) && (((NetOrdersInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) {
                                GraphCalcTypeApplicable netCP = (GraphCalcTypeApplicable) cp;
                                netCP.clearToolTipData();
                                /*****time value*/
                                SimpleDateFormat formatter;
                                formatter = (interval == StockGraph.MONTHLY) ? new SimpleDateFormat("MMM yyyy") : new SimpleDateFormat("dd/MM/yy");    //   "S"
                                Date dd = new Date(((Long) aPoint[0]));
                                netCP.addToolTipData("DATE", formatter.format(dd));

                                /*****time value*/

                                netCP.addToolTipData("BID_PERCENTAGE", formatPriceField(aSource.getValue(INDICATOR), false) + "%");
                                netCP.addToolTipData("BID_VALUE", formatPriceField(aSource.getValue(INNER_High), false));
                                netCP.addToolTipData("OFFER_VALUE", formatPriceField(aSource.getValue(INNER_Low), false));

                            } else if (((cp.getID() == ID_NET_TURNOVER) && (((NetTurnOverInd) cp).getCalcType() == NetTurnOverInd.POINTS)) || (cp.getID() == ID_NET_ORDERS) && (((NetOrdersInd) cp).getCalcType() == NetTurnOverInd.POINTS)) {
                                GraphCalcTypeApplicable netCP = (GraphCalcTypeApplicable) cp;
                                netCP.clearToolTipData();
                                /*****time value*/
                                SimpleDateFormat formatter;
                                formatter = (interval == StockGraph.MONTHLY) ? new SimpleDateFormat("MMM yyyy") : new SimpleDateFormat("dd/MM/yy");    //   "S"
                                Date dd = new Date(((Long) aPoint[0]));
                                netCP.addToolTipData("DATE", formatter.format(dd));

                                /*****time value*/

                                netCP.addToolTipData("CASH_FLOW_VALUE", formatPriceField(aSource.getValue(INDICATOR), false));


                            } else {
                                graph.priceOpenValue = formatPriceField(aSource.getValue(INDICATOR), false);
                                dataLines = 2;
                            }
                            graph.priceHighValue = "";
                            graph.priceLowValue = "";
                            graph.priceCloseValue = "";
                            graph.currVolume = "";
                            graph.currTurnOver = "";
                            graph.currSymbol = cp.toString();
                            graph.currGrfColor = cp.getWarningColor();
                            graph.setTimeValue((Long) aPoint[0]);
                        } else {

                            graph.priceOpenValue = (aSource.Open >= 0) ? formatPriceField(aSource.Open, false) : NA;
                            graph.priceHighValue = (aSource.High != Double.POSITIVE_INFINITY) ? formatPriceField(aSource.High, false) : NA;
                            graph.priceLowValue = (aSource.Low >= 0) ? formatPriceField(aSource.Low, false) : NA;

                            /*graph.priceOpenValue = formatPriceField(aSource.Open, false);
                            graph.priceHighValue = formatPriceField(aSource.High, false);
                            graph.priceLowValue = formatPriceField(aSource.Low, false);*/
                            graph.priceCloseValue = formatPriceField(aSource.Close, false);


                            graph.currVolume = (aSource.Volume > 0) ? thousandformat.format(aSource.Volume) : NA;
                            graph.currTurnOver = (aSource.TurnOver > 0) ? thousandformat.format(aSource.TurnOver) : NA;

                            /*if (aSource.Volume > 0) {
                                graph.currVolume = thousandformat.format(aSource.Volume);
                            } else {
                                graph.currVolume = NA;
                            }
                            if (aSource.TurnOver > 0) {
                                graph.currTurnOver = thousandformat.format(aSource.TurnOver);
                            } else {
                                graph.currTurnOver = NA;
                            }*/

                            graph.currSymbol = StockGraph.extractSymbolFromStr(cp.getSymbol());
                            //graph.currGrfColor = cp.getColor();
                            graph.setTimeValue(((Long) aPoint[0]));
                            dataLines = 6;
                        }


                        if (((cp.getID() == ID_NET_TURNOVER) && (((NetTurnOverInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) || (cp.getID() == ID_NET_ORDERS) && (((NetOrdersInd) cp).getCalcType() == NetTurnOverInd.PERCENT)) {
                            GraphCalcTypeApplicable netCP = (GraphCalcTypeApplicable) cp;
                            if (select) {
                                graph.updateToolTip(false, dataLines, netCP.getToolTipRows(), isMove, "");
                                cp.setSelected(true);
                                sourceHiLighted = j - 1;
                            } else {
                                graph.updateToolTip(true, dataLines, netCP.getToolTipRows(), isMove, "");
                            }

                        } else if (((cp.getID() == ID_NET_TURNOVER) && (((NetTurnOverInd) cp).getCalcType() == NetTurnOverInd.POINTS)) || (cp.getID() == ID_NET_ORDERS) && (((NetOrdersInd) cp).getCalcType() == NetTurnOverInd.POINTS)) {
                            GraphCalcTypeApplicable netCP = (GraphCalcTypeApplicable) cp;
                            if (select) {
                                graph.updateToolTip(false, dataLines, netCP.getToolTipRows(), isMove, "");
                                cp.setSelected(true);
                                sourceHiLighted = j - 1;
                            } else {
                                graph.updateToolTip(true, dataLines, netCP.getToolTipRows(), isMove, "");
                            }

                        } else {
                            if (select) {
                                graph.updateToolTip(false, dataLines, null, isMove, "");
                                cp.setSelected(true);
                                sourceHiLighted = j - 1;
                            } else {
                                if (isMove) {
                                    graph.currGrfColor = cp.getColor();
                                    graph.updateToolTip(true, dataLines, null, isMove, "");//on a line
                                } else {
                                    graph.updateToolTip(false, dataLines, null, !isMove, "");
                                }
                            }
                        }
                        return result;
                    }
                }
                //////////////////// detecting candlestick pattern markings //////////////////////////////////////////
                ChartProperties cpBase = getBaseCP();
                int baseIndex = getIndexForSourceCP(cpBase);
                int dLen = Math.max(barThickness, 7);
                for (int j = Sources.size() - 1; j >= 0; j--) {
                    ChartProperties cp = Sources.get(j);
                    if ((cp.getID() == GraphDataManager.ID_CUSTOM_INDICATOR) && (((IndicatorBase) cp).isDrawSymbol()) && (((IndicatorBase) cp).getSymbolID() >= 1000)) {
                        Rectangle r = cp.getRect();
                        if (!r.contains(X, Y)) {
                            continue;
                        }
                        int len = ((int[]) faTime[j]).length;
                        int[] yInd = (int[]) faOpen[j];
                        int tmpID = ((IndicatorBase) cp).getSymbolID() - 1000;
                        for (int i = len - 1; i >= 0; i--) {
                            int xInd = yInd[i];
                            if (xInd < tmpID) continue;
                            int x1, x2, H, L;
                            x1 = ((int[]) faTime[baseIndex])[xInd - tmpID];
                            x2 = ((int[]) faTime[baseIndex])[xInd];
                            switch (cpBase.getChartStyle()) {
                                case StockGraph.INT_GRAPH_STYLE_LINE:
                                    int[] yPrio = getPriotityArray(cp.getOHLCPriority(), baseIndex);
                                    H = yPrio[xInd - tmpID];
                                    L = yPrio[xInd - tmpID];
                                    if (tmpID > 0) {
                                        for (int k = 0; k < tmpID; k++) {
                                            H = Math.min(H, yPrio[xInd - k]);
                                            L = Math.max(L, yPrio[xInd - k]);
                                        }
                                    }
                                    break;
                                case StockGraph.INT_GRAPH_STYLE_BAR:
                                    int zero = getPixelFortheYValue(0d, graph.getIDforPanel(r));
                                    int[] yPrio1 = getPriotityArray(cp.getOHLCPriority(), baseIndex);
                                    H = Math.min(yPrio1[xInd - tmpID], zero);
                                    L = Math.max(yPrio1[xInd - tmpID], zero);
                                    if (tmpID > 0) {
                                        for (int k = 0; k < tmpID; k++) {
                                            H = Math.min(H, yPrio1[xInd - k]);
                                            L = Math.max(L, yPrio1[xInd - k]);
                                        }
                                    }
                                    break;
                                default: // HLC, OHLC, CANDLE
                                    H = ((int[]) faHigh[baseIndex])[xInd - tmpID];
                                    L = ((int[]) faLow[baseIndex])[xInd - tmpID];
                                    if (tmpID > 0) {
                                        for (int k = 0; k < tmpID; k++) {
                                            H = Math.min(H, ((int[]) faHigh[baseIndex])[xInd - k]);
                                            L = Math.max(L, ((int[]) faLow[baseIndex])[xInd - k]);
                                        }
                                    }
                                    break;
                            }
                            Rectangle rc = new Rectangle(x1 - dLen, H - dLen, 2 * dLen + x2 - x1, L - H + 2 * dLen);
                            if (rc.contains(X, Y)) {
                                /////////////////// setting the tooltip for pattern ///////////////////////
                                int xIndex = Math.min(Math.round(getIndexForthePixel(x2)), graphStore.size() - 1);
                                long time = getTimeMillisec(xIndex);
                                ChartPoint aSource = readChartPoint(time, baseIndex);
                                graph.priceOpenValue = formatPriceField(aSource.Open, false);
                                graph.priceHighValue = formatPriceField(aSource.High, false);
                                graph.priceLowValue = formatPriceField(aSource.Low, false);
                                graph.priceCloseValue = formatPriceField(aSource.Close, false);
                                graph.currVolume = "";
                                graph.currTurnOver = "";
                                graph.currSymbol = cp.toString();
                                graph.setTimeValue(time);
                                dataLines = 5;
                                ///////////////////////////////////////////////////////////////////////////
                                if (select) {
                                    graph.updateToolTip(false, dataLines, null, isMove, "");
                                    cp.setSelected(true);
                                    sourceHiLighted = j;
                                } else {
                                    if (isMove) {
                                        graph.currGrfColor = cp.getColor();
                                        graph.updateToolTip(true, dataLines, null, isMove, "");//on a pattern
                                    } else {
                                        //graph.currGrfColor = getBaseCP().getColor();
                                        graph.updateToolTip(false, dataLines, null, !isMove, "");
                                    }
                                }
                                return true;
                            }
                        }
                    }
                }
            }
        graph.updateToolTip(false, dataLines, null, !isMove, "");   //TODO :charithn-dragging very important here
        return result;
    }

    public void setDragZoom(int x1, int x2) {
        float bInd = getIndexForthePixel(x1);
        float eInd = getIndexForthePixel(x2);
        beginIndex = Math.max(Math.min(bInd, graphStore.size() - 1), periodBeginIndex);
        endIndex = Math.max(Math.min(eInd, graphStore.size() - 1), periodBeginIndex);
        if (graphStore.size() > 1) {
            if (endIndex - beginIndex < 1) {
                beginIndex = Math.max((float) Math.floor(beginIndex), periodBeginIndex);
                endIndex = Math.min(beginIndex + 1, graphStore.size() - 1);
                if (beginIndex == endIndex) {
                    beginIndex = Math.max(endIndex - 1, periodBeginIndex);
                }
            }
        }

        resetSlider();
        reCalculateGraphParams();
    }

    public void setBoxZoom(int x1, int y1, int x2, int y2) {
        int pnlID = graph.getPanelIDatPoint(x1, y1);
        if (pnlID < 0) return;

        float bInd = getIndexForthePixel(x1);
        float eInd = getIndexForthePixel(x2);
        beginIndex = Math.max(Math.min(bInd, graphStore.size() - 1), periodBeginIndex);
        endIndex = Math.max(Math.min(eInd, graphStore.size() - 1), periodBeginIndex);
        if (graphStore.size() > 1) {
            if (endIndex - beginIndex < 1) {
                beginIndex = Math.max((float) Math.floor(beginIndex), periodBeginIndex);
                endIndex = Math.min(beginIndex + 1, graphStore.size() - 1);
                if (beginIndex == endIndex) {
                    beginIndex = Math.max(endIndex - 1, periodBeginIndex);
                }
            }
        }

        if (y1 < y2) {
            minCustomY[pnlID] = getYValueForthePixel(y2, pnlID);
            maxCustomY[pnlID] = getYValueForthePixel(y1, pnlID);
        } else {
            minCustomY[pnlID] = getYValueForthePixel(y1, pnlID);
            maxCustomY[pnlID] = getYValueForthePixel(y2, pnlID);
        }

        resetSlider();
        reCalculateGraphParams();
    }

    //added by charithn
    public void setZoomIn(int x, int Wd, int widthYaxis_1, int widthYaxis_2, int mainRectX) {

        float temp1 = 0.3f * (endIndex - beginIndex);
        float temp2 = 0.2f * (graphStore.size() - periodBeginIndex);
        float adjAmount = Math.min(temp1, temp2);

        float index = getIndexForthePixel(x);

        if (index < (endIndex + 3 * beginIndex) / 4) {
            endIndex = (float) Math.max(Math.min(endIndex - adjAmount, graphStore.size() - 1), periodBeginIndex);
        } else if (index > (3 * endIndex + beginIndex) / 4) {
            beginIndex = (float) Math.max(Math.min(beginIndex + adjAmount, graphStore.size() - 1), periodBeginIndex);
        } else {
            float MPlusN = Wd - widthYaxis_1 - widthYaxis_2;
            float M = x - mainRectX;
            float N = MPlusN - M;
            beginIndex = (float) Math.max(Math.min(beginIndex + M * adjAmount / MPlusN, graphStore.size() - 1), periodBeginIndex);
            endIndex = (float) Math.max(Math.min(endIndex - N * adjAmount / MPlusN, graphStore.size() - 1), periodBeginIndex);
        }

        if (graphStore.size() > 1) {
            if (endIndex - beginIndex < 1) {
                beginIndex = (float) Math.max((float) Math.floor(beginIndex), periodBeginIndex);
                endIndex = (float) Math.min(beginIndex + 1, graphStore.size() - 1);
                if (beginIndex == endIndex) {
                    beginIndex = (float) Math.max(endIndex - 1, periodBeginIndex);
                }
            }
        }
        resetSlider();
    }

    //added by charithn
    public void setZoomOut(int x, int Wd, int widthYaxis_1, int widthYaxis_2, int mainRectX) {

        float adjAmount = Math.min(0.43f * (endIndex - beginIndex), 0.2f * (graphStore.size() - periodBeginIndex));
        float ind = getIndexForthePixel(x);
        float extra, extra2;
        if (ind < (endIndex + 3 * beginIndex) / 4) {
            extra = periodBeginIndex + adjAmount - beginIndex;
            beginIndex = (float) Math.max(Math.min(beginIndex - adjAmount, graphStore.size() - 1), periodBeginIndex);
            if (extra > 0) {
                endIndex = (float) Math.max(Math.min(endIndex + extra, graphStore.size() - 1), periodBeginIndex);
            }
        } else if (ind > (3 * endIndex + beginIndex) / 4) {
            extra = endIndex + adjAmount - graphStore.size() + 1;
            endIndex = (float) Math.max(Math.min(endIndex + adjAmount, graphStore.size() - 1), periodBeginIndex);
            if (extra > 0) {
                beginIndex = (float) Math.max(Math.min(beginIndex - extra, graphStore.size() - 1), periodBeginIndex);
            }
        } else {
            float MPlusN = Wd - widthYaxis_1 - widthYaxis_2;
            float M = x - mainRectX;
            float N = MPlusN - M;
            extra = periodBeginIndex + N * adjAmount / MPlusN - beginIndex;
            extra2 = endIndex + M * adjAmount / MPlusN - graphStore.size() + 1;
            beginIndex = (float) Math.max(Math.min(beginIndex - N * adjAmount / MPlusN, graphStore.size() - 1), periodBeginIndex);
            endIndex = (float) Math.max(Math.min(endIndex + M * adjAmount / MPlusN, graphStore.size() - 1), periodBeginIndex);
            if (extra > 0) {
                endIndex = (float) Math.max(Math.min(endIndex + extra, graphStore.size() - 1), periodBeginIndex);
            }
            if (extra2 > 0) {
                beginIndex = (float) Math.max(Math.min(beginIndex - extra2, graphStore.size() - 1), periodBeginIndex);
            }
        }

        if (graphStore.size() > 1) {
            if (endIndex - beginIndex < 1) {
                beginIndex = (float) Math.max((float) Math.floor(beginIndex), periodBeginIndex);
                endIndex = (float) Math.min(beginIndex + 1, graphStore.size() - 1);
                if (beginIndex == endIndex) {
                    beginIndex = (float) Math.max(endIndex - 1, periodBeginIndex);
                }
            }
        }
        resetSlider();
    }

    public boolean isOnaLine(int ex, int ey, boolean select, boolean isMove) {
        boolean onLine;
        float indX = getIndexForthePixel(ex);
        int x1 = Math.max(0, (int) Math.round(Math.floor(indX)));
        int x2 = Math.min(x1 + 1, graphStore.size() - 1);
        onLine = isaYValueMatching(x1, x2, ex, ey, select, isMove);
        if (!onLine) {
            if (isOnAnAnnouncement(ex, ey, isMove) < 0) {
                isOnAnObject(ex, ey, select, isMove);
            }
        }
        return onLine;
    }

    public boolean isOnAnObject(int ex, int ey, boolean select, boolean isMove) {
        if (ObjectArray != null) {
            for (int i = ObjectArray.size() - 1; i >= 0; i--) {
                AbstractObject oR = (AbstractObject) ObjectArray.get(i);
                //float[] xArr = convertXArrayTimeToIndex(oR.getXArr());
                float[] xArr = null;
                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                boolean isOnObj = false;
                int pnlID = getIndexOfTheRect(graph.panels, oR.getRect());
                if (pnlID > -1) {
                    if (oR.isShownOnAllPanels()) {
                        isOnObj = oR.isCursorOnObject(ex, ey, xArr, select, beginIndex, 0, xFactor, 0, isMove); // pnlID is modified dynamically here
                        pnlID = getIndexOfTheRect(graph.panels, oR.getRect()); // pnlID must be reloaded for objects on all panels
                    } else {
                        isOnObj = oR.isCursorOnObject(ex, ey, xArr, select, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], isMove);
                    }
                }
                if (isOnObj && oR.DISPLAY_TYPE != AbstractObject.DISPLAY_SATATIC) {   //todo: should do for all objects

                    double yVal = getYValueForthePixel(ey, pnlID);
                    if (((WindowPanel) oR.getRect()).isInThousands()) {
                        graph.priceOpenValue = thousandformat.format(yVal);
                    } else {
                        graph.priceOpenValue = formatPriceField(yVal, false);
                    }
                    graph.priceHighValue = "";
                    graph.priceLowValue = "";
                    graph.priceCloseValue = "";
                    graph.currVolume = "";
                    graph.currTurnOver = "";
                    graph.currSymbol = oR.toString(graph.isCurrentMode());
                    graph.currGrfColor = oR.getColor();
                    int indX = Math.round(getIndexForthePixel(ex));
                    graph.setTimeValue(getTimeMillisec(indX));

                    //new data row for Alarm tooltip-shashika
                    if ((oR instanceof Alarmable) && ((Alarmable) oR).isAlarmEnabled()) {    //todo need to finalize this - zig zag tool tip not working
                        oR.getToolTipRows().add(new ToolTipDataRow(ObjectLineSlope.ALARM_TMESSAGE, Boolean.TRUE.toString()));
                    }
                    graph.updateToolTip(true, 2, oR.getToolTipRows(), isMove, "");
                    return true;
                }
            }
        }
        if (StaticObjectArray != null) {
            for (int i = StaticObjectArray.size() - 1; i >= 0; i--) {
                AbstractObject oR = (AbstractObject) StaticObjectArray.get(i);
                boolean isOnObj = false;
                int pnlID = getIndexOfTheRect(graph.panels, oR.getRect());
                if ((oR instanceof ObjectOrderLine && showOrderLines) ||
                        (oR instanceof ObjectHorizontalPriceLine &&
                                ((ObjectHorizontalPriceLine) oR).getType() == ObjectHorizontalPriceLine.CURRENT_PRICE_LINE &&
                                showCurrentPriceLine) || (oR instanceof ObjectHorizontalPriceLine &&
                        ((ObjectHorizontalPriceLine) oR).getType() == ObjectHorizontalPriceLine.PREVIOUS_CLOSE_LINE &&
                        showPreviousCloseLine) ||
                        (oR instanceof ObjectHorizontalPriceLine &&
                                ((ObjectHorizontalPriceLine) oR).getType() == ObjectHorizontalPriceLine.MIN_MAX_PRICE_LINE &&
                                showMinMaxPriceLines) || (oR instanceof ObjectHorizontalPriceLine &&
                        ((ObjectHorizontalPriceLine) oR).getType() == ObjectHorizontalPriceLine.RESISTANCE_PRICE_LINE &&
                        showResistanceLine) ||
                        (oR instanceof ObjectBidAskTag && showBidAskTags)) {
                    float[] xArr = convertXArrayTimeToIndex(oR.getXArr());
                    isOnObj = oR.isCursorOnObject(ex, ey, xArr, select, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], isMove);
                }
                if (isOnObj) {
                    double yVal = getYValueForthePixel(ey, pnlID);
                    if (((WindowPanel) oR.getRect()).isInThousands()) {
                        graph.priceOpenValue = thousandformat.format(yVal);
                    } else {
                        graph.priceOpenValue = formatPriceField(yVal, false);
                    }
                    graph.priceHighValue = "";
                    graph.priceLowValue = "";
                    graph.priceCloseValue = "";
                    graph.currVolume = "";
                    graph.currTurnOver = "";
                    graph.currSymbol = oR.toString(graph.isCurrentMode());
                    graph.currGrfColor = oR.getColor();
                    graph.updateToolTip(true, 0, null, isMove, "");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * *************this is for drawing the drag image after adding the object******************************
     */
    public void drawDraggedImageForHiLightedObject(Point2D.Float MouseDownPoint,
                                                   Point2D.Float PreMouseDragPoint, Point2D.Float MouseDragPoint,
                                                   boolean firstTimeDrawing, DragWindowListner dragWindowListener) {


        if ((objectHiLighted < 0) || (ObjectArray.size() <= objectHiLighted)) return;
        Graphics2D gg = (Graphics2D) graph.getGraphics();
        gg.setXORMode(Color.gray);
        gg.setStroke(BS_1p0f_2by2_dashed);
        int Xadj;
        int Yadj;
        AbstractObject oR = (AbstractObject) ObjectArray.get(objectHiLighted);
        int pnlID = getIndexOfTheRect(graph.panels, oR.getRect());
        if (!firstTimeDrawing) {
            float[] xArr = null;
            if (oR.isFreeStyle()) {
                xArr = oR.getIndexArray();
            } else {
                xArr = convertXArrayTimeToIndex(oR.getXArr());
            }
            //float[] xArr = convertXArrayTimeToIndex(oR.getXArr());

            Xadj = (int) (MouseDownPoint.getX() - PreMouseDragPoint.getX());
            Yadj = (int) (MouseDownPoint.getY() - PreMouseDragPoint.getY());

            if ((oR.getObjType() == AbstractObject.INT_LINE_SLOPE) && isSnapToPrice() && (pnlID == graph.getIDforPanel(getBaseCP().getRect()))) {
                //todo : are we giving snap facilty for arrow line sope as well? sathajtih ? 02-02 - 2009
                double[] yArr = oR.getYArr();//actual values
                final int LINE_SLOPE_ARRSIZE = 2; //remove when genaralize
                int[] yohlcPixarr = getSnapToPriceadjustments(Xadj, oR, pnlID, xArr, LINE_SLOPE_ARRSIZE, Yadj, yArr);
                oR.drawDragImage(gg, xArr, Xadj, Yadj, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], true, yohlcPixarr);
            } else {
                /*******semi log scale netYchange adjustment for Trenline************/
                //if trendline & semilog scale only
                //do semi log adjusments
                //get the panel to chec for semi log
                WindowPanel objPnl = (WindowPanel) oR.getRect();
                if ((oR.getObjType() == AbstractObject.INT_LINE_SLOPE) && objPnl.isSemiLog() && oR.isObjectMoving()) {
                    //TODO .: in future this has to be implemented for all the objects when moving
                    //price gap
                    int yDown = (int) MouseDownPoint.getY(); //y0
                    int yPreDrag = (int) PreMouseDragPoint.getY(); //yBar
                    //get the price move
                    double pDown = getYValueForthePixel(yDown, pnlID);
                    double pPreDrag = getYValueForthePixel(yPreDrag, pnlID);
                    double deltaPrice = pDown - pPreDrag; //send this double value value
                    //add the price differnce to the end of trendline points
                    oR.drawDragImage(gg, xArr, Xadj, deltaPrice, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], false, null);
                } else {
                    oR.drawDragImage(gg, xArr, Xadj, Yadj, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], false, null);
                }
            }
        }

        Xadj = (int) (MouseDownPoint.getX() - MouseDragPoint.getX());
        //float[] xArr = convertXArrayTimeToIndex(oR.getXArr());
        float[] xArr = null;
        if (oR.isFreeStyle()) {
            xArr = oR.getIndexArray();
        } else {
            xArr = convertXArrayTimeToIndex(oR.getXArr());
        }
        Yadj = (int) (MouseDownPoint.getY() - MouseDragPoint.getY());
        double[] yArr = oR.getYArr();
        if ((oR.getObjType() == AbstractObject.INT_LINE_SLOPE) && isSnapToPrice() && (pnlID == graph.getIDforPanel(getBaseCP().getRect()))) {
            final int LINE_SLOPE_ARRSIZE = 2;
            int[] yohlcPixarr = getSnapToPriceadjustments(Xadj, oR, pnlID, xArr, LINE_SLOPE_ARRSIZE, Yadj, yArr);
            oR.drawDragImage(gg, xArr, Xadj, Yadj, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], true, yohlcPixarr);
        } else {

            WindowPanel objPnl = (WindowPanel) oR.getRect();
            //gg.setClip(objPnl.x + graph.widthYaxis_1, objPnl.y, objPnl.width, objPnl.height); //TODO: need to do in future.. CHARITH
            if ((oR.getObjType() == AbstractObject.INT_LINE_SLOPE) && objPnl.isSemiLog() && oR.isObjectMoving()) {
                //TODO .: in future this has to be implemented for all the objects when moving
                //price gap
                int yDown = (int) MouseDownPoint.getY(); //y0
                int yDrag = (int) MouseDragPoint.getY(); //yBar
                //get the price move
                double pDown = getYValueForthePixel(yDown, pnlID);
                double pDrag = getYValueForthePixel(yDrag, pnlID);
                double deltaPrice = pDown - pDrag; //send this double value value
                //add the price differnce to the end of trendline points
                oR.drawDragImage(gg, xArr, Xadj, deltaPrice, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], false, null);
            } else {
                oR.drawDragImage(gg, xArr, Xadj, Yadj, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], false, null);
                //oR.drawDragImage(gg, xArr, Xadj, Yadj, beginIndex, minY[pnlID], xFactor, yFactor[pnlID], false, null, this);
            }

        }

        if (oR.getObjType() == AbstractObject.INT_LINE_HORIZ && dragWindowListener != null) {
            dragWindowListener.dataDragPointChanged(MouseDragPoint, graph, AbstractObject.INT_LINE_HORIZ);
        } else if (oR.getObjType() == AbstractObject.INT_LINE_VERTI && dragWindowListener != null) {
            dragWindowListener.dataDragPointChanged(MouseDragPoint, graph, AbstractObject.INT_LINE_VERTI);
        }
    }

    //y values adjustments also need to set here...
    private int[] getSnapToPriceadjustments(int xadj, AbstractObject oR, int pnlID, float[] xArr, int ObjectArrSize, int yadj, double[] yArr) {
        int movingPtIndex = oR.getMovingPtIndex();
        int x1 = getPixelFortheIndex(xArr[0]);
        int x2 = getPixelFortheIndex(xArr[1]);

        int y1 = getPixelFortheYValue(yArr[0], pnlID);
        int y2 = getPixelFortheYValue(yArr[1], pnlID);


        float[] newMx = new float[ObjectArrSize]; //
        double[] newMy = new double[ObjectArrSize]; //

        //should make a loop
        if (oR.isObjectMoving()) {
            newMx[0] = getIndexForthePixel(x1 - xadj);
            newMx[1] = getIndexForthePixel(x2 - xadj);

            newMy[0] = getYValueForthePixel((y1 - yadj), pnlID);
            newMy[1] = getYValueForthePixel((y2 - yadj), pnlID);

        } else {
            if (movingPtIndex == 0) {
                newMx[0] = getIndexForthePixel(x1 - xadj);
                newMx[1] = getIndexForthePixel(x2);

                if (getSnapToPricePriority() == 0) {
                    newMy[0] = getYValueForthePixel((y1 - yadj), pnlID);
                    newMy[1] = getYValueForthePixel(y2, pnlID);
                }
            } else {
                newMx[0] = getIndexForthePixel(x1);
                newMx[1] = getIndexForthePixel(x2 - xadj);

                if (getSnapToPricePriority() == 0) {
                    newMy[0] = getYValueForthePixel(y1, pnlID);
                    newMy[1] = getYValueForthePixel((y2 - yadj), pnlID);
                }
            }
        }

        double[] newYOHLC;
        if (getSnapToPricePriority() == 0) {
            newYOHLC = convertXArrayIndexToNearestOHLC(newMx, newMy);
        } else {
            newYOHLC = convertXArrayIndexToSnapSelectedOHLC(newMx);
        }

        int[] ohlcYPixelArr = convertYValueArrayToPixelArr(newYOHLC, pnlID);

        return ohlcYPixelArr;
    }

    public void setNewBoundsForHiLightedObject(Point2D.Float MouseDownPoint, Point2D.Float MouseUpPoint, Point2D.Float firstMouseUpPoint, Point2D.Float secondMouseUpPoint, Point2D.Float thirdMouseUpPoint) {
        if ((objectHiLighted < 0) || (ObjectArray.size() <= objectHiLighted)) return;
        Point2D.Double mdPt = null, muPt = null/*, mdPt1, muPt1, mdPt2, muPt2*/;
        int x1, y1, x2, y2, x3, y3/*, y3, y4, y5, y6*/;
        float[] xArr = null;
        double[] yArr = null;
        int movingPtIndex = 0;
        int Xadj = (int) (MouseDownPoint.x - MouseUpPoint.x);
        int Yadj = (int) (MouseDownPoint.y - MouseUpPoint.y);
        AbstractObject oR = (AbstractObject) ObjectArray.get(objectHiLighted);
        int pnlID = getIndexOfTheRect(graph.panels, oR.getRect());
        switch (oR.getObjType()) {
            case AbstractObject.INT_TEXT:
            case AbstractObject.INT_RECT:
            case AbstractObject.INT_RECT_SELECTION:
            case AbstractObject.INT_ELLIPSE:
                //xArr = convertXArrayTimeToIndex(oR.getXArr());
                /*xArr = oR.getIndexArray();
                if (xArr == null) {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }*/
                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                yArr = oR.getYArr();
                movingPtIndex = oR.getMovingPtIndex();
                x1 = getPixelFortheIndex(xArr[0]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);
                x2 = getPixelFortheIndex(xArr[1]);
                y2 = getPixelFortheYValue(yArr[1], pnlID);
                if (oR.isObjectMoving()) {
                    mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                    muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                } else {
                    switch (movingPtIndex) {
                        case 1:
                            mdPt = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                            muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                            break;
                        case 2:
                            mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1), pnlID);
                            muPt = getActualPoint2D(new Point2D.Float(x2, y2 - Yadj), pnlID);
                            break;
                        case 3:
                            mdPt = getActualPoint2D(new Point2D.Float(x1, y1 - Yadj), pnlID);
                            muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2), pnlID);
                            break;
                        case 0:
                        default:
                            mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                            muPt = getActualPoint2D(new Point2D.Float(x2, y2), pnlID);
                            break;
                    }
                }
                float[] faX0 = {(float) mdPt.x, (float) muPt.x};
                double[] faY0 = {mdPt.y, muPt.y};
                oR.setXArr(convertXArrayIndexToTime(faX0));
                oR.setIndexArray(faX0);
                oR.setYArr(faY0);
                break;
            case AbstractObject.INT_REGRESSION:
                xArr = convertXArrayTimeToIndex(oR.getXArr());
                movingPtIndex = oR.getMovingPtIndex();
                int prevX1, prevX2, diff;
                if (oR.objectMoving) {
                    x1 = prevX1 = getPixelFortheIndex(xArr[0]);
                    x2 = prevX2 = getPixelFortheIndex(xArr[1]);
                    diff = Math.abs(x1 - x2);
                    x1 = x1 - Xadj;
                    x2 = x2 - Xadj;
                } else {
                    x1 = prevX1 = getPixelFortheIndex(xArr[movingPtIndex]);
                    x2 = prevX2 = getPixelFortheIndex(xArr[1 - movingPtIndex]);
                    diff = Math.abs(x1 - x2);
                    x1 = x1 - Xadj;
                }
                int xEnd = getPixelFortheIndex(getLastNotNullIndexOfGraphStore());
                if (x1 > xEnd) {
                    x1 = xEnd;
                    if (oR.objectMoving) {
                        x2 = xEnd - diff;
                    } else {
                        x2 = prevX2;
                    }
                }
                if (x2 > xEnd) {
                    if (oR.objectMoving) {
                        x1 = xEnd - diff;
                    } else {
                        x1 = prevX1;
                    }
                    x2 = xEnd;
                }
                int X1 = (int) Math.round(getIndexForthePixel(x1));
                int X2 = (int) Math.round(getIndexForthePixel(x2));
                int[] regX = new int[2];
                int[] regY = new int[2];
                ObjectRegression.calculateRegressionLine(
                        graph, (int) Math.min(X1, X2), (int) Math.max(X1, X2), regX, regY,
                        ((ObjectRegression) oR).getOHLCPriority(), pnlID, true);
                oR.xArray = convertXArrayIndexToTime(new float[]{(float) Math.min(X1, X2), (float) Math.max(X1, X2)});
                oR.yArray = new double[]{getYValueForthePixel(regY[0], pnlID), getYValueForthePixel(regY[1], pnlID)};
                break;
            case AbstractObject.INT_LINE_SLOPE:

                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }

                yArr = oR.getYArr();
                movingPtIndex = oR.getMovingPtIndex();
                x1 = getPixelFortheIndex(xArr[0]);
                x2 = getPixelFortheIndex(xArr[1]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);
                y2 = getPixelFortheYValue(yArr[1], pnlID);
                if (oR.isObjectMoving()) {
                    mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                    muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                    if (((WindowPanel) oR.getRect()).isSemiLog()) {
                        double priceDownPoint = getYValueForthePixel((int) MouseDownPoint.y, pnlID);
                        double priceupPoint = getYValueForthePixel((int) MouseUpPoint.y, pnlID);
                        double deltaPrice = priceDownPoint - priceupPoint;
                        double y1Price = yArr[0] - deltaPrice;
                        double y2Price = yArr[1] - deltaPrice;
                        mdPt.y = y1Price;
                        muPt.y = y2Price;

                    }
                } else {
                    if (movingPtIndex == 0) {
                        mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                        muPt = getActualPoint2D(new Point2D.Float(x2, y2), pnlID);
                    } else {
                        mdPt = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                        muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                    }
                }
                float[] faXSlope = {(float) mdPt.x, (float) muPt.x};
                double[] faYSlope = {mdPt.y, muPt.y};
                //snap to price
                if ((isSnapToPrice()) && (pnlID == graph.getIDforPanel(getBaseCP().getRect()))) {
                    double[] ohlcYArr;
                    if (getSnapToPricePriority() == 0) {
                        ohlcYArr = convertXArrayIndexToNearestOHLC(faXSlope, faYSlope); //auto
                    } else {
                        ohlcYArr = convertXArrayIndexToSnapSelectedOHLC(faXSlope);
                    }
                    faYSlope = ohlcYArr;
                }

                oR.setIndexArray(faXSlope);
                oR.setXArr(convertXArrayIndexToTime(faXSlope));
                oR.setYArr(faYSlope);
                break;

            case AbstractObject.INT_ARROW_LINE_SLOPE:
                //xArr = convertXArrayTimeToIndex(oR.getXArr());
                //xArr = oR.getIndexArray();
                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                yArr = oR.getYArr();
                movingPtIndex = oR.getMovingPtIndex();
                x1 = getPixelFortheIndex(xArr[0]);
                x2 = getPixelFortheIndex(xArr[1]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);
                y2 = getPixelFortheYValue(yArr[1], pnlID);
                if (oR.isObjectMoving()) {
                    mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                    muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                } else {
                    if (movingPtIndex == 0) {
                        mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                        muPt = getActualPoint2D(new Point2D.Float(x2, y2), pnlID);
                    } else {
                        mdPt = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                        muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                    }
                }


                float[] faXArrowSlope = new float[]{(float) mdPt.x, (float) muPt.x};
                double[] faYArrowSlope = new double[]{mdPt.y, muPt.y};

                // If the arrow head shud point to right side always then comment above & un comment below.
                // First point is always set to the left side. - Mevan @ 2007-07-27
//                float[] faXArrowSlope = (mdPt.x <= muPt.x) ? new float[]{(float) mdPt.x, (float) muPt.x} : new float[]{(float) muPt.x, (float) mdPt.x};
//                double[] faYArrowSlope = (mdPt.x <= muPt.x) ? new double[]{mdPt.y, muPt.y} : new double[]{muPt.y, mdPt.y};

                //snap to price
                /*** TODO : if need snap to price just enable this ***********************///can join these two as well
//                if ((isSnapToPrice()) && (pnlID == graph.getIDforPanel(getBaseCP().getRect()))) {
//                    double[] ohlcYArr;
//                    if (getSnapToPricePriority() == 0) {
//                        ohlcYArr = convertXArrayIndexToSnapSelectedOHLC(faXArrowSlope);
//                    } else {
//                        ohlcYArr = convertXArrayIndexToNearestOHLC(faXArrowSlope, faYArrowSlope);
//                    }
//
//                    faYArrowSlope = ohlcYArr;
//                }
                /*** : if need snap to price just enable this ***********************/
                oR.setIndexArray(faXArrowSlope);
                oR.setXArr(convertXArrayIndexToTime(faXArrowSlope));
                oR.setYArr(faYArrowSlope);
                break;
            case AbstractObject.INT_GANN_LINE:
            case AbstractObject.INT_GANN_FAN:
            case AbstractObject.INT_GANN_GRID:
            case AbstractObject.INT_FIBONACCI_ARCS:
            case AbstractObject.INT_FIBONACCI_FANS:
            case AbstractObject.INT_FIBONACCI_RETRACEMENTS:
                /*xArr = oR.getIndexArray();
                if (xArr == null) {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());//for safety
                }*/
                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                yArr = oR.getYArr();
                movingPtIndex = oR.getMovingPtIndex();
                x1 = getPixelFortheIndex(xArr[0]);
                x2 = getPixelFortheIndex(xArr[1]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);
                y2 = getPixelFortheYValue(yArr[1], pnlID);
                if (oR.isObjectMoving()) {
                    mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                    muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                } else {
                    if (movingPtIndex == 0) {
                        mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                        muPt = getActualPoint2D(new Point2D.Float(x2, y2), pnlID);
                    } else {
                        mdPt = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                        muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                    }
                }
                float[] faX = {(float) mdPt.x, (float) muPt.x};
                double[] faY = {mdPt.y, muPt.y};
                oR.setXArr(convertXArrayIndexToTime(faX));
                oR.setIndexArray(faX);
                oR.setYArr(faY);
                break;
            case AbstractObject.INT_CYCLE_LINE:
                /*xArr = oR.getIndexArray();
                if (xArr == null) {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }*/
                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                yArr = oR.getYArr();
                x1 = getPixelFortheIndex(xArr[0]);
                x2 = getPixelFortheIndex(xArr[1]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);

                //used for reference point when moving from 0th index. this is the x coordinate of index = -1
                int gap = Math.abs(x2 - x1);
                int beforeX = x1 - gap;
                int difference = (int) Math.abs(MouseUpPoint.x - beforeX);
                if (difference < ObjectCycleLine.MINIMUM_GAP) {
                    difference = ObjectCycleLine.MINIMUM_GAP;
                }

                //used for reference point when moving from 0th index. this is the x coordinate of index = -1
                int movingIndex = oR.getMovingPtIndex();
                int newGap = 0;
                if (movingIndex != 0) {
                    newGap = (int) (x1 - MouseUpPoint.x) / movingIndex;
                    newGap = Math.abs(newGap);
                    if (newGap < ObjectCycleLine.MINIMUM_GAP) {
                        newGap = ObjectCycleLine.MINIMUM_GAP;
                    }
                }

                if (oR.isObjectMoving()) {
                    mdPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1), pnlID);
                    muPt = getActualPoint2D(new Point2D.Float(x2 - Xadj, y1), pnlID);
                } else {
                    if (movingIndex != 0) {
                        x2 = x1 + Math.abs(newGap);
                    } else {
                        x1 = x1 - Xadj;
                        x2 = x1 + difference;
                    }

                    mdPt = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                    muPt = getActualPoint2D(new Point2D.Float(x2, y1), pnlID);
                }
                float[] faXXXX = {(float) mdPt.x, (float) muPt.x};
                double[] faYYYY = {muPt.y};
                oR.setXArr(convertXArrayIndexToTime(faXXXX));
                oR.setIndexArray(faXXXX);
                oR.setYArr(faYYYY);
                break;
            case AbstractObject.INT_ARC:
            case AbstractObject.INT_ANDREWS_PITCHFORK:
                /*xArr = oR.getIndexArray();
                if (xArr == null) {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }*/
                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                yArr = oR.getYArr();
                x1 = getPixelFortheIndex(xArr[0]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);
                x2 = getPixelFortheIndex(xArr[1]);
                y2 = getPixelFortheYValue(yArr[1], pnlID);
                x3 = getPixelFortheIndex(xArr[2]);
                y3 = getPixelFortheYValue(yArr[2], pnlID);

                Point2D.Double point1 = new Point2D.Double(0, 0);
                Point2D.Double point2 = new Point2D.Double(0, 0);
                Point2D.Double point3 = new Point2D.Double(0, 0);

                Xadj = (int) (MouseDownPoint.x - MouseUpPoint.x);
                Yadj = (int) (MouseDownPoint.y - MouseUpPoint.y);

                movingPtIndex = oR.getMovingPtIndex();
                if (oR.isObjectMoving()) {
                    point1 = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                    point2 = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                    point3 = getActualPoint2D(new Point2D.Float(x3 - Xadj, y3 - Yadj), pnlID);
                } else {
                    if (movingPtIndex == 0) {
                        point1 = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                        point2 = getActualPoint2D(new Point2D.Float(x2, y2), pnlID);
                        point3 = getActualPoint2D(new Point2D.Float(x3, y3), pnlID);
                    } else if (movingPtIndex == 1) {
                        point1 = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                        point2 = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                        point3 = getActualPoint2D(new Point2D.Float(x3, y3), pnlID);
                    } else if (movingPtIndex == 2) {
                        point1 = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                        point2 = getActualPoint2D(new Point2D.Float(x2, y2), pnlID);
                        point3 = getActualPoint2D(new Point2D.Float(x3 - Xadj, y3 - Yadj), pnlID);
                    }
                }

                //muPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                float[] faXX = {(float) point1.x, (float) point2.x, (float) point3.x};
                double[] faYY = {point1.y, point2.y, point3.y};
                oR.setXArr(convertXArrayIndexToTime(faXX));
                oR.setIndexArray(faXX);
                oR.setYArr(faYY);
                break;
            case AbstractObject.INT_FIBONACCI_EXTENSIONS:
                //xArr = convertXArrayTimeToIndex(oR.getXArr());
                //xArr = oR.getIndexArray();
                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                yArr = oR.getYArr();
                movingPtIndex = oR.getMovingPtIndex();
                x1 = getPixelFortheIndex(xArr[0]);
                x2 = getPixelFortheIndex(xArr[1]);
                x3 = getPixelFortheIndex(xArr[2]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);
                y2 = getPixelFortheYValue(yArr[1], pnlID);
                y3 = getPixelFortheYValue(yArr[2], pnlID);

                Point2D.Double point11 = new Point2D.Double(0, 0);
                Point2D.Double point22 = new Point2D.Double(0, 0);
                Point2D.Double point33 = new Point2D.Double(0, 0);

                Xadj = (int) (MouseDownPoint.x - MouseUpPoint.x);
                Yadj = (int) (MouseDownPoint.y - MouseUpPoint.y);

                if (oR.isObjectMoving()) {
                    point11 = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                    point22 = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                    point33 = getActualPoint2D(new Point2D.Float(x3 - Xadj, y3 - Yadj), pnlID);
                } else {
                    if (movingPtIndex == 0) {
                        point11 = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                        point22 = getActualPoint2D(new Point2D.Float(x2, y2), pnlID);
                        point33 = getActualPoint2D(new Point2D.Float(x3, y3), pnlID);
                    } else if (movingPtIndex == 1) {
                        point11 = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                        point22 = getActualPoint2D(new Point2D.Float(x2 - Xadj, y2 - Yadj), pnlID);
                        point33 = getActualPoint2D(new Point2D.Float(x3, y3), pnlID);
                    } else if (movingPtIndex == 2) {
                        point11 = getActualPoint2D(new Point2D.Float(x1, y1), pnlID);
                        point22 = getActualPoint2D(new Point2D.Float(x2, y2), pnlID);
                        point33 = getActualPoint2D(new Point2D.Float(x3 - Xadj, y3 - Yadj), pnlID);
                    }
                }
                faXX = new float[]{(float) point11.x, (float) point22.x, (float) point33.x};
                faYY = new double[]{point11.y, point22.y, point33.y};
                oR.setIndexArray(faXX);
                oR.setXArr(convertXArrayIndexToTime(faXX));
                oR.setYArr(faYY);
                break;
            case AbstractObject.INT_ZIG_ZAG: //TODO: needs to complete here.

                /*xArr = oR.getIndexArray();
                if (xArr == null) {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }*/
                if (oR.isFreeStyle()) {
                    xArr = oR.getIndexArray();
                } else {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                yArr = oR.getYArr();

                Xadj = (int) (MouseDownPoint.x - MouseUpPoint.x);
                Yadj = (int) (MouseDownPoint.y - MouseUpPoint.y);

                float[] faXXX = new float[xArr.length];
                double[] faYYY = new double[xArr.length];

                if (oR.isObjectMoving()) {

                    Point2D.Double[] points = new Point2D.Double[xArr.length];
                    for (int i = 0; i < xArr.length; i++) {
                        int x = getPixelFortheIndex(xArr[i]);
                        int y = getPixelFortheYValue(yArr[i], pnlID);
                        points[i] = getActualPoint2D(new Point2D.Float(x - Xadj, y - Yadj), pnlID);
                        faXXX[i] = (float) points[i].x;
                        faYYY[i] = points[i].y;
                    }

                } else {

                    Point2D.Double[] points = new Point2D.Double[xArr.length];
                    int index = oR.getMovingPtIndex();
                    for (int i = 0; i < xArr.length; i++) {
                        int x = getPixelFortheIndex(xArr[i]);
                        int y = getPixelFortheYValue(yArr[i], pnlID);
                        if (i == index) {
                            points[i] = getActualPoint2D(new Point2D.Float(x - Xadj, y - Yadj), pnlID);
                        } else {
                            points[i] = getActualPoint2D(new Point2D.Float(x, y), pnlID);
                        }

                        faXXX[i] = (float) points[i].x;
                        faYYY[i] = points[i].y;
                    }
                }
                oR.setXArr(convertXArrayIndexToTime(faXXX));
                oR.setIndexArray(faXXX);
                oR.setYArr(faYYY);

                break;
            case AbstractObject.INT_STD_ERROR_CHANNEL:
                xArr = convertXArrayTimeToIndex(oR.getXArr());
                movingPtIndex = oR.getMovingPtIndex();
                if (oR.objectMoving) {
                    x1 = prevX1 = getPixelFortheIndex(xArr[0]);
                    x2 = prevX2 = getPixelFortheIndex(xArr[1]);
                    diff = Math.abs(x1 - x2);
                    x1 = x1 - Xadj;
                    x2 = x2 - Xadj;
                } else {
                    x1 = prevX1 = getPixelFortheIndex(xArr[movingPtIndex]);
                    x2 = prevX2 = getPixelFortheIndex(xArr[1 - movingPtIndex]);
                    diff = Math.abs(x1 - x2);
                    x1 = x1 - Xadj;
                }
                xEnd = getPixelFortheIndex(getLastNotNullIndexOfGraphStore());
                if (x1 > xEnd) {
                    x1 = xEnd;
                    if (oR.objectMoving) {
                        x2 = xEnd - diff;
                    } else {
                        x2 = prevX2;
                    }
                }
                if (x2 > xEnd) {
                    if (oR.objectMoving) {
                        x1 = xEnd - diff;
                    } else {
                        x1 = prevX1;
                    }
                    x2 = xEnd;
                }
                X1 = (int) Math.round(getIndexForthePixel(x1));
                X2 = (int) Math.round(getIndexForthePixel(x2));
                int[] stdX = new int[2];
                int[] stdY = new int[2];
                double[] maxDist = new double[1];
                maxDist[0] = 1;
                ObjectStandardError.calculateStandardError(
                        graph, (int) Math.min(X1, X2), (int) Math.max(X1, X2), stdX, stdY, maxDist,
                        ((ObjectStandardError) oR).rect, ((ObjectStandardError) oR).getOHLCPriority());
                oR.xArray = convertXArrayIndexToTime(new float[]{(float) Math.min(X1, X2), (float) Math.max(X1, X2)});
                oR.yArray = new double[]{getYValueForthePixel(stdY[0], pnlID), getYValueForthePixel(stdY[1], pnlID)};
                break;
            case AbstractObject.INT_RAFF_REGRESSION:
                xArr = convertXArrayTimeToIndex(oR.getXArr());
                movingPtIndex = oR.getMovingPtIndex();
                if (oR.objectMoving) {
                    x1 = prevX1 = getPixelFortheIndex(xArr[0]);
                    x2 = prevX2 = getPixelFortheIndex(xArr[1]);
                    diff = Math.abs(x1 - x2);
                    x1 = x1 - Xadj;
                    x2 = x2 - Xadj;
                } else {
                    x1 = prevX1 = getPixelFortheIndex(xArr[movingPtIndex]);
                    x2 = prevX2 = getPixelFortheIndex(xArr[1 - movingPtIndex]);
                    diff = Math.abs(x1 - x2);
                    x1 = x1 - Xadj;
                }
                xEnd = getPixelFortheIndex(getLastNotNullIndexOfGraphStore());
                if (x1 > xEnd) {
                    x1 = xEnd;
                    if (oR.objectMoving) {
                        x2 = xEnd - diff;
                    } else {
                        x2 = prevX2;
                    }
                }
                if (x2 > xEnd) {
                    if (oR.objectMoving) {
                        x1 = xEnd - diff;
                    } else {
                        x1 = prevX1;
                    }
                    x2 = xEnd;
                }
                X1 = (int) Math.round(getIndexForthePixel(x1));
                X2 = (int) Math.round(getIndexForthePixel(x2));
                int[] raffX = new int[2];
                int[] raffY = new int[2];
                maxDist = new double[1];
                maxDist[0] = 1;
                ObjectRaffRegression.calculateRaffRegression(
                        graph, (int) Math.min(X1, X2), (int) Math.max(X1, X2), raffX, raffY, maxDist,
                        ((ObjectRaffRegression) oR).rect, ((ObjectRaffRegression) oR).getOHLCPriority());
                oR.xArray = convertXArrayIndexToTime(new float[]{(float) Math.min(X1, X2), (float) Math.max(X1, X2)});
                oR.yArray = new double[]{getYValueForthePixel(raffY[0], pnlID), getYValueForthePixel(raffY[1], pnlID)};
                break;
            case AbstractObject.INT_STD_DEV_CHANNEL:
                xArr = convertXArrayTimeToIndex(oR.getXArr());
                movingPtIndex = oR.getMovingPtIndex();
                if (oR.objectMoving) {
                    x1 = prevX1 = getPixelFortheIndex(xArr[0]);
                    x2 = prevX2 = getPixelFortheIndex(xArr[1]);
                    diff = Math.abs(x1 - x2);
                    x1 = x1 - Xadj;
                    x2 = x2 - Xadj;
                } else {
                    x1 = prevX1 = getPixelFortheIndex(xArr[movingPtIndex]);
                    x2 = prevX2 = getPixelFortheIndex(xArr[1 - movingPtIndex]);
                    diff = Math.abs(x1 - x2);
                    x1 = x1 - Xadj;
                }
                xEnd = getPixelFortheIndex(getLastNotNullIndexOfGraphStore());
                if (x1 > xEnd) {
                    x1 = xEnd;
                    if (oR.objectMoving) {
                        x2 = xEnd - diff;
                    } else {
                        x2 = prevX2;
                    }
                }
                if (x2 > xEnd) {
                    if (oR.objectMoving) {
                        x1 = xEnd - diff;
                    } else {
                        x1 = prevX1;
                    }
                    x2 = xEnd;
                }
                X1 = (int) Math.round(getIndexForthePixel(x1));
                X2 = (int) Math.round(getIndexForthePixel(x2));
                raffX = new int[2];
                raffY = new int[2];
                maxDist = new double[1];
                maxDist[0] = 1;
                ObjectStandardDeviationChannel.calculateStandardDeviation(
                        graph, (int) Math.min(X1, X2), (int) Math.max(X1, X2), raffX, raffY, maxDist,
                        ((ObjectStandardDeviationChannel) oR).rect, ((ObjectStandardDeviationChannel) oR).getOHLCPriority());
                oR.xArray = convertXArrayIndexToTime(new float[]{(float) Math.min(X1, X2), (float) Math.max(X1, X2)});
                oR.yArray = new double[]{getYValueForthePixel(raffY[0], pnlID), getYValueForthePixel(raffY[1], pnlID)};
                break;
            case AbstractObject.INT_EQUI_DIST_CHANNEL:
                xArr = convertXArrayTimeToIndex(oR.xArray);
                int dragDist;
                double distance = ((ObjectEquiDistantChannel) oR).getDistance();
                movingPtIndex = oR.getMovingPtIndex();
                if (oR.objectMoving) {
                    x1 = getPixelFortheIndex(xArr[0]);
                    x2 = getPixelFortheIndex(xArr[1]);
                    y1 = getPixelFortheYValue(oR.yArray[0], pnlID);
                    y2 = getPixelFortheYValue(oR.yArray[1], pnlID);
                    x1 = x1 - Xadj;
                    x2 = x2 - Xadj;
                    y1 = y1 - Yadj;
                    y2 = y2 - Yadj;
                    dragDist = getPixelGapFortheYValueGap(distance, pnlID);
                } else {
                    if (movingPtIndex >= 0) { // sizing the line
                        x1 = getPixelFortheIndex(xArr[movingPtIndex]);
                        x2 = getPixelFortheIndex(xArr[1 - movingPtIndex]);
                        y1 = getPixelFortheYValue(oR.yArray[movingPtIndex], pnlID);
                        y2 = getPixelFortheYValue(oR.yArray[1 - movingPtIndex], pnlID);
                        x1 = x1 - Xadj;
                        y1 = y1 - Yadj;
                        dragDist = getPixelGapFortheYValueGap(distance, pnlID);
                    } else { // changing the distance
                        x1 = getPixelFortheIndex(xArr[0]);
                        x2 = getPixelFortheIndex(xArr[1]);
                        y1 = getPixelFortheYValue(oR.yArray[0], pnlID);
                        y2 = getPixelFortheYValue(oR.yArray[1], pnlID);
                        dragDist = getPixelGapFortheYValueGap(distance, pnlID) - Yadj;
                    }
                }
                X1 = (int) Math.round(getIndexForthePixel(x1));
                X2 = (int) Math.round(getIndexForthePixel(x2));

                oR.xArray = convertXArrayIndexToTime(new float[]{(float) Math.min(X1, X2),
                        (float) Math.max(X1, X2)});
                if (movingPtIndex >= 0) {
                    oR.yArray[movingPtIndex] = getYValueForthePixel(y1, pnlID);
                    oR.yArray[1 - movingPtIndex] = getYValueForthePixel(y2, pnlID);
                } else {
                    oR.yArray = new double[]{getYValueForthePixel(y1, pnlID), getYValueForthePixel(y2, pnlID)};
                }
                ((ObjectEquiDistantChannel) oR).setDistance(getYValueGapForthePixelGap(dragDist, pnlID));
                break;
            case AbstractObject.INT_POLYGON:
                break;
            case AbstractObject.INT_SYMBOL:
                xArr = convertXArrayTimeToIndex(oR.getXArr());
                yArr = oR.getYArr();
                x1 = getPixelFortheIndex(xArr[0]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);
                muPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                float[] faX1 = {(float) muPt.x};
                double[] faY1 = {muPt.y};
                oR.setXArr(convertXArrayIndexToTime(faX1));
                oR.setYArr(faY1);
                break;
            case AbstractObject.INT_DATA_LINE_VERTI:
                break;
            case AbstractObject.INT_FIBONACCI_ZONES:
            default:
                //xArr = convertXArrayTimeToIndex(oR.getXArr());
                xArr = oR.getIndexArray(); //give prioroty for original index array-charithn
                if (xArr == null) {
                    xArr = convertXArrayTimeToIndex(oR.getXArr());
                }
                yArr = oR.getYArr();
                x1 = getPixelFortheIndex(xArr[0]);
                y1 = getPixelFortheYValue(yArr[0], pnlID);
                muPt = getActualPoint2D(new Point2D.Float(x1 - Xadj, y1 - Yadj), pnlID);
                float[] faX2 = {(float) muPt.x};
                double[] faY2 = {muPt.y};
                oR.setXArr(convertXArrayIndexToTime(faX2));
                oR.setIndexArray(faX2);
                oR.setYArr(faY2);
                break;
        }
        graph.repaint();
    }

    public void rescaleYAxis(Point2D.Float MouseDownPoint, Point2D.Float MouseUpPoint) {
        int x1, y1, y2;
        x1 = (int) MouseDownPoint.x;
        y1 = (int) MouseDownPoint.y;
        y2 = (int) MouseUpPoint.y;

        int pnlID = graph.getPanelIDatPoint(x1, y1);
        if (pnlID < 0) return;

        WindowPanel r = (WindowPanel) graph.panels.get(pnlID);
        //determining center point
        int yC = r.y + r.height / 2;
        double Pmin = minY[pnlID];
        double Pmax = maxY[pnlID];
        try {
            if (y1 > yC) {
                //rescale section between y1 and top of the panel
                //maxY remains unchanged
                int yMax = r.y;
                if (y2 <= yMax) y2 = yMax + 1;
                if (r.isSemiLog()) {
                    double P1 = getYValueForthePixel(y1, pnlID);
                    double yFNew = (y2 - yMax) / (Math.log(Pmax) - Math.log(P1));
                    double logP = Math.log(P1) - (r.y + r.height - clearance - y2) / yFNew;
                    minCustomY[pnlID] = Math.exp(logP);
                } else {
                    minCustomY[pnlID] = Pmax - (Pmax - Pmin) * (y1 - yMax) / (y2 - yMax);
                }
                maxCustomY[pnlID] = Pmax;
            } else {
                //rescale section between y1 and bottom of the panel
                //minY remains unchanged
                int yMin = r.y + r.height;
                if (y2 >= yMin) y2 = yMin - 1;
                if (r.isSemiLog()) {
                    int yMax = r.y;
                    double P1 = getYValueForthePixel(y1, pnlID);
                    double yFNew = (yMin - y2) / (Math.log(P1) - Math.log(Pmin));
                    double logP = Math.log(Pmin) + (r.y + r.height - clearance - yMax) / yFNew;
                    maxCustomY[pnlID] = Math.exp(logP);
                } else {
                    maxCustomY[pnlID] = Pmin + (Pmax - Pmin) * (yMin - y1) / (yMin - y2);
                }
                minCustomY[pnlID] = Pmin;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        graph.reDrawAll();
    }

    public void rescaleRightMargin(Point2D.Float MouseDownPoint, Point2D.Float MouseUpPoint) {
        int x1, x2, y1, y2;
        x1 = (int) MouseDownPoint.x;
        x2 = (int) MouseUpPoint.x;
        y1 = (int) MouseDownPoint.y;
        y2 = (int) MouseUpPoint.y;

        Rectangle r = new Rectangle(graph.mainRect.x, graph.getHeight() - graph.sliderHeight - graph.borderWidth - graph.heightXaxis, graph.mainRect.width, graph.heightXaxis);

        try {

            float indexDifference = endIndex - beginIndex;

            int pixelDifference = x2 - x1;
            int indexDisplacement = (int) (indexDifference * pixelDifference / r.width);

            if (x1 < x2) {

                if (graph.isCurrentMode()) {
                    setRightMargin(rMarginWidth, rMarginIntraday - indexDisplacement);
                } else {
                    setRightMargin(rMarginWidth - indexDisplacement, rMarginIntraday);
                }


            } else {
                if (graph.isCurrentMode()) {
                    setRightMargin(rMarginWidth, Math.max(rMarginIntraday - indexDisplacement, 10));
                } else {
                    setRightMargin(Math.max(rMarginWidth - indexDisplacement, 10), rMarginIntraday);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        graph.reDrawAll();
    }

    public void removeActiveObject(boolean isRectangualrSelection) {

        if (isRectangualrSelection) {     //select multiple objects and remove alltogether
            //TODO : remove test code from here-added by charithn
            ArrayList<AbstractObject> temp = (ArrayList<AbstractObject>) ObjectArray.clone();
            int selectsCount = 0;
            for (AbstractObject ao : ObjectArray) {
                if (!(ao instanceof ObjectRectangularSelection) && ao.isSelected()) {
                    selectsCount++;
                    temp.remove(ao);
                }
            }
            if (ChartOptions.getCurrentOptions().gen_confirm_del_chart && selectsCount > 0) {  //user option-confirmation
                int option = JOptionPane.showConfirmDialog(graph, Language.getString("MSG_CONFIRM_DELETE"), Language.getString("GRAPH_CONFIRMATION"), JOptionPane.YES_NO_OPTION);
                if (option == JOptionPane.NO_OPTION || option == JOptionPane.CLOSED_OPTION) { //no - do nothing here.
                    for (AbstractObject ao : ObjectArray) {
                        ao.setSelected(false);
                    }
                    setObjArray(ObjectArray);
                    graph.repaint();
                    return;
                } else {    //yes - removes all the active ojects in the graph
                    setObjArray(temp);
                    graph.repaint();
                }
            } else { // user option-not set as confirmation
                setObjArray(temp);
                graph.repaint();
            }

        } else {           //remove objects one by one
            if ((objectHiLighted > -1) && (objectHiLighted < ObjectArray.size())) {
                if (ChartOptions.getCurrentOptions().gen_confirm_del_chart) {
                    int option = JOptionPane.showConfirmDialog(graph, Language.getString("MSG_CONFIRM_DELETE"), Language.getString("GRAPH_CONFIRMATION"), JOptionPane.YES_NO_OPTION);
                    if (option == JOptionPane.NO_OPTION || option == JOptionPane.CLOSED_OPTION) { //no  or close
                        return;
                    } else {
                        ObjectArray.remove(objectHiLighted);
                        graph.repaint();
                    }
                } else {
                    ObjectArray.remove(objectHiLighted);
                    graph.repaint();
                }

            } else if ((sourceHiLighted > -1) && (sourceHiLighted < Sources.size())) {
                ChartProperties cp = (ChartProperties) Sources.get(sourceHiLighted);
                if (cp.getID() == ID_BASE) {
                    //removeDataSource(cp.getSymbol());
                } else if (cp.getID() == ID_VOLUME) {
                    //removeDataSource(cp.getSymbol());
                } else if (cp.getID() == ID_COMPARE) {
                    removeDataSource(cp.getSymbol());
                } else if (cp.getID() > ID_COMPARE) {
                    if (ChartOptions.getCurrentOptions().gen_confirm_del_indicator) {
                        int option = JOptionPane.showConfirmDialog(graph, Language.getString("MSG_CONFIRM_DELETE"), Language.getString("GRAPH_CONFIRMATION"), JOptionPane.YES_NO_OPTION);
                        if (option == JOptionPane.NO_OPTION || option == JOptionPane.CLOSED_OPTION) { //no
                            return;
                        } else { //yes
                            removeIndicator(cp);
                        }
                    } else {
                        removeIndicator(cp);
                    }
                }
            }
        }
    }

    public boolean isBusy() {
        return isBusyNow;
    }

    public void setBusyNow(boolean busyNow) {
        isBusyNow = busyNow;
    }

    public boolean isBusyPainting() {
        return isBusyPaintingNow;
    }

    public void setBusyPainting(boolean busy) {
        isBusyPaintingNow = busy;
    }

    //get/set methods for member fields
    public int getObjectHiLighted() {
        return objectHiLighted;
    }

    public void setObjectHiLighted(int objInd) {
        objectHiLighted = objInd;
    }

    public boolean isObjectReadyToMove() {
        return objectMoving;
    }

    //interval
    public long getInterval() {
        return interval;
    }

    public void setInterval(long i) {
        if (interval != i) {
            interval = i;
            tmpLatestTime = 0;
            reConstructGraphStore();
            refreshZoomAndPos();
            graph.repaint();
        }
    }

    public void setIntervalFlag(long i) {
        interval = i;
    }

    public byte getGraphStyle() {
        if ((Sources.size() > 0) && (Sources.get(0) != null) && (Sources.get(0) instanceof ChartProperties)) {
            ChartProperties cp = (ChartProperties) Sources.get(0);
            return cp.getChartStyle();
        }
        return (byte) graph.INT_GRAPH_STYLE_OHLC;
    }

    //style
    public void setGraphStyle(byte s) {
        if ((activeObject == null) ||
                !(activeObject instanceof ChartProperties)) {
            if ((Sources.size() > 0) && (Sources.get(0) != null) && (Sources.get(0) instanceof ChartProperties)) {
                ChartProperties cp = (ChartProperties) Sources.get(0);
                cp.setChartStyle(s);
            }
        } else {
            ((ChartProperties) activeObject).setChartStyle(s);
        }
        if ((Sources.size() > 0) && (Sources.get(0) != null) && (Sources.get(0) instanceof ChartProperties)) {
            ChartProperties cp = (ChartProperties) Sources.get(0);
            baseGraphStyle = cp.getChartStyle();
        } else {
            baseGraphStyle = s;
        }
    }

    public boolean isAlreadyDrawn(String sKey) {
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            if (cp.getSymbol().equals(sKey)) return true;
        }
        return false;
    }

    public String getBaseGraph() {
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            if (cp.getID() == ID_BASE) {
                return cp.getSymbol();
            }
        }
        return null;
    }

    public ChartProperties getBaseCP() {
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            if (cp.getID() == ID_BASE) {
                return cp;
            }
        }
        return null;
    }

    public int getBaseGraphIndex() {
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            if (cp.getID() == ID_BASE) {
                return i;
            }
        }
        return -1;
    }

    public String[] getCompareGraphs() {
        ArrayList<String> al = new ArrayList<String>();
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            if (cp.getID() == ID_COMPARE) {
                al.add(cp.getSymbol());
            }
        }
        if (al.size() > 0) {
            String[] sa = new String[al.size()];
            for (int i = 0; i < al.size(); i++)
                sa[i] = (String) al.get(i);
            al.clear();
            return sa;
        } else {
            return null;
        }
    }

    public ArrayList getIndicators() {
        ArrayList<ChartProperties> indicatorList = new ArrayList<ChartProperties>();
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            if (cp instanceof Indicator && !(cp instanceof Volume || cp instanceof TurnOver)) {
                indicatorList.add(cp);
            }
        }
        return indicatorList;
    }

    public boolean isSymbolAlreadyDrawn(String smbl) {
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            if ((cp.getID() == ID_BASE) || (cp.getID() == ID_COMPARE)) {
                if (cp.getSymbol().equals(smbl)) return true;
            }
        }
        return false;
    }

    //#####################################################################
    public Point2D.Double getActualPoint2D(Point2D.Float aP2D, int pnlID) {
        float fX = getIndexForthePixel(Math.round(aP2D.x));
        double fY = getYValueForthePixel(Math.round(aP2D.y), pnlID);
        return new Point2D.Double(fX, fY);
    }

    public int[] convertYValueArrayToPixelArr(double[] YValueArr, int PanelID) {
        if (YValueArr == null) return null;
        int[] pixelArr = new int[YValueArr.length];
        for (int i = 0; i < YValueArr.length; i++) {
            pixelArr[i] = getPixelFortheYValue((double) YValueArr[i], PanelID);
        }
        return pixelArr;
    }

    //get relevent ohlc value of base chart relevent to time index
    public double[] convertXArrayIndexToSnapSelectedOHLC(float[] indxArr) {
        if (indxArr == null) return null;
        double[] relOHLCArr = new double[indxArr.length];
        for (int i = 0; i < indxArr.length; i++) {
            relOHLCArr[i] = getSnapOHLCValueForIndex(Math.round(indxArr[i]));
        }
        return relOHLCArr;
    }

    public double[] convertXArrayIndexToNearestOHLC(float[] indxArr, double[] priceValueArr) {
        if (indxArr == null) return null;
        double[] relOHLCArr = new double[indxArr.length];

        for (int i = 0; i < indxArr.length; i++) {
            double priceVal = priceValueArr[i];
            relOHLCArr[i] = getNearestOHLCValueForIndex(Math.round(indxArr[i]), priceVal);
        }
        return relOHLCArr;
    }

    public double[] convertXArrayIndexTopriorityOHLC(float[] indxArr) {
        if (indxArr == null) return null;
        double[] relOHLCArr = new double[indxArr.length];
        for (int i = 0; i < indxArr.length; i++) {
            relOHLCArr[i] = getNearestPriorityOHLCValueForIndex(Math.round(indxArr[i]));
        }
        return relOHLCArr;
    }

    //if candles get nearest one from  ohlc values .ow prioritized ohlc value
    public double getSnapOHLCValueForIndex(int index) {
        try {
            if (index < 0) {
                index = 0;
            } else if (graphStore.size() <= index) {
                index = graphStore.size() - 1;
            }
            Object[] objArr = (Object[]) graphStore.get(index);
            if ((objArr != null) && (objArr.length > 0) && (objArr[0] != null)) { //should check which entry,objarr[1] !=null //sathyajith
                byte indexCorrctionFactor = 1;
                return ((ChartPoint) objArr[1]).getValue((byte) (getSnapToPricePriority() - indexCorrctionFactor)); //ohlc priority value
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public double getNearestOHLCValueForIndex(int index, double priceVal) {
        try {
            if (index < 0) {
                index = 0;
            } else if (graphStore.size() <= index) {
                index = graphStore.size() - 1;
            }
            Object[] objArr = (Object[]) graphStore.get(index);
            if ((objArr != null) && (objArr.length > 0) && (objArr[0] != null)) { //should check which entry,objarr[1] !=null //sathyajith
                /************get the closest among ohlc to the price value*****************/
                return getOHLCNearestToPriceValue((ChartPoint) objArr[1], priceVal);
                /***********get the closest among ohlc to the price value*****************/
                //  return ((ChartPoint) objArr[1]).getValue(getSnapToPricePriority()); //ohlc priority value
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public double getOHLCNearestToPriceValue(ChartPoint cp, double priceVal) {

        double value = cp.Close;
        double dOpen = Math.abs(cp.Open - priceVal);
        double dHigh = Math.abs(cp.High - priceVal);
        double dLow = Math.abs(cp.Low - priceVal);
        double dClose = Math.abs(cp.Close - priceVal);

        if (priceVal >= cp.High) {
            value = cp.High;
        } else if (priceVal <= cp.Low) {
            value = cp.Low;
        } else { //between hight & low
            double diff = dOpen;
            value = cp.Open;
            if (dHigh < diff) {
                value = cp.High;
                diff = dHigh;
            }
            if (dLow < diff) {
                value = cp.Low;
                diff = dLow;
            }
            if (dClose < diff) {
                value = cp.Close;
                diff = dClose;
            }
        }

        return value;


    }

    public double getNearestPriorityOHLCValueForIndex(int index) {
        try {
            if (index < 0) {
                index = 0;
            } else if (graphStore.size() <= index) {
                index = graphStore.size() - 1;
            }
            Object[] objArr = (Object[]) graphStore.get(index);
            if ((objArr != null) && (objArr.length > 0) && (objArr[0] != null)) { //should check which entry,objarr[1] !=null //sathyajith

                return ((ChartPoint) objArr[1]).getValue(getBaseCP().getOHLCPriority()); //ohlc priority value
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public double getOHLCValueForIndex(int index, byte OHLCValue) {
        try {
            if (index < 0) {
                index = 0;
            } else if (graphStore.size() <= index) {
                index = graphStore.size() - 1;
            }
            Object[] objArr = (Object[]) graphStore.get(index);
            if ((objArr != null) && (objArr.length > 0) && (objArr[0] != null)) { //should check which entry,objarr[1] !=null //sathyajith

                return ((ChartPoint) objArr[1]).getValue(OHLCValue); //ohlc priority value
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public void addToObjectArray(AbstractObject oR) {
        if (ObjectArray == null) ObjectArray = new ArrayList<AbstractObject>(5);
        for (int i = 0; i < ObjectArray.size(); i++) {
            AbstractObject oRec = (AbstractObject) ObjectArray.get(i);
            oRec.setSelected(false);
        }
        ObjectArray.add(oR);
        if ((sourceHiLighted > -1) && (sourceHiLighted < Sources.size())) {
            ChartProperties cp = (ChartProperties) Sources.get(sourceHiLighted);
            cp.setSelected(false);
        }
        sourceHiLighted = -1;
        oR.setSelected(true);
        objectHiLighted = ObjectArray.size() - 1;
        graph.repaint();
    }

    public void addToStaticObjectArray(AbstractObject oR) {
        if (StaticObjectArray == null) StaticObjectArray = new ArrayList<AbstractObject>(5);
        StaticObjectArray.add(oR);
        oR.setSelected(false);
        graph.repaint();
    }

    public void removeFromObjectArray(AbstractObject obj) {
        if (ObjectArray == null) return;
        ObjectArray.remove(obj);
        graph.repaint();
    }

    public void removeFromStaticObjectArray(AbstractObject obj) {
        if (StaticObjectArray == null) return;
        StaticObjectArray.remove(obj);
        graph.repaint();
    }

    //beginIndex
    public float getBeginIndex() {
        return beginIndex;
    }

    public void setBeginIndex(float ind) {
        beginIndex = ind;
    }

    //endIndex
    public float getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(float ind) {
        endIndex = ind;
    }

    //indexed
    public boolean isIndexed() {
        return indexed;
    }

    public void setIndexed(boolean ind) {
        indexed = ind;
        reCalculateGraphParams();
        if (graph != null)
            graph.reDrawAll();
    }

    public void setshowsideBar(boolean ind) {
        indexed = ind;
        reCalculateGraphParams();
        if (graph != null)
            graph.reDrawAll();
    }

    //splitAdjust
    public boolean isSplitAdjusted() {
        return splitAdjust;
    }

    public void setSplitAdjusted(boolean adjust) {
        //splitAdjust = false;//adjust;
        splitAdjust = adjust;
        reConstructGraphStore();
        if (graph != null)
            graph.reDrawAll();
    }

    //showSplits
    public boolean isShowingSplits() {
        return showSplits;
    }

    public void setAdjustMethod(byte method) {
        adjustMethod = method;
        reConstructGraphStore();
        if (graph != null)
            graph.reDrawAll();
    }

    public byte getSplitAdjustMethod() {
        return adjustMethod;
    }

    public void setShowSplits(boolean show) {
        showSplits = show;
        if (graph != null)
            graph.repaint();
    }

    //showAnnouncements
    public boolean isShowingAnnouncements() {
        return showAnnouncements;
    }

    public void setShowAnnouncements(boolean show) {
        showAnnouncements = show;
        if (graph != null)
            graph.repaint();
    }

    //int MinimumUnit = 60;
    public int getMinimumUnit() {
        return MinimumUnit;
    }

    public void setMinimumUnit(int mu) {
        MinimumUnit = mu;
    }

    public void setRightMargin() {
        CustomDialog cdlg = CustomDialogs.getRightMarginDialog(rMarginWidth, rMarginIntraday);
        cdlg.setLocationRelativeTo(graph);
        cdlg.setModal(true);
        cdlg.setVisible(true);
        if (cdlg.getModalResult() && (cdlg.result != null)) {
            int[] ia = (int[]) cdlg.result;
            removeDummyPoints(latestTime);
            rMarginWidth = ia[0];
            rMarginIntraday = ia[1];
            insertDummyPoints(latestTime);
            if (graph.zSlider.getBeginPos() + graph.zSlider.getZoom() >= 99.5f) {
                setEndIndex(graphStore.size() - 1);
            }
            graph.reDrawAll();
        }
    }

    //added by charithn
    public void setRightMargin(int history, int intraday) {

        removeDummyPoints(latestTime);
        rMarginWidth = history;
        rMarginIntraday = intraday;
        insertDummyPoints(latestTime);
        if (graph.zSlider.getBeginPos() + graph.zSlider.getZoom() >= 99.5f) {
            setEndIndex(graphStore.size() - 1);
        }
        graph.reDrawAll();

    }

    public int getRightMarginHistory() {
        return rMarginWidth;
    }

    public int getRightMarginIntraday() {
        return rMarginIntraday;
    }

    public void setRightMarginDefault() {
        removeDummyPoints(latestTime);
        int histoyMargin = ChartOptions.getCurrentOptions().chart_right_margin_history;
        int intraDayMargin = ChartOptions.getCurrentOptions().chart_right_margin_intraday;
        this.setRightMargin(histoyMargin, intraDayMargin);
        rMarginWidth = histoyMargin;
        rMarginIntraday = intraDayMargin;
        insertDummyPoints(latestTime);
        if (graph.zSlider.getBeginPos() + graph.zSlider.getZoom() >= 99.5f) {
            setEndIndex(graphStore.size() - 1);
        }

    }

    public int getIndexOfTheRect(ArrayList al, Rectangle r) {
        if ((al != null) && (r != null)) {
            for (int i = 0; i < al.size(); i++) {
                Rectangle rr = (Rectangle) al.get(i);
                if (rr.equals(r)) return i;
            }
        }
        return -1;
    }

    public ArrayList getSources() {
        return Sources;
    }

    public void setSources(ArrayList<ChartProperties> al) {
        Sources = al;
    }

    public ArrayList getObjArray() {
        return ObjectArray;
    }

    public void setObjArray(ArrayList<AbstractObject> al) {
        ObjectArray = al;
    }

    public ArrayList getStaticObjArray() {
        return StaticObjectArray;
    }

    public void setStaticObjArray(ArrayList<AbstractObject> al) {
        StaticObjectArray = al;
    }

    public long getLatestTimeMillisec() {
        if (Sources.size() > 0) {
            long result = 0;
            long time;
            ChartProperties cp;
            for (int i = 0; i < Sources.size(); i++) {
                cp = (ChartProperties) Sources.get(i);
                ArrayList al = cp.getDataSource();
                if ((cp.getID() <= ID_COMPARE) && (al.size() > 0)) {
                    IntraDayOHLC dR = (IntraDayOHLC) al.get(al.size() - 1);

                    if ((dR.getTime() == 0) && (al.size() > 1))
                        dR = (IntraDayOHLC) al.get(al.size() - 2);

                    time = dR.getTime() * 60000L;
                    time += ChartInterface.getTimeZoneAdjustment(graph.Symbol, time, graph.isCurrentMode());
                    result = Math.max(time, result);
                }
            }
            if (result > 0) return result;
        }
        return System.currentTimeMillis();
    }

    public long getEarliestTimeMillisec() {
        if (Sources.size() > 1) {
            long result = Long.MAX_VALUE;
            ChartProperties cp;
            for (int i = 0; i < Sources.size(); i++) {
                cp = (ChartProperties) Sources.get(i);
                if ((cp.getID() <= ID_COMPARE) && (cp.getDataSource().size() > 0)) {
                    IntraDayOHLC dR = (IntraDayOHLC) cp.getDataSource().get(0);
                    result = Math.min(dR.getTime() * 60000L, result);
                }
            }
            if (result < Long.MAX_VALUE) return result;
        }
        return 0;
    }

    public long getPeriodBeginMillisec(int period) {

        //TWDateFormat f = new TWDateFormat("yyyy MM dd - HH:mm");


        long end = getLatestTimeMillisec();
        //System.out.println("++++++++ end ++++++++++ "+f.format(end));
        long adjDays = getDaysAdjusted(period);
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(end);
        switch (period) {
            case StockGraph.ONE_DAY:
            case StockGraph.SIX_DAYS:
                if (adjDays <= 0) return getEarliestTimeMillisec();
                //System.out.println("++++++++ result ++++++++++ "+f.format(end - (adjDays * Miliseconds_Per_Day)));
                return end - (adjDays * Miliseconds_Per_Day);
            case StockGraph.ONE_WEEK:
                return end - 7L * Miliseconds_Per_Day;
            case StockGraph.ONE_MONTH:
                calendar.add(Calendar.MONTH, -1);
                return calendar.getTimeInMillis();
            case StockGraph.THREE_MONTHS:
                calendar.add(Calendar.MONTH, -3);
                return calendar.getTimeInMillis();
            case StockGraph.SIX_MONTHS:
                calendar.add(Calendar.MONTH, -6);
                return calendar.getTimeInMillis();
            case StockGraph.YTD:
                return end - Miliseconds_Per_Day * (long) calendar.get(Calendar.DAY_OF_YEAR);
            case StockGraph.ONE_YEAR:
                calendar.add(Calendar.YEAR, -1);
                return calendar.getTimeInMillis();
            case StockGraph.EIGHTEEN_MONTHS:
                calendar.add(Calendar.MONTH, -18);
                return calendar.getTimeInMillis();
            case StockGraph.TWO_YEARS:
                calendar.add(Calendar.YEAR, -2);
                return calendar.getTimeInMillis();
            case StockGraph.THREE_YEARS:
                calendar.add(Calendar.YEAR, -3);
                return calendar.getTimeInMillis();
            case StockGraph.FIVE_YEARS:
                calendar.add(Calendar.YEAR, -5);
                return calendar.getTimeInMillis();
            case StockGraph.TEN_YEARS:
                calendar.add(Calendar.YEAR, -10);
                return calendar.getTimeInMillis();
            case StockGraph.CUSTOM_PERIODS:
                //calendar.add(currentCustomPeriod, -currentCustomRange);
                //return calendar.getTimeInMillis();
                //return getCustomBeginPeriod(calendar, end, currentCustomPeriod, currentCustomRange);
                return getCustomBeginPeriod(currentCustomPeriod, currentCustomRange);
            default:
                return getEarliestTimeMillisec();
        }
    }

    private long getCustomBeginPeriod(Calendar calendar, long end, int customPeriod, int customRange) {

        int period = StockGraph.SIX_DAYS;
        if (customRange == 1) period = StockGraph.ONE_DAY;
        if (customPeriod == Calendar.DAY_OF_YEAR && (customRange == 6 || customRange == 1)) {

            long adjDays = getDaysAdjusted(period);
            if (adjDays <= 0) return getEarliestTimeMillisec();
            return end - (adjDays * Miliseconds_Per_Day);
        } else {
            calendar.add(customPeriod, -customRange);
            return calendar.getTimeInMillis();
        }
    }

    public void setCustomBeginPeriods(int period, int range) {
        currentCustomPeriod = period;
        currentCustomRange = range;
    }

    private int getDaysAdjusted(int period) {
        int reqdCnt = 0, cnt = 0;
        GregorianCalendar calendar = new GregorianCalendar();
        switch (period) {
            case StockGraph.SIX_DAYS:
                reqdCnt = 5; // 1 will be added again in next line
            case StockGraph.ONE_DAY:
                reqdCnt++;
                if (graphStore.size() < 2) return 1;
                double lTime = getMidnight(getTimeMillisec(graphStore.size() - 1), true) - 1;
                calendar.setTimeInMillis(getTimeMillisec(graphStore.size() - 1));
                int dayOfM = calendar.get(Calendar.DAY_OF_MONTH);
                int minitue = calendar.get(Calendar.MINUTE);
                int Hour = calendar.get(Calendar.HOUR);
                int doM;
                for (int i = graphStore.size() - 2; i >= 0; i--) {
                    calendar.setTimeInMillis(getTimeMillisec(i));
                    doM = calendar.get(Calendar.DAY_OF_MONTH);
                    int Minitue = calendar.get(Calendar.MINUTE);
                    int hour = calendar.get(Calendar.HOUR);
                    if (doM != dayOfM) {
                        cnt++;
                        dayOfM = doM;
                    }
                    if (cnt >= reqdCnt) {
                        double bTime = getMidnight(getTimeMillisec(i), true) - 1;
                        return (int) Math.round((lTime - bTime) / (double) Miliseconds_Per_Day);
                    }
                }
                return -1;
            default:
                return -1;
        }
    }

    private long getAdjustedBegintime(int customPeriod, int customRange) {
        int cnt = 0;
        int reqdCnt = customRange;
        GregorianCalendar calendar = new GregorianCalendar();
        int lastNotNullIndex = getLastNotNullIndexOfGraphStore();
        calendar.setTimeInMillis(getTimeMillisec(lastNotNullIndex));
        //System.out.println(calendar.getTime());
        int dayOfM = calendar.get(customPeriod);
        int doM;
        if ((customPeriod == Calendar.DAY_OF_YEAR && !graph.isCurrentMode()) || customPeriod == Calendar.MINUTE) {   //todo important
            reqdCnt--;
        }
        for (int i = lastNotNullIndex - 1; i >= 0; i--) {
            calendar.setTimeInMillis(getTimeMillisec(i));
            doM = calendar.get(customPeriod);
            if (doM != dayOfM) {
                //System.out.println("------------------"+calendar.getTime());
                cnt++;
                dayOfM = doM;
            }
            if (cnt >= reqdCnt) {
                return getTimeMillisec(i);
            }
        }
        return -1;
    }

    private long getCustomBeginPeriod(int customPeriod, int customRange) {
        int reqdCnt = 0, cnt = 0;
        GregorianCalendar calendar = new GregorianCalendar();
        int lastNotNullIndex = getLastNotNullIndexOfGraphStore();
        calendar.setTimeInMillis(getTimeMillisec(lastNotNullIndex));
        reqdCnt = customRange;
        if (graphStore.size() < 2) return 1;
        if (!graph.isCurrentMode()) {//history mode
            if (customPeriod == Calendar.DAY_OF_YEAR) {
                return getAdjustedBegintime(customPeriod, customRange);
            } else {//when custom period is year of month
                calendar.add(customPeriod, -customRange);
                return calendar.getTimeInMillis();
            }
        } else {  //intraday mode
            if (customPeriod == Calendar.HOUR) {
                calendar.add(customPeriod, -customRange);
                return calendar.getTimeInMillis();
            } else if (customPeriod == Calendar.DAY_OF_YEAR) {
                int minitue = calendar.get(Calendar.MINUTE);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                //int hour = calendar.get(Calendar.HOUR);
                Calendar calTemp = new GregorianCalendar();
                calTemp.setTimeInMillis(getAdjustedBegintime(customPeriod, customRange));  //special case need to add 1 here
                calTemp.set(Calendar.HOUR_OF_DAY, hour);
                calTemp.set(Calendar.MINUTE, minitue);
                return calTemp.getTimeInMillis();

            } else if (customPeriod == Calendar.MINUTE) {
                return getAdjustedBegintime(customPeriod, customRange);
            }
        }
        return -1;
    }

    public void setPeriodBeginIndex(int period, boolean reset) {
        if (period == graph.ALL_HISTORY) {
            periodBeginIndex = 0;
        } else {
            /**cashflow *************************/
            if (graph.isCashFlowMode()) {
                if (CashFlowHistorySummaryUI.getSharedInstance().getSelectedDateRange() != -1) {
                    int periodRange = CashFlowHistorySummaryUI.getSharedInstance().getSelectedDateRange();
                    int cfBeginIndex = getLastNotNullIndexOfGraphStore() - periodRange;
                    periodBeginIndex = Math.max(cfBeginIndex, 0);
                    /**cashflow *************************/
                } else {

                }

            } else {
                long pb = getPeriodBeginMillisec(period);
                periodBeginIndex = Math.max(getClosestIndexFortheTime(pb), 0);
            }

        }
        if (reset) {
            resetBeginAndEndIndecies();
        } else {
            if (beginIndex < periodBeginIndex)
                beginIndex = periodBeginIndex;
            if (endIndex < periodBeginIndex)
                endIndex = Math.max(graphStore.size() - 1, periodBeginIndex);
            resetSlider();
        }
    }

    public void setBeginAndEndIndecies(long bTime, long eTime) {
        beginIndex = Math.max(periodBeginIndex, getClosestIndexFortheTime(bTime));
        endIndex = Math.max(periodBeginIndex, Math.min(getClosestIndexFortheTime(eTime), graphStore.size() - 1));
        resetSlider();
        reCalculateGraphParams();
    }

    public void resetBeginAndEndIndecies() {
        beginIndex = periodBeginIndex;
        endIndex = Math.max(graphStore.size() - 1, periodBeginIndex);
        resetSlider();
    }

    public int getClosestIndexFortheTime(long t) {
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        longArr[0] = new Long(t);
        searchIndex = graphStore.indexOfContainingValue(longArr);
        longArr[0] = null;
        if (searchIndex >= 0) {
            return searchIndex;
        } else {
            return Math.min(-searchIndex - 1, graphStore.size() - 1);
        }
    }

    public long getBeginTime() {
        return getTimeMillisec(Math.round(beginIndex));
    }

    public long getEndTime() {
        return getTimeMillisec(Math.round(endIndex));
    }

    public void drawXGrid(Graphics g, boolean isPrinting, int x,
                          int y, int XImgWd, int XImgHt) {
        if (verticalGridType == GRID_TYPE_NONE) return;
        g.setClip(x, y, XImgWd, XImgHt);
        Graphics2D gX = (Graphics2D) g;
        Date dd = new Date();
        String label;

        java.awt.Font aFont;
        if (isPrinting) {
            aFont = graph.font_PLAIN_8;
            gX.setColor(Color.lightGray);//sathyajith : suitable color for printing
        } else {
            aFont = graph.font_PLAIN_9;
            gX.setColor(graph.gridColor);
        }
        gX.setFont(aFont);

        if (graphStoreSize < 1) {
            g.setClip(null);
            return;
        }

        int lastLabelLength = 0;
        ((Graphics2D) g).setStroke(graph.BS_0p8f_solid);
        int bestMinutePitch = getBestTimeInterval(endIndex - beginIndex, XImgWd - 2 * clearance);

        if (bestMinutePitch > 0) {
            long nextTime = calculateBeginTime(getTimeMillisec(Math.round(beginIndex)), bestMinutePitch);
            long currTime;
            int currPos = Math.round(beginIndex);
            int prevPixVal = Math.round((currPos - beginIndex) * xFactor) + x + clearance - 1000;
            int currPixVal;
            boolean draw = true;
            boolean lastIsANewDay = false;

            while (currPos < graphStoreSize) {  // - clearance
                currTime = getTimeMillisec(currPos);

                if (currTime >= nextTime) {
                    boolean newDay = isANewDay(currTime, nextTime);
                    nextTime = getNextPlottingTime(currTime, bestMinutePitch);
                    currPixVal = Math.round((currPos - beginIndex) * xFactor) + x + clearance;
                    if ((currPixVal >= prevPixVal + lastLabelLength) || (lastIsANewDay)) {
                        dd.setTime(currTime);
                        if (graph.isCurrentMode()) {
                            label = timeFormatter.format(dd);
                        } else {
                            label = getSuitableDateFormatter(bestMinutePitch).format(dd);
                        }
                        lastLabelLength = gX.getFontMetrics(aFont).stringWidth(label) + 5;
                        switch (verticalGridType) {
                            case GRID_TYPE_SUBDIVIDED:
                                if ((!lastIsANewDay) || (hasEnoughRoom(currPixVal - prevPixVal, lastLabelLength))) {
                                    int midPixVal = Math.min((currPixVal + prevPixVal) / 2, currPixVal - lastLabelLength / 2);
                                    gX.drawLine(midPixVal, 0 + y, midPixVal, XImgHt + y);
                                }
                            case GRID_TYPE_NORMAL:
                                gX.drawLine(currPixVal, 0 + y, currPixVal, XImgHt + y);
                                break;
                            case GRID_TYPE_SPACED:
                                if (draw)
                                    gX.drawLine(currPixVal, 0 + y, currPixVal, XImgHt + y);
                                draw ^= true;
//                                draw = !draw;
                                break;
                        }
                        prevPixVal = currPixVal;
                    }
                    lastIsANewDay = newDay;
                }
                currPos++;
            }
        }
        g.setClip(null);
    }

    private boolean hasEnoughRoom(int room, int testLen) {
        return room > testLen / 2;
    }

    private boolean isANewDay(long currTime, long prevTime) {
        if (graph.isCurrentMode() && (getMidnight(currTime, true) > getMidnight(prevTime, true))) {
            return true;
        }
        return false;
    }

    public void drawXGrid(Graphics g, boolean isPrinting) {
        int topGrf_Top = borderWidth + legendHeight + titleHeight;
        int botGrf_Bot = graph.getBottomPanelBottom();
        drawXGrid(g, isPrinting, graph.mainRect.x, topGrf_Top, graph.mainRect.width, botGrf_Bot - topGrf_Top);
    }

    public void drawYGrid(Graphics g, boolean isPrinting) {
        if (horizontalGridType == GRID_TYPE_NONE) return;
        WindowPanel r;
        int top, height;//, left = borderWidth;
        for (int i = 0; i < graph.panels.size(); i++) {
            r = (WindowPanel) graph.panels.get(i);
            top = r.y + titleHeight;
            height = r.height - titleHeight;
            Graphics2D g2D = (Graphics2D) g;
            g2D.setClip(r.x, top, r.width, height);

            double[] semiLogPitch = new double[1];
            double bestPitch = getBestYInterval(i, height - 2 * clearance, semiLogPitch);
            if (bestPitch > 0) {
                double allowedPitch = ((r.isSemiLog()) || (yFactor[i] == 0)) ? bestPitch : getAllowedPitch(bestPitch, height - 2 * clearance, yFactor[i]);
                int count = 0;
                double beginVal = calculateBeginValue(minY[i], allowedPitch);
                double val = beginVal;
                int currPos = getPixelFortheYValue(val, i);//(int)Math.round(height+top-clearance -(val-minY[i])*yFactor[i]);
                int lastPos = Integer.MAX_VALUE;
                if (isPrinting) {
                    g2D.setColor(Color.lightGray);
                } else {
                    g2D.setColor(graph.gridColor);
                }
                g2D.setStroke(graph.BS_0p8f_solid);
                while (currPos > top) {
                    if ((currPos <= height + top) && !(r.isSemiLog() && (val == 0))) {
                        if (r.isSemiLog() && (lastPos - 2 < currPos)) {
                            break;
                        }
                        g2D.drawLine(r.x, currPos, r.x + r.width, currPos);
                        lastPos = currPos;
                    }
                    if (count > 10000) {
                        break;
                    }
                    count++;
                    val = beginVal + count * allowedPitch;
                    currPos = getPixelFortheYValue(val, i);
                }
            }
            g2D.setClip(null);
        }
    }

    private double getMinimumGap(float height) {
        double result = HALF_INCH / 2;
        if (height <= 2 * HALF_INCH) {
            result = HALF_INCH / 4;
        } else if (height <= 4 * HALF_INCH) {
            result = HALF_INCH / 3;
        }
        return result;
    }

    public byte getGridType() {
        return gridType;
    }

    public void setGridType(byte gt) {
        gridType = gt;
        switch (gridType) {
            case GRID_TYPE_NONE:
                verticalGridType = GRID_TYPE_NONE;
                horizontalGridType = GRID_TYPE_NONE;
                break;
            case GRID_TYPE_HORIZONTAL:
                verticalGridType = GRID_TYPE_NONE;
                horizontalGridType = GRID_TYPE_NORMAL;
                break;
            case GRID_TYPE_BOTH:
                verticalGridType = GRID_TYPE_NORMAL;
                horizontalGridType = GRID_TYPE_NORMAL;
                break;
            case GRID_TYPE_VERTICAL:
                verticalGridType = GRID_TYPE_NORMAL;
                horizontalGridType = GRID_TYPE_NONE;
                break;
        }
        graph.repaint();
    }

    public void toggleGridType() {
        setGridType((byte) ((gridType + 1) % 4));
    }

    public float[] convertXArrayTimeToIndex(long[] timeArr) {
        int searchIndex = 0;
        Object[] longArr = new Object[1];

        if (timeArr == null) return null;
        float[] indArr = new float[timeArr.length];
        for (int i = 0; i < timeArr.length; i++) {
            longArr[0] = new Long(timeArr[i]);
            searchIndex = graphStore.indexOfContainingValue(longArr);
            if (searchIndex < 0) {
                searchIndex = -searchIndex - 1;
            }
            indArr[i] = Math.min(searchIndex, graphStore.size() - 1);
        }
        return indArr;
    }

    public long[] convertXArrayIndexToTime(float[] indxArr) {
        if (indxArr == null) return null;
        long[] timeArr = new long[indxArr.length];
        for (int i = 0; i < indxArr.length; i++) {
            timeArr[i] = getTimeMillisec(Math.round(indxArr[i]));
        }
        return timeArr;
    }

    public void applyTimeZoneAdjustment(TimeZone prevZone) {
        reConstructGraphStore();
        setPeriodBeginIndex(graph.getPeriod(), true);
        AbstractObject oR;
        long[] la;
        if ((ObjectArray != null) && (ObjectArray.size() > 0)) {
            for (int i = 0; i < ObjectArray.size(); i++) {
                oR = (AbstractObject) ObjectArray.get(i);
                la = oR.getXArr();
                if ((la != null) && (la.length > 0)) {
                    for (int j = 0; j < la.length; j++) {
                        la[j] = la[j] + ChartInterface.getTimeZoneAdjustment(graph.Symbol, la[j], graph.isCurrentMode()) -
                                ChartInterface.getPreviousTimeZoneAdjustment(la[j]);
                    }
                }
            }
        }
        reCalculateGraphParams();
        graph.repaint();
    }

    public ChartProperties getVolumeCP() {
        for (int k = 0; k < Sources.size(); k++) {
            ChartProperties cp = (ChartProperties) Sources.get(k);
            if (cp.getID() == GraphDataManager.ID_VOLUME) {
                return cp;
            }
        }
        return null;
    }

    public ChartProperties getTurnOverCP() {
        for (int k = 0; k < Sources.size(); k++) {
            ChartProperties cp = (ChartProperties) Sources.get(k);
            if (cp.getID() == GraphDataManager.ID_TURNOVER) {
                return cp;
            }
        }
        return null;
    }

    public int getIndexForSourceCP(ChartProperties source) {
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            if (cp.equals(source)) return i;
        }
        return 0;
    }

    public void setSemiLogEnabledStatus() {
        for (int j = 0; j < Sources.size(); j++) {
            ChartProperties cp = (ChartProperties) Sources.get(j);
            WindowPanel wp = cp.getRect();
            if (!wp.isSemiLogEnabled()) continue;
            for (int i = 0; i < graphStore.size(); i++) {
                ChartPoint point = readChartPointAtIndex(i, j);
                if (point != null) {
                    boolean isPositive = point.getValue(GraphDataManager.INDICATOR) > 0;
                    if (!isPositive) {
                        wp.setSemiLogEnabled(false);
                        break;
                    }
                }
            }
        }
    }

    public void toggleShowCurrentPriceLine() {
        if (showCurrentPriceLine) {
            showCurrentPriceLine = false;
        } else {
            showCurrentPriceLine = true;
        }
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && lastPriceLine != null) {
            double[] yArr = lastPriceLine.getYArr();
            if (showCurrentPriceLine) {
                yArr[0] = ((IntraDayOHLC) baseCp.getDataSource().get(baseCp.getDataSource().size() - 1)).getClose();
            } else {
                yArr[0] = HORIZ_LINE_INITIAL_VALUE;
            }
        }
        graph.repaint();
    }

    public void toggleShowPreviousCloseLine() {
        if (showPreviousCloseLine) {
            showPreviousCloseLine = false;
        } else {
            showPreviousCloseLine = true;
        }
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && previousCloseLine != null) {
            double[] yArr = previousCloseLine.getYArr();
            if (showPreviousCloseLine) {
                yArr[0] = ChartInterface.getPreviousCloseValue(baseCp);
            } else {
                yArr[0] = HORIZ_LINE_INITIAL_VALUE;
            }
        }
        graph.repaint();
    }

    public void setShowPreviousCloseLine(boolean showPreviousCloseLine) {
        this.showPreviousCloseLine = showPreviousCloseLine;
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && previousCloseLine != null) {
            double[] yArr = previousCloseLine.getYArr();
            if (showPreviousCloseLine) {
                yArr[0] = ChartInterface.getPreviousCloseValue(baseCp);
            } else {
                yArr[0] = HORIZ_LINE_INITIAL_VALUE;
            }
        }
        graph.repaint();
    }

    public void toggleShowBidAskTags() {
        if (showBidAskTags) {
            showBidAskTags = false;
        } else {
            showBidAskTags = true;
        }
        if (bidAskTags != null && bidAskTags.length > 0) {
            removeFromStaticObjectArray(bidAskTags[0]);
            removeFromStaticObjectArray(bidAskTags[1]);
        }
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && showBidAskTags) {
            bidAskTags = ChartInterface.getBidTagAndAskTag(baseCp);
            addToStaticObjectArray(bidAskTags[0]);
            addToStaticObjectArray(bidAskTags[1]);
        }
        graph.repaint();
    }

    public void toggleShowOrderLines() {
        if (showOrderLines) {
            showOrderLines = false;
        } else {
            showOrderLines = true;
        }
        refreshOrderLines();
    }

    public void toggleShowMinMaxPriceLines() {
        if (showMinMaxPriceLines) {
            showMinMaxPriceLines = false;
        } else {
            showMinMaxPriceLines = true;
        }
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && minPriceLine != null && maxPriceLine != null) {
            double[] yMin = minPriceLine.getYArr();
            double[] yMax = maxPriceLine.getYArr();

            //added to fix compatibility issues
            String key = baseCp.getSymbol();
            if (baseCp.getSymbol().split("~").length == 2) {
                key += "~0";
            }
            if (showMinMaxPriceLines) {
                yMin[0] = ChartInterface.getStockObject(key).getMinPrice();
                yMax[0] = ChartInterface.getStockObject(key).getMaxPrice();
            } else {
                yMin[0] = HORIZ_LINE_INITIAL_VALUE;
                yMax[0] = HORIZ_LINE_INITIAL_VALUE;
            }
        }
        graph.reDrawAll();
        graph.repaint();
    }

    public void toggleSnapToPrice() {
        if (snapToPrice) {
            snapToPrice = false;
        } else {
            snapToPrice = true;
        }

    }

    public void toggleshowResistanceLines() {
        if (showResistanceLine) {
            showResistanceLine = false;
        } else {
            showResistanceLine = true;
        }
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && resistanceLines != null) {

            double[][] resDYArr = new double[NO_OF_RESITANCE_LINES][0];
            for (int i = 0; i < NO_OF_RESITANCE_LINES; i++) {
                if (resistanceLines[i] != null) {
                    resDYArr[i] = resistanceLines[i].getYArr();
                    if (showResistanceLine) {
                        IntraDayOHLC intOHlc = ((IntraDayOHLC) baseCp.getDataSource().get(baseCp.getDataSource().size() - 1));
                        double pivotPoint = (intOHlc.getClose() + intOHlc.getHigh() + intOHlc.getLow()) / 3;
                        switch (i) {

                            case 0:  //pivot =  PP = (High + Low + Close)/3
                                resDYArr[i][0] = (intOHlc.getClose() + intOHlc.getHigh() + intOHlc.getLow()) / 3;
                                break;
                            case 1:  //R1 = (2 x PP) � Low
                                resDYArr[i][0] = (2 * pivotPoint) - intOHlc.getLow();
                                break;
                            case 2:  //S1 = (2 x PP) � High
                                resDYArr[i][0] = (2 * pivotPoint) - intOHlc.getHigh();
                                break;
                            case 3:  //R2 = PP + (High � Low)
                                resDYArr[i][0] = pivotPoint + (intOHlc.getHigh() - intOHlc.getLow());
                                break;
                            case 4: //S2 = PP � (High � Low)
                                resDYArr[i][0] = pivotPoint - (intOHlc.getHigh() - intOHlc.getLow());
                                break;
                            case 5://R3 = High + 2 x (PP � Low)
                                resDYArr[i][0] = intOHlc.getHigh() + 2 * (pivotPoint - intOHlc.getLow());
                                break;
                            case 6://S3 = Low � 2 x (High � PP)
                                resDYArr[i][0] = intOHlc.getLow() - (2 * (intOHlc.getHigh() - pivotPoint));
                                break;
                            default:
                                resDYArr[i][0] = HORIZ_LINE_INITIAL_VALUE;
                        }
                    } else {
                        resDYArr[i][0] = HORIZ_LINE_INITIAL_VALUE;
                    }
                }
            }

        }
        graph.repaint();

    }

    public boolean isShowCurrentPriceLine() {
        return showCurrentPriceLine;
    }

    public void setShowCurrentPriceLine(boolean showCurrentPriceLine) {
        this.showCurrentPriceLine = showCurrentPriceLine;
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && lastPriceLine != null) {
            double[] yArr = lastPriceLine.getYArr();
            if (showCurrentPriceLine) {
                yArr[0] = ((IntraDayOHLC) baseCp.getDataSource().get(baseCp.getDataSource().size() - 1)).getClose();
            } else {
                yArr[0] = HORIZ_LINE_INITIAL_VALUE;
            }
        }
        graph.repaint();
    }

    public boolean isshowPreviousCloseLine() {
        return showPreviousCloseLine;
    }

    public boolean isShowOrderLines() {
        return showOrderLines;
    }

    public void setShowOrderLines(boolean showOrderLines) {
        this.showOrderLines = showOrderLines;
        refreshOrderLines();
    }

    public boolean isShowBidAskTags() {
        return showBidAskTags;
    }

    public void setShowBidAskTags(boolean showBidAskTags) {
        this.showBidAskTags = showBidAskTags;
        if (bidAskTags != null && bidAskTags.length > 0) {
            removeFromStaticObjectArray(bidAskTags[0]);
            removeFromStaticObjectArray(bidAskTags[1]);
        }
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && showBidAskTags) {
            bidAskTags = ChartInterface.getBidTagAndAskTag(baseCp);
            addToStaticObjectArray(bidAskTags[0]);
            addToStaticObjectArray(bidAskTags[1]);
        }
        graph.repaint();
    }

    public boolean isShowMinMaxPriceLines() {
        return showMinMaxPriceLines;
    }

    public void setShowMinMaxPriceLines(boolean showMinMaxPriceLines) {
        this.showMinMaxPriceLines = showMinMaxPriceLines;
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null && minPriceLine != null && maxPriceLine != null) {
            double[] yMin = minPriceLine.getYArr();
            double[] yMax = maxPriceLine.getYArr();
            if (showMinMaxPriceLines) {
                yMin[0] = ChartInterface.getStockObject(baseCp.getSymbol()).getMinPrice();
                yMax[0] = ChartInterface.getStockObject(baseCp.getSymbol()).getMaxPrice();
            } else {
                yMin[0] = HORIZ_LINE_INITIAL_VALUE;
                yMax[0] = HORIZ_LINE_INITIAL_VALUE;
            }
        }
        graph.reDrawAll();
        graph.repaint();
    }

    public boolean isSnapToPrice() { //layout save
        return snapToPrice;
    }

    public boolean isShowResistanceLines() {
        return showResistanceLine;
    }

    public boolean isShowVolumeByPrice() {
        return showVolumeByPrice;
    }

    public void setShowVolumeByPrice(boolean showVolumeByPrice) {
        this.showVolumeByPrice = showVolumeByPrice;
        graph.repaint();
    }

    public void setShowResistanceLine(boolean showResistanceLine) {
        this.showResistanceLine = showResistanceLine;
    }

    public void tradeServerConnected() {
        refreshOrderLines();
    }

    public void tradeSecondaryPathConnected() {

    }

    public void tradeServerDisconnected() {
        refreshOrderLines();
    }

    public void orderStatusChanged(String orderID) {
        refreshOrderLines();
    }

    private void refreshOrderLines() {
        ChartProperties baseCp = getBaseCP();
        if (baseCp != null) {
            ArrayList<ObjectOrderLine> orderLines = ChartInterface.getOrderLines(baseCp);
            for (ObjectOrderLine objectOrderLine : orderLines) {
                removeFromStaticObjectArray(objectOrderLine);
                if (showOrderLines && !objectOrderLine.isRemove()) {
                    addToStaticObjectArray(objectOrderLine);
                }
            }
        }
        graph.repaint();
    }

    public int getMaxAllowableRightPixelForDragObject(int dragObjectType) {
        switch (dragObjectType) {
            case AbstractObject.INT_REGRESSION:
            case AbstractObject.INT_STD_DEV_CHANNEL:
            case AbstractObject.INT_STD_ERROR_CHANNEL:
            case AbstractObject.INT_RAFF_REGRESSION:
                return getPixelFortheIndex(getLastNonZeroIndex(0)); // TODO: instead of 0 put index of dragObject.getCP()
            default:
                return getPixelFortheIndex(graph.GDMgr.getGraphStoreSize() - 1);

        }
    }

    public int getMinAllowableLeftPixelForDragObject() {
        return getPixelFortheIndex(0); // TODO: instead of 0 put first NonZero index of dragObject.getCP()
    }

    public int getPixelGapFortheIndexGap(float index) {
        if (xFactor > 0)
            return (int) Math.round(index * xFactor);
        return 0;
    }

    public float getIndexGapForthePixelGap(int pixel) {
        if (graph.GDMgr.xFactor > 0)
            return (pixel / graph.GDMgr.xFactor);
        return 0;
    }

    public double getYValueGapForthePixelGap(int pixel, int pnlID) {
        double val = 0;
        if (pnlID >= 0) {
            val = pixel / yFactor[pnlID];
        }
        return val;
    }

    public int getPixelGapFortheYValueGap(double value, int pnlID) {
        int pixel = 0;
        if (pnlID >= 0) {
            pixel = (int) Math.round(value * yFactor[pnlID]);
        }
        return pixel;
    }

    public int getSourceCount() {
        return sourceCount;
    }

    public void setSourceCount(int sourceCount) {
        this.sourceCount = sourceCount;
    }

    public DynamicArray getGraphStore() {
        return graphStore;
    }

    public boolean isTableMode() {
        return tableMode;
    }

    public void setTableMode(boolean tableMode) {
        this.tableMode = tableMode;
    }

    public boolean isVWAP() {
        return isVWAP;
    }

    public void setVWAP(boolean VWAP) {
        isVWAP = VWAP;
        for (int i = 0; i < Sources.size(); i++) {
            ChartProperties cp = (ChartProperties) Sources.get(i);
            cp.setRecordSize(0);
        }
        updateDataSources();
        graph.NotifyChange = true;
        graph.reDrawAll();
    }

    public void setVWAPEnable(boolean vwap) {
        isVWAP = vwap;
    }

    public void toggleShowVWAP() {
        isVWAP ^= true;
//        isVWAP = !isVWAP;
        setVWAP(isVWAP);
    }

    public byte getSnapToPricePriority() {
        return snapToPricePriority;
    }

    public void setSnapToPricePriority(byte snapToPricePriority) {

        this.snapToPricePriority = snapToPricePriority;
    }

    public byte getCrossHairType() {
        return crossHairType;
    }

    public void setCrossHairType(byte crossHairType) {
        if ((crossHairType != SNAP_BALL_CROSS_HAIR) || (crossHairType != SNAP_BALL_ALONE)) {
            setLegendPriceDisplayempty();
        }
        this.crossHairType = crossHairType;
    }

    public byte getVolumeByPriceType() {
        return volumeByPriceType;
    }

    public void setVolumeByPriceType(byte type) {
        this.volumeByPriceType = type;
    }

    public void setLegendPriceDisplayempty() {
        legendDisplayTime = "";
        legendDisplaySnapPrice = "";
    }

    public double getMinMaxDifference(int panelID) {
        double diff = 0;
        diff = Math.abs(maxY[panelID] - minY[panelID]);
        return diff;
    }

    public boolean isYValueInsidePanel(double yValue, int pnlId) {
        boolean isInside = false;
        if ((!(yValue > maxY[pnlId])) && (!(yValue < minY[pnlId]))) {
            isInside = true;

        }
        return isInside;
    }

    public void refreshDataWindow() {

        long beginTime = System.currentTimeMillis();
        DataWindow window = ChartFrame.getSharedInstance().getDataWindow();
        if (window != null && window.isVisible() && !window.isWindowMode()) {
            window.getBtnDataModeImaginary().doClick();
        }
    }

    public int getNoOfCompareGraphs() {
        int count = 0;
        for (int i = 0; i < Sources.size(); i++) {
            if (Sources.get(i).getID() == ID_COMPARE) {
                count++;
            }
        }
        return count;
    }

    public String getGroupForRect(WindowPanel r) {

        String grpID = "";

        for (int i = 0; i < sourceCount; i++) {
            if (((ChartProperties) getSources().get(i)).getRect() == r) {
                grpID = ((ChartProperties) getSources().get(i)).getGroupID();
            }
        }
        return grpID;
    }

    public ArrayList<HighlightedArea> getHighLightedSources() {
        return highLightedSources;
    }

    public Color getInversionColor(Color c) {
        int r = (c.getRed() + 127) % 255;
        int g = (c.getGreen() + 127) % 255;
        int b = (c.getBlue() + 127) % 255;

        return new Color(r, g, b);
    }

    // To be used for synchronizing
    // old sync with graphStore should be replaced with this
    static class GraphLock {
    }
}



