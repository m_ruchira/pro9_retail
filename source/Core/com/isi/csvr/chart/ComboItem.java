package com.isi.csvr.chart;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class ComboItem {

    public static byte ITEM_NORMAL = 0;
    public static byte ITEM_CUSTOM_COLOR = 1;
    JColorChooser colorChooser;
    JDialog colorDialog;
    private String description;
    private Object value;
    final ActionListener cancelListner = new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            colorChooser.setColor((Color) value);
        }
    };
    private Icon image;
    private Icon disabledImage = null;
    private byte itemType;

    public ComboItem(String description, Object value, Icon image, byte itemType) {
        this.description = description;
        this.value = value;
        this.image = image;//(new ComboImage(color));
        this.itemType = itemType;
    }

    public String getDescription() {
        return description;
    }

    public Object getValue() {
        if (itemType == ITEM_CUSTOM_COLOR) {
            value = showCustomColorDialog((Color) value);
            ((ComboImage) image).setColor((Color) value);
        }
        return value;
    }

    public Icon getImage() {
        return image;
    }

    public Icon getDisabledImage() {
        if (disabledImage != null) {
            return disabledImage;
        }
        return image;
    }

    public void setDisabledImage(Icon ico) {
        disabledImage = ico;
    }

    public Color showCustomColorDialog(Color oldColor) {
        colorChooser = new JColorChooser(oldColor);
        colorChooser.setPreviewPanel(new JLabel());
        colorDialog = JColorChooser.createDialog(Client.getInstance().getFrame(),
                Language.getString("SELECT_COLOR"), true, colorChooser,
                null, cancelListner);
        colorDialog.show();
        return colorChooser.getColor();
    }
}