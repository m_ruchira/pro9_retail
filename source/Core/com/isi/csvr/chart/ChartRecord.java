package com.isi.csvr.chart;

/**
 * User: udaka
 * Date: May 3, 2006
 * Time: 3:00:20 AM
 */

public class ChartRecord extends ChartPoint {

    public static final byte STEP_1 = 5;
    public static final byte STEP_2 = 6;
    public static final byte STEP_3 = 7;
    public static final byte STEP_4 = 8;
    public static final byte STEP_5 = 9;
    public static final byte STEP_6 = 10;
    public static final byte STEP_7 = 11;
    public static final byte STEP_8 = 12;
    public static final byte STEP_9 = 13;
    public static final byte STEP_10 = 14;

    public long Time;
    private double[] tmpSteps = null;

    public ChartRecord() {

    }

    public ChartRecord(ChartPoint chartPoint, long time) {
        this.Open = chartPoint.Open;
        this.High = chartPoint.High;
        this.Low = chartPoint.Low;
        this.Close = chartPoint.Close;
        this.Volume = chartPoint.Volume;
        this.TurnOver = chartPoint.TurnOver;
        this.Time = time;
    }

    public double getValue(byte index) {
        if (index > GraphDataManager.INNER_Volume) {
            return tmpSteps[index - 5];
        } else {
            return super.getValue(index);
        }
    }

    public void setValue(byte index, double value) {
        if (index > GraphDataManager.INNER_Volume) {
            tmpSteps[index - 5] = value;
        } else {
            super.setValue(index, value);
        }
    }

    public double getStepValue(byte index) {
        switch (index) {
            case GraphDataManager.INNER_Open:
                return this.Open;
            case GraphDataManager.INNER_High:
                return this.High;
            case GraphDataManager.INNER_Low:
                return this.Low;
            case GraphDataManager.INNER_Close:
                return this.Close;
            case GraphDataManager.INNER_Volume:
                return this.Volume;
            default:
                return tmpSteps[index - 5];
        }
    }

    // never alter O,H,L,C,V values
    // use this only when index >= 5
    // Also must have set the StepSize > index-5
    public void setStepValue(byte index, double value) {
        switch (index) {
            case GraphDataManager.INNER_Open:
                this.Open = value;
                break;
            case GraphDataManager.INNER_High:
                this.High = value;
                break;
            case GraphDataManager.INNER_Low:
                this.Low = value;
                break;
            case GraphDataManager.INNER_Close:
                this.Close = value;
                break;
            case GraphDataManager.INNER_Volume:
                this.Volume = value;
                break;
            default:
                tmpSteps[index - 5] = value;
                break;
        }
    }

    public void setStepSize(int size) {
        tmpSteps = new double[size];
        for (int i = 0; i < size; i++) {
            tmpSteps[i] = Indicator.NULL;
        }
    }

    public void cloneStep(byte srcIndex, byte destIndex) {
        tmpSteps[destIndex - 5] = (srcIndex >= 5) ? tmpSteps[srcIndex - 5] : getValue(srcIndex);
    }
}
