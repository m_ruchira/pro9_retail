package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 29, 2006
 * Time: 4:45:34 PM
 */
public class VerticalHorizontalFilter extends ChartProperties implements Indicator {
    private static final long serialVersionUID = UID_VERT_HORI_FILTER;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public VerticalHorizontalFilter(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_VERT_HORI_FILTER") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_VERT_HORI_FILTER");
        this.timePeriods = 28;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    //############################################
    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte srcIndex, byte destIndex) {
        //long entryTime = System.currentTimeMillis();

        ChartRecord cr, crOld;

        for (int i = 0; i < al.size(); i++) {
            if (i >= t_im_ePeriods) {
                double sum = 0, highestC = -100000d, lowestC = Double.MAX_VALUE;
                for (int j = 0; j < t_im_ePeriods; j++) {
                    cr = (ChartRecord) al.get(i - j);
                    //numerator
                    highestC = Math.max(cr.getValue(srcIndex), highestC);
                    lowestC = Math.min(cr.getValue(srcIndex), lowestC);
                    //denominator
                    crOld = (ChartRecord) al.get(i - j - 1);
                    sum += Math.abs(cr.getValue(srcIndex) - crOld.getValue(srcIndex));
                }

                cr = (ChartRecord) al.get(i);
                /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(sum == 0 ? 0 : (highestC - lowestC) / sum);
                }*/
                cr.setValue(destIndex, sum == 0 ? 0 : (highestC - lowestC) / sum);
            } else {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
        }

        //System.out.println("**** VerticalHorizontalFilter Calc time " + (entryTime - System.currentTimeMillis()));

    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 28;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof VerticalHorizontalFilter) {
            VerticalHorizontalFilter vhf = (VerticalHorizontalFilter) cp;
            this.timePeriods = vhf.timePeriods;
            this.innerSource = vhf.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_VERT_HORI_FILTER") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();

        ChartRecord cr, crOld;

        for (int i = 0; i < al.size(); i++) {
            if (i >= timePeriods) {
                double sum = 0, highestC = -100000d, lowestC = Double.MAX_VALUE;
                for (int j = 0; j < timePeriods; j++) {
                    cr = (ChartRecord) al.get(i - j);
                    //numerator
                    highestC = Math.max(cr.getValue(getOHLCPriority()), highestC);
                    lowestC = Math.min(cr.getValue(getOHLCPriority()), lowestC);
                    //denominator
                    crOld = (ChartRecord) al.get(i - j - 1);
                    sum += Math.abs(cr.getValue(getOHLCPriority()) - crOld.getValue(getOHLCPriority()));
                }

                cr = (ChartRecord) al.get(i);
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(sum == 0 ? 0 : (highestC - lowestC) / sum);
                }
            } else {
                cr = (ChartRecord) al.get(i);
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }

        //System.out.println("**** VerticalHorizontalFilter Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_VERT_HORI_FILTER");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
