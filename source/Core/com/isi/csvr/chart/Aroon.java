package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 6:11:35 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public class Aroon extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_AROON;
    public static byte AROON_DOWN = 2;
    public static byte AROON_UP = 1;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private byte style;
    private String tableColumnHeading;

    public Aroon(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_AROON") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_AROON");
        this.timePeriods = 14;
        style = Aroon.AROON_UP;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }

    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int timePeriods, byte style,
                                          byte destIndex) {

        ChartRecord cr;
        double currVal;
        int currIndex;

        long entryTime = System.currentTimeMillis();

        int loopBegin = bIndex + timePeriods;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = loopBegin; i < al.size(); i++) {
            currIndex = 0;
            if (style == Aroon.AROON_UP) {
                currVal = Float.MIN_VALUE;
                for (int k = 0; k < timePeriods + 1; k++) {
                    cr = (ChartRecord) al.get(i - k);
                    boolean isANewHigh = cr.High > currVal;
                    if (isANewHigh) {
                        currVal = cr.High;
                        currIndex = k;
                    }
                }
            } else { // AROON_DOWN
                currVal = Float.MAX_VALUE;
                for (int k = 0; k < timePeriods + 1; k++) {
                    cr = (ChartRecord) al.get(i - k);
                    if (cr.Low < currVal) {
                        currVal = cr.Low;
                        currIndex = k;
                    }
                }
            }
            cr = (ChartRecord) al.get(i);
            /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue((timePeriods - currIndex) * 100f / timePeriods);*/
            cr.setValue(destIndex, (timePeriods - currIndex) * 100f / timePeriods);
        }
        //System.out.println("**** Aroon Calc time " + (entryTime - System.currentTimeMillis()));

    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.style = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/AroonStyle"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "AroonStyle", Byte.toString(this.style));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof Aroon) {
            Aroon arn = (Aroon) cp;
            this.timePeriods = arn.timePeriods;
            this.innerSource = arn.innerSource;
            this.style = arn.style;
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return (style == Aroon.AROON_UP) ? Language.getString("AROON_UP") + " " + parent :
                Language.getString("AROON_DOWN") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    /*
    I have used MFI = posFlow*100f/(posFlow+negFlow)
    which is correct and the best equation - if you follow the steps in metastock
    to arrive at this by first calculating "Money Ratio" it can lead to a division by zero
    when negative flow is zero - so it should not be used.
    */
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        ChartRecord cr;
        double currVal;
        int currIndex;

        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods;
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        for (int i = loopBegin; i < al.size(); i++) {
            currIndex = 0;
            if (style == Aroon.AROON_UP) {
                currVal = Float.MIN_VALUE;
                for (int k = 0; k < timePeriods + 1; k++) {
                    cr = (ChartRecord) al.get(i - k);
                    boolean isANewHigh = cr.High > currVal;
                    if (isANewHigh) {
                        currVal = cr.High;
                        currIndex = k;
                    }
                }
            } else { // AROON_DOWN
                currVal = Float.MAX_VALUE;
                for (int k = 0; k < timePeriods + 1; k++) {
                    cr = (ChartRecord) al.get(i - k);
                    if (cr.Low < currVal) {
                        currVal = cr.Low;
                        currIndex = k;
                    }
                }
            }
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue((timePeriods - currIndex) * 100f / timePeriods);
        }
        //System.out.println("**** Aroon Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }
    //############################################

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    public String getShortName() {
        return (style == Aroon.AROON_UP) ? Language.getString("AROON_UP") : Language.getString("AROON_DOWN");
    }

    public byte getStyle() {
        return style;
    }

    public void setStyle(byte style) {
        this.style = style;
    }
}

