package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: P.Dissanayake
 * Date: Feb 8, 2006
 * Time: 2:00:58 PM
 */
public class PriceROC extends ChartProperties implements Indicator {

    static final int PERCENT = 0;
    static final int POINTS = 1;
    private static final long serialVersionUID = UID_ROC;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public PriceROC(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_ROC") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_ROC");
        this.timePeriods = 7;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, CalcMethod calcMethod, byte srcIndex, byte destIndex) {

        ChartRecord cr, crOld;
        //long entryTime = System.currentTimeMillis();
        double val;

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            val = cr.getValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////

        int loopBegin = bIndex + t_im_ePeriods;
        if ((loopBegin >= al.size()) || (t_im_ePeriods < 1)) { //not enough data points
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - t_im_ePeriods);
            cr = (ChartRecord) al.get(i);
            if (calcMethod == CalcMethod.PERCENT) {
                val = 100 * (cr.getValue(srcIndex) / crOld.getValue(srcIndex) - 1);
            } else {
                val = cr.getValue(srcIndex) - crOld.getValue(srcIndex);
            }
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                cr.setValue(destIndex, val);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        //System.out.println("**** Inner ROC Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 7;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof PriceROC) {
            PriceROC roc = (PriceROC) cp;
            this.timePeriods = roc.timePeriods;
            this.innerSource = roc.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_ROC") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }
    //############################################

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        long entryTime = System.currentTimeMillis();
        int loopBegin = timePeriods;
        ChartRecord cr, crOld;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - loopBegin);
            cr = (ChartRecord) al.get(i);

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(100 * (cr.getValue(getOHLCPriority()) - crOld.getValue(getOHLCPriority())) / crOld.getValue(getOHLCPriority()));
            }
        }
        //System.out.println("**** ROC Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_ROC");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
