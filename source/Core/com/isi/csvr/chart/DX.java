package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 21, 2006
 * Time: 5:02:29 PM
 */
public class DX extends ChartProperties implements Indicator {

    private static final long serialVersionUID = UID_DX;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public DX(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_DX") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_DX");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    // This needs 3 intermediate steps in total to keep PlusDI, MinusDI and SumOfTR
    public static void getDX(ArrayList al, int bIndex, int timePeriods, byte destIndex, byte stepPlusDI, byte stepMinusDI, byte stepSumOfTR) {
        double diff, sum;

        long entryTime = System.currentTimeMillis();
        ChartRecord cr;

        // values returned are NULL upto and including index timePeriods-1 (Count=timePeriods)
        PlusDI.getPlusDI(al, 0, timePeriods, stepPlusDI, stepSumOfTR);
        MinusDI.getMinusDI(al, 0, timePeriods, stepMinusDI, stepSumOfTR);

        int loopBegin = bIndex + timePeriods;
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(destIndex, Indicator.NULL);
        }

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            diff = Math.abs(cr.getStepValue(stepPlusDI) - cr.getStepValue(stepMinusDI));
            sum = cr.getStepValue(stepPlusDI) + cr.getStepValue(stepMinusDI);

            cr.setStepValue(destIndex, Math.round(diff * 100 / sum));
        }

        //System.out.println("**** Inter DX Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof DX) {
            DX dx = (DX) cp;
            this.timePeriods = dx.timePeriods;
            this.innerSource = dx.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_DX") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // calculation includes 3 intermediate steps: sumOfTR, PlusDI and MinusDI
    // note that sumOfTR step is common to both PlusDI and MinusDI calculations
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double diff, sum;

        long entryTime = System.currentTimeMillis();
        ChartRecord cr;
        //setting step size 3
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }
        // steps involved
        byte stepSumOfTR = ChartRecord.STEP_1;
        byte stepPlusDI = ChartRecord.STEP_2;
        byte stepMinusDI = ChartRecord.STEP_3;

        // values returned are NULL upto and including index timePeriods-1 (Count=timePeriods)
        PlusDI.getPlusDI(al, 0, timePeriods, stepPlusDI, stepSumOfTR);
        MinusDI.getMinusDI(al, 0, timePeriods, stepMinusDI, stepSumOfTR);

        for (int i = 0; i < timePeriods; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        for (int i = timePeriods; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            diff = Math.abs(cr.getStepValue(stepPlusDI) - cr.getStepValue(stepMinusDI));
            sum = cr.getStepValue(stepPlusDI) + cr.getStepValue(stepMinusDI);

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(Math.round(diff * 100 / sum));
        }

        //System.out.println("**** DX Calc time " + (entryTime - System.currentTimeMillis()));
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public String getShortName() {
        return Language.getString("IND_DX");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
