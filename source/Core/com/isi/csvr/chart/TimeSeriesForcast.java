package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Apr 4, 2006
 * Time: 1:16:31 PM
 */
public class TimeSeriesForcast extends ChartProperties implements Indicator {
    private static final long serialVersionUID = UID_TSF;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public TimeSeriesForcast(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_TSF") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_TSF");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    //############################################
    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, byte srcIndex, byte destIndex) {
        getForecast(al, bIndex, t_im_ePeriods, GDM, srcIndex, destIndex);
    }

    public static void getForecast(ArrayList al, int bIndex, int timePeriods, GraphDataManagerIF GDM, byte srcIndex, byte destIndex) {

        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;

        int loopBegin = bIndex + timePeriods;
        if ((loopBegin >= al.size()) || (timePeriods < 1)) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        if (timePeriods == 1) { // single point
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.cloneStep(srcIndex, destIndex);
            }
            return;
        }

        ///////////// this is done to eleminate all the null points ////////////
        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        ArrayList alNotNull = new ArrayList();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                alNotNull.add(cr);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        ////////////////////////////////////////////////////////////////////////

        // if not enough data after filtering
        if (alNotNull.size() < timePeriods) {
            for (int i = 0; i < alNotNull.size(); i++) {
                cr = (ChartRecord) alNotNull.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        double zigmaX = 0, zigmaY = 0, zigmaXX = 0, zigmaXY = 0, yi, yPi, Sxx, Sxy;
        double xi = 0, xPi, b1, b0;
        // first point
        for (int i = 0; i < timePeriods; i++) {
            cr = (ChartRecord) alNotNull.get(i);

            xi = i;//GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getValue(srcIndex);
            zigmaX += xi;
            zigmaY += yi;
            zigmaXX += xi * xi;
            zigmaXY += xi * yi;

            if (i == timePeriods - 1) {
                Sxx = zigmaXX - zigmaX * zigmaX / timePeriods;
                Sxy = zigmaXY - zigmaX * zigmaY / timePeriods;
                b1 = Sxx == 0 ? 0 : Sxy / Sxx;
                b0 = zigmaY / timePeriods - b1 * zigmaX / timePeriods;
                cr.setValue(destIndex, b0 + b1 * xi + b1); //Add the slope to the linear regression indicator
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        // rest of the points
        for (int i = timePeriods; i < alNotNull.size(); i++) {
            cr = (ChartRecord) alNotNull.get(i);
            crOld = (ChartRecord) alNotNull.get(i - timePeriods);

            xi = i; //GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getValue(srcIndex);
            xPi = i - timePeriods; //GDM.getClosestIndexFortheTime(crOld.Time);
            yPi = crOld.getValue(srcIndex);
            zigmaX = zigmaX - xPi + xi;
            zigmaY = zigmaY - yPi + yi;
            zigmaXX = zigmaXX - xPi * xPi + xi * xi;
            zigmaXY = zigmaXY - xPi * yPi + xi * yi;

            Sxx = zigmaXX - zigmaX * zigmaX / timePeriods;
            Sxy = zigmaXY - zigmaX * zigmaY / timePeriods;
            b1 = Sxx == 0 ? 0 : Sxy / Sxx;
            b0 = zigmaY / timePeriods - b1 * zigmaX / timePeriods;

            cr.setValue(destIndex, b0 + b1 * xi + b1); //Add the slope to the linear regression indicator
        }

        //System.out.println("**** Inner TimeSeriesForcast Calc time "  + (entryTime - System.currentTimeMillis()));
    }

    public static void getForcast(ArrayList al, int bIndex, int timePeriods, GraphDataManagerIF GDM, byte srcIndex, byte destIndex) {
        ChartRecord cr, crOld;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////
        if ((bIndex + timePeriods >= al.size()) || (timePeriods < 1)) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }

        if (timePeriods == 1) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.cloneStep(srcIndex, destIndex);
            }
            return;
        }

        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(destIndex, Indicator.NULL);
        }

        double zigmaX = 0, zigmaY = 0, zigmaXX = 0, zigmaXY = 0, yi, yPi, Sxx, Sxy;
        double xi = 0, xPi, b1, b0;

        int count = 0;
        for (int i = bIndex; i < (bIndex + timePeriods); i++) {
            cr = (ChartRecord) al.get(i);
            boolean isValid = false;
            double val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                isValid = true;
                count++;

                xi = GDM.getClosestIndexFortheTime(cr.Time);
                yi = val;
                zigmaX += xi;
                zigmaY += yi;
                zigmaXX += xi * xi;
                zigmaXY += xi * yi;
            }
            if ((i >= bIndex + timePeriods - 1) && isValid) {
                Sxx = zigmaXX - zigmaX * zigmaX / count; //timePeriods >= count
                Sxy = zigmaXY - zigmaX * zigmaY / count; //timePeriods >= count
                b1 = Sxx == 0 ? 0 : Sxy / Sxx;
                b0 = zigmaY / count - b1 * zigmaX / count; //timePeriods >= count
                cr.setStepValue(destIndex, b0 + b1 * xi + b1); //Add the slope to the linear regression indicator
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }

        for (int i = (bIndex + timePeriods); i < al.size(); i++) {
            boolean isValid = false;
            crOld = (ChartRecord) al.get(i - timePeriods);
            double val = crOld.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                isValid = true;
                count--;

                xPi = GDM.getClosestIndexFortheTime(crOld.Time);
                yPi = val;
                zigmaX = zigmaX - xPi;
                zigmaY = zigmaY - yPi;
                zigmaXX = zigmaXX - xPi * xPi;
                zigmaXY = zigmaXY - xPi * yPi;
            }

            cr = (ChartRecord) al.get(i);
            val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                isValid = true;
                count++;

                xi = GDM.getClosestIndexFortheTime(cr.Time);
                yi = val;
                zigmaX = zigmaX + xi;
                zigmaY = zigmaY + yi;
                zigmaXX = zigmaXX + xi * xi;
                zigmaXY = zigmaXY + xi * yi;
            } else {
                isValid = false;
            }

            if (isValid && (count > 0)) {
                Sxx = zigmaXX - zigmaX * zigmaX / count;
                Sxy = zigmaXY - zigmaX * zigmaY / count;
                b1 = Sxx == 0 ? 0 : Sxy / Sxx;
                b0 = zigmaY / count - b1 * zigmaX / count;

                cr.setStepValue(destIndex, b0 + b1 * xi + b1); //Add the slope to the linear regression indicator
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }

        //System.out.println("**** Inner TimeSeriesForcast Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof TimeSeriesForcast) {
            TimeSeriesForcast tf = (TimeSeriesForcast) cp;
            this.timePeriods = tf.timePeriods;
            this.innerSource = tf.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_TSF") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        if (innerSource instanceof Indicator) {
            return ((Indicator) innerSource).hasItsOwnScale();
        }
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;

        double zigmaX = 0, zigmaY = 0, zigmaXX = 0, zigmaXY = 0, yi, yPi, Sxx, Sxy;
        double xi, xPi, b1, b0;
        ChartRecord cr, crOld;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            xi = GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getValue(getOHLCPriority());
            zigmaX += xi;
            zigmaY += yi;
            zigmaXX += xi * xi;
            zigmaXY += xi * yi;
            if (i >= loopBegin - 1) {
                Sxx = zigmaXX - zigmaX * zigmaX / timePeriods;
                Sxy = zigmaXY - zigmaX * zigmaY / timePeriods;
                b1 = Sxy / Sxx;
                b0 = zigmaY / timePeriods - b1 * zigmaX / timePeriods;
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(b0 + b1 * xi + b1); //Add the slope to the linear regression indicator
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - timePeriods);
            cr = (ChartRecord) al.get(i);
            xi = GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getValue(getOHLCPriority());
            xPi = GDM.getClosestIndexFortheTime(crOld.Time);
            yPi = crOld.getValue(getOHLCPriority());

            zigmaX = zigmaX + xi - xPi;
            zigmaY = zigmaY + yi - yPi;
            zigmaXX = zigmaXX + xi * xi - xPi * xPi;
            zigmaXY = zigmaXY + xi * yi - xPi * yPi;

            Sxx = zigmaXX - zigmaX * zigmaX / timePeriods;
            Sxy = zigmaXY - zigmaX * zigmaY / timePeriods;
            b1 = Sxy / Sxx;
            b0 = zigmaY / timePeriods - b1 * zigmaX / timePeriods;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(b0 + b1 * xi + b1); //Add the slope to the linear regression indicator
            }
        }
        //System.out.println("**** TimeSeriesForcast Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_TSF");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
