package com.isi.csvr.chart;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;


public class RSI extends ChartProperties implements Indicator, Serializable {

    private static final long serialVersionUID = UID_RSI;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public RSI(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("INDI_RSI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_RSI");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    // tobe removed - this methoed must not be used!!!!!!!!!!!!!!!!
    public static double getRSI(LinkedList<ChartRecord> ll, int timePeriod, byte srcIndex) {
        double currVal;
        if ((timePeriod >= ll.size()) || (timePeriod < 2)) return 0.0;

        ChartRecord cr, crOld, newCr;

        // steps involved
        byte stepUpSum = ChartRecord.STEP_1;
        byte stepDownSum = ChartRecord.STEP_2;
        byte stepWSUpSum = ChartRecord.STEP_3;
        byte stepWSDownSum = ChartRecord.STEP_4;

        ArrayList al = new ArrayList();

        for (int i = 1; i < ll.size(); i++) {
            crOld = ll.get(i - 1);
            cr = ll.get(i);

            newCr = new ChartRecord();
            newCr.Open = cr.Open;
            newCr.Close = cr.Close;
            newCr.High = cr.High;
            newCr.Low = cr.Low;
            newCr.Volume = cr.Volume;
            newCr.Time = cr.Time;
            newCr.setStepSize(4);

            currVal = cr.getValue(srcIndex) - crOld.getValue(srcIndex);
            if (currVal >= 0) {
                newCr.setStepValue(stepUpSum, currVal);
                newCr.setStepValue(stepDownSum, 0d);
            } else {
                newCr.setStepValue(stepUpSum, 0d);
                newCr.setStepValue(stepDownSum, -currVal);
            }
            al.add(newCr);
        }

        WildersSmoothing.getWildersSmoothing(al, 0, timePeriod, stepUpSum, stepWSUpSum);
        WildersSmoothing.getWildersSmoothing(al, 0, timePeriod, stepDownSum, stepWSDownSum);

        double upSum;
        double downSum;
        cr = (ChartRecord) al.get(al.size() - 1);
        upSum = cr.getStepValue(stepWSUpSum);
        downSum = cr.getStepValue(stepWSDownSum);
        newCr = null;
        al = null;

        return ((upSum + downSum) == 0) ? 0.0 : 100.0 * upSum / (upSum + downSum);
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods,
                                          byte stepUpSum, byte stepDownSum, byte stepWSUpSum, byte stepWSDownSum, byte srcIndex, byte destIndex) {
        getRelativeStrengthIndex(al, bIndex, t_im_ePeriods, GDM,
                stepUpSum, stepDownSum, stepWSUpSum, stepWSDownSum, srcIndex, destIndex);
    }

    public static void getRelativeStrengthIndex(ArrayList al, int bIndex, int t_im_ePeriods, GraphDataManagerIF GDM,
                                                byte stepUpSum, byte stepDownSum, byte stepWSUpSum, byte stepWSDownSum, byte srcIndex, byte destIndex) {

        long entryTime = System.currentTimeMillis();

        double currVal;
        ChartRecord cr, crOld;

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////

        int loopBegin = bIndex + t_im_ePeriods;

        if ((loopBegin >= al.size()) || (t_im_ePeriods < 1)) { //not enough data points
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        for (int i = bIndex + 1; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(srcIndex) - crOld.getValue(srcIndex);
            if (!Double.isNaN(currVal) && (Double.NEGATIVE_INFINITY != currVal) && (Double.POSITIVE_INFINITY != currVal) &&
                    (Indicator.NULL != currVal)) {
                if (currVal >= 0) {
                    cr.setValue(stepUpSum, currVal);
                    cr.setValue(stepDownSum, 0d);
                } else {
                    cr.setValue(stepUpSum, 0d);
                    cr.setValue(stepDownSum, -currVal);
                }
            } else {
                cr.setValue(stepUpSum, Indicator.NULL);
                cr.setValue(stepDownSum, Indicator.NULL);
            }
        }

        WildersSmoothing.getWildersSmoothing(al, bIndex + 1, t_im_ePeriods, stepUpSum, stepWSUpSum);
        WildersSmoothing.getWildersSmoothing(al, bIndex + 1, t_im_ePeriods, stepDownSum, stepWSDownSum);

        double upSum;
        double downSum;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            upSum = cr.getValue(stepWSUpSum);
            downSum = cr.getValue(stepWSDownSum);
            double val = (upSum + downSum == 0) ? 0 : 100f * upSum / (upSum + downSum);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                cr.setValue(destIndex, val);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        //System.out.println("**** Inner RSI Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public static int getAuxStepCount() {
        return 4;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof RSI) {
            RSI rsi = (RSI) cp;
            this.timePeriods = rsi.timePeriods;
            this.innerSource = rsi.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("INDI_RSI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }
    //############################################

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    // this includes 4 intermediate steps: UpSum, DownSum, WSUpSum and WSDownSum
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double currVal;
        int loopBegin = timePeriods;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;

        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;
        //setting step size 4
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(4);
        }
        // steps involved
        byte stepUpSum = ChartRecord.STEP_1;
        byte stepDownSum = ChartRecord.STEP_2;
        byte stepWSUpSum = ChartRecord.STEP_3;
        byte stepWSDownSum = ChartRecord.STEP_4;

        for (int i = 1; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(getOHLCPriority()) - crOld.getValue(getOHLCPriority());
            if (currVal >= 0) {
                cr.setStepValue(stepUpSum, currVal);
                cr.setStepValue(stepDownSum, 0d);
            } else {
                cr.setStepValue(stepUpSum, 0d);
                cr.setStepValue(stepDownSum, -currVal);
            }
        }

        WildersSmoothing.getWildersSmoothing(al, 1, timePeriods, stepUpSum, stepWSUpSum);
        WildersSmoothing.getWildersSmoothing(al, 1, timePeriods, stepDownSum, stepWSDownSum);

        double upSum;
        double downSum;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            upSum = cr.getStepValue(stepWSUpSum);
            downSum = cr.getStepValue(stepWSDownSum);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(upSum + downSum == 0 ? 0 : 100f * upSum / (upSum + downSum));  // check for division by 0
            }
        }
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        //System.out.println("**** RSI Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("INDI_RSI");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}