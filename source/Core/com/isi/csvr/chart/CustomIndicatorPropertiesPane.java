package com.isi.csvr.chart;

import com.isi.csvr.chart.customindicators.IndicatorMenuObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.shared.ValueFormatter;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * User: Pramoda
 * Date: Feb 23, 2007
 * Time: 11:19:07 AM
 */
public class CustomIndicatorPropertiesPane extends TWTabbedPane {

    final static Dimension DIM_DIALOG = new Dimension(320, 250);
    final static int CMB_HEIGHT = 22;
    final static int ITEM_WIDTH = 120;
    final static int LBL_HEIGHT = 20;
    final static int GAP = 1;
    final static int LEFT = 5;
    final static int VADJ = 8;
    //    final static String METHOD_SET_COLOR = "setColor";
    final static String METHOD_SET_COLOR = "setColor";
    //    final static String METHOD_WARNING_COLOR = "setWarningColor";
    final static String METHOD_WARNING_COLOR = "setWarningColor";
    final static String METHOD_SET_STYLE = "setStyle";
    final static String METHOD_SET_PEN_STYLE = "setPenStyle";
    final static String METHOD_SET_WIDTH = "setPenWidth";
    public static String COLOR_SEP = ",";
    static ImageIcon icon = new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif");
    static Color[] colors = {Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY,
            Color.GRAY, Color.LIGHT_GRAY, Color.GREEN, Color.MAGENTA, Color.ORANGE, Color.PINK, Color.RED,
            Color.WHITE, Color.YELLOW};
    static String[] colorNames = extractColorNames();
    //static String[] lineStyles = {"Solid", "Dot", "Dash", "DashDot", "DashDotDot"};
    static String[] lineStyles = {Language.getString("CS_SOLID"), Language.getString("CS_DOT"), Language.getString("CS_DASH")
            , Language.getString("CS_DASH_DOT"), Language.getString("CS_DASH_DOT_DOT")};
    static Icon[] penStyles = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_solid.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dot.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash_dot.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash_dot_dot.gif")
    };
    static Icon[] penWidths = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_0p5.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_1p0.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_1p5.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_2p0.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_3p0.gif")
    };
    static String[] saPriority = {Language.getString("OPEN"),
            Language.getString("HIGH"), Language.getString("LOW"),
            Language.getString("CLOSE")
    };
    static String[] saMAMethods = {Language.getString("METHOD_SIMPLE"),
            Language.getString("METHOD_WEIGHTED")
            , Language.getString("METHOD_EXPONENTIAL")
            , Language.getString("METHOD_VARIABLE")
            , Language.getString("METHOD_TIME_SERIES")
            , Language.getString("METHOD_TRANGULAR")
            , Language.getString("METHOD_VOLUME_ADJUSTED")
    };
    static String[] saCCPeriods = {new String("Monthly"),
            new String("Daily")
    };
    static String[] saCalcMethod = {Language.getString("PERCENT"),
            Language.getString("POINTS")};


    static String[] lineWidths = {"1.0", "1.5", "2.0", "3.0", "5.0"};

    private int numberOfParams = 0;


    public CustomIndicatorPropertiesPane(ChartProperties origin, ChartProperties target, StockGraph graph) {
        IndicatorBase ib = (IndicatorBase) origin;
        if (ib.isDrawSymbol()) {
            if (ib.isPattern()) {
                addTab(Language.getString("GENERAL"), createPatternPanel(origin, target, graph), Language.getString("SELECT_COLORS_AND_STYLES"));
            } else {
                addTab(Language.getString("GENERAL"), createStrategyPanel(origin, target, graph), Language.getString("SELECT_COLORS_AND_STYLES"));
            }
        } else {
            addTab(Language.getString("GENERAL"), createGeneralPanel(origin, target, graph),
                    Language.getString("SELECT_COLORS_AND_STYLES"));
        }
        //JScrollPane paramPane = createAdvancedPanel(origin, target, graph);
        JPanel paramPane = createAdvancedPanel(origin, target, graph);
        if (numberOfParams > 0) {
            addTab(Language.getString("PARAMETERS"), paramPane, Language.getString("ADVANCED_TOOLTIP"));
            setSelectedIndex(1);
        } else {
            setSelectedIndex(0);
        }
    }

    public static String[] extractColorNames() {
        String[] sa = null;
        try {
            sa = Language.getString("COLOR_NAMES").split(COLOR_SEP);
        } catch (Exception ex) {
        }
        return sa;
    }

    private static JComboBox getPriorityCombo(ChartProperties target, final String[] items, int x, int y, int w, int h) {
        JComboBox cmbPrio = new JComboBox();
        cmbPrio.setBounds(x, y, w, h);
        cmbPrio.setEditable(false);
        cmbPrio.setFont(StockGraph.font_BOLD_10);
        if (items.length == 1) {
            cmbPrio.addItem(items[0]);
            cmbPrio.setSelectedIndex(0);
        } else {
            for (int i = 0; i < items.length; i++) {
                cmbPrio.addItem(items[i]);
                if (i == target.getOHLCPriority()) {
                    cmbPrio.setSelectedIndex(i);
                }
            }
        }
        return cmbPrio;
    }

    private static JComboBox getPriorityCombo(ChartProperties target, final String[] items) {
        JComboBox cmbPrio = new JComboBox();
        cmbPrio.setEditable(false);
        cmbPrio.setFont(StockGraph.font_BOLD_10);
        if (items.length == 1) {
            cmbPrio.addItem(items[0]);
            cmbPrio.setSelectedIndex(0);
        } else {
            for (int i = 0; i < items.length; i++) {
                cmbPrio.addItem(items[i]);
                if (i == target.getOHLCPriority()) {
                    cmbPrio.setSelectedIndex(i);
                }
            }
        }
        return cmbPrio;
    }

    private JPanel createPatternPanel(final ChartProperties origin, final ChartProperties target,
                                      final StockGraph graph) {

        //parameters panel
        JPanel paramPanel = new JPanel(new FlexGridLayout(new String[]{"40%"}, new String[]{"20", "20", "20", "20", "20", "20"}, 10, 5));
        paramPanel.setSize(DIM_DIALOG);

        GraphLabel lblColor = new GraphLabel(Language.getString("COLOR"));
        JComboBox cmbColor = getColorCombo(origin, target, graph, METHOD_SET_COLOR, target.getColor(), LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblBorderColor = new GraphLabel(Language.getString("STRATEGY_BORDER"));
        JComboBox cmbBorderColor = getColorCombo(origin, target, graph, METHOD_WARNING_COLOR, target.getWarningColor(), LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblPosition = new GraphLabel(Language.getString("PATTERN_POSITION"));
        JComboBox cmbPosition = new JComboBox();
        cmbPosition.setEditable(false);
        cmbPosition.setFont(StockGraph.font_BOLD_10);
        String[] sa = IndicatorBase.getLabelPositionArray();
        for (int i = 0; i < sa.length; i++) {
            cmbPosition.addItem(sa[i]);
            if (((IndicatorBase) target).getPosition() + 1 == i) {
                cmbPosition.setSelectedIndex(i);
            }
        }
        cmbPosition.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((IndicatorBase) target).setPosition(cmb.getSelectedIndex() - 1);
                    //dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblColor);
        paramPanel.add(cmbColor);
        paramPanel.add(lblBorderColor);
        paramPanel.add(cmbBorderColor);
        paramPanel.add(lblPosition);
        paramPanel.add(cmbPosition);

        return paramPanel;
    }

    private JPanel createStrategyPanel(final ChartProperties origin, final ChartProperties target,
                                       final StockGraph graph) {

        //parameters panel
        JPanel paramPanel = new JPanel();
        paramPanel.setSize(DIM_DIALOG);
        paramPanel.setLayout(null);

        GraphLabel lblSymbol = new GraphLabel(Language.getString("STRATEGY_SYMBOL"));
        lblSymbol.setBounds(LEFT, GAP, 100, CMB_HEIGHT);
        JComboBox cmbSymbol = new JComboBox();
        cmbSymbol.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbSymbol.setEditable(false);
        cmbSymbol.setFont(StockGraph.font_BOLD_10);
        String[] sa = IndicatorBase.getSymbolArray();
        for (int i = 0; i < sa.length; i++) {
            cmbSymbol.addItem(sa[i]);
            if (((IndicatorBase) target).getSymbolID() - 1 == i) {
                cmbSymbol.setSelectedIndex(i);
            }
        }
        cmbSymbol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((IndicatorBase) target).setSymbolID(cmb.getSelectedIndex() + 1);
                    //dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        GraphLabel lblColor = new GraphLabel(Language.getString("COLOR"));
        lblColor.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbColor = getColorCombo(origin, target, graph, METHOD_SET_COLOR, target.getColor(), LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);

        GraphLabel lblPosition = new GraphLabel(Language.getString("STRATEGY_POSITION"));
        lblPosition.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, 100, CMB_HEIGHT);
        JComboBox cmbPosition = new JComboBox();
        cmbPosition.setBounds(LEFT, 6 * GAP + 5 * CMB_HEIGHT, 100, CMB_HEIGHT);
        cmbPosition.setEditable(false);
        cmbPosition.setFont(StockGraph.font_BOLD_10);
        sa = IndicatorBase.getSymbolPositionArray();
        for (int i = 0; i < sa.length; i++) {
            cmbPosition.addItem(sa[i]);
            if (((IndicatorBase) target).getPosition() + 1 == i) {
                cmbPosition.setSelectedIndex(i);
            }
        }
        cmbPosition.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    ((IndicatorBase) target).setPosition(cmb.getSelectedIndex() - 1);
                    //dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });

        paramPanel.add(lblSymbol);
        paramPanel.add(cmbSymbol);
        paramPanel.add(lblColor);
        paramPanel.add(cmbColor);
        paramPanel.add(lblPosition);
        paramPanel.add(cmbPosition);

        return paramPanel;
    }

    private JPanel createAdvancedPanel(final ChartProperties origin, final ChartProperties target,
                                       final StockGraph graph) {
        JPanel panel = new JPanel();
        //panel.setLayout(null);
        JScrollPane scrollPane = new JScrollPane();

        String longName = ((IndicatorBase) target).getLongName();
        boolean isStrategyPattern = false;
        try {
            Class indClass = null;
            for (IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
                if (imo.getIndicatorName().equals(longName)) {
                    indClass = imo.getIndicatorClass();
                }
            }
            if (indClass == null) {
                for (IndicatorMenuObject imo : IndicatorDetailStore.alStrategies) {
                    if (imo.getIndicatorName().equals(longName)) {
                        indClass = imo.getIndicatorClass();
                        isStrategyPattern = true;
                    }
                }
            }
            if (indClass == null) {
                for (IndicatorMenuObject imo : IndicatorDetailStore.alPatterns) {
                    if (imo.getIndicatorName().equals(longName)) {
                        indClass = imo.getIndicatorClass();
                        isStrategyPattern = true;
                    }
                }
            }
            //if (indClass == null) return scrollPane;

            Field[] fields = indClass.getDeclaredFields();
            String[] horz = new String[]{"50%", "40%"};
            String[] vert = new String[fields.length];
            for (int i = 0; i < fields.length; i++) {
                vert[i] = "25";
            }
            panel = new JPanel(new FlexGridLayout(horz, vert, 10, 10));
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                final String fieldName = field.getName();

                if (fieldName.startsWith("_f_")) {
                    if (field.getType().equals(Integer.TYPE)) {
//                        GraphLabel lblTimePeriods = new GraphLabel(getApprpriateFieldName(fieldName.substring(3)));
                        String lblParamName = isStrategyPattern ? getApprpriateFieldName(fieldName.substring(3)) : fieldName.substring(3);
                        GraphLabel lblTimePeriods = new GraphLabel(lblParamName);
                        //lblTimePeriods.setBounds(LEFT, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        //panel.add(lblTimePeriods);
                        //panel.setPreferredSize(new Dimension(panel.getWidth(), 10 + i * (10 + CMB_HEIGHT) + CMB_HEIGHT));
                        //panel.repaint();
                        //panel.revalidate();
                        panel.add(lblTimePeriods);
                        SpinnerNumberModel model = new SpinnerNumberModel(20, 1, 1000, 1);
                        final JSpinner spinTimePeriods = new JSpinner(model);
                        //spinTimePeriods.setBounds(LEFT + 150, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        panel.add(spinTimePeriods);
                        Method getMethod = indClass.getMethod("get" + fieldName.substring(3));
                        Object[] args = new Object[]{};
                        Object returnVal = getMethod.invoke(origin, args);
                        spinTimePeriods.getModel().setValue(returnVal);
                        final Class indClass1 = indClass;
                        spinTimePeriods.addChangeListener(new ChangeListener() {
                            public void stateChanged(ChangeEvent e) {
                                if (target != null) {
                                    int val = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                                    System.out.println("integer spinner stateChanged " + val);
                                    try {
                                        Class[] paramClasses = new Class[]{Integer.TYPE};
                                        Method setMethod = indClass1.getMethod("set" + fieldName.substring(3), paramClasses);
                                        Object[] args = new Object[]{val};
                                        setMethod.invoke(target, args);
                                        PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                                    } catch (NoSuchMethodException e1) {
                                        e1.printStackTrace();
                                    } catch (IllegalAccessException e1) {
                                        e1.printStackTrace();
                                    } catch (InvocationTargetException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });
                        numberOfParams++;
                    } else if (field.getType().equals(OhlcPriority.class)) {
                        String lblParamName = isStrategyPattern ? getApprpriateFieldName(fieldName.substring(3)) : fieldName.substring(3);
                        GraphLabel lblPrice = new GraphLabel(lblParamName);
                        //lblPrice.setBounds(LEFT, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        //panel.add(lblPrice);
                        //panel.setPreferredSize(new Dimension(panel.getWidth(), 10 + i * (10 + CMB_HEIGHT) + CMB_HEIGHT));
                        //panel.repaint();
                        //panel.revalidate();
                        panel.add(lblPrice);
                        //final JComboBox cmbOHLCPrio = getPriorityCombo(target, saPriority, LEFT + 150, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        final JComboBox cmbOHLCPrio = getPriorityCombo(target, saPriority);
                        panel.add(cmbOHLCPrio);
                        Method getMethod = indClass.getMethod("get" + fieldName.substring(3));
                        Object[] args = new Object[]{};
                        Object returnVal = getMethod.invoke(origin, args);
                        cmbOHLCPrio.setSelectedIndex(((OhlcPriority) returnVal).ordinal());
                        final Class indClass1 = indClass;
                        cmbOHLCPrio.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if (target != null) {
                                    byte val = (byte) cmbOHLCPrio.getSelectedIndex();
                                    OhlcPriority ohlcPriority = OhlcPriority.values()[val];
                                    System.out.println("OHLCPriority combo stateChanged " + val);
                                    try {
                                        Class[] paramClasses = new Class[]{OhlcPriority.class};
                                        Method setMethod = indClass1.getMethod("set" + fieldName.substring(3), paramClasses);
                                        Object[] args = new Object[]{ohlcPriority};
                                        setMethod.invoke(target, args);
                                        PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                                    } catch (NoSuchMethodException e1) {
                                        e1.printStackTrace();
                                    } catch (IllegalAccessException e1) {
                                        e1.printStackTrace();
                                    } catch (InvocationTargetException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });
                        numberOfParams++;
                    } else if (field.getType().equals(MAMethod.class)) {
                        String lblParamName = isStrategyPattern ? getApprpriateFieldName(fieldName.substring(3)) : fieldName.substring(3);
                        GraphLabel lblPrice = new GraphLabel(lblParamName);
                        //lblPrice.setBounds(LEFT, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        //panel.setPreferredSize(new Dimension(panel.getWidth(), 10 + i * (10 + CMB_HEIGHT) + CMB_HEIGHT));
                        //panel.repaint();
                        //panel.revalidate();
                        panel.add(lblPrice);
                        //final JComboBox cmbMAMethod = getMAMethodCombo(saMAMethods, LEFT + 150, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        final JComboBox cmbMAMethod = getMAMethodCombo(saMAMethods);
                        panel.add(cmbMAMethod);
                        Method getMethod = indClass.getMethod("get" + fieldName.substring(3));
                        Object[] args = new Object[]{};
                        Object returnVal = getMethod.invoke(origin, args);
                        cmbMAMethod.setSelectedIndex(((MAMethod) returnVal).ordinal());
                        final Class indClass1 = indClass;
                        cmbMAMethod.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if (target != null) {
                                    byte val = (byte) cmbMAMethod.getSelectedIndex();
                                    MAMethod maMethod = MAMethod.values()[val];
                                    System.out.println("OHLCPriority combo stateChanged " + val);
                                    try {
                                        Class[] paramClasses = new Class[]{MAMethod.class};
                                        Method setMethod = indClass1.getMethod("set" + fieldName.substring(3), paramClasses);
                                        Object[] args = new Object[]{maMethod};
                                        setMethod.invoke(target, args);
                                        PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                                    } catch (NoSuchMethodException e1) {
                                        e1.printStackTrace();
                                    } catch (IllegalAccessException e1) {
                                        e1.printStackTrace();
                                    } catch (InvocationTargetException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });
                        numberOfParams++;
                    } else if (field.getType().equals(CopCurvePeriod.class)) {
                        String lblParamName = isStrategyPattern ? getApprpriateFieldName(fieldName.substring(3)) : fieldName.substring(3);
                        GraphLabel lblPrice = new GraphLabel(lblParamName);
                        //lblPrice.setBounds(LEFT, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        //panel.setPreferredSize(new Dimension(panel.getWidth(), 10 + i * (10 + CMB_HEIGHT) + CMB_HEIGHT));
                        //panel.repaint();
                        //panel.revalidate();
                        panel.add(lblPrice);
                        //final JComboBox cmbMAMethod = getMAMethodCombo(saMAMethods, LEFT + 150, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        final JComboBox cmbCCPeriod = getCCPeriodCombo(saCCPeriods);
                        panel.add(cmbCCPeriod);
                        Method getMethod = indClass.getMethod("get" + fieldName.substring(3));
                        Object[] args = new Object[]{};
                        Object returnVal = getMethod.invoke(origin, args);
                        cmbCCPeriod.setSelectedIndex(((CopCurvePeriod) returnVal).ordinal());
                        final Class indClass1 = indClass;
                        cmbCCPeriod.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if (target != null) {
                                    byte val = (byte) cmbCCPeriod.getSelectedIndex();
                                    CopCurvePeriod ccPeriod = CopCurvePeriod.values()[val];
                                    System.out.println("CoppockCurve combo stateChanged " + val);
                                    try {
                                        Class[] paramClasses = new Class[]{CopCurvePeriod.class};
                                        Method setMethod = indClass1.getMethod("set" + fieldName.substring(3), paramClasses);
                                        Object[] args = new Object[]{ccPeriod};
                                        setMethod.invoke(target, args);
                                        PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                                    } catch (NoSuchMethodException e1) {
                                        e1.printStackTrace();
                                    } catch (IllegalAccessException e1) {
                                        e1.printStackTrace();
                                    } catch (InvocationTargetException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });
                        numberOfParams++;
                    } else if (field.getType().equals(CalcMethod.class)) {
                        String lblParamName = isStrategyPattern ? getApprpriateFieldName(fieldName.substring(3)) : fieldName.substring(3);
                        GraphLabel lblPrice = new GraphLabel(lblParamName);
                        //lblPrice.setBounds(LEFT, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        //panel.setPreferredSize(new Dimension(panel.getWidth(), 10 + i * (10 + CMB_HEIGHT) + CMB_HEIGHT));
                        //panel.repaint();
                        //panel.revalidate();
                        panel.add(lblPrice);
                        //final JComboBox cmbCaclMethod = getCalcMethodCombo(saCalcMethod, LEFT + 150, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        final JComboBox cmbCaclMethod = getCalcMethodCombo(saCalcMethod);
                        panel.add(cmbCaclMethod);
                        Method getMethod = indClass.getMethod("get" + fieldName.substring(3));
                        Object[] args = new Object[]{};
                        Object returnVal = getMethod.invoke(origin, args);
                        cmbCaclMethod.setSelectedIndex(((CalcMethod) returnVal).ordinal());
                        final Class indClass1 = indClass;
                        cmbCaclMethod.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if (target != null) {
                                    byte val = (byte) cmbCaclMethod.getSelectedIndex();
                                    CalcMethod calcMethod = CalcMethod.values()[val];
                                    System.out.println("OHLCPriority combo stateChanged " + val);
                                    try {
                                        Class[] paramClasses = new Class[]{CalcMethod.class};
                                        Method setMethod = indClass1.getMethod("set" + fieldName.substring(3), paramClasses);
                                        Object[] args = new Object[]{calcMethod};
                                        setMethod.invoke(target, args);
                                        PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                                    } catch (NoSuchMethodException e1) {
                                        e1.printStackTrace();
                                    } catch (IllegalAccessException e1) {
                                        e1.printStackTrace();
                                    } catch (InvocationTargetException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });
                        numberOfParams++;
                    } else if (field.getType().equals(Double.TYPE)) {
                        String lblParamName = isStrategyPattern ? getApprpriateFieldName(fieldName.substring(3)) : fieldName.substring(3);
                        GraphLabel lblDouble = new GraphLabel(lblParamName);
                        //lblDouble.setBounds(LEFT, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH , CMB_HEIGHT);
                        //panel.setPreferredSize(new Dimension(panel.getWidth(), 10 + i * (10 + CMB_HEIGHT) + CMB_HEIGHT));
                        //panel.repaint();
                        //panel.revalidate();
                        panel.add(lblDouble);
                        final JTextField txtDouble = new JTextField("");
                        txtDouble.setDocument(new ValueFormatter(ValueFormatter.DECIMAL, 8, 4));
                        //txtDouble.setBounds(LEFT + 150, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        panel.add(txtDouble);
                        Method getMethod = indClass.getMethod("get" + fieldName.substring(3));
                        Object[] args = new Object[]{};
                        Object returnVal = getMethod.invoke(origin, args);
                        TWDecimalFormat formatter = new TWDecimalFormat("###########0.00");
                        //txtDouble.setText(Double.toString((Double) returnVal));
                        txtDouble.setText(formatter.format((Double) returnVal));
                        final Class indClass1 = indClass;
                        txtDouble.addCaretListener(new CaretListener() {
                            public void caretUpdate(CaretEvent e) {
                                if (target != null) {
                                    double val = Double.parseDouble(txtDouble.getText().trim());
                                    System.out.println("double txtField stateChanged " + val);
                                    try {
                                        Class[] paramClasses = new Class[]{Double.TYPE};
                                        Method setMethod = indClass1.getMethod("set" + fieldName.substring(3), paramClasses);
                                        Object[] args = new Object[]{val};
                                        setMethod.invoke(target, args);
                                        PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                                    } catch (NoSuchMethodException e1) {
                                        e1.printStackTrace();
                                    } catch (IllegalAccessException e1) {
                                        e1.printStackTrace();
                                    } catch (InvocationTargetException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });
                        numberOfParams++;
                    } else if (field.getType().equals(Boolean.TYPE)) {
                        String lblParamName = isStrategyPattern ? getApprpriateFieldName(fieldName.substring(3)) : fieldName.substring(3);
                        GraphLabel lblBoolean = new GraphLabel(lblParamName);
                        //lblBoolean.setBounds(LEFT, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        //panel.setPreferredSize(new Dimension(panel.getWidth(), 10 + i * (10 + CMB_HEIGHT) + CMB_HEIGHT));
                        //panel.repaint();
                        //panel.revalidate();
                        panel.add(lblBoolean);
                        //final JComboBox cmbBoolean = getBooleanCombo(LEFT + 150, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        final JComboBox cmbBoolean = getBooleanCombo(LEFT + 150, 10 + i * (10 + CMB_HEIGHT), ITEM_WIDTH, CMB_HEIGHT);
                        panel.add(cmbBoolean);
                        Method getMethod = indClass.getMethod("get" + fieldName.substring(3));
                        Object[] args = new Object[]{};
                        Object returnVal = getMethod.invoke(origin, args);
                        cmbBoolean.setSelectedItem(returnVal);
                        final Class indClass1 = indClass;
                        cmbBoolean.addItemListener(new ItemListener() {
                            public void itemStateChanged(ItemEvent e) {
                                if (target != null) {
                                    boolean val = (Boolean) cmbBoolean.getSelectedItem();
                                    System.out.println("boolean combo stateChanged " + val);
                                    try {
                                        Class[] paramClasses = new Class[]{Boolean.TYPE};
                                        Method setMethod = indClass1.getMethod("set" + fieldName.substring(3), paramClasses);
                                        Object[] args = new Object[]{val};
                                        setMethod.invoke(target, args);
                                        PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                                    } catch (NoSuchMethodException e1) {
                                        e1.printStackTrace();
                                    } catch (IllegalAccessException e1) {
                                        e1.printStackTrace();
                                    } catch (InvocationTargetException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });
                        numberOfParams++;
                    }
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } /*catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }*/
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setSize(DIM_DIALOG);
        scrollPane.getViewport().add(panel);

        //return scrollPane;
        return panel;
    }

    public JPanel createGeneralPanel(final ChartProperties origin, final ChartProperties target, final StockGraph graph) {
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"30%", "65%"}, 10, 5));
        panel.setSize(DIM_DIALOG);

        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"35%", "22%", "33%", "10%"}, new String[]{"20", "20"}, 2, 0));
        JPanel bottomPanel = new JPanel(new FlexGridLayout(new String[]{"55%", "2%", "43%"}, new String[]{"100%"}, 0, 0));

        JPanel pnlColor = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"65%", "30%"}, 0, 0));
        JPanel pnlTopColor = new JPanel(new FlexGridLayout(new String[]{"35%", "60%"}, new String[]{"20", "20"}, 0, 0));
        JPanel pnlBottomColor = new JPanel(new FlexGridLayout(new String[]{"80%"}, new String[]{"20"}, 0, 0));

        JPanel bottomRightPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "20", "20", "20", "20"}, 0, 3));

        pnlColor.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("CHART_COLOR"), TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));

        GraphLabel lblUpColor = new GraphLabel(Language.getString("UP"));
        JComboBox cmbUpColor = getColorCombo(origin, target, graph, METHOD_SET_COLOR, target.getColor(), LEFT + 40, 2 + CMB_HEIGHT, 100, CMB_HEIGHT);

        final GraphLabel lblDownColor = new GraphLabel(Language.getString("DOWN_COLON"));
        final JComboBox cmbDownColor = getColorCombo(origin, target, graph, METHOD_WARNING_COLOR, target.getWarningColor(), LEFT + 40, 3 + 2 * CMB_HEIGHT, 100, CMB_HEIGHT);

        final JCheckBox chkUseSameColor = new JCheckBox(Language.getString("USE_SAME_COLOR"));
        chkUseSameColor.setSelected(target.isUsingSameColor());
        if (target.isUsingSameColor()) {
            lblDownColor.setEnabled(false);
            cmbDownColor.setEnabled(false);
        }
        chkUseSameColor.setFont(Theme.getDefaultFont(Font.PLAIN, 12));
        chkUseSameColor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean useSame = chkUseSameColor.isSelected();
                target.setUseSameColor(useSame);
                lblDownColor.setEnabled(!useSame);
                cmbDownColor.setEnabled(!useSame);
                PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
            }
        });

        GraphLabel lblLineStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        JComboBox cmbLineStyle = getPenStyleCombo(origin, target, graph, GAP + 170, VADJ + 4 + 3 * CMB_HEIGHT, 130, CMB_HEIGHT);

        GraphLabel lblLineWeight = new GraphLabel(Language.getString("LINE_WEIGHT"));
        JComboBox cmbLineThickness = getPenWidthCombo(origin, target, graph, METHOD_SET_WIDTH,
                target.getPenWidth(), GAP + 170, VADJ + 6 + 5 * CMB_HEIGHT, 130, CMB_HEIGHT);

        GraphLabel lblStyle = new GraphLabel(Language.getString("CHART_STYLE"));
        JComponent[] ca = {lblLineStyle, cmbLineStyle, lblLineWeight, cmbLineThickness};
        JComboBox cmbChartStyle = getGraphStyleCombo(origin, target, graph, ca, LEFT, CMB_HEIGHT + 2, 100, CMB_HEIGHT);

        topPanel.setLayout((new FlexGridLayout(new String[]{"35%", "65%"}, new String[]{"20", "20"}, 0, 0)));
        topPanel.add(lblStyle);
        topPanel.add(new JLabel(""));
        topPanel.add(cmbChartStyle);
        topPanel.add(new JLabel(""));
        pnlTopColor.add(lblUpColor);
        pnlTopColor.add(cmbUpColor);
        pnlTopColor.add(lblDownColor);
        pnlTopColor.add(cmbDownColor);
        pnlBottomColor.add(chkUseSameColor);
        pnlColor.add(pnlTopColor);
        pnlColor.add(pnlBottomColor);
        bottomRightPanel.add(new JLabel(""));
        bottomRightPanel.add(lblLineStyle);
        bottomRightPanel.add(cmbLineStyle);
        bottomRightPanel.add(lblLineWeight);
        bottomRightPanel.add(cmbLineThickness);
        bottomPanel.add(pnlColor);
        bottomPanel.add(new JPanel());
        bottomPanel.add(bottomRightPanel);

        panel.add(topPanel);
        panel.add(bottomPanel);

        return panel;
    }

    private JComboBox getPenStyleCombo(final ChartProperties origin, final ChartProperties target,
                                       final StockGraph graph, int x, int y, int w, int h) {
        final String method = METHOD_SET_STYLE;
        byte penStyle = target.getPenStyle();
        JComboBox cmbLineStyle = new JComboBox();
        cmbLineStyle.setBounds(x, y, w, h);
        cmbLineStyle.setEditable(false);
        cmbLineStyle.setRenderer(new ComboRenderer());
        cmbLineStyle.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        //int penWidth = getEquivalentWidth(target.getPenWidth());
        for (int i = 0; i < lineStyles.length; i++) {
            ci = new ComboItem(lineStyles[i], lineStyles[i], penStyles[i], ComboItem.ITEM_NORMAL);
            cmbLineStyle.addItem(ci);
            if (i == penStyle) {
                cmbLineStyle.setSelectedItem(ci);
            }
        }
        cmbLineStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    if ((method.equals(METHOD_SET_STYLE))) {
                        target.setPenStyle((byte) cmb.getSelectedIndex());
                    }
                    PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        return cmbLineStyle;
    }

    private JComboBox getColorCombo(final ChartProperties origin, final ChartProperties target,
                                    final StockGraph graph, final String method, Color oldColor,
                                    int x, int y, int w, int h) {
        JComboBox cmbColor = new JComboBox();
        cmbColor.setBounds(x, y, w, h);
        cmbColor.setEditable(false);
        cmbColor.setRenderer(new ComboRenderer());
        cmbColor.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        boolean customColor = true;
        for (int i = 0; i < colors.length; i++) {
            ci = new ComboItem(colorNames[i], colors[i], new ComboImage(colors[i]), ComboItem.ITEM_NORMAL);
            cmbColor.addItem(ci);
            if (colors[i].equals(oldColor)) {
                cmbColor.setSelectedIndex(i);
                customColor = false;
            }
        }
        ci = new ComboItem(Language.getString("CUSTOM"), oldColor, new ComboImage(oldColor), ComboItem.ITEM_CUSTOM_COLOR);
        cmbColor.addItem(ci);
        if (customColor) {
            cmbColor.setSelectedItem(ci);
        }
        cmbColor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    try {
                        Color color = (Color) ((ComboItem) cmb.getSelectedItem()).getValue();
                        if (method.equals(METHOD_SET_COLOR)) {
                            target.setColor(color);
                        } else if (method.equals(METHOD_WARNING_COLOR)) {
                            target.setWarningColor(color);
                        }
                        PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                    } catch (Exception ex) {
                    }
                }
            }
        });
        return cmbColor;
    }

    private JComboBox getPenWidthCombo(final ChartProperties origin, final ChartProperties target,
                                       final StockGraph graph, final String method, float penWidth,
                                       int x, int y, int w, int h) {
        JComboBox cmbLineWidth = new JComboBox();
        cmbLineWidth.setBounds(x, y, w, h);
        cmbLineWidth.setEditable(false);
        cmbLineWidth.setRenderer(new ComboRenderer());
        cmbLineWidth.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        int equiW = getEquivalentWidth(penWidth);
        for (int i = 0; i < lineWidths.length; i++) {
            ci = new ComboItem(lineWidths[i], lineWidths[i], penWidths[i], ComboItem.ITEM_NORMAL);
            cmbLineWidth.addItem(ci);
            if (equiW == i) {
                cmbLineWidth.setSelectedItem(ci);
            }
        }
        cmbLineWidth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    Object obj = ((ComboItem) cmb.getSelectedItem()).getValue();
                    if (method.equals(METHOD_SET_WIDTH)) {
                        target.setPenWidth(Float.parseFloat((String) obj));
                    }
                    PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        return cmbLineWidth;
    }

    private JComboBox getGraphStyleCombo(final ChartProperties origin, final ChartProperties target,
                                         final StockGraph graph, final JComponent[] subComps, int x, int y, int w, int h) {
        JComboBox cmbStyle = new JComboBox();
        cmbStyle.setBounds(x, y, w, h);
        cmbStyle.setEditable(false);
        String[] sa = GraphFrame.getGraphStyles();
        Icon[] ia = GraphToolBar.styleIcon;
        cmbStyle.setRenderer(new ComboRenderer());
        cmbStyle.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        int breakIndex = 0; //changed by sathyajith
//        int breakIndex = 1; //changed by sathyajith

        for (int i = 0; i < sa.length; i++) {
            ci = new ComboItem(sa[i], sa[i], ia[i], ComboItem.ITEM_NORMAL);
            cmbStyle.addItem(ci);
            if (i == target.getChartStyle()) {
                cmbStyle.setSelectedIndex(i);
            }
            if (i >= breakIndex) break;
        }
        if (cmbStyle.getSelectedIndex() == StockGraph.INT_GRAPH_STYLE_LINE) {
            setCompsEnabled(subComps, true);
        } else {
            setCompsEnabled(subComps, false);
        }
        cmbStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (target != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    if (cmb.getSelectedIndex() >= 0) {
                        target.setChartStyle((byte) cmb.getSelectedIndex());
                    }
                    if (cmb.getSelectedIndex() == StockGraph.INT_GRAPH_STYLE_LINE) {
                        setCompsEnabled(subComps, true);
                    } else {
                        setCompsEnabled(subComps, false);
                    }
                    PropertyDialogFactory.dynamicChangeChartProperties(origin, target, graph);
                }
            }
        });
        return cmbStyle;
    }

    private void setCompsEnabled(JComponent[] ca, boolean enabled) {
        for (JComponent aCa : ca) {
            aCa.setEnabled(enabled);
        }
    }

    private int getEquivalentWidth(float pw) {
        if (pw <= 1.1f) {
            return 0;
        } else if (pw <= 1.6f) {
            return 1;
        } else if (pw <= 2.1f) {
            return 2;
        } else if (pw <= 3.1f) {
            return 3;
        } else return 4;
    }

    private JComboBox getMAMethodCombo(String[] saMethods, int x, int y, int w, int h) {
        JComboBox cmbMethod = new JComboBox();
        cmbMethod.setBounds(x, y, w, h);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        for (int i = 0; i < saMethods.length; i++) {
            cmbMethod.addItem(saMethods[i]);
        }

        return cmbMethod;
    }

    private JComboBox getMAMethodCombo(String[] saMethods) {
        JComboBox cmbMethod = new JComboBox();
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        for (int i = 0; i < saMethods.length; i++) {
            cmbMethod.addItem(saMethods[i]);
        }

        return cmbMethod;
    }

    private JComboBox getCCPeriodCombo(String[] ccPeriods) {
        JComboBox cmbCCPeriod = new JComboBox();
        cmbCCPeriod.setEditable(false);
        cmbCCPeriod.setFont(StockGraph.font_BOLD_10);
        for (int i = 0; i < ccPeriods.length; i++) {
            cmbCCPeriod.addItem(ccPeriods[i]);
        }

        return cmbCCPeriod;
    }

    private JComboBox getCalcMethodCombo(String[] saMethods, int x, int y, int w, int h) {
        JComboBox cmbMethod = new JComboBox();
        cmbMethod.setBounds(x, y, w, h);
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        for (int i = 0; i < saMethods.length; i++) {
            cmbMethod.addItem(saMethods[i]);
        }

        return cmbMethod;
    }

    private JComboBox getCalcMethodCombo(String[] saMethods) {
        JComboBox cmbMethod = new JComboBox();
        cmbMethod.setEditable(false);
        cmbMethod.setFont(StockGraph.font_BOLD_10);
        for (int i = 0; i < saMethods.length; i++) {
            cmbMethod.addItem(saMethods[i]);
        }

        return cmbMethod;
    }


    private JComboBox getBooleanCombo(int x, int y, int w, int h) {
        JComboBox cmbPrio = new JComboBox();
        //cmbPrio.setBounds(x, y, w, h);
        cmbPrio.setEditable(false);
        cmbPrio.setFont(StockGraph.font_BOLD_10);
        cmbPrio.addItem(true);
        cmbPrio.addItem(false);
        cmbPrio.setSelectedIndex(0);
        return cmbPrio;
    }

    public String getApprpriateFieldName(String planeText) {
        return Language.getString("CI_" + planeText.toUpperCase());
//        return planeText; //todo this is a tempory solution
    }
}

//better if could inform  the importancy of the releases and release dates little bit in Advance which Will defineately help  to prioritize tasks.