package com.isi.csvr.chart;

import com.isi.csvr.chart.cashflowcharts.NetOrdersInd;
import com.isi.csvr.chart.cashflowcharts.NetTurnOverInd;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.chartobjects.ObjectFibonacciExtensions;
import com.isi.csvr.chart.chartobjects.ObjectFibonacciRetracements;
import com.isi.csvr.chart.customindicators.IndicatorMenuObject;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.tabbedpane.TWTabbedPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class PropertyDialog extends JDialog implements PropertyManager,
        ActionListener {

    public static byte DIALOG_RESULT_OK = 0;
    public static byte DIALOG_RESULT_CANCEL = 1;
    private byte dialogResult = DIALOG_RESULT_CANCEL;
    public static byte DIALOG_RESULT_RESET = 2;
    protected ChartProperties targetCP = null;
    protected AbstractObject targetOR = null;
    protected TWTabbedPane tabPane = null;
    private boolean ModalResult = false;
    private JPanel bottomPanel = new JPanel();
    private TWButton btnOK = new TWButton(Language.getString("OK"));
    private TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
    private TWButton btnReset = new TWButton(Language.getString("RESET"));
    private JCheckBox chkSetDefault = new JCheckBox(Language.getString("SET_AS_DEFAULT2"));
    private StockGraph graph;
    private ChartProperties tempCP;
    private AbstractObject tempOR;
    private Hashtable tempHash;

    //for graphs ,indicators
    public PropertyDialog(JFrame parent, ChartProperties cp, int tabIndex, StockGraph graph, boolean showSources) {
        super(parent);
        setResizable(false);

        tempCP = getRelevantTargetCP(cp);
        tempCP.assignValuesFrom(cp);
        this.graph = graph;

        targetCP = getRelevantTargetCP(cp);
        targetCP.assignValuesFrom(cp);
        tabPane = PropertyDialogFactory.createTabbedPane(cp.getID(), cp, targetCP, tabIndex, graph, showSources);
        initiallizeUI(false);
        this.setTitle(Language.getString("SET_PROPERTIES") + " " + targetCP.getShortName());
        this.setSize(PropertyDialogFactory.DIM_DIALOG);
        GUISettings.applyOrientation(this);
    }

    //for objects
    public PropertyDialog(JFrame owner, AbstractObject or, StockGraph graph) {
        super(owner);
        setResizable(false);

        tempOR = AbstractObject.CreateObject(or.getObjType(), null, null, or.getIndexArray(), null, null, null, graph);
        tempOR.assignValuesFrom(or);
        this.graph = graph;

        targetOR = AbstractObject.CreateObject(or.getObjType(), null, null, or.getIndexArray(), null, null, null, graph);
        targetOR.assignValuesFrom(or);
        tabPane = PropertyDialogFactory.createTabbedPane(or, targetOR, graph);
        initiallizeUI(true);
        this.setTitle(Language.getString("SET_PROPERTIES") + " " + SharedMethods.getTextFromHTML(targetOR.getShortName()));

        if (or.getObjType() == AbstractObject.INT_FIBONACCI_RETRACEMENTS) {
            ObjectFibonacciRetracements fibonacciRetracements = (ObjectFibonacciRetracements) or;
            tempHash = fibonacciRetracements.getFibonacciLines();
            this.setSize(PropertyDialogFactory.DIM_FIB_RETR_DIALOG);
        } else if (or.getObjType() == AbstractObject.INT_FIBONACCI_EXTENSIONS) {
            ObjectFibonacciExtensions fibonacciRetracements = (ObjectFibonacciExtensions) or;
            tempHash = fibonacciRetracements.getDefaultFibonacciExtensionLines();
            this.setSize(PropertyDialogFactory.DIM_FIB_EXT_DIALOG);
        } else { //default
            this.setSize(PropertyDialogFactory.DIM_DIALOG);
        }

        //TODO: need to do for all objects
        /*if (or instanceof ObjectLineSlope || or instanceof ObjectArrowLineSlope || or instanceof ObjectLineHorizontal || or instanceof ObjectLineVertical) {
            GUISettings.applyOrientation(this);
        }*/

        GUISettings.applyOrientation(this);
    }

    private ChartProperties getRelevantTargetCP(ChartProperties cp) {
        byte Indicator_ID = cp.getID();
        switch (Indicator_ID) {
            case GraphDataManager.ID_MOVING_AVERAGE:
                return new MovingAverage(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_MACD:
                MACD macd = new MACD(null, null, Indicator_ID, Color.red, null);
                MovingAverage ma = new MovingAverage(null, null, GraphDataManager.ID_MOVING_AVERAGE, Color.DARK_GRAY, null);
                ma.setPenStyle(GraphDataManager.STYLE_DASH);
                macd.setSignalLine(ma);
                return macd;
            case GraphDataManager.ID_RSI:
                return new RSI(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_MOMENTUM:
                return new Momentum(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_LRI:
                return new LinearRegression(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_SDI:
                return new StandardDeviation(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_BOLLINGER_BANDS:
                return new BollingerBand(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ACCU_DISTRI:
                return new AccumulationDistribution(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_WILLS_R:
                return new WilliamsR(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_STOCHAST_OSCILL:
                return new StocasticOscillator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_AVG_TRUE_RANGE:
                return new AverageTrueRange(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_MASS_INDEX:
                return new MassIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_MEDIAN_PRICE:
                return new MedianPrice(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ALLIGATOR:
                return new Alligator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_MONEY_FLOW_INDEX:
                return new MoneyFlowIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_TRIX:
                return new TRIX(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_VOLATILITY:
                return new Volatility(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ZIGZAG:
                return new ZigZag(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_AROON:
                return new Aroon(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_CHAIKINS_OSCILL:
                return new ChaikinOscillator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_CCI:
                return new CommodityChannelIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_CHAIKIN_MONEY_FLOW:
                return new ChaikinMoneyFlow(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_EASE_OF_MOVEMENT:
                return new EaseOfMovement(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ICHIMOKU_KINKO_HYO:
                return new IchimokuKinkoHyo(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_KLINGER_OSCILL:
                KlingerOscillator ko = new KlingerOscillator(null, null, Indicator_ID, Color.red, null);
                MovingAverage maKO = new MovingAverage(null, null, GraphDataManager.ID_MOVING_AVERAGE, Color.DARK_GRAY, null);
                maKO.setPenStyle(GraphDataManager.STYLE_DASH);
                ko.setSignalLine(maKO);
                return ko;
            //return new KlingerOscillator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_PARABOLIC_SAR:
                return new ParabolicSAR(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_PRICE_VOLUME_TREND:
                return new PriceVolumeTrend(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_VOLUME_OSCILL:
                return new VolumeOscillator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_WILLS_ACCU_DISTRI:
                return new WillsAccumDistribution(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_RMI:
                return new RelativeMomentumIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ENVILOPES:
                return new Envelope(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_PRICE_OSCILL:
                return new PriceOscillator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_INTRADAY_MOMENTUM_INDEX:
                return new IntradayMomentumIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ON_BALANCE_VOLUME:
                return new OnBalanceVolume(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ROC:
                return new PriceROC(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_WILDERS_SMOOTHING:
                return new WildersSmoothing(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_PLUS_DI:
                return new PlusDI(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_MINUS_DI:
                return new MinusDI(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_DX:
                return new DX(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ADX:
                return new ADX(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_ADXR:
                return new ADXR(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_DPO:
                return new DetrendPriceOscillator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_SWING_INDEX:
                return new SwingIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_RVI:
                return new RelativeVolatilityIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_QSTICK:
                return new Qstick(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_VERT_HORI_FILTER:
                return new VerticalHorizontalFilter(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_VOLUME_ROC:
                return new VolumeROC(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_DEMA:
                return new DEMA(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_COPP_CURVE:
                return new CoppockCurve(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_PRICE_CHANNELS:
                return new PriceChannel(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_TSF:
                return new TimeSeriesForcast(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_FO:
                return new ForcastOscillator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_CMO:
                return new ChandeMomentumOscillator(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_SMO:
                return new StochasticMomentumIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_TEMA:
                return new TEMA(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_MFI:
                return new MarketFacilitationIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_NVI:
                return new NegativeVolumeIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_PERFORMANCE:
                return new Performance(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_PVI:
                return new PositiveVolumeIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_TYPICAL_PRICE:
                return new TypicalPrice(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_WEIGHTED_CLOSE:
                return new WeightedClose(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_STD_ERR:
                return new StandardError(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_MACD_VOLUME:
                MACDVolume macdVolume = new MACDVolume(null, null, Indicator_ID, Color.red, null);
                MovingAverage maMacdVolume = new MovingAverage(null, null, GraphDataManager.ID_MOVING_AVERAGE, Color.DARK_GRAY, null);
                maMacdVolume.setPenStyle(GraphDataManager.STYLE_DASH);
                macdVolume.setSignalLine(maMacdVolume);
                return macdVolume;
            case GraphDataManager.ID_NET_ORDERS:
                return new NetOrdersInd(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_NET_TURNOVER:
                return new NetTurnOverInd(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_DISPARITY_INDEX:
                return new DisparityIndex(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_HULL_MOVING_AVG:
                return new HullMovingAverage(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_KELTNER_CHANNELS:
                return new KeltnerChannels(null, null, Indicator_ID, Color.red, null);
            case GraphDataManager.ID_CUSTOM_INDICATOR:
                String longName = ((IndicatorBase) cp).getLongName();
                Class indClass = null;
                for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().customIndicators) {
                    if (imo.getIndicatorName().equals(longName)) {
                        indClass = imo.getIndicatorClass();
                    }
                }
                if (indClass == null) {
                    for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().alStrategies) {
                        if (imo.getIndicatorName().equals(longName)) {
                            indClass = imo.getIndicatorClass();
                        }
                    }
                }
                if (indClass == null) {
                    for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().alPatterns) {
                        if (imo.getIndicatorName().equals(longName)) {
                            indClass = imo.getIndicatorClass();
                        }
                    }
                }
                if (indClass != null) {
                    try {
                        Constructor constructor = indClass.getConstructor(ArrayList.class, String.class, Color.class, WindowPanel.class);
                        return (ChartProperties) constructor.newInstance(null, null, Color.blue, null);
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }

                return new IndicatorBase(null, null, Indicator_ID, Color.blue, null) {
                    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
                    }
                };
            default:
                return new ChartProperties(null, null, GraphDataManager.ID_BASE, Color.black, null);
        }
    }

    private void initiallizeUI(boolean isAbstractObject) {

        btnOK.addActionListener(this);
        btnCancel.addActionListener(this);
        btnReset.addActionListener(this);

        bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        bottomPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));


        if (getTargetCP() != null && getTargetCP().isIndicator && !getTargetCP().isSignalIndicator()) {
            chkSetDefault.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    getTargetCP().setUsingUserDefault(chkSetDefault.isSelected());
                    bottomPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
                }
            });
            bottomPanel.add(chkSetDefault);
        }
        bottomPanel.add(btnOK);
        if (getTargetCP() != null && getTargetCP().isIndicator && !getTargetCP().isSignalIndicator()) {
            bottomPanel.add(btnReset);
        } else if (getTargetOR() != null) {
            bottomPanel.add(btnReset);
        }
        bottomPanel.add(btnCancel);

        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(tabPane, BorderLayout.CENTER);
        this.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
        ModalResult = false;
    }

    public void actionPerformed(ActionEvent e) {
        //System.out.println("entering inside actionPerformed");
        Object obj = e.getSource();
        if (getTargetCP() != null && getTargetCP().isIndicator) {
            targetCP.isUsingUserDefault = chkSetDefault.isSelected();
        }
        ModalResult = false;
        if (obj == btnOK) {
            dialogResult = PropertyDialog.DIALOG_RESULT_OK;
            ModalResult = true;
            this.dispose();
        } else if (obj == btnCancel) {
            dialogResult = PropertyDialog.DIALOG_RESULT_CANCEL;
            ModalResult = false;
            this.dispose();
        } else if (obj == btnReset) {
            dialogResult = PropertyDialog.DIALOG_RESULT_RESET;
            this.dispose();

        }
        //System.out.println("hidden proprty dialog ModalResult "+ModalResult);
    }

    public boolean getModalResult() {
        return ModalResult;
    }

    public byte getModalDialgResult() {
        return dialogResult;
    }

    public ChartProperties getTargetCP() {
        return targetCP;
    }

    public void setTargetCP(ChartProperties aTarget) {
        targetCP = aTarget;
    }

    public AbstractObject getTargetOR() {
        return targetOR;
    }

    public void setTargetOR(AbstractObject aTarget) {
        targetOR = aTarget;
    }

    public ChartProperties getTempCP() {
        return tempCP;
    }

    public AbstractObject getTempOR() {
        return tempOR;
    }

    public Hashtable getTempHash() {
        return tempHash;
    }

    public TWTabbedPane getTabPane() {
        return tabPane;
    }
}
