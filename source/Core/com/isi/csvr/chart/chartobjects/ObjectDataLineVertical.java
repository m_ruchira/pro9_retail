package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.StockGraph;
import com.isi.csvr.chart.WindowPanel;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Sep 9, 2008
 * Time: 2:57:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectDataLineVertical extends AbstractObject {

    private static final long serialVersionUID = AbstractObject.UID_LINE_VERTI;
    SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");

    private ArrayList panels;

    public ObjectDataLineVertical() {
        super();
        objType = AbstractObject.INT_DATA_LINE_VERTI;
        showOnAllPanels = true;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
    }

    public ObjectDataLineVertical(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, ArrayList panels, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_DATA_LINE_VERTI;
        this.panels = panels;
        showOnAllPanels = true;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
        penStyle = GraphDataManager.STYLE_DASH;
        DISPLAY_TYPE = AbstractObject.DISPLAY_SATATIC;
        isShowingDragToolTip = false;
    }

    // extending LineVertical independently to draw on all windows ################################################

    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {

        super.drawDragImage(g, xArr, Xadj, Yadj, grfLeft, grfWidth, bIndex, grf_Top,
                grf_Ht, minY, xFactor, yFactor, false, null);
        for (Object panel : panels) {
            WindowPanel rect = (WindowPanel) panel;
            grfLeft = rect.x + StockGraph.clearance;
            int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            //g.drawLine(x1 - Xadj, rect.y, x1 - Xadj, rect.y + rect.height);
        }

    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);

        boolean drawSelected = selected && !isPrinting;
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel rect = (WindowPanel) panels.get(i);

            grfLeft = rect.x + StockGraph.clearance;
            int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            g.setClip(rect.x, rect.y, rect.width, rect.height);
            g.drawLine(x1, rect.y, x1, rect.y + rect.height);

          /*  String yValue = timeFormatter.format(new Date(xArray[0]));

            if (yValue.trim().endsWith("00:00")) {
                int index = yValue.lastIndexOf("-");
                yValue = yValue.substring(0, index);
            }
*/
            //drawing the date string in vertical direction
           /* if (i == 0) { // date should draw only for the top rectangle.
                int y1 = rect.y;
                g.translate(x1, y1);
                g.rotate(Math.toRadians(90));
                g.drawString(yValue, 15, y1 - 5);
                g.rotate(Math.toRadians(270));
                g.translate(-x1, -y1);
            }*/
            if (drawSelected) {
                g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                g.fillRect(x1 - halfBox, rect.y + rect.height / 2 - halfBox, halfBox * 2, halfBox * 2);
                g.drawRect(x1 - halfBox, rect.y + rect.height / 2 - halfBox, halfBox * 2, halfBox * 2);
            }
        }

    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel rect = (WindowPanel) panels.get(i);
            grfLeft = rect.x + StockGraph.clearance;
            int x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
            if (((x - x1) * (x - x1) < 4) && (rect.contains(x, y))) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                movingPtIndex = 0;
                this.rect = rect;
                return true;
            }
        }
        return false;
    }

    public String getShortName() {
        //return Language.getString("VERTICAL_LINE");
        return "";
    }

    protected String getValueString(boolean currMode) {
      /*  SimpleDateFormat timeFormatter;
        if (currMode) {
            timeFormatter = new SimpleDateFormat("HH:mm");
        } else {y");
        }
            timeFormatter = new SimpleDateFormat("dd/MM/y
        Date D1;
        D1 = new Date(xArray[0]);
        return timeFormatter.format(D1);*/
        return "";
    }

}
