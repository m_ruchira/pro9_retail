package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * User: Pramoda
 * Date: Aug 1, 2006
 * Time: 3:06:41 PM
 */
public class ObjectGannFan extends AbstractObject {

    public static float[] GANN_ANGLES = {8f, 4f, 3f, 2f, 0.5f, 1f / 3f, 0.25f, 0.125f};
    public static String[] ANGLE_STRINGS = {"1x8", "1x4", "1x3", "1x2", "2x1", "3x1", "4x1", "8x1"};
    public static String keyLineString = "1x1";
    TWDecimalFormat ganAngleFormat = new TWDecimalFormat("#############.00");
    private float[] indexArray;
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>();
    private SimpleDateFormat timeFormatter = null;

    public ObjectGannFan() {
        super();
        objType = INT_GANN_FAN;
    }

    public ObjectGannFan(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, null, cp, r, graph);
        objType = INT_GANN_FAN;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
        drawingOnBackground = false;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
        }
    }

    protected void drawDragImage(Graphics gg, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        int x1, x2, y1, y2;
        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        x2 = x2 - Xadj;
        y2 = y2 - (int) Yadj;
        if (objectMoving) {
            x1 = x1 - Xadj;
            y1 = y1 - (int) Yadj;
        }
        Graphics2D g = (Graphics2D) gg;
        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);
        int tempX2, tempY2;
        if ((x1 == x2)) {
            tempX2 = x2;
            tempY2 = (y1 > y2) ? rect.y : rect.y + rect.height; //what is this rect ?
            g.drawLine(x1, y1, tempX2, tempY2);
        } else {
            float ratio = (float) (y2 - y1) / (float) (x2 - x1);
            int pixGap = (x2 > x1) ? rect.width : -rect.width;
            tempX2 = x1 + pixGap;
            tempY2 = y1 + Math.round(ratio * pixGap);
            g.drawLine(x1, y1, tempX2, tempY2);
            if (ratio != 0) {
                for (int i = 0; i < GANN_ANGLES.length; i++) {
                    tempY2 = y1 + Math.round(ratio * GANN_ANGLES[i] * pixGap); //need to sjhow the angle inthe drag image as well?
                    //draw String here
                    g.drawLine(x1, y1, tempX2, tempY2);
                }
            }
        }

        //constructing tool tip data
        dataRows.clear();

        if (graph.isCurrentMode()) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }

        float index1 = bIndex + (x1 - grfLeft) / xFactor;
        long[] newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1});
        Date D1 = new Date(newXarray[0]);
        String beginDate = timeFormatter.format(D1);

        double begin = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
        String beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));

        dataRows.add(new ToolTipDataRow(Language.getString("DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_TOOL_TIP_VALUE"), beginPrice));

    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        int x1, x2, y1, y2;
        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect.x, rect.y, rect.width, rect.height);

        boolean drawSelected = selected && !isPrinting;
        int tempX2, tempY2;
        if ((x1 == x2)) {
            tempX2 = x2;
            tempY2 = (y1 > y2) ? rect.y : rect.y + rect.height;
            g.drawLine(x1, y1, tempX2, tempY2);
        } else {
            float ratio = (float) (y2 - y1) / (float) (x2 - x1);
            //int pixGap = (x2 > x1) ? extendX : -extendX;
            int pixGap = (x2 > x1) ? rect.width : -rect.width;
            tempX2 = x1 + pixGap;
            tempY2 = y1 + Math.round(ratio * pixGap);
            g.drawLine(x1, y1, tempX2, tempY2);


            int grfRight = grfLeft + grfWidth;
            int grfDown = grf_Top + grf_Ht;

            float ratioUpRightDiagnal = (float) (grf_Top - y1) / (float) (grfRight - x1);
            float ratioDownRightDiagnal = (float) (grfDown - y1) / (float) (grfRight - x1);
            float ratioUpLeftDiagnal = (float) (grf_Top - y1) / (float) (grfLeft - x1);
            float ratioDownLefttDiagnal = (float) (grfDown - y1) / (float) (grfLeft - x1);

            if (ratio != 0) {
                for (int i = 0; i < GANN_ANGLES.length; i++) {
                    tempY2 = y1 + Math.round(ratio * GANN_ANGLES[i] * pixGap);
                    String fanAngle = ANGLE_STRINGS[i];
                    g.setColor(Color.blue); //TODO: use a themable color
                    Font axisFont = new Font("Arial", Font.ITALIC, 11);
                    g.setFont(axisFont);
                    float gannAngleRatio = -ratio * GANN_ANGLES[i];
                    if (i > 3) { //small  angles
                        if (Math.toDegrees(Math.atan(-ratio)) < 90 && Math.toDegrees(Math.atan(-ratio)) > 0) {
                            if (y1 > y2) {
                                if (Math.toDegrees(gannAngleRatio) < Math.toDegrees(-ratioUpRightDiagnal)) {
                                    int edgeY = (int) (((float) (grfRight - x1)) * gannAngleRatio);
                                    g.drawString(fanAngle, grfRight - 18, y1 - edgeY);
                                } else {
                                    int edgX = (int) (((float) -(grf_Top - y1)) / gannAngleRatio);
                                    g.drawString(fanAngle, edgX + x1, grf_Top + 5);
                                }
                            } else {
                                if (Math.toDegrees(gannAngleRatio) < Math.toDegrees(-ratioDownLefttDiagnal)) { //left Y
                                    int edgeY = (int) (((float) -(grfLeft - x1)) * gannAngleRatio);
                                    g.drawString(fanAngle, grfLeft, y1 + edgeY);
                                } else {       //down left x
                                    int edgX = (int) (((float) -(grfDown - y1)) / gannAngleRatio);
                                    g.drawString(fanAngle, edgX + x1, grfDown - 3);
                                }
                            }
                        } else if (Math.toDegrees(Math.atan(-ratio)) < 0 && Math.toDegrees(Math.atan(-ratio)) > -90) { //downLeft
                            if (x2 < x1) { //0 < theta< 180
                                if (Math.toDegrees(Math.atan(gannAngleRatio)) < Math.toDegrees(Math.atan(-ratioUpLeftDiagnal))) {  //top
                                    int edgex = (int) (((float) -(grf_Top - y1)) / gannAngleRatio);
                                    g.drawString(fanAngle, x1 + edgex, grf_Top + 5);
                                } else { //  lefty
                                    int edgey = (int) (((float) (grfLeft - x1)) * gannAngleRatio);
                                    g.drawString(fanAngle, grfLeft, y1 - edgey);
                                }
                            } else {
                                if (Math.toDegrees(Math.atan(gannAngleRatio)) < Math.toDegrees(Math.atan(-ratioDownRightDiagnal))) {  //down right edge
                                    int edgex = (int) (((float) -(grfDown - y1)) / gannAngleRatio);
                                    g.drawString(fanAngle, x1 + edgex, grfDown - 3);
                                } else { // right y edge
                                    int edgY = (int) (((float) -(grfRight - x1)) * gannAngleRatio); //ok
                                    g.drawString(fanAngle, grfRight - 20, y1 + edgY);
                                }
                            }
                        }
                    } else { //big  angles
                        if (Math.toDegrees(Math.atan(-ratio)) < 90 && Math.toDegrees(Math.atan(-ratio)) > 0) {
                            if (y1 > y2) {
                                if (Math.toDegrees(gannAngleRatio) < Math.toDegrees(-ratioUpRightDiagnal)) {
                                    int edgeY = (int) (((float) (grfRight - x1)) * gannAngleRatio);
                                    g.drawString(fanAngle, grfRight - 20, y1 - edgeY);
                                } else {
                                    int edgX = (int) (((float) -(grf_Top - y1)) / gannAngleRatio);
                                    g.drawString(fanAngle, edgX + x1, grf_Top + 5);
                                }
                            } else {   //3rd radian
                                if (Math.toDegrees(gannAngleRatio) < Math.toDegrees(-ratioDownLefttDiagnal)) { //left Y
                                    int edgeY = (int) (((float) -(grfLeft - x1)) * gannAngleRatio);
                                    g.drawString(fanAngle, grfLeft, y1 + edgeY);
                                } else {       //down left x
                                    int edgX = (int) (((float) -(grfDown - y1)) / gannAngleRatio);
                                    g.drawString(fanAngle, edgX + x1, grfDown - 3);
                                }
                            }
                        } else if (Math.toDegrees(Math.atan(-ratio)) < 0 && Math.toDegrees(Math.atan(-ratio)) > -90) { //down left
                            if (x2 < x1) {
                                if (Math.toDegrees(Math.atan(gannAngleRatio)) < Math.toDegrees(Math.atan(-ratioUpLeftDiagnal))) {  //top along -  left
                                    int edgex = (int) (((float) -(grf_Top - y1)) / gannAngleRatio);
                                    g.drawString(fanAngle, x1 + edgex, grf_Top + 5);
                                } else { //  lefty
                                    int edgey = (int) (((float) (grfLeft - x1)) * gannAngleRatio);
                                    g.drawString(fanAngle, grfLeft, y1 - edgey);
                                }
                            } else {
                                if (Math.toDegrees(Math.atan(gannAngleRatio)) < Math.toDegrees(Math.atan(-ratioDownRightDiagnal))) { //down right edge
                                    int edgex = (int) (((float) -(grfDown - y1)) / gannAngleRatio);
                                    g.drawString(fanAngle, x1 + edgex, grfDown - 3);
                                } else {
                                    int edgY = (int) (((float) -(grfRight - x1)) * gannAngleRatio);//ok
                                    g.drawString(fanAngle, grfRight - 20, y1 + edgY);
                                }
                            }

                        }
                    }

                    g.setColor(color);
                    g.drawLine(x1, y1, tempX2, tempY2);
                }


                // ***********************the key line ******************/
                if (Math.toDegrees(Math.atan(-ratio)) < 90 && Math.toDegrees(Math.atan(-ratio)) > 0) {

                    if (y1 > y2) {
                        if (Math.toDegrees(-ratio) < Math.toDegrees(-ratioUpRightDiagnal)) {
                            int edgeY = (int) (((float) (grfRight - x1)) * (-ratio));
                            g.drawString(keyLineString, grfRight - 18, y1 - edgeY);
                        } else {
                            int edgX = (int) (((float) -(grf_Top - y1)) / (-ratio));
                            g.drawString(keyLineString, edgX + x1, grf_Top + 5);
                        }
                    } else {
                        if (Math.toDegrees(-ratio) < Math.toDegrees(-ratioDownLefttDiagnal)) { //left Y
                            int edgeY = (int) (((float) -(grfLeft - x1)) * (-ratio));
                            g.drawString(keyLineString, grfLeft, y1 + edgeY);
                        } else {       //down left x
                            int edgX = (int) (((float) -(grfDown - y1)) / (-ratio));
                            g.drawString(keyLineString, edgX + x1, grfDown - 3);
                        }
                    }


                } else if (Math.toDegrees(Math.atan(-ratio)) < 0 && Math.toDegrees(Math.atan(-ratio)) > -90) { //downLeft
                    if (x2 < x1) { //0 < theta< 180

                        if (Math.toDegrees(Math.atan(-ratio)) < Math.toDegrees(Math.atan(-ratioUpLeftDiagnal))) {  //top
                            int edgex = (int) (((float) -(grf_Top - y1)) / -ratio);
                            g.drawString(keyLineString, x1 + edgex, grf_Top + 5);
                        } else { //  lefty
                            int edgey = (int) (((float) (grfLeft - x1)) * -ratio);
                            g.drawString(keyLineString, grfLeft, y1 - edgey);
                        }

                    } else {
                        if (Math.toDegrees(Math.atan(-ratio)) < Math.toDegrees(Math.atan(-ratioDownRightDiagnal))) {  //down right edge
                            int edgex = (int) (((float) -(grfDown - y1)) / (-ratio));
                            g.drawString(keyLineString, x1 + edgex, grfDown - 3);
                        } else { // right y edge
                            int edgY = (int) (((float) -(grfRight - x1)) * (-ratio)); //ok
                            g.drawString(keyLineString, grfRight - 20, y1 + edgY);
                        }
                    }

                }
                // ***********************the key line ******************/


            }
        }
        if (drawSelected) {
            g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
            g.setColor(Color.BLACK);
            g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
        }
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        if (!rect.contains(x, y) || !isMove) return false;
        indexArray = xArray;
        int x1, x2, y1, y2;
        x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);

        if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
            if (!objectMoving && (movingPtIndex == 0)) {
                objectMoving = true;
            }
            return true;
        }

        if (rect.contains(x, y)) {
            int tempX2, tempY2;
            if ((x1 == x2)) {
                tempX2 = x2;
                tempY2 = (y1 > y2) ? rect.y : rect.y + rect.height;
                if (isOnLineForMove(x1, y1, tempX2, tempY2, x, y, select)) {
                    return true;
                }
            } else {
                float ratio = (float) (y2 - y1) / (float) (x2 - x1);
                //int pixGap = (x2 > x1) ? extendX : -extendX;
                int pixGap = (x2 > x1) ? rect.width : -rect.width;
                tempX2 = x1 + pixGap;
                tempY2 = y1 + Math.round(ratio * pixGap);
                if (isOnLineForMove(x1, y1, tempX2, tempY2, x, y, select)) {
                    return true;
                }
                if (ratio != 0) {
                    for (int i = 0; i < GANN_ANGLES.length; i++) {
                        tempY2 = y1 + Math.round(ratio * GANN_ANGLES[i] * pixGap);
                        if (isOnLineForMove(x1, y1, tempX2, tempY2, x, y, select)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public String getShortName() {
        return Language.getString("OBJECT_GANN_FAN");
    }

    protected String getValueString(boolean currMode) {
        SimpleDateFormat timeFormatter;
        if (currMode) {
            timeFormatter = new SimpleDateFormat("HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }
        DecimalFormat indexFormat = new DecimalFormat("###,###0");
        DecimalFormat yValueFormat = new DecimalFormat("###,##0.00000");
        Date DT1 = new Date(xArray[0]);
        String val1 = yValueFormat.format(yArray[0]);
        String indexGap = indexFormat.format(indexArray[1] - indexArray[0]);
        String val2 = yValueFormat.format(yArray[1] - yArray[0]);
        return timeFormatter.format(DT1) + ", " + val1 + ", " +
                val2 + " x " + indexGap;
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {
        pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
        int y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);

        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[0]});
        int x1 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        int y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[1]});
        int x2 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        //if end points within rectangle
        if (r.contains(x1, y1)) {
            return true;
        }
        if (r.contains(x2, y2)) {
            return true;
        }

        double m = (y2 - y1) / (x2 - x1);
        double c = (y1 - m * x1);

        int x0 = r.x;

        double y = m * x0 + c;

        //m is postitve and intersects the rectangle
        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        x0 = x0 + r.width;
        y = m * x0 + c;

        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        //added later
        double x = (r.y - c) / m;

        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }

        x = ((r.y + r.height) - c) / m;
        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }
        return false;
    }

    public ArrayList<ToolTipDataRow> getToolTipRows() {
        return dataRows;
    }
}
